﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Workflows.Dto
{
    public class AdvancedPaymentRequestToBusinessTripAdvancedPaymentRequestDtoMapping : ClassMapping<AdvancedPaymentRequest, BusinessTripAdvancedPaymentRequestDto>
    {
        public AdvancedPaymentRequestToBusinessTripAdvancedPaymentRequestDtoMapping()
        {
            Mapping = e => new BusinessTripAdvancedPaymentRequestDto
            {
                Id = e.Id,
                Amount = e.Amount,
                CurrencyId = e.CurrencyId,
                CurrencySymbol = e.Currency.Symbol,
                Status = e.Request.Status,
                RequestorFirstName = e.Request.RequestingUser.FirstName,
                RequestorLastName = e.Request.RequestingUser.LastName,
                ParticipantFullName = e.Participant.Employee.FullName,
                BusinessTripId = e.BusinessTripId
            };
        }
    }
}
