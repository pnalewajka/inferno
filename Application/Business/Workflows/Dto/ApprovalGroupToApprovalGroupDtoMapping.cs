﻿using System.Linq;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Workflows.Dto
{
    public class ApprovalGroupToApprovalGroupDtoMapping : ClassMapping<ApprovalGroup, ApprovalGroupDto>
    {
        public ApprovalGroupToApprovalGroupDtoMapping(IClassMapping<Approval, ApprovalDto> approvalMapping)
        {
            Mapping = e => new ApprovalGroupDto
            {
                Id = e.Id,
                RequestId = e.RequestId,
                Mode = e.Mode,
                Approvals = e.Approvals.Select(approvalMapping.CreateFromSource).ToList(),
            };
        }
    }
}
