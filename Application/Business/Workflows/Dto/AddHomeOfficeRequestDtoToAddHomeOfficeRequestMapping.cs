﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Workflows.Dto
{
    public class AddHomeOfficeRequestDtoToAddHomeOfficeRequestMapping : ClassMapping<AddHomeOfficeRequestDto, AddHomeOfficeRequest>
    {
        public AddHomeOfficeRequestDtoToAddHomeOfficeRequestMapping()
        {
            Mapping = d => new AddHomeOfficeRequest
            {
                Id = d.Id,
                From = d.From,
                To = d.To,
            };
        }
    }
}
