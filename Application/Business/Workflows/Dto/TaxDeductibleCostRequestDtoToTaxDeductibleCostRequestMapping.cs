﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class TaxDeductibleCostRequestDtoToTaxDeductibleCostRequestMapping : ClassMapping<TaxDeductibleCostRequestDto, TaxDeductibleCostRequest>
    {
        public TaxDeductibleCostRequestDtoToTaxDeductibleCostRequestMapping()
        {
            Mapping = d => new TaxDeductibleCostRequest
            {
                Id = d.Id,
                AffectedUserId = d.AffectedUserId,
                Month = d.Month,
                Year = d.Year,
                ProjectId = d.ProjectId,
                WorkName = d.WorkName,
                Hours = d.Hours,
                Description = d.Description,
                UrlField = d.UrlField
            };
        }
    }
}