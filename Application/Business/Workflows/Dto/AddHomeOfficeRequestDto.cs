﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Business.Workflows.Interfaces;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class AddHomeOfficeRequestDto : IRequestObject
    {
        public long Id { get; set; }

        public DateTime From { get; set; }

        public DateTime To { get; set; }
    }
}
