﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class EmployeeDataChangeRequestToEmployeeDataChangeRequestDtoMapping : ClassMapping<EmployeeDataChangeRequest, EmployeeDataChangeRequestDto>
    {
        public EmployeeDataChangeRequestToEmployeeDataChangeRequestDtoMapping()
        {
            Mapping = e => new EmployeeDataChangeRequestDto
            {
                Id = e.Id,
                EffectiveOn = e.EffectiveOn,
                EmployeeIds = e.Employees.Select(s => s.Id).ToArray(),
                JobProfile = new Optional<long?>(e.IsSetJobProfile, e.JobProfileId),
                OrgUnit = new Optional<long?>(e.IsSetOrgUnit, e.OrgUnitId),
                OrgUnitNameTooltip = e.OrgUnit != null ? e.OrgUnit.Name : string.Empty,
                Location = new Optional<long?>(e.IsSetLocation, e.LocationId),
                PlaceOfWork = new Optional<PlaceOfWork?>(e.IsSetPlaceOfWork, e.PlaceOfWork),
                LineManager = new Optional<long?>(e.IsSetLineManager, e.LineManagerId)
            };
        }

        public override IEnumerable<string> NavigationProperties
        {
            get
            {
                // Optimize description
                return base.NavigationProperties.Union(
                    new[]
                    {
                        nameof(EmployeeDataChangeRequest.Employees),
                    });
            }
        }
    }
}
