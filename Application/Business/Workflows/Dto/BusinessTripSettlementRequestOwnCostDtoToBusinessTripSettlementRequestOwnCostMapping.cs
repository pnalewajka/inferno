﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BusinessTripSettlementRequestOwnCostDtoToBusinessTripSettlementRequestOwnCostMapping : ClassMapping<BusinessTripSettlementRequestOwnCostDto, BusinessTripSettlementRequestOwnCost>
    {
        public BusinessTripSettlementRequestOwnCostDtoToBusinessTripSettlementRequestOwnCostMapping()
        {
            Mapping = d => new BusinessTripSettlementRequestOwnCost
            {
                Description = d.Description,
                TransactionDate = d.TransactionDate,
                Amount = d.Amount,
                CurrencyId = d.CurrencyId,
                HasInvoiceIssued = d.HasInvoiceIssued,
            };
        }
    }
}
