﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BusinessTripSettlementRequestOwnCostToBusinessTripSettlementRequestOwnCostDtoMapping : ClassMapping<BusinessTripSettlementRequestOwnCost, BusinessTripSettlementRequestOwnCostDto>
    {
        public BusinessTripSettlementRequestOwnCostToBusinessTripSettlementRequestOwnCostDtoMapping()
        {
            Mapping = e => new BusinessTripSettlementRequestOwnCostDto
            {
                Description = e.Description,
                TransactionDate = e.TransactionDate,
                Amount = e.Amount,
                CurrencyId = e.CurrencyId,
                HasInvoiceIssued = e.HasInvoiceIssued,
            };
        }
    }
}
