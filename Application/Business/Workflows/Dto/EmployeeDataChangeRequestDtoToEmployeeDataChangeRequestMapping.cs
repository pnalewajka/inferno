﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class EmployeeDataChangeRequestDtoToEmployeeDataChangeRequestMapping : ClassMapping<EmployeeDataChangeRequestDto, EmployeeDataChangeRequest>
    {
        public EmployeeDataChangeRequestDtoToEmployeeDataChangeRequestMapping()
        {
            Mapping = d => new EmployeeDataChangeRequest
            {
                Id = d.Id,
                EffectiveOn = d.EffectiveOn,
                Employees = d.EmployeeIds != null ? d.EmployeeIds.Select(e => new Employee { Id = e }).ToList() : new List<Employee>(),
                IsSetJobProfile = d.JobProfile.IsSet,
                JobProfileId = d.JobProfile.Value,
                IsSetOrgUnit = d.OrgUnit.IsSet,
                OrgUnitId = d.OrgUnit.Value,
                IsSetLocation = d.Location.IsSet,
                LocationId = d.Location.Value,
                IsSetPlaceOfWork = d.PlaceOfWork.IsSet,
                PlaceOfWork = d.PlaceOfWork.Value,
                IsSetLineManager = d.LineManager.IsSet,
                LineManagerId = d.LineManager.Value
            };
        }
    }
}
