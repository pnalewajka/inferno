﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BusinessTripSettlementRequestCompanyCostDtoToBusinessTripSettlementRequestCompanyCostMapping : ClassMapping<BusinessTripSettlementRequestCompanyCostDto, BusinessTripSettlementRequestCompanyCost>
    {
        public BusinessTripSettlementRequestCompanyCostDtoToBusinessTripSettlementRequestCompanyCostMapping()
        {
            Mapping = d => new BusinessTripSettlementRequestCompanyCost
            {
                Description = d.Description,
                TransactionDate = d.TransactionDate,
                CurrencyId = d.CurrencyId,
                Amount = d.Amount,
                PaymentMethod = d.PaymentMethod,
                CardHolderEmployeeId = d.CardHolderEmployeeId,
                HasInvoiceIssued = d.HasInvoiceIssued,
            };
        }
    }
}
