﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Workflows.Dto
{
    public class AddEmployeeRequestDtoToAddEmployeeRequestMapping : ClassMapping<AddEmployeeRequestDto, AddEmployeeRequest>
    {
        public AddEmployeeRequestDtoToAddEmployeeRequestMapping()
        {
            Mapping = d => new AddEmployeeRequest
            {
                Id = d.Id,
                FirstName = d.FirstName,
                LastName = d.LastName,
                Email = d.Email,
                Login = d.Login,
                JobTitleId = d.JobTitleId,
                CrmId = d.CrmId,
                JobProfileId = d.JobProfileId,
                OrgUnitId = d.OrgUnitId,
                LocationId = d.LocationId,
                PlaceOfWork = d.PlaceOfWork,
                CompanyId = d.CompanyId,
                LineManagerId = d.LineManagerId,
                ContractFrom = d.ContractFrom,
                ContractTo = d.ContractTo,
                ContractSignDate = d.ContractSignDate,
                MondayHours = d.MondayHours,
                TuesdayHours = d.TuesdayHours,
                WednesdayHours = d.WednesdayHours,
                ThursdayHours = d.ThursdayHours,
                FridayHours = d.FridayHours,
                SaturdayHours = d.SaturdayHours,
                SundayHours = d.SundayHours,
                ContractTypeId = d.ContractTypeId,
                ContractDuration = d.ContractDuration,
                VacationHourToDayRatio = d.VacationHourToDayRatio,
                TimeReportingMode = d.TimeReportingMode,
                AbsenceOnlyDefaultProjectId = d.AbsenceOnlyDefaultProjectId,
                CalendarId = d.CalendarId
            };
        }
    }
}
