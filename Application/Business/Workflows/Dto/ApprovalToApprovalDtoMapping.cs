﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Workflows.Dto
{
    public class ApprovalToApprovalDtoMapping : ClassMapping<Approval, ApprovalDto>
    {
        public ApprovalToApprovalDtoMapping()
        {
            Mapping = e => new ApprovalDto
            {
                Id = e.Id,
                Status = e.Status,
                UserId = e.UserId,
                ApproverFullName = $"{e.User.FirstName} {e.User.LastName}",
                ApproverEmail = e.User.Email,
                ForcedByUserId = e.ForcedByUserId,
                ForcedByUserEmail = e.ForcedByUser != null ? e.ForcedByUser.Email : null,
                ForcedByUserFullName = e.ForcedByUser != null ? $"{e.ForcedByUser.FirstName} {e.ForcedByUser.LastName}" : null,
            };
        }
    }
}
