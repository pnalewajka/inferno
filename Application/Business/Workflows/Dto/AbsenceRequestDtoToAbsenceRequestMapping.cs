﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Workflows.Dto
{
    public class AbsenceRequestDtoToAbsenceRequestMapping : ClassMapping<AbsenceRequestDto, AbsenceRequest>
    {
        public AbsenceRequestDtoToAbsenceRequestMapping()
        {
            Mapping = d => new AbsenceRequest
            {
                Id = d.Id,
                AffectedUserId = d.AffectedUserId,
                AbsenceTypeId = d.AbsenceTypeId,
                From = d.From,
                To = d.To,
                Hours = d.Hours,
                SubstituteUserId = d.SubstituteUserId,
                Comment = d.Comment,
            };
        }
    }
}
