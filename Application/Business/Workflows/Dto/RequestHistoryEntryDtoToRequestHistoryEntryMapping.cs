﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class RequestHistoryDtoToRequestHistoryMapping : ClassMapping<RequestHistoryEntryDto, RequestHistoryEntry>
    {
        public RequestHistoryDtoToRequestHistoryMapping()
        {
            Mapping = d => new RequestHistoryEntry
            {
                Id = d.Id,
                Date = d.Date,
                Type = d.Type,
                ExecutingUserId = d.ExecutingUserId,
                ApprovingUserId = d.ApprovingUserId,
                ActionIdentifier = d.ActionIdentifier,
                JiraIssueKey = d.JiraIssueKey,
                EmailSubject = d.EmailSubject,
                EmailRecipients = d.EmailRecipients,
                WatcherNames = d.WatcherNames,
                RejectionReason = d.RejectionReason,
                IsActionActive = d.IsActionActive,
                UpdatesOn = d.UpdatesOn,
                RequestId = d.RequestId,
                Timestamp = d.Timestamp
            };
        }
    }
}
