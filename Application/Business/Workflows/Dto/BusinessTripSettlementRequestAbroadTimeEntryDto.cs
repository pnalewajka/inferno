﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BusinessTripSettlementRequestAbroadTimeEntryDto
    {
        public DateTime DepartureDateTime { get; set; }

        public long DepartureCountryId { get; set; }

        public long? DepartureCityId { get; set; }

        public BusinessTripDestinationType? DestinationType { get; set; }
    }
}
