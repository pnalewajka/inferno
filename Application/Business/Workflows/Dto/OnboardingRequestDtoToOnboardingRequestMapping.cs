﻿using System;
using System.Collections.ObjectModel;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class OnboardingRequestDtoToOnboardingRequestMapping : ClassMapping<OnboardingRequestDto, OnboardingRequest>
    {
        public OnboardingRequestDtoToOnboardingRequestMapping()
        {
            Mapping = d => new OnboardingRequest
            {
                Id = d.Id,
                LineManagerEmployeeId = d.LineManagerEmployeeId,
                RecruiterEmployeeId = d.RecruiterEmployeeId,
                FirstName = d.FirstName,
                LastName = d.LastName,
                Login = d.Login,
                SignedOn = d.SignedOn,
                StartDate = d.StartDate,
                LocationId = d.LocationId,
                PlaceOfWork = d.PlaceOfWork,
                CompanyId = d.CompanyId,
                JobTitleId = d.JobTitleId,
                JobProfileId = d.JobProfileId,
                ContractTypeId = d.ContractTypeId,
                ProbationPeriodContractTypeId = d.ProbationPeriodContractTypeId,
                Compensation = d.Compensation,
                ProbationPeriodCompensation = d.ProbationPeriodCompensation,
                CommentsForPayroll = d.CommentsForPayroll,
                OrgUnitId = d.OrgUnitId,
                CommentsForHiringManager = d.CommentsForHiringManager,
                IsForeignEmployee = d.IsForeignEmployee,
                IsComingFromReferralProgram = d.IsComingFromReferralProgram,
                ReferralProgramDetails = d.ReferralProgramDetails,
                IsRelocationPackageNeeded = d.IsRelocationPackageNeeded,
                RelocationPackageDetails = d.RelocationPackageDetails,
                OnboardingDate = d.OnboardingDate,
                OnboardingLocationId = d.OnboardingLocationId,
                WelcomeAnnounceEmailInformation = d.WelcomeAnnounceEmailInformation,
                WelcomeAnnounceEmailPhotos = new Collection<EmployeePhotoDocument>(),
                PayrollDocuments = new Collection<PayrollDocument>(),
                ForeignEmployeeDocuments = new Collection<ForeignEmployeeDocument>(),
                WelcomeAnnounceEmailPhotosSecret = d.WelcomeAnnounceEmailPhotosSecret.HasValue
                    ? d.WelcomeAnnounceEmailPhotosSecret.Value
                    : Guid.NewGuid(),
                PayrollDocumentsSecret = d.PayrollDocumentsSecret.HasValue
                    ? d.PayrollDocumentsSecret.Value
                    : Guid.NewGuid(),
                ForeignEmployeeDocumentsSecret = d.ForeignEmployeeDocumentsSecret.HasValue
                    ? d.ForeignEmployeeDocumentsSecret.Value
                    : Guid.NewGuid(),
                ResultingUserId = d.ResultingUserId
            };
        }
    }
}
