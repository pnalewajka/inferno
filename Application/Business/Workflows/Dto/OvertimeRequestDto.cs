﻿using Smt.Atomic.Business.Workflows.Interfaces;
using System;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class OvertimeRequestDto : IRequestObject
    {
        public long Id { get; set; }

        public long ProjectId { get; set; }

        public DateTime From { get; set; }

        public DateTime To { get; set; }

        public decimal HourLimit { get; set; }

        public string Comment { get; set; }
    }
}
