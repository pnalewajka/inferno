﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Workflows.Dto
{
    public class AdvancedPaymentRequestDtoToAdvancedPaymentRequestMapping : ClassMapping<AdvancedPaymentRequestDto, AdvancedPaymentRequest>
    {
        public AdvancedPaymentRequestDtoToAdvancedPaymentRequestMapping()
        {
            Mapping = d => new AdvancedPaymentRequest
            {
                Id = d.Id,
                Amount = d.Amount,
                CurrencyId = d.CurrencyId,
                BusinessTripId = d.BusinessTripId,
                ParticipantId = d.ParticipantId,
            };
        }
    }
}
