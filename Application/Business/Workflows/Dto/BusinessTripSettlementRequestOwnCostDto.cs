﻿using System;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BusinessTripSettlementRequestOwnCostDto : IBusinessTripSettlementRequestCost
    {
        public string Description { get; set; }

        public DateTime TransactionDate { get; set; }

        public decimal Amount { get; set; }

        public long CurrencyId { get; set; }

        public bool HasInvoiceIssued { get; set; }
    }
}
