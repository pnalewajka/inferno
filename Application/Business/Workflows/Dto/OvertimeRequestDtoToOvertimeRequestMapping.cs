﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Workflows.Dto
{
    public class OvertimeRequestDtoToOvertimeRequestMapping : ClassMapping<OvertimeRequestDto, OvertimeRequest>
    {
        public OvertimeRequestDtoToOvertimeRequestMapping()
        {
            Mapping = d => new OvertimeRequest
            {
                Id = d.Id,
                ProjectId = d.ProjectId,
                From = d.From,
                To = d.To,
                HourLimit = d.HourLimit,
                Comment = d.Comment,
            };
        }
    }
}
