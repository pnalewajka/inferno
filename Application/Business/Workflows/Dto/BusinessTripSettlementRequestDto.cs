﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BusinessTripSettlementRequestDto : IRequestObject
    {
        public long Id { get; set; }

        public long BusinessTripId { get; set; }

        public long BusinessTripParticipantId { get; set; }

        public bool IsEmpty { get; set; }

        public long? DefaultCurrencyId { get; set; }

        public string DefaultCurrencyName { get; set; }

        public DateTime? SettlementDate { get; set; }

        public DateTime? DepartureDateTime { get; set; }

        public DateTime? ArrivalDateTime { get; set; }

        public decimal HoursWorkedForClient { get; set; }

        public decimal? SimplifiedPrivateVehicleMileage { get; set; }

        public MileageUnit? PreferredDistanceUnit { get; set; }

        public ICollection<BusinessTripSettlementRequestAbroadTimeEntryDto> AbroadTimeEntries { get; set; }

        public ICollection<BusinessTripSettlementRequestMealDto> Meals { get; set; }

        public ICollection<BusinessTripSettlementRequestOwnCostDto> OwnCosts { get; set; }

        public ICollection<BusinessTripSettlementRequestCompanyCostDto> CompanyCosts { get; set; }

        public ICollection<BusinessTripSettlementRequestAdvancePaymentDto> AdditionalAdvancePayments { get; set; }

        public BusinessTripSettlementRequestVehicleMileageDto PrivateVehicleMileage { get; set; }

        public string CalculationResult { get; set; }

        public BusinessTripSettlementControllingStatus ControllingStatus { get; set; }

        public long? DefaultCardHolderEmployeeId { get; set; }

        public string DefaultCardHolderEmployeeName { get; set; }

        public BusinessTripSettlementRequestDto()
        {
            AbroadTimeEntries = new BusinessTripSettlementRequestAbroadTimeEntryDto[0];
            Meals = new BusinessTripSettlementRequestMealDto[0];
            OwnCosts = new BusinessTripSettlementRequestOwnCostDto[0];
            CompanyCosts = new BusinessTripSettlementRequestCompanyCostDto[0];
            AdditionalAdvancePayments = new BusinessTripSettlementRequestAdvancePaymentDto[0];
        }
    }
}
