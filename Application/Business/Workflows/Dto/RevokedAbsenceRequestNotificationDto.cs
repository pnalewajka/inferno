﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.Business.Workflows.EmailModels;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class RevokedAbsenceRequestNotificationDto
    {
        public string RequesterFullName { get; set; }

        public DateTime From { get; set; }

        public DateTime To { get; set; }

        public decimal Hours { get; set; }

        public string AbsenceName { get; set; }

        public string CompanyName { get; set; }

        public string ContractType { get; set; }

        public string AffectedUserFullName { get; set; }
    }
}
