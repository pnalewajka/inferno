﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class OnboardingRequestToOnboardingRequestDtoMapping : ClassMapping<OnboardingRequest, OnboardingRequestDto>
    {
        public OnboardingRequestToOnboardingRequestDtoMapping()
        {
            Mapping = e => new OnboardingRequestDto
            {
                Id = e.Id,
                LineManagerEmployeeId = e.LineManagerEmployeeId,
                RecruiterEmployeeId = e.RecruiterEmployeeId,
                FirstName = e.FirstName,
                LastName = e.LastName,
                Login = e.Login,
                SignedOn = e.SignedOn,
                StartDate = e.StartDate,
                LocationId = e.LocationId,
                PlaceOfWork = e.PlaceOfWork,
                CompanyId = e.CompanyId,
                JobTitleId = e.JobTitleId,
                JobProfileId = e.JobProfileId,
                ContractTypeId = e.ContractTypeId,
                ProbationPeriodContractTypeId = e.ProbationPeriodContractTypeId,
                Compensation = e.Compensation,
                ProbationPeriodCompensation = e.ProbationPeriodCompensation,
                CommentsForPayroll = e.CommentsForPayroll,
                OrgUnitId = e.OrgUnitId,
                CommentsForHiringManager = e.CommentsForHiringManager,
                IsForeignEmployee = e.IsForeignEmployee,
                IsComingFromReferralProgram = e.IsComingFromReferralProgram,
                ReferralProgramDetails = e.ReferralProgramDetails,
                IsRelocationPackageNeeded = e.IsRelocationPackageNeeded,
                RelocationPackageDetails = e.RelocationPackageDetails,
                OnboardingDate = e.OnboardingDate,
                OnboardingLocationId = e.OnboardingLocationId,
                WelcomeAnnounceEmailInformation = e.WelcomeAnnounceEmailInformation,
                WelcomeAnnounceEmailPhotos = e.WelcomeAnnounceEmailPhotos.Select(p => new DocumentDto
                {
                    DocumentId = p.Id,
                    ContentType = p.ContentType,
                    DocumentName = p.Name,
                }).ToArray(),
                PayrollDocuments = e.PayrollDocuments.Select(d => new DocumentDto
                {
                    DocumentId = d.Id,
                    ContentType = d.ContentType,
                    DocumentName = d.Name,
                }).ToArray(),
                ForeignEmployeeDocuments = e.ForeignEmployeeDocuments.Select(d => new DocumentDto
                {
                    DocumentId = d.Id,
                    ContentType = d.ContentType,
                    DocumentName = d.Name,
                }).ToArray(),
                WelcomeAnnounceEmailPhotosSecret = e.WelcomeAnnounceEmailPhotosSecret,
                PayrollDocumentsSecret = e.PayrollDocumentsSecret,
                ForeignEmployeeDocumentsSecret = e.ForeignEmployeeDocumentsSecret,
                ResultingUserId = e.ResultingUserId
            };
        }
    }
}
