﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using System.Collections.Generic;
using System.Linq;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class EmploymentTerminationRequestToEmploymentTerminationRequestDtoMapping : ClassMapping<EmploymentTerminationRequest, EmploymentTerminationRequestDto>
    {
        public EmploymentTerminationRequestToEmploymentTerminationRequestDtoMapping()
        {
            Mapping = e => new EmploymentTerminationRequestDto
            {
                Id = e.Id,
                EmployeeId = e.EmployeeId,
                TerminationDate = e.TerminationDate,
                EndDate = e.EndDate,
                ReasonId = e.ReasonId,
                BackupData = e.BackupData
            };
        }

        public override IEnumerable<string> NavigationProperties
        {
            get
            {
                // Optimize description
                return base.NavigationProperties.Union(
                    new[]
                    {
                        nameof(EmploymentTerminationRequest.Employee),
                    });
            }
        }
    }
}
