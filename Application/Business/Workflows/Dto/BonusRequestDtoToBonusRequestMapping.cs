﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BonusRequestDtoToBonusRequestMapping : ClassMapping<BonusRequestDto, BonusRequest>
    {
        public BonusRequestDtoToBonusRequestMapping()
        {
            Mapping = d => new BonusRequest
            {
                Id = d.Id,
                StartDate = d.StartDate,
                EndDate = d.EndDate,
                ProjectId = d.ProjectId,
                EmployeeId = d.EmployeeId,
                BonusAmount = d.BonusAmount,
                CurrencyId = d.CurrencyId,
                Justification = d.Justification,
                Type = d.Type,
                Reinvoice = d.Reinvoice
            };
        }
    }
}