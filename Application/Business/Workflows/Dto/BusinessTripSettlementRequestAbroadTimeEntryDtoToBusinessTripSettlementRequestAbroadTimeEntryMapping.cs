﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BusinessTripSettlementRequestAbroadTimeEntryDtoToBusinessTripSettlementRequestAbroadTimeEntryMapping : ClassMapping<BusinessTripSettlementRequestAbroadTimeEntryDto, BusinessTripSettlementRequestAbroadTimeEntry>
    {
        public BusinessTripSettlementRequestAbroadTimeEntryDtoToBusinessTripSettlementRequestAbroadTimeEntryMapping()
        {
            Mapping = d => new BusinessTripSettlementRequestAbroadTimeEntry
            {
                DepartureDateTime = d.DepartureDateTime,
                DepartureCountryId = d.DepartureCountryId,
                DepartureCityId = d.DepartureCityId,
                DestinationType = d.DestinationType,
            };
        }
    }
}
