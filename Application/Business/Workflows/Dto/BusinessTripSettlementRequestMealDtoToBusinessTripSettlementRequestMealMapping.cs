﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BusinessTripSettlementRequestMealDtoToBusinessTripSettlementRequestMealMapping : ClassMapping<BusinessTripSettlementRequestMealDto, BusinessTripSettlementRequestMeal>
    {
        public BusinessTripSettlementRequestMealDtoToBusinessTripSettlementRequestMealMapping()
        {
            Mapping = d => new BusinessTripSettlementRequestMeal
            {
                CountryId = d.CountryId,
                Day = d.Day,
                BreakfastCount = d.BreakfastCount,
                LunchCount = d.LunchCount,
                DinnerCount = d.DinnerCount,
            };
        }
    }
}
