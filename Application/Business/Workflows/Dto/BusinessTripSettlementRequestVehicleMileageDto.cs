﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BusinessTripSettlementRequestVehicleMileageDto
    {
        public VehicleType VehicleType { get; set; }

        public bool IsCarEngineCapacityOver900cc { get; set; }

        public string RegistrationNumber { get; set; }

        public long OutboundFromCityId { get; set; }

        public long OutboundToCityId { get; set; }

        public decimal OutboundKilometers { get; set; }

        public long InboundFromCityId { get; set; }

        public long InboundToCityId { get; set; }

        public decimal InboundKilometers { get; set; }
    }
}