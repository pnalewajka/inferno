﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Workflows.Dto
{
    public class SubstitutionDtoToSubstitutionMapping : ClassMapping<SubstitutionDto, Substitution>
    {
        public SubstitutionDtoToSubstitutionMapping()
        {
            Mapping = d => new Substitution
            {
                Id = d.Id,
                ReplacedUserId = d.ReplacedUserId,
                SubstituteUserId = d.SubstituteUserId,
                From = d.From,
                To = d.To,
                RequestId = d.RequestId,
                Timestamp = d.Timestamp
            };
        }
    }
}
