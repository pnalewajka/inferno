﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class ActionSetToActionSetDtoMapping : ClassMapping<ActionSet, ActionSetDto>
    {
        public ActionSetToActionSetDtoMapping()
        {
            Mapping = e => new ActionSetDto
            {
                Id = e.Id,
                Code = e.Code,
                Description = e.Description,
                TriggeringRequestType = e.TriggeringRequestType,
                TriggeringRequestStatusIds = e.TriggeringRequestStatuses.Select(s => (long)s.RequestStatus).ToArray(),
                Definition = e.Definition,
                Timestamp = e.Timestamp
            };
        }
    }
}
