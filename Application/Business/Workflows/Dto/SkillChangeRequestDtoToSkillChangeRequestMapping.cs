﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using System.Linq;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using System.Collections.Generic;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Data.Repositories.Helpers;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class SkillChangeRequestDtoToSkillChangeRequestMapping : ClassMapping<SkillChangeRequestDto, SkillChangeRequest>
    {
        public SkillChangeRequestDtoToSkillChangeRequestMapping()
        {
            Mapping = d => new SkillChangeRequest
            {
                Id = d.Id,
                EmployeeId = d.EmployeeId,
                Skills = d.SkillIds != null ? d.SkillIds.Select(s => new Skill { Id = s }).ToList() : new List<Skill>(),
            };
        }
    }
}
