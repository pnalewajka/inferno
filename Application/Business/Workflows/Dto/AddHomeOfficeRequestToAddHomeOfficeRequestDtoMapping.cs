﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Workflows.Dto
{
    public class AddHomeOfficeRequestToAddHomeOfficeRequestDtoMapping : ClassMapping<AddHomeOfficeRequest, AddHomeOfficeRequestDto>
    {
        public AddHomeOfficeRequestToAddHomeOfficeRequestDtoMapping()
        {
            Mapping = e => new AddHomeOfficeRequestDto
            {
                Id = e.Id,
                From = e.From,
                To = e.To,
            };
        }
    }
}
