﻿using System;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BusinessTripSettlementRequestMealDto
    {
        public long? CountryId { get; set; }

        public string CountryName { get; set; }

        public DateTime? Day { get; set; }

        public long BreakfastCount { get; set; }

        public long LunchCount { get; set; }

        public long DinnerCount { get; set; }
    }
}
