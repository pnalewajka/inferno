﻿using System.Collections.ObjectModel;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class FeedbackRequestDtoToFeedbackRequestMapping : ClassMapping<FeedbackRequestDto, FeedbackRequest>
    {
        public FeedbackRequestDtoToFeedbackRequestMapping()
        {
            Mapping = d => new FeedbackRequest
            {
                Id = d.Id,
                EvaluatedEmployeeId = d.EvaluatedEmployeeId,
                EvaluatorEmployeeId = d.EvaluatorEmployeeId,
                FeedbackContent = d.FeedbackContent,
                Comment = d.Comment,
                Visibility = d.Visibility,
                OriginScenario = d.OriginScenario,
                EvaluatorEmployees = d.EvaluatorEmployeeIds == null ? null : new Collection<Employee>(d.EvaluatorEmployeeIds.Select(m => new Employee { Id = m }).ToList())
            };
        }
    }
}
