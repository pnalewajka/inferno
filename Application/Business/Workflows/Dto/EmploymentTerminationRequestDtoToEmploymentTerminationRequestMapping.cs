﻿using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Workflows.Dto
{
    public class EmploymentTerminationRequestDtoToEmploymentTerminationRequestMapping : ClassMapping<EmploymentTerminationRequestDto, EmploymentTerminationRequest>
    {
        public EmploymentTerminationRequestDtoToEmploymentTerminationRequestMapping()
        {
            Mapping = d => new EmploymentTerminationRequest
            {
                Id = d.Id,
                EmployeeId = d.EmployeeId,
                TerminationDate = d.TerminationDate,
                EndDate = d.EndDate,
                ReasonId = d.ReasonId,
                BackupData = d.BackupData
            };
        }
    }
}
