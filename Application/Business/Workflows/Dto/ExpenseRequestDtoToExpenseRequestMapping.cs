﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class ExpenseRequestDtoToExpenseRequestMapping : ClassMapping<ExpenseRequestDto, ExpenseRequest>
    {
        public ExpenseRequestDtoToExpenseRequestMapping()
        {
            Mapping = d => new ExpenseRequest
            {
                Id = d.Id,
                ProjectId = d.ProjectId,
                Date = d.Date,
                EmployeeId = d.EmployeeId,
                Amount = d.Amount,
                CurrencyId = d.CurrencyId,
                Justification = d.Justification
            };
        }
    }
}
