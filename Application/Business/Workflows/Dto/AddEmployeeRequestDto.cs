﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Business.Workflows.Interfaces;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class AddEmployeeRequestDto : IRequestObject
    {
        public long Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Login { get; set; }

        public long JobTitleId { get; set; }

        public string CrmId { get; set; }

        public long JobProfileId { get; set; }

        public long OrgUnitId { get; set; }

        public long LocationId { get; set; }

        public PlaceOfWork PlaceOfWork { get; set; }

        public long CompanyId { get; set; }

        public long LineManagerId { get; set; }

        public DateTime ContractFrom { get; set; }

        public DateTime? ContractTo { get; set; }

        public DateTime ContractSignDate { get; set; }

        public decimal MondayHours { get; set; }

        public decimal TuesdayHours { get; set; }

        public decimal WednesdayHours { get; set; }

        public decimal ThursdayHours { get; set; }

        public decimal FridayHours { get; set; }

        public decimal SaturdayHours { get; set; }

        public decimal SundayHours { get; set; }

        public long ContractTypeId { get; set; }

        public ContractDurationType? ContractDuration { get; set; }

        public decimal VacationHourToDayRatio { get; set; }

        public TimeReportingMode TimeReportingMode { get; set; }

        public long? AbsenceOnlyDefaultProjectId { get; set; }

        public long? CalendarId { get; set; }
    }
}
