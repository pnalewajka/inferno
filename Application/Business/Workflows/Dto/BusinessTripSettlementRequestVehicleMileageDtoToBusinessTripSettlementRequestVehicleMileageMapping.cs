﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BusinessTripSettlementRequestVehicleMileageDtoToBusinessTripSettlementRequestVehicleMileageMapping : ClassMapping<BusinessTripSettlementRequestVehicleMileageDto, BusinessTripSettlementRequestVehicleMileage>
    {
        public BusinessTripSettlementRequestVehicleMileageDtoToBusinessTripSettlementRequestVehicleMileageMapping()
        {
            Mapping = d => new BusinessTripSettlementRequestVehicleMileage
            {
                VehicleType = d.VehicleType,
                IsCarEngineCapacityOver900cc = d.IsCarEngineCapacityOver900cc,
                RegistrationNumber = d.RegistrationNumber,
                OutboundFromCityId = d.OutboundFromCityId,
                OutboundToCityId = d.OutboundToCityId,
                OutboundKilometers = d.OutboundKilometers,
                InboundFromCityId = d.InboundFromCityId,
                InboundToCityId = d.InboundToCityId,
                InboundKilometers = d.InboundKilometers,
            };
        }
    }
}
