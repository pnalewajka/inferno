﻿using System;

namespace Smt.Atomic.Business.Workflows.Dto
{
    public class BusinessTripSettlementRequestAdvancePaymentDto
    {
        public long CurrencyId { get; set; }

        public decimal Amount { get; set; }

        public DateTime WithdrawnOn { get; set; }
    }
}
