﻿namespace Smt.Atomic.Business.Workflows.Consts
{
    public static class FilterCodes
    {
        public const string RequestType = "request-type";
        public const string CurrentMonth = "current-month";
        public const string PreviousMonth = "previous-month";
        public const string DateRange = "date-range";
        public const string RequesterCompany = "requester-company";
        public const string RequesterOrgUnit = "requester-orgUnit";
        public const string MyOpenRequests = "my-requests";
        public const string MyClosedRequests = "my-closed-requests";
        public const string DecisionDone = "decision-done";
        public const string WatchedByMe = "watched-by-me";
        public const string RequestsRequestedByMe = "by-me";
        public const string DecisionAwaiting = "decision-awaiting";
        public const string AllRequests = "all-requests";
        public const string ContractType = "contract-type";
        public const string Attachments = "attachments";
        public const string AbsenceRequestType = "absence-request-type";
        public const string AffectedUser = "affected-user";
    }
}
