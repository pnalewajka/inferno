﻿using System.Collections.Generic;
using System.Globalization;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using System.Linq;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Workflows.EmailModels;
using Smt.Atomic.Business.Workflows.Enums;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Business.Configuration.Interfaces;

namespace Smt.Atomic.Business.Workflows.Services
{
    public class EmailWorkflowNotificationService : IWorkflowNotificationService
    {
        private readonly IReliableEmailService _reliableEmailService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IMessageTemplateService _messageTemplateService;

        public EmailWorkflowNotificationService(
            IReliableEmailService reliableEmailService,
            ISystemParameterService systemParameterService,
            IMessageTemplateService messageTemplateService)
        {
            _reliableEmailService = reliableEmailService;
            _systemParameterService = systemParameterService;
            _messageTemplateService = messageTemplateService;
        }

        public void NotifyAboutForcedAction(RequestDto request, OnBehalfAction action, IList<ApprovalDto> affectedApprovals)
        {
            var email = new EmailMessageDto
            {
                From = GetPortalAddress(),
            };

            var approval = affectedApprovals.First();
            var emailRecipients = GetForcedActionRecipients(request, approval);
            email.Recipients.AddRange(emailRecipients);

            var model = new RequestEmailDto(request)
            {
                Action = action,
                ApproverFullName = approval.ApproverFullName,
                ForcedByUserFullName = approval.ForcedByUserFullName,
            };

            SetSubjectAndBody(
                email,
                TemplateCodes.WorkflowsForcedActionSubject,
                TemplateCodes.WorkflowsForcedActionBody,
                model);

            Send(email);
        }

        public void NotifyAboutApproval(IRequestService requestService, RequestDto request)
        {
            var fromAddress = GetPortalAddress();
            var emailRecipient = CreateRequesterRecipient(request);

            var email = new EmailMessageDto
            {
                From = fromAddress,
            };

            var model = new RequestEmailDto(request)
            {
                AdditionalContent = requestService.ApprovalNotificationAdditionalContent(request),
            };

            SetSubjectAndBody(
                email,
                TemplateCodes.WorkflowsRequestApprovedMessageSubject,
                requestService.ApprovalNotificationEmailBodyTemplateCode,
                model);

            email.Recipients.Add(emailRecipient);
            email.Recipients.AddRange(requestService.GetExtraWorkflowNotificationRecipients(request));
            Send(email);
        }

        public void NotifyAboutException(IRequestService requestService, RequestDto request)
        {
            var fromAddress = GetPortalAddress();
            var recipients = requestService.GetExceptionNotificationRecipients(request);

            var email = new EmailMessageDto
            {
                From = fromAddress,
            };

            var model = new RequestEmailDto(request)
            {
                AdditionalContent = requestService.ExceptionNotificationAdditionalContent(request),
            };

            SetSubjectAndBody(
                email,
                TemplateCodes.WorkflowsExecutionExceptionMessageSubject,
                requestService.ExceptionNotificationEmailBodyTemplateCode,
                model);

            email.Recipients.AddRange(recipients);
            email.Recipients.AddRange(requestService.GetExtraWorkflowNotificationRecipients(request));
            Send(email);
        }

        public void NotifyAboutRejection(IRequestService requestService, RequestDto request)
        {
            var fromAddress = GetPortalAddress();
            var emailRecipient = CreateRequesterRecipient(request);

            var email = new EmailMessageDto
            {
                From = fromAddress,
            };

            var rejecterFullNames =
                string.Join(", ",
                    request.ApprovalGroups.SelectMany(g => g.Approvals.Where(a => a.Status == ApprovalStatus.Rejected))
                        .DistinctBy(a => a.UserId)
                        .Select(a => a.ApproverFullName)
                        .ToList());

            var model = new RequestEmailDto(request)
            {
                RejecterFullNames = rejecterFullNames,
                AdditionalContent = requestService.RejectionNotificationAdditionalContent(request),
            };

            SetSubjectAndBody(
                email,
                TemplateCodes.WorkflowsRequestRejectedMessageSubject,
                requestService.RejectionNotificationEmailBodyTemplateCode,
                model);

            email.Recipients.Add(emailRecipient);
            email.Recipients.AddRange(requestService.GetExtraWorkflowNotificationRecipients(request));
            Send(email);
        }

        public void NotifyAboutPendingApproval(IRequestService requestService, RequestDto request)
        {
            var recipients = CreateApproverRecipients(request);

            if (recipients.Any())
            {
                var fromAddress = GetPortalAddress();

                var email = new EmailMessageDto
                {
                    From = fromAddress,
                };

                var model = new RequestEmailDto(request)
                {
                    AdditionalContent = requestService.PendingApprovalNotificationAdditionalContent(request),
                };

                SetSubjectAndBody(
                    email,
                    TemplateCodes.WorkflowsNotifyApproversMessageSubject,
                    requestService.PendingApprovalNotificationEmailBodyTemplateCode,
                    model);

                email.Recipients.AddRange(recipients);
                Send(email);
            }
        }

        public void NotifyAboutExecution(IRequestService requestService, RequestDto request)
        {
            var fromAddress = GetPortalAddress();
            var recipient = CreateRequesterRecipient(request);

            var email = new EmailMessageDto
            {
                From = fromAddress,
            };

            var model = new RequestEmailDto(request)
            {
                AdditionalContent = requestService.ExecutionNotificationAdditionalContent(request),
            };

            SetSubjectAndBody(
                email,
                TemplateCodes.WorkflowsRequestExecutedMessageSubject,
                requestService.ExecutionNotificationEmailBodyTemplateCode,
                model);

            email.Recipients.Add(recipient);
            email.Recipients.AddRange(requestService.GetExtraWorkflowNotificationRecipients(request));
            Send(email);
        }

        public void NotifyAboutPendingRequests(UserDto user, IEnumerable<RequestDto> requests)
        {
            var fromAddress = GetPortalAddress();
            var emailRecipient = CreateRequesterRecipient(user);

            var email = new EmailMessageDto
            {
                From = fromAddress,
            };

            var requestEmails = requests.Select(d => new RequestEmailDto(d));
            var model = new RequestsReminderEmailDto(user, requestEmails);

            SetSubjectAndBody(
                email,
                TemplateCodes.WorkflowsPendingRequestsReminderMessageSubject,
                TemplateCodes.WorkflowsPendingRequestsReminderMessageBody,
                model);

            email.Recipients.Add(emailRecipient);
            Send(email);
        }

        public void NotifyAboutNotSubmittedRequests(UserDto user, IEnumerable<RequestDto> requests)
        {
            var fromAddress = GetPortalAddress();
            var emailRecipient = CreateRequesterRecipient(user);

            var email = new EmailMessageDto
            {
                From = fromAddress,
            };

            var requestEmails = requests.Select(d => new RequestEmailDto(d));
            var model = new RequestsReminderEmailDto(user, requestEmails);

            SetSubjectAndBody(
                email,
                TemplateCodes.WorkflowsNotSubmittedRequestsReminderMessageSubject,
                TemplateCodes.WorkflowsNotSubmittedRequestsReminderMessageBody,
                model);

            email.Recipients.Add(emailRecipient);
            Send(email);
        }

        private void Send(EmailMessageDto message)
        {
            if (message.Recipients.Count > 0)
            {
                _reliableEmailService.EnqueueMessage(message);
            }
        }

        private static EmailRecipientDto CreateRequesterRecipient(RequestDto request)
        {
            return new EmailRecipientDto
            {
                EmailAddress = request.RequesterEmail,
                FullName = request.RequesterFullName
            };
        }

        private static EmailRecipientDto CreateRequesterRecipient(UserDto user)
        {
            return new EmailRecipientDto
            {
                EmailAddress = user.Email,
                FullName = user.GetFullName()
            };
        }

        private static EmailRecipientDto[] CreateApproverRecipients(RequestDto request)
        {
            if (!request.CurrentApprovalGroupId.HasValue)
            {
                return new EmailRecipientDto[0];
            }

            var approvers = request.ApprovalGroups
                .Single(g => g.Id == request.CurrentApprovalGroupId).Approvals
                .GroupBy(a => a.UserId).Select(a => a.First());

            return approvers.Select(approval => new EmailRecipientDto
            {
                EmailAddress = approval.ApproverEmail,
                FullName = approval.ApproverFullName
            }).ToArray();
        }

        private EmailRecipientDto[] GetForcedActionRecipients(RequestDto request, ApprovalDto approval)
        {
            var recipients = new List<EmailRecipientDto>
            {
                new EmailRecipientDto
                {
                    EmailAddress = approval.ApproverEmail,
                    FullName = approval.ApproverEmail
                },
                new EmailRecipientDto
                {
                    EmailAddress = request.RequesterEmail,
                    FullName = request.RequesterFullName
                }
            }.ToArray();

            return recipients;
        }

        private EmailRecipientDto GetPortalAddress()
        {
            return new EmailRecipientDto
            {
                EmailAddress = _systemParameterService.GetParameter<string>(ParameterKeys.SmtpEmailServiceDefaultFromEmailAddress),
                FullName = _systemParameterService.GetParameter<string>(ParameterKeys.SmtpEmailServiceDefaultFromFullName)
            };
        }

        private void SetSubjectAndBody(
            EmailMessageDto email,
            string subjectTemplateCode,
            string bodyTemplateCode,
            object modelDto)
        {
            var recipientsCulture = new CultureInfo("en-US");

            using (CultureInfoHelper.SetCurrentCulture(recipientsCulture)) // Date, numbers, etc.. formatting
            using (CultureInfoHelper.SetCurrentUICulture(recipientsCulture)) // Resource language selection
            {
                // Mapping to model is automatically done internally
                var emailSubject = _messageTemplateService.ResolveTemplate(subjectTemplateCode, modelDto);
                var emailBody = _messageTemplateService.ResolveTemplate(bodyTemplateCode, modelDto);

                email.Subject = emailSubject.Content;
                email.IsHtml = emailBody.IsHtml;
                email.Body = emailBody.Content;
            }
        }
    }
}
