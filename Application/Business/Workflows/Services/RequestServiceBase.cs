﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Workflows.BusinessLogics;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Enums;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Workflows.Services
{
    public abstract class RequestServiceBase<TEntity, TDto> : IRequestService
        where TEntity : class, IRequestEntity
        where TDto : class, IRequestObject, new()
    {
        protected readonly IActionSetService ActionSetService;
        protected readonly IPrincipalProvider PrincipalProvider;
        protected readonly IClassMappingFactory ClassMappingFactory;
        protected readonly IWorkflowNotificationService WorkflowNotificationService;
        protected readonly IRequestWorkflowService RequestWorkflowService;
        protected readonly ISystemParameterService SystemParameterService;

        public virtual bool ShouldStartAsDraft => false;

        protected RequestServiceBase(
            IActionSetService actionSetService,
            IPrincipalProvider principalProvider,
            IClassMappingFactory classMappingFactory,
            IWorkflowNotificationService workflowNotificationService,
            IRequestWorkflowService requestWorkflowService,
            ISystemParameterService systemParameterService)
        {
            ActionSetService = actionSetService;
            PrincipalProvider = principalProvider;
            ClassMappingFactory = classMappingFactory;
            WorkflowNotificationService = workflowNotificationService;
            RequestWorkflowService = requestWorkflowService;
            SystemParameterService = systemParameterService;
        }

        public abstract RequestType RequestType { get; }
        public abstract RequestTypeGroup RequestTypeGroup { get; }
        public abstract string RequestName { get; }
        public Type DtoType => typeof(TDto);
        public Type EntityType => typeof(TEntity);

        protected virtual string ViewModelAreaName
        {
            get
            {
                const string prefix = "Smt.Atomic.Business.";
                const string suffix = ".Workflows.Services";

                var serviceNamespace = GetType().Namespace;

                if (serviceNamespace == null || !serviceNamespace.StartsWith(prefix) || !serviceNamespace.EndsWith(suffix))
                {
                    throw new InvalidOperationException();
                }

                return serviceNamespace.Replace(prefix, string.Empty).Replace(suffix, string.Empty);
            }
        }

        public virtual string ViewModelTypeName => $"Smt.Atomic.WebApp.Areas.{ViewModelAreaName}.Models.{typeof(TDto).Name.Replace("Dto", "ViewModel")}";

        public abstract SecurityRoleType? RequiredRoleType { get; }

        public abstract void Execute(RequestDto request, TDto requestDto);

        public abstract string GetRequestDescription(RequestDto request, TDto data);

        public abstract IEnumerable<ApproverGroupDto> GetApproverGroups(RequestDto request, TDto requestDto);

        public abstract IQueryable<Request> ConfigureIncludes(IQueryable<Request> repository);

        public virtual string ApprovalNotificationAdditionalContent(RequestDto request) => null;

        public virtual string RejectionNotificationAdditionalContent(RequestDto request) => null;

        public virtual string ExecutionNotificationAdditionalContent(RequestDto request) => null;

        public virtual string ExceptionNotificationAdditionalContent(RequestDto request) => null;

        public virtual string PendingApprovalNotificationAdditionalContent(RequestDto request) => null;

        public virtual string ApprovalNotificationEmailBodyTemplateCode => TemplateCodes.WorkflowsRequestApprovedMessageBody;

        public virtual string RejectionNotificationEmailBodyTemplateCode => TemplateCodes.WorkflowsRequestRejectedMessageBody;

        public virtual string ExecutionNotificationEmailBodyTemplateCode => TemplateCodes.WorkflowsRequestExecutedMessageBody;

        public virtual string ExceptionNotificationEmailBodyTemplateCode => TemplateCodes.WorkflowsExecutionExceptionMessageBody;

        public virtual string PendingApprovalNotificationEmailBodyTemplateCode => TemplateCodes.WorkflowsNotifyApproversMessageBody;

        public virtual IEnumerable<long> GetBusinessWatchersUserIds(RequestDto request, TDto requestDto)
        {
            return Enumerable.Empty<long>();
        }

        public virtual bool CanBeStartedManually()
        {
            return true;
        }

        public virtual TDto GetDraft(IDictionary<string, object> parameters)
        {
            return new TDto();
        }

        public virtual bool ShouldExecuteAsAsync(RequestDto request, TDto requestDto)
        {
            return false;
        }
        
        public virtual IEnumerable<AlertDto> Validate(RequestDto request, TDto requestDto)
        {
            yield break;
        }

        protected void ValidateCurrentPrincipalHasEmployee()
        {
            if (!PrincipalProvider.Current.Id.HasValue
                || PrincipalProvider.Current.Id <= 0
                || PrincipalProvider.Current.EmployeeId <= 0)
            {
                throw new BusinessException(WorkflowResources.CannotCreateRequestBecauseEmployeeNotDefined);
            }
        }

        public virtual void OnForcedAction(RequestDto request, OnBehalfAction action, IList<ApprovalDto> affectedApprovals)
        {
            WorkflowNotificationService.NotifyAboutForcedAction(request, action, affectedApprovals);
        }

        public virtual void OnAdding(RecordAddingEventArgs<Request, RequestDto, IWorkflowsDbScope> addingEventArgs)
        {
        }

        public virtual void OnAdded(RecordAddedEventArgs<Request, RequestDto> addedEventArgs)
        {
            if (ForceRequestSubmitOnAdd(addedEventArgs.InputEntity, addedEventArgs.InputDto))
            {
                var result = RequestWorkflowService.Submit(addedEventArgs.InputEntity.Id);
                addedEventArgs.Alerts.AddRange(result.Alerts);
            }
        }

        public virtual bool ForceRequestSubmitOnAdd(Request request, RequestDto requestDto)
        {
            if (!ShouldStartAsDraft)
            {
                return true;
            }

            return requestDto.SaveActionType == RequestSaveAction.SaveAndSubmit;
        }

        public virtual bool ForceRequestSubmitOnEdit(Request request, RequestDto requestDto)
        {
            return requestDto.SaveActionType == RequestSaveAction.SaveAndSubmit && request.Status == RequestStatus.Draft;
        }

        public virtual void OnAllApproved(Request request, RequestDto requestDto)
        {
        }

        public virtual void OnRejected(Request request, RequestDto requestDto)
        {
            WorkflowNotificationService.NotifyAboutRejection(this, requestDto);
        }

        public virtual void OnSubmited(Request request, RequestDto requestDto)
        {
            WorkflowNotificationService.NotifyAboutPendingApproval(this, requestDto);
        }

        public virtual void OnExecuted(Request request, RequestDto requestDto)
        {
            var lastApprover = RequestBusinessLogic.GetLastApprover.Call(request);

            if (lastApprover == null || lastApprover.UserId != requestDto.RequestingUserId)
            {
                WorkflowNotificationService.NotifyAboutExecution(this, requestDto);
            }
        }

        public virtual void OnException(Request request, RequestDto requestDto)
        {
            WorkflowNotificationService.NotifyAboutException(this, requestDto);
        }

        public virtual void OnEditing(RecordEditingEventArgs<Request, RequestDto, IWorkflowsDbScope> editingEventArgs)
        {
            var entityToDtoMapping = ClassMappingFactory.CreateMapping<TEntity, TDto>();

            var newRequestObjectDto = (TDto)editingEventArgs.InputDto.RequestObject;
            var oldRequestObjectDto = entityToDtoMapping.CreateFromSource((TEntity)editingEventArgs.DbEntity.RequestObject);

            var mappedProperties = entityToDtoMapping.GetFieldMappings(typeof(TEntity), typeof(TDto)).SelectMany(m => m.Value).ToHashSet();

            EntityHelper.ShallowCopy((TEntity)editingEventArgs.InputEntity.RequestObject, (TEntity)editingEventArgs.DbEntity.RequestObject, mappedProperties);

            var viewModelType = TypeHelper.FindTypeInLoadedAssemblies(ViewModelTypeName);
            var result = ActionSetService.UpdateActions(editingEventArgs.DbEntity, oldRequestObjectDto, newRequestObjectDto, viewModelType);

            foreach (var alert in result.Alerts)
            {
                editingEventArgs.Alerts.Add(alert);
            }
        }

        public virtual void OnEdited(RecordEditedEventArgs<Request, RequestDto> editedEventArgs)
        {
            if (ForceRequestSubmitOnEdit(editedEventArgs.InputEntity, editedEventArgs.InputDto))
            {
                var result = RequestWorkflowService.Submit(editedEventArgs.InputEntity.Id);
                editedEventArgs.Alerts.AddRange(result.Alerts);
            }
        }

        public virtual BoolResult OnDeleting(Request request, RequestDto requestDto, IUnitOfWork<IWorkflowsDbScope> unitOfWork)
        {
            var requestObjectRepository = ((IRepositoryFactory)unitOfWork.Repositories).Create<TEntity>();
            requestObjectRepository.DeleteEach(o => o.Id == request.Id); // Delete RequestObject (inner)

            unitOfWork.Repositories.RequestDocumentContents.DeleteEach(d => d.RequestDocument.RequestId == request.Id);
            unitOfWork.Repositories.RequestDocuments.DeleteEach(d => d.RequestId == request.Id);
            unitOfWork.Repositories.Approvals.DeleteEach(a => a.ApprovalGroup.RequestId == request.Id);
            unitOfWork.Repositories.ApprovalGroups.DeleteEach(g => g.RequestId == request.Id);
            unitOfWork.Repositories.RequestHistoryEntries.DeleteEach(h => h.RequestId == request.Id);

            return new BoolResult(true);
        }

        public virtual BoolResult OnApproving(Request request, RequestDto requestDto, IList<ApprovalDto> affectedApprovals)
        {
            return BoolResult.Success;
        }

        public virtual bool CanBeEdited(RequestDto requestDto, long byUserId)
        {
            return requestDto.RequestingUserId == byUserId
                   && requestDto.Status == RequestStatus.Draft;
        }

        public virtual bool CanBeDeleted(RequestDto requestDto, long byUserId)
        {
            return requestDto.RequestingUserId == byUserId
                   && (requestDto.Status == RequestStatus.Draft
                   || (requestDto.Status == RequestStatus.Pending
                        && requestDto.ApprovalGroups.All(g => g.Approvals.All(a => a.Status == ApprovalStatus.Pending))));
        }

        public virtual bool CanBeAdded()
        {
            return true;
        }

        public virtual bool CanBeSubmitted(RequestDto requestDto, long byUserId)
        {
            return requestDto.Status == RequestStatus.Draft && requestDto.RequestingUserId == byUserId;
        }

        public virtual AlertDto GetRequestSubmittedAlert(RequestDto request, ICollection<string> approvalNames)
        {
            return new AlertDto
            {
                Type = AlertType.Success,
                Message = string.Format(WorkflowResources.RequestSubmitted, string.Join(", ", approvalNames)),
                IsDismissable = true
            };
        }

        IRequestObject IRequestService.GetDraft(IDictionary<string, object> parameters)
        {
            return GetDraft(parameters);
        }

        bool IRequestService.ShouldExecuteAsAsync(RequestDto request)
        {
            return ShouldExecuteAsAsync(request, (TDto)request.RequestObject);
        }

        IEnumerable<AlertDto> IRequestService.Validate(RequestDto request)
        {
            return Validate(request, (TDto)request.RequestObject);
        }

        IEnumerable<ApproverGroupDto> IRequestService.GetApproverGroups(RequestDto request)
        {
            return GetApproverGroups(request, (TDto)request.RequestObject);
        }

        public IEnumerable<long> GetBusinessWatchersUserIds(RequestDto request)
        {
            return GetBusinessWatchersUserIds(request, (TDto)request.RequestObject);
        }

        public EmailRecipientDto[] GetExtraWorkflowNotificationRecipients(RequestDto request)
        {
            return GetExtraWorkflowNotificationRecipients(request, (TDto)request.RequestObject);
        }

        public virtual EmailRecipientDto[] GetExtraWorkflowNotificationRecipients(RequestDto request, TDto requestDto)
        {
            return new EmailRecipientDto[0];
        }

        string IRequestService.GetRequestDescription(RequestDto request)
        {
            return GetRequestDescription(request, (TDto)request.RequestObject);
        }

        string IRequestService.GetRequestDescription(Request request)
        {
            var requestToDtoMapping = ClassMappingFactory.CreateMapping<Request, RequestDto>();
            var dto = requestToDtoMapping.CreateFromSource(request);

            return GetRequestDescription(dto, (TDto)dto.RequestObject);
        }

        void IRequestService.Execute(RequestDto request)
        {
            Execute(request, (TDto)request.RequestObject);
        }

        AlertDto IRequestService.GetRequestSubmittedAlert(RequestDto request, ICollection<string> approvalNames)
        {
            return GetRequestSubmittedAlert(request, approvalNames);
        }

        public virtual IEnumerable<EmailRecipientDto> GetExceptionNotificationRecipients(RequestDto request)
        {
            return SystemParameterService.GetParameter<string[]>(ParameterKeys.AdminEmailRecipients)
                .Select(email => new EmailRecipientDto(email, "Dante Administrators")).ToArray();
        }
    }
}