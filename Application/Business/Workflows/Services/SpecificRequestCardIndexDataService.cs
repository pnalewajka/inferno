﻿using System.Linq;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Workflows.Services
{
    public abstract class SpecificRequestCardIndexDataService<TInnerRequest>
        : RequestCardIndexDataService
        where TInnerRequest : class, IEntity
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public SpecificRequestCardIndexDataService(
            IRequestServiceResolver requestServiceResolver,
            ICardIndexServiceDependencies<IWorkflowsDbScope> dependencies,
            IUploadedDocumentHandlingService uploadedDocumentHandlingService,
            IRequestWorkflowService requestWorkflowService,
            IPrincipalProvider principalProvider,
            ITimeService timeService,
            IClassMapping<Request, RequestDto> requestMapping,
            IReadOnlyUnitOfWorkService<IWorkflowsDbScope> readOnlyUnitOfWorkService)
            : base(requestServiceResolver, dependencies, uploadedDocumentHandlingService, requestWorkflowService, principalProvider, timeService, requestMapping, readOnlyUnitOfWorkService)
        {
        }

        protected abstract BusinessLogic<Request, TInnerRequest> InnerSelector { get; }

        public override IQueryable<Request> ApplyContextFiltering(IQueryable<Request> records, object context)
        {
            var predicate = new BusinessLogic<Request, bool>(
                r => InnerSelector.Call(r) != null);

            return base.ApplyContextFiltering(records, context)
                .Where(predicate);
        }
    }
}
