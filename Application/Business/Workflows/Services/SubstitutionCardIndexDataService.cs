﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Workflows.Consts;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Workflows.Services
{
    public class SubstitutionCardIndexDataService : CardIndexDataService<SubstitutionDto, Substitution, IWorkflowsDbScope>, ISubstitutionCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public SubstitutionCardIndexDataService(ICardIndexServiceDependencies<IWorkflowsDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override NamedFilters<Substitution> NamedFilters
        {
            get
            {
                return new NamedFilters<Substitution>(new[]
                {
                    new NamedFilter<Substitution, DateTime, DateTime>(
                        FilterCodes.DateRange,
                        (s, from, to) => s.To >= from && s.From <= to
                    )
                });
            }
        }

        protected override IEnumerable<Expression<Func<Substitution, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return s => s.ReplacedUser.FirstName;
            yield return s => s.ReplacedUser.LastName;
            yield return s => s.SubstituteUser.FirstName;
            yield return s => s.SubstituteUser.LastName;
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<Substitution, SubstitutionDto, IWorkflowsDbScope> eventArgs)
        {
            ValidateSubstitute(eventArgs.InputEntity, eventArgs.UnitOfWork, eventArgs);

            base.OnRecordAdding(eventArgs);
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<Substitution, SubstitutionDto, IWorkflowsDbScope> eventArgs)
        {
            ValidateSubstitute(eventArgs.InputEntity, eventArgs.UnitOfWork, eventArgs);

            base.OnRecordEditing(eventArgs);
        }

        private static void ValidateSubstitute(Substitution substitution, IUnitOfWork<IWorkflowsDbScope> unitOfWork, CardIndexEventArgs eventArgs)
        {
            if (!IsUserActivelyWorking(substitution.ReplacedUserId, unitOfWork))
            {
                eventArgs.AddError(WorkflowResources.InvalidReplacedUser);
            }

            if (!IsUserActivelyWorking(substitution.SubstituteUserId, unitOfWork))
            {
                eventArgs.AddError(WorkflowResources.InvalidSubstituteUser);
            }
        }

        private static bool IsUserActivelyWorking(long userId, IUnitOfWork<IWorkflowsDbScope> unitOfWork)
        {
            var user = unitOfWork.Repositories.Users.GetById(userId);
            
            return UserBusinessLogic.IsActivelyWorking.Call(user);
        }
    }
}
