﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;
using Castle.Core.Logging;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.Kernel;
using RazorEngine.Templating;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Workflows.Actions;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Jira.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Presentation.Renderers.CardIndex;
using Smt.Atomic.Presentation.Renderers.GridViews;

namespace Smt.Atomic.Business.Workflows.Services
{
    public class ActionSetService : IActionSetService
    {
        private readonly ILogger _logger;
        private readonly IDictionary<Type, IAction> _actions;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IClassMappingFactory _classMappingFactory;
        private readonly IJiraService _jiraService;
        private readonly IReliableEmailService _emailService;
        private readonly IRazorTemplateService _razorTemplateService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IUnitOfWorkService<IWorkflowsDbScope> _unitOfWorkService;

        public ActionSetService(
            ILogger logger,
            IEnumerable<IAction> actions,
            IPrincipalProvider principalProvider,
            IClassMappingFactory classMappingFactory,
            IJiraService jiraService,
            IReliableEmailService emailService,
            IRazorTemplateService razorTemplateService,
            IMessageTemplateService messageTemplateService,
            ISystemParameterService systemParameterService,
            IUnitOfWorkService<IWorkflowsDbScope> unitOfWorkService)
        {
            _logger = logger;
            _principalProvider = principalProvider;
            _classMappingFactory = classMappingFactory;
            _jiraService = jiraService;
            _emailService = emailService;
            _razorTemplateService = razorTemplateService;
            _messageTemplateService = messageTemplateService;
            _systemParameterService = systemParameterService;
            _unitOfWorkService = unitOfWorkService;

            _actions = actions.ToDictionary(a => a.DefinitionType, a => a);
        }

        public BusinessResult ExecuteActionSets(Request request, RequestDto requestDto)
        {
            var result = new BusinessResult();
            var actionSetsEnabled = _systemParameterService.GetParameter<bool>(ParameterKeys.WorkflowsActionSetsEnabled);
            var requestHistoryMapping = _classMappingFactory.CreateMapping<RequestHistoryEntryDto, RequestHistoryEntry>();

            if (!actionSetsEnabled)
            {
                return result;
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var actionSets = FindActionSets(unitOfWork, requestDto.RequestType, requestDto.Status)
                    .Select(s => new { Code = s.Code, Definition = GetActionSetDefinition(s, requestDto) })
                    .ToList();

                foreach (var actionSet in actionSets)
                {
                    if (actionSet.Definition == null)
                    {
                        result.AddAlert(AlertType.Warning, ActionSetResources.SyntaxErrorAlert);

                        _logger.Warn($"Syntax error in {actionSet.Code} action set's definition with request's status {requestDto.Status}.");

                        continue;
                    }

                    foreach (var actionDefinition in actionSet.Definition.ActionDefinitions.OrderByDescending(d => d.IsCrucial))
                    {
                        var action = _actions[actionDefinition.GetType()];
                        var validationResults = action.Validate(actionDefinition, requestDto);

                        if (validationResults.Any())
                        {
                            result.AddAlert(AlertType.Warning, action.ToErrorMessage(actionDefinition));

                            _logger.WarnFormat("Action {0} of {1} action set was not executed due to validation errors: {2}",
                                actionDefinition, actionSet.Code, string.Join(", ", validationResults.Select(r => r.ErrorMessage)));

                            continue;
                        }

                        try
                        {
                            var historyEntry = action.Execute(actionDefinition, requestDto);

                            if (historyEntry != null)
                            {
                                request.HistoryEntries.Add(requestHistoryMapping.CreateFromSource(historyEntry));
                            }
                        }
                        catch (Exception exception)
                        {
                            if (actionDefinition.IsCrucial)
                            {
                                result.AddAlert(AlertType.Error, action.ToErrorMessage(actionDefinition));
                               
                                _logger.ErrorFormat("Action {0} of {1} action set was not executed due to unhandled exception: {2}",
                                    actionDefinition, actionSet.Code, exception);

                                return result;
                            }

                            result.AddAlert(AlertType.Warning, action.ToErrorMessage(actionDefinition));

                            _logger.WarnFormat("Action {0} of {1} action set was not executed due to unhandled exception: {2}",
                                actionDefinition, actionSet.Code, exception);
                        }
                    }
                }

            }

            return result;
        }

        public IEnumerable<ValidationResult> ValidateActionSet(ActionSet actionSet, IRequestService requestService)
        {
            var fixture = new Fixture();
            var result = new List<ValidationResult>();
            var specimenContext = new SpecimenContext(fixture);

            fixture.Register<IEntity>(() => null);

            var modelType = _razorTemplateService.GetTemplateModelType(actionSet.Definition, actionSet.Code);
            var request = fixture.Build<RequestDto>()
                .With(x => x.RequestType, requestService.RequestType)
                .With(x => x.RequestObject, specimenContext.Resolve(requestService.DtoType))
                .Create();

            foreach (var status in Enum.GetValues(typeof(RequestStatus)).Cast<RequestStatus>())
            {
                request.Status = status;
                fixture.Register(() => status);

                var actionSetModel = specimenContext.Resolve(modelType);
                var actionSetDefinition = GetActionSetDefinition(actionSet, actionSetModel);

                if (actionSetDefinition == null)
                {
                    result.Add(new ValidationResult(ActionSetResources.SyntaxError));

                    continue;
                }

                result.AddRange(actionSetDefinition.ActionDefinitions.SelectMany(d => _actions[d.GetType()].Validate(d, request)));
            }

            return result;
        }

        public BusinessResult UpdateActions<TDto>(Request request, TDto oldRequestObjectDto, TDto newRequestObjectDto, Type viewModelType)
        {
            var result = new BusinessResult();

            var viewModelMapping = _classMappingFactory.CreateMapping(oldRequestObjectDto.GetType(), viewModelType);
            var oldViewModel = viewModelMapping.CreateFromSource(oldRequestObjectDto);
            var newViewModel = viewModelMapping.CreateFromSource(newRequestObjectDto);

            var authorFullName = $"{_principalProvider.Current.FirstName} {_principalProvider.Current.LastName}";
            var differingProperties = oldViewModel.GetType().GetProperties()
                .Where(p => !Equals(p.GetValue(oldViewModel), p.GetValue(newViewModel)))
                .Select(p => p.Name)
                .ToList();

            var createJiraIssueAction = GetAction<CreateJiraIssueAction>();

            foreach (var entry in request.HistoryEntries.Where(e => e.Type == RequestHistoryEntryType.JiraIssueCreated && e.IsActionActive == true))
            {
                var differencesDescription = GetDifferencesDescription(entry.UpdatesOn, oldViewModel, newViewModel, differingProperties);

                if (!string.IsNullOrEmpty(differencesDescription))
                {
                    try
                    {
                        createJiraIssueAction.Update(entry.JiraIssueKey, differencesDescription, authorFullName);
                    }
                    catch (Exception exception)
                    {
                        var supportAddress = _systemParameterService.GetParameter<string>(ParameterKeys.SystemSupportEmailAddress);

                        result.AddAlert(AlertType.Warning, string.Format(ActionSetResources.CreateJiraIssueUpdateError, entry.JiraIssueKey, supportAddress));

                        _logger.Warn($"Attempt to add a comment in Jira issue {entry.JiraIssueKey} resulted in exception: {exception}");
                    }
                }
            }

            var sendEmailAction = GetAction<SendEmailAction>();

            foreach (var entry in request.HistoryEntries.Where(e => e.Type == RequestHistoryEntryType.MailSent && e.IsActionActive == true))
            {
                var changeDescription = new SendEmailActionChangeDescriptionDto
                {
                    AuthorFullName = authorFullName,
                    ChangeDescription = GetDifferencesDescription(entry.UpdatesOn, oldViewModel, newViewModel, differingProperties)
                };

                if (!string.IsNullOrEmpty(changeDescription.ChangeDescription))
                {
                    try
                    {
                        sendEmailAction.Update(entry.EmailSubject, entry.EmailRecipients, changeDescription);
                    }
                    catch (Exception exception)
                    {
                        var supportAddress = _systemParameterService.GetParameter<string>(ParameterKeys.SystemSupportEmailAddress);

                        result.AddAlert(AlertType.Warning, string.Format(ActionSetResources.SendEmailMessageError, supportAddress));

                        _logger.Warn($"Attempt to send a reply to email with subject {entry.EmailSubject} resulted in exception: {exception}");
                    }
                }
            }

            return result;
        }

        public void AddCommentToJiraIssues(Request request, string comment)
        {
            using (var context = _jiraService.CreateReadWrite())
            {
                foreach (var entry in request.HistoryEntries.Where(e => e.Type == RequestHistoryEntryType.JiraIssueCreated))
                {
                    context.AddCommentToIssue(entry.JiraIssueKey, comment);
                }
            }
        }

        public void SendNotificationToEmailRecipients(Request request, RequestDto requestDto, string subjectTemplate, string bodyTemplate)
        {
            var emailBody = _messageTemplateService.ResolveTemplate(bodyTemplate, requestDto);
            var emailSubject = _messageTemplateService.ResolveTemplate(subjectTemplate, requestDto);

            foreach (var entry in request.HistoryEntries.Where(e => e.Type == RequestHistoryEntryType.MailSent))
            {
                if (string.IsNullOrEmpty(entry.EmailRecipients))
                {
                    continue;
                }

                var recipients = JsonHelper.Deserialize<List<EmailRecipientDto>>(entry.EmailRecipients);

                var message = new EmailMessageDto
                {
                    Body = emailBody.Content,
                    IsHtml = emailBody.IsHtml,
                    Subject = emailSubject.Content,
                    Recipients = recipients
                };

                _emailService.EnqueueMessage(message);
            }
        }

        private static IEnumerable<ActionSet> FindActionSets(IUnitOfWork<IWorkflowsDbScope> unitOfWork, RequestType type, RequestStatus status)
        {
            return unitOfWork.Repositories.ActionSets
                .Where(a => a.TriggeringRequestType == type && a.TriggeringRequestStatuses.Any(s => s.RequestStatus == status))
                .ToList();
        }

        private T GetAction<T>() where T : IAction
        {
            return _actions.Values.OfType<T>().Single();
        }

        private ActionSetDefinition GetActionSetDefinition(ActionSet actionSet, object model)
        {
            try
            {
                var compiledActionSetDefinition = _razorTemplateService.TemplateRunAndCompile(actionSet.Code, actionSet.Definition, model);

                using (var reader = new StringReader(compiledActionSetDefinition))
                {
                    var serializer = new XmlSerializer(typeof(ActionSetDefinition));
                    var result = (ActionSetDefinition)serializer.Deserialize(reader);

                    if (result.ActionDefinitions == null)
                    {
                        result.ActionDefinitions = new ActionDefinition[0];
                    }

                    return result;
                }
            }
            catch (InvalidOperationException)
            {
                return null;
            }
            catch (TemplateCompilationException)
            {
                return null;
            }
        }

        private static string GetDifferencesDescription<TViewModel>(string updatesOn, TViewModel oldViewModel, TViewModel newViewModel, IList<string> differingProperties)
        {
            var stringBuilder = new StringBuilder();

            using (CultureInfoHelper.SetCurrentCulture(CultureInfo.InvariantCulture))
            {
                foreach (var property in SplitUpdatesOnField(updatesOn).Where(p => differingProperties.Contains(p)))
                {
                    var viewModelType = oldViewModel.GetType();
                    var propertyInfo = viewModelType.GetProperty(property);
                    var propertyRenderer = CreatePropertyRenderer(viewModelType, propertyInfo, oldViewModel, newViewModel);

                    var oldValue = propertyRenderer.RenderForExport(oldViewModel, null);
                    var newValue = propertyRenderer.RenderForExport(newViewModel, null);

                    if (!Equals(oldValue, newValue))
                    {
                        stringBuilder.AppendLine($"{PropertyHelper.GetPropertyFriendlyName(propertyInfo)}: was {oldValue} and now is {newValue}");
                    }
                }
            }

            return stringBuilder.ToString();
        }

        private static PropertyBasedGridColumnViewModel CreatePropertyRenderer(Type type, PropertyInfo property, params object[] rows)
        {
            var propertyConfigurationFactory = typeof(GridPropertyConfiguration<>).MakeGenericType(type)
                .GetMethod(nameof(GridPropertyConfiguration<object>.FromPropertyInfo));
            var propertyConfiguration = (IGridPropertyColumnViewConfiguration)propertyConfigurationFactory.Invoke(null, new[] { property });

            return new PropertyBasedGridColumnViewModel(propertyConfiguration, SortingDirection.Unspecified, rows);
        }

        private static string[] SplitUpdatesOnField(string updatesOn)
        {
            return (updatesOn ?? string.Empty).Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
