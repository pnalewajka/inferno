﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Business.Workflows.Services
{
    public class RequestStatusCardIndexDataService : EnumBasedCardIndexDataService<RequestStatusDto, RequestStatus>, IRequestStatusCardIndexDataService
    {
        protected override IEnumerable<Expression<Func<RequestStatusDto, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return s => s.Description;
        }

        protected override RequestStatusDto ConvertToDto(RequestStatus enumValue)
        {
            return new RequestStatusDto
            {
                Id = (long)enumValue,
                Description = enumValue.GetDescription()
            };
        }
    }
}
