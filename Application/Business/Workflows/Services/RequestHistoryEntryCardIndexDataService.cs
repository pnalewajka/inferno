﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Workflows.Services
{
    public class RequestHistoryEntryCardIndexDataService : CardIndexDataService<RequestHistoryEntryDto, RequestHistoryEntry, IWorkflowsDbScope>, IRequestHistoryEntryCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public RequestHistoryEntryCardIndexDataService(ICardIndexServiceDependencies<IWorkflowsDbScope> dependencies)
            : base(dependencies)
        {
        }

        public override IQueryable<RequestHistoryEntry> ApplyContextFiltering(IQueryable<RequestHistoryEntry> records, object context)
        {
            var parentId = (context as ParentIdContext)?.ParentId;

            return records.Where(e => e.RequestId == parentId);
        }

        protected override IOrderedQueryable<RequestHistoryEntry> GetDefaultOrderBy(IQueryable<RequestHistoryEntry> records, QueryCriteria criteria)
        {
            return records.OrderBy(h => h.Date);
        }
    }
}
