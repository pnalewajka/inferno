﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Workflows.Services
{
    public class ExpenseRequestCardIndexDataService : CardIndexDataService<ExpenseRequestDto, ExpenseRequest, IWorkflowsDbScope>, IExpenseRequestCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public ExpenseRequestCardIndexDataService(ICardIndexServiceDependencies<IWorkflowsDbScope> dependencies)
            : base(dependencies)
        {
        }
    }
}