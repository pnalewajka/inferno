﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Workflows.Services
{
    public class TaxDeductibleCostRequestCardIndexDataService : CardIndexDataService<TaxDeductibleCostRequestDto, TaxDeductibleCostRequest, IWorkflowsDbScope>, ITaxDeductibleCostRequestCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public TaxDeductibleCostRequestCardIndexDataService(ICardIndexServiceDependencies<IWorkflowsDbScope> dependencies)
            : base(dependencies)
        {
        }
    }
}
