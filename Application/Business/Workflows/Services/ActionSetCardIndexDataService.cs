﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Workflows.Services
{
    public class ActionSetCardIndexDataService : CardIndexDataService<ActionSetDto, ActionSet, IWorkflowsDbScope>, IActionSetCardIndexDataService
    {
        private readonly IActionSetService _actionSetService;
        private readonly IRequestServiceResolver _requestServiceResolver;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public ActionSetCardIndexDataService(
            IRequestServiceResolver requestServiceResolver,
            IActionSetService actionSetService,
            ICardIndexServiceDependencies<IWorkflowsDbScope> dependencies)
            : base(dependencies)
        {
            _actionSetService = actionSetService;
            _requestServiceResolver = requestServiceResolver;
        }

        public void CreateOrUpdateActionSet(string code, string description, RequestType triggeringRequestType, RequestStatus[] triggeringRequestStatuses, string definition)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var actionSet = unitOfWork.Repositories.ActionSets.SingleOrDefault(t => t.Code == code);

                if (actionSet == null)
                {
                    actionSet = unitOfWork.Repositories.ActionSets.CreateEntity();

                    unitOfWork.Repositories.ActionSets.Add(actionSet);
                }

                actionSet.Code = code;
                actionSet.Description = description;
                actionSet.TriggeringRequestType = triggeringRequestType;
                actionSet.Definition = definition;

                unitOfWork.Repositories.ActionSetTriggeringStatus.DeleteRange(actionSet.TriggeringRequestStatuses);
                actionSet.TriggeringRequestStatuses = triggeringRequestStatuses.Select(s => new ActionSetTriggeringStatus { RequestStatus = s }).ToList();

                unitOfWork.Commit();
            }
        }

        protected override IEnumerable<Expression<Func<ActionSet, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return s => s.Code;
            yield return s => s.Description;
            yield return s => s.Definition;
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<ActionSet, ActionSetDto, IWorkflowsDbScope> eventArgs)
        {
            if (!ValidateActionSet(eventArgs, eventArgs.InputEntity))
            {
                return;
            }

            base.OnRecordAdding(eventArgs);
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<ActionSet, ActionSetDto, IWorkflowsDbScope> eventArgs)
        {
            if (!ValidateActionSet(eventArgs, eventArgs.InputEntity))
            {
                return;
            }

            EntityMergingService.MergeCollections(eventArgs.UnitOfWork,
                eventArgs.DbEntity.TriggeringRequestStatuses, eventArgs.InputEntity.TriggeringRequestStatuses, s => s.RequestStatus, true);

            base.OnRecordEditing(eventArgs);
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<ActionSet, IWorkflowsDbScope> eventArgs)
        {
            eventArgs.UnitOfWork.Repositories.ActionSetTriggeringStatus.DeleteRange(eventArgs.DeletingRecord.TriggeringRequestStatuses, true);
        }

        private bool ValidateActionSet(CardIndexEventArgs eventArgs, ActionSet actionSet)
        {
            if (!actionSet.TriggeringRequestType.HasValue)
            {
                return false;
            }

            var requestService = _requestServiceResolver.GetByServiceType(actionSet.TriggeringRequestType.Value);
            var validationResults = _actionSetService.ValidateActionSet(actionSet, requestService);

            if (validationResults.Any())
            {
                eventArgs.Canceled = true;

                foreach (var errorMessage in validationResults.Select(r => r.ErrorMessage).Distinct())
                {
                    eventArgs.Alerts.Add(AlertDto.CreateError(errorMessage));
                }

                return false;
            }

            return true;
        }
    }
}
