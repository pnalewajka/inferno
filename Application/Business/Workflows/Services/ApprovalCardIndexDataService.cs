﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Workflows.BusinessLogics;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Workflows.Services
{
    public class ApprovalCardIndexDataService : CardIndexDataService<ApprovalDto, Approval, IWorkflowsDbScope>, IApprovalCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public ApprovalCardIndexDataService(ICardIndexServiceDependencies<IWorkflowsDbScope> dependencies)
            : base(dependencies)
        {
        }

        public override IQueryable<Approval> ApplyContextFiltering(IQueryable<Approval> records, object context)
        {
            var currentUserId = PrincipalProvider.Current.Id.Value;
            var parentIdContext = (ParentIdContext)context;

            if (!PrincipalProvider.Current.IsInRole(SecurityRoleType.CanManageWorkflowsOnBehalf))
            {
                var currentDate = TimeService.GetCurrentDate();
                records = records.Where(ApprovalBusinessLogic.IsSubstitute.Parametrize(currentUserId, currentDate));
            }

            return
                records.Where(new BusinessLogic<Approval, bool>(a =>
                    a.ApprovalGroup.RequestId == parentIdContext.ParentId
                    && RequestBusinessLogic.CurrentApprovalGroup.Call(a.ApprovalGroup.Request) != null
                    && RequestBusinessLogic.CurrentApprovalGroup.Call(a.ApprovalGroup.Request).Id == a.ApprovalGroupId
                    && a.Status == ApprovalStatus.Pending
                    && a.UserId != currentUserId))
                    .GroupBy(a => a.UserId)
                    .Select(g => g.FirstOrDefault());
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<Approval> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var sortingDirection = sortingCriterion.Ascending ? SortingDirection.Ascending : SortingDirection.Descending;

            if (sortingCriterion.GetSortingColumnName() == nameof(ApprovalDto.ApproverFullName))
            {
                orderedQueryBuilder.ApplySortingKey(a => a.User.LastName, sortingDirection);
            }
            else
            {
                base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
            }
        }
    }
}
