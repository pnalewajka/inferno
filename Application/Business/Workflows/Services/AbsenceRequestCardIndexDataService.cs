﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Filters;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Workflows.Services
{
    public class AbsenceRequestCardIndexDataService
        : SpecificRequestCardIndexDataService<AbsenceRequest>
        , IAbsenceRequestCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public AbsenceRequestCardIndexDataService(IRequestServiceResolver requestServiceResolver,
            ICardIndexServiceDependencies<IWorkflowsDbScope> dependencies,
            IUploadedDocumentHandlingService uploadedDocumentHandlingService,
            IRequestWorkflowService requestWorkflowService,
            IPrincipalProvider principalProvider,
            ITimeService timeService,
            IClassMapping<Request, RequestDto> requestMapping,
            IReadOnlyUnitOfWorkService<IWorkflowsDbScope> readOnlyUnitOfWorkService)
            : base(requestServiceResolver, dependencies, uploadedDocumentHandlingService, requestWorkflowService, principalProvider, timeService, requestMapping, readOnlyUnitOfWorkService)
        {
        }

        protected override BusinessLogic<Request, AbsenceRequest> InnerSelector
            => new BusinessLogic<Request, AbsenceRequest>(r => r.AbsenceRequest);

        protected override NamedFilters<Request> NamedFilters
        {
            get
            {
                var baseNameFilters = base.NamedFilters;
                baseNameFilters.AppendUniqueFilters(new NamedFilters<Request>(
                  new NamedFilter<Request>[] {
                        AbsenceTypeFilter.AbsenceRequestFilter
                  }));

                return baseNameFilters;
            }
        }
    }
}
