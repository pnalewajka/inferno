﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Workflows.BusinessLogics;
using Smt.Atomic.Business.Workflows.Consts;
using Smt.Atomic.Business.Workflows.DocumentMappings;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Exceptions;
using Smt.Atomic.Business.Workflows.Filters;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.PeopleManagement;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using System.Collections.Concurrent;

namespace Smt.Atomic.Business.Workflows.Services
{
    public class RequestCardIndexDataService :
        CardIndexDataService<RequestDto, Request, IWorkflowsDbScope>,
        IRequestCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        private readonly IUploadedDocumentHandlingService _uploadedDocumentHandlingService;
        private readonly IRequestServiceResolver _requestServiceResolver;
        private readonly IRequestWorkflowService _requestWorkflowService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly ITimeService _timeService;
        private readonly IClassMapping<Request, RequestDto> _requestMapping;
        private readonly IReadOnlyUnitOfWorkService<IWorkflowsDbScope> _readOnlyUnitOfWorkService;

        public RequestCardIndexDataService(
            IRequestServiceResolver requestServiceResolver,
            ICardIndexServiceDependencies<IWorkflowsDbScope> dependencies,
            IUploadedDocumentHandlingService uploadedDocumentHandlingService,
            IRequestWorkflowService requestWorkflowService,
            IPrincipalProvider principalProvider,
            ITimeService timeService,
            IClassMapping<Request, RequestDto> requestMapping,
            IReadOnlyUnitOfWorkService<IWorkflowsDbScope> readOnlyUnitOfWorkService)
            : base(dependencies)
        {
            _requestServiceResolver = requestServiceResolver;
            _requestWorkflowService = requestWorkflowService;
            _principalProvider = principalProvider;
            _timeService = timeService;
            _uploadedDocumentHandlingService = uploadedDocumentHandlingService;
            _requestMapping = requestMapping;
            _readOnlyUnitOfWorkService = readOnlyUnitOfWorkService;
        }

        public override RequestDto GetRecordById(long id)
        {
            var requestDto = base.GetRecordById(id);

            //Approvers preview
            if (requestDto.Status == RequestStatus.Draft)
            {
                _requestWorkflowService.AddApprovers(requestDto);
            }

            return requestDto;
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<Request> orderedQueryBuilder,
            SortingCriterion sortingCriterion)
        {
            var direction = sortingCriterion.GetSortingDirection();

            if (sortingCriterion.GetSortingColumnName() == nameof(RequestDto.RequesterFullName))
            {
                orderedQueryBuilder.ApplySortingKey(d => d.RequestingUser.FirstName, direction);
                orderedQueryBuilder.ApplySortingKey(d => d.RequestingUser.LastName, direction);
            }
            else if (sortingCriterion.GetSortingColumnName() == nameof(RequestDto.Company))
            {
                orderedQueryBuilder.ApplySortingKey(d => d.RequestingUser.Employees
                    .Any(e => e.Company != null && e.Company.Name != null)
                    ? d.RequestingUser.Employees.FirstOrDefault(e => e.Company != null && e.Company.Name != null).Company.Name
                    : "", direction);
            }
            else if (sortingCriterion.GetSortingColumnName() == nameof(RequestDto.OrgUnit))
            {
                orderedQueryBuilder.ApplySortingKey(d => d.RequestingUser.Employees
                    .Any(e => e.OrgUnit != null && e.OrgUnit.Name != null)
                    ? d.RequestingUser.Employees.FirstOrDefault(e => e.OrgUnit != null && e.OrgUnit.Name != null).OrgUnit.Name
                    : "", direction);
            }
            else if (sortingCriterion.GetSortingColumnName() == nameof(RequestDto.ContractType))
            {
                DateTime currentDate = _timeService.GetCurrentDate();
                orderedQueryBuilder.ApplySortingKey(d => d.RequestingUser.Employees.Any()
                    ? d.RequestingUser.Employees.FirstOrDefault()
                        .EmploymentPeriods
                            .Where(ep => ep.StartDate <= currentDate && (!ep.EndDate.HasValue || ep.EndDate >= currentDate))
                            .Select(p => p.ContractTypeId)
                            .FirstOrDefault()
                    : -1, direction);
            }
            else
            {
                base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
            }
        }

        protected override IOrderedQueryable<Request> GetDefaultOrderBy(IQueryable<Request> records, QueryCriteria criteria)
        {
            if (criteria.Filters.Any(f => f.Codes.Any(c => c == FilterCodes.DecisionDone)) &&
                _principalProvider.Current.Id.HasValue)
            {
                var userId = _principalProvider.Current.Id.Value;
                var isPendingApproverExpression = new BusinessLogic<Approval, bool>(a =>
                    !ApprovalBusinessLogic.IsPending.Call(a) && ApprovalBusinessLogic.IsDirectApprover.Call(a, userId));

                return records.OrderByDescending(
                    r => r.ApprovalGroups.AsQueryable()
                        .SelectMany(a => a.Approvals)
                        .Where(isPendingApproverExpression)
                        .Select(a => a.ModifiedOn)
                        .Concat(new[] { r.ModifiedOn })
                        .Max()
                );
            }

            return records.OrderByDescending(r => r.ModifiedOn);
        }

        protected override AddRecordResult InternalAddRecord(RequestDto requestDto, object context)
        {
            return base.InternalAddRecord(requestDto, context);
        }

        protected override void OnRecordAdding(
            RecordAddingEventArgs<Request, RequestDto, IWorkflowsDbScope> eventArgs)
        {

            base.OnRecordAdding(eventArgs);

            var request = eventArgs.InputEntity;
            var requestDto = eventArgs.InputDto;
            var requestService = _requestServiceResolver.GetByServiceType(requestDto.RequestType);

            _uploadedDocumentHandlingService.MergeDocumentCollections(
                eventArgs.UnitOfWork,
                eventArgs.InputEntity.Documents,
                eventArgs.InputDto.Documents,
                p => new RequestDocument
                {
                    Name = p.DocumentName,
                    ContentType = p.ContentType
                }
            );

            eventArgs.InputEntity.Watchers =
                EntityMergingService.CreateEntitiesFromIds<User, IWorkflowsDbScope>(eventArgs.UnitOfWork,
                    eventArgs.InputEntity.Watchers.GetIds());

            if (request.RequestType == RequestType.SkillChangeRequest)
            {
                request.SkillChangeRequest.Skills =
                    EntityMergingService.CreateEntitiesFromIds<Skill, IWorkflowsDbScope>(eventArgs.UnitOfWork,
                        request.SkillChangeRequest.Skills.GetIds());
            }
            else if (request.RequestType == RequestType.ReopenTimeTrackingReportRequest)
            {
                request.ReopenTimeTrackingReportRequest.Projects =
                    EntityMergingService.CreateEntitiesFromIds<Project, IWorkflowsDbScope>(eventArgs.UnitOfWork,
                        request.ReopenTimeTrackingReportRequest.Projects.GetIds());
            }
            else if (request.RequestType == RequestType.EmployeeDataChangeRequest)
            {
                request.EmployeeDataChangeRequest.Employees =
                    EntityMergingService.CreateEntitiesFromIds<Employee, IWorkflowsDbScope>(eventArgs.UnitOfWork,
                        request.EmployeeDataChangeRequest.Employees.GetIds());
            }
            else if (request.RequestType == RequestType.BusinessTripRequest)
            {
                request.BusinessTripRequest.Participants =
                    EntityMergingService.CreateEntitiesFromIds<Employee, IWorkflowsDbScope>(eventArgs.UnitOfWork,
                        request.BusinessTripRequest.Participants.GetIds());

                request.BusinessTripRequest.Projects =
                     EntityMergingService.CreateEntitiesFromIds<Project, IWorkflowsDbScope>(eventArgs.UnitOfWork,
                        request.BusinessTripRequest.Projects.GetIds());

                request.BusinessTripRequest.DepartureCities =
                     EntityMergingService.CreateEntitiesFromIds<City, IWorkflowsDbScope>(eventArgs.UnitOfWork,
                        request.BusinessTripRequest.DepartureCities.GetIds());
            }
            else if (request.RequestType == RequestType.AssetsRequest)
            {
                request.AssetsRequest.AssetTypes = EntityMergingService.CreateEntitiesFromIds<AssetType, IWorkflowsDbScope>(eventArgs.UnitOfWork,
                    request.AssetsRequest.AssetTypes.GetIds());
            }
            else if (request.RequestType == RequestType.OnboardingRequest)
            {
                var onboardingRequestDto = (OnboardingRequestDto)eventArgs.InputDto.RequestObject;

                _uploadedDocumentHandlingService.MergeDocumentCollections(
                    eventArgs.UnitOfWork,
                    eventArgs.InputEntity.OnboardingRequest.WelcomeAnnounceEmailPhotos,
                    onboardingRequestDto.WelcomeAnnounceEmailPhotos,
                    p => new EmployeePhotoDocument
                    {
                        Name = p.DocumentName,
                        ContentType = p.ContentType
                    }
                );

                _uploadedDocumentHandlingService.MergeDocumentCollections(
                    eventArgs.UnitOfWork,
                    eventArgs.InputEntity.OnboardingRequest.PayrollDocuments,
                    onboardingRequestDto.PayrollDocuments,
                    p => new PayrollDocument
                    {
                        Name = p.DocumentName,
                        ContentType = p.ContentType
                    }
                );

                _uploadedDocumentHandlingService.MergeDocumentCollections(
                    eventArgs.UnitOfWork,
                    eventArgs.InputEntity.OnboardingRequest.ForeignEmployeeDocuments,
                    onboardingRequestDto.ForeignEmployeeDocuments,
                    p => new ForeignEmployeeDocument
                    {
                        Name = p.DocumentName,
                        ContentType = p.ContentType
                    }
                );
            }
            else if (request.RequestType == RequestType.FeedbackRequest)
            {
                if (eventArgs.InputEntity.FeedbackRequest.EvaluatorEmployees != null && eventArgs.InputEntity.FeedbackRequest.EvaluatorEmployees.Any())
                {
                    request.FeedbackRequest.EvaluatorEmployees =
                       EntityMergingService.CreateEntitiesFromIds<Employee, IWorkflowsDbScope>(eventArgs.UnitOfWork,
                           eventArgs.InputEntity.FeedbackRequest.EvaluatorEmployees.GetIds());
                }
            }

            eventArgs.InputEntity.HistoryEntries = new Collection<RequestHistoryEntry>(new[]
            {
                new RequestHistoryEntry
                {
                    Date = _timeService.GetCurrentTime(),
                    Type = RequestHistoryEntryType.Created,
                    ExecutingUserId = _principalProvider.Current.Id
                }
            });

            var validationResults = requestService.Validate(requestDto);
            eventArgs.Alerts.AddRange(validationResults);

            requestService.OnAdding(eventArgs);
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<Request, RequestDto> eventArgs)
        {
            base.OnRecordAdded(eventArgs);

            var requestDto = eventArgs.InputDto;
            var requestService = _requestServiceResolver.GetByServiceType(requestDto.RequestType);

            _uploadedDocumentHandlingService.PromoteTemporaryDocuments<RequestDocument, RequestDocumentContent, RequestDocumentMapping, IWorkflowsDbScope>(eventArgs.InputDto.Documents);

            if (eventArgs.InputEntity.RequestType == RequestType.OnboardingRequest)
            {
                var onboardingRequestDto = (OnboardingRequestDto)eventArgs.InputDto.RequestObject;

                _uploadedDocumentHandlingService.PromoteTemporaryDocuments<EmployeePhotoDocument, EmployeePhotoDocumentContent, EmployeePhotoDocumentMapping, IWorkflowsDbScope>(onboardingRequestDto.WelcomeAnnounceEmailPhotos);
                _uploadedDocumentHandlingService.PromoteTemporaryDocuments<PayrollDocument, PayrollDocumentContent, PayrollDocumentMapping, IWorkflowsDbScope>(onboardingRequestDto.PayrollDocuments);
                _uploadedDocumentHandlingService.PromoteTemporaryDocuments<ForeignEmployeeDocument, ForeignEmployeeDocumentContent, ForeignEmployeeDocumentMapping, IWorkflowsDbScope>(onboardingRequestDto.ForeignEmployeeDocuments);
            }

            requestService.OnAdded(eventArgs);
        }

        protected override void OnRecordEditing(
            RecordEditingEventArgs<Request, RequestDto, IWorkflowsDbScope> eventArgs)
        {
            var requestDto = eventArgs.InputDto;
            var requestService = _requestServiceResolver.GetByServiceType(requestDto.RequestType);
            var canBeEdited = CanBeEdited(requestDto, requestService, eventArgs.Alerts.Add);

            if (!canBeEdited)
            {
                eventArgs.Canceled = true;
                return;
            }

            _uploadedDocumentHandlingService.MergeDocumentCollections(
               eventArgs.UnitOfWork,
               eventArgs.DbEntity.Documents,
               eventArgs.InputDto.Documents,
               p => new RequestDocument
               {
                   Name = p.DocumentName,
                   ContentType = p.ContentType
               }
           );

            // Avoid losing business watchers
            var watchers = requestService.GetBusinessWatchersUserIds(requestDto);
            eventArgs.InputDto.WatchersUserIds = eventArgs.InputDto.WatchersUserIds.Union(watchers).ToArray();

            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.Watchers, eventArgs.InputEntity.Watchers);

            if (eventArgs.DbEntity.RequestType == RequestType.SkillChangeRequest)
            {
                EntityMergingService.MergeCollections(eventArgs.UnitOfWork,
                    eventArgs.DbEntity.SkillChangeRequest.Skills,
                    eventArgs.InputEntity.SkillChangeRequest.Skills);
            }
            else if (eventArgs.DbEntity.RequestType == RequestType.ReopenTimeTrackingReportRequest)
            {
                EntityMergingService.MergeCollections(eventArgs.UnitOfWork,
                    eventArgs.DbEntity.ReopenTimeTrackingReportRequest.Projects,
                    eventArgs.InputEntity.ReopenTimeTrackingReportRequest.Projects);
            }
            else if (eventArgs.DbEntity.RequestType == RequestType.EmployeeDataChangeRequest)
            {
                EntityMergingService.MergeCollections(eventArgs.UnitOfWork,
                    eventArgs.DbEntity.EmployeeDataChangeRequest.Employees,
                    eventArgs.InputEntity.EmployeeDataChangeRequest.Employees);
            }
            else if (eventArgs.DbEntity.RequestType == RequestType.AssetsRequest)
            {
                EntityMergingService.MergeCollections(eventArgs.UnitOfWork,
                    eventArgs.DbEntity.AssetsRequest.AssetTypes,
                    eventArgs.InputEntity.AssetsRequest.AssetTypes);
            }
            else if (eventArgs.DbEntity.RequestType == RequestType.OnboardingRequest)
            {
                var onboardingRequestDto = (OnboardingRequestDto)eventArgs.InputDto.RequestObject;

                _uploadedDocumentHandlingService.MergeDocumentCollections(
                    eventArgs.UnitOfWork,
                    eventArgs.DbEntity.OnboardingRequest.WelcomeAnnounceEmailPhotos,
                    onboardingRequestDto.WelcomeAnnounceEmailPhotos,
                    p => new EmployeePhotoDocument
                    {
                        Name = p.DocumentName,
                        ContentType = p.ContentType
                    }
                );

                _uploadedDocumentHandlingService.MergeDocumentCollections(
                    eventArgs.UnitOfWork,
                    eventArgs.DbEntity.OnboardingRequest.PayrollDocuments,
                    onboardingRequestDto.PayrollDocuments,
                    p => new PayrollDocument
                    {
                        Name = p.DocumentName,
                        ContentType = p.ContentType
                    }
                );

                _uploadedDocumentHandlingService.MergeDocumentCollections(
                    eventArgs.UnitOfWork,
                    eventArgs.DbEntity.OnboardingRequest.ForeignEmployeeDocuments,
                    onboardingRequestDto.ForeignEmployeeDocuments,
                    p => new ForeignEmployeeDocument
                    {
                        Name = p.DocumentName,
                        ContentType = p.ContentType
                    }
                );
            }
            else if (eventArgs.DbEntity.RequestType == RequestType.FeedbackRequest)
            {
                EntityMergingService.MergeCollections(eventArgs.UnitOfWork,
                    eventArgs.DbEntity.FeedbackRequest.EvaluatorEmployees,
                    eventArgs.InputEntity.FeedbackRequest.EvaluatorEmployees);
            }

            requestService.OnEditing(eventArgs);

            base.OnRecordEditing(eventArgs);

            var validationResults = requestService.Validate(requestDto);
            eventArgs.Alerts.AddRange(validationResults);

            eventArgs.DbEntity.HistoryEntries.Add(new RequestHistoryEntry
            {
                Date = _timeService.GetCurrentTime(),
                Type = RequestHistoryEntryType.Edited,
                ExecutingUserId = _principalProvider.Current.Id
            });
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<Request, RequestDto> eventArgs)
        {
            base.OnRecordEdited(eventArgs);

            var requestService = _requestServiceResolver.GetByServiceType(eventArgs.InputDto.RequestType);

            _uploadedDocumentHandlingService.PromoteTemporaryDocuments<RequestDocument, RequestDocumentContent, RequestDocumentMapping, IWorkflowsDbScope>(eventArgs.InputDto.Documents);

            if (eventArgs.InputEntity.RequestType == RequestType.OnboardingRequest)
            {
                var onboardingRequestDto = (OnboardingRequestDto)eventArgs.InputDto.RequestObject;

                _uploadedDocumentHandlingService.PromoteTemporaryDocuments<EmployeePhotoDocument, EmployeePhotoDocumentContent, EmployeePhotoDocumentMapping, IWorkflowsDbScope>(onboardingRequestDto.WelcomeAnnounceEmailPhotos);
                _uploadedDocumentHandlingService.PromoteTemporaryDocuments<PayrollDocument, PayrollDocumentContent, PayrollDocumentMapping, IWorkflowsDbScope>(onboardingRequestDto.PayrollDocuments);
                _uploadedDocumentHandlingService.PromoteTemporaryDocuments<ForeignEmployeeDocument, ForeignEmployeeDocumentContent, ForeignEmployeeDocumentMapping, IWorkflowsDbScope>(onboardingRequestDto.ForeignEmployeeDocuments);
            }

            requestService.OnEdited(eventArgs);
        }

        protected override DeleteRecordsResult InternalDeleteRecords(IEnumerable<long> idsToDelete, out IEnumerable<RequestDto> deletedRecords, object context, bool isDeletePermanent)
        {
            using (var transactionScope = TransactionHelper.CreateDefaultTransactionScope())
            {
                var result = base.InternalDeleteRecords(idsToDelete, out deletedRecords, context, isDeletePermanent);

                if (result.IsSuccessful)
                {
                    transactionScope.Complete();
                }

                return result;
            }
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<Request, IWorkflowsDbScope> eventArgs)
        {
            var request = eventArgs.DeletingRecord;
            var requestDto = _requestMapping.CreateFromSource(request);
            var requestService = _requestServiceResolver.GetByServiceType(request.RequestType);

            var canBeDeleted = CanBeDeleted(requestDto, requestService, eventArgs.Alerts.Add);

            if (!canBeDeleted)
            {
                eventArgs.Canceled = true;
                return;
            }

            var result = requestService.OnDeleting(request, requestDto, eventArgs.UnitOfWork);
            eventArgs.Canceled |= !result.IsSuccessful;
            eventArgs.Alerts.AddRange(result.Alerts);

            base.OnRecordDeleting(eventArgs);
        }

        protected override IQueryable<Request> ConfigureIncludes(IQueryable<Request> sourceQueryable)
        {
            return sourceQueryable
                .Include(r => r.Watchers)
                .Include(r => r.ApprovalGroups)
                .Include(r => r.ApprovalGroups.Select(g => g.Approvals))
                .Include(r => r.ApprovalGroups.Select(g => g.Approvals.Select(a => a.User)))
                .Include(r => r.RequestingUser);
        }

        protected override IEnumerable<Expression<Func<Request, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            if (searchCriteria.Includes(SearchAreaCodes.RequestingUser))
            {
                yield return a => a.RequestingUser.FirstName;
                yield return a => a.RequestingUser.LastName;
                yield return a => a.RequestingUser.Employees.Select(e => e.Acronym);
            }

            if (searchCriteria.Includes(SearchAreaCodes.ApprovingUser))
            {
                yield return a => a.ApprovalGroups.SelectMany(x => x.Approvals.Select(y => y.User.FirstName));
                yield return a => a.ApprovalGroups.SelectMany(x => x.Approvals.Select(y => y.User.LastName));
                yield return a => a.ApprovalGroups.SelectMany(x => x.Approvals.SelectMany(y => y.User.Employees.Select(e => e.Acronym)));
                yield return a => a.ApprovalGroups.SelectMany(x => x.Approvals.Select(y => y.ForcedByUser).Where(u => u != null).Select(u => u.FirstName));
                yield return a => a.ApprovalGroups.SelectMany(x => x.Approvals.Select(y => y.ForcedByUser).Where(u => u != null).Select(u => u.LastName));
                yield return a => a.ApprovalGroups.SelectMany(x => x.Approvals.Select(y => y.ForcedByUser).Where(u => u != null).SelectMany(u => u.Employees.Select(e => e.Acronym)));
            }

            if (searchCriteria.Includes(SearchAreaCodes.Watchers))
            {
                yield return a => a.Watchers.Select(w => w.FirstName);
                yield return a => a.Watchers.Select(w => w.LastName);
                yield return a => a.Watchers.SelectMany(w => w.Employees.Select(e => e.Acronym));
            }

            if (searchCriteria.Includes(SearchAreaCodes.Description))
            {
                yield return r => r.DescriptionEn;
                yield return r => r.DescriptionPl;
            }
        }

        private bool CanBeEdited(RequestDto requestDto, IRequestService service, Action<AlertDto> addAlert)
        {
            var canEdit = service.CanBeEdited(requestDto, _principalProvider.Current.Id.Value);

            if (!canEdit)
            {
                addAlert(new AlertDto
                {
                    Type = AlertType.Warning,
                    Message = WorkflowResources.RequestAccessError,
                    IsDismissable = true
                });
            }

            return canEdit;
        }

        private bool CanBeDeleted(RequestDto requestDto, IRequestService service, Action<AlertDto> addAlert)
        {
            var canDelete = service.CanBeDeleted(requestDto, _principalProvider.Current.Id.Value);

            if (!canDelete)
            {
                addAlert(new AlertDto
                {
                    Type = AlertType.Warning,
                    Message = WorkflowResources.RequestAccessError,
                    IsDismissable = true
                });
            }

            return canDelete;
        }

        public RequestType GetRequestTypeById(long id)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Requests.Filtered().Where(r => r.Id == id).Select(r => r.RequestType).Single();
            }
        }

        public BulkAddRecordResult CreateRequestsExternally<TRequestDto>(RequestType requestType,
            IList<TRequestDto> requestObjectsDto) where TRequestDto : IRequestObject
        {
            var currentUserId = _principalProvider.Current.Id.GetValueOrDefault();
            var requestService = _requestServiceResolver.GetByServiceType(requestType);
            var bulkAddRecordResult = BulkAddRecordResult.Default;

            if (requestType != requestService.RequestType || requestService.DtoType != typeof(TRequestDto))
            {
                throw new InvalidRequestTypeException();
            }

            using (var transactionScope = TransactionHelper.CreateDefaultTransactionScope())
            {
                foreach (var requestObjectDto in requestObjectsDto)
                {
                    var requestDto = new RequestDto
                    {
                        RequestObject = requestObjectDto,
                        RequestType = requestType,
                        RequestingUserId = currentUserId,
                        Status = RequestStatus.Draft,
                        WatchersUserIds = new long[0],
                        Documents = new DocumentDto[0],
                    };

                    var addRecordResult = AddRecord(requestDto);
                    bulkAddRecordResult.AddedRecordIds.Add(addRecordResult.AddedRecordId);
                    bulkAddRecordResult.UniqueKeyViolations = bulkAddRecordResult.UniqueKeyViolations.Concat(addRecordResult.UniqueKeyViolations);
                    bulkAddRecordResult.Alerts.AddRange(addRecordResult.Alerts);
                    bulkAddRecordResult.IsSuccessful &= addRecordResult.IsSuccessful;
                }

                if (bulkAddRecordResult.IsSuccessful)
                {
                    transactionScope.Complete();
                }
            }

            return bulkAddRecordResult;
        }

        private static class RequestRetrievalSteps
        {
            public class Projection
            {
                public long Id { get; set; }

                public RequestType RequestType { get; set; }
            }

            public static ProjectionsWithPagination GetRequestProjections(
                IReadOnlyUnitOfWorkService<IWorkflowsDbScope> unitOfWorkService,
                Func<IQueryable<Request>, QueryCriteria, PaginationData<IQueryable<Request>>> applyFiltering,
                [CanBeNull] QueryCriteria criteria)
            {
                using (var unitOfWork = unitOfWorkService.Create())
                {
                    var records = System.Data.Entity.QueryableExtensions.AsNoTracking(unitOfWork.Repositories.Requests.Filtered());
                    var paginationData = applyFiltering(records, criteria);

                    return new ProjectionsWithPagination
                    {
                        PaginationData = paginationData,
                        EntityProjections = paginationData.Records.Select(r => new Projection
                        {
                            Id = r.Id,
                            RequestType = r.RequestType,
                        }).ToList(),
                    };
                }
            }

            public class ProjectionsWithPagination
            {
                public PaginationData<IQueryable<Request>> PaginationData { get; set; }

                public IList<Projection> EntityProjections { get; set; }

                public ICollection<QueryResult> RetrieveCompleteEntities(
                    IReadOnlyUnitOfWorkService<IWorkflowsDbScope> unitOfWorkService,
                    Func<IQueryable<Request>, IQueryable<Request>> configureIncludes,
                    IRequestServiceResolver requestServiceResolver,
                    IClassMapping<Request, RequestDto> requestToDtoMapping)
                {
                    var requests = new ConcurrentQueue<QueryResult>();

                    EntityProjections.GroupBy(e => e.RequestType).AsParallel().ForAll(typeAndIds =>
                    {
                        QueryResult queryResult;

                        using (var unitOfWork = unitOfWorkService.Create(true))
                        {
                            var records = configureIncludes(unitOfWork.Repositories.Requests);
                            records = System.Data.Entity.QueryableExtensions.AsNoTracking(records);

                            var service = requestServiceResolver.GetByServiceType(typeAndIds.Key);
                            var entities = service.ConfigureIncludes(records).GetByIds(typeAndIds.Select(r => r.Id));

                            var entitiesList = entities.ToList();

                            queryResult = new QueryResult
                            {
                                Requests = entitiesList,
                                RequestDtos = entitiesList.Select(requestToDtoMapping.CreateFromSource).ToList(),
                            };
                        }

                        requests.Enqueue(queryResult);
                    });

                    return requests.ToList();
                }
            }

            public class QueryResult
            {
                public IList<Request> Requests { get; set; }

                public IList<RequestDto> RequestDtos { get; set; }
            }
        }

        public override CardIndexRecords<RequestDto> GetRecords([CanBeNull] QueryCriteria criteria)
        {
            criteria = AdjustPageSize(criteria);

            var projections = RequestRetrievalSteps.GetRequestProjections(_readOnlyUnitOfWorkService, ApplyFiltering, criteria);
            var requests = projections.RetrieveCompleteEntities(_readOnlyUnitOfWorkService, ConfigureIncludes, _requestServiceResolver, _requestMapping);

            // Respect original order
            var originalRecordIds = projections.EntityProjections.Select(r => r.Id).ToList();
            var orderedEntities = requests.SelectMany(r => r.Requests).OrderBy(e => originalRecordIds.IndexOf(e.Id)).ToList();
            var orderedDtos = requests.SelectMany(r => r.RequestDtos).OrderBy(e => originalRecordIds.IndexOf(e.Id)).ToList();

            OnAfterRecordsRetrieved(new RecordsRetrievedEventArgs<Request>(orderedEntities, null, RetrievalScenario.MultipleRecords));

            // Approvers preview
            foreach (var requestDto in orderedDtos.Where(r => r.Status == RequestStatus.Draft))
            {
                _requestWorkflowService.AddApprovers(requestDto);
            }

            return new CardIndexRecords<RequestDto>(
                orderedDtos,
                criteria.CurrentPageIndex,
                projections.PaginationData.LastPageIndex,
                projections.PaginationData.RecordCount.Result);
        }

        protected override NamedFilters<Request> NamedFilters
        {
            get
            {
                var currentPrincipal = _principalProvider.Current;

                if (!currentPrincipal.IsAuthenticated || !currentPrincipal.Id.HasValue)
                {
                    return null;
                }

                var currentUserId = currentPrincipal.Id.Value;
                var currentDate = _timeService.GetCurrentDate();
                var currentMonth = DateHelper.BeginningOfMonth(currentDate);
                var previousMonth = currentMonth.AddMonths(-1);
                var nextMonth = currentMonth.AddMonths(1);

                return new NamedFilters<Request>(new[]
                {
                    new NamedFilter<Request>(FilterCodes.PreviousMonth,
                        RequestBusinessLogic.IsBetweenDates.Parametrize(previousMonth, currentMonth)),
                    new NamedFilter<Request>(FilterCodes.CurrentMonth,
                        RequestBusinessLogic.IsBetweenDates.Parametrize(currentMonth, nextMonth)),
                    new NamedFilter<Request, DateTime, DateTime>(FilterCodes.DateRange,
                        RequestBusinessLogic.IsBetweenDates.BaseExpression),

                    new NamedFilter<Request, long[]>(FilterCodes.RequesterCompany,
                        (r, companyIds) =>
                            companyIds.Contains(r.RequestingUser.Employees.FirstOrDefault().CompanyId.Value)),
                    new NamedFilter<Request, long[]>(FilterCodes.RequesterOrgUnit,
                        (r, orgUnitIds) =>
                            orgUnitIds.Any(
                                id => r.RequestingUser.Employees.Any(e => e.OrgUnit.Path.Contains("/" + id + "/")))),

                    new NamedFilter<Request>(FilterCodes.MyOpenRequests,
                        new BusinessLogic<Request, bool>(r =>
                            (RequestBusinessLogic.IsRequester.Call(r, currentUserId)
                                || RequestBusinessLogic.IsWatcher.Call(r, currentUserId)
                                || RequestBusinessLogic.IsWaitingForUserDecision.Call(r, currentUserId, currentDate)
                            ) && !RequestBusinessLogic.IsProcessed.Call(r))),

                    new NamedFilter<Request>(FilterCodes.MyClosedRequests,
                        new BusinessLogic<Request, bool>(r =>
                            (RequestBusinessLogic.IsRequester.Call(r, currentUserId)
                                || RequestBusinessLogic.IsWatcher.Call(r, currentUserId)
                                || RequestBusinessLogic.IsApprover.Call(r, currentUserId, currentDate)
                            ) && RequestBusinessLogic.IsProcessed.Call(r))),

                    new NamedFilter<Request>(FilterCodes.DecisionDone,
                        RequestBusinessLogic.IsDirectApproverWithDecissionDone.Parametrize(currentUserId)),

                    new NamedFilter<Request>(FilterCodes.RequestsRequestedByMe,
                        new BusinessLogic<Request, bool>(r =>
                            RequestBusinessLogic.IsRequester.Call(r, currentUserId) && !RequestBusinessLogic.IsProcessed.Call(r))),

                    new NamedFilter<Request>(FilterCodes.WatchedByMe, RequestBusinessLogic.IsWatcher.Parametrize(currentUserId)),

                    new NamedFilter<Request>(FilterCodes.AllRequests, r => true),

                    new NamedFilter<Request>(FilterCodes.DecisionAwaiting,
                        RequestBusinessLogic.IsWaitingForUserDecision.Parametrize(currentUserId, currentDate)),

                    new NamedFilter<Request>(FilterCodes.Attachments,
                        RequestBusinessLogic.HasDocuments),

                    new NamedFilter<Request, long[]>(
                        FilterCodes.ContractType,
                        (e, contractTypeIds) =>
                            e.RequestingUser.Employees.Any(
                                m => m.EmploymentPeriods.Any(p => contractTypeIds.Contains(p.ContractTypeId)))
                        ),

                    AffectedUserFilter.RequestFilter
                }

                .Union(
                    CardIndexServiceHelper.BuildEnumBasedFilters<Request, RequestType>(
                        r => r.RequestType))
                .Union(
                    CardIndexServiceHelper.BuildEnumBasedFilters<Request, RequestStatus>(
                        r => r.Status)));
            }
        }
    }
}
