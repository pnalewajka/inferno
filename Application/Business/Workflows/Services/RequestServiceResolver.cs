﻿using System;
using System.Linq;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Business.Workflows.Services
{
    public class RequestServiceResolver : IRequestServiceResolver
    {
        private IRequestService[] _allServices;

        public IRequestService GetByServiceType(RequestType type)
        {
            return GetAll().Single(s => s.RequestType == type);
        }

        public IRequestService GetByServiceType(string requestTypeName)
        {
            RequestType type;
            var isValid = Enum.TryParse(requestTypeName, out type);

            if (!isValid)
            {
                throw new ArgumentException("Incorrect RequestType Name");
            }

            return GetByServiceType(type);
        }

        public IRequestService[] GetAll()
        {
            return _allServices ?? (_allServices = ReflectionHelper.ResolveAll<IRequestService>().ToArray());
        }
    }
}
