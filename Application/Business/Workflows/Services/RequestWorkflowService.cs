﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Castle.Core.Internal;
using Castle.Core.Logging;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Workflows.BusinessLogics;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Enums;
using Smt.Atomic.Business.Workflows.Exceptions;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Workflows.Services
{
    public class RequestWorkflowService : IRequestWorkflowService
    {
        private readonly IActionSetService _actionSetService;
        private readonly IRequestServiceResolver _requestServiceResolver;
        private readonly IClassMapping<Request, RequestDto> _requestToDtoMapper;
        private readonly IClassMapping<Approval, ApprovalDto> _approvalToDtoMapper;
        private readonly IUnitOfWorkService<IWorkflowsDbScope> _unitOfWorkService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly ILogger _logger;
        private readonly ITimeService _timeService;
        private readonly IWorkflowNotificationService _workflowNotificationService;

        public RequestWorkflowService(
            IRequestServiceResolver requestServiceResolver,
            IActionSetService actionSetService,
            IClassMapping<Request, RequestDto> requestToDtoMapper,
            IUnitOfWorkService<IWorkflowsDbScope> unitOfWorkService,
            IPrincipalProvider principalProvider,
            IClassMapping<Approval, ApprovalDto> approvalToDtoMapper,
            ILogger logger,
            ITimeService timeService,
            IWorkflowNotificationService workflowNotificationService)
        {
            _actionSetService = actionSetService;
            _requestServiceResolver = requestServiceResolver;
            _requestToDtoMapper = requestToDtoMapper;
            _unitOfWorkService = unitOfWorkService;
            _principalProvider = principalProvider;
            _approvalToDtoMapper = approvalToDtoMapper;
            _logger = logger;
            _timeService = timeService;
            _workflowNotificationService = workflowNotificationService;
        }

        public BusinessResult Submit(long requestId)
        {
            BoolResult result;

            using (var transactionScope = TransactionHelper.CreateDefaultTransactionScope())
            {
                using (var unitOfWork = _unitOfWorkService.Create())
                {
                    var request = unitOfWork.Repositories.Requests
                        .Include(r => r.RequestingUser)
                        .Include(r => r.ApprovalGroups)
                        .GetById(requestId);
                    
                    if (request.Status != RequestStatus.Draft)
                    {
                        throw new InvalidRequestStateException();
                    }

                    var currentUserId = GetCurrentUserId();
                    var requestDto = _requestToDtoMapper.CreateFromSource(request);
                    var requestService = _requestServiceResolver.GetByServiceType(requestDto.RequestType);

                    if (!requestService.CanBeSubmitted(requestDto, currentUserId))
                    {
                        throw new RequestAccessException();
                    }

                    var validationResults = requestService.Validate(requestDto).ToList();

                    if (validationResults.Count > 0)
                    {
                        return new BusinessResult(validationResults);
                    }

                    var approvers = requestService.GetApproverGroups(requestDto).ToList();

                    var watchers = requestService.GetBusinessWatchersUserIds(requestDto);
                    CreateBusinessWatchers(unitOfWork, request, watchers);

                    if (approvers.Any())
                    {
                        var approvalNames = CreateApprovals(unitOfWork, request, approvers);

                        result = ChangeRequestStatus(request, RequestStatus.Pending);

                        if (result.IsSuccessful)
                        {
                            result.Alerts.Insert(0, requestService.GetRequestSubmittedAlert(requestDto, approvalNames));
                        }
                    }
                    else
                    {
                        var currentTime = _timeService.GetCurrentTime();
                        var statusChangingResult = ChangeRequestStatus(request, RequestStatus.Approved);

                        if (statusChangingResult.IsSuccessful)
                        {
                            if (requestService.ShouldExecuteAsAsync(requestDto) ||
                                (requestDto.DeferredUntil.HasValue && requestDto.DeferredUntil > currentTime))
                            {
                                result = ResultApproved(statusChangingResult.Alerts, true);
                            }
                            else
                            {
                                result = ExecuteRequest(request, requestDto, requestService);

                                foreach (var alert in statusChangingResult.Alerts.Reverse())
                                {
                                    result.Alerts.Insert(0, alert);
                                }
                            }
                        }
                        else
                        {
                            result = statusChangingResult;
                        }
                    }

                    unitOfWork.Commit();
                }

                if (result.IsSuccessful)
                {
                    transactionScope.Complete();
                }
            }

            return result;
        }

        private void CreateBusinessWatchers(IUnitOfWork<IWorkflowsDbScope> unitOfWork, Request request, IEnumerable<long> watchersUserIds)
        {
            var newWatchers = watchersUserIds.Where(w => request.Watchers.All(u => u.Id != w));
            var watcherUsers = unitOfWork.Repositories.Users.GetByIds(newWatchers);

            watcherUsers.ForEach(u => request.Watchers.Add(u));
        }

        public BusinessResult Approve(long requestId)
        {
            var currentUserId = GetCurrentUserId();

            return Approve(requestId, currentUserId, null);
        }

        private BusinessResult Approve(long requestId, long userId, long? forcedByUserId)
        {
            BoolResult result;
            using (var transactionScope = TransactionHelper.CreateDefaultTransactionScope())
            {
                using (var unitOfWork = _unitOfWorkService.Create())
                {
                    var request = unitOfWork.Repositories.Requests
                        .Include(r => r.RequestingUser)
                        .Include(r => r.ApprovalGroups)
                        .GetById(requestId);

                    var previousApprovalGroupId = GetApprovalGroupId(request);

                    if (forcedByUserId.HasValue && !CanForceActionOnBehalf(request, userId, forcedByUserId.Value))
                    {
                        throw new InvalidRequestStateException();
                    }

                    if (!IsPending(request) || IsApprovedByMyself(request, userId))
                    {
                        return RequestAlreadyProcessed(request, RequestStatus.Approved, RequestStatus.Completed, RequestStatus.Pending);
                    }

                    var requestDto = _requestToDtoMapper.CreateFromSource(request);
                    var requestService = _requestServiceResolver.GetByServiceType(requestDto.RequestType);

                    var approvals = GetUserApprovals(request, userId);

                    var approvalDtos = approvals.Select(_approvalToDtoMapper.CreateFromSource).ToList();
                    var approvingResult = requestService.OnApproving(request, requestDto, approvalDtos);

                    if (!approvingResult.IsSuccessful)
                    {
                        return new BusinessResult(approvingResult.Alerts);
                    }

                    approvals.ForEach(a =>
                    {
                        a.Status = ApprovalStatus.Approved;
                        a.ForcedByUserId = forcedByUserId;
                    });

                    request.HistoryEntries.Add(new RequestHistoryEntry
                    {
                        Date = _timeService.GetCurrentTime(),
                        Type = RequestHistoryEntryType.Approved,
                        ExecutingUserId = forcedByUserId ?? userId,
                        ApprovingUserId = userId
                    });

                    unitOfWork.Commit();

                    if (forcedByUserId != null)
                    {
                        var updatedApprovalDtos = approvals.Select(_approvalToDtoMapper.CreateFromSource).ToList();
                        requestService.OnForcedAction(requestDto, OnBehalfAction.Approve, updatedApprovalDtos);
                    }

                    var currentApprovalGroupId = GetApprovalGroupId(request);

                    // new group: new approvers available, let's notify them
                    if (currentApprovalGroupId.HasValue
                        && previousApprovalGroupId != currentApprovalGroupId)
                    {
                        _workflowNotificationService.NotifyAboutPendingApproval(
                            requestService,
                            _requestToDtoMapper.CreateFromSource(request));
                    }

                    if (RequestBusinessLogic.IsFullyApproved.Call(request))
                    {
                        var currentTime = _timeService.GetCurrentTime();

                        if (requestService.ShouldExecuteAsAsync(requestDto) ||
                            (requestDto.DeferredUntil.HasValue && requestDto.DeferredUntil > currentTime))
                        {
                            var statusChangingResult = ChangeRequestStatus(request, RequestStatus.Approved);

                            result = statusChangingResult.IsSuccessful
                                ? ResultApproved(statusChangingResult.Alerts, true)
                                : statusChangingResult;
                        }
                        else
                        {
                            var statusChangingResult = ChangeRequestStatus(request, RequestStatus.Approved);

                            if (statusChangingResult.IsSuccessful)
                            {
                                result = ExecuteRequest(request, requestDto, requestService);

                                foreach (var alert in statusChangingResult.Alerts)
                                {
                                    result.AddAlert(alert.Type, alert.Message, alert.IsDismissable);
                                }
                            }
                            else
                            {
                                result = statusChangingResult;
                            }
                        }
                    }
                    else
                    {
                        result = ResultApproved(new List<AlertDto>(), false);
                    }

                    unitOfWork.Commit();
                }

                if (result.IsSuccessful)
                {
                    transactionScope.Complete();
                }
            }

            return result;
        }

        private static long? GetApprovalGroupId(Request request)
        {
            return RequestBusinessLogic.CurrentApprovalGroup.Call(request)?.Id;
        }

        private static bool IsApprovedByMyself(Request request, long userId)
        {
            var currentApprovalGroup = RequestBusinessLogic.CurrentApprovalGroup.Call(request);

            if (currentApprovalGroup == null)
            {
                return false;
            }

            return currentApprovalGroup
                .Approvals.AsQueryable()
                .Where(ApprovalBusinessLogic.IsOriginalApprover.Parametrize(userId))
                .Any(ApprovalBusinessLogic.IsApproved.GetFunction());
        }

        public BusinessResult ApproveOnBehalfOf(long requestId, long onBehalfOfUserId)
        {
            var currentUserId = GetCurrentUserId();

            return Approve(requestId, onBehalfOfUserId, currentUserId);
        }

        public BusinessResult Reject(long requestId, string reason, bool cancellationRequest = false)
        {
            var currentUserId = GetCurrentUserId();

            return Reject(requestId, currentUserId, null, reason, cancellationRequest);
        }

        private bool CanForceActionOnBehalf(Request request, long userId, long forcedByUserId)
        {
            if (CannotBeForcedStatuses.Contains(request.Status))
            {
                return false;
            }

            if (_principalProvider.Current.IsInRole(SecurityRoleType.CanManageWorkflowsOnBehalf))
            {
                return true;
            }

            return IsSubstitute(userId, forcedByUserId);
        }

        private bool IsSubstitute(long replacedUserId, long substituteUserId)
        {
            var currentDate = _timeService.GetCurrentDate();

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return
                    unitOfWork.Repositories.Substitutions.Any(
                        SubstitutionBusinessLogic.IsSubstituting.Parametrize(replacedUserId, substituteUserId,currentDate));
            }
        }

        private BusinessResult Reject(long requestId, long userId, long? forcedByUserId, string reason, bool cancellationRequest = false)
        {
            BoolResult statusChangingResult;

            using (var transactionScope = TransactionHelper.CreateDefaultTransactionScope())
            {
                using (var unitOfWork = _unitOfWorkService.Create())
                {
                    var request = unitOfWork.Repositories.Requests
                            .Include(r => r.RequestingUser)
                            .Include(r => r.ApprovalGroups)
                            .GetById(requestId);

                    if (forcedByUserId.HasValue && !CanForceActionOnBehalf(request, userId, forcedByUserId.Value))
                    {
                        throw new InvalidRequestStateException();
                    }
                    
                    if (!IsPending(request))
                    {
                        return RequestAlreadyProcessed(request, RequestStatus.Rejected);
                    }

                    var requestDto = _requestToDtoMapper.CreateFromSource(request);
                    var requestService = _requestServiceResolver.GetByServiceType(request.RequestType);

                    if (!cancellationRequest)
                    {
                        var approvals = GetUserApprovals(request, userId);
                        approvals.ForEach(a =>
                        {
                            a.Status = ApprovalStatus.Rejected;
                            a.ForcedByUserId = forcedByUserId;
                        });

                        unitOfWork.Commit();

                        if (forcedByUserId != null)
                        {
                            var approvalDtos = approvals.Select(_approvalToDtoMapper.CreateFromSource).ToList();
                            requestService.OnForcedAction(requestDto, OnBehalfAction.Reject, approvalDtos);
                        }
                    }

                    request.HistoryEntries.Add(new RequestHistoryEntry
                    {
                        Date = _timeService.GetCurrentTime(),
                        Type = RequestHistoryEntryType.Rejected,
                        ExecutingUserId = forcedByUserId ?? userId,
                        ApprovingUserId = userId,
                        RejectionReason = reason
                    });

                    request.RejectionReason = reason;
                    statusChangingResult = ChangeRequestStatus(request, RequestStatus.Rejected);

                    if (!statusChangingResult.IsSuccessful)
                    {
                        return statusChangingResult;
                    }

                    unitOfWork.Commit();
                }

                transactionScope.Complete();
            }

            return ResultRejected(statusChangingResult.Alerts);
        }

        public BusinessResult RejectOnBehalfOf(long requestId, long onBehalfOfUserId, string reason,
            bool cancellationRequest = false)
        {
            var currentUserId = GetCurrentUserId();

            return Reject(requestId, onBehalfOfUserId, currentUserId, reason, cancellationRequest);
        }

        private BoolResult ForceDelete(Request request, IUnitOfWork<IWorkflowsDbScope> unitOfWork)
        {
            var dto = _requestToDtoMapper.CreateFromSource(request);
            var service = _requestServiceResolver.GetByServiceType(request.RequestType);

            var result = service.OnDeleting(request, dto, unitOfWork);

            if (result.IsSuccessful)
            {
                unitOfWork.Repositories.RequestHistoryEntries.DeleteRange(request.HistoryEntries);
                unitOfWork.Repositories.Requests.Delete(request);
                unitOfWork.Commit();
            }

            return result;
        }

        public BoolResult Delete(long requestId)
        {
            using (var transactionScope = TransactionHelper.CreateDefaultTransactionScope())
            {
                using (var unitOfWork = _unitOfWorkService.Create())
                {
                    var request = unitOfWork.Repositories.Requests.GetById(requestId);
                    var result = ForceDelete(request, unitOfWork);

                    if (result.IsSuccessful)
                    {
                        transactionScope.Complete();
                    }

                    return result;
                }
            }
        }

        public BoolResult ExecuteApproved(long requestId)
        {
            BoolResult result;
            using (var transactionScope = TransactionHelper.CreateDefaultTransactionScope())
            {
                using (var unitOfWork = _unitOfWorkService.Create())
                {
                    var request = unitOfWork.Repositories.Requests.GetById(requestId);

                    if (request.Status != RequestStatus.Approved)
                    {
                        throw new InvalidRequestStateException();
                    }

                    var requestDto = _requestToDtoMapper.CreateFromSource(request);
                    var requestService = _requestServiceResolver.GetByServiceType(requestDto.RequestType);

                    result = ExecuteRequest(request, requestDto, requestService);

                    unitOfWork.Commit();
                }

                if (result.IsSuccessful)
                {
                    transactionScope.Complete();
                }
            }

            return result;
        }

        public long[] GetApprovedRequestIds()
        {
            var currentTime = _timeService.GetCurrentTime();

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Requests
                    .Where(d => d.Status == RequestStatus.Approved && (d.DeferredUntil == null || d.DeferredUntil <= currentTime))
                    .Select(d => d.Id)
                    .ToArray();
            }
        }

        public void AddApprovers(RequestDto requestDto)
        {
            var requestService = _requestServiceResolver.GetByServiceType(requestDto.RequestType);

            var approverGroups = requestService.GetApproverGroups(requestDto).ToList();
            var approversEmployeeIds = approverGroups.SelectMany(g => g.Approvers.Select(a => a.EmployeeId)).Distinct().ToList();

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employeeDictionary = unitOfWork.Repositories
                    .Employees
                    .GetByIds(approversEmployeeIds)
                    .ToList()
                    .Select(e => new
                    {
                        e.Id,
                        e.UserId,
                        e.DisplayName,
                        e.Email
                    })
                    .ToDictionary(e => e.Id);

                requestDto.CurrentApprovalGroupId = null;
                requestDto.ApprovalGroups = approverGroups.Select(g => new ApprovalGroupDto
                {
                    Mode = g.Mode,
                    Approvals = g.Approvers.Select(a =>
                    {
                        if (!employeeDictionary.ContainsKey(a.EmployeeId))
                        {
                            throw new InvalidOperationException($"Approvers with Id={a.EmployeeId} is not user in this system");
                        }

                        var employee = employeeDictionary[a.EmployeeId];

                        return new ApprovalDto
                        {
                            UserId = employee.UserId.Value,
                            Status = ApprovalStatus.Pending,
                            ApproverFullName = employee.DisplayName,
                            ApproverEmail = employee.Email,
                        };
                    }).ToList()
                }).ToArray();
            }
        }

        public ApprovalDto GetApproval(long requestId, long approvalId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var approval =
                    unitOfWork.Repositories.Approvals.Single(
                        a => a.ApprovalGroup.RequestId == requestId && a.Id == approvalId);

                return _approvalToDtoMapper.CreateFromSource(approval);
            }
        }

        public RequestStatus[] CannotBeForcedStatuses => new[] { RequestStatus.Completed, RequestStatus.Error, RequestStatus.Rejected };

        private ICollection<string> CreateApprovals(IUnitOfWork<IWorkflowsDbScope> unitOfWork, Request request, IList<ApproverGroupDto> approverGroups)
        {
            var employeeIds = approverGroups.SelectMany(g => g.Approvers.Select(a => a.EmployeeId)).Distinct().ToList();
            var employees = unitOfWork.Repositories.Employees
                .Include(e => e.User)
                .GetOrderedByIds(employeeIds);

            if (employees.Any(e => e.UserId == null))
            {
                throw new InvalidOperationException("Some of the approvers are not users in this system");
            }

            var approvalGroups = approverGroups.Select((g, i) => new ApprovalGroup
            {
                Order = i,
                Mode = g.Mode,
                Approvals =
                    g.Approvers.Select(a => CreateApproval(employees.Single(e => e.Id == a.EmployeeId).User)).ToList()
            }).ToList();

            unitOfWork.Repositories.Approvals.DeleteRange(request.ApprovalGroups.SelectMany(g => g.Approvals));
            unitOfWork.Repositories.ApprovalGroups.DeleteRange(request.ApprovalGroups);

            // Saving to DB!!!
            approvalGroups.ForEach(g => request.ApprovalGroups.Add(g));
            unitOfWork.Commit();

            return employees
                .Select(e => e.DisplayName)
                .ToList();
        }

        private IList<Approval> GetUserApprovals(Request request, long userId)
        {
            if (!IsUserAuthorizedToApprove(request, userId))
            {
                throw new RequestAccessException();
            }

            var approvals = request.ApprovalGroups.SelectMany(
                g => g.Approvals.AsQueryable().Where(ApprovalBusinessLogic.IsDirectApprover.Parametrize(userId)))
                .ToList();

            if (approvals.All(a => !ApprovalBusinessLogic.IsPending.Call(a)))
            {
                throw new InvalidRequestStateException();
            }

            return approvals;
        }

        private BoolResult ExecuteRequest(
            Request request,
            RequestDto requestDto,
            IRequestService requestService)
        {
            BoolResult statusChangingResult;

            try
            {
                requestService.Execute(requestDto);

                request.HistoryEntries.Add(new RequestHistoryEntry
                {
                    Date = _timeService.GetCurrentTime(),
                    Type = RequestHistoryEntryType.Executed,
                    ExecutingUserId = _principalProvider.Current.Id
                });
            }
            catch (Exception exception)
            {
                request.HistoryEntries.Add(new RequestHistoryEntry
                {
                    Date = _timeService.GetCurrentTime(),
                    Type = RequestHistoryEntryType.ErrorOccurred,
                    ExecutingUserId = _principalProvider.Current.Id
                });

                _logger.Error($"Execution of request has failed: {exception.ToString()}");

                request.Exception = exception.Message;

                statusChangingResult = ChangeRequestStatus(request, RequestStatus.Error);

                return ResultFailed(statusChangingResult.Alerts);
            }

            statusChangingResult = ChangeRequestStatus(request, RequestStatus.Completed);

            return statusChangingResult.IsSuccessful
                ? ResultExecuted(statusChangingResult.Alerts)
                : statusChangingResult;
        }

        private BoolResult ChangeRequestStatus(Request request, RequestStatus status)
        {
            var requestService = _requestServiceResolver.GetByServiceType(request.RequestType);

            request.Status = status;

            var description = new LocalizedString(() => requestService.GetRequestDescription(request));
            request.DescriptionEn = description.English;
            request.DescriptionPl = description.Polish;

            var requestDto = _requestToDtoMapper.CreateFromSource(request);

            switch (status)
            {
                case RequestStatus.Approved:
                    requestService.OnAllApproved(request, requestDto);
                    break;

                case RequestStatus.Rejected:
                    requestService.OnRejected(request, requestDto);
                    break;

                case RequestStatus.Pending:
                    requestService.OnSubmited(request, requestDto);
                    break;

                case RequestStatus.Error:
                    requestService.OnException(request, requestDto);
                    break;

                case RequestStatus.Completed:
                    requestService.OnExecuted(request, requestDto);
                    break;
            }

            var executionResult = _actionSetService.ExecuteActionSets(request, requestDto);

            return new BoolResult(!executionResult.ContainsErrors, executionResult.Alerts);
        }

        private long GetCurrentUserId()
        {
            return _principalProvider.Current.Id.Value;
        }

        private static bool IsPending(Request request)
        {
            return request.Status == RequestStatus.Pending;
        }

        private static Approval CreateApproval(User user)
        {
            var approval = new Approval
            {
                Status = ApprovalStatus.Pending,
                UserId = user.Id,
                User = user
            };

            return approval;
        }

        private bool IsUserAuthorizedToApprove(Request request, long userId)
        {
            return RequestBusinessLogic.IsApprover.Call(request, userId, _timeService.GetCurrentDate());
        }

        public bool IsWaitingForDirectUserDecision(long requestId, long userId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var request = unitOfWork.Repositories.Requests
                    .Include(r => r.RequestingUser)
                    .Include(r => r.ApprovalGroups)
                    .GetByIdOrDefault(requestId);

                if (request == null)
                {
                    return false;
                }

                return RequestBusinessLogic.IsWaitingForDirectUserDecision.Call(request, userId);
            }
        }

        public bool IsWaitingForSubstituteUserDecision(long requestId, long userId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var request = unitOfWork.Repositories.Requests
                    .Include(r => r.RequestingUser)
                    .Include(r => r.ApprovalGroups)
                    .GetByIdOrDefault(requestId);

                if (request == null)
                {
                    return false;
                }

                return
                    RequestBusinessLogic.IsWaitingForSubstituteUserDecision.Call(
                        request, userId, _timeService.GetCurrentDate());
            }
        }

        private BoolResult ResultApproved(IList<AlertDto> alerts, bool lastApproval)
        {
            alerts.Insert(0, new AlertDto
            {
                Type = AlertType.Success,
                Message = lastApproval ? WorkflowResources.RequestApprovedByAll : WorkflowResources.RequestApproved,
                IsDismissable = true
            });

            return new BoolResult(true, alerts);
        }

        private BoolResult ResultRejected(IList<AlertDto> alerts)
        {
            alerts.Insert(0, new AlertDto
            {
                Type = AlertType.Success,
                Message = WorkflowResources.RequestRejected,
                IsDismissable = true
            });

            return new BoolResult(true, alerts);
        }

        private BusinessResult RequestAlreadyProcessed(Request request, params RequestStatus[] desiredStatuses)
        {
            if (!new[]
            {
                RequestStatus.Approved, RequestStatus.Rejected, RequestStatus.Completed, RequestStatus.Pending
            }.Contains(request.Status))
            {
                throw new InvalidRequestStateException();
            }

            var alertType = desiredStatuses.Contains(request.Status) ? AlertType.Success : AlertType.Warning;
            var message = request.Status == RequestStatus.Rejected
                ? string.Format(WorkflowResources.RequestHasBeenAlreadyRejected, FormatApprovers(request, ApprovalStatus.Rejected))
                : string.Format(WorkflowResources.RequestHasBeenAlreadyApproved, FormatApprovers(request, ApprovalStatus.Approved));
            
            return new BusinessResult(alertType, message);
        }

        private string FormatApprovers(Request request, ApprovalStatus status)
        {
            var approvers = request.ApprovalGroups
                .SelectMany(g => g.Approvals)
                .Where(a => a.Status == status)
                .DistinctBy(a => a.User.Id)
                .Select(a => a.User.FullName)
                .OrderBy(n => n);

            return string.Join(",", approvers);
        }

        private BoolResult ResultExecuted(IList<AlertDto> alerts)
        {
            alerts.Insert(0, new AlertDto
            {
                Type = AlertType.Success,
                Message = WorkflowResources.RequestExecuted,
                IsDismissable = true
            });

            return new BoolResult(true, alerts);
        }

        private BoolResult ResultFailed(IList<AlertDto> alerts)
        {
            alerts.Insert(0, new AlertDto
            {
                Type = AlertType.Warning,
                Message = WorkflowResources.RequestFailed,
                IsDismissable = true
            });

            return new BoolResult(false, alerts);
        }
    }
}
