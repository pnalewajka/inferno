﻿using System;
using System.Collections.Generic;
using Castle.Core.Logging;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Workflows.Jobs
{
    [Identifier("Job.Workflows.ExecuteAsyncRequestsJobs")]
    [JobDefinition(
        Code = "ExecuteAsyncRequests",
        Description = "Executes approved requests",
        ScheduleSettings = "*/2 * * * * *",
        PipelineName = PipelineCodes.System)]
    public class ExecuteAsyncRequestsJobs : IJob
    {
        private readonly ILogger _logger;
        private readonly IRequestWorkflowService _requestWorkflowService;

        public ExecuteAsyncRequestsJobs(
            ILogger logger,
            IRequestWorkflowService requestWorkflowService)
        {
            _logger = logger;
            _requestWorkflowService = requestWorkflowService;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            try
            {
                var requestToExecuteIds = _requestWorkflowService.GetApprovedRequestIds();

                foreach (var id in requestToExecuteIds)
                {
                    ExecuteRequest(id);
                }

                return JobResult.Success();
            }
            catch (Exception ex)
            {

                _logger.Error(ex.Message);
                return JobResult.Fail();
            }
        }

        private void ExecuteRequest(long id)
        {
            var result = _requestWorkflowService.ExecuteApproved(id);

            if (!result.IsSuccessful)
            {
                LogAlerts(id, result.Alerts);
            }
        }

        private void LogAlerts(long id, IEnumerable<AlertDto> alerts)
        {
            foreach (var alert in alerts)
            {
                var log = GetLogMethod(alert.Type);
                var message = $"Executing Request (id={id}) failed: {alert.Message}";
                log(message);
            }
        }

        private Action<string> GetLogMethod(AlertType type)
        {
            switch (type)
            {
                case AlertType.Error:
                    return _logger.Error;
                case AlertType.Warning:
                    return _logger.Warn;
                case AlertType.Success:
                case AlertType.Information:
                    return message => { };
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }
    }
}
