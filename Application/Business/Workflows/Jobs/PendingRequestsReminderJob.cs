﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Logging;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.Business.Workflows.BusinessLogics;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Comparers;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Workflows.Jobs
{
    [Identifier("Job.Workflows.PendingRequestReminderJob")]
    [JobDefinition(
        Code = "PendingRequestsReminder",
        Description = "Sends reminders about requests",
        ScheduleSettings = "0 0 0 * * *",
        MaxExecutioPeriodInSeconds = 1800,
        PipelineName = PipelineCodes.Notifications)]
    public class PendingRequestsReminderJob : IJob
    {
        private readonly ILogger _logger;
        private readonly IWorkflowNotificationService _workflowNotificationService;
        private readonly ITimeService _timeService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IUnitOfWorkService<IWorkflowsDbScope> _unitOfWorkService;
        private readonly IClassMapping<User, UserDto> _userClassMapping;
        private readonly IClassMapping<Request, RequestDto> _requestClassMapping;

        public PendingRequestsReminderJob(
            ILogger logger,
            IWorkflowNotificationService workflowNotificationService,
            ITimeService timeService,
            ISystemParameterService systemParameterService,
            IUnitOfWorkService<IWorkflowsDbScope> unitOfWorkService,
            IClassMapping<User, UserDto> userClassMapping,
            IClassMapping<Request, RequestDto> requestClassMapping)
        {
            _logger = logger;
            _workflowNotificationService = workflowNotificationService;
            _timeService = timeService;
            _systemParameterService = systemParameterService;
            _unitOfWorkService = unitOfWorkService;
            _userClassMapping = userClassMapping;
            _requestClassMapping = requestClassMapping;
        }

        public JobResult DoJob(JobExecutionContext jobExecutionContext)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                SendRemindersAboutRequestsToBeApproved(unitOfWork);
                SendRemindersAboutNotSubmittedRequests(unitOfWork);
            }

            return JobResult.Success();
        }

        private void SendRemindersAboutRequestsToBeApproved(IUnitOfWork<IWorkflowsDbScope> unitOfWork)
        {
            var userEqualityComparer = new ByKeyEqualityComparer<User, long>(u => u.Id);
            var currentDate = _timeService.GetCurrentDate();
            var daysUntilSendingRequestReminders = _systemParameterService.GetParameter<int>(ParameterKeys.DaysUntilSendingRequestReminders);
            var minimumNotificationDate = currentDate.AddDays(-daysUntilSendingRequestReminders);

            var approvalGroups = unitOfWork.Repositories.Requests
                .Where(r => r.Status == RequestStatus.Pending && r.CreatedOn < minimumNotificationDate)
                .Select(RequestBusinessLogic.CurrentApprovalGroup.AsExpression());

            var relevantApprovals = new List<Approval>();

            foreach (var group in approvalGroups)
            {
                var previousGroup = group.Request.ApprovalGroups.OrderBy(g => g.Order).FirstOrDefault(g => g.Order == group.Order - 1);
                var latestActionDate = previousGroup?.Approvals.Max(a => a.ModifiedOn) ?? group.Request.CreatedOn;

                if (latestActionDate < minimumNotificationDate)
                {
                    relevantApprovals.AddRange(
                        group.Approvals.Where(ApprovalBusinessLogic.IsPending.GetFunction()));
                }
            }

            var approvalsByUser = relevantApprovals
                .GroupBy(a => a.User, userEqualityComparer)
                .ToDictionary(g => g.Key, g => g.ToList(), userEqualityComparer);

            AddSubstitutes(currentDate, approvalsByUser);

            foreach (var pair in approvalsByUser)
            {
                var user = pair.Key;

                if (user.IsActive)
                {
                    var userDto = _userClassMapping.CreateFromSource(user);

                    try
                    {
                        var requestIds = pair.Value.Select(a => a.ApprovalGroup.RequestId).Distinct();

                        var requestsDtos = unitOfWork.Repositories.Requests
                            .GetByIds(requestIds)
                            .Select(_requestClassMapping.CreateFromSource)
                            .ToList();

                        _workflowNotificationService.NotifyAboutPendingRequests(userDto, requestsDtos);
                    }
                    catch (Exception e)
                    {
                        _logger.ErrorFormat(e, "Pending requests notification for user {0} failed", userDto.Email);
                    }
                }
            }
        }

        private void SendRemindersAboutNotSubmittedRequests(IUnitOfWork<IWorkflowsDbScope> unitOfWork)
        {
            var currentDate = _timeService.GetCurrentDate();
            var daysUntilSendingRequestReminders = _systemParameterService.GetParameter<int>(ParameterKeys.DaysUntilSendingRequestReminders);
            var minimumNotificationDate = currentDate.AddDays(-daysUntilSendingRequestReminders);

            var notSubmittedRequests = unitOfWork.Repositories.Requests
                .Where(r => r.Status == RequestStatus.Draft && r.RequestingUser.IsActive && r.CreatedOn < minimumNotificationDate)
                .ToList()
                .GroupBy(r => r.RequestingUser, new ByKeyEqualityComparer<User, long>(u => u.Id));

            foreach (var group in notSubmittedRequests)
            {
                var user = group.Key;
                var requests = group.ToList();

                try
                {
                    var userDto = _userClassMapping.CreateFromSource(user);
                    var requestsDtos = requests.Select(_requestClassMapping.CreateFromSource);

                    _workflowNotificationService.NotifyAboutNotSubmittedRequests(userDto, requestsDtos);
                }
                catch (Exception e)
                {
                    _logger.ErrorFormat(e, "Not submitted requests notification for user {0} failed", user.Email);
                }
            }
        }

        private void AddSubstitutes(DateTime currentDate, Dictionary<User, List<Approval>> approvalsByUser)
        {
            Dictionary<User, List<Approval>> approvalsBySubstitute = new Dictionary<User, List<Approval>>();

            foreach (var pair in approvalsByUser)
            {
                var user = pair.Key;
                var approvals = pair.Value;

                var substituteUsers = user.Substitutions
                    .Where(s => s.SubstituteUser.IsActive && s.From <= currentDate && s.To >= currentDate)
                    .DistinctBy(u => u.SubstituteUser.Id)
                    .Select(s => s.SubstituteUser)
                    .ToList();

                foreach (var substituteUser in substituteUsers)
                {
                    if (!approvalsBySubstitute.ContainsKey(substituteUser))
                    {
                        approvalsBySubstitute.Add(substituteUser, approvals);
                    }
                }
            }

            foreach (var pair in approvalsBySubstitute)
            {
                var user = pair.Key;
                var approvals = pair.Value;

                List<Approval> list;

                if (approvalsByUser.TryGetValue(user, out list))
                {
                    list.AddRange(approvals);
                }
                else
                {
                    list = new List<Approval>(approvals);
                    approvalsByUser.Add(user, list);
                }
            }
        }
    }
}
