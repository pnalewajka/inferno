﻿using System.Xml.Serialization;

namespace Smt.Atomic.Business.Workflows.Actions
{
    public abstract class ActionDefinition
    {
        [XmlAttribute("Id")]
        public string Identifier { get; set; }

        [XmlAttribute("UpdatesOn")]
        public string UpdatesOn { get; set; }

        [XmlAttribute("Active")]
        public bool IsActive { get; set; } = true;

        [XmlAttribute("Crucial")]
        public bool IsCrucial { get; set; }
    }
}
