﻿using System.Xml.Serialization;

namespace Smt.Atomic.Business.Workflows.Actions
{
    [XmlType("CustomField")]
    public class JiraIssueCustomFieldValue
    {
        [XmlAttribute("Id")]
        public string Id { get; set; }

        [XmlAttribute("Value")]
        public string Value { get; set; }
    }
}
