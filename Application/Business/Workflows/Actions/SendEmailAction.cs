﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Enums;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Workflows.Actions
{
    public class SendEmailAction : BaseAction<SendEmailActionDefinition>
    {
        private readonly ITimeService _timeService;
        private readonly IReliableEmailService _emailService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IUnitOfWorkService<IAllocationDbScope> _unitOfWorkService;

        public SendEmailAction(
            ITimeService timeService,
            IReliableEmailService emailService,
            IMessageTemplateService messageTemplateService,
            ISystemParameterService systemParameterService,
            IUnitOfWorkService<IAllocationDbScope> unitOfWorkService)
        {
            _timeService = timeService;
            _emailService = emailService;
            _messageTemplateService = messageTemplateService;
            _systemParameterService = systemParameterService;
            _unitOfWorkService = unitOfWorkService;
        }

        protected override RequestHistoryEntryDto Execute(SendEmailActionDefinition definition, RequestDto request)
        {
            var isHtml = false;
            var body = definition.Body;

            if (body == null)
            {
                var resolvedBody = _messageTemplateService.ResolveTemplate(definition.BodyTemplateCode, request);

                body = resolvedBody.Content;
                isHtml = resolvedBody.IsHtml;
            }

            var subject = definition.Subject;

            if (subject == null)
            {
                subject = _messageTemplateService.ResolveTemplate(definition.SubjectTemplateCode, request).Content;
            }

            var recipients = new List<EmailRecipientDto>();

            recipients.AddRange(GetRecipients(definition.To, RecipientType.To));
            recipients.AddRange(GetRecipients(definition.CC, RecipientType.Cc));

            var message = new EmailMessageDto
            {
                Body = body,
                IsHtml = isHtml,
                Subject = subject,
                Recipients = recipients
            };

            _emailService.EnqueueMessage(message);

            return new RequestHistoryEntryDto
            {
                Date = _timeService.GetCurrentTime(),
                Type = RequestHistoryEntryType.MailSent,
                ActionIdentifier = definition.Identifier,
                EmailSubject = message.Subject,
                EmailRecipients = JsonHelper.Serialize(message.Recipients),
                IsActionActive = definition.IsActive,
                UpdatesOn = definition.UpdatesOn
            };
        }

        protected override IEnumerable<ValidationResult> Validate(SendEmailActionDefinition definition, RequestDto request)
        {
            if (string.IsNullOrEmpty(definition.Body) == string.IsNullOrEmpty(definition.BodyTemplateCode))
            {
                yield return new ValidationResult(ActionSetResources.SendEmailBodyError);
            }

            if (string.IsNullOrEmpty(definition.Subject) == string.IsNullOrEmpty(definition.SubjectTemplateCode))
            {
                yield return new ValidationResult(ActionSetResources.SendEmailSubjectError);
            }

            if (string.IsNullOrEmpty(definition.To) && string.IsNullOrEmpty(definition.CC))
            {
                yield return new ValidationResult(ActionSetResources.SendEmailRecipientsError);
            }
        }

        public void Update(string subject, string recipients, SendEmailActionChangeDescriptionDto changeDescription)
        {
            if (string.IsNullOrEmpty(recipients))
            {
                return;
            }
            
            var deserializedRecipients = JsonHelper.Deserialize<List<EmailRecipientDto>>(recipients);
            var body = _messageTemplateService.ResolveTemplate(TemplateCodes.WorkflowsSendEmailActionUpdateBody, changeDescription);

            var message = new EmailMessageDto
            {
                Body = body.Content,
                IsHtml = body.IsHtml,
                Subject = $"Re: {subject}",
                Recipients = deserializedRecipients
            };

            _emailService.EnqueueMessage(message);
        }

        private IEnumerable<EmailRecipientDto> GetRecipients(string recipients, RecipientType recipientType)
        {
            if (string.IsNullOrEmpty(recipients))
            {
                yield break;
            }

            var recipientsArray = recipients.Split(new[] { ' ', ';' }, StringSplitOptions.RemoveEmptyEntries);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                foreach (var recipient in recipientsArray)
                {
                    var employee = unitOfWork.Repositories.Employees
                        .Where(e => e.Acronym == recipient || e.Email == recipient)
                        .Select(e => new { FullName = (e.FirstName + " " + e.LastName), e.Email })
                        .SingleOrDefault();

                    if (employee != null)
                    {
                        yield return new EmailRecipientDto
                        {
                            FullName = employee.FullName,
                            EmailAddress = employee.Email,
                            Type = recipientType
                        };
                    }
                    else if (EmailHelper.IsValidEmail(recipient))
                    {
                        yield return new EmailRecipientDto
                        {
                            EmailAddress = recipient,
                            Type = recipientType
                        };
                    }
                }
            }
        }

        protected override string ToErrorMessage(SendEmailActionDefinition definition)
        {
            var supportAddress = _systemParameterService.GetParameter<string>(ParameterKeys.SystemSupportEmailAddress);

            return !string.IsNullOrEmpty(definition.Subject)
                ? string.Format(ActionSetResources.SendEmailWithSubjectMessageError, definition.Subject, supportAddress)
                : string.Format(ActionSetResources.SendEmailMessageError, supportAddress);
        }

        public override string ToString()
        {
            return nameof(SendEmailAction);
        }
    }
}
