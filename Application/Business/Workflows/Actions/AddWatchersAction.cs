﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Workflows.Actions
{
    public class AddWatchersAction : BaseAction<AddWatchersActionDefinition>
    {
        private readonly ITimeService _timeService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IUnitOfWorkService<IWorkflowsDbScope> _unitOfWorkService;

        public AddWatchersAction(
            ITimeService timeService,
            ISystemParameterService systemParameterService,
            IUnitOfWorkService<IWorkflowsDbScope> unitOfWorkService)
        {
            _timeService = timeService;
            _systemParameterService = systemParameterService;
            _unitOfWorkService = unitOfWorkService;
        }

        protected override RequestHistoryEntryDto Execute(AddWatchersActionDefinition definition, RequestDto request)
        {
            var acronyms = SplitAcronyms(definition.Acronyms);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var requestEntity = unitOfWork.Repositories.Requests.GetById(request.Id);
                var watchersToAdd = unitOfWork.Repositories.Employees
                    .Where(e => acronyms.Contains(e.Acronym) && e.UserId.HasValue)
                    .Select(e => new { e.User, e.FirstName, e.LastName })
                    .AsEnumerable()
                    .Where(u => !request.WatchersUserIds.Contains(u.User.Id))
                    .ToList();
                
                foreach (var watcher in watchersToAdd)
                {
                    requestEntity.Watchers.Add(watcher.User);
                }

                unitOfWork.Commit();

                return !watchersToAdd.Any() ? null : new RequestHistoryEntryDto
                {
                    Date = _timeService.GetCurrentTime(),
                    Type = RequestHistoryEntryType.WatchersAdded,
                    ActionIdentifier = definition.Identifier,
                    WatcherNames = string.Join(", ", watchersToAdd.Select(w => $"{w.FirstName} {w.LastName}")),
                    IsActionActive = false
                };
            }
        }

        protected override IEnumerable<ValidationResult> Validate(AddWatchersActionDefinition definition, RequestDto request)
        {
            if (!SplitAcronyms(definition.Acronyms).Any())
            {
                yield return new ValidationResult(ActionSetResources.AddWatcherAcronymsError);
            }
        }

        protected override string ToErrorMessage(AddWatchersActionDefinition definition)
        {
            var supportAddress = _systemParameterService.GetParameter<string>(ParameterKeys.SystemSupportEmailAddress);

            return string.Format(ActionSetResources.AddWatcherMessageError, supportAddress);
        }

        public override string ToString()
        {
            return nameof(AddWatchersAction);
        }

        private static string[] SplitAcronyms(string acronyms)
        {
            return acronyms.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Select(a => a.Trim()).ToArray();
        }
    }
}
