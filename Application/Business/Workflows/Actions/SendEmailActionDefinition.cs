﻿using System.Xml.Serialization;

namespace Smt.Atomic.Business.Workflows.Actions
{
    [XmlType("SendEmail")]
    public class SendEmailActionDefinition : ActionDefinition
    {
        [XmlText]
        public string Body { get; set; }

        [XmlAttribute("TemplateCode")]
        public string BodyTemplateCode { get; set; }

        [XmlAttribute("Subject")]
        public string Subject { get; set; }

        [XmlAttribute("SubjectTemplateCode")]
        public string SubjectTemplateCode { get; set; }

        [XmlAttribute("To")]
        public string To { get; set; }

        [XmlAttribute("CC")]
        public string CC { get; set; }
    }
}
