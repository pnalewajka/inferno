﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Jira.Dto;
using Smt.Atomic.Data.Jira.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Workflows.Actions
{
    public class CreateJiraIssueAction : BaseAction<CreateJiraIssueActionDefinition>
    {
        private readonly IJiraService _jiraService;
        private readonly ITimeService _timeService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IUnitOfWorkService<IAccountsDbScope> _unitOfWorkService;

        public CreateJiraIssueAction(
            IJiraService jiraService,
            ITimeService timeService,
            ISystemParameterService systemParameterService,
            IUnitOfWorkService<IAccountsDbScope> unitOfWorkService)
        {
            _jiraService = jiraService;
            _timeService = timeService;
            _systemParameterService = systemParameterService;
            _unitOfWorkService = unitOfWorkService;
        }

        protected override RequestHistoryEntryDto Execute(CreateJiraIssueActionDefinition definition, RequestDto request)
        {
            using (var context = _jiraService.CreateReadWrite())
            {
                var newIssue = new NewIssueDto
                {
                    ProjectKey = definition.Project,
                    IssueType = definition.Type,
                    Summary = definition.Summary,
                    Description = definition.Description,
                    DueDate = definition.DueDate,
                    Security = definition.Security,
                    Reporter = definition.Reporter ?? request.RequesterEmail
                };

                foreach (var customField in definition.CustomFields ?? Enumerable.Empty<JiraIssueCustomFieldValue>())
                {
                    newIssue.CustomFields.Add($"customfield_{customField.Id}", new[] { customField.Value });
                }

                var issueKey = context.CreateIssue(newIssue);

                return new RequestHistoryEntryDto
                {
                    Date = _timeService.GetCurrentTime(),
                    Type = RequestHistoryEntryType.JiraIssueCreated,
                    ActionIdentifier = definition.Identifier,
                    JiraIssueKey = issueKey,
                    IsActionActive = definition.IsActive,
                    UpdatesOn = definition.UpdatesOn
                };
            }
        }

        protected override IEnumerable<ValidationResult> Validate(CreateJiraIssueActionDefinition definition, RequestDto request)
        {
            if (string.IsNullOrEmpty(definition.Project))
            {
                yield return new ValidationResult(ActionSetResources.CreateJiraIssueProjectError);
            }

            if (string.IsNullOrEmpty(definition.Summary))
            {
                yield return new ValidationResult(ActionSetResources.CreateJiraIssueSummaryError);
            }

            if (string.IsNullOrEmpty(definition.Type))
            {
                yield return new ValidationResult(ActionSetResources.CreateJiraIssueTypeError);
            }

            foreach (var customField in definition.CustomFields ?? Enumerable.Empty<JiraIssueCustomFieldValue>())
            {
                if (string.IsNullOrEmpty(customField.Id))
                {
                    yield return new ValidationResult(ActionSetResources.CreateJiraIssueCustomFieldIdError);
                }
            }
        }

        public void Update(string issueKey, string differencesDescription, string authorFullName)
        {
            using (var context = _jiraService.CreateReadWrite())
            {
                context.AddCommentToIssue(issueKey, string.Format(ActionSetResources.CreateJiraIssueUpdateComment, differencesDescription, authorFullName));
            }
        }

        protected override string ToErrorMessage(CreateJiraIssueActionDefinition definition)
        {
            var supportAddress = _systemParameterService.GetParameter<string>(ParameterKeys.SystemSupportEmailAddress);

            return string.Format(ActionSetResources.CreateJiraIssueMessageError, definition.Summary, supportAddress);
        }

        public override string ToString()
        {
            return nameof(CreateJiraIssueAction);
        }
    }
}
