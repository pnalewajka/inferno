﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;

namespace Smt.Atomic.Business.Workflows.Actions
{
    public abstract class BaseAction<TActionDefinition> : IAction where TActionDefinition : ActionDefinition
    {
        public Type DefinitionType => typeof(TActionDefinition);

        public RequestHistoryEntryDto Execute(object definition, RequestDto request)
        {
            return Execute((TActionDefinition)definition, request);
        }

        public IEnumerable<ValidationResult> Validate(object definition, RequestDto request)
        {
            return Validate((TActionDefinition)definition, request);
        }

        public string ToErrorMessage(object definition)
        {
            return ToErrorMessage((TActionDefinition)definition);
        }

        protected abstract RequestHistoryEntryDto Execute(TActionDefinition definition, RequestDto request);

        protected abstract IEnumerable<ValidationResult> Validate(TActionDefinition definition, RequestDto request);

        protected abstract string ToErrorMessage(TActionDefinition definition);
    }
}
