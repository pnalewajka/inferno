﻿using System.Xml.Serialization;

namespace Smt.Atomic.Business.Workflows.Actions
{
    [XmlRoot("Actions")]
    public class ActionSetDefinition
    {
        [XmlElement(typeof(AddWatchersActionDefinition))]
        [XmlElement(typeof(CreateJiraIssueActionDefinition))]
        [XmlElement(typeof(SendEmailActionDefinition))]
        public ActionDefinition[] ActionDefinitions { get; set; }
    }
}
