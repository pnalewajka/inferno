﻿using System.Xml.Serialization;

namespace Smt.Atomic.Business.Workflows.Actions
{
    [XmlType("AddWatchers")]
    public class AddWatchersActionDefinition : ActionDefinition
    {
        [XmlAttribute("Acronyms")]
        public string Acronyms { get; set; }
    }
}
