﻿using System;
using System.Globalization;
using System.Xml.Serialization;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Business.Workflows.Actions
{
    [XmlType("CreateJiraIssue")]
    public class CreateJiraIssueActionDefinition : ActionDefinition
    {
        private const string JiraDateFormat = "dd/MMM/yy";
        
        [XmlArray("CustomFields")]
        [XmlArrayItem(typeof(JiraIssueCustomFieldValue))]
        public JiraIssueCustomFieldValue[] CustomFields { get; set; }

        [XmlText]
        public string Description { get; set; }

        [XmlIgnore]
        public DateTime? DueDate { get; set; }

        [XmlAttribute("DueDate")]
        public string FormattedDueDate
        {
            get
            {
                return DueDate.HasValue ? DueDate.Value.ToInvariantString(JiraDateFormat) : null;
            }
            set
            {
                DueDate = !string.IsNullOrWhiteSpace(value)
                    ? DateTime.ParseExact(value, JiraDateFormat, CultureInfo.InvariantCulture)
                    : (DateTime?)null;
            }
        }

        [XmlAttribute("Project")]
        public string Project { get; set; }

        [XmlAttribute("Reporter")]
        public string Reporter { get; set; }

        [XmlAttribute("Security")]
        public string Security { get; set; }

        [XmlAttribute("Summary")]
        public string Summary { get; set; }

        [XmlAttribute("Type")]
        public string Type { get; set; }
    }
}
