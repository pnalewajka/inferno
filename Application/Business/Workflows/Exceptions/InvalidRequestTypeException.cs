﻿using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.CrossCutting.Common.Exceptions;

namespace Smt.Atomic.Business.Workflows.Exceptions
{
    public class InvalidRequestTypeException : BusinessException
    {
        public InvalidRequestTypeException()
            : base(WorkflowResources.InvalidRequestState)
        {
        }
    }
}
