﻿using System;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.CrossCutting.Common.Exceptions;

namespace Smt.Atomic.Business.Workflows.Exceptions
{
    public class InvalidRequestStateException : BusinessException
    {
        public InvalidRequestStateException()
            : base(WorkflowResources.InvalidRequestState)
        {
        }
    }
}
