﻿using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.CrossCutting.Common.Exceptions;

namespace Smt.Atomic.Business.Workflows.Exceptions
{
    public class RequestAccessException : BusinessException
    {
        public RequestAccessException()
            : base(WorkflowResources.RequestAccessError)
        {
        }
    }
}
