﻿using Smt.Atomic.CrossCutting.Common.Exceptions;

namespace Smt.Atomic.Business.Workflows.Exceptions
{
    public class RequestMissingDataForStatusChangeBusinessException : BusinessException
    {
        public RequestMissingDataForStatusChangeBusinessException(string message) : base(message)
        {
        }
    }
}
