﻿using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.Business.Workflows.Enums
{
    // Update ActionOnBehalf.ts too!
    public enum OnBehalfAction
    {
        [DescriptionLocalized("Approve", typeof(WorkflowResources))]
        Approve = 0,

        [DescriptionLocalized("Reject", typeof(WorkflowResources))]
        Reject = 1,
    }
}
