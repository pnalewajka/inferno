﻿namespace Smt.Atomic.Business.Workflows.Enums
{
    public enum RequestSaveAction
    {
        Save = 0,
        SaveAndSubmit = 1
    }
}
