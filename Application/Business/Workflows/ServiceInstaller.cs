﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Workflows.Actions;
using Smt.Atomic.Business.Workflows.ChoreProviders;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.Workflows.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.Workflows
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            switch (containerType)
            {
                case ContainerType.WebApp:
                case ContainerType.WebApi:
                case ContainerType.WebServices:
                case ContainerType.JobScheduler:
                case ContainerType.UnitTests:
                    {
                        container.Register(Component.For<IAction>().ImplementedBy<AddWatchersAction>().LifestyleTransient());
                        container.Register(Component.For<IAction>().ImplementedBy<CreateJiraIssueAction>().LifestyleTransient());
                        container.Register(Component.For<IAction>().ImplementedBy<SendEmailAction>().LifestyleTransient());
                        container.Register(Component.For<IActionSetService>().ImplementedBy<ActionSetService>().LifestyleTransient());
                        container.Register(Component.For<IRequestStatusCardIndexDataService>().ImplementedBy<RequestStatusCardIndexDataService>().LifestyleTransient());
                        container.Register(Component.For<IActionSetCardIndexDataService>().ImplementedBy<ActionSetCardIndexDataService>().LifestyleTransient());
                        container.Register(Component.For<IRequestCardIndexDataService>().ImplementedBy<RequestCardIndexDataService>().LifestyleTransient());
                        container.Register(Component.For<IRequestHistoryEntryCardIndexDataService>().ImplementedBy<RequestHistoryEntryCardIndexDataService>().LifestyleTransient());
                        container.Register(Component.For<IRequestWorkflowService>().ImplementedBy<RequestWorkflowService>().LifestyleTransient());
                        container.Register(Component.For<IWorkflowNotificationService>().ImplementedBy<EmailWorkflowNotificationService>().LifestyleTransient());
                        container.Register(Component.For<IApprovalCardIndexDataService>().ImplementedBy<ApprovalCardIndexDataService>().LifestyleTransient());
                        container.Register(Component.For<ISubstitutionCardIndexDataService>().ImplementedBy<SubstitutionCardIndexDataService>().LifestyleTransient());
                        container.Register(Component.For<IAbsenceRequestCardIndexDataService>().ImplementedBy<AbsenceRequestCardIndexDataService>().LifestyleTransient());
                        container.Register(Component.For<ITaxDeductibleCostRequestCardIndexDataService>().ImplementedBy<TaxDeductibleCostRequestCardIndexDataService>().LifestyleTransient());
                        container.Register(Component.For<IBonusRequestCardIndexDataService>().ImplementedBy<BonusRequestCardIndexDataService>().LifestyleTransient());
                        container.Register(Component.For<IExpenseRequestCardIndexDataService>().ImplementedBy<ExpenseRequestCardIndexDataService>().LifestyleTransient());
                        break;
                    }
            }

            if (containerType == ContainerType.JobScheduler)
            {
                container.Register(Component.For<IRequestServiceResolver>().ImplementedBy<RequestServiceResolver>().LifestylePerThread());
                container.Register(Component.For<IChoreProvider>().ImplementedBy<PendingRequestChoreProvider>().LifestylePerThread());
            }
            else
            {
                container.Register(Component.For<IRequestServiceResolver>().ImplementedBy<RequestServiceResolver>().LifestylePerWebRequest());
                container.Register(Component.For<IChoreProvider>().ImplementedBy<PendingRequestChoreProvider>().LifestylePerWebRequest());
            }
        }
    }
}