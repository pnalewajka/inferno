﻿using System;
using System.Collections.Generic;

namespace Smt.Atomic.Business.Workflows.EmailModels
{
    public class RequestsReminderEmailModel
    {
        public string ApproverFullName { get; set; }

        public IEnumerable<RequestEmailModel> Requests { get; set; }

        public Uri RequestsLink { get; set; }
    }
}
