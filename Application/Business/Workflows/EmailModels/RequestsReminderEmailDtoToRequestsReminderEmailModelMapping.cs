﻿using System;
using System.Linq;
using Smt.Atomic.Business.Workflows.Consts;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.Business.Workflows.EmailModels
{
    public class RequestsReminderEmailDtoToRequestsReminderEmailModelMapping : ClassMapping<RequestsReminderEmailDto, RequestsReminderEmailModel>
    {
        public RequestsReminderEmailDtoToRequestsReminderEmailModelMapping(
            IClassMapping<RequestEmailDto, RequestEmailModel> requestMapping,
            ISystemParameterService systemParameterService)
        {
            var baseUri = new Uri(systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress));

            Mapping =
                d =>
                    new RequestsReminderEmailModel
                    {
                        ApproverFullName = d.ApproverFullName,
                        Requests = d.Requests.Select(requestMapping.CreateFromSource),
                        RequestsLink = new Uri(baseUri, "Workflows/Request/List?filter=" + FilterCodes.MyOpenRequests)
                    };
        }
    }
}
