﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Business.Workflows.EmailModels
{
    public class RequestEmailDto : RequestDto
    {
        public OnBehalfAction Action { get; set; }

        public string ApproverFullName { get; set; }

        public string ForcedByUserFullName { get; set; }

        public string RejecterFullNames { get; set; }

        public string AdditionalContent { get; set; }

        public RequestEmailDto(RequestDto parent)
        {
            // Copy all property values
            foreach (var prop in TypeHelper.GetPublicInstanceProperties(parent.GetType()))
            {
                GetType().GetProperty(prop.Name).SetValue(this, prop.GetValue(parent, null), null);
            }
        }
    }
}
