﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using System;
using Smt.Atomic.Business.Workflows.Consts;

namespace Smt.Atomic.Business.Workflows.EmailModels
{
    public class RequestEmailDtoToRequestEmailModelMapping: ClassMapping<RequestEmailDto, RequestEmailModel>
    {
        private readonly IClassMappingFactory _classMappingFactory;
        private readonly IRequestServiceResolver _requestServiceResolver;
      
        public RequestEmailDtoToRequestEmailModelMapping(
            IClassMappingFactory classMappingFactory,
            ISystemParameterService systemParameters,
            IRequestServiceResolver requestServiceResolver)
        {
            _classMappingFactory = classMappingFactory;
            _requestServiceResolver = requestServiceResolver;
            Uri baseUri = new Uri(systemParameters.GetParameter<string>(ParameterKeys.ServerAddress));
            Mapping =
                d =>
                    new RequestEmailModel
                    {
                        Id = d.Id,
                        Status = d.Status,
                        RequestType = d.RequestType,
                        RequestObject = ConvertRequestObjectToViewModel(d),
                        RequestingUserId = d.RequestingUserId,
                        RequesterFullName = d.RequesterFullName,
                        RequesterEmail = d.RequesterEmail,
                        RejectionReason = d.RejectionReason,
                        Exception = d.Exception,
                        Description = d.Description.English,
                        AllRequestsLink = new Uri(baseUri, "Workflows/Request/List"),
                        AllAwaitingRequestsLink = new Uri(baseUri, "Workflows/Request/List?filter=decision-awaiting"),
                        MyRequestsLink = new Uri(baseUri, "Workflows/Request/List?filter=" + FilterCodes.MyOpenRequests),
                        RequestsWatchedByMeLink = new Uri(baseUri, "/Workflows/Request/List?filter=" + FilterCodes.WatchedByMe),
                        RequestLink = new Uri(baseUri, "Workflows/Request/View/" + d.Id.ToString()),
                        RequestEditLink = new Uri(baseUri, "Workflows/Request/Edit/" + d.Id.ToString()),
                        Action = d.Action,
                        ApproverFullName = d.ApproverFullName,
                        ForcedByUserFullName = d.ForcedByUserFullName,
                        RejecterFullNames = d.RejecterFullNames,
                        AdditionalContent = d.AdditionalContent,
                    };
        }

        private object ConvertRequestObjectToViewModel(RequestDto requestDto)
        {
            var requestService = _requestServiceResolver.GetByServiceType(requestDto.RequestType);
            var mapping = _classMappingFactory.CreateMapping(
                requestDto.RequestObject.GetType(),
                TypeHelper.FindTypeInLoadedAssemblies(requestService.ViewModelTypeName));

            return mapping.CreateFromSource(requestDto.RequestObject);
        }
    }
}
