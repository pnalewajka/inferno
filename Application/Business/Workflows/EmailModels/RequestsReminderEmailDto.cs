﻿using System.Collections.Generic;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Business.Workflows.EmailModels
{
    public class RequestsReminderEmailDto
    {
        public string ApproverFullName { get; set; }

        public IEnumerable<RequestEmailDto> Requests { get; set; }

        public RequestsReminderEmailDto(UserDto approver, IEnumerable<RequestEmailDto> requests)
        {
            ApproverFullName = approver.GetFullName();
            Requests = requests;
        }
    }
}
