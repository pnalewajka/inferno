﻿using System;
using Smt.Atomic.Business.Workflows.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Workflows.EmailModels
{
    public class RequestEmailModel
    {
        public long Id { get; set; }

        public RequestStatus Status { get; set; }

        public RequestType RequestType { get; set; }

        public object RequestObject { get; set; }

        public long RequestingUserId { get; set; }

        public string RequesterFullName { get; set; }

        public string RequesterEmail { get; set; }

        public string RejectionReason { get; set; }

        public string Exception { get; set; }

        public string Description { get; set; }

        public Uri AllRequestsLink { get; set; }

        public Uri AllAwaitingRequestsLink { get; set; }

        public Uri MyRequestsLink { get; set; }

        public Uri RequestsWatchedByMeLink { get; set; }

        public Uri RequestLink { get; set; }

        public Uri RequestEditLink { get; set; }

        public OnBehalfAction Action { get; set; }

        public string ApproverFullName { get; set; }

        public string ForcedByUserFullName { get; set; }

        public string RejecterFullNames { get; set; }

        public string AdditionalContent { get; set; }
    }
}
