﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Workflows.BusinessLogics;
using Smt.Atomic.Business.Workflows.Consts;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Filters
{
    public static class AbsenceTypeFilter
    {
        public static readonly NamedFilter<Request, long> AbsenceRequestFilter = new NamedFilter<Request, long>(
            FilterCodes.AbsenceRequestType,
            (request, absenceTypeId) => request.AbsenceRequest.AbsenceTypeId == absenceTypeId);
    }
}
