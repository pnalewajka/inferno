﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Workflows.BusinessLogics;
using Smt.Atomic.Business.Workflows.Consts;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Filters
{
    public static class AffectedUserFilter
    {
        public static readonly NamedFilter<Request> RequestFilter = new NamedFilter<Request, long>(
            FilterCodes.AffectedUser,
            RequestBusinessLogic.BelongsToAnyOfAffectedUsers);
    }
}
