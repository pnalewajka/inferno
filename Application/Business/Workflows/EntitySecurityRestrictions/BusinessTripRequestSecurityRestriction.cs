﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.EntitySecurityRestrictions
{
    public class BusinessTripRequestSecurityRestriction
        : RequestRestriction<BusinessTripRequest>
    {
        public BusinessTripRequestSecurityRestriction(
            IPrincipalProvider principalProvider,
            ITimeService timeService)
            : base(principalProvider, timeService)
        {
        }

        public override Expression<Func<BusinessTripRequest, Request>> RequestSelector => r => r.Request;

        public override Expression<Func<Request, BusinessTripRequest>> RequestObjectSelector => r => r.BusinessTripRequest;

        public override RequestType RequestType => RequestType.BusinessTripRequest;

        public override Expression<Func<BusinessTripRequest, bool>> GetSpecificRequestRestriction()
        {
            if (Roles.Contains(SecurityRoleType.CanViewAllBusinessTripRequests))
            {
                return AllRecords();
            }

            return NoRecords();
        }
    }
}
