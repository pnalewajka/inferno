﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.EntitySecurityRestrictions
{
    public class AdvancedPaymentRequestSecurityRestriction
        : RequestRestriction<AdvancedPaymentRequest>
    {
        public AdvancedPaymentRequestSecurityRestriction(
            IPrincipalProvider principalProvider,
            ITimeService timeService)
            : base(principalProvider, timeService)
        {
        }

        public override Expression<Func<AdvancedPaymentRequest, Request>> RequestSelector => r => r.Request;

        public override Expression<Func<Request, AdvancedPaymentRequest>> RequestObjectSelector => r => r.AdvancedPaymentRequest;

        public override RequestType RequestType => RequestType.AdvancedPaymentRequest;

        public override Expression<Func<AdvancedPaymentRequest, bool>> GetSpecificRequestRestriction()
        {
            if (Roles.Contains(SecurityRoleType.CanViewAllAdvancedPaymentRequests))
            {
                return AllRecords();
            }

            return r => r.Participant.EmployeeId == CurrentPrincipal.EmployeeId;
        }
    }
}
