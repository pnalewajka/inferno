﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Workflows.BusinessLogics;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.Workflows.EntitySecurityRestrictions
{
    public class RequestSecurityRestriction : EntitySecurityRestriction<Request>
    {
        private readonly ITimeService _timeService;

        public RequestSecurityRestriction(
            IPrincipalProvider principalProvider,
            ITimeService timeService)
            : base(principalProvider)
        {
            _timeService = timeService;

            RequiresAuthentication = true;
        }

        public override Expression<Func<Request, bool>> GetRestriction()
        {
            if (Roles.Contains(SecurityRoleType.CanViewAllRequests))
            {
                return AllRecords();
            }

            if (Roles.Contains(SecurityRoleType.CanViewRequest))
            {
                var requestRestrictions = TypeHelper.GetAllSubClassesOf(typeof(RequestRestriction<>), false);

                var restriction = RequestRestriction.GetBaseExpression(CurrentPrincipal.Id.Value, _timeService.GetCurrentDate());

                foreach (var requestRestriction in requestRestrictions)
                {
                    var instance = (IRequestRestriction)ReflectionHelper.CreateInstanceWithIocDependencies(requestRestriction);
                    var predicate = new BusinessLogic<Request, bool>(instance.GetRequestRestriction());

                    restriction = restriction.Or(new BusinessLogic<Request, bool>(
                        r => r.RequestType == instance.RequestType && predicate.Call(r)));
                }

                return restriction;
            }

            return NoRecords();
        }
    }
}
