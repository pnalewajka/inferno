﻿using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.Workflows.EntitySecurityRestrictions
{
    public class EmployeePhotoDocumentSecurityRestriction : RelatedEntitySecurityRestriction<EmployeePhotoDocument, Request>
    {
        public EmployeePhotoDocumentSecurityRestriction(
            IPrincipalProvider principalProvider)
            : base(principalProvider)
        {
        }

        protected override BusinessLogic<EmployeePhotoDocument, Request> RelatedEntitySelector =>
            new BusinessLogic<EmployeePhotoDocument, Request>(employeePhotoDocument => employeePhotoDocument.OnboardingRequest.Request);
    }
}
