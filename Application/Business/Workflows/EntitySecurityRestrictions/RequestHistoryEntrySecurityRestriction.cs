﻿using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.Workflows.EntitySecurityRestrictions
{
    public class RequestHistoryEntrySecurityRestriction : RelatedEntitySecurityRestriction<RequestHistoryEntry, Request>
    {
        public RequestHistoryEntrySecurityRestriction(
           IPrincipalProvider principalProvider)
            : base(principalProvider)
        {
        }

        protected override BusinessLogic<RequestHistoryEntry, Request> RelatedEntitySelector =>
            new BusinessLogic<RequestHistoryEntry, Request>(requestHistoryEntry => requestHistoryEntry.Request);
    }
}
