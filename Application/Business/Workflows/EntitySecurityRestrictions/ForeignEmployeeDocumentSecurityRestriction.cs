﻿using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.Workflows.EntitySecurityRestrictions
{
    public class ForeignEmployeeDocumentSecurityRestriction : RelatedEntitySecurityRestriction<ForeignEmployeeDocument, Request>
    {
        public ForeignEmployeeDocumentSecurityRestriction(
            IPrincipalProvider principalProvider)
            : base(principalProvider)
        {
        }

        protected override BusinessLogic<ForeignEmployeeDocument, Request> RelatedEntitySelector =>
            new BusinessLogic<ForeignEmployeeDocument, Request>(foreignEmployeeDocument => foreignEmployeeDocument.OnboardingRequest.Request);
    }
}
