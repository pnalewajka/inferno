﻿using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.Workflows.EntitySecurityRestrictions
{
    public class PayrollDocumentSecurityRestriction : RelatedEntitySecurityRestriction<PayrollDocument, Request>
    {
        public PayrollDocumentSecurityRestriction(
            IPrincipalProvider principalProvider)
            : base(principalProvider)
        {
        }

        protected override BusinessLogic<PayrollDocument, Request> RelatedEntitySelector =>
            new BusinessLogic<PayrollDocument, Request>(payrollDocument => payrollDocument.OnboardingRequest.Request);
    }
}
