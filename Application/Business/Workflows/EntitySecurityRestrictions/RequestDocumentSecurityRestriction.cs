﻿using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.Workflows.EntitySecurityRestrictions
{
    public class RequestDocumentSecurityRestriction : RelatedEntitySecurityRestriction<RequestDocument, Request>
    {
        public RequestDocumentSecurityRestriction(
            IPrincipalProvider principalProvider)
            : base(principalProvider)
        {
        }

        protected override BusinessLogic<RequestDocument, Request> RelatedEntitySelector =>
            new BusinessLogic<RequestDocument, Request>(requestDocument => requestDocument.Request);
    }
}
