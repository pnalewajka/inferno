﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.Business.Workflows.BusinessLogics;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.Workflows.EntitySecurityRestrictions
{
    public static class RequestRestriction
    {
        public static Expression<Func<Request, bool>> GetBaseExpression(long userId, DateTime date)
        {
            return new BusinessLogic<Request, bool>(r => RequestBusinessLogic.IsRequester.Call(r, userId)
                || RequestBusinessLogic.IsWatcher.Call(r, userId)
                || RequestBusinessLogic.IsDirectApprover.Call(r, userId)
                || RequestBusinessLogic.IsWaitingForSubstituteUserDecision.Call(r, userId, date));
        }
    }

    public abstract class RequestRestriction<TRequest>
        : EntitySecurityRestriction<TRequest>
        , IRequestRestriction
        where TRequest : IRequestEntity
    {
        protected readonly ITimeService TimeService;

        public RequestRestriction(
            IPrincipalProvider principalProvider,
            ITimeService timeService)
            : base(principalProvider)
        {
            TimeService = timeService;
        }

        public abstract RequestType RequestType { get; }

        public abstract Expression<Func<Request, TRequest>> RequestObjectSelector { get; }
        public abstract Expression<Func<TRequest, Request>> RequestSelector { get; }

        public sealed override Expression<Func<TRequest, bool>> GetRestriction()
        {
            var currentUserId = CurrentPrincipal.Id.Value;
            var currentDate = TimeService.GetCurrentDate();

            var expression = new BusinessLogic<Request, bool>(
                RequestRestriction.GetBaseExpression(currentUserId, currentDate));

            var selector = new BusinessLogic<TRequest, Request>(RequestSelector);

            var innerRestriction = new BusinessLogic<TRequest, bool>(GetSpecificRequestRestriction());

            return new BusinessLogic<TRequest, bool>(r => expression.Call(selector.Call(r)) || innerRestriction.Call(r));
        }

        public abstract Expression<Func<TRequest, bool>> GetSpecificRequestRestriction();

        public Expression<Func<Request, bool>> GetRequestRestriction()
        {
            var expression = new BusinessLogic<TRequest, bool>(GetSpecificRequestRestriction());
            var selector = new BusinessLogic<Request, TRequest>(RequestObjectSelector);

            return new BusinessLogic<Request, bool>(r => expression.Call(selector.Call(r)));
        }
    }
}
