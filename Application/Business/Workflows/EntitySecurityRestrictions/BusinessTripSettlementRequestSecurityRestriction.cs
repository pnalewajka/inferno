﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.EntitySecurityRestrictions
{
    public class BusinessTripSettlementRequestSecurityRestriction
        : RequestRestriction<BusinessTripSettlementRequest>
    {
        public BusinessTripSettlementRequestSecurityRestriction(
            IPrincipalProvider principalProvider,
            ITimeService timeService)
            : base(principalProvider, timeService)
        {
        }

        public override Expression<Func<BusinessTripSettlementRequest, Request>> RequestSelector => r => r.Request;

        public override Expression<Func<Request, BusinessTripSettlementRequest>> RequestObjectSelector => r => r.BusinessTripSettlementRequest;

        public override RequestType RequestType => RequestType.BusinessTripSettlementRequest;

        public override Expression<Func<BusinessTripSettlementRequest, bool>> GetSpecificRequestRestriction()
        {
            if (Roles.Contains(SecurityRoleType.CanViewAllBusinessTripSettlementRequests))
            {
                return AllRecords();
            }

            var currentEmployeeId = CurrentPrincipal.EmployeeId;

            return request =>
                request.BusinessTripParticipant.EmployeeId == currentEmployeeId;
        }
    }
}
