﻿using System;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Workflows.BusinessLogics;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.Workflows.ChoreProviders
{
    public class PendingRequestChoreProvider
        : IChoreProvider
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly ITimeService _timeService;

        public PendingRequestChoreProvider(
            IPrincipalProvider principalProvider,
            ITimeService timeService)
        {
            _principalProvider = principalProvider;
            _timeService = timeService;
        }

        public IQueryable<ChoreDto> GetActiveChores(IReadOnlyRepositoryFactory repositoryFactory)
        {
            var currentUserId = _principalProvider.Current.Id.Value;
            var currentDate = _timeService.GetCurrentDate();
            var isWaitingForUserDecision = RequestBusinessLogic.IsWaitingForUserDecision.Parametrize(currentUserId, currentDate);

            var requests = repositoryFactory.Create<Request>();

            var pendingRequests = requests
                .Where(isWaitingForUserDecision);

            return pendingRequests
                .GroupBy(r => 0)
                .Select(g => new ChoreDto
                {
                    Type = ChoreType.PendingRequests,
                    DueDate = null,
                    Parameters = g.Count().ToString(),
                });
        }
    }
}
