﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Workflows.Interfaces
{
    public interface IRequestObject
    {
        long Id { get; set; }
    }
}
