﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.Workflows.Interfaces
{
    public interface IRequestRestriction : IEntitySecurityRestriction
    {
        RequestType RequestType { get; }

        Expression<Func<Request, bool>> GetRequestRestriction();
    }
}
