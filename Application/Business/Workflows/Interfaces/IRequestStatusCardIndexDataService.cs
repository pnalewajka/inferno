﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;

namespace Smt.Atomic.Business.Workflows.Interfaces
{
    public interface IRequestStatusCardIndexDataService : ICardIndexDataService<RequestStatusDto>
    {
    }
}
