﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.Interfaces
{
    public interface IActionSetService 
    {
        BusinessResult ExecuteActionSets(Request request, RequestDto requestDto);

        IEnumerable<ValidationResult> ValidateActionSet(ActionSet actionSet, IRequestService requestService);

        BusinessResult UpdateActions<TDto>(Request request, TDto oldRequestObjectDto, TDto newRequestObjectDto, Type viewModelType);

        void AddCommentToJiraIssues(Request request, string comment);

        void SendNotificationToEmailRecipients(Request request, RequestDto requestDto, string subject, string body);
    }
}
