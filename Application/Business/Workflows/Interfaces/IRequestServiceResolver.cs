﻿using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Workflows.Interfaces
{
    public interface IRequestServiceResolver
    {
        IRequestService GetByServiceType(RequestType type);

        IRequestService GetByServiceType(string requestTypeName);

        IRequestService[] GetAll();
    }
}
