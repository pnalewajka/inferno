﻿namespace Smt.Atomic.Business.Workflows.Interfaces
{
    public interface IRequestAdditionalInformation
    {
        string AdditionalInformation { get; }
    }
}