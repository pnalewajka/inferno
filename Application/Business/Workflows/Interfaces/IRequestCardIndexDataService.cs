﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Workflows.Interfaces
{
    public interface IRequestCardIndexDataService : ICardIndexDataService<RequestDto>
    {
        BulkAddRecordResult CreateRequestsExternally<TRequestDto>(RequestType requestType, IList<TRequestDto> requestsDto) where TRequestDto : IRequestObject;

        RequestType GetRequestTypeById(long id);
    }
}

