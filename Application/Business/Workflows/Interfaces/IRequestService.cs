﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Workflows.Interfaces
{
    public interface IRequestService
    {
        RequestType RequestType { get; }
        RequestTypeGroup RequestTypeGroup { get; }
        string RequestName { get; }
        Type DtoType { get; }
        Type EntityType { get; }
        string ViewModelTypeName { get; }
        SecurityRoleType? RequiredRoleType { get; }
        bool CanBeStartedManually();
        IRequestObject GetDraft(IDictionary<string, object> parameters);
        bool ShouldStartAsDraft { get; }
        IEnumerable<AlertDto> Validate(RequestDto request);
        IEnumerable<ApproverGroupDto> GetApproverGroups(RequestDto request);
        IEnumerable<long> GetBusinessWatchersUserIds(RequestDto request);
        EmailRecipientDto[] GetExtraWorkflowNotificationRecipients(RequestDto request);
        IQueryable<Request> ConfigureIncludes(IQueryable<Request> repository);
        string GetRequestDescription(RequestDto request);
        string GetRequestDescription(Request request);
        bool ShouldExecuteAsAsync(RequestDto request);
        void Execute(RequestDto request);
        bool CanBeAdded();
        bool CanBeEdited(RequestDto requestDto, long byUserId);
        bool CanBeDeleted(RequestDto requestDto, long byUserId);
        bool CanBeSubmitted(RequestDto requestDto, long byUserId);
        void OnForcedAction(RequestDto request, OnBehalfAction action, IList<ApprovalDto> affectedApprovals);
        void OnAdding(RecordAddingEventArgs<Request, RequestDto, IWorkflowsDbScope> addingEventArgs);
        void OnAdded(RecordAddedEventArgs<Request, RequestDto> addedEventArgs);
        void OnAllApproved(Request request, RequestDto requestDto);
        void OnRejected(Request request, RequestDto requestDto);
        void OnSubmited(Request request, RequestDto requestDto);
        void OnExecuted(Request request, RequestDto requestDto);
        void OnException(Request request, RequestDto requestDto);
        void OnEditing(RecordEditingEventArgs<Request, RequestDto, IWorkflowsDbScope> editingEventArgs);
        void OnEdited(RecordEditedEventArgs<Request, RequestDto> editedEventArgs);
        BoolResult OnDeleting(Request request, RequestDto requestDto, IUnitOfWork<IWorkflowsDbScope> unitOfWork);
        BoolResult OnApproving(Request request, RequestDto requestDto, IList<ApprovalDto> affectedApprovals);
        AlertDto GetRequestSubmittedAlert(RequestDto request, ICollection<string> approvalNames);
        string ApprovalNotificationAdditionalContent(RequestDto request);
        string RejectionNotificationAdditionalContent(RequestDto request);
        string ExecutionNotificationAdditionalContent(RequestDto request);
        string ExceptionNotificationAdditionalContent(RequestDto request);
        string PendingApprovalNotificationAdditionalContent(RequestDto request);
        string ApprovalNotificationEmailBodyTemplateCode { get; }
        string RejectionNotificationEmailBodyTemplateCode { get; }
        string ExecutionNotificationEmailBodyTemplateCode { get; }
        string ExceptionNotificationEmailBodyTemplateCode { get; }
        string PendingApprovalNotificationEmailBodyTemplateCode { get; }
        IEnumerable<EmailRecipientDto> GetExceptionNotificationRecipients(RequestDto request);
    }
}