﻿using System.Collections.Generic;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Enums;

namespace Smt.Atomic.Business.Workflows.Interfaces
{
    public interface IWorkflowNotificationService
    {
        void NotifyAboutForcedAction(RequestDto request, OnBehalfAction action, IList<ApprovalDto> affectedApprovals);

        void NotifyAboutApproval(IRequestService requestService, RequestDto request);

        void NotifyAboutRejection(IRequestService requestService, RequestDto request);

        void NotifyAboutExecution(IRequestService requestService, RequestDto request);

        void NotifyAboutException(IRequestService requestService, RequestDto request);

        void NotifyAboutPendingApproval(IRequestService requestService, RequestDto request);

        void NotifyAboutPendingRequests(UserDto user, IEnumerable<RequestDto> requests);

        void NotifyAboutNotSubmittedRequests(UserDto user, IEnumerable<RequestDto> requests);
    }
}
