﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Workflows.Interfaces
{
    public interface IActionSetCardIndexDataService : ICardIndexDataService<ActionSetDto>
    {
        void CreateOrUpdateActionSet(string code, string description, RequestType triggeringRequestType, RequestStatus[] triggeringRequestStatuses, string definition);
    }
}
