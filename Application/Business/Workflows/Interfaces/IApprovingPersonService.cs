﻿
using System;
using System.Collections.Generic;

namespace Smt.Atomic.Business.Workflows.Interfaces
{
    public interface IApprovingPersonService
    {
        long? GetStandardHRApprover(long employeeId, long requesterEmployeeId);

        long? GetStandardHRApprover(long employeeId);

        long[] GetProjectApprovers(long employeeId, long projectId);

        long? GetEmployeeMainManager(long employeeId);

        long[] GetProjectApprovers(long employeeId, DateTime from, DateTime to);

        long[] GetStandardCostApprovers(IEnumerable<long> employeeIds, IEnumerable<long> projectIds);

        long[] GetBusinessTripSettlementApprovers(long businessTripParticipantId);

        long? GetAdvancePaymentRequestApprover(long participantId);

        long GetProjectSupervisorApprover(long employeeId, long projectId);
    }
}
