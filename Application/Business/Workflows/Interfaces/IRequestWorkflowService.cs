﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Workflows.Interfaces
{
    public interface IRequestWorkflowService
    {
        BusinessResult Submit(long requestId);
        BusinessResult Approve(long requestId);
        BusinessResult ApproveOnBehalfOf(long requestId, long onBehalfOfUserId);
        BusinessResult Reject(long requestId, string reason, bool cancellationRequest = false);
        BusinessResult RejectOnBehalfOf(long requestId, long onBehalfOfUserId, string reason, bool cancellationRequest = false);
        BoolResult Delete(long requestId);

        BoolResult ExecuteApproved(long requestId);
        long[] GetApprovedRequestIds();

        void AddApprovers(RequestDto requestId);

        ApprovalDto GetApproval(long requestId, long approvalId);

        RequestStatus[] CannotBeForcedStatuses { get; }

        bool IsWaitingForDirectUserDecision(long requestId, long userId);

        bool IsWaitingForSubstituteUserDecision(long requestId, long userId);
    }
}
