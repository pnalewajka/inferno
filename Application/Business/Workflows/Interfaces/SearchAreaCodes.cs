﻿namespace Smt.Atomic.Business.Workflows.Interfaces
{
    public class SearchAreaCodes
    {
        public const string RequestingUser = "requesting-user";
        public const string ApprovingUser = "approving-user";
        public const string Watchers = "watchers";
        public const string Description = "description";
    }
}
