﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Workflows.Dto;

namespace Smt.Atomic.Business.Workflows.Interfaces
{
    public interface IAction
    {
        Type DefinitionType { get; }

        RequestHistoryEntryDto Execute(object definition, RequestDto request);

        IEnumerable<ValidationResult> Validate(object definition, RequestDto request);

        string ToErrorMessage(object definition);
    }
}
