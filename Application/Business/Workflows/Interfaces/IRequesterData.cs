﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Workflows.Interfaces
{
    public interface IRequesterData
    {
        long RequestingUserId { get; set; }

        string RequesterFullName { get; set; }
    }
}
