﻿using System;
using System.Linq;
using Smt.Atomic.Business.Common.Abstracts;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Workflows.DocumentMappings
{
    [Identifier("Pictures.EmployeePhotoDocuments")]
    public class EmployeePhotoDocumentMapping : DocumentMapping<EmployeePhotoDocument, EmployeePhotoDocumentContent, IWorkflowsDbScope>
    {
        public EmployeePhotoDocumentMapping(IUnitOfWorkService<IWorkflowsDbScope> unitOfWorkService) : base(unitOfWorkService)
        {
            NameColumnExpression = o => o.Name;
            ContentColumnExpression = o => o.Content;
            ContentTypeColumnExpression = o => o.ContentType;
            ContentLengthColumnExpression = o => o.ContentLength;
        }

        protected override EmployeePhotoDocument GetMetaEntity(IRepository<EmployeePhotoDocument> metaEntityRepository, long id, Guid? secret)
        {
            if (secret.HasValue)
            {
                return metaEntityRepository.Single(p => p.Id == id && p.OnboardingRequest.WelcomeAnnounceEmailPhotosSecret == secret);
            }

            return base.GetMetaEntity(metaEntityRepository, id, secret);
        }
    }
}
