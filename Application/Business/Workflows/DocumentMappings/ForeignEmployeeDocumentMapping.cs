﻿using Smt.Atomic.Business.Common.Abstracts;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Workflows.DocumentMappings
{
    [Identifier("Documents.ForeignEmployeeDocuments")]
    public class ForeignEmployeeDocumentMapping : DocumentMapping<ForeignEmployeeDocument, ForeignEmployeeDocumentContent, IWorkflowsDbScope>
    {
        public ForeignEmployeeDocumentMapping(IUnitOfWorkService<IWorkflowsDbScope> unitOfWorkService) : base(unitOfWorkService)
        {
            NameColumnExpression = o => o.Name;
            ContentColumnExpression = o => o.Content;
            ContentTypeColumnExpression = o => o.ContentType;
            ContentLengthColumnExpression = o => o.ContentLength;
        }
    }
}
