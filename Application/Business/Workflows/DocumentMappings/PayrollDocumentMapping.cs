﻿using System;
using System.Linq;
using Smt.Atomic.Business.Common.Abstracts;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Workflows.DocumentMappings
{
    [Identifier("Documents.PayrollDocuments")]
    public class PayrollDocumentMapping : DocumentMapping<PayrollDocument, PayrollDocumentContent, IWorkflowsDbScope>
    {
        public PayrollDocumentMapping(IUnitOfWorkService<IWorkflowsDbScope> unitOfWorkService) : base(unitOfWorkService)
        {
            NameColumnExpression = o => o.Name;
            ContentColumnExpression = o => o.Content;
            ContentTypeColumnExpression = o => o.ContentType;
            ContentLengthColumnExpression = o => o.ContentLength;
        }

        protected override PayrollDocument GetMetaEntity(IRepository<PayrollDocument> metaEntityRepository, long id, Guid? secret)
        {
            if (secret.HasValue)
            {
                return metaEntityRepository.Single(d => d.Id == id && d.OnboardingRequest.PayrollDocumentsSecret == secret);
            }

            return base.GetMetaEntity(metaEntityRepository, id, secret);
        }
    }
}
