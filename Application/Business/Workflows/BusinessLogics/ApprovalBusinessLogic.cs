﻿using System;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Workflows.BusinessLogics
{
    public static class ApprovalBusinessLogic
    {
        public static readonly BusinessLogic<Approval, long, bool> IsOriginalApprover = new BusinessLogic<Approval, long, bool>(
            (approval, userId) => approval.UserId == userId);

        public static readonly BusinessLogic<Approval, long, bool> IsDirectApprover = new BusinessLogic
            <Approval, long, bool>(
            (approval, userId) => IsOriginalApprover.Call(approval, userId) || approval.ForcedByUserId == userId);

        public static readonly BusinessLogic<Approval, long, DateTime, bool> IsSubstitute = new BusinessLogic
            <Approval, long, DateTime, bool>(
            (approval, userId, date) => approval.User.Substitutions.Any(
                s => s.SubstituteUserId == userId
                    && s.From <= date
                    && s.To >= date));

        public static readonly BusinessLogic<Approval, long, DateTime, bool> IsApprover = new BusinessLogic
            <Approval, long, DateTime, bool>(
            (approval, userId, date) =>
                IsDirectApprover.Call(approval, userId) || IsSubstitute.Call(approval, userId, date));

        public static readonly BusinessLogic<Approval, bool> IsApproved = new BusinessLogic<Approval, bool>(
            (approval) => approval.Status == ApprovalStatus.Approved);

        public static readonly BusinessLogic<Approval, bool> IsPending = new BusinessLogic<Approval, bool>(
            (approval) => approval.Status == ApprovalStatus.Pending);
    }
}
