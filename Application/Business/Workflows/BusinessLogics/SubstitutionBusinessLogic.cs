﻿using System;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.BusinessLogics
{
    public static class SubstitutionBusinessLogic
    {
        public static readonly BusinessLogic<Substitution, long, DateTime, bool> IsSubstitute = new BusinessLogic
            <Substitution, long, DateTime, bool>(
            (substitution, userId, date) => substitution.SubstituteUserId == userId
                                            && substitution.From <= date
                                            && substitution.To >= date);

        public static readonly BusinessLogic<Substitution, long, long, DateTime, bool> IsSubstituting = new BusinessLogic
            <Substitution, long, long, DateTime, bool>((substitution, replacedUserId, substituteUserId, date) =>
                substitution.ReplacedUserId == replacedUserId
                && IsSubstitute.Call(substitution, substituteUserId, date)
            );
    }
}
