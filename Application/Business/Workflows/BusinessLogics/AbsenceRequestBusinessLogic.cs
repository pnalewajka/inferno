﻿using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.BusinessLogics
{
    public static class AbsenceRequestBusinessLogic
    {
        public static readonly BusinessLogic<Request, long, bool> BelongsToOneOfAbsenceTypes =
            new BusinessLogic<Request, long, bool>(
            (request, absenceTypeId) => request.AbsenceRequest.AbsenceTypeId == absenceTypeId);
    }
}
