﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.BusinessLogics
{
    public static class RequestBusinessLogic
    {
        public static readonly BusinessLogic<Request, DateTime, DateTime, bool> IsBetweenDates
            = new BusinessLogic<Request, DateTime, DateTime, bool>(
                (request, from, to) =>
                    request.ModifiedOn >= from && request.ModifiedOn <= to);

        public static readonly BusinessLogic<Request, long, bool> IsRequester
            = new BusinessLogic<Request, long, bool>(
                (request, userId) =>
                    request.RequestingUserId == userId);

        public static readonly BusinessLogic<Request, long, DateTime, bool> IsApprover
            = new BusinessLogic<Request, long, DateTime, bool>(
                (request, userId, date) =>
                    request.ApprovalGroups.Any(
                        g => g.Approvals.AsQueryable().Any(a => ApprovalBusinessLogic.IsApprover.Call(a, userId, date))));

        public static readonly BusinessLogic<Request, long, bool> IsWatcher
            = new BusinessLogic<Request, long, bool>(
                (request, userId) =>
                    request.Watchers.Any(w => w.Id == userId));

        public static readonly BusinessLogic<Request, long, bool> IsDirectApprover
            = new BusinessLogic<Request, long, bool>(
                (request, userId) =>
                    request.ApprovalGroups.Any(
                        g => g.Approvals.AsQueryable().Any(a => ApprovalBusinessLogic.IsDirectApprover.Call(a, userId))));

        public static readonly BusinessLogic<Request, long, bool> IsDirectApproverWithDecissionDone
            = new BusinessLogic<Request, long, bool>(
                (request, userId) =>
                    request.ApprovalGroups.AsQueryable().Any(
                        g => g.Approvals.AsQueryable().Any(
                            a => !ApprovalBusinessLogic.IsPending.Call(a)
                            && ApprovalBusinessLogic.IsDirectApprover.Call(a, userId))));

        public static readonly BusinessLogic<Request, ApprovalGroup> CurrentApprovalGroup
            = new BusinessLogic<Request, ApprovalGroup>(
                (request) => request.ApprovalGroups
                    .Where(g => !ApprovalGroupBusinessLogic.IsApproved.Call(g))
                    .OrderBy(g => g.Order).FirstOrDefault());

        public static readonly BusinessLogic<Request, long, bool> IsWaitingForDirectUserDecision
            = new BusinessLogic<Request, long, bool>(
                (request, userId) =>
                    request.Status == RequestStatus.Pending
                    && new[] { CurrentApprovalGroup.Call(request) }.Any(g => g != null
                        && g.Approvals.AsQueryable().Any(
                            a => ApprovalBusinessLogic.IsPending.Call(a)
                                && ApprovalBusinessLogic.IsDirectApprover.Call(a, userId))));

        public static readonly BusinessLogic<Request, long, DateTime, bool> IsWaitingForSubstituteUserDecision
            = new BusinessLogic<Request, long, DateTime, bool>(
                (request, userId, date) =>
                    request.Status == RequestStatus.Pending
                    && new[] { CurrentApprovalGroup.Call(request) }.Any(g => g != null
                        && g.Approvals.AsQueryable().Any(
                            a => ApprovalBusinessLogic.IsPending.Call(a)
                                && ApprovalBusinessLogic.IsSubstitute.Call(a, userId, date))));

        public static readonly BusinessLogic<Request, long, DateTime, bool> IsWaitingForUserDecision
            = new BusinessLogic<Request, long, DateTime, bool>(
                (request, userId, date) =>
                    request.Status == RequestStatus.Pending
                    && new[] { CurrentApprovalGroup.Call(request) }.Any(g => g != null
                        && g.Approvals.AsQueryable().Any(
                            a => ApprovalBusinessLogic.IsPending.Call(a)
                                && ApprovalBusinessLogic.IsApprover.Call(a, userId, date))));

        public static readonly BusinessLogic<Request, bool> IsFullyApproved
            = new BusinessLogic<Request, bool>(
                (request) => request.ApprovalGroups.AsQueryable().All(ApprovalGroupBusinessLogic.IsApproved));

        public static readonly BusinessLogic<Request, bool> HasDocuments
            = new BusinessLogic<Request, bool>(
                request => request.Documents.Any());

        public static readonly BusinessLogic<Request, bool> IsProcessed
            = new BusinessLogic<Request, bool>(
                (request) => request.Status != RequestStatus.Draft && request.Status != RequestStatus.Pending);

        public static readonly BusinessLogic<Request, bool> IsApprovedOrCompleted
            = new BusinessLogic<Request, bool>(
                (request) => request.Status == RequestStatus.Approved || request.Status == RequestStatus.Completed);

        public static readonly BusinessLogic<AbsenceRequest, IQueryable<User>> GetAbsenceRequestAffectedUser
            = new BusinessLogic<AbsenceRequest, IQueryable<User>>(
                DynamicQueryHelper.CreateArrayOrEmpty<AbsenceRequest, User>(r => r != null, r => r.AffectedUser));

        public static readonly BusinessLogic<EmployeeDataChangeRequest, IQueryable<User>> GetEmployeeDataChangeRequestAffectedUser
            = new BusinessLogic<EmployeeDataChangeRequest, IQueryable<User>>(
                DynamicQueryHelper.SelectArrayOrEmpty<EmployeeDataChangeRequest, User>(
                    r => r != null, r => r.Employees.AsQueryable().Select(e => e.User)));

        public static readonly BusinessLogic<SkillChangeRequest, IQueryable<User>> GetSkillChangeRequestAffectedUser
           = new BusinessLogic<SkillChangeRequest, IQueryable<User>>(
               DynamicQueryHelper.CreateArrayOrEmpty<SkillChangeRequest, User>(r => r != null, r => r.Employee.User));

        public static readonly BusinessLogic<AddHomeOfficeRequest, IQueryable<User>> GetAddHomeOfficeRequestAffectedUser
           = new BusinessLogic<AddHomeOfficeRequest, IQueryable<User>>(
               DynamicQueryHelper.CreateArrayOrEmpty<AddHomeOfficeRequest, User>(r => r != null, r => r.Request.RequestingUser));

        public static readonly BusinessLogic<OvertimeRequest, IQueryable<User>> GetOvertimeRequestAffectedUser
          = new BusinessLogic<OvertimeRequest, IQueryable<User>>(
              DynamicQueryHelper.CreateArrayOrEmpty<OvertimeRequest, User>(r => r != null, r => r.Request.RequestingUser));

        public static readonly BusinessLogic<ProjectTimeReportApprovalRequest, IQueryable<User>> GetProjectTimeReportApprovalRequestAffectedUser
            = new BusinessLogic<ProjectTimeReportApprovalRequest, IQueryable<User>>(
                DynamicQueryHelper.CreateArrayOrEmpty<ProjectTimeReportApprovalRequest, User>(r => r != null, r => r.Employee.User));

        public static readonly BusinessLogic<EmploymentTerminationRequest, IQueryable<User>> GetEmploymentTerminationRequestAffectedUser
            = new BusinessLogic<EmploymentTerminationRequest, IQueryable<User>>(
                DynamicQueryHelper.CreateArrayOrEmpty<EmploymentTerminationRequest, User>(r => r != null, r => r.Employee.User));

        public static readonly BusinessLogic<BusinessTripRequest, IQueryable<User>> GetBusinessTripRequestAffectedUser
            = new BusinessLogic<BusinessTripRequest, IQueryable<User>>(
                DynamicQueryHelper.SelectArrayOrEmpty<BusinessTripRequest, User>(
                        r => r != null, r => r.Participants.AsQueryable().Select(e => e.User)));

        public static readonly BusinessLogic<EmploymentDataChangeRequest, IQueryable<User>> GetEmploymentDataChangeRequestAffectedUser
            = new BusinessLogic<EmploymentDataChangeRequest, IQueryable<User>>(
                DynamicQueryHelper.CreateArrayOrEmpty<EmploymentDataChangeRequest, User>(r => r != null, r => r.Employee.User));

        public static readonly BusinessLogic<ReopenTimeTrackingReportRequest, IQueryable<User>> GetReopenTimeTrackingReportRequestAffectedUser
            = new BusinessLogic<ReopenTimeTrackingReportRequest, IQueryable<User>>(
                DynamicQueryHelper.CreateArrayOrEmpty<ReopenTimeTrackingReportRequest, User>(r => r != null, r => r.Request.RequestingUser));

        public static readonly BusinessLogic<AdvancedPaymentRequest, IQueryable<User>> GetAdvancedPaymentRequestAffectedUser
            = new BusinessLogic<AdvancedPaymentRequest, IQueryable<User>>(
                DynamicQueryHelper.CreateArrayOrEmpty<AdvancedPaymentRequest, User>(r => r != null, r => r.Participant.Employee.User));

        public static readonly BusinessLogic<OnboardingRequest, IQueryable<User>> GetOnboardingRequestAffectedUser
            = new BusinessLogic<OnboardingRequest, IQueryable<User>>(
                DynamicQueryHelper.CreateArrayOrEmpty<OnboardingRequest, User>(r => r != null, r => r.ResultingUser));

        public static readonly BusinessLogic<AssetsRequest, IQueryable<User>> GetAssetsRequestAffectedUser
            = new BusinessLogic<AssetsRequest, IQueryable<User>>(
                DynamicQueryHelper.CreateArrayOrEmpty<AssetsRequest, User>(r => r != null, r => r.Employee.User));

        public static readonly BusinessLogic<FeedbackRequest, IQueryable<User>> GetFeedbackRequestAffectedUser
            = new BusinessLogic<FeedbackRequest, IQueryable<User>>(
                DynamicQueryHelper.CreateArrayOrEmpty<FeedbackRequest, User>(r => r != null, r => r.EvaluatedEmployee.User));

        public static readonly BusinessLogic<BusinessTripSettlementRequest, IQueryable<User>> GetBusinessTripSettlementRequestAffectedUser
            = new BusinessLogic<BusinessTripSettlementRequest, IQueryable<User>>(
                DynamicQueryHelper.CreateArrayOrEmpty<BusinessTripSettlementRequest, User>(
                    r => r != null && r.BusinessTripParticipant != null, r => r.BusinessTripParticipant.Employee.User));

        public static readonly BusinessLogic<TaxDeductibleCostRequest, IQueryable<User>> GetTaxDeductibleCostRequestAffectedUser
            = new BusinessLogic<TaxDeductibleCostRequest, IQueryable<User>>(
                DynamicQueryHelper.CreateArrayOrEmpty<TaxDeductibleCostRequest, User>(r => r != null, r => r.AffectedUser));

        public static readonly BusinessLogic<BonusRequest, IQueryable<User>> GetBonusRequestAffectedUser
            = new BusinessLogic<BonusRequest, IQueryable<User>>(
                DynamicQueryHelper.CreateArrayOrEmpty<BonusRequest, User>(r => r != null, r => r.Employee.User));

        public static readonly BusinessLogic<ExpenseRequest, IQueryable<User>> GetExpenseRequestAffectedUser
            = new BusinessLogic<ExpenseRequest, IQueryable<User>>(
                DynamicQueryHelper.CreateArrayOrEmpty<ExpenseRequest, User>(r => r != null, r => r.Employee.User));

        public static readonly BusinessLogic<Request, IEnumerable<User>> GetAffectedUsers
            = new BusinessLogic<Request, IEnumerable<User>>(
                request => new User[] { }.AsQueryable()
                    .Concat(GetAbsenceRequestAffectedUser.Call(request.AbsenceRequest))
                    .Concat(GetEmployeeDataChangeRequestAffectedUser.Call(request.EmployeeDataChangeRequest))
                    .Concat(GetEmploymentDataChangeRequestAffectedUser.Call(request.EmploymentDataChangeRequest))
                    .Concat(GetEmploymentTerminationRequestAffectedUser.Call(request.EmploymentTerminationRequest))
                    .Concat(GetSkillChangeRequestAffectedUser.Call(request.SkillChangeRequest))
                    .Concat(GetAddHomeOfficeRequestAffectedUser.Call(request.AddHomeOfficeRequest))
                    .Concat(GetOvertimeRequestAffectedUser.Call(request.OvertimeRequest))
                    .Concat(GetBusinessTripRequestAffectedUser.Call(request.BusinessTripRequest))
                    .Concat(GetBusinessTripSettlementRequestAffectedUser.Call(request.BusinessTripSettlementRequest))
                    .Concat(GetFeedbackRequestAffectedUser.Call(request.FeedbackRequest))
                    .Concat(GetAssetsRequestAffectedUser.Call(request.AssetsRequest))
                    .Concat(GetOnboardingRequestAffectedUser.Call(request.OnboardingRequest))
                    .Concat(GetAdvancedPaymentRequestAffectedUser.Call(request.AdvancedPaymentRequest))
                    .Concat(GetReopenTimeTrackingReportRequestAffectedUser.Call(request.ReopenTimeTrackingReportRequest))
                    .Concat(GetProjectTimeReportApprovalRequestAffectedUser.Call(request.ProjectTimeReportApprovalRequest))
                    .Concat(GetTaxDeductibleCostRequestAffectedUser.Call(request.TaxDeductibleCostRequest))
                    .Concat(GetBonusRequestAffectedUser.Call(request.BonusRequest))
                    .Concat(GetExpenseRequestAffectedUser.Call(request.ExpenseRequest))
                .Where(i => i != null)
           );

        public static readonly BusinessLogic<Request, long, bool> BelongsToAnyOfAffectedUsers
            = new BusinessLogic<Request, long, bool>(
                (request, affectedUserId) => GetAffectedUsers.Call(request).Any(u => u.Id == affectedUserId));

        public static readonly BusinessLogic<Request, ApprovalGroup> GetLastApprovalGroup
            = new BusinessLogic<Request, ApprovalGroup>(
                (request) => request.ApprovalGroups.OrderByDescending(ag => ag.Order).FirstOrDefault());

        public static readonly BusinessLogic<Request, Approval> GetLastApprover
            = new BusinessLogic<Request, Approval>(
                (request) => ApprovalGroupBusinessLogic.GetLastApproval.Call(GetLastApprovalGroup.Call(request)));
    }
}