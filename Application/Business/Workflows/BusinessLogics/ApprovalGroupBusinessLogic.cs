﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Workflows.BusinessLogics
{
    public static class ApprovalGroupBusinessLogic
    {
        public static readonly BusinessLogic<ApprovalGroup, bool> IsApproved
            = new BusinessLogic<ApprovalGroup, bool>(
                g => g.Mode == ApprovalGroupMode.And
                    ? g.Approvals.AsQueryable().All(ApprovalBusinessLogic.IsApproved)
                    : g.Approvals.AsQueryable().Any(ApprovalBusinessLogic.IsApproved));

        public static readonly BusinessLogic<ApprovalGroup, Approval> GetLastApproval
            = new BusinessLogic<ApprovalGroup, Approval>(
                g => g == null || g.Approvals == null ? null : g.Approvals.OrderByDescending(a => a.ModifiedOn).FirstOrDefault());
    }
}
