using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Filters
{
    public static class AllEmployeesFilter
    {
        public static NamedFilter<AllocationRequest> AllocationRequestFilter
            => GetFilter<AllocationRequest>();

        public static NamedFilter<Employee> EmployeeFilter
            => GetFilter<Employee>();

        private static NamedFilter<T> GetFilter<T>() where T : class
        {
            return new NamedFilter<T>(FilterCodes.AllEmployees, a => true);
        }
    }
}
