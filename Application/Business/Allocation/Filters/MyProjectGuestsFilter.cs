using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Filters
{
    public static class MyProjectGuestsFilter
    {
        public static NamedFilter<Employee> GetEmployeeFilter(
            IPrincipalProvider principalProvider,
            IOrgUnitService orgUnitService,
            IReadOnlyUnitOfWorkService<IAllocationDbScope> unitOfWorkAllocationService)
        {
            var employeeId = principalProvider.Current.EmployeeId;
            var managerOrgUnitsIds = orgUnitService.GetManagerOrgUnitDescendantIds(employeeId);
            var projectManagerIds = GetProjectManagerIdsFromOrgUnits(managerOrgUnitsIds, unitOfWorkAllocationService);

            return new NamedFilter<Employee, DateTime, DateTime>(
                FilterCodes.MyProjectGuestsFilter,
                (e, from, to) => (e.OrgUnit == null || managerOrgUnitsIds.All(id => id != e.OrgUnitId))
                                 && (e.AllocationRequests.Any(ar => projectManagerIds.Any(pm =>
                                 pm == ar.Project.ProjectSetup.ProjectManagerId &&
                                 ar.StartDate <= to &&
                                 (!ar.EndDate.HasValue || ar.EndDate >= from)))),
                SecurityRoleType.CanFilterByMyProjectGuests);
        }

        private static IList<long> GetProjectManagerIdsFromOrgUnits(IList<long> orgUnitsIds, IReadOnlyUnitOfWorkService<IAllocationDbScope> unitOfWorkAllocationService)
        {
            using (var unitOfWork = unitOfWorkAllocationService.Create())
            {
                return unitOfWork.Repositories.Projects
                    .Where(p => p.ProjectSetup.ProjectManager != null && orgUnitsIds.Contains(p.ProjectSetup.ProjectManager.OrgUnitId))
                    .Select(p => p.ProjectSetup.ProjectManagerId.Value)
                    .ToList();
            }
        }
    }
}
