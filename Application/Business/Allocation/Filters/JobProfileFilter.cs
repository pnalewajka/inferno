using System.Linq;

using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Filters
{
    public static class JobProfileFilter
    {
        public static NamedFilter<Employee> EmployeeFilter
        {
            get
            {
                return new NamedFilter<Employee, long[]>(
                    FilterCodes.JobProfileFilter,
                    (e, jobProfileIds) => (e.JobProfiles.Any(x => jobProfileIds.Contains(x.Id))));
            }
        }
    }
}
