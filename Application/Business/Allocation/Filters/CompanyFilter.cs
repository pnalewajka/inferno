﻿using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Filters
{
    public static class CompanyFilter
    {
        public static NamedFilter<AllocationRequest> AllocationRequestFilter
        {
            get
            {
                var filteringRule = new BusinessLogic<AllocationRequest, long[], bool>(
                    (allocationRequest, companyIds) => EmployeeBusinessLogic.BelongsToOneOfCompanies.Call(
                        allocationRequest.Employee,
                        companyIds));

                return new NamedFilter<AllocationRequest, long[]>(
                      FilterCodes.CompanyFilter,
                      filteringRule.BaseExpression);
            }
        }

        public static NamedFilter<Employee> EmployeeFilter
            => new NamedFilter<Employee, long[]>(
            FilterCodes.CompanyFilter,
            EmployeeBusinessLogic.BelongsToOneOfCompanies.BaseExpression);
    }
}
