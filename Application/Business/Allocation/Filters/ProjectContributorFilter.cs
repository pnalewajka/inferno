﻿using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Filters
{
    public static class ProjectContributorFilter
    {
        public static NamedFilter<Employee> EmployeeFilter
            => new NamedFilter<Employee>(FilterCodes.ProjectContributorFilter, e => e.IsProjectContributor);
    }
}
