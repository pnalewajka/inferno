﻿using System;
using System.Linq;

using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Filters
{
    public static class UtilizationFilter
    {
        public static NamedFilter<Employee> EmployeeFilter
        {
            get
            {
                return new NamedFilter<Employee, DateTime, long[]>(
                    FilterCodes.UtilizationFilter,
                    (e, week, allocationStatuses) => e.WeeklyAllocationStatuses.Any(
                        was => was.Week == week && allocationStatuses.Any(s => s == (long)was.AllocationStatus)));
            }
        }
    }
}
