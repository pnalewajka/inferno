﻿using System;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;

namespace Smt.Atomic.Business.Allocation.Filters
{
    public static class ContractTypeFilter
    {
        public static NamedFilter<Employee> EmployeeFilter
        {
            get
            {
                return new NamedFilter<Employee, long[], DateTime>(
                     FilterCodes.ContractTypeFilter,
                     (e, contractTypeIds, date) => (e.EmploymentPeriods.Any(
                         p => (!contractTypeIds.Any() || contractTypeIds.Contains(p.ContractTypeId))
                              && p.StartDate <= date && (!p.EndDate.HasValue || p.EndDate.Value >= date))));
            }
        }

        public static NamedFilter<TimeReport, long[]> GetTimeReportFilter(string filterCode)
        {
            return new NamedFilter<TimeReport, long[]>(
                filterCode,
                new BusinessLogic<TimeReport, long[], bool>(
                    (r, contractTypeIds) => r.Employee.EmploymentPeriods.AsQueryable()
                        .Where(p => !contractTypeIds.Any() || contractTypeIds.Contains(p.ContractTypeId))
                        .Any(p => EmploymentPeriodBusinessLogic.OverlapingRange.Call(p,
                            DbFunctions.CreateDateTime(r.Year, r.Month, 1, 0, 0, 0).Value,
                            DbFunctions.AddSeconds(DbFunctions.AddMonths(
                                    DbFunctions.CreateDateTime(r.Year, r.Month, 1, 0, 0, 0), 1), -1).Value))));
        }
    }
}
