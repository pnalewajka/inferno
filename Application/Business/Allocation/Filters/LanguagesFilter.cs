﻿using System.Linq;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Filters
{
    public static class LanguagesFilter
    {
        public static NamedFilter<Employee> EmployeeFilter
        {
            get
            {
                return new NamedFilter<Employee, long[]>(
                    FilterCodes.LanguagesFilter,
                    (e, languageIds) => e.Resumes.Any(
                        r => r.ResumeLanguages.Any(l => languageIds.Contains(l.LanguageId))));                    
            }
        }
    }
}
