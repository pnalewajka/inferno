﻿using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Filters
{
    using Smt.Atomic.CrossCutting.Common.BusinessLogic;

    public static class SkillsAndKnowledgeFilter
    {
        public static NamedFilter<Employee> EmployeeFilter
            => new NamedFilter<Employee, long[], long[], long[]>(
                FilterCodes.SkillsAndKnowledgeFilter,
                EmployeeBusinessLogic.HasSkillsAndKnowledge.BaseExpression);

        public static NamedFilter<AllocationRequest> AllocationRequestFilter
        {
            get
            {
                var filteringRule = new BusinessLogic<AllocationRequest, long[], long[], long[], bool>(
                    (allocationRequest, jobProfileIds, jobMatrixLevels, skillIds) => EmployeeBusinessLogic.HasSkillsAndKnowledge.Call(allocationRequest.Employee, jobProfileIds, jobMatrixLevels, skillIds));

                return new NamedFilter<AllocationRequest, long[], long[], long[]>(
                    FilterCodes.SkillsAndKnowledgeFilter,
                    filteringRule.BaseExpression);
            }
        }
    }
}
