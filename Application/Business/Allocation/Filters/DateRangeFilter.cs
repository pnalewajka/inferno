﻿using System;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Filters
{
    public static class DateRangeFilter
    {
        public static NamedFilter<AllocationRequest> AllocationRequestFilter 
            => new NamedFilter<AllocationRequest, DateTime, DateTime>(
            FilterCodes.DateRange,
            (a, from, to) => a.StartDate <= to && (!a.EndDate.HasValue || a.EndDate >= @from));

        public static NamedFilter<Employee> EmployeeFilter 
            => new NamedFilter<Employee>(FilterCodes.DateRange, e => true);
    }
}
