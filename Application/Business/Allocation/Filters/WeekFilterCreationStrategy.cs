﻿using System;
using System.Linq;

using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Filters
{
    public static class WeekFilter
    {
        public static NamedFilter<Employee> EmployeeFilter
        {
            get
            {
                return new NamedFilter<Employee, DateTime>(
                    FilterCodes.WeekFilter,
                    (e, week) => e.WeeklyAllocationStatuses.Any(
                        a => a.Week == week && (a.StaffingStatus == StaffingStatus.Free
                                                || a.StaffingStatus == StaffingStatus.Planned
                                                || a.StaffingStatus == StaffingStatus.PlannedAndConfirmed)));
            }
        }
    }
}
