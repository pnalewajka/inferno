﻿using System;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.Allocation.Filters
{
    class EmployeesProjectsFilterObject : DateRangeFilterObject
    {
        private readonly FilterParameters _filterParameters;

        public EmployeesProjectsFilterObject(DateTime @from, DateTime to, FilterParameters filterParameters) : base(@from, to)
        {
            _filterParameters = filterParameters;
        }

        public override FilterParameters GetFilterParameters()
        {
            var baseFilterParameters = base.GetFilterParameters();

            if (_filterParameters != null)
            {
                foreach (var filterParameter in _filterParameters)
                {
                    baseFilterParameters.Add(filterParameter.Key, filterParameter.Value);
                }
            }

            return baseFilterParameters;
        }
    }
}