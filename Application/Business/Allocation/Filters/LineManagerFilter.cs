﻿using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Filters
{
    using Smt.Atomic.CrossCutting.Common.BusinessLogic;

    public static class LineManagerFilter
    {
        public static NamedFilter<AllocationRequest> AllocationRequestFilter
        {
            get
            {
                var filteringRule = new BusinessLogic<AllocationRequest, long[], bool>(
                    (allocationRequest, lineManagerIds) => EmployeeBusinessLogic.HasGivenOneOfLineManagers.Call(allocationRequest.Employee, lineManagerIds));

                return new NamedFilter<AllocationRequest, long[]>(
                    FilterCodes.LineManagerFilter,
                    filteringRule.BaseExpression);
            }
        }

        public static NamedFilter<Employee> EmployeeFilter
            => new NamedFilter<Employee, long[]>(
            FilterCodes.LineManagerFilter,
            EmployeeBusinessLogic.HasGivenOneOfLineManagers.BaseExpression);
    }
}
