﻿using System.Linq;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Filters
{
    public static class OrgUnitsFilter
    {
        public static NamedFilter<Employee> EmployeeFilter
        {
            get
            {
                return new NamedFilter<Employee, long[]>(
                    FilterCodes.OrgUnitsFilter,
                    (e, orgUnitIds) => orgUnitIds.Any(id => e.OrgUnit.Path.Contains("/" + id + "/")));
            }
        }

        public static NamedFilter<AllocationRequest> AllocationRequestFilter
        {
            get
            {
                return new NamedFilter<AllocationRequest, long[]>(
                    FilterCodes.OrgUnitsFilter,
                    (a, orgUnitIds) => orgUnitIds.Any(id => a.Employee.OrgUnit.Path.Contains("/" + id + "/")));
            }
        }
    }
}
