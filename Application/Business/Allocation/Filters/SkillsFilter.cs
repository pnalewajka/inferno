﻿using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Filters
{
    using Smt.Atomic.CrossCutting.Common.BusinessLogic;

    public static class SkillsFilter
    {
        public static NamedFilter<Employee> EmployeeFilter
            => new NamedFilter<Employee, long[], long, long[], long, long[], long, long[], long, long[], long>(
                FilterCodes.SkillsFilter,
                EmployeeBusinessLogic.HasSkills.BaseExpression);

        public static NamedFilter<AllocationRequest> AllocationRequestFilter
        {
            get
            {
                var filteringRule = new BusinessLogic<AllocationRequest, long[], long, long[], long, long[], long, long[], long, long[], long, bool>(
                    (allocationRequest, skill1Name, skill1MinimumLevel, skill2Name, skill2MinimumLevel, skill3Name, skill3MinimumLevel, skill4Name, skill4MinimumLevel, skill5Name, skill5MinimumLevel)
                    => EmployeeBusinessLogic.HasSkills.Call(allocationRequest.Employee, skill1Name, skill1MinimumLevel, skill2Name, skill2MinimumLevel, skill3Name, skill3MinimumLevel, skill4Name, skill4MinimumLevel, skill5Name, skill5MinimumLevel));

                return new NamedFilter<AllocationRequest, long[], long, long[], long, long[], long, long[], long, long[], long>(
                    FilterCodes.SkillsFilter,
                    filteringRule.BaseExpression);
            }
        }
    }
}
