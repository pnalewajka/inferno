﻿using System;
using System.Linq;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Filters
{
    public static class MyProjectsFilter
    {
        public static NamedFilter<Employee> GetEmployeeFilter(
            IPrincipalProvider principalProvider,
            IOrgUnitService orgUnitService)
        {
            var employeeId = principalProvider.Current.EmployeeId;
            var managerOrgUnitsIds = orgUnitService.GetManagerOrgUnitDescendantIds(employeeId);

            return new NamedFilter<Employee, DateTime, DateTime>(
                FilterCodes.MyProjects,
                (e, from, to) => (e.OrgUnit != null && managerOrgUnitsIds.Any(id => id == e.OrgUnitId))
                                 || (e.AllocationRequests.Any(
                                            ar => ar.Project.ProjectSetup.ProjectManagerId == employeeId && ar.StartDate <= to
                                                  && (!ar.EndDate.HasValue || ar.EndDate >= from))));
        }
    }
}
