﻿using System.Linq;

using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Filters
{
    public static class IndustriesFilter
    {
        public static NamedFilter<Employee> EmployeeFilter
        {
            get
            {
                return new NamedFilter<Employee, long[]>(
                    FilterCodes.Industries,
                    (e, industryIds) => e.Resumes.Any(
                        c => c.ResumeCompanies.Any(p => p.ResumeProjects.Any(pi => pi.Industries.Any(i => industryIds.Contains(i.Id))))));
            }
        }
    }
}
