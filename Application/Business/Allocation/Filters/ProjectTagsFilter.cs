﻿using System.Linq;

using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Filters
{
    public static class ProjectTagsFilter
    {
        public static NamedFilter<Employee> EmployeeFilter
        {
            get
            {
                return new NamedFilter<Employee, long[]>(
                    FilterCodes.ProjectTags,
                    (e, projectTagIds) => !projectTagIds.Any()
                    || e.AllocationRequests.Any(allocationRequest => allocationRequest.Project.ProjectSetup.Tags.Any(t => projectTagIds.Contains(t.Id))));
            }
        }

        public static NamedFilter<AllocationRequest> AllocationRequestFilter
        {
            get
            {
                return new NamedFilter<AllocationRequest, long[]>(
                    FilterCodes.ProjectTags,
                    (allocationRequest, projectTagIds) => !projectTagIds.Any()
                    || allocationRequest.Project.ProjectSetup.Tags.Any(t => projectTagIds.Contains(t.Id)));
            }
        }
    }
}
