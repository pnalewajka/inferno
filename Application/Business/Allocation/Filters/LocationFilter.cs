using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Filters
{
    using Smt.Atomic.CrossCutting.Common.BusinessLogic;

    public static class LocationFilter
    {
        public static NamedFilter<AllocationRequest> AllocationRequestFilter
        {
            get
            {
                var filteringRule = new BusinessLogic<AllocationRequest, long[], bool>(
                    (allocationRequest, locationIds) => EmployeeBusinessLogic.HasGivenOneOfLocations.Call(allocationRequest.Employee, locationIds));


                return new NamedFilter<AllocationRequest, long[]>(
                    FilterCodes.LocationFilter,
                    filteringRule.BaseExpression);
            }
        }

        public static NamedFilter<Employee> EmployeeFilter
            => new NamedFilter<Employee, long[]>(
            FilterCodes.LocationFilter,
            EmployeeBusinessLogic.HasGivenOneOfLocations.BaseExpression);
    }
}
