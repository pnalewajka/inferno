﻿using System;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;

namespace Smt.Atomic.Business.Allocation.Filters
{
    public class DateRangeFilterObject : IFilteringObject
    {
        private readonly DateTime _from;
        private readonly DateTime _to;

        public DateRangeFilterObject(DateTime from, DateTime to)
        {
            _from = from;
            _to = to;
        }

        public virtual FilterParameters GetFilterParameters()
        {
            return new FilterParameters
            {
                {"from", new FilterParameter(typeof(DateTime), _from, "from", "from")},
                {"to", new FilterParameter(typeof(DateTime), _to, "to", "to")}
            };
        }

        public object GetFilterDto()
        {
            throw new NotImplementedException();
        }
    }
}