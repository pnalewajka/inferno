﻿using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Filters
{ 
    public static class AllAllocationsFilter
    {
        public static NamedFilter<Employee> EmployeeFilter 
            => new NamedFilter<Employee>(FilterCodes.AllAllocations, a => true);
    }
}
