using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Filters
{
    public static class MyEmployeesFilter
    {
        public static NamedFilter<AllocationRequest> GetAllocationRequestFilter(
            IOrgUnitService orgUnitService,
            IPrincipalProvider principalProvider)
        {
            var managerOrgUnitsIds = orgUnitService.GetManagerOrgUnitDescendantIds(principalProvider.Current.EmployeeId);

            var filteringRule = new BusinessLogic<AllocationRequest, long, IEnumerable<long>, bool>(
                (allocationRequest, managerId, managerOrgUnitIds) => EmployeeBusinessLogic.IsEmployeeOf.Call(allocationRequest.Employee, managerId, managerOrgUnitsIds));

            return new NamedFilter<AllocationRequest>(
                FilterCodes.MyEmployees,
                filteringRule.Parametrize(principalProvider.Current.EmployeeId, managerOrgUnitsIds));
        }

        public static NamedFilter<T> GetDescendantEmployeeFilter<T>(
            string filterCode,
            Expression<Func<T, Employee>> employeeSelector,
            IOrgUnitService orgUnitService,
            IPrincipalProvider principalProvider,
            SecurityRoleType? requiredRole = null)
            where T : class
        {
            var filter = GetDescendantEmployeeFilter(orgUnitService, principalProvider);
            var filterLogic = new BusinessLogic<Employee, bool>(filter.Expression);
            var employeeSelectorLogic = new BusinessLogic<T, Employee>(employeeSelector);

            return new NamedFilter<T>(
                filterCode,
                t => filterLogic.Call(employeeSelectorLogic.Call(t)),
                requiredRole);
        }

        public static NamedFilter<Employee> GetDescendantEmployeeFilter(
            IOrgUnitService orgUnitService,
            IPrincipalProvider principalProvider,
            SecurityRoleType? requiredRole = null)
        {
            var currentEmployeeId = principalProvider.Current.EmployeeId;
            var managerOrgUnitsIds = orgUnitService.GetManagerOrgUnitDescendantIds(principalProvider.Current.EmployeeId);

            return new NamedFilter<Employee>(
                FilterCodes.MyEmployees,
                new BusinessLogic<Employee, bool>(e =>
                    e.Id == currentEmployeeId || EmployeeBusinessLogic.IsEmployeeOf.Call(e, currentEmployeeId, managerOrgUnitsIds)),
                requiredRole);
        }

        public static NamedFilter<T> GetChildEmployeeFilter<T>(
            string filterCode,
            Expression<Func<T, Employee>> employeeSelector,
            IOrgUnitService orgUnitService,
            IPrincipalProvider principalProvider,
            SecurityRoleType? requiredRole = null)
            where T : class
        {
            var filter = GetChildEmployeeFilter(orgUnitService, principalProvider);
            var filterLogic = new BusinessLogic<Employee, bool>(filter.Expression);
            var employeeSelectorLogic = new BusinessLogic<T, Employee>(employeeSelector);

            return new NamedFilter<T>(
                filterCode,
                t => filterLogic.Call(employeeSelectorLogic.Call(t)),
                requiredRole);
        }

        public static NamedFilter<Employee> GetChildEmployeeFilter(
            IOrgUnitService orgUnitService,
            IPrincipalProvider principalProvider,
            SecurityRoleType? requiredRole = null)
        {
            var currentEmployeeId = principalProvider.Current.EmployeeId;
            var managerOrgUnitsIds = orgUnitService.GetDirectlyManagedOrgUnitIds(currentEmployeeId);
            var managerIdsOfManagerOrgUnitChildren = orgUnitService.GetManagerIdsOfManagerOrgUnitChildren(currentEmployeeId);

            return new NamedFilter<Employee>(
                FilterCodes.MyDirectEmployees,
                new BusinessLogic<Employee, bool>(e =>
                    e.Id == currentEmployeeId
                    || EmployeeBusinessLogic.IsEmployeeOf.Call(e, currentEmployeeId, managerOrgUnitsIds)
                    || EmployeeBusinessLogic.IsOneOfManagers.Call(e, managerIdsOfManagerOrgUnitChildren)),
                requiredRole);
        }
    }
}
