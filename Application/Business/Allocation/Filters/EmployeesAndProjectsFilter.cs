﻿using System;
using System.Linq;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Filters
{
    public static class EmployeesAndProjectsFilter
    {
        public static NamedFilter<AllocationRequest> AllocationRequestFilter
        {
            get
            {
                return new NamedFilter<AllocationRequest, long[], long[]>(
                      FilterCodes.EmployeesAndProjects,
                      (x, projectsIds, employeesIds) => (!employeesIds.Any() || employeesIds.Contains(x.EmployeeId))
                                                        && (!projectsIds.Any() || projectsIds.Contains(x.ProjectId)));
            }
        }

        public static NamedFilter<Employee> EmployeeFilter
        {
            get
            {
                return new NamedFilter<Employee, DateTime, DateTime, long[], long[]>(
                      FilterCodes.EmployeesAndProjects,
                      (x, from, to, projectsIds, employeesIds) =>
                          (!employeesIds.Any() || employeesIds.Contains(x.Id)) && (!projectsIds.Any()
                              || x.AllocationRequests.Any(a => projectsIds.Contains(a.ProjectId)
                              && a.StartDate <= to && (!a.EndDate.HasValue || a.EndDate >= @from))));
            }
        }
    }
}
