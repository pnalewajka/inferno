﻿using System;
using System.Linq;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Helpers;

namespace Smt.Atomic.Business.Allocation.Filters
{
    public static class MyTeamAllocationsFilter
    {
        public static NamedFilter<Employee> GetEmployeeFilter(IPrincipalProvider principalProvider)
        {
            var employeeId = principalProvider.Current.EmployeeId;

            return new NamedFilter<Employee, DateTime, DateTime>(
                FilterCodes.MyTeamAllocations,
                (e, from, to) =>
                    (e.Id == employeeId && e.AllocationRequests.Filtered().Any(ar => ar.StartDate <= to && (!ar.EndDate.HasValue || ar.EndDate >= from))) ||
                    (e.AllocationRequests.Filtered().Any(ar => ar.Project.AllocationRequests.Filtered().Any(
                            par => par.EmployeeId == employeeId
                                && par.StartDate <= to
                                && (!par.EndDate.HasValue || par.EndDate >= from))
                                && ar.StartDate <= to && (!ar.EndDate.HasValue || ar.EndDate >= from)
                    )), SecurityRoleType.CanViewMyTeamAllocations);
        }
    }
}
