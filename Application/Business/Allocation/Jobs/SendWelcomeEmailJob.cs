﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Castle.Core.Logging;
using Smt.Atomic.Business.Allocation.Dto.WelcomeEmail;
using Smt.Atomic.Business.Allocation.EmailDataSources;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Allocation.Resources;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Runtime.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Jobs
{
    [Identifier("Job.Allocation.SendWelcomeEmails")]
    [JobDefinition(
        Code = "SendWelcomeEmailJob",
        Description = "Sends welcome emails to new employees",
        ScheduleSettings = "0 0 7 * * *",
        MaxExecutioPeriodInSeconds = 5400,
        PipelineName = PipelineCodes.Notifications)]
    public sealed class SendWelcomeEmailJob : IJob
    {
        private readonly IUnitOfWorkService<IAllocationDbScope> _unitOfWorkService;
        private readonly IEmployeeService _employeeService;
        private readonly ILogger _logger;
        private readonly IReliableEmailService _emailService;
        private readonly ITimeService _timeService;
        private readonly IMessageTemplateService _templateService;
        private readonly ISystemParameterService _parameterService;
        private readonly IJobRunInfoService _jobRunInfoService;

        public SendWelcomeEmailJob(
            IUnitOfWorkService<IAllocationDbScope> unitOfWorkService,
            IEmployeeService employeeService,
            ILogger logger,
            IReliableEmailService emailService,
            ITimeService timeService, IMessageTemplateService templateService,
            ISystemParameterService parameterService,
            IJobRunInfoService jobRunInfoService)
        {
            _unitOfWorkService = unitOfWorkService;
            _employeeService = employeeService;
            _logger = logger;
            _emailService = emailService;
            _timeService = timeService;
            _templateService = templateService;
            _parameterService = parameterService;
            _jobRunInfoService = jobRunInfoService;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            if (WasJobExecutedToday(executionContext.JobDefinitionId))
            {
                return JobResult.Success();
            }

            var currentDate = _timeService.GetCurrentDate();
            var followUpEmailDelayDays = _parameterService.GetParameter<int>(ParameterKeys.FollowUpEmailDelayDays);
            var firstDayInternal = GetNewEmployees(new[] { PlaceOfWork.CompanyOffice }, currentDate);
            var followUpInternal = GetNewEmployees(new[] { PlaceOfWork.CompanyOffice }, currentDate.AddDays(-followUpEmailDelayDays));
            var firstDayTeamAugmentation = GetNewEmployees(new[] { PlaceOfWork.ClientOffice, PlaceOfWork.HomeOffice }, currentDate);
            var followUpTeamAugmentation = GetNewEmployees(new[] { PlaceOfWork.ClientOffice, PlaceOfWork.HomeOffice }, currentDate.AddDays(-followUpEmailDelayDays));
            var emailModels = WelcomeEmailModels.GetEmailModels();
            var mailings = new List<WelcomeEmailParametersDto>
            {
                new WelcomeEmailParametersDto
                {
                    EmployeeIds = firstDayInternal,
                    BodyTemplateCode = TemplateCodes.WelcomeEmailFirstDayBody,
                    SubjectTemplateCode = TemplateCodes.WelcomeEmailFirstDaySubject,
                    MessageModel = emailModels.FirstOrDefault(x => x.EmailType == WelcomeEmailResources.FirstDayInternalModel)
                },
                new WelcomeEmailParametersDto
                {
                    EmployeeIds = firstDayTeamAugmentation,
                    BodyTemplateCode = TemplateCodes.WelcomeEmailFirstDayBody,
                    SubjectTemplateCode = TemplateCodes.WelcomeEmailFirstDaySubject,
                    MessageModel = emailModels.FirstOrDefault(x => x.EmailType == WelcomeEmailResources.FirstDayTeamAugmentationModel)
                },
                new WelcomeEmailParametersDto
                {
                    EmployeeIds = followUpInternal,
                    BodyTemplateCode = TemplateCodes.FollowUpWelcomeEmailBody,
                    SubjectTemplateCode = TemplateCodes.FollowUpWelcomeEmailSubject,
                    MessageModel = emailModels.FirstOrDefault(x => x.EmailType == WelcomeEmailResources.FollowUpInternalModel)
                },
                new WelcomeEmailParametersDto
                {
                    EmployeeIds = followUpTeamAugmentation,
                    BodyTemplateCode = TemplateCodes.FollowUpWelcomeEmailBody,
                    SubjectTemplateCode = TemplateCodes.FollowUpWelcomeEmailSubject,
                    MessageModel = emailModels.FirstOrDefault(x => x.EmailType == WelcomeEmailResources.FollowUpTeamAugmentationModel)
                }
            };

            var anySuccessful = false;

            foreach (var mailing in mailings)
            {
                anySuccessful |= SendEmails(mailing, executionContext);
            }

            return anySuccessful ? JobResult.Success() : JobResult.Fail();
        }

        private bool SendEmails(WelcomeEmailParametersDto parametersDto, JobExecutionContext executionContext)
        {
            var success = true;

            foreach (var employeeId in parametersDto.EmployeeIds
                .TakeWhileNotCancelled(executionContext.CancellationToken)
                .TakeIfEnoughTime(executionContext.MaxExecutionTime))
            {
                try
                {
                    SendWelcomeEmail(employeeId, parametersDto.BodyTemplateCode, parametersDto.SubjectTemplateCode, parametersDto.MessageModel);
                }
                catch (Exception ex)
                {
                    success = false;
                    _logger.ErrorFormat(ex, $"Sending welcome email for employee ({employeeId}) failed");
                }
            }

            return success;
        }

        private void SendWelcomeEmail(long employeeId, string bodyTemplateCode, string subjectTemplateCode, WelcomeEmailDto messageModel)
        {
            var employee = _employeeService.GetEmployeeOrDefaultById(employeeId);

            messageModel.FirstName = employee.FirstName;

            var body = _templateService.ResolveTemplate(bodyTemplateCode, messageModel);
            var subject = _templateService.ResolveTemplate(subjectTemplateCode, null);
            var messageDto = new EmailMessageDto
            {
                Body = body.Content,
                Subject = subject.Content,
                IsHtml = true
            };

            messageDto.Recipients.Add(new EmailRecipientDto(employee.Email, employee.FullName));

            _emailService.EnqueueMessage(messageDto);
        }

        private IEnumerable<long> GetNewEmployees(IList<PlaceOfWork> placeOfWork, DateTime employmentPeriodStartDate)
        {
            var result = new HashSet<long>();
            var orgUnitCodes = _parameterService.GetParameter<string[]>(ParameterKeys.OrganizationUnitsForSendWelcomeEmail);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                foreach (var orgUnitCode in orgUnitCodes)
                {
                    var orgUnitPath = unitOfWork.Repositories.OrgUnits.Where(o => o.Code == orgUnitCode).Select(o => o.Path).FirstOrDefault();

                    if (orgUnitPath != null)
                    {
                        var employeeIds = unitOfWork.Repositories.Employees
                            .Where(e => e.PlaceOfWork != null
                                && placeOfWork.Contains(e.PlaceOfWork.Value)
                                && e.OrgUnit.Path.StartsWith(orgUnitPath))
                            .Where(IsFirstDayOfWork(employmentPeriodStartDate))
                            .Select(x => x.Id)
                            .ToList();

                        result.UnionWith(employeeIds);
                    }
                }
            }

            return result;
        }

        private bool WasJobExecutedToday(long jobDefinitionId)
        {
            var runInfos = _jobRunInfoService.GetJobRunInfos(jobDefinitionId);

            return runInfos.Any(x => x.FinishedOn.GetValueOrDefault().Date == _timeService.GetCurrentDate());
        }

        private Expression<Func<Employee, bool>> IsFirstDayOfWork(DateTime date)
        {
            return e => e.EmploymentPeriods.Any(p => p.StartDate == date) && !e.EmploymentPeriods.Any(p => p.EndDate < date);
        }
    }
}
