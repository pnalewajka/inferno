﻿using Castle.Core.Logging;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.Business.Allocation.Jobs
{
    [Identifier("Job.Allocation.PrepareUnderUtilizedEmployeesEmailJob")]
    [JobDefinition(
        Code = "PrepareUnderUtilizedEmployeesEmailReportEmail",
        Description = "Prepare under utilized employees review emails",
        ScheduleSettings = "0 0 6 * * 4",
        MaxExecutioPeriodInSeconds = 120,
        PipelineName = PipelineCodes.Allocation)]
    public class PrepareUnderUtilizedEmployeesEmailJob : StatusReviewEmailBase, IJob
    {
        private const string ReportName = "Underutilized employees";

        public PrepareUnderUtilizedEmployeesEmailJob(IStatusReviewEmailService statusReviewEmailService, ILogger logger)
            : base(statusReviewEmailService, logger, ReportName)
        {
        }

        protected override void PrepareStatusEmails()
        {
        }
    }
}