﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using Castle.Core.Logging;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Business.Extensions;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Jobs
{
    /// <summary>
    /// This job is used to recalculate allocation Weekly/Daily data every month
    /// </summary>
    [Identifier("Job.Allocation.EmployeeAllocationRegenerateJob")]
    [JobDefinition(
        Code = "EmployeeAllocationRegenerateJob",
        Description = "Employee Allocation Regenerate Job",
        ScheduleSettings = "0 0 0 1 * *",
        MaxExecutioPeriodInSeconds = 500000,
        PipelineName = PipelineCodes.Allocation)]
    internal class EmployeeAllocationRegenerateJob : IJob
    {
        private readonly IDailyAllocationService _dailyAllocationService;
        private readonly IReadOnlyUnitOfWorkService<IAllocationDbScope> _allocationDbScope;
        private readonly ISystemParameterService _systemParameters;
        private readonly ILogger _logger;
        private readonly IClassMapping<AllocationRequest, AllocationRequestDto> _allocationRequestToAllocationRequestDtoClassMapping;
        private readonly IClassMapping<EmploymentPeriod, EmploymentPeriodDto> _employmentPeriodToEmploymentPeriodDtoClassMapping;

        private const string StartRegeneratingMessage = "Start regenerating EmployeeAllocation for Id > {0}";
        private const string ProcessingEmploymentPeriodsMessage = "{0} EmployeeAllocations will be regenerated";
        private const string FinishRegeneratingMessage = "Finish regenerating EmployeeAllocation";
        private const string FinishBySystemParamMessage = "Regenerating EmployeeAllocation stop, because system parameter (Job.RunEmployeeAllocationRegenerateJob) is false";
        private const string RegeneratingCrashedMessage = "Exception regenerating EmployeeAllocation";
        private const string CurrentRowMessage = "Currently we are rebuilding EmploymentPeriodId: {0}, for Employee: {1}";
        private const string RebuildDailyRecordsByPeriodMessage = "RebuildDailyRecordsByPeriod in EmployeeAllocation succeed";
        private const string RebuildWeeklyStatusMessage = "RebuildWeeklyStatus in EmployeeAllocation succeed";
        private const string RegeneratingTimeoutMessage = "Timeout exception while RebuildWeeklyStatus for Employee {0}. {1} attempt will be executed after delay";

        public EmployeeAllocationRegenerateJob(IDailyAllocationService dailyAllocationService,
            IReadOnlyUnitOfWorkService<IAllocationDbScope> allocationDbScope, ISystemParameterService systemParameters,
            ILogger logger,
            IClassMapping<AllocationRequest, AllocationRequestDto> allocationRequestToAllocationRequestDtoClassMapping,
            IClassMapping<EmploymentPeriod, EmploymentPeriodDto> employmentPeriodToEmploymentPeriodDtoClassMapping)
        {
            _dailyAllocationService = dailyAllocationService;
            _allocationDbScope = allocationDbScope;
            _systemParameters = systemParameters;
            _logger = logger;
            _allocationRequestToAllocationRequestDtoClassMapping = allocationRequestToAllocationRequestDtoClassMapping;
            _employmentPeriodToEmploymentPeriodDtoClassMapping = employmentPeriodToEmploymentPeriodDtoClassMapping;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            try
            {
                var runEmployeeAllocationRegenerateJob = _systemParameters.GetParameter<bool>(ParameterKeys.RunEmployeeAllocationRegenerateJob);
                var idStartPoint = _systemParameters.GetParameter<int>(ParameterKeys.StartPointEmployeeAllocationRegenerateJob);
                var delayInMilliseconds = _systemParameters.GetParameter<int>(ParameterKeys.DelayInMillisecondsEmployeeAllocationRegenerateJob);
                var maximumAttemptsCount = _systemParameters.GetParameter<int>(ParameterKeys.MaximumAttemptsCountEmployeeAllocationRegenerateJob);

                _logger.Info(string.Format(StartRegeneratingMessage, idStartPoint));

                if (!runEmployeeAllocationRegenerateJob)
                {
                    _logger.Info(FinishBySystemParamMessage);
                    return JobResult.Success();
                }
                
                using (var scope = _allocationDbScope.Create())
                {
                    List<AllocationRequestDto> allocationRequest = scope.Repositories.AllocationRequests.Include(a => a.Project).Include(a => a.Employee).ToList()
                        .Select(e => _allocationRequestToAllocationRequestDtoClassMapping.CreateFromSource(e)).ToList();

                    List<EmploymentPeriodDto> employmentPeriodList = scope.Repositories.EmploymentPeriods.ToList()
                        .Select(_employmentPeriodToEmploymentPeriodDtoClassMapping.CreateFromSource).ToList();

                    var employmentPeriods = employmentPeriodList.Where(e => e.Id > idStartPoint).ToList();
                    var employeeIds = employmentPeriods.Select(e => e.EmployeeId).ToList();

                    var employeesOrgUnits = scope.Repositories.Employees.Where(x => employeeIds.Contains(x.Id))
                        .ToDictionary(e => e.Id, e => e.OrgUnitId);

                    _logger.Info(string.Format(ProcessingEmploymentPeriodsMessage, employmentPeriods.Count));

                    foreach (var emp in employmentPeriods)
                    {
                        _logger.Info(string.Format(CurrentRowMessage, emp.EmployeeId, emp.Id));

                        var fromDate = SubstractFromNoticePeriod(emp.StartDate, emp.SignedOn, emp.NoticePeriod);
                        var employeeAllocationRequest = allocationRequest.Where(ar => ar.EmployeeId == emp.EmployeeId).ToList();
                        var employeeEmploymentPeriod = employmentPeriodList.Where(x => x.EmployeeId == emp.EmployeeId).ToList();

                        RebuildDailyRecordsByPeriod(employeeAllocationRequest, employeeEmploymentPeriod, fromDate);

                        _logger.Info(RebuildDailyRecordsByPeriodMessage);

                        for (var attempt = 1; attempt <= maximumAttemptsCount; attempt++)
                        {
                            try
                            {
                                _dailyAllocationService.RebuildWeeklyStatus(employeeEmploymentPeriod, emp.EmployeeId,
                                    fromDate, employeesOrgUnits[emp.EmployeeId]);

                                break;
                            }
                            catch (Exception exception) when (ExceptionHelper.IsTransientException(exception))
                            {
                                if (attempt >= maximumAttemptsCount)
                                {
                                    throw;
                                }

                                _logger.Info(string.Format(RegeneratingTimeoutMessage, emp.EmployeeId, attempt + 1));
                                Thread.Sleep(delayInMilliseconds);
                            }
                        }

                        _logger.Info(RebuildWeeklyStatusMessage);
                    }
                }

                _logger.Info(FinishRegeneratingMessage);
            }
            catch (Exception ex)
            {
                _logger.Error(RegeneratingCrashedMessage, ex);
                return JobResult.Fail();
            }

            return JobResult.Success();
        }

        private void RebuildDailyRecordsByPeriod(List<AllocationRequestDto> employeeAllocationRequest, List<EmploymentPeriodDto> employeeEmploymentPeriod, DateTime from)
        {
            var allocationRequests = employeeAllocationRequest.Where(r => r.StartDate >= from).ToList();

            foreach (var allocationRequest in allocationRequests)
            {
                _dailyAllocationService.RebuildDailyRecords(allocationRequest, employeeEmploymentPeriod);
            }
        }

        private DateTime SubstractFromNoticePeriod(DateTime date, DateTime signedOn, NoticePeriod? period)
        {
            var result = date.SubstractNoticePeriod(period);
            if (result > signedOn)
            {
                return signedOn;
            }

            return result;
        }
    }
}
