﻿using System;
using System.Data.Entity;
using System.Linq;
using Castle.Core.Logging;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Jobs
{
    [Identifier("Job.Allocation.AllocationRegenerateJob")]
    [JobDefinition(
        Code = "AllocationRegenerateJob",
        Description = "Allocation Regenerate Job",
        ScheduleSettings = "0 0 22 * * *",
        MaxExecutioPeriodInSeconds = 5400,
        PipelineName = PipelineCodes.Allocation)]
    public class AllocationRegenerateJob : IJob
    {
        private readonly IDailyAllocationService _dailyAllocationService;
        private readonly IReadOnlyUnitOfWorkService<IAllocationDbScope> _allocationDbScope;
        private readonly IClassMapping<AllocationRequest, AllocationRequestDto> _allocationRequestToAllocationRequestDtoClassMapping;
        private readonly IClassMapping<EmploymentPeriod, EmploymentPeriodDto> _employmentPeriodToEmploymentPeriodDtoClassMapping;
        private readonly ITimeService _timeService;
        private readonly ILogger _logger;

        private const string StartRegeneratingMessage = "Start AllocationRegenerateJob";
        private const string ProcessingEmploymentPeriodsMessage = "{0} Allocations to regenerat in AllocationRegenerateJob";
        private const string FinishRegeneratingMessage = "Finish AllocationRegenerateJob";
        private const string RegeneratingCrashedMessage = "Exception in AllocationRegenerateJob";

        public AllocationRegenerateJob(
            IDailyAllocationService service,
            IReadOnlyUnitOfWorkService<IAllocationDbScope> allocationScope,
            IClassMapping<AllocationRequest, AllocationRequestDto> allocationRequestToAllocationRequestDtoClassMapping,
            IClassMapping<EmploymentPeriod, EmploymentPeriodDto> employmentPeriodToEmploymentPeriodDtoClassMapping, ITimeService timeService, ILogger logger)
        {
            _dailyAllocationService = service;
            _allocationDbScope = allocationScope;
            _allocationRequestToAllocationRequestDtoClassMapping = allocationRequestToAllocationRequestDtoClassMapping;
            _employmentPeriodToEmploymentPeriodDtoClassMapping = employmentPeriodToEmploymentPeriodDtoClassMapping;
            _timeService = timeService;
            _logger = logger;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            try
            {
                _logger.Info(StartRegeneratingMessage);

                using (var scope = _allocationDbScope.Create())
                {
                    var now = _timeService.GetCurrentDate();
                    var allocationRequests =
                        scope.Repositories.AllocationRequests.Include(a => a.Project).Include(a => a.Employee)
                            .Where(a => a.AllocationRequestMetadata.NextRebuildTime.HasValue && a.AllocationRequestMetadata.NextRebuildTime.Value < now)
                            .Select(_allocationRequestToAllocationRequestDtoClassMapping.CreateFromSource).ToList();

                    var employeeIds = allocationRequests.Select(a => a.EmployeeId).ToList();

                    var employeesOrgUnits = scope.Repositories.Employees.Where(x => employeeIds.Contains(x.Id))
                        .ToDictionary(e => e.Id, e => e.OrgUnitId);

                    var employmentPeriods = scope.Repositories.EmploymentPeriods
                        .Where(e => employeeIds.Contains(e.EmployeeId))
                        .Select(_employmentPeriodToEmploymentPeriodDtoClassMapping.CreateFromSource).ToList();

                    _logger.Info(string.Format(ProcessingEmploymentPeriodsMessage, allocationRequests.Count));

                    foreach (var record in allocationRequests)
                    {
                        _dailyAllocationService.RebuildDailyRecords(record, employmentPeriods);

                        _dailyAllocationService.RebuildWeeklyStatus(employmentPeriods, record.EmployeeId,
                            record.StartDate, employeesOrgUnits[record.EmployeeId]);
                    }
                }

                _logger.Info(FinishRegeneratingMessage);
            }
            catch (Exception ex)
            {
                _logger.Error(RegeneratingCrashedMessage, ex);
                return JobResult.Fail();
            }

            return JobResult.Success();
        }
    }
}
