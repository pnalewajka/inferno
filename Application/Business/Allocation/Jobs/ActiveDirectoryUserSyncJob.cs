﻿using System.Linq;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.Business.Allocation.Jobs
{
    [Identifier("Job.Allocation.ActiveDirectoryUserSync")]
    [JobDefinition(
        Code = "ActiveDirectoryUserSync",
        Description = "Creates users based on Active Directory and assigns profiles based on mapping",
        ScheduleSettings = "0 0 0,12,14 * * *",
        MaxExecutioPeriodInSeconds = 5400,
        PipelineName = PipelineCodes.System)]
    public class ActiveDirectoryUserSyncJob : IJob
    {
        private readonly IActiveDirectoryUserSyncService _activeDirectoryUserSyncService;
        private readonly ISystemParameterService _systemParameterService;

        public ActiveDirectoryUserSyncJob(IActiveDirectoryUserSyncService activeDirectoryUserSyncService, ISystemParameterService systemParameterService)
        {
            _activeDirectoryUserSyncService = activeDirectoryUserSyncService;
            _systemParameterService = systemParameterService;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            var activeDirectoryEmployees = _activeDirectoryUserSyncService.GetAllEmployeesFromActiveDirectory(executionContext.CancellationToken).ToList();

            var activeUsers = activeDirectoryEmployees.Where(au => au.IsActive).ToList();

            string externalPath = _systemParameterService.GetParameter<string>(ParameterKeys.ActiveDirectoryExternalPath);

            var externalUserIds = activeDirectoryEmployees.Where(au => au.Path.Contains(externalPath))
                    .Select(x => x.ActiveDirectoryId)
                    .Distinct()
                    .ToArray();

            _activeDirectoryUserSyncService.DeactivateUsersNotAvailableInActiveDirectory(activeUsers, externalUserIds, executionContext.CancellationToken);

            _activeDirectoryUserSyncService.SynchronizeUsersWithActiveDirectory(activeUsers, executionContext.CancellationToken);

            _activeDirectoryUserSyncService.SynchronizeEmployeesWithActiveDirectory(activeDirectoryEmployees, executionContext.CancellationToken);

            return JobResult.Success();
        }
    }
}