﻿using Castle.Core.Logging;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.Business.Allocation.Jobs
{
    [Identifier("Job.Allocation.PrepareFreeOrPlannedEmailJob")]
    [JobDefinition(
        Code = "PrepareFreeOrPlannedReportEmail",
        Description = "Prepare free or planned review emails",
        ScheduleSettings = "0 0 6 * * 4",
        MaxExecutioPeriodInSeconds = 120,
        PipelineName = PipelineCodes.Allocation)]
    public class PrepareFreeOrPlannedEmailJob : StatusReviewEmailBase, IJob
    {
        private const string ReportName = "Free or planned";

        public PrepareFreeOrPlannedEmailJob(IStatusReviewEmailService statusReviewEmailService, ILogger logger)
            : base(statusReviewEmailService, logger, ReportName)
        {
        }

        protected override void PrepareStatusEmails()
        {
            StatusReviewEmailService.PrepareFreeOrPlanned();
        }
    }
}