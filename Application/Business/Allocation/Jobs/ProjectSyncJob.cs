﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Atlassian.Jira;
using Castle.Core.Logging;
using Smt.Atomic.Business.Allocation.BusinessEvents;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Helpers;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Allocation.Services;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.Business.Finance.Dto;
using Smt.Atomic.Business.Finance.Interfaces;
using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Jira.Entities;
using Smt.Atomic.Data.Jira.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using IProjectService = Smt.Atomic.Business.Allocation.Interfaces.IProjectService;

namespace Smt.Atomic.Business.Allocation.Jobs
{
    [Identifier("Job.Allocation.ProjectSyncJob")]
    [JobDefinition(
        Code = "ImportJiraProjects",
        Description = "Import jira projects",
        ScheduleSettings = "0 0 12,14 * * *",
        MaxExecutioPeriodInSeconds = 3200,
        PipelineName = PipelineCodes.System)]
    public class ProjectSyncJob : IJob
    {
        private class ProjectIdentifier
        {
            public string Project { get; set; }
            public string Client { get; set; }
        }

        private readonly IEmployeeService _employeeService;
        private readonly IProjectService _projectService;
        private readonly IOrgUnitService _orgUnitService;
        private readonly ICalendarService _calendarService;
        private readonly IProjectSynchronizationService _projectSynchronizationService;
        private readonly IJiraService _jiraService;
        private readonly ISystemParameterService _systemParameters;
        private readonly ILogger _logger;
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _timeTrackingUnitOfWorkService;
        private readonly IBusinessEventPublisher _businessEventPublisher;
        private readonly IClassMapping<Data.Entities.Modules.Allocation.Project, ProjectDto> _projectToProjectDto;

        private long DefaultOrgUnitId { get; set; }

        public ProjectSyncJob(IProjectService projectService,
            IEmployeeService employeeService,
            IJiraService jiraService,
            ISystemParameterService systemParameters,
            IOrgUnitService orgUnitService,
            ICalendarService calendarService,
            IProjectSynchronizationService projectSynchronizationService,
            ILogger logger,
            IBusinessEventPublisher businessEventPublisher,
            IUnitOfWorkService<ITimeTrackingDbScope> timeTrackingUnitOfWorkService,
            IClassMapping<Data.Entities.Modules.Allocation.Project, ProjectDto> projectToProjectDto)
        {
            _projectService = projectService;
            _employeeService = employeeService;
            _jiraService = jiraService;
            _systemParameters = systemParameters;
            _orgUnitService = orgUnitService;
            _calendarService = calendarService;
            _projectSynchronizationService = projectSynchronizationService;
            _logger = logger;
            _timeTrackingUnitOfWorkService = timeTrackingUnitOfWorkService;
            _businessEventPublisher = businessEventPublisher;
            _projectToProjectDto = projectToProjectDto;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            _logger.Info("Start importing projects from Jira");

            ImportProjectsFromIssues(executionContext);

            return JobResult.Success();
        }

        private void ImportProjectsFromIssues(JobExecutionContext executionContext)
        {
            var startSyncDate = _systemParameters.GetParameter<DateTime>(ParameterKeys.JiraProjectSyncStartDate);
            var defaultCalendarCode = _systemParameters.GetParameter<string>(ParameterKeys.AllocationDefaultCalendarCode);

            long defaultJiraIssueMapperId = _projectService.GetDefaultJiraIssueToTimeReportRowMapperId();

            _logger.Info("Creating JiraService Context");

            using (var jiraScope = _jiraService.CreateReadOnly())
            {
                const string projectSearchQuery = "project = \"Project Management\"  AND Type = Project AND status != Rejected AND created > \"{0}\"";
                const string jiraQueryDateTimeFormat = "yyyy-MM-dd";
                const int pageSize = 100;

                _logger.Info("Getting Jira projects");
                var startAt = 0;
                List<ProjectIssue> issues;

                do
                {
                    var searchQuery = string.Format(projectSearchQuery, startSyncDate.ToString(jiraQueryDateTimeFormat));

                    issues = jiraScope.GetIssuesByJql<ProjectIssue>(searchQuery, pageSize, startAt)
                        .ToList();

                    _logger.Info($"Got {issues.Count} issues. Synchronizing...");

                    DefaultOrgUnitId = _orgUnitService.DefaultOrgUnit().Id;

                    foreach (var issue in issues.TakeWhileNotCancelled(executionContext.CancellationToken))
                    {
                        CreateOrEditProject(issue, defaultCalendarCode, defaultJiraIssueMapperId);
                    }

                    startAt += pageSize;
                    _logger.Info($"Page {startAt / pageSize} finished.");

                } while (issues.Count == pageSize);

                using (var unitOfWork = _timeTrackingUnitOfWorkService.Create())
                {
                    var subProjects = unitOfWork.Repositories.Projects.Where(ProjectBusinessLogic.IsSubProject).ToList();

                    foreach (var subProject in subProjects)
                    {
                        var projectDto = _projectToProjectDto.CreateFromSource(subProject);
                        SynchronizeWithThirdParties(projectDto, null);
                    }
                }

                _logger.Info("Finished synchronizing Jira projects in Dante");
            }
        }

        private void CreateOrEditProject(ProjectIssue issue, string defaultCalendarCode, long defaultJiraIssueMapperId)
        {
            var jiraProjectKey = GetProjectKey(issue);

            var projectDto = _projectService.GetProjectByJiraIssueKeyOrDefault(issue.Key);
            var projectKey = GetProjectIdentifier(issue);

            if (projectDto == null)
            {
                _logger.Debug($"Creating project with key: {jiraProjectKey}");

                var regionCurrencyId = TryGetProjectCurrency(issue.Region);

                projectDto = _projectService.GetProjectByClientAndNameOrDefault(projectKey.Project, projectKey.Client)
                    ?? new ProjectDto
                    {
                        IsMainProject = true,
                        ProjectStatus = ProjectStatus.Fixed,
                        Color = HtmlColorFromHsv(Math.Abs(issue.Key.GetHashCode() % 255), 175 / 255D, 175 / 255D),
                        JiraIssueToTimeReportRowMapperId = defaultJiraIssueMapperId,
                        CostApprovalPolicy = CostApprovalPolicy.ManualApprovalIfExceedsCompanyPolicy,
                        ReinvoicingToCustomer = true,
                        MaxCostsCurrencyId = regionCurrencyId,
                        ReinvoiceCurrencyId = regionCurrencyId,
                    };
            }

            projectDto.JiraKey = jiraProjectKey;
            projectDto.JiraIssueKey = issue.Key;
            projectDto.PetsCode = issue.ApnPets;
            projectDto.Apn = NormalizeApnJira(issue.ApnJira);
            projectDto.KupferwerkProjectId = issue.KupferwerkProjectId;
            projectDto.Description = GetProjectDescription(issue);
            projectDto.StartDate = issue.BeginDate;
            projectDto.EndDate = issue.EndDate;
            projectDto.ProjectManagerId = TryGetEmployeeId(issue.ProjectManager);
            projectDto.SalesAccountManagerId = TryGetEmployeeId(issue.SalesAccountManager);
            projectDto.SupervisorManagerId = GetEmployeeIdOrDefaultByLogin(issue.SupervisorManagerLogin);
            projectDto.CalendarId = GetCalendarId(issue, defaultCalendarCode);
            projectDto.OrgUnitId = GetOrgUnitId(issue) ?? DefaultOrgUnitId;
            projectDto.ProjectShortName = NormalizeStringProperty(issue.Summary, nameof(issue.Summary), 128, jiraProjectKey);
            projectDto.ClientShortName = NormalizeStringProperty(issue.FullCustomerName, nameof(issue.FullCustomerName), 128, jiraProjectKey);
            projectDto.TimeTrackingType = GetTimeTrackingProjectType(projectDto);
            projectDto.ProjectType = ProjectTypeHelper.GetProjectTypeByJiraIdentifier(issue.ProjectType);
            projectDto.Region = issue.Region;

            SetAcronym(projectDto);

            SynchronizeWithThirdParties(projectDto, issue);
        }

        private void SetAcronym(ProjectDto project)
        {
            if (!project.Acronym.IsNullOrEmpty())
            {
                return;
            }

            using (var unitOfWork = _timeTrackingUnitOfWorkService.Create())
            {
                var takenAcronyms = unitOfWork.Repositories.Projects
                    .Where(p => !string.IsNullOrEmpty(p.Acronym))
                    .Select(p => p.Acronym)
                    .ToList();

                var acronymGenerator = new ProjectAcronymGeneratorService(takenAcronyms);
                var newAcronym = acronymGenerator.TryUseAsNewAcronym(project.JiraKey);

                project.Acronym = !string.IsNullOrEmpty(newAcronym)
                    ? newAcronym
                    : acronymGenerator.GenerateAcronym(project);
            }
        }

        private void SynchronizeWithThirdParties(ProjectDto projectDto, ProjectIssue issue)
        {
            try
            {
                var operationalManagerId = _employeeService.GetEmployeeDirectManagerOrDefaultByOrgUnits(projectDto.ProjectManagerId, OrgUnitManagerRole.OperationalManager);
                var operationalDirectorId = _employeeService.GetEmployeeDirectManagerOrDefaultByOrgUnits(projectDto.ProjectManagerId, OrgUnitManagerRole.OperationalDirector);
                var generalManagerId = _employeeService.GetEmployeeDirectManagerOrDefaultByOrgUnits(projectDto.SalesAccountManagerId, OrgUnitManagerRole.GeneralManager);

                var financialProjectDto = new FinancialProjectDto
                {
                    Apn = projectDto.Apn,
                    Description = projectDto.ProjectShortName,
                    StartDate = projectDto.StartDate,
                    ProjectManagerId = projectDto.ProjectManagerId,
                    SalesPersonId = projectDto.SalesAccountManagerId,
                    OperationalManagerId = operationalManagerId,
                    OperationalDirectorId = operationalDirectorId,
                    GeneralManagerId = generalManagerId,
                    Region = issue?.Region
                };

                SynchronizeUtilizationCategories(projectDto, financialProjectDto); // navision to dante
                projectDto = _projectService.CreateOrEditProject(projectDto);
                SynchronizeProjectTags(issue, projectDto); // sync with jira
                _projectSynchronizationService.CreateOrUpdateFinansialProject(financialProjectDto); // dante to navision
            }
            catch (DatabaseOperationException ex) when (ex.InnerException is DbUpdateException)
            {
                _logger.Error($"Project with jira project key: {projectDto.JiraKey} has duplicated name: {projectDto.ProjectShortName} and client: {projectDto.ClientShortName} and it cannot be imported", ex);
            }
            catch (Exception ex)
            {
                _logger.Error($"Unknown error when creating or editing project: `{projectDto.ProjectShortName}` of client `{projectDto.ClientShortName}`", ex);
            }
        }

        private void SynchronizeProjectTags(ProjectIssue project, ProjectDto projectDto)
        {
            if (project == null)
            {
                return;
            }

            var tagsToCreate = new List<ProjectTagDto>();
            var tagsToAssign = new List<ProjectTagDto>();
            var tagsToDetach = new List<ProjectTagDto>();
            var projectTagsFromJira = _projectService.GetProjectTagsByNames(project.Labels);

            if (project.Labels.Any())
            {
                foreach (var label in project.Labels)
                {
                    var projectTagDto = projectTagsFromJira.FirstOrDefault(x => x.Name == label);

                    if (projectTagDto == null)
                    {
                        projectTagDto = new ProjectTagDto()
                        {
                            Description = "Automatically imported from Jira.",
                            TagType = ProjectTagType.Jira,
                            Name = label,
                        };

                        tagsToCreate.Add(projectTagDto);
                        tagsToAssign.Add(projectTagDto);
                    }
                    else
                    {
                        var assigned = projectDto.TagIds != null && projectDto.TagIds.Contains(projectTagDto.Id);

                        if (!assigned)
                        {
                            tagsToAssign.Add(projectTagDto);
                        }
                    }
                }
            }

            projectDto = _projectService.GetProjectById(projectDto.Id);

            if (projectDto.TagIds.Any())
            {
                var tagsAssignedToProject = _projectService.GetProjectTagsByProjectId(projectDto.Id);
                tagsToDetach =
                    tagsAssignedToProject.Where(x => projectTagsFromJira.FirstOrDefault(p => p.Name == x.Name) == null).ToList();
            }

            _projectService.CreateOrEditProjectTags(tagsToCreate);
            _projectService.AssignAndDetachTags(tagsToAssign, tagsToDetach, projectDto.SetupId);
        }

        private void SynchronizeUtilizationCategories(ProjectDto projectDto, FinancialProjectDto financialProjectDto)
        {
            projectDto.UtilizationCategoryId = _projectSynchronizationService.SynchronizeAndGetUtilizationCategoryId(financialProjectDto);
        }

        private TimeTrackingProjectType GetTimeTrackingProjectType(ProjectDto project)
        {
            if (project.ClientShortName == "intive")
            {
                var projectName = (project.ProjectShortName ?? string.Empty).ToLower();
                var projectPetsCode = (project.PetsCode ?? string.Empty).ToLower();

                var isAbsenceProject = projectName == "absence";

                if (isAbsenceProject)
                {
                    return TimeTrackingProjectType.Absence;
                }

                var isTrainingProgram = projectName.Contains("training program");

                if (isTrainingProgram)
                {
                    return TimeTrackingProjectType.Training;
                }

                var isGuild = projectName.Contains("guild") && projectPetsCode.Contains("guild");

                if (isGuild)
                {
                    return TimeTrackingProjectType.Guild;
                }
            }

            return TimeTrackingProjectType.Normal;
        }

        private string GetProjectKey(ProjectIssue issue)
        {
            if (issue.JiraProjectDevelopmentLink == null)
            {
                return null;
            }

            var possibleKeys = issue.JiraProjectDevelopmentLink.Split('/').ToArray();
            possibleKeys = possibleKeys.Skip(Array.IndexOf(possibleKeys, "jira") + 1).ToArray();
            possibleKeys = possibleKeys.TakeWhile(e => e != string.Empty && e.ToLower() != "summary" && e.ToLower() != "issues").ToArray();
            var projectKey = possibleKeys.Last().Split('?').First();

            return projectKey;
        }

        /// <summary>
        /// Heuristics based on Michał C. approach
        /// Split summary into two parts, left for client name, right for project name
        /// Delimiters are - , ] and space
        /// </summary>
        /// <param name="issue"></param>
        /// <returns></returns>
        private ProjectIdentifier GetProjectIdentifier(ProjectIssue issue)
        {
            string summary = issue.Summary;
            string client = string.Empty;
            string project = string.Empty;
            char selectedSplitCharacter = ' ';
            char[] splitCharacters = { '-', ',', ']' };

            foreach (char splitCharacter in splitCharacters)
            {
                if (summary.Contains(splitCharacter))
                {
                    selectedSplitCharacter = splitCharacter;
                    break;
                }
            }

            string[] splitResult = summary.Split(selectedSplitCharacter);
            int splitResultCount = splitResult.Count();

            if (splitResultCount > 0)
            {
                client = splitResult[0].Replace("[", string.Empty).Replace("]", string.Empty);
            }

            if (splitResultCount > 1)
            {
                project =
                    splitResult.Skip(1)
                        .Aggregate((current, next) => current + " " + next)
                        .Replace("[", string.Empty)
                        .Replace("]", string.Empty);
            }

            return new ProjectIdentifier
            {
                Project = project.Trim(),
                Client = client.Trim()
            };
        }

        private long? GetOrgUnitId(ProjectIssue issue)
        {
            if (string.IsNullOrEmpty(issue.Department))
            {
                return null;
            }

            OrgUnitDto orgUnitDto = _orgUnitService.GetOrgUnit(issue.Department);

            return orgUnitDto?.Id;
        }

        private long? GetCalendarId(ProjectIssue issue, string defaultCalendarCode)
        {
            if (string.IsNullOrEmpty(issue.CalendarCode))
            {
                return _calendarService.GetCalendarId(defaultCalendarCode);
            }

            return _calendarService.GetCalendarId(issue.CalendarCode);
        }

        private long? GetEmployeeIdOrDefaultByLogin(string employeeLogin)
        {
            if (string.IsNullOrEmpty(employeeLogin))
            {
                return null;
            }

            var employee = _employeeService.GetEmployeeOrDefaultByLogin(employeeLogin);

            if (employee == null)
            {
                _logger.Warn($"Cannot find unique {employeeLogin} user by login in database.");
            }

            return employee?.Id;
        }

        private long? TryGetEmployeeId(JiraUser user)
        {
            if (user == null)
            {
                return null;
            }

            var employee = _employeeService.GetEmployeeOrDefaultByEmail(user.Email);

            return employee?.Id;
        }

        private long? TryGetProjectCurrency(string region)
        {
            var currencyIsoCode = region == "Poland" ? CurrencyIsoCodes.PolishZloty : CurrencyIsoCodes.UnitedStatesDollar;

            using (var unitOfWork = _timeTrackingUnitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Currencies
                    .Where(c => c.IsoCode == currencyIsoCode)
                    .Select(c => c.Id)
                    .SingleOrDefault();
            }
        }

        private static string GetProjectDescription(ProjectIssue issue)
        {
            if (string.IsNullOrEmpty(issue.ProjectDescription))
            {
                return string.IsNullOrEmpty(issue.Description) ? issue.Summary : issue.Description;
            }

            return issue.ProjectDescription;
        }

        private static string HtmlColorFromHsv(double hue, double saturation, double value)
        {
            int hi = Convert.ToInt32(Math.Floor(hue / 60)) % 6;
            double f = hue / 60 - Math.Floor(hue / 60);

            value = value * 255;
            int v = Convert.ToInt32(value);
            int p = Convert.ToInt32(value * (1 - saturation));
            int q = Convert.ToInt32(value * (1 - f * saturation));
            int t = Convert.ToInt32(value * (1 - (1 - f) * saturation));

            if (hi == 0)
                return string.Format("#{0:X2}{1:X2}{2:X2}", v, t, p);
            else if (hi == 1)
                return string.Format("#{0:X2}{1:X2}{2:X2}", q, v, p);
            else if (hi == 2)
                return string.Format("#{0:X2}{1:X2}{2:X2}", p, v, t);
            else if (hi == 3)
                return string.Format("#{0:X2}{1:X2}{2:X2}", p, q, v);
            else if (hi == 4)
                return string.Format("#{0:X2}{1:X2}{2:X2}", t, p, v);
            else
                return string.Format("#{0:X2}{1:X2}{2:X2}", v, p, q);
        }

        private static string NormalizeApnJira(string apnJira)
        {
            if (string.IsNullOrEmpty(apnJira) || apnJira == "n/a")
            {
                return null;
            }

            const int apnJiraNormalizedLength = 8;

            return apnJira.Trim().PadLeft(apnJiraNormalizedLength, '0');
        }

        private string NormalizeStringProperty(string value, string propertyName, int maxLength, string jiraProjectKey)
        {
            if (value == null)
            {
                return null;
            }

            value = value.Trim();

            if (value.Length <= maxLength)
            {
                return value;
            }

            _logger.Warn($"{propertyName} of project with jira project key: {jiraProjectKey} has been truncated (maximal allowed length: {maxLength})");

            return value.Substring(0, maxLength);
        }
    }
}