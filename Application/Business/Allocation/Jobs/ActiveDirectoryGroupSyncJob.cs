﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.Business.Allocation.Jobs
{
    [Identifier("Job.Allocation.ActiveDirectoryGroupSync")]
    [JobDefinition(
        Code = "ActiveDirectoryGroupSync",
        Description = "Creates groups based on Active Directory",
        ScheduleSettings = "0 0 23 * * *",
        MaxExecutioPeriodInSeconds = 5400,
        PipelineName = PipelineCodes.System)]
    internal class ActiveDirectoryGroupSyncJob : IJob
    {
        private readonly IActiveDirectoryGroupSyncService _activeDirectoryGroupSyncService;
        private readonly ISystemParameterService _systemParameterService;

        public ActiveDirectoryGroupSyncJob(
            IActiveDirectoryGroupSyncService activeDirectoryGroupSyncService,
            ISystemParameterService systemParameterService)
        {
            _activeDirectoryGroupSyncService = activeDirectoryGroupSyncService;
            _systemParameterService = systemParameterService;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            var groups = _activeDirectoryGroupSyncService.GetAllGroupsFromActiveDirectory(executionContext.CancellationToken);
            var filteredGroups = GetFilteredGroups(groups);

            _activeDirectoryGroupSyncService.SynchronizeGroupWithActiveDirectory(filteredGroups, executionContext.CancellationToken);

            return JobResult.Success();
        }

        private IEnumerable<ActiveDirectoryGroupDto> GetFilteredGroups(IEnumerable<ActiveDirectoryGroupDto> groups)
        {
            var toExclude = _systemParameterService.GetParameter<string[]>(ParameterKeys.ActiveDirectoryGroupFilter);
            var synchronizationPaths = _systemParameterService.GetParameter<string[]>(ParameterKeys.ActiveDirectoryGroupSyncPaths);

            return groups.Where(g =>
                !toExclude.Any(f => g.InternalDistinguishedName.Contains(f))
                && synchronizationPaths.Any(p => g.InternalDistinguishedName.EndsWith(p)));
        }
    }
}