﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Logging;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Jobs
{
    [Identifier("Job.Allocation.UpdateEmployeeStatus")]
    [JobDefinition(
        Code = "UpdateEmployeeStatusJob",
        Description = "Updates current employee status",
        ScheduleSettings = "0 0 0 * * *",
        MaxExecutioPeriodInSeconds = 5400,
        PipelineName = PipelineCodes.Allocation)]
    public sealed class UpdateEmployeeStatusJob : IJob
    {
        private readonly IUnitOfWorkService<IAllocationDbScope> _unitOfWorkService;
        private readonly IEmployeeService _employeeService;
        private readonly ILogger _logger;

        public UpdateEmployeeStatusJob(IUnitOfWorkService<IAllocationDbScope> unitOfWorkService, IEmployeeService employeeService, ILogger logger)
        {
            _unitOfWorkService = unitOfWorkService;
            _employeeService = employeeService;
            _logger = logger;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            IEnumerable<long> ids;

            try
            {
                ids = GetAllEmployeesIds();
            }
            catch (Exception ex)
            {
                _logger.Error("Retrieving employees ids failed", ex);
                return JobResult.Fail();
            }

            ids = ids
                .TakeWhileNotCancelled(executionContext.CancellationToken)
                .TakeIfEnoughTime(executionContext.MaxExecutionTime);

            var wasAtLeastOneSuccessful = false;

            foreach (var id in ids)
            {
                try
                {
                    _employeeService.UpdateCurrentEmployeeStatus(id);
                    _employeeService.UpdateEmployeeBasedOnEmploymentPeriod(id);

                    wasAtLeastOneSuccessful = true;
                }
                catch (Exception ex)
                {
                    _logger.ErrorFormat(ex, "Updating status for employee (id={0}) failed", id);
                }
            }

            return wasAtLeastOneSuccessful ? JobResult.Success() : JobResult.Fail();
        }

        private List<long> GetAllEmployeesIds()
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Employees.Select(e => e.Id).ToList();
            }
        }
    }
}
