﻿using System;
using Castle.Core.Logging;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Models;

namespace Smt.Atomic.Business.Allocation.Jobs
{
    public abstract class StatusReviewEmailBase
    {
        protected readonly IStatusReviewEmailService StatusReviewEmailService;
        protected readonly ILogger Logger;
        private readonly string _raportName;

        protected const string StartPreparingEmailsMessage = "Start preparing status review emails";
        protected const string FinishPreparingEmailsMessage = "Finish preparing status review emails";
        protected const string PreparingEmailsCrashedMessage = "Exception preparing emails";

        protected StatusReviewEmailBase(IStatusReviewEmailService statusReviewEmailService, ILogger logger, string raportName)
        {
            StatusReviewEmailService = statusReviewEmailService;
            Logger = logger;
            _raportName = raportName;
        }

        protected abstract void PrepareStatusEmails();

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            try
            {
                Logger.Info($"{StartPreparingEmailsMessage}: {_raportName}");
                PrepareStatusEmails();
                Logger.Info($"{FinishPreparingEmailsMessage}: {_raportName}");
            }
            catch (Exception ex)
            {
                Logger.Error($"{PreparingEmailsCrashedMessage}: {_raportName}", ex);
                return JobResult.Fail();
            }
            return JobResult.Success();
        }
    }
}