﻿using Castle.Core.Logging;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.Business.Allocation.Jobs
{
    [Identifier("Job.Allocation.PrepareNonAllocatedNewHiresEmailJob")]
    [JobDefinition(
        Code = "PrepareNonAllocatedNewHiresEmailReportEmail",
        Description = "Prepare non allocated new hires review emails",
        ScheduleSettings = "0 0 6 * * 4",
        MaxExecutioPeriodInSeconds = 120,
        PipelineName = PipelineCodes.Allocation)]
    public class PrepareNonAllocatedNewHiresEmailJob : StatusReviewEmailBase, IJob
    {
        private const string ReportName = "Non allocated new hires";

        public PrepareNonAllocatedNewHiresEmailJob(IStatusReviewEmailService statusReviewEmailService, ILogger logger)
            : base(statusReviewEmailService, logger, ReportName)
        {
        }

        protected override void PrepareStatusEmails()
        {
            StatusReviewEmailService.PrepareNonAllocatedNewHires();
        }
    }
}