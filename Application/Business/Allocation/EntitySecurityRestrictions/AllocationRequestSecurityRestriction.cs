﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.Allocation.EntitySecurityRestrictions
{
    public class AllocationRequestSecurityRestriction : EntitySecurityRestriction<AllocationRequest>
    {
        public AllocationRequestSecurityRestriction(IPrincipalProvider principalProvider) : base(principalProvider)
        {
            RequiresAuthentication = true;
        }

        public override Expression<Func<AllocationRequest, bool>> GetRestriction()
        {
            if (Roles.Contains(SecurityRoleType.CanViewAllocationRequest))
            {
                return AllRecords();
            }

            if (Roles.Contains(SecurityRoleType.CanViewMyTeamAllocations))
            {
                return IsProjectWithMyAllocation(CurrentPrincipal.EmployeeId);
            }

            return NoRecords();
        }

        private Expression<Func<AllocationRequest, bool>> IsProjectWithMyAllocation(long employeeId)
        {
            return ar => ar.Project.AllocationRequests.Any(par => par.EmployeeId == employeeId);
        }
    }
}
