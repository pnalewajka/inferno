﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.Allocation.EntitySecurityRestrictions
{
    public class ActiveDirectoryGroupSecurityRestriction : EntitySecurityRestriction<ActiveDirectoryGroup>
    {
        public ActiveDirectoryGroupSecurityRestriction(IPrincipalProvider principalProvider) : base(principalProvider)
        {
            RequiresAuthentication = true;
        }

        public override Expression<Func<ActiveDirectoryGroup, bool>> GetRestriction()
        {
            if (Roles.Contains(SecurityRoleType.CanViewAllActiveDirectoryGroups) || Roles.Contains(SecurityRoleType.CanEditActiveDirectoryGroup))
            {
                return AllRecords();
            }

            if (Roles.Contains(SecurityRoleType.CanViewActiveDirectoryGroup))
            {
                return IsManagerOrOwner(CurrentPrincipal.Id.Value);
            }

            return NoRecords();
        }

        private Expression<Func<ActiveDirectoryGroup, bool>> IsManagerOrOwner(long userId)
        {
            return a => a.Users.Any(u => u.Id == userId)
                    || a.Managers.Any(u => u.Id == userId)
                    || (a.OwnerId.HasValue && a.OwnerId.Value == userId);
        }
    }
}
