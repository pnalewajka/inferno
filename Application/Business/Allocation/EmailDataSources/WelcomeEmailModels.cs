﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;
using Smt.Atomic.Business.Allocation.Dto.WelcomeEmail;

namespace Smt.Atomic.Business.Allocation.EmailDataSources
{
    public static class WelcomeEmailModels
    {
        public static IEnumerable<WelcomeEmailDto> GetEmailModels()
        {
            const string resourceName = "EmailModels.json";

            var emailModels = string.Empty;
            var assembly = Assembly.GetExecutingAssembly();

            using (var stream = assembly.GetManifestResourceStream($"{assembly.GetName().Name}.EmailDataSources.{resourceName}"))
            using (var reader = new StreamReader(stream))
            {
                emailModels = reader.ReadToEnd();
            }

            return JsonConvert.DeserializeObject<List<WelcomeEmailDto>>(emailModels);
        }
    }
}
