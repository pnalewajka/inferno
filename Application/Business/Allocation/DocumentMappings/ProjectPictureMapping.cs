﻿using Smt.Atomic.Business.Common.Abstracts;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.DocumentMappings
{
    [Identifier("Pictures.ProjectPictureMapping")]
    public class ProjectPictureMapping : DocumentMapping<ProjectPicture, ProjectPictureContent, IAllocationDbScope>
    {
        public ProjectPictureMapping(IUnitOfWorkService<IAllocationDbScope> unitOfWorkService)
        : base(unitOfWorkService)
        {
            ContentColumnExpression = o => o.Content;
            NameColumnExpression = o => o.Name;
            ContentTypeColumnExpression = o => o.ContentType;
            ContentLengthColumnExpression = o => o.ContentLength;
        }
    }
}
