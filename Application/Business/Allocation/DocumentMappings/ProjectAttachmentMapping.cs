﻿using Smt.Atomic.Business.Common.Abstracts;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.DocumentMappings
{
    [Identifier("Documents.ProjectAttachmentMapping")]
    public class ProjectAttachmentMapping : DocumentMapping<ProjectAttachment, ProjectAttachmentContent, IAllocationDbScope>
    {
        public ProjectAttachmentMapping(IUnitOfWorkService<IAllocationDbScope> unitOfWorkService)
        : base(unitOfWorkService)
        {
            ContentColumnExpression = o => o.Content;
            NameColumnExpression = o => o.Name;
            ContentTypeColumnExpression = o => o.ContentType;
            ContentLengthColumnExpression = o => o.ContentLength;
        }
    }
}
