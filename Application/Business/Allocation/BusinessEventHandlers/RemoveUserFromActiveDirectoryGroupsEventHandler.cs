﻿using Smt.Atomic.Business.ActiveDirectory.BusinessEvents;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.EventSourcing.Services;

namespace Smt.Atomic.Business.Allocation.BusinessEventHandlers
{
    public class RemoveUserFromActiveDirectoryGroupsEventHandler : BusinessEventHandler<RemoveUserFromActiveDirectoryGroupsEvent>
    {
        private readonly IActiveDirectoryGroupService _activeDirectoryGroupService;

        public RemoveUserFromActiveDirectoryGroupsEventHandler(IActiveDirectoryGroupService activeDirectoryGroupService)
        {
            _activeDirectoryGroupService = activeDirectoryGroupService;
        }

        public override void Handle(RemoveUserFromActiveDirectoryGroupsEvent businessEvent)
        {
            _activeDirectoryGroupService.RemoveUserFromGroups(businessEvent.UserId, businessEvent.ActiveDirectoryGroupNames, true);
        }
    }
}
