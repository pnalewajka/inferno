﻿using Smt.Atomic.Business.ActiveDirectory.BusinessEvents;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.EventSourcing.Services;

namespace Smt.Atomic.Business.Allocation.BusinessEventHandlers
{
    public class AddUserToActiveDirectoryGroupsEventHandler : BusinessEventHandler<AddUserToActiveDirectoryGroupsEvent>
    {
        private readonly IActiveDirectoryGroupService _activeDirectoryGroupService;

        public AddUserToActiveDirectoryGroupsEventHandler(IActiveDirectoryGroupService activeDirectoryGroupService)
        {
            _activeDirectoryGroupService = activeDirectoryGroupService;
        }

        public override void Handle(AddUserToActiveDirectoryGroupsEvent businessEvent)
        {
            _activeDirectoryGroupService.AddUserToGroups(businessEvent.UserId, businessEvent.ActiveDirectoryGroupNames, true);
        }
    }
}
