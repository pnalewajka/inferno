﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessEvents;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.BusinessEventHandlers
{
    public class DailyAllocationNeedsToRegenerateEventHandler : BusinessEventHandler<DailyAllocationNeedsToRegenerateBusinessEvent>
    {
        private readonly IDailyAllocationService _dailyAllocationService;
        private readonly IReadOnlyUnitOfWorkService<IAllocationDbScope> _allocationDbScope;
        private readonly IClassMapping<AllocationRequest, AllocationRequestDto> _allocationRequestToAllocationRequestDtoClassMapping;
        private readonly IClassMapping<EmploymentPeriod, EmploymentPeriodDto> _employmentPeriodToEmploymentPeriodDtoClassMapping;

        public DailyAllocationNeedsToRegenerateEventHandler(
            IDailyAllocationService service,
            IReadOnlyUnitOfWorkService<IAllocationDbScope> allocationScope,
            IClassMapping<EmploymentPeriod, EmploymentPeriodDto> employmentPeriodToEmploymentPeriodDtoClassMapping,
            IClassMapping<AllocationRequest, AllocationRequestDto> allocationRequestToAllocationRequestDtoClassMapping)
        {
            _dailyAllocationService = service;
            _allocationDbScope = allocationScope;
            _allocationRequestToAllocationRequestDtoClassMapping = allocationRequestToAllocationRequestDtoClassMapping;
            _employmentPeriodToEmploymentPeriodDtoClassMapping = employmentPeriodToEmploymentPeriodDtoClassMapping;
        }

        public override void Handle(DailyAllocationNeedsToRegenerateBusinessEvent businessEvent)
        {
            switch (businessEvent.Scope)
            {
                case DailyAllocationNeedsToRegenerateScope.AllocationRequest:
                    if (!businessEvent.EmployeeId.HasValue || !businessEvent.PeriodFrom.HasValue)
                    {
                        throw new ArgumentException("DailyAllocationNeedsToRegenerateScope.AllocationRequest");
                    }

                    if (businessEvent.AllocationRequestId.HasValue)
                    {
                        _dailyAllocationService.RebuildDailyRecords(
                            businessEvent.AllocationRequestId.Value);
                    }

                    _dailyAllocationService.RebuildWeeklyStatus(
                            businessEvent.EmployeeId.Value,
                            businessEvent.PeriodFrom.Value);

                    if (businessEvent.AllocationRequestId.HasValue)
                    {
                        _dailyAllocationService.SetAllocationRequestExecuted(businessEvent.AllocationRequestId.Value);
                    }

                    break;

                case DailyAllocationNeedsToRegenerateScope.Project:
                    if (!businessEvent.Projectid.HasValue)
                    {
                        throw new ArgumentException("DailyAllocationNeedsToRegenerateScope.Project");
                    }

                    RebuildWeeklyStatusByProject(businessEvent.Projectid.Value);
                    break;

                case DailyAllocationNeedsToRegenerateScope.EmploymentPeriod:
                    if (!businessEvent.EmployeeId.HasValue || !businessEvent.PeriodFrom.HasValue)
                    {
                        throw new ArgumentException("DailyAllocationNeedsToRegenerateScope.EmploymentPeriod");
                    }

                    RebuildDailyRecordsByPeriod(
                        businessEvent.EmployeeId.Value,
                        businessEvent.PeriodFrom.Value);

                    _dailyAllocationService.RebuildWeeklyStatus(
                        businessEvent.EmployeeId.Value,
                        businessEvent.PeriodFrom.Value);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void RebuildDailyRecordsByPeriod(long employeeId, DateTime from)
        {
            using (var scope = _allocationDbScope.Create())
            {
                var allocationRequests = scope.Repositories.AllocationRequests.Include(a => a.Project).Include(a => a.Employee)
                    .Where(r => r.EmployeeId == employeeId
                        && (r.StartDate >= from || (r.StartDate < from && (!r.EndDate.HasValue || r.EndDate >= from))))
                    .Select(_allocationRequestToAllocationRequestDtoClassMapping.CreateFromSource).ToList();

                var employmentPeriods = scope.Repositories.EmploymentPeriods
                    .Where(e => e.EmployeeId == employeeId)
                    .Select(_employmentPeriodToEmploymentPeriodDtoClassMapping.CreateFromSource).ToList();

                foreach (var allocationRequest in allocationRequests)
                {
                    _dailyAllocationService.RebuildDailyRecords(allocationRequest, employmentPeriods);
                }
            }
        }

        private void RebuildWeeklyStatusByProject(long projectId)
        {
            using (var scope = _allocationDbScope.Create())
            {
                var employeeWeeksQuery =
                    from d in scope.Repositories.WeeklyAllocationDetails
                    where d.ProjectId == projectId
                    select new
                    {
                        d.WeeklyAllocationStatus.EmployeeId,
                        d.WeeklyAllocationStatus.Week,
                    }
                    into d
                    group d by new { d.EmployeeId }
                    into g
                    select new
                    {
                        g.Key.EmployeeId,
                        From = g.Min(x => x.Week)
                    };

                var employeeIds = employeeWeeksQuery.Select(e => e.EmployeeId).ToList();

                List<EmploymentPeriodDto> employmentPeriodList = scope.Repositories.EmploymentPeriods.Where(x => employeeIds.Contains(x.EmployeeId))
                    .Select(_employmentPeriodToEmploymentPeriodDtoClassMapping.CreateFromSource).ToList();

                var employeesOrgUnits = scope.Repositories.Employees.Where(x => employeeIds.Contains(x.Id))
                    .ToDictionary(e => e.Id, e => e.OrgUnitId);

                foreach (var employeeWeek in employeeWeeksQuery.ToList())
                {
                    var employeeEmploymentPeriod = employmentPeriodList.Where(x => x.EmployeeId == employeeWeek.EmployeeId).ToList();

                    _dailyAllocationService.RebuildWeeklyStatus(employeeEmploymentPeriod,
                        employeeWeek.EmployeeId,
                        employeeWeek.From,
                        employeesOrgUnits[employeeWeek.EmployeeId]);
                }
            }
        }
    }
}
