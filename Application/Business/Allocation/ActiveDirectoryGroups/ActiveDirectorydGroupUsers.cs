using System;
using System.Collections.Generic;
using System.Linq;

namespace Smt.Atomic.Business.Allocation.ActiveDirectoryGroups
{
    public class ActiveDirectoryGroupUsers
    {
        private readonly ISet<Guid> _usersIds;
        private readonly ISet<Guid> _managersIds;

        public ActiveDirectoryGroupUsers(Guid activeDirectoryGroupId)
        {
            _usersIds = new HashSet<Guid>();
            _managersIds = new HashSet<Guid>();

            ActiveDirectoryGroupId = activeDirectoryGroupId;
        }

        public Guid ActiveDirectoryGroupId { get; }

        public Guid OwnerId { get; set; }

        public void AddManager(Guid id)
        {
            if (!_managersIds.Contains(id))
            {
                _managersIds.Add(id);
            }
        }

        public void AddUser(Guid id)
        {
            if (!_usersIds.Contains(id))
            {
                _usersIds.Add(id);
            }
        }

        public Guid[] GetManagersIds()
        {
            return _managersIds.ToArray();
        }

        public Guid[] GetUsersIds()
        {
            return _usersIds.ToArray();
        }
    }
}
