﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Business.Allocation.Helpers
{
    public static class ProjectTypeHelper
    {
        private static readonly IDictionary<string, ProjectType> Cache = new ConcurrentDictionary<string, ProjectType>();

        public static ProjectType GetProjectTypeByJiraIdentifier(string identifier)
        {
            if (Cache.Count == 0)
            {
                Enum.GetValues(typeof(ProjectType))
                    .OfType<ProjectType>()
                    .Select(t => new KeyValuePair<string, ProjectType>(AttributeHelper.GetEnumFieldAttribute<ProjectTypeJiraIdentifierAttribute>(t)?.Identifier, t))
                    .Where(p => p.Key != null)
                    .ToList()
                    .ForEach(Cache.Add);
            }

            if (Cache.TryGetValue(identifier, out ProjectType value))
            {
                return value;
            }

            return ProjectType.Unknown;
        }
    }
}
