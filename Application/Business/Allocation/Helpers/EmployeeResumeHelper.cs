﻿namespace Smt.Atomic.Business.Allocation.Helpers
{
    public static class EmployeeResumeHelper
    {
        public const string ResumesZipFileName = "resumes";
        public const string ResumeReportDataSource = "ResumeReportDataSource";
    }
}
