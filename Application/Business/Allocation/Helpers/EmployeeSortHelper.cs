﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Helpers
{
    internal class EmployeeSortHelper
    {
        public static void OrderByLocation(OrderedQueryBuilder<Employee> builder, SortingDirection direction)
        {
            builder.ApplySortingKey(r => r.Location.Name, direction);
        }

        public static void OrderByFullName(OrderedQueryBuilder<Employee> builder, SortingDirection direction)
        {
            builder.ApplySortingKey(r => r.LastName, direction);
            builder.ApplySortingKey(r => r.FirstName, direction);
        }

        public static void OrderByJobProfile(OrderedQueryBuilder<Employee> builder, SortingDirection direction)
        {
            builder.ApplySortingKey(r => !r.JobProfiles.Any(), direction);
            builder.ApplySortingKey(JobProfileSortingExpression, direction);
        }

        public static void OrderByLineManager(OrderedQueryBuilder<Employee> builder, SortingDirection direction)
        {
            builder.ApplySortingKey(r => r.LineManager.LastName, direction);
            builder.ApplySortingKey(r => r.LineManager.FirstName, direction);
        }

        public static void OrderBySkills(OrderedQueryBuilder<Employee> builder, SortingDirection direction)
        {
            builder.ApplySortingKey(e => !e.Resumes.SelectMany(r => r.ResumeTechnicalSkills).Any(), direction);
            builder.ApplySortingKey(SkillsSortingExpression, direction);
        }

        public static void OrderByJobMatrices(OrderedQueryBuilder<Employee> builder, SortingDirection direction)
        {
            builder.ApplySortingKey(r => !r.JobMatrixs.Any(), direction);
            builder.ApplySortingKey(JobMatricesLevelSortingExpression, direction);
            builder.ApplySortingKey(JobMatricesSortingExpression, direction);
        }

        public static void OrderByResumeFillPercetage(OrderedQueryBuilder<Employee> builder, SortingDirection direction)
        {
            builder.ApplySortingKey(ResumeFillPercentageSortingExpression, direction);
        }

        public static void OrderByResumeModificationDate(OrderedQueryBuilder<Employee> builder, SortingDirection direction)
        {
            builder.ApplySortingKey(ResumeModificationDateSortingExpression, direction);
        }

        public static Expression<Func<Employee, long?>> JobProfileSortingExpression => r =>
            r.JobProfiles.OrderBy(s => s.CustomOrder)
                .Select(s => s.CustomOrder)
                .FirstOrDefault();

        public static Expression<Func<Employee, string>> SkillsSortingExpression => e =>
            e.Resumes.SelectMany(r => r.ResumeTechnicalSkills).Select(r => r.TechnicalSkill).OrderBy(s => s.Name)
                .Select(s => s.Name)
                .FirstOrDefault();

        public static Expression<Func<Employee, int?>> JobMatricesLevelSortingExpression => r =>
            r.JobMatrixs.OrderBy(s => s.Level)
                .Select(s => s.Level)
                .FirstOrDefault();

        public static Expression<Func<Employee, string>> JobMatricesSortingExpression => r =>
            r.JobMatrixs.OrderBy(s => s.Level)
                .Select(s => s.Code)
                .FirstOrDefault();

        public static Expression<Func<Employee, int?>> ResumeFillPercentageSortingExpression => r =>
            r.Resumes.OrderBy(s => s.FillPercentage)
                .Select(s => s.FillPercentage)
                .FirstOrDefault();

        public static Expression<Func<Employee, DateTime?>> ResumeModificationDateSortingExpression => r =>
            r.Resumes.OrderBy(s => s.ModifiedOn)
                .Select(s => s.ModifiedOn)
                .FirstOrDefault();
    }
}