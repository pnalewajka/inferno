﻿using System;
using System.ComponentModel;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.Business.Allocation.Helpers
{
    public static class WeeklyHoursHelper
    {
        public static decimal GetHoursByDay(IWeeklyHours hours, DayOfWeek day)
        {
            if (hours == null)
            {
                throw new ArgumentNullException(nameof(hours));
            }

            switch (day)
            {
                case DayOfWeek.Monday:
                    return hours.MondayHours;

                case DayOfWeek.Tuesday:
                    return hours.TuesdayHours;

                case DayOfWeek.Wednesday:
                    return hours.WednesdayHours;

                case DayOfWeek.Thursday:
                    return hours.ThursdayHours;

                case DayOfWeek.Friday:
                    return hours.FridayHours;

                case DayOfWeek.Saturday:
                    return hours.SaturdayHours;

                case DayOfWeek.Sunday:
                    return hours.SundayHours;

                default:
                    throw new InvalidEnumArgumentException(nameof(day), (int)day, typeof(DayOfWeek));
            }
        }

        public static decimal GetHoursByDayOrDefault(IWeeklyHours hours, DayOfWeek day)
        {
            const decimal defaultHours = 0M;

            return hours != null ? GetHoursByDay(hours, day) : defaultHours;
        }
    }
}