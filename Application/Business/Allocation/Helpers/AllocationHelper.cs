﻿using System;
using Smt.Atomic.Business.Allocation.BusinessEvents;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Business.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Helpers
{
    public static class AllocationHelper
    {
        public static DailyAllocationNeedsToRegenerateBusinessEvent CreateEventFromEmploymentPeriod(EmploymentPeriod employmentPeriod)
        {
            return CreateEventFromEmploymentPeriod(ConvertToDto(employmentPeriod));
        }

        public static DailyAllocationNeedsToRegenerateBusinessEvent CreateEventFromEmploymentPeriod(EmploymentPeriodDto employmentPeriod)
        {
            return new DailyAllocationNeedsToRegenerateBusinessEvent
            {
                Scope = DailyAllocationNeedsToRegenerateScope.EmploymentPeriod,
                EmployeeId = employmentPeriod.EmployeeId,
                PeriodFrom = CalculatePeriodFrom(employmentPeriod)
            };
        }

        public static DailyAllocationNeedsToRegenerateBusinessEvent CreateEventFromEmploymentPeriods(
            EmploymentPeriod firstEmploymentPeriod, EmploymentPeriod secondEmploymentPeriod)
        {
            return CreateEventFromEmploymentPeriods(ConvertToDto(firstEmploymentPeriod), ConvertToDto(secondEmploymentPeriod));
        }

        public static DailyAllocationNeedsToRegenerateBusinessEvent CreateEventFromEmploymentPeriods(
            EmploymentPeriodDto firstEmploymentPeriod, EmploymentPeriodDto secondEmploymentPeriod)
        {
            if (firstEmploymentPeriod.EmployeeId != secondEmploymentPeriod.EmployeeId)
            {
                throw new InvalidOperationException("Employment periods don't belong to the same employee");
            }

            var periodFrom = DateHelper.Min(CalculatePeriodFrom(firstEmploymentPeriod), CalculatePeriodFrom(secondEmploymentPeriod));

            return new DailyAllocationNeedsToRegenerateBusinessEvent
            {
                Scope = DailyAllocationNeedsToRegenerateScope.EmploymentPeriod,
                EmployeeId = firstEmploymentPeriod.EmployeeId,
                PeriodFrom = periodFrom
            };
        }

        private static DateTime CalculatePeriodFrom(EmploymentPeriodDto employmentPeriod)
        {
            var periodFrom = employmentPeriod.StartDate.SubstractNoticePeriod(employmentPeriod.NoticePeriod);

            return DateHelper.Min(periodFrom, employmentPeriod.SignedOn);
        }

        private static EmploymentPeriodDto ConvertToDto(EmploymentPeriod employmentPeriod)
        {
            return new EmploymentPeriodDto
            {
                EmployeeId = employmentPeriod.EmployeeId,
                StartDate = employmentPeriod.StartDate,
                SignedOn = employmentPeriod.SignedOn,
                NoticePeriod = employmentPeriod.NoticePeriod
            };
        }
    }
}
