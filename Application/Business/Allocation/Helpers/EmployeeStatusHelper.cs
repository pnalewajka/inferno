﻿using System;
using System.Linq;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Business.Allocation.BusinessLogics;

namespace Smt.Atomic.Business.Allocation.Helpers
{
    public static class EmployeeStatusHelper
    {
        public static EmployeeStatus ComputeEmployeeStatus(Employee employee, DateTime date)
        {
            if (!employee.EmploymentPeriods.Any())
            {
                return EmployeeStatus.Terminated;
            }

            var currentEmploymentPeriod = employee.EmploymentPeriods.AsQueryable()
                .SingleOrDefault(EmploymentPeriodBusinessLogic.OfEmployeeInDate.Parametrize(employee.Id, date));

            if (currentEmploymentPeriod == null)
            {
                if (!HasPastEmploymentPeriod(employee, date) || HasSignedFutureEmploymentPeriod(employee, date))
                {
                    return EmployeeStatus.NewHire;
                }

                return HasFutureEmploymentPeriod(employee, date)
                    ? EmployeeStatus.Inactive
                    : EmployeeStatus.Terminated;
            }

            if (!currentEmploymentPeriod.IsActive)
            {
                return EmployeeStatus.Inactive;
            }

            return TerminationHasBeenSigned(currentEmploymentPeriod, date)
                ? EmployeeStatus.Leave
                : EmployeeStatus.Active;
        }

        private static bool HasPastEmploymentPeriod(Employee employee, DateTime date)
        {
            return employee.EmploymentPeriods.Any(p => p.EndDate < date);
        }

        private static bool HasFutureEmploymentPeriod(Employee employee, DateTime date)
        {
            return employee.EmploymentPeriods.Any(p => date < p.StartDate);
        }

        private static bool HasSignedFutureEmploymentPeriod(Employee employee, DateTime date)
        {
            return employee.EmploymentPeriods.Any(p => p.SignedOn <= date && date < p.StartDate);
        }

        private static bool TerminationHasBeenSigned(EmploymentPeriod currentEmploymentPeriod, DateTime date)
        {
            return currentEmploymentPeriod.TerminatedOn <= date;
        }
    }
}
