﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Allocation.ReportModels
{
    public class BenchAllocationReportModel
    {
        public string Acronym { get; set; }

        public DateTime Week { get; set; }

        public int WeekOfTheYear { get; set; }

        public string EmployeeOrgUnitName { get; set; }

        public string EmployeeOrgUnitFlatName { get; set; }

        public string RootOrgUnitName { get; set; }

        public string RootOrgUnitFlatName { get; set; }

        public string SubOrgUnitName { get; set; }

        public string SubOrgUnitFlatName { get; set; }

        public string OrgUnitPath { get; set; }

        public string JobProfile { get; set; }

        public decimal FteRatio { get; set; }

        public PlaceOfWork? PlaceOfWork { get; set; }

        public string Location { get; set; }

        public string Seniority { get; set; }

        public long EmployeeId { get; set; }

        public decimal Fte { get; set; }
    }
}
