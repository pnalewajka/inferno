﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Allocation.ReportModels
{
    public class AttritionRateReportModel
    {
        public string Acronym { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Company { get; set; }

        public string LineManager { get; set; }

        public string JobTitle { get; set; }

        public string ContractType { get; set; }

        public DateTime Date { get; set; }

        public string EmployeeOrgUnitName { get; set; }

        public string EmployeeOrgUnitFlatName { get; set; }

        public string RootOrgUnitName { get; set; }

        public string RootOrgUnitFlatName { get; set; }

        public string SubOrgUnitName { get; set; }

        public string SubOrgUnitFlatName { get; set; }

        public string OrgUnitPath { get; set; }

        public int IsVoluntarilyTurnover { get; set; }

        public int IsOtherDepartureReasons { get; set; }

        public decimal FteRatio { get; set; }

        public string Seniority { get; set; }

        public string JobProfile { get; set; }

        public int NewHire { get; set; }

        public int EmployeeRotation { get; set; }

        public PlaceOfWork? PlaceOfWork { get; set; }

        public string Location { get; set; }
    }
}
