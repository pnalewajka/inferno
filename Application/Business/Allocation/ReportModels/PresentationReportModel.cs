﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Allocation.ReportModels
{
    public class PresentationReportModel
    {
        public IEnumerable<ProjectReportModel> ProjectPresentationViewModels { get; set; }

        public string GenerationDate { get; set; }

        public string AuthorFirstName { get; set; }

        public string AuthorLastName { get; set; }

        public string AuthorLocation { get; set; }

        public string Footer { get; set; }
    }
}
