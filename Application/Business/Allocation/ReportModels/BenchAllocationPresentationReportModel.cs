﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Allocation.ReportModels
{
    public class BenchAllocationPresentationReportModel
    {
        public IEnumerable<BenchAllocationReportModel> BenchAllocationReportModels { get; set; }
    }
}
