﻿namespace Smt.Atomic.Business.Allocation.ReportModels
{
    public class ProjectReportModel
    {
        public string Name { get; set; }

        public string ClientShortName { get; set; }

        public string ProjectShortName { get; set; }

        public string PetsCode { get; set; }

        public string JiraCode { get; set; }

        public string JiraId { get; set; }

        public string ProjectFeatures { get; set; }

        public string Description { get; set; }

        public string ProjectManager { get; set; }

        public string OrgUnits { get; set; }

        public string Industries { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public string ProjectStatus { get; set; }

        public string GenericName { get; set; }

        public string KeyTechnologies { get; set; }

        public string Technologies { get; set; }

        public string ServiceLines { get; set; }

        public string SoftwareCategories { get; set; }

        public string Tags { get; set; }

        public byte[] Picture { get; set; }

        public string CurrentPhase { get; set; }

        public string Disclosures { get; set; }

        public string HasClientReferences { get; set; }

        public string CustomerSize { get; set; }

        public string BusinessCase { get; set; }
    }
}
