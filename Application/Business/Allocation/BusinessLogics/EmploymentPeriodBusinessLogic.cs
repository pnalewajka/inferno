﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.BusinessLogics
{
    public static class EmploymentPeriodBusinessLogic
    {
        public static BusinessLogic<EmploymentPeriod, DateTime, bool> ForDate = new BusinessLogic<EmploymentPeriod, DateTime, bool>(
            (employmentPeriod, date) => employmentPeriod.StartDate <= date && (!employmentPeriod.EndDate.HasValue || date <= employmentPeriod.EndDate));

        public static BusinessLogic<EmploymentPeriod, long, DateTime, bool> OfEmployeeInDate = new BusinessLogic<EmploymentPeriod, long, DateTime, bool>(
            (employmentPeriod, employeeId, date) => employmentPeriod.EmployeeId == employeeId && ForDate.Call(employmentPeriod, date));

        public static BusinessLogic<EmploymentPeriod, DateTime, DateTime, bool> OverlapingRange = new BusinessLogic<EmploymentPeriod, DateTime, DateTime, bool>(
            (employmentPeriod, startDate, endDate) =>
                employmentPeriod.StartDate <= endDate && (!employmentPeriod.EndDate.HasValue || employmentPeriod.EndDate >= startDate));

        public static BusinessLogic<IEnumerable<EmploymentPeriod>, DateTime, DateTime, bool> IsActiveInPeriod = new BusinessLogic<IEnumerable<EmploymentPeriod>, DateTime, DateTime, bool>(
            (p, startDate, endDate) =>
                p.All(e => e.IsActive)
                && p.Any(e => e.StartDate <= startDate)
                && p.Any(e => !e.EndDate.HasValue || e.EndDate.Value >= endDate));

        public static BusinessLogic<EmploymentPeriod, long, DateTime, DateTime?, bool> OverlappingPeriodsForEmployee = new BusinessLogic<EmploymentPeriod, long, DateTime, DateTime?, bool>(
                (ep, employeeId, startDate, endDate) => ep.EmployeeId == employeeId &&
                                                          !((ep.StartDate < startDate && ep.EndDate.HasValue && ep.EndDate < startDate)
                                                            || (endDate.HasValue && ep.StartDate > endDate.Value))
            );

        public static BusinessLogic<EmploymentPeriod, DateTime, long[], bool> HasContractType = new BusinessLogic<EmploymentPeriod, DateTime, long[], bool>(
            (ep, date, contractTypeIds) =>
                ep.StartDate <= date && (!ep.EndDate.HasValue || date <= ep.EndDate) && contractTypeIds.Contains(ep.ContractTypeId));
    }
}
