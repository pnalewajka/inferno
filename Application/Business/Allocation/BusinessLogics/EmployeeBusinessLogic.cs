using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Business.Helpers;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.BusinessLogics
{
    public static class EmployeeBusinessLogic
    {
        public static readonly BusinessLogic<Employee, string> FullName = new BusinessLogic<Employee, string>(
            employee => employee.FirstName + " " + employee.LastName);

        public static readonly BusinessLogic<Employee, string> FullNameOrDefault = new BusinessLogic<Employee, string>(
            employee => employee == null ? null : employee.FirstName + " " + employee.LastName);

        public static readonly BusinessLogic<Employee, string> FullNameReversed = new BusinessLogic<Employee, string>(
            employee => employee.LastName + " " + employee.FirstName);

        public static readonly BusinessLogic<Employee, bool> IsNotTerminated = new BusinessLogic<Employee, bool>(
            employee => employee.CurrentEmployeeStatus != EmployeeStatus.Terminated);

        public static readonly BusinessLogic<Employee, SecurityRoleType, bool> HasRole = new BusinessLogic<Employee, SecurityRoleType, bool>(
            (employee, role) => UserBusinessLogic.HasRole.Call(employee.User, role));

        public static readonly BusinessLogic<Employee, bool> CanWorkWithRecruitmentData = new BusinessLogic<Employee, bool>(
            employee => HasRole.Call(employee, SecurityRoleType.CanWorkWithRecruitmentData));

        public static readonly BusinessLogic<Employee, bool> CanBeRecruitmentDataWatcher = new BusinessLogic<Employee, bool>(
                employee => HasRole.Call(employee, SecurityRoleType.CanBeRecruitmentDataWatcher)
                            && CanWorkWithRecruitmentData.Call(employee));

        public static readonly BusinessLogic<Employee, bool> IsTechnicalInterviewer = new BusinessLogic<Employee, bool>(
            employee => employee.TechnicalInterviewers.Any());

        public static readonly BusinessLogic<Employee, bool> IsWorking = new BusinessLogic<Employee, bool>(
            employee => employee.CurrentEmployeeStatus != EmployeeStatus.NewHire && IsNotTerminated.Call(employee));

        public static readonly BusinessLogic<Employee, IEnumerable<long>, bool> BelongsToOneOfOrgUnits = new BusinessLogic<Employee, IEnumerable<long>, bool>(
            (employee, orgUnitsIds) => employee.OrgUnitId != BusinessLogicHelper.NonExistingId && orgUnitsIds.Any(id => id == employee.OrgUnitId));

        public static readonly BusinessLogic<Employee, long, bool> HasGivenLineManager = new BusinessLogic<Employee, long, bool>(
            (employee, lineManagerId) => employee.LineManagerId.HasValue && employee.LineManagerId == lineManagerId);

        public static readonly BusinessLogic<Employee, long, IEnumerable<long>, bool> IsEmployeeOf = new BusinessLogic<Employee, long, IEnumerable<long>, bool>(
            (employee, managerId, managerOrgUnitIds) =>
                HasGivenLineManager.Call(employee, managerId)
                || BelongsToOneOfOrgUnits.Call(employee, managerOrgUnitIds));

        public static readonly BusinessLogic<Employee, IEnumerable<long>, bool> IsOneOfManagers = new BusinessLogic<Employee, IEnumerable<long>, bool>(
            (employee, managerIds) => managerIds.Any(id => id == employee.Id));

        public static readonly BusinessLogic<Employee, long[], bool> BelongsToOneOfCompanies = new BusinessLogic<Employee, long[], bool>(
            (employee, companyIds) => employee.CompanyId.HasValue && companyIds.Contains(employee.CompanyId.Value));

        public static readonly BusinessLogic<Employee, long[], bool> HasGivenOneOfLineManagers = new BusinessLogic<Employee, long[], bool>(
            (employee, lineManagerIds) => employee.LineManagerId.HasValue && lineManagerIds.Contains(employee.LineManagerId.Value));

        public static readonly BusinessLogic<Employee, long[], bool> HasGivenOneOfLocations = new BusinessLogic<Employee, long[], bool>(
            (employee, locationIds) => employee.LocationId.HasValue && locationIds.Contains(employee.LocationId.Value));

        public static readonly BusinessLogic<Employee, long[], long[], long[], bool> HasSkillsAndKnowledge = new BusinessLogic<Employee, long[], long[], long[], bool>(
            (employee, jobProfileIds, jobMatrixLevels, skillIds) => (!jobProfileIds.Any() || employee.JobProfiles.Any(p => jobProfileIds.Contains(p.Id)))
                                                                    && (!jobMatrixLevels.Any() || employee.JobMatrixs.Any(j => jobMatrixLevels.Contains(j.Level)))
                                                                    && (!skillIds.Any() || employee.Resumes.SelectMany(r => r.ResumeTechnicalSkills).Any(s => skillIds.Contains(s.TechnicalSkillId))));

        public static readonly BusinessLogic<Employee, long[], long, long[], long, long[], long, long[], long, long[], long, bool> HasSkills =
                           new BusinessLogic<Employee, long[], long, long[], long, long[], long, long[], long, long[], long, bool>(
                (e, skill1Name, skill1MinimumLevel, skill2Name, skill2MinimumLevel, skill3Name, skill3MinimumLevel, skill4Name, skill4MinimumLevel, skill5Name, skill5MinimumLevel)
                    => (!skill1Name.Any() || e.Resumes.SelectMany(r => r.ResumeTechnicalSkills).Any(
                               x => skill1Name.Any(s => s == x.TechnicalSkillId) && skill1MinimumLevel <= (long)x.ExpertiseLevel))
                       && (!skill2Name.Any() || e.Resumes.SelectMany(r => r.ResumeTechnicalSkills).Any(
                               x => skill2Name.Any(s => s == x.TechnicalSkillId) && skill2MinimumLevel <= (long)x.ExpertiseLevel))
                       && (!skill3Name.Any() || e.Resumes.SelectMany(r => r.ResumeTechnicalSkills).Any(
                               x => skill3Name.Any(s => s == x.TechnicalSkillId) && skill3MinimumLevel <= (long)x.ExpertiseLevel))
                       && (!skill4Name.Any() || e.Resumes.SelectMany(r => r.ResumeTechnicalSkills).Any(
                               x => skill4Name.Any(s => s == x.TechnicalSkillId) && skill4MinimumLevel <= (long)x.ExpertiseLevel))
                       && (!skill5Name.Any() || e.Resumes.SelectMany(r => r.ResumeTechnicalSkills).Any(
                               x => skill5Name.Any(s => s == x.TechnicalSkillId) && skill5MinimumLevel <= (long)x.ExpertiseLevel)));

        public static BusinessLogic<Employee, DateTime, bool> HasValidConsent =
            new BusinessLogic<Employee, DateTime, bool>((r, today) => true);

        public static BusinessLogic<Employee, string, bool> HasProfile = new BusinessLogic<Employee, string, bool>(
            (e, adGroupName) => UserBusinessLogic.HasProfile.Call(e.User, adGroupName));

        public static readonly BusinessLogic<Employee, long, DateTime, bool> BelongsToOneOfProject = new BusinessLogic<Employee, long, DateTime, bool>(
            (employee, projectId, date) => employee.AllocationRequests.Any(a => a.StartDate <= date && a.ProjectId == projectId));
    }
}
