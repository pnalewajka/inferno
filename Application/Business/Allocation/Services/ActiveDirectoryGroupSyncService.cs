﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Smt.Atomic.Business.Allocation.ActiveDirectoryGroups;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Data.ActiveDirectory;
using Smt.Atomic.Data.ActiveDirectory.Entities;

namespace Smt.Atomic.Business.Allocation.Services
{
    public class ActiveDirectoryGroupSyncService : IActiveDirectoryGroupSyncService
    {
        private readonly ISettingsProvider _settingsProvider;
        private readonly IActiveDirectoryGroupService _activeDirectoryGroupService;

        public ActiveDirectoryGroupSyncService(
            ISettingsProvider settingsProvider,
            IActiveDirectoryGroupService activeDirectoryGroupService)
        {
            _settingsProvider = settingsProvider;
            _activeDirectoryGroupService = activeDirectoryGroupService;
        }

        public IEnumerable<ActiveDirectoryGroupDto> GetAllGroupsFromActiveDirectory(CancellationToken cancellationToken)
        {
            var result = new List<ActiveDirectoryGroupDto>();

            using (var context = new CoreDirectoryContext(_settingsProvider, useCustomResultMapper: true))
            {
                var groupsWithUsers = GetGroupsWithUsers(context);

                foreach (var group in context.Groups.TakeWhileNotCancelled(cancellationToken))
                {
                    ActiveDirectoryGroupUsers groupUsers;

                    if (!groupsWithUsers.TryGetValue(group.Id, out groupUsers))
                    {
                        groupUsers = GetGroupWithoutUsers(group, context);
                    }

                    var groupDto = new ActiveDirectoryGroupDto
                    {
                        ActiveDirectoryId = group.Id,
                        Email = group.Mail,
                        GroupType = string.IsNullOrEmpty(group.Mail) ? ActiveDirectoryGroupType.Security : ActiveDirectoryGroupType.Mailing,
                        Name = group.Name,
                        Description = group.Description,
                        Info = group.Info,
                        ActiveDirectoryUserIds = groupUsers.GetUsersIds(),
                        ActiveDirectoryManagerIds = groupUsers.GetManagersIds(),
                        ActiveDirectorySubgroupIds = group.Subgroups.Select(g => g.Id).ToArray(),
                        ActiveDirectoryOwnerId = groupUsers.OwnerId,
                        InternalDistinguishedName = group.InternalDn
                    };

                    if (group.Aliases != null)
                    {
                        const string aliasPrefix = "smtp:";

                        groupDto.Aliases = string.Join("; ", group.Aliases.Select(a =>
                            a.StartsWith(aliasPrefix, StringComparison.OrdinalIgnoreCase) ? a.Substring(aliasPrefix.Length) : a));
                    }

                    result.Add(groupDto);
                }
            }

            return result;
        }

        public void SynchronizeGroupWithActiveDirectory(IEnumerable<ActiveDirectoryGroupDto> groups, CancellationToken cancellationToken)
        {
            _activeDirectoryGroupService.DeleteUnlistedGroups(groups.Select(g => g.ActiveDirectoryId).ToList());

            foreach (var group in groups.TakeWhileNotCancelled(cancellationToken))
            {
                _activeDirectoryGroupService.SynchronizeGroup(group);
            }

            foreach (var group in groups.TakeWhileNotCancelled(cancellationToken))
            {
                _activeDirectoryGroupService.AssignSubgroups(group);
            }

            if (cancellationToken.IsCancellationRequested)
            {
                return;
            }

            var dbUserGroups = _activeDirectoryGroupService
                .GetAll()
                .SelectMany(g => g.ActiveDirectoryUserIds.Select(id => new { GroupId = g.ActiveDirectoryId, UserId = id }))
                .GroupBy(g => g.UserId)
                .ToDictionary(g => g.Key, g => g.Select(e => e.GroupId).ToList());

            var importedUserGroups = groups
                .SelectMany(g => g.ActiveDirectoryUserIds.Select(id => new { GroupId = g.ActiveDirectoryId, UserId = id }))
                .GroupBy(g => g.UserId)
                .ToDictionary(g => g.Key, g => g.Select(e => e.GroupId).ToList());

            var allUsers = dbUserGroups.Select(g => g.Key).Union(importedUserGroups.Select(g => g.Key)).Distinct().ToList();

            foreach (var userId in allUsers.TakeWhileNotCancelled(cancellationToken))
            {
                var dbGroups = new HashSet<Guid>(dbUserGroups.GetByKeyOrDefault(userId) ?? Enumerable.Empty<Guid>());
                var importedGroups = new HashSet<Guid>(importedUserGroups.GetByKeyOrDefault(userId) ?? Enumerable.Empty<Guid>());

                _activeDirectoryGroupService.AddUserToGroups(userId, importedGroups.Except(dbGroups), false);
                _activeDirectoryGroupService.RemoveUserFromGroups(userId, dbGroups.Except(importedGroups), false);
            }
        }

        private IDictionary<Guid, ActiveDirectoryGroupUsers> GetGroupsWithUsers(CoreDirectoryContext context)
        {
            var groups = new Dictionary<Guid, ActiveDirectoryGroupUsers>();

            foreach (var user in context.Users)
            {
                foreach (var group in user.Groups)
                {
                    ActiveDirectoryGroupUsers groupUsers;

                    if (!groups.TryGetValue(group.Id, out groupUsers))
                    {
                        groupUsers = GetGroupWithoutUsers(group, context);

                        groups.Add(group.Id, groupUsers);
                    }

                    groupUsers.AddUser(user.Id);
                }
            }

            return groups;
        }

        private ActiveDirectoryGroupUsers GetGroupWithoutUsers(Group group, CoreDirectoryContext context)
        {
            var groupUsers = new ActiveDirectoryGroupUsers(group.Id);

            SetGroupOwner(groupUsers, group, context);
            SetGroupManagers(groupUsers, group);

            return groupUsers;
        }

        private void SetGroupManagers(ActiveDirectoryGroupUsers groupUsers, Group group)
        {
            foreach (var id in group.ManagersIds ?? Enumerable.Empty<string>())
            {
                Guid guid;

                if (Guid.TryParse(id, out guid))
                {
                    groupUsers.AddManager(guid);
                }
            }
        }

        private void SetGroupOwner(ActiveDirectoryGroupUsers groupUsers, Group group, CoreDirectoryContext context)
        {
            if (!string.IsNullOrEmpty(group.OwnerDn))
            {
                var owner = context.Users.SingleOrDefault(u => u.Path == group.OwnerDn);

                if (owner != null)
                {
                    groupUsers.OwnerId = owner.Id;

                    return;
                }
            }

            Guid guid;

            if (Guid.TryParse(group.OwnerId, out guid))
            {
                groupUsers.OwnerId = guid;
            }
        }
    }
}
