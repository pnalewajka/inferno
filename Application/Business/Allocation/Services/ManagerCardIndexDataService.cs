﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{
    public class ManagerCardIndexDataService : CardIndexDataService<EmployeeDto, Employee, IAllocationDbScope>,
        IManagerCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public ManagerCardIndexDataService(ICardIndexServiceDependencies<IAllocationDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override IEnumerable<Expression<Func<Employee, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return a => a.FirstName;
            yield return a => a.LastName;
            yield return a => a.JobTitle.NameEn;
            yield return a => a.JobTitle.NamePl;
        }

        public override IQueryable<Employee> ApplyContextFiltering(IQueryable<Employee> records, object context)
        {
            var requiredRoleContext = context as RequiredRoleContext;

            if (requiredRoleContext == null)
            {
                throw new InvalidCastException("Invalid context given for manager entry query.");
            }

            if (string.IsNullOrEmpty(requiredRoleContext.Role))
            {
                return records;
            }

            return records.Where(e => e.User.Roles.Any(r => r.Name == requiredRoleContext.Role) ||
                e.User.Profiles.Any(p => p.Roles.Any(pr => pr.Name == requiredRoleContext.Role)));
        }

        protected override IQueryable<Employee> ConfigureIncludes(IQueryable<Employee> sourceQueryable)
        {
            var technicalSkillPath = string.Join(".",
            PropertyHelper.GetPropertyPathMemberInfo(
            (Employee e) => e.Resumes.SelectMany(r => r.ResumeTechnicalSkills).Select(r => r.TechnicalSkill))
            .Select(mi => mi.Name));

            return sourceQueryable
                .Include(technicalSkillPath)
                .Include(e => e.JobMatrixs)
                .Include(e => e.JobProfiles)
                .Include(e => e.Location)
                .Include(e => e.JobTitle)
                .Include(e => e.OrgUnit)
                .Include(e => e.LineManager)
                .Include(e => e.User)
                .Include(e => e.Resumes);
        }
    }
}