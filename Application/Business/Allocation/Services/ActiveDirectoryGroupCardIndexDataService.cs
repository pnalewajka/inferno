﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using Castle.Core.Logging;
using Smt.Atomic.Business.Accounts.BusinessEvents;
using Smt.Atomic.Business.ActiveDirectory.Dto;
using Smt.Atomic.Business.ActiveDirectory.Interfaces;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Allocation.Resources;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Data.Entities.Dto;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{
    public class ActiveDirectoryGroupCardIndexDataService : CardIndexDataService<ActiveDirectoryGroupDto, ActiveDirectoryGroup, IAllocationDbScope>, IActiveDirectoryGroupCardIndexDataService
    {
        private readonly IActiveDirectoryWriter _activeDirectoryWriter;
        private readonly ILogger _logger;
        private readonly long _loggedUserId = -1;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public ActiveDirectoryGroupCardIndexDataService(ICardIndexServiceDependencies<IAllocationDbScope> dependencies,
            IActiveDirectoryWriter activeDirectoryWriter,
            ILogger logger)
            : base(dependencies)
        {
            _activeDirectoryWriter = activeDirectoryWriter;
            _logger = logger;

            if (PrincipalProvider.Current.Id.HasValue)
            {
                _loggedUserId = PrincipalProvider.Current.Id.Value;
            }
        }

        public bool CurrentUserCanEdit
        {
            get
            {
                if (PrincipalProvider.IsSet)
                {
                    return PrincipalProvider.Current.IsInRole(SecurityRoleType.CanEditActiveDirectoryGroup);
                }

                return false;
            }
        }

        protected override IEnumerable<BusinessEvent> GetAddRecordEvents(AddRecordResult result, ActiveDirectoryGroupDto record)
        {
            var businessEvents = base.GetAddRecordEvents(result, record);

            if (!record.UsersIds.Any())
            {
                return businessEvents;
            }

            businessEvents = businessEvents.Concat(
                new[] { new AddUserToProfilesEvent { UserIds = record.UsersIds, ActiveDirectoryGroupName = record.Name } });

            return businessEvents;
        }

        protected override IEnumerable<BusinessEvent> GetDeleteRecordEvents(DeleteRecordsResult result, ActiveDirectoryGroupDto record)
        {
            var businessEvents = base.GetDeleteRecordEvents(result, record);

            if (!record.UsersIds.Any())
            {
                return businessEvents;
            }

            businessEvents = businessEvents.Concat(
                new[] { new RemoveUserFromProfilesEvent { UserIds = record.UsersIds, ActiveDirectoryGroupName = record.Name } });

            return businessEvents;
        }

        protected override IEnumerable<BusinessEvent> GetEditRecordEvents(EditRecordResult result, ActiveDirectoryGroupDto newRecord,
            ActiveDirectoryGroupDto originalRecord)
        {
            var businessEvents = base.GetEditRecordEvents(result, newRecord, originalRecord);

            if (newRecord.ActiveDirectoryUserIds == originalRecord.ActiveDirectoryUserIds)
            {
                return businessEvents;
            }

            var newUsersInGroup = newRecord.UsersIds.Where(ad => !originalRecord.UsersIds.Contains(ad)).ToArray();

            if (newUsersInGroup.Any())
            {
                businessEvents = businessEvents.Concat(
                    new[] { new AddUserToProfilesEvent { UserIds = newUsersInGroup, ActiveDirectoryGroupName = newRecord.Name } });
            }

            var removedUsersInGroup = originalRecord.UsersIds.Where(ad => !newRecord.UsersIds.Contains(ad)).ToArray();

            if (removedUsersInGroup.Any())
            {
                businessEvents = businessEvents.Concat(
                    new[] { new RemoveUserFromProfilesEvent { UserIds = removedUsersInGroup, ActiveDirectoryGroupName = newRecord.Name } });
            }

            return businessEvents;
        }

        protected override NamedFilters<ActiveDirectoryGroup> NamedFilters => new NamedFilters<ActiveDirectoryGroup>(new[]
        {
            new NamedFilter<ActiveDirectoryGroup>(FilterCodes.UsersActiveDirectoryGroups, FilterByLoggedUserGroups(), SecurityRoleType.CanViewActiveDirectoryGroup),
            new NamedFilter<ActiveDirectoryGroup>(FilterCodes.AllActiveDirectoryGroups, a => true, SecurityRoleType.CanViewAllActiveDirectoryGroups),
            new NamedFilter<ActiveDirectoryGroup>(FilterCodes.WithOwnerGroup, a => a.OwnerId.HasValue),
            new NamedFilter<ActiveDirectoryGroup>(FilterCodes.WithoutOwnerGroup, a => !a.OwnerId.HasValue),
            new NamedFilter<ActiveDirectoryGroup>(FilterCodes.AnnounceLabel, a => a.Name.StartsWith("announce")),
            new NamedFilter<ActiveDirectoryGroup>(FilterCodes.DanteLabel, a => a.Name.StartsWith("dante")),
            new NamedFilter<ActiveDirectoryGroup>(FilterCodes.OtherLabel, a => !a.Name.StartsWith("announce") && !a.Name.StartsWith("dante"))
        }
            .Union(CardIndexServiceHelper.BuildEnumBasedFilters<ActiveDirectoryGroup, ActiveDirectoryGroupType>(x => x.GroupType))
        );

        protected override IEnumerable<Expression<Func<ActiveDirectoryGroup, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            if (searchCriteria.Includes(SearchAreaCodes.Name))
            {
                yield return x => x.Name;
            }

            if (searchCriteria.Includes(SearchAreaCodes.Email))
            {
                yield return x => x.Email;
            }

            if (searchCriteria.Includes(SearchAreaCodes.Description))
            {
                yield return x => x.Description;
            }

            if (searchCriteria.Includes(SearchAreaCodes.Aliases))
            {
                yield return x => x.Aliases;
            }

            if (searchCriteria.Includes(SearchAreaCodes.ActiveDirectoryGroupOwner))
            {
                yield return x => x.Owner.FirstName;
                yield return x => x.Owner.LastName;
            }

            if (searchCriteria.Includes(SearchAreaCodes.Managers))
            {
                yield return x => x.Managers.Select(m => m.FirstName);
                yield return x => x.Managers.Select(m => m.LastName);
            }

            if (searchCriteria.Includes(SearchAreaCodes.Users))
            {
                yield return x => x.Users.Select(u => u.FirstName);
                yield return x => x.Users.Select(u => u.LastName);
            }
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<ActiveDirectoryGroup, ActiveDirectoryGroupDto, IAllocationDbScope> eventArgs)
        {
            ValidateChangeScope(eventArgs.DbEntity, eventArgs.InputDto);
            UpdateActiveDirectoryGroup(eventArgs);

            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.Managers, eventArgs.InputEntity.Managers);
            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.Users, eventArgs.InputEntity.Users);

            base.OnRecordEditing(eventArgs);
        }

        private void ValidateChangeScope(ActiveDirectoryGroup dbEntity, ActiveDirectoryGroupDto inputDto)
        {
            if (dbEntity.Name != inputDto.Name
                || dbEntity.GroupType != inputDto.GroupType
                || dbEntity.Email != inputDto.Email
                || dbEntity.ActiveDirectoryId != inputDto.ActiveDirectoryId)
            {
                throw new InvalidDataException("Attempting to manipulate data");
            }

            var dbOwnerId = dbEntity.OwnerId;
            var dbManagersIds = dbEntity.Managers.Select(x => x.Id).ToArray();
            var dbMembersIds = dbEntity.Users.Select(x => x.Id).ToArray();

            var isOwner = dbOwnerId.HasValue && dbOwnerId.Value == _loggedUserId;
            var isManager = dbManagersIds.Contains(_loggedUserId);
            var isMember = dbMembersIds.Contains(_loggedUserId);

            if (!isOwner && !CurrentUserCanEdit && isManager)
            {
                if (!dbManagersIds.OrderBy(x => x).SequenceEqual(inputDto.ManagersIds.OrderBy(x => x))
                    || dbEntity.Description != inputDto.Description
                    || dbEntity.Info != inputDto.Info
                    || dbOwnerId != inputDto.OwnerId)
                {
                    throw new InvalidDataException("Attempting to manipulate data");
                }
            }
            else if (!isOwner && !CurrentUserCanEdit && isMember)
            {
                if (!dbManagersIds.OrderBy(x => x).SequenceEqual(inputDto.ManagersIds.OrderBy(x => x))
                    || dbMembersIds.OrderBy(x => x).SequenceEqual(inputDto.UsersIds.OrderBy(x => x))
                    || dbEntity.Description != inputDto.Description
                    || dbEntity.Info != inputDto.Info
                    || dbOwnerId != inputDto.OwnerId)
                {
                    throw new InvalidDataException("Attempting to manipulate data");
                }
            }
        }

        private Expression<Func<ActiveDirectoryGroup, bool>> FilterByLoggedUserGroups()
        {
            return a => a.Users.Any(u => u.Id == _loggedUserId)
                || a.Managers.Any(u => u.Id == _loggedUserId)
                || (a.OwnerId.HasValue && a.OwnerId.Value == _loggedUserId);
        }

        private void UpdateActiveDirectoryGroup(RecordEditingEventArgs<ActiveDirectoryGroup, ActiveDirectoryGroupDto, IAllocationDbScope> eventArgs)
        {
            var activeDirectoryGroupChange = new ActiveDirectoryGroupChangeDto
            {
                GroupId = eventArgs.InputDto.Id
            };

            if (eventArgs.DbEntity.OwnerId != eventArgs.InputDto.OwnerId)
            {
                activeDirectoryGroupChange.OwnerId = eventArgs.InputDto.OwnerId;
            }

            if (!eventArgs.DbEntity.Managers.Select(u => u.Id).OrderBy(u => u).SequenceEqual(eventArgs.InputDto.ManagersIds.OrderBy(u => u)))
            {
                activeDirectoryGroupChange.ManagersIds = eventArgs.InputDto.ManagersIds;
            }

            var dbUserIds = eventArgs.DbEntity.Users.Select(u => u.Id).ToHashSet();
            var inputUserIds = eventArgs.InputDto.UsersIds.ToHashSet();

            var toAddUserIds = inputUserIds.Except(dbUserIds).ToArray();

            if (toAddUserIds.Any())
            {
                activeDirectoryGroupChange.MembersToAddIds = toAddUserIds;
            }

            var toRemoveUserIds = dbUserIds.Except(inputUserIds).ToArray();

            if (toRemoveUserIds.Any())
            {
                activeDirectoryGroupChange.MembersToRemoveIds = toRemoveUserIds;
            }

            if (eventArgs.DbEntity.Description != eventArgs.InputDto.Description)
            {
                activeDirectoryGroupChange.Description = eventArgs.InputDto.Description;
            }

            if (eventArgs.DbEntity.Info != eventArgs.InputDto.Info)
            {
                activeDirectoryGroupChange.Info = eventArgs.InputDto.Info;
            }

            try
            {
                var result = _activeDirectoryWriter.ModifyActiveDirectoryGroupUsers(activeDirectoryGroupChange);

                if (result.Alerts.Any())
                {
                    eventArgs.Alerts.AddRange(result.Alerts);
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error modifying ActiveDirectory group", ex);
                eventArgs.AddError(AlertResources.ProblemWithUpdateActiveDirectory);
            }
        }
    }
}

