﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Business.Allocation.Services
{
    public class DateRangeService : IDateRangeService
    {
        public IEnumerable<DateTime> GetAllWeeksInDateRanges(DateTime from, DateTime to)
        {
            return GetDaysInRange(GetLastMonday(from), to, 7, true);
        }

        public IEnumerable<DateTime> GetDaysInRange(DateTime from, DateTime to, bool includeWeekends = false)
        {
            return GetDaysInRange(from, to, 1, includeWeekends);
        }

        private DateTime GetLastMonday(DateTime date)
        {
            switch (date.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    return date;

                case DayOfWeek.Tuesday:
                    return date.AddDays(-1);

                case DayOfWeek.Wednesday:
                    return date.AddDays(-2);

                case DayOfWeek.Thursday:
                    return date.AddDays(-3);

                case DayOfWeek.Friday:
                    return date.AddDays(-4);

                case DayOfWeek.Saturday:
                    return date.AddDays(-5);

                case DayOfWeek.Sunday:
                    return date.AddDays(-6);

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private IEnumerable<DateTime> GetDaysInRange(DateTime from, DateTime to, int step, bool includeWeekends)
        {
            var count = 1 + to.Subtract(from).Days / step;

            for (var offset = 0; offset < count; offset++)
            {
                var date = from.AddDays(step * offset);
                var skipDateBecauseOfWeekend = !includeWeekends && date.IsWeekend();

                if (skipDateBecauseOfWeekend)
                {
                    continue;
                }

                yield return date;
            }
        }
    }
}
