﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{

    public class ProjectClientCardIndexDataService : CardIndexDataService<ProjectClientDto, Project, IAllocationDbScope>, IProjectClientCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public ProjectClientCardIndexDataService(ICardIndexServiceDependencies<IAllocationDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override IEnumerable<Expression<Func<Project, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return u => u.ProjectSetup.ClientShortName;
        }

        public override IQueryable<Project> ApplyContextFiltering(IQueryable<Project> records, object context)
        {
            return records
                .GroupBy(r => r.ProjectSetup.ClientShortName)
                .Select(ta => ta.FirstOrDefault());
        }
    }
}
