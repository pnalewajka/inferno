﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Filters;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Dictionaries.BusinessLogic;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using FilterCodes = Smt.Atomic.Business.Allocation.Consts.FilterCodes;

namespace Smt.Atomic.Business.Allocation.Services
{
    internal class EmployeeAllocationCardIndexDataService : EmployeeAllocationBaseCardIndexDataService, IEmployeeAllocationCardIndexDataService
    {
        private readonly IDateRangeService _dateRangeService;
        private readonly IReadOnlyUnitOfWorkService<IAllocationDbScope> _unitOfWorkAllocationService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IJobProfileCardIndexDataService _jobProfileCardIndexDataService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public EmployeeAllocationCardIndexDataService(ICardIndexServiceDependencies<IAllocationDbScope> dependencies,
            IOrgUnitService orgUnitService,
            IDateRangeService dateRangeService,
            ITimeService timeService,
            IReadOnlyUnitOfWorkService<IAllocationDbScope> unitOfWorkAllocationService,
            WeeklyAllocationDataService weeklyAllocationService,
            DailyAllocationDataService dailyAllocationDataService,
            RequestAllocationDataService requestAllocationDataService,
            ISystemParameterService systemParameterService,
            IEmployeeCardIndexDataService employeeService,
            ICalendarService calendarService,
            IAllocationRequestCardIndexDataService allocationRequestCardIndexDataService,
            IPrincipalProvider principalProvider,
            IClassMappingFactory classMappingFactory,
            IJobProfileCardIndexDataService jobProfileCardIndexDataService)
            : base(dependencies, orgUnitService, timeService, weeklyAllocationService, dailyAllocationDataService, requestAllocationDataService, calendarService, employeeService, allocationRequestCardIndexDataService, classMappingFactory, principalProvider)
        {
            _dateRangeService = dateRangeService;
            _unitOfWorkAllocationService = unitOfWorkAllocationService;
            _systemParameterService = systemParameterService;
            _jobProfileCardIndexDataService = jobProfileCardIndexDataService;
        }

        public IDictionary<string, IReadOnlyCollection<EmployeeAllocationPeriodDto>> CalculateWeekAllocations(AllocationChartSearchDto searchModel)
        {
            var currentDate = _timeService.GetCurrentDate();
            const int chartDateRangeMonthsInPast = 10;
            const int chartDateRangeDaysInFuture = 12 * 7;

            var startDate = searchModel.DateFrom ?? DateHelper.BeginningOfMonth(currentDate.AddMonths(-chartDateRangeMonthsInPast));
            var endDate = searchModel.DateTo ?? DateHelper.EndOfMonth(currentDate.AddDays(chartDateRangeDaysInFuture));

            if (endDate <= startDate)
            {
                return new Dictionary<string, IReadOnlyCollection<EmployeeAllocationPeriodDto>>();
            }

            var cutOffDate = _systemParameterService.GetParameter<DateTime>(ParameterKeys.AllocationDataCutOffDate);

            if (startDate < cutOffDate)
            {
                startDate = cutOffDate;
            }

            var weeks = _dateRangeService.GetAllWeeksInDateRanges(startDate, endDate).Where(w => w >= startDate && w <= endDate).ToList();

            var returnDictionary = new Dictionary<string, IReadOnlyCollection<EmployeeAllocationPeriodDto>>();

            if (!searchModel.OrgUnitIds.Any())
            {
                return returnDictionary;
            }

            using (var unitOfWork = _unitOfWorkAllocationService.Create())
            {
                var weekStatuses = unitOfWork.Repositories.WeeklyAllocationStatus
                   .Where(w => w.Employee.IsProjectContributor)
                   .Where(w => w.Week <= endDate && w.Week >= startDate)
                   .Include(a => a.Employee.OrgUnit)
                   .Include(a => a.Employee.Location.City)
                   .ToList();

                var orgUnitPaths = unitOfWork.Repositories.OrgUnits.GetByIds(searchModel.OrgUnitIds).Select(o => o.Path).Distinct().OrderBy(o => o.Length).ToList();

                var weekAllocations = weekStatuses
                    .Where(s => orgUnitPaths.Any(s.Employee.OrgUnit.Path.StartsWith))
                    .Where(s => !searchModel.LocationIds.Any() || (s.Employee.LocationId.HasValue && searchModel.LocationIds.Contains(s.Employee.LocationId.Value)))
                    .Where(s =>
                    !searchModel.CountryIds.Any()
                    || (s.Employee.Location != null && s.Employee.Location.Country != null && searchModel.CountryIds.Contains(s.Employee.Location.CountryId.Value))
                    || (s.Employee.Location != null && s.Employee.Location.City != null && searchModel.CountryIds.Contains(s.Employee.Location.City.CountryId))
                    )
                .GroupBy(ws => ws.Week)
                .Select(wsg => new EmployeeAllocationPeriodDto
                {
                    Week = wsg.Key,
                    AllocatedFTE = wsg.Sum(w => w.CalendarHours == 0 ? 0 : w.AllocatedHours / w.CalendarHours),
                    PlannedFTE = wsg.Sum(w => w.CalendarHours == 0 ? 0 : w.PlannedHours / w.CalendarHours),
                    AvailableFTE = wsg.Sum(w => w.CalendarHours == 0 ? 0 : w.AvailableHours / w.CalendarHours)
                })
                .ToList();

                returnDictionary.Add(orgUnitPaths.First(), weeks
                    .Select(w => new EmployeeAllocationPeriodDto { Week = w })
                    .Select(w => weekAllocations.FirstOrDefault(wa => wa.Week == w.Week) ?? w)
                    .ToList());

                return returnDictionary;
            }
        }
        public IReadOnlyCollection<EmployeeAllocationPeriodDto> CalculateWeekAllocationsForJobProfile(long? jobProfileId, long? locationId, long? level)
        {
            var currentDate = _timeService.GetCurrentDate();
            const int chartDateRangeMonthsInPast = 10;
            const int chartDateRangeMonthInFuture = 2;

            var startDate = DateHelper.BeginningOfMonth(currentDate.AddMonths(-chartDateRangeMonthsInPast));
            var endDate = DateHelper.EndOfMonth(currentDate.AddMonths(chartDateRangeMonthInFuture));

            var cutOffDate = _systemParameterService.GetParameter<DateTime>(ParameterKeys.AllocationDataCutOffDate);

            if (startDate < cutOffDate)
            {
                startDate = cutOffDate;
            }

            var weeks = _dateRangeService.GetAllWeeksInDateRanges(startDate, endDate).Where(w => w >= startDate && w <= endDate).ToList();

            using (var unitOfWork = _unitOfWorkAllocationService.Create())
            {
                var weekStatuses = unitOfWork.Repositories.WeeklyAllocationStatus
                   .Where(w => w.Employee.IsProjectContributor)
                   .Where(w => w.Week <= endDate && w.Week >= startDate)
                   .Where(w => !jobProfileId.HasValue || w.Employee.JobProfiles.Any(j => j.Id == jobProfileId.Value))
                   .Where(w => !locationId.HasValue || w.Employee.LocationId == locationId)
                   .Where(w => !level.HasValue || w.Employee.JobMatrixs.Any(jm => jm.Level == level))
                   .ToList();

                var returnCollection = new List<EmployeeAllocationPeriodDto>();

                var weekAllocations = weekStatuses
                .GroupBy(ws => ws.Week)
                .Select(wsg => new EmployeeAllocationPeriodDto
                {
                    Week = wsg.Key,
                    AllocatedFTE = wsg.Sum(w => w.CalendarHours == 0 ? 0 : w.AllocatedHours / w.CalendarHours),
                    PlannedFTE = wsg.Sum(w => w.CalendarHours == 0 ? 0 : w.PlannedHours / w.CalendarHours),
                    AvailableFTE = wsg.Sum(w => w.CalendarHours == 0 ? 0 : w.AvailableHours / w.CalendarHours)
                })
                .ToList();

                returnCollection.AddRange(
                    weeks.Select(w => new EmployeeAllocationPeriodDto { Week = w })
                        .Select(w => weekAllocations.FirstOrDefault(wa => wa.Week == w.Week) ?? w)
                        .ToList());

                return returnCollection;
            }
        }

        public IEnumerable<JobProfileChartDetailsDto> JobProfileChartDetails(AllocationChartSearchDto searchModel, DateTime period)
        {
            using (var unitOfWork = _unitOfWorkAllocationService.Create())
            {
                var orgUnitPaths = unitOfWork.Repositories.OrgUnits.GetByIds(searchModel.OrgUnitIds).Select(o => o.Path).Distinct().OrderBy(o => o.Length).ToList();

                var weekStatuses = unitOfWork.Repositories.WeeklyAllocationStatus
                    .Where(s => orgUnitPaths.Any(p => s.Employee.OrgUnit.Path.StartsWith(p)))
                    .Where(s => !searchModel.LocationIds.Any() || (s.Employee.LocationId.HasValue && searchModel.LocationIds.Contains(s.Employee.LocationId.Value)))
                    .Where(s => !searchModel.CountryIds.Any() || (s.Employee.Location != null && s.Employee.Location.Country != null && searchModel.CountryIds.Contains(s.Employee.Location.CountryId.Value)))
                    .Where(w => w.Employee.IsProjectContributor)
                    .Where(w => w.Week == period)
                    .Where(w => w.PlannedHours > 0 || w.AvailableHours > 0 || w.AllocatedHours > 0)
                    .ToList();

                var jobProfiles =
                    unitOfWork.Repositories.JobProfiles
                    .SelectMany(jp => jp.UsedInEmployees.Select(e => new { EmployeeId = e.Id, JobProfileName = jp.Name, JobProfileId = jp.Id }))
                    .ToList();

                var jobMatrixLevels =
                    unitOfWork.Repositories.JobMatrixLevels
                    .SelectMany(jml => jml.Employees.Select(e => new { EmployeeId = e.Id, JobMatrixLevelCode = jml.Code, JobMatrixLevel = jml.Level }))
                    .ToList();

                foreach (var jobProfileGroup in jobProfiles.GroupBy(jp => new { jp.JobProfileName, jp.JobProfileId }))
                {
                    var jobProfileKey = jobProfileGroup.Key;

                    var jobProfileEmployeeIds = jobProfileGroup.Select(jpg => jpg.EmployeeId).Distinct();

                    var jobProfileStatuses = weekStatuses
                        .Where(ws => jobProfileEmployeeIds.Contains(ws.EmployeeId))
                        .GroupBy(ws => new { LocationName = ws.Employee.Location.Name, ws.Employee.LocationId })
                        .ToList();

                    foreach (var jobProfileStatus in jobProfileStatuses)
                    {
                        yield return new JobProfileChartDetailsDto
                        {
                            JobProfileName = jobProfileKey.JobProfileName,
                            JobProfileId = jobProfileKey.JobProfileId,
                            JobMatrixLevelCode = string.Empty,
                            LocationName = jobProfileStatus.Key.LocationName,
                            LocationId = jobProfileStatus.Key.LocationId,
                            PlannedFte = jobProfileStatus.Sum(w => w.CalendarHours == 0 ? 0 : w.PlannedHours / w.CalendarHours),
                            AllocatedFte = jobProfileStatus.Sum(w => w.CalendarHours == 0 ? 0 : w.AllocatedHours / w.CalendarHours),
                            AvailableFte = jobProfileStatus.Sum(w => w.CalendarHours == 0 ? 0 : w.AvailableHours / w.CalendarHours)
                        };
                    }

                    var jobProfileJobMatrixLevels =
                        jobMatrixLevels.Where(jml => jobProfileEmployeeIds.Contains(jml.EmployeeId))
                        .ToList();

                    foreach (var jobProfileJobMatrixLevel in jobProfileJobMatrixLevels.GroupBy(jpml => new { jpml.JobMatrixLevelCode, jpml.JobMatrixLevel }))
                    {
                        var jobMatrixLevel = jobProfileJobMatrixLevel.Key;

                        var employeeIds = jobProfileJobMatrixLevel.Select(ml => ml.EmployeeId).Distinct().ToList();

                        var jobMatrixLevelStatuses = weekStatuses
                        .Where(ws => employeeIds.Contains(ws.EmployeeId))
                        .GroupBy(ws => new { LocationName = ws.Employee.Location.Name, ws.Employee.LocationId })
                        .ToList();

                        foreach (var jobMatrixLevelStatus in jobMatrixLevelStatuses)
                        {
                            yield return new JobProfileChartDetailsDto
                            {
                                JobProfileName = jobProfileKey.JobProfileName,
                                JobProfileId = jobProfileKey.JobProfileId,
                                JobMatrixLevelCode = jobMatrixLevel.JobMatrixLevelCode,
                                JobMatrixLevel = jobMatrixLevel.JobMatrixLevel,
                                LocationName = jobMatrixLevelStatus.Key.LocationName,
                                LocationId = jobMatrixLevelStatus.Key.LocationId,
                                PlannedFte = jobMatrixLevelStatus.Sum(w => w.CalendarHours == 0 ? 0 : w.PlannedHours / w.CalendarHours),
                                AllocatedFte = jobMatrixLevelStatus.Sum(w => w.CalendarHours == 0 ? 0 : w.AllocatedHours / w.CalendarHours),
                                AvailableFte = jobMatrixLevelStatus.Sum(w => w.CalendarHours == 0 ? 0 : w.AvailableHours / w.CalendarHours)
                            };
                        }
                    }
                }
            }
        }

        protected override NamedFilters<Employee> NamedFilters
        {
            get
            {
                var namedFilters = base.NamedFilters;

                namedFilters.AppendUniqueFilters(new NamedFilters<Employee>(
                    new[]{
                            MyEmployeesFilter.GetDescendantEmployeeFilter(_orgUnitService, _principalProvider),
                            MyEmployeesFilter.GetChildEmployeeFilter(_orgUnitService, _principalProvider),
                            AllAllocationsFilter.EmployeeFilter,
                            MyProjectGuestsFilter.GetEmployeeFilter(_principalProvider, _orgUnitService, _unitOfWorkAllocationService),
                            MyEmployeesVisitingFilter.GetEmployeeFilter(_principalProvider, _orgUnitService, _unitOfWorkAllocationService),
                            LineManagerFilter.EmployeeFilter,
                            OrgUnitsFilter.EmployeeFilter,
                            LocationFilter.EmployeeFilter,
                            SkillsAndKnowledgeFilter.EmployeeFilter,
                            SkillsFilter.EmployeeFilter,
                            LanguageWithLevelBusinessLogic.EmployeeFilter
                        }.Concat(GetFakeFilters())));

                return namedFilters;
            }
        }

        private IEnumerable<NamedFilter<Employee>> GetFakeFilters()
        {
            return new[]
            {
                FilterCodes.FakeDetailedFilter,
                FilterCodes.FakePlannedFilter,
                FilterCodes.FakeColorsFilter,
                FilterCodes.FakeWeekendsFilter
            }
            .Select(code => new NamedFilter<Employee>(code, e => true));
        }

        protected override void EnhanceQueryCriteriaBeforeGetRecords(QueryCriteria criteria)
        {
            CreateDateRangeFilterObjectIfFilterExists(criteria, FilterCodes.MyEmployeesVisitingFilter);
            CreateDateRangeFilterObjectIfFilterExists(criteria, FilterCodes.MyProjectGuestsFilter);
        }
    }
}
