﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{
    public class ProjectTagCardIndexDataService : CardIndexDataService<ProjectTagDto, ProjectTag, IAllocationDbScope>, IProjectTagCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public ProjectTagCardIndexDataService(ICardIndexServiceDependencies<IAllocationDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override IEnumerable<Expression<Func<ProjectTag, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return a => a.Name;
            yield return a => a.Description;
            yield return a => a.TagType;
        }
    }
}
