﻿using System.Linq;
using Castle.Core;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{
    [Interceptor(typeof(CacheInterceptor))]
    public class AllocationContractTypeService : IAllocationContractTypeService
    {
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService;

        public AllocationContractTypeService(IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        [Cached(CacheArea = CacheAreas.ContractTypes, ExpirationInSeconds = 600)]
        public long GetContractTypeIdByActiveDirectoryCode(string code)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var result = unitOfWork.Repositories.ContractTypes
                    .Where(c => c.ActiveDirectoryCode == code)
                    .Select(c => (long?)c.Id)
                    .SingleOrDefault();

                return result ?? GetDefaultContractTypeId();
            }
        }

        [Cached(CacheArea = CacheAreas.ContractTypes)]
        public long GetDefaultContractTypeId()
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                const string noDataName = "NO DATA";

                return unitOfWork.Repositories.ContractTypes
                    .Where(c => c.NameEn == noDataName)
                    .Select(c => c.Id)
                    .Single();
            }
        }

        [Cached(CacheArea = CacheAreas.ContractTypes, ExpirationInSeconds = 600)]
        public TimeTrackingCompensationCalculators? GetTimeTrackingCompensationCalculatorOrDefaultByContractTypeId(long contractId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var calculator = unitOfWork.Repositories.ContractTypes
                    .Where(c => c.Id == contractId)
                    .Select(c => c.TimeTrackingCompensationCalculator)
                    .SingleOrDefault();

                return calculator;
            }
        }

        [Cached(CacheArea = CacheAreas.ContractTypes, ExpirationInSeconds = 600)]
        public BusinessTripCompensationCalculators? GetBusinessTripCompensationCalculatorOrDefaultByContractTypeId(long contractId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var calculator = unitOfWork.Repositories.ContractTypes
                    .Where(c => c.Id == contractId)
                    .Select(c => c.BusinessTripCompensationCalculator)
                    .SingleOrDefault();

                return calculator;
            }
        }
    }
}