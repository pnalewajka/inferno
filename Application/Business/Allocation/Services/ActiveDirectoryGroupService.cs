﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Castle.Core.Logging;
using Smt.Atomic.Business.ActiveDirectory.Dto;
using Smt.Atomic.Business.ActiveDirectory.Interfaces;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{
    using static ActiveDirectoryGroupVisualizationDto;

    public class ActiveDirectoryGroupService : IActiveDirectoryGroupService
    {
        private readonly IEntityMergingService _entityMergingService;
        private readonly IUnitOfWorkService<IAllocationDbScope> _unitOfWorkService;
        private readonly IClassMapping<ActiveDirectoryGroup, ActiveDirectoryGroupDto> _activeDirectoryGroupToDtoMapping;
        private readonly IActiveDirectoryWriter _activeDirectoryWriter;
        private readonly ILogger _logger;

        public ActiveDirectoryGroupService(
            IEntityMergingService entityMergingService,
            IUnitOfWorkService<IAllocationDbScope> unitOfWorkService,
            IActiveDirectoryWriter activeDirectoryWriter,
            IClassMapping<ActiveDirectoryGroup, ActiveDirectoryGroupDto> activeDirectoryGroupToDtoMapping,
            ILogger logger)
        {
            _entityMergingService = entityMergingService;
            _unitOfWorkService = unitOfWorkService;
            _activeDirectoryWriter = activeDirectoryWriter;
            _activeDirectoryGroupToDtoMapping = activeDirectoryGroupToDtoMapping;
            _logger = logger;
        }

        public IEnumerable<ActiveDirectoryGroupDto> GetAll()
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.ActiveDirectoryGroups
                    .AsEnumerable()
                    .Select(g => _activeDirectoryGroupToDtoMapping.CreateFromSource(g))
                    .ToList();
            }
        }

        public ActiveDirectoryGroupVisualizationDto GetActiveDirectoryGroupVisualizationById(long id)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return GetActiveDirectoryGroupVisualizationById(id, unitOfWork, Enumerable.Empty<long>(), new[] { id });
            }
        }

        private ActiveDirectoryGroupVisualizationDto GetActiveDirectoryGroupVisualizationById(
            long id, IUnitOfWork<IAllocationDbScope> unitOfWork, IEnumerable<long> userIds, IEnumerable<long> groupIds)
        {
            var usedUserIds = new HashSet<long>(userIds);
            var usedGroupIds = new HashSet<long>(groupIds);

            var group = unitOfWork.Repositories.ActiveDirectoryGroups.Where(g => g.Id == id).Single();

            var visualization = new ActiveDirectoryGroupVisualizationDto
            {
                Id = group.Id,
                Name = group.Name,
                Email = group.Email,
                GroupType = group.GroupType,
                Subgroups = new List<ActiveDirectoryGroupVisualizationDto>(),
                Owner = unitOfWork.Repositories.Users
                    .Where(u => u.Id == group.OwnerId)
                    .Select(ConvertToActiveDirectoryGroupUserDto)
                    .SingleOrDefault(),
                Managers = unitOfWork.Repositories.Users
                    .Where(u => u.ActiveDirectoryGroupsManager.Any(g => g.Id == id))
                    .Select(ConvertToActiveDirectoryGroupUserDto)
                    .ToList(),
                Members = unitOfWork.Repositories.Users
                   .Where(u => !usedUserIds.Contains(u.Id) && u.ActiveDirectoryGroups.Any(g => g.Id == id))
                   .Select(ConvertToActiveDirectoryGroupUserDto)
                   .ToList()
            };

            var subgroupIds = unitOfWork.Repositories.ActiveDirectoryGroups
                .Where(g => !usedGroupIds.Contains(g.Id) && g.ParentGroups.Any(p => p.Id == id))
                .Select(g => g.Id)
                .ToList();

            usedGroupIds.UnionWith(subgroupIds);
            usedUserIds.UnionWith(visualization.Members.Select(m => m.Id));

            foreach (var subgroupId in subgroupIds)
            {
                visualization.Subgroups.Add(GetActiveDirectoryGroupVisualizationById(subgroupId, unitOfWork, usedUserIds, usedGroupIds));
            }

            return visualization;
        }

        private static Expression<Func<User, ActiveDirectoryGroupUserDto>> ConvertToActiveDirectoryGroupUserDto =>
            user => new ActiveDirectoryGroupUserDto
            {
                Id = user.Id,
                EmployeeId = user.Employees.FirstOrDefault().Id,
                FullName = user.FirstName + " " + user.LastName
            };

        public void SynchronizeGroup(ActiveDirectoryGroupDto group)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var dbGroup = unitOfWork.Repositories.ActiveDirectoryGroups.SingleOrDefault(g => g.ActiveDirectoryId == group.ActiveDirectoryId);

                if (dbGroup == null)
                {
                    dbGroup = unitOfWork.Repositories.ActiveDirectoryGroups.CreateEntity();

                    unitOfWork.Repositories.ActiveDirectoryGroups.Add(dbGroup);
                }

                dbGroup.ActiveDirectoryId = group.ActiveDirectoryId;
                dbGroup.Email = group.Email;
                dbGroup.GroupType = group.GroupType;
                dbGroup.Description = group.Description;
                dbGroup.Info = group.Info;
                dbGroup.Aliases = group.Aliases;
                dbGroup.Name = group.Name;
                dbGroup.Owner = group.ActiveDirectoryOwnerId.HasValue
                    ? unitOfWork.Repositories.Users.SingleOrDefault(u => u.ActiveDirectoryId == group.ActiveDirectoryOwnerId.Value)
                    : null;

                var managers = unitOfWork.Repositories.Users.Where(u => group.ActiveDirectoryManagerIds.Contains(u.ActiveDirectoryId.Value)).ToList();

                if (dbGroup.Managers == null && managers.Any())
                {
                    dbGroup.Managers = new List<User>();
                }

                foreach (var manager in managers)
                {
                    if (dbGroup.Managers.All(m => m.ActiveDirectoryId != manager.ActiveDirectoryId))
                    {
                        dbGroup.Managers.Add(manager);
                    }
                }

                unitOfWork.Commit();
            }
        }

        public void AssignSubgroups(ActiveDirectoryGroupDto group)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var dbGroup = unitOfWork.Repositories.ActiveDirectoryGroups.SingleOrDefault(g => g.ActiveDirectoryId == group.ActiveDirectoryId);
                var subgroups = unitOfWork.Repositories.ActiveDirectoryGroups
                    .Where(g => group.ActiveDirectorySubgroupIds.Contains(g.ActiveDirectoryId))
                    .ToList();

                _entityMergingService.MergeCollections(unitOfWork, dbGroup.Subgroups, subgroups);

                unitOfWork.Commit();
            }
        }

        public void DeleteUnlistedGroups(IEnumerable<Guid> groupIds)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var groups = unitOfWork.Repositories.ActiveDirectoryGroups.Where(g => !groupIds.Contains(g.ActiveDirectoryId)).ToList();

                foreach (var group in groups)
                {
                    group.Subgroups.Clear();
                    group.ParentGroups.Clear();
                }

                unitOfWork.Repositories.ActiveDirectoryGroups.DeleteRange(groups);

                unitOfWork.Commit();
            }
        }

        public void AddUserToGroups(Guid userId, IEnumerable<Guid> groupIds, bool shouldUpdateAd)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var groups = unitOfWork.Repositories.ActiveDirectoryGroups.Where(x => groupIds.Contains(x.ActiveDirectoryId)).ToArray();

                AddUserToGroupsInternal(unitOfWork, userId, groups, shouldUpdateAd);
            }
        }

        public void AddUserToGroups(Guid userId, IEnumerable<string> activeDirectoriesGroupNames, bool shouldUpdateAd)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var groups = GetGroupsByActiveDirectoryNames(unitOfWork, activeDirectoriesGroupNames);

                if (!groups.Any())
                {
                    return;
                }

                AddUserToGroupsInternal(unitOfWork, userId, groups, shouldUpdateAd);
            }
        }

        public void RemoveUserFromGroups(Guid userId, IEnumerable<Guid> groupIds, bool shouldUpdateAd)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var groups = unitOfWork.Repositories.ActiveDirectoryGroups.Where(x => groupIds.Contains(x.ActiveDirectoryId)).ToArray();

                RemoveUserFromGroupsInternal(unitOfWork, userId, groups, shouldUpdateAd);
            }
        }

        public void RemoveUserFromGroups(Guid userId, IEnumerable<string> activeDirectoriesGroupNames, bool shouldUpdateAd)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var groups = GetGroupsByActiveDirectoryNames(unitOfWork, activeDirectoriesGroupNames);

                if (!groups.Any())
                {
                    return;
                }

                RemoveUserFromGroupsInternal(unitOfWork, userId, groups, shouldUpdateAd);
            }
        }

        private void AddUserToGroupsInternal(IUnitOfWork<IAllocationDbScope> unitOfWork, Guid userId,
            IEnumerable<ActiveDirectoryGroup> activeDirectoryGroups, bool shouldUpdateAd)
        {
            var user = unitOfWork.Repositories.Users.SingleOrDefault(x => x.ActiveDirectoryId == userId);

            if (user == null)
            {
                return;
            }

            foreach (var group in activeDirectoryGroups)
            {
                group.Users.Add(user);

                if (shouldUpdateAd)
                {
                    var activeDirectoryGroupChange = new ActiveDirectoryGroupChangeDto
                    {
                        GroupId = group.Id,
                        MembersToAddIds = new[] { user.Id }
                    };

                    try
                    {
                        var result = _activeDirectoryWriter.ModifyActiveDirectoryGroupUsers(activeDirectoryGroupChange);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error($"Error while adding user (ID = {user.Id}) to ActiveDirectory group", ex);
                    }
                }
            }

            unitOfWork.Commit();
        }

        private void RemoveUserFromGroupsInternal(IUnitOfWork<IAllocationDbScope> unitOfWork, Guid userId,
            IEnumerable<ActiveDirectoryGroup> activeDirectoryGroups, bool shouldUpdateAd)
        {
            var user = unitOfWork.Repositories.Users.SingleOrDefault(x => x.ActiveDirectoryId == userId);

            if (user == null)
            {
                return;
            }

            foreach (var group in activeDirectoryGroups)
            {
                group.Users.Remove(user);

                if (shouldUpdateAd)
                {
                    var activeDirectoryGroupChange = new ActiveDirectoryGroupChangeDto
                    {
                        GroupId = group.Id,
                        MembersToRemoveIds = new[] { user.Id }
                    };

                    try
                    {
                        var result = _activeDirectoryWriter.ModifyActiveDirectoryGroupUsers(activeDirectoryGroupChange);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error($"Error while removing user (ID = {user.Id}) from ActiveDirectory group", ex);
                    }
                }
            }

            unitOfWork.Commit();
        }

        private IEnumerable<ActiveDirectoryGroup> GetGroupsByActiveDirectoryNames(IUnitOfWork<IAllocationDbScope> unitOfWork,
            IEnumerable<string> activeDirectoriesNames)
        {
            return unitOfWork
                    .Repositories
                    .ActiveDirectoryGroups
                    .Where(ad => activeDirectoriesNames.Contains(ad.Name))
                    .ToList();
        }
    }
}
