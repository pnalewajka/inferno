﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{
    internal class WeeklyAllocationDataService
    {
        private readonly IDateRangeService _dateRangeService;
        private readonly IReadOnlyUnitOfWorkService<IAllocationDbScope> _unitOfWorkAllocationService;
        private readonly IEmployeeService _employeeService;
        private readonly IEmployeeAvailabilityDataService _employeeAvailabilityDataService;

        public WeeklyAllocationDataService(
            IDateRangeService dateRangeService,
            IReadOnlyUnitOfWorkService<IAllocationDbScope> unitOfWorkAllocationService,
            IEmployeeService employeeService,
            IEmployeeAvailabilityDataService employeeAvailabilityDataService)
        {
            _dateRangeService = dateRangeService;
            _unitOfWorkAllocationService = unitOfWorkAllocationService;
            _employeeService = employeeService;
            _employeeAvailabilityDataService = employeeAvailabilityDataService;
        }

        private AllocationCertainty? CalculateAllocationCertainty(IReadOnlyCollection<AllocationRequest> requests)
        {
            if (!requests.Any())
            {
                return null;
            }

            return requests.All(ar => ar.AllocationCertainty == AllocationCertainty.Certain)
                ? AllocationCertainty.Certain
                : AllocationCertainty.Planned;
        }

        public IDictionary<long, IReadOnlyCollection<EmployeeAllocationDto.EmployeeAllocationSingleRecordDto>> CalculateAllocations(IReadOnlyCollection<long> employeeIds, DateTime dateFrom, DateTime dateTo)
        {
            var resultDictionary = new Dictionary<long, IReadOnlyCollection<EmployeeAllocationDto.EmployeeAllocationSingleRecordDto>>();
            var weekRanges = _dateRangeService.GetAllWeeksInDateRanges(dateFrom, dateTo).OrderBy(w => w).ToList();

            if (!weekRanges.Any())
            {
                return employeeIds.Distinct().ToDictionary(
                        e => e,
                        e => new List<EmployeeAllocationDto.EmployeeAllocationSingleRecordDto>() as IReadOnlyCollection<EmployeeAllocationDto.EmployeeAllocationSingleRecordDto>);
            }

            var weekRangeFrom = weekRanges.First();
            var weekRangesTo = weekRanges.Last();

            var absencePeriodsDictionary = _employeeAvailabilityDataService.GetDetailedAbsencesForEmployees(employeeIds, dateFrom, dateTo).ToList();

            using (var unit = _unitOfWorkAllocationService.Create())
            {
                var allocationRequestsInProgress = unit.Repositories.AllocationRequests
                    .Where(ar => ar.AllocationRequestMetadata.ProcessingStatus == ProcessingStatus.Processing &&
                                 employeeIds.Contains(ar.EmployeeId))
                    .GroupBy(e => e.EmployeeId)
                    .ToDictionary(a => a.Key, a => a.ToList());

                var weekStatus = unit.Repositories.WeeklyAllocationStatus
                    .Where(w =>
                        employeeIds.Contains(w.EmployeeId) &&
                        w.Week >= weekRangeFrom && w.Week <= weekRangesTo
                        )
                    .Include($"{nameof(WeeklyAllocationStatus.Details)}.{nameof(WeeklyAllocationDetail.Project)}")
                    .ToList();

                var employeeDictionary =
                    weekStatus.GroupBy(
                        e =>
                            new EmployeeAllocationTimePeriodIdentifier
                            {
                                EmployeeId = e.EmployeeId,
                                AllocationPeriodStart = e.Week
                            }).ToDictionary(e => e.Key, e => e.ToList());

                var userEmployeeDictionary =
                    _employeeService.GetUserIdToEmployeeIdDictionary(
                        absencePeriodsDictionary
                            .Select(a => a.AbsenceRequest.AffectedUserId)
                            .Distinct()
                            .ToList());

                foreach (var employeeId in employeeIds)
                {
                    var allocations = new List<EmployeeAllocationDto.EmployeeAllocationSingleRecordDto>();

                    var employeeAllocationRequests = allocationRequestsInProgress.ContainsKey(employeeId) ? allocationRequestsInProgress[employeeId] : new List<AllocationRequest>();

                    foreach (var week in weekRanges)
                    {
                        var key = new EmployeeAllocationTimePeriodIdentifier
                        {
                            EmployeeId = employeeId,
                            AllocationPeriodStart = week
                        };

                        var weekStart = key.AllocationPeriodStart.Date;
                        var endWeek = weekStart.AddDays(6).Date;

                        var isAllocationRequestInProgress = employeeAllocationRequests.Any(ar => ar.StartDate <= endWeek && (!ar.EndDate.HasValue || ar.EndDate >= weekStart));

                        var currentWeekAllocationRequestsCertainty =
                            unit.Repositories.AllocationRequests.Where(
                                ar =>
                                    ar.EmployeeId == employeeId && ar.StartDate <= endWeek &&
                                    (!ar.EndDate.HasValue || ar.EndDate >= weekStart)).ToList();

                        var periodAbsenceRequests =
                            absencePeriodsDictionary
                                .Where(a => userEmployeeDictionary[a.AbsenceRequest.AffectedUserId] == employeeId)
                                .Where(DateRangeHelper.OverlapingDateRange<DetailedAbsenceDto>(
                                    a => a.AbsenceRequest.From,
                                    a => a.AbsenceRequest.To,
                                    weekStart,
                                    endWeek).Compile())
                                .ToList();

                        if (!employeeDictionary.ContainsKey(key))
                        {
                            allocations.Add(new EmployeeAllocationDto.EmployeeAllocationSingleRecordDto
                            {
                                AllocationPeriod = week,
                                Status = isAllocationRequestInProgress ? AllocationStatus.Processing : AllocationStatus.Unavailable,
                                ProjectAllocations = new EmployeeAllocationDto.ProjectAllocationDto[] { },
                                EmployeeAbsences = periodAbsenceRequests.Select(r => r.AbsenceRequest).ToList(),
                            });
                        }
                        else
                        {
                            var status = employeeDictionary[key].First();
                            status.AllocationStatus = isAllocationRequestInProgress ? AllocationStatus.Processing : status.AllocationStatus;

                            allocations.Add(new EmployeeAllocationDto.EmployeeAllocationSingleRecordDto
                            {
                                AllocationPeriod = week,
                                Status = status.AllocationStatus,
                                AvailableHours = status.AvailableHours,
                                ProjectAllocations = status.Details.Select(d =>
                                    new EmployeeAllocationDto.ProjectAllocationDto
                                    {
                                        HoursAllocated = d.Hours,
                                        ProjectColor = d.Project.ProjectSetup.Color,
                                        ProjectName = ProjectBusinessLogic.ClientProjectName.Call(d.Project),
                                        ProjectId = d.Project.Id,
                                        Certainty = CalculateAllocationCertainty(currentWeekAllocationRequestsCertainty.Where(ar =>
                                    ar.EmployeeId == employeeId && ar.StartDate <= week &&
                                    (!ar.EndDate.HasValue || ar.EndDate >= week) && ar.ProjectId == d.ProjectId).ToList())
                                    })
                                    .OrderBy(x => x.ProjectName)
                                    .ToArray(),
                                EmployeeAbsences = periodAbsenceRequests.Select(r => r.AbsenceRequest).ToList(),
                            });
                        }
                    }
                    resultDictionary.Add(employeeId, allocations.ToArray());
                }
            }

            return resultDictionary;
        }
    }
}
