﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{
    public class DetailedAbsenceDto
    {
        public AbsenceRequestDto AbsenceRequest { get; set; }

        public IEnumerable<DailyContractedHoursDto> DailyHours { get; set; }

        public bool IsHourlyReporting =>
            AbsenceRequest.From.StartOfDay() == AbsenceRequest.To.StartOfDay()
            && AbsenceRequest.Hours < (DailyHours.SingleOrDefault(d => d.Date == AbsenceRequest.From)?.Hours ?? 0);

        public decimal GetHoursForDay(DateTime day)
        {
            if (IsHourlyReporting)
            {
                return AbsenceRequest.Hours;
            }

            return DailyHours.SingleOrDefault(d => d.Date == day)?.Hours ?? 0;
        }
    }

    internal class EmployeeAvailabilityDataService : IEmployeeAvailabilityDataService
    {
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService;
        private readonly IClassMappingFactory _classMappingFactory;
        private readonly IEmploymentPeriodService _employmentPeriodService;

        public EmployeeAvailabilityDataService(
            IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
            IClassMappingFactory classMappingFactory,
            IEmploymentPeriodService employmentPeriodService)
        {
            _unitOfWorkService = unitOfWorkService;
            _classMappingFactory = classMappingFactory;
            _employmentPeriodService = employmentPeriodService;
        }

        public IEnumerable<DetailedAbsenceDto> GetDetailedAbsencesForEmployees(IReadOnlyCollection<long> employeeIds,
            DateTime dateFrom, DateTime dateTo)
        {
            var acceptableAbsenceStatuses = new[]
            {
                RequestStatus.Pending,
                RequestStatus.Approved,
                RequestStatus.Completed
            };

            var mapping = _classMappingFactory.CreateMapping<AbsenceRequest, AbsenceRequestDto>();

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var absenceRequests = unitOfWork.Repositories.AbsenceRequests.Include(x => x.Request)
                    .Where(
                        ar =>
                            ar.AffectedUser.Employees.Any(e => employeeIds.Contains(e.Id)) &&
                            acceptableAbsenceStatuses.Contains(ar.Request.Status))
                    .Where(DateRangeHelper.OverlapingDateRange<AbsenceRequest>(r => r.From, r => r.To, dateFrom.StartOfDay(), dateTo.EndOfDay()));

                var userContractedHours = absenceRequests.GroupBy(a => a.AffectedUserId).Select(g => new
                {
                    UserId = g.Key,
                    EmployeeId = g.FirstOrDefault().AffectedUser.Employees.Select(e => e.Id).FirstOrDefault(),
                    From = g.Min(x => x.From),
                    To = g.Max(x => x.To),
                }).ToDictionary(o => o.UserId, o => _employmentPeriodService.GetContractedHours(o.EmployeeId, o.From, o.To));

                return absenceRequests.ToList().Select(a => new DetailedAbsenceDto
                {
                    AbsenceRequest = mapping.CreateFromSource(a),
                    DailyHours = userContractedHours[a.AffectedUserId]
                        .Where(d => a.From.StartOfDay() <= d.Date && a.To.EndOfDay() >= d.Date)
                        .Where(d => d.Hours > 0)
                }).ToList();
            }
        }
    }
}
