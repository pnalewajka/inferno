﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.Dto;

namespace Smt.Atomic.Business.Allocation.Services
{
    internal class ProjectAcronymGeneratorService
    {
        private char[] AlphabetChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();

        private IList<string> AcronymsTaken { get; set; }

        internal ProjectAcronymGeneratorService(IReadOnlyCollection<string> acronymsTaken)
        {
            AcronymsTaken = acronymsTaken.ToList();
        }

        private char[] GetProjectNameChars(string projectName)
        {
            var projectNameFirstChars = projectName
                        .Split(' ', '/')
                        .Where(s => !string.IsNullOrEmpty(s))
                        .Select(w => w.First())
                        .Where(c => char.IsLetter(c) || char.IsNumber(c));

            var upperCaseChars = projectName.Where(char.IsUpper);

            return projectNameFirstChars.Concat(upperCaseChars).ToArray();
        }

        private string GenerateFromChars(char[] chars, int tryCount, int onChar = 3, int size = 4)
        {
            var baseChars = chars.Take(size).ToArray();
            var extraChars = chars.Skip(size).Take(chars.Length - size).Distinct().ToArray();

            if (tryCount == 0)
            {
                return string.Join("", baseChars).Trim().ToUpper();
            }

            if (extraChars.Length - 1 > tryCount)
            {
                var extraChar = extraChars[tryCount];
                baseChars[onChar > baseChars.Length - 1 ? baseChars.Length - 1 : onChar] = extraChar;

                var carsToJoin =
                    baseChars.Take(onChar - 1)
                    .Concat(new[] { extraChar })
                    .Concat(baseChars.Skip(onChar + 1).Take(baseChars.Count() - onChar + 1));

                return (string.Join("", carsToJoin).ToUpper());
            }

            return null;
        }

        public string TryUseAsNewAcronym(string newAcronym)
        {
            if (CanUseAsNewAcronym(newAcronym))
            {
                AcronymsTaken.Add(newAcronym);

                return newAcronym;
            }

            return null;
        }

        public bool CanUseAsNewAcronym(string newAcronym)
        {
            return !string.IsNullOrEmpty(newAcronym) && newAcronym.Length <= 4 && !AcronymsTaken.Contains(newAcronym);
        }

        private string TryGenerateAcronym(char[] projectChars, byte tryRun)
        {
            for (var i = 0; i < (projectChars.Length + AlphabetChars.Length); i++)
            {
                var newAcronym = GenerateFromChars(projectChars.Concat(AlphabetChars).ToArray(), i, tryRun);

                if (string.IsNullOrEmpty(newAcronym))
                {
                    return null;
                }

                newAcronym = TryUseAsNewAcronym(newAcronym);

                if (!string.IsNullOrEmpty(newAcronym))
                {
                    return newAcronym;
                }
            }

            return null;
        }

        public string GenerateAcronym(ProjectDto project)
        {
            var projectName = project.ClientProjectName
                ?? $"{project.ProjectShortName}/{project.ClientShortName}";

            var projectChars = GetProjectNameChars(projectName);
            var newAcronym = TryGenerateAcronym(projectChars, 3);

            if (string.IsNullOrEmpty(newAcronym))
            {
                newAcronym = TryGenerateAcronym(projectChars, 2);
            }

            return newAcronym;
        }
    }
}
