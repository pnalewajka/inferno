﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Smt.Atomic.Business.Allocation.Services
{
    public class AllocationRequestService : IAllocationRequestService
    {
        private readonly IReadOnlyUnitOfWorkService<IAllocationDbScope> _unitOfWorkAllocationService;
        private readonly IClassMappingFactory _classMappingFactory;

        public AllocationRequestService(
            IReadOnlyUnitOfWorkService<IAllocationDbScope> unitOfWorkAllocationService,
            IClassMappingFactory classMappingFactory)
        {
            _unitOfWorkAllocationService = unitOfWorkAllocationService;
            _classMappingFactory = classMappingFactory;
        }

        public IReadOnlyCollection<AllocationRequestDto> GetAllocationRequests(long employeeId,
            DateTime dateFrom, DateTime dateTo)
        {
            var mapping = _classMappingFactory.CreateMapping<AllocationRequest, AllocationRequestDto>();

            using (var unitofWork = _unitOfWorkAllocationService.Create())
            {
                IReadOnlyCollection<AllocationRequestDto> response = null;

                var allocationRequests = unitofWork
                    .Repositories
                    .AllocationRequests
                    .Where(a => a.EmployeeId == employeeId)
                    .Where(DateRangeHelper.OverlapingDateRange<AllocationRequest>(a =>
                        a.StartDate, a => a.EndDate ??
                        DateTime.MaxValue, dateFrom, dateTo))
                    .ToList();

                if (allocationRequests != null)
                {
                    response = mapping.CreateFromSource(allocationRequests).ToList();
                }

                return response;
            }
        }
    }
}
