﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Dto.StatusReviewEmail;
using Smt.Atomic.Business.Allocation.Enums;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Allocation.Interfaces.StatusReviewEmail;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Consents.Enums;
using Smt.Atomic.Business.Consents.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{
    public class StatusReviewEmailService : IStatusReviewEmailService
    {
        private class EmployeeWithWeekStatuses
        {
            public Employee Employee { get; set; }
            public IList<WeeklyAllocationStatus> WeeklyAllocationStatuses { get; set; }
        }

        const int DaysInWeek = 7;

        private readonly IUnitOfWorkService<IAllocationDbScope> _unitOfWorkService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly ITimeService _timeService;
        private readonly IDateRangeService _dateRangeService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly IConsentDataService _consentDataService;
        private readonly IEmployeeAvailabilityDataService _employeeAvailabilityDataService;
        private readonly IEmploymentPeriodService _employmentPeriodService;

        public StatusReviewEmailService(
            IUnitOfWorkService<IAllocationDbScope> unitOfWorkService,
            ISystemParameterService systemParameterService,
            ITimeService timeService,
            IDateRangeService dateRangeService,
            IMessageTemplateService messageTemplateService,
            IReliableEmailService reliableEmailService,
            IConsentDataService consentDataService,
            IEmployeeAvailabilityDataService employeeAvailabilityDataService,
            IEmploymentPeriodService employmentPeriodService)
        {
            _unitOfWorkService = unitOfWorkService;
            _systemParameterService = systemParameterService;
            _timeService = timeService;
            _dateRangeService = dateRangeService;
            _messageTemplateService = messageTemplateService;
            _reliableEmailService = reliableEmailService;
            _consentDataService = consentDataService;
            _employeeAvailabilityDataService = employeeAvailabilityDataService;
            _employmentPeriodService = employmentPeriodService;
        }

        public void PrepareFreeOrPlanned()
        {
            var currentDate = _timeService.GetCurrentDate();
            var periodInWeeks = _systemParameterService.GetParameter<int>(ParameterKeys.StatusReviewReportPeriodInWeeks);
            var range = _dateRangeService.GetAllWeeksInDateRanges(currentDate, currentDate.AddDays(periodInWeeks * DaysInWeek)).ToList();
            var from = range.First();
            var to = range.Last();
            var threshold = _systemParameterService.GetParameter<decimal>(ParameterKeys.StatusReviewReportMinimumAllocationThreshold);
            var serverAddress = _systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);

            IList<StatusReviewEmailDto<FreeOrPlannedEmployeeDto>> statusReviewReports;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var relevantEmployees =
                    unitOfWork.Repositories.Employees.Where(
                        e =>
                            e.IsProjectContributor
                            && e.WeeklyAllocationStatuses.Any(wa => wa.Week >= from && wa.Week <= to && wa.EmployeeStatus == EmployeeStatus.Active)
                            && (e.LineManagerId.HasValue || e.OrgUnit.EmployeeId.HasValue))
                        .ToList();

                if (!relevantEmployees.Any())
                {
                    return;
                }

                var preprocessedEmployees = ProcessFreeOrPlannedEmployees(relevantEmployees, from, to, threshold);
                statusReviewReports = GetAllManagersEmails(relevantEmployees, preprocessedEmployees, unitOfWork);
                statusReviewReports.ForEach(r => r.OrganizationUnits.ForEach(o => o.Employees.ForEach(e => e.RelevantWeekStatuses = MergeSimilarWeekStatuses(e.RelevantWeekStatuses))));
            }

            statusReviewReports.ForEach(r => r.ServerAddress = serverAddress);
            statusReviewReports.ForEach(r => r.From = from);
            statusReviewReports.ForEach(r => r.To = to);

            var emailMessages = GetEmailMessageDtos(FilterEmailsWithoutConsent(statusReviewReports, AllocationRequestEmailConsentEnum.FreeOrPlannedRaportEmail), TemplateCodes.AllocationStatusReviewEmailFreeOrPlannedSubject, TemplateCodes.AllocationStatusReviewEmailFreeOrPlannedBody);
            SendEmails(emailMessages);
        }

        public void PrepareNonAllocatedNewHires()
        {
            var currentDate = _timeService.GetCurrentDate();
            var range = _dateRangeService.GetAllWeeksInDateRanges(currentDate, currentDate.AddDays(2 * DaysInWeek)).ToList();
            var from = range.First();
            var to = range.Last();
            var serverAddress = _systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);

            IList<StatusReviewEmailDto<NonAllocatedEmployeeDto>> statusReviewReports;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var periodForCurrentDate = EmploymentPeriodBusinessLogic.ForDate.Parametrize(currentDate);

                var relevantEmployees =
                    unitOfWork.Repositories.Employees.Where(
                        e => e.IsProjectContributor
                            && e.WeeklyAllocationStatuses.Any(wa => wa.Week >= from && wa.Week <= to && wa.EmployeeStatus == EmployeeStatus.NewHire)
                            && (e.LineManagerId.HasValue || e.OrgUnit.EmployeeId.HasValue)
                            && e.EmploymentPeriods.AsQueryable().Any(periodForCurrentDate))
                        .ToList();

                if (!relevantEmployees.Any())
                {
                    return;
                }

                var preprocessedEmployees = ProcessNonAllocatedOrPlannedBeforeOneWeekNewHires(relevantEmployees, from, to);
                statusReviewReports = GetAllManagersEmails(relevantEmployees, preprocessedEmployees, unitOfWork);
            }

            statusReviewReports.ForEach(r => r.ServerAddress = serverAddress);
            statusReviewReports.ForEach(r => r.From = from);
            statusReviewReports.ForEach(r => r.To = to);

            var emailMessages = GetEmailMessageDtos(FilterEmailsWithoutConsent(statusReviewReports, AllocationRequestEmailConsentEnum.NonAllocatedNewHiresEmailRaportEmail), TemplateCodes.AllocationStatusReviewEmailNonAllocatedNewHiresSubject, TemplateCodes.AllocationStatusReviewEmailNonAllocatedNewHiresBody);
            SendEmails(emailMessages);
        }

        private IList<WeekStatus> MergeSimilarWeekStatuses(IList<WeekStatus> weekStatuses)
        {
            var mergedWeekStatuses = new List<WeekStatus>();

            foreach (var weekStatus in weekStatuses)
            {
                var last = mergedWeekStatuses.LastOrDefault();

                if (last != null && last.Percentage == weekStatus.Percentage)
                {
                    last.To = weekStatus.To;
                }
                else
                {
                    mergedWeekStatuses.Add(weekStatus);
                }
            }

            return mergedWeekStatuses;
        }

        private void SendEmails(IList<EmailMessageDto> emailMessages)
        {
            foreach (var emailMessage in emailMessages)
            {
                _reliableEmailService.EnqueueMessage(emailMessage);
            }
        }

        private IList<EmailMessageDto> GetEmailMessageDtos<TEmployeeDto>(IList<StatusReviewEmailDto<TEmployeeDto>> statusReviewReports, string subjectTemplateCode, string bodyTemplateCode) where TEmployeeDto : IEmployeeDto
        {
            var emailMessages = statusReviewReports
                .GroupBy(e => new { e.ManagerEmail, e.ManagerFullName })
                .Select(r => r.First())
                .Select(r =>
                {
                    var subject = _messageTemplateService.ResolveTemplate(subjectTemplateCode, null);
                    var body = _messageTemplateService.ResolveTemplate(bodyTemplateCode, r);

                    return new EmailMessageDto
                    {
                        IsHtml = body.IsHtml,
                        Recipients = new List<EmailRecipientDto> {new EmailRecipientDto(r.ManagerEmail, r.ManagerFullName)},
                        Subject = subject.Content,
                        Body = body.Content
                    };
                }).ToList();

            return emailMessages;
        }

        private IList<StatusReviewEmailDto<TEmployeeDto>> FilterEmailsWithoutConsent<TEmployeeDto>(
            IList<StatusReviewEmailDto<TEmployeeDto>> items, AllocationRequestEmailConsentEnum consentType)
        {
            var userIds = items.Where(i => i.ManagerUserId.HasValue).Select(i => i.ManagerUserId.Value).Distinct().ToList();

            var allowedConsents = _consentDataService.ExcludeUsersWithoutConsents(consentType, userIds).ToList();

            return items.Where(i => i.ManagerUserId.HasValue && allowedConsents.Contains(i.ManagerUserId.Value)).ToList();
        }

        private IList<StatusReviewEmailDto<TEmployeeDto>> GetAllManagersEmails<TEmployeeDto>(IList<Employee> relevantEmployees, IList<TEmployeeDto> preprocessedEmployees, IUnitOfWork<IAllocationDbScope> unitOfWork) where TEmployeeDto : IEmployeeDto
        {
            var preprocessedRelevantEmployees = relevantEmployees.Where(r => preprocessedEmployees.Any(p => p.Id == r.Id)).ToList();
            var preprocessedOrganizationUnits = ProcessOrganizationUnits<TEmployeeDto>(preprocessedRelevantEmployees);

            var lineManagersEmails = GetLineManagersEmails(preprocessedRelevantEmployees, preprocessedEmployees, preprocessedOrganizationUnits);
            var orgUnitManagersEmails = GetOrgUnitManagersEmails(preprocessedRelevantEmployees, preprocessedEmployees, preprocessedOrganizationUnits, unitOfWork);

            return lineManagersEmails.Concat(orgUnitManagersEmails).ToList();
        }

        private IEnumerable<StatusReviewEmailDto<TEmployeeDto>> GetLineManagersEmails<TEmployeeDto>(IList<Employee> employees, IList<TEmployeeDto> preprocessedEmployees,
            IDictionary<long, OrgUnitDto<TEmployeeDto>> preprocessedOrganizationUnits) where TEmployeeDto : IEmployeeDto
        {
            var lineManagerIds = GetLineManagerIds(employees);

            foreach (var lineManagerId in lineManagerIds)
            {
                var emailDto = GetLineManagerStatusReviewEmailDto(lineManagerId, employees, preprocessedEmployees, preprocessedOrganizationUnits);
                if (emailDto != null)
                {
                    yield return emailDto;
                }
            }
        }

        private IEnumerable<StatusReviewEmailDto<TEmployeeDto>> GetOrgUnitManagersEmails<TEmployeeDto>(IList<Employee> employees,
            IList<TEmployeeDto> preprocessedEmployees, IDictionary<long, OrgUnitDto<TEmployeeDto>> preprocessedOrganizationUnits, IUnitOfWork<IAllocationDbScope> unitOfWork) where TEmployeeDto : IEmployeeDto
        {
            var orgUnitManagerIds = GetOrgUnitManagerIds(employees, unitOfWork);

            foreach (var orgUnitManagerId in orgUnitManagerIds)
            {
                var emailDto = GetOrgUnitManagerStatusReviewEmailDto(orgUnitManagerId, employees, preprocessedEmployees, preprocessedOrganizationUnits, unitOfWork);
                if (emailDto != null)
                {
                    yield return emailDto;
                }
            }
        }

        private StatusReviewEmailDto<TEmployeeDto> GetStatusReviewEmailDto<TEmployeeDto>(Employee manager,
            IEnumerable<TEmployeeDto> employees, IDictionary<long, OrgUnitDto<TEmployeeDto>> orgUnits) where TEmployeeDto : IEmployeeDto
        {
            var employeeOrgUnits = new List<OrgUnitDto<TEmployeeDto>>();
            foreach (var orgUnit in employees.GroupBy(e => e.OrganizationUnitId))
            {
                var processedOrgUnit = orgUnits[orgUnit.Key];
                employeeOrgUnits.Add(new OrgUnitDto<TEmployeeDto>
                {
                    Id = processedOrgUnit.Id,
                    Name = processedOrgUnit.Name,
                    ParentOrgUnits = processedOrgUnit.ParentOrgUnits,
                    Employees = orgUnit.ToList()
                });
            }

            return new StatusReviewEmailDto<TEmployeeDto>
            {
                ManagerFullName = $"{manager.FirstName} {manager.LastName}",
                ManagerUserId = manager.UserId,
                ManagerEmail = manager.Email,
                OrganizationUnits = employeeOrgUnits,
            };
        }

        private StatusReviewEmailDto<TEmployeeDto> GetLineManagerStatusReviewEmailDto<TEmployeeDto>(
            long lineManagerId, IList<Employee> employees, IList<TEmployeeDto> preprocessedEmployees,
            IDictionary<long, OrgUnitDto<TEmployeeDto>> preprocessedOrganizationUnits)
            where TEmployeeDto : IEmployeeDto
        {
            var ownEmployees = GetLineManagerEmployees(employees, lineManagerId);
            if (!ownEmployees.Any())
            {
                return null;
            }

            var manager = ownEmployees.Select(m => m.LineManager).First();
            var preprocessedOwnEmployees = preprocessedEmployees.Where(e => ownEmployees.Any(o => o.Id == e.Id));
            var emailDto = GetStatusReviewEmailDto(manager, preprocessedOwnEmployees, preprocessedOrganizationUnits);
            return emailDto;
        }

        private StatusReviewEmailDto<TEmployeeDto> GetOrgUnitManagerStatusReviewEmailDto<TEmployeeDto>(
            long orgUnitManagerId, IList<Employee> employees, IList<TEmployeeDto> preprocessedEmployees,
            IDictionary<long, OrgUnitDto<TEmployeeDto>> preprocessedOrganizationUnits, IUnitOfWork<IAllocationDbScope> unitOfWork)
            where TEmployeeDto : IEmployeeDto
        {
            var ownEmployees = GetOrgUnitManagerEmployees(employees, orgUnitManagerId, unitOfWork);
            if (!ownEmployees.Any())
            {
                return null;
            }

            var manager = unitOfWork.Repositories.Employees.Single(e => e.Id == orgUnitManagerId);
            var preprocessedOwnEmployees = preprocessedEmployees.Where(e => ownEmployees.Any(o => o.Id == e.Id)).ToList();
            var emailDto = GetStatusReviewEmailDto(manager, preprocessedOwnEmployees, preprocessedOrganizationUnits);
            return emailDto;
        }

        private IList<FreeOrPlannedEmployeeDto> ProcessFreeOrPlannedEmployees(IList<Employee> employees, DateTime from, DateTime to, decimal threshold)
        {
            var employeeDtos = new List<FreeOrPlannedEmployeeDto>();

            var employeesWithWeeklyStatuses = GetEmployeesWithWeekStatuses(employees, from, to);
            var freeEmployees = GetFreeEmployees(employeesWithWeeklyStatuses, threshold).ToList();
            var plannedEmployees = GetPlannedEmployees(employeesWithWeeklyStatuses).ToList();

            if (freeEmployees.Any())
            {
                freeEmployees.ForEach(
                    e =>
                    {
                        if (e.WeeklyAllocationStatuses.Any())
                        {
                            employeeDtos.Add(GetStatusReviewEmailFreeOrPlannedEmployeeDto(e, StatusReviewEmailEmployeeStatus.Free, threshold));
                        }
                    });
            }

            if (plannedEmployees.Any())
            {
                plannedEmployees.ForEach(
                    e =>
                    {
                        if (e.WeeklyAllocationStatuses.Any())
                        {
                            employeeDtos.Add(GetStatusReviewEmailFreeOrPlannedEmployeeDto(e, StatusReviewEmailEmployeeStatus.Planned, threshold));
                        }
                    });
            }

            return employeeDtos;
        }

        private IList<NonAllocatedEmployeeDto> ProcessNonAllocatedOrPlannedBeforeOneWeekNewHires(IList<Employee> employees, DateTime from, DateTime to)
        {
            var employeeDtos = new List<NonAllocatedEmployeeDto>();

            var employeesWithWeeklyStatuses = GetEmployeesWithWeekStatuses(employees, from, to);
            var nonAllocatedEmployees = GetNonAllocatedEmployees(employeesWithWeeklyStatuses);
            var onlyPlannedEmployees = GetOnlyPlannedEmployeesOneWeekBeforeStartDate(employeesWithWeeklyStatuses, from, to);

            if (nonAllocatedEmployees.Any())
            {
                nonAllocatedEmployees.ForEach(
                    e =>
                    {
                        if (e.WeeklyAllocationStatuses.Any())
                        {
                            employeeDtos.Add(GetStatusReviewEmailNonAllocatedNewHireDto(e, StatusReviewEmailEmployeeStatus.NonAllocated));
                        }
                    });
            }

            if (onlyPlannedEmployees.Any())
            {
                onlyPlannedEmployees.ForEach(
                    e =>
                    {
                        if (e.WeeklyAllocationStatuses.Any())
                        {
                            employeeDtos.Add(GetStatusReviewEmailNonAllocatedNewHireDto(e, StatusReviewEmailEmployeeStatus.OnlyPlannedNewHire));
                        }
                    });
            }

            return employeeDtos;
        }

        private IList<UnderUtilizedEmployeeDto> ProcessUnderUtilizedEmployees(IList<Employee> employees, DateTime from, DateTime to)
        {
            var employeeDtos = new List<UnderUtilizedEmployeeDto>();

            var employeesWithWeeklyStatuses = GetEmployeesWithWeekStatuses(employees, from, to);
            var underUtilizedEmployees = GetUnderUtilizedEmployees(employeesWithWeeklyStatuses);

            if (underUtilizedEmployees.Any())
            {
                underUtilizedEmployees.ForEach(
                    e =>
                    {
                        if (e.WeeklyAllocationStatuses.Any())
                        {
                            employeeDtos.Add(GetStatusReviewEmailUnderUtilizedEmployeeDto(e, StatusReviewEmailEmployeeStatus.UnderUtilized));
                        }
                    });
            }

            return employeeDtos;
        }

        private IDictionary<long, OrgUnitDto<TEmployeeDto>> ProcessOrganizationUnits<TEmployeeDto>(IList<Employee> employees)
        {
            var statusReviewEmailOrgUnitDtos = new Dictionary<long, OrgUnitDto<TEmployeeDto>>();
            foreach (var employee in employees)
            {
                if (statusReviewEmailOrgUnitDtos.ContainsKey(employee.OrgUnitId))
                {
                    continue;
                }

                var currentOrgUnit = employee.OrgUnit;
                var orgUnit = new OrgUnitDto<TEmployeeDto>
                {
                    Id = currentOrgUnit.Id,
                    Name = currentOrgUnit.Name,
                    ParentOrgUnits = new List<SimpleOrgUnitDto>(),
                    Employees = new List<TEmployeeDto>()
                };
                currentOrgUnit = currentOrgUnit.Parent;

                while (currentOrgUnit?.ParentId != null)
                {
                    orgUnit.ParentOrgUnits.Insert(0, new SimpleOrgUnitDto
                    {
                        Id = currentOrgUnit.Id,
                        Name = currentOrgUnit.Name
                    });
                    currentOrgUnit = currentOrgUnit.Parent;
                }

                if (currentOrgUnit != null)
                {
                    orgUnit.ParentOrgUnits.Insert(0, new SimpleOrgUnitDto
                    {
                        Id = currentOrgUnit.Id,
                        Name = currentOrgUnit.Name
                    });
                }

                statusReviewEmailOrgUnitDtos.Add(employee.OrgUnitId, orgUnit);
            }

            return statusReviewEmailOrgUnitDtos;
        }

        private FreeOrPlannedEmployeeDto GetStatusReviewEmailFreeOrPlannedEmployeeDto(EmployeeWithWeekStatuses employee, StatusReviewEmailEmployeeStatus status, decimal threshold)
        {

            var freePeriodEndsOn = GetFreePeriodEndsOnFor(employee, status, threshold);
            var relevantWeekStatuses = GetRelevantWeekStatusesForFreeOrPlannedEmployee(employee, status, threshold);

            return new FreeOrPlannedEmployeeDto
            {
                Id = employee.Employee.Id,
                OrganizationUnitId = employee.Employee.OrgUnitId,
                FullName = $"{employee.Employee.FirstName} {employee.Employee.LastName}",
                JobProfiles = employee.Employee.JobProfiles.Select(jp => jp.Description).ToList(),
                JobMatrixLevels = employee.Employee.JobMatrixs.Select(jm => jm.Code).ToList(),
                Location = employee.Employee.Location.Name,
                Skills = employee.Employee.Resumes.SelectMany(r => r.ResumeTechnicalSkills).Select(s => s.TechnicalSkill.Name).ToList(),
                Status = status,
                RelevantWeekStatuses = relevantWeekStatuses,
                FreePeriodEndsOn = freePeriodEndsOn,
            };
        }

        private DateTime GetFreePeriodEndsOnFor(EmployeeWithWeekStatuses employee, StatusReviewEmailEmployeeStatus status, decimal threshold)
        {
            switch (status)
            {
                case StatusReviewEmailEmployeeStatus.Free:
                    {
                        return GetFreeUntil(employee.WeeklyAllocationStatuses, threshold);
                    }
                case StatusReviewEmailEmployeeStatus.Planned:
                    {
                        return GetPlannedUntil(employee.WeeklyAllocationStatuses);
                    }
            }

            return _timeService.GetCurrentDate();
        }

        private IList<WeekStatus> GetRelevantWeekStatusesForFreeOrPlannedEmployee(EmployeeWithWeekStatuses employee, StatusReviewEmailEmployeeStatus status, decimal threshold)
        {
            switch (status)
            {
                case StatusReviewEmailEmployeeStatus.Free:
                    {
                        return GetFreeWeeks(employee.WeeklyAllocationStatuses, threshold);
                    }
                case StatusReviewEmailEmployeeStatus.Planned:
                    {
                        return GetPlannedWeeks(employee.WeeklyAllocationStatuses);
                    }
            }

            return new List<WeekStatus>();
        }

        private NonAllocatedEmployeeDto GetStatusReviewEmailNonAllocatedNewHireDto(EmployeeWithWeekStatuses employee, StatusReviewEmailEmployeeStatus status)
        {
            return new NonAllocatedEmployeeDto
            {
                Id = employee.Employee.Id,
                OrganizationUnitId = employee.Employee.OrgUnitId,
                FullName = $"{employee.Employee.FirstName} {employee.Employee.LastName}",
                JobProfiles = employee.Employee.JobProfiles.Select(jp => jp.Description).ToList(),
                JobMatrixLevels = employee.Employee.JobMatrixs.Select(jm => jm.Code).ToList(),
                Location = employee.Employee.Location.Name,
                Skills = employee.Employee.Resumes.SelectMany(r => r.ResumeTechnicalSkills).Select(s => s.TechnicalSkill.Name).ToList(),
                Status = status,
            };
        }

        private UnderUtilizedEmployeeDto GetStatusReviewEmailUnderUtilizedEmployeeDto(EmployeeWithWeekStatuses employee, StatusReviewEmailEmployeeStatus status)
        {
            return new UnderUtilizedEmployeeDto
            {
                Id = employee.Employee.Id,
                OrganizationUnitId = employee.Employee.OrgUnitId,
                FullName = $"{employee.Employee.FirstName} {employee.Employee.LastName}",
                JobProfiles = employee.Employee.JobProfiles.Select(jp => jp.Description).ToList(),
                JobMatrixLevels = employee.Employee.JobMatrixs.Select(jm => jm.Code).ToList(),
                Location = employee.Employee.Location.Name,
                Skills = employee.Employee.Resumes.SelectMany(r => r.ResumeTechnicalSkills).Select(s => s.TechnicalSkill.Name).ToList(),
                Status = status,
                RelevantWeekStatuses = GetUnderUtilizedWeeks(employee.WeeklyAllocationStatuses),
            };
        }

        private IList<WeekStatus> GetFreeWeeks(IList<WeeklyAllocationStatus> weeklyAllocationStatuses, decimal threshold)
        {
            return
                weeklyAllocationStatuses.Where(w => w.AllocatedHours + w.PlannedHours < w.AvailableHours * (1m - threshold))
                    .Select(e => new WeekStatus
                    {
                        From = e.Week,
                        To = e.Week.GetNextDayOfWeek(DayOfWeek.Friday),
                        Percentage = GetFreeTimePercentage(e),
                    }).ToList();
        }

        private IList<WeekStatus> GetPlannedWeeks(IList<WeeklyAllocationStatus> weeklyAllocationStatuses)
        {
            return
                weeklyAllocationStatuses.Where(w => w.PlannedHours > 0).Select(e => new WeekStatus
                {
                    From = e.Week,
                    To = e.Week.GetNextDayOfWeek(DayOfWeek.Friday),
                    Percentage = GetPlannedTimePercentage(e),
                }).ToList();
        }

        private IList<WeekStatus> GetUnderUtilizedWeeks(IList<WeeklyAllocationStatus> weeklyAllocationStatuses)
        {
            return
                weeklyAllocationStatuses.Where(w => GetFreeTimePercentage(w) > 0)
                    .Select(e => new WeekStatus
                    {
                        From = e.Week,
                        To = e.Week.GetNextDayOfWeek(DayOfWeek.Friday),
                        Percentage = 100 - GetFreeTimePercentage(e),
                    }).ToList();
        }

        private DateTime GetFreeUntil(IList<WeeklyAllocationStatus> weeklyAllocationStatuses, decimal threshold)
        {
            return weeklyAllocationStatuses.Last(w => w.AllocatedHours + w.PlannedHours < w.AvailableHours * (1m - threshold)).Week;
        }

        private DateTime GetPlannedUntil(IList<WeeklyAllocationStatus> weeklyAllocationStatuses)
        {
            return weeklyAllocationStatuses.Last(w => w.PlannedHours > 0).Week.GetNextDayOfWeek(DayOfWeek.Friday);
        }

        private decimal GetFreeTimePercentage(WeeklyAllocationStatus weeklyAllocationStatus)
        {
            if (weeklyAllocationStatus.AvailableHours == 0)
            {
                return 100m;
            }

            return (weeklyAllocationStatus.AvailableHours - (weeklyAllocationStatus.AllocatedHours + weeklyAllocationStatus.PlannedHours)) * 100m / weeklyAllocationStatus.AvailableHours;
        }

        private decimal GetPlannedTimePercentage(WeeklyAllocationStatus weeklyAllocationStatus)
        {
            return weeklyAllocationStatus.PlannedHours * 100m / weeklyAllocationStatus.AvailableHours;
        }

        private HashSet<long> GetLineManagerIds(IList<Employee> relevantEmployees)
        {
            return relevantEmployees.Where(e => e.LineManagerId.HasValue).Select(e => e.LineManagerId.Value).ToHashSet();
        }

        private HashSet<long> GetOrgUnitManagerIds(IList<Employee> relevantEmployees,
            IUnitOfWork<IAllocationDbScope> unitOfWork)
        {
            return unitOfWork.Repositories.OrgUnits.ToList().Where(
                o => o.EmployeeId.HasValue && relevantEmployees.Any(e => e.OrgUnit.Path.StartsWith(o.Path)))
                .Select(o => o.EmployeeId.Value).ToHashSet();
        }

        private IList<Employee> GetLineManagerEmployees(IList<Employee> relevantEmployees, long lineManagerId)
        {
            return relevantEmployees.Where(e => e.LineManagerId == lineManagerId).ToList();
        }

        private IList<Employee> GetOrgUnitManagerEmployees(IList<Employee> relevantEmployees, long orgUnitManagerId, IUnitOfWork<IAllocationDbScope> unitOfWork)
        {
            var ownOrgUnits = unitOfWork.Repositories.OrgUnits.Where(o => o.EmployeeId == orgUnitManagerId).ToList();
            return relevantEmployees.Where(e => ownOrgUnits.Any(o => e.OrgUnit.Path.StartsWith(o.Path))).ToList();
        }

        private IList<EmployeeWithWeekStatuses> GetEmployeesWithWeekStatuses(IList<Employee> relevantEmployees, DateTime from, DateTime to)
        {
            var absencesInPeriod = _employeeAvailabilityDataService
                .GetDetailedAbsencesForEmployees(relevantEmployees.Select(e => e.Id).ToList(), from, to)
                .GroupBy(a => a.AbsenceRequest.AffectedUserId)
                .ToDictionary(g => g.Key, g => g.ToList());

            return relevantEmployees.Select(e => new EmployeeWithWeekStatuses
            {
                Employee = e,
                WeeklyAllocationStatuses = e.WeeklyAllocationStatuses
                    .Where(w => w.Week >= from && w.Week <= to).OrderBy(w => w.Week)
                    .Select(w => AddAbsenceInfo(e, w, e.UserId.HasValue && absencesInPeriod.ContainsKey(e.UserId.Value)
                        ? absencesInPeriod[e.UserId.Value]
                        : new List<DetailedAbsenceDto>())).ToList()
            }).ToList();
        }

        private WeeklyAllocationStatus AddAbsenceInfo(Employee employee, WeeklyAllocationStatus weeklyStatus, IEnumerable<DetailedAbsenceDto> absences)
        {
            var weekEnd = weeklyStatus.Week.GetNextDayOfWeek(DayOfWeek.Sunday).EndOfDay();

            var dailyAllocationStatuses = employee.AllocationDailyRecords
                .Where(d => d.Day >= weeklyStatus.Week && d.Day <= weekEnd)
                .ToList();

            var contractedHoursList = new Lazy<IEnumerable<DailyContractedHoursDto>>(
                () => _employmentPeriodService.GetContractedHours(employee.Id, weeklyStatus.Week, weekEnd));

            foreach (var absence in absences)
            {
                var absenceRequest = absence.AbsenceRequest;

                foreach (var dayAbsence in absence.DailyHours.Where(dh => dh.Date >= weeklyStatus.Week && dh.Date <= weekEnd).ToList())
                {
                    var dayAllocations = dailyAllocationStatuses.Where(d => d.Day == dayAbsence.Date);

                    if (!dayAllocations.Any())
                    {
                        weeklyStatus.AvailableHours -= absence.GetHoursForDay(dayAbsence.Date);
                        continue;
                    }

                    var dayAllocationHours = dayAllocations.Sum(d => d.Hours);

                    if (!absence.IsHourlyReporting)
                    {
                        var contractedHours = absence.GetHoursForDay(dayAbsence.Date);

                        if (dayAllocationHours < contractedHours)
                        {
                            weeklyStatus.AvailableHours -= contractedHours - dayAllocationHours; // Fill the hour difference
                        }
                    }
                    else
                    {
                        var contractedHours = contractedHoursList.Value.SingleOrDefault(d => d.Date == dayAbsence.Date)?.Hours;

                        if (contractedHours != null)
                        {
                            if (dayAllocationHours + dayAbsence.Hours < contractedHours.Value)
                            {
                                weeklyStatus.AvailableHours -= dayAbsence.Hours;
                            }
                            else
                            {
                                weeklyStatus.AvailableHours -= contractedHours.Value - dayAllocationHours;
                            }
                        }
                    }
                }
            }

            return weeklyStatus;
        }

        private IList<EmployeeWithWeekStatuses> GetFreeEmployees(IList<EmployeeWithWeekStatuses> relevantEmployees, decimal threshold)
        {
            return
                relevantEmployees.Where(
                    e =>
                        e.WeeklyAllocationStatuses.All(
                            w => w.AllocatedHours + w.PlannedHours < w.AvailableHours * (1m - threshold))).ToList();
        }

        private IList<EmployeeWithWeekStatuses> GetPlannedEmployees(IList<EmployeeWithWeekStatuses> relevantEmployees)
        {
            return
                relevantEmployees.Where(
                    e =>
                        e.WeeklyAllocationStatuses.Any(w => w.PlannedHours > 0)).ToList();
        }

        private IList<EmployeeWithWeekStatuses> GetNonAllocatedEmployees(IList<EmployeeWithWeekStatuses> relevantEmployees)
        {
            return
                relevantEmployees.Where(
                    e => !e.WeeklyAllocationStatuses.Any(
                        w => e.Employee.AllocationRequests.Any(r => r.StartDate <= w.Week && r.EndDate >= w.Week)
                    )).ToList();
        }

        private IList<EmployeeWithWeekStatuses> GetOnlyPlannedEmployeesOneWeekBeforeStartDate(
            IList<EmployeeWithWeekStatuses> relevantEmployees, DateTime currentWeek, DateTime to)
        {
            return
                relevantEmployees.Where(
                    e => e.WeeklyAllocationStatuses.Any(
                        w => e.Employee.EmploymentPeriods.Any(p => p.StartDate >= currentWeek && p.StartDate <= to
                                                                   && e.Employee.AllocationRequests.Any(
                                                                       r =>
                                                                           r.StartDate <= w.Week
                                                                           && (!r.EndDate.HasValue || r.EndDate >= w.Week)
                                                                           && w.AllocationStatus == AllocationStatus.Planned)))).ToList();
        }

        private IList<EmployeeWithWeekStatuses> GetUnderUtilizedEmployees(
            IList<EmployeeWithWeekStatuses> relevantEmployees)
        {
            return relevantEmployees.Where(e => e.WeeklyAllocationStatuses.Any(w => GetFreeTimePercentage(w) > 0)).ToList();
        }
    }
}