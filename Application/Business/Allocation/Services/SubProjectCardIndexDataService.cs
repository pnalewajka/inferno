﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Allocation.Resources;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{
    public class SubProjectCardIndexDataService : CardIndexDataService<SubProjectDto, Project, IAllocationDbScope>, ISubProjectCardIndexDataService
    {
        private readonly IReadOnlyUnitOfWorkService<IAllocationDbScope> _unitOfWorkAllocationService;
        private readonly ITimeService _timeService;
        private readonly IProjectService _projectService;

        public SubProjectCardIndexDataService(
            ICardIndexServiceDependencies<IAllocationDbScope> dependencies,
            IReadOnlyUnitOfWorkService<IAllocationDbScope> unitOfWorkAllocationService,
            IProjectService projectService,
            IOrgUnitService orgUnitService)
            : base(dependencies)
        {
            _timeService = dependencies.TimeService;
            _unitOfWorkAllocationService = unitOfWorkAllocationService;
            _projectService = projectService;
        }

        protected override IEnumerable<BusinessLogic<Project, int>> GetSearchRelevanceFactors(QueryCriteria criteria)
        {
            yield return DynamicQueryHelper.CreateRelevanceFactor<Project>(
                score: 2,
                predicate: p => (p.SubProjectShortName != null && p.SubProjectShortName.StartsWith(criteria.SearchPhrase)));

            yield return DynamicQueryHelper.CreateRelevanceFactor<Project>(
                score: 1,
                predicate: p => (p.ProjectSetup.ProjectShortName.StartsWith(criteria.SearchPhrase)));
        }

        protected override IEnumerable<Expression<Func<Project, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            if (searchCriteria.Includes(SearchAreaCodes.ProjectAcronym))
            {
                yield return u => u.Acronym;
            }

            if (searchCriteria.Includes(SearchAreaCodes.ProjectShortName))
            {
                yield return u => u.SubProjectShortName;
            }

            if (searchCriteria.Includes(SearchAreaCodes.Description))
            {
                yield return u => u.Description;
            }

            if (searchCriteria.Includes(SearchAreaCodes.JiraKey))
            {
                yield return u => u.JiraKey;
            }

            if (searchCriteria.Includes(SearchAreaCodes.Apn))
            {
                yield return u => u.Apn;
            }

            if (searchCriteria.Includes(SearchAreaCodes.KupferwerkProjectId))
            {
                yield return u => u.KupferwerkProjectId;
            }

            if (searchCriteria.Includes(SearchAreaCodes.Region))
            {
                yield return u => u.ProjectSetup.Region;
            }
        }

        public override SubProjectDto GetDefaultNewRecord()
        {
            var newRecord = base.GetDefaultNewRecord();

            newRecord.IsMainProject = false;
            newRecord.StartDate = _timeService.GetCurrentDate();
            newRecord.JiraIssueToTimeReportRowMapperId = _projectService.GetDefaultJiraIssueToTimeReportRowMapperId();
            
            return newRecord;
        }

        protected override void InitializeNavigationProperties(Project entity)
        {
            base.InitializeNavigationProperties(entity);

            if (entity.ProjectSetup == null)
            {
                entity.ProjectSetup = new ProjectSetup();
            }
        }

        public override IQueryable<Project> ApplyContextFiltering(IQueryable<Project> records, object context)
        {
            var parentIdContext = (ParentIdContext)context;

            records = records.Where(r => r.ProjectSetup.Projects.Any(p => p.Id == parentIdContext.ParentId));
            records = records.Where(ProjectBusinessLogic.IsSubProject);

            return base.ApplyContextFiltering(records, context);
        }

        public override void SetContext(Project entity, object context)
        {
            var parentIdContext = (ParentIdContext)context;
            var setupId = _projectService.GetSetupIdByProjectId(parentIdContext.ParentId);

            entity.ProjectSetupId = setupId;
        }

        protected override NamedFilters<Project> NamedFilters => new NamedFilters<Project>();

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        protected override IQueryable<Project> ConfigureIncludes(IQueryable<Project> sourceQueryable)
        {
            return sourceQueryable
                .Include(x => x.TimeReportAcceptingPersons)
                .Include(x => x.Attributes);
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<Project, SubProjectDto, IAllocationDbScope> eventArgs)
        {
            eventArgs.InputEntity.IsMainProject = false;

            eventArgs.InputEntity.TimeReportAcceptingPersons = EntityMergingService.AttachEntities(eventArgs.UnitOfWork, eventArgs.InputEntity.TimeReportAcceptingPersons);
            eventArgs.InputEntity.Attributes = EntityMergingService.AttachEntities(eventArgs.UnitOfWork, eventArgs.InputEntity.Attributes);
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<Project, SubProjectDto, IAllocationDbScope> eventArgs)
        {
            if (eventArgs.DbEntity.IsMainProject)
            {
                throw new NotSupportedException("Trying to edit a main project");
            }

            eventArgs.InputDto.IsMainProject = eventArgs.InputEntity.IsMainProject = eventArgs.DbEntity.IsMainProject;
            eventArgs.InputDto.SetupId = eventArgs.InputEntity.ProjectSetupId = eventArgs.DbEntity.ProjectSetupId;

            // Block setting StartsOn if there is past TimeReportRows without picked attributes
            // (only not accepted ones, for legacy data support)
            if (eventArgs.InputDto.ReportingStartsOn.HasValue
                && eventArgs.DbEntity.ReportingStartsOn != eventArgs.InputDto.ReportingStartsOn
                && eventArgs.InputDto.AttributesIds.Any())
            {
                var empoyeesWithInvalidRows = eventArgs.DbEntity.TimeReportRows
                    .Where(r => r.Status != TimeReportStatus.Accepted
                        && r.DailyEntries.Any(d => d.Day < eventArgs.InputDto.ReportingStartsOn.Value && d.Hours != 0)
                        && r.AttributeValues.Count != eventArgs.DbEntity.Attributes.Count)
                    .Select(r => new KeyValuePair<string, DateTime>(
                        r.TimeReport.Employee.DisplayName,
                        new DateTime(r.TimeReport.Year, r.TimeReport.Month, 1)))
                    .ToHashSet();

                foreach (var error in empoyeesWithInvalidRows)
                {
                    eventArgs.Alerts.Add(new AlertDto
                    {
                        Message = string.Format(AlertResources.ErrorReportingStartsOnNotSetAttributes, error.Key, error.Value.ToShortDateString()),
                        Type = AlertType.Error
                    });
                }
            }

            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.TimeReportAcceptingPersons, eventArgs.InputEntity.TimeReportAcceptingPersons);
            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.Attributes, eventArgs.InputEntity.Attributes);
        }
        
        protected override void OnRecordDeleting(RecordDeletingEventArgs<Project, IAllocationDbScope> eventArgs)
        {
            using (var unitOfWorkAllocationService = _unitOfWorkAllocationService.Create())
            {
                if (unitOfWorkAllocationService.Repositories.AllocationDailyRecords.Any(x => x.ProjectId == eventArgs.DeletingRecord.Id)
                    || unitOfWorkAllocationService.Repositories.AllocationRequests.Any(x => x.ProjectId == eventArgs.DeletingRecord.Id)
                    || unitOfWorkAllocationService.Repositories.StaffingDemands.Any(x => x.ProjectId == eventArgs.DeletingRecord.Id))
                {
                    eventArgs.Alerts.Add(new AlertDto
                    {
                        Message = string.Format(AlertResources.ProjectDeletingAlert, eventArgs.DeletingRecord.JiraIssueKey),
                        Type = AlertType.Error,
                    });
                }
            }
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<Project> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var direction = sortingCriterion.GetSortingDirection();

            switch (sortingCriterion.GetSortingColumnName())
            {
                case nameof(ProjectDto.IsJiraProject):
                {
                    orderedQueryBuilder.ApplySortingKey(r => r.JiraIssueKey, direction);
                    break;
                }

                case nameof(ProjectDto.ProjectName):
                {
                    orderedQueryBuilder.ApplySortingKey(ProjectBusinessLogic.ProjectName.AsExpression(), direction);
                    break;
                }

                default:
                {
                    base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
                    break;
                }
            }
        }
    }
}
