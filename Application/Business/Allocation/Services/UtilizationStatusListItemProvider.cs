﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Business.Allocation.Services
{
    public class AllocationStatusListItemProvider : IListItemProvider
    {
        public IEnumerable<ListItem> GetItems()
        {
            var allowedValues = new[]
            {
                AllocationStatus.Allocated,
                AllocationStatus.Planned,
                AllocationStatus.Free,
                AllocationStatus.UnderAllocated, 
                AllocationStatus.UnderPlanned, 
                AllocationStatus.AlmostFree,
            };

            return allowedValues.Select(v => new ListItem
            {
                Id = (long)v,
                DisplayName = v.GetDescription()
            });
        }
    }
}