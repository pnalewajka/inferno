﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Castle.Core.Logging;
using Smt.Atomic.Business.ActiveDirectory.Dto;
using Smt.Atomic.Business.ActiveDirectory.Interfaces;
using Smt.Atomic.Business.Allocation.BusinessEvents;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Helpers;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Allocation.Resources;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Dto;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{
    public class EmploymentPeriodCardIndexDataService : CardIndexDataService<EmploymentPeriodDto, EmploymentPeriod, IAllocationDbScope>, IEmploymentPeriodCardIndexDataService
    {
        private readonly ILogger _logger;
        private readonly IActiveDirectoryWriter _activeDirectoryWriter;
        private readonly IEmploymentPeriodService _employmentPeriodService;
        private readonly ITimeService _timeService;
        private readonly IBusinessEventPublisher _businessEventPublisher;
        private readonly IUnitOfWorkService<IMeetMeDbScope> _unitOfWorkService;
        private readonly ISystemParameterService _systemParameterService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public EmploymentPeriodCardIndexDataService(ICardIndexServiceDependencies<IAllocationDbScope> dependencies,
            IEmploymentPeriodService employmentPeriodService,
            IActiveDirectoryWriter activeDirectoryWriter,
            ILogger logger,
            IUnitOfWorkService<IMeetMeDbScope> unitOfWorkService,
            ISystemParameterService systemParameterService)
            : base(dependencies)
        {
            _logger = logger;
            _activeDirectoryWriter = activeDirectoryWriter;
            _employmentPeriodService = employmentPeriodService;
            _timeService = dependencies.TimeService;
            _businessEventPublisher = dependencies.BusinessEventPublisher;
            _unitOfWorkService = unitOfWorkService;
            _systemParameterService = systemParameterService;
        }

        protected override IEnumerable<BusinessEvent> GetAddRecordEvents(AddRecordResult result, EmploymentPeriodDto record)
        {
            var events = base.GetAddRecordEvents(result, record);

            if (result.IsSuccessful)
            {
                events = events.Concat(new BusinessEvent[]
                {
                    AllocationHelper.CreateEventFromEmploymentPeriod(record),
                    new EmploymentPeriodChangedBusinessEvent
                    {
                        EmployeeId = record.EmployeeId,
                        From = record.StartDate,
                        To = record.EndDate
                    }
                });
            }

            return events;
        }

        protected override IEnumerable<BusinessEvent> GetEditRecordEvents(
            EditRecordResult result, EmploymentPeriodDto newRecord, EmploymentPeriodDto originalRecord)
        {
            var events = base.GetEditRecordEvents(result, newRecord, originalRecord);

            if (result.IsSuccessful)
            {
                events = events.Concat(new BusinessEvent[]
                {
                    AllocationHelper.CreateEventFromEmploymentPeriods(originalRecord, newRecord),
                    new EmploymentPeriodChangedBusinessEvent
                    {
                        EmployeeId = originalRecord.EmployeeId,
                        From = originalRecord.StartDate < newRecord.StartDate ? originalRecord.StartDate : newRecord.StartDate,
                        To = GetLaterDate(originalRecord.EndDate, newRecord.EndDate)
                    }
                });
            }

            return events;
        }

        protected override IEnumerable<BusinessEvent> GetDeleteRecordEvents(DeleteRecordsResult result, EmploymentPeriodDto record)
        {
            var events = base.GetDeleteRecordEvents(result, record);

            if (result.IsSuccessful)
            {
                events = events.Concat(new BusinessEvent[]
                {
                    AllocationHelper.CreateEventFromEmploymentPeriod(record),
                    new EmploymentPeriodChangedBusinessEvent
                    {
                        EmployeeId = record.EmployeeId,
                        From = record.StartDate,
                        To = record.EndDate
                    }
                });
            }

            return events;
        }

        protected override NamedFilters<EmploymentPeriod> NamedFilters
        {
            get
            {
                return new NamedFilters<EmploymentPeriod>(new[]
                {
                    new NamedDtoFilter<EmploymentPeriod, EmploymentPeriodDateFilterDto>(
                        "EmploymentDateRange",
                        (ep, dto) => !(ep.EndDate < dto.From || ep.StartDate > dto.To)
                    )
                });
            }
        }

        protected override IEnumerable<Expression<Func<EmploymentPeriod, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return a => a.Employee.FirstName;
            yield return a => a.Employee.LastName;
            yield return a => a.Employee.Email;
            yield return u => u.Employee.Location.Name;
            yield return u => u.Employee.LineManager.LastName;
            yield return u => u.ContractType.EmploymentType;
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<EmploymentPeriod, IAllocationDbScope> eventArgs)
        {
            var validateResult = _employmentPeriodService.ValidateEmploymentPeriodDeleting(eventArgs.DeletingRecord.Id);

            if (validateResult.Type != AlertType.Error)
            {
                base.OnRecordDeleting(eventArgs);
            }
            else
            {
                eventArgs.Canceled = true;
                eventArgs.Alerts.Add(validateResult);
            }
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<EmploymentPeriod, EmploymentPeriodDto, IAllocationDbScope> eventArgs)
        {
            var validationResults = _employmentPeriodService.ValidateEmploymentPeriodEditing(eventArgs.DbEntity, eventArgs.InputDto);

            eventArgs.Alerts.AddRange(validationResults);

            if (validationResults.Any(r => r.Type == AlertType.Error))
            {
                eventArgs.Canceled = true;
            }
            else
            {
                base.OnRecordEditing(eventArgs);
                UpdateActiveDirectoryUser(eventArgs, eventArgs.UnitOfWork, eventArgs.InputDto);
            }
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<EmploymentPeriod, EmploymentPeriodDto> recordEditedEventArgs)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employeeMeetings = unitOfWork.Repositories.Meetings.Where(t => t.EmployeeId == recordEditedEventArgs.InputDto.EmployeeId).ToList();

                if (employeeMeetings.Count == 1
                    && employeeMeetings.First().Status == MeetingStatus.Pending)
                {
                    var firstMeeting = employeeMeetings.First();
                    var interval = _systemParameterService.GetParameter<int>(ParameterKeys.FirstMeetMeWeeksInterval);

                    firstMeeting.MeetingDueDate = DateHelper.AddWeeks(recordEditedEventArgs.InputDto.StartDate, interval);

                    unitOfWork.Commit();
                }
            }

            base.OnRecordEdited(recordEditedEventArgs);
        }

        public override IQueryable<EmploymentPeriod> ApplyContextFiltering(IQueryable<EmploymentPeriod> records, object context)
        {
            if (context != null)
            {
                var parentIdContext = (ParentIdContext)context;
                return records.Where(e => e.EmployeeId == parentIdContext.ParentId);
            }

            return base.ApplyContextFiltering(records, context);
        }
        protected override void OnRecordAdding(RecordAddingEventArgs<EmploymentPeriod, EmploymentPeriodDto, IAllocationDbScope> eventArgs)
        {
            var validationResults = _employmentPeriodService.ValidateEmploymentPeriodAdding(eventArgs.InputDto);

            if (validationResults.Any(r => r.Type == AlertType.Error))
            {
                eventArgs.Canceled = true;
                eventArgs.Alerts.AddRange(validationResults);
            }
            else
            {
                base.OnRecordAdding(eventArgs);

                var inputDto = eventArgs.InputDto;

                if (inputDto.ClosePreviousPeriod)
                {
                    CloseOverlappingEmploymentPeriods(inputDto, eventArgs.UnitOfWork.Repositories.EmploymentPeriods);
                }

                UpdateActiveDirectoryUser(eventArgs, eventArgs.UnitOfWork, eventArgs.InputDto);
            }
        }

        private void CloseOverlappingEmploymentPeriods(EmploymentPeriodDto inputDto, IRepository<EmploymentPeriod> employmentPeriods)
        {
            var periodsToClose = employmentPeriods.Where(p => EmploymentPeriodBusinessLogic.OverlappingPeriodsForEmployee.Call(p, inputDto.EmployeeId, inputDto.StartDate, inputDto.EndDate ?? DateTime.MaxValue));

            if (periodsToClose.Any(p => p.StartDate >= inputDto.StartDate))
            {
                throw new InvalidOperationException("Invalid configuration, cannot close some employment periods");
            }

            foreach (var periodToClose in periodsToClose)
            {
                periodToClose.EndDate = inputDto.StartDate.AddDays(-1);

                var employmentPeriodEvent = new EmploymentPeriodChangedBusinessEvent
                {
                    EmployeeId = periodToClose.EmployeeId,
                    From = periodToClose.StartDate,
                    To = periodToClose.EndDate
                };

                _businessEventPublisher.PublishBusinessEvent(employmentPeriodEvent);
            }
        }

        private void UpdateActiveDirectoryUser(CardIndexEventArgs eventArgs, IUnitOfWork<IAllocationDbScope> unitOfWork, EmploymentPeriodDto employmentPeriodDto)
        {
            var employmentPeriods = unitOfWork.Repositories.EmploymentPeriods
                .Where(p => p.EmployeeId == employmentPeriodDto.EmployeeId && p.Id != employmentPeriodDto.Id)
                .Select(p => new { p.StartDate, p.EndDate, p.ContractTypeId })
                .AsEnumerable()
                .Concat(new[] { new { employmentPeriodDto.StartDate, employmentPeriodDto.EndDate, employmentPeriodDto.ContractTypeId } })
                .OrderBy(p => p.StartDate)
                .ToList();

            var oldestEmploymentPeriod = employmentPeriods.First();
            var latestEmploymentPeriod = employmentPeriods.Last();

            try
            {
                var activeDirectoryUserChangeDescription = new ActiveDirectoryUserChangeDescriptionDto
                {
                    EmployeeId = employmentPeriodDto.EmployeeId,
                    StartDate = oldestEmploymentPeriod.StartDate,
                    EndDate = latestEmploymentPeriod.EndDate,
                    ContractTypeId = latestEmploymentPeriod.ContractTypeId
                };

                var result = _activeDirectoryWriter.ModifyActiveDirectoryUser(activeDirectoryUserChangeDescription);

                if (result.Alerts.Any())
                {
                    eventArgs.Alerts.AddRange(result.Alerts);
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Modify ActiveDirectory user", ex);
                eventArgs.AddError(AlertResources.ProblemWithUpdateActiveDirectory);
            }
        }

        protected override IQueryable<EmploymentPeriod> ApplyOrderByCriteria(IQueryable<EmploymentPeriod> records, QueryCriteria criteria)
        {
            return criteria.OrderBy.IsNullOrEmpty() ? records.OrderByDescending(r => r.StartDate) : base.ApplyOrderByCriteria(records, criteria);
        }

        private static DateTime? GetLaterDate(DateTime? firstDate, DateTime? secondDate)
        {
            return firstDate.HasValue && secondDate.HasValue
                ? (firstDate.Value > secondDate.Value ? firstDate : secondDate)
                : null;
        }
    }
}
