﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Castle.Core.Logging;
using Smt.Atomic.Business.Allocation.BusinessEvents;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Enums;
using Smt.Atomic.Business.Allocation.Filters;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Allocation.Resources;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Consents.Enums;
using Smt.Atomic.Business.Consents.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Enums;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Dto;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{
    public class AllocationRequestCardIndexDataService : CardIndexDataService<AllocationRequestDto, AllocationRequest, IAllocationDbScope>, IAllocationRequestCardIndexDataService
    {
        private AllocationCertainty? _previousAllocationCertainty;
        private AllocationRequestContentType? _contentType;

        private readonly IAllocationNotificationService _allocationNotificationService;
        private readonly IReadOnlyUnitOfWorkService<IAllocationDbScope> _unitOfWorkAllocationService;
        private readonly ILogger _logger;
        private readonly IClassMapping<AllocationRequestDto, AllocationRequest> _allocationClassMapping;
        private readonly IOrgUnitService _orgUnitService;
        private readonly IConsentDataService _consentDataService;
        private readonly IProjectService _projectService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly List<Employee> _deletedEmployeesRecordsFromOnDeletingEvent = new List<Employee>();
        private readonly List<Project> _deletedProjectsRecordsFromOnDeletingEvent = new List<Project>();

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public AllocationRequestCardIndexDataService(ICardIndexServiceDependencies<IAllocationDbScope> dependencies,
            IAllocationNotificationService allocationNotificationService,
            IReadOnlyUnitOfWorkService<IAllocationDbScope> unitOfWorkAllocationService,
            ILogger logger,
            IClassMapping<AllocationRequestDto, AllocationRequest> allocationClassMapping,
            IOrgUnitService orgUnitService,
            IConsentDataService consentDataService,
            IProjectService projectService,
            IPrincipalProvider principalProvider)
            : base(dependencies)
        {
            _allocationNotificationService = allocationNotificationService;
            _unitOfWorkAllocationService = unitOfWorkAllocationService;
            _logger = logger;
            _allocationClassMapping = allocationClassMapping;
            _orgUnitService = orgUnitService;
            _consentDataService = consentDataService;
            _projectService = projectService;
            _principalProvider = principalProvider;
        }

        protected override IEnumerable<Expression<Func<AllocationRequest, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return a => a.Employee.FirstName;
            yield return a => a.Employee.LastName;
            yield return a => a.Project.ProjectSetup.ClientShortName;
            yield return a => a.Project.SubProjectShortName;
            yield return a => a.Project.ProjectSetup.GenericName;
            yield return a => a.Project.JiraIssueKey;
        }

        protected override NamedFilters<AllocationRequest> NamedFilters
        {
            get
            {
                return new NamedFilters<AllocationRequest>(
                    new[]
                        {
                            DateRangeFilter.AllocationRequestFilter,
                            EmployeesAndProjectsFilter.AllocationRequestFilter,
                            ProjectTagsFilter.AllocationRequestFilter,
                            MyEmployeesFilter.GetAllocationRequestFilter(_orgUnitService,_principalProvider),
                            AllEmployeesFilter.AllocationRequestFilter,
                            LineManagerFilter.AllocationRequestFilter,
                            OrgUnitsFilter.AllocationRequestFilter,
                            SkillsAndKnowledgeFilter.AllocationRequestFilter,
                            LocationFilter.AllocationRequestFilter,
                            CompanyFilter.AllocationRequestFilter,
                            SkillsFilter.AllocationRequestFilter
                        }
                    .Union(CardIndexServiceHelper.BuildEnumBasedFilters<AllocationRequest, EmployeeStatus>(
                            e => e.Employee.CurrentEmployeeStatus))
                    .Union(CardIndexServiceHelper.BuildEnumBasedFilters<AllocationRequest, AllocationChangeDemand>(
                            e => e.ChangeDemand))
                    .Union(CardIndexServiceHelper.BuildEnumBasedFilters<AllocationRequest, EmploymentType>(
                            e => e.Employee.CurrentEmploymentType)));
            }
        }

        #region Add records

        protected override AddRecordResult InternalAddRecord(AllocationRequestDto record, object context)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var entityRepository = unitOfWork.Repositories.AllocationRequests;

                var recordAddingEventsArgs = record.EmployeeIds.Select(eId =>
                {
                    var entity = _allocationClassMapping.CreateFromSource(record);
                    entity.EmployeeId = eId;
                    return entity;
                })
                .Select(
                        entity =>
                            new RecordAddingEventArgs<AllocationRequest, AllocationRequestDto, IAllocationDbScope>(
                                unitOfWork, entity, record, context))
                .ToList();

                var alerts = new List<AlertDto>();
                var allViolations = new List<PropertyGroup>();
                foreach (var eventArgs in recordAddingEventsArgs)
                {
                    record.EmployeeId = eventArgs.InputEntity.EmployeeId;
                    var entity = eventArgs.InputEntity;
                    SetContext(entity, context);

                    var violations = entityRepository.GetUniqueKeyViolations(entity).ToList();

                    if (violations.Any())
                    {
                        eventArgs.Canceled = true;
                        allViolations.AddRange(violations);
                    }
                    else
                    {
                        OnRecordAdding(eventArgs);
                        alerts.AddRange(eventArgs.Alerts);

                        entityRepository.Add(entity);
                    }
                }

                if (recordAddingEventsArgs.All(ea => !ea.Canceled))
                {
                    unitOfWork.Commit();

                    foreach (var eventArgs in recordAddingEventsArgs)
                    {
                        var recordAddedEventArgs = new RecordAddedEventArgs<AllocationRequest, AllocationRequestDto>(eventArgs.InputEntity, record, context);
                        OnRecordAdded(recordAddedEventArgs);
                        alerts.AddRange(recordAddedEventArgs.Alerts);
                        PublishBusinessEvents(GetAddAllocationRequestEvents(eventArgs.InputEntity));
                    }

                }

                var propertyNames = ClassMappingHelper.TranslateToDestinationPropertyNames<AllocationRequest, AllocationRequestDto>(
                        allViolations, _allocationClassMapping);

                return new AddRecordResult(recordAddingEventsArgs.All(ea => !ea.Canceled), 0, propertyNames, alerts);
            }
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<AllocationRequest, AllocationRequestDto> addedEventArgs)
        {
            var allocationCertainty = addedEventArgs.InputDto.AllocationCertainty;
            var allocationEvent = allocationCertainty == AllocationCertainty.Certain ? AllocationEvent.OnFixedAdded : AllocationEvent.OnPlannedAdded;

            PrepareEmailForAddedEvent(allocationEvent, addedEventArgs);
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<AllocationRequest, AllocationRequestDto, IAllocationDbScope> eventArgs)
        {
            var newAllocationDto = eventArgs.InputDto;
            var checkResult = CheckEmploymentPeriodInDateRange(newAllocationDto, eventArgs.UnitOfWork, eventArgs.InputEntity);

            if (checkResult == ResultOfChangingEmploymentPeriod.IncorectEmploymentPeriod)
            {
                eventArgs.Canceled = true;
                eventArgs.Alerts.Add(GetDateChangeAlert(checkResult, AlertType.Error));

                return;
            }

            if (checkResult == ResultOfChangingEmploymentPeriod.EndDateChanged
                || checkResult == ResultOfChangingEmploymentPeriod.BeginDateChanged
                || checkResult == ResultOfChangingEmploymentPeriod.BothDatesChanged)
            {
                eventArgs.Alerts.Add(GetDateChangeAlert(checkResult, AlertType.Information));
            }

            var project = _projectService.GetProjectById(newAllocationDto.ProjectId);

            var minAllowedDate = project.StartDate ?? DateTime.MinValue;
            var maxAllowedDate = project.EndDate ?? DateTime.MaxValue;

            if (newAllocationDto.StartDate < minAllowedDate
                || newAllocationDto.StartDate > maxAllowedDate
                || newAllocationDto.EndDate.HasValue && newAllocationDto.EndDate > maxAllowedDate)
            {
                var alertDto = new AlertDto { Message = AlertResources.ProjectNotActiveInSelectedDateRange, Type = AlertType.Error };
                eventArgs.Alerts.Add(alertDto);
            }

            eventArgs.InputEntity.AllocationRequestMetadata = new AllocationRequestMetadata();

            AdjustAllocationCertaintyTypeBasedOnProjectStatus(newAllocationDto, eventArgs.InputEntity, eventArgs, eventArgs.UnitOfWork);
            base.OnRecordAdding(eventArgs);
        }

        private ResultOfChangingEmploymentPeriod CheckEmploymentPeriodInDateRange(AllocationRequestDto allocationRequestDto, IUnitOfWork<IAllocationDbScope> unitOfWork, AllocationRequest allocationRequest)
        {
            var employmentPeriods =
                unitOfWork.Repositories.EmploymentPeriods.Where(e => e.IsActive && e.EmployeeId == allocationRequestDto.EmployeeId)
                    .OrderBy(e => e.StartDate)
                    .ToArray();

            if (employmentPeriods.Any(ep => ep.StartDate <= allocationRequestDto.StartDate && (!ep.EndDate.HasValue ||
                         (allocationRequestDto.EndDate.HasValue && ep.EndDate.Value >= allocationRequestDto.EndDate.Value))))
            {
                return ResultOfChangingEmploymentPeriod.NoConflicts;
            }

            var firstEmployment = employmentPeriods.FirstOrDefault(
                e => e.StartDate <= allocationRequestDto.StartDate && e.EndDate.HasValue && e.EndDate.Value >= allocationRequestDto.StartDate);

            bool beginDateChanged = false;

            if (firstEmployment?.EndDate == null)
            {
                var allocationEndDate = allocationRequest.EndDate ?? DateTime.MaxValue;

                firstEmployment = employmentPeriods.FirstOrDefault(e => e.StartDate > allocationRequestDto.StartDate
                    && e.StartDate < allocationEndDate);

                if (firstEmployment == null)
                {
                    return ResultOfChangingEmploymentPeriod.IncorectEmploymentPeriod;
                }

                beginDateChanged = true;
                allocationRequestDto.StartDate = firstEmployment.StartDate;
                allocationRequest.StartDate = firstEmployment.StartDate;

                if (!firstEmployment.EndDate.HasValue ||
                    (allocationRequestDto.EndDate.HasValue &&
                     firstEmployment.EndDate.Value >= allocationRequestDto.EndDate.Value))
                {
                    return ResultOfChangingEmploymentPeriod.BeginDateChanged;
                }
            }

            var startDate = firstEmployment.EndDate.Value.AddDays(1);

            for (var i = 0; i <= employmentPeriods.Length; i++)
            {
                var employmentPeriod = employmentPeriods.FirstOrDefault(e => e.StartDate.Date == startDate.Date);

                if (employmentPeriod == null)
                {
                    allocationRequestDto.EndDate = startDate.AddDays(-1);
                    allocationRequest.EndDate = startDate.AddDays(-1);

                    return beginDateChanged ? ResultOfChangingEmploymentPeriod.BothDatesChanged : ResultOfChangingEmploymentPeriod.EndDateChanged;
                }

                if (!employmentPeriod.EndDate.HasValue || (allocationRequestDto.EndDate.HasValue && employmentPeriod.EndDate.Value >= allocationRequestDto.EndDate.Value))
                {
                    return beginDateChanged ? ResultOfChangingEmploymentPeriod.BeginDateChanged : ResultOfChangingEmploymentPeriod.NoConflicts;
                }

                startDate = employmentPeriod.EndDate.Value.AddDays(1);
            }

            return ResultOfChangingEmploymentPeriod.NoConflicts;
        }

        private void AdjustAllocationCertaintyTypeBasedOnProjectStatus(AllocationRequestDto inputDto, AllocationRequest inputEntity, CardIndexEventArgs eventArgs, IUnitOfWork<IAllocationDbScope> unitOfWork)
        {
            var isProjectStatusPlanned = unitOfWork.Repositories.Projects.Any(p => p.Id == inputDto.ProjectId && p.ProjectSetup.ProjectStatus == ProjectStatus.Planned);

            if (isProjectStatusPlanned && inputDto.AllocationCertainty == AllocationCertainty.Certain)
            {
                inputDto.AllocationCertainty = AllocationCertainty.Planned;
                inputEntity.AllocationCertainty = AllocationCertainty.Planned;

                eventArgs.AddWarning(string.Format(
                    AllocationRequestCardIndexDataServiceResources.ProjectPlannedStatusValidationMessageFormat,
                    AllocationCertainty.Certain.GetDescription(),
                    ProjectStatus.Planned.GetDescription()
                    ));
            }
        }

        private IEnumerable<BusinessEvent> GetAddAllocationRequestEvents(AllocationRequest entity)
        {
            yield return new DailyAllocationNeedsToRegenerateBusinessEvent
            {
                Scope = DailyAllocationNeedsToRegenerateScope.AllocationRequest,
                AllocationRequestId = entity.Id,
                EmployeeId = entity.EmployeeId,
                PeriodFrom = entity.StartDate
            };
        }

        #endregion

        #region Edit records

        protected override IEnumerable<BusinessEvent> GetEditRecordEvents(
            EditRecordResult result, AllocationRequestDto newRecord, AllocationRequestDto originalRecord)
        {
            var events = base.GetEditRecordEvents(result, newRecord, originalRecord);
            if (result.IsSuccessful)
            {
                if (originalRecord.EmployeeId != newRecord.EmployeeId)
                {
                    events = events.Concat(new[]
                    {
                        new DailyAllocationNeedsToRegenerateBusinessEvent
                        {
                            Scope = DailyAllocationNeedsToRegenerateScope.AllocationRequest,
                            AllocationRequestId = result.EditedRecordId,
                            EmployeeId = originalRecord.EmployeeId,
                            PeriodFrom = new DateTime(
                                Math.Min(
                                    newRecord.StartDate.Ticks,
                                    originalRecord.StartDate.Ticks))
                        }
                    });
                }

                events = events.Concat(new[]
                {
                    new DailyAllocationNeedsToRegenerateBusinessEvent
                    {
                        Scope = DailyAllocationNeedsToRegenerateScope.AllocationRequest,
                        AllocationRequestId = result.EditedRecordId,
                        EmployeeId = newRecord.EmployeeId,
                        PeriodFrom = new DateTime(
                            Math.Min(
                                newRecord.StartDate.Ticks,
                                originalRecord.StartDate.Ticks))
                    }
                });
            }

            return events;
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<AllocationRequest, AllocationRequestDto> editedEventArgs)
        {
            if (_previousAllocationCertainty == AllocationCertainty.Planned && editedEventArgs.DbEntity.AllocationCertainty == AllocationCertainty.Certain)
            {
                PrepeareEmailForEditedEvent(AllocationEvent.OnPlannedToFixed, editedEventArgs);
            }
            else
            {
                PrepeareEmailForEditedEvent(AllocationEvent.OnChanged, editedEventArgs);
            }
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<AllocationRequest, AllocationRequestDto, IAllocationDbScope> eventArgs)
        {
            var checkResult = CheckEmploymentPeriodInDateRange(eventArgs.InputDto, eventArgs.UnitOfWork, eventArgs.InputEntity);

            if (checkResult == ResultOfChangingEmploymentPeriod.IncorectEmploymentPeriod)
            {
                eventArgs.Canceled = true;
                eventArgs.Alerts.Add(GetDateChangeAlert(checkResult, AlertType.Error));

                return;
            }

            if (checkResult == ResultOfChangingEmploymentPeriod.EndDateChanged
                || checkResult == ResultOfChangingEmploymentPeriod.BeginDateChanged
                || checkResult == ResultOfChangingEmploymentPeriod.BothDatesChanged)
            {
                eventArgs.Alerts.Add(GetDateChangeAlert(checkResult, AlertType.Information));
            }

            AdjustAllocationCertaintyTypeBasedOnProjectStatus(eventArgs.InputDto, eventArgs.InputEntity, eventArgs, eventArgs.UnitOfWork);
            _previousAllocationCertainty = eventArgs.DbEntity.AllocationCertainty;

            eventArgs.DbEntity.AllocationRequestMetadata.ProcessingStatus = ProcessingStatus.Processing;

            base.OnRecordEditing(eventArgs);
        }


        #endregion

        #region Delete records

        protected override void OnRecordDeleting(RecordDeletingEventArgs<AllocationRequest, IAllocationDbScope> eventArgs)
        {
            _deletedEmployeesRecordsFromOnDeletingEvent.Add(eventArgs.DeletingRecord.Employee);
            _deletedProjectsRecordsFromOnDeletingEvent.Add(eventArgs.DeletingRecord.Project);

            eventArgs.UnitOfWork.Repositories.AllocationRequestMetadatas.Delete(
                eventArgs.DeletingRecord.AllocationRequestMetadata);
        }

        protected override void OnRecordsDeleted(RecordDeletedEventArgs<AllocationRequest> eventArgs)
        {
            PrepareEmailForDeletedEvent(AllocationEvent.OnDeleted, eventArgs.DeletedEntities, _deletedEmployeesRecordsFromOnDeletingEvent,
                _deletedProjectsRecordsFromOnDeletingEvent);
        }

        protected override IEnumerable<BusinessEvent> GetDeleteRecordEvents(DeleteRecordsResult result, AllocationRequestDto record)
        {
            var events = base.GetDeleteRecordEvents(result, record);

            if (result.IsSuccessful)
            {
                events = events.Concat(new[]
                {
                    new DailyAllocationNeedsToRegenerateBusinessEvent
                    {
                        Scope = DailyAllocationNeedsToRegenerateScope.AllocationRequest,
                        EmployeeId = record.EmployeeId,
                        PeriodFrom = record.StartDate,
                    }
                });
            }

            return events;
        }

        #endregion

        private void PrepareEmailForAddedEvent(AllocationEvent allocationEvent, RecordAddedEventArgs<AllocationRequest, AllocationRequestDto> eventArgs)
        {
            PrepareEmail(allocationEvent, eventArgs.InputEntity, eventArgs.InputEntity.ModifiedById);
        }

        private void PrepeareEmailForEditedEvent(AllocationEvent allocationEvent, RecordEditedEventArgs<AllocationRequest, AllocationRequestDto> eventArgs)
        {
            PrepareEmail(allocationEvent, eventArgs.InputEntity, eventArgs.DbEntity.ModifiedById);
        }

        private void PrepareEmail(AllocationEvent allocationEvent, AllocationRequest allocationRequest, long? userId)
        {
            AllocationNotificationDto allocationNotificationDto;
            Employee employee;

            using (var unitOfWork = _unitOfWorkAllocationService.Create())
            {
                employee = GetEmployee(allocationRequest.EmployeeId, unitOfWork);

                if (employee == null)
                {
                    _logger.ErrorFormat("Employee is null in PrepeareDataAndSendEmailForAddedEvent");
                    return;
                }

                allocationNotificationDto = GetAllocationNotificationDto(unitOfWork, allocationRequest, employee, userId);

                if (allocationNotificationDto == null)
                {
                    _logger.ErrorFormat("AllocationNotificationDto is null in PrepeareDataAndSendEmailForAddedEvent");
                    return;
                }

                var allocationProject = unitOfWork.Repositories.Projects.FirstOrDefault(p => p.Id == allocationRequest.ProjectId);

                if (allocationProject == null)
                {
                    _logger.ErrorFormat($"Can't find project with Id: {allocationRequest.ProjectId}");
                    return;
                }

                var consentType = allocationEvent == AllocationEvent.OnChanged ||
                                  allocationEvent == AllocationEvent.OnPlannedToFixed
                    ? AllocationRequestEmailConsentEnum.RequestEditedEmail
                    : AllocationRequestEmailConsentEnum.RequestAddedEmail;

                var recipientsDictionary = EmailRecivers(unitOfWork, employee, consentType);

                _allocationNotificationService.SendNotification(allocationNotificationDto, allocationEvent, recipientsDictionary);
            }

        }

        private Dictionary<string, string> EmailRecivers(IReadOnlyUnitOfWork<IAllocationDbScope> unitOfWork, Employee employee, AllocationRequestEmailConsentEnum consent)
        {
            var employeeId = _principalProvider.Current.EmployeeId;

            var activeManagers = unitOfWork.Repositories.Employees.Where(e =>
                (
                    (employee.LineManagerId.HasValue && employee.LineManagerId.Value == e.Id)
                    || (employee.OrgUnit.EmployeeId == e.Id)
                    )
                && e.Id != employeeId
                && e.UserId.HasValue
                )
                .Select(e => new { e.Email, FullName = e.FirstName + " " + e.LastName, UserId = e.UserId.Value })
                .Distinct();

            var activeManagersIds = activeManagers.Select(m => m.UserId).Distinct().ToList();

            var activeManagersWithConsents = _consentDataService.ExcludeUsersWithoutConsents(consent, activeManagersIds);

            return activeManagers.Where(am => activeManagersWithConsents.Contains(am.UserId))
                .ToDictionary(a => a.Email, a => a.FullName);
        }

        private void PrepareEmailForDeletedEvent(AllocationEvent allocationEvent, IList<AllocationRequest> deletedAllocationRequests, IList<Employee> deletedRecordsEmployees, IList<Project> deletedRecordsProjects)
        {
            long? modifiedById = deletedAllocationRequests.Select(x => x.ModifiedById).FirstOrDefault();

            for (var i = 0; i < deletedRecordsProjects.Count; i++)
            {
                var deletedAllocationRequest = deletedAllocationRequests[i];
                var deletedEmployee = deletedRecordsEmployees[i];

                using (var unitOfWork = _unitOfWorkAllocationService.Create())
                {
                    var projectId = deletedRecordsProjects[i].Id;

                    var allocationNotificationDto = GetAllocationNotificationDto(unitOfWork, deletedAllocationRequest, projectId, modifiedById);

                    if (allocationNotificationDto == null)
                    {
                        _logger.ErrorFormat("AllocationNotificationDto is null in PrepeareDataAndSendEmailForDeletedEvent");
                        continue;
                    }
                    var recipentsDictionary = EmailRecivers(unitOfWork, deletedEmployee,
                        AllocationRequestEmailConsentEnum.RequestDeletedEmail);

                    allocationNotificationDto.FirstName = deletedEmployee.FirstName;
                    allocationNotificationDto.LastName = deletedEmployee.LastName;
                    allocationNotificationDto.Certainty = deletedAllocationRequest.AllocationCertainty.ToString();
                    allocationNotificationDto.MondayHours = deletedAllocationRequest.MondayHours;
                    allocationNotificationDto.TuesdayHours = deletedAllocationRequest.TuesdayHours;
                    allocationNotificationDto.WednesdayHours = deletedAllocationRequest.WednesdayHours;
                    allocationNotificationDto.ThursdayHours = deletedAllocationRequest.ThursdayHours;
                    allocationNotificationDto.FridayHours = deletedAllocationRequest.FridayHours;
                    allocationNotificationDto.SaturdayHours = deletedAllocationRequest.SaturdayHours;
                    allocationNotificationDto.SundayHours = deletedAllocationRequest.SundayHours;

                    _allocationNotificationService.SendNotification(allocationNotificationDto, allocationEvent, recipentsDictionary);
                }
            }
        }

        private AllocationNotificationDto GetAllocationNotificationDto(IReadOnlyUnitOfWork<IAllocationDbScope> unitOfWork, AllocationRequest allocationRequest, Employee employee, long? userId)
        {
            var project = unitOfWork.Repositories.Projects.GetByIdOrDefault(allocationRequest.ProjectId);

            if (project == null)
            {
                throw new BusinessException($"Can't find project with Id={allocationRequest.ProjectId}");
            }

            return new AllocationNotificationDto
            {
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                ModifiedBy = _principalProvider.Current.FirstName + " " + _principalProvider.Current.LastName,
                Certainty = allocationRequest.AllocationCertainty.ToString(),
                DateFrom = allocationRequest.StartDate,
                DateTo = allocationRequest.EndDate,
                ProjectName = ProjectBusinessLogic.ProjectName.Call(project),
                Client = project.ProjectSetup.ClientShortName,
                Comments = project.Description,
                ProjectManagerId = project.ProjectSetup.ProjectManagerId,
                IsSimple = allocationRequest.ContentType == AllocationRequestContentType.Simple,
                MondayHours = allocationRequest.MondayHours,
                TuesdayHours = allocationRequest.TuesdayHours,
                WednesdayHours = allocationRequest.WednesdayHours,
                ThursdayHours = allocationRequest.ThursdayHours,
                FridayHours = allocationRequest.FridayHours,
                SaturdayHours = allocationRequest.SaturdayHours,
                SundayHours = allocationRequest.SundayHours,
            };
        }

        private AllocationNotificationDto GetAllocationNotificationDto(IReadOnlyUnitOfWork<IAllocationDbScope> unitOfWork, AllocationRequest allocationRequest, long tempProjectId, long? modifiedById)
        {
            var allocationNotificationDto = (from employees in unitOfWork.Repositories.Employees
                                             join project in unitOfWork.Repositories.Projects on
                                                 tempProjectId equals project.Id
                                             where
                                                 employees.UserId == modifiedById
                                             select new AllocationNotificationDto
                                             {
                                                 ModifiedBy = employees.FirstName + " " + employees.LastName,
                                                 DateFrom = allocationRequest.StartDate,
                                                 DateTo = allocationRequest.EndDate,
                                                 ProjectName = project.SubProjectShortName,
                                                 Client = project.ProjectSetup.ClientShortName,
                                                 Comments = project.Description,
                                                 ProjectManagerId = project.ProjectSetup.ProjectManagerId
                                             }).FirstOrDefault();
            return allocationNotificationDto;
        }

        private Employee GetEmployee(long employeeId, IReadOnlyUnitOfWork<IAllocationDbScope> unitOfWork)
        {
            return (from e in unitOfWork.Repositories.Employees where e.Id == employeeId select e).FirstOrDefault();
        }

        public long? ResolveAllocationRequest(long projectId, long employeeId, DateTime period)
        {
            var endOfWeek = period.GetNextDayOfWeek(DayOfWeek.Sunday);

            using (var unitOfWork = _unitOfWorkAllocationService.Create())
            {
                var allocationRequest = unitOfWork
                    .Repositories
                    .AllocationRequests
                    .OrderBy(ar => ar.StartDate)
                    .FirstOrDefault(ar =>
                        ar.ProjectId == projectId &&
                        ar.EmployeeId == employeeId &&
                        (ar.StartDate <= period // started before
                        || (ar.StartDate <= endOfWeek && ar.StartDate >= period)) // started in this week
                        && (!ar.EndDate.HasValue || ar.EndDate.Value >= period) // not ended before this week
                    );

                return allocationRequest?.Id;
            }
        }

        private static AlertDto GetDateChangeAlert(ResultOfChangingEmploymentPeriod checkResult, AlertType alertType)
        {
            return new AlertDto
            {
                IsDismissable = true,
                Message = checkResult.GetDescription(),
                Type = alertType
            };
        }

        public void SetRecordContentType(AllocationRequestContentType? contentType)
        {
            _contentType = contentType;
        }

        public override AllocationRequestDto GetRecordById(long id)
        {
            var originalRecord = base.GetRecordById(id);

            if (_contentType.HasValue)
            {
                originalRecord.ContentType = _contentType.Value;
            }

            return originalRecord;
        }
    }
}