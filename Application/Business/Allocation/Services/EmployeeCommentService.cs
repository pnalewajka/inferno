﻿using System;
using System.Linq;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{
    public class EmployeeCommentService : IEmployeeCommentService
    {
        private readonly IUnitOfWorkService<IAllocationDbScope> _unitOfWorkService;
        private readonly IClassMapping<EmployeeComment, EmployeeCommentDto> _employeeCommentToEmployeeCommentDtoClassMapping;

        public EmployeeCommentService(IUnitOfWorkService<IAllocationDbScope> unitOfWorkService,
            IClassMapping<EmployeeComment, EmployeeCommentDto> employeeCommentToEmployeeCommentDtoClassMapping)
        {
            _unitOfWorkService = unitOfWorkService;
            _employeeCommentToEmployeeCommentDtoClassMapping = employeeCommentToEmployeeCommentDtoClassMapping;
        }

        public void AddEmployeeComment(EmployeeCommentDto employeeCommentDto)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employeeComment = new EmployeeComment
                {
                    Comment = employeeCommentDto.Comment,
                    EmployeeCommentType = employeeCommentDto.EmployeeCommentType,
                    EmployeeId = employeeCommentDto.EmployeeId,
                    RelevantOn = employeeCommentDto.RelevantOn
                };

                unitOfWork.Repositories.EmployeeComments.Add(employeeComment);
                unitOfWork.Commit();
            }
        }

        public void EditEmployeeComment(EmployeeCommentDto employeeCommentDto)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employeeComment = unitOfWork.Repositories.EmployeeComments.GetById(employeeCommentDto.Id);

                employeeComment.Comment = employeeCommentDto.Comment;
                unitOfWork.Repositories.EmployeeComments.Edit(employeeComment);

                unitOfWork.Commit();
            }
        }

        public EmployeeCommentDto GetEmployeeComment(EmployeeCommentType employeeCommentType, long employeeId,
            DateTime? relevantOn)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employeeComments =
                    unitOfWork.Repositories.EmployeeComments.FirstOrDefault(
                        e => e.EmployeeId == employeeId && e.EmployeeCommentType == employeeCommentType
                             && e.RelevantOn == relevantOn);

                return employeeComments == null ? null : _employeeCommentToEmployeeCommentDtoClassMapping.CreateFromSource(employeeComments);
            }
        }
    }
}
