﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Smt.Atomic.Business.Allocation.DailyAllocation;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Helpers;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{
    public class DailyAllocationService : IDailyAllocationService
    {
        private readonly IUnitOfWorkService<IAllocationDbScope> _allocationDbScope;
        private readonly IUnitOfWorkService<IConfigurationDbScope> _configurationDbScope;
        private readonly ICalendarService _calendarService;
        private readonly IDateRangeService _dateRangeService;
        private readonly ISystemParameterService _systemParameters;
        private readonly ITimeService _timeService;
        private readonly IClassMapping<AllocationRequest, AllocationRequestDto> _allocationRequestToAllocationRequestDtoClassMapping;
        private readonly IClassMapping<EmploymentPeriod, EmploymentPeriodDto> _employmentPeriodToEmploymentPeriodDtoClassMapping;
        private readonly IDanteCalendarService _danteCalendarService;

        private const decimal FullTimeEquivalentDailyHours = 8;

        public DailyAllocationService(
            IUnitOfWorkService<IAllocationDbScope> allocationDbScope,
            ICalendarService calendarService,
            IDateRangeService dateRangeService,
            ISystemParameterService systemParameters,
            ITimeService timeservice,
            IClassMapping<AllocationRequest, AllocationRequestDto> allocationRequestToAllocationRequestDtoClassMapping,
            IClassMapping<EmploymentPeriod, EmploymentPeriodDto> employmentPeriodToEmploymentPeriodDtoClassMapping,
            IDanteCalendarService danteCalendarService, IUnitOfWorkService<IConfigurationDbScope> configurationDbScope)
        {
            _allocationDbScope = allocationDbScope;
            _calendarService = calendarService;
            _dateRangeService = dateRangeService;
            _systemParameters = systemParameters;
            _timeService = timeservice;
            _allocationRequestToAllocationRequestDtoClassMapping = allocationRequestToAllocationRequestDtoClassMapping;
            _employmentPeriodToEmploymentPeriodDtoClassMapping = employmentPeriodToEmploymentPeriodDtoClassMapping;
            _danteCalendarService = danteCalendarService;
            _configurationDbScope = configurationDbScope;
        }

        public bool RebuildDailyRecords(long allocationRequestId)
        {
            using (var unitOfWork = _allocationDbScope.Create())
            {
                AllocationRequestDto allocationRequest = unitOfWork.Repositories.AllocationRequests.AsNoTracking().Where(x => x.Id == allocationRequestId)
                    .Select(_allocationRequestToAllocationRequestDtoClassMapping.CreateFromSource).FirstOrDefault();

                if (allocationRequest == null)
                    return false;

                List<EmploymentPeriodDto> employmentPeriods = unitOfWork.Repositories.EmploymentPeriods.AsNoTracking().Where(x => x.EmployeeId == allocationRequest.EmployeeId)
                    .Select(_employmentPeriodToEmploymentPeriodDtoClassMapping.CreateFromSource).ToList();

                long orgUnitId = unitOfWork.Repositories.Employees.AsNoTracking().GetById(allocationRequest.EmployeeId).OrgUnitId;
                long? calendarId = unitOfWork.Repositories.Projects.AsNoTracking().GetById(allocationRequest.ProjectId).ProjectSetup.CalendarId;
                
                return RebuildDailyRecords(allocationRequest, employmentPeriods, orgUnitId, calendarId);
            }
        }

        public bool RebuildDailyRecords(AllocationRequestDto allocationRequest, List<EmploymentPeriodDto> employmentPeriods)
        {
            using (var unitOfWork = _allocationDbScope.Create())
            {
                long orgUnitId = unitOfWork.Repositories.Employees.GetById(allocationRequest.EmployeeId).OrgUnitId;
                long? calendarId = unitOfWork.Repositories.Projects.GetById(allocationRequest.ProjectId).ProjectSetup.CalendarId;

                employmentPeriods = employmentPeriods.Where(x => x.EmployeeId == allocationRequest.EmployeeId).ToList();

                return RebuildDailyRecords(allocationRequest, employmentPeriods, orgUnitId, calendarId);
            }
        }

        private bool RebuildDailyRecords(AllocationRequestDto allocationReques, List<EmploymentPeriodDto> employmentPeriodList, long orgUnitId, long? calendarId)
        {
            DateTime endDate;

            var startRegenerateDate = _systemParameters.GetParameter<DateTime>(ParameterKeys.AllocationCalculationStartDate);
            var startDate = allocationReques.StartDate > startRegenerateDate ? allocationReques.StartDate : startRegenerateDate;

            if (allocationReques.EndDate.HasValue)
            {
                endDate = allocationReques.EndDate.Value;
                allocationReques.NextRebuildTime = null;
            }
            else
            {
                endDate = GetDefaultEndDate();
                allocationReques.NextRebuildTime = GetDefaultRebuildDate();
            }

            if (startDate > endDate)
            {
                return true;
            }

            using (var unitOfWork = _allocationDbScope.CreateExtended())
            {
                const string deleteSqlCommand = @"EXEC [Allocation].[DeleteAllocationDailyRecords] @AllocationRequestId ";
                unitOfWork.ExecuteSqlCommand(deleteSqlCommand, new SqlParameter("@AllocationRequestId", allocationReques.Id));

                //create new daily allocation records
                var dateRanges = _dateRangeService.GetDaysInRange(startDate, endDate, includeWeekends: true);

                //select current calendar
                var employeeCalendarId = calendarId ?? _danteCalendarService.GetCalendarIdByOrgUnitId(orgUnitId);

                foreach (var day in dateRanges)
                {
                    var employmentPeriod = employmentPeriodList.SingleOrDefault(p => p.StartDate <= day && (!p.EndDate.HasValue || p.EndDate.Value >= day));

                    if (employmentPeriod != null)
                    {
                        var dayCalendarId = employmentPeriod.CalendarId ?? employeeCalendarId;
                        var isWorking = _calendarService.IsWorkday(day, dayCalendarId);
                        var allocatedHours = WeeklyHoursHelper.GetHoursByDay(allocationReques, day.DayOfWeek);

                        if (allocatedHours != decimal.Zero)
                        {
                            var dailyAllocationRecord =
                                unitOfWork.Repositories.AllocationDailyRecords.CreateEntity();

                            dailyAllocationRecord.AllocationRequestId = allocationReques.Id;
                            dailyAllocationRecord.EmployeeId = allocationReques.EmployeeId;
                            dailyAllocationRecord.ProjectId = allocationReques.ProjectId;
                            dailyAllocationRecord.CalendarId = dayCalendarId;
                            dailyAllocationRecord.Hours = allocatedHours;
                            dailyAllocationRecord.Day = day;
                            dailyAllocationRecord.IsWorkDay = isWorking;

                            unitOfWork.Repositories.AllocationDailyRecords.Add(dailyAllocationRecord);
                        }
                    }
                }

                var dbAllocationRequestMetadatas = unitOfWork.Repositories.AllocationRequestMetadatas.First(a => a.AllocationRequestId == allocationReques.Id);
                dbAllocationRequestMetadatas.NextRebuildTime = allocationReques.NextRebuildTime;
                unitOfWork.Repositories.AllocationRequestMetadatas.Edit(dbAllocationRequestMetadatas);

                unitOfWork.Commit();
            }

            return true;
        }

        public bool RebuildWeeklyStatus(List<EmploymentPeriodDto> employmentPeriods, long employeeId, DateTime from, long orgUnitId)
        {
            var startRegenerateDate = _systemParameters.GetParameter<DateTime>(ParameterKeys.AllocationCalculationStartDate);
            var startFrom = from > startRegenerateDate ? from : startRegenerateDate;

            var weeks = _dateRangeService.GetAllWeeksInDateRanges(startFrom, GetDefaultEndDate());
            return RebuildWeeklyStatus(employmentPeriods, employeeId, weeks.ToList(), orgUnitId);
        }

        public bool RebuildWeeklyStatus(long employeeId, DateTime from)
        {
            using (var unitOfWork = _allocationDbScope.Create())
            {
                List<EmploymentPeriodDto> employmentPeriods = unitOfWork.Repositories.EmploymentPeriods.Where(x => x.EmployeeId == employeeId)
                    .Select(_employmentPeriodToEmploymentPeriodDtoClassMapping.CreateFromSource).ToList();

                var orgUnitId = unitOfWork.Repositories.Employees.GetById(employeeId).OrgUnitId;
                var weekDay = from.GetPreviousDayOfWeek(DayOfWeek.Monday);

                var allocationStatus = unitOfWork.Repositories.WeeklyAllocationStatus
                    .FirstOrDefault(w => w.EmployeeId == employeeId && w.Week == weekDay);

                if (allocationStatus?.OnBenchSince != null)
                {
                    from = allocationStatus.OnBenchSince.Value;
                }

                return RebuildWeeklyStatus(employmentPeriods, employeeId, from, orgUnitId);
            }
        }

        public void SetAllocationRequestExecuted(long allocationRequestId)
        {
            using (var unitOfWork = _allocationDbScope.Create())
            {
                var allocationRequestMetadatas = unitOfWork.Repositories.AllocationRequestMetadatas.FirstOrDefault(a => a.AllocationRequestId == allocationRequestId);

                if (allocationRequestMetadatas == null)
                {
                    return;
                }

                allocationRequestMetadatas.ProcessingStatus = ProcessingStatus.Processed;
                unitOfWork.Repositories.AllocationRequestMetadatas.Edit(allocationRequestMetadatas);
                unitOfWork.Commit();
            }
        }

        private bool RebuildWeeklyStatus(List<EmploymentPeriodDto> employmentPeriods, long employeeId, IReadOnlyCollection<DateTime> weeks, long orgUnitId)
        {
            var weeklyAllocationStatuses = new List<WeeklyAllocationStatus>();
            var processedWeeks = new List<WeeklyAllocationStatus>();
            DateTime? wasOnBenchSince = null;

            if (!weeks.Any())
            {
                return true;
            }

            var calendarId = GetCalendarIdByOrgUnitId(orgUnitId);

            using (var unitOfWork = _allocationDbScope.CreateExtended())
            {
                var allocationDailyRecords =
                    unitOfWork.Repositories.AllocationDailyRecords.Where(a => a.EmployeeId == employeeId).ToList();

                //clear old status
                var fromDate = weeks.Min();
                var toDate = weeks.Max();

                const string deleteSqlCommand = @"EXEC [Allocation].[DeleteWeeklyAllocations] @EmployeeId, @FromDate, @ToDate";
                unitOfWork.ExecuteSqlCommand(deleteSqlCommand, new SqlParameter("@EmployeeId", employeeId), new SqlParameter("@FromDate", fromDate), new SqlParameter("@ToDate", toDate));

                foreach (var week in weeks)
                {
                    var lastDay = week.GetNextDayOfWeek(DayOfWeek.Sunday);

                    //calculate daily data
                    var dailyData = _dateRangeService.GetDaysInRange(week, lastDay, true)
                        .Select(day =>
                            CalculateEmployeAllocationDailyData(day, employmentPeriods,
                                allocationDailyRecords, calendarId))
                        .Where(x => x != null)
                        .ToList();

                    //create new record
                    var status = unitOfWork.Repositories.WeeklyAllocationStatus.CreateEntity();

                    status.EmployeeId = employeeId;
                    status.Week = week;
                    status.AllocatedHours = CalculateAllocatedHours(dailyData);
                    status.PlannedHours = CalculatePlannedHours(dailyData);
                    status.AvailableHours = CalculateAvailableHours(dailyData);
                    status.CalendarHours = CalculateCalendarHours(dailyData);
                    status.AllocationStatus = GetAllocationStatusForWeeklyAllocation(dailyData);
                    status.EmployeeStatus = GetEmployeeStatusForWeeklyAllocation(dailyData);

                    status.StaffingStatus = GetStaffingStatusForWeeklyAllocation(status.AllocationStatus);
                    var onBenchSince = CalculateOnBenchSinceDate(status.StaffingStatus, week, processedWeeks);
                    status.OnBenchSince = onBenchSince;

                    if (wasOnBenchSince.HasValue && !onBenchSince.HasValue)
                    {
                        var onBenchUntil = week.GetPreviousWeekDay(DayOfWeek.Friday);
                        ReassignPreviousOnBenches(wasOnBenchSince.Value, weeklyAllocationStatuses, onBenchUntil);
                    }

                    wasOnBenchSince = onBenchSince;

                    processedWeeks.Add(status);

                    var detailsQuery = from d in dailyData.SelectMany(x => x.Projects)
                        group d by d.ProjectID
                        into g
                        select new
                        {
                            ProjectId = g.Key,
                            Hours = g.Sum(x => x.WorkingHours)
                        };

                    if (status.EmployeeStatus != EmployeeStatus.Terminated)
                    {
                        weeklyAllocationStatuses.Add(status);

                        foreach (var detail in detailsQuery)
                        {
                            var statusDetail = unitOfWork.Repositories.WeeklyAllocationDetails.CreateEntity();

                            statusDetail.WeeklyAllocationStatus = status;
                            statusDetail.ProjectId = detail.ProjectId;
                            statusDetail.Hours = detail.Hours;

                            unitOfWork.Repositories.WeeklyAllocationDetails.Add(statusDetail);
                        }
                    }
                }

                foreach (var weeklyAllocationStatus in weeklyAllocationStatuses)
                {
                    unitOfWork.Repositories.WeeklyAllocationStatus.Add(weeklyAllocationStatus);
                }

                unitOfWork.Commit();
            }

            return true;
        }

        private void ReassignPreviousOnBenches(DateTime onBenchSince, List<WeeklyAllocationStatus> processedWeeks, DateTime onBenchUntil)
        {
            var statusesToFill = processedWeeks.Where(p => p.OnBenchSince.HasValue && p.OnBenchSince == onBenchSince);

            foreach (var status in statusesToFill)
            {
                status.OnBenchUntil = onBenchUntil;

                if (status.StaffingStatus == StaffingStatus.Planned)
                {
                    status.StaffingStatus = StaffingStatus.PlannedAndConfirmed;
                }
            }
        }

        private DateTime? CalculateOnBenchSinceDate(StaffingStatus staffingStatus, DateTime week, List<WeeklyAllocationStatus> processedWeeks)
        {
            if (!IsOnBench(staffingStatus) && staffingStatus != StaffingStatus.NotAvaliable)
            {
                return null;
            }

            var lastWeek = processedWeeks.SingleOrDefault(w => w.Week == week.GetPreviousWeekDay(DayOfWeek.Monday) && IsOnBench(w.StaffingStatus));

            return lastWeek == null ? week : lastWeek.OnBenchSince;
        }

        private bool IsOnBench(StaffingStatus staffingStatus)
        {
            return staffingStatus == StaffingStatus.Free || staffingStatus == StaffingStatus.Planned;
        }

        private StaffingStatus GetStaffingStatusForWeeklyAllocation(AllocationStatus allocationStatus)
        {
            var mapping = new Dictionary<StaffingStatus, AllocationStatus[]>
            {
                { StaffingStatus.Free, new []{ AllocationStatus.Free, AllocationStatus.AlmostFree }},
                { StaffingStatus.Planned, new []{ AllocationStatus.Planned, AllocationStatus.UnderPlanned, AllocationStatus.Mixed }},
                { StaffingStatus.Allocated, new []{ AllocationStatus.Allocated, AllocationStatus.UnderAllocated }},
                { StaffingStatus.NotAvaliable, new []{ AllocationStatus.Unavailable, AllocationStatus.InActive }},
            };

            return mapping.Single(x => x.Value.Contains(allocationStatus)).Key;
        }

        private AllocationStatus GetAllocationStatusForWeeklyAllocation(IList<EmployeAllocationDayData> dailyData)
        {
            // no records (Unavailable state)
            if (!dailyData.Any())
            {
                return AllocationStatus.Unavailable;
            }

            //IsActive state
            if (dailyData.All(x => !x.IsActive))
            {
                return AllocationStatus.InActive;
            }

            var highLimit = _systemParameters.GetParameter<int>(ParameterKeys.AllocationLimitHighStatusLimit) / 100M;
            var lowLimit = _systemParameters.GetParameter<int>(ParameterKeys.AllocationLimitLowStatusLimit) / 100M;

            var allocatedHours = CalculateAllocatedHours(dailyData);
            var plannedHours = CalculatePlannedHours(dailyData);
            var availableHours = CalculateAvailableHours(dailyData);

            if (allocatedHours > highLimit*availableHours)
            {
                return AllocationStatus.Allocated;
            }

            if (plannedHours > highLimit*availableHours)
            {
                return AllocationStatus.Planned;
            }

            if (availableHours - (plannedHours + allocatedHours) > highLimit*availableHours)
            {
                return AllocationStatus.Free;
            }

            if (allocatedHours > lowLimit*availableHours)
            {
                return AllocationStatus.UnderAllocated;
            }

            if (plannedHours > lowLimit*availableHours)
            {
                return AllocationStatus.UnderPlanned;
            }

            if (availableHours - (plannedHours + allocatedHours) > lowLimit*availableHours)
            {
                return AllocationStatus.AlmostFree;
            }

            if (plannedHours + allocatedHours > lowLimit * availableHours)
            {
                return allocatedHours > plannedHours ? AllocationStatus.UnderAllocated : AllocationStatus.UnderPlanned;
            }

            return AllocationStatus.Mixed;
        }

        private EmployeeStatus GetEmployeeStatusForWeeklyAllocation(IList<EmployeAllocationDayData> dailyData)
        {
            if (!dailyData.Any())
            {
                return EmployeeStatus.Terminated;
            }

            if (dailyData.Any(x => x.IsLeave))
            {
                return EmployeeStatus.Leave;
            }

            if (dailyData.Any(x => x.IsNewHire))
            {
                return EmployeeStatus.NewHire;
            }

            if (dailyData.Any(x => x.IsActive))
            {
                return EmployeeStatus.Active;
            }

            return EmployeeStatus.Inactive;
        }

        private decimal CalculateAllocatedHours(IEnumerable<EmployeAllocationDayData> dailyData)
        {
            return dailyData.SelectMany(x => x.Projects)
                .Where(x => x.AllocationCertainty == AllocationCertainty.Certain)
                .Sum(x => x.WorkingHours);
        }

        private decimal CalculatePlannedHours(IEnumerable<EmployeAllocationDayData> dailyData)
        {
            return dailyData.SelectMany(x => x.Projects)
                .Where(x => x.AllocationCertainty == AllocationCertainty.Planned)
                .Sum(x => x.WorkingHours);
        }

        private decimal CalculateAvailableHours(IEnumerable<EmployeAllocationDayData> dailyData)
        {
            return dailyData.Sum(x => x.AvailableHours);
        }

        private decimal CalculateCalendarHours(IEnumerable<EmployeAllocationDayData> dailyData)
        {
            return dailyData.Sum(x => x.CalendarHours);
        }

        private EmployeAllocationDayData CalculateEmployeAllocationDailyData(DateTime day, List<EmploymentPeriodDto> employmentPeriods, 
            List<AllocationDailyRecord> allocationDailyRecords, long calendarId)
        {
            var employmentPeriod = employmentPeriods
                .Where(x => x.StartDate <= day && (!x.EndDate.HasValue || x.EndDate.Value >= day))
                .OrderByDescending(x => x.StartDate)
                .FirstOrDefault();

            if (employmentPeriod == null)
            {
                var isNewHire = employmentPeriods.Any(x => x.SignedOn <= day && x.StartDate > day);

                if (isNewHire)
                {
                    return new EmployeAllocationDayData
                    {
                        Day = day,
                        Projects = new EmployeAllocationProjectData[] { },
                        IsNewHire = true,
                    };
                }

                return null;
            }

            var dailyProjectQuery =
                from a in allocationDailyRecords
                where a.Day == day
                select new
                {
                    a.Project.Id,
                    a.AllocationRequest.AllocationCertainty,
                    a.Hours,
                    a.IsWorkDay
                }
                into g
                group g by new
                {
                    g.Id,
                    g.AllocationCertainty
                }
                into q
                select new
                {
                    q.Key.Id,
                    q.Key.AllocationCertainty,
                    WorkingHours = (decimal?)q.Where(x => x.IsWorkDay).Sum(x => x.Hours),
                    FreeHours = (decimal?)q.Where(x => !x.IsWorkDay).Sum(x => x.Hours),
                };

            var projectAllocation = dailyProjectQuery.Select(x => new EmployeAllocationProjectData
            {
                ProjectID = x.Id,
                AllocationCertainty = x.AllocationCertainty,
                WorkingHours = x.WorkingHours ?? decimal.Zero,
                FreeHours = x.FreeHours ?? decimal.Zero,
            }).ToArray();

            var allWorkingHours = projectAllocation.Sum(x => x.WorkingHours);
            var allFreeHours = projectAllocation.Sum(x => x.FreeHours);
            var allHours = allFreeHours + allWorkingHours;
            
            decimal atWorkRatio;
            if (projectAllocation.Any() && allHours != 0)
            {
                atWorkRatio = allWorkingHours / allHours;
            }
            else
            {
                var isWorkDay = _calendarService.IsWorkday(day, employmentPeriod.CalendarId ?? calendarId);
                atWorkRatio = isWorkDay ? 1m : 0m;
            }

            var isLeave = false;

            if ((day.DayOfWeek != DayOfWeek.Saturday) && (day.DayOfWeek != DayOfWeek.Sunday))
            {
                isLeave = employmentPeriod.TerminatedOn.HasValue && day >= employmentPeriod.TerminatedOn.Value;
            }

            return new EmployeAllocationDayData
            {
                Day = day,
                IsActive = employmentPeriod.IsActive,
                AvailableHours = atWorkRatio * WeeklyHoursHelper.GetHoursByDay(employmentPeriod, day.DayOfWeek),
                WorkingHours = allWorkingHours,
                FreeHours = allFreeHours,
                CalendarHours = atWorkRatio * FullTimeEquivalentDailyHours,
                Projects = projectAllocation,
                IsLeave = isLeave,
            };
        }

        private long GetCalendarIdByOrgUnitId(long? orgUnitId)
        {
            if (!orgUnitId.HasValue)
                return GetDefaultCalendarId();

            using (var unitOfWork = _allocationDbScope.Create())
            {
                var orgUnit = unitOfWork.Repositories.OrgUnits.FirstOrDefault(x => x.Id == orgUnitId.Value);
                if (orgUnit == null)
                    return GetDefaultCalendarId();
                while (orgUnit != null)
                {
                    if (orgUnit.Calendar != null)
                        return orgUnit.Calendar.Id;
                    orgUnit = orgUnit.Parent;
                }
                return GetDefaultCalendarId();
            }
        }

        private long GetDefaultCalendarId()
        {
            using (var scope = _configurationDbScope.Create())
            {
                var defaultCalendarCode = _systemParameters.GetParameter<string>(ParameterKeys.AllocationDefaultCalendarCode);
                var calendar = scope.Repositories.Calendars.FirstOrDefault(c => c.Code == defaultCalendarCode);
                if (calendar == null)
                    throw new ArgumentException("Unknown parameter", ParameterKeys.AllocationDefaultCalendarCode);
                return calendar.Id;
            }
        }

        private DateTime GetDefaultEndDate()
        {
            var offset = _systemParameters.GetParameter<int>(ParameterKeys.AllocationEvaluationDefaultEndMonthOffset);
            return _timeService.GetCurrentDate().AddMonths(offset);
        }

        private DateTime GetDefaultRebuildDate()
        {
            var offset = _systemParameters.GetParameter<int>(ParameterKeys.AllocationEvaluationDefaultRebuildMonthOffset);
            return _timeService.GetCurrentDate().AddMonths(offset);
        }
    }
}
