﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Filters;
using Smt.Atomic.Business.Allocation.Helpers;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Dictionaries.BusinessLogic;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{
    public class BenchEmployeeCardIndexDataService : CardIndexDataService<BenchEmployeeDto, Employee, IAllocationDbScope>, IBenchEmployeeCardIndexDataService
    {
        private readonly IOrgUnitService _orgUnitService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IBenchEmployeesService _benchEmployeesService;
        private readonly ITimeService _timeService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public DateTime CurrentWeek { get; private set; }

        public BenchEmployeeCardIndexDataService(ICardIndexServiceDependencies<IAllocationDbScope> dependencies,
            IBenchEmployeesService benchEmployeesService,
            ISystemParameterService systemParameterService,
            ITimeService timeService,
            IOrgUnitService orgUnitService,
            IPrincipalProvider principalProvider)
            : base(dependencies)
        {
            _benchEmployeesService = benchEmployeesService;
            _timeService = timeService;
            _systemParameterService = systemParameterService;
            _orgUnitService = orgUnitService;
            _principalProvider = principalProvider;
        }

        protected override IEnumerable<Expression<Func<Employee, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            if (searchCriteria.Includes(SearchAreaCodesBenchEmployee.FirstName))
            {
                yield return a => a.FirstName;
            }

            if (searchCriteria.Includes(SearchAreaCodesBenchEmployee.LastName))
            {
                yield return a => a.LastName;
            }

            if (searchCriteria.Includes(SearchAreaCodesBenchEmployee.EmployeeStatus))
            {
                yield return a => a.CurrentEmployeeStatus;
            }

            if (searchCriteria.Includes(SearchAreaCodesBenchEmployee.LocationName))
            {
                yield return a => a.Location.Name;
            }

            if (searchCriteria.Includes(SearchAreaCodesBenchEmployee.LineMaganer))
            {
                yield return a => a.LineManager.FirstName;
                yield return a => a.LineManager.LastName;
            }

            if (searchCriteria.Includes(SearchAreaCodesBenchEmployee.OrganizationUnit))
            {
                yield return a => a.OrgUnit.Name;
            }

            if (searchCriteria.Includes(SearchAreaCodesBenchEmployee.Skills))
            {
                yield return a => a.Resumes.SelectMany(r => r.ResumeTechnicalSkills).Select(s => s.TechnicalSkill.Name);
            }

            if (searchCriteria.Includes(SearchAreaCodesBenchEmployee.JobProfileName))
            {
                yield return a => a.JobProfiles.Select(jp => jp.Name);
            }

            if (searchCriteria.Includes(SearchAreaCodesBenchEmployee.JobTitle))
            {
                yield return a => a.JobTitle.NameEn;
                yield return a => a.JobTitle.NamePl;
            }
        }

        protected override Expression<Func<Employee, bool>> AlterSearchPhraseCondition(SearchCriteria searchCriteria, Expression<Func<Employee, bool>> condition, string word)
        {
            var result = condition;

            if (searchCriteria.Includes(SearchAreaCodesBenchEmployee.StaffingStatus))
            {
                var weekStatusTypeFilter = Enum.GetValues(typeof(StaffingStatus))
                    .Cast<StaffingStatus>().Where(type => type.GetDescriptionOrValue().ToLower().Contains(word)).ToList();

                if (weekStatusTypeFilter.Any())
                {
                    Expression<Func<Employee, bool>> weekAllocationStatusWhere = e => e.WeeklyAllocationStatuses
                        .Where(w => w.Week == CurrentWeek)
                        .Any(s => weekStatusTypeFilter.Contains(s.StaffingStatus));

                    result = result == null ? weekAllocationStatusWhere : result.Or(weekAllocationStatusWhere);
                }
            }

            return result;
        }

        protected override Expression<Func<Employee, bool>> GetCustomSearchExpression(IReadOnlyCollection<string> wordCollection)
        {
            return e => false;
        }

        protected override NamedFilters<Employee> NamedFilters
        {
            get
            {
                var consideredFuture = _systemParameterService.GetParameter<int>(ParameterKeys.NumberOfFutureMonths);
                var consideredFutureTime = CurrentWeek.AddMonths(consideredFuture);

                return new NamedFilters<Employee>(
                    new[] {
                        ProjectContributorFilter.EmployeeFilter,
                        WeekFilter.EmployeeFilter,
                        MyEmployeesFilter.GetDescendantEmployeeFilter(_orgUnitService, _principalProvider),
                        MyEmployeesFilter.GetChildEmployeeFilter(_orgUnitService, _principalProvider),
                        AllEmployeesFilter.EmployeeFilter,
                        LineManagerFilter.EmployeeFilter,
                        OrgUnitsFilter.EmployeeFilter,
                        SkillsAndKnowledgeFilter.EmployeeFilter,
                        LocationFilter.EmployeeFilter,
                        CompanyFilter.EmployeeFilter,
                        SkillsFilter.EmployeeFilter,
                        LanguageWithLevelBusinessLogic.EmployeeFilter,
                        new NamedFilter<Employee>(FilterCodes.AllocationStatusFreeFilter,
                                e => !e.AllocationRequests.Any(a => a.StartDate < consideredFutureTime && (!a.EndDate.HasValue || a.EndDate.Value > CurrentWeek))),
                        new NamedFilter<Employee>(FilterCodes.AllocationStatusBusyFilter,
                            e => e.AllocationRequests.Any(a => a.StartDate < consideredFutureTime && (!a.EndDate.HasValue || a.EndDate.Value > CurrentWeek)))}
                        .Union(CardIndexServiceHelper.BuildEnumBasedFilters<Employee, EmployeeStatus>(
                            e => e.WeeklyAllocationStatuses.FirstOrDefault(w => w.Week == CurrentWeek).EmployeeStatus))
                        .Union(
                            CardIndexServiceHelper.BuildEnumBasedFilters<Employee, EmploymentType>(
                                e => e.CurrentEmploymentType)));
            }
        }

        public override CardIndexRecords<BenchEmployeeDto> GetRecords(QueryCriteria criteria)
        {
            if (criteria.FilterObjects.ContainsKey(FilterCodes.WeekFilter))
            {
                var dateRangeFilterParameters = criteria.FilterObjects[FilterCodes.WeekFilter].GetFilterParameters();
                CurrentWeek = (DateTime)dateRangeFilterParameters[$"{FilterCodes.WeekFilter}.week"].Value;
            }
            else
            {
                CurrentWeek = _timeService.GetCurrentDate();
            }

            CurrentWeek = CurrentWeek.GetPreviousDayOfWeek(DayOfWeek.Monday);

            var result = base.GetRecords(criteria);

            _benchEmployeesService.RewriteBenchFields(result, CurrentWeek);

            return result;
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<Employee> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var direction = sortingCriterion.GetSortingDirection();

            if (sortingCriterion.GetSortingColumnName() == nameof(BenchEmployeeDto.WeeksOnBench))
            {
                orderedQueryBuilder.ApplySortingKey(WeeksOnBenchSortingExpression, direction);
            }
            else if (sortingCriterion.GetSortingColumnName() == nameof(EmployeeDto.JobProfileIds))
            {
                EmployeeSortHelper.OrderByJobProfile(orderedQueryBuilder, direction);
            }
            else if (sortingCriterion.GetSortingColumnName() == nameof(EmployeeDto.SkillIds))
            {
                EmployeeSortHelper.OrderBySkills(orderedQueryBuilder, direction);
            }
            else if (sortingCriterion.GetSortingColumnName() == nameof(EmployeeDto.JobMatrixIds))
            {
                EmployeeSortHelper.OrderByJobMatrices(orderedQueryBuilder, direction);
            }
            else if (sortingCriterion.GetSortingColumnName() == "CurrentUtilization")
            {
                orderedQueryBuilder.ApplySortingKey(AllocationPercentageExpression, direction);
                orderedQueryBuilder.ApplySortingKey(StaffingStatusExpression, direction);
            }
            else if (sortingCriterion.GetSortingColumnName() == nameof(BenchEmployeeDto.LastResumeModifiedOn))
            {
                EmployeeSortHelper.OrderByResumeModificationDate(orderedQueryBuilder, direction);
            }
            else if (sortingCriterion.GetSortingColumnName() == nameof(BenchEmployeeDto.FillPercentage))
            {
                EmployeeSortHelper.OrderByResumeFillPercetage(orderedQueryBuilder, direction);
            }
            else
            {
                base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
            }
        }

        private Expression<Func<Employee, decimal?>> AllocationPercentageExpression => e =>
            e.WeeklyAllocationStatuses
                .Where(v => v.Week == CurrentWeek)
                .Select(v => v.AvailableHours > decimal.Zero
                    ? (int)(100 * (v.AllocatedHours / v.AvailableHours))
                    : 0)
                .FirstOrDefault();

        private Expression<Func<Employee, StaffingStatus?>> StaffingStatusExpression => r =>
             r.WeeklyAllocationStatuses.Where(w => w.Week == CurrentWeek)
                .Select(w => w.StaffingStatus)
                .FirstOrDefault();

        private Expression<Func<Employee, DateTime?>> WeeksOnBenchSortingExpression => r =>
            r.WeeklyAllocationStatuses.Where(w => w.Week == CurrentWeek)
                .Select(w => w.OnBenchSince)
                .FirstOrDefault();
    }
}
