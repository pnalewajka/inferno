﻿using System.Linq;
using Castle.Core;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{
    [Interceptor(typeof(CacheInterceptor))]
    public class AllocationJobTitleService : IAllocationJobTitleService
    {
        private readonly IUnitOfWorkService<ISkillManagementDbScope> _unitOfWorkService;

        public AllocationJobTitleService(IUnitOfWorkService<ISkillManagementDbScope> unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        [Cached(CacheArea = CacheAreas.JobTitles, ExpirationInSeconds = 600)]
        public long? GetJobTitleIdByTitle(string title)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.JobTitles
                    .Where(c => c.NameEn == title || c.NamePl == title)
                    .Select(c => (long?)c.Id)
                    .SingleOrDefault();
            }
        }
    }
}