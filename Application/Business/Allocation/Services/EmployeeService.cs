﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Castle.Core;
using Castle.Core.Logging;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.ActiveDirectory.Dto;
using Smt.Atomic.Business.ActiveDirectory.Interfaces;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Helpers;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Allocation.Resources;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using SystemQueryableExtensions = System.Data.Entity.QueryableExtensions;

namespace Smt.Atomic.Business.Allocation.Services
{
    [Interceptor(typeof(CacheInterceptor))]
    public class EmployeeService : IEmployeeService
    {
        private readonly IAllocationJobTitleService _allocationJobTitleService;
        private readonly IUnitOfWorkService<IAllocationDbScope> _unitOfWorkService;
        private readonly IClassMapping<Employee, EmployeeDto> _employeeToEmployeeDtoClassMapping;
        private readonly IClassMapping<EmployeeDto, Employee> _employeeDtoToEmployeeClassMapping;
        private readonly IActiveDirectoryWriter _activeDirectoryWriter;
        private readonly IOrgUnitService _orgUnitService;
        private readonly ITimeService _timeService;
        private readonly IReportingService _reportingService;
        private readonly IDanteCalendarService _danteCalendarService;
        private readonly ILogger _logger;
        private readonly IDataActivityLogService _dataActivityLogService;
        private readonly IEmploymentPeriodService _employmentPeriodService;
        private readonly IPrincipalProvider PrincipalProvider;

        public EmployeeService(
            IAllocationJobTitleService allocationJobTitleService,
            IUnitOfWorkService<IAllocationDbScope> unitOfWorkService,
            IClassMapping<Employee, EmployeeDto> employeeToEmployeeDtoClassMapping,
            IClassMapping<EmployeeDto, Employee> employeeDtoToEmployeeClassMapping,
            IDanteCalendarService danteCalendarService,
            IActiveDirectoryWriter activeDirectoryWriter,
            IOrgUnitService orgUnitService,
            ITimeService timeService,
            ILogger logger,
            IReportingService reportingService,
            IDataActivityLogService dataActivityLogService,
            IEmploymentPeriodService employmentPeriodService,
            IPrincipalProvider principalProvider)
        {
            _allocationJobTitleService = allocationJobTitleService;
            _unitOfWorkService = unitOfWorkService;
            _employeeDtoToEmployeeClassMapping = employeeDtoToEmployeeClassMapping;
            _activeDirectoryWriter = activeDirectoryWriter;
            _orgUnitService = orgUnitService;
            _timeService = timeService;
            _logger = logger;
            _employeeToEmployeeDtoClassMapping = employeeToEmployeeDtoClassMapping;
            _reportingService = reportingService;
            _danteCalendarService = danteCalendarService;
            _dataActivityLogService = dataActivityLogService;
            _employmentPeriodService = employmentPeriodService;
            PrincipalProvider = principalProvider;
        }

        public EmployeeDto GetEmployeeOrDefaultByFullName(string fullName)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employees = unitOfWork.Repositories.Employees
                    .Where(e => e.FirstName.ToLower() + " " + e.LastName.ToLower() == fullName.ToLower())
                    .AsNoTracking()
                    .ToArray();

                if (employees.Length != 1)
                {
                    return null;
                }

                var employeeDto = _employeeToEmployeeDtoClassMapping.CreateFromSource(employees[0]);

                return employeeDto;
            }
        }


        public EmployeeDto GetEmployeeOrDefaultByEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return null;
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employee = unitOfWork.Repositories.Employees.FirstOrDefault(e => e.Email.ToLower() == email.ToLower());
                if (employee == null)
                {
                    return null;
                }

                var employeeDto = _employeeToEmployeeDtoClassMapping.CreateFromSource(employee);

                return employeeDto;
            }
        }

        public EmployeeDto GetEmployeeOrDefaultByAcronym(string acronym)
        {
            if (string.IsNullOrEmpty(acronym))
            {
                return null;
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employee = unitOfWork.Repositories.Employees.SingleOrDefault(e => e.Acronym.ToLower() == acronym.ToLower());

                if (employee == null)
                {
                    return null;
                }

                var employeeDto = _employeeToEmployeeDtoClassMapping.CreateFromSource(employee);

                return employeeDto;
            }
        }

        public IEnumerable<EmployeeDto> GetEmployeesOrDefaultByAcronyms(IEnumerable<string> acronyms)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employees = unitOfWork.Repositories.Employees.Where(e => acronyms.Select(a => a.ToLower()).Contains(e.Acronym.ToLower()));

                var employeesDto = employees.Select(_employeeToEmployeeDtoClassMapping.CreateFromSource).ToList();

                return employeesDto;
            }
        }

        public EmployeeDto GetEmployeeOrDefaultByUserId(long userId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employee = unitOfWork.Repositories.Employees.SingleOrDefault(e => e.UserId == userId);

                return employee != null
                     ? _employeeToEmployeeDtoClassMapping.CreateFromSource(employee)
                     : null;
            }
        }

        public long? GetEmployeeIdOrDefaultByUserId(long userId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Employees
                    .Where(e => e.UserId == userId)
                    .Select(e => (long?)e.Id)
                    .SingleOrDefault();
            }
        }

        public long CreateEmployee(EmployeeDto employeeDto)
        {
            var employeeEntity = _employeeDtoToEmployeeClassMapping.CreateFromSource(employeeDto);

            using (var unitOfWork = _unitOfWorkService.Create())
            {

                if (employeeDto.JobMatrixIds != null && employeeDto.JobMatrixIds.Any())
                {
                    var employeeJobMatrixLevelId = employeeDto.JobMatrixIds.First();
                    var jobMatrixLevel = unitOfWork.Repositories.JobMatrixLevels.First(j => j.Id == employeeJobMatrixLevelId);
                    employeeEntity.JobMatrixs = new List<JobMatrixLevel> { jobMatrixLevel };
                }

                if (employeeDto.JobProfileIds != null && employeeDto.JobProfileIds.Any())
                {
                    var employeeJobProfileId = employeeDto.JobProfileIds.First();
                    var jobProfile = unitOfWork.Repositories.JobProfiles.First(j => j.Id == employeeJobProfileId);
                    employeeEntity.JobProfiles = new List<JobProfile> { jobProfile };
                }

                unitOfWork.Repositories.Employees.Add(employeeEntity);
                unitOfWork.Commit();

                _dataActivityLogService.OnEmployeeAdded(employeeEntity.Id);

                return employeeEntity.Id;
            }
        }

        public void SynchronizeWithActiveDirectory(long employeeId, ActiveDirectoryUserDto activeDirectoryUserDto)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employee = unitOfWork.Repositories.Employees.GetById(employeeId);
                var isOrgUnitChanged = activeDirectoryUserDto.OrgUnitId != employee.OrgUnitId;

                employee.FirstName = activeDirectoryUserDto.FirstName;
                employee.LastName = activeDirectoryUserDto.LastName;
                employee.Email = activeDirectoryUserDto.Email;
                employee.JobTitleId = _allocationJobTitleService.GetJobTitleIdByTitle(activeDirectoryUserDto.Title);
                employee.CompanyId = activeDirectoryUserDto.CompanyId;
                employee.LocationId = activeDirectoryUserDto.LocationId ?? employee.LocationId;
                employee.PlaceOfWork = activeDirectoryUserDto.PlaceOfWork;
                employee.OrgUnitId = activeDirectoryUserDto.OrgUnitId;
                employee.Acronym = activeDirectoryUserDto.Acronym;

                if (activeDirectoryUserDto.JobMatrixLevelId.HasValue && (employee.JobMatrixs.Count > 1 || employee.JobMatrixs.All(x => x.Id != activeDirectoryUserDto.JobMatrixLevelId.Value)))
                {
                    var jobMatrixLevel = unitOfWork.Repositories.JobMatrixLevels.First(j => j.Id == activeDirectoryUserDto.JobMatrixLevelId.Value);
                    employee.JobMatrixs.Clear();
                    employee.JobMatrixs.Add(jobMatrixLevel);
                }

                if (activeDirectoryUserDto.JobProfileId.HasValue &&
                    employee.JobProfiles.All(x => x.Id != activeDirectoryUserDto.JobProfileId.Value))
                {
                    var jobProfile = unitOfWork.Repositories.JobProfiles.First(j => j.Id == activeDirectoryUserDto.JobProfileId.Value);
                    employee.JobProfiles.Add(jobProfile);
                }

                if (isOrgUnitChanged)
                {
                    employee.IsProjectContributor = activeDirectoryUserDto.IsProjectContributor;
                }

                unitOfWork.Commit();
            }
        }

        public void DeactivateEmployeeMissingInActiveDirectory(long employeeId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employee = unitOfWork.Repositories.Employees.GetById(employeeId);
                employee.Email = null;
                unitOfWork.Commit();
            }
        }

        public void SetManager(long employeeId, long? employeeIdLineManager)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employee = unitOfWork.Repositories.Employees.GetById(employeeId);

                if (employee.LineManagerId != employeeIdLineManager)
                {
                    employee.LineManagerId = employeeIdLineManager;
                    unitOfWork.Commit();
                }
            }
        }

        public void UpdateCurrentEmployeeStatus(long employeeId)
        {
            var currentDate = _timeService.GetCurrentDate();

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employee = unitOfWork.Repositories.Employees.Include(nameof(Employee.EmploymentPeriods)).GetById(employeeId);

                var newStatus = EmployeeStatusHelper.ComputeEmployeeStatus(employee, currentDate);

                if (newStatus != employee.CurrentEmployeeStatus)
                {
                    employee.CurrentEmployeeStatus = newStatus;

                    _logger.InfoFormat("Status for employee {0} (id={1}) updated to {2}", employee.FullName, employeeId, newStatus);

                    unitOfWork.Commit();

                    _dataActivityLogService.LogEmployeeDataActivity(employee.Id, ActivityType.UpdatedRecord);
                }
            }
        }

        public void UpdateEmployeeBasedOnEmploymentPeriod(long employeeId)
        {
            var currentDate = _timeService.GetCurrentDate();

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employeeChanged = false;
                var employee =
                    SystemQueryableExtensions.Include(SystemQueryableExtensions.Include(SystemQueryableExtensions.Include(
                        unitOfWork.Repositories.Employees,
                        e => e.EmploymentPeriods),
                        e => e.EmploymentPeriods.Select(p => p.ContractType)),
                        e => e.JobMatrixs)
                    .GetById(employeeId);
                var currentEmploymentPeriod = employee.GetCurrentEmploymentPeriod();

                if (currentEmploymentPeriod != null)
                {
                    if (currentEmploymentPeriod.ContractType.EmploymentType != employee.CurrentEmploymentType)
                    {
                        employeeChanged = true;
                        employee.CurrentEmploymentType = currentEmploymentPeriod.ContractType.EmploymentType;

                        _logger.InfoFormat("Current employment type of employee {0} (id={1}) updated to {2}",
                            employee.FullName, employeeId, currentEmploymentPeriod.ContractType.EmploymentType);
                    }

                    if (currentEmploymentPeriod.CompanyId.HasValue && currentEmploymentPeriod.CompanyId != employee.CompanyId)
                    {
                        employeeChanged = true;
                        employee.CompanyId = currentEmploymentPeriod.CompanyId;

                        _logger.InfoFormat("Company of employee {0} (id={1}) updated to {2}", employee.FullName, employeeId, currentEmploymentPeriod.Company.Name);
                    }

                    if (currentEmploymentPeriod.JobTitleId.HasValue && currentEmploymentPeriod.JobTitleId != employee.JobTitleId)
                    {
                        employeeChanged = true;
                        employee.JobTitleId = currentEmploymentPeriod.JobTitleId;
                        employee.JobMatrixs = GetJobMatrixLevels(unitOfWork, currentEmploymentPeriod.JobTitleId);

                        _logger.InfoFormat("Job title of employee {0} (id={1}) updated to {2}", employee.FullName, employeeId, currentEmploymentPeriod.JobTitle.NameEn);
                    }

                    if (currentEmploymentPeriod.LocationId.HasValue && currentEmploymentPeriod.LocationId != employee.LocationId)
                    {
                        employeeChanged = true;
                        employee.LocationId = currentEmploymentPeriod.LocationId;

                        _logger.InfoFormat("Location of employee {0} (id={1}) updated to {2}", employee.FullName, employeeId, currentEmploymentPeriod.Location.Name);
                    }
                }

                if (employeeChanged)
                {
                    ModifyActiveDirectoryUser(employee);
                    unitOfWork.Commit();

                    _dataActivityLogService.LogEmployeeDataActivity(employee.Id, ActivityType.UpdatedRecord);
                }
            }
        }

        private void ModifyActiveDirectoryUser(Employee employee)
        {
            try
            {
                var activeDirectoryUserChangeDescription = new ActiveDirectoryUserChangeDescriptionDto
                {
                    EmployeeId = employee.Id,
                    CompanyId = employee.CompanyId,
                    JobTitleId = employee.JobTitleId,
                    ProfileType = employee.CurrentEmploymentType,
                    JobMatrixLevelIds = employee.JobMatrixs.Select(m => m.Id).ToArray(),
                    LocationId = employee.LocationId
                };

                _activeDirectoryWriter.ModifyActiveDirectoryUser(activeDirectoryUserChangeDescription);
            }
            catch (Exception exception)
            {
                _logger.Error("Problem with saving changes to Active Directory", exception);
            }
        }

        private static ICollection<JobMatrixLevel> GetJobMatrixLevels(IUnitOfWork<IAllocationDbScope> unitOfWork, long? jobTitleId)
        {
            var result = new List<JobMatrixLevel>();

            if (jobTitleId.HasValue)
            {
                var jobMatrixLevel = unitOfWork.Repositories.JobTitles.Where(t => t.Id == jobTitleId).Select(t => t.JobMatrixLevel).FirstOrDefault();

                if (jobMatrixLevel != null)
                {
                    result.Add(jobMatrixLevel);
                }
            }

            return result;
        }

        public IList<EmployeeDto> GetEmployeesById(IEnumerable<long> ids)
        {
            return GetEmployees(e => ids.Contains(e.Id));
        }

        public long? GetEmployeeIdByUserId(long userId)
        {
            return GetEmployeeIds(e => e.UserId == userId).SingleOrDefault();
        }

        public IList<long> GetEmployeeIdsByUserIds(IEnumerable<long> userIds)
        {
            return GetEmployeeIds(e => e.UserId.HasValue && userIds.Contains(e.UserId.Value));
        }

        public IList<EmployeeDto> GetNotTerminatedEmployeesById(IEnumerable<long> ids)
        {
            return GetEmployees(e => e.CurrentEmployeeStatus != EmployeeStatus.Terminated && ids.Contains(e.Id));
        }

        public IList<long> GetEmployeeIds(Expression<Func<Employee, bool>> predicate)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork
                    .Repositories
                    .Employees
                    .Where(predicate).Select(e => e.Id)
                    .ToList();
            }
        }

        public IList<EmployeeDto> GetEmployees(Expression<Func<Employee, bool>> predicate)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork
                    .Repositories
                    .Employees
                    .Where(predicate)
                    .AsEnumerable()
                    .Select(_employeeToEmployeeDtoClassMapping.CreateFromSource)
                    .ToList();
            }
        }

        public EmployeeDto GetEmployeeOrDefaultById(long employeeId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employee = unitOfWork.Repositories.Employees.GetByIdOrDefault(employeeId);

                return employee != null
                     ? _employeeToEmployeeDtoClassMapping.CreateFromSource(employee)
                     : null;
            }
        }

        public EmployeeDto GetEmployeeOrDefaultByLogin(string login)
        {
            var employee = GetEmployeesOrDefaultByLogins(new[] { login }).SingleOrDefault();

            return employee;
        }

        public IList<EmployeeDto> GetEmployeesOrDefaultByLogins(IEnumerable<string> logins)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employees = unitOfWork.Repositories.Employees.Where(e => e.UserId.HasValue && logins.Contains(e.User.Login)).ToList();

                return employees.Select(e => e != null ? _employeeToEmployeeDtoClassMapping.CreateFromSource(e) : null).ToList();
            }
        }

        public IList<long> GetEmployeeIdsByLogins(IEnumerable<string> logins)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Employees
                    .Where(e => e.UserId.HasValue && logins.Contains(e.User.Login))
                    .Select(e => e.Id)
                    .ToList();
            }
        }

        [Cached(CacheArea = CacheAreas.HasEmployees, ExpirationInSeconds = 600)]
        public bool HasEmployees(long employeeId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Employees.Any(e =>
                    (e.LineManagerId.HasValue && e.LineManagerId == employeeId)
                    || (e.OrgUnit.EmployeeId.HasValue && e.OrgUnit.EmployeeId == employeeId));
            }
        }

        public string GetUserLogin(long employeeId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Employees.GetById(employeeId).User.Login;
            }
        }

        public IList<EmployeeDto> GetEmployeesByLineManagerId(long id)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork
                    .Repositories
                    .Employees
                    .Where(e => e.LineManagerId == id)
                    .AsEnumerable()
                    .Select(e => _employeeToEmployeeDtoClassMapping.CreateFromSource(e))
                    .ToList();
            }
        }

        public IList<long> GetEmployeeUserIdsByLineManagerEmployeeId(long id)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork
                    .Repositories
                    .Employees
                    .Where(e => e.LineManagerId == id
                                && e.UserId != null)
                    .Select(e => e.UserId.Value)
                    .ToList();
            }
        }

        public IList<long> GetEmployeeIdsByLineManagerEmployeeId(long id)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork
                    .Repositories
                    .Employees
                    .Where(e => e.LineManagerId == id)
                    .GetIds()
                    .ToList();
            }
        }

        public long? GetEmployeeDirectManagerOrDefaultByOrgUnits(long? employeeId, OrgUnitManagerRole orgUnitManagerRole)
        {
            if (employeeId == null)
            {
                return null;
            }

            var employee = GetEmployeeOrDefaultById(employeeId.Value);

            if (employee == null)
            {
                return null;
            }

            foreach (var orgUnitDto in _orgUnitService.GetOrgUnitParents(employee.OrgUnitId))
            {
                if (orgUnitDto.EmployeeId != null && orgUnitDto.OrgUnitManagerRole == orgUnitManagerRole)
                {
                    return orgUnitDto.EmployeeId;
                }
            }

            if (employee.LineManagerId != null)
            {
                return GetEmployeeDirectManagerOrDefaultByOrgUnits(employee.LineManagerId, orgUnitManagerRole);
            }

            return null;
        }

        public IList<long> GetLineManagersWithNotTerminatedChildrenIds()
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Employees
                    .Where(e => e.LineManagerId.HasValue && e.CurrentEmployeeStatus != EmployeeStatus.Terminated)
                    .Select(e => e.LineManagerId.Value)
                    .Distinct()
                    .ToList();
            }
        }

        public bool HasEmployeesByLineManagerId(long lineManagerId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Employees
                    .Any(e => e.LineManagerId == lineManagerId);
            }
        }

        public IList<long> GetEmployeeIds(IReadOnlyList<long> userIds)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Employees
                    .Where(p => userIds.Contains(p.UserId.Value))
                    .Select(e => e.Id).ToList();
            }
        }

        public IDictionary<long, long> GetUserIdToEmployeeIdDictionary(IReadOnlyCollection<long> userIds)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories
                    .Employees
                    .Where(e => e.UserId.HasValue && userIds.Contains(e.UserId.Value))
                    .ToDictionary(e => e.UserId.Value, e => e.Id);
            }
        }

        public ICollection<long> GetUserIdsByEmployeeIds(params long[] employeeIds)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories
                    .Employees
                    .Where(e => employeeIds.Contains(e.Id))
                    .Where(e => e.UserId.HasValue)
                    .Select(e => e.UserId.Value)
                    .ToArray();
            }
        }

        public IList<EmployeeCalendarInfoDto> GetCalendarInfoByEmployeeIds(IEnumerable<long> employeeIds, DateTime from, DateTime to)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employeeCalendarInfos = unitOfWork.Repositories.EmploymentPeriods
                    .Where(p => employeeIds.Contains(p.EmployeeId)
                        && p.CalendarId != null
                        && p.StartDate <= to && (!p.EndDate.HasValue || p.EndDate >= from))
                    .Select(r => new EmployeeCalendarInfoDto
                    {
                        CalendarId = r.CalendarId.Value,
                        EmployeeId = r.EmployeeId,
                        StartDate = r.StartDate,
                        EndDate = r.EndDate
                    })
                    .ToList();

                var employeeIdsWithoutCalendar = employeeIds.Where(id => !employeeCalendarInfos.Any(p => p.EmployeeId == id));
                var employeeWithoutCalendarOrgUnits = unitOfWork.Repositories.Employees
                    .Where(e => employeeIdsWithoutCalendar.Contains(e.Id))
                    .Select(e => new { e.Id, e.OrgUnitId })
                    .AsEnumerable()
                    .Select(r => new EmployeeCalendarInfoDto
                    {
                        EmployeeId = r.Id,
                        CalendarId = _danteCalendarService.GetCalendarIdByOrgUnitId(r.OrgUnitId)
                    })
                    .ToList();

                employeeCalendarInfos.AddRange(employeeWithoutCalendarOrgUnits);

                return employeeCalendarInfos;
            }
        }

        public bool IsEmployeeOfManager(long employeeId, long managerId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employee = unitOfWork.Repositories.Employees.GetById(employeeId);

                return EmployeeBusinessLogic.IsEmployeeOf.Call(
                    employee,
                    managerId,
                    _orgUnitService.GetManagerOrgUnitDescendantIds(managerId));
            }
        }

        public long GetEmployeeCompanyId(long employeeId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var companyId = unitOfWork.Repositories.Employees.Single(e => e.Id == employeeId).CompanyId;

                if (!companyId.HasValue)
                {
                    throw new BusinessException(AlertResources.CannotFindEmployeeCompany);
                }

                return companyId.Value;
            }
        }

        [Cached(ExpirationInSeconds = 300, CacheArea = CacheAreas.EmployeeDeductibleCost)]
        public bool IsDeductibleCostEnabledForEmployee(long employeeId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var currentDate = _timeService.GetCurrentDate();
                var beginningOfMonth = DateHelper.BeginningOfMonth(currentDate.Year, (byte)currentDate.Month);
                var endOfMonth = DateHelper.EndOfMonth(currentDate.Year, (byte)currentDate.Month);
                var employmentPeriod = _employmentPeriodService
                    .GetEmploymentPeriodsInDateRange(employeeId, beginningOfMonth, endOfMonth)
                    .OrderBy(p => p.StartDate)
                    .FirstOrDefault();

                return PrincipalProvider.Current.IsInRole(SecurityRoleType.CanRequestTaxDeductibleCost)
                    && (employmentPeriod?.IsTaxDeductibleCostEnabled ?? false);
            }
        }
    }
}