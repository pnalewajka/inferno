﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Allocation.BusinessEvents;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.DocumentMappings;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Allocation.Resources;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Dto;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{
    public class ProjectCardIndexDataService : CardIndexDataService<ProjectDto, Project, IAllocationDbScope>, IProjectCardIndexDataService
    {
        private readonly IReadOnlyUnitOfWorkService<IAllocationDbScope> _unitOfWorkAllocationService;
        private readonly ITimeService _timeService;
        private readonly IProjectService _projectService;
        private readonly IOrgUnitService _orgUnitService;
        private readonly IUploadedDocumentHandlingService _uploadedDocumentHandlingService;
        private readonly IClassMapping<ProjectInvoicingRateDto, ProjectInvoicingRate> _rateDtoToEntityMapping;
        private readonly IClassMapping<ProjectDto, ProjectSetup> _dtoToSetup;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public ProjectCardIndexDataService(ICardIndexServiceDependencies<IAllocationDbScope> dependencies,
            IUploadedDocumentHandlingService uploadedDocumentHandlingService,
            IOrgUnitService orgUnitService,
            IProjectService projectService,
            IReadOnlyUnitOfWorkService<IAllocationDbScope> unitOfWorkAllocationService,
            ITimeService timeService)
            : base(dependencies)
        {
            _uploadedDocumentHandlingService = uploadedDocumentHandlingService;
            _unitOfWorkAllocationService = unitOfWorkAllocationService;
            _orgUnitService = orgUnitService;
            _timeService = timeService;
            _projectService = projectService;
            _rateDtoToEntityMapping = dependencies.ClassMappingFactory.CreateMapping<ProjectInvoicingRateDto, ProjectInvoicingRate>();
            _dtoToSetup = dependencies.ClassMappingFactory.CreateMapping<ProjectDto, ProjectSetup>();
        }

        protected override IEnumerable<BusinessLogic<Project, int>> GetSearchRelevanceFactors(QueryCriteria criteria)
        {
            yield return DynamicQueryHelper.CreateRelevanceFactor<Project>(
                score: 2,
                predicate: p => (p.SubProjectShortName != null && p.SubProjectShortName.StartsWith(criteria.SearchPhrase)));

            yield return DynamicQueryHelper.CreateRelevanceFactor<Project>(
                score: 1,
                predicate: p => (p.ProjectSetup.ProjectShortName.StartsWith(criteria.SearchPhrase)));
        }

        protected override IOrderedQueryable<Project> GetDefaultOrderBy(IQueryable<Project> records, QueryCriteria criteria)
        {
            if (!criteria.SearchPhrase.IsNullOrEmpty())
            {
                return DynamicQueryHelper.OrderByRelevanceFactors(records, GetSearchRelevanceFactors(criteria));
            }

            return base.GetDefaultOrderBy(records, criteria);
        }

        protected override IEnumerable<Expression<Func<Project, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            if (searchCriteria.Includes(SearchAreaCodes.ClientShortName))
            {
                yield return u => u.ProjectSetup.ClientShortName;
            }

            if (searchCriteria.Includes(SearchAreaCodes.ProjectAcronym))
            {
                yield return u => u.Acronym;
            }

            if (searchCriteria.Includes(SearchAreaCodes.ProjectShortName))
            {
                yield return u => u.SubProjectShortName;
                yield return u => u.ProjectSetup.GenericName;
                yield return u => u.ProjectSetup.ProjectShortName;
            }

            if (searchCriteria.Includes(SearchAreaCodes.Description))
            {
                yield return u => u.Description;
            }

            if (searchCriteria.Includes(SearchAreaCodes.SalesAccountManager))
            {
                yield return u => u.ProjectSetup.SalesAccountManager.FirstName;
                yield return u => u.ProjectSetup.SalesAccountManager.LastName;
            }

            if (searchCriteria.Includes(SearchAreaCodes.ProjectManager))
            {
                yield return u => u.ProjectSetup.ProjectManager.FirstName;
                yield return u => u.ProjectSetup.ProjectManager.LastName;
            }

            if (searchCriteria.Includes(SearchAreaCodes.OrgUnit))
            {
                yield return u => u.ProjectSetup.OrgUnit.Name;
            }

            if (searchCriteria.Includes(SearchAreaCodes.JiraKey))
            {
                yield return u => u.JiraKey;
            }

            if (searchCriteria.Includes(SearchAreaCodes.Apn))
            {
                yield return u => u.Apn;
            }

            if (searchCriteria.Includes(SearchAreaCodes.KupferwerkProjectId))
            {
                yield return u => u.KupferwerkProjectId;
            }

            if (searchCriteria.Includes(SearchAreaCodes.Region))
            {
                yield return u => u.ProjectSetup.Region;
            }
        }

        public override ProjectDto GetDefaultNewRecord()
        {
            var newRecord = base.GetDefaultNewRecord();

            newRecord.IsMainProject = true;
            newRecord.StartDate = _timeService.GetCurrentDate();
            newRecord.OrgUnitId = _orgUnitService.DefaultOrgUnit().Id;
            newRecord.JiraIssueToTimeReportRowMapperId = _projectService.GetDefaultJiraIssueToTimeReportRowMapperId();

            return newRecord;
        }

        protected override void InitializeNavigationProperties(Project entity)
        {
            base.InitializeNavigationProperties(entity);

            if (entity.ProjectSetup == null)
            {
                entity.ProjectSetup = new ProjectSetup();
            }
        }

        public override IQueryable<Project> ApplyContextFiltering(IQueryable<Project> records, object context)
        {
            if (context is ProjectPickerContext projectContext)
            {
                if (projectContext.ProjectStatus != null)
                {
                    records = records.Where(p => p.ProjectSetup.ProjectStatus == projectContext.ProjectStatus);
                }

                if (projectContext.OnlyActiveProjects)
                {
                    var currentDate = TimeService.GetCurrentDate();

                    records = records.Where(DateRangeHelper.IsDateInRange<Project>(p => p.StartDate.Value, p => p.EndDate, currentDate));
                }

                if (projectContext.OnlyProjectsWithKey)
                {
                    records = records.Where(ProjectBusinessLogic.IsProjectWithKey);
                }

                if (projectContext.OnlyMainProjects)
                {
                    records = records.Where(p => p.IsMainProject);
                }

                if (projectContext.OnlySubProjects)
                {
                    records = records.Where(ProjectBusinessLogic.IsSubProject);
                }

                if (projectContext.ProjectAllowedBonuses != BonusTypeSet.None)
                {
                    records = records.Where(p => p.AllowedBonusTypes == projectContext.ProjectAllowedBonuses);
                }
            }
            else
            {
                // List view: display only main projects
                records = records.Where(p => p.IsMainProject);
            }

            return base.ApplyContextFiltering(records, context);
        }

        protected override NamedFilters<Project> NamedFilters
        {
            get
            {
                var principalProvider = ReflectionHelper.ResolveInterface<IPrincipalProvider>();
                var employeeId = principalProvider.Current.EmployeeId;
                var managerOrgUnitsIds = _orgUnitService.GetManagerOrgUnitDescendantIds(employeeId);
                var currentDate = _timeService.GetCurrentDate();

                return new NamedFilters<Project>(new[]
                {
                    new NamedFilter<Project>(FilterCodes.ActiveProjects, p => !p.EndDate.HasValue || p.EndDate.Value >= currentDate),

                    new NamedFilter<Project>(FilterCodes.InactiveProjects, p => p.EndDate.HasValue && p.EndDate < currentDate),

                    new NamedFilter<Project, DateTime, DateTime>(
                        FilterCodes.DateRange,
                        (a, from, to) => a.StartDate >= from && (a.EndDate == null || (to != null && a.EndDate != null && a.EndDate <= to))
                    ),

                    new NamedFilter<Project>(FilterCodes.MyProjects,
                        a =>
                            (a.ProjectSetup.ProjectManagerId.HasValue && a.ProjectSetup.ProjectManagerId == employeeId)
                            || (a.ProjectSetup.OrgUnit != null && managerOrgUnitsIds.Any(id => id == a.ProjectSetup.OrgUnitId))
                    ),

                    new NamedFilter<Project, long[]>(FilterCodes.OrgUnitsFilter, (p, orgUnitIds) => orgUnitIds.Contains(p.ProjectSetup.OrgUnitId)),

                    new NamedFilter<Project, BoolFilterOptionsEnum, BoolFilterOptionsEnum, BoolFilterOptionsEnum>(
                        FilterCodes.ProjectDisclosures,
                        (a, weCanTalkAboutClientFilterOption, weCanTalkAboutProjectFilterOption, weCanUseClientLogoFilterOption) =>
                            (((weCanTalkAboutClientFilterOption == BoolFilterOptionsEnum.No && !a.ProjectSetup.Disclosures.HasFlag(ProjectDisclosures.WeCanTalkAboutClient))
                            || (weCanTalkAboutClientFilterOption == BoolFilterOptionsEnum.Yes && a.ProjectSetup.Disclosures.HasFlag(ProjectDisclosures.WeCanTalkAboutClient))
                            || weCanTalkAboutClientFilterOption == BoolFilterOptionsEnum.Either)
                            && ((weCanTalkAboutProjectFilterOption == BoolFilterOptionsEnum.No && !a.ProjectSetup.Disclosures.HasFlag(ProjectDisclosures.WeCanTalkAboutProject))
                            ||(weCanTalkAboutProjectFilterOption == BoolFilterOptionsEnum.Yes && a.ProjectSetup.Disclosures.HasFlag(ProjectDisclosures.WeCanTalkAboutProject))
                            || weCanTalkAboutProjectFilterOption == BoolFilterOptionsEnum.Either)
                            && ((weCanUseClientLogoFilterOption == BoolFilterOptionsEnum.No && !a.ProjectSetup.Disclosures.HasFlag(ProjectDisclosures.WeCanUseClientLogo))
                            || (weCanUseClientLogoFilterOption == BoolFilterOptionsEnum.Yes && a.ProjectSetup.Disclosures.HasFlag(ProjectDisclosures.WeCanUseClientLogo))
                            || weCanUseClientLogoFilterOption == BoolFilterOptionsEnum.Either))
                    ),

                    new NamedFilter<Project>(FilterCodes.AllProjects, a => true),

                    new NamedFilter<Project, long[]>(FilterCodes.TechnologyFilter, (p, technologyIds) =>
                        technologyIds.All(tid => p.ProjectSetup.Technologies.Any(t => t.Id == tid))),

                    new NamedFilter<Project, long[]>(FilterCodes.KeyTechnologyFilter, (p, keyTechnologyIds) =>
                        keyTechnologyIds.All(tid => p.ProjectSetup.KeyTechnologies.Any(t => t.Id == tid))),

                    new NamedFilter<Project>(FilterCodes.UnavailableForSalesPresentation, p => !p.ProjectSetup.IsAvailableForSalesPresentation),
                    new NamedFilter<Project>(FilterCodes.AvailableForSalesPresentation, p => p.ProjectSetup.IsAvailableForSalesPresentation),
                    new NamedFilter<Project>(FilterCodes.WithReferences, a => a.ProjectSetup.HasClientReferences ),
                    new NamedFilter<Project>(FilterCodes.WithoutReferences, a => !a.ProjectSetup.HasClientReferences),

                    new NamedFilter<Project, long[]>(
                        FilterCodes.Industries,
                        (a, industryIds) => a.ProjectSetup.Industries.Any(x=> industryIds.Contains( x.Id ))
                        ),


                    new NamedFilter<Project, long[]>(
                        FilterCodes.SoftwareCategories,
                        (a, softwareCategoryIds) => a.ProjectSetup.SoftwareCategories.Any(x => softwareCategoryIds.Contains(x.Id))
                    ),

                    new NamedFilter<Project, long[]>(FilterCodes.ServiceLineFilter, (p, serviceLineIds) =>
                        serviceLineIds.All(sid => p.ProjectSetup.ServiceLines.Any(x => x.Id == sid))),

                    new NamedFilter<Project, long>(FilterCodes.CustomerSizeFilter, (p, customerSizeId) => p.ProjectSetup.CustomerSizeId == customerSizeId),

                    new NamedFilter<Project>(FilterCodes.InfernoProjectsFilter, p => string.IsNullOrEmpty(p.JiraIssueKey)),

                    new NamedFilter<Project>(FilterCodes.JiraProjectsFilter, p => !string.IsNullOrEmpty(p.JiraIssueKey))
                }
                .Union(CardIndexServiceHelper.BuildEnumBasedFilters<Project, ProjectStatus>(e => e.ProjectSetup.ProjectStatus))
                .Union(CardIndexServiceHelper.BuildEnumBasedFilters<Project, ProjectPhase>(e => e.ProjectSetup.CurrentPhase)));
            }
        }

        protected override IQueryable<Project> ConfigureIncludes(IQueryable<Project> sourceQueryable)
        {
            return sourceQueryable
                .Include(x => x.ProjectSetup.OrgUnit)
                .Include(x => x.ProjectSetup.ProjectManager)
                .Include(x => x.ProjectSetup.Industries)
                .Include(x => x.ProjectSetup.Technologies)
                .Include(x => x.ProjectSetup.KeyTechnologies)
                .Include(x => x.ProjectSetup.SoftwareCategories)
                .Include(x => x.ProjectSetup.Tags)
                .Include(x => x.ProjectSetup.Picture)
                .Include(x => x.ProjectSetup.ServiceLines)
                .Include(x => x.ProjectSetup.Attachments)
                .Include(x => x.TimeReportAcceptingPersons)
                .Include(x => x.Attributes);
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<Project, ProjectDto, IAllocationDbScope> eventArgs)
        {
            SetGenericNameNullIfEmpty(eventArgs.InputDto);

            eventArgs.InputEntity.IsMainProject = eventArgs.InputDto.IsMainProject = true;
            eventArgs.InputEntity.ProjectSetup = _dtoToSetup.CreateFromSource(eventArgs.InputDto);

            TryUpdateSetup(eventArgs.UnitOfWork, eventArgs.InputDto, eventArgs.InputEntity, null);

            eventArgs.InputEntity.TimeReportAcceptingPersons = EntityMergingService.AttachEntities(eventArgs.UnitOfWork, eventArgs.InputEntity.TimeReportAcceptingPersons);
            eventArgs.InputEntity.Attributes = EntityMergingService.AttachEntities(eventArgs.UnitOfWork, eventArgs.InputEntity.Attributes);
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<Project, ProjectDto, IAllocationDbScope> eventArgs)
        {
            if (!eventArgs.DbEntity.IsMainProject)
            {
                throw new NotSupportedException("Trying to edit a subproject");
            }

            SetGenericNameNullIfEmpty(eventArgs.InputDto);

            eventArgs.InputDto.IsMainProject = eventArgs.InputEntity.IsMainProject = eventArgs.DbEntity.IsMainProject;

            eventArgs.InputDto.SetupId = eventArgs.InputEntity.ProjectSetupId = eventArgs.DbEntity.ProjectSetupId;
            eventArgs.InputEntity.ProjectSetup = _dtoToSetup.CreateFromSource(eventArgs.InputDto);

            var previousProjectStatus = eventArgs.DbEntity.ProjectSetup.ProjectStatus;

            if (previousProjectStatus == ProjectStatus.Fixed && eventArgs.InputDto.ProjectStatus == ProjectStatus.Planned)
            {
                eventArgs.Alerts.Add(new AlertDto
                {
                    Message = string.Format(AlertResources.FixedProjectTypeChangeAlert),
                    Type = AlertType.Error
                });
            }

            // Block setting StartsOn if there is past TimeReportRows without picked attributes
            // (only not accepted ones, for legacy data support)
            if (eventArgs.InputDto.ReportingStartsOn.HasValue
                && eventArgs.DbEntity.ReportingStartsOn != eventArgs.InputDto.ReportingStartsOn
                && eventArgs.InputDto.AttributesIds.Any())
            {
                var empoyeesWithInvalidRows = eventArgs.DbEntity.TimeReportRows
                    .Where(r => r.Status != TimeReportStatus.Accepted
                        && r.DailyEntries.Any(d => d.Day < eventArgs.InputDto.ReportingStartsOn.Value && d.Hours != 0)
                        && r.AttributeValues.Count != eventArgs.DbEntity.Attributes.Count)
                    .Select(r => new KeyValuePair<string, DateTime>(
                        r.TimeReport.Employee.DisplayName,
                        new DateTime(r.TimeReport.Year, r.TimeReport.Month, 1)))
                    .ToHashSet();

                foreach (var error in empoyeesWithInvalidRows)
                {
                    eventArgs.Alerts.Add(new AlertDto
                    {
                        Message = string.Format(AlertResources.ErrorReportingStartsOnNotSetAttributes, error.Key, error.Value.ToShortDateString()),
                        Type = AlertType.Error
                    });
                }
            }

            TryUpdateSetup(eventArgs.UnitOfWork, eventArgs.InputDto, eventArgs.InputEntity, eventArgs.DbEntity);

            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.TimeReportAcceptingPersons, eventArgs.InputEntity.TimeReportAcceptingPersons);
            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.Attributes, eventArgs.InputEntity.Attributes);
        }

        protected override void ApplyChanges(Project dbEntity, Project inputEntity)
        {
            base.ApplyChanges(dbEntity, inputEntity);

            EntityHelper.ShallowCopy(inputEntity.ProjectSetup, dbEntity.ProjectSetup);
        }

        protected override CardIndexEventArgs OnBulkEditing(BulkEditDto dto, object context = null)
        {
            var result = base.OnBulkEditing(dto, context);

            if (dto.PropertyName == nameof(ProjectDto.TimeTrackingType)
                && (TimeTrackingProjectType)dto.PropertyValue == TimeTrackingProjectType.Absence)
            {
                result.Canceled = true;
                result.AddError(AlertResources.BulkEditAbsenceTimeTrackingType);
            }

            return result;
        }

        private void TryUpdateSetup(IUnitOfWork<IAllocationDbScope> unitOfWork, ProjectDto inputProjectDto, Project inputProject, Project dbProject)
        {
            if (dbProject == null)
            {
                inputProject.ProjectSetup.Industries = EntityMergingService.AttachEntities(unitOfWork, inputProject.ProjectSetup.Industries);
                inputProject.ProjectSetup.Technologies = EntityMergingService.AttachEntities(unitOfWork, inputProject.ProjectSetup.Technologies);
                inputProject.ProjectSetup.KeyTechnologies = EntityMergingService.AttachEntities(unitOfWork, inputProject.ProjectSetup.KeyTechnologies);
                inputProject.ProjectSetup.ServiceLines = EntityMergingService.AttachEntities(unitOfWork, inputProject.ProjectSetup.ServiceLines);
                inputProject.ProjectSetup.SoftwareCategories = EntityMergingService.AttachEntities(unitOfWork, inputProject.ProjectSetup.SoftwareCategories);
                inputProject.ProjectSetup.Tags = EntityMergingService.AttachEntities(unitOfWork, inputProject.ProjectSetup.Tags);
                inputProject.ProjectSetup.InvoicingRates = EntityMergingService.AttachEntities(unitOfWork, inputProject.ProjectSetup.InvoicingRates);
            }

            (dbProject ?? inputProject).ProjectSetup.Picture = _uploadedDocumentHandlingService
                    .UpdateRelatedDocument(
                        unitOfWork,
                        (dbProject ?? inputProject).ProjectSetup.Picture,
                        inputProjectDto.Picture,
                        p => new ProjectPicture
                        {
                            Name = p.DocumentName,
                            ContentType = p.ContentType
                        }
                    );

            if (dbProject?.IsMainProject ?? false)
            {
                EntityMergingService.MergeCollections(unitOfWork, dbProject.ProjectSetup.Industries, inputProject.ProjectSetup.Industries);
                EntityMergingService.MergeCollections(unitOfWork, dbProject.ProjectSetup.Technologies, inputProject.ProjectSetup.Technologies);
                EntityMergingService.MergeCollections(unitOfWork, dbProject.ProjectSetup.KeyTechnologies, inputProject.ProjectSetup.KeyTechnologies);
                EntityMergingService.MergeCollections(unitOfWork, dbProject.ProjectSetup.ServiceLines, inputProject.ProjectSetup.ServiceLines);
                EntityMergingService.MergeCollections(unitOfWork, dbProject.ProjectSetup.SoftwareCategories, inputProject.ProjectSetup.SoftwareCategories);
                EntityMergingService.MergeCollections(unitOfWork, dbProject.ProjectSetup.Tags, inputProject.ProjectSetup.Tags);
                EntityMergingService.MergeCollections(unitOfWork, dbProject.ProjectSetup.InvoicingRates, inputProject.ProjectSetup.InvoicingRates, e => e.Id, true);

                // dbProject.ProjectSetup.Picture = inputProject.ProjectSetup.Picture;

                var temporaryReferenceMaterialAttachments = MergeAttachments(unitOfWork, dbProject.ProjectSetup.Attachments, inputProjectDto.ReferenceMaterialAttachments, ProjectAttachmentType.ReferenceMaterial);
                var temporaryProjectInformationAttachments = MergeAttachments(unitOfWork, dbProject.ProjectSetup.Attachments, inputProjectDto.ProjectInformationAttachments, ProjectAttachmentType.ProjectInformation);
                var temporaryOtherAttachments = MergeAttachments(unitOfWork, dbProject.ProjectSetup.Attachments, inputProjectDto.OtherAttachments, ProjectAttachmentType.Other);

                dbProject.ProjectSetup.Attachments = inputProject.ProjectSetup.Attachments =
                    dbProject.ProjectSetup.Attachments
                    .Concat(temporaryReferenceMaterialAttachments)
                    .Concat(temporaryProjectInformationAttachments)
                    .Concat(temporaryOtherAttachments).ToList();
            }
        }

        private static ICollection<TEntity> CreateForeignEntities<TEntity>(IEnumerable<long> ids)
            where TEntity : IEntity, new()
        {
            return ids?.Select(i => new TEntity { Id = i }).ToList();
        }

        private static ICollection<Industry> GetIndustries(IUnitOfWork<IAllocationDbScope> unitOfWork, ProjectDto projectDto)
        {
            return unitOfWork.Repositories.Industries.GetByIds(projectDto.IndustryIds).ToArray();
        }

        private static ICollection<SoftwareCategory> GetSoftwareCategories(IUnitOfWork<IAllocationDbScope> unitOfWork, ProjectDto projectDto)
        {
            return unitOfWork.Repositories.SoftwareCategories.GetByIds(projectDto.SoftwareCategoryIds).ToArray();
        }

        private static ICollection<ProjectTag> GetTags(IUnitOfWork<IAllocationDbScope> unitOfWork, ProjectDto projectDto)
        {
            return unitOfWork.Repositories.ProjectTags.GetByIds(projectDto.TagIds).ToArray();
        }

        private static ICollection<SkillTag> GetKeyTechnologies(IUnitOfWork<IAllocationDbScope> unitOfWork, ICollection<long> technologyIds)
        {
            return unitOfWork.Repositories.SkillTags
                .Where(t => t.TagType.HasFlag(SkillTagType.KeyTechnology) && t.Skills.Any(s => technologyIds.Contains(s.Id)))
                .ToArray();
        }

        private List<ProjectAttachment> MergeAttachments(IUnitOfWork<IAllocationDbScope> unitOfWork, ICollection<ProjectAttachment> dbAttachments, IEnumerable<DocumentDto> inputAttachments, ProjectAttachmentType projectAttachmentType)
        {
            var attachments = dbAttachments.Where(a => a.AttachmentType == projectAttachmentType).ToList();
            _uploadedDocumentHandlingService
                .MergeDocumentCollections(
                    unitOfWork,
                    attachments,
                    inputAttachments,
                    p => new ProjectAttachment
                    {
                        Name = p.DocumentName,
                        ContentType = p.ContentType,
                        AttachmentType = projectAttachmentType
                    }
                );
            var temporaryInputAttachments = attachments.Where(x => x.Id == 0).ToList();
            attachments.RemoveAll(x => x.Id == 0);

            return temporaryInputAttachments;
        }

        private void SetGenericNameNullIfEmpty(ProjectDto projectDto)
        {
            if (projectDto.GenericName == string.Empty)
            {
                projectDto.GenericName = null;
            }
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<Project, ProjectDto> recordAddedEventArgs)
        {
            if (recordAddedEventArgs.InputDto.Picture != null)
            {
                _uploadedDocumentHandlingService.PromoteTemporaryDocuments<ProjectPicture, ProjectPictureContent, ProjectPictureMapping, IAllocationDbScope>(new[] { recordAddedEventArgs.InputDto.Picture });
            }

            _uploadedDocumentHandlingService.PromoteTemporaryDocuments<ProjectAttachment, ProjectAttachmentContent, ProjectAttachmentMapping, IAllocationDbScope>(recordAddedEventArgs.InputDto.ReferenceMaterialAttachments);
            _uploadedDocumentHandlingService.PromoteTemporaryDocuments<ProjectAttachment, ProjectAttachmentContent, ProjectAttachmentMapping, IAllocationDbScope>(recordAddedEventArgs.InputDto.ProjectInformationAttachments);
            _uploadedDocumentHandlingService.PromoteTemporaryDocuments<ProjectAttachment, ProjectAttachmentContent, ProjectAttachmentMapping, IAllocationDbScope>(recordAddedEventArgs.InputDto.OtherAttachments);
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<Project, ProjectDto> editedEventArgs)
        {
            if (editedEventArgs.InputDto.Picture != null)
            {
                _uploadedDocumentHandlingService.PromoteTemporaryDocuments<ProjectPicture, ProjectPictureContent, ProjectPictureMapping, IAllocationDbScope>(new[] { editedEventArgs.InputDto.Picture });
            }

            _uploadedDocumentHandlingService.PromoteTemporaryDocuments<ProjectAttachment, ProjectAttachmentContent, ProjectAttachmentMapping, IAllocationDbScope>(editedEventArgs.InputDto.ReferenceMaterialAttachments);
            _uploadedDocumentHandlingService.PromoteTemporaryDocuments<ProjectAttachment, ProjectAttachmentContent, ProjectAttachmentMapping, IAllocationDbScope>(editedEventArgs.InputDto.ProjectInformationAttachments);
            _uploadedDocumentHandlingService.PromoteTemporaryDocuments<ProjectAttachment, ProjectAttachmentContent, ProjectAttachmentMapping, IAllocationDbScope>(editedEventArgs.InputDto.OtherAttachments);
        }

        protected override IEnumerable<BusinessEvent> GetEditRecordEvents(
            EditRecordResult result, ProjectDto newRecord, ProjectDto originalRecord)
        {
            var events = base.GetEditRecordEvents(result, newRecord, originalRecord);
            if (result.IsSuccessful)
            {
                if (newRecord.ProjectStatus != originalRecord.ProjectStatus)
                {
                    events = events.Concat(new[]
                    {
                        new DailyAllocationNeedsToRegenerateBusinessEvent
                        {
                            Scope = DailyAllocationNeedsToRegenerateScope.Project,
                            Projectid = result.EditedRecordId
                        }
                    });
                }
            }

            return events;
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<Project, IAllocationDbScope> eventArgs)
        {
            using (var unitOfWorkAllocationService = _unitOfWorkAllocationService.Create())
            {
                if (unitOfWorkAllocationService.Repositories.AllocationDailyRecords.Any(x => x.ProjectId == eventArgs.DeletingRecord.Id)
                    || unitOfWorkAllocationService.Repositories.AllocationRequests.Any(x => x.ProjectId == eventArgs.DeletingRecord.Id)
                    || unitOfWorkAllocationService.Repositories.StaffingDemands.Any(x => x.ProjectId == eventArgs.DeletingRecord.Id))
                {
                    eventArgs.Alerts.Add(new AlertDto
                    {
                        Message = string.Format(AlertResources.ProjectDeletingAlert, eventArgs.DeletingRecord.JiraIssueKey),
                        Type = AlertType.Error,
                    });
                }
            }
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<Project> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var direction = sortingCriterion.GetSortingDirection();

            switch (sortingCriterion.GetSortingColumnName())
            {
                case nameof(ProjectDto.IsJiraProject):
                {
                    orderedQueryBuilder.ApplySortingKey(r => r.JiraIssueKey, direction);
                    break;
                }

                case nameof(ProjectDto.ProjectName):
                {
                    orderedQueryBuilder.ApplySortingKey(ProjectBusinessLogic.ProjectName.AsExpression(), direction);
                    break;
                }

                default:
                {
                    base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
                    break;
                }
            }
        }
    }
}
