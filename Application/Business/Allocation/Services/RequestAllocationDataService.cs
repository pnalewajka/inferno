﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using System.Data.Entity;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Business.Allocation.Services
{
    internal class AllocationDailyDetails
    {
        public long AllocationRequestId { get; set; }
        public long EmployeeId { get; set; }
        public long ProjectId { get; set; }
        public string ProjectName { get; set; }
        public DateTime Day { get; set; }
        public decimal Hours { get; set; }
        public AllocationCertainty Certainty { get; set; }
        public bool IsWorkingDay { get; set; }
    }

    internal class RequestAllocationDataService
    {
        private readonly IReadOnlyUnitOfWorkService<IAllocationDbScope> _unitOfWorkAllocationService;
        private readonly IClassMappingFactory _classMappingFactory;
        private readonly IPrincipalProvider _principalProvider;
        public RequestAllocationDataService(
            IReadOnlyUnitOfWorkService<IAllocationDbScope> unitOfWorkAllocationService,
            IPrincipalProvider principalProvider,
            IClassMappingFactory classMappingFactory)
        {
            _unitOfWorkAllocationService = unitOfWorkAllocationService;
            _classMappingFactory = classMappingFactory;
            _principalProvider = principalProvider;
        }

        public IDictionary<long, EmployeeAllocationDto.AbsenceBlockDto[]> GetEmployeeAbsences(IReadOnlyCollection<long> employeeIds, DateTime from, DateTime to)
        {
            var canSeeAbsenceTaskName = _principalProvider.Current.IsInRole(SecurityRoleType.CanSeeAbsenceTaskName);

            using (var unitOfWork = _unitOfWorkAllocationService.Create())
            {
                var acceptableAbsenceStatuses = new[] { RequestStatus.Pending, RequestStatus.Approved, RequestStatus.Completed };

                var absences = unitOfWork.Repositories.AbsenceRequests.Include(r => r.Request)
                    .Where(r => r.AffectedUser.Employees.Any(e => employeeIds.Contains(e.Id)) && acceptableAbsenceStatuses.Contains(r.Request.Status))
                    .Where(DateRangeHelper.OverlapingDateRange<AbsenceRequest>(r => r.From, r => r.To, from, to))
                    .GroupBy(r => r.AffectedUser.Employees.FirstOrDefault().Id)
                    .ToDictionary(g => g.Key, g => g.Select(r => new EmployeeAllocationDto.AbsenceBlockDto
                    {
                        AbsenceName = canSeeAbsenceTaskName ? new LocalizedString { English = r.AbsenceType.NameEn, Polish = r.AbsenceType.NamePl } : null,
                        Status = r.Request.Status,
                        StatusName = r.Request.Status.GetDescription(),
                        StartDate = r.From,
                        EndDate = r.To,
                        RequestId = r.Id,
                        Hours = r.Hours
                    }).ToArray());

                return absences;
            }
        }

        public IDictionary<long, AllocationRequestDto[]> GetAllocationRequests(IReadOnlyCollection<long> employeeIds,
            DateTime from, DateTime to)
        {
            var mapping = _classMappingFactory.CreateMapping<AllocationRequest, AllocationRequestDto>();

            using (var unitofWork = _unitOfWorkAllocationService.Create())
            {
                var allocationDictionary = unitofWork.Repositories.AllocationRequests
                    .Include(a => a.Employee.OrgUnit)
                    .Include(a => a.Project)
                    .Where(a => employeeIds.Contains(a.EmployeeId))
                    .Where(DateRangeHelper.OverlapingDateRange<AllocationRequest>(a => a.StartDate, a => a.EndDate ?? DateTime.MaxValue, from, to))
                    .GroupBy(a => a.EmployeeId)
                    .ToDictionary(a => a.Key, a => a.Select(mapping.CreateFromSource).ToArray());

                return allocationDictionary;
            }
        }

        public IDictionary<long, EmploymentPeriodDto[]> GetEmploymentPeriods(IReadOnlyCollection<long> employeeIds,
            DateTime from, DateTime to)
        {
            var mapping = _classMappingFactory.CreateMapping<EmploymentPeriod, EmploymentPeriodDto>();

            using (var unitofWork = _unitOfWorkAllocationService.Create())
            {
                var employmentPeriods = unitofWork.Repositories.EmploymentPeriods
                    .Where(a => employeeIds.Contains(a.EmployeeId))
                    .Where(DateRangeHelper.OverlapingDateRange<EmploymentPeriod>(a => a.StartDate, a => a.EndDate ?? DateTime.MaxValue, from, to))
                    .GroupBy(a => a.EmployeeId)
                    .ToDictionary(a => a.Key, a => a.Select(mapping.CreateFromSource).ToArray());

                return employmentPeriods;
            }
        }
    }
}
