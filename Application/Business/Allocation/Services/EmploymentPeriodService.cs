﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Allocation.BusinessEvents;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Helpers;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Allocation.Resources;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Dto;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{
    public class EmploymentPeriodService : IEmploymentPeriodService
    {
        private readonly ITimeService _timeService;
        private readonly IUnitOfWorkService<IAllocationDbScope> _unitOfWorkService;
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkTimeTrackingService;
        private readonly IClassMapping<EmploymentPeriodDto, EmploymentPeriod> _employmentPeriodDtoToEmploymentPeriodClassMapping;
        private readonly IOrgUnitService _orgUnitService;
        private readonly ICalendarDataService _calendarDataService;
        private readonly IDanteCalendarService _danteCalendarService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IAllocationContractTypeService _allocationContractTypeService;
        private readonly IAllocationJobTitleService _allocationJobTitleService;
        private readonly IBusinessEventPublisher _businessEventPublisher;
        private readonly IDateRangeService _dateRangeService;
        private readonly IClassMapping<EmploymentPeriod, EmploymentPeriodDto> _employmentPeriodToEmploymentPeriodDtoClassMapping;

        private const string EmptyStartDate = "Start date cannot be empty";

        public EmploymentPeriodService(
            ITimeService timeService,
            IUnitOfWorkService<IAllocationDbScope> unitOfWorkService,
            IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkTimeTrackingService,
            IBusinessEventPublisher businessEventPublisher,
            IClassMapping<EmploymentPeriodDto, EmploymentPeriod> employmentPeriodDtoToEmploymentPeriodClassMapping,
            IOrgUnitService orgUnitService,
            ICalendarDataService calendarDataService,
            IDanteCalendarService danteCalendarService,
            ISystemParameterService systemParameterService,
            IAllocationContractTypeService allocationContractTypeService,
            IAllocationJobTitleService allocationJobTitleService,
            IDateRangeService dateRangeService,
            IClassMapping<EmploymentPeriod, EmploymentPeriodDto> employmentPeriodToEmploymentPeriodDtoClassMapping)
        {
            _timeService = timeService;
            _unitOfWorkService = unitOfWorkService;
            _unitOfWorkTimeTrackingService = unitOfWorkTimeTrackingService;
            _businessEventPublisher = businessEventPublisher;
            _employmentPeriodDtoToEmploymentPeriodClassMapping = employmentPeriodDtoToEmploymentPeriodClassMapping;
            _orgUnitService = orgUnitService;
            _calendarDataService = calendarDataService;
            _danteCalendarService = danteCalendarService;
            _systemParameterService = systemParameterService;
            _allocationContractTypeService = allocationContractTypeService;
            _allocationJobTitleService = allocationJobTitleService;
            _dateRangeService = dateRangeService;
            _employmentPeriodToEmploymentPeriodDtoClassMapping = employmentPeriodToEmploymentPeriodDtoClassMapping;
        }

        public AlertDto ValidateEmploymentPeriodDeleting(long employmentPeriodId)
        {
            var monthsOffset = _systemParameterService.GetParameter<int>(ParameterKeys.AllocationEmploymentPeriodCanBeDeletedInThePastMonths);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var selectedEmploymentPeriod = unitOfWork
                    .Repositories
                    .EmploymentPeriods
                    .GetById(employmentPeriodId);

                var deletePossibleFromDate = DateHelper.BeginningOfMonth(_timeService.GetCurrentDate().AddMonths(-monthsOffset)).Date;

                var isPeriodDeletionCollidingWithAbsence = IsPeriodDeletionCollidingWithAbsence(selectedEmploymentPeriod.EmployeeId, selectedEmploymentPeriod.StartDate, selectedEmploymentPeriod.EndDate);

                if (isPeriodDeletionCollidingWithAbsence)
                {
                    return new AlertDto
                    {
                        Type = AlertType.Information,
                        Message = AlertResources.DeletingEmploymentPeriodCollidesWithAbsences,
                    };
                }

                if (selectedEmploymentPeriod.StartDate.Date > deletePossibleFromDate
                    && (!selectedEmploymentPeriod.EndDate.HasValue || selectedEmploymentPeriod.EndDate.Value.Date > deletePossibleFromDate))
                {
                    return new AlertDto
                    {
                        Type = AlertType.Success
                    };
                }
            }

            return new AlertDto()
            {
                Type = AlertType.Error,
                Message = string.Format(AlertResources.DeletingEmploymentAlert, monthsOffset)
            };
        }

        public List<AlertDto> ValidateEmploymentPeriodEditing(EmploymentPeriod entityEmploymentPeriod, EmploymentPeriodDto inputEmploymentPeriodDto)
        {
            var validationResults = new List<AlertDto>();
            var todayDate = _timeService.GetCurrentDate();

            if (inputEmploymentPeriodDto.IsActive != entityEmploymentPeriod.IsActive)
            {
                using (var unitOfWork = _unitOfWorkService.Create())
                {
                    var isEmploymentPeriodUsed = OverlapingAllocationRequests(unitOfWork.Repositories.AllocationRequests,
                            entityEmploymentPeriod.EmployeeId, entityEmploymentPeriod.StartDate, entityEmploymentPeriod.EndDate ?? DateTime.MaxValue).Any();

                    if (isEmploymentPeriodUsed)
                    {
                        validationResults.Add(new AlertDto
                        {
                            Type = AlertType.Error,
                            Message = AlertResources.EditingEmploymentWithActiveAllocationAlert
                        });

                        return validationResults;
                    }
                }
            }

            var monthsOffset = _systemParameterService.GetParameter<int>(ParameterKeys.AllocationEmploymentPeriodCanBeClosedInThePastMonthsOnEdit);
            var changePossibleFromDate = DateHelper.BeginningOfMonth(todayDate.AddMonths(-monthsOffset)).Date;

            if (entityEmploymentPeriod.StartDate.Date != inputEmploymentPeriodDto.StartDate.Date)
            {
                if (entityEmploymentPeriod.StartDate.Date < changePossibleFromDate)
                {
                    validationResults.Add(new AlertDto
                    {
                        Type = AlertType.Error,
                        Message = string.Format(AlertResources.StartDateIsOlderThanAndCannotBeChanged, changePossibleFromDate.ToShortDateString())
                    });
                }
                else if (inputEmploymentPeriodDto.StartDate.Date < changePossibleFromDate)
                {
                    validationResults.Add(new AlertDto
                    {
                        Type = AlertType.Error,
                        Message = string.Format(AlertResources.StartDateCannotBeSetToDateOlderThan, changePossibleFromDate.ToShortDateString())
                    });
                }
            }

            if (entityEmploymentPeriod.EndDate.HasValue)
            {
                if (inputEmploymentPeriodDto.EndDate.HasValue)
                {
                    if (entityEmploymentPeriod.EndDate.Value.Date != inputEmploymentPeriodDto.EndDate.Value.Date)
                    {
                        if (entityEmploymentPeriod.EndDate.Value.Date < changePossibleFromDate)
                        {
                            validationResults.Add(new AlertDto
                            {
                                Type = AlertType.Error,
                                Message = string.Format(AlertResources.EndtDateIsOlderThanAndCannotBeChanged, changePossibleFromDate.ToShortDateString())
                            });
                        }
                        else if (inputEmploymentPeriodDto.EndDate.Value.Date < changePossibleFromDate)
                        {
                            validationResults.Add(new AlertDto
                            {
                                Type = AlertType.Error,
                                Message = string.Format(AlertResources.EndDateCannotBeSetToDateOlderThan, changePossibleFromDate.ToShortDateString())
                            });
                        }
                    }
                }
                else if (entityEmploymentPeriod.EndDate.Value.Date < changePossibleFromDate)
                {
                    validationResults.Add(new AlertDto
                    {
                        Type = AlertType.Error,
                        Message = string.Format(AlertResources.EndtDateIsOlderThanAndCannotBeChanged, changePossibleFromDate.ToShortDateString())
                    });
                }
            }
            else if (inputEmploymentPeriodDto.EndDate.HasValue
                     && inputEmploymentPeriodDto.EndDate.Value.Date < changePossibleFromDate)
            {
                validationResults.Add(new AlertDto
                {
                    Type = AlertType.Error,
                    Message = string.Format(AlertResources.EndDateCannotBeSetToDateOlderThan, changePossibleFromDate.ToShortDateString())
                });
            }

            if (inputEmploymentPeriodDto.EndDate.HasValue && inputEmploymentPeriodDto.TerminatedOn.HasValue
                && inputEmploymentPeriodDto.EndDate.Value < inputEmploymentPeriodDto.TerminatedOn.Value)
            {
                validationResults.Add(new AlertDto
                {
                    Type = AlertType.Error,
                    Message = string.Format(AlertResources.TerminatedOnGreaterThanEndDate)
                });
            }

            if (!validationResults.Any() && inputEmploymentPeriodDto.EndDate.HasValue
                && inputEmploymentPeriodDto.EndDate.Value < (entityEmploymentPeriod.EndDate ?? DateTime.MaxValue))
            {
                using (var unitOfWork = _unitOfWorkService.Create())
                {
                    if (inputEmploymentPeriodDto.ChangeExistingAllocations)
                    {
                        AllocationRequestCleaning(unitOfWork, inputEmploymentPeriodDto, entityEmploymentPeriod);
                    }
                    else
                    {
                        var isEmploymentPeriodUsed =
                            OverlapingAllocationRequests(unitOfWork.Repositories.AllocationRequests, entityEmploymentPeriod.EmployeeId,
                                inputEmploymentPeriodDto.EndDate.Value, entityEmploymentPeriod.EndDate ?? DateTime.MaxValue).Any();

                        if (isEmploymentPeriodUsed)
                        {
                            validationResults.Add(new AlertDto
                            {
                                Type = AlertType.Error,
                                Message = AlertResources.EditingEmploymentWithActiveAllocationAlertAndCheckbox
                            });
                        }
                    }
                }
            }

            bool hasOverlapingEmploymentPeriods;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                hasOverlapingEmploymentPeriods = OverlapingEmploymentPeriods(unitOfWork.Repositories.EmploymentPeriods, inputEmploymentPeriodDto.EmployeeId,
                    inputEmploymentPeriodDto.StartDate, inputEmploymentPeriodDto.EndDate).Any(ep => ep.Id != inputEmploymentPeriodDto.Id);
            }

            if (hasOverlapingEmploymentPeriods)
            {
                validationResults.Add(new AlertDto
                {
                    Type = AlertType.Error,
                    Message = AlertResources.ErrorOverlapingPeriod
                });
            }

            var isPeriodChangeCollidingWithAbsence = IsPeriodChangeCollidingWithAbsence(inputEmploymentPeriodDto.EmployeeId, inputEmploymentPeriodDto.StartDate, inputEmploymentPeriodDto.EndDate);

            if (isPeriodChangeCollidingWithAbsence)
            {
                validationResults.Add(new AlertDto
                {
                    Type = AlertType.Information,
                    Message = AlertResources.UpdatingEmploymentPeriodCollidesWithAbsences
                });
            }

            return validationResults;
        }

        public List<AlertDto> ValidateEmploymentPeriodAdding(EmploymentPeriodDto inputEmploymentPeriodDto)
        {
            var validationResults = new List<AlertDto>();
            var monthsOffset = _systemParameterService.GetParameter<int>(ParameterKeys.AllocationEmploymentPeriodCanBeClosedInThePastMonthsOnAdd);
            var changePossibleFromDate = DateHelper.BeginningOfMonth(_timeService.GetCurrentDate().AddMonths(-monthsOffset)).Date;

            if (inputEmploymentPeriodDto.StartDate.Date < changePossibleFromDate)
            {
                validationResults.Add(new AlertDto
                {
                    Type = AlertType.Error,
                    Message = string.Format(AlertResources.StartDateCannotBeSetToDateOlderThan, changePossibleFromDate.ToShortDateString())
                });
            }

            if (inputEmploymentPeriodDto.EndDate.HasValue
                   && inputEmploymentPeriodDto.EndDate.Value.Date < changePossibleFromDate)
            {
                validationResults.Add(new AlertDto
                {
                    Type = AlertType.Error,
                    Message = string.Format(AlertResources.EndDateCannotBeSetToDateOlderThan, changePossibleFromDate.ToShortDateString())
                });
            }

            bool hasOverlapingEmploymentPeriods;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                hasOverlapingEmploymentPeriods = OverlapingEmploymentPeriods(
                    unitOfWork.Repositories.EmploymentPeriods, inputEmploymentPeriodDto.EmployeeId,
                    inputEmploymentPeriodDto.StartDate, inputEmploymentPeriodDto.EndDate).Any();
            }

            if (hasOverlapingEmploymentPeriods && !inputEmploymentPeriodDto.ClosePreviousPeriod)
            {
                validationResults.Add(new AlertDto
                {
                    Type = AlertType.Error,
                    Message = AlertResources.ErrorOverlapingPeriod
                });
            }

            return validationResults;
        }

        public IQueryable<AllocationRequest> OverlapingAllocationRequests(IQueryable<AllocationRequest> allocationRequestRepository,
                long employeeId, DateTime startDate, DateTime endDate)
        {
            return allocationRequestRepository
                .Where(ar => ar.EmployeeId == employeeId
                   && !((ar.StartDate < startDate && ar.EndDate.HasValue
                   && ar.EndDate < startDate) || (ar.StartDate > endDate)));
        }

        private bool IsPeriodDeletionCollidingWithAbsence(long employeeId, DateTime employmentPeriodStartDate, DateTime? employmentPeriodEndDate)
        {
            using (var unitOfWork = _unitOfWorkTimeTrackingService.Create())
            {
                var inCollision = unitOfWork.Repositories.EmployeeAbsences
                    .Where(ea => ea.EmployeeId == employeeId)
                    .Any(ea =>
                        ea.From >= employmentPeriodStartDate && (!employmentPeriodEndDate.HasValue || ea.From <= employmentPeriodEndDate)
                        ||
                        ea.To >= employmentPeriodStartDate && (!employmentPeriodEndDate.HasValue || ea.To <= employmentPeriodEndDate)
                    );
                return inCollision;
            }

        }

        private bool IsPeriodChangeCollidingWithAbsence(long employeeId, DateTime employmentPeriodStartDate, DateTime? employmentPeriodEndDate)
        {
            using (var unitOfWork = _unitOfWorkTimeTrackingService.Create())
            {
                var inCollision = !unitOfWork.Repositories.EmployeeAbsences
                    .Where(ea => ea.EmployeeId == employeeId)
                    .All(ea =>
                        ea.To < employmentPeriodStartDate
                        ||
                        employmentPeriodEndDate.HasValue && ea.From > employmentPeriodEndDate
                        ||
                        ea.From >= employmentPeriodStartDate && (!employmentPeriodEndDate.HasValue || ea.To <= employmentPeriodEndDate
                    ));

                return inCollision;
            }

        }
 
        private IQueryable<EmploymentPeriod> OverlapingEmploymentPeriods(IQueryable<EmploymentPeriod> employmentPeriodRepository,
                long employeeId, DateTime startDate, DateTime? endDate)
        {
            return employmentPeriodRepository.Where(ep => ep.EmployeeId == employeeId
                                                          &&
                                                          !((ep.StartDate < startDate && ep.EndDate.HasValue && ep.EndDate < startDate)
                                                            || (endDate.HasValue && ep.StartDate > endDate.Value)));
        }

        private void AllocationRequestCleaning(IUnitOfWork<IAllocationDbScope> unitOfWork, EmploymentPeriodDto newEmploymentPeriod, EmploymentPeriod oldEmploymentPeriod)
        {
            var allocationRequests = OverlapingAllocationRequests(unitOfWork.Repositories.AllocationRequests,
                    oldEmploymentPeriod.EmployeeId, oldEmploymentPeriod.StartDate, oldEmploymentPeriod.EndDate ?? DateTime.MaxValue).ToList();

            foreach (var allocationRequest in allocationRequests)
            {
                if (newEmploymentPeriod.EndDate.HasValue && allocationRequest.StartDate > newEmploymentPeriod.EndDate.Value)
                {
                    unitOfWork.Repositories.AllocationRequests.Delete(allocationRequest);
                }
                else if ((allocationRequest.EndDate ?? DateTime.MaxValue) > newEmploymentPeriod.EndDate)
                {
                    allocationRequest.EndDate = newEmploymentPeriod.EndDate;

                    unitOfWork.Repositories.AllocationRequests.Edit(allocationRequest);
                }
            }

            unitOfWork.Commit();
        }

        public long CreateEmploymentPeriod(EmploymentPeriodDto employmentPeriod)
        {
            var employmentPeriodEntity = _employmentPeriodDtoToEmploymentPeriodClassMapping.CreateFromSource(employmentPeriod);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                unitOfWork.Repositories.EmploymentPeriods.Add(employmentPeriodEntity);
                unitOfWork.Commit();

                var businessEvents = GetAddEmploymentPeriodEvents(employmentPeriodEntity).ToList();

                businessEvents.ForEach(_businessEventPublisher.PublishBusinessEvent);

                return employmentPeriodEntity.Id;
            }
        }

        public long CreateEmploymentPeriod(long employeeId, ActiveDirectoryUserDto activeDirectoryUserDto)
        {
            if (!activeDirectoryUserDto.StartDate.HasValue)
            {
                throw new InvalidOperationException(EmptyStartDate);
            }

            var currentDate = _timeService.GetCurrentDate();
            var signedOnDate = activeDirectoryUserDto.StartDate.Value < currentDate
                ? activeDirectoryUserDto.StartDate.Value
                : currentDate;
            var orgUnit = _orgUnitService.GetOrgUnitWithDefaultTimeTrackingMode(activeDirectoryUserDto.OrgUnitId);

            if (!orgUnit.TimeReportingMode.HasValue)
            {
                const string emptyTimeReporting = "Organizational unit has no default time reporting mode set";

                throw new InvalidOperationException(emptyTimeReporting);
            }

            var employmentPeriodDto = new EmploymentPeriodDto
            {
                EmployeeId = employeeId,
                StartDate = activeDirectoryUserDto.StartDate.Value,
                EndDate = activeDirectoryUserDto.EndDate,
                SignedOn = signedOnDate,
                CompanyId = activeDirectoryUserDto.CompanyId,
                JobTitleId = _allocationJobTitleService.GetJobTitleIdByTitle(activeDirectoryUserDto.Title),
                IsActive = true,
                MondayHours = 8,
                TuesdayHours = 8,
                WednesdayHours = 8,
                ThursdayHours = 8,
                FridayHours = 8,
                SaturdayHours = 0,
                SundayHours = 0,
                InactivityReason = 0,
                ContractTypeId = _allocationContractTypeService.GetContractTypeIdByActiveDirectoryCode(activeDirectoryUserDto.ContractType),
                NoticePeriod = GetNoticePeriod(activeDirectoryUserDto.StartDate.Value),
                TerminatedOn = GetTerminatedDate(activeDirectoryUserDto),
                TimeReportingMode = orgUnit.TimeReportingMode.Value,
                AbsenceOnlyDefaultProjectId = orgUnit.AbsenceOnlyDefaultProjectId
            };

            return CreateEmploymentPeriod(employmentPeriodDto);
        }

        public IList<DailyContractedHoursDto> GetContractedHours(long employeeId, DateTime from, DateTime to, Expression<Func<EmploymentPeriod, bool>> whereExpression = null)
        {
            var days = new List<DailyContractedHoursDto>();

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                EmploymentPeriod period = null;
                long calendarId, orgUnitCalendarId;
                CalendarData calendarData, orgUnitCalendarData;

                var dateList = _dateRangeService.GetDaysInRange(from, to, true);

                var orgUnitId = unitOfWork.Repositories.Employees.Where(e => e.Id == employeeId).Select(e => e.OrgUnitId).Single();
                calendarId = orgUnitCalendarId = _danteCalendarService.GetCalendarIdByOrgUnitId(orgUnitId);
                calendarData = orgUnitCalendarData = _calendarDataService.GetCalendarDataById(calendarId);

                foreach (var date in dateList)
                {
                    if (period == null || (period.EndDate.HasValue && period.EndDate.Value < date))
                    {
                        var periodQuery = unitOfWork.Repositories.EmploymentPeriods.Where(e => e.EmployeeId == employeeId);

                        if (whereExpression != null)
                        {
                            periodQuery = periodQuery.Where(whereExpression);
                        }

                        period = periodQuery.SingleOrDefault(p => p.StartDate <= date && (!p.EndDate.HasValue || p.EndDate.Value >= date));
                    }

                    if (period != null)
                    {
                        if (period.CalendarId != null
                            && calendarId != period.CalendarId)
                        {
                            calendarId = period.CalendarId.Value;
                            calendarData = _calendarDataService.GetCalendarDataById(calendarId);
                        }
                        else if (period.CalendarId == null
                            && calendarId != orgUnitCalendarId)
                        {
                            calendarId = orgUnitCalendarId;
                            calendarData = orgUnitCalendarData;
                        }

                        var contractedHours = WeeklyHoursHelper.GetHoursByDayOrDefault(period, date.DayOfWeek);

                        if (calendarData.KnownHolidays.IsHoliday(date))
                        {
                            contractedHours = Math.Min(calendarData.KnownHolidays.GetHolidayWorkingHoursByDate(date),
                                contractedHours);
                        }

                        days.Add(new DailyContractedHoursDto
                        {
                            EmploymentPeriod =
                                _employmentPeriodToEmploymentPeriodDtoClassMapping.CreateFromSource(period),
                            Date = date,
                            Hours = contractedHours
                        });
                    }
                }
            }

            return days;
        }

        public IList<DailyContractedHoursDto> GetContractedHours(long employeeId, int year, byte month, Expression<Func<EmploymentPeriod, bool>> whereExpression = null)
        {
            var from = new DateTime(year, month, 1);
            var to = DateHelper.EndOfMonth(from);

            return GetContractedHours(employeeId, from, to, whereExpression);
        }

        private DateTime? GetTerminatedDate(ActiveDirectoryUserDto activeDirectoryUserDto)
        {
            if (activeDirectoryUserDto.EndDate.HasValue)
            {
                return activeDirectoryUserDto.EffectiveDate ?? activeDirectoryUserDto.EndDate.Value;
            }

            return null;
        }

        public void UpdateEmploymentPeriods(long employeeId, ActiveDirectoryUserDto activeDirectoryUserDto)
        {
            if (!activeDirectoryUserDto.StartDate.HasValue)
            {
                throw new InvalidOperationException(EmptyStartDate);
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employmentPeriods = unitOfWork.Repositories.EmploymentPeriods.Where(
                        e => e.EmployeeId == employeeId
                            && (!e.EndDate.HasValue || e.EndDate >= activeDirectoryUserDto.StartDate.Value)).ToList();

                if (employmentPeriods.Any())
                {
                    //switching off employment period changes synchronization
                    //shouldn't be necessary anymore
                    return;
                }

                CreateEmploymentPeriod(employeeId, activeDirectoryUserDto);
            }
        }

        public EmploymentPeriodDto GetEmploymentPeriod(long employeeId, DateTime date)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employmentPeriod = unitOfWork.Repositories.EmploymentPeriods
                    .Include(e => e.Company)
                    .Include(e => e.ContractType)
                    .SingleOrDefault(EmploymentPeriodBusinessLogic.OfEmployeeInDate.Parametrize(employeeId, date));

                return employmentPeriod != null
                    ? _employmentPeriodToEmploymentPeriodDtoClassMapping.CreateFromSource(employmentPeriod)
                    : null;
            }
        }

        public IList<EmploymentPeriodDto> GetEmploymentPeriodsInDateRange(long employeeId, DateTime startDate, DateTime endDate)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.EmploymentPeriods
                    .Where(e => e.EmployeeId == employeeId)
                    .Where(EmploymentPeriodBusinessLogic.OverlapingRange.Parametrize(startDate, endDate))
                    .AsEnumerable()
                    .Select(_employmentPeriodToEmploymentPeriodDtoClassMapping.CreateFromSource)
                    .ToList();
            }
        }

        public bool IsEmployeeHiredInCompletePeriod(long employeeId, DateTime startDate, DateTime endDate)
        {
            if (startDate.Date > endDate.Date)
            {
                return false;
            }

            IList<EmploymentPeriod> employmentPeriods;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                employmentPeriods = OverlapingEmploymentPeriods(unitOfWork.Repositories.EmploymentPeriods,
                    employeeId, startDate, endDate).ToList();
            }

            if (employmentPeriods.Count == 0)
            {
                return false;
            }

            var days = _dateRangeService.GetDaysInRange(startDate, endDate);

            var result = days.All(day =>
                employmentPeriods.Any(
                    ep => ep.StartDate <= day && (!ep.EndDate.HasValue || ep.EndDate.Value >= day)));

            return result;
        }

        public bool IsClockCardRequiredInGivenMonth(long employeeId, int year, byte month)
        {
            var monthEnd = DateHelper.EndOfMonth(year, month);
            var monthStart = DateHelper.BeginningOfMonth(year, month);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.EmploymentPeriods
                    .Where(ep => ep.EmployeeId == employeeId)
                    .Where(EmploymentPeriodBusinessLogic.OverlapingRange.Parametrize(monthStart, monthEnd))
                    .Any(ep => ep.IsClockCardRequired);
            }
        }

        private static IEnumerable<BusinessEvent> GetAddEmploymentPeriodEvents(EmploymentPeriod employmentPeriod)
        {
            yield return AllocationHelper.CreateEventFromEmploymentPeriod(employmentPeriod);

            yield return new EmploymentPeriodChangedBusinessEvent
            {
                EmployeeId = employmentPeriod.EmployeeId,
                From = employmentPeriod.StartDate,
                To = employmentPeriod.EndDate
            };
        }

        private NoticePeriod GetNoticePeriod(DateTime startDate)
        {
            var todayDate = _timeService.GetCurrentDate();

            if (startDate.AddMonths(6) > todayDate)
            {
                return NoticePeriod.TwoWeeks;
            }

            if (startDate.AddMonths(6) <= todayDate && startDate.AddMonths(36) > todayDate)
            {
                return NoticePeriod.OneMonth;
            }

            return NoticePeriod.ThreeMonths;
        }

        public OverTimeTypes AllowedOvertimeInGivenMonth(long employeeId, int year, byte month)
        {
            var monthEnd = DateHelper.EndOfMonth(year, month);
            var monthStart = DateHelper.BeginningOfMonth(year, month);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var periodOvertimes = unitOfWork.Repositories.EmploymentPeriods
                    .Where(ep => ep.EmployeeId == employeeId)
                    .Where(EmploymentPeriodBusinessLogic.OverlapingRange.Parametrize(monthStart, monthEnd))
                    .Select(ep => ep.ContractType.AllowedOverTimeTypes)
                    .ToList();

                var allowedOvertime = OverTimeTypes.NoOverTime;
                periodOvertimes.ForEach(a => allowedOvertime |= a);

                return allowedOvertime;
            }
        }
    }
}
