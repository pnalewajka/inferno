﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Allocation.Resources;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{
    public class StaffingDemandService : IStaffingDemandService
    {
        private readonly ITimeService _timeService;
        private readonly IUnitOfWorkService<IAllocationDbScope> _unitOfWorkService;

        public StaffingDemandService(
             ITimeService timeService,
             IUnitOfWorkService<IAllocationDbScope> unitOfWorkService)
        {
            _timeService = timeService;
            _unitOfWorkService = unitOfWorkService;
        }

        public AlertDto ValidateStaffingDemandDeleting(long staffingDemandId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var selectedStaffingDemand = unitOfWork.Repositories.StaffingDemands.FirstOrDefault(s => s.Id == staffingDemandId);

                var todayDate = _timeService.GetCurrentDate();

                if (selectedStaffingDemand != null && selectedStaffingDemand.StartDate.Date > todayDate &&
                    (!selectedStaffingDemand.EndDate.HasValue || (selectedStaffingDemand.EndDate.Value.Date > todayDate.Date)))
                {
                    return new AlertDto()
                    {
                        Type = AlertType.Success
                    };
                }
            }

            return new AlertDto()
            {
                Type = AlertType.Error,
                Message = AlertResources.DeletingStaffingDemandAlert
            };
        }

        public AlertDto ValidateStaffingDemandEditing(long staffingDemandId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var selectedStaffingDemand = unitOfWork.Repositories.StaffingDemands.FirstOrDefault(s => s.Id == staffingDemandId);

                var todayDate = _timeService.GetCurrentDate();

                if (selectedStaffingDemand != null &&
                   (!selectedStaffingDemand.EndDate.HasValue || (selectedStaffingDemand.EndDate.Value.Date >= todayDate.Date)))
                {
                    return new AlertDto()
                    {
                        Type = AlertType.Success
                    };
                }
            }

            return new AlertDto()
            {
                Type = AlertType.Error,
                Message = AlertResources.EditingStaffingDemandAlert
            };
        }
    }
}
