﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Enums;
using Smt.Atomic.Business.Allocation.Filters;
using Smt.Atomic.Business.Allocation.Helpers;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{
    internal abstract class EmployeeAllocationBaseCardIndexDataService : CardIndexDataService<EmployeeAllocationDto, Employee, IAllocationDbScope>, IEmployeeAllocationBaseCardIndexDataService
    {
        protected readonly ITimeService _timeService;
        protected readonly IOrgUnitService _orgUnitService;
        protected readonly IPrincipalProvider _principalProvider;
        private readonly WeeklyAllocationDataService _weeklyAllocationService;
        private readonly DailyAllocationDataService _dailyAllocationDataService;
        private readonly RequestAllocationDataService _requestAllocationDataService;
        private readonly IEmployeeCardIndexDataService _employeeService;
        private readonly IAllocationRequestCardIndexDataService _allocationRequestCardIndexDataService;
        private readonly ICalendarService _calendarService;
        private readonly IClassMappingFactory _classMappingFactory;

        public DateTime? StatusFrom { get; protected set; }
        public DateTime? StatusTo { get; protected set; }

        protected EmployeeAllocationBaseCardIndexDataService(ICardIndexServiceDependencies<IAllocationDbScope> dependencies,
            IOrgUnitService orgUnitService,
            ITimeService timeService,
            WeeklyAllocationDataService weeklyAllocationService,
            DailyAllocationDataService dailyAllocationDataService,
            RequestAllocationDataService requestAllocationDataService,
            ICalendarService calendarService,
            IEmployeeCardIndexDataService employeeService,
            IAllocationRequestCardIndexDataService allocationRequestCardIndexDataService,
            IClassMappingFactory classMappingFactory,
            IPrincipalProvider principalProvider)
            : base(dependencies)
        {
            _orgUnitService = orgUnitService;
            _timeService = timeService;
            _weeklyAllocationService = weeklyAllocationService;
            _dailyAllocationDataService = dailyAllocationDataService;
            _requestAllocationDataService = requestAllocationDataService;
            _employeeService = employeeService;
            _allocationRequestCardIndexDataService = allocationRequestCardIndexDataService;
            _principalProvider = principalProvider;
            _calendarService = calendarService;
            _classMappingFactory = classMappingFactory;
        }

        protected override IEnumerable<Expression<Func<Employee, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            if (searchCriteria.Includes(SearchAreaCodesAllocation.FirstName))
            {
                yield return a => a.FirstName;
            }

            if (searchCriteria.Includes(SearchAreaCodesAllocation.LastName))
            {
                yield return a => a.LastName;
            }

            if (searchCriteria.Includes(SearchAreaCodesAllocation.Email))
            {
                yield return a => a.Email;
            }

            if (searchCriteria.Includes(SearchAreaCodesAllocation.LocationName))
            {
                yield return u => u.Location.Name;
            }

            if (searchCriteria.Includes(SearchAreaCodesAllocation.LineMaganerLastName))
            {
                yield return u => u.LineManager.LastName;
            }

            if (searchCriteria.Includes(SearchAreaCodesAllocation.Skills))
            {
                yield return u => u.Resumes.SelectMany(r => r.ResumeTechnicalSkills).Select(s => s.TechnicalSkill.Name);
            }

            if (searchCriteria.Includes(SearchAreaCodesAllocation.JobProfileName))
            {
                yield return u => u.JobProfiles.Select(jp => jp.Name);
            }

            if (searchCriteria.Includes(SearchAreaCodesAllocation.JobTitle))
            {
                yield return u => u.JobTitle.NameEn;
                yield return u => u.JobTitle.NamePl;
            }

            if (searchCriteria.Includes(SearchAreaCodesAllocation.ProjectName))
            {
                yield return u => u.AllocationRequests
                    .Where(r => (!r.EndDate.HasValue || r.EndDate >= StatusFrom.Value) && (StatusTo.Value >= r.StartDate))
                    .Select(r => ProjectBusinessLogic.ProjectName.Call(r.Project));
            }
        }

        protected override IOrderedQueryable<Employee> GetDefaultOrderBy(IQueryable<Employee> records, QueryCriteria criteria)
        {
            return records.OrderBy(e => e.OrgUnit.NameSortOrder).ThenBy(e => e.LineManager.Email);
        }

        protected override Expression<Func<Employee, bool>> AlterSearchPhraseCondition(SearchCriteria searchCriteria, Expression<Func<Employee, bool>> condition, string word)
        {
            var result = condition;

            if (searchCriteria.Includes(SearchAreaCodesAllocation.AllocationStatus))
            {
                if (StatusFrom.HasValue && StatusTo.HasValue)
                {
                    var weekStatusTypeFilter = Enum.GetValues(typeof(AllocationStatus))
                        .Cast<AllocationStatus>().Where(type => type.GetDescriptionOrValue().ToLower().Contains(word)).ToList();

                    if (weekStatusTypeFilter.Any())
                    {
                        Expression<Func<Employee, bool>> weekStatusWhere = e => e.WeeklyAllocationStatuses
                            .Where(w => w.Week >= StatusFrom.Value && w.Week <= StatusTo.Value)
                            .Any(s => weekStatusTypeFilter.Contains(s.AllocationStatus));

                        result = result == null ? weekStatusWhere : result.And(weekStatusWhere);
                    }
                }
            }

            return result;
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<Employee> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var sortingColumnName = sortingCriterion.GetSortingColumnName();
            var direction = sortingCriterion.GetSortingDirection();

            if (sortingColumnName == nameof(EmployeeAllocationDto.FullName))
            {
                EmployeeSortHelper.OrderByFullName(orderedQueryBuilder, direction);
            }
            else if (sortingColumnName == nameof(EmployeeAllocationDto.LocationName))
            {
                EmployeeSortHelper.OrderByLocation(orderedQueryBuilder, direction);
            }
            else if (sortingColumnName == nameof(EmployeeAllocationDto.JobProfileIds))
            {
                EmployeeSortHelper.OrderByJobProfile(orderedQueryBuilder, direction);
            }
            else if (sortingColumnName == nameof(EmployeeAllocationDto.SkillIds))
            {
                EmployeeSortHelper.OrderBySkills(orderedQueryBuilder, direction);
            }
            else if (sortingColumnName == nameof(EmployeeAllocationDto.JobMatrixIds))
            {
                EmployeeSortHelper.OrderByJobMatrices(orderedQueryBuilder, direction);
            }
            else if (sortingColumnName == nameof(EmployeeAllocationDto.LineManagerId))
            {
                EmployeeSortHelper.OrderByLineManager(orderedQueryBuilder, direction);
            }
            else
            {
                base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
            }
        }

        protected virtual void EnhanceQueryCriteriaBeforeGetRecords(QueryCriteria criteria)
        {
        }

        protected override IQueryable<Employee> ConfigureIncludes(IQueryable<Employee> sourceQueryable)
        {
            var technicalSkillPath = string.Join(".",
              PropertyHelper.GetPropertyPathMemberInfo(
                  (Employee e) => e.Resumes.SelectMany(r => r.ResumeTechnicalSkills).Select(r => r.TechnicalSkill))
                  .Select(mi => mi.Name));

            return sourceQueryable
                .Include(x => x.JobProfiles)
                .Include(technicalSkillPath)
                .Include(x => x.JobMatrixs);

        }

        public override CardIndexRecords<EmployeeAllocationDto> GetRecords(QueryCriteria criteria)
        {
            if (criteria.FilterObjects.ContainsKey(FilterCodes.DateRange))
            {
                var dateRangeFilterParameters = criteria.FilterObjects[FilterCodes.DateRange].GetFilterParameters();
                StatusFrom = (DateTime)dateRangeFilterParameters[$"{FilterCodes.DateRange}.from"].Value;
                StatusTo = (DateTime)dateRangeFilterParameters[$"{FilterCodes.DateRange}.to"].Value;
            }
            else
            {
                if (criteria.FilterObjects.ContainsKey(FilterCodes.UtilizationFilter))
                {
                    var utilizationFilterParameters = criteria.FilterObjects[FilterCodes.UtilizationFilter].GetFilterParameters();
                    var week = (DateTime)utilizationFilterParameters[$"{FilterCodes.UtilizationFilter}.week"].Value;
                    StatusFrom = week.AddDays(-15);
                    StatusTo = week.AddDays(15);
                }
                else
                {
                    StatusFrom = GetDefaultFromValue();
                    StatusTo = GetDefaultToValue();
                }
            }

            if (StatusFrom.HasValue)
            {
                StatusFrom = StatusFrom.Value.GetPreviousDayOfWeek(DayOfWeek.Monday);
            }

            if (StatusTo.HasValue)
            {
                StatusTo = StatusTo.Value.GetNextDayOfWeek(DayOfWeek.Sunday);
            }

            CreateDateRangeFilterObjectIfFilterExists(criteria, FilterCodes.MyProjects);
            CreateDateRangeFilterObjectIfFilterExists(criteria, FilterCodes.MyTeamAllocations);
            CreateEmployeeProjectFilterObjectIfFilterExists(criteria, FilterCodes.EmployeesAndProjects);
            EnhanceQueryCriteriaBeforeGetRecords(criteria);

            var result = base.GetRecords(criteria);

            var context = criteria.Context as EmployeeAllocationContext ?? new EmployeeAllocationContext();

            var employeeIds = result.Rows.Select(r => r.Id).Distinct().ToList();

            if (context.Beta)
            {
                var employmentPeriods = _requestAllocationDataService.GetEmploymentPeriods(employeeIds, StatusFrom.Value, StatusTo.Value);
                var allocationRequests = _requestAllocationDataService.GetAllocationRequests(employeeIds, StatusFrom.Value, StatusTo.Value);
                var absences = _requestAllocationDataService.GetEmployeeAbsences(employeeIds, StatusFrom.Value, StatusTo.Value);

                var calendarIds = employmentPeriods
                    .SelectMany(p => p.Value.Select(e => e.CalendarId))
                    .Where(c => c.HasValue)
                    .Select(c => c.Value)
                    .Distinct();

                var holidayDictionary = calendarIds.ToDictionary(c => c, c => _calendarService.GetHolidays(StatusFrom.Value, StatusTo.Value, c));

                foreach (var employee in result.Rows)
                {
                    var employmentPeriodsForEmployee = employmentPeriods.ContainsKey(employee.EmployeeId) ? employmentPeriods[employee.EmployeeId] : new EmploymentPeriodDto[0];

                    var employeeRequests = allocationRequests.ContainsKey(employee.EmployeeId) ? allocationRequests[employee.EmployeeId] : new AllocationRequestDto[0];

                    employee.AllocationBlocks = CreateBlocksForEmployee(employeeRequests).ToArray();
                    employee.EmploymentPeriodBlocks = CreateEmploymentBlocksForEmployee(employmentPeriodsForEmployee).ToArray();
                    employee.Absences = absences.ContainsKey(employee.EmployeeId) ? absences[employee.EmployeeId] : new EmployeeAllocationDto.AbsenceBlockDto[0];
                    employee.Holidays = employmentPeriodsForEmployee
                        .Select(e => e.CalendarId)
                        .Concat(employeeRequests.Select(c => c.CalendarId))
                        .Distinct()
                        .Where(p => p.HasValue && holidayDictionary.ContainsKey(p.Value))
                        .SelectMany(p => holidayDictionary[p.Value])
                        .Distinct()
                        .ToArray();
                }
            }
            else
            {
                var allocations = context.PeriodType == EmployeeAllocationPeriodType.Day ?
                    _dailyAllocationDataService.CalculateAllocations(employeeIds, StatusFrom.Value, StatusTo.Value) :
                    _weeklyAllocationService.CalculateAllocations(employeeIds, StatusFrom.Value, StatusTo.Value);

                foreach (var allocation in allocations)
                {
                    var employee = result.Rows.FirstOrDefault(e => e.Id == allocation.Key);

                    if (employee != null)
                    {
                        employee.Allocations = allocation.Value.ToArray();
                    }
                }
            }

            RemoveEmployeesWithoutAllocationsIfMyProjectsFilterWasSet(criteria, result);

            return result;
        }

        private IEnumerable<EmployeeAllocationDto.EmploymentPeriodBlockDto> CreateEmploymentBlocksForEmployee(IEnumerable<EmploymentPeriodDto> employmentPeriods)
        {
            return employmentPeriods.Select(e => new EmployeeAllocationDto.EmploymentPeriodBlockDto
            {
                StartDate = e.StartDate,
                EndDate = e.EndDate ?? DateTime.MaxValue,
                ContractedHours = new[] { e.MondayHours, e.TuesdayHours, e.WednesdayHours, e.ThursdayHours, e.FridayHours, e.SaturdayHours, e.SundayHours }
            });
        }

        private IEnumerable<EmployeeAllocationDto.AllocationBlockDto> CreateBlocksForEmployee(IEnumerable<AllocationRequestDto> allocationRequests)
        {
            var blocks = new List<EmployeeAllocationDto.AllocationBlockDto>();
            var dtoMapping = _classMappingFactory.CreateMapping<AllocationRequestDto, EmployeeAllocationDto.AllocationBlockDto>();

            foreach (var request in allocationRequests.GroupBy(d => d.ProjectId).SelectMany(r => r))
            {
                var currentBlock = dtoMapping.CreateFromSource(request);

                currentBlock.PeriodStartDate = StatusFrom.Value;
                currentBlock.PeriodEndDate = StatusTo.Value;

                yield return currentBlock;
            }
        }

        protected void CreateEmployeeProjectFilterObjectIfFilterExists(QueryCriteria criteria, string filter)
        {
            if (criteria.Filters.SelectMany(f => f.Codes).Any(c => c == filter))
            {
                var currentFilterParameters = criteria.FilterObjects[filter].GetFilterParameters();

                criteria.FilterObjects[filter] = new EmployeesProjectsFilterObject(
                    StatusFrom.GetValueOrDefault(GetDefaultFromValue()),
                    StatusTo.GetValueOrDefault(GetDefaultToValue()),
                    currentFilterParameters);
            }
        }

        protected void CreateDateRangeFilterObjectIfFilterExists(QueryCriteria criteria, string filter)
        {
            if (criteria.Filters.SelectMany(f => f.Codes).Any(c => c == filter))
            {
                criteria.FilterObjects[filter] = new DateRangeFilterObject(
                    StatusFrom.GetValueOrDefault(GetDefaultFromValue()),
                    StatusTo.GetValueOrDefault(GetDefaultToValue()));
            }
        }

        private void RemoveEmployeesWithoutAllocationsIfMyProjectsFilterWasSet(QueryCriteria criteria,
            CardIndexRecords<EmployeeAllocationDto> result)
        {
            var onlyMyProjects = criteria.Filters.Any(f => f.Codes.Any(c => c == FilterCodes.MyProjects));

            if (onlyMyProjects)
            {
                var rowsToRemove = result.Rows.Where(ea => ea.Allocations.SelectMany(
                    a => a.ProjectAllocations).All(pa => pa.HoursAllocated == 0)).ToList();
                rowsToRemove.ForEach(r => result.Rows.Remove(r));
            }
        }

        public override IDictionary<long, EmployeeAllocationDto> GetRecordsByIds(long[] ids, bool filtered = true)
        {
            var result = base.GetRecordsByIds(ids, filtered);
            var allocations = _dailyAllocationDataService.CalculateAllocations(ids, GetDefaultFromValue(), GetDefaultToValue());

            foreach (var allocation in allocations)
            {
                var employee = result.Values.FirstOrDefault(e => e.Id == allocation.Key);
                if (employee != null)
                {
                    employee.Allocations = allocation.Value.ToArray();
                }
            }
            return result;
        }

        protected override NamedFilters<Employee> NamedFilters
        {
            get
            {
                return new NamedFilters<Employee>(
                    new[] {
                           DateRangeFilter.EmployeeFilter,
                           MyProjectsFilter.GetEmployeeFilter(_principalProvider, _orgUnitService),
                           MyTeamAllocationsFilter.GetEmployeeFilter(_principalProvider),
                           UtilizationFilter.EmployeeFilter,
                           ProjectContributorFilter.EmployeeFilter,
                           EmployeesAndProjectsFilter.EmployeeFilter,
                           ProjectTagsFilter.EmployeeFilter,
                           CompanyFilter.EmployeeFilter
                          }.Union(CardIndexServiceHelper.BuildEnumBasedFilters<Employee, EmployeeStatus>(e => e.CurrentEmployeeStatus))
                           .Union(CardIndexServiceHelper.BuildEnumBasedFilters<Employee, EmploymentType>(e => e.CurrentEmploymentType)));
            }
        }

        /// <summary>
        /// Gets default start date for allocation grid
        /// </summary>
        /// <returns></returns>
        public DateTime GetDefaultFromValue()
        {
            return _timeService.GetCurrentDate().GetPreviousDayOfWeek(DayOfWeek.Monday);
        }

        /// <summary>
        /// Gets default end date for allocation grid
        /// </summary>
        /// <returns></returns>
        public DateTime GetDefaultToValue()
        {
            return _timeService.GetCurrentDate().AddMonths(2).GetLastDayInMonth();
        }

        public IEnumerable<OverAllocatedWarningDto> ValidateAddAllocationRequest(AllocationRequestDto requestDto)
        {
            var allocationsDictionary = _dailyAllocationDataService.CalculateAllocations(requestDto.EmployeeIds, requestDto.StartDate,
                requestDto.EndDate ?? GetDefaultToValue());

            var employeeDictionary = _employeeService.GetRecordsByIds(requestDto.EmployeeIds);

            foreach (var employeeAllocations in allocationsDictionary)
            {
                if (employeeDictionary.ContainsKey(employeeAllocations.Key))
                {
                    var employeeDto = employeeDictionary[employeeAllocations.Key];
                    var hasProjectAllocations = employeeAllocations.Value
                        .Where(s => s.Status != AllocationStatus.Unavailable)
                        .SelectMany(a => a.ProjectAllocations)
                        .Any(p => p.ProjectId == requestDto.ProjectId);

                    var isOverallocated = employeeAllocations.Value
                        .Where(s => s.Status != AllocationStatus.Unavailable)
                        .Select(
                        x => new
                        {
                            x.AvailableHours,
                            HoursAllocated = x.ProjectAllocations.Sum(p => p.HoursAllocated) + WeeklyHoursHelper.GetHoursByDay(requestDto, x.AllocationPeriod.DayOfWeek)
                        })
                        .Any(x => x.AvailableHours < x.HoursAllocated);


                    if (hasProjectAllocations)
                    {
                        yield return new OverAllocatedWarningDto { Employee = employeeDto, ProjectId = requestDto.ProjectId, Period = requestDto.StartDate };
                    }

                    if (isOverallocated)
                    {
                        yield return new OverAllocatedWarningDto { Employee = employeeDto };
                    }
                }
            }
        }

        public IEnumerable<OverAllocatedWarningDto> ValidateEditAllocationRequest(AllocationRequestDto requestDto)
        {
            var allocationsDictionary = _dailyAllocationDataService.CalculateAllocations(new[] { requestDto.EmployeeId }, requestDto.StartDate,
                requestDto.EndDate ?? GetDefaultToValue());

            var employeeDto = _employeeService.GetRecordById(requestDto.EmployeeId);
            var allocationRequest = _allocationRequestCardIndexDataService.GetRecordById(requestDto.Id);

            foreach (var employeeAllocations in allocationsDictionary)
            {
                var overallocated = employeeAllocations.Value.Select(
                    x => new
                    {
                        x.AllocationPeriod,
                        x.AvailableHours,
                        HoursAllocated =
                        x.ProjectAllocations.Sum(p => p.HoursAllocated)
                            - WeeklyHoursHelper.GetHoursByDay(allocationRequest, x.AllocationPeriod.DayOfWeek)
                            + WeeklyHoursHelper.GetHoursByDay(requestDto, x.AllocationPeriod.DayOfWeek)
                    })
                    .Where(x => x.AvailableHours < x.HoursAllocated);

                if (overallocated.Any())
                {
                    yield return new OverAllocatedWarningDto { Employee = employeeDto };
                }
            }
        }
    }
}
