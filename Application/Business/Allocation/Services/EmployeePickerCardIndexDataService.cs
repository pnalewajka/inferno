﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Filters;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{
    public class EmployeePickerCardIndexDataService : CardIndexDataService<EmployeeDto, Employee, IAllocationDbScope>,
        IEmployeePickerCardIndexDataService
    {
        private readonly IOrgUnitService _orgUnitService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IPrincipalProvider _principalProvider;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public EmployeePickerCardIndexDataService(ICardIndexServiceDependencies<IAllocationDbScope> dependencies,
            IOrgUnitService orgUnitService,
            ISystemParameterService systemParameterService,
            IPrincipalProvider principalProvider)
            : base(dependencies)
        {
            _orgUnitService = orgUnitService;
            _systemParameterService = systemParameterService;
            _principalProvider = principalProvider;
        }

        protected override IOrderedQueryable<Employee> GetDefaultOrderBy(IQueryable<Employee> records, QueryCriteria criteria)
        {
            return records.OrderBy(e => e.LastName).ThenBy(e => e.FirstName);
        }

        protected override NamedFilters<Employee> NamedFilters
        {
            get
            {
                return new NamedFilters<Employee>
                (
                    new[]
                    {
                        MyEmployeesFilter.GetDescendantEmployeeFilter(_orgUnitService, _principalProvider),
                        new NamedFilter<Employee,long,DateTime>(FilterCodes.ProjectAndDateFilter,
                            (e, projectId, date) => e.AllocationRequests
                                .Where(a => a.StartDate <= date)
                                .Select(a => a.ProjectId)
                                .Contains(projectId)),
                        new NamedFilter<Employee>(FilterCodes.AllEmployees, e => true)
                    }
                    .Union(CardIndexServiceHelper.BuildEnumBasedFilters<Employee, EmployeeStatus>(e => e.CurrentEmployeeStatus))
                );
            }
        }

        protected override IEnumerable<Expression<Func<Employee, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return a => a.FirstName;
            yield return a => a.LastName;
            yield return a => a.Acronym;
        }

        public override IQueryable<Employee> ApplyContextFiltering(IQueryable<Employee> records, object context)
        {
            var employeePickerContext = context as EmployeePickerContext;
            var currentPrincipalEmployeeId = PrincipalProvider.Current.EmployeeId;

            if (employeePickerContext != null)
            {
                var managerId = _principalProvider.Current.EmployeeId;
                var managerOrgUnitIds = _orgUnitService.GetManagerOrgUnitDescendantIds(managerId);

                switch (employeePickerContext.Mode)
                {
                    case EmployeePickerContext.PickerMode.EmployeesToChange:
                        {
                            records = records
                                .Where(e => e.Id != currentPrincipalEmployeeId)
                                .Where(new BusinessLogic<Employee, bool>(e =>
                                    EmployeeBusinessLogic.IsWorking.Call(e) &&
                                    EmployeeBusinessLogic.IsEmployeeOf.Call(e, managerId, managerOrgUnitIds)).AsExpression());

                            break;
                        }

                    case EmployeePickerContext.PickerMode.EmployeesToTerminate:
                        {
                            records = records
                                .Where(e => e.Id != currentPrincipalEmployeeId)
                                .Where(EmployeeBusinessLogic.IsWorking);

                            if (!_principalProvider.Current.IsInRole(SecurityRoleType.CanTerminateAllEmployees))
                            {
                                records = _principalProvider.Current.IsInRole(SecurityRoleType.CanTerminateMyEmployees)
                                    ? records.Where(EmployeeBusinessLogic.IsEmployeeOf.Parametrize(managerId, managerOrgUnitIds))
                                    : records.Where(e => false);
                            }

                            break;
                        }

                    case EmployeePickerContext.PickerMode.Recruiters:
                        {
                            var recruiterGroupName = _systemParameterService.GetParameter<string>(ParameterKeys.RecruitmentSpecialistAdGroup);

                            records = records.Where(new BusinessLogic<Employee, bool>(e =>
                                EmployeeBusinessLogic.IsWorking.Call(e) &&
                                e.User.Profiles.Any(p => p.ActiveDirectoryGroupName == recruiterGroupName)).AsExpression());

                            break;
                        }

                    case EmployeePickerContext.PickerMode.ReportingRecruiters:
                        {
                            records = FilterRecruiters(records);

                            if (_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateRecruitmentReportsForAllEmployees))
                            {
                                records = records.Where(p => true);
                            }
                            else if (_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateRecruitmentReportsForMyEmployees))
                            {
                                records = records.Where(p => p.LineManagerId == _principalProvider.Current.EmployeeId);
                            }
                            else
                            {
                                records = records.Where(p => false);
                            }

                            break;
                        }

                    case EmployeePickerContext.PickerMode.ReportingHiringManagers:
                        {
                            records = FilterHiringManagers(records);

                            if (_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateRecruitmentReportForAllJobOpenings))
                            {
                                records = records.Where(p => true);
                            }
                            else if (_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateRecruitmentReportForMyHiringManagerJobOpenings))
                            {
                                records = records.Where(p => p.LineManagerId == _principalProvider.Current.EmployeeId);
                            }
                            else if (_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateRecruitmentReportForMyJobOpenings))
                            {
                                records = records.Where(p => p.Id == _principalProvider.Current.EmployeeId);
                            }
                            else
                            {
                                records = records.Where(p => false);
                            }

                            break;
                        }

                    case EmployeePickerContext.PickerMode.HiringManagers:
                        {
                            records = FilterHiringManagers(records);
                            break;
                        }

                    case EmployeePickerContext.PickerMode.NotTerminatedEmployees:
                        {
                            records = records.Where(EmployeeBusinessLogic.IsNotTerminated.AsExpression());

                            break;
                        }

                    case EmployeePickerContext.PickerMode.RecruitmentDataCertified:
                        {
                            records = records.Where(EmployeeBusinessLogic.CanWorkWithRecruitmentData.AsExpression());

                            break;
                        }

                    case EmployeePickerContext.PickerMode.TechnicalInterviewers:
                        {
                            records = records.Where(EmployeeBusinessLogic.IsTechnicalInterviewer.AsExpression());

                            break;
                        }

                    case EmployeePickerContext.PickerMode.RecruitmentDataWatcher:
                        {
                            records = records.Where(EmployeeBusinessLogic.CanBeRecruitmentDataWatcher.AsExpression());

                            break;
                        }

                    case EmployeePickerContext.PickerMode.RecruitmentDataCertifiedOrTechnicalInterviewers:
                        {
                            var condition = EmployeeBusinessLogic.IsTechnicalInterviewer.AsExpression()
                                .Or(EmployeeBusinessLogic.CanWorkWithRecruitmentData.AsExpression());

                            records = records.Where(condition);

                            break;
                        }
                }
            }

            return records;
        }

        protected override IQueryable<Employee> ConfigureIncludes(IQueryable<Employee> sourceQueryable)
        {
            var querable = base.ConfigureIncludes(sourceQueryable);

            return querable.Include(e => e.Resumes.Select(r => r.ResumeTechnicalSkills.Select(t => t.TechnicalSkill)));
        }

        private IQueryable<Employee> FilterRecruiters(IQueryable<Employee> records)
        {
            var recruiterGroupName = _systemParameterService.GetParameter<string>(ParameterKeys.RecruitmentSpecialistAdGroup);

            return records.Where(new BusinessLogic<Employee, bool>(e =>
                EmployeeBusinessLogic.IsWorking.Call(e)
                && e.User.Profiles.Any(p => p.ActiveDirectoryGroupName == recruiterGroupName)).AsExpression());
        }

        private IQueryable<Employee> FilterHiringManagers(IQueryable<Employee> records)
        {
            var recruiterManagerGroupName = _systemParameterService.GetParameter<string>(ParameterKeys.HiringManagerAdGroup);

            return records
                .Where(EmployeeBusinessLogic.CanWorkWithRecruitmentData.AsExpression())
                .Where(new BusinessLogic<Employee, bool>(e =>
                    EmployeeBusinessLogic.IsWorking.Call(e)
                    && e.User.Profiles.Any(p => p.ActiveDirectoryGroupName == recruiterManagerGroupName)).AsExpression());
        }
    }
}