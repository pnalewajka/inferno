using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Helpers;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{
    internal class DailyAllocationDataService
    {
        private readonly IDateRangeService _dateRangeService;
        private readonly IReadOnlyUnitOfWorkService<IAllocationDbScope> _unitOfWorkAllocationService;
        private readonly ICalendarDataService _calendarDataService;
        private readonly IEmployeeService _employeeService;
        private readonly IEmployeeAvailabilityDataService _employeeAvailabilityDataService;

        public DailyAllocationDataService(
            IDateRangeService dateRangeService,
            IReadOnlyUnitOfWorkService<IAllocationDbScope> unitOfWorkAllocationService,
            ICalendarDataService calendarDataService,
            IEmployeeService employeeService,
            IEmployeeAvailabilityDataService employeeAvailabilityDataService)
        {
            _dateRangeService = dateRangeService;
            _unitOfWorkAllocationService = unitOfWorkAllocationService;
            _calendarDataService = calendarDataService;
            _employeeService = employeeService;
            _employeeAvailabilityDataService = employeeAvailabilityDataService;
        }

        public IDictionary<long, IReadOnlyCollection<EmployeeAllocationDto.EmployeeAllocationSingleRecordDto>>
            CalculateAllocations(IReadOnlyCollection<long> employeeIds, DateTime dateFrom, DateTime dateTo)
        {
            var resultDictionary =
                new Dictionary<long, IReadOnlyCollection<EmployeeAllocationDto.EmployeeAllocationSingleRecordDto>>();

            var absencePeriodsDictionary = _employeeAvailabilityDataService.GetDetailedAbsencesForEmployees(employeeIds, dateFrom, dateTo).ToList();
            var userEmployeeDictionary =
                _employeeService.GetUserIdToEmployeeIdDictionary(
                    absencePeriodsDictionary
                        .Select(a => a.AbsenceRequest.AffectedUserId)
                        .Distinct()
                        .ToList());

            using (var unit = _unitOfWorkAllocationService.Create())
            {
                var allocationRequestsInProgress = unit.Repositories.AllocationRequests
                        .Where(ar => ar.AllocationRequestMetadata.ProcessingStatus == ProcessingStatus.Processing &&
                                     employeeIds.Contains(ar.EmployeeId))
                        .GroupBy(e => e.EmployeeId)
                        .ToDictionary(a => a.Key, a => a.ToList());

                var dailyAllocations =
                    unit.Repositories.AllocationDailyRecords
                        .Where(adl => employeeIds.Contains(adl.EmployeeId) && adl.Day <= dateTo && adl.Day >= dateFrom)
                        .Include(adr => adr.Project)
                        .ToList();

                var dailyAllocationsDictionary = dailyAllocations
                    .GroupBy(da => new EmployeeAllocationTimePeriodIdentifier
                    {
                        EmployeeId = da.EmployeeId,
                        AllocationPeriodStart = da.Day
                    })
                    .ToDictionary(da => da.Key);

                var workPeriods = GetEmploymentPeriodsForActiveEmployees(employeeIds, dateFrom, dateTo, unit);

                var days = _dateRangeService.GetDaysInRange(dateFrom, dateTo).ToList();

                foreach (var employeeId in employeeIds)
                {
                    var allocations = new List<EmployeeAllocationDto.EmployeeAllocationSingleRecordDto>();
                    var employeeAllocationRequests = allocationRequestsInProgress.ContainsKey(employeeId) ? allocationRequestsInProgress[employeeId] : new List<AllocationRequest>();

                    foreach (var day in days)
                    {
                        var key = new EmployeeAllocationTimePeriodIdentifier
                        {
                            EmployeeId = employeeId,
                            AllocationPeriodStart = day
                        };

                        var periodForDay = workPeriods.FirstOrDefault(wp =>
                            wp.EmployeeId == employeeId &&
                            day >= wp.StartDate && (!wp.EndDate.HasValue || day <= wp.EndDate));

                        var periodAbsenceRequests =
                            absencePeriodsDictionary.Where(
                                a =>
                                    userEmployeeDictionary[a.AbsenceRequest.AffectedUserId] == employeeId
                                    && day >= a.AbsenceRequest.From && day <= a.AbsenceRequest.To)
                                .ToList();

                        var availableHours = WeeklyHoursHelper.GetHoursByDayOrDefault(periodForDay, day.DayOfWeek);
                        var hasActivePeriod = dailyAllocationsDictionary.ContainsKey(key);

                        var isAllocationRequestInProgress = employeeAllocationRequests.Any(ar => day >= ar.StartDate && (!ar.EndDate.HasValue || day <= ar.EndDate));

                        var allocationStatus = isAllocationRequestInProgress ? AllocationStatus.Processing : hasActivePeriod ? default(AllocationStatus) : AllocationStatus.Unavailable;

                        allocations.Add(new EmployeeAllocationDto.EmployeeAllocationSingleRecordDto
                        {
                            AllocationPeriod = day,
                            AvailableHours = availableHours,
                            Status = allocationStatus,
                            ProjectAllocations =
                                !hasActivePeriod
                                ? new EmployeeAllocationDto.ProjectAllocationDto[0]
                                : dailyAllocationsDictionary[key].Select(dea =>
                                {
                                    var isVacation = !dea.IsWorkDay && dea.Hours > 0;

                                    if (isVacation)
                                    {
                                        var vacationDescription = _calendarDataService.GetCalendarDataById(dea.CalendarId).KnownHolidays.GetHolidayDescriptionByDate(dea.Day);

                                        return new EmployeeAllocationDto.ProjectAllocationDto
                                        {
                                            ProjectName = vacationDescription,
                                            ProjectColor = "#000",
                                            HoursAllocated = decimal.Zero,
                                            IsVacation = true,
                                            Certainty = dea.AllocationRequest.AllocationCertainty
                                        };
                                    }

                                    return new EmployeeAllocationDto.ProjectAllocationDto
                                    {
                                        ProjectName = ProjectBusinessLogic.ClientProjectName.Call(dea.Project),
                                        ProjectId = dea.Project.Id,
                                        ProjectColor = dea.Project.ProjectSetup.Color,
                                        HoursAllocated = dea.Hours,
                                        IsVacation = false,
                                        Certainty = dea.AllocationRequest.AllocationCertainty
                                    };
                                }).ToArray(),
                            EmployeeAbsences = periodAbsenceRequests.Select(r => r.AbsenceRequest).ToList(),
                        });
                    }
                    resultDictionary.Add(employeeId, allocations);
                }
            }

            return resultDictionary;
        }

        private List<EmploymentPeriod> GetEmploymentPeriodsForActiveEmployees(IEnumerable<long> employeeIds, DateTime from,
            DateTime to, IReadOnlyUnitOfWork<IAllocationDbScope> unitOfWork)
        {
            return unitOfWork.Repositories.EmploymentPeriods
                .Where(ep => employeeIds.Contains(ep.EmployeeId) && ep.IsActive
                             &&
                             !((ep.StartDate < from && ep.EndDate.HasValue && ep.EndDate < from) || ep.StartDate > to))
                .ToList();
        }
    }
}