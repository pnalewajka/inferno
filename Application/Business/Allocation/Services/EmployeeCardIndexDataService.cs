﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Castle.Core.Internal;
using Castle.Core.Logging;
using Smt.Atomic.Business.ActiveDirectory.Dto;
using Smt.Atomic.Business.ActiveDirectory.Interfaces;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Filters;
using Smt.Atomic.Business.Allocation.Helpers;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Allocation.Resources;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Dictionaries.BusinessLogic;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.Business.Survey.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Resumes;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{
    public class EmployeeCardIndexDataService : CardIndexDataService<EmployeeDto, Employee, IAllocationDbScope>,
        IEmployeeCardIndexDataService
    {
        private readonly IClassMapping<Employee, EmployeeDto> _employeeClassMapping;
        private readonly IOrgUnitService _orgUnitService;
        private readonly ILocationService _locationService;
        private readonly ISurveyRespondentService _surveyRespondentService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IReadOnlyUnitOfWorkService<IAllocationDbScope> _unitOfWorkAllocationService;
        private readonly IActiveDirectoryWriter _activeDirectoryWriter;
        private readonly IJobTitleCardIndexDataService _jobTitleService;
        private readonly ILogger _logger;
        private readonly IDataActivityLogService _dataActivityLogService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public EmployeeCardIndexDataService(ICardIndexServiceDependencies<IAllocationDbScope> dependencies,
            IReadOnlyUnitOfWorkService<IAllocationDbScope> unitOfWorkAllocationService,
            IOrgUnitService orgUnitService,
            ILocationService locationService,
            ISurveyRespondentService surveyRespondentService,
            IPrincipalProvider principalProvider,
            IClassMapping<Employee, EmployeeDto> employeeClassMapping,
            IActiveDirectoryWriter activeDirectoryWriter,
            IJobTitleCardIndexDataService jobTitleService,
            ILogger logger,
            IDataActivityLogService dataActivityLogService)
            : base(dependencies)
        {
            _orgUnitService = orgUnitService;
            _locationService = locationService;
            _unitOfWorkAllocationService = unitOfWorkAllocationService;
            _surveyRespondentService = surveyRespondentService;
            _principalProvider = principalProvider;
            _employeeClassMapping = employeeClassMapping;
            _activeDirectoryWriter = activeDirectoryWriter;
            _jobTitleService = jobTitleService;
            _logger = logger;
            _dataActivityLogService = dataActivityLogService;
        }

        public override IQueryable<Employee> ApplyContextFiltering(IQueryable<Employee> records, object context)
        {
            if (!PrincipalProvider.Current.IsInRole(SecurityRoleType.CanViewEmployeeStatus))
            {
                records = records.Where(x => x.CurrentEmployeeStatus != EmployeeStatus.Terminated);
            }

            return base.ApplyContextFiltering(records, context);
        }

        protected override NamedFilters<Employee> NamedFilters
        {
            get
            {
                return new NamedFilters<Employee>(
                    new[] {
                                MyEmployeesFilter.GetDescendantEmployeeFilter(_orgUnitService, _principalProvider),
                                MyEmployeesFilter.GetChildEmployeeFilter(_orgUnitService, _principalProvider),
                                AllEmployeesFilter.EmployeeFilter,
                                LineManagerFilter.EmployeeFilter,
                                OrgUnitsFilter.EmployeeFilter,
                                JobProfileFilter.EmployeeFilter,
                                ContractTypeFilter.EmployeeFilter,
                                SkillsAndKnowledgeFilter.EmployeeFilter,
                                LocationFilter.EmployeeFilter,
                                ProjectContributorFilter.EmployeeFilter,
                                CompanyFilter.EmployeeFilter,
                                IndustriesFilter.EmployeeFilter,
                                SkillsFilter.EmployeeFilter,
                                LanguageWithLevelBusinessLogic.EmployeeFilter
                            }.Union(CardIndexServiceHelper.BuildEnumBasedFilters<Employee, EmployeeStatus>(e => e.CurrentEmployeeStatus, SecurityRoleType.CanViewEmployeeStatus))
                             .Union(CardIndexServiceHelper.BuildEnumBasedFilters<Employee, EmploymentType>(e => e.CurrentEmploymentType))
                             .Union(CardIndexServiceHelper.BuildEnumBasedFilters<Employee, PlaceOfWork>(e => e.PlaceOfWork)));
            }
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<Employee> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var direction = sortingCriterion.GetSortingDirection();

            if (sortingCriterion.GetSortingColumnName() == nameof(EmployeeDto.JobProfileIds))
            {
                EmployeeSortHelper.OrderByJobProfile(orderedQueryBuilder, direction);
            }
            else if (sortingCriterion.GetSortingColumnName() == nameof(EmployeeDto.LineManagerFullName))
            {
                EmployeeSortHelper.OrderByLineManager(orderedQueryBuilder, direction);
            }
            else if (sortingCriterion.GetSortingColumnName() == nameof(EmployeeDto.JobMatrixName))
            {
                EmployeeSortHelper.OrderByJobMatrices(orderedQueryBuilder, direction);
            }
            else if (sortingCriterion.GetSortingColumnName() == nameof(EmployeeDto.JobProfileNames))
            {
                EmployeeSortHelper.OrderByJobProfile(orderedQueryBuilder, direction);
            }
            else if (sortingCriterion.GetSortingColumnName() == nameof(EmployeeDto.SkillNames))
            {
                EmployeeSortHelper.OrderBySkills(orderedQueryBuilder, direction);
            }
            else if (sortingCriterion.GetSortingColumnName() == nameof(EmployeeDto.SkillIds))
            {
                EmployeeSortHelper.OrderBySkills(orderedQueryBuilder, direction);
            }
            else if (sortingCriterion.GetSortingColumnName() == nameof(EmployeeDto.JobMatrixIds))
            {
                EmployeeSortHelper.OrderByJobMatrices(orderedQueryBuilder, direction);
            }
            else if (sortingCriterion.GetSortingColumnName() == nameof(EmployeeDto.FillPercentage))
            {
                EmployeeSortHelper.OrderByResumeFillPercetage(orderedQueryBuilder, direction);
            }
            else if (sortingCriterion.GetSortingColumnName() == nameof(EmployeeDto.ResumeModificationDate))
            {
                EmployeeSortHelper.OrderByResumeModificationDate(orderedQueryBuilder, direction);
            }
            else
            {
                base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
            }
        }

        public IReadOnlyCollection<EmployeeDto> GetChartsResponsibleManagers()
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var employees = unitOfWork.Repositories.OrgUnits
                    .OrderBy(ou => ou.NameSortOrder)
                    .Where(ou => ou.EmployeeId.HasValue && ou.OrgUnitFeatures.HasFlag(OrgUnitFeatures.ShowChart))
                    .Select(ou => ou.Employee);

                var employeeDtos = employees.Select(_employeeClassMapping.CreateFromSource).ToList();

                return employeeDtos.GroupBy(rm => rm.Id, rm => rm)
                    .Select(g => g.First())
                    .ToList();
            }
        }

        protected override IEnumerable<Expression<Func<Employee, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            if (searchCriteria.Includes(SearchAreaCodes.FirstName))
            {
                yield return a => a.FirstName;
            }

            if (searchCriteria.Includes(SearchAreaCodes.LastName))
            {
                yield return a => a.LastName;
            }

            if (searchCriteria.Includes(SearchAreaCodes.Login))
            {
                yield return a => a.User.Login;
            }

            if (searchCriteria.Includes(SearchAreaCodes.Acronym))
            {
                yield return a => a.Acronym;
            }

            if (searchCriteria.Includes(SearchAreaCodes.LineMaganer))
            {
                yield return a => a.LineManager.FirstName;
                yield return a => a.LineManager.LastName;
            }

            if (searchCriteria.Includes(SearchAreaCodes.OrganizationUnit))
            {
                yield return a => a.OrgUnit.Name;
            }

            if (searchCriteria.Includes(SearchAreaCodes.JobTitle))
            {
                yield return a => a.JobTitle.NameEn;
                yield return a => a.JobTitle.NamePl;
            }

            if (searchCriteria.Includes(SearchAreaCodes.Skills))
            {
                yield return a => a.Resumes.SelectMany(r => r.ResumeAdditionalSkills).Select(s => s.Name);
                yield return a => a.Resumes.SelectMany(r => r.ResumeTechnicalSkills).Select(s => s.TechnicalSkill.Name);
            }
        }

        protected override IQueryable<Employee> ConfigureIncludes(IQueryable<Employee> sourceQueryable)
        {
            return sourceQueryable
                .Include(e => e.JobMatrixs)
                .Include(e => e.JobProfiles)
                .Include(e => e.Location)
                .Include(e => e.JobTitle)
                .Include(e => e.OrgUnit)
                .Include(e => e.LineManager)
                .Include(e => e.User)
                .Include(e => e.Resumes)
                .Include(e => e.Resumes.Select(r => r.ResumeTechnicalSkills.Select(t => t.TechnicalSkill)));
        }

        protected override void OnRecordAdding(
            RecordAddingEventArgs<Employee, EmployeeDto, IAllocationDbScope> eventArgs)
        {
            ValidatePlaceOfWork(eventArgs.InputDto);

            eventArgs.InputEntity.JobProfiles =
                eventArgs.UnitOfWork.Repositories.JobProfiles.GetByIds(eventArgs.InputDto.JobProfileIds).ToArray();

            var jobMatrixLevels = GetJobMatrixLevels(eventArgs.InputDto.JobTitleId);
            eventArgs.InputEntity.JobMatrixs =
                eventArgs.UnitOfWork.Repositories.JobMatrixLevels.GetByIds(jobMatrixLevels).ToArray();

            CreateOrUpdateResumeTechnicalSkills(eventArgs.UnitOfWork, eventArgs.InputEntity.Resumes, eventArgs.InputDto.SkillIds.ToArray());
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<Employee, EmployeeDto> recordAddedEventArgs)
        {
            _surveyRespondentService.AddOrgUnitSurveysForEmployee(recordAddedEventArgs.InputEntity);
            _dataActivityLogService.OnEmployeeAdded(recordAddedEventArgs.InputEntity.Id);
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<Employee, EmployeeDto> recordEditedEventArgs)
        {
            _dataActivityLogService.LogEmployeeDataActivity(recordEditedEventArgs.InputEntity.Id, ActivityType.UpdatedRecord);
        }

        protected override void OnRecordEditing(
            RecordEditingEventArgs<Employee, EmployeeDto, IAllocationDbScope> eventArgs)
        {
            ValidatePlaceOfWork(eventArgs.InputDto);

            var jobMatrixLevels = GetJobMatrixLevels(eventArgs.InputDto.JobTitleId).ToArray();

            eventArgs.InputDto.JobMatrixIds = jobMatrixLevels;

            UpdateActiveDirectoryUser(eventArgs);

            eventArgs.InputEntity.JobMatrixs =
                eventArgs.UnitOfWork.Repositories.JobMatrixLevels.GetByIds(jobMatrixLevels).ToArray();
            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.JobMatrixs,
                eventArgs.InputEntity.JobMatrixs);

            eventArgs.InputEntity.JobProfiles =
                eventArgs.UnitOfWork.Repositories.JobProfiles.GetByIds(eventArgs.InputDto.JobProfileIds).ToArray();
            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.JobProfiles,
                eventArgs.InputEntity.JobProfiles);

            CreateOrUpdateResumeTechnicalSkills(eventArgs.UnitOfWork, eventArgs.DbEntity.Resumes, eventArgs.InputDto.SkillIds.ToArray());
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<Employee, IAllocationDbScope> eventArgs)
        {
            using (var unitOfWorkAllocationService = _unitOfWorkAllocationService.Create())
            {
                var dailyRecordsConnectedWithAllocation =
                    (from x in unitOfWorkAllocationService.Repositories.AllocationDailyRecords
                     where x.EmployeeId == eventArgs.DeletingRecord.Id
                     select x.Id).ToList();

                if (dailyRecordsConnectedWithAllocation.Any())
                {
                    eventArgs.Alerts.Add(new AlertDto
                    {
                        Message = string.Format(AlertResources.EmployeeDeletingAlert,
                            eventArgs.DeletingRecord.FirstName,
                            eventArgs.DeletingRecord.LastName),
                        Type = AlertType.Error
                    });
                }
            }

            eventArgs.UnitOfWork.Repositories.DataOwners.Where(d => d.EmployeeId == eventArgs.DeletingRecord.Id).ForEach(d => d.EmployeeId = null);
        }
        
        private void ValidatePlaceOfWork(EmployeeDto employee)
        {
            if (employee.LocationId.HasValue && employee.PlaceOfWork.HasValue)
            {
                if (!_locationService.CheckIfPlaceOfWorkIsAllowedForLocation(employee.LocationId.Value, employee.PlaceOfWork.Value))
                {
                    throw new BusinessException(AlertResources.InvalidPlaceOfWorkAlert);
                }
            }
        }

        private IEnumerable<long> GetJobMatrixLevels(long? jobTitleId)
        {
            var jobMatrixLevels = new List<long>();

            if (!jobTitleId.HasValue)
            {
                return jobMatrixLevels;
            }

            var jobTitle = _jobTitleService.GetRecordById(jobTitleId.Value);

            if (jobTitle.JobMatrixLevelId != null)
            {
                jobMatrixLevels.Add(jobTitle.JobMatrixLevelId.Value);
            }

            return jobMatrixLevels;
        }

        private void UpdateActiveDirectoryUser(RecordEditingEventArgs<Employee, EmployeeDto, IAllocationDbScope> eventArgs)
        {
            var newEmployeeData = eventArgs.InputDto;
            var oldEmployeeData = eventArgs.DbEntity;

            var oldJobMatrixs = oldEmployeeData.JobMatrixs.Select(x => x.Id).ToArray();
            var oldJobProfiles = oldEmployeeData.JobProfiles.Select(x => x.Id).ToArray();

            var activeDirectoryUserChangeDescription = new ActiveDirectoryUserChangeDescriptionDto
            {
                EmployeeId = newEmployeeData.Id
            };

            if (!ArrayHelper.AreEqual(newEmployeeData.JobMatrixIds.ToArray(), oldJobMatrixs))
            {
                activeDirectoryUserChangeDescription.JobMatrixLevelIds = newEmployeeData.JobMatrixIds.ToArray();
            }

            if (!ArrayHelper.AreEqual(newEmployeeData.JobProfileIds.ToArray(), oldJobProfiles))
            {
                activeDirectoryUserChangeDescription.JobProfileIds = newEmployeeData.JobProfileIds.ToArray();
            }

            if (newEmployeeData.FirstName != oldEmployeeData.FirstName)
            {
                activeDirectoryUserChangeDescription.FirstName = newEmployeeData.FirstName;
            }

            if (newEmployeeData.LastName != oldEmployeeData.LastName)
            {
                activeDirectoryUserChangeDescription.LastName = newEmployeeData.LastName;
            }

            if (newEmployeeData.Email != oldEmployeeData.Email)
            {
                activeDirectoryUserChangeDescription.Email = newEmployeeData.Email;
            }

            if (newEmployeeData.JobTitleId.HasValue && newEmployeeData.JobTitleId != oldEmployeeData.JobTitleId)
            {
                activeDirectoryUserChangeDescription.JobTitleId = newEmployeeData.JobTitleId.Value;
            }

            if (newEmployeeData.CompanyId != oldEmployeeData.CompanyId)
            {
                activeDirectoryUserChangeDescription.CompanyId = newEmployeeData.CompanyId;
            }

            if (newEmployeeData.LocationId.HasValue && newEmployeeData.LocationId != oldEmployeeData.LocationId)
            {
                activeDirectoryUserChangeDescription.LocationId = newEmployeeData.LocationId.Value;
            }

            if (newEmployeeData.PlaceOfWork.HasValue && newEmployeeData.PlaceOfWork != oldEmployeeData.PlaceOfWork)
            {
                activeDirectoryUserChangeDescription.PlaceOfWork = newEmployeeData.PlaceOfWork.Value;
            }

            if (newEmployeeData.LineManagerId != oldEmployeeData.LineManagerId)
            {
                activeDirectoryUserChangeDescription.LineManagerId = newEmployeeData.LineManagerId;
            }

            if (newEmployeeData.OrgUnitId != oldEmployeeData.OrgUnitId)
            {
                activeDirectoryUserChangeDescription.OrgUnitId = newEmployeeData.OrgUnitId;
            }

            try
            {
                var result = _activeDirectoryWriter.ModifyActiveDirectoryUser(activeDirectoryUserChangeDescription);

                if (result.Alerts.Any())
                {
                    eventArgs.Alerts.AddRange(result.Alerts);
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Modify ActiveDirectory user", ex);
                eventArgs.AddError(AlertResources.ProblemWithUpdateActiveDirectory);
            }
        }

        private void CreateOrUpdateResumeTechnicalSkills(IUnitOfWork<IAllocationDbScope> unitOfWork, ICollection<Resume> resumes, long[] technicalSkillIds)
        {
            var resume = GetOrCreateResume(unitOfWork.Repositories, resumes);
            var skills = unitOfWork.Repositories.Skills
                .GetByIds(technicalSkillIds)
                .Select(s => s.Id)
                .AsEnumerable()
                .Select(i => new ResumeTechnicalSkill() { TechnicalSkillId = i, Resume = resume })
                .ToList();

            EntityMergingService.MergeCollections(unitOfWork, resume.ResumeTechnicalSkills, skills, s => s.TechnicalSkillId, true);
        }

        private static Resume GetOrCreateResume(IAllocationDbScope dbScope, ICollection<Resume> resumes)
        {
            var resume = resumes.SingleOrDefault();

            if (resume != null)
            {
                return resume;
            }

            resume = dbScope.Resumes.CreateEntity();
            resume.ResumeTechnicalSkills = new List<ResumeTechnicalSkill>();
            resumes.Add(resume);

            return resume;
        }
    }
}