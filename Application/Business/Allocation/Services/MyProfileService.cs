﻿using System.Collections.Generic;
using Smt.Atomic.Business.Allocation.Interfaces;
using System.Linq;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using System.Runtime.InteropServices;
using Castle.Core.Logging;
using Smt.Atomic.Business.Allocation.Resources;
using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.ActiveDirectory;
using Smt.Atomic.Business.Workflows.BusinessLogics;

namespace Smt.Atomic.Business.Allocation.Services
{
    public class MyProfileService : IMyProfileService
    {
        private readonly IUnitOfWorkService<IAllocationDbScope> _unitOfWorkService;
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _timetrackingUnitOfWorkService;
        private readonly IUnitOfWorkService<ISurveyDbScope> _surveyUnitOfWorkService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IClassMapping<Employee, MyProfileDto> _employeeToMyProfileDtoClassMapping;
        private readonly IClassMapping<Data.Entities.Modules.Survey.Survey, SurveyDto> _surveyToSurveyDtoClassMapping;
        private readonly ISettingsProvider _settingsProvider;
        private readonly ITimeService _timeService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly ILogger _logger;
        private const string AdEmailGroupIdentifier = "ou=mail";

        public MyProfileService(
            IUnitOfWorkService<IAllocationDbScope> unitOfWorkService,
            IClassMapping<Employee, MyProfileDto> employeeToMyProfileDtoClassMapping,
            IClassMapping<Data.Entities.Modules.Survey.Survey, SurveyDto> surveyToSurveyDtoClassMapping,
            ISettingsProvider settingsProvider,
            ISystemParameterService systemParameterService,
            ILogger logger,
            IUnitOfWorkService<ITimeTrackingDbScope> timetrackingUnitOfWorkService,
            ITimeService timeService,
            IUnitOfWorkService<ISurveyDbScope> surveyUnitOfWorkService,
            IPrincipalProvider principalProvider)
        {
            _unitOfWorkService = unitOfWorkService;
            _employeeToMyProfileDtoClassMapping = employeeToMyProfileDtoClassMapping;
            _surveyToSurveyDtoClassMapping = surveyToSurveyDtoClassMapping;
            _settingsProvider = settingsProvider;
            _systemParameterService = systemParameterService;
            _logger = logger;
            _timetrackingUnitOfWorkService = timetrackingUnitOfWorkService;
            _timeService = timeService;
            _surveyUnitOfWorkService = surveyUnitOfWorkService;
            _principalProvider = principalProvider;
        }

        public MyProfileDto GetMyProfile(long userId, long employeeId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var sourceEmployee = unitOfWork.Repositories.Employees.GetById(employeeId);
                var resultProfile = _employeeToMyProfileDtoClassMapping.CreateFromSource(sourceEmployee);
                using (var context = new CoreDirectoryContext(_settingsProvider, useCustomResultMapper: true))
                {
                    resultProfile.Surveys = GetMySurveys(userId);
                    resultProfile.PendingRequestCount = GetPendingRequestCount(userId);

                    var hourToDayRatio = GetHourToDayRatio(unitOfWork, employeeId);

                    using (var ttUnitOfWork = _timetrackingUnitOfWorkService.Create())
                    {
                        resultProfile.AvailableVacations = hourToDayRatio > 0 ? decimal.Floor(GetAvailableVacationHours(ttUnitOfWork, employeeId) / hourToDayRatio) : 0;
                        resultProfile.TotalVacations = hourToDayRatio > 0 ? decimal.Floor(GetTotalVacationHours(ttUnitOfWork, employeeId) / hourToDayRatio) : 0;
                        resultProfile.AvailableOvertime = GetAvailableOvertime(ttUnitOfWork, employeeId);
                    }

                    try
                    {
                        SetActiveDirectoryGroupsNames(unitOfWork, sourceEmployee, resultProfile, context);
                    }
                    catch (COMException ex)
                    {                       
                        _logger.Error("Unable to retrieve data from Active Directory", ex);
                        resultProfile.ActiveDirectoryEmailGroupNames = new Dictionary<long, string> { { 0, MyProfileResources.ActiveDirectoryDataRetrieveError } };
                        resultProfile.ActiveDirectorySecurityGroupNames = new Dictionary<long, string> { { 0, MyProfileResources.ActiveDirectoryDataRetrieveError } };
                    }
                }
                return resultProfile;
            }
        }

        private void SetActiveDirectoryGroupsNames(IUnitOfWork<IAllocationDbScope> unitOfWork, Employee sourceEmployee, MyProfileDto resultProfile, CoreDirectoryContext context)
        {
            var adEmployee = context.Users.Single(u => u.UserName == sourceEmployee.User.Login);
            var emailGroups = adEmployee.Groups.Where(g => g.InternalDn.ToLower().Contains(AdEmailGroupIdentifier)).ToList();
            var securityGroups = adEmployee.Groups.Except(emailGroups).ToList();

            var groupsGuids = emailGroups
                .Select(g => g.Id)
                .Union(securityGroups.Select(g => g.Id))
                .ToList();

            var groupIds = unitOfWork.Repositories.ActiveDirectoryGroups
                .Where(g => groupsGuids.Contains(g.ActiveDirectoryId))
                .Select(g => new { g.Id, g.ActiveDirectoryId })
                .ToList();

            emailGroups = emailGroups.Where(g => groupIds.Select(x => x.ActiveDirectoryId).Contains(g.Id)).ToList();
            securityGroups = securityGroups.Where(g => groupIds.Select(x => x.ActiveDirectoryId).Contains(g.Id)).ToList();
            resultProfile.CanViewActiveDirectoryGroups = _principalProvider.IsSet && _principalProvider.Current.IsInRole(SecurityRoleType.CanViewActiveDirectoryGroup);
            resultProfile.ActiveDirectorySecurityGroupNames = securityGroups.ToDictionary(g => groupIds.First(i => i.ActiveDirectoryId == g.Id).Id, x => x.Name);
            resultProfile.ActiveDirectoryEmailGroupNames = emailGroups.ToDictionary(g => groupIds.First(i => i.ActiveDirectoryId == g.Id).Id, x => x.Name);
        }

        private SurveyDto[] GetMySurveys(long userId)
        {
            using (var unitOfWork = _surveyUnitOfWorkService.Create())
            {
                return
                    unitOfWork.Repositories.Surveys.Where(s => s.RespondentId == userId && !s.SurveyConduct.IsClosed).ToList()
                        .Select(s => _surveyToSurveyDtoClassMapping.CreateFromSource(s))
                        .ToArray();
            }
        }

        private long GetPendingRequestCount(long userId)
        {
            var currentTime = _timeService.GetCurrentTime();

            using (var unitOfWork = _timetrackingUnitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Requests
                    .Count(RequestBusinessLogic.IsWaitingForUserDecision.Parametrize(userId, currentTime));
            }
        }

        private decimal GetHourToDayRatio(IUnitOfWork<IAllocationDbScope> unitOfWork, long employeeId)
        {
            var today = _timeService.GetCurrentDate();

            var hourToDayRatio =
                unitOfWork.Repositories.EmploymentPeriods.Where(
                    ep => ep.EmployeeId == employeeId && ep.IsActive)
                    .FirstOrDefault(ep => ep.StartDate <= today && (!ep.EndDate.HasValue || ep.EndDate.Value > today))?.VacationHourToDayRatio;

            return hourToDayRatio ?? 0;
        }

        private decimal GetTotalVacationHours(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, long employeeId)
        {
            var maxDate = _timeService.GetCurrentDate().GetLastDayOfYear();

            var hourBalance = unitOfWork.Repositories
                                      .VacationBalance
                                      .Where(x => x.EmployeeId == employeeId
                                                && x.Operation != VacationOperation.Vacation
                                                && x.EffectiveOn <= maxDate).Max(vb => (decimal?)vb.TotalHourBalance);

            return hourBalance ?? 0;
        }

        private decimal GetAvailableVacationHours(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, long employeeId)
        {
            var maxDate = _timeService.GetCurrentDate().GetLastDayOfYear();

            var actualVacationBalance = unitOfWork.Repositories
                                      .VacationBalance
                                      .Where(x => x.EmployeeId == employeeId &&
                                                  x.EffectiveOn <= maxDate)
                                      .OrderByDescending(x => x.EffectiveOn)
                                      .ThenByDescending(x => x.Id)
                                      .FirstOrDefault();

            return actualVacationBalance?.TotalHourBalance ?? 0;
        }

        private decimal GetAvailableOvertime(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, long employeeId)
        {
            var actualOvertimeBalance = unitOfWork.Repositories
                                                  .OvertimeBanks
                                                  .Where(x => x.EmployeeId == employeeId)
                                                  .OrderByDescending(x => x.EffectiveOn)
                                                  .ThenByDescending(x => x.Id)
                                                  .FirstOrDefault();

            return actualOvertimeBalance?.OvertimeBalance ?? 0;
        }
    }
}
