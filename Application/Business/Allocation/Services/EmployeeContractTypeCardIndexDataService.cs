﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Filters;
using Smt.Atomic.Business.Allocation.Helpers;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Dictionaries.BusinessLogic;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{
    public class EmployeeContractTypeCardIndexDataService : ReadOnlyCardIndexDataService<EmployeeContractTypeDto, Employee, IAllocationDbScope>, IEmployeeContractTypeCardIndexDataService
    {
        private readonly IOrgUnitService _orgUnitService;

        public EmployeeContractTypeCardIndexDataService(ICardIndexServiceDependencies<IAllocationDbScope> dependencies, 
            IOrgUnitService orgUnitService)
            : base(dependencies)
        {
            _orgUnitService = orgUnitService;
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<Employee> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var direction = sortingCriterion.GetSortingDirection();

            if (sortingCriterion.GetSortingColumnName() == nameof(EmployeeContractTypeDto.JobMatrixLevel))
            {
                EmployeeSortHelper.OrderByJobMatrices(orderedQueryBuilder, direction);
            }
            else
            {
                base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
            }
        }

        protected override IEnumerable<Expression<Func<Employee, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return a => a.FirstName;
            yield return a => a.LastName;
            yield return a => a.Acronym;
        }

        protected override IQueryable<Employee> GetFilteredRecords(IQueryable<Employee> records)
        {
            records = base.GetFilteredRecords(records);

            if (PrincipalProvider.Current.IsInRole(SecurityRoleType.CanViewAllEmployeeContractType))
            {
                return records;
            }

            if (PrincipalProvider.Current.IsInRole(SecurityRoleType.CanViewMyEmployeeContractType))
            {
                return records.Where(EmployeeBusinessLogic.HasGivenLineManager.Parametrize(PrincipalProvider.Current.EmployeeId));
            }

            return records.Where(r => false);
        }

        protected override NamedFilters<Employee> NamedFilters
        {
            get
            {
                return new NamedFilters<Employee>(
                    new[] {
                            MyEmployeesFilter.GetDescendantEmployeeFilter(_orgUnitService, PrincipalProvider),
                            MyEmployeesFilter.GetChildEmployeeFilter(_orgUnitService, PrincipalProvider),
                            AllEmployeesFilter.EmployeeFilter,
                            LineManagerFilter.EmployeeFilter,
                            OrgUnitsFilter.EmployeeFilter,
                            JobProfileFilter.EmployeeFilter,
                            ContractTypeFilter.EmployeeFilter,
                            SkillsAndKnowledgeFilter.EmployeeFilter,
                            LocationFilter.EmployeeFilter,
                            ProjectContributorFilter.EmployeeFilter,
                            CompanyFilter.EmployeeFilter,
                            IndustriesFilter.EmployeeFilter,
                            SkillsFilter.EmployeeFilter,
                            LanguageWithLevelBusinessLogic.EmployeeFilter
                        }.Union(CardIndexServiceHelper.BuildEnumBasedFilters<Employee, EmployeeStatus>(e => e.CurrentEmployeeStatus, SecurityRoleType.CanViewEmployeeStatus))
                        .Union(CardIndexServiceHelper.BuildEnumBasedFilters<Employee, EmploymentType>(e => e.CurrentEmploymentType))
                        .Union(CardIndexServiceHelper.BuildEnumBasedFilters<Employee, PlaceOfWork>(e => e.PlaceOfWork)));
            }
        }
    }
}
