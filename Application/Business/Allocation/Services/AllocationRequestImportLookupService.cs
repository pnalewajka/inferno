﻿using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Smt.Atomic.Business.Allocation.Services
{
    public class AllocationRequestImportLookupService : IAllocationRequestImportLookupService
    {
        private readonly IReadOnlyUnitOfWorkService<IAllocationDbScope> _unitOfWorkAllocationService;
        private Dictionary<long, string> _employeeIdToEmployeeEmailMapping = new Dictionary<long, string>();
        private Dictionary<long, string> _projectIdToProjectCodeMapping = new Dictionary<long, string>();

        public AllocationRequestImportLookupService(IReadOnlyUnitOfWorkService<IAllocationDbScope> unitOfWorkAllocationService)
        {
            _unitOfWorkAllocationService = unitOfWorkAllocationService;
        }

        private void InitializeProjectsDict()
        {
            using (var unit = _unitOfWorkAllocationService.Create())
            {
                _projectIdToProjectCodeMapping = unit.Repositories.Projects.ToDictionary(p => p.Id, p => ProjectBusinessLogic.ClientProjectName.Call(p));
            }
        }

        private void InitializeEmployeesDict()
        {
            using (var unit = _unitOfWorkAllocationService.Create())
            {
                _employeeIdToEmployeeEmailMapping = unit.Repositories.Employees.ToDictionary(e => e.Id, e => e.Email);
            }
        }

        private bool TryGetKey(Dictionary<long, string> dict, out long id, string value)
        {
            id = dict.FirstOrDefault(
                e => string.Equals(e.Value, value, StringComparison.InvariantCultureIgnoreCase)).Key;

            if (id != 0)
            {
                return true;
            }

            return false;
        }
        
        public long GetProjectIdByCode(string projectCode)
        {
            if (!_projectIdToProjectCodeMapping.Any())
            {
                InitializeProjectsDict();
            }

            long projectId;
            if (TryGetKey(_projectIdToProjectCodeMapping, out projectId, projectCode))
            {
                return projectId;
            }

            throw new ArgumentException("projectName");
        }

        public string GetProjectCodeById(long id)
        {
            if (!_projectIdToProjectCodeMapping.Any())
            {
                InitializeProjectsDict();
            }

            string projectCode;
            if (_projectIdToProjectCodeMapping.TryGetValue(id, out projectCode))
            {
                return projectCode;
            }

            throw new ArgumentException("projectId");
        }

        public long GetEmployeeIdByEmail(string email)
        {
            if (!_employeeIdToEmployeeEmailMapping.Any())
            {
                InitializeEmployeesDict();
            }

            long employeeId;
            if (TryGetKey(_employeeIdToEmployeeEmailMapping, out employeeId, email))
            {
                return employeeId;
            }

            throw new ArgumentException("employeeEmail");
        }

        public string GetEmployeeEmailById(long id)
        {
            if (!_employeeIdToEmployeeEmailMapping.Any())
            {
                InitializeEmployeesDict();
            }

            string employeeName;
            if (_employeeIdToEmployeeEmailMapping.TryGetValue(id, out employeeName))
            {
                return employeeName;
            }

            throw new ArgumentException("employeeId");
        }
    }
}
