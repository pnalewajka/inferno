﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using Castle.Core.Logging;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Allocation.BusinessEvents;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.Business.Survey.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Business.Extensions;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Data.ActiveDirectory;
using Smt.Atomic.Data.ActiveDirectory.Entities;
using Smt.Atomic.Data.Entities.Dto;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{
    internal class ActiveDirectoryUserSyncService : IActiveDirectoryUserSyncService
    {
        private readonly ISettingsProvider _settingsProvider;
        private readonly IUserService _userService;
        private readonly IOrgUnitService _orgUnitService;
        private readonly IEmployeeService _employeeService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IUnitOfWorkService<IAccountsDbScope> _unitOfWorkService;
        private readonly IUnitOfWorkService<IAllocationDbScope> _allocationUnitOfWorkService;
        private readonly IUserPermissionService _userPermissionService;
        private readonly ISecurityService _securityService;
        private readonly ILogger _logger;
        private readonly IEmploymentPeriodService _employmentPeriodService;
        private readonly IBusinessEventPublisher _businessEventPublisher;
        private readonly ITimeService _timeService;
        private readonly ISurveyRespondentService _surveyRespondentService;

        private const string DateTimeFormatInAd = "yyyy-MM-dd";

        private readonly Lazy<Dictionary<string, CompanyDto>> _allCompanies;
        private readonly Lazy<Dictionary<string, LocationDto>> _allLocations;
        private readonly Lazy<Dictionary<string, OrgUnitDto>> _allOrgUnits;
        private readonly Lazy<Dictionary<string, JobMatrixLevelDto>> _allJobMatrixLevels;
        private readonly Lazy<Dictionary<string, JobProfileDto>> _allJobProfiles;

        public ActiveDirectoryUserSyncService(
            ISettingsProvider settingsProvider,
            IUserService userService,
            ICompanyService companyService,
            IEmployeeService employeeService,
            ISystemParameterService systemParameterService,
            ILocationService locationService,
            IOrgUnitService orgUnitService,
            IUnitOfWorkService<IAccountsDbScope> unitOfWorkService,
            IUserPermissionService userPermissionService,
            ISecurityService securityService,
            ILogger logger,
            ISkillService skillService,
            IEmploymentPeriodService employmentPeriodService,
            IBusinessEventPublisher businessEventPublisher,
            ITimeService timeService,
            ISurveyRespondentService surveyRespondentService,
            IUnitOfWorkService<IAllocationDbScope> allocationUnitOfWorkService
           )
        {
            _userService = userService;
            _orgUnitService = orgUnitService;
            _employeeService = employeeService;
            _systemParameterService = systemParameterService;
            _unitOfWorkService = unitOfWorkService;
            _userPermissionService = userPermissionService;
            _securityService = securityService;
            _logger = logger;
            _employmentPeriodService = employmentPeriodService;
            _businessEventPublisher = businessEventPublisher;
            _timeService = timeService;
            _surveyRespondentService = surveyRespondentService;
            _allocationUnitOfWorkService = allocationUnitOfWorkService;
            _settingsProvider = settingsProvider;

            _allCompanies = new Lazy<Dictionary<string, CompanyDto>>(() => companyService.GetAllCompanies()
               .ToDictionary(c => c.ActiveDirectoryCode, c => c, StringComparer.InvariantCultureIgnoreCase));

            _allLocations = new Lazy<Dictionary<string, LocationDto>>(() => locationService.GetAllLocations()
                .ToDictionary(l => l.Name, l => l, StringComparer.InvariantCultureIgnoreCase));

            _allOrgUnits = new Lazy<Dictionary<string, OrgUnitDto>>(() => orgUnitService.GetAllOrgUnits()
                .ToDictionary(o => o.Code, o => o, StringComparer.InvariantCultureIgnoreCase));

            _allJobMatrixLevels = new Lazy<Dictionary<string, JobMatrixLevelDto>>(() => skillService.GetAllJobMatrixLevels()
                .ToDictionary(j => j.Code, j => j, StringComparer.InvariantCultureIgnoreCase));

            _allJobProfiles = new Lazy<Dictionary<string, JobProfileDto>>(() => skillService.GetAllJobProfiles()
                .ToDictionary(j => j.Name, j => j, StringComparer.InvariantCultureIgnoreCase));
        }

        public IEnumerable<ActiveDirectoryUserDto> GetAllEmployeesFromActiveDirectory(CancellationToken cancellationToken)
        {
            var activeDirectorySynchronizationPaths = _systemParameterService.GetParameter<string[]>(ParameterKeys.ActiveDirectoryUserSyncPaths);
            var activeDirectoryEmployeeGroup = _systemParameterService.GetParameter<string>(ParameterKeys.ActiveDirectoryEmployeeGroup);
            var defaultOrgUnitCode = _systemParameterService.GetParameter<string>(ParameterKeys.OrganizationDefaultOrgUnitCode);
            var showInactiveUserSyncWarnings = _systemParameterService.GetParameter<bool>(ParameterKeys.ShowInactiveUserSyncWarnings);

            var result = new List<ActiveDirectoryUserDto>();

            using (var context = new CoreDirectoryContext(_settingsProvider, useCustomResultMapper: true))
            {
                foreach (var user in context.Users.TakeWhileNotCancelled(cancellationToken))
                {
                    var groups = user.Groups.Select(g => g.Name).ToHashSet();
                    var isUserActive = groups.Contains(activeDirectoryEmployeeGroup) &&
                                       activeDirectorySynchronizationPaths.Any(p => user.Path.EndsWith(p));
                    var showUserSyncWarnings = isUserActive || showInactiveUserSyncWarnings;

                    var activeDirectoryUserDto = new ActiveDirectoryUserDto
                    {
                        ActiveDirectoryId = user.Id,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Email = user.Email,
                        Title = user.Title,
                        Acronym = user.Acronym,
                        Login = user.UserName,
                        CompanyId = GetCompanyId(user, showUserSyncWarnings),
                        LocationId = GetLocationId(user, showUserSyncWarnings),
                        PlaceOfWork = GetPlaceOfWork(user, showUserSyncWarnings),
                        OrgUnitId = GetOrgUnitId(user, defaultOrgUnitCode, showUserSyncWarnings),
                        Path = user.Path,
                        TreePath = user.TreePath,
                        CrmId = user.CrmId,
                        ActiveDirectoryIdLineManager = DecodeManager(user.LineManager, user.Email, showUserSyncWarnings),
                        Groups = groups,
                        JobMatrixLevelId = GetJobMatrixLevelId(user, showUserSyncWarnings),
                        JobProfileId = GetJobProfileId(user, showUserSyncWarnings),
                        StartDate = ParseToDate(user.StartDate, user, showUserSyncWarnings),
                        EndDate = ParseToDate(user.EndDate, user, showUserSyncWarnings),
                        ContractType = user.ContractType,
                        IsActive = isUserActive,
                        EffectiveDate = user.EffectiveDate
                    };

                    result.Add(activeDirectoryUserDto);
                }
            }

            return cancellationToken.IsCancellationRequested
                ? Enumerable.Empty<ActiveDirectoryUserDto>()
                : result;
        }

        private DateTime? ParseToDate(string date, User activeDirectoryUser, bool showUserSyncWarnings)
        {
            DateTime parsedDate;

            if (!string.IsNullOrEmpty(date))
            {
                if (DateTime.TryParseExact(date, DateTimeFormatInAd, CultureInfo.InvariantCulture, DateTimeStyles.None,
                    out parsedDate))
                {
                    return parsedDate;
                }

                if (showUserSyncWarnings)
                {
                    _logger.Error(
                        $"[AD data issue] Not able to map start, end date for user {activeDirectoryUser.Email}. Invalid value is '{date}'.");
                }
            }

            return null;
        }

        public void DeactivateUsersNotAvailableInActiveDirectory(IEnumerable<ActiveDirectoryUserDto> activeDirectoryUsers, IReadOnlyCollection<Guid> externalActiveDirectoryUserIds, CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                return;
            }

            var activeDirectoryIds = activeDirectoryUsers.Select(u => u.ActiveDirectoryId).ToHashSet();

            List<long> userIds;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                userIds = unitOfWork.Repositories.Users.Where(u => u.IsActive &&
                                                                    u.ActiveDirectoryId.HasValue &&
                                                                    !activeDirectoryIds.Contains(u.ActiveDirectoryId.Value) &&
                                                                    !externalActiveDirectoryUserIds.Contains(u.ActiveDirectoryId.Value))
                                                       .Select(u => u.Id)
                                                       .ToList();
            }

            foreach (var userId in userIds.TakeWhileNotCancelled(cancellationToken))
            {
                DeactivateUser(userId);
            }
        }

        public void SynchronizeUsersWithActiveDirectory(IEnumerable<ActiveDirectoryUserDto> activeDirectoryUsers, CancellationToken cancellationToken)
        {
            foreach (var activeDirectoryUserDto in activeDirectoryUsers.TakeWhileNotCancelled(cancellationToken))
            {
                try
                {
                    var userId = FindUser(activeDirectoryUserDto);

                    if (userId.HasValue)
                    {
                        _userService.SynchronizeWithActiveDirectory(activeDirectoryUserDto);
                    }
                    else
                    {
                        userId = CreateUser(activeDirectoryUserDto);
                    }

                    EnsureUserCredentialExists(activeDirectoryUserDto, userId.Value);

                    var profileIds = _userPermissionService.GetSecurityProfileIdsFromActiveDirectoryGroups(activeDirectoryUserDto.Groups);
                    _userPermissionService.AssignSecurityProfilesToUser(userId.Value, profileIds, removeOtherProfiles: true);
                }
                catch (Exception ex)
                {
                    _logger.ErrorFormat(ex, "Problem while synchronizing Active Directory user with Id={0}, FirstName={1}, LastName={2}",
                        activeDirectoryUserDto.ActiveDirectoryId,
                        activeDirectoryUserDto.FirstName,
                        activeDirectoryUserDto.LastName
                    );
                }
            }
        }

        public void SynchronizeEmployeesWithActiveDirectory(IEnumerable<ActiveDirectoryUserDto> activeDirectoryUsers, CancellationToken cancellationToken)
        {
            var activeDirectoryUserList = activeDirectoryUsers as IList<ActiveDirectoryUserDto> ?? activeDirectoryUsers.ToList();

            foreach (var activeDirectoryUserDto in activeDirectoryUserList.TakeWhileNotCancelled(cancellationToken))
            {
                try
                {
                    var userId = FindUser(activeDirectoryUserDto);
                    if (!userId.HasValue)
                    {
                        continue;
                    }

                    if (activeDirectoryUserDto.IsActive)
                    {
                        SynchronizeActiveEmployeesWithActiveDirectory(activeDirectoryUserDto, userId.Value);
                    }
                    else
                    {
                        SynchronizeNotActiveEmployeesWithActiveDirectory(activeDirectoryUserDto, userId.Value);
                    }
                }
                catch (Exception ex)
                {
                    _logger.ErrorFormat(ex, "Problem while synchronizing Active Directory user with Id={0}, FirstName={1}, LastName={2}",
                        activeDirectoryUserDto.ActiveDirectoryId,
                        activeDirectoryUserDto.FirstName,
                        activeDirectoryUserDto.LastName
                    );
                }
            }
        }

        private void SynchronizeActiveEmployeesWithActiveDirectory(ActiveDirectoryUserDto activeDirectoryUserDto, long userId)
        {
            var employeeId = FindEmployee(userId);

            if (employeeId.HasValue)
            {
                EditEmployee(employeeId.Value, activeDirectoryUserDto);
            }
            else if (activeDirectoryUserDto.IsActive)
            {
                employeeId = CreateEmployee(activeDirectoryUserDto, userId);
            }

            if (employeeId.HasValue)
            {
                var employeeIdLineManager = FindEmployeeByActiveDirectoryId(activeDirectoryUserDto.ActiveDirectoryIdLineManager);

                _employeeService.SetManager(employeeId.Value, employeeIdLineManager);
                _employeeService.UpdateCurrentEmployeeStatus(employeeId.Value);
                _employeeService.UpdateEmployeeBasedOnEmploymentPeriod(employeeId.Value);
                _surveyRespondentService.AddOrgUnitSurveysForEmployeeId(employeeId.Value);
            }
        }

        private void SynchronizeNotActiveEmployeesWithActiveDirectory(ActiveDirectoryUserDto activeDirectoryUserDto, long userId)
        {
            var employeeId = FindEmployee(userId);

            if (employeeId.HasValue)
            {
                EditEmployee(employeeId.Value, activeDirectoryUserDto);
                _employeeService.UpdateCurrentEmployeeStatus(employeeId.Value);
            }
        }

        private void DeactivateUser(long userId)
        {
            using (var transactionScope = TransactionHelper.CreateDefaultTransactionScope())
            {
                _userPermissionService.RemoveAllSecurityProfilesFromUser(userId);
                _securityService.RevokeUserPasswordCredentials(userId);
                _userService.DeactivateUserMissingInActiveDirectory(userId);

                var employeeId = FindEmployee(userId);

                if (employeeId.HasValue)
                {
                    _employeeService.DeactivateEmployeeMissingInActiveDirectory(employeeId.Value);
                }

                transactionScope.Complete();
            }
        }

        private long? FindUser(ActiveDirectoryUserDto activeDirectoryUserDto)
        {
            var existingUserDto = _userService.GetUserOrDefaultByActiveDirectoryId(activeDirectoryUserDto.ActiveDirectoryId)
                               ?? _userService.GetUserOrDefaultByCrmId(activeDirectoryUserDto.CrmId);

            return existingUserDto?.Id;
        }

        private long CreateUser(ActiveDirectoryUserDto activeDirectoryUserDto)
        {
            var userDto = new UserDto
            {
                Email = activeDirectoryUserDto.Email,
                FirstName = activeDirectoryUserDto.FirstName,
                LastName = activeDirectoryUserDto.LastName,
                Login = activeDirectoryUserDto.Login,
                IsActive = activeDirectoryUserDto.IsActive,
                ActiveDirectoryId = activeDirectoryUserDto.ActiveDirectoryId,
                CrmId = activeDirectoryUserDto.CrmId,
            };

            return _userService.CreateUser(userDto);
        }

        private void EnsureUserCredentialExists(ActiveDirectoryUserDto activeDirectoryUserDto, long userId)
        {
            if (_userService.DoesUserCredentialExist(activeDirectoryUserDto.Email, AuthorizationProviderType.ActiveDirectory))
            {
                return;
            }

            _userService.CreateUserCredential(new CredentialDto
            {
                Email = activeDirectoryUserDto.Email,
                ProviderType = AuthorizationProviderType.ActiveDirectory,
                UserId = userId
            });
        }

        private long? FindEmployee(long userId)
        {
            var employeeDto = _employeeService.GetEmployeeOrDefaultByUserId(userId);
            return employeeDto?.Id;
        }

        private void EditEmployee(long employeeId, ActiveDirectoryUserDto activeDirectoryUserDto)
        {
            activeDirectoryUserDto.IsProjectContributor =
                _orgUnitService.GetDefaultProjectContributorForOrgUnit(activeDirectoryUserDto.OrgUnitId);

            _employeeService.SynchronizeWithActiveDirectory(employeeId, activeDirectoryUserDto);

            if (activeDirectoryUserDto.StartDate.HasValue)
            {
                _employmentPeriodService.UpdateEmploymentPeriods(employeeId, activeDirectoryUserDto);
            }

            if (activeDirectoryUserDto.EndDate.HasValue)
            {
                var businessEvents = CloseEmployeeAllocationRequests(employeeId,
                    activeDirectoryUserDto.EndDate.Value).ToList();

                businessEvents.ForEach(_businessEventPublisher.PublishBusinessEvent);
            }
        }

        private IEnumerable<BusinessEvent> CloseEmployeeAllocationRequests(long employeeId, DateTime endDate)
        {
            var currentDate = _timeService.GetCurrentDate();

            if (currentDate < endDate)
            {
                // don't close requests in future
                yield break;
            }

            using (var unitOfWork = _allocationUnitOfWorkService.Create())
            {
                var allPeriodsClosed =
                    unitOfWork.Repositories.EmploymentPeriods.Where(ep => ep.EmployeeId == employeeId)
                        .All(ep => ep.EndDate.HasValue);

                if (allPeriodsClosed)
                {
                    var notclosedAllocationRequests =
                        unitOfWork.Repositories.AllocationRequests
                        .Include(nameof(AllocationRequest.AllocationRequestMetadata))
                        .Where(
                            ar => ar.EmployeeId == employeeId && !ar.EndDate.HasValue)
                        .ToList();

                    foreach (var notclosedAllocationRequest in notclosedAllocationRequests)
                    {
                        notclosedAllocationRequest.EndDate = endDate;
                        notclosedAllocationRequest.AllocationRequestMetadata.ProcessingStatus = ProcessingStatus.Processing;

                        yield return new DailyAllocationNeedsToRegenerateBusinessEvent
                        {
                            Scope = DailyAllocationNeedsToRegenerateScope.AllocationRequest,
                            AllocationRequestId = notclosedAllocationRequest.Id,
                            EmployeeId = notclosedAllocationRequest.EmployeeId,
                            PeriodFrom = notclosedAllocationRequest.StartDate
                        };
                    }

                    unitOfWork.Commit();
                }
            }
        }

        private long CreateEmployee(ActiveDirectoryUserDto activeDirectoryUserDto, long userId)
        {
            var employeeDto = new EmployeeDto()
            {
                FirstName = activeDirectoryUserDto.FirstName,
                LastName = activeDirectoryUserDto.LastName,
                Email = activeDirectoryUserDto.Email,
                UserId = userId,
                LocationId = activeDirectoryUserDto.LocationId,
                PlaceOfWork = activeDirectoryUserDto.PlaceOfWork,
                OrgUnitId = activeDirectoryUserDto.OrgUnitId,
                CurrentEmployeeStatus = EmployeeStatus.Active,
                IsProjectContributor = _orgUnitService.GetDefaultProjectContributorForOrgUnit(activeDirectoryUserDto.OrgUnitId),
                Acronym = activeDirectoryUserDto.Acronym
            };

            if (activeDirectoryUserDto.JobMatrixLevelId.HasValue)
            {
                employeeDto.JobMatrixIds = new[] { activeDirectoryUserDto.JobMatrixLevelId.Value };
            }

            if (activeDirectoryUserDto.JobProfileId.HasValue)
            {
                employeeDto.JobProfileIds = new[] { activeDirectoryUserDto.JobProfileId.Value };
            }

            var employeeId = _employeeService.CreateEmployee(employeeDto);

            if (activeDirectoryUserDto.StartDate.HasValue)
            {
                _employmentPeriodService.CreateEmploymentPeriod(employeeId, activeDirectoryUserDto);
            }

            return employeeId;
        }

        private long? GetCompanyId(User activeDirectoryUser, bool showUserSyncWarnings)
        {
            if (activeDirectoryUser.CompanyOfOrigin != null)
            {
                CompanyDto company;

                if (_allCompanies.Value.TryGetValue(activeDirectoryUser.CompanyOfOrigin, out company))
                {
                    return company.Id;
                }

                if (activeDirectoryUser.TreePath.Contains("OU=SMT"))
                {
                    return _allCompanies.Value.TryGetValue("SmtSoftware", out company) ? company.Id : (long?)null;
                }

                if (activeDirectoryUser.TreePath.Contains("OU=BLStream"))
                {
                    return _allCompanies.Value.TryGetValue("BlStream", out company) ? company.Id : (long?)null;
                }
            }

            if (showUserSyncWarnings)
            {
                _logger.Info(
                    $"[AD data issue] Not able to map CompanyOfOrgin for user {activeDirectoryUser.Email}. Invalid value is '{activeDirectoryUser.TreePath[1]}'");
            }

            return null;
        }

        private long? GetLocationId(User activeDirectoryUser, bool showUserSyncWarnings)
        {
            LocationDto locationDto = null;

            if (activeDirectoryUser.Location == null || !_allLocations.Value.TryGetValue(activeDirectoryUser.Location, out locationDto))
            {
                if (showUserSyncWarnings)
                {
                    _logger.Info(
                        $"[AD data issue] Not able to map Location for user {activeDirectoryUser.Email}. Invalid value is '{activeDirectoryUser.Location}'");
                }

                _allLocations.Value.TryGetValue("UNASSIGNED", out locationDto);
            }

            return locationDto?.Id;
        }

        private long GetOrgUnitId(User activeDirectoryUser, string defaultOrgUnitCode, bool showUserSyncWarnings)
        {
            OrgUnitDto orgUnitDto;

            var orgUnitCode = activeDirectoryUser.Department;

            if (orgUnitCode == null || !_allOrgUnits.Value.TryGetValue(orgUnitCode, out orgUnitDto))
            {
                if (showUserSyncWarnings)
                {
                    _logger.Info(
                        $"[AD data issue] Not able to map OrgUnit for user {activeDirectoryUser.Email}. Invalid value is '{orgUnitCode}'");
                }

                _allOrgUnits.Value.TryGetValue(defaultOrgUnitCode, out orgUnitDto);
            }

            if (orgUnitDto == null)
            {
                throw new ObjectNotFoundException(
                    $"Not able to find OrgUnit with code '{orgUnitCode}' or '{defaultOrgUnitCode}'");
            }

            return orgUnitDto.Id;
        }

        private Guid? DecodeManager(string manager, string userEmail, bool showUserSyncWarnings)
        {
            // Line and staffing managers in Active Directory are encoded in the following format:
            // FirstName LastName | ActiveDirectoryId
            //
            // We need to get ActiveDirectoryId to find corresponding user in the system.

            if (manager != null)
            {
                var separatorIndex = manager.IndexOf("|", StringComparison.Ordinal);

                Guid activeDirectoryId;

                if (Guid.TryParse(manager.Substring(separatorIndex + 1).Trim(), out activeDirectoryId))
                {
                    return activeDirectoryId;
                }
            }

            if (showUserSyncWarnings)
            {
                _logger.Info($"[AD data issue] Not able to map Manager for user {userEmail}. Invalid value is '{manager}'");
            }

            return null;
        }

        private long? FindEmployeeByActiveDirectoryId(Guid? activeDirectoryId)
        {
            if (activeDirectoryId.HasValue)
            {
                var userDto = _userService.GetUserOrDefaultByActiveDirectoryId(activeDirectoryId.Value);

                if (userDto == null)
                {
                    _logger.Error($"User not found for ActiveDirectoryId={activeDirectoryId.Value}");

                    return default(long?);
                }

                if (!userDto.IsActive)
                {
                    _logger.Error($"User not active for ActiveDirectoryId={activeDirectoryId.Value}, UserId={userDto.Id}");

                    return default(long?);
                }

                var employeeDto = _employeeService.GetEmployeeOrDefaultByUserId(userDto.Id);

                if (employeeDto == null)
                {
                    _logger.Error($"Employee not found for ActiveDirectoryId={activeDirectoryId.Value}, UserId={userDto.Id}");

                    return default(long?);
                }

                return employeeDto.Id;
            }

            return default(long?);
        }

        private long? GetJobMatrixLevelId(User activeDirectoryUser, bool showUserSyncWarnings)
        {
            JobMatrixLevelDto jobMatrixLevelDto;

            var jobMatrixLevelCode = activeDirectoryUser.JobMatrixLevel;

            if (jobMatrixLevelCode == null || !_allJobMatrixLevels.Value.TryGetValue(jobMatrixLevelCode, out jobMatrixLevelDto))
            {
                if (showUserSyncWarnings)
                {
                    _logger.Info(
                        $"[AD data issue] Not able to map jobMatrix for user {activeDirectoryUser.Email}. Invalid value is '{jobMatrixLevelCode}'");
                }
                return null;
            }

            return jobMatrixLevelDto.Id;
        }

        private long? GetJobProfileId(User activeDirectoryUser, bool showUserSyncWarnings)
        {
            JobProfileDto jobProfileDto;

            var jobProfileName = activeDirectoryUser.MainCompetence;

            if (jobProfileName == null || !_allJobProfiles.Value.TryGetValue(jobProfileName, out jobProfileDto))
            {
                if (showUserSyncWarnings)
                {
                    _logger.Info(
                        $"[AD data issue] Not able to map JobProfile for user {activeDirectoryUser.Email}. Invalid value is '{jobProfileName}'");
                }
                return null;
            }

            return jobProfileDto.Id;
        }

        private PlaceOfWork? GetPlaceOfWork(User activeDirectoryUser, bool showUserSyncWarnings)
        {
            var placeOfWork = activeDirectoryUser.PlaceOfWork;

            if (string.Compare(placeOfWork, PlaceOfWork.CompanyOffice.GetActiveDirectoryCode(), StringComparison.OrdinalIgnoreCase) == 0)
            {
                return PlaceOfWork.CompanyOffice;
            }
            else if (string.Compare(placeOfWork, PlaceOfWork.ClientOffice.GetActiveDirectoryCode(), StringComparison.OrdinalIgnoreCase) == 0)
            {
                return PlaceOfWork.ClientOffice;
            }
            else if (string.Compare(placeOfWork, PlaceOfWork.HomeOffice.GetActiveDirectoryCode(), StringComparison.OrdinalIgnoreCase) == 0)
            {
                return PlaceOfWork.HomeOffice;
            }

            if (showUserSyncWarnings)
            {
                _logger.Info(
                    $"[AD data issue] Not able to map PlaceOfWork for user {activeDirectoryUser.Email}. Invalid value is '{placeOfWork}'");
            }

            return null;
        }
    }
}