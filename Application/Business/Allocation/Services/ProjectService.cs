﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Castle.Core;
using Smt.Atomic.Business.Allocation.BusinessEvents;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Allocation.Resources;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Dto;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Navision.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Presentation.Common.Interfaces;

namespace Smt.Atomic.Business.Allocation.Services
{
    [Interceptor(typeof(CacheInterceptor))]
    public class ProjectService : IProjectService
    {
        private readonly IUnitOfWorkService<IAllocationDbScope> _unitOfWorkService;
        private readonly IReadOnlyUnitOfWorkService<ITimeTrackingDbScope> _timeTrackingUnitOfWorkService;
        private readonly IClassMapping<Data.Entities.Modules.Allocation.Project, ProjectDto> _projectToProjectDtoClassMapping;
        private readonly IClassMapping<ProjectTagDto, ProjectTag> _projectTagDtoToProjectTagClassMapping;
        private readonly IClassMapping<ProjectTag, ProjectTagDto> _projectTagToProjectTagDtoClassMapping;
        private readonly IClassMapping<ProjectDto, Data.Entities.Modules.Allocation.Project> _projectDtoToprojectClassMapping;
        private readonly IClassMapping<ProjectDto, ProjectSetup> _projectDtoToSetupClassMapping;
        private readonly IBusinessEventPublisher _businessEventPublisher;
        private readonly IOrgUnitService _orgUnitService;
        private readonly INavisionUnitOfWorkService _navisionUnitOfWorkService;
        private readonly IAlertService _alertService;
        private readonly ITimeService _timeService;

        public ProjectService(IUnitOfWorkService<IAllocationDbScope> unitOfWorkService,
            IReadOnlyUnitOfWorkService<ITimeTrackingDbScope> timeTrackingUnitOfWorkService,
            IClassMapping<Data.Entities.Modules.Allocation.Project, ProjectDto> projectToProjectDtoClassMapping,
            IClassMapping<ProjectDto, Data.Entities.Modules.Allocation.Project> projectDtoToprojectClassMapping,
            IBusinessEventPublisher businessEventPublisher,
            IOrgUnitService orgUnitService,
            INavisionUnitOfWorkService navisionUnitOfWorkService,
            IAlertService alertService,
            ITimeService timeService, IClassMapping<ProjectTagDto, ProjectTag> projectTagDtoToprojectTagClassMapping,
            IClassMapping<ProjectTag, ProjectTagDto> projectTagToProjectTagDtoClassMapping,
            IClassMapping<ProjectDto, ProjectSetup> projectDtoToSetupClassMapping)
        {
            _unitOfWorkService = unitOfWorkService;
            _timeTrackingUnitOfWorkService = timeTrackingUnitOfWorkService;
            _projectToProjectDtoClassMapping = projectToProjectDtoClassMapping;
            _projectDtoToprojectClassMapping = projectDtoToprojectClassMapping;
            _businessEventPublisher = businessEventPublisher;
            _orgUnitService = orgUnitService;
            _navisionUnitOfWorkService = navisionUnitOfWorkService;
            _alertService = alertService;
            _timeService = timeService;
            _projectTagDtoToProjectTagClassMapping = projectTagDtoToprojectTagClassMapping;
            _projectTagToProjectTagDtoClassMapping = projectTagToProjectTagDtoClassMapping;
            _projectDtoToSetupClassMapping = projectDtoToSetupClassMapping;
        }

        public ProjectDto CreateOrEditProject(ProjectDto projectDto)
        {
            var projectEntity = _projectDtoToprojectClassMapping.CreateFromSource(projectDto);
            var setupEntity = _projectDtoToSetupClassMapping.CreateFromSource(projectDto);

            if (projectEntity.IsMainProject)
            {
                projectEntity.ProjectSetup = setupEntity;
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                if (projectEntity.Id == 0)
                {
                    if (projectEntity.ProjectSetup != null)
                    {
                        unitOfWork.Repositories.ProjectSetups.Add(projectEntity.ProjectSetup);
                    }

                    unitOfWork.Repositories.Projects.Add(projectEntity);
                }
                else
                {
                    //do not modify time tracking project type if project already exists
                    projectEntity.TimeTrackingType = GetProjectById(projectEntity.Id).TimeTrackingType;

                    var existingSetup = unitOfWork.Repositories.ProjectSetups.GetById(setupEntity.Id);
                    var existingProject = unitOfWork.Repositories.Projects.GetById(projectEntity.Id);

                    EntityHelper.ShallowCopy(projectEntity, existingProject);
                    unitOfWork.Repositories.Projects.Edit(existingProject);

                    EntityHelper.ShallowCopy(setupEntity, existingSetup);
                    unitOfWork.Repositories.ProjectSetups.Edit(existingSetup);
                }

                unitOfWork.Commit();

                return _projectToProjectDtoClassMapping.CreateFromSource(projectEntity);
            }
        }

        public ProjectDto GetProjectOrDefaultById(long id)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var project = unitOfWork.Repositories.Projects.GetByIdOrDefault(id);

                if (project == null)
                {
                    return null;
                }

                return _projectToProjectDtoClassMapping.CreateFromSource(project);
            }
        }

        public IList<ProjectTagDto> GetProjectTagsByNames(string[] names)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork
                    .Repositories
                    .ProjectTags
                    .Where(x => names.Contains(x.Name))
                    .AsEnumerable()
                    .Select(_projectTagToProjectTagDtoClassMapping.CreateFromSource)
                    .ToList();
            }
        }

        [Cached(CacheArea = CacheAreas.HasEmployees, ExpirationInSeconds = 600)]
        public ProjectTagDto GetProjectTagByName(string name)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var tagEntity = unitOfWork
                    .Repositories
                    .ProjectTags
                    .FirstOrDefault(x => x.Name == name);

                return tagEntity == null ? null : _projectTagToProjectTagDtoClassMapping.CreateFromSource(tagEntity);
            }
        }

        public IList<ProjectTagDto> GetProjectTagsByProjectId(long projectId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork
                    .Repositories
                    .ProjectSetups
                    .Where(s => s.Projects.Any(p => p.Id == projectId))
                    .Select(s => s.Tags)
                    .FirstOrDefault()?
                    .AsEnumerable()
                    .Select(_projectTagToProjectTagDtoClassMapping.CreateFromSource)
                    .ToList();
            }
        }

        public void CreateOrEditProjectTags(List<ProjectTagDto> tagsToCreate)
        {
            var tagEntities = tagsToCreate.Select(_projectTagDtoToProjectTagClassMapping.CreateFromSource);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                foreach (var projectTag in tagEntities)
                {
                    if (projectTag.Id == 0)
                    {
                        unitOfWork.Repositories.ProjectTags.Add(projectTag);
                    }
                    else
                    {
                        unitOfWork.Repositories.ProjectTags.Edit(projectTag);
                    }
                }

                unitOfWork.Commit();
            }
        }

        public void AssignAndDetachTags(List<ProjectTagDto> tagsToAssign, List<ProjectTagDto> tagsToDetach,
            long projectSetupId)
        {

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var setup = unitOfWork.Repositories.ProjectSetups.FirstOrDefault(x => x.Id == projectSetupId);

                if (setup == null)
                {
                    return;
                }

                foreach (var projectTagDto in tagsToAssign)
                {
                    var tag = unitOfWork.Repositories.ProjectTags.FirstOrDefault(x => x.Name == projectTagDto.Name);

                    if (tag == null)
                    {
                        continue;
                    }

                    setup.Tags.Add(tag);
                }

                foreach (var projectTagDto in tagsToDetach)
                {
                    var tag = unitOfWork.Repositories.ProjectTags.FirstOrDefault(x => x.Name == projectTagDto.Name);

                    if (tag == null)
                    {
                        continue;
                    }

                    setup.Tags.Remove(tag);
                }

                unitOfWork.Commit();
            }
        }

        public ProjectDto GetProjectById(long id)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var project = unitOfWork.Repositories.Projects.GetById(id);

                return _projectToProjectDtoClassMapping.CreateFromSource(project);
            }
        }

        public IEnumerable<ProjectDto> GetProjectsByIds(IEnumerable<long> ids)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var projects = unitOfWork.Repositories.Projects.GetByIds(ids);

                return projects.Select(_projectToProjectDtoClassMapping.CreateFromSource).ToList();
            }
        }

        [Cached(CacheArea = CacheAreas.JiraProjects, ExpirationInSeconds = 3600)]
        public ProjectDto GetProjectByJiraKeyOrDefault(string jiraKey)
        {
            if (string.IsNullOrEmpty(jiraKey))
            {
                return null;
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var possibleProjects = unitOfWork.Repositories.Projects.Where(p => p.JiraKey == jiraKey).ToList();
                var project = possibleProjects.FirstOrDefault();

                if (possibleProjects.Count > 1)
                {
                    _alertService.AddWarning(ProjectResources.MultiplePossibleProjectsDetected);

                    // Try guess correct project
                    var currentDate = _timeService.GetCurrentDate();
                    var mostPossibleProject = possibleProjects.FirstOrDefault(p => p.StartDate <= currentDate && (!p.EndDate.HasValue || p.EndDate >= currentDate));

                    if (mostPossibleProject != null)
                    {
                        project = mostPossibleProject;
                    }
                }

                if (project == null)
                {
                    return null;
                }

                var projectDto = _projectToProjectDtoClassMapping.CreateFromSource(project);

                return projectDto;
            }
        }

        [Cached(CacheArea = CacheAreas.JiraProjects, ExpirationInSeconds = 3600)]
        public ProjectDto GetProjectByClientAndNameOrDefault(string project, string client)
        {
            if (string.IsNullOrEmpty(project) || string.IsNullOrEmpty(client))
            {
                return null;
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var projectEntity =
                    unitOfWork.Repositories.Projects.SingleOrDefault(
                        p =>
                            (p.SubProjectShortName == project || p.ProjectSetup.ProjectShortName == project)
                            && p.ProjectSetup.ClientShortName == client);

                if (projectEntity == null)
                {
                    return null;
                }

                var projectDto = _projectToProjectDtoClassMapping.CreateFromSource(projectEntity);

                return projectDto;
            }
        }

        public ProjectDto GetProjectByJiraIssueKeyOrDefault(string jiraIssueKey)
        {
            if (string.IsNullOrEmpty(jiraIssueKey))
            {
                return null;
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var projectEntity =
                    unitOfWork.Repositories.Projects.SingleOrDefault(
                        p =>
                            p.JiraIssueKey == jiraIssueKey);

                if (projectEntity == null)
                {
                    return null;
                }

                var projectDto = _projectToProjectDtoClassMapping.CreateFromSource(projectEntity);

                return projectDto;
            }
        }

        public IEnumerable<ProjectPresentationDto> GetProjecListByIdsForPresentationGeneration(long[] ids)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var projects = unitOfWork.Repositories.Projects.GetByIds(ids);

                foreach (var project in projects.Where(p => (p.ProjectSetup.Disclosures & ProjectDisclosures.WeCanTalkAboutClient) != 0))
                {
                    yield return new ProjectPresentationDto
                    {
                        Name = project.ProjectSetup.Disclosures.HasFlag(ProjectDisclosures.WeCanTalkAboutProject)
                            ? $"{ProjectBusinessLogic.ProjectName.Call(project)} {ProjectResources.For} {project.ProjectSetup.ClientShortName}"
                            : project.ProjectSetup.GenericName,
                        ClientShortName = project.ProjectSetup.ClientShortName,
                        ProjectShortName = ProjectBusinessLogic.ProjectName.Call(project),
                        PetsCode = project.PetsCode,
                        JiraCode = project.JiraKey,
                        JiraId = project.JiraIssueKey,
                        ProjectFeatures = project.ProjectSetup.ProjectFeatures,
                        Description = project.Description,
                        OrgUnits = project.ProjectSetup.OrgUnit == null ? string.Empty : project.ProjectSetup.OrgUnit.Name,
                        ProjectManager =
                            project.ProjectSetup.ProjectManager == null
                                ? string.Empty
                                : $"{project.ProjectSetup.ProjectManager.FirstName} {project.ProjectSetup.ProjectManager.LastName}",
                        Industries =
                            !project.ProjectSetup.Industries.Any()
                                ? string.Empty
                                : project.ProjectSetup.Industries.Select(x => x.Name)
                                    .Aggregate((x, y) => $"{x}, {y}"),
                        StartDate = project.StartDate,
                        EndDate = project.EndDate,
                        ProjectStatus = project.ProjectSetup.ProjectStatus,
                        GenericName = project.ProjectSetup.GenericName,
                        KeyTechnologies =
                            !project.ProjectSetup.KeyTechnologies.Any()
                                ? string.Empty
                                : project.ProjectSetup.KeyTechnologies.Select(x => x.Name).Aggregate((x, y) => $"{x}, {y}"),
                        Technologies =
                            !project.ProjectSetup.Technologies.Any()
                                ? string.Empty
                                : project.ProjectSetup.Technologies.Select(x => x.Name).Aggregate((x, y) => $"{x}, {y}"),
                        ServiceLines =
                            !project.ProjectSetup.ServiceLines.Any()
                                ? string.Empty
                                : project.ProjectSetup.ServiceLines.Select(x => x.Name).Aggregate((x, y) => $"{x}, {y}"),
                        SoftwareCategories =
                            !project.ProjectSetup.SoftwareCategories.Any()
                                ? string.Empty
                                : project.ProjectSetup.SoftwareCategories.Select(x => x.Name).Aggregate((x, y) => $"{x}, {y}"),
                        Tags =
                            !project.ProjectSetup.Tags.Any()
                                ? string.Empty
                                : project.ProjectSetup.Tags.Select(x => x.Name).Aggregate((x, y) => $"{x}, {y}"),
                        CurrentPhase = project.ProjectSetup.CurrentPhase,
                        Disclosures = project.ProjectSetup.Disclosures,
                        BusinessCase = project.ProjectSetup.BusinessCase,
                        Picture = project.ProjectSetup.Picture?.DocumentContent.Content,
                        SoftwareCategoryIds =
                            !project.ProjectSetup.SoftwareCategories.Any()
                                ? new long[0]
                                : project.ProjectSetup.SoftwareCategories.Select(x => x.Id).ToArray(),
                        OrgUnitId = project.ProjectSetup.OrgUnitId,
                        ProjectManagerId = project.ProjectSetup.ProjectManagerId,
                        KeyTechnologyIds =
                            !project.ProjectSetup.KeyTechnologies.Any()
                                ? new long[0]
                                : project.ProjectSetup.KeyTechnologies.Select(x => x.Id).ToArray(),
                        TechnologyIds =
                            project.ProjectSetup.Technologies.Any()
                                ? new long[0]
                                : project.ProjectSetup.Technologies.Select(x => x.Id).ToArray(),
                        IndustryIds =
                            !project.ProjectSetup.KeyTechnologies.Any()
                                ? new long[0]
                                : project.ProjectSetup.KeyTechnologies.Select(x => x.Id).ToArray(),
                        HasClientReferences = project.ProjectSetup.HasClientReferences,
                        IsAvailableForSalesPresentation = project.ProjectSetup.IsAvailableForSalesPresentation,
                        TagIds = !project.ProjectSetup.Tags.Any()
                            ? new long[0]
                            : project.ProjectSetup.Tags.Select(x => x.Id).ToArray(),
                    };
                }
            }
        }

        public void MergeProjects(IList<long> projectsIds, long targetProjectId)
        {
            using (var unitOfWork = _unitOfWorkService.CreateExtended())
            {
                var isTargetProject =
                    unitOfWork.Repositories.Projects.Any(p => p.Id == targetProjectId && !string.IsNullOrEmpty(p.JiraIssueKey));
                var areSourceProject = unitOfWork.Repositories.Projects.Where(p => projectsIds.Contains(p.Id)).All(p =>
                        string.IsNullOrEmpty(p.JiraIssueKey));

                if (!isTargetProject || !areSourceProject)
                {
                    throw new Exception($"Passed parameters are incorect. TargetProjectId = '{targetProjectId}', SourceProjectsIds = '{string.Join(",", projectsIds)}'");
                }

                var sourceAllocationRequest =
                    unitOfWork.Repositories.AllocationRequests.Where(a => projectsIds.Contains(a.ProjectId)).ToList();

                foreach (var allocationRequest in sourceAllocationRequest)
                {
                    allocationRequest.ProjectId = targetProjectId;
                    allocationRequest.AllocationRequestMetadata.ProcessingStatus = ProcessingStatus.Processing;

                    unitOfWork.Repositories.AllocationRequests.Edit(allocationRequest);
                    unitOfWork.Commit();

                    _businessEventPublisher.PublishBusinessEvent(AddAllcationRequestEvent(allocationRequest));
                }

                const string deleteSqlCommand = @"EXEC [Allocation].[CleanAllocationDailyByProject] @ProjectIds";

                DataTable projectsIdsDataTable = ToDataTable(projectsIds);

                var projectIdsSqlParam = new SqlParameter("@ProjectIds", projectsIdsDataTable)
                {
                    TypeName = "[dbo].[CollectionItems]",
                    SqlDbType = SqlDbType.Structured
                };

                unitOfWork.ExecuteSqlCommand(deleteSqlCommand, projectIdsSqlParam);
            }
        }

        public IReadOnlyCollection<ProjectDto> GetActiveProjectsForManagerEmployeeId(long managerId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var currentDate = _timeService.GetCurrentDate();
                var projects = unitOfWork.Repositories.Projects
                    .Where(ProjectBusinessLogic.IsActiveOnDate.Parametrize(currentDate))
                    .Where(p => p.ProjectSetup.ProjectManagerId.HasValue && p.ProjectSetup.ProjectManagerId == managerId
                        && p.ProjectSetup.CurrentPhase != ProjectPhase.Finished)
                    .AsEnumerable();

                return projects.Select(p => _projectToProjectDtoClassMapping.CreateFromSource(p)).ToList();
            }
        }

        private static BusinessEvent AddAllcationRequestEvent(AllocationRequest allocationRequest)
        {
            return new DailyAllocationNeedsToRegenerateBusinessEvent
            {
                Scope = DailyAllocationNeedsToRegenerateScope.AllocationRequest,
                AllocationRequestId = allocationRequest.Id,
                EmployeeId = allocationRequest.EmployeeId,
                PeriodFrom = allocationRequest.StartDate
            };
        }

        private DataTable ToDataTable<T>(IList<T> data)
        {
            DataTable table = new DataTable();
            table.Columns.Add("ID", typeof(T));

            foreach (T item in data)
            {
                table.Rows.Add(item);
            }

            return table;
        }

        [Cached(CacheArea = CacheAreas.HasProjects, ExpirationInSeconds = 600)]
        public bool HasProjects(long employeeId)
        {
            var managerOrgUnitsIds = _orgUnitService.GetManagerOrgUnitDescendantIds(employeeId);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Projects.Any(a =>
                    (a.ProjectSetup.ProjectManagerId.HasValue && a.ProjectSetup.ProjectManagerId == employeeId)
                    || (a.ProjectSetup.OrgUnit != null && managerOrgUnitsIds.Any(id => id == a.ProjectSetup.OrgUnitId)));
            }
        }

        public long GetDefaultJiraIssueToTimeReportRowMapperId()
        {
            using (var unitOfWork = _timeTrackingUnitOfWorkService.Create())
            {
                var mapperId = unitOfWork.Repositories.JiraIssueToTimeReportRowMappers.Where(m => m.Name == "Default").Select(m => (long?)m.Id).SingleOrDefault();

                if (!mapperId.HasValue)
                {
                    throw new Exception("Unable to find default JiraIssueToTimeReportRow Mapper (where Name == Default)");
                }

                return mapperId.Value;
            }
        }

        public ICollection<long> GetProjectIdsByUserId(long userId, DateTime date)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var currentEmployee =
                    unitOfWork.Repositories.Employees.FirstOrDefault(e => e.UserId == userId);

                var projectIds = currentEmployee?.AllocationRequests
                    .Where(ar => ar.StartDate <= date && (!ar.EndDate.HasValue || ar.EndDate >= date))
                    .Select(ar => ar.ProjectId)
                    .ToArray();

                return projectIds;
            }
        }

        public bool IsEmployeeOwnProject(long employeeId, long projectId)
        {
            var managerOrgUnitsIds = _orgUnitService.GetManagerOrgUnitDescendantIds(employeeId);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Projects.Where(a => a.Id == projectId).Any(
                    ProjectBusinessLogic.IsOwnProjectForEmployee.Parametrize(employeeId, managerOrgUnitsIds));
            }
        }

        public IReadOnlyCollection<string> GetAllProjectAcronyms()
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Projects.Where(p => string.IsNullOrEmpty(p.Acronym)).Select(p => p.Acronym).ToList();
            }
        }

        [Cached(CacheArea = CacheAreas.HasProjects, ExpirationInSeconds = 600)]
        public ProjectDto GetProjectByApn(string apn)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var project = unitOfWork.Repositories.Projects
                    .Where(p => p.Apn == apn)
                    .Single();

                return _projectToProjectDtoClassMapping.CreateFromSource(project);
            }
        }

        [Cached(CacheArea = CacheAreas.HasProjects, ExpirationInSeconds = 600)]
        public long GetSetupIdByProjectId(long projectId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Projects
                    .Where(p => p.Id == projectId)
                    .Select(p => p.ProjectSetupId)
                    .Single();
            }
        }
    }
}
