﻿using System;
using Smt.Atomic.Business.Allocation.BusinessEvents;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Allocation.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Dto;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using static Smt.Atomic.Business.Allocation.Dto.EmployeeAllocationDto;

namespace Smt.Atomic.Business.Allocation.Services
{
    internal class EmployeeAllocationService : IEmployeeAllocationService
    {
        private readonly IUnitOfWorkService<IAllocationDbScope> _unitOfWorkService;
        private readonly ITimeService _timeService;
        private readonly IClassMappingFactory _classMappingFactory;
        private readonly IBusinessEventPublisher _businessEventPublisher;

        public EmployeeAllocationService(IUnitOfWorkService<IAllocationDbScope> unitOfWorkService, ITimeService timeService, IBusinessEventPublisher businessEventPublisher, IClassMappingFactory classMappingFactory)
        {
            _unitOfWorkService = unitOfWorkService;
            _timeService = timeService;
            _classMappingFactory = classMappingFactory;
            _businessEventPublisher = businessEventPublisher;
        }

        public AllocationBlockDto Edit(long allocationRequestId, IEditAllocationContract contract)
        {
            var classMapping = _classMappingFactory.CreateMapping<AllocationRequest, AllocationBlockDto>();

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var allocationRequest = GetAllocationRequest(unitOfWork, allocationRequestId);

                var editEvent = GetEditEvent(allocationRequest, contract);

                allocationRequest = UpdateRequestDates(allocationRequest, contract);
                allocationRequest = UpdateRequestHours(allocationRequest, contract);
                allocationRequest = UpdateRequestCertainty(allocationRequest, contract.Certainty);

                _businessEventPublisher.PublishBusinessEvent(editEvent);

                unitOfWork.Commit();

                return classMapping.CreateFromSource(allocationRequest);
            }
        }

        private BusinessEvent GetEditEvent(AllocationRequest request, IDateRange contract)
        {
            var editEvent = new DailyAllocationNeedsToRegenerateBusinessEvent
            {
                Scope = DailyAllocationNeedsToRegenerateScope.AllocationRequest,
                AllocationRequestId = request.Id,
                EmployeeId = request.EmployeeId,
                PeriodFrom = new DateTime(
                                Math.Min(
                                    request.StartDate.Ticks,
                                    contract.StartDate.Ticks)
                                    )
            };

            return editEvent;
        }

        private BusinessEvent GetAddEvent(AllocationRequest request)
        {
            var addEvent = new DailyAllocationNeedsToRegenerateBusinessEvent
            {
                Scope = DailyAllocationNeedsToRegenerateScope.AllocationRequest,
                AllocationRequestId = request.Id,
                EmployeeId = request.EmployeeId,
                PeriodFrom = request.StartDate
            };

            return addEvent;
        }

        private BusinessEvent GetDeleteEvent(AllocationRequest request)
        {
            var deleteEvent = new DailyAllocationNeedsToRegenerateBusinessEvent
            {
                Scope = DailyAllocationNeedsToRegenerateScope.AllocationRequest,
                EmployeeId = request.EmployeeId,
                PeriodFrom = request.StartDate,
            };

            return deleteEvent;
        }

        private AllocationRequest GetAllocationRequest(IUnitOfWork<IAllocationDbScope> unitOfWork, long id)
        {
            return unitOfWork
                .Repositories
                .AllocationRequests
                .Filtered()
                .GetById(id);
        }

        private bool CanChangeStartDate(IDateRange model)
        {
            return model.StartDate.Date >= _timeService.GetCurrentDate().AddDays(-30).Date;
        }

        private bool TryingToChangeStartDate(IDateRange model, IDateRange contract)
        {
            return model.StartDate.Date != contract.StartDate.Date;
        }

        private bool TryingToChangeEndDate(IDateRange model, IDateRange contract)
        {
            return model.EndDate != contract.EndDate;
        }

        private bool CanChangeEndDate(IDateRange model)
        {
            return !model.EndDate.HasValue || model.EndDate.Value.Date >= _timeService.GetCurrentDate().AddDays(-30).Date;
        }

        private AllocationRequest UpdateRequestDates(AllocationRequest allocationRequest, IDateRange contract)
        {
            if (TryingToChangeStartDate(allocationRequest, contract))
            {
                if (CanChangeStartDate(allocationRequest) && CanChangeStartDate(contract))
                {
                    allocationRequest = UpdateRequestStartDate(allocationRequest, contract.StartDate);
                }
                else
                {
                    throw new BusinessException(string.Format(AllocationRequestCardIndexDataServiceResources.CantChaneAllocationStartDateFormat, allocationRequest.StartDate, contract.StartDate));
                }
            }

            if (TryingToChangeEndDate(allocationRequest, contract))
            {
                if (CanChangeEndDate(allocationRequest) && CanChangeEndDate(contract))
                {
                    allocationRequest = UpdateRequestEndDate(allocationRequest, contract.EndDate);
                }
                else
                {
                    throw new BusinessException(string.Format(AllocationRequestCardIndexDataServiceResources.CantChangeAllocationEndDateFormat, allocationRequest.EndDate, contract.EndDate));
                }
            }

            return allocationRequest;
        }

        private AllocationRequest UpdateRequestEndDate(AllocationRequest request, DateTime? endDate)
        {
            request.EndDate = endDate.HasValue ? (DateTime?)endDate.Value.Date : null;

            return request;
        }

        private AllocationRequest UpdateRequestStartDate(AllocationRequest request, DateTime startDate)
        {
            request.StartDate = startDate;

            return request;
        }

        private AllocationRequest UpdateRequestHours(AllocationRequest allocationRequest, IWeeklyHours contract)
        {
            if (CanChangeStartDate(allocationRequest) && CanChangeEndDate(allocationRequest))
            {
                allocationRequest.MondayHours = contract.MondayHours;
                allocationRequest.TuesdayHours = contract.TuesdayHours;
                allocationRequest.WednesdayHours = contract.WednesdayHours;
                allocationRequest.ThursdayHours = contract.ThursdayHours;
                allocationRequest.FridayHours = contract.FridayHours;
                allocationRequest.SaturdayHours = contract.SaturdayHours;
                allocationRequest.SundayHours = contract.SundayHours;
            }

            return allocationRequest;
        }

        private AllocationRequest UpdateRequestCertainty(AllocationRequest allocationRequest, AllocationCertainty certainty)
        {
            if (CanChangeStartDate(allocationRequest) && CanChangeEndDate(allocationRequest))
            {
                allocationRequest.AllocationCertainty = certainty;
            }

            return allocationRequest;
        }
    }
}
