﻿using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{
    internal class MyTeamAllocationCardIndexDataService : EmployeeAllocationBaseCardIndexDataService, IMyTeamAllocationCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public MyTeamAllocationCardIndexDataService(
            ICardIndexServiceDependencies<IAllocationDbScope> dependencies,
            IOrgUnitService orgUnitService,
            ITimeService timeService,
            WeeklyAllocationDataService weeklyAllocationService,
            DailyAllocationDataService dailyAllocationDataService,
            RequestAllocationDataService requestAllocationDataService,
            ICalendarService calendarService,
            IEmployeeCardIndexDataService employeeService,
            IAllocationRequestCardIndexDataService allocationRequestCardIndexDataService,
            IClassMappingFactory classMappingFactory,
            IPrincipalProvider principalProvider)
            : base(dependencies, orgUnitService, timeService, weeklyAllocationService, dailyAllocationDataService, requestAllocationDataService, calendarService, employeeService, allocationRequestCardIndexDataService, classMappingFactory, principalProvider)
        {
        }
    }
}
