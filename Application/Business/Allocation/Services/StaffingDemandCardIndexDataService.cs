﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{
    public class StaffingDemandCardIndexDataService : CardIndexDataService<StaffingDemandDto, StaffingDemand, IAllocationDbScope>, IStaffingDemandCardIndexDataService
    {
        private readonly IStaffingDemandService _staffingDemandService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public StaffingDemandCardIndexDataService(ICardIndexServiceDependencies<IAllocationDbScope> dependencies, IStaffingDemandService staffingDemandService)
            : base(dependencies)
        {
            _staffingDemandService = staffingDemandService;
        }

        protected override IEnumerable<Expression<Func<StaffingDemand, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return a => a.Description;
        }

        public override IQueryable<StaffingDemand> ApplyContextFiltering(IQueryable<StaffingDemand> records, object context)
        {
            if (context != null)
            {
                var parentIdContext = (ParentIdContext)context;
                return records.Where(p => p.ProjectId == parentIdContext.ParentId);
            }
            return base.ApplyContextFiltering(records, context);
        }

        protected override NamedFilters<StaffingDemand> NamedFilters
        {
            get
            {
                return new NamedFilters<StaffingDemand>(new[]
                {
                    new NamedFilter<StaffingDemand, DateTime, DateTime>(
                    FilterCodes.DateRange,
                        (a, from, to) => a.StartDate >= from && a.EndDate <= to
                    ),
                });
            }
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<StaffingDemand, StaffingDemandDto, IAllocationDbScope> eventArgs)
        {
            eventArgs.InputEntity.Skills = eventArgs.UnitOfWork.Repositories.Skills.GetByIds(eventArgs.InputDto.SkillsIds).ToArray();
        }
        protected override void OnRecordEditing(RecordEditingEventArgs<StaffingDemand, StaffingDemandDto, IAllocationDbScope> eventArgs)
        {
            var validateResult = _staffingDemandService.ValidateStaffingDemandEditing(eventArgs.InputDto.Id);

            if (validateResult.Type != AlertType.Error)
            {
                eventArgs.InputEntity.Skills = eventArgs.UnitOfWork.Repositories.Skills.GetByIds(eventArgs.InputDto.SkillsIds).ToArray();
                EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.Skills, eventArgs.InputEntity.Skills);
                base.OnRecordEditing(eventArgs);
            }
            else
            {
                eventArgs.Canceled = true;
                eventArgs.Alerts.Add(validateResult);
            }
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<StaffingDemand, IAllocationDbScope> eventArgs)
        {
            var validateResult = _staffingDemandService.ValidateStaffingDemandDeleting(eventArgs.DeletingRecord.Id);

            if (validateResult.Type != AlertType.Error)
            {
                base.OnRecordDeleting(eventArgs);
            }
            else
            {
                eventArgs.Canceled = true;
                eventArgs.Alerts.Add(validateResult);
            }
        }

        public override void SetContext(StaffingDemand entity, object context)
        {
            if (context == null) return;
            var parentIdContext = (ParentIdContext)context;
            Debug.Assert(parentIdContext != null, "parentIdContext != null");
            entity.ProjectId = parentIdContext.ParentId;
        }
    }
}
