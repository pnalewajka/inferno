﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Helpers;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Allocation.Resources;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Services
{
    public class BenchEmployeesService : IBenchEmployeesService
    {
        private readonly IUnitOfWorkService<IAllocationDbScope> _allocationDbScope;
        private readonly ISystemParameterService _systemParameterService;
        private readonly ITimeService _timeService;
        private readonly IDateRangeService _dateRangeService;
        private readonly IEmployeeAvailabilityDataService _employeeAvailabilityDataService;
        private readonly IEmployeeService _employeeService;
        private readonly IEmploymentPeriodService _employmentPeriodService;

        public BenchEmployeesService(
            IUnitOfWorkService<IAllocationDbScope> allocationDbScope,
            ISystemParameterService systemParameterService,
            IDateRangeService dateRangeService,
            ITimeService timeService,
            IEmployeeAvailabilityDataService employeeAvailabilityDataService,
            IEmployeeService employeeService,
            IEmploymentPeriodService employmentPeriodService)
        {
            _timeService = timeService;
            _allocationDbScope = allocationDbScope;
            _systemParameterService = systemParameterService;
            _dateRangeService = dateRangeService;
            _employeeAvailabilityDataService = employeeAvailabilityDataService;
            _employeeService = employeeService;
            _employmentPeriodService = employmentPeriodService;
        }

        public void RewriteBenchFields(CardIndexRecords<BenchEmployeeDto> result, DateTime week)
        {
            var weekStart = week.GetPreviousDayOfWeek(DayOfWeek.Monday).StartOfDay();
            var weekEnd = weekStart.GetNextDayOfWeek(DayOfWeek.Sunday).EndOfDay();

            var consideredFuture = _systemParameterService.GetParameter<int>(ParameterKeys.NumberOfFutureMonths);
            var consideredFutureTime = weekStart.AddMonths(consideredFuture);

            using (var unitOfWork = _allocationDbScope.Create())
            {
                var employeeIds = result.Rows.Select(r => r.Id).ToList();

                var weeklyAllocationStatuses = unitOfWork.Repositories.WeeklyAllocationStatus
                    .Where(s => employeeIds.Contains(s.EmployeeId) && s.Week == weekStart)
                    .ToDictionary(w => w.EmployeeId, a => a);

                var dailyAllocationStatuses = unitOfWork.Repositories.AllocationDailyRecords
                    .Where(d => employeeIds.Contains(d.EmployeeId)
                        && d.Day >= weekStart && d.Day <= weekEnd)
                    .GroupBy(d => d.EmployeeId)
                    .ToDictionary(w => w.Key, a => a);

                var employeesComments = unitOfWork.Repositories.EmployeeComments.Where(
                    e => employeeIds.Contains(e.EmployeeId)
                    && e.EmployeeCommentType == EmployeeCommentType.StaffingComment).ToList();

                var allocationRequestDictionary = unitOfWork.Repositories.AllocationRequests
                    .Where(a => employeeIds.Contains(a.EmployeeId) && a.StartDate < consideredFutureTime
                                && (!a.EndDate.HasValue || a.EndDate.Value >= weekStart))
                    .OrderByDescending(a => a.AllocationCertainty)
                    .GroupBy(g => g.EmployeeId)
                    .ToDictionary(g => g.Key, g => g.ToList());

                var employmentPeriodDictionary = unitOfWork.Repositories.EmploymentPeriods
                    .Where(e => employeeIds.Contains(e.EmployeeId) && e.StartDate < consideredFutureTime
                        && (!e.EndDate.HasValue || e.EndDate.Value >= weekStart))
                    .GroupBy(g => g.EmployeeId)
                    .ToDictionary(g => g.Key, g => g.ToList());

                var absencesInPeriod = _employeeAvailabilityDataService
                    .GetDetailedAbsencesForEmployees(employeeIds, weekStart, weekEnd)
                    .GroupBy(g => g.AbsenceRequest.AffectedUserId)
                    .ToDictionary(g => _employeeService.GetEmployeeIdByUserId(g.Key).Value, g => g.ToList());

                foreach (var benchEmployeeDto in result.Rows)
                {
                    WeeklyAllocationStatus weeklyAllocationStatus;
                    var currentDate = _timeService.GetCurrentDate();
                    var isweeklyAllocationStatus = weeklyAllocationStatuses.TryGetValue(benchEmployeeDto.Id, out weeklyAllocationStatus);

                    if (!isweeklyAllocationStatus)
                    {
                        continue;
                    }

                    benchEmployeeDto.StaffingStatus =
                        employmentPeriodDictionary.ContainsKey(benchEmployeeDto.Id) && allocationRequestDictionary.ContainsKey(benchEmployeeDto.Id)
                        ? GetStaffingList(allocationRequestDictionary[benchEmployeeDto.Id], employmentPeriodDictionary[benchEmployeeDto.Id])
                        : new List<StaffingStatusDto>
                        {
                            new StaffingStatusDto { AllocationText = StaffingStatus.Free.GetDescription() }
                        };

                    benchEmployeeDto.AvailableHours = weeklyAllocationStatus.AvailableHours;
                    benchEmployeeDto.HoursAllocated = weeklyAllocationStatus.AllocatedHours;
                    benchEmployeeDto.CurrentDate = currentDate;

                    if (weeklyAllocationStatus.OnBenchSince != null)
                    {
                        const int daysInWeek = 7;

                        benchEmployeeDto.WeeksOnBench =
                            (int)(currentDate.Subtract(weeklyAllocationStatus.OnBenchSince.Value).TotalDays / daysInWeek);
                    }

                    benchEmployeeDto.OnBenchSince = weeklyAllocationStatus.OnBenchSince;

                    var employeeComments = employeesComments.FirstOrDefault(
                            e => e.EmployeeId == benchEmployeeDto.Id
                            && (!e.RelevantOn.HasValue || weeklyAllocationStatus.OnBenchSince == e.RelevantOn.Value));

                    if (!string.IsNullOrEmpty(employeeComments?.Comment))
                    {
                        benchEmployeeDto.StaffingComment = employeeComments.Comment;
                    }
                    else
                    {
                        if (weeklyAllocationStatus.EmployeeStatus == EmployeeStatus.Leave)
                        {
                            benchEmployeeDto.StaffingComment = EmployeeStatus.Leave.GetDescription();
                        }
                        else
                        {
                            if (weeklyAllocationStatus.OnBenchUntil.HasValue)
                            {
                                var numberOfWeeks = (int)Math.Ceiling((weeklyAllocationStatus.OnBenchUntil.Value - weekStart).TotalDays / 7);
                                benchEmployeeDto.StaffingComment = string.Format(BenchEmployeeResources.FreeWeeks, numberOfWeeks);
                            }
                        }
                    }

                    AddAbsenceInfo(
                        benchEmployeeDto,
                        weekStart,
                        weekEnd,
                        dailyAllocationStatuses.ContainsKey(benchEmployeeDto.Id)
                            ? dailyAllocationStatuses[benchEmployeeDto.Id]
                            : Enumerable.Empty<AllocationDailyRecord>(),
                        absencesInPeriod.ContainsKey(benchEmployeeDto.Id)
                            ? absencesInPeriod[benchEmployeeDto.Id]
                            : Enumerable.Empty<DetailedAbsenceDto>());
                }
            }
        }

        private void AddAbsenceInfo(
            BenchEmployeeDto benchEmployee,
            DateTime weekStart,
            DateTime weekEnd,
            IEnumerable<AllocationDailyRecord> dailyAllocationStatuses,
            IEnumerable<DetailedAbsenceDto> absences)
        {
            var contractedHoursList = new Lazy<IEnumerable<DailyContractedHoursDto>>(
                () => _employmentPeriodService.GetContractedHours(benchEmployee.Id, weekStart, weekEnd));

            foreach (var absence in absences)
            {
                var absenceRequest = absence.AbsenceRequest;

                benchEmployee.StaffingComment = string.Join(Environment.NewLine,
                    new[] {
                        benchEmployee.StaffingComment,
                        string.Format(
                            BenchEmployeeResources.AbsenceCommentFormat,
                            absenceRequest.From.ToShortDateString(),
                            absenceRequest.To.ToShortDateString()) }
                    .Where(s => !string.IsNullOrEmpty(s)));

                // Remove absence hours from AvailableHours
                foreach (var dayAbsence in absence.DailyHours.Where(DateRangeHelper.IsDateInRange<DailyContractedHoursDto>(weekStart, weekEnd, a => a.Date).GetFunction()))
                {
                    var dayAllocations = dailyAllocationStatuses.Where(d => d.Day == dayAbsence.Date);

                    if (!dayAllocations.Any())
                    {
                        benchEmployee.AvailableHours -= absence.GetHoursForDay(dayAbsence.Date);
                        continue;
                    }

                    var dayAllocationHours = dayAllocations.Sum(d => d.Hours);

                    if (!absence.IsHourlyReporting)
                    {
                        var contractedHours = absence.GetHoursForDay(dayAbsence.Date);

                        if (dayAllocationHours < contractedHours)
                        {
                            benchEmployee.AvailableHours -= contractedHours - dayAllocationHours; // Fill the hour difference
                        }
                    }
                    else
                    {
                        var contractedHours = contractedHoursList.Value.SingleOrDefault(d => d.Date == dayAbsence.Date)?.Hours;

                        if (contractedHours != null)
                        {
                            if (dayAllocationHours + dayAbsence.Hours < contractedHours.Value)
                            {
                                benchEmployee.AvailableHours -= dayAbsence.Hours;
                            }
                            else
                            {
                                benchEmployee.AvailableHours -= contractedHours.Value - dayAllocationHours;
                            }
                        }
                    }
                }
            }
        }

        private List<StaffingStatusDto> GetStaffingList(IReadOnlyCollection<AllocationRequest> allocationRequests, IReadOnlyCollection<EmploymentPeriod> employmentPeriods)
        {
            var stuffingStatusList = new List<StaffingStatusDto>();

            foreach (var allocationRequest in allocationRequests)
            {
                var allocationUsage = RequestAllocationPercentage(employmentPeriods, allocationRequest);

                var allocationText = allocationRequest.EndDate.HasValue
                    ? BenchEmployeeResources.ConfirmedFromTextWithEnd
                    : BenchEmployeeResources.ConfirmedFromText;

                if (allocationRequest.AllocationCertainty == AllocationCertainty.Planned)
                {
                    allocationText = allocationRequest.EndDate.HasValue
                        ? BenchEmployeeResources.PlannedFromTextWithEnd
                        : BenchEmployeeResources.PlannedFromText;
                }

                var staffingStatusDto = new StaffingStatusDto
                {
                    AllocationText = allocationText,
                    ProjectName = ProjectBusinessLogic.ClientProjectName.Call(allocationRequest.Project),
                    StartDate = allocationRequest.StartDate,
                    EndDate = allocationRequest.EndDate,
                    AllocationUsage = allocationUsage
                };

                stuffingStatusList.Add(staffingStatusDto);
            }

            return stuffingStatusList;
        }

        private decimal RequestAllocationPercentage(IReadOnlyCollection<EmploymentPeriod> employmentPeriods, AllocationRequest allocationRequest)
        {
            if (allocationRequest.EndDate.HasValue)
            {
                var requestDays = _dateRangeService.GetDaysInRange(allocationRequest.StartDate,
                    allocationRequest.EndDate.Value).ToList();

                var requestedHours =
                    requestDays.Sum(d => WeeklyHoursHelper.GetHoursByDayOrDefault(allocationRequest, d.DayOfWeek));
                var periodHours = requestDays.Sum(d =>
                {
                    var period =
                        employmentPeriods.SingleOrDefault(
                            ep => ep.StartDate <= d && (!ep.EndDate.HasValue || d <= ep.EndDate.Value));

                    return period != null && period.IsActive
                        ? WeeklyHoursHelper.GetHoursByDayOrDefault(period, d.DayOfWeek)
                        : 0;
                });

                if (periodHours == 0)
                {
                    return 0;
                }

                return requestedHours / periodHours * 100;
            }
            else
            {
                var requestedHours = allocationRequest.MondayHours + allocationRequest.TuesdayHours
                                     + allocationRequest.WednesdayHours + allocationRequest.ThursdayHours
                                     + allocationRequest.FridayHours + allocationRequest.SaturdayHours
                                     + allocationRequest.SundayHours;

                var period =
                    employmentPeriods.SingleOrDefault(
                        ep =>
                            ep.StartDate <= allocationRequest.StartDate &&
                            (!ep.EndDate.HasValue || allocationRequest.StartDate <= ep.EndDate.Value));

                if (period == null)
                {
                    return 0;
                }

                var periodHours = period.IsActive
                    ? (period.MondayHours + period.TuesdayHours
                        + period.WednesdayHours + period.ThursdayHours
                        + period.FridayHours + period.SaturdayHours
                        + period.SundayHours)
                    : 0;

                return periodHours != 0
                    ? requestedHours / periodHours * 100
                    : 0;
            }
        }
    }
}
