pivot.dataConverter = function (data) {
    return data.BenchAllocationReportModels.map(function (a) {
        const date = new Date(a.Week);
        const month = ("0" + (date.getUTCMonth() + 1)).slice(-2);

        return {
            "Acronym": a.Acronym,
            "Year": date.getFullYear(),
            "Month": month,
            "YearMonth": date.getFullYear() + "-" + month,
            "WeekOfTheYear": a.WeekOfTheYear,
            "Employee Org Unit": a.EmployeeOrgUnitName,
            "Sub Org Unit": a.SubOrgUnitName,
            "Org Unit": a.RootOrgUnitName,
            "Sub Org Unit Flat": a.SubOrgUnitFlatName,
            "Org Unit Flat": a.RootOrgUnitFlatName,
            "Seniority": a.Seniority,
            "JobProfile": a.JobProfile,
            "Location": a.Location,
            "PlaceOfWork": a.PlaceOfWork,
            "IsOnBench": 1,
            "Fte": a.Fte
        };
    });
};

pivot.configurations = [
    {
        name: "Org Unit vs Employee (Count)",
        code: "orgunit-employee-count",
        config: {
            rows: ["Org Unit", "Sub Org Unit"],
            cols: ["YearMonth", "WeekOfTheYear"],
            vals: ["IsOnBench"],
            aggregatorName: "Integer Sum",
            rendererName: "Heatmap"
        },
    },
    {
        name: "Org Unit vs Employee (Fte)",
        code: "orgunit-fte-count",
        config: {
            rows: ["Org Unit", "Sub Org Unit"],
            cols: ["YearMonth", "WeekOfTheYear"],
            vals: ["Fte"],
            aggregatorName: "Sum",
            rendererName: "Heatmap"
        },
    },
    {
        name: "Location vs Employee (Count)",
        code: "location-employee-count",
        config: {
            rows: ["Location"],
            cols: ["YearMonth", "WeekOfTheYear"],
            vals: ["IsOnBench"],
            aggregatorName: "Integer Sum",
            rendererName: "Heatmap"
        },
    },
    {
        name: "Location vs Employee (Fte)",
        code: "location-fte-count",
        config: {
            rows: ["Location"],
            cols: ["YearMonth", "WeekOfTheYear"],
            vals: ["Fte"],
            aggregatorName: "Sum",
            rendererName: "Heatmap"
        },
    },
    {
        name: "JobProfile vs Employee (Count)",
        code: "job-profile-employee-count",
        config: {
            rows: ["JobProfile"],
            cols: ["YearMonth", "WeekOfTheYear"],
            vals: ["IsOnBench"],
            aggregatorName: "Integer Sum",
            rendererName: "Heatmap"
        },
    },
    {
        name: "JobProfile vs Employee (Fte)",
        code: "job-profile-fte-count",
        config: {
            rows: ["JobProfile"],
            cols: ["YearMonth", "WeekOfTheYear"],
            vals: ["Fte"],
            aggregatorName: "Sum",
            rendererName: "Heatmap"
        },
    },
    {
        name: "Seniority vs Employee (Count)",
        code: "seniority-employee-count",
        config: {
            rows: ["Seniority"],
            cols: ["YearMonth", "WeekOfTheYear"],
            vals: ["IsOnBench"],
            aggregatorName: "Integer Sum",
            rendererName: "Heatmap"
        },
    },
    {
        name: "Seniority vs Employee (Fte)",
        code: "seniority-fte-count",
        config: {
            rows: ["Seniority"],
            cols: ["YearMonth", "WeekOfTheYear"],
            vals: ["Fte"],
            aggregatorName: "Sum",
            rendererName: "Heatmap"
        },
    },
];
