var AttritionChart = function () {
    var data = function () {
        var timelines = getTimelines();
        var units = [];
        var seniorities = [];
        var jobprofiles = [];
        var locations = [];
        var totalItem = createItem('Total', timelines);

        reportData.Terminations.map(function (termination) {
            var unit = findOrCreateItem(termination.SubOrgUnitFlatName, timelines, units);
            var seniority = findOrCreateItem(termination.Seniority, timelines, seniorities);
            var jobprofile = findOrCreateItem(termination.JobProfile, timelines, jobprofiles);
            var location = findOrCreateItem(termination.Location, timelines, locations);

            addTerminationDataToItem(termination, unit);            
            addTerminationDataToItem(termination, seniority);
            addTerminationDataToItem(termination, jobprofile);
            addTerminationDataToItem(termination, location);
            addTerminationDataToItem(termination, totalItem);
        });

        units.push(totalItem);
        seniorities.push(totalItem);
        jobprofiles.push(totalItem);
        locations.push(totalItem);

        return {
            Timelines: timelines,
            Units: units,
            Seniorities: seniorities,
            Jobprofiles: jobprofiles,
            Locations: locations
        }
    }();

    function findOrCreateItem(name, timelines, itemList) {
        var item = itemList.filter(function (item) {
            return item.Name === name;
        })[0];

        if (!item) {
            item = createItem(name, timelines);
            itemList.push(item);
        }

        return item;
    };

    function createItem(name, timelines) {
        return {
            Name: name,
            Periods: timelines.map(function (d) {
                return {
                    Name: d,
                    Attritions: 0,
                    Turnovers: 0,
                    AttritionFte: 0,
                    TurnoverFte: 0,
                    NewHire: 0,
                    NewHireFte: 0,
                    HeadCountChange: 0,
                    HeadCountChangeFte: 0,
                }
            })
        };
    }

    function addTerminationDataToItem(termination, itemData) {
        var yearMonth = getYearMonthDate(new Date(termination.Date));
        var period = itemData.Periods.filter(function (period) {
            return period.Name === yearMonth;
        })[0];

        if (period !== undefined)
        {
            period.Attritions += termination.IsVoluntarilyTurnover;
            period.Turnovers += termination.IsOtherDepartureReasons;
            period.AttritionFte += termination.IsVoluntarilyTurnover * termination.FteRatio;
            period.TurnoverFte += termination.IsOtherDepartureReasons * termination.FteRatio;
            period.NewHire += termination.NewHire;
            period.NewHireFte += termination.NewHire * termination.FteRatio;
            period.HeadCountChange += termination.EmployeeRotation;
            period.HeadCountChangeFte += termination.EmployeeRotation * termination.FteRatio;
        }
    };

    var getOptions = function (reportType, yAxisLabel) {
        return {
            responsive: true,
            title: {
                text: reportType + ' report',
                display: true,
                lineHeight: 1
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Time'
                    },
                    ticks: {
                        maxTicksLimit: 20
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        labelString: yAxisLabel,
                        display: true
                    },
                    ticks: {
                        beginAtZero: true,
                        stepSize: 1,
                        maxTicksLimit: 10
                    }
                }]
            },
            elements: {
                line: {
                    cubicInterpolationMode: 'monotone',
                }
            }
        }
    };

    function getTimelines() {
        var minDate = new Date(reportParameters.DateFrom);
        var maxDate = new Date(reportParameters.DateTo);

        var timelines = [];
        var currentDate = new Date(minDate.getFullYear(), minDate.getMonth());

        while (currentDate.getFullYear() < maxDate.getFullYear() ||
            (currentDate.getFullYear() === maxDate.getFullYear() && currentDate.getMonth() <= maxDate.getMonth())) {
            var yearMonth = getYearMonthDate(currentDate);
            timelines.push(yearMonth);
            currentDate.setMonth(currentDate.getMonth() + 1, 1);
        }

        return timelines;
    };

    function getYearMonthDate(date) {
        var month = ("0" + (date.getMonth() + 1)).slice(-2);
        var yearMonth = date.getFullYear() + "-" + month;

        return yearMonth;
    };

    return {
        Data: data,
        GetOptions: getOptions
    }
}();

var configurations = [
    {
        name: "Org Unit/Voluntary Turnover (Count)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Units.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.Attritions; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('Voluntary Turnover (count)', 'Voluntary Turnover (Count)')
        }
    },
    {
        name: "Org Unit/Other Departure Reasons (Count)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Units.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.Turnovers; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('Other Departure Reasons (count)', 'Other Departure Reasons (Count)')
        }
    },
    {
        name: "Org Unit/Voluntary Turnover (FTE)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Units.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.AttritionFte; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('Voluntary Turnover (fte)', 'Voluntary Turnover (Fte)')
        }
    },
    {
        name: "Org Unit/Other Departure Reasons (FTE)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Units.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.TurnoverFte; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('Other Departure Reasons (fte)', 'Other Departure Reasons (Fte)')
        }
    },
    {
        name: "Org Unit/New Hire (Count)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Units.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.NewHire; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('New Hire (count)', 'New Hire (Count)')
        }
    },
    {
        name: "Org Unit/New Hire (FTE)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Units.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.NewHireFte; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('New Hire (fte)', 'New Hire (Fte)')
        }
    },
    {
        name: "Org Unit/Head Count Change (Count)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Units.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.HeadCountChange; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('Head Count Change (count)', 'Head Count Change (Count)')
        }
    },
    {
        name: "Org Unit/Head Count Change (FTE)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Units.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.HeadCountChangeFte; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('Head Count Change (fte)', 'Head Count Change (Fte)')
        }
    }, 
    {
        name: "Seniority/Voluntary Turnover (Count)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Seniorities.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.Attritions; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('Voluntary Turnover (count)', 'Voluntary Turnover (Count)')
        }
    },
    {
        name: "Seniority/Other Departure Reasons (Count)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Seniorities.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.Turnovers; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('Other Departure Reasons (count)', 'Other Departure Reasons (Count)')
        }
    },
    {
        name: "Seniority/Voluntary Turnover (FTE)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Seniorities.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.AttritionFte; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('Voluntary Turnover (fte)', 'Voluntary Turnover (Fte)')
        }
    },
    {
        name: "Seniority/Other Departure Reasons (FTE)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Seniorities.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.TurnoverFte; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('Other Departure Reasons (fte)', 'Other Departure Reasons (Fte)')
        }
    },
    {
        name: "Seniority/New Hire (Count)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Seniorities.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.NewHire; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('New Hire (count)', 'New Hire (Count)')
        }
    },
    {
        name: "Seniority/New Hire (FTE)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Seniorities.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.NewHireFte; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('New Hire (fte)', 'New Hire (Fte)')
        }
    },
    {
        name: "Seniority/Head Count Change (Count)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Seniorities.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.HeadCountChange; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('Head Count Change (count)', 'Head Count Change (Count)')
        }
    },
    {
        name: "Seniority/Head Count Change (FTE)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Seniorities.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.HeadCountChangeFte; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('Head Count Change (fte)', 'Head Count Change (Fte)')
        }
    },
    {
        name: "JobProfile/Voluntary Turnover (Count)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Jobprofiles.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.Attritions; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('Voluntary Turnover (count)', 'Voluntary Turnover (Count)')
        }
    },
    {
        name: "JobProfile/Other Departure Reasons (Count)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Jobprofiles.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.Turnovers; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('Other Departure Reasons (count)', 'Other Departure Reasons (Count)')
        }
    },
    {
        name: "JobProfile/Voluntary Turnover (FTE)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Jobprofiles.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.AttritionFte; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('Voluntary Turnover (fte)', 'Voluntary Turnover (Fte)')
        }
    },
    {
        name: "JobProfile/Other Departure Reasons (FTE)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Jobprofiles.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.TurnoverFte; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('Other Departure Reasons (fte)', 'Other Departure Reasons (Fte)')
        }
    },
    {
        name: "JobProfile/New Hire (Count)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Jobprofiles.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.NewHire; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('New Hire (count)', 'New Hire (Count)')
        }
    },
    {
        name: "JobProfile/New Hire (FTE)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Jobprofiles.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.NewHireFte; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('New Hire (fte)', 'New Hire (Fte)')
        }
    },
    {
        name: "JobProfile/Head Count Change (Count)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Jobprofiles.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.HeadCountChange; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('Head Count Change (count)', 'Head Count Change (Count)')
        }
    },
    {
        name: "JobProfile/Head Count Change (FTE)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Jobprofiles.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.HeadCountChangeFte; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('Head Count Change (fte)', 'Head Count Change (Fte)')
        }
    },
    {
        name: "Localization/Voluntary Turnover (Count)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Locations.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.Attritions; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('Voluntary Turnover (count)', 'Voluntary Turnover (Count)')
        }
    },
    {
        name: "Localization/Other Departure Reasons (Count)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Locations.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.Turnovers; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('Other Departure Reasons (count)', 'Other Departure Reasons (Count)')
        }
    },
    {
        name: "Localization/Voluntary Turnover (FTE)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Locations.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.AttritionFte; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('Voluntary Turnover (fte)', 'Voluntary Turnover (Fte)')
        }
    },
    {
        name: "Localization/Other Departure Reasons (FTE)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Locations.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.TurnoverFte; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('Other Departure Reasons (fte)', 'Other Departure Reasons (Fte)')
        }
    },
    {
        name: "Localization/New Hire (Count)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Locations.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.NewHire; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('New Hire (count)', 'New Hire (Count)')
        }
    },
    {
        name: "Localization/New Hire (FTE)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Locations.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.NewHireFte; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('New Hire (fte)', 'New Hire (Fte)')
        }
    },
    {
        name: "Localization/Head Count Change (Count)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Locations.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.HeadCountChange; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('Head Count Change (count)', 'Head Count Change (Count)')
        }
    },
    {
        name: "Localization/Head Count Change (FTE)",
        config: {
            type: 'line',
            data: {
                labels: AttritionChart.Data.Timelines,
                datasets: AttritionChart.Data.Locations.map(function (item, index) {
                    return {
                        label: item.Name,
                        backgroundColor: ChartReport.getColor(index),
                        borderColor: ChartReport.getColor(index),
                        data: item.Periods.map(function (period) { return period.HeadCountChangeFte; }),
                        fill: false
                    }
                })
            },
            options: AttritionChart.GetOptions('Head Count Change (fte)', 'Head Count Change (Fte)')
        }
    }
];

