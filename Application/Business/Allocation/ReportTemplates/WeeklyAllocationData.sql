--DECLARE @EmployeeOrgUnitId BIGINT
--SET @EmployeeOrgUnitId = 168

--DECLARE @ProjectOrgUnitId BIGINT
--SET @ProjectOrgUnitId = 2

--DECLARE @DateFrom DATETIME
--SET @DateFrom = '2018-05-01 00:00:00.000'

--DECLARE @DateTo DATETIME
--SET @DateTo = '2018-05-31 23:59:59.999'

--first day current week
SET @DateFrom = DATEADD(wk, 0, DATEADD(DAY, 2-DATEPART(WEEKDAY, @DateFrom), DATEDIFF(dd, 0, @DateFrom))) 

--last day current week
SET @DateTo = DATEADD(day, -1, DATEADD(week, (DATEPART(week, @DateTo) - DATEPART(week, @DateFrom)), @DateFrom))


SELECT 
		eou.Path,
		c.[Name] AS Company,
		eou.FlatName AS EmployeeOrgUnit,
		jp.[Name] AS JobProfile,
		jml.Code,
		e.FirstName + ' ' + e.LastName AS Employee,
		l.[Name] AS [Location],
		pou.FlatName AS ProjectOrgUnit,
		s.ClientShortName AS Client,
		p.Apn AS APN,
		p.KupferwerkProjectId AS KupferwerkId,
		s.ProjectShortName + IIF(p.SubProjectShortName IS NULL, '', ' / ' + p.SubProjectShortName) AS ProjectName,
		f.WeekNo,		
		f.FTEAllocated AS "FTE Allocated",
		f.FTEPlanned AS "FTE Planned",
		f.FTEFree AS "FTE Available"
FROM
(
	-- FTE Free
	SELECT 
		ws.EmployeeId AS EmployeeId,	
		DATEPART(week, [Week]) AS WeekNo,
		0 AS FTEAllocated,
		0 AS FTEPlanned,
		(ws.AvailableHours - ws.PlannedHours - ws.AllocatedHours) / 40 AS FTEFree,
		NULL AS ProjectId
	FROM Allocation.WeeklyAllocationStatus ws
	WHERE ws.AvailableHours - ws.PlannedHours - ws.AllocatedHours > 0
		AND [Week] >= @DateFrom AND [Week] < @DateTo
	UNION
	-- FTE Planned
	SELECT 
		dr.EmployeeId AS EmployeeId,
		DATEPART(week, dr.[Day]) AS WeekNo,
		0 AS FTEAllocated,
		SUM(dr.Hours) / 40 AS FTEPlanned,
		0 AS FTEFree,
		dr.ProjectId
	FROM Allocation.AllocationDailyRecord dr
	JOIN Allocation.AllocationRequest r ON dr.AllocationRequestId = r.Id
	WHERE r.AllocationCertainty = 0 AND dr.[Day] >= @DateFrom AND dr.[Day] < @DateTo
	GROUP BY dr.EmployeeId, dr.ProjectId, dr.Hours, DATEPART(week, dr.Day)
	HAVING SUM(dr.Hours)>0
	UNION
	-- FTE Allocated
	SELECT 
		dr.EmployeeId AS EmployeeId,
		DATEPART(week, dr.Day) AS WeekNo,
		SUM(dr.Hours) / 40 AS FTEAllocated,
		0 AS FTEPlanned,
		0 AS FTEFree,
		dr.ProjectId
	FROM Allocation.AllocationDailyRecord dr
	JOIN Allocation.AllocationRequest r ON dr.AllocationRequestId = r.Id
	WHERE r.AllocationCertainty = 1 AND dr.[Day] >= @DateFrom AND dr.[Day] < @DateTo
	GROUP BY dr.EmployeeId, dr.ProjectId, dr.Hours, DATEPART(week, dr.Day)
	HAVING SUM(dr.Hours)>0
) AS f
JOIN Allocation.Employee e ON e.Id = f.EmployeeId
LEFT JOIN Allocation.Project p ON p.Id = f.ProjectId
LEFT JOIN Allocation.ProjectSetup s ON s.Id = p.ProjectSetupId
LEFT JOIN Organization.OrgUnit pou ON s.OrgUnitId = pou.Id
LEFT JOIN Organization.OrgUnit eou ON e.OrgUnitId = eou.Id
LEFT JOIN Dictionaries.Company c ON e.CompanyId = c.Id
LEFT JOIN SkillManagement.JobProfile jp ON jp.Id = (SELECT Max(ejp.JobProfile_Id) FROM Allocation.EmployeeJobProfile ejp WHERE ejp.Employee_Id = e.Id)
LEFT JOIN SkillManagement.JobMatrixLevel jml ON jml.Id = (SELECT Max(ejml.JobMatrixLevel_Id) FROM Allocation.EmployeeJobMatrixLevel ejml WHERE ejml.Employee_Id = e.Id)
LEFT JOIN Dictionaries.[Location] l ON l.Id = e.LocationId
WHERE 
	(pou.Path IS NULL OR pou.Path LIKE (SELECT Path FROM Organization.OrgUnit WHERE Id = @ProjectOrgUnitId) + '%' ESCAPE '^' OR @ProjectOrgUnitId IS NULL)
	AND (eou.Path LIKE (SELECT Path FROM Organization.OrgUnit WHERE Id = @EmployeeOrgUnitId) + '%' ESCAPE '^' OR @EmployeeOrgUnitId IS NULL)