﻿namespace Smt.Atomic.Business.Allocation.Enums
{
    /// <summary>
    /// User picture sizes (used for different presentation purposes)
    /// </summary>
    public enum ProjectPictureSize
    {
        Small = 20,
        Normal = 70,
        Big = 240
    }
}