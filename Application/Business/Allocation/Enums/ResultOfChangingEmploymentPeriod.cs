﻿
using Smt.Atomic.Business.Allocation.Resources;
using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.Business.Allocation.Enums
{
    public enum ResultOfChangingEmploymentPeriod
    {
        NoConflicts,

        [DescriptionLocalized("AllocationRequestEndDateChangeMessage", typeof(AllocationRequestCardIndexDataServiceResources))]
        EndDateChanged,

        [DescriptionLocalized("NoEmploymentInPeriod", typeof(AllocationRequestCardIndexDataServiceResources))]
        IncorectEmploymentPeriod,

        [DescriptionLocalized("AllocationRequestBeginDateChangeMessage", typeof(AllocationRequestCardIndexDataServiceResources))]
        BeginDateChanged,

        [DescriptionLocalized("AllocationRequestBothDatesChangeMessage", typeof(AllocationRequestCardIndexDataServiceResources))]
        BothDatesChanged
    }
}
