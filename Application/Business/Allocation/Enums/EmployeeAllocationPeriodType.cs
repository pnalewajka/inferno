﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Allocation.Enums
{
    public enum EmployeeAllocationPeriodType
    {
        Week, Day
    }
}
