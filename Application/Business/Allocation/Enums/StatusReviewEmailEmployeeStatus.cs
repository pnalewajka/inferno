﻿using System.ComponentModel;

namespace Smt.Atomic.Business.Allocation.Enums
{
    public enum StatusReviewEmailEmployeeStatus
    {
        [Description("Free")]
        Free,

        [Description("Planned")]
        Planned,

        [Description("Non Allocated")]
        NonAllocated,

        [Description("Only planned new hire")]
        OnlyPlannedNewHire,

        [Description("Under utilized")]
        UnderUtilized,
    }
}
