﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.Allocation.BusinessEventHandlers;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Allocation.Services;
using Smt.Atomic.Business.Allocation.Workflows.Services;
using Smt.Atomic.Business.EventSourcing.Interfaces;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.Allocation
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            if (containerType == ContainerType.JobScheduler)
            {
                container.Register(Component.For<IProjectService>().ImplementedBy<ProjectService>().LifestyleTransient());
                container.Register(Component.For<IBusinessEventHandler>().ImplementedBy<DailyAllocationNeedsToRegenerateEventHandler>().LifestyleTransient());
                container.Register(Component.For<IBusinessEventHandler>().ImplementedBy<AddUserToActiveDirectoryGroupsEventHandler>().LifestyleTransient());
                container.Register(Component.For<IBusinessEventHandler>().ImplementedBy<RemoveUserFromActiveDirectoryGroupsEventHandler>().LifestyleTransient());
                container.Register(Component.For<IDateRangeService>().ImplementedBy<DateRangeService>().LifestyleTransient());
                container.Register(Component.For<IEmployeeAvailabilityDataService>().ImplementedBy<EmployeeAvailabilityDataService>().LifestyleTransient());
                container.Register(Component.For<IDailyAllocationService>().ImplementedBy<DailyAllocationService>().LifestyleTransient());
                container.Register(Component.For<IEmployeeService>().ImplementedBy<EmployeeService>().LifestyleTransient());
                container.Register(Component.For<IMyProfileService>().ImplementedBy<MyProfileService>().LifestyleTransient());
                container.Register(Component.For<IActiveDirectoryUserSyncService>().ImplementedBy<ActiveDirectoryUserSyncService>().LifestyleTransient());
                container.Register(Component.For<IStatusReviewEmailService>().ImplementedBy<StatusReviewEmailService>().LifestyleTransient());
                container.Register(Component.For<IAllocationContractTypeService>().ImplementedBy<AllocationContractTypeService>().LifestyleTransient());
                container.Register(Component.For<IAllocationJobTitleService>().ImplementedBy<AllocationJobTitleService>().LifestyleTransient());
                container.Register(Component.For<IEmploymentPeriodService>().ImplementedBy<EmploymentPeriodService>().LifestyleTransient());
                container.Register(Component.For<IActiveDirectoryGroupService>().ImplementedBy<ActiveDirectoryGroupService>().LifestyleTransient());
                container.Register(Component.For<IActiveDirectoryGroupSyncService>().ImplementedBy<ActiveDirectoryGroupSyncService>().LifestyleTransient());
                container.Register(Component.For<IRequestService>().ImplementedBy<SkillChangeRequestService>().LifestyleTransient());
                container.Register(Component.For<IEmployeeCardIndexDataService>().ImplementedBy<EmployeeCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IProjectCardIndexDataService>().ImplementedBy<ProjectCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ISubProjectCardIndexDataService>().ImplementedBy<SubProjectCardIndexDataService>().LifestyleTransient());
            }

            if (containerType == ContainerType.WebApp)
            {
                container.Register(Component.For<IProjectService>().ImplementedBy<ProjectService>().LifestylePerWebRequest());
                container.Register(Component.For<IEmployeeCardIndexDataService>().ImplementedBy<EmployeeCardIndexDataService>().LifestylePerWebRequest());
                container.Register(Component.For<IEmployeePickerCardIndexDataService>().ImplementedBy<EmployeePickerCardIndexDataService>().LifestylePerWebRequest());
                container.Register(Component.For<IManagerCardIndexDataService>().ImplementedBy<ManagerCardIndexDataService>().LifestylePerWebRequest());
                container.Register(Component.For<IEmploymentPeriodCardIndexDataService>().ImplementedBy<EmploymentPeriodCardIndexDataService>().LifestylePerWebRequest());
                container.Register(Component.For<IActiveDirectoryGroupService>().ImplementedBy<ActiveDirectoryGroupService>().LifestyleTransient());
                container.Register(Component.For<IProjectCardIndexDataService>().ImplementedBy<ProjectCardIndexDataService>().LifestylePerWebRequest());
                container.Register(Component.For<ISubProjectCardIndexDataService>().ImplementedBy<SubProjectCardIndexDataService>().LifestylePerWebRequest());
                container.Register(Component.For<IStaffingDemandCardIndexDataService>().ImplementedBy<StaffingDemandCardIndexDataService>().LifestylePerWebRequest());
                container.Register(Component.For<IDateRangeService>().ImplementedBy<DateRangeService>().LifestylePerWebRequest());
                container.Register(Component.For<WeeklyAllocationDataService>().LifestylePerWebRequest());
                container.Register(Component.For<DailyAllocationDataService>().LifestylePerWebRequest());
                container.Register(Component.For<RequestAllocationDataService>().LifestylePerWebRequest());
                container.Register(Component.For<IEmployeeAvailabilityDataService>().ImplementedBy<EmployeeAvailabilityDataService>().LifestylePerWebRequest());
                container.Register(Component.For<IEmployeeAllocationCardIndexDataService>().ImplementedBy<EmployeeAllocationCardIndexDataService>().LifestylePerWebRequest());
                container.Register(Component.For<IMyTeamAllocationCardIndexDataService>().ImplementedBy<MyTeamAllocationCardIndexDataService>().LifestylePerWebRequest());
                container.Register(Component.For<IAllocationRequestCardIndexDataService>().ImplementedBy<AllocationRequestCardIndexDataService>().LifestylePerWebRequest());
                container.Register(Component.For<IAllocationContractTypeService>().ImplementedBy<AllocationContractTypeService>().LifestylePerWebRequest());
                container.Register(Component.For<IAllocationJobTitleService>().ImplementedBy<AllocationJobTitleService>().LifestylePerWebRequest());
                container.Register(Component.For<IEmploymentPeriodService>().ImplementedBy<EmploymentPeriodService>().LifestylePerWebRequest());
                container.Register(Component.For<IEmployeeService>().ImplementedBy<EmployeeService>().LifestylePerWebRequest());
                container.Register(Component.For<IAllocationRequestImportLookupService>().ImplementedBy<AllocationRequestImportLookupService>().LifestylePerWebRequest());
                container.Register(Component.For<IStaffingDemandService>().ImplementedBy<StaffingDemandService>().LifestylePerWebRequest());
                container.Register(Component.For<IDailyAllocationService>().ImplementedBy<DailyAllocationService>().LifestylePerWebRequest());
                container.Register(Component.For<IBenchEmployeeCardIndexDataService>().ImplementedBy<BenchEmployeeCardIndexDataService>().LifestylePerWebRequest());
                container.Register(Component.For<IBenchEmployeesService>().ImplementedBy<BenchEmployeesService>().LifestylePerWebRequest());
                container.Register(Classes.FromThisAssembly().BasedOn<IUrlTokenResolver>().WithServiceFromInterface().LifestylePerWebRequest());
                container.Register(Component.For<IEmployeeCommentService>().ImplementedBy<EmployeeCommentService>().LifestylePerWebRequest());
                container.Register(Component.For<IMyProfileService>().ImplementedBy<MyProfileService>().LifestylePerWebRequest());
                container.Register(Component.For<IRequestService>().ImplementedBy<SkillChangeRequestService>().LifestylePerWebRequest());
                container.Register(Component.For<IProjectTagCardIndexDataService>().ImplementedBy<ProjectTagCardIndexDataService>().LifestylePerWebRequest());
                container.Register(Component.For<IActiveDirectoryGroupCardIndexDataService>().ImplementedBy<ActiveDirectoryGroupCardIndexDataService>().LifestylePerWebRequest());
                container.Register(Component.For<IEmployeeContractTypeCardIndexDataService>().ImplementedBy<EmployeeContractTypeCardIndexDataService>().LifestylePerWebRequest());
                container.Register(Component.For<IAllocationRequestService>().ImplementedBy<AllocationRequestService>().LifestylePerWebRequest());
                container.Register(Component.For<IEmployeeAllocationService>().ImplementedBy<EmployeeAllocationService>().LifestylePerWebRequest());
            }

            container.Register(Component.For<IApprovingPersonService>().ImplementedBy<ApprovingPersonService>().LifestyleTransient());
            container.Register(Component.For<IProjectClientCardIndexDataService>().ImplementedBy<ProjectClientCardIndexDataService>().LifestyleTransient());
        }
    }
}