﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class ProjectDto : SubProjectDto
    {
        public string Region { get; set; }

        public ProjectType? ProjectType { get; set; }

        public ProjectFeatures ProjectFeatures { get; set; }

        public string ClientShortName { get; set; }

        public long OrgUnitId { get; set; }

        public long? ProjectManagerId { get; set; }

        public long? SupervisorManagerId { get; set; }

        public long? SalesAccountManagerId { get; set; }

        public long? StaffingDemandId { get; set; }

        public ProjectStatus ProjectStatus { get; set; }

        public long? CalendarId { get; set; }

        public string Color { get; set; }

        public string GenericName { get; set; }

        public byte[] Timestamp { get; set; }

        public IEnumerable<long> IndustryIds { get; set; }

        public bool IsAvailableForSalesPresentation { get; set; }

        public IEnumerable<long> TechnologyIds { get; set; }

        public IEnumerable<long> KeyTechnologyIds { get; set; }

        public IEnumerable<long> TagIds { get; set; }

        public DocumentDto Picture { get; set; }

        public IEnumerable<long> ServiceLineIds { get; set; }

        public ProjectPhase CurrentPhase { get; set; }

        public ProjectDisclosures Disclosures { get; set; }

        public bool HasClientReferences { get; set; }

        public IEnumerable<long> SoftwareCategoryIds { get; set; }

        public long? CustomerSizeId { get; set; }

        public string BusinessCase { get; set; }

        public List<DocumentDto> ReferenceMaterialAttachments { get; set; }

        public List<DocumentDto> ProjectInformationAttachments { get; set; }

        public List<DocumentDto> OtherAttachments { get; set; }

        // TimeTracking

        public string ProjectManagerFullName { get; set; }

        public string SupervisorFullName { get; set; }

        public string OrgUnitName { get; set; }

        public bool ReinvoicingToCustomer { get; set; }

        public long? ReinvoiceCurrencyId { get; set; }

        public CostApprovalPolicy CostApprovalPolicy { get; set; }

        public long? MaxCostsCurrencyId { get; set; }

        public decimal? MaxHotelCosts { get; set; }

        public decimal? MaxTotalFlightsCosts { get; set; }

        public decimal? MaxOtherTravelCosts { get; set; }

        public ProjectInvoicingRateDto[] InvoicingRates { get; set; }

        public string InvoicingAlgorithm { get; set; }
    }
}