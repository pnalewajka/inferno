﻿using System;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class EmploymentPeriodDateFilterDto
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }
}
