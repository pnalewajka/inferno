﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
using System.Collections.Generic;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class BenchEmployeeDto
    {
        public long Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public long? LocationId { get; set; }

        public long? LineManagerId { get; set; }

        public long OrgUnitId { get; set; }

        public long? ManagerId { get; set; }

        public DateTime CurrentDate { get; set; }

        public int WeeksOnBench { get; set; }

        public DateTime? OnBenchSince { get; set; }

        public IList<StaffingStatusDto> StaffingStatus { get; set; }

        public long[] JobProfileIds { get; set; }

        public long[] SkillIds { get; set; }

        public long[] JobMatrixIds { get; set; }

        public string StaffingComment { get; set; }

        public decimal HoursAllocated { get; set; }

        public decimal AvailableHours { get; set; }

        public DateTime? LastResumeModifiedOn { get; set; }

        public int FillPercentage { get; set; }

        public string OrgUnitFlatName { get; set; }

        public decimal VacationBalance { get; set; }
    }
}
