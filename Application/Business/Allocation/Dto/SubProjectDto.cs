﻿using System;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class SubProjectDto
    {
        public long Id { get; set; }

        public long SetupId { get; set; }

        public bool IsMainProject { get; set; }

        public string ProjectShortName { get; set; }

        public string PetsCode { get; set; }

        public string JiraKey { get; set; }

        public string JiraIssueKey { get; set; }

        public bool IsJiraProject => !string.IsNullOrEmpty(JiraIssueKey);

        public long JiraIssueToTimeReportRowMapperId { get; set; }

        public string Apn { get; set; }

        public string KupferwerkProjectId { get; set; }

        public string Description { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public long? TimeTrackingImportSourceId { get; set; }

        public bool IsApprovedByRequesterMainManager { get; set; }

        public bool IsApprovedByMainManagerForProjectManager { get; set; }

        public IEnumerable<long> TimeReportAcceptingPersonIds { get; set; }

        public OverTimeTypes AllowedOvertimeTypes { get; set; }

        public TimeTrackingProjectType TimeTrackingType { get; set; }

        public long? UtilizationCategoryId { get; set; }

        public IEnumerable<long> AttributesIds { get; set; }

        public DateTime? ReportingStartsOn { get; set; }

        public string Acronym { get; set; }

        public bool IsOwnProject { get; set; }

        public string ClientProjectName { get; set; }

        public string ProjectName { get; set; }

        public BonusTypeSet AllowedBonusTypes { get; set; }
    }
}
