namespace Smt.Atomic.Business.Allocation.Dto
{
    public class JobProfileChartDetailsDto
    {
        public long? LocationId { get; set; }

        public long JobProfileId { get; set; }

        public long? JobMatrixLevel { get; set; }

        public string LocationName { get; set; }

        public string JobProfileName { get; set; }

        public string JobMatrixLevelCode { get; set; }

        public string JobMatrixLevelName { get; set; }

        public decimal AllocatedFte { get; set; }

        public decimal AvailableFte { get; set; }

        public decimal PlannedFte { get; set; }

        public decimal FreeFte
            => AllocatedFte + PlannedFte >= AvailableFte ? 0 : AvailableFte - AllocatedFte - PlannedFte;
    }
}