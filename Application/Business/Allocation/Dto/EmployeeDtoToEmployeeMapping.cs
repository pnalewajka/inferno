﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Resumes;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class EmployeeDtoToEmployeeMapping : ClassMapping<EmployeeDto, Employee>
    {
        public EmployeeDtoToEmployeeMapping()
        {
            Mapping = d => new Employee
            {
                Id = d.Id,
                FirstName = d.FirstName,
                LastName = d.LastName,
                Email = d.Email,
                DateOfBirth = d.DateOfBirth,
                PersonalNumber = d.PersonalNumber,
                PersonalNumberType = d.PersonalNumberType,
                IdCardNumber = d.IdCardNumber,
                PhoneNumber = d.PhoneNumber,
                SkypeName = d.SkypeName,
                HomeAddress = d.HomeAddress,
                AvailabilityDetails = d.AvailabilityDetails,
                JobTitleId = d.JobTitleId,
                MinimumOvertimeBankBalance = d.MinimumOvertimeBankBalance,
                CurrentEmployeeStatus = d.CurrentEmployeeStatus,
                IsProjectContributor = d.IsProjectContributor,
                IsCoreAsset = d.IsCoreAsset,
                Timestamp = d.Timestamp,
                LocationId = d.LocationId,
                PlaceOfWork = d.PlaceOfWork,
                CompanyId = d.CompanyId,
                UserId = d.UserId,
                OrgUnitId = d.OrgUnitId,
                LineManagerId = d.LineManagerId,
                Acronym = d.Acronym,
                EmployeeFeatures = d.EmployeeFeatures,
                Resumes = d.SkillIds != null && d.SkillIds.Any()
                    ? new List<Resume>
                    {
                        new Resume
                        {
                            ResumeTechnicalSkills = d.SkillIds.Select(id => new ResumeTechnicalSkill
                            {
                                TechnicalSkillId = id
                            }).ToList()
                        }
                    }
                    : new List<Resume>(),
            };
        }
    }
}
