﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class EmployeeAllocationDto
    {
        public EmployeeAllocationDto()
        {
            Allocations = new EmployeeAllocationSingleRecordDto[0];
            AllocationBlocks = new AllocationBlockDto[0];
            EmploymentPeriodBlocks = new EmploymentPeriodBlockDto[0];
            Absences = new AbsenceBlockDto[0];
            Holidays = new DateTime[0];
        }

        public class ProjectAllocationDto
        {
            public decimal HoursAllocated { get; set; }

            public string ProjectColor { get; set; }

            public string ProjectName { get; set; }

            public long ProjectId { get; set; }

            public bool IsVacation { get; set; }

            public AllocationCertainty? Certainty { get; set; }
        }

        public class EmployeeAllocationSingleRecordDto
        {
            public AllocationStatus Status { get; set; }

            public decimal AvailableHours { get; set; }

            public DateTime AllocationPeriod { get; set; }

            public ProjectAllocationDto[] ProjectAllocations { get; set; }

            public IReadOnlyCollection<AbsenceRequestDto> EmployeeAbsences { get; set; }
        }

        public class AbsenceBlockDto
        {
            public LocalizedString AbsenceName { get; set; }

            public DateTime StartDate { get; set; }

            public DateTime EndDate { get; set; }

            public long? RequestId { get; set; }

            public RequestStatus Status { get; set; }

            public string StatusName { get; set; }

            public decimal Hours { get; set; }
        }

        public class EmploymentPeriodBlockDto
        {
            public DateTime StartDate { get; set; }

            public DateTime EndDate { get; set; }

            public decimal[] ContractedHours { get; set; }
        }

        public class AllocationBlockDto
        {
            public long RequestId { get; set; }

            public DateTime StartDate { get; set; }

            public DateTime? EndDate { get; set; }

            public string AllocationName { get; set; }

            public decimal[] AllocationDayHours { get; set; }

            public long ProjectId { get; set; }

            public string ProjectName { get; set; }

            public string ProjectShortName { get; set; }

            public string ProjectColor { get; set; }

            public string ProjectAcronym { get; set; }

            public bool ProjectColorIsDark { get; set; }

            public long? ProjectPictureId { get; set; }

            public string ClientShortName { get; set; }

            public string JiraIssueKey { get; set; }

            public string JiraKey { get; set; }

            public long EmployeeId { get; set; }

            public string EmployeeName { get; set; }

            public string EmployeeAcronym { get; set; }

            public AllocationCertainty Certainty { get; set; }

            public DateTime PeriodStartDate { get; set; }

            public DateTime PeriodEndDate { get; set; }

            public ProcessingStatus ProcessingStatus { get; set; }

            public AllocationRequestContentType ContentType { get; set; }
        }

        public long EmployeeId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public long? LocationId { get; set; }

        public string LocationName { get; set; }

        public long Id { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public long[] JobProfileIds { get; set; }

        public string[] JobProfileNames { get; set; }

        public long[] SkillIds { get; set; }

        public long[] JobMatrixIds { get; set; }

        public string[] JobMatrixLevels { get; set; }

        public long? UserId { get; set; }

        public long? LineManagerId { get; set; }

        public string LineManagerName { get; set; }

        public long OrgUnitId { get; set; }

        public string OrgUnitName { get; set; }

        public EmployeeAllocationSingleRecordDto[] Allocations { get; set; }

        public AllocationBlockDto[] AllocationBlocks { get; set; }

        public EmploymentPeriodBlockDto[] EmploymentPeriodBlocks { get; set; }

        public AbsenceBlockDto[] Absences { get; set; }

        public DateTime[] Holidays { get; set; }

        public string FullName { get; set; }
    }
}
