﻿namespace Smt.Atomic.Business.Allocation.Dto.StatusReviewEmail
{
    public class JobProfileDto
    {
        public string Name { get; set; }
        public string Level { get; set; }
    }
}