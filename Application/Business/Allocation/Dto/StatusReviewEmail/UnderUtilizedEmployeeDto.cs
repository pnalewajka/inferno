﻿using System.Collections.Generic;
using Smt.Atomic.Business.Allocation.Enums;
using Smt.Atomic.Business.Allocation.Interfaces.StatusReviewEmail;

namespace Smt.Atomic.Business.Allocation.Dto.StatusReviewEmail
{
    public class UnderUtilizedEmployeeDto : IEmployeeDto
    {
        public long Id { get; set; }
        public long OrganizationUnitId { get; set; }
        public string Location { get; set; }
        public string FullName { get; set; }
        public StatusReviewEmailEmployeeStatus Status { get; set; }
        public IList<string> JobProfiles { get; set; }
        public IList<string> JobMatrixLevels { get; set; }
        public IList<string> Skills { get; set; }
        public IList<WeekStatus> RelevantWeekStatuses { get; set; }
    }
}
