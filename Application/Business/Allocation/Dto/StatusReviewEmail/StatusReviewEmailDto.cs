﻿using System;
using System.Collections.Generic;

namespace Smt.Atomic.Business.Allocation.Dto.StatusReviewEmail
{
    public class StatusReviewEmailDto<TEmployeeDto>
    {
        public string ManagerFullName { get; set; }
        public long? ManagerUserId { get; set; }
        public string ManagerEmail { get; set; }
        public string ServerAddress { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public IList<OrgUnitDto<TEmployeeDto>> OrganizationUnits { get; set; }
    }
}