﻿namespace Smt.Atomic.Business.Allocation.Dto.StatusReviewEmail
{
    public class SimpleOrgUnitDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}