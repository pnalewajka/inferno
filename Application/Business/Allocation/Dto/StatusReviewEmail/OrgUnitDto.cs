﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Allocation.Dto.StatusReviewEmail
{
    public class OrgUnitDto<TEmployeeDto> : SimpleOrgUnitDto
    {
        public IList<SimpleOrgUnitDto> ParentOrgUnits { get; set; }
        public IList<TEmployeeDto> Employees { get; set; }
    }
}