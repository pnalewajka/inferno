﻿using System;

namespace Smt.Atomic.Business.Allocation.Dto.StatusReviewEmail
{
    public class WeekStatus
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public decimal Percentage { get; set; }
    }
}