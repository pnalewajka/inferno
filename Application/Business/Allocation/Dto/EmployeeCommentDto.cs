﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class EmployeeCommentDto
    {
        public long Id { get; set; }

        public EmployeeCommentType EmployeeCommentType { get; set; }

        public long EmployeeId { get; set; }

        public string Comment { get; set; }

        public DateTime? RelevantOn { get; set; }
    }
}
