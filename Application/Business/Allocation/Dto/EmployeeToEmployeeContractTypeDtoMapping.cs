﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class EmployeeToEmployeeContractTypeDtoMapping : ClassMapping<Employee, EmployeeContractTypeDto>
    {
        public EmployeeToEmployeeContractTypeDtoMapping(IClassMapping<EmploymentPeriod, EmploymentPeriodDto> mapping)
        {
            Mapping = e => new EmployeeContractTypeDto
            {
                EmployeeId = e.Id,
                FirstName = e.FirstName,
                LastName = e.LastName,
                Acronym = e.Acronym,
                CompanyId = e.CompanyId,
                LocationId = e.LocationId,
                CurrentEmployeeStatus = e.CurrentEmployeeStatus,
                ContractDuration = e.GetCurrentEmploymentPeriod() != null
                    ? e.GetCurrentEmploymentPeriod().ContractDuration
                    : (e.GetLastEmploymentPeriod() != null
                        ? e.GetLastEmploymentPeriod().ContractDuration
                        : null),
                ContractTypeId = e.GetCurrentEmploymentPeriod() != null
                    ? e.GetCurrentEmploymentPeriod().ContractTypeId
                    : (e.GetLastEmploymentPeriod() != null
                        ? (long?)e.GetLastEmploymentPeriod().ContractTypeId
                        : null),
                LineManagerId = e.LineManagerId,
                EmployeeEmploymentPeriods = e.EmploymentPeriods != null ? e.EmploymentPeriods.Select(mapping.CreateFromSource).ToArray() : null,
                CurrentEmployeeEmploymentPeriod = e.GetCurrentEmploymentPeriod() != null ? mapping.CreateFromSource(e.GetCurrentEmploymentPeriod()) : null,
                LastEmployeeEmploymentPeriod = e.GetLastEmploymentPeriod() != null ? mapping.CreateFromSource(e.GetLastEmploymentPeriod()) : null,
                EmployeeHourBalance = e.CurrentVacationBalance != null ? e.CurrentVacationBalance.TotalHourBalance : 0,
                LeavingReasonText = e.GetCurrentEmploymentPeriod() != null && e.GetCurrentEmploymentPeriod().TerminationReason != null
                    ? new LocalizedString
                    {
                        English = e.GetCurrentEmploymentPeriod().TerminationReason.NameEn,
                        Polish = e.GetCurrentEmploymentPeriod().TerminationReason.NamePl
                    }
                    : null,
                JobMatrixLevel = e.JobMatrixs != null && e.JobMatrixs.Any() ? e.JobMatrixs.Single().Code : "unspecified",
            };
        }
    }
}