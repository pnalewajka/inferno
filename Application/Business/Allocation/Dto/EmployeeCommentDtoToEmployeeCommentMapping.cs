﻿using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Allocation.Dto
{
    public class EmployeeCommentDtoToEmployeeCommentMapping : ClassMapping<EmployeeCommentDto, EmployeeComment>
    {
        public EmployeeCommentDtoToEmployeeCommentMapping()
        {
            Mapping =
                d =>
                    new EmployeeComment
                    {
                        Id = d.Id,
                        EmployeeCommentType = d.EmployeeCommentType,
                        EmployeeId = d.EmployeeId,
                        Comment = d.Comment,
                        RelevantOn = d.RelevantOn,
                    };
        }
    }
}
