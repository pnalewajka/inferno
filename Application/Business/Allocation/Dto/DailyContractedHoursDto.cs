﻿using System;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class DailyContractedHoursDto
    {
        public DateTime Date { get; set; }
        public EmploymentPeriodDto EmploymentPeriod { get; set; }
        public decimal Hours { get; set; }
    }
}
