﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class EmployeeContractTypeDtoToEmployeeMapping : ClassMapping<EmployeeContractTypeDto, Employee>
    {
    }
}