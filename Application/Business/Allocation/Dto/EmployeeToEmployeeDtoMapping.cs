﻿using System;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class EmployeeToEmployeeDtoMapping : ClassMapping<Employee, EmployeeDto>
    {
        public EmployeeToEmployeeDtoMapping(
            IOrgUnitService orgUnitService,
            IPrincipalProvider principalProvider)
        {
            Mapping = e => new EmployeeDto
            {
                Id = e.Id,
                FirstName = e.FirstName,
                LastName = e.LastName,
                Email = e.Email,
                DateOfBirth = e.DateOfBirth,
                PersonalNumber = e.PersonalNumber,
                PersonalNumberType = e.PersonalNumberType,
                IdCardNumber = e.IdCardNumber,
                PhoneNumber = e.PhoneNumber,
                SkypeName = e.SkypeName,
                HomeAddress = e.HomeAddress,
                AvailabilityDetails = e.AvailabilityDetails,
                JobTitleId = e.JobTitleId,
                MinimumOvertimeBankBalance = e.MinimumOvertimeBankBalance,
                CurrentEmployeeStatus = e.CurrentEmployeeStatus,
                IsProjectContributor = e.IsProjectContributor,
                IsCoreAsset = e.IsCoreAsset,
                Timestamp = e.Timestamp,
                CompanyId = e.CompanyId,
                UserId = e.UserId,
                LocationId = e.LocationId,
                PlaceOfWork = e.PlaceOfWork,
                JobProfileIds = e.JobProfiles.Select(m => m.Id).ToList(),
                SkillIds = e.Resumes.SelectMany(r => r.ResumeTechnicalSkills).OrderByDescending(r=>r.ExpertiseLevel).Select(r => r.TechnicalSkillId).ToList(),
                JobMatrixIds = e.JobMatrixs.Select(s => s.Id).ToList(),
                OrgUnitId = e.OrgUnitId,
                LineManagerId = e.LineManagerId,
                JobProfileNames = e.JobProfiles.Select(j => j.Name).ToList(),
                LocationName = e.Location == null ? "" : e.Location.Name,
                CompanyName = e.Company == null ? "" : e.Company.Name,
                JobMatrixName = e.JobMatrixs.Any() ? e.JobMatrixs.First().Code : string.Empty,
                SkillNames = e.Resumes.SelectMany(r => r.ResumeTechnicalSkills).OrderByDescending(r => r.ExpertiseLevel).Select(r => r.TechnicalSkill.Name).ToList(),
                JobTitleName = e.JobTitle == null
                    ? null
                    : new LocalizedString
                    {
                        English = e.JobTitle.NameEn,
                        Polish = e.JobTitle.NamePl
                    },
                OrgUnitName = e.OrgUnit == null ? "" : e.OrgUnit.FlatName,
                LineManagerFullName = e.LineManager == null ? "" : e.LineManager.FullName,
                Login = e.User == null ? "" : e.User.Login,
                Acronym = e.Acronym,
                ResumeModificationDate = e.Resumes.Count> 0 ? e.Resumes.Max(r=>r.ModifiedOn) : (DateTime?)null,
                LastResumeId = e.Resumes.OrderByDescending(r => r.ModifiedOn).Select(r => r.Id).FirstOrDefault(),
                FillPercentage = e.Resumes.Select(r=>r.FillPercentage).ToList(),
                EmployeeFeatures = e.EmployeeFeatures,
                IsOwnEmployee = EmployeeBusinessLogic.IsEmployeeOf.Call(
                    e,
                    principalProvider.Current.EmployeeId,
                    orgUnitService.GetManagerOrgUnitDescendantIds(principalProvider.Current.EmployeeId))
            };
        }
    }
}