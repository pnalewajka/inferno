﻿using System.Collections.Generic;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Dto.WelcomeEmail
{
    public class WelcomeEmailParametersDto
    {
        public IEnumerable<long> EmployeeIds { get; set; }
        public string BodyTemplateCode { get; set; }
        public string SubjectTemplateCode { get; set; }
        public WelcomeEmailDto MessageModel { get; set; }
    }
}
