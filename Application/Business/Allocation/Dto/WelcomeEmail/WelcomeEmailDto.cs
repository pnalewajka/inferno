﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Allocation.Dto.WelcomeEmail
{
    public class WelcomeEmailDto
    {
        public string EmailType { get; set; }

        public string FirstName { get; set; }

        public List<MessageSectionDto> Sections { get; set; }
    }
}
