﻿using System.Collections.Generic;


namespace Smt.Atomic.Business.Allocation.Dto.WelcomeEmail
{
    public class MessageSectionDto
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public List<MessageElementDto> Elements { get; set; }
    }
}
