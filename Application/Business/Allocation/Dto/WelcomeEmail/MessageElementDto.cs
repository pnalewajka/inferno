﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Allocation.Dto.WelcomeEmail
{
    public class MessageElementDto
    {
        public string Content { get; set; }
        public string Uri { get; set; }
    }
}
