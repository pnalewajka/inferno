﻿using System.Collections.Generic;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class MyProfileDto
    {
        public long Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public LocalizedString JobTitle { get; set; }

        public string LocationName { get; set; }

        public EmployeeStatus CurrentEmployeeStatus { get; set; }

        public bool IsProjectContributor { get; set; }

        public bool IsCoreAsset { get; set; }

        public byte[] Timestamp { get; set; }

        public string JobProfileNames { get; set; }

        public string SkillNames { get; set; }

        public IDictionary<long, string> ActiveDirectoryEmailGroupNames { get; set; }

        public IDictionary<long, string> ActiveDirectorySecurityGroupNames { get; set; }

        public bool CanViewActiveDirectoryGroups { get; set; }

        public CompanyDto Company { get; set; }

        public UserDto User { get; set; }

        public EmployeeDto LineManager { get; set; }

        public string OrgUnitName { get; set; }

        public bool IsInOrgChart { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public long? UserId { get; set; }

        public long OrgUnitId { get; set; }

        public decimal AvailableVacations { get; set; }

        public decimal TotalVacations { get; set; }

        public decimal AvailableOvertime { get; set; }

        public SurveyDto[] Surveys { get; set; }

        public long PendingRequestCount { get; set; }
    }
}
