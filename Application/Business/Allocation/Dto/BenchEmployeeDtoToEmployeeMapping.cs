﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Resumes;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class BenchEmployeeDtoToEmployeeMapping : ClassMapping<BenchEmployeeDto, Employee>
    {
        public BenchEmployeeDtoToEmployeeMapping()
        {
            Mapping =
                d =>
                    new Employee
                    {
                        Id = d.Id,
                        FirstName = d.FirstName,
                        LastName = d.LastName,
                        LocationId = d.LocationId,
                        LineManagerId = d.LineManagerId,
                        OrgUnitId = d.OrgUnitId,
                        Resumes = d.SkillIds.Any()
                            ? new List<Resume>
                            {
                                new Resume
                                {
                                    ResumeTechnicalSkills = d.SkillIds.Select(id => new ResumeTechnicalSkill
                                    {
                                        TechnicalSkillId = id
                                    }).ToList()
                                }
                            }
                            : new List<Resume>()
                    };
        }
    }
}
