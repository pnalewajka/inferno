﻿using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Allocation.Dto
{
    public class StaffingDemandDtoToStaffingDemandMapping : ClassMapping<StaffingDemandDto, StaffingDemand>
    {
        public StaffingDemandDtoToStaffingDemandMapping()
        {
            Mapping = d => new StaffingDemand
            {
                StartDate = d.StartDate,
                EndDate = d.EndDate,
                Description = d.Description,
                EmployeesNumber = d.EmployeesNumber,
                ProjectId = d.ProjectId,
                JobMatrixLevelId = d.JobMatrixLevelId,
                JobProfileId = d.JobProfileId,
                Id = d.Id,
                Timestamp = d.Timestamp
            };
        }
    }
}
