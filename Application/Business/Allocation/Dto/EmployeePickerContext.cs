namespace Smt.Atomic.Business.Allocation.Dto
{
    public class EmployeePickerContext
    {
        public enum PickerMode
        {
            NoFilter = 0,

            EmployeesToChange = 1,

            EmployeesToTerminate = 2,

            Recruiters = 3,

            NotTerminatedEmployees = 4,

            RecruitmentDataCertified = 5,

            TechnicalInterviewers = 6,

            RecruitmentDataWatcher = 7,

            ReportingRecruiters = 8,

            ReportingHiringManagers = 9,

            HiringManagers = 10,

            RecruitmentDataCertifiedOrTechnicalInterviewers = 11,
        }

        public PickerMode Mode { get; set; }
    }
}
