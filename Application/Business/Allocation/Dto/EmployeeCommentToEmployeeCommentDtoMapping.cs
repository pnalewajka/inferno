﻿using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Allocation.Dto
{
    public class EmployeeCommentToEmployeeCommentDtoMapping : ClassMapping<EmployeeComment, EmployeeCommentDto>
    {
        public EmployeeCommentToEmployeeCommentDtoMapping()
        {
            Mapping =
                e =>
                    new EmployeeCommentDto
                    {
                        Id = e.Id,
                        EmployeeCommentType = e.EmployeeCommentType,
                        EmployeeId = e.EmployeeId,
                        Comment = e.Comment,
                        RelevantOn = e.RelevantOn,
                    };
        }
    }
}
