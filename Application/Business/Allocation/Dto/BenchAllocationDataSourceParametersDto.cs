﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class BenchAllocationDataSourceParametersDto
    {
        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }

        public long? OrgUnitId { get; set; }

        public long[] ContractTypeIds { get; set; }

        public bool SkipWorkingStudents { get; set; }

        public long[] LocationIds { get; set; }

        public PlaceOfWork? PlaceOfWork { get; set; }

        public long[] JobProfileIds { get; set; }

        public StaffingStatus[] StaffingStatuses { get; set; }

        public EmployeeStatus[] EmployeeStatuses { get; set; }
    }
}
