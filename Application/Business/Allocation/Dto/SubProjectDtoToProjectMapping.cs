﻿using System.Collections.ObjectModel;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class SubProjectDtoToProjectMapping : ClassMapping<SubProjectDto, Project>
    {
        public SubProjectDtoToProjectMapping(
            IClassMappingFactory mappingFactory)
        {
            Mapping = d => new Project
            {
                Id = d.Id,
                ProjectSetupId = d.SetupId,
                IsMainProject = d.IsMainProject,
                JiraIssueKey = d.JiraIssueKey,
                JiraKey = d.JiraKey,
                PetsCode = d.PetsCode,
                Apn = d.Apn,
                KupferwerkProjectId = d.KupferwerkProjectId,
                SubProjectShortName = d.IsMainProject ? null : d.ProjectShortName,
                Description = d.Description,
                StartDate = d.StartDate,
                EndDate = d.EndDate,
                TimeTrackingImportSourceId = d.TimeTrackingImportSourceId,
                AllowedOvertimeTypes = d.AllowedOvertimeTypes,
                TimeReportAcceptingPersons = d.TimeReportAcceptingPersonIds == null ? null : new Collection<Employee>(d.TimeReportAcceptingPersonIds.Select(v => new Employee { Id = v }).ToList()),
                IsApprovedByRequesterMainManager = d.IsApprovedByRequesterMainManager,
                IsApprovedByMainManagerForProjectManager = d.IsApprovedByMainManagerForProjectManager,
                TimeTrackingType = d.TimeTrackingType,
                JiraIssueToTimeReportRowMapperId = d.JiraIssueToTimeReportRowMapperId,
                UtilizationCategoryId = d.UtilizationCategoryId,
                Attributes = d.AttributesIds == null ? null : new Collection<TimeReportProjectAttribute>(d.AttributesIds.Select(v => new TimeReportProjectAttribute { Id = v }).ToList()),
                ReportingStartsOn = d.ReportingStartsOn,
                Acronym = d.Acronym,
                AllowedBonusTypes = d.AllowedBonusTypes,
            };
        }
    }
}
