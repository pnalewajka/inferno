﻿using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Allocation.Dto
{
    public class ProjectTagToProjectTagDtoMapping : ClassMapping<ProjectTag, ProjectTagDto>
    {
        public ProjectTagToProjectTagDtoMapping()
        {
            Mapping = e => new ProjectTagDto
            {
                Id = e.Id,
                Name = e.Name,
                Description = e.Description,
                TagType = e.TagType,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
