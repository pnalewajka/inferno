﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class ActiveDirectoryGroupToActiveDirectoryGroupDtoMapping : ClassMapping<ActiveDirectoryGroup, ActiveDirectoryGroupDto>
    {
        public ActiveDirectoryGroupToActiveDirectoryGroupDtoMapping()
        {
            Mapping = e => new ActiveDirectoryGroupDto
            {
                Id = e.Id,
                ActiveDirectoryId = e.ActiveDirectoryId,
                Name = e.Name,
                GroupType = e.GroupType,
                Email = e.Email,
                Description = e.Description,
                Info = e.Info,
                Aliases = e.Aliases,
                UsersIds = e.Users.Select(x => x.Id).ToArray(),
                ManagersIds = e.Managers.Select(x => x.Id).ToArray(),
                ActiveDirectoryUserIds = e.Users.Where(x => x.ActiveDirectoryId.HasValue).Select(x => x.ActiveDirectoryId.Value).ToArray(),
                ActiveDirectoryManagerIds = e.Managers.Where(x => x.ActiveDirectoryId.HasValue).Select(x => x.ActiveDirectoryId.Value).ToArray(),
                ActiveDirectorySubgroupIds = e.Subgroups.Select(g => g.ActiveDirectoryId).ToArray(),
                OwnerId = e.OwnerId,
                SubgroupIds = e.Subgroups.Select(g => g.Id).ToArray(),
                Timestamp = e.Timestamp,
            };
        }
    }
}
