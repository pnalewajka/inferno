﻿using System;

namespace Smt.Atomic.Business.Allocation.Dto
{
    internal struct EmployeeAllocationTimePeriodIdentifier
    {
        public long EmployeeId { get; set; }
        public DateTime AllocationPeriodStart { get; set; }
    }
}
