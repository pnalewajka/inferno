﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class EmploymentPeriodDto : IWeeklyHours
    {
        public long Id { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public DateTime SignedOn { get; set; }

        public long EmployeeId { get; set; }

        public EmploymentType? EmploymentType { get; set; }

        public long? CompanyId { get; set; }

        public string CompanyName { get; set; }

        public long? JobTitleId { get; set; }

        public bool IsActive { get; set; }

        public EmployeeInactivityReason InactivityReason { get; set; }

        public LocalizedString TerminationReasonText { get; set; }

        public NoticePeriod? NoticePeriod { get; set; }

        public decimal MondayHours { get; set; }

        public decimal TuesdayHours { get; set; }

        public decimal WednesdayHours { get; set; }

        public decimal ThursdayHours { get; set; }

        public decimal FridayHours { get; set; }

        public decimal SaturdayHours { get; set; }

        public decimal SundayHours { get; set; }

        public DateTime? TerminatedOn { get; set; }

        public bool ChangeExistingAllocations { get; set; }

        public bool ClosePreviousPeriod { get; set; }

        public long ContractTypeId { get; set; }

        public LocalizedString ContractTypeName { get; set; }

        public ContractDurationType? ContractDuration { get; set; }

        public long? CalendarId { get; set; }

        public decimal VacationHourToDayRatio { get; set; }

        public TimeReportingMode TimeReportingMode { get; set; }

        public long? AbsenceOnlyDefaultProjectId { get; set; }

        public string AbsenceOnlyDefaultProjectShortName { get; set; }

        public bool IsClockCardRequired { get; set; }

        public long? TerminationReasonId { get; set; }

        public long? CompensationCurrencyId { get; set; }

        public string ContractorCompanyName { get; set; }

        public string ContractorCompanyTaxIdentificationNumber { get; set; }

        public string ContractorCompanyStreetAddress { get; set; }

        public string ContractorCompanyCity { get; set; }

        public string ContractorCompanyZipCode { get; set; }

        public decimal? HourlyRate { get; set; }

        public decimal? HourlyRateDomesticOutOfOfficeBonus { get; set; }

        public decimal? HourlyRateForeignOutOfOfficeBonus { get; set; }

        public decimal? MonthlyRate { get; set; }

        public decimal? SalaryGross { get; set; }

        public decimal? AuthorRemunerationRatio { get; set; }

        public bool IsTaxDeductibleCostEnabled { get; set; }

        public long? LocationId { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
