﻿namespace Smt.Atomic.Business.Allocation.Dto
{
    public class ProjectClientDto
    {
        public long ProjectId { get; set; }

        public string ClientName { get; set; }
    }
}
