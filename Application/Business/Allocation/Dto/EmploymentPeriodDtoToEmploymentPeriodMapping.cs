﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class EmploymentPeriodDtoToEmploymentPeriodMapping : ClassMapping<EmploymentPeriodDto, EmploymentPeriod>
    {
        public EmploymentPeriodDtoToEmploymentPeriodMapping()
        {
            Mapping = d => new EmploymentPeriod
            {
                Id = d.Id,
                StartDate = d.StartDate,
                EndDate = d.EndDate,
                SignedOn = d.SignedOn,
                EmployeeId = d.EmployeeId,
                CompanyId = d.CompanyId,
                JobTitleId = d.JobTitleId,
                IsActive = d.IsActive,
                InactivityReason = d.InactivityReason,
                NoticePeriod = d.NoticePeriod,
                MondayHours = d.MondayHours,
                TuesdayHours = d.TuesdayHours,
                WednesdayHours = d.WednesdayHours,
                ThursdayHours = d.ThursdayHours,
                FridayHours = d.FridayHours,
                SaturdayHours = d.SaturdayHours,
                SundayHours = d.SundayHours,
                ContractTypeId = d.ContractTypeId,
                ContractDuration = d.ContractDuration,
                CalendarId = d.CalendarId,
                VacationHourToDayRatio = d.VacationHourToDayRatio,
                TimeReportingMode = d.TimeReportingMode,
                AbsenceOnlyDefaultProjectId = d.AbsenceOnlyDefaultProjectId,
                IsClockCardRequired = d.IsClockCardRequired,
                CompensationCurrencyId = d.CompensationCurrencyId,
                ContractorCompanyName = d.ContractorCompanyName,
                ContractorCompanyTaxIdentificationNumber = d.ContractorCompanyTaxIdentificationNumber,
                ContractorCompanyStreetAddress = d.ContractorCompanyStreetAddress,
                ContractorCompanyCity = d.ContractorCompanyCity,
                ContractorCompanyZipCode = d.ContractorCompanyZipCode,
                HourlyRate = d.HourlyRate,
                HourlyRateDomesticOutOfOfficeBonus = d.HourlyRateDomesticOutOfOfficeBonus,
                HourlyRateForeignOutOfOfficeBonus = d.HourlyRateForeignOutOfOfficeBonus,
                MonthlyRate = d.MonthlyRate,
                SalaryGross = d.SalaryGross,
                AuthorRemunerationRatio = d.AuthorRemunerationRatio,
                TerminatedOn = d.TerminatedOn,
                TerminationReasonId = d.TerminationReasonId,
                LocationId = d.LocationId,
                Timestamp = d.Timestamp,
            };
        }
    }
}
