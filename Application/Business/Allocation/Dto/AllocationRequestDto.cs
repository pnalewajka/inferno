﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class AllocationRequestDto : IWeeklyHours
    {
        public long Id { get; set; }

        public long EmployeeId { get; set; }

        public long[] EmployeeIds { get; set; }

        public long ProjectId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public AllocationRequestContentType ContentType { get; set; }

        public AllocationCertainty AllocationCertainty { get; set; }

        public decimal MondayHours { get; set; }

        public decimal TuesdayHours { get; set; }

        public decimal WednesdayHours { get; set; }

        public decimal ThursdayHours { get; set; }

        public decimal FridayHours { get; set; }

        public decimal SaturdayHours { get; set; }

        public decimal SundayHours { get; set; }

        public string EmployeeFirstName { get; set; }

        public string EmployeeLastName { get; set; }

        public string EmployeeAcronym { get; set; }

        public string ProjectName { get; set; } 

        public DateTime? NextRebuildTime { get; set; }

        public ProcessingStatus ProcessingStatus { get; set; }

        public string ProjectShortName { get; set; }

        public string ProjectColor { get; set; }

        public long? ProjectPictureId { get; set; }

        public string ClientShortName { get; set; }

        public string JiraIssueKey { get; set; }

        public string JiraKey { get; set; }

        public string ProjectAcronym { get; set; }

        public long? CalendarId { get; set; }

        public AllocationChangeDemand ChangeDemand { get; set; }

        public string Comment { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
