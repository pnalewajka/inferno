﻿using System;
using System.Linq;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Allocation.Dto
{
    public class EmployeeToBenchEmployeeDtoMapping : ClassMapping<Employee, BenchEmployeeDto>
    {
        public EmployeeToBenchEmployeeDtoMapping()
        {
            Mapping =
                e =>
                    new BenchEmployeeDto
                    {
                        Id = e.Id,
                        FirstName = e.FirstName,
                        LastName = e.LastName,
                        LocationId = e.LocationId,
                        LineManagerId = e.LineManagerId,
                        OrgUnitId = e.OrgUnitId,
                        ManagerId = e.OrgUnit.EmployeeId,
                        JobProfileIds = e.JobProfiles.Select(m => m.Id).ToArray(),
                        SkillIds = e.Resumes.SelectMany(r => r.ResumeTechnicalSkills).Select(r => r.TechnicalSkillId).ToArray(),
                        JobMatrixIds = e.JobMatrixs.Select(s => s.Id).ToArray(),
                        LastResumeModifiedOn = e.Resumes.Any()
                            ? e.Resumes.Max(r => r.ModifiedOn)
                            : default(DateTime?),
                        FillPercentage = e.Resumes.Select(r => r.FillPercentage).FirstOrDefault(),
                        OrgUnitFlatName = e.OrgUnit.FlatName,
                        VacationBalance = e.VacationBalances != null && e.VacationBalances.Any()
                            ? e.VacationBalances.OrderByDescending(x => x.EffectiveOn).ThenByDescending(x => x.Id).FirstOrDefault().TotalHourBalance 
                            : default(decimal)
                    };
        }
    }
}
