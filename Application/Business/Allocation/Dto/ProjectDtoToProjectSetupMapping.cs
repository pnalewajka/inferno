﻿using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class ProjectDtoToProjectSetupMapping : ClassMapping<ProjectDto, ProjectSetup>
    {
        private readonly IClassMappingFactory _classMappingFactory;

        public ProjectDtoToProjectSetupMapping(
            IClassMappingFactory mappingFactory)
        {
            _classMappingFactory = mappingFactory;

            Mapping = d => new ProjectSetup 
            {
                Id = d.SetupId,
                ProjectType = d.ProjectType,
                ProjectFeatures = d.ProjectFeatures,
                ProjectShortName = d.ProjectShortName,
                ClientShortName = d.ClientShortName,
                OrgUnitId = d.OrgUnitId,
                ProjectManagerId = d.ProjectManagerId,
                SalesAccountManagerId = d.SalesAccountManagerId,
                SupervisorManagerId = d.SupervisorManagerId,
                ProjectStatus = d.ProjectStatus,
                CalendarId = d.CalendarId,
                Color = d.Color,
                GenericName = d.GenericName,
                IsAvailableForSalesPresentation = d.IsAvailableForSalesPresentation,
                CurrentPhase = d.CurrentPhase,
                Disclosures = d.Disclosures,
                HasClientReferences = d.HasClientReferences,
                CustomerSizeId = d.CustomerSizeId,
                BusinessCase = d.BusinessCase,
                Region = d.Region,
                ReinvoicingToCustomer = d.ReinvoicingToCustomer,
                ReinvoiceCurrencyId = d.ReinvoiceCurrencyId,
                CostApprovalPolicy = d.CostApprovalPolicy,
                MaxCostsCurrencyId = d.MaxCostsCurrencyId,
                MaxHotelCosts = d.MaxHotelCosts,
                MaxTotalFlightsCosts = d.MaxTotalFlightsCosts,
                MaxOtherTravelCosts = d.MaxOtherTravelCosts,
                InvoicingAlgorithm = d.InvoicingAlgorithm,
                
                Industries = EntityHelper.CreateForeignEntities<Industry>(d.IndustryIds),
                Technologies = EntityHelper.CreateForeignEntities<Skill>(d.TechnologyIds),
                KeyTechnologies = EntityHelper.CreateForeignEntities<SkillTag>(d.KeyTechnologyIds),
                ServiceLines = EntityHelper.CreateForeignEntities<ServiceLine>(d.ServiceLineIds),
                SoftwareCategories = EntityHelper.CreateForeignEntities<SoftwareCategory>(d.SoftwareCategoryIds),
                Tags = EntityHelper.CreateForeignEntities<ProjectTag>(d.TagIds),
                Attachments = new List<ProjectAttachment>(),
                InvoicingRates = EntityHelper.CreateForeignEntities(d.InvoicingRates, _classMappingFactory.CreateMapping<ProjectInvoicingRateDto, ProjectInvoicingRate>())
            };
        }
    }
}
