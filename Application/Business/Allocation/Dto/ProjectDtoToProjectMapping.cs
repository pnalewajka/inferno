﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class ProjectDtoToProjectMapping : ClassMapping<ProjectDto, Project>
    {
        public ProjectDtoToProjectMapping(
            IClassMappingFactory mappingFactory)
        {
            var subProjectMapping = mappingFactory.CreateMapping<SubProjectDto, Project>();

            Mapping = d => subProjectMapping.CreateFromSource(d);
        }
    }
}
