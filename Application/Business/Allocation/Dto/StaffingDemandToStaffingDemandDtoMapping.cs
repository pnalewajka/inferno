﻿using System.Linq;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Allocation.Dto
{
    public class StaffingDemandToStaffingDemandDtoMapping : ClassMapping<StaffingDemand, StaffingDemandDto>
    {
        public StaffingDemandToStaffingDemandDtoMapping()
        {
            Mapping = e => new StaffingDemandDto
            {
                StartDate = e.StartDate,
                EndDate = e.EndDate,
                Description = e.Description,
                ProjectId = e.ProjectId,
                EmployeesNumber = e.EmployeesNumber,
                JobMatrixLevelId = e.JobMatrixLevelId,
                JobProfileId = e.JobProfileId,
                SkillsIds = e.Skills.Select(s => s.Id).ToArray(),
                Id = e.Id,
                Timestamp = e.Timestamp
            };
        }
    }
}
