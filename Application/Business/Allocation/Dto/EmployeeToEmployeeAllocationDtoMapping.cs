﻿using System.Linq;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Allocation.Dto
{
    public class EmployeeToEmployeeAllocationDtoMapping : ClassMapping<Employee, EmployeeAllocationDto>
    {
        public EmployeeToEmployeeAllocationDtoMapping()
        {
            Mapping = e => new EmployeeAllocationDto
            {
                EmployeeId = e.Id,
                FirstName = e.FirstName,
                LastName = e.LastName,
                FullName = e.FullName,
                Email = e.Email,
                Id = e.Id,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp,
                UserId = e.UserId,
                LocationId = e.LocationId,
                LocationName = e.Location.Name,
                JobProfileIds = e.JobProfiles.Select(m => m.Id).ToArray(),
                JobProfileNames = e.JobProfiles.Select(m => m.Name).ToArray(),
                SkillIds = e.Resumes.SelectMany(r => r.ResumeTechnicalSkills).Select(r => r.TechnicalSkillId).ToArray(),
                JobMatrixIds = e.JobMatrixs.Select(s => s.Id).ToArray(),
                JobMatrixLevels = e.JobMatrixs.Select(s => s.Code).ToArray(),
                OrgUnitId = e.OrgUnitId,
                OrgUnitName = e.OrgUnit.Name,
                LineManagerId = e.LineManagerId,
                LineManagerName = e.LineManager != null ? e.LineManager.DisplayName : null
            };
        }
    }
}
