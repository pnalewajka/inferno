﻿using System.Collections.ObjectModel;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class ActiveDirectoryGroupDtoToActiveDirectoryGroupMapping : ClassMapping<ActiveDirectoryGroupDto, ActiveDirectoryGroup>
    {
        public ActiveDirectoryGroupDtoToActiveDirectoryGroupMapping()
        {
            Mapping = d => new ActiveDirectoryGroup
            {
                Id = d.Id,
                ActiveDirectoryId = d.ActiveDirectoryId,
                Name = d.Name,
                GroupType = d.GroupType,
                Email = d.Email,
                Description = d.Description,
                Info = d.Info,
                Aliases = d.Aliases,
                Users = d.UsersIds == null ? null : new Collection<User>(d.UsersIds.Select(v => new User { Id = v }).ToList()),
                Managers = d.ManagersIds == null ? null : new Collection<User>(d.ManagersIds.Select(v => new User { Id = v }).ToList()),
                Subgroups = d.SubgroupIds == null ? null : new Collection<ActiveDirectoryGroup>(d.SubgroupIds.Select(id => new ActiveDirectoryGroup { Id = id }).ToList()),
                OwnerId = d.OwnerId,
                Timestamp = d.Timestamp,
            };
        }
    }
}
