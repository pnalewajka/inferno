﻿using System;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class EmployeeAllocationPeriodDto
    {
        public DateTime Week { get; set; }

        public decimal AllocatedFTE { get; set; }

        public decimal PlannedFTE { get; set; }

        public decimal AvailableFTE { get; set; }

        public decimal NotAllocatedFTE => AvailableFTE - AllocatedFTE;

        public decimal NotAllocatedPercentage => AvailableFTE == 0 ? 0 : (NotAllocatedFTE / AvailableFTE) * 100;
    }
}