﻿using System;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class AllocationChartSearchDto
    {
        public long[] OrgUnitIds { get; set; }

        public long[] CountryIds { get; set; }

        public long[] LocationIds { get; set; }

        public DateTime? DateFrom { get; set; }

        public DateTime? DateTo { get; set; }
    }
}
