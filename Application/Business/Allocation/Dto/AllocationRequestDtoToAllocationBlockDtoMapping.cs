﻿using System;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using static Smt.Atomic.Business.Allocation.Dto.EmployeeAllocationDto;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class AllocationRequestDtoToAllocationBlockDtoMapping : ClassMapping<AllocationRequestDto, AllocationBlockDto>
    {
        public AllocationRequestDtoToAllocationBlockDtoMapping()
        {
            Mapping = request => new AllocationBlockDto
            {
                RequestId = request.Id,
                StartDate = request.StartDate,
                EndDate = request.EndDate,
                Certainty = request.AllocationCertainty,
                AllocationName = request.ProjectName,
                AllocationDayHours = new[] { request.MondayHours, request.TuesdayHours, request.WednesdayHours, request.ThursdayHours, request.FridayHours, request.SaturdayHours, request.SundayHours },
                ProjectId = request.ProjectId,
                ProjectName = request.ProjectName,
                ProjectShortName = request.ProjectShortName,
                ProjectColor = request.ProjectColor,
                ProjectAcronym = request.ProjectAcronym,
                ProjectColorIsDark = string.IsNullOrEmpty(request.ProjectColor) ? false : ColorHelper.IsDark(request.ProjectColor, 0.5f),
                ProjectPictureId = request.ProjectPictureId,
                ClientShortName = request.ClientShortName,
                JiraIssueKey = request.JiraIssueKey,
                JiraKey = request.JiraKey,
                EmployeeId = request.EmployeeId,
                EmployeeAcronym = request.EmployeeAcronym,
                EmployeeName = $"{request.EmployeeFirstName} {request.EmployeeLastName}",
                ProcessingStatus = request.ProcessingStatus,
                ContentType = request.ContentType,
            };
        }
    }
}
