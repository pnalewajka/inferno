﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class ProjectPresentationDto
    {
        public string Name { get; set; }

        public string ClientShortName { get; set; }

        public string ProjectShortName { get; set; }

        public string PetsCode { get; set; }

        public string JiraCode { get; set; }

        public string JiraId { get; set; }

        public ProjectFeatures ProjectFeatures { get; set; }

        public string Description { get; set; }

        public string ProjectManager { get; set; }

        public string OrgUnits { get; set; }

        public string Industries { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public ProjectStatus ProjectStatus { get; set; }

        public string GenericName { get; set; }

        public string KeyTechnologies { get; set; }

        public string Technologies { get; set; }

        public string ServiceLines { get; set; }

        public string SoftwareCategories { get; set; }

        public string Tags { get; set; }

        public byte[] Picture { get; set; }

        public ProjectPhase CurrentPhase { get; set; }

        public ProjectDisclosures Disclosures { get; set; }

        public bool HasClientReferences { get; set; }

        public string CustomerSize { get; set; }

        public long Id { get; set; }

        public long? OrgUnitId { get; set; }

        public long? ProjectManagerId { get; set; }

        public long? StaffingDemandId { get; set; }

        public long? CalendarId { get; set; }

        public string Color { get; set; }

        public byte[] Timestamp { get; set; }

        public long[] IndustryIds { get; set; }

        public bool IsAvailableForSalesPresentation { get; set; }

        public long[] TechnologyIds { get; set; }

        public long[] KeyTechnologyIds { get; set; }

        public long[] TagIds { get; set; }

        public long[] SoftwareCategoryIds { get; set; }

        public long? CustomerSizeId { get; set; }

        public string BusinessCase { get; set; }
    }
}
