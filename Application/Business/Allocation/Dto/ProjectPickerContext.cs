﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class ProjectPickerContext
    {
        public ProjectStatus? ProjectStatus { get; set; }

        public bool OnlyActiveProjects { get; set; }

        public bool OnlyProjectsWithKey { get; set; }

        public bool OnlyMainProjects { get; set; }

        public bool OnlySubProjects { get; set; }

        public BonusTypeSet ProjectAllowedBonuses { get; set; }
    }
}
