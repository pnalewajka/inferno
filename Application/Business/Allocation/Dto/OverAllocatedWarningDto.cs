﻿using System;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class OverAllocatedWarningDto
    {
        public EmployeeDto Employee { get; set; }

        public long? ProjectId { get; set; }

        public DateTime? Period { get; set; }
    }
}
