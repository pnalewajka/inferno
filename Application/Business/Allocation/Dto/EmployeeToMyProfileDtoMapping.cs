﻿using System;
using System.Linq;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class EmployeeToMyProfileDtoMapping : ClassMapping<Employee, MyProfileDto>
    {
        public EmployeeToMyProfileDtoMapping(
            IClassMapping<Employee, EmployeeDto> employeeToEmployeeDtoMap,
            IClassMapping<Company, CompanyDto> companyToCompanyDtoMap,
            IClassMapping<User, UserDto> userToUserDtoMap)
        {
            Mapping = e => new MyProfileDto
            {
                Id = e.Id,
                Timestamp = e.Timestamp,
                FirstName = e.FirstName,
                LastName = e.LastName,
                Email = e.Email,
                JobTitle = e.JobTitle == null 
                    ? new LocalizedString() 
                    : new LocalizedString
                    {
                        English = e.JobTitle.NameEn,
                        Polish = e.JobTitle.NamePl
                    },
                LocationName = e.Location != null ? e.Location.Name : "",
                CurrentEmployeeStatus = e.CurrentEmployeeStatus,
                IsProjectContributor = e.IsProjectContributor,
                IsCoreAsset = e.IsCoreAsset,
                JobProfileNames = e.JobProfiles!=null ? string.Join(", ", e.JobProfiles.Select(p => p.Name)) : "",
                SkillNames = e.Resumes != null ? string.Join(", ", e.Resumes.SelectMany(r => r.ResumeTechnicalSkills).Select(p => p.TechnicalSkill.Name)) : "",
                Company = e.Company != null ? companyToCompanyDtoMap.CreateFromSource(e.Company) : null,
                User = e.User != null ? userToUserDtoMap.CreateFromSource(e.User) : null,
                LineManager = e.LineManager != null ? employeeToEmployeeDtoMap.CreateFromSource(e.LineManager) : null,
                OrgUnitName = e.OrgUnit != null ? e.OrgUnit.Name : "",
                IsInOrgChart = e.OrgUnit != null && !"UNASSIGNED".Equals(e.OrgUnit.Code, StringComparison.OrdinalIgnoreCase),
                OrgUnitId = e.OrgUnitId,
                UserId = e.UserId
            };
        }
    }
}
