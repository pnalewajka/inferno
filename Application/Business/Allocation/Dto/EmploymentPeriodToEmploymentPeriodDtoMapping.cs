﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class EmploymentPeriodToEmploymentPeriodDtoMapping : ClassMapping<EmploymentPeriod, EmploymentPeriodDto>
    {
        public EmploymentPeriodToEmploymentPeriodDtoMapping()
        {
            Mapping = e => new EmploymentPeriodDto
            {
                Id = e.Id,
                StartDate = e.StartDate,
                EndDate = e.EndDate,
                SignedOn = e.SignedOn,
                EmployeeId = e.EmployeeId,
                EmploymentType = e.ContractType.EmploymentType,
                CompanyId = e.CompanyId,
                CompanyName = e.Company == null ? "" : e.Company.Name,
                JobTitleId = e.JobTitleId,
                IsActive = e.IsActive,
                InactivityReason = e.InactivityReason,
                TerminationReasonText = e.TerminationReason == null ? null : new LocalizedString
                {
                    English = e.TerminationReason.NameEn,
                    Polish = e.TerminationReason.NamePl
                },
                NoticePeriod = e.NoticePeriod,
                MondayHours = e.MondayHours,
                TuesdayHours = e.TuesdayHours,
                WednesdayHours = e.WednesdayHours,
                ThursdayHours = e.ThursdayHours,
                FridayHours = e.FridayHours,
                SaturdayHours = e.SaturdayHours,
                SundayHours = e.SundayHours,
                ContractTypeId = e.ContractTypeId,
                ContractTypeName = e.ContractType == null ? null : new LocalizedString
                {
                    English = e.ContractType.NameEn,
                    Polish = e.ContractType.NamePl
                },
                ContractDuration = e.ContractDuration,
                CalendarId = e.CalendarId,
                VacationHourToDayRatio = e.VacationHourToDayRatio,
                TimeReportingMode = e.TimeReportingMode,
                AbsenceOnlyDefaultProjectId = e.AbsenceOnlyDefaultProjectId,
                AbsenceOnlyDefaultProjectShortName = e.AbsenceOnlyDefaultProject != null ? ProjectBusinessLogic.ProjectName.Call(e.AbsenceOnlyDefaultProject) : "",
                IsClockCardRequired = e.IsClockCardRequired,
                CompensationCurrencyId = e.CompensationCurrencyId,
                ContractorCompanyName = e.ContractorCompanyName,
                ContractorCompanyTaxIdentificationNumber = e.ContractorCompanyTaxIdentificationNumber,
                ContractorCompanyStreetAddress = e.ContractorCompanyStreetAddress,
                ContractorCompanyCity = e.ContractorCompanyCity,
                ContractorCompanyZipCode = e.ContractorCompanyZipCode,
                HourlyRate = e.HourlyRate,
                HourlyRateDomesticOutOfOfficeBonus = e.HourlyRateDomesticOutOfOfficeBonus,
                HourlyRateForeignOutOfOfficeBonus = e.HourlyRateForeignOutOfOfficeBonus,
                MonthlyRate = e.MonthlyRate,
                SalaryGross = e.SalaryGross,
                AuthorRemunerationRatio = e.AuthorRemunerationRatio,
                TerminatedOn = e.TerminatedOn,
                TerminationReasonId = e.TerminationReasonId,
                IsTaxDeductibleCostEnabled = e.ContractType != null ? e.ContractType.IsTaxDeductibleCostEnabled : false,
                LocationId = e.LocationId,
                Timestamp = e.Timestamp,
            };
        }
    }
}
