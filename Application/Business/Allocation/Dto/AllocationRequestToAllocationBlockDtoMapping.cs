﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using static Smt.Atomic.Business.Allocation.Dto.EmployeeAllocationDto;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class AllocationRequestToAllocationBlockDtoMapping : ClassMapping<AllocationRequest, AllocationBlockDto>
    {
        public AllocationRequestToAllocationBlockDtoMapping(IClassMappingFactory classMappingFactory)
        {
            var entityToDto = classMappingFactory.CreateMapping<AllocationRequest, AllocationRequestDto>();
            var dtoToDto = classMappingFactory.CreateMapping<AllocationRequestDto, AllocationBlockDto>();

            Mapping = e => dtoToDto.CreateFromSource(entityToDto.CreateFromSource(e));
        }
    }
}
