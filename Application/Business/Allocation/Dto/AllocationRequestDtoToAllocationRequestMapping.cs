﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class AllocationRequestDtoToAllocationRequestMapping : ClassMapping<AllocationRequestDto, AllocationRequest>
    {
        public AllocationRequestDtoToAllocationRequestMapping()
        {
            Mapping = d => new AllocationRequest
            {
                Id = d.Id,
                EmployeeId = d.EmployeeId,
                ProjectId = d.ProjectId,
                StartDate = d.StartDate,
                EndDate = d.EndDate,
                ContentType = d.ContentType,
                AllocationCertainty = d.AllocationCertainty,
                MondayHours = d.MondayHours,
                TuesdayHours = d.TuesdayHours,
                WednesdayHours = d.WednesdayHours,
                ThursdayHours = d.ThursdayHours,
                FridayHours = d.FridayHours,
                SaturdayHours = d.SaturdayHours,
                SundayHours = d.SundayHours,
                ChangeDemand = d.ChangeDemand,
                Comment = d.Comment,
                Timestamp = d.Timestamp,
            };
        }
    }
}
