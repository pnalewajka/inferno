﻿using System;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class EmployeeCalendarInfoDto
    {
        public long CalendarId { get; set; }
        public long EmployeeId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
