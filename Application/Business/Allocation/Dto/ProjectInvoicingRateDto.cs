﻿using System;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class ProjectInvoicingRateDto
    {
        public string Code { get; set; }

        public decimal Value { get; set; }

        public string Description { get; set; }
    }
}
