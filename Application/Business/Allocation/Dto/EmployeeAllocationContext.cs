﻿using Smt.Atomic.Business.Allocation.Enums;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class EmployeeAllocationContext
    {
        public EmployeeAllocationContext()
        {
            PeriodType = EmployeeAllocationPeriodType.Week;
        }

        public EmployeeAllocationPeriodType PeriodType { get; set; }

        public bool Beta { get; set; }
    }
}
