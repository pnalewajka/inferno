﻿using System;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class StaffingDemandDto
    {
        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Description { get; set; }

        public decimal EmployeesNumber { get; set; }

        public long ProjectId { get; set; }

        public long? JobMatrixLevelId { get; set; }

        public long[] SkillsIds { get; set; }

        public long? JobProfileId { get; set; }

        public long Id { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
