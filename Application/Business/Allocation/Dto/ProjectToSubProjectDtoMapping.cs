﻿using System.Linq;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class ProjectToSubProjectDtoMapping
        : ClassMapping<Project, SubProjectDto>
    {
        public ProjectToSubProjectDtoMapping(
            IPrincipalProvider principalProvider,
            IOrgUnitService orgUnitService)
        {
            Mapping = e => new SubProjectDto
            {
                Id = e.Id,
                SetupId = e.ProjectSetupId,
                IsMainProject = e.IsMainProject,
                ProjectShortName = e.SubProjectShortName,
                Description = e.Description,
                KupferwerkProjectId = e.KupferwerkProjectId,
                JiraIssueKey = e.JiraIssueKey,
                JiraKey = e.JiraKey,
                PetsCode = e.PetsCode,
                Apn = e.Apn,
                StartDate = e.StartDate,
                EndDate = e.EndDate,
                TimeTrackingImportSourceId = e.TimeTrackingImportSourceId,
                AllowedOvertimeTypes = e.AllowedOvertimeTypes,
                TimeReportAcceptingPersonIds = e.TimeReportAcceptingPersons == null ? null : e.TimeReportAcceptingPersons.Select(p => p.Id),
                IsApprovedByRequesterMainManager = e.IsApprovedByRequesterMainManager,
                IsApprovedByMainManagerForProjectManager = e.IsApprovedByMainManagerForProjectManager,
                TimeTrackingType = e.TimeTrackingType,
                JiraIssueToTimeReportRowMapperId = e.JiraIssueToTimeReportRowMapperId,
                UtilizationCategoryId = e.UtilizationCategoryId,
                AttributesIds = e.Attributes == null ? null : e.Attributes.Select(a => a.Id),
                ReportingStartsOn = e.ReportingStartsOn,
                Acronym = e.Acronym,
                IsOwnProject = ProjectBusinessLogic.IsOwnProjectForEmployee.Call(
                    e,
                    principalProvider.Current.EmployeeId,
                    orgUnitService.GetManagerOrgUnitDescendantIds(principalProvider.Current.EmployeeId)),
                ClientProjectName = ProjectBusinessLogic.ClientProjectName.Call(e),
                ProjectName = ProjectBusinessLogic.ProjectName.Call(e),
                AllowedBonusTypes = e.AllowedBonusTypes,
            };
        }
    }
}
