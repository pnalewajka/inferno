﻿namespace Smt.Atomic.Business.Allocation.Dto
{
    public class ProjectReportParametersDto
    {
        public long[] ProjectIds { get; set; }
    }
}
