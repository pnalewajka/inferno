﻿using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class ActiveDirectoryGroupVisualizationDto
    {
        public class ActiveDirectoryGroupUserDto
        {
            public long Id { get; set; }

            public long EmployeeId { get; set; }

            public string FullName { get; set; }
        }

        public long Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public ActiveDirectoryGroupType GroupType { get; set; }

        public ActiveDirectoryGroupUserDto Owner { get; set; }
        
        public ICollection<ActiveDirectoryGroupUserDto> Managers { get; set; }

        public ICollection<ActiveDirectoryGroupUserDto> Members { get; set; }

        public ICollection<ActiveDirectoryGroupVisualizationDto> Subgroups { get; set; }
    }
}
