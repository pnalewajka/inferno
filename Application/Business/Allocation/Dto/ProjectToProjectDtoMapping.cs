using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class ProjectToProjectDtoMapping : ClassMapping<Project, ProjectDto>
    {
        public ProjectToProjectDtoMapping(
            IClassMappingFactory mappingFactory, 
            IPrincipalProvider principalProvider,
            IOrgUnitService orgUnitService)
        {
            Mapping = e => new ProjectDto
            {
                Id = e.Id,
                SetupId = e.ProjectSetupId,
                IsMainProject = e.IsMainProject,
                ClientProjectName = ProjectBusinessLogic.ClientProjectName.Call(e),
                JiraIssueKey = e.JiraIssueKey,
                JiraKey = e.JiraKey,
                PetsCode = e.PetsCode,
                Apn = e.Apn,
                Region = e.ProjectSetup.Region,
                KupferwerkProjectId = e.KupferwerkProjectId,
                ProjectType = e.ProjectSetup.ProjectType,
                ProjectFeatures = e.ProjectSetup.ProjectFeatures,
                Description = e.Description,
                ClientShortName = e.ProjectSetup.ClientShortName,
                ProjectShortName = e.ProjectSetup.ProjectShortName, // ISC: temporary
                ProjectName = ProjectBusinessLogic.ProjectName.Call(e),
                OrgUnitId = e.ProjectSetup.OrgUnitId,
                OrgUnitName = e.ProjectSetup.OrgUnit == null ? string.Empty : e.ProjectSetup.OrgUnit.Name,
                ProjectManagerId = e.ProjectSetup.ProjectManagerId,
                SalesAccountManagerId = e.ProjectSetup.SalesAccountManagerId,
                SupervisorManagerId = e.ProjectSetup.SupervisorManagerId,
                SupervisorFullName = e.ProjectSetup.SupervisorManager == null ? string.Empty : e.ProjectSetup.SupervisorManager.FullName,
                ProjectManagerFullName = e.ProjectSetup.ProjectManager == null ? string.Empty : e.ProjectSetup.ProjectManager.FullName,
                StartDate = e.StartDate,
                EndDate = e.EndDate,
                ProjectStatus = e.ProjectSetup.ProjectStatus,
                CalendarId = e.ProjectSetup.CalendarId,
                Color = e.ProjectSetup.Color,
                GenericName = e.ProjectSetup.GenericName,
                Timestamp = e.Timestamp,
                IndustryIds = e.ProjectSetup.Industries == null ? null : e.ProjectSetup.Industries.Select(i => i.Id),
                IsAvailableForSalesPresentation = e.ProjectSetup.IsAvailableForSalesPresentation,
                TechnologyIds = e.ProjectSetup.Technologies == null ? null : e.ProjectSetup.Technologies.Select(m => m.Id),
                KeyTechnologyIds = e.ProjectSetup.KeyTechnologies == null ? null : e.ProjectSetup.KeyTechnologies.Select(m => m.Id),
                SoftwareCategoryIds = e.ProjectSetup.SoftwareCategories == null ? null : e.ProjectSetup.SoftwareCategories.Select(m => m.Id),
                TagIds = e.ProjectSetup.Tags == null ? null : e.ProjectSetup.Tags.Select(m => m.Id),
                Picture = e.ProjectSetup.Picture == null
                    ? null
                    : new DocumentDto
                    {
                        ContentType = e.ProjectSetup.Picture.ContentType,
                        DocumentName = e.ProjectSetup.Picture.Name,
                        DocumentId = e.ProjectSetup.Picture.Id
                    },
                ServiceLineIds = e.ProjectSetup.ServiceLines == null ? null : e.ProjectSetup.ServiceLines.Select(s => s.Id),
                CurrentPhase = e.ProjectSetup.CurrentPhase,
                Disclosures = e.ProjectSetup.Disclosures,
                HasClientReferences = e.ProjectSetup.HasClientReferences,
                CustomerSizeId = e.ProjectSetup.CustomerSizeId,
                BusinessCase = e.ProjectSetup.BusinessCase,
                ReferenceMaterialAttachments = e.ProjectSetup.Attachments == null ? null : e.ProjectSetup.Attachments
                    .Where(x => x.AttachmentType == ProjectAttachmentType.ReferenceMaterial)
                    .Select(p => new DocumentDto
                    {
                        ContentType = p.ContentType,
                        DocumentName = p.Name,
                        DocumentId = p.Id
                    }).ToList(),
                ProjectInformationAttachments = e.ProjectSetup.Attachments == null ? null : e.ProjectSetup.Attachments
                    .Where(x => x.AttachmentType == ProjectAttachmentType.ProjectInformation)
                    .Select(p => new DocumentDto
                    {
                        ContentType = p.ContentType,
                        DocumentName = p.Name,
                        DocumentId = p.Id
                    }).ToList(),
                OtherAttachments = e.ProjectSetup.Attachments == null ? null : e.ProjectSetup.Attachments
                    .Where(x => x.AttachmentType == ProjectAttachmentType.Other)
                    .Select(p => new DocumentDto
                    {
                        ContentType = p.ContentType,
                        DocumentName = p.Name,
                        DocumentId = p.Id
                    }).ToList(),
                TimeTrackingImportSourceId = e.TimeTrackingImportSourceId,
                AllowedOvertimeTypes = e.AllowedOvertimeTypes,
                TimeReportAcceptingPersonIds = e.TimeReportAcceptingPersons == null ? null : e.TimeReportAcceptingPersons.Select(p => p.Id),
                IsApprovedByRequesterMainManager = e.IsApprovedByRequesterMainManager,
                IsApprovedByMainManagerForProjectManager = e.IsApprovedByMainManagerForProjectManager,
                TimeTrackingType = e.TimeTrackingType,
                JiraIssueToTimeReportRowMapperId = e.JiraIssueToTimeReportRowMapperId,
                UtilizationCategoryId = e.UtilizationCategoryId,
                AttributesIds = e.Attributes == null ? null : e.Attributes.Select(a => a.Id),
                ReportingStartsOn = e.ReportingStartsOn,
                ReinvoicingToCustomer = e.ProjectSetup.ReinvoicingToCustomer,
                ReinvoiceCurrencyId = e.ProjectSetup.ReinvoiceCurrencyId,
                CostApprovalPolicy = e.ProjectSetup.CostApprovalPolicy,
                MaxCostsCurrencyId = e.ProjectSetup.MaxCostsCurrencyId,
                MaxHotelCosts = e.ProjectSetup.MaxHotelCosts,
                MaxTotalFlightsCosts = e.ProjectSetup.MaxTotalFlightsCosts,
                MaxOtherTravelCosts = e.ProjectSetup.MaxOtherTravelCosts,
                IsOwnProject = ProjectBusinessLogic.IsOwnProjectForEmployee.Call(
                    e,
                    principalProvider.Current.EmployeeId,
                    orgUnitService.GetManagerOrgUnitDescendantIds(principalProvider.Current.EmployeeId)),
                InvoicingRates = e.ProjectSetup.InvoicingRates == null ? null : e.ProjectSetup.InvoicingRates.Select(
                    mappingFactory.CreateMapping<ProjectInvoicingRate, ProjectInvoicingRateDto>().CreateFromSource).ToArray(),
                InvoicingAlgorithm = e.ProjectSetup.InvoicingAlgorithm,
                Acronym = e.Acronym,
                AllowedBonusTypes = e.AllowedBonusTypes,
            };
        }
    }
}
