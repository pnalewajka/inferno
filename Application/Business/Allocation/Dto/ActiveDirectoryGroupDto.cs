﻿using System;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class ActiveDirectoryGroupDto
    {
        public long Id { get; set; }

        public Guid ActiveDirectoryId { get; set; }

        public string Name { get; set; }

        public ActiveDirectoryGroupType GroupType { get; set; }

        public string Email { get; set; }

        public string Description { get; set; }

        public string Info { get; set; }

        public string Aliases { get; set; }

        public ICollection<Guid> ActiveDirectoryUserIds { get; set; }

        public ICollection<Guid> ActiveDirectoryManagerIds { get; set; }

        public ICollection<Guid> ActiveDirectorySubgroupIds { get; set; }

        public Guid? ActiveDirectoryOwnerId { get; set; }

        public string InternalDistinguishedName { get; set; }

        public long[] UsersIds { get; set; }

        public long[] ManagersIds { get; set; }

        public long? OwnerId { get; set; }

        public long[] SubgroupIds { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
