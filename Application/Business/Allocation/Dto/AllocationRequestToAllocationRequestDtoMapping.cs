﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class AllocationRequestToAllocationRequestDtoMapping : ClassMapping<AllocationRequest, AllocationRequestDto>
    {
        public AllocationRequestToAllocationRequestDtoMapping()
        {
            Mapping = e => new AllocationRequestDto
            {
                Id = e.Id,
                EmployeeId = e.EmployeeId,
                ProjectId = e.ProjectId,
                ProjectShortName = ProjectBusinessLogic.ProjectName.Call(e.Project),
                JiraIssueKey = e.Project.JiraIssueKey,
                JiraKey = e.Project.JiraKey,
                ClientShortName = e.Project.ProjectSetup.ClientShortName,
                ProjectColor = e.Project.ProjectSetup.Color,
                ProjectAcronym = e.Project.Acronym,
                ProjectPictureId = e.Project.ProjectSetup.PictureId,
                EmployeeAcronym = e.Employee.Acronym,
                StartDate = e.StartDate,
                EndDate = e.EndDate,
                ContentType = e.ContentType,
                AllocationCertainty = e.AllocationCertainty,
                MondayHours = e.MondayHours,
                TuesdayHours = e.TuesdayHours,
                WednesdayHours = e.WednesdayHours,
                ThursdayHours = e.ThursdayHours,
                FridayHours = e.FridayHours,
                SaturdayHours = e.SaturdayHours,
                SundayHours = e.SundayHours,
                EmployeeFirstName = e.Employee.FirstName,
                EmployeeLastName = e.Employee.LastName,
                ProjectName = ProjectBusinessLogic.ClientProjectName.Call(e.Project),
                NextRebuildTime = e.AllocationRequestMetadata.NextRebuildTime,
                ProcessingStatus = e.AllocationRequestMetadata.ProcessingStatus,
                CalendarId = e.Project.ProjectSetup.CalendarId ?? e.Employee.OrgUnit.CalendarId,
                Comment = e.Comment,
                ChangeDemand = e.ChangeDemand,
                Timestamp = e.Timestamp,
            };
        }
    }
}
