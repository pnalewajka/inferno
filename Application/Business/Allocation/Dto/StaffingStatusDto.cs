﻿using System;
using System.Globalization;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class StaffingStatusDto
    {
        public string AllocationText { get; set; }

        public string ProjectName { get; set; }
        
        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public decimal AllocationUsage { get; set; }

        public override string ToString()
        {
            if (EndDate.HasValue)
            {
                return string.Format(AllocationText, ProjectName, StartDate.ToShortDateString(), EndDate.Value.ToShortDateString(), AllocationUsage.ToString("0.",  CultureInfo.InvariantCulture));
            }

            return string.Format(AllocationText, ProjectName, StartDate.ToShortDateString(), AllocationUsage.ToInvariantString("0."));
        }
    }
}
