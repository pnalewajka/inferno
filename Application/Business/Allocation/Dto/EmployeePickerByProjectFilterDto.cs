﻿using System;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class EmployeePickerByProjectFilterDto
    {
        public long ProjectId { get; set; }

        public long[] EmployeeIds { get; set; }

        public DateTime Date { get; set; }
    }
}             