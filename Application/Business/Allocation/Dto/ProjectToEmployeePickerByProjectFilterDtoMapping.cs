﻿﻿using System;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class ProjectToEmployeePickerByProjectFilterDtoMapping : ClassMapping<Project, EmployeePickerByProjectFilterDto>
    {
        public ProjectToEmployeePickerByProjectFilterDtoMapping()
        {
            Mapping = e => new EmployeePickerByProjectFilterDto
            {
                ProjectId = e.Id,
                EmployeeIds = (e.AllocationRequests != null && e.AllocationRequests.Any()) ? e.AllocationRequests.Select(a => a.EmployeeId).ToArray() : new long[0],
                Date = e.StartDate.Value
            };
        }
    }
}