﻿using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Allocation.Dto
{
    public class ProjectTagDtoToProjectTagMapping : ClassMapping<ProjectTagDto, ProjectTag>
    {
        public ProjectTagDtoToProjectTagMapping()
        {
            Mapping = d => new ProjectTag
            {
                Id = d.Id,
                Name = d.Name,
                Description = d.Description,
                TagType = d.TagType,
                Timestamp = d.Timestamp
            };
        }
    }
}
