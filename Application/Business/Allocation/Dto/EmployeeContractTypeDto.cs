﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class EmployeeContractTypeDto
    {
        public long EmployeeId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Acronym { get; set; }

        public long? CompanyId { get; set; }

        public long? LocationId { get; set; }

        public long? LineManagerId { get; set; }

        public ContractDurationType? ContractDuration { get; set; }

        public EmployeeStatus CurrentEmployeeStatus { get; set; }

        public long? ContractTypeId { get; set; }

        public EmploymentPeriodDto CurrentEmployeeEmploymentPeriod { get; set; }

        public EmploymentPeriodDto LastEmployeeEmploymentPeriod { get; set; }

        public EmploymentPeriodDto[] EmployeeEmploymentPeriods { get; set; }

        public decimal EmployeeHourBalance { get; set; }

        public LocalizedString LeavingReasonText { get; set; }

        public string JobMatrixLevel { get; set; }
    }
}
