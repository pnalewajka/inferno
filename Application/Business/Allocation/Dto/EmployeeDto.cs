﻿using Smt.Atomic.CrossCutting.Business.Enums;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Models;
using System;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class EmployeeDto
    {
        public long Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Acronym { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string PersonalNumber { get; set; }

        public PersonalNumberType? PersonalNumberType { get; set; }

        public string IdCardNumber { get; set; }

        public string PhoneNumber { get; set; }

        public string SkypeName { get; set; }

        public string HomeAddress { get; set; }

        public string AvailabilityDetails { get; set; }

        public long? LocationId { get; set; }
        
        public PlaceOfWork? PlaceOfWork { get; set; }

        public long? JobTitleId { get; set; }

        public decimal MinimumOvertimeBankBalance { get; set; }

        public EmployeeStatus CurrentEmployeeStatus { get; set; }

        public bool IsProjectContributor { get; set; }

        public bool IsCoreAsset { get; set; }

        public byte[] Timestamp { get; set; }

        public ICollection<long> JobProfileIds { get; set; }

        public ICollection<long> SkillIds { get; set; }

        public ICollection<long> JobMatrixIds { get; set; }

        public long? CompanyId { get; set; }

        public long? UserId { get; set; }

        public long? LineManagerId { get; set; }

        public long OrgUnitId { get; set; }

        public ICollection<string> JobProfileNames { get; set; }

        public ICollection<string> SkillNames { get; set; }

        public string OrgUnitName { get; set; }

        public string LocationName { get; set; }

        public string CompanyName { get; set; }

        public string JobMatrixName { get; set; }

        public LocalizedString JobTitleName { get; set; }

        public string LineManagerFullName { get; set; }

        public string FullName => $"{LastName} {FirstName}".Trim();

        public string Login { get; set; }

        public DateTime? ResumeModificationDate { get; set; }

        public long? LastResumeId { get; set; }

        public ICollection<int> FillPercentage { get; set; }

        public EmployeeFeatures EmployeeFeatures { get; set; }

        public bool IsOwnEmployee { get; set; }
    }
}
