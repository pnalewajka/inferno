﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using System.Linq;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class ProjectClientDtoToProjectMapping : ClassMapping<ProjectClientDto, Project>
    {
        public ProjectClientDtoToProjectMapping()
        {
            Mapping = d => new Project
            {
                Id = d.ProjectId,
            };
        }
    }
}

