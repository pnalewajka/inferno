﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class ProjectInvoicingRateDtoToProjectInvoicingRateMapping : ClassMapping<ProjectInvoicingRateDto, ProjectInvoicingRate>
    {
        public ProjectInvoicingRateDtoToProjectInvoicingRateMapping()
        {
            Mapping = d => new ProjectInvoicingRate
            {
                Code = d.Code,
                Value = d.Value,
                Description = d.Description
            };
        }
    }
}
