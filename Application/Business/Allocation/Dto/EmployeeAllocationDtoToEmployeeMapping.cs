﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Resumes;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class EmployeeAllocationDtoToEmployeeMapping : ClassMapping<EmployeeAllocationDto, Employee>
    {
        public EmployeeAllocationDtoToEmployeeMapping()
        {
            Mapping = d => new Employee
            {
                FirstName = d.FirstName,
                LastName = d.LastName,
                Email = d.Email,
                Id = d.Id,
                Timestamp = d.Timestamp,
                LocationId = d.LocationId,
                UserId = d.UserId,
                OrgUnitId = d.OrgUnitId,
                LineManagerId = d.LineManagerId,
                Resumes = d.SkillIds.Any()
                    ? new List<Resume>
                    {
                        new Resume
                        {
                            ResumeTechnicalSkills = d.SkillIds.Select(id => new ResumeTechnicalSkill
                            {
                                TechnicalSkillId = id
                            }).ToList()
                        }
                    }
                    : new List<Resume>(),
            };
        }
    }
}
