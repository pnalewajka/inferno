﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Dto
{
    public class ProjectInvoicingRateToProjectInvoicingRateDtoMapping : ClassMapping<ProjectInvoicingRate, ProjectInvoicingRateDto>
    {
        public ProjectInvoicingRateToProjectInvoicingRateDtoMapping()
        {
            Mapping = e => new ProjectInvoicingRateDto
            {
                Code = e.Code,
                Value = e.Value,
                Description = e.Description
            };
        }
    }
}
