﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Allocation.Services;

namespace Smt.Atomic.Business.Allocation.Interfaces
{
    public interface IEmployeeAvailabilityDataService
    {
        IEnumerable<DetailedAbsenceDto> GetDetailedAbsencesForEmployees(IReadOnlyCollection<long> employeeIds,
            DateTime dateFrom, DateTime dateTo);
    }
}
