﻿using System;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.Allocation.Interfaces
{
    public interface IBenchEmployeesService
    {
        void RewriteBenchFields(CardIndexRecords<BenchEmployeeDto> result, DateTime weekStart);
    }
}
