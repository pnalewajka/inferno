﻿namespace Smt.Atomic.Business.Allocation.Interfaces
{
    public interface IAllocationJobTitleService
    {
        long? GetJobTitleIdByTitle(string title);
    }
}