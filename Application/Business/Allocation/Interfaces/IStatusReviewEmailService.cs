﻿namespace Smt.Atomic.Business.Allocation.Interfaces
{
    public interface IStatusReviewEmailService
    {
        void PrepareFreeOrPlanned();
        void PrepareNonAllocatedNewHires();
    }
}
