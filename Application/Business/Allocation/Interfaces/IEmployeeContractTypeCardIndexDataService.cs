﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Common.Interfaces;

namespace Smt.Atomic.Business.Allocation.Interfaces
{
    public interface IEmployeeContractTypeCardIndexDataService : ICardIndexDataService<EmployeeContractTypeDto>
    {
    }
}

