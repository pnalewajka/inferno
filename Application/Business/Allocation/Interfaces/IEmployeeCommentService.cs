﻿using System;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Allocation.Interfaces
{
    public interface IEmployeeCommentService
    {
        void AddEmployeeComment(EmployeeCommentDto employeeCommentDto);

        void EditEmployeeComment(EmployeeCommentDto employeeCommentDto);

        EmployeeCommentDto GetEmployeeComment(EmployeeCommentType employeeCommentType, long employeeId,
            DateTime? relevantOn);
    }
}
