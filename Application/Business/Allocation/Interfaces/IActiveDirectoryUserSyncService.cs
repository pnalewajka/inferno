﻿using System;
using System.Collections.Generic;
using System.Threading;
using Smt.Atomic.Business.Accounts.Dto;

namespace Smt.Atomic.Business.Allocation.Interfaces
{
    public interface IActiveDirectoryUserSyncService
    {
        IEnumerable<ActiveDirectoryUserDto> GetAllEmployeesFromActiveDirectory(CancellationToken cancellationToken);

        void DeactivateUsersNotAvailableInActiveDirectory(IEnumerable<ActiveDirectoryUserDto> activeDirectoryUsers, IReadOnlyCollection<Guid> externalActiveDirectoryUserIds, CancellationToken cancellationToken);

        void SynchronizeUsersWithActiveDirectory(IEnumerable<ActiveDirectoryUserDto> activeDirectoryUsers, CancellationToken cancellationToken);

        void SynchronizeEmployeesWithActiveDirectory(IEnumerable<ActiveDirectoryUserDto> activeDirectoryUsers, CancellationToken cancellationToken);

    }
}
