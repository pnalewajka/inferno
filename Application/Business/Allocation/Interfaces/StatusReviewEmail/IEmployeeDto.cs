﻿namespace Smt.Atomic.Business.Allocation.Interfaces.StatusReviewEmail
{
    public interface IEmployeeDto
    {
        long Id { get; set; }
        long OrganizationUnitId { get; set; }
    }
}