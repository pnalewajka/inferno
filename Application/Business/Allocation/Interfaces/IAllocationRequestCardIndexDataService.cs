﻿using System;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Allocation.Interfaces
{
    public interface IAllocationRequestCardIndexDataService : ICardIndexDataService<AllocationRequestDto>
    {
        long? ResolveAllocationRequest(long projectId, long employeeId, DateTime period);

        void SetRecordContentType(AllocationRequestContentType? contentType);
    }
}

