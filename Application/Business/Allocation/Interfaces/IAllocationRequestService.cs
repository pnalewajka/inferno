﻿using Smt.Atomic.Business.Allocation.Dto;
using System;
using System.Collections.Generic;

namespace Smt.Atomic.Business.Allocation.Interfaces
{
    public interface IAllocationRequestService
    {
        IReadOnlyCollection<AllocationRequestDto> GetAllocationRequests(long employeeId, DateTime dateFrom, DateTime dateTo);
    }
}
