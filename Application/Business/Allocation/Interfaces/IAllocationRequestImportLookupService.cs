﻿namespace Smt.Atomic.Business.Allocation.Interfaces
{
    public interface IAllocationRequestImportLookupService
    {
        long GetProjectIdByCode(string name);

        string GetProjectCodeById(long id);

        long GetEmployeeIdByEmail(string email);

        string GetEmployeeEmailById(long id);
    }
}
