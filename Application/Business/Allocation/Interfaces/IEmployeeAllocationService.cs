﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Smt.Atomic.Business.Allocation.Dto.EmployeeAllocationDto;

namespace Smt.Atomic.Business.Allocation.Interfaces
{
    public interface IEmployeeAllocationService
    {
        AllocationBlockDto Edit(long allocationRequestId, IEditAllocationContract contract);
    }
}
