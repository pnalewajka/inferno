﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Allocation.Dto;

namespace Smt.Atomic.Business.Allocation.Interfaces
{
    public interface IEmployeeAllocationCardIndexDataService : IEmployeeAllocationBaseCardIndexDataService
    {
        IDictionary<string, IReadOnlyCollection<EmployeeAllocationPeriodDto>> CalculateWeekAllocations(AllocationChartSearchDto searchModel);

        IReadOnlyCollection<EmployeeAllocationPeriodDto> CalculateWeekAllocationsForJobProfile(long? jobProfileId, long? locationId, long? level);

        IEnumerable<JobProfileChartDetailsDto> JobProfileChartDetails(AllocationChartSearchDto searchModel, DateTime period);

        DateTime? StatusFrom { get; }
        
        DateTime? StatusTo { get; }
    }
}

