﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Allocation.Dto;

namespace Smt.Atomic.Business.Allocation.Interfaces
{
    public interface IEmployeePickerCardIndexDataService : ICardIndexDataService<EmployeeDto>
    {
    }
}

