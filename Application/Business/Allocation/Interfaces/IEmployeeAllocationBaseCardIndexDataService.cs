﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Allocation.Dto;

namespace Smt.Atomic.Business.Allocation.Interfaces
{
    public interface IEmployeeAllocationBaseCardIndexDataService : ICardIndexDataService<EmployeeAllocationDto>
    {
        DateTime GetDefaultFromValue();

        DateTime GetDefaultToValue();

        IEnumerable<OverAllocatedWarningDto> ValidateAddAllocationRequest(AllocationRequestDto requestDto);

        IEnumerable<OverAllocatedWarningDto> ValidateEditAllocationRequest(AllocationRequestDto requestDto);
    }
}

