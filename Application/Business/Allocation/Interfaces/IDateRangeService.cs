﻿using System;
using System.Collections.Generic;

namespace Smt.Atomic.Business.Allocation.Interfaces
{
    public interface IDateRangeService
    {
        IEnumerable<DateTime> GetAllWeeksInDateRanges(DateTime from, DateTime to);

        IEnumerable<DateTime> GetDaysInRange(DateTime from, DateTime to, bool includeWeekends = false);
    }
}
