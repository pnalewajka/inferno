﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Allocation.Dto;

namespace Smt.Atomic.Business.Allocation.Interfaces
{
    public interface IProjectService
    {
        ProjectDto CreateOrEditProject(ProjectDto project);

        ProjectDto GetProjectById(long id);

        IEnumerable<ProjectDto> GetProjectsByIds(IEnumerable<long> ids);

        ProjectDto GetProjectOrDefaultById(long id);

        ProjectDto GetProjectByJiraKeyOrDefault(string jiraKey);

        ProjectDto GetProjectByClientAndNameOrDefault(string project, string client);

        ProjectDto GetProjectByJiraIssueKeyOrDefault(string jiraIssueKey);

        long GetDefaultJiraIssueToTimeReportRowMapperId();

        IEnumerable<ProjectPresentationDto> GetProjecListByIdsForPresentationGeneration(long[] ids);

        void MergeProjects(IList<long> projectsIds, long targetProjectId);

        bool HasProjects(long employeeId);

        ProjectTagDto GetProjectTagByName(string name);

        IList<ProjectTagDto> GetProjectTagsByNames(string[] names);

        void CreateOrEditProjectTags(List<ProjectTagDto> tagsToCreate);

        void AssignAndDetachTags(List<ProjectTagDto> tagsToAssign, List<ProjectTagDto> tagsToDetach, long projectSetupId);

        IList<ProjectTagDto> GetProjectTagsByProjectId(long projectId);

        ICollection<long> GetProjectIdsByUserId(long userId, DateTime date);

        bool IsEmployeeOwnProject(long employeeId, long projectId);

        IReadOnlyCollection<string> GetAllProjectAcronyms();

        ProjectDto GetProjectByApn(string apn);

        long GetSetupIdByProjectId(long projectId);

        IReadOnlyCollection<ProjectDto> GetActiveProjectsForManagerEmployeeId(long employeeId);
    }
}
