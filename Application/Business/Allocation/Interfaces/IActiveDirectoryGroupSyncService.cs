﻿using System.Collections.Generic;
using System.Threading;
using Smt.Atomic.Business.Allocation.Dto;

namespace Smt.Atomic.Business.Allocation.Interfaces
{
    public interface IActiveDirectoryGroupSyncService
    {
        IEnumerable<ActiveDirectoryGroupDto> GetAllGroupsFromActiveDirectory(CancellationToken cancellationToken);

        void SynchronizeGroupWithActiveDirectory(IEnumerable<ActiveDirectoryGroupDto> groups, CancellationToken cancellationToken);
    }
}
