﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.Business.Allocation.Interfaces
{
    public interface IEditAllocationContract : IWeeklyHours, IDateRange
    {
        AllocationCertainty Certainty { get; }
    }
}
