﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Allocation.Dto;

namespace Smt.Atomic.Business.Allocation.Interfaces
{
    public interface IDailyAllocationService
    {
        bool RebuildDailyRecords(long allocationRequestId);

        bool RebuildDailyRecords(AllocationRequestDto allocationRequest, List<EmploymentPeriodDto> employmentPeriodList);

        bool RebuildWeeklyStatus(List<EmploymentPeriodDto> employmentPeriods, long employeeId, DateTime from, long orgUnitId);
        
        bool RebuildWeeklyStatus(long employeeId, DateTime from);

        void SetAllocationRequestExecuted(long allocationRequestId);
    }
}
