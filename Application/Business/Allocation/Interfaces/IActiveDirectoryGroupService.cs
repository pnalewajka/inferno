﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Allocation.Dto;

namespace Smt.Atomic.Business.Allocation.Interfaces
{
    public interface IActiveDirectoryGroupService
    {
        IEnumerable<ActiveDirectoryGroupDto> GetAll();

        ActiveDirectoryGroupVisualizationDto GetActiveDirectoryGroupVisualizationById(long id);

        void SynchronizeGroup(ActiveDirectoryGroupDto group);

        void AssignSubgroups(ActiveDirectoryGroupDto group);

        void DeleteUnlistedGroups(IEnumerable<Guid> groupIds);

        void AddUserToGroups(Guid userId, IEnumerable<Guid> groupId, bool shouldUpdateAd);

        void AddUserToGroups(Guid userId, IEnumerable<string> activeDirectoryGroupNames, bool shouldUpdateAd);

        void RemoveUserFromGroups(Guid userId, IEnumerable<Guid> groupId, bool shouldUpdateAd);

        void RemoveUserFromGroups(Guid userId, IEnumerable<string> activeDirectoryGroupNames, bool shouldUpdateAd);
    }
}
