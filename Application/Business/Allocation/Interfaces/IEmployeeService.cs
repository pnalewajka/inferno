﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Allocation.Interfaces
{
    public interface IEmployeeService
    {
        EmployeeDto GetEmployeeOrDefaultByEmail(string email);

        EmployeeDto GetEmployeeOrDefaultByFullName(string fullName);

        EmployeeDto GetEmployeeOrDefaultByAcronym(string acronym);

        IEnumerable<EmployeeDto> GetEmployeesOrDefaultByAcronyms(IEnumerable<string> acronyms);

        EmployeeDto GetEmployeeOrDefaultByUserId(long userId);

        long? GetEmployeeIdOrDefaultByUserId(long userId);

        long CreateEmployee(EmployeeDto employeeDto);

        void SynchronizeWithActiveDirectory(long employeeId, ActiveDirectoryUserDto activeDirectoryUserDto);

        void DeactivateEmployeeMissingInActiveDirectory(long employeeId);

        void SetManager(long employeeId, long? employeeIdLineManager);

        void UpdateCurrentEmployeeStatus(long employeeId);

        void UpdateEmployeeBasedOnEmploymentPeriod(long employeeId);

        IList<EmployeeDto> GetEmployeesById(IEnumerable<long> ids);

        long? GetEmployeeIdByUserId(long userId);

        IList<long> GetEmployeeIdsByUserIds(IEnumerable<long> userIds);

        IList<EmployeeDto> GetNotTerminatedEmployeesById(IEnumerable<long> ids);

        IList<long> GetEmployeeIds(Expression<Func<Employee, bool>> predicate);

        IList<EmployeeDto> GetEmployees(Expression<Func<Employee, bool>> predicate);

        EmployeeDto GetEmployeeOrDefaultById(long employeeId);

        EmployeeDto GetEmployeeOrDefaultByLogin(string login);

        IList<EmployeeDto> GetEmployeesOrDefaultByLogins(IEnumerable<string> logins);

        IList<long> GetEmployeeIdsByLogins(IEnumerable<string> logins);

        bool HasEmployees(long employeeId);

        string GetUserLogin(long employeeId);

        IList<EmployeeDto> GetEmployeesByLineManagerId(long id);

        IList<long> GetEmployeeUserIdsByLineManagerEmployeeId(long id);

        IList<long> GetEmployeeIdsByLineManagerEmployeeId(long id);

        long? GetEmployeeDirectManagerOrDefaultByOrgUnits(long? employeeId, OrgUnitManagerRole orgUnitManagerRole);

        IList<long> GetLineManagersWithNotTerminatedChildrenIds();

        bool HasEmployeesByLineManagerId(long lineManagerId);

        IList<long> GetEmployeeIds(IReadOnlyList<long> userIds);

        ICollection<long> GetUserIdsByEmployeeIds(params long[] employeeIds);

        IDictionary<long, long> GetUserIdToEmployeeIdDictionary(IReadOnlyCollection<long> userIds);

        IList<EmployeeCalendarInfoDto> GetCalendarInfoByEmployeeIds(IEnumerable<long> employeeIds, DateTime from, DateTime to);

        bool IsEmployeeOfManager(long employeeId, long managerId);

        long GetEmployeeCompanyId(long employeeId);

        bool IsDeductibleCostEnabledForEmployee(long employeeId);
    }
}
