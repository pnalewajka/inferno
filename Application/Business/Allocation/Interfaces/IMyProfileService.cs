﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.Allocation.Interfaces
{
    public interface IMyProfileService
    {
        MyProfileDto GetMyProfile(long userId, long employeeId);
    }
}
