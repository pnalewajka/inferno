﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.Allocation.Interfaces
{
    public interface IStaffingDemandService
    {
        AlertDto ValidateStaffingDemandDeleting(long staffingDemandId);

        AlertDto ValidateStaffingDemandEditing(long staffingDemandId);
    }
}
