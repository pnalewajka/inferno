﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Allocation.Interfaces
{
    public interface IEmploymentPeriodService
    {
        AlertDto ValidateEmploymentPeriodDeleting(long employmentPeriodId);

        List<AlertDto> ValidateEmploymentPeriodEditing(EmploymentPeriod entityEmploymentPeriod, EmploymentPeriodDto inputEmploymentPeriodDto);

        List<AlertDto> ValidateEmploymentPeriodAdding(EmploymentPeriodDto inputEmploymentPeriodDto);

        long CreateEmploymentPeriod(EmploymentPeriodDto employmentPeriod);

        long CreateEmploymentPeriod(long employeeId, ActiveDirectoryUserDto activeDirectoryUserDto);

        void UpdateEmploymentPeriods(long employeeId, ActiveDirectoryUserDto activeDirectoryUserDto);

        IList<DailyContractedHoursDto> GetContractedHours(long employeeId, DateTime from, DateTime to,
            Expression<Func<EmploymentPeriod, bool>> whereExpression = null);

        IList<DailyContractedHoursDto> GetContractedHours(long employeeId, int year, byte month,
            Expression<Func<EmploymentPeriod, bool>> whereExpression = null);

        EmploymentPeriodDto GetEmploymentPeriod(long employeeId, DateTime date);

        IList<EmploymentPeriodDto> GetEmploymentPeriodsInDateRange(long employeeId, DateTime startDate, DateTime endDate);

        IQueryable<AllocationRequest> OverlapingAllocationRequests(
            IQueryable<AllocationRequest> allocationRequestRepository, long employeeId, DateTime startDate,
            DateTime endDate);

        bool IsEmployeeHiredInCompletePeriod(long id, DateTime startDate, DateTime endDate);

        bool IsClockCardRequiredInGivenMonth(long employeeId, int year, byte month);

        OverTimeTypes AllowedOvertimeInGivenMonth(long employeeId, int year, byte month);
    }
}
