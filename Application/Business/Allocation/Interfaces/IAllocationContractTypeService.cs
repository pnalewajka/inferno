﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Allocation.Interfaces
{
    public interface IAllocationContractTypeService
    {
        long GetDefaultContractTypeId();

        long GetContractTypeIdByActiveDirectoryCode(string code);

        TimeTrackingCompensationCalculators? GetTimeTrackingCompensationCalculatorOrDefaultByContractTypeId(long contractId);

        BusinessTripCompensationCalculators? GetBusinessTripCompensationCalculatorOrDefaultByContractTypeId(long contractId);
    }
}