﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Allocation.DailyAllocation
{
    public class EmployeAllocationDayData
    {
        public DateTime Day { get; set; }

        public bool IsActive { get; set; }

        public bool IsNewHire { get; set; }

        public bool IsLeave { get; set; }

        public decimal AvailableHours { get; set; }

        public decimal WorkingHours { get; set; }

        public decimal FreeHours { get; set; }

        public decimal CalendarHours { get; set; }

        public EmployeAllocationProjectData[] Projects { get; set; }
    }
}
