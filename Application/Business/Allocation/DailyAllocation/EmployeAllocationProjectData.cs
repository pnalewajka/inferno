﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Allocation.DailyAllocation
{
    public class EmployeAllocationProjectData
    {
        public long ProjectID { get; set; }

        public AllocationCertainty AllocationCertainty { get; set; }

        public decimal WorkingHours { get; set; }

        public decimal FreeHours { get; set; }

        public bool IsWorkDay { get; set; }
    }
}
