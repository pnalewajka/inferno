﻿namespace Smt.Atomic.Business.Allocation.Consts
{
    public static class SearchAreaCodesBenchEmployee
    {
        public const string FirstName = "first-name";
        public const string LastName = "last-name";
        public const string EmployeeStatus = "status";
        public const string StaffingStatus = "staffing-status";
        public const string LocationName = "location-name";
        public const string LineMaganer = "line-manager";
        public const string OrganizationUnit = "organization-unit";
        public const string Skills = "skills";
        public const string JobProfileName = "job-profile-name";
        public const string JobTitle = "job-title";
    }
}