﻿namespace Smt.Atomic.Business.Allocation.Consts
{
    public static class SearchAreaCodes
    {
        public const string Name = "name";
        public const string FirstName = "first-name";
        public const string LastName = "last-name";
        public const string Acronym = "acronym";
        public const string Login = "login";
        public const string LineMaganer = "line-manager";
        public const string OrganizationUnit = "organization-unit";
        public const string JobTitle = "job-title";
        public const string PetsCode = "pets-code";
        public const string ProjectShortName = "project";
        public const string ProjectAcronym = "acronym";
        public const string ClientShortName = "client";
        public const string OrgUnit = "org-unit";
        public const string Description = "descr";
        public const string Aliases = "aliases";
        public const string SalesAccountManager = "sale-manager";
        public const string ProjectManager = "pm";
        public const string JiraKey = "jira-key";
        public const string Apn = "apn";
        public const string KupferwerkProjectId = "kw-id";
        public const string Email = "email";
        public const string ActiveDirectoryGroupOwner = "owner";
        public const string Managers = "managers";
        public const string Users = "users";
        public const string Region = "region";
        public const string Skills = "skills";
    }
}