﻿namespace Smt.Atomic.Business.Allocation.Consts
{
    public static class FilterCodes
    {
        public const string MyEmployees = "my-employees";
        public const string MyDirectEmployees = "my-direct-employees";
        public const string MyProjects = "my-projects";
        public const string ActiveProjects = "active-projects";
        public const string InactiveProjects = "inactive-projects";
        public const string AllEmployees = "all-employees";
        public const string ActiveEmployees = "active";
        public const string AllProjects = "all-projects";
        public const string DateRange = "date-range";
        public const string LineManagerFilter = "line-manager";
        public const string OrgUnitsFilter = "org-units-filter";
        public const string JobProfileFilter = "job-profile";
        public const string SkillFilter = "skill";
        public const string JobMatrixFilter = "job-matrix";
        public const string EmployeesAndProjects = "employees-and-projects";
        public const string ProjectTags = "project-tags";
        public const string CompanyFilter = "company";
        public const string LocationFilter = "location";
        public const string AllAllocations = "all-allocations";
        public const string UtilizationFilter = "utilization";
        public const string MyTeamAllocations = "my-team-allocations";
        public const string WeekFilter = "week-start";
        public const string ProjectContributorFilter = "projectcontributor";
        public const string MyProjectGuestsFilter = "my-project-guests";
        public const string MyEmployeesVisitingFilter = "my-employees-visiting";
        public const string TechnologyFilter = "technology";
        public const string KeyTechnologyFilter = "key-technology";
        public const string AvailableForSalesPresentation = "sales-show";
        public const string UnavailableForSalesPresentation = "sales-hide";
        public const string WithReferences = "with-references";
        public const string WithoutReferences = "without-references";
        public const string Industries = "industries";
        public const string SoftwareCategories = "software-categories";
        public const string ServiceLineFilter = "service-line";
        public const string CustomerSizeFilter = "customer-size";
        public const string ProjectDisclosures = "disclosures";
        public const string SkillsAndKnowledgeFilter = "skills-and-knowledge";
        public const string InfernoProjectsFilter = "inferno-projects";
        public const string JiraProjectsFilter = "jira-projects";
        public const string AllocationStatusFreeFilter = "allocation-status-free";
        public const string AllocationStatusBusyFilter = "allocation-status-busy";
        public const string ContractTypeFilter = "contract-type";
        public const string UsersActiveDirectoryGroups = "my-ad-groups";
        public const string AllActiveDirectoryGroups = "all-ad-groups";
        public const string WithOwnerGroup = "with-owner";
        public const string WithoutOwnerGroup = "without-owner";
        public const string AnnounceLabel = "announce-group";
        public const string DanteLabel = "dante-group";
        public const string OtherLabel = "other-group";
        public const string SkillsFilter = "skills";
        public const string LanguagesFilter = "languages";
        public const string ProjectAndDateFilter = "project-and-date";

        public const string FakeDetailedFilter = "detailed";
        public const string FakePlannedFilter = "planned";
        public const string FakeColorsFilter = "colors";
        public const string FakeWeekendsFilter = "weekends";
    }
}