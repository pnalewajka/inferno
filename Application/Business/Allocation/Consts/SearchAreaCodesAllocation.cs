﻿namespace Smt.Atomic.Business.Allocation.Consts
{
    public static class SearchAreaCodesAllocation
    {
        public const string FirstName = "first-name";
        public const string LastName = "last-name";
        public const string Email = "email";
        public const string LocationName = "location-name";
        public const string LineMaganerLastName = "line-manager";
        public const string Skills = "skills";
        public const string JobProfileName = "job-profile-name";
        public const string JobTitle = "job-title";
        public const string AllocationStatus = "status";
        public const string ProjectName = "project-name";
    }
}
