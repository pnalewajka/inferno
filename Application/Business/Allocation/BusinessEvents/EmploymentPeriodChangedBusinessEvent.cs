﻿using System;
using System.Runtime.Serialization;
using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.Allocation.BusinessEvents
{
    [DataContract]
    public class EmploymentPeriodChangedBusinessEvent : BusinessEvent
    {
        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public DateTime From { get; set; }

        [DataMember]
        public DateTime? To { get; set; }
    }
}