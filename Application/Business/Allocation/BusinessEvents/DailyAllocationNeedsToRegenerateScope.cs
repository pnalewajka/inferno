﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Allocation.BusinessEvents
{
    public enum DailyAllocationNeedsToRegenerateScope
    {
        AllocationRequest = 0,
        Project = 1,
        EmploymentPeriod = 2,
    }
}
