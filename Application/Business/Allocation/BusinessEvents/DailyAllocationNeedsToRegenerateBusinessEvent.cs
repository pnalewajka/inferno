﻿using System;
using System.Runtime.Serialization;
using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.Allocation.BusinessEvents
{
    [DataContract]
    public class DailyAllocationNeedsToRegenerateBusinessEvent : BusinessEvent
    {
        [DataMember]
        public DailyAllocationNeedsToRegenerateScope Scope { get; set; }

        [DataMember]
        public long? AllocationRequestId { get; set; }

        [DataMember]
        public long? Projectid { get; set; }

        [DataMember]
        public long? EmployeeId { get; set; }

        [DataMember]
        public DateTime? PeriodFrom { get; set; }
    }
}
