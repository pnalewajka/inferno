﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Allocation.Resources;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Workflows.Services
{
    public class ApprovingPersonService : IApprovingPersonService
    {
        private readonly IReadOnlyUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService;
        private readonly IReadOnlyUnitOfWorkService<IBusinessTripsDbScope> _btUnitOfWorkService;
        private readonly IEmploymentPeriodService _employmentPeriodService;
        private readonly IOrgUnitListCacheService _orgUnitListCacheService;
        private readonly ICompanyService _companyService;
        private readonly IProjectService _projectService;
        private readonly IOrgUnitService _orgUnitService;

        public ApprovingPersonService(
            IReadOnlyUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
            IReadOnlyUnitOfWorkService<IBusinessTripsDbScope> btUnitOfWorkService,
            IEmploymentPeriodService employmentPeriodService,
            IOrgUnitListCacheService orgUnitListCacheService,
            ICompanyService companyService,
            IProjectService projectService,
            IOrgUnitService orgUnitService)
        {
            _unitOfWorkService = unitOfWorkService;
            _btUnitOfWorkService = btUnitOfWorkService;
            _employmentPeriodService = employmentPeriodService;
            _orgUnitListCacheService = orgUnitListCacheService;
            _companyService = companyService;
            _projectService = projectService;
            _orgUnitService = orgUnitService;
        }

        public long? GetStandardHRApprover(long employeeId, long requesterEmployeeId)
        {
            var approverId = GetStandardHRApprover(employeeId);

            if (approverId.HasValue)
            {
                using (var unitOfWork = _unitOfWorkService.Create())
                {
                    var managerOrgUnitIds = _orgUnitService.GetManagerOrgUnitDescendantIds(requesterEmployeeId);
                    var isSupervisorLogic = new BusinessLogic<Employee, bool>(e =>
                        EmployeeBusinessLogic.IsWorking.Call(e)
                        && EmployeeBusinessLogic.IsEmployeeOf.Call(e, requesterEmployeeId, managerOrgUnitIds));
                    var isRequesterSupervisorOfApprover = unitOfWork.Repositories.Employees
                        .Where(isSupervisorLogic.AsExpression())
                        .Any(e => e.Id == approverId);

                    if (isRequesterSupervisorOfApprover || approverId == requesterEmployeeId)
                    {
                        var orgUnits = _orgUnitListCacheService.GetAllOrgUnits();

                        if (managerOrgUnitIds.Any()
                            && managerOrgUnitIds.Select(id => orgUnits[id]).Any(o => o.OrgUnitFeatures.HasFlag(OrgUnitFeatures.AutonomousOrgUnit)))
                        {
                            return requesterEmployeeId;
                        }

                        approverId = GetStandardHRApprover(requesterEmployeeId);
                    }
                }
            }

            return approverId;
        }

        public long? GetStandardHRApprover(long employeeId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var orgUnits = _orgUnitListCacheService.GetAllOrgUnits();
                var employee = unitOfWork.Repositories.Employees
                    .Where(e => e.Id == employeeId)
                    .Select(e => new { e.LineManagerId, e.OrgUnitId })
                    .SingleOrDefault();

                if (employee == null)
                {
                    return null;
                }

                var currentOrgUnit = orgUnits[employee.OrgUnitId];

                while (currentOrgUnit.ParentId.HasValue)
                {
                    if (currentOrgUnit.EmployeeId != employeeId && (currentOrgUnit.EmployeeId != employee.LineManagerId
                        || currentOrgUnit.OrgUnitFeatures.HasFlag(OrgUnitFeatures.AutonomousOrgUnit)))
                    {
                        return currentOrgUnit.EmployeeId;
                    }

                    currentOrgUnit = orgUnits[currentOrgUnit.ParentId.Value];
                }

                return currentOrgUnit.EmployeeId;
            }
        }

        public long[] GetProjectApprovers(long employeeId, long projectId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var project = unitOfWork.Repositories.Projects.GetById(projectId);

                if (project.IsApprovedByRequesterMainManager)
                {
                    var mainManagerId = GetEmployeeMainManager(employeeId);

                    return mainManagerId.HasValue ? new[] { mainManagerId.Value } : new long[] { };
                }

                if (project.IsApprovedByMainManagerForProjectManager && project.ProjectSetup.ProjectManager.Id == employeeId)
                {
                    var mainManagerId = GetEmployeeMainManager(employeeId);

                    return mainManagerId.HasValue ? new[] { mainManagerId.Value } : new long[] { };
                }

                if (project.TimeReportAcceptingPersons.Any())
                {
                    return project.TimeReportAcceptingPersons.Select(p => p.Id).Distinct().ToArray();
                }

                var approver = project.ProjectSetup.ProjectManagerId ?? GetOrgUnitApproverId(project.ProjectSetup.OrgUnitId);
                approver = RemoveRequesterFromApprover(employeeId, approver);

                return approver.HasValue ? new[] { approver.Value } : new long[] { };
            }
        }

        public long GetProjectSupervisorApprover(long employeeId, long projectId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var approver = unitOfWork.Repositories.Projects.SelectById(projectId, p => p.ProjectSetup.SupervisorManagerId);
                approver = RemoveRequesterFromApprover(employeeId, approver);

                return approver.GetValueOrDefault();
            }
        }

        private long? GetOrgUnitApproverId(long orgUnitId)
        {
            var orgUnits = _orgUnitListCacheService.GetAllOrgUnits();
            var currentOrgUnit = orgUnits[orgUnitId];

            while (!currentOrgUnit.EmployeeId.HasValue && currentOrgUnit.ParentId != null)
            {
                currentOrgUnit = orgUnits[currentOrgUnit.ParentId.Value];
            }

            return currentOrgUnit.EmployeeId;
        }

        public long? GetEmployeeMainManager(long employeeId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employee = unitOfWork.Repositories.Employees.GetByIdOrDefault(employeeId);

                if (employee == null)
                {
                    return null;
                }

                var approver = employee.LineManagerId ?? GetOrgUnitApproverId(employee.OrgUnit.Id);

                return approver != employeeId ? approver : null;
            }
        }

        private bool IsUnderInOrgUnit(long parentEmployeeId, long childEmployeeId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var parentPath = unitOfWork.Repositories.Employees.Where(e => e.Id == parentEmployeeId).Select(e => e.OrgUnit.Path).Single();
                var childPath = unitOfWork.Repositories.Employees.Where(e => e.Id == childEmployeeId).Select(e => e.OrgUnit.Path).Single();

                return childPath.Contains(parentPath);
            }
        }

        public long[] GetProjectApprovers(long employeeId, DateTime from, DateTime to)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var involvedInProjectIds =
                    _employmentPeriodService.OverlapingAllocationRequests(unitOfWork.Repositories.AllocationRequests,
                        employeeId, from, to)
                        .Where(ar => ar.AllocationCertainty == AllocationCertainty.Certain)
                        .Select(ar => ar.ProjectId).ToList();

                var approvers =
                    involvedInProjectIds.SelectMany(p => GetProjectApprovers(employeeId, p))
                        .Distinct()
                        .ToArray();

                return RemoveRequesterFromApprovers(employeeId, approvers);
            }
        }

        private long[] RemoveRequesterFromApprovers(long employeeId, long[] approvers)
        {
            return
                approvers.Select(a => RemoveRequesterFromApprover(employeeId, a))
                    .Where(a => a.HasValue)
                    .Select(a => a.Value)
                    .Distinct()
                    .ToArray();
        }

        private long? RemoveRequesterFromApprover(long employeeId, long? approver)
        {
            return approver == employeeId ? GetEmployeeMainManager(employeeId) : approver;
        }

        public long[] GetStandardCostApprovers(IEnumerable<long> employeeIds, IEnumerable<long> projectIds)
        {
            var projects = _projectService.GetProjectsByIds(projectIds);
            var approverIds = new List<long>();

            foreach (long projectId in projectIds)
            {
                approverIds.AddRange(GetStandardCostProjectApprover(
                    employeeIds,
                    projects.Single(p => p.Id == projectId)));
            }

            return approverIds.Distinct().ToArray();
        }

        private IEnumerable<long> GetStandardCostProjectApprover(IEnumerable<long> employeeIds, ProjectDto project)
        {
            foreach (long employeeId in employeeIds)
            {
                var employeeMainManager = GetEmployeeMainManager(employeeId);

                if (project.IsApprovedByRequesterMainManager)
                {
                    if (employeeMainManager.HasValue)
                    {
                        yield return employeeMainManager.Value;
                    }
                }
                else
                {
                    var projectMainApprover = project.ProjectManagerId ?? project.SupervisorManagerId ?? project.SalesAccountManagerId;

                    if (projectMainApprover == null)
                    {
                        throw new BusinessException(string.Format(AlertResources.NoProjectApproverDetectedMessageFormat, project.ProjectShortName));
                    }

                    var projectApprovers = project.TimeReportAcceptingPersonIds.Any()
                        ? project.TimeReportAcceptingPersonIds
                        : new[] { projectMainApprover.Value };

                    foreach (var projectApprover in projectApprovers)
                    {
                        if (projectApprover == employeeId)
                        {
                            if (!project.IsApprovedByMainManagerForProjectManager)
                            {
                                yield return projectApprover;
                            }
                            else
                            {
                                var approverMainManager = GetEmployeeMainManager(projectApprover);

                                if (approverMainManager.HasValue)
                                {
                                    yield return approverMainManager.Value;
                                }
                            }
                        }
                        else
                        {
                            if (employeeMainManager.HasValue && IsUnderInOrgUnit(employeeId, projectApprover))
                            {
                                yield return employeeMainManager.Value;
                            }
                            else
                            {
                                yield return projectApprover;
                            }
                        }
                    }
                }
            }
        }

        public long[] GetBusinessTripSettlementApprovers(long businessTripParticipantId)
        {
            using (var unitOfWork = _btUnitOfWorkService.Create())
            {
                var participant = unitOfWork.Repositories.BusinessTripParticipants.GetById(businessTripParticipantId);
                var specialists = participant.Employee.Company.TravelExpensesSpecialists.Select(s => s.Id).ToArray();

                if (!specialists.Any())
                {
                    throw new BusinessException(string.Format(AlertResources.SettlementRequestHasNoApproversFormat, participant.Employee.Company.Name, participant.Employee.DisplayName));
                }

                return specialists;
            }
        }

        public long? GetAdvancePaymentRequestApprover(long participantId)
        {
            using (var unitOfWork = _btUnitOfWorkService.Create())
            {
                var participant = unitOfWork.Repositories.BusinessTripParticipants.GetById(participantId);

                var company = _companyService.GetCompanyOrDefaultByEmployeeId(
                    _employmentPeriodService.GetEmploymentPeriod(
                            participant.EmployeeId,
                            participant.BusinessTrip.StartedOn)
                        .EmployeeId);

                if (company.AdvancePaymentAcceptingEmployeeId == null)
                {
                    throw new BusinessException(string.Format(AlertResources.CompanyHasNoAdvancePaymentApprover, company.Name, participant.Employee.DisplayName));
                }

                return company.AdvancePaymentAcceptingEmployeeId;
            }
        }
    }
}