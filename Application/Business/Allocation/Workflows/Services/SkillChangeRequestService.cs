﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.Business.Workflows.Services;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Resumes;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.Workflows.Services
{
    internal class SkillChangeRequestService : RequestServiceBase<SkillChangeRequest, SkillChangeRequestDto>
    {
        private readonly IUnitOfWorkService<IAllocationDbScope> _unitOfWorkService;
        private readonly IApprovingPersonService _approvingPersonService;

        public SkillChangeRequestService(
            IPrincipalProvider principalProvider,
            IWorkflowNotificationService workflowNotificationService,
            IUnitOfWorkService<IAllocationDbScope> unitOfWorkService,
            IActionSetService actionSetService,
            IClassMappingFactory classMappingFactory,
            IApprovingPersonService approvingPersonService,
            IRequestWorkflowService requestWorkflowService,
            ISystemParameterService systemParameterService)
            : base(actionSetService, principalProvider, classMappingFactory, workflowNotificationService, requestWorkflowService, systemParameterService)
        {
            _unitOfWorkService = unitOfWorkService;
            _approvingPersonService = approvingPersonService;
        }

        public override RequestType RequestType => RequestType.SkillChangeRequest;
        public override string RequestName => WorkflowResources.SkillChangeRequestName;

        public override RequestTypeGroup RequestTypeGroup => RequestTypeGroup.Other;

        public override SecurityRoleType? RequiredRoleType => SecurityRoleType.CanRequestSkillChange;

        public override SkillChangeRequestDto GetDraft(IDictionary<string, object> parameters)
        {
            ValidateCurrentPrincipalHasEmployee();

            return new SkillChangeRequestDto
            {
                EmployeeId = PrincipalProvider.Current.EmployeeId
            };
        }

        public override IEnumerable<ApproverGroupDto> GetApproverGroups(RequestDto request,
            SkillChangeRequestDto skillChangeRequest)
        {
            var approver = _approvingPersonService.GetEmployeeMainManager(skillChangeRequest.EmployeeId);

            if (approver.HasValue)
            {
                yield return new ApproverGroupDto(ApprovalGroupMode.And, new List<long>{approver.Value});
            }
        }

        public override void Execute(RequestDto request, SkillChangeRequestDto skillChangeRequest)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employee = unitOfWork.Repositories.Employees.GetById(skillChangeRequest.EmployeeId);
                var skills = unitOfWork.Repositories.Skills.GetByIds(skillChangeRequest.SkillIds);
                var resume = employee.Resumes.SingleOrDefault();

                if (resume == null)
                {
                    resume = unitOfWork.Repositories.Resumes.CreateEntity();
                    resume.ResumeTechnicalSkills = new List<ResumeTechnicalSkill>();
                    employee.Resumes.Add(resume);
                }

                foreach (var skill in skills)
                {
                    if (resume.ResumeTechnicalSkills.All(rts => rts.TechnicalSkillId != skill.Id))
                    {
                        var resumeTechnicalSkill = unitOfWork.Repositories.ResumeTechnicalSkills.CreateEntity();
                        resumeTechnicalSkill.TechnicalSkill = skill;
                        resume.ResumeTechnicalSkills.Add(resumeTechnicalSkill);
                    }
                }

                unitOfWork.Commit();
            }
        }

        public override string GetRequestDescription(RequestDto request, SkillChangeRequestDto skillChangeRequest)
        {
            return RequestName;
        }

        public override IQueryable<Request> ConfigureIncludes(IQueryable<Request> repository)
        {
            return repository.Include(r => r.SkillChangeRequest);
        }
    }
}
