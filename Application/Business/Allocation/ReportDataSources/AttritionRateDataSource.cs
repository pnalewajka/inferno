﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Allocation.ReportModels;
using Smt.Atomic.Business.Allocation.Resources;
using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.ReportDataSources
{
    [Identifier("DataSource.ContractsTerminationsDataSource")]
    [RequireRole(SecurityRoleType.CanGenerateEmploymentChangeReport)]
    [DefaultReportDefinition("EmploymentChangeReport",
        nameof(AttritionRateResource.EmploymentChangeReportName),
        nameof(AttritionRateResource.EmploymentChangeReportDescription),
        MenuAreas.Organization,
        MenuGroups.PeopleManagement,
        ReportingEngine.PivotTable, "Smt.Atomic.Business.Allocation.ReportTemplates.AttritionRatePivotTable-config.js",
        typeof(AttritionRateResource))]
    [DefaultReportDefinition("EmploymentChangeChart",
        nameof(AttritionRateResource.EmploymentChangeChartReportName),
        nameof(AttritionRateResource.EmploymentChangeChartReportDescription),
        MenuAreas.Organization,
        MenuGroups.PeopleManagement,
        ReportingEngine.Chart, "Smt.Atomic.Business.Allocation.ReportTemplates.AttritionRateChart-config.js",
        typeof(AttritionRateResource))]
    public class AttritionRateDataSource : BaseReportDataSource<AttritionRateDataSourceParametersDto>
    {
        private readonly ITimeService _timeService;
        private readonly IUnitOfWorkService<IAllocationDbScope> _unitOfWorkService;
        private readonly ISystemParameterService _parameterService;
        private readonly IOrgUnitService _orgUnitService;
        private readonly IAllocationContractTypeService _allocationContractTypeService;

        [Flags]
        private enum EmoploymentPeriodType
        {
            None = 0,
            StartOfEmployment = 1 << 0,
            EndOfEmployment = 1 << 1
        }

        public AttritionRateDataSource(
            ITimeService timeService,
            IUnitOfWorkService<IAllocationDbScope> unitOfWorkService,
            ISystemParameterService parameterService,
            IOrgUnitService orgUnitService,
            IAllocationContractTypeService allocationContractTypeService)
        {
            _timeService = timeService;
            _unitOfWorkService = unitOfWorkService;
            _parameterService = parameterService;
            _orgUnitService = orgUnitService;
            _allocationContractTypeService = allocationContractTypeService;
        }

        public override object GetData(ReportGenerationContext<AttritionRateDataSourceParametersDto> reportGenerationContext)
        {
            var workingHoursInWeek = _parameterService.GetParameter<int>(ParameterKeys.WorkingHoursInWeek);
            var maxDaysBetweenEmploymentPeriods = _parameterService.GetParameter<int>(ParameterKeys.MaxDaysBetweenEmploymentPeriods);

            OrgUnitDto orgUnitDto = reportGenerationContext.Parameters.OrgUnitId.HasValue
                ? _orgUnitService.GetOrgUnitOrDefaultById(reportGenerationContext.Parameters.OrgUnitId.Value)
                : _orgUnitService.GetOrgUnitByCode(_parameterService.GetParameter<string>(ParameterKeys.OrganizationRootUnitCode));

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var listAttritionRateReportModel = new List<AttritionRateReportModel>();
                EmploymentPeriod previousEmploymentPeriod = null;
                var startDate = default(DateTime);
                reportGenerationContext.Parameters.DateTo = reportGenerationContext.Parameters.DateTo.EndOfDay();
                var employeePeriods = FilterDataScope(unitOfWork.Repositories, reportGenerationContext.Parameters, orgUnitDto)
                    .GroupBy(p => p.EmployeeId);

                foreach (var employmentPeriods in employeePeriods)
                {
                    previousEmploymentPeriod = null;
                    startDate = default(DateTime);

                    foreach (var currentEmploymentPeriod in employmentPeriods.OrderBy(p => p.StartDate))
                    {
                        if (previousEmploymentPeriod != null)
                        {
                            var isContinuationOfEmploymentPeriod = !previousEmploymentPeriod.EndDate.HasValue || (previousEmploymentPeriod.EndDate.HasValue
                                && (currentEmploymentPeriod.StartDate - previousEmploymentPeriod.EndDate.Value).TotalDays <= maxDaysBetweenEmploymentPeriods);

                            if (!isContinuationOfEmploymentPeriod)
                            {
                                var periodType = EmoploymentPeriodType.EndOfEmployment;

                                if (startDate >= reportGenerationContext.Parameters.DateFrom)
                                {
                                    var hasContinuationInPreviousPeriod = startDate.AddDays(maxDaysBetweenEmploymentPeriods * -1) < reportGenerationContext.Parameters.DateFrom
                                        ? HasContinuationInPreviousPeriod(unitOfWork, employmentPeriods.Key, startDate, maxDaysBetweenEmploymentPeriods)
                                        : false;

                                    if (!hasContinuationInPreviousPeriod)
                                    {
                                        periodType |= EmoploymentPeriodType.StartOfEmployment;
                                    }
                                }

                                listAttritionRateReportModel.AddRange(CreateAttritionRateReportModelList(
                                    startDate,
                                    previousEmploymentPeriod,
                                    orgUnitDto,
                                    workingHoursInWeek,
                                    periodType));

                                startDate = currentEmploymentPeriod.StartDate;
                            }
                        }
                        else
                        {
                            startDate = currentEmploymentPeriod.StartDate;
                        }

                        previousEmploymentPeriod = currentEmploymentPeriod;
                    }

                    if (previousEmploymentPeriod != null)
                    {
                        var periodType = EmoploymentPeriodType.None;

                        if (startDate >= reportGenerationContext.Parameters.DateFrom)
                        {
                            var hasContinuationInPreviousPeriod = startDate.AddDays(maxDaysBetweenEmploymentPeriods * -1) < reportGenerationContext.Parameters.DateFrom
                                ? HasContinuationInPreviousPeriod(unitOfWork, employmentPeriods.Key, startDate, maxDaysBetweenEmploymentPeriods)
                                : false;

                            if (!hasContinuationInPreviousPeriod)
                            {
                                periodType = EmoploymentPeriodType.StartOfEmployment;
                            }
                        }

                        if (previousEmploymentPeriod.EndDate.HasValue && previousEmploymentPeriod.EndDate.Value <= reportGenerationContext.Parameters.DateTo)
                        {
                            var hasContinuationInNextPeriod = previousEmploymentPeriod.EndDate.Value.AddDays(maxDaysBetweenEmploymentPeriods) > reportGenerationContext.Parameters.DateTo
                                ? HasContinuationInNextPeriod(unitOfWork, employmentPeriods.Key, previousEmploymentPeriod.EndDate.Value, maxDaysBetweenEmploymentPeriods)
                                : false;

                            if (!hasContinuationInNextPeriod)
                            {
                                periodType |= EmoploymentPeriodType.EndOfEmployment;
                            }
                        }

                        if ((periodType & (EmoploymentPeriodType.StartOfEmployment | EmoploymentPeriodType.EndOfEmployment)) != 0)
                        {
                            listAttritionRateReportModel.AddRange(CreateAttritionRateReportModelList(
                                startDate,
                                previousEmploymentPeriod,
                                orgUnitDto,
                                workingHoursInWeek,
                                periodType));
                        }
                    }
                }

                AssignSubOrgUnitName(listAttritionRateReportModel, orgUnitDto);

                return new AttritionRatesReportModel
                {
                    Terminations = listAttritionRateReportModel
                };
            }
        }

        private bool HasContinuationInNextPeriod(IUnitOfWork<IAllocationDbScope> unitOfWork, long employeeId, DateTime endOfPreviousPeriod, int maxDaysBetweenEmploymentPeriods)
        {
            return unitOfWork.Repositories.EmploymentPeriods
                .Where(e => e.EmployeeId == employeeId)
                .Any(DateRangeHelper.IsDateInRange<EmploymentPeriod>(endOfPreviousPeriod, endOfPreviousPeriod.AddDays(maxDaysBetweenEmploymentPeriods), e => e.StartDate));
        }

        private bool HasContinuationInPreviousPeriod(IUnitOfWork<IAllocationDbScope> unitOfWork, long employeeId, DateTime startOfCurrentPeriod, int maxDaysBetweenEmploymentPeriods)
        {
            return unitOfWork.Repositories.EmploymentPeriods
                .Where(e => e.EmployeeId == employeeId && e.EndDate.HasValue)
                .Any(DateRangeHelper.IsDateInRange<EmploymentPeriod>(startOfCurrentPeriod.AddDays(maxDaysBetweenEmploymentPeriods * -1), startOfCurrentPeriod, e => e.EndDate.Value));
        }

        private IEnumerable<AttritionRateReportModel> CreateAttritionRateReportModelList(
            DateTime startDate,
            EmploymentPeriod employmentPeriod,
            OrgUnitDto orgUnitDto,
            int workingHoursInWeek,
            EmoploymentPeriodType periodType)
        {
            if (periodType.HasFlag(EmoploymentPeriodType.EndOfEmployment))
            {
                yield return new AttritionRateReportModel
                {
                    Acronym = employmentPeriod.Employee.Acronym,
                    FirstName = employmentPeriod.Employee.FirstName,
                    LastName = employmentPeriod.Employee.LastName,
                    Company = employmentPeriod.Employee.Company?.Name ?? string.Empty,
                    LineManager = employmentPeriod.Employee.LineManager?.FullName ?? string.Empty,
                    JobTitle = employmentPeriod.Employee.JobTitle?.NameEn ?? string.Empty,
                    ContractType = employmentPeriod.ContractType.NameEn ?? string.Empty,
                    Date = employmentPeriod.EndDate.Value,
                    EmployeeOrgUnitName = employmentPeriod.Employee.OrgUnit.Name,
                    EmployeeOrgUnitFlatName = employmentPeriod.Employee.OrgUnit.FlatName,
                    RootOrgUnitName = orgUnitDto.Name,
                    RootOrgUnitFlatName = orgUnitDto.FlatName,
                    SubOrgUnitName = employmentPeriod.Employee.OrgUnit.Name == orgUnitDto.Name ? "" : null,
                    SubOrgUnitFlatName = employmentPeriod.Employee.OrgUnit.FlatName == orgUnitDto.FlatName ? orgUnitDto.FlatName : null,
                    OrgUnitPath = employmentPeriod.Employee.OrgUnit.Path,
                    IsVoluntarilyTurnover = employmentPeriod.TerminationReason != null && employmentPeriod.TerminationReason.IsVoluntarilyTurnover ? 1 : 0,
                    IsOtherDepartureReasons = employmentPeriod.TerminationReason != null && employmentPeriod.TerminationReason.IsOtherDepartureReasons ? 1 : 0,
                    FteRatio = CalculateFteRatio(employmentPeriod, workingHoursInWeek),
                    Seniority = employmentPeriod.Employee.JobMatrixs != null && employmentPeriod.Employee.JobMatrixs.Any() ? employmentPeriod.Employee.JobMatrixs.Single().Level.ToString() : "unspecified",
                    JobProfile = employmentPeriod.Employee.JobProfiles != null && employmentPeriod.Employee.JobProfiles.Any() ? employmentPeriod.Employee.JobProfiles.First().Name : "unspecified",
                    NewHire = 0,
                    EmployeeRotation = -1,
                    Location = employmentPeriod.Employee.Location.Name,
                    PlaceOfWork = employmentPeriod.Employee.PlaceOfWork
                };
            }

            if (periodType.HasFlag(EmoploymentPeriodType.StartOfEmployment))
            {
                yield return new AttritionRateReportModel
                {
                    Acronym = employmentPeriod.Employee.Acronym,
                    FirstName = employmentPeriod.Employee.FirstName,
                    LastName = employmentPeriod.Employee.LastName,
                    Company = employmentPeriod.Employee.Company?.Name ?? string.Empty,
                    LineManager = employmentPeriod.Employee.LineManager?.FullName ?? string.Empty,
                    JobTitle = employmentPeriod.Employee.JobTitle?.NameEn ?? string.Empty,
                    ContractType = employmentPeriod.ContractType.NameEn ?? string.Empty,
                    Date = startDate,
                    EmployeeOrgUnitName = employmentPeriod.Employee.OrgUnit.Name,
                    EmployeeOrgUnitFlatName = employmentPeriod.Employee.OrgUnit.FlatName,
                    RootOrgUnitName = orgUnitDto.Name,
                    RootOrgUnitFlatName = orgUnitDto.FlatName,
                    OrgUnitPath = employmentPeriod.Employee.OrgUnit.Path,
                    IsVoluntarilyTurnover = 0,
                    IsOtherDepartureReasons = 0,
                    FteRatio = CalculateFteRatio(employmentPeriod, workingHoursInWeek),
                    Seniority = employmentPeriod.Employee.JobMatrixs != null && employmentPeriod.Employee.JobMatrixs.Any() ? employmentPeriod.Employee.JobMatrixs.Single().Level.ToString() : "unspecified",
                    JobProfile = employmentPeriod.Employee.JobProfiles != null && employmentPeriod.Employee.JobProfiles.Any() ? employmentPeriod.Employee.JobProfiles.First().Name : "unspecified",
                    NewHire = !periodType.HasFlag(EmoploymentPeriodType.EndOfEmployment) ? 1 : 0,
                    EmployeeRotation = 1,
                    Location = employmentPeriod.Employee.Location.Name,
                    PlaceOfWork = employmentPeriod.Employee.PlaceOfWork
                };
            }
        }

        public override AttritionRateDataSourceParametersDto GetDefaultParameters(ReportGenerationContext<AttritionRateDataSourceParametersDto> reportGenerationContext)
        {
            var today = _timeService.GetCurrentDate();

            return new AttritionRateDataSourceParametersDto
            {
                DateFrom = DateHelper.BeginningOfMonth(today).AddMonths(-12),
                DateTo = today,
                SkipWorkingStudents = true
            };
        }

        private void AssignSubOrgUnitName(
            IList<AttritionRateReportModel> attritionRateReportModelList,
            OrgUnitDto orgUnitDto)
        {
            var orgUnitDtos = _orgUnitService.GetOrgUnitsByParent(orgUnitDto.Id);

            foreach (var reportModel in attritionRateReportModelList)
            {
                if (orgUnitDtos.Any())
                {
                    if (reportModel.EmployeeOrgUnitName != orgUnitDto.Name)
                    {
                        var orgUnit = orgUnitDtos
                            .Where(a => a.Path.Length <= reportModel.OrgUnitPath.Length && reportModel.OrgUnitPath.StartsWith(a.Path))
                            .Single();

                        reportModel.SubOrgUnitName = orgUnit.Name;
                        reportModel.SubOrgUnitFlatName = orgUnit.FlatName;
                    }
                    else
                    {
                        reportModel.SubOrgUnitName = "";
                        reportModel.SubOrgUnitFlatName = orgUnitDto.FlatName;
                    }
                }
                else
                {
                    reportModel.SubOrgUnitName = reportModel.EmployeeOrgUnitName;
                    reportModel.SubOrgUnitFlatName = reportModel.EmployeeOrgUnitFlatName;
                }
            }
        }

        private decimal CalculateFteRatio(EmploymentPeriod employmentPeriod, int workingHoursInWeek)
        {
            return (employmentPeriod.MondayHours
                + employmentPeriod.TuesdayHours
                + employmentPeriod.WednesdayHours
                + employmentPeriod.ThursdayHours
                + employmentPeriod.FridayHours
                + employmentPeriod.SaturdayHours
                + employmentPeriod.SundayHours) / workingHoursInWeek;
        }

        private IEnumerable<EmploymentPeriod> FilterDataScope(
            IAllocationDbScope dbScope,
            AttritionRateDataSourceParametersDto attritionRateDataSourceParametersDto,
            OrgUnitDto orgUnitDto)
        {
            var rows = dbScope.EmploymentPeriods
                .Where(DateRangeHelper.OverlapingDateRange<EmploymentPeriod>(e => e.StartDate, e => e.EndDate, attritionRateDataSourceParametersDto.DateFrom, attritionRateDataSourceParametersDto.DateTo))
                .Where(a => !a.EndDate.HasValue || a.StartDate <= a.EndDate.Value);

            rows = rows.Where(a => a.Employee.OrgUnit.Path.StartsWith(orgUnitDto.Path));

            if (!attritionRateDataSourceParametersDto.ContractTypeIds.IsNullOrEmpty())
            {
                rows = rows.Where(p => attritionRateDataSourceParametersDto.ContractTypeIds.Contains(p.ContractTypeId));
            }

            if (attritionRateDataSourceParametersDto.LocationId.HasValue)
            {
                rows = rows.Where(a => a.Employee.LocationId == attritionRateDataSourceParametersDto.LocationId.Value);
            }

            if (attritionRateDataSourceParametersDto.PlaceOfWork.HasValue)
            {
                rows = rows.Where(a => a.Employee.PlaceOfWork == attritionRateDataSourceParametersDto.PlaceOfWork.Value);
            }

            if (attritionRateDataSourceParametersDto.SkipWorkingStudents)
            {
                var workingStudentsContractTypesActiveDirectoryCodes = _parameterService.GetParameter<string[]>(ParameterKeys.WorkingStudentsContractTypesActiveDirectoryCodes);

                foreach (string workingStudentsContractTypesADCode in workingStudentsContractTypesActiveDirectoryCodes)
                {
                    var workingStudentsContractTypeId = _allocationContractTypeService.GetContractTypeIdByActiveDirectoryCode(workingStudentsContractTypesADCode);

                    rows = rows.Where(p => p.ContractTypeId != workingStudentsContractTypeId);
                }
            }

            return rows;
        }
    }
}
