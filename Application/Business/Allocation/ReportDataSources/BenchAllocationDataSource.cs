﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Allocation.ReportModels;
using Smt.Atomic.Business.Allocation.Resources;
using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.ReportDataSources
{
    [Identifier("DataSource.BenchAllocationDataSource")]
    [RequireRole(SecurityRoleType.CanGenerateBenchAllocationReport)]
    [DefaultReportDefinition("BenchAllocationReport",
        nameof(ReportsResources.BenchAllocationReportName),
        nameof(ReportsResources.BenchAllocationReportDescription),
        MenuAreas.Allocations,
        MenuGroups.AllocationsReports,
        ReportingEngine.PivotTable, "Smt.Atomic.Business.Allocation.ReportTemplates.BenchAllocationPivotTable-config.js",
        typeof(ReportsResources))]
    public class BenchAllocationDataSource : BaseReportDataSource<BenchAllocationDataSourceParametersDto>
    {
        private readonly ITimeService _timeService;
        private readonly IUnitOfWorkService<IAllocationDbScope> _unitOfWorkService;
        private readonly IOrgUnitService _orgUnitService;
        private readonly ISystemParameterService _parameterService;
        private readonly IAllocationContractTypeService _allocationContractTypeService;
        private readonly ISystemParameterService _systemParameterService;

        public BenchAllocationDataSource(
            ITimeService timeService,
            IUnitOfWorkService<IAllocationDbScope> unitOfWorkService,
            IOrgUnitService orgUnitService,
            ISystemParameterService parameterService,
            IAllocationContractTypeService allocationContractTypeService,
            ISystemParameterService systemParameterService)
        {
            _timeService = timeService;
            _unitOfWorkService = unitOfWorkService;
            _orgUnitService = orgUnitService;
            _parameterService = parameterService;
            _allocationContractTypeService = allocationContractTypeService;
            _systemParameterService = systemParameterService;
        }

        private class EmployeesWithWeeklyAllocationField
        {
            public string Acronym { get; set; }

            public DateTime Week { get; set; }

            public OrgUnit OrgUnit { get; set; }

            public string LocationName { get; set; }

            public AllocationStatus AllocationStatus { get; set; }

            public ICollection<JobProfile> JobProfiles { get; set; }

            public ICollection<JobMatrixLevel> JobMatrixs { get; set; }

            public PlaceOfWork? PlaceOfWork { get; set; }

            public long EmployeeId { get; set; }
        }

        private class EmploymentPeriodFte
        {
            public decimal Fte { get; set; }

            public DateTime StartDate { get; set; }

            public DateTime? EndDate { get; set; }

            public long EmployeeId { get; set; }
        }

        public override object GetData(ReportGenerationContext<BenchAllocationDataSourceParametersDto> reportGenerationContext)
        {
            OrgUnitDto orgUnitDto = reportGenerationContext.Parameters.OrgUnitId.HasValue
                ? _orgUnitService.GetOrgUnitOrDefaultById(reportGenerationContext.Parameters.OrgUnitId.Value)
                : _orgUnitService.GetOrgUnitByCode(_parameterService.GetParameter<string>(ParameterKeys.OrganizationRootUnitCode));

            const string unspecified = "unspecified";

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var benchAllocationReportModels = FilterDataScope(unitOfWork.Repositories, reportGenerationContext.Parameters, orgUnitDto)
                    .OrderBy(a => a.Acronym)
                    .Select(a => new BenchAllocationReportModel
                    {
                        Acronym = a.Acronym,
                        FteRatio = 0,
                        EmployeeOrgUnitName = a.OrgUnit.Name,
                        EmployeeOrgUnitFlatName = a.OrgUnit.FlatName,
                        RootOrgUnitName = orgUnitDto.Name,
                        RootOrgUnitFlatName = orgUnitDto.FlatName,
                        OrgUnitPath = a.OrgUnit.Path,
                        Week = a.Week,
                        EmployeeId = a.EmployeeId,
                        JobProfile = a.JobProfiles.Any() ? a.JobProfiles.FirstOrDefault().Name : unspecified,
                        Seniority = a.JobMatrixs.Any() ? a.JobMatrixs.FirstOrDefault().Level.ToString() : unspecified,
                        Location = a.LocationName,
                        PlaceOfWork = a.PlaceOfWork
                    })
                    .ToList();

                AssignCalculatedFields(reportGenerationContext.Parameters, benchAllocationReportModels, orgUnitDto);

                return new BenchAllocationPresentationReportModel
                {
                    BenchAllocationReportModels = benchAllocationReportModels
                };
            }
        }

        private IQueryable<EmployeesWithWeeklyAllocationField> FilterDataScope(
           IAllocationDbScope dbScope,
           BenchAllocationDataSourceParametersDto benchAllocationDataSourceParametersDto,
           OrgUnitDto orgUnitDto)
        {
            var employees = dbScope.Employees.Where(e => e.OrgUnit.Path.StartsWith(orgUnitDto.Path) && e.IsProjectContributor);

            if (benchAllocationDataSourceParametersDto.ContractTypeIds.Any())
            {
                employees = employees.Where(e => e.EmploymentPeriods.Any(ep => benchAllocationDataSourceParametersDto.ContractTypeIds.Contains(ep.ContractTypeId)));
            }

            if (benchAllocationDataSourceParametersDto.LocationIds.Any())
            {
                employees = employees.Where(e => e.LocationId.HasValue && benchAllocationDataSourceParametersDto.LocationIds.Contains(e.LocationId.Value));
            }

            if (benchAllocationDataSourceParametersDto.JobProfileIds.Any())
            {
                employees = employees.Where(e => e.JobProfiles.Any(jp => benchAllocationDataSourceParametersDto.JobProfileIds.Contains(jp.Id)));
            }

            if (benchAllocationDataSourceParametersDto.PlaceOfWork.HasValue)
            {
                employees = employees.Where(e => e.PlaceOfWork == benchAllocationDataSourceParametersDto.PlaceOfWork.Value);
            }

            if (benchAllocationDataSourceParametersDto.SkipWorkingStudents)
            {
                var workingStudentsContractTypesActiveDirectoryCodes = _parameterService.GetParameter<string[]>(ParameterKeys.WorkingStudentsContractTypesActiveDirectoryCodes);
                var workingStudentsContractTypeIds = workingStudentsContractTypesActiveDirectoryCodes
                    .Select(c => _allocationContractTypeService.GetContractTypeIdByActiveDirectoryCode(c))
                    .ToList();

                employees = employees
                    .Where(e => e.EmploymentPeriods.Any(p => !workingStudentsContractTypeIds.Contains(p.ContractTypeId)));
            }

            var weeklyStatuses = dbScope.WeeklyAllocationStatus.Where(
                DateRangeHelper.IsDateInRange<WeeklyAllocationStatus>(benchAllocationDataSourceParametersDto.DateFrom, benchAllocationDataSourceParametersDto.DateTo, w => w.Week)
                .AsExpression());

            if (benchAllocationDataSourceParametersDto.StaffingStatuses.Any())
            {
                weeklyStatuses = weeklyStatuses.Where(w => benchAllocationDataSourceParametersDto.StaffingStatuses.Contains(w.StaffingStatus));
            }

            if (benchAllocationDataSourceParametersDto.EmployeeStatuses.Any())
            {
                employees = employees.Where(w => benchAllocationDataSourceParametersDto.EmployeeStatuses.Contains(w.CurrentEmployeeStatus));
            }

            return employees.Join(weeklyStatuses, e => e.Id, w => w.EmployeeId,
                (e, w) => new EmployeesWithWeeklyAllocationField
                {
                    Acronym = e.Acronym,
                    Week = w.Week,
                    OrgUnit = e.OrgUnit,
                    JobMatrixs = e.JobMatrixs,
                    LocationName = e.Location.Name,
                    JobProfiles = e.JobProfiles,
                    AllocationStatus = w.AllocationStatus,
                    PlaceOfWork = e.PlaceOfWork,
                    EmployeeId = e.Id
                });
        }

        private void AssignCalculatedFields(
            BenchAllocationDataSourceParametersDto benchAllocationDataSourceParametersDto,
            IList<BenchAllocationReportModel> benchAllocationReportModel,
            OrgUnitDto orgUnitDto)
        {
            var orgUnitDtos = _orgUnitService.GetOrgUnitsByParent(orgUnitDto.Id).ToList();
            var employeeIdList = benchAllocationReportModel.Select(a => a.EmployeeId).Distinct();
            var employmentPeriodFteList = GetEmploymentPeriodsWithUnusualWorkingHours(
                benchAllocationDataSourceParametersDto.DateFrom,
                benchAllocationDataSourceParametersDto.DateTo,
                employeeIdList);
            string previousAcronim = null;
            string previousSubOrgUnitName = null;
            string previousSubOrgUnitFlatName = null;

            foreach (var reportModel in benchAllocationReportModel)
            {
                reportModel.WeekOfTheYear = reportModel.Week.GetWeekOfYear();

                if (previousAcronim != reportModel.Acronym)
                {
                    if (orgUnitDtos.Any())
                    {
                        if (reportModel.EmployeeOrgUnitName != orgUnitDto.Name)
                        {
                            var orgUnit = orgUnitDtos
                               .Where(a => a.Path.Length <= reportModel.OrgUnitPath.Length && reportModel.OrgUnitPath.StartsWith(a.Path))
                               .Single();

                            reportModel.SubOrgUnitName = orgUnit.Name;
                            reportModel.SubOrgUnitFlatName = orgUnit.FlatName;
                        }
                        else
                        {
                            reportModel.SubOrgUnitName = "";
                            reportModel.SubOrgUnitFlatName = "";
                        }
                    }
                    else
                    {
                        reportModel.SubOrgUnitName = reportModel.EmployeeOrgUnitName;
                        reportModel.SubOrgUnitName = reportModel.EmployeeOrgUnitFlatName;
                    }

                    previousSubOrgUnitName = reportModel.SubOrgUnitName;
                    previousSubOrgUnitFlatName = reportModel.SubOrgUnitFlatName;
                    previousAcronim = reportModel.Acronym;
                }
                else
                {
                    reportModel.SubOrgUnitName = previousSubOrgUnitName;
                    reportModel.SubOrgUnitFlatName = previousSubOrgUnitFlatName;
                }

                var isDateInRange = DateRangeHelper.IsDateInRange<EmploymentPeriodFte>(
                    p => p.StartDate, p => p.EndDate,
                    DateComparer.LeftClosed | DateComparer.RightClosed);

                reportModel.Fte = employmentPeriodFteList
                    .Where(e => e.EmployeeId == reportModel.EmployeeId)
                    .Where(e => isDateInRange.Call(e, reportModel.Week))
                    .SingleOrDefault()?.Fte ?? 1;
            }
        }

        public override BenchAllocationDataSourceParametersDto GetDefaultParameters(ReportGenerationContext<BenchAllocationDataSourceParametersDto> reportGenerationContext)
        {
            var today = _timeService.GetCurrentDate();

            return new BenchAllocationDataSourceParametersDto
            {
                DateFrom = DateHelper.BeginningOfMonth(today).AddMonths(-12),
                DateTo = today,
                SkipWorkingStudents = true,
                StaffingStatuses = new[] { StaffingStatus.Free, StaffingStatus.Planned, StaffingStatus.PlannedAndConfirmed }
            };
        }

        private IList<EmploymentPeriodFte> GetEmploymentPeriodsWithUnusualWorkingHours(DateTime startDate, DateTime endDate, IEnumerable<long> employeeIdList)
        {
            var workingHoursInWeek = _systemParameterService.GetParameter<int>(ParameterKeys.WorkingHoursInWeek);

            if (workingHoursInWeek <=0 )
            {
                throw new ArgumentException($"System parameter {ParameterKeys.WorkingHoursInWeek} has to be greater than 0");
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.EmploymentPeriods
                    .Where(e => (e.MondayHours + e.ThursdayHours + e.WednesdayHours + e.ThursdayHours + e.FridayHours + e.SaturdayHours + e.SundayHours) != workingHoursInWeek)
                    .Where(DateRangeHelper.OverlapingDateRange<EmploymentPeriod>(e => e.StartDate, e => e.EndDate ?? DateTime.MaxValue, startDate, endDate))
                    .Where(e => employeeIdList.Contains(e.EmployeeId))
                    .Select(e => new EmploymentPeriodFte
                    {
                        StartDate = e.StartDate,
                        EndDate = e.EndDate,
                        EmployeeId = e.EmployeeId,
                        Fte = (e.MondayHours + e.ThursdayHours + e.WednesdayHours + e.ThursdayHours + e.FridayHours + e.SaturdayHours + e.SundayHours) / workingHoursInWeek
                    })
                    .ToList();
            }
        }
    }
}
