﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.Business.Allocation.Resources;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Allocation.ReportDataSources
{
    [Identifier("DataSource.WeeklyAllocationData")]
    [RequireRole(SecurityRoleType.CanGenerateWeeklyAllocationDataReport)]
    [DefaultReportDefinition(
        "WeeklyAllocationData",
        nameof(ReportsResources.WeeklyAllocationDataReportName),
        nameof(ReportsResources.WeeklyAllocationDataReportDescription),
        MenuAreas.Allocations,
        MenuGroups.AllocationsReports,
        ReportingEngine.MsExcelNoTemplate,
        new[]
        {
            "Smt.Atomic.Business.Allocation.ReportTemplates.WeeklyAllocationData.manifest",
            "Smt.Atomic.Business.Allocation.ReportTemplates.WeeklyAllocationData.sql"
        },
        typeof(ReportsResources))]

    internal class WeeklyAllocationData : SqlBasedDataSource
    {
        public WeeklyAllocationData(IUnitOfWorkService<IDbScope> unitOfWork, IPrincipalProvider principalProvider, IUnitOfWorkService<IAccountsDbScope> unitOfWorkService)
            : base(unitOfWork, principalProvider, unitOfWorkService)
        {
        }
    }
}
