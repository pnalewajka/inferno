﻿using System.Collections.Generic;
using System.Text;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Allocation.ReportModels;
using Smt.Atomic.Business.Allocation.Resources;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.Business.Allocation.ReportDataSources
{
    [Identifier("Report.ProjectPresentation")]
    [DefaultReportDefinition(
        "Presentation.ProjectDescription",
        nameof(ReportsResources.ProjectDescriptionPresentationName),
        nameof(ReportsResources.ProjectDescriptionPresentationDescription),
        MenuAreas.Organization,
        MenuGroups.Project,
        ReportingEngine.MsPowerPoint, 
        "Smt.Atomic.Business.Allocation.ReportTemplates.ProjectPresentationTemplateDescription.pptx",
        typeof(ReportsResources))]
    [DefaultReportDefinition(
        "Presentation.ProjectBusinessCase",
        nameof(ReportsResources.ProjectBusinessCasePresentationName),
        nameof(ReportsResources.ProjectBusinessCasePresentationDescription),
        MenuAreas.Organization,
        MenuGroups.Project,
        ReportingEngine.MsPowerPoint, 
        "Smt.Atomic.Business.Allocation.ReportTemplates.ProjectPresentationTemplateBusinessCase.pptx",
        typeof(ReportsResources))]
    [RequireRole(SecurityRoleType.CanGenerateProjectPresentations)]
    public class ProjectPresentationDataSource : BaseReportDataSource<ProjectReportParametersDto>
    {
        private readonly ITimeService _timeService;
        private readonly IProjectService _projectService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IEmployeeCardIndexDataService _employeeCardIndexDataService;
        private readonly ILocationCardIndexDataService _locationCardIndexDataService;

        public ProjectPresentationDataSource(
            ITimeService timeService,
            IProjectService projectService,
            IPrincipalProvider principalProvider,
            IEmployeeCardIndexDataService employeeCardIndexDataService,
            ILocationCardIndexDataService locationCardIndexDataService)
        {
            _timeService = timeService;
            _projectService = projectService;
            _principalProvider = principalProvider;
            _employeeCardIndexDataService = employeeCardIndexDataService;
            _locationCardIndexDataService = locationCardIndexDataService;
        }

        private static string GetPresentationFooter(IAtomicPrincipal currentPrincipal)
        {
            var footerBuilder = new StringBuilder();
            footerBuilder.AppendFormat(ProjectResources.GeneratedByFormat, currentPrincipal.FirstName, currentPrincipal.LastName);
            footerBuilder.AppendLine();

            if (!string.IsNullOrWhiteSpace(currentPrincipal.JobTitle))
            {
                footerBuilder.AppendLine(currentPrincipal.JobTitle);
            }

            footerBuilder.AppendLine(currentPrincipal.Email);

            return footerBuilder.ToString();
        }

        private string GetUserLocation(IAtomicPrincipal currentPrincipal)
        {
            string currentUserLocation = null;

            if (currentPrincipal.EmployeeId > 0)
            {
                var location = _locationCardIndexDataService.GetLocationForEmployeeOrDefault(currentPrincipal.EmployeeId);
                currentUserLocation = location?.Name;
            }

            return currentUserLocation;
        }

        public override object GetData(ReportGenerationContext<ProjectReportParametersDto> context)
        {
            var projectDtos = new List<ProjectReportModel>();
            var projects = _projectService.GetProjecListByIdsForPresentationGeneration(context.Parameters.ProjectIds);

            foreach (var project in projects)
            {
                projectDtos.Add(new ProjectReportModel()
                {
                    Name = project.Name,
                    ClientShortName = project.ClientShortName,
                    ProjectShortName = project.ProjectShortName,
                    PetsCode = project.PetsCode,
                    JiraCode = project.JiraCode,
                    JiraId = project.JiraId,
                    ProjectFeatures = project.ProjectFeatures.GetDescription(),
                    Description = project.Description,
                    OrgUnits = project.OrgUnits,
                    ProjectManager = project.ProjectManager,
                    Industries = project.Industries,
                    StartDate = project.StartDate?.ToShortDateString() ?? string.Empty,
                    EndDate = project.EndDate?.ToShortDateString() ?? ProjectResources.Ongoing,
                    ProjectStatus = project.ProjectStatus.GetDescription(),
                    GenericName = project.GenericName,
                    KeyTechnologies = project.KeyTechnologies,
                    Technologies = project.Technologies,
                    ServiceLines = project.ServiceLines,
                    SoftwareCategories = project.SoftwareCategories,
                    Tags = project.Tags,
                    CurrentPhase = project.CurrentPhase.GetDescription(),
                    Disclosures = project.Disclosures.GetDescription(),
                    Picture = project.Picture,
                    HasClientReferences = project.HasClientReferences ? ProjectResources.Yes : ProjectResources.No,
                    BusinessCase = project.BusinessCase
                });
            }

            var currentPrincipal = _principalProvider.IsSet ? _principalProvider.Current : AtomicPrincipal.Anonymous;
            var currentUserLocation = GetUserLocation(currentPrincipal);
            var presentationViewModel = new PresentationReportModel()
            {
                ProjectPresentationViewModels = projectDtos,
                GenerationDate = _timeService.GetCurrentDate().ToShortDateString(),
                AuthorFirstName = currentPrincipal.FirstName,
                AuthorLastName = currentPrincipal.LastName,
                AuthorLocation = currentUserLocation,
                Footer = GetPresentationFooter(currentPrincipal)
            };

            return presentationViewModel;
        }

        public override ProjectReportParametersDto GetDefaultParameters(ReportGenerationContext<ProjectReportParametersDto> reportGenerationContext)
        {
            return new ProjectReportParametersDto();
        }
    }
}
