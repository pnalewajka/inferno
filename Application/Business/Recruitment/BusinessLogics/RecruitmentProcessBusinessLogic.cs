﻿using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.BusinessLogics
{
    public class RecruitmentProcessBusinessLogic
    {
        public static readonly BusinessLogic<RecruitmentProcess, string> CandidateFullName =
            new BusinessLogic<RecruitmentProcess, string>(
                process => process.Candidate != null ? CandidateBusinessLogic.FullName.Call(process.Candidate) : null);

        public static readonly BusinessLogic<RecruitmentProcess, string> DecisionMakerFullName =
            new BusinessLogic<RecruitmentProcess, string>(
                process => process.JobOpening != null && process.JobOpening.DecisionMakerEmployee != null ? EmployeeBusinessLogic.FullName.Call(process.JobOpening.DecisionMakerEmployee) : null);

        public static readonly BusinessLogic<RecruitmentProcess, string> DisplayName =
            new BusinessLogic<RecruitmentProcess, string>(
                process => CandidateFullName.Call(process)
                      + " → "
                      + (process.JobOpening != null
                         ? process.JobOpening.PositionName + " (" + process.JobOpening.HiringMode + ")"
                         : null));

        public static readonly BusinessLogic<RecruitmentProcess, long, long, bool> IsOwnRecruitmentProcess =
            new BusinessLogic<RecruitmentProcess, long, long, bool>(
                (process, userId, employeeId) => process.RecruiterId == employeeId
                    || process.JobOpening.DecisionMakerEmployeeId == employeeId
                    || process.CreatedById == userId
                    || process.ProcessSteps.Any(s => s.AssignedEmployeeId == employeeId
                                                     || s.OtherAttendingEmployees.Any(e => e.Id == employeeId)));

        public static readonly BusinessLogic<RecruitmentProcess, long, long, bool> IsMatchedProcess =
            new BusinessLogic<RecruitmentProcess, long, long, bool>(
                (process, candidateId, jobOpeningId) => process.Status != RecruitmentProcessStatus.Closed
                    && !process.IsDeleted
                    && process.CandidateId == candidateId
                    && process.JobOpeningId == jobOpeningId);

        public static readonly BusinessLogic<RecruitmentProcess, bool> IsHighlyConfidential =
            new BusinessLogic<RecruitmentProcess, bool>(
                process => process.Candidate.IsHighlyConfidential || process.JobOpening.IsHighlyConfidential);

        public static readonly BusinessLogic<RecruitmentProcess, bool> CanBeReopened =
            new BusinessLogic<RecruitmentProcess, bool>(
                process => process.Status == RecruitmentProcessStatus.Closed
                    && ((process.ClosedReason == RecruitmentProcessClosedReason.Hired && process.OnboardingRequest != null
                    && (process.OnboardingRequest != null
                        && (process.OnboardingRequest.Request.Status == RequestStatus.Draft || process.OnboardingRequest.Request.Status == RequestStatus.Rejected))))
                    || process.ClosedReason != RecruitmentProcessClosedReason.Hired);

        public static readonly BusinessLogic<RecruitmentProcess, bool> IsAffectedByJobOpeningChanges =
            new BusinessLogic<RecruitmentProcess, bool>(
                process => process.Status == RecruitmentProcessStatus.Active);
    }
}