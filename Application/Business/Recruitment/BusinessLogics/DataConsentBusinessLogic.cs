﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.GDPR;

namespace Smt.Atomic.Business.Recruitment.BusinessLogics
{
    public static class DataConsentBusinessLogic
    {
        public static readonly BusinessLogic<DataConsent, DateTime, bool> IsExpired =
            new BusinessLogic<DataConsent, DateTime, bool>(
                (dataConsent, today) => dataConsent.ExpiresOn != null && dataConsent.ExpiresOn < today);

        public static readonly BusinessLogic<DateTime?, DateTime, bool> IsNotExpired =
            new BusinessLogic<DateTime?, DateTime, bool>(
                (expiresOn, today) => expiresOn == null || expiresOn >= today);

        public static readonly BusinessLogic<DataConsent, bool> IsCandidateConsent =
            new BusinessLogic<DataConsent, bool>(
                dataConsent => dataConsent.Type == DataConsentType.CandidateConsentGeneral
                               || dataConsent.Type == DataConsentType.CandidateConsentSpecificPosition
                               || dataConsent.Type == DataConsentType.CandidateConsentLegacy
                               || dataConsent.Type == DataConsentType.CandidateConsentSocialNetwork
                               || dataConsent.Type == DataConsentType.CandidateConsentInitialContact);

        public static readonly BusinessLogic<DataConsentType, bool> IsCandidateConsentType =
            new BusinessLogic<DataConsentType, bool>(
                type => type == DataConsentType.CandidateConsentGeneral
                        || type == DataConsentType.CandidateConsentSpecificPosition
                        || type == DataConsentType.CandidateConsentLegacy
                        || type == DataConsentType.CandidateConsentSocialNetwork
                        || type == DataConsentType.CandidateConsentInitialContact);

        public static readonly BusinessLogic<DataConsentType, bool> DoesConsentTypeRequireExpirationDate =
            new BusinessLogic<DataConsentType, bool>(
                type => type == DataConsentType.CandidateConsentSpecificPosition
                        || type == DataConsentType.CandidateConsentSocialNetwork
                        || type == DataConsentType.CandidateConsentInitialContact);

        public static readonly BusinessLogic<DataConsentType?, bool> DoesConsentTypeRequireAttachment =
            new BusinessLogic<DataConsentType?, bool>(
                type => type == DataConsentType.CandidateConsentGeneral
                        || type == DataConsentType.CandidateConsentSpecificPosition
                        || type == DataConsentType.CandidateConsentLegacy
                        || type == DataConsentType.RecommendingPersonConsentRecommendation);

        public static readonly BusinessLogic<DataConsentType?, bool> DoesConsentTypeRequireScope =
            new BusinessLogic<DataConsentType?, bool>(
                type => type == DataConsentType.CandidateConsentSpecificPosition);

        public static readonly BusinessLogic<DataConsent, bool> IsRecommendingPersonConsent =
            new BusinessLogic<DataConsent, bool>(
                dataConsent => dataConsent.Type == DataConsentType.RecommendingPersonConsentRecommendation
                               || dataConsent.Type == DataConsentType.RecommendingEmployeeConsent);

        public static readonly BusinessLogic<DataConsentType, bool> IsRecommendingPersonConsentType =
            new BusinessLogic<DataConsentType, bool>(
                type => type == DataConsentType.RecommendingPersonConsentRecommendation
                        || type == DataConsentType.RecommendingEmployeeConsent);

        public static readonly BusinessLogic<DataConsent, DateTime, bool> IsNotExpiredCandidateConsent =
            new BusinessLogic<DataConsent, DateTime, bool>(
                (dataConsent, today) =>
                    IsCandidateConsent.Call(dataConsent)
                    && IsNotExpired.Call(dataConsent.ExpiresOn, today));

        public static readonly BusinessLogic<DataConsent, DateTime, bool> IsExpiredCandidateConsent =
            new BusinessLogic<DataConsent, DateTime, bool>(
                (dataConsent, today) =>
                    IsCandidateConsent.Call(dataConsent)
                    && IsExpired.Call(dataConsent, today));

        public static readonly BusinessLogic<DataConsent, DateTime, bool> IsNotExpiredRecommendingPersonConsent =
            new BusinessLogic<DataConsent, DateTime, bool>(
                (dataConsent, today) =>
                    IsRecommendingPersonConsent.Call(dataConsent) && IsNotExpired.Call(dataConsent.ExpiresOn, today));

        public static readonly BusinessLogic<DataConsent, DateTime, bool> IsExpiredRecommendingPersonConsent =
            new BusinessLogic<DataConsent, DateTime, bool>(
                (dataConsent, today) =>
                    IsRecommendingPersonConsent.Call(dataConsent) && IsExpired.Call(dataConsent, today));

        public static bool IsEffectivelyGeneralCandidateConsent(DataConsentType consentType)
        {
            return consentType == DataConsentType.CandidateConsentGeneral
                || consentType == DataConsentType.CandidateConsentLegacy
                || consentType == DataConsentType.RecommendingEmployeeConsent;
        }

        public static bool CanOverrideConsent(DataConsentType superior, DataConsentType inferior)
        {
            switch (inferior)
            {
                case DataConsentType.CandidateConsentInitialContact:
                    return superior == DataConsentType.CandidateConsentInitialContact
                        || superior == DataConsentType.CandidateConsentSpecificPosition
                        || IsEffectivelyGeneralCandidateConsent(superior);

                case DataConsentType.CandidateConsentSocialNetwork:
                    return superior == DataConsentType.CandidateConsentSocialNetwork
                        || superior == DataConsentType.CandidateConsentSpecificPosition
                        || IsEffectivelyGeneralCandidateConsent(superior);

                case DataConsentType.CandidateConsentSpecificPosition:
                    return superior == DataConsentType.CandidateConsentSpecificPosition
                        || IsEffectivelyGeneralCandidateConsent(superior);
            }

            if (IsEffectivelyGeneralCandidateConsent(inferior))
            {
                return IsEffectivelyGeneralCandidateConsent(superior);
            }

            return false;
        }
    }
}
