﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.BusinessLogics
{
    public class MentionedNoteBusinessLogic
    {
        public static readonly BusinessLogic<IMentionedNote, long, bool> IsMentionedEmployee =
            new BusinessLogic<IMentionedNote, long, bool>(
                (note, employeeId) => note.MentionedEmployees.Any(me => me.Id == employeeId));
    }
}
