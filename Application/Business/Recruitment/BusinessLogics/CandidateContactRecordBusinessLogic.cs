﻿using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.BusinessLogics
{
    public class CandidateContactRecordBusinessLogic
    {
        public static BusinessLogic<CandidateContactRecord, long, long, bool> IsOwnCandidateContactRecord =
            new BusinessLogic<CandidateContactRecord, long, long, bool>((contactRecord, userId, employeeId)
                => contactRecord.CreatedById == userId || contactRecord.ContactedById == employeeId);
    }
}
