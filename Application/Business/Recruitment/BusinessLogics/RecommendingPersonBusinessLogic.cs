﻿using System;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.BusinessLogics
{
    public static class RecommendingPersonBusinessLogic
    {
        public static BusinessLogic<RecommendingPerson, DateTime, bool> HasValidConsent =
            new BusinessLogic<RecommendingPerson, DateTime, bool>(
                (r, today) => r.DataOwners.Any(o => o.DataConsents
                                                     .Any(dc => DataConsentBusinessLogic.IsNotExpiredRecommendingPersonConsent.Call(dc, today))));

        public static BusinessLogic<RecommendingPerson, string> FullName = new BusinessLogic<RecommendingPerson, string>(
            r => r.FirstName + " " + r.LastName);
    }
}
