﻿using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.BusinessLogics
{
    public static class TechnicalReviewBusinessLogic
    {
        public static BusinessLogic<TechnicalReview, string> CandidateFullName =
            new BusinessLogic<TechnicalReview, string>(
                c => c.RecruitmentProcess != null && c.RecruitmentProcess.Candidate != null 
                ? CandidateBusinessLogic.FullName.Call(c.RecruitmentProcess.Candidate) : null);

        public static BusinessLogic<TechnicalReview, string> PositionName =
           new BusinessLogic<TechnicalReview, string>(
               c => c.RecruitmentProcess != null && c.RecruitmentProcess.JobOpening != null
               ? c.RecruitmentProcess.JobOpening.PositionName : null);

        public static BusinessLogic<TechnicalReview, string> AssigneeFullName =
           new BusinessLogic<TechnicalReview, string>(
               c => c.RecruitmentProcessStep != null && c.RecruitmentProcessStep.AssignedEmployee != null
               ? EmployeeBusinessLogic.FullName.Call(c.RecruitmentProcessStep.AssignedEmployee) : null);
    }
}
