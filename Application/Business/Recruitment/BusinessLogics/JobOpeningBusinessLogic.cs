﻿using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.BusinessLogics
{
    public static class JobOpeningBusinessLogic
    {
        public static BusinessLogic<JobOpening, int> VacancyTotal = new BusinessLogic<JobOpening, int>
            (o => o.VacancyLevel1 + o.VacancyLevel2 + o.VacancyLevel3 + o.VacancyLevel4 + o.VacancyLevel5 + o.VacancyLevelNotApplicable);

        public static BusinessLogic<JobOpening, long, long, bool> IsMyJobOpening = new BusinessLogic<JobOpening, long, long, bool>(
                (jobOpening, employeeId, userId) => jobOpening.AcceptingEmployeeId == employeeId
                                                    || jobOpening.DecisionMakerEmployeeId == employeeId
                                                    || jobOpening.Recruiters.Any(r => r.Id == employeeId)
                                                    || jobOpening.RecruitmentOwners.Any(r => r.Id == employeeId)
                                                    || jobOpening.CreatedById == userId
                                                    || jobOpening.Watchers.Any(w => w.Id == employeeId));

        public static BusinessLogic<JobOpening, string> DecisionMakerFullName =
            new BusinessLogic<JobOpening, string>(
                o => o.DecisionMakerEmployee != null ? EmployeeBusinessLogic.FullName.Call(o.DecisionMakerEmployee) : null);

        public static BusinessLogic<JobOpening, JobOpeningFiltersDto, bool> FitlerJobOpenings =
            new BusinessLogic<JobOpening, JobOpeningFiltersDto, bool>(
                (jobOpening, filterDto) => (!filterDto.JobProfileIds.Any() || jobOpening.JobProfiles.Any(s => filterDto.JobProfileIds.Any(d => d == s.Id)))
                                        && (!filterDto.CityIds.Any() || jobOpening.Cities.Any(c => filterDto.CityIds.Any(d => d == c.Id)))
                                        && (filterDto.DecisionMakerId == default(long?) || jobOpening.DecisionMakerEmployeeId == filterDto.DecisionMakerId)
                                        && (filterDto.OrgUnitId == default(long?) || jobOpening.OrgUnitId == filterDto.OrgUnitId)
                                        && (!filterDto.Priority.HasValue || jobOpening.Priority == filterDto.Priority.Value)
                                        && (!filterDto.Status.HasValue || jobOpening.Status == filterDto.Status.Value)
                                        && (!filterDto.HiringMode.Any() || filterDto.HiringMode.Contains((int)jobOpening.HiringMode))
                                        && (!filterDto.OpenedOnFrom.HasValue || jobOpening.RoleOpenedOn >= filterDto.OpenedOnFrom.Value)
                                        && (!filterDto.OpenedOnTo.HasValue || jobOpening.RoleOpenedOn <= filterDto.OpenedOnTo.Value)
                                        && (!filterDto.AgencyEmploymentMode.HasValue || jobOpening.AgencyEmploymentMode == filterDto.AgencyEmploymentMode.Value));
    }
}
