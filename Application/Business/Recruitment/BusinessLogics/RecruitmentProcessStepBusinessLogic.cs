﻿using System.Linq;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.BusinessLogics
{
    public class RecruitmentProcessStepBusinessLogic
    {
        public static readonly BusinessLogic<RecruitmentProcessStep, bool> IsExistingToDoOrDone =
            new BusinessLogic<RecruitmentProcessStep, bool>(
                step => !step.IsDeleted
                     && (step.Status == RecruitmentProcessStepStatus.ToDo || step.Status == RecruitmentProcessStepStatus.Done));

        public static readonly BusinessLogic<RecruitmentProcessStepType, bool> IsMakingProcessMatureForHiringManager =
            new BusinessLogic<RecruitmentProcessStepType, bool>(
              type => type == RecruitmentProcessStepType.HiringManagerResumeApproval
                || type == RecruitmentProcessStepType.HiringManagerInterview
                || type == RecruitmentProcessStepType.ClientResumeApproval
                || type == RecruitmentProcessStepType.ClientInterview
                || type == RecruitmentProcessStepType.HiringDecision
                || type == RecruitmentProcessStepType.ContractNegotiations);

        public static readonly BusinessLogic<RecruitmentProcessStep, long, bool> IsOwnRecruitmentProcessStep =
            new BusinessLogic<RecruitmentProcessStep, long, bool>(
                (step, employeeId) => step.AssignedEmployeeId == employeeId
                                      || step.OtherAttendingEmployees.Any(e => e.Id == employeeId));

        public static readonly BusinessLogic<RecruitmentProcessStep, long, long, bool> IsOwnRecruitmentProcess =
            new BusinessLogic<RecruitmentProcessStep, long, long, bool>(
                (step, userId, employeeId) => RecruitmentProcessBusinessLogic.IsOwnRecruitmentProcess.Call(step.RecruitmentProcess, userId, employeeId));

        public static readonly BusinessLogic<RecruitmentProcessStep, bool> IsSuitableForNotifications =
            new BusinessLogic<RecruitmentProcessStep, bool>(
                (step) => (step.AssignedEmployeeId != null || step.OtherAttendingEmployees.Any())
                            && step.Status == RecruitmentProcessStepStatus.Draft || step.Status == RecruitmentProcessStepStatus.ToDo
                            && !step.IsDeleted);

        public static readonly BusinessLogic<RecruitmentProcessStep, long, bool> CanEditStep =
            new BusinessLogic<RecruitmentProcessStep, long, bool>(
                (step, employeeId) => (step.Status == RecruitmentProcessStepStatus.Draft || step.Status == RecruitmentProcessStepStatus.ToDo)
                                       && (step.AssignedEmployeeId == employeeId || step.RecruitmentProcess.RecruiterId == employeeId));

        public static readonly BusinessLogic<RecruitmentProcessStepType, bool> IsClientSpecificStep =
            new BusinessLogic<RecruitmentProcessStepType, bool>(
              type => type == RecruitmentProcessStepType.ClientResumeApproval
                || type == RecruitmentProcessStepType.ClientInterview);

        public static readonly BusinessLogic<RecruitmentProcessStepType, bool> IsHiringManagerSpecificStep =
            new BusinessLogic<RecruitmentProcessStepType, bool>(
              type => type == RecruitmentProcessStepType.HiringManagerInterview
                || type == RecruitmentProcessStepType.HiringManagerResumeApproval);

        public static readonly BusinessLogic<RecruitmentProcessStep, bool> IsActiveStep =
            new BusinessLogic<RecruitmentProcessStep, bool>(
                step => (step.Status == RecruitmentProcessStepStatus.Draft || step.Status == RecruitmentProcessStepStatus.ToDo)
                    && !step.IsDeleted);
    }
}
