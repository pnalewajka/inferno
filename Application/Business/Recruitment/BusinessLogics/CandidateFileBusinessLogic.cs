﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;

namespace Smt.Atomic.Business.Recruitment.BusinessLogics
{
    public class CandidateFileBusinessLogic
    {
        public static BusinessLogic<RecruitmentDocumentType, bool> ShouldTrackRecentActivity =
         new BusinessLogic<RecruitmentDocumentType, bool>(documentType
             => documentType == RecruitmentDocumentType.OriginalResume
                || documentType == RecruitmentDocumentType.JobReference
                || documentType == RecruitmentDocumentType.Portfolio
                || documentType == RecruitmentDocumentType.CoverLetter
                || documentType == RecruitmentDocumentType.Certificate);
    }
}
