﻿using System;
using System.Linq;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.BusinessLogics
{
    public static class CandidateBusinessLogic
    {
        public static BusinessLogic<Candidate, DateTime, bool> HasValidConsent =
            new BusinessLogic<Candidate, DateTime, bool>(
                (c, today) => c.DataOwners.Any(o => o.DataConsents
                                                     .Any(dc => DataConsentBusinessLogic.IsCandidateConsent.Call(dc)
                                                                && !DataConsentBusinessLogic.IsExpired.Call(dc, today))));

        public static BusinessLogic<Candidate, string> FullName = new BusinessLogic<Candidate, string>(
            c => c.FirstName + " " + c.LastName);

        public static BusinessLogic<Candidate, DateTime> RecentActivityOn = new BusinessLogic<Candidate, DateTime>(
            c => c.LastActivityOn.HasValue ? c.LastActivityOn.Value : DateTime.Now);

        public static BusinessLogic<Candidate, DateTime, bool> IsFollowUpNeeded =
            new BusinessLogic<Candidate, DateTime, bool>(
                (c, today) => !c.IsAnonymized && c.NextFollowUpDate != null && c.NextFollowUpDate < today
                && (c.LastFollowUpReminderDate == null || c.LastFollowUpReminderDate < c.NextFollowUpDate));

        public static BusinessLogic<CandidateFile, RecruitmentDocumentType, bool> FileIsNotDeletedAndOfGivenType =
                new BusinessLogic<CandidateFile, RecruitmentDocumentType, bool>(
                    (f, documentType) => !f.IsDeleted && f.DocumentType == documentType);

        public static readonly BusinessLogic<Candidate, long, bool> HasActiveProcessesForJobOpening =
            new BusinessLogic<Candidate, long, bool>(
                (candidate, jobOpeningId) => candidate.RecruitmentProcesses.Any(r => r.JobOpeningId == jobOpeningId && !r.IsDeleted && r.Status != RecruitmentProcessStatus.Closed));

        public static BusinessLogic<Candidate, CandidateAdvancedFiltersDto, bool> CandidatePropertiesFiltering =
            new BusinessLogic<Candidate, CandidateAdvancedFiltersDto, bool>(
                (candidate, filterDto) =>
                (!filterDto.JobProfileIds.Any() || candidate.JobProfiles.Any(s => filterDto.JobProfileIds.Any(d => d == s.Id)))
                && (!filterDto.CityIds.Any() || candidate.Cities.Any(s => filterDto.CityIds.Any(d => d == s.Id)))
                && (!filterDto.EmployeeStatuses.Any() || filterDto.EmployeeStatuses.Contains((long)candidate.EmployeeStatus))
                && (!filterDto.LanguageId.HasValue || candidate.Languages.Any(l =>
                        filterDto.LanguageId == l.LanguageId && filterDto.LanguageReferenceLevelId <= l.Level))
                && (!filterDto.CoordinatorIds.Any() || filterDto.CoordinatorIds.Any(c => candidate.ConsentCoordinatorId == c || candidate.ContactCoordinatorId == c))
                && (!filterDto.LastContactFrom.HasValue || (candidate.CandidateContactRecords.Max(c => c.ContactedOn) != null
                    && candidate.CandidateContactRecords.Max(c => c.ContactedOn) >= filterDto.LastContactFrom.Value))
                && (!filterDto.LastContactTo.HasValue || (candidate.CandidateContactRecords.Max(c => c.ContactedOn) != null
                    && candidate.CandidateContactRecords.Max(c => c.ContactedOn) <= filterDto.LastContactTo.Value)));
    }
}
