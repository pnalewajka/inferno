﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Recruitment.ChoreProviders;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification;
using Smt.Atomic.Business.Recruitment.Services;
using Smt.Atomic.Business.Recruitment.Services.Notification;
using Smt.Atomic.Business.Recruitment.Services.Notification.RecipientsProviders;
using Smt.Atomic.Business.Recruitment.Services.Notification.RecruitmentProcessNoteServices;
using Smt.Atomic.Business.Recruitment.Services.Notification.RecruitmentProcessServices;
using Smt.Atomic.Business.Recruitment.Services.Notification.RecruitmentProcessStepServices;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.Recruitment
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            if (containerType == ContainerType.WebApp)
            {
                container.Register(Component.For<ICandidateCardIndexDataService>().ImplementedBy<CandidateCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ICandidateContactRecordCardIndexDataService>().ImplementedBy<CandidateContactRecordCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IJobOpeningCardIndexDataService>().ImplementedBy<JobOpeningCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IJobOpeningPickerCardIndexDataService>().ImplementedBy<JobOpeningPickerCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IRecruitmentProcessCardIndexDataService>().ImplementedBy<RecruitmentProcessCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ICandidateRecruitmentProcessCardIndexDataService>().ImplementedBy<CandidateRecruitmentProcessCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IRecruitmentProcessStepCardIndexDataService>().ImplementedBy<RecruitmentProcessStepCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ITechnicalReviewCardIndexDataService>().ImplementedBy<TechnicalReviewCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ICandidateTechnicalReviewCardIndexDataService>().ImplementedBy<CandidateTechnicalReviewCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IApplicationOriginCardIndexDataService>().ImplementedBy<ApplicationOriginCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IApplicationOriginPickerCardIndexDataService>().ImplementedBy<ApplicationOriginPickerCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IRecommendingPersonCardIndexDataService>().ImplementedBy<RecommendingPersonCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IRecommendingPersonPickerCardIndexDataService>().ImplementedBy<RecommendingPersonPickerCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IJobApplicationCardIndexDataService>().ImplementedBy<JobApplicationCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ICandidatePickerCardIndexDataService>().ImplementedBy<CandidatePickerCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IInboundEmailPickerCardIndexDataService>().ImplementedBy<InboundEmailPickerCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IJobOpeningService>().ImplementedBy<JobOpeningService>().LifestyleTransient());
                container.Register(Component.For<IRecruitmentProcessPickerCardIndexDataService>().ImplementedBy<RecruitmentProcessPickerCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IJobOpeningRecruitmentProcessCardIndexDataService>().ImplementedBy<JobOpeningRecruitmentProcessCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ICandidateNoteCardIndexDataService>().ImplementedBy<CandidateNoteCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IJobOpeningNoteCardIndexDataService>().ImplementedBy<JobOpeningNoteCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IRecruitmentProcessNoteCardIndexDataService>().ImplementedBy<RecruitmentProcessNoteCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IRecommendingPersonNoteCardIndexDataService>().ImplementedBy<RecommendingPersonNoteCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IInboundEmailCardIndexDataService>().ImplementedBy<InboundEmailCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IInboundEmailDocumentCardIndexDataService>().ImplementedBy<InboundEmailDocumentCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IInboundEmailDocumentPickerCardIndexDataService>().ImplementedBy<InboundEmailDocumentPickerCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ICandidateFileCardIndexDataService>().ImplementedBy<AllCandidateFilesCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IRecommendingPersonContactRecordCardIndexDataService>().ImplementedBy<RecommendingPersonContactRecordCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IJobOpeningNoteService>().ImplementedBy<JobOpeningNoteService>().LifestyleTransient());
                container.Register(Component.For<ICandidateService>().ImplementedBy<CandidateService>().LifestyleTransient());
                container.Register(Component.For<IInboundEmailValueCardIndexDataService>().ImplementedBy<InboundEmailValueCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IRecommendingPersonService>().ImplementedBy<RecommendingPersonService>().LifestyleTransient());
                container.Register(Component.For<IRecruitmentProcessService>().ImplementedBy<RecruitmentProcessService>().LifestyleTransient());
                container.Register(Component.For<IRecruitmentProcessStepService>().ImplementedBy<RecruitmentProcessStepService>().LifestyleTransient());
                container.Register(Component.For<ICandidateFileService>().ImplementedBy<CandidateFileService>().LifestyleTransient());
                container.Register(Component.For<IRecruitmentProcessStepNotificationService>().ImplementedBy<RecruitmentProcessStepNotificationService>().LifestyleTransient());
                container.Register(Component.For<IJobApplicationService>().ImplementedBy<JobApplicationService>().LifestyleTransient());
                container.Register(Component.For<ICandidateNotificationService>().ImplementedBy<CandidateNotificationService>().LifestyleTransient());
                container.Register(Component.For<IJobApplicationPickerCardIndexDataService>().ImplementedBy<JobApplicationPickerCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IApplicationOriginService>().ImplementedBy<ApplicationOriginService>().LifestyleTransient());
                container.Register(Component.For<IJobOpeningNotificationService>().ImplementedBy<JobOpeningNotificationService>().LifestyleTransient());
                container.Register(Component.For<IJobOpeningRecipientsProvider>().ImplementedBy<JobOpeningRecipientsProvider>().LifestyleTransient());
                container.Register(Component.For<IRecruitmentProcessNoteNotificationService>().ImplementedBy<RecruitmentProcessNoteNotificationService>().LifestyleTransient());
                container.Register(Component.For<IRecruitmentProcessNotificationService>().ImplementedBy<RecruitmentProcessNotificationService>().LifestyleTransient());
                container.Register(Component.For<IRecruitmentProcessRecipientsProvider>().ImplementedBy<RecruitmentProcessRecipientsProvider>().LifestyleTransient());
                container.Register(Component.For<IRecruitmentProcessStepRecipientsProvider>().ImplementedBy<RecruitmentProcessStepRecipientsProvider>().LifestyleTransient());
                container.Register(Component.For<IInboundEmailNotificationService>().ImplementedBy<InboundEmailNotificationService>().LifestyleTransient());
                container.Register(Component.For<IJobOpeningNoteNotificationService>().ImplementedBy<JobOpeningNoteNotificationService>().LifestyleTransient());
                container.Register(Component.For<IJobOpeningNoteRecipientProvider>().ImplementedBy<JobOpeningNoteRecipientProvider>().LifestyleTransient());
                container.Register(Component.For<ICandidateDataConsentFileCardIndexDataService>().ImplementedBy<CandidateDataConsentFileCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ICandidateNoteRecipientProvider>().ImplementedBy<CandidateNoteRecipientProvider>().LifestyleTransient());
                container.Register(Component.For<ICandidateRecipientProvider>().ImplementedBy<CandidateRecipientProvider>().LifestyleTransient());
                container.Register(Component.For<ICandidateNoteNotificationService>().ImplementedBy<CandidateNoteNotificationService>().LifestyleTransient());
                container.Register(Component.For<IRecruitmentProcessNoteRecipientProvider>().ImplementedBy<RecruitmentProcessNoteRecipientsProvider>().LifestyleTransient());
                container.Register(Component.For<IRecruitmentProcessHistoryService>().ImplementedBy<RecruitmentProcessHistoryService>().LifestyleTransient());
                container.Register(Component.For<IJobOpeningHistoryService>().ImplementedBy<JobOpeningHistoryService>().LifestyleTransient());
                container.Register(Component.For<IInboundEmailService>().ImplementedBy<InboundEmailService>().LifestyleTransient());
                container.Register(Component.For<IChoreProvider>().ImplementedBy<InboundEmailChoreProvider>().LifestylePerWebRequest());
            }

            if (containerType == ContainerType.JobScheduler)
            {
                container.Register(Component.For<IInboundEmailService>().ImplementedBy<InboundEmailService>().LifestyleTransient());
                container.Register(Component.For<ICandidateService>().ImplementedBy<CandidateService>().LifestyleTransient());
                container.Register(Component.For<ICandidateNoteService>().ImplementedBy<CandidateNoteService>().LifestyleTransient());
                container.Register(Component.For<IRecommendingPersonService>().ImplementedBy<RecommendingPersonService>().LifestyleTransient());
                container.Register(Component.For<IRecruitmentProcessStepNotificationService>().ImplementedBy<RecruitmentProcessStepNotificationService>().LifestyleTransient());
            }
        }
    }
}