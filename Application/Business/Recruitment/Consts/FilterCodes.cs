﻿namespace Smt.Atomic.Business.Recruitment.Consts
{
    public static class FilterCodes
    {
        public const string StatusDefault = "default";
        public const string StatusAll = "all-records";
        public const string RecruitmentProcessStepStatusDefault = "default";
        public const string RecruitmentProcessStepStatusAll = "all-records";
        public const string MyRecords = "my-records";
        public const string MyProcessRecords = "my-processes";
        public const string AnyOwner = "any-owner";
        public const string JobOpeningFilters = "job-openig-filters";
        public const string CandidateAllCandidates = "all-candidates";
        public const string CandidateMyCandidates = "my-candidates";
        public const string CandidateMyProcessCandidates = "my-process-candidates";
        public const string CandidateProcessedByMeCandidates = "processed-by-me";
        public const string CandidatePropertiesFilters = "candidate-properties-fiters";
        public const string InboundEmailsUnread = "inbound-email-unread-fiters";
        public const string CandidateMyFavorites = "my-favorite";
        public const string InboundEmailAllColors = "all-colors";
        public const string MaturedProcessesOnly = "matured-processes";
        public const string AnyMaturityWillDo = "any-maturity";
    }
}