﻿namespace Smt.Atomic.Business.Recruitment.Consts
{
    public static class SearchAreaCodes
    {
        public const string Name = "name";
        public const string Email = "email";
        public const string Phone = "phone";
        public const string JobProfiles = "profiles";
        public const string Cities = "cities";
        public const string Resume = "cv";
        public const string Candidate = "candidate";
        public const string CandidateEmails = "candidate-emails";
        public const string RecommendigPerson = "recommending-person";
        public const string RecommendigPersonEmails = "recommending-person-emails";
    }
}