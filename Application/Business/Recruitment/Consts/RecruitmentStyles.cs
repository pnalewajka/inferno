﻿namespace Smt.Atomic.Business.Recruitment.Consts
{
    public static class RecruitmentStyles
    {
        public const string RowClassInboundEmailUnreadByMe = "inbound-email-unread-row";
        public const string RowClassInboundEmailReadByOthers = "inbound-email-others-read-row";
        public const string RowClassCandidateFavorite = "favorite-candidate";
        public const string ClassCategoryRed = "category-red";
        public const string ClassCategoryBlue = "category-blue";
        public const string ClassCategoryGreen = "category-green";
        public const string ClassCategoryOrange = "category-orange";
        public const string ClassCategoryPurple = "category-purple";
        public const string ClassCategoryYellow = "category-yellow";
    }
}