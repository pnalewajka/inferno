﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Smt.Atomic.Business.Recruitment.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class JobOpeningResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal JobOpeningResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Smt.Atomic.Business.Recruitment.Resources.JobOpeningResources", typeof(JobOpeningResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid Org Unit.
        /// </summary>
        internal static string InvalidOrgUnit {
            get {
                return ResourceManager.GetString("InvalidOrgUnit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Information on approving the job opening &quot;{0}&quot; has been sent to {1}..
        /// </summary>
        internal static string JobOpeningAppprovalInformation {
            get {
                return ResourceManager.GetString("JobOpeningAppprovalInformation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Information on closing the job opening &quot;{0}&quot; has been sent to {1}..
        /// </summary>
        internal static string JobOpeningCloseInformation {
            get {
                return ResourceManager.GetString("JobOpeningCloseInformation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Job opening &quot;{0}&quot; has been rejected. Rejection reason: {1}.
        /// </summary>
        internal static string JobOpeningRejectedNoteMessage {
            get {
                return ResourceManager.GetString("JobOpeningRejectedNoteMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Information on rejecting the job opening &quot;{0}&quot; has been sent to {1}..
        /// </summary>
        internal static string JobOpeningRejectionInformation {
            get {
                return ResourceManager.GetString("JobOpeningRejectionInformation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Information with approval request of job opening &quot;{0}&quot; has been sent to Recruitment Leaders..
        /// </summary>
        internal static string JobOpeningSentAprovalInformation {
            get {
                return ResourceManager.GetString("JobOpeningSentAprovalInformation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Information about updating request of job opening has been sent to watchers..
        /// </summary>
        internal static string JobOpeningUpdateInformation {
            get {
                return ResourceManager.GetString("JobOpeningUpdateInformation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Not Required.
        /// </summary>
        internal static string NotRequired {
            get {
                return ResourceManager.GetString("NotRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} has put the job opening on hold. Role was opened on {1}..
        /// </summary>
        internal static string PutOnHoldNoteContent {
            get {
                return ResourceManager.GetString("PutOnHoldNoteContent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Recruitment Leaders.
        /// </summary>
        internal static string RecruitmentLeadersEmailsRecipient {
            get {
                return ResourceManager.GetString("RecruitmentLeadersEmailsRecipient", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Required.
        /// </summary>
        internal static string Required {
            get {
                return ResourceManager.GetString("Required", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} has taken the job opening off hold. Role opened on was changed to {1}.
        /// </summary>
        internal static string TakeOffHoldNoteContent {
            get {
                return ResourceManager.GetString("TakeOffHoldNoteContent", resourceCulture);
            }
        }
    }
}
