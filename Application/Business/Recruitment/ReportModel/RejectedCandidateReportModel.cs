﻿using System;

namespace Smt.Atomic.Business.Recruitment.ReportModel
{
    public class RejectedCandidateReportModel
    {
        public string CandidateFullName { get; set; }

        public string JobOpening { get; set; }

        public DateTime DecisionDate { get; set; }

        public string StepName { get; set; }

        public string Reason { get; set; }

        public string Comment { get; set; }
    }
}
