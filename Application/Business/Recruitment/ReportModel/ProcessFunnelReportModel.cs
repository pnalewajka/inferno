﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Recruitment.ReportModel
{
    public class ProcessFunnelReportModel
    {
        public List<ProcessFunnelReportModelData> Records { get; set; }

        public List<string> ColumnOrder { get; set; }
    }
}
