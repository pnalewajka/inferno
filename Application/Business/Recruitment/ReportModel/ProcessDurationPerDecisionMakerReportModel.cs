﻿namespace Smt.Atomic.Business.Recruitment.ReportModel
{
    public class ProcessDurationPerDecisionMakerReportModel
    {
        public string DecisionMakerFullName { get; set; }

        public string ProcessResult { get; set; }

        public int ProcessDurationInDays { get; set; }
    }
}
