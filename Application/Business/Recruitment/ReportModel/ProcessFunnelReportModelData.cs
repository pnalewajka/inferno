﻿namespace Smt.Atomic.Business.Recruitment.ReportModel
{
    public class ProcessFunnelReportModelData
    {
        public string RecruiterFullName { get; set; }

        public string ProcessStepType { get; set; }

        public int Value { get; set; }
    }
}
