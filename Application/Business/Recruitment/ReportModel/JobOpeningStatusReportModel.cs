﻿using System;

namespace Smt.Atomic.Business.Recruitment.ReportModel
{
    public class JobOpeningStatusReportModel
    {
        public string Position { get; set; }

        public string DecisionMakerFullName { get; set; }

        public string Cities { get; set; }

        public string JobProfiles { get; set; }

        public string Status { get; set; }

        public string AcceptedByFullName { get; set; }

        public DateTime OpenedOn { get; set; }

        public string ClosedReason { get; set; }

        public string ClosedComment { get; set; }
    }
}
