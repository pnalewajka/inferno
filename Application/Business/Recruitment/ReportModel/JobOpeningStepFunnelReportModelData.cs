﻿namespace Smt.Atomic.Business.Recruitment.ReportModel
{
    public class JobOpeningStepFunnelReportModelData
    {        
        public string JobOpeningName { get; set; }

        public string DecisionMakerFullName { get; set; }

        public string ProcessStepType { get; set; }

        public int Value { get; set; }
    }
}
