﻿namespace Smt.Atomic.Business.Recruitment.ReportModel
{
    public class ProcessProgressReportModel
    {
        public string CandidateFullName { get; set; }

        public string JobOpening { get; set; }

        public string JobProfiles { get; set; }

        public string CurrentStep { get; set; }

        public string AssignedEmployeeFullName { get; set; }

        public string RecruiterFullName { get; set; }

        public string City { get; set; }

        public string OrgUnitFlatName { get; set; }
    }
}
