﻿namespace Smt.Atomic.Business.Recruitment.ReportModel
{
    public class JobOpeningCurrentStepReportModel
    {        
        public string JobOpening { get; set; }

        public string CandidateFullName { get; set; }

        public string JobProfiles { get; set; }

        public string CurrentStage { get; set; }

        public string HiringManagerFullName { get; set; }

        public string City { get; set; }
    }
}
