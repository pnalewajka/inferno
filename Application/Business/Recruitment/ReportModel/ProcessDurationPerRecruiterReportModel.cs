﻿namespace Smt.Atomic.Business.Recruitment.ReportModel
{
    public class ProcessDurationPerRecruiterReportModel
    {
        public string RecruiterFullName { get; set; }

        public string ProcessResult { get; set; }

        public int ProcessDurationInDays { get; set; }
    }
}
