﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Recruitment.ReportModel
{
    public class JobOpeningStepFunnelReportModel
    {
        public List<JobOpeningStepFunnelReportModelData> Records { get; set; }

        public List<string> ColumnOrder { get; set; }
    }
}
