﻿using System;

namespace Smt.Atomic.Business.Recruitment.ReportModel
{
    public class HiredCandidateReportModel
    {
        public string JobOpening { get; set; }

        public string CandidateFullName { get; set; }

        public string JobProfiles { get; set; }

        public string FinalPosition { get; set; }

        public string OrgUnit { get; set; }

        public string TrialContractType { get; set; }

        public string LongTermContractType { get; set; }

        public string EmploymentOrigin { get; set; }

        public string RecruiterFullName { get; set; }

        public DateTime? StartDate { get; set; }

        public string RecruitmentLeaderFullName { get; set; }
    }
}
