﻿namespace Smt.Atomic.Business.Recruitment.ReportModel
{
    public class TechnicalReviewSummaryReportModel
    {
        public string CandidateFirstName { get; set; }
        public string CandidateLastName { get; set; }
        public string PositionName { get; set; }
        public string Seniority { get; set; }
        public string Profiles { get; set; }
        public string InterviewDate { get; set; }
        public string Interviewer { get; set; }
        public string TechnicalKnowledge { get; set; }
        public string TechnicalExercise { get; set; }
        public string EnglishUsage { get; set; }
        public string Strengths { get; set; }
        public string Risk { get; set; }
        public string SuitabilityForProject { get; set; }
        public string OtherRemarks { get; set; }
    }
}
