﻿using Smt.Atomic.Business.Common.Abstracts;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.DocumentMappings
{
    [Identifier("Documents.InboundEmailDocumentMapping")]
    public class InboundEmailDocumentMapping : DocumentMapping<InboundEmailDocument, InboundEmailDocumentContent, IRecruitmentDbScope>
    {
        public InboundEmailDocumentMapping(IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService)
            : base(unitOfWorkService)
        {
            ContentColumnExpression = o => o.Content;
            NameColumnExpression = o => o.Name;
            ContentTypeColumnExpression = o => o.ContentType;
            ContentLengthColumnExpression = o => o.ContentLength;
        }
    }
}