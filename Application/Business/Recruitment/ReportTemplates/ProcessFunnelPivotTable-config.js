﻿pivot.dataConverter = function (data) {
    return data.Records.map(function (t) {
        return {
            "Recruiter": t.RecruiterFullName,
            "ProcessStepType": t.ProcessStepType,
            "ProcessStepTypeCount": t.Value
        };
    });
};

pivot.configurations = [
    {
        name: "Process Funnel Report",
        code: "T1",
        config: {
            rows: ["Recruiter"],
            cols: ["ProcessStepType"],
            vals: ["ProcessStepTypeCount"],
            aggregatorName: "Integer Sum",
            rowOrder: "key_a_to_z",
            sorters: {
                ProcessStepType: $.pivotUtilities.sortAs(reportData.ColumnOrder)
            }
        }
    },
];
