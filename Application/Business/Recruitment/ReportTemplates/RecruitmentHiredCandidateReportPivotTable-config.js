﻿pivot.dataConverter = function (data) {
    return data.map(function (t) {
        return {
            "JobOpening": t.JobOpening,
            "Candidate": t.CandidateFullName,
            "JobProfiles": t.JobProfiles,
            "FinalPosition": t.FinalPosition,
            "OrgUnit": t.OrgUnit,
            "TrialContractType": t.TrialContractType,
            "LongTermContractType": t.LongTermContractType,
            "EmploymentOrigin": t.EmploymentOrigin,
            "Recruiter": t.RecruiterFullName,
            "StartDate": Reporting.displayDate(t.StartDate),
            "RecruitmentLeader": t.RecruitmentLeaderFullName
        };
    });
};

pivot.configurations = [
    {
        name: "Recruitment Hired Candidate Report",
        code: "T1",
        config: {
            rows: ["JobOpening", "Candidate", "JobProfiles", "FinalPosition", "OrgUnit", "TrialContractType", "LongTermContractType", "EmploymentOrigin", "Recruiter", "StartDate", "RecruitmentLeader"],
            cols: [],
            vals: [],
            aggregatorName: "First",
            rowOrder: "key_a_to_z",
            colOrder: "key_a_to_z",
        }
    },
];
