﻿pivot.dataConverter = function (data) {
    return data.map(function (t) {
        return {
            "Candidate": t.CandidateFullName,
            "JobOpening": t.JobOpening,
            "JobProfiles": t.JobProfiles,
            "HiringManager": t.HiringManagerFullName,
            "City": t.City,
            "CurrentStage": t.CurrentStage
        };
    });
};

pivot.configurations = [
    {
        name: "Recruitment Job Opening Current Step",
        code: "T1",
        config: {
            rows: ["JobOpening", "Candidate", "HiringManager", "JobProfiles", "City", "CurrentStage"],
            cols: [],
            vals: [],
            aggregatorName: "First",
            rowOrder: "key_a_to_z",
            colOrder: "key_a_to_z",
        }
    },
];
