SELECT
	   RecruitmentProcessStep.Id, RecruitmentProcessStep.Type,
	   (Employee.FirstName + ' ' + Employee.LastName) AS TechnicalReviewer,
	   (Candidate.FirstName + ' ' + Candidate.LastName) AS Candidate,
	   RecruitmentProcessStep.PlannedOn AS VerifiedOn,
	   TechnicalReview.SeniorityLevelAssessment AS Seniority,
	   RecruitmentProcessStepDecision.Code AS Decision,
	   RecruitmentProcessStep.DecisionComment AS DecisionComment,
	   JobProfile.Name AS Profile
  FROM Recruitment.RecruitmentProcessStep AS RecruitmentProcessStep
 INNER 
  JOIN Recruitment.RecruitmentProcess AS RecruitmentProcess
    ON RecruitmentProcess.Id = RecruitmentProcessStep.RecruitmentProcessId
 INNER 
  JOIN Recruitment.TechnicalReview AS TechnicalReview
    ON RecruitmentProcessStep.Id = TechnicalReview.RecruitmentProcessStepId
 LEFT 
  JOIN Allocation.Employee AS Employee
    ON Employee.Id = RecruitmentProcessStep.AssignedEmployeeId
 INNER 
  JOIN Recruitment.Candidate AS Candidate
    ON Candidate.Id = RecruitmentProcess.CandidateId
 LEFT
  JOIN Recruitment.RecruitmentProcessStepDecision as RecruitmentProcessStepDecision
	ON RecruitmentProcessStep.Decision = RecruitmentProcessStepDecision.Value
 LEFT
  JOIN Recruitment.TechnicalReviewJobProfiles AS TechnicalReviewJobProfiles
   ON  TechnicalReview.Id = TechnicalReviewJobProfiles.TechnicalReviewId
 LEFT
  JOIN SkillManagement.JobProfile AS JobProfile
    ON TechnicalReviewJobProfiles.JobProfileId = JobProfile.Id 
 WHERE RecruitmentProcessStep.Type = 300
   AND RecruitmentProcessStep.Status = 2
   AND (RecruitmentProcessStep.PlannedOn IS NULL OR (RecruitmentProcessStep.PlannedOn >= @from AND RecruitmentProcessStep.PlannedOn <= @to))
   AND (@IsJobProfileIdsSet = 0 OR TechnicalReviewJobProfiles.JobProfileId IN ( @jobProfileIds ))
   AND (@IsReviewerIdsSet = 0 OR RecruitmentProcessStep.AssignedEmployeeId IN ( @reviewerIds ))
 