﻿pivot.dataConverter = function (data) {
    return data.map(function (t) {
        return {
            "Position": t.Position,
            "DecisionMaker": t.DecisionMakerFullName,
            "Cities": t.Cities,
            "Technologies": t.JobProfiles,
            "Status": t.Status,
            "AcceptedBy": t.AcceptedByFullName,
            "OpenedOn": Reporting.displayDate(t.OpenedOn),
            "ClosedReason": t.ClosedReason,
            "ClosedComment": t.ClosedComment,
        };
    });
};

pivot.configurations = [
    {
        name: "Recruitment Job Opening Status",
        code: "T1",
        config: {
            rows: ["Position", "DecisionMaker", "Cities", "Technologies", "Status", "AcceptedBy", "OpenedOn", "ClosedReason", "ClosedComment"],
            cols: [],
            vals: [],
            aggregatorName: "First",
            rowOrder: "key_a_to_z",
            colOrder: "key_a_to_z",
        }
    },
];
