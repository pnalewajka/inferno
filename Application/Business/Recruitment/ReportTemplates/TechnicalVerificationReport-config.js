﻿pivot.dataConverter = function (data) {
    return data.Records.map(function (t) {
        return {
            "TechnicalReviewer": t.TechnicalReviewer,
            "Candidate": t.Candidate,
            "VerifiedOn": Reporting.displayDate(t.VerifiedOn),
            "Seniority": t.Seniority + 1,
            "Decision": t.Decision,
            "DecisionComment": t.DecisionComment,
            "Profile": t.Profile
        }
    });
};

pivot.configurations = [
    {
        name: "Technical Verification Report",
        code: "T1",
        config: {
            rows: ["TechnicalReviewer", "Candidate", "VerifiedOn", "Seniority", "Decision", "DecisionComment", "Profile"],
            cols: [],
            vals: [],
            aggregatorName: "First",
            rowOrder: "key_a_to_z",
            colOrder: "key_a_to_z",
        }
    },
];
