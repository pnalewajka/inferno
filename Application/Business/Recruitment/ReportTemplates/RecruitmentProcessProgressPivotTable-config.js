﻿pivot.dataConverter = function (data) {
    return data.map(function (t) {
        return {
            "Candidate": t.CandidateFullName,
            "JobOpening": t.JobOpening,
            "JobProfiles": t.JobProfiles,
            "Recruiter": t.RecruiterFullName,
            "City": t.City,
            "CurrentStep": t.CurrentStep,
            "Assignee": t.AssignedEmployeeFullName,
            "OrgUnit": t.OrgUnitFlatName
        };
    });
};

pivot.configurations = [
    {
        name: "Recruitment Process Progress report",
        code: "T1",
        config: {
            rows: ["Candidate", "JobOpening", "JobProfiles", "Recruiter", "City", "CurrentStep", "Assignee"],
            cols: [],
            vals: [],
            aggregatorName: "First",
            rowOrder: "key_a_to_z",
            colOrder: "key_a_to_z",
        }
    },
];
