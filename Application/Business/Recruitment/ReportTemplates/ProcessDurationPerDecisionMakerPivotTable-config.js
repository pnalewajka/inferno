﻿pivot.dataConverter = function (data) {
    return data.map(function (t) {
        return {
            "DecisionMaker": t.DecisionMakerFullName,
            "ProcessDurationInDays": t.ProcessDurationInDays,
            "ProcessResult": t.ProcessResult
        };
    });
};

pivot.configurations = [
    {
        name: "Average process duration in days per decision maker",
        code: "T1",
        config: {
            rows: ["DecisionMaker"],
            cols: ["ProcessResult"],
            vals: ["ProcessDurationInDays"],
            aggregatorName: "Average",
            rowOrder: "key_a_to_z"
        }
    },
];
