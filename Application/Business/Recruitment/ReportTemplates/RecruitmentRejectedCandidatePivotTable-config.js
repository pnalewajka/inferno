﻿pivot.dataConverter = function (data) {
    return data.map(function (t) {
        return {
            "Candidate": t.CandidateFullName,
            "JobOpening": t.JobOpening,
            "DecisionDate": Reporting.displayDate(t.DecisionDate),
            "StepName": t.StepName,
            "Comment": t.Comment,
            "Reason": t.Reason
        };
    });
};

pivot.configurations = [
    {
        name: "Recruitment Rejected Candidate Report",
        code: "T1",
        config: {
            rows: ["Candidate", "JobOpening", "DecisionDate", "StepName", "Reason", "Comment"],
            cols: [],
            vals: [],
            aggregatorName: "First",
            rowOrder: "key_a_to_z",
            colOrder: "key_a_to_z",
        }
    },
];
