﻿pivot.dataConverter = function (data) {
    return data.map(function (t) {
        return {
            "Recruiter": t.RecruiterFullName,
            "ProcessDurationInDays": t.ProcessDurationInDays,
            "ProcessResult": t.ProcessResult
        };
    });
};

pivot.configurations = [
    {
        name: "Average process duration in days per recruiter",
        code: "T1",
        config: {
            rows: ["Recruiter"],
            cols: ["ProcessResult"],
            vals: ["ProcessDurationInDays"],
            aggregatorName: "Average",
            rowOrder: "key_a_to_z"
        }
    },
];
