﻿pivot.dataConverter = function (data) {
    return data.Records.map(function (t) {
        return {
            "JobOpening": t.JobOpeningName,
            "ProcessStepType": t.ProcessStepType,
            "DecisionMaker": t.DecisionMakerFullName,
            "ProcessStepTypeCount": t.Value
        };
    });
};

pivot.configurations = [
    {
        name: "Recruitment Job Opening Step Funnel",
        code: "T1",
        config: {
            rows: ["DecisionMaker", "JobOpening"],
            cols: ["ProcessStepType"],
            vals: ["ProcessStepTypeCount"],
            aggregatorName: "Integer Sum",
            rowOrder: "key_a_to_z",
            sorters: {
                ProcessStepType: $.pivotUtilities.sortAs(reportData.ColumnOrder)
            }
        }
    },
];
