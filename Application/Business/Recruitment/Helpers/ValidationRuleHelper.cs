﻿using System;
using System.Text.RegularExpressions;

namespace Smt.Atomic.Business.Recruitment.Helpers
{
    public class ValidationRuleHelper
    {
        public static bool IsEmail(string email)
        {
            const string emailRegex = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
               @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$";

            return Regex.IsMatch(email, emailRegex, RegexOptions.IgnoreCase) || Uri.IsWellFormedUriString(email, UriKind.Absolute);
        }
    }
}
