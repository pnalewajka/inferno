﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.Business.Recruitment.Consts;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Helpers
{
    public static class InboundEmailHelper
    {
        private static IDictionary<InboundEmailColorCategory, string> InboundEmailColorClasses = new Dictionary<InboundEmailColorCategory, string>()
        {
            { InboundEmailColorCategory.Blue,  RecruitmentStyles.ClassCategoryBlue },
            { InboundEmailColorCategory.Green,  RecruitmentStyles.ClassCategoryGreen },
            { InboundEmailColorCategory.Orange,  RecruitmentStyles.ClassCategoryOrange },
            { InboundEmailColorCategory.Purple,  RecruitmentStyles.ClassCategoryPurple },
            { InboundEmailColorCategory.Red,  RecruitmentStyles.ClassCategoryRed },
            { InboundEmailColorCategory.Yellow,  RecruitmentStyles.ClassCategoryYellow },
        };

        public static string GetEmailColorCategoryCssClass(InboundEmailColorCategory inboundEmailColorCategory)
        {
            if (InboundEmailColorClasses.TryGetValue(inboundEmailColorCategory, out string value))
            {
                return value;
            }

            return null;
        }
    }
}
