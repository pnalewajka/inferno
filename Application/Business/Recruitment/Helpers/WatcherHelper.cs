﻿using System.Linq;
using Smt.Atomic.Business.Recruitment.Enums;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Recruitment.BusinessLogics
{
    public static class WatcherHelper
    {
        public static void Toggle(IQueryable<IWatchedEntity> collection, Employee watcherEmployee, WatchingStatus newStatus)
        {
            foreach (var item in collection)
            {
                if (newStatus == WatchingStatus.Watch && !item.Watchers.Contains(watcherEmployee))
                {
                    item.Watchers.Add(watcherEmployee);
                }
                else if (newStatus == WatchingStatus.Unwatch && item.Watchers.Contains(watcherEmployee))
                {
                    item.Watchers.Remove(watcherEmployee);
                }
            }
        }
    }
}