﻿namespace Smt.Atomic.Business.Recruitment.Helpers
{
    public static class RecruitmentUrlHelper
    {
        public static string GetCandidateUrl(string serverAddress, long candidateId)
                => $"{serverAddress}Recruitment/Candidate/Details/{candidateId}";

        public static string GetCandidateNoteUrl(string serverAddress, long candidateNoteId, long candidateId)
              => $"{serverAddress}Recruitment/CandidateNote/Details/{candidateNoteId}?parent-id={candidateId}";

        public static string GetRecruitmentProcessNoteUrl(string serverAddress, long processNoteId, long processId)
                => $"{serverAddress}Recruitment/RecruitmentProcessNote/Details/{processNoteId}?parent-id={processId}";

        public static string GetRecruitmentProcessStepUrl(string serverAddress, long processStepId, long processId)
                => $"{serverAddress}Recruitment/RecruitmentProcessStep/Details/{processStepId}?parent-id={processId}";

        public static string GetRecruitmentProcessUrl(string serverAddress, long processId)
                => $"{serverAddress}Recruitment/RecruitmentProcessStep/List?parent-id={processId}";

        public static string GetInboundEmailUrl(string serverAddress, long InboundEmail)
                => $"{serverAddress}Recruitment/InboundEmail/Details/{InboundEmail}";

        public static string GetJobOpeningNoteUrl(string serverAddress, long jobOpeningNoteId, long jobOpeningId)
                => $"{serverAddress}Recruitment/JobOpeningNote/View/{jobOpeningNoteId}?parent-id={jobOpeningId}";

        public static string GetJobOpeningUrl(string serverAddress, long jobOpeningId)
                => $"{serverAddress}Recruitment/JobOpening/Details/{jobOpeningId}";
    }
}
