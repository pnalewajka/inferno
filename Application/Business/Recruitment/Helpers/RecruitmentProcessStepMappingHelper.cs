﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Helpers
{
    public static class RecruitmentProcessStepMappingHelper
    {
        public static void ApplyTechnicalReviewStepDetailsDtoToTechnicalReview(TechnicalReviewStepDetailsDto technicalReviewStepDetails, TechnicalReview technicalReview)
        {
            technicalReview.EnglishUsageAssessment = technicalReviewStepDetails.EnglishUsageAssessment;
            technicalReview.TeamProjectSuitabilityAssessment = technicalReviewStepDetails.TeamProjectSuitabilityAssessment;
            technicalReview.OtherRemarks = technicalReviewStepDetails.OtherRemarks;
            technicalReview.RiskAssessment = technicalReviewStepDetails.RiskAssessment;
            technicalReview.SeniorityLevelAssessment = technicalReviewStepDetails.SeniorityLevelAssessment;
            technicalReview.TechnicalKnowledgeAssessment = technicalReviewStepDetails.TechnicalKnowledgeAssessment;
            technicalReview.StrengthAssessment = technicalReviewStepDetails.StrengthAssessment;
            technicalReview.TechnicalAssignmentAssessment = technicalReviewStepDetails.TechnicalAssignmentAssessment;
        }

        public static void ApplyScreeningStepDetailsDtoToRecruitmentProcess(ScreeningStepDetailsDto screeningStepDetailsDto, RecruitmentProcess recruitmentProcess)
        {
            recruitmentProcess.Screening.Availability = screeningStepDetailsDto.ScreeningAvailability;
            recruitmentProcess.Screening.BusinessTripsCanDo = screeningStepDetailsDto.ScreeningBusinessTripsCanDo;
            recruitmentProcess.Screening.BusinessTripsComment = screeningStepDetailsDto.ScreeningBusinessTripsComment;
            recruitmentProcess.Screening.ContractExpectations = screeningStepDetailsDto.ScreeningContractExpectations;
            recruitmentProcess.Screening.CounterOfferCriteria = screeningStepDetailsDto.ScreeningCounterOfferCriteria;
            recruitmentProcess.Screening.GeneralOpinion = screeningStepDetailsDto.ScreeningGeneralOpinion;
            recruitmentProcess.Screening.LanguageEnglish = screeningStepDetailsDto.ScreeningLanguageEnglish;
            recruitmentProcess.Screening.LanguageGerman = screeningStepDetailsDto.ScreeningLanguageGerman;
            recruitmentProcess.Screening.LanguageOther = screeningStepDetailsDto.ScreeningLanguageOther;
            recruitmentProcess.Screening.Motivation = screeningStepDetailsDto.ScreeningMotivation;
            recruitmentProcess.Screening.OtherProcesses = screeningStepDetailsDto.ScreeningOtherProcesses;
            recruitmentProcess.Screening.RelocationCanDo = screeningStepDetailsDto.ScreeningRelocationCanDo;
            recruitmentProcess.Screening.RelocationComment = screeningStepDetailsDto.ScreeningRelocationComment;
            recruitmentProcess.Screening.Skills = screeningStepDetailsDto.ScreeningSkills;
            recruitmentProcess.Screening.WorkPermitComment = screeningStepDetailsDto.ScreeningWorkPermitComment;
            recruitmentProcess.Screening.WorkPermitNeeded = screeningStepDetailsDto.ScreeningWorkPermitNeeded;
            recruitmentProcess.Screening.WorkplaceExpectations = screeningStepDetailsDto.ScreeningWorkplaceExpectations;
            recruitmentProcess.Screening.EveningWorkCanDo = screeningStepDetailsDto.ScreeningEveningWorkCanDo;
            recruitmentProcess.Screening.EveningWorkComment = screeningStepDetailsDto.ScreeningEveningWorkComment;
        }

        public static void ApplyFinalDtoToRecruitmentProcess(ContractNegotiationsDetailsDto contractNegotiationsDetails, RecruitmentProcess recruitmentProcess)
        {
            if (recruitmentProcess.Final == null)
            {
                recruitmentProcess.Final = new RecruitmentProcessFinal { Id = recruitmentProcess.Id };
            }

            recruitmentProcess.Final.Comment = contractNegotiationsDetails.FinalComment;
            recruitmentProcess.Final.LocationId = contractNegotiationsDetails.FinalLocationId;
            recruitmentProcess.Final.LongTermContractTypeId = contractNegotiationsDetails.FinalLongTermContractTypeId;
            recruitmentProcess.Final.LongTermSalary = contractNegotiationsDetails.FinalLongTermSalary;
            recruitmentProcess.Final.PositionId = contractNegotiationsDetails.FinalPositionId;
            recruitmentProcess.Final.StartDate = contractNegotiationsDetails.FinalStartDate;
            recruitmentProcess.Final.SignedDate = contractNegotiationsDetails.FinalSignedDate;
            recruitmentProcess.Final.TrialContractTypeId = contractNegotiationsDetails.FinalTrialContractTypeId;
            recruitmentProcess.Final.TrialSalary = contractNegotiationsDetails.FinalTrialSalary;
            recruitmentProcess.Final.EmploymentSourceId = contractNegotiationsDetails.FinalEmploymentSourceId;

            if (recruitmentProcess.Offer == null)
            {
                recruitmentProcess.Offer = new RecruitmentProcessOffer { Id = recruitmentProcess.Id };
            }

            recruitmentProcess.Offer.Comment = contractNegotiationsDetails.OfferComment;
            recruitmentProcess.Offer.ContractType = contractNegotiationsDetails.OfferContractType;
            recruitmentProcess.Offer.LocationId = contractNegotiationsDetails.OfferLocationId;
            recruitmentProcess.Offer.PositionId = contractNegotiationsDetails.OfferPositionId;
            recruitmentProcess.Offer.Salary = contractNegotiationsDetails.OfferSalary;
            recruitmentProcess.Offer.StartDate = contractNegotiationsDetails.OfferStartDate;
        }
    }
}