﻿using System;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Helpers
{
    public static class RecruitmentProcessHelper
    {
        public static void SetRecruitmentProcessLastActivity(IUnitOfWork<IRecruitmentDbScope> unitOfWork, long recruitmentProcessId, DateTime activityDate, long activityByEmployeeId)
        {
            var recruitmentProcess = unitOfWork.Repositories.RecruitmentProcesses.GetById(recruitmentProcessId);
            
            recruitmentProcess.LastActivityOn = activityDate;
            recruitmentProcess.LastActivityById = activityByEmployeeId;
        }
    }
}
