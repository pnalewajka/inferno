﻿using System.Collections.Generic;
using System.Web.Script.Serialization;
using Smt.Atomic.Business.Recruitment.SnapshotComparer;

namespace Smt.Atomic.Business.Recruitment.Helpers
{
    public static class SnapshotComaparerHelper
    {
        public static string SerializeSnapshotChanges(Dictionary<string, ValueChange> snapshotChanges)
        {
            return (new JavaScriptSerializer()).Serialize((object)snapshotChanges);
        }

        public static Dictionary<string, ValueChange> DeserializeSnapshotChanges(string serializedSnapshotchanges)
        {
            if (serializedSnapshotchanges == null)
            {
                return null;
            }

            return (Dictionary<string, ValueChange>)(new JavaScriptSerializer()).Deserialize(serializedSnapshotchanges, typeof(Dictionary<string, ValueChange>));
        }
    }
}
