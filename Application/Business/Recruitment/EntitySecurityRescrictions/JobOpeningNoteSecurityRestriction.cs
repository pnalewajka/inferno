﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.Recruitment.EntitySecurityRescrictions
{
    public class JobOpeningNoteSecurityRestriction : EntitySecurityRestriction<JobOpeningNote>
    {
        public JobOpeningNoteSecurityRestriction(IPrincipalProvider principalProvider) : base(principalProvider)
        {
            RequiresAuthentication = true;
        }

        public override Expression<Func<JobOpeningNote, bool>> GetRestriction()
        {
            Expression<Func<JobOpeningNote, bool>> expression = null;

            if (Roles.Contains(SecurityRoleType.CanViewRecruitmentJobOpeningNotes))
            {
                if (Roles.Contains(SecurityRoleType.CanViewPublicRecruitmentJobOpeningNotes))
                {
                    expression = v => v.NoteVisibility == NoteVisibility.Public;
                }

                if (Roles.Contains(SecurityRoleType.CanViewOnlyRecruitmentTeamRecruitmentJobOpeningNotes))
                {
                    expression = expression == null
                        ? v => v.NoteVisibility == NoteVisibility.RecruitmentTeamOnly
                        : expression.Or(v => v.NoteVisibility == NoteVisibility.RecruitmentTeamOnly);
                }

                if (Roles.Contains(SecurityRoleType.CanViewOnlyHiringManagerRecruitmentJobOpeningNotes))
                {
                    expression = expression == null
                        ? v => v.NoteVisibility == NoteVisibility.HiringManagerOnly
                        : expression.Or(v => v.NoteVisibility == NoteVisibility.HiringManagerOnly);
                }

                if (Roles.Contains(SecurityRoleType.CanViewOnlyTechnicalReviewerRecruitmentJobOpeningNotes))
                {
                    expression = expression == null
                        ? v => v.NoteVisibility == NoteVisibility.TechnicalReviewerOnly
                        : expression.Or(v => v.NoteVisibility == NoteVisibility.TechnicalReviewerOnly);
                }
            }
            
            return expression ?? NoRecords();
        }
    }
}
