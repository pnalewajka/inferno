﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.Recruitment.EntitySecurityRescrictions
{
    class CandidateSecurityRestriction : EntitySecurityRestriction<Candidate>
    {
        public CandidateSecurityRestriction(IPrincipalProvider principalProvider)
           : base(principalProvider)
        {
            RequiresAuthentication = true;
        }

        public override Expression<Func<Candidate, bool>> GetRestriction()
        {
            Expression<Func<Candidate, bool>> result = null;

            if (Roles.Contains(SecurityRoleType.CanViewAllCandidates))
            {
                if (Roles.Contains(SecurityRoleType.CanViewHighlyConfidentialCandidates))
                {
                    return AllRecords();
                }

                result = c => !c.IsHighlyConfidential;
            }

            if (Roles.Contains(SecurityRoleType.CanViewMyCandidates))
            {
                var currentUserId = CurrentPrincipal.Id;
                var currentEmployeeId = CurrentPrincipal.EmployeeId;

                Expression<Func<Candidate, bool>> myCandidateExpresion =
                    v => v.ConsentCoordinatorId == currentEmployeeId
                             || v.ContactCoordinatorId == currentEmployeeId
                             || v.CreatedById == currentUserId
                             || v.Watchers.Any(w => w.Id == currentEmployeeId);

                result = result == null
                    ? myCandidateExpresion
                    : result.Or(myCandidateExpresion);
            }

            return result ?? NoRecords();
        }
    }
}
