﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.Recruitment.EntitySecurityRescrictions
{
    class RecruitmentProcessSecurityRestriction : EntitySecurityRestriction<RecruitmentProcess>
    {
        private readonly JobOpeningSecurityRestriction _jobOpeningSecurityRestriction;
        private readonly CandidateSecurityRestriction _candidateSecurityRestriction;

        public RecruitmentProcessSecurityRestriction(IPrincipalProvider principalProvider, IOrgUnitService orgUnitService, IEmployeeService employeeService)
          : base(principalProvider)
        {
            RequiresAuthentication = true;

            _candidateSecurityRestriction = new CandidateSecurityRestriction(principalProvider);
            _jobOpeningSecurityRestriction = new JobOpeningSecurityRestriction(principalProvider, orgUnitService, employeeService);
        }

        public override Expression<Func<RecruitmentProcess, bool>> GetRestriction()
        {
            Expression<Func<RecruitmentProcess, bool>> result = null;

            if (Roles.Contains(SecurityRoleType.CanViewAllRecruitmentProcesses))
            {
                var isAllowedCandidate = new BusinessLogic<Candidate, bool>(_candidateSecurityRestriction.GetRestriction());
                var isAllowedJobOpening = new BusinessLogic<JobOpening, bool>(_jobOpeningSecurityRestriction.GetRestriction());

                var allowedProcesses = new BusinessLogic<RecruitmentProcess, bool>(p => isAllowedCandidate.Call(p.Candidate)
                                                                                        && isAllowedJobOpening.Call(p.JobOpening)).AsExpression();

                result = allowedProcesses;
            }

            if (Roles.Contains(SecurityRoleType.CanViewMyRecruitmentProcesses)
                || Roles.Contains(SecurityRoleType.CanViewAllRecruitmentProcessStepsFromMyRecruitmentProcesses))
            {
                var currentUserId = CurrentPrincipal.Id;
                var currentEmployeeId = CurrentPrincipal.EmployeeId;

                var ownProcesses = new BusinessLogic<RecruitmentProcess, bool>(p =>
                    RecruitmentProcessBusinessLogic.IsOwnRecruitmentProcess
                    .Call(p, currentUserId.Value, currentEmployeeId))
                    .AsExpression();

                result = result == null
                    ? ownProcesses
                    : result.Or(ownProcesses);
            }

            result = result ?? NoRecords();

            return result;
        }
    }
}
