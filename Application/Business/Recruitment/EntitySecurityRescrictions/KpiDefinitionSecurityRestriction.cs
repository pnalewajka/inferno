﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.KPI;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.Recruitment.EntitySecurityRescrictions
{
    class KpiDefinitionSecurityRestriction : EntitySecurityRestriction<KpiDefinition>
    {
        public KpiDefinitionSecurityRestriction(IPrincipalProvider principalProvider)
            : base(principalProvider)
        {
            RequiresAuthentication = true;
        }

        public override Expression<Func<KpiDefinition, bool>> GetRestriction()
        {
            return k => !k.Roles.Any() || Roles.Any(ur => k.Roles.Select(r => r.Id).Contains((int)ur));
        }
    }
}
