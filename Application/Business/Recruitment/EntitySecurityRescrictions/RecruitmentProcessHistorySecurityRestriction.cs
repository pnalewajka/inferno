﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.Recruitment.EntitySecurityRescrictions
{
    class RecruitmentProcessHistorySecurityRestriction : EntitySecurityRestriction<RecruitmentProcessHistoryEntry>
    {
        public RecruitmentProcessHistorySecurityRestriction(IPrincipalProvider principalProvider)
            : base(principalProvider)
        {
            RequiresAuthentication = true;
        }

        public override Expression<Func<RecruitmentProcessHistoryEntry, bool>> GetRestriction()
        {
            if (Roles.Contains(SecurityRoleType.CanViewRecruitmentProcessHistory))
            {
                return AllRecords();
            }

            return NoRecords();
        }
    }
}
