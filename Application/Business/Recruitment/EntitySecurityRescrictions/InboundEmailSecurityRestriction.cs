﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.Recruitment.EntitySecurityRescrictions
{
    class InboundEmailSecurityRestriction : EntitySecurityRestriction<InboundEmail>
    {
        public InboundEmailSecurityRestriction(IPrincipalProvider principalProvider)
            : base(principalProvider)
        {
            RequiresAuthentication = true;
        }

        public override Expression<Func<InboundEmail, bool>> GetRestriction()
        {
            if (Roles.Contains(SecurityRoleType.CanViewAllInboundEmails))
            {
                if (Roles.Contains(SecurityRoleType.CanViewInboundEmailsSendForResearch))
                {
                    return AllRecords();
                }

                return j => j.Status != InboundEmailStatus.SentForResearch;
            }

            if (Roles.Contains(SecurityRoleType.CanViewMyInboundEmails))
            {
                var currentUserId = CurrentPrincipal.Id;
                var currentEmployeeId = CurrentPrincipal.EmployeeId;

                return v => v.CreatedBy.Id == currentUserId 
                    || v.ProcessedById == currentEmployeeId;
            }

            return NoRecords();
        }
    }
}
