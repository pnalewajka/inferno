﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.Recruitment.EntitySecurityRescrictions
{
    class CandidateContactRecordSecurityRestriction : EntitySecurityRestriction<CandidateContactRecord>
    {
        public CandidateContactRecordSecurityRestriction(IPrincipalProvider principalProvider) : base(principalProvider)
        {
        }
        public override Expression<Func<CandidateContactRecord, bool>> GetRestriction()
        {
            if (Roles.Contains(SecurityRoleType.CanViewRecruitmentContactRecord))
            {
                return AllRecords();
            }

            return NoRecords();
        }
    }
}
