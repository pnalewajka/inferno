﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.Recruitment.EntitySecurityRescrictions
{
    class RecommendingPersonSecurityRestriction : EntitySecurityRestriction<RecommendingPerson>
    {
        public RecommendingPersonSecurityRestriction(IPrincipalProvider principalProvider)
            : base(principalProvider)
        {
            RequiresAuthentication = true;
        }

        public override Expression<Func<RecommendingPerson, bool>> GetRestriction()
        {
            if (Roles.Contains(SecurityRoleType.CanViewRecommendingPersons))
            {
                return AllRecords();
            }

            return NoRecords();
        }
    }
}
