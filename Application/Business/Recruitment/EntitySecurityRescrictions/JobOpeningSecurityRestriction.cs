﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.Recruitment.EntitySecurityRescrictions
{
    class JobOpeningSecurityRestriction : EntitySecurityRestriction<JobOpening>
    {
        private readonly IOrgUnitService _orgUnitService;
        private readonly IEmployeeService _employeeService;

        public JobOpeningSecurityRestriction(IPrincipalProvider principalProvider, IOrgUnitService orgUnitService, IEmployeeService employeeService)
            : base(principalProvider)
        {
            _orgUnitService = orgUnitService;
            _employeeService = employeeService;

            RequiresAuthentication = true;
        }

        public override Expression<Func<JobOpening, bool>> GetRestriction()
        {
            var currentUserId = CurrentPrincipal.Id;
            var currentEmployeeId = CurrentPrincipal.EmployeeId;

            var allExpression = GetAllJobOpenings(currentUserId, currentEmployeeId);
            var myExpression = GetMyJobOpenings(currentUserId, currentEmployeeId);

            var result = allExpression.Or(myExpression);

            return result;
        }

        private Expression<Func<JobOpening, bool>> GetMyJobOpenings(long? currentUserId, long currentEmployeeId)
        {
            if (!Roles.Contains(SecurityRoleType.CanViewMyJobOpenings))
            {
                return NoRecords();
            }

            return JobOpeningBusinessLogic.IsMyJobOpening.Parametrize(currentEmployeeId, currentUserId.Value);
        }

        private bool CanViewAll => Roles.Contains(SecurityRoleType.CanViewAllJobOpenings)
                                   || Roles.Contains(SecurityRoleType.CanViewAllJobOpeningsBySameLineManager)
                                   || Roles.Contains(SecurityRoleType.CanViewAllJobOpeningsInOrgUnit);

        private Expression<Func<JobOpening, bool>> GetAllJobOpenings(long? currentUserId, long currentEmployeeId)
        {
            if (!CanViewAll)
            {
                return NoRecords();
            }

            var expression = GetAll(currentEmployeeId);

            if (!Roles.Contains(SecurityRoleType.CanViewAllJobOpenings)
                || !Roles.Contains(SecurityRoleType.CanViewHighlyConfidentialJobOpenings))
            {
                expression = expression.And(o => !o.IsHighlyConfidential);
            }

            if (!Roles.Contains(SecurityRoleType.CanViewDraftJobOpeningsFromOtherAuthors))
            {
                expression = expression.And(o =>
                    o.Status != JobOpeningStatus.Draft || o.CreatedById == currentUserId);
            }

            if (!Roles.Contains(SecurityRoleType.CanViewJobOpeningsOtherThanActiveOrClosed))
            {
                expression = expression.And(o =>
                    o.Status == JobOpeningStatus.Active 
                                || o.Status == JobOpeningStatus.OnHold
                                || o.Status == JobOpeningStatus.Closed);
            }

            return expression;
        }

        private Expression<Func<JobOpening, bool>> GetAll(long currentEmployeeId)
        {
            if (Roles.Contains(SecurityRoleType.CanViewAllJobOpenings))
            {
                return AllRecords();
            }

            Expression<Func<JobOpening, bool>> result = null;

            if (Roles.Contains(SecurityRoleType.CanViewAllJobOpeningsInOrgUnit))
            {
                var parentOrgUnitId = _employeeService.GetEmployeeOrDefaultById(currentEmployeeId).OrgUnitId;

                var orgUnitIds = _orgUnitService.GetDescendantOrgUnitsIds(parentOrgUnitId)
                    .Concat(new[] { parentOrgUnitId })
                    .ToList();

                result = new BusinessLogic<JobOpening, bool>(o => orgUnitIds.Contains(o.OrgUnitId)
                             || EmployeeBusinessLogic.BelongsToOneOfOrgUnits.Call(o.DecisionMakerEmployee, orgUnitIds)).AsExpression();
            }

            if (Roles.Contains(SecurityRoleType.CanViewAllJobOpeningsBySameLineManager))
            {
                var lineManagerId = _employeeService.GetEmployeeOrDefaultById(currentEmployeeId).LineManagerId;

                if (lineManagerId == null)
                {
                    return result ?? NoRecords();
                }

                var sameLineManagerUserIds = _employeeService.GetEmployeeUserIdsByLineManagerEmployeeId(lineManagerId.Value);

                Expression<Func<JobOpening, bool>> sameLineManagerExpression = o => sameLineManagerUserIds.Contains(o.CreatedById.Value);

                result = result == null
                         ? sameLineManagerExpression
                         : result.Or(sameLineManagerExpression);
            }

            return result ?? NoRecords();
        }
    }
}
