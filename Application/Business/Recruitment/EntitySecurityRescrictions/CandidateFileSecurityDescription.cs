﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.Recruitment.EntitySecurityRescrictions
{
    class CandidateFileSecurityDescription : EntitySecurityRestriction<CandidateFile>
    {
        public CandidateFileSecurityDescription(IPrincipalProvider principalProvider) : base(principalProvider)
        {
        }
        public override Expression<Func<CandidateFile, bool>> GetRestriction()
        {
            if (Roles.Contains(SecurityRoleType.CanViewRecruitmentCandidateFile))
            {
                return AllRecords();
            }

            return NoRecords();
        }
    }
}
