﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.Recruitment.EntitySecurityRescrictions
{
    public class RecommendingPersonNoteSecurityRestriction : EntitySecurityRestriction<RecommendingPersonNote>
    {
        public RecommendingPersonNoteSecurityRestriction(IPrincipalProvider principalProvider) : base(principalProvider)
        {
            RequiresAuthentication = true;
        }

        public override Expression<Func<RecommendingPersonNote, bool>> GetRestriction()
        {
            Expression<Func<RecommendingPersonNote, bool>> expression = null;

            if (Roles.Contains(SecurityRoleType.CanViewRecommendingPersonNotes))
            {
                if (Roles.Contains(SecurityRoleType.CanViewPublicRecruitmentRecommendingPersonNotes))
                {
                    expression = v => v.NoteVisibility == NoteVisibility.Public;
                }

                if (Roles.Contains(SecurityRoleType.CanViewOnlyRecruitmentTeamRecruitmentRecommendingPersonNotes))
                {
                    expression = expression == null
                        ? v => v.NoteVisibility == NoteVisibility.RecruitmentTeamOnly
                        : expression.Or(v => v.NoteVisibility == NoteVisibility.RecruitmentTeamOnly);
                }

                if (Roles.Contains(SecurityRoleType.CanViewOnlyHiringManagerRecruitmentRecommendingPersonNotes))
                {
                    expression = expression == null
                        ? v => v.NoteVisibility == NoteVisibility.HiringManagerOnly
                        : expression.Or(v => v.NoteVisibility == NoteVisibility.HiringManagerOnly);
                }

                if (Roles.Contains(SecurityRoleType.CanViewOnlyTechnicalReviewerRecruitmentRecommendingPersonNotes))
                {
                    expression = expression == null
                        ? v => v.NoteVisibility == NoteVisibility.TechnicalReviewerOnly
                        : expression.Or(v => v.NoteVisibility == NoteVisibility.TechnicalReviewerOnly);
                }
            }

            return expression ?? NoRecords();
        }
    }
}
