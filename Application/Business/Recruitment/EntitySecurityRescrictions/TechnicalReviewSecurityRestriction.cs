﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.Recruitment.EntitySecurityRescrictions
{
    class TechnicalReviewSecurityRestriction : EntitySecurityRestriction<TechnicalReview>
    {
        public TechnicalReviewSecurityRestriction(IPrincipalProvider principalProvider)
          : base(principalProvider)
        {
            RequiresAuthentication = true;
        }

        public override Expression<Func<TechnicalReview, bool>> GetRestriction()
        {
            if (Roles.Contains(SecurityRoleType.CanViewAllTechnicalReviews))
            {
                return AllRecords();
            }

            if (Roles.Contains(SecurityRoleType.CanViewMyTechnicalReviews))
            {
                var currentUserId = CurrentPrincipal.Id;
                var currentEmployeeId = CurrentPrincipal.EmployeeId;

                return v => v.RecruitmentProcessStep.AssignedEmployeeId == currentEmployeeId 
                            || v.RecruitmentProcessStep.OtherAttendingEmployees.Any(e => e.Id == currentEmployeeId)
                            || v.CreatedById == currentUserId;
            }

            return NoRecords();
        }
    }
}
