﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.Recruitment.EntitySecurityRescrictions
{
    class JobApplicationSecurityRestriction : EntitySecurityRestriction<JobApplication>
    {
        public JobApplicationSecurityRestriction(IPrincipalProvider principalProvider) : base(principalProvider)
        {
        }
        public override Expression<Func<JobApplication, bool>> GetRestriction()
        {
            if (Roles.Contains(SecurityRoleType.CanViewJobApplications))
            {
                return AllRecords();
            }

            return NoRecords();
        }
    }
}
