﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.Recruitment.EntitySecurityRescrictions
{
    class RecruitmentProcessStepSecurityRestriction : EntitySecurityRestriction<RecruitmentProcessStep>
    {
        private readonly RecruitmentProcessSecurityRestriction _recruitmentProcessSecurityRestriction;

        public RecruitmentProcessStepSecurityRestriction(IPrincipalProvider principalProvider, IOrgUnitService orgUnitService, IEmployeeService employeeService)
          : base(principalProvider)
        {
            RequiresAuthentication = true;

            _recruitmentProcessSecurityRestriction = new RecruitmentProcessSecurityRestriction(principalProvider, orgUnitService, employeeService);
        }

        public override Expression<Func<RecruitmentProcessStep, bool>> GetRestriction()
        {
            Expression<Func<RecruitmentProcessStep, bool>> result = null;

            if (Roles.Contains(SecurityRoleType.CanViewAllRecruitmentProcessSteps)
                || Roles.Contains(SecurityRoleType.CanViewAllRecruitmentProcessStepsFromMyRecruitmentProcesses))
            {
                var isAllowedProcess = new BusinessLogic<RecruitmentProcess, bool>(_recruitmentProcessSecurityRestriction.GetRestriction());
                var allowedProcessSteps = new BusinessLogic<RecruitmentProcessStep, bool>(s => isAllowedProcess.Call(s.RecruitmentProcess)).AsExpression();

                result = result == null ? allowedProcessSteps : result.And(allowedProcessSteps);
            }

            if (Roles.Contains(SecurityRoleType.CanViewMyRecruitmentProcessSteps))
            {
                var currentEmployeeId = CurrentPrincipal.EmployeeId;
                var isOwnStep = new BusinessLogic<RecruitmentProcessStep, bool>(s => RecruitmentProcessStepBusinessLogic.IsOwnRecruitmentProcessStep.Call(s, currentEmployeeId)).AsExpression();

                result = result == null
                    ? isOwnStep
                    : result.Or(isOwnStep);
            }

            if (!Roles.Contains(SecurityRoleType.CanViewStepDetailsWhenDone))
            {
                Expression<Func<RecruitmentProcessStep, bool>> whenDoneCondition =
                    s => s.Status != RecruitmentProcessStepStatus.Done
                         && s.Status != RecruitmentProcessStepStatus.Cancelled;

                result = result == null
                    ? whenDoneCondition
                    : result.And(whenDoneCondition);
            }

            return result ?? NoRecords();
        }
    }
}
