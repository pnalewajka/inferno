﻿namespace Smt.Atomic.Business.Recruitment.Enums
{
    public enum FavoriteStatus
    {
        Favorite = 1 ,
        Disliked = 2
    }
}
