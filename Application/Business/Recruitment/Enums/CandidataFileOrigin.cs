﻿namespace Smt.Atomic.Business.Recruitment.Enums
{
    public enum CandidataFileOrigin
    {
        CandidateFile = 1,

        DataConsentFile = 2
    }
}
