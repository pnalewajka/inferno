﻿namespace Smt.Atomic.Business.Recruitment.Enums
{
    public enum WatchingStatus
    {
        Watch = 1,
        Unwatch = 2,
    }
}
