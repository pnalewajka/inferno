﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Consts;
using Smt.Atomic.Business.Recruitment.DocumentMappings;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification;
using Smt.Atomic.Business.Recruitment.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class InboundEmailCardIndexDataService : CardIndexDataService<InboundEmailDto, InboundEmail, IRecruitmentDbScope>, IInboundEmailCardIndexDataService
    {
        private readonly IUploadedDocumentHandlingService _uploadedDocumentHandlingService;
        private readonly IClassMapping<Candidate, CandidateDto> _candidateClassMapping;
        private readonly IInboundEmailNotificationService _inboundEmailNotificationService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public InboundEmailCardIndexDataService(ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies,
            IClassMapping<Candidate, CandidateDto> candidateClassMapping,
            IUploadedDocumentHandlingService uploadedDocumentHandlingService,
            IInboundEmailNotificationService inboundEmailNotificationService)
            : base(dependencies)
        {
            _uploadedDocumentHandlingService = uploadedDocumentHandlingService;
            _candidateClassMapping = candidateClassMapping;
            _inboundEmailNotificationService = inboundEmailNotificationService;
        }

        public BoolResult AssignMessage(long inboundEmailId, long employeeId)
        {
            var result = new BoolResult(true);

            using (var transactionScope = TransactionHelper.CreateDefaultTransactionScope())
            {
                using (var unitOfWork = UnitOfWorkService.Create())
                {
                    var inboundEmail = unitOfWork.Repositories.InboundEmails.GetByIdOrDefault(inboundEmailId);

                    if (inboundEmail == null)
                    {
                        result.AddAlert(AlertType.Error, string.Format(AlertResources.CannotFindInboundEmail, inboundEmailId));
                        result.IsSuccessful = false;

                        return result;
                    }

                    inboundEmail.ProcessedById = employeeId;
                    inboundEmail.Status = InboundEmailStatus.Assigned;

                    unitOfWork.Repositories.InboundEmails.Edit(inboundEmail);
                    unitOfWork.Commit();
                }

                _inboundEmailNotificationService.NotifyAboutAssignment(inboundEmailId);

                transactionScope.Complete();
            }
            return result;
        }

        public BoolResult UnassignMessage(long inboundEmailId)
        {
            var result = new BoolResult(true);

            using (var transactionScope = TransactionHelper.CreateDefaultTransactionScope())
            {
                _inboundEmailNotificationService.NotifyAboutUnassignment(inboundEmailId);

                using (var unitOfWork = UnitOfWorkService.Create())
                {
                    var inboundEmail = unitOfWork.Repositories.InboundEmails.GetByIdOrDefault(inboundEmailId);

                    if (inboundEmail == null)
                    {
                        result.AddAlert(AlertType.Error, string.Format(AlertResources.CannotFindInboundEmail, inboundEmailId));
                        result.IsSuccessful = false;

                        return result;
                    }

                    inboundEmail.ProcessedById = null;
                    inboundEmail.Status = InboundEmailStatus.New;

                    unitOfWork.Repositories.InboundEmails.Edit(inboundEmail);
                    unitOfWork.Commit();
                }

                transactionScope.Complete();
            }
            return result;
        }

        public override CardIndexRecords<InboundEmailDto> GetRecords(QueryCriteria criteria)
        {
            var records = base.GetRecords(criteria);

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var recordsById = records.Rows.ToDictionary(r => r.Id);

                var matchingCandidateEmailValues = unitOfWork.Repositories.InboundEmailValues
                    .Where(v => recordsById.Keys.Contains(v.InboundEmailId)
                                && v.Key == InboundEmailValueType.MatchingCandidateId
                                && v.ValueLong != null)
                    .Select(v => new { CandidateId = v.ValueLong.Value, EmailId = v.InboundEmailId })
                    .Join(unitOfWork.Repositories.Candidates,
                        inboundEmailValue => inboundEmailValue.CandidateId,
                        candidate => candidate.Id,
                        (inboundEmailValue, candidate) => new { inboundEmailValue.EmailId, Candidate = candidate })
                    .ToList();

                foreach (var candidateEmailValue in matchingCandidateEmailValues)
                {
                    recordsById[candidateEmailValue.EmailId].MatchingCandidateDto = _candidateClassMapping.CreateFromSource(candidateEmailValue.Candidate);
                }
            }

            return records;
        }

        public string GetDocumentNameById(long inboundEmailFileId)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                return unitOfWork.Repositories.InboundEmailDocuments
                    .Filtered()
                    .SelectById(inboundEmailFileId, d => d.Name);
            }
        }

        public long GetInboundEmailIdByDocumentId(long inboundEmailFileId)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                return unitOfWork.Repositories.InboundEmailDocuments
                    .Filtered()
                    .SelectById(inboundEmailFileId, d => d.InboundEmailId);
            }
        }

        protected void CloseInboundEmail(long inboundEmailId, InboundEmailDto inputDto)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var inboundEmail = unitOfWork.Repositories.InboundEmails.GetByIdOrDefault(inboundEmailId);

                if (inboundEmail == null)
                {
                    throw new BusinessException(String.Format(AlertResources.CannotFindInboundEmail, inboundEmailId));
                }

                inboundEmail.ClosedReason = inputDto.ClosedReason;
                inboundEmail.ClosedComment = inputDto.ClosedComment;
                inboundEmail.ClosedOn = TimeService.GetCurrentTime();
                inboundEmail.ClosedById = PrincipalProvider.Current.EmployeeId;
                inboundEmail.Status = InboundEmailStatus.Closed;

                unitOfWork.Repositories.InboundEmails.Edit(inboundEmail);
                unitOfWork.Commit();
            }
        }

        public BoolResult SendForResearch(long inboundEmailId)
        {
            var result = new BoolResult(true);

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var inboundEmail = unitOfWork.Repositories.InboundEmails.GetByIdOrDefault(inboundEmailId);

                if (inboundEmail == null)
                {
                    result.AddAlert(AlertType.Error, string.Format(AlertResources.CannotFindInboundEmail, inboundEmailId));
                    result.IsSuccessful = false;

                    return result;
                }

                inboundEmail.ProcessedById = null;
                inboundEmail.Status = InboundEmailStatus.SentForResearch;

                unitOfWork.Commit();
            }

            return result;
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<InboundEmail, InboundEmailDto, IRecruitmentDbScope> eventArgs)
        {
            _uploadedDocumentHandlingService
                .MergeDocumentCollections(
                    eventArgs.UnitOfWork,
                    eventArgs.InputEntity.Attachments,
                    eventArgs.InputDto.Documents,
                    p => new InboundEmailDocument
                    {
                        Name = p.DocumentName,
                        ContentType = p.ContentType
                    }
                );
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<InboundEmail, InboundEmailDto> eventArgs)
        {
            _uploadedDocumentHandlingService
                .PromoteTemporaryDocuments<InboundEmailDocument, InboundEmailDocumentContent, InboundEmailDocumentMapping, IRecruitmentDbScope>(
                    eventArgs.InputDto.Documents
                );

            if (eventArgs.InputDto.WorkflowAction.HasValue)
            {
                HandleWorkflowAction(eventArgs.InputDto.WorkflowAction.Value, eventArgs.InputEntity, eventArgs.InputDto, eventArgs.Alerts);
            }
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<InboundEmail, InboundEmailDto, IRecruitmentDbScope> eventArgs)
        {
            _uploadedDocumentHandlingService
                .MergeDocumentCollections(
                    eventArgs.UnitOfWork,
                    eventArgs.DbEntity.Attachments,
                    eventArgs.InputDto.Documents,
                    p => new InboundEmailDocument
                    {
                        Name = p.DocumentName,
                        ContentType = p.ContentType
                    }
                );

            MergeValues(eventArgs.UnitOfWork, eventArgs.DbEntity.Values, eventArgs.InputEntity.Values, eventArgs.DbEntity.Id);

            if (eventArgs.InputDto.WorkflowAction.HasValue && eventArgs.InputDto.WorkflowAction.Value == InboundEmailWorkflowAction.SendForResearch)
            {
                HandleWorkflowAction(eventArgs.InputDto.WorkflowAction.Value, eventArgs.InputEntity, eventArgs.InputDto, eventArgs.Alerts);
            }
        }

        private void MergeValues(IUnitOfWork<IRecruitmentDbScope> unitOfWork, ICollection<InboundEmailValue> dbEntityValues, ICollection<InboundEmailValue> inputEntityValues, long inboundEmailId)
        {
            foreach (var inboundEmailValue in inputEntityValues)
            {
                inboundEmailValue.InboundEmailId = inboundEmailId;
            }

            EntityMergingService.MergeCollections(unitOfWork, dbEntityValues, inputEntityValues, s => s.Id, true);
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<InboundEmail, InboundEmailDto> eventArgs)
        {
            _uploadedDocumentHandlingService
                .PromoteTemporaryDocuments<InboundEmailDocument, InboundEmailDocumentContent, InboundEmailDocumentMapping, IRecruitmentDbScope>(
                    eventArgs.InputDto.Documents
                );

            if (eventArgs.InputDto.WorkflowAction.HasValue)
            {
                HandleWorkflowAction(eventArgs.InputDto.WorkflowAction.Value, eventArgs.InputEntity, eventArgs.InputDto, eventArgs.Alerts);
            }
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<InboundEmail, IRecruitmentDbScope> eventArgs)
        {
            base.OnRecordDeleting(eventArgs);
            eventArgs.UnitOfWork.Repositories.InboundEmailDocuments.DeleteEach(p => p.InboundEmailId == eventArgs.DeletingRecord.Id);
        }

        protected override IEnumerable<Expression<Func<InboundEmail, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return c => c.To;
            yield return c => c.Body;
            yield return c => c.FromEmailAddress;
            yield return c => c.FromFullName;
            yield return c => c.Subject;

            if (searchCriteria.Includes(SearchAreaCodes.CandidateEmails) || searchCriteria.Includes(SearchAreaCodes.RecommendigPersonEmails))
            {
                if (searchCriteria.Includes(SearchAreaCodes.CandidateEmails))
                {
                    yield return c => c.Values.Where(v => v.Key == InboundEmailValueType.CandidateEmailAddress).Select(v => v.ValueString);
                }

                if (searchCriteria.Includes(SearchAreaCodes.RecommendigPersonEmails))
                {
                    yield return c => c.Values.Where(v => v.Key == InboundEmailValueType.RecommendingPersonEmailAddress).Select(v => v.ValueString);
                }

                yield break;
            }

            if (searchCriteria.Includes(SearchAreaCodes.Candidate))
            {
                yield return c => c.Values.Where(v => v.Key == InboundEmailValueType.CandidateFirstName).Select(v => v.ValueString);
                yield return c => c.Values.Where(v => v.Key == InboundEmailValueType.CandidateLastName).Select(v => v.ValueString);
                yield return c => c.Values.Where(v => v.Key == InboundEmailValueType.CandidatePhoneNumber).Select(v => v.ValueString);
            }

            if (searchCriteria.Includes(SearchAreaCodes.RecommendigPerson))
            {
                yield return c => c.Values.Where(v => v.Key == InboundEmailValueType.RecommendingPersonFirstName).Select(v => v.ValueString);
                yield return c => c.Values.Where(v => v.Key == InboundEmailValueType.RecommendingPersonLastName).Select(v => v.ValueString);
                yield return c => c.Values.Where(v => v.Key == InboundEmailValueType.RecommendingPersonPhoneNumber).Select(v => v.ValueString);
            }
        }

        protected override IQueryable<InboundEmail> ApplySearchTextCriteria(IQueryable<InboundEmail> records, QueryCriteria criteria)
        {
            if (criteria.SearchCriteria.Includes(SearchAreaCodes.Candidate) || criteria.SearchCriteria.Includes(SearchAreaCodes.RecommendigPerson))
            {
                var originalSearchCriteria = criteria.SearchCriteria;

                var newSearchCriteria = new SearchCriteria();

                if (criteria.SearchCriteria.Includes(SearchAreaCodes.Candidate))
                {
                    newSearchCriteria.Codes.Add(SearchAreaCodes.CandidateEmails);
                }

                if (criteria.SearchCriteria.Includes(SearchAreaCodes.RecommendigPerson))
                {
                    newSearchCriteria.Codes.Add(SearchAreaCodes.RecommendigPersonEmails);
                }

                criteria.SearchCriteria = newSearchCriteria;

                var recordsWithEmail = base.ApplySearchTextCriteria(records, criteria);

                if (recordsWithEmail.Any())
                {
                    return recordsWithEmail;
                }

                criteria.SearchCriteria = originalSearchCriteria;
            }

            return base.ApplySearchTextCriteria(records, criteria);
        }

        protected override NamedFilters<InboundEmail> NamedFilters => new NamedFilters<InboundEmail>(new[]
                {
                    new NamedFilter<InboundEmail>(FilterCodes.StatusDefault, GetNonClosedCondition()),
                    new NamedFilter<InboundEmail>(FilterCodes.StatusAll, e => true),
                    new NamedFilter<InboundEmail>(FilterCodes.MyRecords, e => e.ProcessedById == PrincipalProvider.Current.EmployeeId, SecurityRoleType.CanFilterInboundEmailByMyRecords),
                    new NamedFilter<InboundEmail>(FilterCodes.AnyOwner, e => true, SecurityRoleType.CanFilterInboundEmailByAnyOwner),
                    new NamedFilter<InboundEmail>(FilterCodes.InboundEmailsUnread, e => !e.Readers.Any(r => r.Id == PrincipalProvider.Current.Id.Value)),
                    new NamedFilter<InboundEmail>(FilterCodes.InboundEmailAllColors, e => true)
                }
                .Union(GetStatusBasedFilters())
                .Union(CardIndexServiceHelper.BuildSoftDeletableFilters<InboundEmail>())
                .Union(CardIndexServiceHelper.BuildEnumBasedFilters<InboundEmail, InboundEmailColorCategory>(i => i.EmailColorCategory)));

        private IEnumerable<NamedFilter<InboundEmail>> GetStatusBasedFilters()
        {
            Func<InboundEmailStatus, bool> predicate = e => true;

            if (!PrincipalProvider.Current.IsInRole(SecurityRoleType.CanViewAllInboundEmails))
            {
                predicate = e => e != InboundEmailStatus.New && e != InboundEmailStatus.SentForResearch;
            }
            else if (!PrincipalProvider.Current.IsInRole(SecurityRoleType.CanViewInboundEmailsSendForResearch))
            {
                predicate = e => e != InboundEmailStatus.SentForResearch;
            }

            return CardIndexServiceHelper.BuildEnumBasedFilters<InboundEmail, InboundEmailStatus>(u => u.Status, predicate);
        }

        private Expression<Func<InboundEmail, bool>> GetNonClosedCondition()
        {
            if (PrincipalProvider.Current.IsInRole(SecurityRoleType.CanFilterRecruitmentInboundEmailNonAssigned))
            {
                return e => e.Status == InboundEmailStatus.New;
            }

            return e => e.Status != InboundEmailStatus.Closed;
        }

        private void HandleWorkflowAction(InboundEmailWorkflowAction action, InboundEmail dbEntity, InboundEmailDto inputDto, IList<AlertDto> alerts)
        {
            switch (action)
            {
                case InboundEmailWorkflowAction.Assign:
                    AssignMessage(dbEntity.Id, inputDto.WorkflowProcessedById.Value);
                    break;

                case InboundEmailWorkflowAction.Unassign:
                    UnassignMessage(dbEntity.Id);
                    break;

                case InboundEmailWorkflowAction.Close:
                    CloseInboundEmail(dbEntity.Id, inputDto);
                    break;

                case InboundEmailWorkflowAction.SendForResearch:
                    SendForResearch(dbEntity.Id);
                    break;
            }
        }

        private void AddNewReaderToInboundEmail(IUnitOfWork<IRecruitmentDbScope> unitOfWork, long inboundEmailId)
        {
            var currentInboundEmail = unitOfWork.Repositories.InboundEmails.GetById(inboundEmailId);
            var hasBeenReadByCurrentUser = currentInboundEmail.Readers.Any(r => r.Id == PrincipalProvider.Current.Id.Value);

            if (!hasBeenReadByCurrentUser)
            {
                var currentUser = unitOfWork.Repositories.Users.GetById(PrincipalProvider.Current.Id.Value);

                currentInboundEmail.Readers.Add(currentUser);
            }
        }

        public void LogDataActivity(long inboundEmailId, ActivityType activityType, params string[] details)
        {
            details = details ?? new string[0];
            var detailsCount = details.Length;

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                if (activityType == ActivityType.ViewedRecord)
                {
                    AddNewReaderToInboundEmail(unitOfWork, inboundEmailId);
                }

                var activity = new InboundEmailDataActivity
                {
                    InboundEmailId = inboundEmailId,
                    PerformedByUserId = PrincipalProvider.Current.Id.Value,
                    ActivityType = activityType,
                    PerformedOn = TimeService.GetCurrentTime(),
                    S1 = detailsCount > 0 ? details[0] : null,
                    S2 = detailsCount > 1 ? details[1] : null,
                    S3 = detailsCount > 2 ? details[2] : null,
                };

                unitOfWork.Repositories.InboundEmailDataActivities.Add(activity);
                unitOfWork.Commit();
            }
        }
    }
}