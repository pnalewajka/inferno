﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Castle.Core.Internal;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class RecommendingPersonCardIndexDataService : CardIndexDataService<RecommendingPersonDto, RecommendingPerson, IRecruitmentDbScope>, IRecommendingPersonCardIndexDataService
    {
        private readonly IDataActivityLogService _dataActivityLogService;
        private readonly IEmployeeService _employeeService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public RecommendingPersonCardIndexDataService(
            ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies,
            IDataActivityLogService dataActivityLogService,
            IEmployeeService employeeService)
            : base(dependencies)
        {
            _dataActivityLogService = dataActivityLogService;
            _employeeService = employeeService;
        }

        protected override IEnumerable<Expression<Func<RecommendingPerson, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return r => r.FirstName;
            yield return r => r.LastName;
            yield return r => r.EmailAddress;
            yield return r => r.PhoneNumber;
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<RecommendingPerson, RecommendingPersonDto> recordAddedEventArgs)
        {
            _dataActivityLogService.LogRecommendingPersonDataActivity(recordAddedEventArgs.InputEntity.Id, ActivityType.CreatedRecord);

            if (recordAddedEventArgs.InputDto.ShouldAddRelatedDataConsent)
            {
                AddDataConsent(recordAddedEventArgs.InputEntity.Id);
            }
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<RecommendingPerson, RecommendingPersonDto, IRecruitmentDbScope> eventArgs)
        {
            eventArgs.InputEntity.DeletedOn = eventArgs.DbEntity.DeletedOn;

            base.OnRecordEditing(eventArgs);
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<RecommendingPerson, RecommendingPersonDto> recordEditedEventArgs)
        {
            _dataActivityLogService.LogRecommendingPersonDataActivity(recordEditedEventArgs.InputEntity.Id, ActivityType.UpdatedRecord);
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<RecommendingPerson, IRecruitmentDbScope> eventArgs)
        {
            base.OnRecordDeleting(eventArgs);

            if (eventArgs.DeletingRecord.DataOwners != null && eventArgs.DeletingRecord.DataOwners.Any())
            {
                eventArgs.UnitOfWork.Repositories.DataOwner.RemoveRange(eventArgs.DeletingRecord.DataOwners);
            }

            eventArgs.DeletingRecord.DeletedOn = TimeService.GetCurrentTime();
            eventArgs.UnitOfWork.Repositories.DataOwners.Where(d => d.RecommendingPersonId == eventArgs.DeletingRecord.Id).ForEach(d => d.RecommendingPersonId = null);
        }

        private void AddDataConsent(long recomendingPersonId)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var companyId = _employeeService.GetEmployeeCompanyId(PrincipalProvider.Current.EmployeeId);

                var newDataConsent = new DataConsent
                {
                    Type = DataConsentType.RecommendingPersonConsentRecommendation,
                    DataAdministratorId = companyId,
                    DataOwnerId = unitOfWork.Repositories.DataOwner.Where(o => o.RecommendingPersonId == recomendingPersonId).Single().Id
                };

                unitOfWork.Repositories.DataConsents.Add(newDataConsent);
                unitOfWork.Commit();
            }
        }

        protected override NamedFilters<RecommendingPerson> NamedFilters
        {
            get
            {
                return new NamedFilters<RecommendingPerson>(CardIndexServiceHelper.BuildSoftDeletableFilters<RecommendingPerson>());
            }
        }

        public override RecommendingPersonDto GetDefaultNewRecord()
        {
            var record = base.GetDefaultNewRecord();
            record.CoordinatorId = PrincipalProvider.Current.EmployeeId;

            return record;
        }
    }
}
