﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Dto.GDPR;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Interfaces.GDPR;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.Business.Recruitment.Enums;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Business.Recruitment.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class CandidateService : ICandidateService
    {
        private readonly IUploadedDocumentHandlingService _uploadedDocumentHandlingService;
        private readonly IClassMappingFactory _classMappingFactory;
        private readonly ISystemParameterService _systemParameterService;
        private readonly ITimeService _timeService;
        private readonly IDataActivityLogService _dataActivityLogService;
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;
        private readonly ICompanyService _companyService;
        private readonly IDataConsentCardIndexDataService _dataConsentCardIndexDataService;
        private readonly IPrincipalProvider _principalProvider;

        public CandidateService(
            IClassMappingFactory classMappingFactory,
            ISystemParameterService systemParameterService,
            ITimeService timeService,
            IDataActivityLogService dataActivityLogService,
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService,
            IUploadedDocumentHandlingService uploadedDocumentHandlingService,
            ICompanyService companyService,
            IDataConsentCardIndexDataService dataConsentCardIndexDataService,
            IPrincipalProvider principalProvider)
        {
            _classMappingFactory = classMappingFactory;
            _systemParameterService = systemParameterService;
            _unitOfWorkService = unitOfWorkService;
            _companyService = companyService;
            _dataConsentCardIndexDataService = dataConsentCardIndexDataService;
            _principalProvider = principalProvider;
            _timeService = timeService;
            _dataActivityLogService = dataActivityLogService;
            _uploadedDocumentHandlingService = uploadedDocumentHandlingService;
        }

        public void AnonymizeCandidateById(long id, bool isAnonymizedPermanently = false)
        {
            const string firstNameAnonymized = "Anonymized";
            const string lastNameAnonymized = "Anonymized";
            const string emailAnonymized = "Anonymized-{0}@Anonymized";

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var candidate = unitOfWork.Repositories.Candidates.GetByIdOrDefault(id);

                if (candidate == null)
                {
                    throw new ArgumentException(nameof(id));
                }

                if (isAnonymizedPermanently)
                {
                    candidate.FirstName = firstNameAnonymized;
                    candidate.LastName = lastNameAnonymized;
                    candidate.EmailAddress = string.Format(emailAnonymized, candidate.Id);
                    candidate.OtherEmailAddress = string.Empty;
                    candidate.MobilePhone = string.Empty;
                    candidate.OtherPhone = string.Empty;
                    candidate.SkypeLogin = string.Empty;
                    candidate.SocialLink = string.Empty;
                    candidate.IsAnonymized = true;
                }
                else
                {
                    candidate.DeletedOn = _timeService.GetCurrentTime();
                    unitOfWork.Repositories.Candidates.Delete(candidate);
                }

                unitOfWork.Commit();
            }
        }

        public void ToggleWatching(ICollection<long> candidateIds, long employeeId, WatchingStatus candidateWatchingStatus)
        {
            if (candidateIds == null || !candidateIds.Any())
            {
                throw new ArgumentException(nameof(candidateIds));
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var candidates = unitOfWork.Repositories.Candidates.Filtered().GetByIds(candidateIds);
                var employee = unitOfWork.Repositories.Employees.GetById(employeeId);

                WatcherHelper.Toggle(candidates, employee, candidateWatchingStatus);

                unitOfWork.Commit();

                foreach (var candidate in candidates)
                {
                    _dataActivityLogService.LogCandidateDataActivity(candidate.Id,
                        candidateWatchingStatus == WatchingStatus.Watch ? ActivityType.Watch : ActivityType.Unwatch, GetType().Name);
                }
            }
        }

        public void AddDataConsent(long candidateId, DataConsentType consentType, string consentDetails, DocumentDto[] documents)
        {
            var companyId = GetDefaultCompanyId();

            DateTime? expiresOn = null;

            if (DataConsentBusinessLogic.DoesConsentTypeRequireExpirationDate.Call(consentType))
            {
                expiresOn = _timeService.GetCurrentDate().AddDays(_systemParameterService.GetParameter<int>(ParameterKeys.RecruitmentDefaultConsentExpirationDays));
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var dataConsentDto = new DataConsentDto
                {
                    Type = consentType,
                    Scope = consentDetails,
                    DataOwnerId = unitOfWork.Repositories.DataOwner.Single(o => o.CandidateId == candidateId).Id,
                    DataAdministratorId = companyId,
                    ExpiresOn = expiresOn,
                    Documents = documents.Where(d => d.IsTemporary).ToArray()
                };

                var addResult = _dataConsentCardIndexDataService.AddRecord(dataConsentDto);
                var dataConsent = unitOfWork.Repositories.DataConsents.GetById(addResult.AddedRecordId);

                var copiedAttachments = new List<DataConsentDocument>();

                foreach (var document in documents.Where(d => !d.IsTemporary))
                {
                    var inboundEmailDocument = unitOfWork.Repositories.InboundEmailDocuments.GetById(document.DocumentId.Value);

                    dataConsent.Attachments.Add(new DataConsentDocument
                    {
                        ContentType = inboundEmailDocument.ContentType,
                        ContentLength = inboundEmailDocument.ContentLength,
                        Name = inboundEmailDocument.Name,
                        DocumentContent = new DataConsentDocumentContent
                        {
                            Content = inboundEmailDocument.DocumentContent.Content
                        }
                    });
                }

                unitOfWork.Commit();

                _dataActivityLogService.LogCandidateDataActivity(candidateId, ActivityType.CreatedRecord, ReferenceType.DataConsent, dataConsent.Id);
            }
        }

        public CandidateDto GetCandidateOrDefaultById(long candidateId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var candidate = unitOfWork.Repositories.Candidates.GetByIdOrDefault(candidateId);

                return candidate != null
                     ? _classMappingFactory.CreateMapping<Candidate, CandidateDto>().CreateFromSource(candidate)
                     : null;
            }
        }

        public BoolResult AddNote(AddNoteDto note)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var candidateNote = _classMappingFactory.CreateMapping<AddNoteDto, CandidateNote>().CreateFromSource(note);
                candidateNote.NoteType = NoteType.UserNote;

                unitOfWork.Repositories.CandidateNotes.Add(candidateNote);
                unitOfWork.Commit();
            }

            return BoolResult.Success;
        }

        private long GetDefaultCompanyId()
        {
            var defaultCompanyCode = _systemParameterService.GetParameter<string>(ParameterKeys.RecruitmentDefaultCompanyActiveDirectoryCode);
            var defaultCompanyId = _companyService.GetCompanyIdOrDefaultByActiveDirectorycode(defaultCompanyCode);

            return defaultCompanyId.Value;
        }

        public void UpdateLastActivity(long candidateId, ReferenceType lastRefereceType, long lastReferenceId)
        {
            var currentEmployeeId = _principalProvider.Current.EmployeeId;
            var currentTime = _timeService.GetCurrentTime();

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var candidate = unitOfWork.Repositories.Candidates.GetById(candidateId);

                candidate.LastActivityById = currentEmployeeId;
                candidate.LastActivityOn = currentTime;
                candidate.LastReferenceId = lastReferenceId;
                candidate.LastReferenceType = lastRefereceType;

                unitOfWork.Commit();
            }
        }

        public BoolResult ToogleFavorite(ICollection<long> candidateId, FavoriteStatus favoriteStatus)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var currentEmployeeId = _principalProvider.Current.EmployeeId;

                var candidates = unitOfWork.Repositories.Candidates.GetByIds(candidateId);
                var employee = unitOfWork.Repositories.Employees.GetById(currentEmployeeId);

                foreach (var candidate in candidates)
                {
                    if (favoriteStatus == FavoriteStatus.Favorite)
                    {
                        candidate.Favorites.Add(employee);
                    }
                    else
                    {
                        var removeStatus = RemoveFromFavorites(candidate, employee);

                        if (!removeStatus.IsSuccessful)
                        {
                            return removeStatus;
                        }
                    }
                }

                unitOfWork.Commit();
            }

            return BoolResult.Success;
        }

        private BoolResult RemoveFromFavorites(Candidate candidate, Employee employee)
        {
            if (candidate.Favorites.All(e => e.Id != employee.Id))
            {
                return new BoolResult(false, new List<AlertDto>
                {
                    new AlertDto
                    {
                        Message = CandidateResources.CandidateWasRemovedFromFavorite,
                        Type = AlertType.Error
                    }
                });
            }

            candidate.Favorites.Remove(employee);

            return BoolResult.Success;
        }

        public bool IsWatchedByCurrentUser(long candidateId)
        {
            if (!_principalProvider.IsSet)
            {
                return false;
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Candidates
                    .Any(c => c.Id == candidateId && c.Watchers.Any(w => w.Id == _principalProvider.Current.EmployeeId));
            }
        }
    }
}