﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class RecommendingPersonNoteCardIndexDataService : CardIndexDataService<RecommendingPersonNoteDto, RecommendingPersonNote, IRecruitmentDbScope>, IRecommendingPersonNoteCardIndexDataService
    {
        private readonly IDataActivityLogService _dataActivityLogService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public RecommendingPersonNoteCardIndexDataService(ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies,
            IDataActivityLogService dataActivityLogService)
            : base(dependencies)
        {
            _dataActivityLogService = dataActivityLogService;
        }

        public override IQueryable<RecommendingPersonNote> ApplyContextFiltering(IQueryable<RecommendingPersonNote> records, object context)
        {
            if (context is ParentIdContext parentIdContext)
            {
                return records.Where(c => c.RecommendingPersonId == parentIdContext.ParentId);
            }

            return base.ApplyContextFiltering(records, context);
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<RecommendingPersonNote, RecommendingPersonNoteDto> recordAddedEventArgs)
        {
            _dataActivityLogService.LogRecommendingPersonDataActivity(recordAddedEventArgs.InputEntity.RecommendingPersonId, ActivityType.CreatedRecord, ReferenceType.RecommendingPersonNote, recordAddedEventArgs.InputEntity.Id);
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<RecommendingPersonNote, RecommendingPersonNoteDto> recordEditedEventArgs)
        {
            _dataActivityLogService.LogRecommendingPersonDataActivity(recordEditedEventArgs.InputEntity.RecommendingPersonId, ActivityType.UpdatedRecord, ReferenceType.RecommendingPersonNote, recordEditedEventArgs.InputEntity.Id);
        }

        protected override void OnRecordsDeleted(RecordDeletedEventArgs<RecommendingPersonNote> eventArgs)
        {
            foreach (var note in eventArgs.DeletedEntities)
            {
                _dataActivityLogService.LogRecommendingPersonDataActivity(note.RecommendingPersonId, ActivityType.RemovedRecord, ReferenceType.RecommendingPersonNote, note.Id);
            }
        }

        protected override NamedFilters<RecommendingPersonNote> NamedFilters
        {
            get
            {
                return new NamedFilters<RecommendingPersonNote>(CardIndexServiceHelper.BuildSoftDeletableFilters<RecommendingPersonNote>());
            }
        }

        protected override IEnumerable<Expression<Func<RecommendingPersonNote, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return n => n.Content;
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<RecommendingPersonNote, RecommendingPersonNoteDto, IRecruitmentDbScope> eventArgs)
        {
            base.OnRecordAdding(eventArgs);

            AdjustMentionedEmployeeId(eventArgs.InputEntity, eventArgs.UnitOfWork);

            eventArgs.InputEntity.MentionedEmployees = EntityMergingService.CreateEntitiesFromIds<Employee, IRecruitmentDbScope>(eventArgs.UnitOfWork, eventArgs.InputEntity.MentionedEmployees.GetIds());
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<RecommendingPersonNote, RecommendingPersonNoteDto, IRecruitmentDbScope> eventArgs)
        {
            base.OnRecordEditing(eventArgs);

            AdjustMentionedEmployeeId(eventArgs.InputEntity, eventArgs.UnitOfWork);

            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.MentionedEmployees, eventArgs.InputEntity.MentionedEmployees);
        }

        private void AdjustMentionedEmployeeId(RecommendingPersonNote recommendingPersonNote, IUnitOfWork<IRecruitmentDbScope> unitOfWork)
        {
            var employeeIds = MentionItemHelper.GetTokenIds(recommendingPersonNote.Content).Distinct();

            recommendingPersonNote.MentionedEmployees = unitOfWork.Repositories.Employees.GetByIds(employeeIds).ToList();
        }
    }
}
