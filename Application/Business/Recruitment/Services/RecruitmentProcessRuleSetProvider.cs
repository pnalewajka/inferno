﻿using System.Collections.Generic;
using Smt.Atomic.Business.Recruitment.Dto.RecruitmentProcessStepAction;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class RecruitmentProcessWorkflowRulesetProvider
    {
        public RecruitmentProcessStepRuleset[] Ruleset { get; }

        public RecruitmentProcessWorkflowRulesetProvider()
        {
            Ruleset = new[]
            {
                GetHrCallRule(),
                GetHrInterviewRule(),
                GetHiringManagerResumeApprovalRule(),
                GetHiringManagerInterviewRule(),
                GetContractNegotiationsRule(),
                GetHiringDecisionRule(),
                GetClientInterviewRule(),
                GetTechnicalReviewRule(),
                GetClientResumeApprovalRule()
            };
        }

        private RecruitmentProcessStepRuleset GetHrCallRule() => new RecruitmentProcessStepRuleset
        {
            StepType = RecruitmentProcessStepType.HrCall,
            AssigneeActions = new[]
            {
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.RejectCandidate),
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.ApproveCandidate),
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.AddHrInterview)
                {
                    AddedStepType = RecruitmentProcessStepType.HrInterview
                },
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.SendResumeToHiringManager)
                {
                    AddedStepType = RecruitmentProcessStepType.HiringManagerResumeApproval,
                    AllowCloseStepDecision = true
                },
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.AddHiringManagerInterview)
                {
                    AddedStepType = RecruitmentProcessStepType.HiringManagerInterview
                },
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.AddTechnicalReview)
                {
                    AddedStepType = RecruitmentProcessStepType.TechnicalReview,
                    AllowCloseStepDecision = true
                }
            },
            RecruiterActions = GetDefaultRecruiterRules(false)
        };

        private RecruitmentProcessStepRuleset GetHrInterviewRule() => new RecruitmentProcessStepRuleset
        {
            StepType = RecruitmentProcessStepType.HrInterview,
            AssigneeActions = new[]
            {
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.RejectCandidate),
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.ApproveCandidate),
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.SendResumeToHiringManager)
                {
                    AddedStepType = RecruitmentProcessStepType.HiringManagerResumeApproval,
                    AllowCloseStepDecision = true
                },
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.AddHiringManagerInterview)
                {
                    AddedStepType = RecruitmentProcessStepType.HiringManagerInterview
                },
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.AddTechnicalReview)
                {
                    AddedStepType = RecruitmentProcessStepType.TechnicalReview,
                    AllowCloseStepDecision = true
                }
            },
            RecruiterActions = GetDefaultRecruiterRules(false)
        };

        private RecruitmentProcessStepRuleset GetHiringManagerResumeApprovalRule() => new RecruitmentProcessStepRuleset
        {
            StepType = RecruitmentProcessStepType.HiringManagerResumeApproval,
            AssigneeActions = new[]
            {
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.ApproveCandidate),
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.RejectCandidate)
                {
                    TypicalConditions = rp => rp.JobOpening?.CustomerId == null
                },
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.SendResumeToClient)
                {
                    AddedStepType = RecruitmentProcessStepType.ClientResumeApproval,
                    TypicalConditions = rp => rp.JobOpening?.CustomerId != null
                },
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.PutProcessOnHold),
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.AddHiringManagerInterview)
                {
                    AddedStepType = RecruitmentProcessStepType.HiringManagerInterview
                }
            },
            RecruiterActions = GetDefaultRecruiterRules(true)
        };

        private RecruitmentProcessStepRuleset GetHiringManagerInterviewRule() => new RecruitmentProcessStepRuleset
        {
            StepType = RecruitmentProcessStepType.HiringManagerInterview,
            AssigneeActions = new[]
            {
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.RejectCandidate),
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.ApproveCandidate),
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.SendResumeToClient)
                {
                    AddedStepType = RecruitmentProcessStepType.ClientResumeApproval,
                    TypicalConditions = rp => rp.JobOpening?.CustomerId != null
                },
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.AddHiringManagerInterview)
                {
                    AddedStepType = RecruitmentProcessStepType.HiringManagerInterview
                },
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.PutProcessOnHold),
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.StartContractNegotations)
                {
                    AddedStepType = RecruitmentProcessStepType.ContractNegotiations
                }
            },
            RecruiterActions = GetDefaultRecruiterRules(true)
        };

        private RecruitmentProcessStepRuleset GetClientResumeApprovalRule() => new RecruitmentProcessStepRuleset
        {
            StepType = RecruitmentProcessStepType.ClientResumeApproval,
            AssigneeActions = new[]
            {
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.ApproveCandidate),
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.RejectCandidate),
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.AddClientInterview)
                {
                    AddedStepType = RecruitmentProcessStepType.ClientInterview,
                    TypicalConditions = rp => rp.JobOpening?.CustomerId != null
                },
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.PutProcessOnHold),
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.AddHiringManagerInterview)
                {
                    AddedStepType = RecruitmentProcessStepType.HiringManagerInterview
                },
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.StartContractNegotations)
                {
                    AddedStepType = RecruitmentProcessStepType.ContractNegotiations
                }
            },
            RecruiterActions = GetDefaultRecruiterRules(true)
        };

        private RecruitmentProcessStepRuleset GetTechnicalReviewRule() => new RecruitmentProcessStepRuleset
        {
            StepType = RecruitmentProcessStepType.TechnicalReview,
            AssigneeActions = new[]
            {
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.RejectCandidate),
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.ApproveCandidate),
            },
            RecruiterActions = GetDefaultRecruiterRules(true)
        };

        private RecruitmentProcessStepRuleset GetClientInterviewRule() => new RecruitmentProcessStepRuleset
        {
            StepType = RecruitmentProcessStepType.ClientInterview,
            AssigneeActions = new[]
            {
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.ApproveCandidate),
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.RejectCandidate),
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.AddClientInterview)
                {
                    AddedStepType = RecruitmentProcessStepType.ClientInterview,
                    TypicalConditions = rp => rp.JobOpening?.CustomerId != null,
                },
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.AddHiringManagerInterview)
                {
                    AddedStepType = RecruitmentProcessStepType.HiringManagerInterview
                },
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.PutProcessOnHold),
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.StartContractNegotations)
                {
                    AddedStepType = RecruitmentProcessStepType.ContractNegotiations
                }
            },
            RecruiterActions = GetDefaultRecruiterRules(true)
        };

        private RecruitmentProcessStepRuleset GetHiringDecisionRule() => new RecruitmentProcessStepRuleset
        {
            StepType = RecruitmentProcessStepType.HiringDecision,
            AssigneeActions = new[]
            {
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.ApproveCandidate),
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.RejectCandidate),
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.AddClientInterview)
                {
                    AddedStepType = RecruitmentProcessStepType.ClientInterview,
                    TypicalConditions = rp => rp.JobOpening?.CustomerId != null
                },
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.AddHiringManagerInterview)
                {
                    AddedStepType = RecruitmentProcessStepType.HiringManagerInterview
                },
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.PutProcessOnHold),
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.StartContractNegotations)
                {
                    AddedStepType = RecruitmentProcessStepType.ContractNegotiations
                }
            },
            RecruiterActions = GetDefaultRecruiterRules(true)
        };

        private RecruitmentProcessStepRuleset GetContractNegotiationsRule() => new RecruitmentProcessStepRuleset
        {
            StepType = RecruitmentProcessStepType.ContractNegotiations,
            AssigneeActions = new[]
            {
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.RejectCandidate),
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.ApproveCandidate)
            },
            RecruiterActions = GetDefaultRecruiterRules(true)
        };

        private RecruitmentProcessStepRule[] GetDefaultRecruiterRules(bool isCancelAllowed)
        {
            var rules = new List<RecruitmentProcessStepRule>
            {
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.RejectCandidate),
                new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.ApproveCandidate),
            };

            if (isCancelAllowed)
            {
                rules.Add(new RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType.CancelProcessStep));
            }

            return rules.ToArray();
        }
    }
}
