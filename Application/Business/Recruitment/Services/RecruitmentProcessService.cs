﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.PeopleManagement.Helpers;
using Smt.Atomic.Business.PeopleManagement.Interfaces;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Enums;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Business.Recruitment.Resources;
using Smt.Atomic.Business.Recruitment.Services.Notification.RecruitmentProcessServices;
using Smt.Atomic.Business.Recruitment.SnapshotComparer;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Presentation.Common.Interfaces;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class RecruitmentProcessService : IRecruitmentProcessService
    {
        private readonly IEmployeeService _employeeService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IRecruitmentProcessStepNotificationService _recruitmentProcessStepNotificationService;
        private readonly IRecruitmentProcessNotificationService _recruitmentProcessNotificationService;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IOnboardingRequestService _onboardingRequestService;
        private readonly IRequestCardIndexDataService _requestCardIndexDataService;
        private readonly ITimeService _timeService;
        private readonly IClassMappingFactory _classMappingFactory;
        private readonly ICandidateService _candidateService;
        private readonly ICityService _cityService;
        private readonly ILocationService _locationService;
        private readonly IJobOpeningService _jobOpeningService;
        private readonly IJobTitleService _jobTitleService;
        private readonly IAlertService _alertService;
        private readonly IContractTypeService _contractTypeService;
        private readonly IUserService _userService;
        private readonly IDataActivityLogService _dataActivityLogService;
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;
        private readonly IRecruitmentProcessHistoryService _recruitmentProcessDataActivityService;

        public RecruitmentProcessService(
            IEmployeeService employeeService,
            IMessageTemplateService messageTemplateService,
            IPrincipalProvider principalProvider,
            IRecruitmentProcessStepNotificationService recruitmentProcessStepNotificationService,
            IRecruitmentProcessNotificationService recruitmentProcessNotificationService,
            IReliableEmailService reliableEmailService,
            ISystemParameterService systemParameterService,
            IOnboardingRequestService onboardingRequestService,
            IRequestCardIndexDataService requestCardIndexDataService,
            ITimeService timeService,
            IClassMappingFactory classMappingFactory,
            ICandidateService candidateService,
            ICityService cityService,
            ILocationService locationService,
            IJobOpeningService jobOpeningService,
            IJobTitleService jobTitleService,
            IAlertService alertService,
            IContractTypeService contractTypeService,
            IUserService userService,
            IDataActivityLogService dataActivityLogService,
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService,
            IRecruitmentProcessHistoryService recruitmentProcessDataActivityService)
        {
            _employeeService = employeeService;
            _messageTemplateService = messageTemplateService;
            _recruitmentProcessStepNotificationService = recruitmentProcessStepNotificationService;
            _recruitmentProcessNotificationService = recruitmentProcessNotificationService;
            _reliableEmailService = reliableEmailService;
            _principalProvider = principalProvider;
            _systemParameterService = systemParameterService;
            _timeService = timeService;
            _classMappingFactory = classMappingFactory;
            _candidateService = candidateService;
            _cityService = cityService;
            _locationService = locationService;
            _jobOpeningService = jobOpeningService;
            _jobTitleService = jobTitleService;
            _alertService = alertService;
            _contractTypeService = contractTypeService;
            _userService = userService;
            _dataActivityLogService = dataActivityLogService;
            _unitOfWorkService = unitOfWorkService;
            _recruitmentProcessDataActivityService = recruitmentProcessDataActivityService;
            _onboardingRequestService = onboardingRequestService;
            _requestCardIndexDataService = requestCardIndexDataService;
        }

        public void ToggleWatching(long[] recruitmentProcessIds, long employeeId, WatchingStatus recruitmentProcessStatus)
        {
            if (recruitmentProcessIds == null || !recruitmentProcessIds.Any())
            {
                throw new ArgumentException(nameof(recruitmentProcessIds));
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recruitmentProcesses = unitOfWork.Repositories.RecruitmentProcesses
                    .Filtered()
                    .Include(rp => rp.Watchers)
                    .GetByIds(recruitmentProcessIds);

                var employee = unitOfWork.Repositories.Employees.GetById(employeeId);

                WatcherHelper.Toggle(recruitmentProcesses, employee, recruitmentProcessStatus);

                unitOfWork.Commit();

                foreach (var recruitmentProcess in recruitmentProcesses)
                {
                    _dataActivityLogService.LogCandidateDataActivity(
                        GetCandidateId(recruitmentProcess.Id),
                        recruitmentProcessStatus == WatchingStatus.Watch ? ActivityType.Watch : ActivityType.Unwatch,
                        ReferenceType.RecruitmentProcessStep,
                        recruitmentProcess.Id,
                        GetType().Name);

                    _recruitmentProcessDataActivityService.LogActivity(
                        recruitmentProcess.Id,
                        recruitmentProcessStatus == WatchingStatus.Watch ? RecruitmentProcessHistoryEntryType.Watched : RecruitmentProcessHistoryEntryType.Unwatched);
                }
            }
        }

        public void CloseRecruitmentProcess(RecruitmentProcessCloseDto dto)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recruitmentProcess = unitOfWork.Repositories.RecruitmentProcesses.Include(p => p.ProcessSteps).Filtered().GetById(dto.Id);

                if (recruitmentProcess.Status != RecruitmentProcessStatus.Closed)
                {
                    var noteText = string.Format(RecruitmentProcessResources.RecruitmentProcessCloseInformation, recruitmentProcess.ClosedReason?.GetDescription(CultureInfo.GetCultureInfo(CultureCodes.English)));

                    recruitmentProcess.ClosedReason = dto.Reason;
                    recruitmentProcess.ClosedComment = dto.Comment;
                    recruitmentProcess.ClosedOn = _timeService.GetCurrentTime();
                    recruitmentProcess.Status = RecruitmentProcessStatus.Closed;

                    AddStatusNote(unitOfWork, recruitmentProcess, noteText);

                    var stepsToCancel = recruitmentProcess.ProcessSteps.Where(s => s.Status == RecruitmentProcessStepStatus.ToDo || s.Status == RecruitmentProcessStepStatus.Draft);

                    foreach (var processStep in stepsToCancel)
                    {
                        processStep.Status = RecruitmentProcessStepStatus.Cancelled;
                        processStep.DecidedOn = _timeService.GetCurrentTime();
                        _recruitmentProcessStepNotificationService.NotifyAllPartiesAboutStepCancelled(processStep.Id);
                    }

                    unitOfWork.Commit();

                    foreach (var watcher in recruitmentProcess.Watchers)
                    {
                        SendRecruitmentProcessCloseNotoficationEmail(recruitmentProcess, watcher);
                    }

                    _recruitmentProcessDataActivityService.LogActivity(dto.Id, RecruitmentProcessHistoryEntryType.Closed);
                }
            }
        }

        public void AddNoteToRecruitmentProcess(AddNoteDto note)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recruitmentProcessNote = new RecruitmentProcessNote
                {
                    RecruitmentProcessId = note.ParentId,
                    Content = note.Content,
                    NoteVisibility = note.NoteVisibility,
                    NoteType = NoteType.UserNote,
                    IsDeleted = false,
                };

                unitOfWork.Repositories.RecruitmentProcessNotes.Add(recruitmentProcessNote);
                unitOfWork.Commit();

                _recruitmentProcessDataActivityService.LogActivity(note.ParentId, RecruitmentProcessHistoryEntryType.AddedNote);
            }
        }

        public BoolResult PutRecruitmentProcessOnHold(long id, long? processStepId = null)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recruitmentProcess = unitOfWork.Repositories.RecruitmentProcesses.Filtered().GetById(id);

                if (recruitmentProcess.Status != RecruitmentProcessStatus.OnHold && recruitmentProcess.Status != RecruitmentProcessStatus.Closed)
                {
                    var employee = unitOfWork.Repositories.Employees.GetById(_principalProvider.Current.EmployeeId);
                    var noteText = string.Format(RecruitmentProcessResources.PutOnHoldNoteContent, employee.FullName);

                    recruitmentProcess.Status = RecruitmentProcessStatus.OnHold;

                    AddStatusNote(unitOfWork, recruitmentProcess, noteText);

                    unitOfWork.Commit();

                    if (processStepId.HasValue)
                    {
                        _recruitmentProcessStepNotificationService.NotifyAboutProcessPutOnHold(processStepId.Value);
                    }
                    else
                    {
                        _recruitmentProcessNotificationService.NotifyAboutProcessPutOnHold(id);
                    }

                    _recruitmentProcessDataActivityService.LogActivity(id, RecruitmentProcessHistoryEntryType.PutOnHold, processStepId);

                    return new BoolResult(true);
                }

                return new BoolResult(false);
            }
        }

        public void TakeRecruitmentProcessOffHold(long id)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recruitmentProcess = unitOfWork.Repositories.RecruitmentProcesses.Filtered().GetById(id);

                if (recruitmentProcess.Status == RecruitmentProcessStatus.OnHold)
                {
                    var employee = unitOfWork.Repositories.Employees.GetById(_principalProvider.Current.EmployeeId);
                    var noteText = string.Format(RecruitmentProcessResources.TakeOffHoldNoteContent, employee.FullName);

                    recruitmentProcess.Status = RecruitmentProcessStatus.Active;

                    AddStatusNote(unitOfWork, recruitmentProcess, noteText);

                    unitOfWork.Commit();

                    _recruitmentProcessNotificationService.NotifyRecruiterAboutProcessTakenOffHold(id);
                    _recruitmentProcessNotificationService.NotifyOtherDecisionMakersAboutProcessTakenOffHold(id);
                    _recruitmentProcessNotificationService.NotifyWatcherAboutProcessTakenOffHold(id);

                    _recruitmentProcessDataActivityService.LogActivity(id, RecruitmentProcessHistoryEntryType.TookOffHold);
                }
            }
        }

        private void AddStatusNote(IUnitOfWork<IRecruitmentDbScope> unitOfWork, RecruitmentProcess recruitmentProcess, string content)
        {
            var note = new RecruitmentProcessNote
            {
                RecruitmentProcess = recruitmentProcess,
                NoteVisibility = NoteVisibility.Public,
                NoteType = NoteType.SystemNote,
                IsDeleted = false,
                Content = content
            };

            unitOfWork.Repositories.RecruitmentProcessNotes.Add(note);
        }

        private void SendRecruitmentProcessCloseNotoficationEmail(RecruitmentProcess recruitmentProcess, Employee watcher)
        {
            var recipient = new EmailRecipientDto
            {
                EmailAddress = watcher.Email,
                FullName = watcher.FullName
            };

            var serverAddress = _systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);

            var recruitmentProcessNotificationDto = new RecruitmentProcessClosedReminderDto()
            {
                ServerAddress = serverAddress,
                Comment = recruitmentProcess.ClosedComment,
                FirstName = watcher.FirstName,
                Reason = recruitmentProcess?.ClosedReason?.GetDescription(CultureInfo.GetCultureInfo(CultureCodes.English)),
                RecruitmentProcessId = recruitmentProcess.Id,
                DisplayName = RecruitmentProcessBusinessLogic.DisplayName.Call(recruitmentProcess),
            };

            var emailSubject = _messageTemplateService.ResolveTemplate(TemplateCodes.RecruitmentProcessClosedSubject, recruitmentProcessNotificationDto);
            var emailBody = _messageTemplateService.ResolveTemplate(TemplateCodes.RecruitmentProcessClosedBody, recruitmentProcessNotificationDto);

            var emailMessageDto = new EmailMessageDto
            {
                IsHtml = emailBody.IsHtml,
                Subject = emailSubject.Content,
                Body = emailBody.Content,
                Recipients = new List<EmailRecipientDto> { recipient }
            };

            _reliableEmailService.EnqueueMessage(emailMessageDto);
        }

        public AddRecordResult AddOnboardingRequest(long recruitmentProcessStepId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recruitmentProcessStep = unitOfWork.Repositories.RecruitmentProcessSteps
                    .Include(s => s.AssignedEmployee)
                    .Include(s => s.RecruitmentProcess)
                    .Include(s => s.RecruitmentProcess.JobApplication)
                    .SelectById(recruitmentProcessStepId, s => new { s.AssignedEmployee, s.RecruitmentProcess });
                var recruitmentProcess = recruitmentProcessStep.RecruitmentProcess;
                var onboardingRequest = (OnboardingRequestDto)_onboardingRequestService.GetDraft(null);

                var candidate = recruitmentProcess.Candidate;

                onboardingRequest.FirstName = candidate.FirstName;
                onboardingRequest.LastName = candidate.LastName;
                onboardingRequest.Login = UserLoginHelper.GetSuggestedLogin(candidate.FirstName, candidate.LastName);

                onboardingRequest.RecruiterEmployeeId = recruitmentProcess.RecruiterId;

                onboardingRequest.LocationId = recruitmentProcess.Final.LocationId.Value;
                onboardingRequest.StartDate = recruitmentProcess.Final.StartDate.Value;
                onboardingRequest.SignedOn = recruitmentProcess.Final.SignedDate.Value;
                onboardingRequest.ContractTypeId = recruitmentProcess.Final.LongTermContractTypeId.Value;
                onboardingRequest.ProbationPeriodContractTypeId = recruitmentProcess.Final.TrialContractTypeId.Value;
                onboardingRequest.Compensation = recruitmentProcess.Final.LongTermSalary;
                onboardingRequest.ProbationPeriodCompensation = recruitmentProcess.Final.TrialSalary;
                onboardingRequest.JobTitleId = recruitmentProcess.Final.PositionId.Value;
                onboardingRequest.IsComingFromReferralProgram = recruitmentProcess.JobApplication?.IsEligibleForReferralProgram ?? false;

                onboardingRequest.OrgUnitId = recruitmentProcess.JobOpening.OrgUnitId;
                onboardingRequest.CompanyId = recruitmentProcess.JobOpening.CompanyId;
                onboardingRequest.JobProfileId = recruitmentProcess.JobOpening.JobProfiles.First().Id;

                onboardingRequest.IsCreatedAutomatically = true;

                var requestDto = new RequestDto
                {
                    RequestType = RequestType.OnboardingRequest,
                    RequestObject = onboardingRequest,
                    RequestingUserId = recruitmentProcessStep.AssignedEmployee.UserId.Value,
                };

                var currentUserId = _principalProvider.Current.Id;

                if (currentUserId.HasValue && currentUserId != requestDto.RequestingUserId)
                {
                    requestDto.WatchersUserIds = new[] { currentUserId.Value };
                }

                var response = _requestCardIndexDataService.AddRecord(requestDto);

                if (response.IsSuccessful)
                {
                    recruitmentProcess.OnboardingRequestId = response.AddedRecordId;
                    unitOfWork.Commit();
                }

                return response;
            }
        }

        public long? GetOnboardingRequestId(long recruitmentProcessId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.RecruitmentProcesses.GetById(recruitmentProcessId).OnboardingRequestId;
            }
        }

        public BoolResult CloneRecruitmentProcess(long recruitmentProcessId, long jobOpeningId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recruitmentProcess = unitOfWork.Repositories.RecruitmentProcesses
                    .Include(s => s.Screening)
                    .Include(s => s.ProcessSteps)
                    .GetById(recruitmentProcessId);

                if (CandidateBusinessLogic.HasActiveProcessesForJobOpening.Call(recruitmentProcess.Candidate, jobOpeningId))
                {
                    return new BoolResult(false, new[]
                    {
                        new AlertDto
                        {
                            Message = RecruitmentProcessResources.CandidateHasOtherProcessesError,
                            Type = AlertType.Error
                        }
                    });
                }

                var clonedRecruitmentProcess = EntityHelper.ShallowClone(recruitmentProcess);

                clonedRecruitmentProcess.JobOpeningId = jobOpeningId;
                clonedRecruitmentProcess.Status = RecruitmentProcessStatus.Active;

                if (recruitmentProcess.Screening != null)
                {
                    var screeningCloned = EntityHelper.ShallowClone(recruitmentProcess.Screening);
                    clonedRecruitmentProcess.Screening = screeningCloned;
                }

                var doneHrSteps = recruitmentProcess.ProcessSteps
                    .Where(s => s.Type == RecruitmentProcessStepType.HrCall || s.Type == RecruitmentProcessStepType.HrInterview)
                    .Where(s => s.Status == RecruitmentProcessStepStatus.Done);

                clonedRecruitmentProcess.ProcessSteps = new List<RecruitmentProcessStep>();

                foreach (var step in doneHrSteps)
                {
                    var clonedStep = EntityHelper.ShallowClone(step);

                    clonedStep.RecruitmentProcessId = clonedRecruitmentProcess.Id;
                    clonedStep.CloneOfStepId = step.Id;

                    clonedRecruitmentProcess.ProcessSteps.Add(clonedStep);
                }

                unitOfWork.Repositories.RecruitmentProcesses.Add(clonedRecruitmentProcess);
                unitOfWork.Commit();

                _recruitmentProcessDataActivityService.LogActivity(recruitmentProcessId, RecruitmentProcessHistoryEntryType.Cloned);

                return BoolResult.Success;
            }
        }

        public void NotifyWatchersAboutRecordChanges(RecruitmentProcess sourceEntity, RecruitmentProcess inputEntity)
        {
            var mapping = _classMappingFactory.CreateMapping<RecruitmentProcess, RecruitmentProcessDto>();
            var sourceDto = mapping.CreateFromSource(sourceEntity);
            var innputDto = mapping.CreateFromSource(inputEntity);

            var snapshotComparer = new SnapshotComparer<RecruitmentProcessDto>();

            SetComparableFields(snapshotComparer);
            SetRecordChangeConverters(snapshotComparer);
            SetSecuredFields(snapshotComparer);

            snapshotComparer.Compare(sourceDto, innputDto);

            foreach (var watcher in inputEntity.Watchers)
            {
                var employee = _employeeService.GetEmployeeOrDefaultById(watcher.Id);
                var modifiedByFullName = $"{_principalProvider.Current.FirstName} {_principalProvider.Current.LastName}";
                var changedValues = snapshotComparer.GetChangedValues(employee.UserId, _userService);

                if (changedValues.IsNullOrEmpty())
                {
                    continue;
                }

                SendEditChangesNotificationEmail(inputEntity, employee, changedValues, modifiedByFullName, _timeService.GetCurrentTime());
            }

            _alertService.AddInformation(RecruitmentProcessResources.RecruitmentProcessUpdateInformation);
        }

        public Dictionary<string, ValueChange> GetShanpshotChanges(RecruitmentProcessDto sourceDto, RecruitmentProcessDto inputDto)
        {
            var snapshotComparer = new SnapshotComparer<RecruitmentProcessDto>();

            SetComparableFields(snapshotComparer);
            SetRecordChangeConverters(snapshotComparer);
            SetSecuredFields(snapshotComparer);

            snapshotComparer.Compare(sourceDto, inputDto);

            return snapshotComparer.GetChangedValues();
        }

        private void SetComparableFields(SnapshotComparer<RecruitmentProcessDto> snapshotComparer)
        {
            snapshotComparer.AddComparedFields(GetComparableFields());

            IEnumerable<Expression<Func<RecruitmentProcessDto, object>>> GetComparableFields()
            {
                yield return p => p.OfferPositionId;
                yield return p => p.OfferContractType;
                yield return p => p.OfferSalary;
                yield return p => p.OfferStartDate;
                yield return p => p.OfferLocationId;
                yield return p => p.OfferComment;
                yield return p => p.CandidateId;
                yield return p => p.JobOpeningId;
                yield return p => p.RecruiterId;
                yield return p => p.CityId;
                yield return p => p.JobApplicationId;
                yield return p => p.Status;
                yield return p => p.ResumeShownToClientOn;
                yield return p => p.ClosedOn;
                yield return p => p.ClosedReason;
                yield return p => p.ClosedComment;
                yield return p => p.FinalPositionId;
                yield return p => p.FinalTrialContractTypeId;
                yield return p => p.FinalLongTermContractTypeId;
                yield return p => p.FinalTrialSalary;
                yield return p => p.FinalLongTermSalary;
                yield return p => p.FinalStartDate;
                yield return p => p.FinalSignedDate;
                yield return p => p.FinalLocationId;
                yield return p => p.FinalComment;
                yield return p => p.FinalEmploymentSourceId;
                yield return p => p.ScreeningGeneralOpinion;
                yield return p => p.ScreeningMotivation;
                yield return p => p.ScreeningWorkplaceExpectations;
                yield return p => p.ScreeningSkills;
                yield return p => p.ScreeningLanguageEnglish;
                yield return p => p.ScreeningLanguageGerman;
                yield return p => p.ScreeningLanguageOther;
                yield return p => p.ScreeningContractExpectations;
                yield return p => p.ScreeningAvailability;
                yield return p => p.ScreeningOtherProcesses;
                yield return p => p.ScreeningCounterOfferCriteria;
                yield return p => p.ScreeningRelocationCanDo;
                yield return p => p.ScreeningRelocationComment;
                yield return p => p.ScreeningBusinessTripsCanDo;
                yield return p => p.ScreeningBusinessTripsComment;
                yield return p => p.ScreeningWorkPermitNeeded;
                yield return p => p.ScreeningWorkPermitComment;
                yield return p => p.ScreeningEveningWorkCanDo;
                yield return p => p.ScreeningEveningWorkComment;
            }
        }

        private void SetSecuredFields(SnapshotComparer<RecruitmentProcessDto> snapshotComparer)
        {
            snapshotComparer.AddSecuredField(p => p.OfferSalary, SecurityRoleType.CanViewRecruitmentProcessOfferedSalary);
            snapshotComparer.AddSecuredField(p => p.FinalLongTermSalary, SecurityRoleType.CanViewRecruitmentProcessFinalSalary);
            snapshotComparer.AddSecuredField(p => p.FinalTrialSalary, SecurityRoleType.CanViewRecruitmentProcessFinalSalary);
        }

        private void SetRecordChangeConverters(SnapshotComparer<RecruitmentProcessDto> SnapshotComparer)
        {
            SnapshotComparer.AddDisplayNameConverter<long>(
                p => p.CandidateId,
                id => _candidateService.GetCandidateOrDefaultById(id)?.FullName);

            SnapshotComparer.AddCustomObjectConverters<long>(
                GetCityProperties(),
                id => string.Join(", ", _cityService.GetCityOrDefaultById(id)?.Name));

            SnapshotComparer.AddCustomObjectConverters<long>(
                GetLocationProperties(),
                id => string.Join(", ", _locationService.GetLocationOrDefaultById(id)?.Name));

            SnapshotComparer.AddDisplayNameConverter<long>(
                p => p.RecruiterId,
                id => _employeeService.GetEmployeeOrDefaultById(id)?.FullName);

            SnapshotComparer.AddDisplayNameConverter<long[]>(
                p => p.WatcherIds,
                ids => string.Join(", ", _employeeService.GetEmployeesById(ids)?.Select(e => e.FullName)));

            SnapshotComparer.AddDisplayNameConverter<long>(
                p => p.JobOpeningId,
                id => _jobOpeningService.GetJobOpeningOrDefaultById(id)?.PositionName);

            SnapshotComparer.AddCustomObjectConverters<long>(
                GetJobTitleProperties(),
                id => string.Join(", ", _jobTitleService.GetJobTitleOrDefaultById(id)?.Name));

            SnapshotComparer.AddCustomObjectConverters<long>(
                GetContractTypeProperties(),
                id => string.Join(", ", _contractTypeService.GetContractTypeOrDefaultById(id)?.Name));

            IEnumerable<Expression<Func<RecruitmentProcessDto, object>>> GetCityProperties()
            {
                yield return p => p.CityId;
                yield return p => p.OfferLocationId;
            }

            IEnumerable<Expression<Func<RecruitmentProcessDto, object>>> GetJobTitleProperties()
            {
                yield return p => p.FinalPositionId;
                yield return p => p.OfferPositionId;
            }

            IEnumerable<Expression<Func<RecruitmentProcessDto, object>>> GetContractTypeProperties()
            {
                yield return p => p.FinalTrialContractTypeId;
                yield return p => p.FinalLongTermContractTypeId;
            }

            IEnumerable<Expression<Func<RecruitmentProcessDto, object>>> GetLocationProperties()
            {
                yield return p => p.FinalLocationId;
            }
        }

        private void SendEditChangesNotificationEmail(RecruitmentProcess recruitmentProcess, EmployeeDto employee, Dictionary<string, ValueChange> changes, string modifiedBy, DateTime modifiedOn)
        {
            const string recruitmentProcessUrl = "{0}Recruitment/RecruitmentProcessSteps/List?parent-id={1}";

            var recipient = new EmailRecipientDto
            {
                EmailAddress = employee.Email,
                FullName = employee.FullName
            };

            var serverAddress = _systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);

            var recruitmentProcessChangesNotificationDto = new RecruitmentProcessChangesNotificationDto
            {
                RecipientFirstName = employee.FirstName,
                RecruitmentProcessUrl = string.Format(recruitmentProcessUrl, serverAddress, recruitmentProcess.Id),
                RecruitmentProcessName = RecruitmentProcessBusinessLogic.DisplayName.Call(recruitmentProcess),
                Changes = changes,
                ChangedOnDate = modifiedOn.ToString(CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern),
                ChangedOnTime = modifiedOn.ToString(CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern),
                ChangedByFullName = modifiedBy
            };

            var emailSubject = _messageTemplateService.ResolveTemplate(TemplateCodes.RecruitmentProcessEditChangesSubject, recruitmentProcessChangesNotificationDto);
            var emailBody = _messageTemplateService.ResolveTemplate(TemplateCodes.RecruitmentProcessEditChangesBody, recruitmentProcessChangesNotificationDto);

            var emailMessageDto = new EmailMessageDto
            {
                IsHtml = emailBody.IsHtml,
                Subject = emailSubject.Content,
                Body = emailBody.Content,
                Recipients = new List<EmailRecipientDto> { recipient }
            };

            _reliableEmailService.EnqueueMessage(emailMessageDto);
        }

        public long GetCandidateId(long recruitmentProcessId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.RecruitmentProcesses.SelectById(recruitmentProcessId, p => p.CandidateId);
            }
        }

        public RecruitmentProcessStatus GetStatus(long recruitmentProcessId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.RecruitmentProcesses.SelectById(recruitmentProcessId, p => p.Status);
            }
        }

        public BoolResult ReopenProcess(RecruitmentProcessReopenDto dto)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var principal = _principalProvider.Current;
                var process = unitOfWork.Repositories.RecruitmentProcesses.GetById(dto.Id);

                if (process.OnboardingRequestId != null)
                {
                    var rejectRequestResult = RejectExistingOnboardingRequest(process.OnboardingRequest);

                    if (!rejectRequestResult.IsSuccessful)
                    {
                        return rejectRequestResult;
                    }
                }

                process.OnboardingRequestId = null;
                process.ClosedReason = null;
                process.Status = RecruitmentProcessStatus.Active;

                process.RecruitmentProcessNotes.Add(new RecruitmentProcessNote
                {
                    NoteType = NoteType.SystemNote,
                    RecruitmentProcessId = dto.Id,
                    NoteVisibility = NoteVisibility.Public,
                    Content = string.Format(RecruitmentProcessResources.ReopenedMessage, principal.FirstName, principal.LastName, dto.Comment)
                });

                unitOfWork.Commit();
            }

            _recruitmentProcessDataActivityService.LogActivity(dto.Id, RecruitmentProcessHistoryEntryType.Reopened);

            return BoolResult.Success;
        }

        private BoolResult RejectExistingOnboardingRequest(OnboardingRequest onboardingRequest)
        {
            if (onboardingRequest.Request.Status != RequestStatus.Draft && onboardingRequest.Request.Status != RequestStatus.Rejected)
            {
                return new BoolResult(false, new[]
                {
                        new AlertDto()
                        {
                            Message = RecruitmentProcessResources.ReopenErrorBadRequestStatusMessage,
                            Type = AlertType.Error
                        }
                });
            }

            if (onboardingRequest.Request.Status == RequestStatus.Draft)
            {
                onboardingRequest.Request.Status = RequestStatus.Rejected;
            }

            return BoolResult.Success;
        }

        public bool CanProcessBeReopened(long recruitmentProcessId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var process = unitOfWork.Repositories.RecruitmentProcesses.GetById(recruitmentProcessId);

                return RecruitmentProcessBusinessLogic.CanBeReopened.Call(process);
            }
        }
    }
}