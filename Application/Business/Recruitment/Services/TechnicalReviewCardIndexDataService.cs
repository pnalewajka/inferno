﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Helpers;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class TechnicalReviewCardIndexDataService : CardIndexDataService<TechnicalReviewDto, TechnicalReview, IRecruitmentDbScope>, ITechnicalReviewCardIndexDataService
    {
        private readonly ICardIndexServiceDependencies<IRecruitmentDbScope> _dependencies;
        private readonly IDataActivityLogService _dataActivityLogService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public TechnicalReviewCardIndexDataService(
            ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies,
            IDataActivityLogService dataActivityLogService)
            : base(dependencies)
        {
            _dependencies = dependencies;
            _dataActivityLogService = dataActivityLogService;
        }

        public long GetCandidateId(long technicalReviewId)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                return unitOfWork.Repositories.TechnicalReviews
                    .SelectById(technicalReviewId, r => r.RecruitmentProcessStep.RecruitmentProcess.CandidateId);
            }
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<TechnicalReview, TechnicalReviewDto, IRecruitmentDbScope> eventArgs)
        {
            eventArgs.InputEntity.JobProfiles = EntityMergingService.CreateEntitiesFromIds<JobProfile, IRecruitmentDbScope>(eventArgs.UnitOfWork, eventArgs.InputEntity.JobProfiles.GetIds());
            RecruitmentProcessHelper.SetRecruitmentProcessLastActivity(eventArgs.UnitOfWork, eventArgs.InputEntity.RecruitmentProcessId, TimeService.GetCurrentTime(), CurrentEmployeeId);
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<TechnicalReview, TechnicalReviewDto, IRecruitmentDbScope> eventArgs)
        {
            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.JobProfiles, eventArgs.InputEntity.JobProfiles);
            RecruitmentProcessHelper.SetRecruitmentProcessLastActivity(eventArgs.UnitOfWork, eventArgs.InputEntity.RecruitmentProcessId, TimeService.GetCurrentTime(), CurrentEmployeeId);
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<TechnicalReview, TechnicalReviewDto> recordEditedEventArgs)
        {
            _dataActivityLogService.LogCandidateDataActivity(GetCandidateIdByRecruitmentProcessId(recordEditedEventArgs.InputEntity.RecruitmentProcessId), ActivityType.UpdatedRecord,
                ReferenceType.TechnicalReview, recordEditedEventArgs.InputEntity.Id);

            base.OnRecordEdited(recordEditedEventArgs);
        }

        protected override void OnRecordsDeleted(RecordDeletedEventArgs<TechnicalReview> eventArgs)
        {
            foreach (var entitie in eventArgs.DeletedEntities)
            {
                _dataActivityLogService.LogCandidateDataActivity(GetCandidateIdByRecruitmentProcessId(entitie.RecruitmentProcessId), ActivityType.RemovedRecord,
                    ReferenceType.TechnicalReview, entitie.Id);
            }

            base.OnRecordsDeleted(eventArgs);
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<TechnicalReview, TechnicalReviewDto> recordAddedEventArgs)
        {
            _dataActivityLogService.LogCandidateDataActivity(GetCandidateIdByRecruitmentProcessId(recordAddedEventArgs.InputEntity.RecruitmentProcessId), ActivityType.CreatedRecord,
                ReferenceType.TechnicalReview, recordAddedEventArgs.InputEntity.Id);

            base.OnRecordAdded(recordAddedEventArgs);
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<TechnicalReview> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var direction = sortingCriterion.GetSortingDirection();

            if (sortingCriterion.GetSortingColumnName() == nameof(TechnicalReviewDto.CandidateFullName))
            {
                orderedQueryBuilder.ApplySortingKey(TechnicalReviewBusinessLogic.CandidateFullName.AsExpression(), direction);
            }
            else if (sortingCriterion.GetSortingColumnName() == nameof(TechnicalReviewDto.PositionName))
            {
                orderedQueryBuilder.ApplySortingKey(TechnicalReviewBusinessLogic.PositionName.AsExpression(), direction);
            }
            else if (sortingCriterion.GetSortingColumnName() == nameof(TechnicalReviewDto.AssigneeFullName))
            {
                orderedQueryBuilder.ApplySortingKey(TechnicalReviewBusinessLogic.AssigneeFullName.AsExpression(), direction);
            }
            else
            {
                base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
            }
        }

        protected override IEnumerable<Expression<Func<TechnicalReview, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return r => r.RecruitmentProcess.Candidate.FirstName;
            yield return r => r.RecruitmentProcess.Candidate.LastName;
            yield return r => r.RecruitmentProcess.JobOpening.PositionName;
            yield return r => r.RecruitmentProcessStep.Decision;
            yield return r => r.RecruitmentProcessStep.AssignedEmployee.FirstName;
            yield return r => r.RecruitmentProcessStep.AssignedEmployee.LastName;
            yield return r => r.RecruitmentProcessStep.AssignedEmployee.Acronym;
            yield return r => r.JobProfiles.Select(p => p.Name);
            yield return r => r.OtherRemarks;
        }

        protected override NamedFilters<TechnicalReview> NamedFilters
        {
            get
            {
                return new NamedFilters<TechnicalReview>(CardIndexServiceHelper.BuildSoftDeletableFilters<TechnicalReview>());
            }
        }

        private long GetCandidateIdByRecruitmentProcessId(long recruitmentProcessId)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                return unitOfWork.Repositories.RecruitmentProcesses.Where(p => p.Id == recruitmentProcessId).Select(p => p.CandidateId).Single();
            }
        }

        private long CurrentEmployeeId => _dependencies.PrincipalProvider.Current.EmployeeId;
    }
}
