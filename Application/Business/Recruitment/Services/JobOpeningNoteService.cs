﻿using System;
using System.Linq;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class JobOpeningNoteService : IJobOpeningNoteService
    {
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;
        private readonly IClassMappingFactory _classMappingFactory;

        public JobOpeningNoteService(
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService,
            IClassMappingFactory classMappingFactory)
        {
            _unitOfWorkService = unitOfWorkService;
            _classMappingFactory = classMappingFactory;
        }

        public void AddNote(JobOpeningNoteDto jobOpeningNoteDto)
        {
            if (string.IsNullOrEmpty(jobOpeningNoteDto.Content))
            {
                throw new ArgumentNullException(nameof(jobOpeningNoteDto.Content));
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                if (!unitOfWork.Repositories.JobOpenings.Any(j => j.Id == jobOpeningNoteDto.JobOpeningId))
                {
                    throw new ArgumentException(nameof(jobOpeningNoteDto.JobOpeningId));
                }

                var mapping = _classMappingFactory.CreateMapping<JobOpeningNoteDto, JobOpeningNote>();
                var jobOpeningNote = mapping.CreateFromSource(jobOpeningNoteDto);

                unitOfWork.Repositories.JobOpeningNotes.Add(jobOpeningNote);
                unitOfWork.Commit();
            }
        }
    }
}
