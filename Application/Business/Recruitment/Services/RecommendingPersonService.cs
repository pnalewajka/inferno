﻿using System;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class RecommendingPersonService : IRecommendingPersonService
    {
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;
        private readonly IClassMappingFactory _classMappingFactory;
        private readonly ITimeService _timeService;

        public RecommendingPersonService(
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService,
            IClassMappingFactory classMappingFactory,
            ITimeService timeService)
        {
            _unitOfWorkService = unitOfWorkService;
            _timeService = timeService;
            _classMappingFactory = classMappingFactory;
        }

        public void AnonymizeRecommendingPersonById(long id, bool isAnonymizedPermanently = false)
        {
            const string firstNameAnonymized = "Anonymized";
            const string lastNameAnonymized = "Anonymized";
            const string emailAnonymized = "Anonymized-{0}@Anonymized";

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recommendingPerson = unitOfWork.Repositories.RecommendingPersons.GetByIdOrDefault(id);

                if (recommendingPerson == null)
                {
                    throw new ArgumentException(nameof(id));
                }

                if (isAnonymizedPermanently)
                {
                    recommendingPerson.FirstName = firstNameAnonymized;
                    recommendingPerson.LastName = lastNameAnonymized;
                    recommendingPerson.EmailAddress = string.Format(emailAnonymized, recommendingPerson.Id);
                    recommendingPerson.PhoneNumber = string.Empty;
                    recommendingPerson.IsAnonymized = true;
                }
                else
                {
                    recommendingPerson.DeletedOn = _timeService.GetCurrentTime();
                    unitOfWork.Repositories.RecommendingPersons.Delete(recommendingPerson);
                }

                unitOfWork.Commit();
            }
        }

        public bool AddNote(AddNoteDto note)
        {
            var classMapping = _classMappingFactory.CreateMapping<AddNoteDto, RecommendingPersonNote>();
            var recommendingPersonNote = classMapping.CreateFromSource(note);

            return AddNote(recommendingPersonNote);
        }

        public bool AddNote(RecommendingPersonNote note)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                note.NoteType = NoteType.UserNote;

                unitOfWork.Repositories.RecommendingPersonNotes.Add(note);
                unitOfWork.Commit();
            }

            return true;
        }
    }
}
