﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class JobApplicationService : IJobApplicationService
    {
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;
        private readonly IClassMappingFactory _classMappingFactory;

        public JobApplicationService(
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService,
            IClassMappingFactory classMappingFactory)
        {
            _unitOfWorkService = unitOfWorkService;
            _classMappingFactory = classMappingFactory;
        }

        public JobApplicationDto GetJobApplicationById(long jobApplicationId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var jobApplication = unitOfWork.Repositories.JobApplications.GetById(jobApplicationId);

                return _classMappingFactory.CreateMapping<JobApplication, JobApplicationDto>().CreateFromSource(jobApplication);
            }
        }
    }
}
