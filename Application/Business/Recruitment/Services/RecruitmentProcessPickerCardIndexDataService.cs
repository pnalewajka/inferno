﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class RecruitmentProcessPickerCardIndexDataService : CardIndexDataService<RecruitmentProcessDto, RecruitmentProcess, IRecruitmentDbScope>,
        IRecruitmentProcessPickerCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public RecruitmentProcessPickerCardIndexDataService(ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies)
            : base(dependencies)
        {
        }

        public override IQueryable<RecruitmentProcess> ApplyContextFiltering(IQueryable<RecruitmentProcess> records, object context)
        {
            if (context is RecruitmentProcessPickerContext recruitmentProcessPickerContext)
            {
                if (recruitmentProcessPickerContext.OnlyActive)
                {
                    return records.Where(p => p.Status == RecruitmentProcessStatus.Active);
                }
            }

            return records;
        }

        protected override NamedFilters<RecruitmentProcess> NamedFilters
        {
            get
            {
                return new NamedFilters<RecruitmentProcess>(CardIndexServiceHelper.BuildSoftDeletableFilters<RecruitmentProcess>());
            }
        }
    }
}
