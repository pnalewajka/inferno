﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class InitialRecruitmentProcessStepTypeItemProvider : EnumBasedListItemProvider<RecruitmentProcessStepType>
    {
        private readonly RecruitmentProcessStepType[] InitialSteps =
            { RecruitmentProcessStepType.HrCall, RecruitmentProcessStepType.HrInterview,
            RecruitmentProcessStepType.TechnicalReview, RecruitmentProcessStepType.HiringManagerInterview };

        public InitialRecruitmentProcessStepTypeItemProvider(IPrincipalProvider principalProvider) : base(principalProvider)
        {
        }

        public override IEnumerable<ListItem> GetItems()
        {
            return base.GetItems().Where(i => InitialSteps.Contains((RecruitmentProcessStepType)i.Id));
        }
    }
}
