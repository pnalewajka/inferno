﻿using System.Linq;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class InboundEmailDocumentPickerCardIndexDataService : InboundEmailDocumentCardIndexDataService,
        IInboundEmailDocumentPickerCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public InboundEmailDocumentPickerCardIndexDataService(ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies)
            : base(dependencies)
        {
        }

        public override IQueryable<InboundEmailDocument> ApplyContextFiltering(IQueryable<InboundEmailDocument> records, object context)
        {
            var filteredRecords = base.ApplyContextFiltering(records, context);

            if (context is InboundEmailPickerContext inboundEmailPickerContext)
            {
                return ApplyInboundEmailPickerContextFiltering(records, inboundEmailPickerContext);
            }

            return records;
        }

        private IQueryable<InboundEmailDocument> ApplyInboundEmailPickerContextFiltering(IQueryable<InboundEmailDocument> records, InboundEmailPickerContext inboundEmailPickerContext)
        {
            records = inboundEmailPickerContext.AssignedOnly
                ? records.Where(r => r.InboundEmail.ProcessedById == PrincipalProvider.Current.EmployeeId)
                : records;

            records = inboundEmailPickerContext.InboundEmailId != null
                ? records.Where(r => r.InboundEmailId == inboundEmailPickerContext.InboundEmailId)
                : records;

            return records;
        }
    }
}
