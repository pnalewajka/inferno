﻿using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class CandidateDataConsentFileCardIndexDataService : ReadOnlyCardIndexDataService<CandidateFileDto, DataConsentDocument, IRecruitmentDbScope>, ICandidateDataConsentFileCardIndexDataService
    {
        public CandidateDataConsentFileCardIndexDataService(ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies) : base(dependencies)
        {
        }

        public override IQueryable<DataConsentDocument> ApplyContextFiltering(IQueryable<DataConsentDocument> records, object context)
        {
            if (context is ParentIdContext parentIdContext)
            {
                return records.Where(c => c.DataConsent.DataOwner.CandidateId == parentIdContext.ParentId);
            }

            return base.ApplyContextFiltering(records, context);
        }

        protected override IQueryable<DataConsentDocument> ConfigureIncludes(IQueryable<DataConsentDocument> sourceQueryable)
        {
            sourceQueryable = sourceQueryable.Include(c => c.DataConsent).Include(c => c.DataConsent.DataOwner);

            return base.ConfigureIncludes(sourceQueryable);
        }
    }
}
