﻿using System.Linq;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class CandidateFileService : ICandidateFileService
    {
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;

        public CandidateFileService(
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        public string GetDocumentNameByDocumentId(long candidateDocumentId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.CandidateDocuments
                    .Filtered()
                    .SelectById(candidateDocumentId, f => f.Name);
            }
        }

        public long GetCandidateFileDocumentIdByFileId(long candidateFileId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.CandidateDocuments
                    .Filtered()
                    .Where(c => c.CandidateFile.Id == candidateFileId)
                    .Select(c => c.Id)
                    .Single();
            }
        }

        public long GetCandidateIdByDocumentId(long candidateDocumentId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.CandidateDocuments
                    .Filtered()
                    .SelectById(candidateDocumentId, a => a.CandidateFile.CandidateId);
            }
        }
    }
}
