﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class BasicNoteVisibilityItemProvider : EnumBasedListItemProvider<NoteVisibility>
    {
        private IList<long> _basicTypeIds = new List<long>
        {
            (long)NoteVisibility.Public,
            (long)NoteVisibility.RecruitmentTeamOnly
        };

        public BasicNoteVisibilityItemProvider(IPrincipalProvider principalProvider)
            : base(principalProvider)
        {
        }

        public override IEnumerable<ListItem> GetItems()
        {
            return base.GetItems().Where(i => _basicTypeIds.Contains(i.Id.Value));
        }
    }
}
