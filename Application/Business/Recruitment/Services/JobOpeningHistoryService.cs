﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Smt.Atomic.Business.Recruitment.Helpers;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Business.Recruitment.SnapshotComparer;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class JobOpeningHistoryService : IJobOpeningHistoryService
    {
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly ITimeService _timeService;

        public JobOpeningHistoryService(
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService,
            IPrincipalProvider principalProvider,
            ITimeService timeService)
        {
            _unitOfWorkService = unitOfWorkService;
            _principalProvider = principalProvider;
            _timeService = timeService;
        }

        public void LogActivity(long jobOpeningId, JobOpeningHistoryEntryType activityType, Dictionary<string, ValueChange> snapshotChanges = null)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var activity = new JobOpeningHistoryEntry
                {
                    JobOpeningId = jobOpeningId,
                    PerformedByUserId = _principalProvider.Current.Id.Value,
                    ActivityType = activityType,
                    PerformedOn = _timeService.GetCurrentTime(),
                    ChangesDescription = SnapshotComaparerHelper.SerializeSnapshotChanges(snapshotChanges)
                };

                unitOfWork.Repositories.JobOpeningHistoryEntries.Add(activity);
                unitOfWork.Commit();
            }
        }        
    }
}
