﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Consts;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.Business.Recruitment.Helpers;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Business.Recruitment.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Business.Helpers;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class RecruitmentProcessStepCardIndexDataService : CardIndexDataService<RecruitmentProcessStepDto, RecruitmentProcessStep, IRecruitmentDbScope>, IRecruitmentProcessStepCardIndexDataService
    {
        private readonly IRecruitmentProcessStepNotificationService _recruitmentProcessStepNotificationService;
        private readonly IRecruitmentProcessService _recruitmentProcessService;
        private readonly IRecruitmentProcessStepService _recruitmentProcessStepService;
        private readonly IDataActivityLogService _dataActivityLogService;
        private readonly ICalendarService _calendarService;
        private readonly ICardIndexServiceDependencies<IRecruitmentDbScope> _dependencies;
        private readonly ICandidateService _candidateService;
        private readonly IRecruitmentProcessHistoryService _recruitmentProcessHistoryService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public RecruitmentProcessStepCardIndexDataService(IRecruitmentProcessStepNotificationService recruitmentProcessNotificationService,
            IRecruitmentProcessService recruitmentProcessService,
            IRecruitmentProcessStepService recruitmentProcessStepService,
            IDataActivityLogService dataActivityLogService,
            ICalendarService calendarService,
            ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies,
            ICandidateService candidateService,
            IRecruitmentProcessHistoryService recruitmentProcessHistoryService)
            : base(dependencies)
        {
            _recruitmentProcessStepNotificationService = recruitmentProcessNotificationService;
            _recruitmentProcessService = recruitmentProcessService;
            _recruitmentProcessStepService = recruitmentProcessStepService;
            _dataActivityLogService = dataActivityLogService;
            _calendarService = calendarService;
            _dependencies = dependencies;
            _candidateService = candidateService;
            _recruitmentProcessHistoryService = recruitmentProcessHistoryService;
        }

        public override IQueryable<RecruitmentProcessStep> ApplyContextFiltering(IQueryable<RecruitmentProcessStep> records, object context)
        {
            if (context is ParentIdContext parentIdContext && parentIdContext.ParentId != default(long))
            {
                return records.Where(c => c.RecruitmentProcessId == parentIdContext.ParentId);
            }

            if (context is RecruitmentProcessStepPickerContext recruitmentProcessStepPickerContext)
            {
                return records.Where(p => p.RecruitmentProcessId == recruitmentProcessStepPickerContext.RecruitmentProcessId);
            }

            return base.ApplyContextFiltering(records, context);
        }

        protected override IOrderedQueryable<RecruitmentProcessStep> GetDefaultOrderBy(IQueryable<RecruitmentProcessStep> records, QueryCriteria criteria)
        {
            var context = criteria.Context;

            if ((context is ParentIdContext parentIdContext && parentIdContext.ParentId != default(long))
                || context is RecruitmentProcessStepPickerContext)
            {
                return records.OrderByDescending(s => s.Type).ThenBy(s => s.Status).ThenByDescending(s => s.DueOn);
            }

            return records.OrderByDescending(s => s.ModifiedOn);
        }

        protected override NamedFilters<RecruitmentProcessStep> NamedFilters
        {
            get
            {
                var currentEmployeeId = PrincipalProvider.Current.EmployeeId;
                var currentUserId = PrincipalProvider.Current.Id ?? BusinessLogicHelper.NonExistingId;

                return new NamedFilters<RecruitmentProcessStep>((new[]
                {
                    new NamedFilter<RecruitmentProcessStep>(FilterCodes.MyRecords, RecruitmentProcessStepBusinessLogic.IsOwnRecruitmentProcessStep.Parametrize(currentEmployeeId), SecurityRoleType.CanFilterRecruitmentProcessStepsByMyRecords),
                    new NamedFilter<RecruitmentProcessStep>(FilterCodes.MyProcessRecords, RecruitmentProcessStepBusinessLogic.IsOwnRecruitmentProcess.Parametrize(currentUserId, currentEmployeeId), SecurityRoleType.CanFilterRecruitmentProcessStepsByMyProcess),
                    new NamedFilter<RecruitmentProcessStep>(FilterCodes.AnyOwner, r => true, SecurityRoleType.CanFilterRecruitmentProcessStepsByAnyOwner),
                    new NamedFilter<RecruitmentProcessStep>(FilterCodes.RecruitmentProcessStepStatusDefault, a => a.Status != RecruitmentProcessStepStatus.Cancelled),
                    new NamedFilter<RecruitmentProcessStep>(FilterCodes.RecruitmentProcessStepStatusAll, a => true)
                })
                .Union(CardIndexServiceHelper.BuildEnumBasedFilters<RecruitmentProcessStep, RecruitmentProcessStepType>(u => u.Type))
                .Union(CardIndexServiceHelper.BuildEnumBasedFilters<RecruitmentProcessStep, RecruitmentProcessStepStatus>(u => u.Status))
                .Union(CardIndexServiceHelper.BuildSoftDeletableFilters<RecruitmentProcessStep>()));
            }
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<RecruitmentProcessStep, RecruitmentProcessStepDto> recordAddedEventArgs)
        {
            _dataActivityLogService.LogCandidateDataActivity(_recruitmentProcessService.GetCandidateId(recordAddedEventArgs.InputEntity.RecruitmentProcessId),
                ActivityType.CreatedRecord, ReferenceType.RecruitmentProcessStep, recordAddedEventArgs.InputEntity.Id, GetType().Name);

            _recruitmentProcessHistoryService.LogActivity(recordAddedEventArgs.InputEntity.RecruitmentProcessId,
                RecruitmentProcessHistoryEntryType.CreatedStep, recordAddedEventArgs.InputEntity.Id);

            if (recordAddedEventArgs.InputEntity.Type == RecruitmentProcessStepType.TechnicalReview)
            {
                using (var unitOfWork = UnitOfWorkService.Create())
                {
                    AddNewTechnicalReview(recordAddedEventArgs.InputDto, recordAddedEventArgs.InputEntity.Id, unitOfWork);
                }
            }

            if (recordAddedEventArgs.InputEntity.Status == RecruitmentProcessStepStatus.ToDo)
            {
                _recruitmentProcessStepNotificationService.NotifyAboutPlannedStep(recordAddedEventArgs.InputEntity.Type, recordAddedEventArgs.InputEntity.Id, recordAddedEventArgs.InputDto.FromStepId);
            }

            _candidateService.UpdateLastActivity(recordAddedEventArgs.InputEntity.RecruitmentProcess.CandidateId, ReferenceType.RecruitmentProcessStep, recordAddedEventArgs.InputEntity.Id);

            base.OnRecordAdded(recordAddedEventArgs);
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<RecruitmentProcessStep, RecruitmentProcessStepDto> recordEditedEventArgs)
        {
            var inputEntity = recordEditedEventArgs.InputEntity;
            var dbEntity = recordEditedEventArgs.DbEntity;
            var dto = recordEditedEventArgs.InputDto;
            var orginalDto = recordEditedEventArgs.OriginalDto;

            _dataActivityLogService.LogCandidateDataActivity(
                _recruitmentProcessService.GetCandidateId(inputEntity.RecruitmentProcessId),
                ActivityType.UpdatedRecord, ReferenceType.RecruitmentProcessStep, inputEntity.Id, GetType().Name);

            Compare(orginalDto, dto);

            if (inputEntity.Status == RecruitmentProcessStepStatus.ToDo
                && (dto.PlannedOn != orginalDto.PlannedOn
                || dto.AssignedEmployeeId != orginalDto.AssignedEmployeeId))
            {
                _recruitmentProcessStepNotificationService.NotifyAboutPlannedStep(inputEntity.Type, inputEntity.Id, dto.FromStepId);
            }

            _recruitmentProcessHistoryService.LogActivity(inputEntity.RecruitmentProcessId,
                RecruitmentProcessHistoryEntryType.UpdatedStep, inputEntity.Id);

            Compare(recordEditedEventArgs.OriginalDto, recordEditedEventArgs.InputDto);

            if (dto.WorkflowAction.HasValue)
            {
                HandleWorkflowActionOnEdited(dto.WorkflowAction.Value, inputEntity, dto, recordEditedEventArgs.Alerts);
            }


            if (recordEditedEventArgs.DbEntity.Type != RecruitmentProcessStepType.HiringDecision)
            {
                _candidateService.UpdateLastActivity(dbEntity.RecruitmentProcess.CandidateId, ReferenceType.RecruitmentProcessStep, inputEntity.Id);
            }

            base.OnRecordEdited(recordEditedEventArgs);
        }

        private void Compare(RecruitmentProcessStepDto originalDto, RecruitmentProcessStepDto inputDto)
        {
            if (originalDto.PlannedOn == inputDto.PlannedOn && originalDto.RecruitmentHint == inputDto.RecruitmentHint)
            {
                return;
            }

            _recruitmentProcessStepNotificationService.NotifyAboutStepUpdate(originalDto.Id);
        }

        protected override void OnRecordsDeleted(RecordDeletedEventArgs<RecruitmentProcessStep> eventArgs)
        {
            foreach (var entitie in eventArgs.DeletedEntities)
            {
                _dataActivityLogService.LogCandidateDataActivity(
                    _recruitmentProcessService.GetCandidateId(entitie.RecruitmentProcessId),
                    ActivityType.RemovedRecord, ReferenceType.RecruitmentProcessStep, entitie.Id);

                _recruitmentProcessHistoryService.LogActivity(entitie.RecruitmentProcessId,
                    RecruitmentProcessHistoryEntryType.RemovedStep, entitie.Id);
            }

            base.OnRecordsDeleted(eventArgs);
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<RecruitmentProcessStep, RecruitmentProcessStepDto, IRecruitmentDbScope> eventArgs)
        {
            eventArgs.InputEntity.OtherAttendingEmployees = EntityMergingService.CreateEntitiesFromIds<Employee, IRecruitmentDbScope>(eventArgs.UnitOfWork, eventArgs.InputEntity.OtherAttendingEmployees.GetIds());

            RecruitmentProcessHelper.SetRecruitmentProcessLastActivity(eventArgs.UnitOfWork, eventArgs.InputEntity.RecruitmentProcessId, TimeService.GetCurrentTime(), CurrentEmployeeId);

            UpdateStatusField(eventArgs.InputEntity);
            AdjustDueDateOnAdding(eventArgs.InputEntity);

            PropagateChangesToRelatedEntitiesOnAdding(eventArgs.InputDto, eventArgs.UnitOfWork);

            base.OnRecordAdding(eventArgs);
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<RecruitmentProcessStep, RecruitmentProcessStepDto, IRecruitmentDbScope> eventArgs)
        {
            if (eventArgs.DbEntity.Status == RecruitmentProcessStepStatus.Done
                || eventArgs.DbEntity.Status == RecruitmentProcessStepStatus.Cancelled)
            {
                throw new InvalidOperationException("Record can't be edited when status is Done or Cancelled");
            }

            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.OtherAttendingEmployees, eventArgs.InputEntity.OtherAttendingEmployees);

            RecruitmentProcessHelper.SetRecruitmentProcessLastActivity(eventArgs.UnitOfWork, eventArgs.InputEntity.RecruitmentProcessId, TimeService.GetCurrentTime(), CurrentEmployeeId);

            UpdateStatusField(eventArgs.InputEntity);
            AdjustDueDateOnEditing(eventArgs.InputEntity);

            if (eventArgs.InputDto.WorkflowAction.HasValue)
            {
                HandleWorkflowActionOnEditing(eventArgs.InputDto.WorkflowAction.Value, eventArgs.InputEntity, eventArgs.InputDto, eventArgs.Alerts);
            }

            var relatedEntitesEditResult = PropagateChangesToRelatedEntitiesOnEditing(eventArgs.InputDto, eventArgs.UnitOfWork);

            foreach (var alert in relatedEntitesEditResult.Alerts)
            {
                eventArgs.Alerts.Add(alert);
            }

            base.OnRecordEditing(eventArgs);
        }

        private void PropagateChangesToRelatedEntitiesOnAdding(RecruitmentProcessStepDto record, IUnitOfWork<IRecruitmentDbScope> unitOfWork)
        {
            if (RecruitmentProcessStepBusinessLogic.IsMakingProcessMatureForHiringManager.Call(record.Type))
            {
                MakeProcessMatureForHiringManagerToSee(record, unitOfWork);
            }

            switch (record.Type)
            {
                case RecruitmentProcessStepType.HrCall:
                case RecruitmentProcessStepType.HrInterview:
                    AddScreening(record, unitOfWork);
                    break;

                case RecruitmentProcessStepType.ContractNegotiations:
                    EditContractNegotiations(record, unitOfWork);
                    break;

                case RecruitmentProcessStepType.ClientResumeApproval:
                    EditRecruitmentProcess(record.RecruitmentProcessId, (ClientResumeApprovalDto)record.StepDetails, unitOfWork);
                    break;
            }
        }

        private EditRecordResult PropagateChangesToRelatedEntitiesOnEditing(RecruitmentProcessStepDto record, IUnitOfWork<IRecruitmentDbScope> unitOfWork)
        {
            var alerts = new List<AlertDto>();

            switch (record.Type)
            {
                case RecruitmentProcessStepType.HrCall:
                case RecruitmentProcessStepType.HrInterview:
                    EditScreening(record, unitOfWork);
                    break;

                case RecruitmentProcessStepType.TechnicalReview:
                    EditTechnicalReview(record, unitOfWork);
                    break;

                case RecruitmentProcessStepType.ClientResumeApproval:
                    EditRecruitmentProcess(record.RecruitmentProcessId, (ClientResumeApprovalDto)record.StepDetails, unitOfWork);
                    break;

                case RecruitmentProcessStepType.ContractNegotiations:
                    EditContractNegotiations(record, unitOfWork);
                    break;

                case RecruitmentProcessStepType.HiringManagerResumeApproval:
                case RecruitmentProcessStepType.HiringManagerInterview:
                case RecruitmentProcessStepType.ClientInterview:
                case RecruitmentProcessStepType.HiringDecision:
                default:
                    break;
            }

            if (alerts.Any())
            {
                return new EditRecordResult(false, record.Id, null, alerts);
            }

            return new EditRecordResult(true, record.Id, null, alerts);
        }

        public override RecruitmentProcessStepDto GetRecordById(long id)
        {
            var dto = base.GetRecordById(id);

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                switch (dto.Type)
                {
                    case RecruitmentProcessStepType.HrCall:
                    case RecruitmentProcessStepType.HrInterview:
                    case RecruitmentProcessStepType.HiringManagerResumeApproval:
                    case RecruitmentProcessStepType.HiringManagerInterview:
                    case RecruitmentProcessStepType.HiringDecision:
                        dto.StepDetails = GetScreeningDetails(dto.RecruitmentProcessId, unitOfWork);
                        break;

                    case RecruitmentProcessStepType.TechnicalReview:
                        dto.StepDetails = GetTechnicalReviewDetails(id, dto.RecruitmentProcessId, unitOfWork);
                        break;

                    case RecruitmentProcessStepType.ClientResumeApproval:
                        dto.StepDetails = GetClientResumeApprovalDetails(dto.RecruitmentProcessId, unitOfWork);
                        break;

                    case RecruitmentProcessStepType.ClientInterview:
                        break;

                    case RecruitmentProcessStepType.ContractNegotiations:
                        dto.StepDetails = GetContractNegotiationsDetailsDto(dto.RecruitmentProcessId, unitOfWork);
                        break;

                    default:
                        break;
                }

                var candidateToCandidateDtoMapping = _dependencies.ClassMappingFactory.CreateMapping<Candidate, CandidateDto>();
                var candidateFileToCandidateFileDetailsDtoMapping = _dependencies.ClassMappingFactory.CreateMapping<CandidateFile, CandidateFileDetailsDto>();
                var dataConsentDocumentToCandidateFileDetailsDtoMapping = _dependencies.ClassMappingFactory.CreateMapping<DataConsentDocument, CandidateFileDetailsDto>();
                var jobOpeningToJobOpeningDtoMapping = _dependencies.ClassMappingFactory.CreateMapping<JobOpening, JobOpeningDto>();
                var recruitmentProcessToRecruitmentProcessDtoMapping = _dependencies.ClassMappingFactory.CreateMapping<RecruitmentProcess, RecruitmentProcessDto>();
                var technicalReviewToTechnicalReviewDtoMapping = _dependencies.ClassMappingFactory.CreateMapping<TechnicalReview, TechnicalReviewDto>();
                var timelineMapping = _dependencies.ClassMappingFactory.CreateMapping<RecruitmentProcessStep, RecruitmentProcessStepTimelineDto>();

                dto.Candidate = candidateToCandidateDtoMapping.CreateFromSource(
                    unitOfWork.Repositories.RecruitmentProcesses.Where(r => r.Id == dto.RecruitmentProcessId)
                        .Select(r => r.Candidate)
                        .Include(r => r.Languages.Select(l => l.Language))
                        .Single());

                dto.Candidate.FileDetails = unitOfWork.Repositories.CandidateFiles.Filtered()
                    .Where(f => f.CandidateId == dto.Candidate.Id)
                    .Select(candidateFileToCandidateFileDetailsDtoMapping.CreateFromSource)
                    .Concat(
                        unitOfWork.Repositories.DataConsentDocument.Where(d => d.DataConsent.DataOwner.CandidateId == dto.Candidate.Id)
                        .Select(dataConsentDocumentToCandidateFileDetailsDtoMapping.CreateFromSource)
                    )
                    .OrderByDescending(f => f.ModifiedOn)
                    .ToList();

                dto.JobOpening = jobOpeningToJobOpeningDtoMapping.CreateFromSource(
                    unitOfWork.Repositories.RecruitmentProcesses.Where(r => r.Id == dto.RecruitmentProcessId)
                        .Select(r => r.JobOpening)
                        .Single());

                dto.RecruitmentProcess = recruitmentProcessToRecruitmentProcessDtoMapping.CreateFromSource(
                    unitOfWork.Repositories.RecruitmentProcesses.Where(r => r.Id == dto.RecruitmentProcessId)
                        .Include(p => p.JobOpening)
                        .Include(p => p.JobOpening.DecisionMakerEmployee)
                        .Include(p => p.RecruitmentProcessNotes)
                        .Single(), new CreationContext(RetrievalScenario.SingleRecord));

                dto.TechnicalReviews = unitOfWork.Repositories.Candidates
                    .GetById(dto.Candidate.Id)
                    .RecruitmentProcesses
                    .SelectMany(s => s.TechnicalReviews)
                    .Where(s => s.RecruitmentProcessStep.Status == RecruitmentProcessStepStatus.Done && !s.RecruitmentProcessStep.IsDeleted)
                    .OrderByDescending(tr => tr.ModifiedOn)
                    .Select(tr => technicalReviewToTechnicalReviewDtoMapping.CreateFromSource(tr))
                    .ToList();

                var accessibleStepIds = unitOfWork.Repositories.RecruitmentProcessSteps.Filtered()
                    .Where(r => r.RecruitmentProcessId == dto.RecruitmentProcessId)
                    .Select(r => r.Id)
                    .ToList();

                dto.StepTimeline =
                    unitOfWork.Repositories.RecruitmentProcessSteps
                    .Where(r => r.RecruitmentProcessId == dto.RecruitmentProcessId)
                    .OrderBy(r => r.CreatedOn)
                    .Select(timelineMapping.CreateFromSource)
                    .ToList();

                foreach (var stepTimeLine in dto.StepTimeline)
                {
                    stepTimeLine.CurrentId = dto.Id;
                    stepTimeLine.CanBeViewedByCurrentUser = accessibleStepIds.Contains(stepTimeLine.Id);
                }
            }

            return dto;
        }

        private object GetClientResumeApprovalDetails(long recruitmentProcessId, IUnitOfWork<IRecruitmentDbScope> unitOfWork)
        {
            var recruitmentProcess = unitOfWork.Repositories.RecruitmentProcesses.Single(r => r.Id == recruitmentProcessId);

            return new ClientResumeApprovalDto { ResumeShownToClientOn = recruitmentProcess.ResumeShownToClientOn };
        }

        private object GetContractNegotiationsDetailsDto(long recruitmentProcessId, IUnitOfWork<IRecruitmentDbScope> unitOfWork)
        {
            var recruitmentProcess = unitOfWork.Repositories.RecruitmentProcesses.Single(r => r.Id == recruitmentProcessId);

            return new RecruitmentProcessToContractNegotiationsDetailsDtoMapping().CreateFromSource(recruitmentProcess);
        }

        private object GetScreeningDetails(long recruitmentProcessId, IUnitOfWork<IRecruitmentDbScope> unitOfWork)
        {
            var recruitmentProcess = unitOfWork.Repositories.RecruitmentProcesses.Single(r => r.Id == recruitmentProcessId);

            return new RecruitmentProcessToScreeningStepDetailsDtoMapping().CreateFromSource(recruitmentProcess.Screening);
        }

        public void ReOpenProcessStep(long processStepId)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var processStep = unitOfWork.Repositories.RecruitmentProcessSteps.GetByIdOrDefault(processStepId);

                if (processStep == null)
                {
                    throw new BusinessException(String.Format(AlertResources.CannotFindRecruitmentProcessStep, processStepId));
                }

                processStep.Status = RecruitmentProcessStepStatus.ToDo;

                var principal = PrincipalProvider.Current;

                processStep.RecruitmentProcess.RecruitmentProcessNotes.Add(new RecruitmentProcessNote
                {
                    NoteType = NoteType.SystemNote,
                    RecruitmentProcessId = processStep.RecruitmentProcessId,
                    NoteVisibility = NoteVisibility.Public,
                    Content = String.Format(RecruitmentProcessStepResources.ReopenedMessage, principal.FirstName, principal.LastName)
                });

                unitOfWork.Repositories.RecruitmentProcessSteps.Edit(processStep);
                unitOfWork.Commit();

                _recruitmentProcessHistoryService.LogActivity(processStep.RecruitmentProcessId, RecruitmentProcessHistoryEntryType.ReopenedStep, processStepId);
            }
        }

        public RecruitmentProcessStepStatus GetStatus(long id)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var processStep = unitOfWork.Repositories.RecruitmentProcessSteps.GetByIdOrDefault(id);

                if (processStep == null)
                {
                    throw new BusinessException(String.Format(AlertResources.CannotFindRecruitmentProcessStep, id));
                }

                return processStep.Status;
            }
        }

        private void HandleWorkflowActionOnEditing(RecruitmentProcessStepWorkflowActionType workflowAction, RecruitmentProcessStep inputEntity,
            RecruitmentProcessStepDto inputDto, IList<AlertDto> alerts)
        {
            switch (workflowAction)
            {
                case RecruitmentProcessStepWorkflowActionType.ApproveCandidate:
                    inputEntity.Decision = RecruitmentProcessStepDecision.Positive;
                    inputEntity.Status = RecruitmentProcessStepStatus.Done;
                    inputEntity.DecidedOn = TimeService.GetCurrentTime();

                    if (inputDto.NextStepId == null)
                    {
                        _recruitmentProcessStepNotificationService.NotifyAboutApproval(inputEntity.Type, inputEntity.Id);
                    }

                    break;

                case RecruitmentProcessStepWorkflowActionType.RejectCandidate:
                    inputEntity.Decision = RecruitmentProcessStepDecision.Negative;
                    inputEntity.Status = RecruitmentProcessStepStatus.Done;
                    inputEntity.DecidedOn = TimeService.GetCurrentTime();

                    _recruitmentProcessStepNotificationService.NotifyAboutRejection(inputEntity.Type, inputEntity.Id);
                    break;

                case RecruitmentProcessStepWorkflowActionType.CancelProcessStep:
                    inputEntity.Decision = RecruitmentProcessStepDecision.None;
                    inputEntity.Status = RecruitmentProcessStepStatus.Cancelled;
                    inputEntity.DecidedOn = TimeService.GetCurrentTime();

                    _recruitmentProcessStepNotificationService.NotifyAllPartiesAboutStepCancelled(inputEntity.Id);
                    break;
            }
        }

        private void HandleWorkflowActionOnEdited(RecruitmentProcessStepWorkflowActionType workflowAction, RecruitmentProcessStep inputEntity,
            RecruitmentProcessStepDto inputDto, IList<AlertDto> alerts)
        {
            switch (workflowAction)
            {
                case RecruitmentProcessStepWorkflowActionType.PutProcessOnHold:
                    {
                        var result = _recruitmentProcessService.PutRecruitmentProcessOnHold(inputEntity.RecruitmentProcessId, inputEntity.Id);

                        if (!result.IsSuccessful)
                        {
                            alerts.Add(AlertDto.CreateError(RecruitmentProcessStepResources.ClosedRecruitmentProcessErrorMessage));
                        }
                    }
                    break;

                case RecruitmentProcessStepWorkflowActionType.RejectCandidate:
                    CloseRecruitmentProcessIfApplicable(inputEntity.RecruitmentProcessId, inputDto);
                    _recruitmentProcessHistoryService.LogActivity(inputEntity.RecruitmentProcessId, RecruitmentProcessHistoryEntryType.CandidateRejected);
                    break;

                case RecruitmentProcessStepWorkflowActionType.ApproveCandidate:
                    if (inputDto.NextStepId != null)
                    {
                        _recruitmentProcessStepService.RefreshStepModification(inputDto.NextStepId.Value);
                    }

                    if (inputEntity.Type == RecruitmentProcessStepType.ContractNegotiations)
                    {
                        var addOnboardingRequestResult = _recruitmentProcessService.AddOnboardingRequest(inputEntity.Id);

                        if (!addOnboardingRequestResult.IsSuccessful)
                        {
                            alerts.AddRange(addOnboardingRequestResult.Alerts);
                        }

                        _recruitmentProcessHistoryService.LogActivity(inputEntity.RecruitmentProcessId, RecruitmentProcessHistoryEntryType.CandidateApproved);
                    }
                    break;
            }
        }

        private void MakeProcessMatureForHiringManagerToSee(RecruitmentProcessStepDto recruitmentProcessStep, IUnitOfWork<IRecruitmentDbScope> unitOfWork)
        {
            var recruitmentProcess = unitOfWork.Repositories.RecruitmentProcesses.Single(r => r.Id == recruitmentProcessStep.RecruitmentProcessId);
            recruitmentProcess.IsMatureEnoughForHiringManager = true;
        }

        private void EditContractNegotiations(RecruitmentProcessStepDto recruitmentProcessStep, IUnitOfWork<IRecruitmentDbScope> unitOfWork)
        {
            var recruitmentProcess = unitOfWork.Repositories.RecruitmentProcesses.Single(r => r.Id == recruitmentProcessStep.RecruitmentProcessId);
            var contractNegotiationsDetails = (ContractNegotiationsDetailsDto)recruitmentProcessStep.StepDetails;

            if (contractNegotiationsDetails == null)
            {
                return;
            }

            RecruitmentProcessStepMappingHelper.ApplyFinalDtoToRecruitmentProcess(contractNegotiationsDetails, recruitmentProcess);

            if (recruitmentProcessStep.WorkflowAction != RecruitmentProcessStepWorkflowActionType.ApproveCandidate)
            {
                return;
            }

            recruitmentProcess.ClosedReason = RecruitmentProcessClosedReason.Hired;
            recruitmentProcess.ClosedOn = TimeService.GetCurrentTime();
            recruitmentProcess.Status = RecruitmentProcessStatus.Closed;
        }

        private void EditRecruitmentProcess(long recruitmentProcessId, ClientResumeApprovalDto stepDetails, IUnitOfWork<IRecruitmentDbScope> unitOfWork)
        {
            var recruitmentProcess = unitOfWork.Repositories.RecruitmentProcesses.Single(r => r.Id == recruitmentProcessId);
            recruitmentProcess.ResumeShownToClientOn = stepDetails.ResumeShownToClientOn;

            unitOfWork.Commit();
        }

        private void EditTechnicalReview(RecruitmentProcessStepDto recruitmentProcessStep, IUnitOfWork<IRecruitmentDbScope> unitOfWork)
        {
            var technicalReview = unitOfWork.Repositories.TechnicalReviews.Single(r => r.RecruitmentProcessStepId == recruitmentProcessStep.Id);
            var technicalReviewStepDetails = (TechnicalReviewStepDetailsDto)recruitmentProcessStep.StepDetails;

            technicalReview.RecruitmentProcessId = recruitmentProcessStep.RecruitmentProcessId;
            technicalReview.RecruitmentProcessStepId = recruitmentProcessStep.Id;

            RecruitmentProcessStepMappingHelper.ApplyTechnicalReviewStepDetailsDtoToTechnicalReview(technicalReviewStepDetails, technicalReview);

            EntityMergingService.MergeCollections(
                unitOfWork,
                technicalReview.JobProfiles,
                technicalReviewStepDetails.JobProfileIds.Select(t => new JobProfile
                {
                    Id = t
                }).ToList());

            unitOfWork.Commit();
        }

        private void EditScreening(RecruitmentProcessStepDto recruitmentProcessStep, IUnitOfWork<IRecruitmentDbScope> unitOfWork)
        {
            var recruitmentProcess = unitOfWork.Repositories.RecruitmentProcesses.Single(r => r.Id == recruitmentProcessStep.RecruitmentProcessId);
            var screeningStepDetailsDto = (ScreeningStepDetailsDto)recruitmentProcessStep.StepDetails;

            RecruitmentProcessStepMappingHelper.ApplyScreeningStepDetailsDtoToRecruitmentProcess(screeningStepDetailsDto, recruitmentProcess);

            unitOfWork.Commit();
        }

        private void AddScreening(RecruitmentProcessStepDto recruitmentProcessStep, IUnitOfWork<IRecruitmentDbScope> unitOfWork)
        {
            var repositories = unitOfWork.Repositories;
            var recruitmentProcess = repositories.RecruitmentProcesses.Single(r => r.Id == recruitmentProcessStep.RecruitmentProcessId);

            if (repositories.Screening.Any(s => s.Id == recruitmentProcess.Id))
            {
                return;
            }

            repositories.Screening.Add(new RecruitmentProcessScreening { Id = recruitmentProcess.Id });

            unitOfWork.Commit();
        }

        private void UpdateStatusField(RecruitmentProcessStep recruitmentProcessStep)
        {
            if (recruitmentProcessStep.Status == RecruitmentProcessStepStatus.Draft
                && recruitmentProcessStep.AssignedEmployeeId.HasValue)
            {
                recruitmentProcessStep.Status = RecruitmentProcessStepStatus.ToDo;
            }
        }


        private object GetTechnicalReviewDetails(long recruitmentProcessStepId, long recruitmentProcessId, IUnitOfWork<IRecruitmentDbScope> unitOfWork)
        {
            var technicalReview = unitOfWork.Repositories.TechnicalReviews.SingleOrDefault(r => r.RecruitmentProcessStepId == recruitmentProcessStepId);
            var recruitmentProcess = unitOfWork.Repositories.RecruitmentProcesses.GetById(recruitmentProcessId);

            if (technicalReview == null)
            {
                return new TechnicalReviewStepDetailsDto
                {
                    JobProfileIds = new List<long>(),
                    Vacancies = new VacanciesDto[]
                    {
                        new VacanciesDto { Seniority = SeniorityLevelEnum.Seniority1, VacancyNumber = recruitmentProcess.JobOpening.VacancyLevel1 },
                        new VacanciesDto { Seniority = SeniorityLevelEnum.Seniority2, VacancyNumber = recruitmentProcess.JobOpening.VacancyLevel2 },
                        new VacanciesDto { Seniority = SeniorityLevelEnum.Seniority3, VacancyNumber = recruitmentProcess.JobOpening.VacancyLevel3 },
                        new VacanciesDto { Seniority = SeniorityLevelEnum.Seniority4, VacancyNumber = recruitmentProcess.JobOpening.VacancyLevel4 },
                        new VacanciesDto { Seniority = SeniorityLevelEnum.Seniority5, VacancyNumber = recruitmentProcess.JobOpening.VacancyLevel5 },
                        new VacanciesDto { Seniority = null, VacancyNumber = recruitmentProcess.JobOpening.VacancyLevelNotApplicable }
                    },
                    SeniorityLevelAssessment = default(SeniorityLevelEnum)
                };
            }

            return new TechnicalReviewStepDetailsDto
            {
                JobProfileIds = technicalReview.JobProfiles.Select(p => p.Id).ToList(),
                Vacancies = new VacanciesDto[]
                {
                    new VacanciesDto { Seniority = SeniorityLevelEnum.Seniority1, VacancyNumber = recruitmentProcess.JobOpening.VacancyLevel1 },
                    new VacanciesDto { Seniority = SeniorityLevelEnum.Seniority2, VacancyNumber = recruitmentProcess.JobOpening.VacancyLevel2 },
                    new VacanciesDto { Seniority = SeniorityLevelEnum.Seniority3, VacancyNumber = recruitmentProcess.JobOpening.VacancyLevel3 },
                    new VacanciesDto { Seniority = SeniorityLevelEnum.Seniority4, VacancyNumber = recruitmentProcess.JobOpening.VacancyLevel4 },
                    new VacanciesDto { Seniority = SeniorityLevelEnum.Seniority5, VacancyNumber = recruitmentProcess.JobOpening.VacancyLevel5 },
                    new VacanciesDto { Seniority = null, VacancyNumber = recruitmentProcess.JobOpening.VacancyLevelNotApplicable }
                },
                EnglishUsageAssessment = technicalReview.EnglishUsageAssessment,
                TeamProjectSuitabilityAssessment = technicalReview.TeamProjectSuitabilityAssessment,
                OtherRemarks = technicalReview.OtherRemarks,
                RiskAssessment = technicalReview.RiskAssessment,
                SeniorityLevelAssessment = technicalReview.SeniorityLevelAssessment,
                TechnicalKnowledgeAssessment = technicalReview.TechnicalKnowledgeAssessment,
                StrengthAssessment = technicalReview.StrengthAssessment,
                TechnicalAssignmentAssessment = technicalReview.TechnicalAssignmentAssessment,
            };
        }

        public RecruitmentProcessStepDto GetDefaultNewRecord(RecruitmentProcessStepType processStepType, long? recruitmentProcessId)
        {
            return _recruitmentProcessStepService.GetDefaultNewRecord(processStepType, recruitmentProcessId, base.GetDefaultNewRecord());
        }

        protected override IEnumerable<Expression<Func<RecruitmentProcessStep, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return r => r.AssignedEmployee.FirstName;
            yield return r => r.AssignedEmployee.LastName;
            yield return r => r.DecisionComment;

            yield return r => r.RecruitmentHint;

            yield return r => r.RecruitmentProcess.Candidate.FirstName;
            yield return r => r.RecruitmentProcess.Candidate.LastName;
            yield return r => r.RecruitmentProcess.Candidate.EmailAddress;
        }

        private void CloseRecruitmentProcessIfApplicable(long recruitmentProcessId, RecruitmentProcessStepDto recruitmentProcess)
        {
            if (recruitmentProcess.Type == RecruitmentProcessStepType.TechnicalReview)
            {
                return;
            }

            _recruitmentProcessService.CloseRecruitmentProcess(new RecruitmentProcessCloseDto
            {
                Id = recruitmentProcessId,
                Comment = recruitmentProcess.DecisionComment,
                Reason = GetRejectCloseReason(recruitmentProcess.Type)
            });
        }

        private RecruitmentProcessClosedReason GetRejectCloseReason(RecruitmentProcessStepType recruitmentProcessStepType)
        {
            switch (recruitmentProcessStepType)
            {
                case RecruitmentProcessStepType.ContractNegotiations:
                    return RecruitmentProcessClosedReason.RejectedByCandidate;

                case RecruitmentProcessStepType.HrCall:
                case RecruitmentProcessStepType.HrInterview:
                    return RecruitmentProcessClosedReason.RejectedByRecruiter;

                case RecruitmentProcessStepType.ClientInterview:
                case RecruitmentProcessStepType.ClientResumeApproval:
                    return RecruitmentProcessClosedReason.RejectedByClient;

                case RecruitmentProcessStepType.HiringDecision:
                case RecruitmentProcessStepType.HiringManagerInterview:
                case RecruitmentProcessStepType.HiringManagerResumeApproval:
                    return RecruitmentProcessClosedReason.RejectedByHiringManager;

                case RecruitmentProcessStepType.TechnicalReview:
                default:
                    throw new ArgumentOutOfRangeException(nameof(recruitmentProcessStepType));
            }

            throw new NotImplementedException("Not implemented reject reason on recruitment process step");
        }

        private void AddNewTechnicalReview(RecruitmentProcessStepDto recruitmentProcessStep, long processStepId, IUnitOfWork<IRecruitmentDbScope> unitOfWork)
        {
            var technicalReviewStepDetails = (TechnicalReviewStepDetailsDto)recruitmentProcessStep.StepDetails;

            if (technicalReviewStepDetails == null)
            {
                return;
            }

            var newTechnicalReview = new TechnicalReview
            {
                RecruitmentProcessId = recruitmentProcessStep.RecruitmentProcessId,
                RecruitmentProcessStepId = processStepId,
                JobProfiles = technicalReviewStepDetails.JobProfileIds.Select(t => new JobProfile
                {
                    Id = t
                }).ToList()

            };

            newTechnicalReview.JobProfiles = EntityMergingService.CreateEntitiesFromIds<JobProfile, IRecruitmentDbScope>(unitOfWork, newTechnicalReview.JobProfiles.GetIds());

            unitOfWork.Repositories.TechnicalReviews.Add(newTechnicalReview);

            unitOfWork.Commit();
        }

        private void AdjustDueDateOnAdding(RecruitmentProcessStep step)
        {
            var defaultCalendarCode = SystemParameters.GetParameter<string>(ParameterKeys.RecruitmentDefaultCalendarCode);
            var today = TimeService.GetCurrentDate();
            var now = TimeService.GetCurrentTime();

            switch (step.Type)
            {
                case RecruitmentProcessStepType.HiringManagerResumeApproval:
                    var resumeApprovalDays = SystemParameters.GetParameter<int>(ParameterKeys.HiringManagerResumeApprovalWorkingDays);
                    step.DueOn = _calendarService.AddWorkdays(now, resumeApprovalDays, defaultCalendarCode);
                    break;

                case RecruitmentProcessStepType.ClientResumeApproval:
                    var clientApprovalDays = SystemParameters.GetParameter<int>(ParameterKeys.ClientResumeApprovalWorkingDays);
                    step.DueOn = _calendarService.AddWorkdays(now, clientApprovalDays, defaultCalendarCode);
                    break;

                case RecruitmentProcessStepType.HiringDecision:
                    var hiringDecisionDays = SystemParameters.GetParameter<int>(ParameterKeys.HiringDecisionWorkingDays);
                    step.DueOn = _calendarService.AddWorkdays(now, hiringDecisionDays, defaultCalendarCode);
                    break;

                case RecruitmentProcessStepType.ContractNegotiations:
                    var contractNegotiationDays = SystemParameters.GetParameter<int>(ParameterKeys.ContractNegotiationCalendarDays);
                    step.DueOn = today.AddDays(contractNegotiationDays);
                    break;

                default:
                    AdjustDueDateOnEditing(step);
                    break;
            }
        }

        private void AdjustDueDateOnEditing(RecruitmentProcessStep step)
        {
            if (step.PlannedOn == null)
            {
                return;
            }

            var defaultCalendarCode = SystemParameters.GetParameter<string>(ParameterKeys.RecruitmentDefaultCalendarCode);

            switch (step.Type)
            {
                case RecruitmentProcessStepType.HrCall:
                case RecruitmentProcessStepType.HrInterview:
                    var hrDays = SystemParameters.GetParameter<int>(ParameterKeys.HrInterviewResponseDays);
                    step.DueOn = _calendarService.AddWorkdays(step.PlannedOn.Value, hrDays, defaultCalendarCode);
                    break;

                case RecruitmentProcessStepType.HiringManagerInterview:
                    var hiringManagerDays = SystemParameters.GetParameter<int>(ParameterKeys.HiringManagerInterviewResponseDays);
                    step.DueOn = _calendarService.AddWorkdays(step.PlannedOn.Value, hiringManagerDays, defaultCalendarCode);
                    break;

                case RecruitmentProcessStepType.TechnicalReview:
                    var technicalDays = SystemParameters.GetParameter<int>(ParameterKeys.TechnicalInterviewResponseDays);
                    step.DueOn = _calendarService.AddWorkdays(step.PlannedOn.Value, technicalDays, defaultCalendarCode);
                    break;
            }
        }

        private long CurrentEmployeeId => _dependencies.PrincipalProvider.Current.EmployeeId;
    }
}
