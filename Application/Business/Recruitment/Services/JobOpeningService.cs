﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Enums;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification;
using Smt.Atomic.Business.Recruitment.Resources;
using Smt.Atomic.Business.Recruitment.SnapshotComparer;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Presentation.Common.Interfaces;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class JobOpeningService : IJobOpeningService
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly IUserService _userService;
        private readonly IEmployeeService _employeeService;
        private readonly ICityService _cityService;
        private readonly IAlertService _alertService;
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;
        private readonly IClassMappingFactory _classMappingFactory;
        private readonly IOrgUnitService _orgUnitService;
        private readonly ISkillService _skillService;
        private readonly ITimeService _timeService;
        private readonly IJobOpeningNoteService _jobOpeningNotesService;
        private readonly IJobOpeningNotificationService _jobOpeningNotificationService;
        private readonly IJobProfileService _jobProfileService;
        private readonly IJobOpeningHistoryService _jobOpeningHistoryService;

        public JobOpeningService(
            IPrincipalProvider principalProvider,
            IUserService userService,
            IEmployeeService employeeService,
            IAlertService alertService,
            IClassMappingFactory classMappingFactory,
            ICityService cityService,
            IOrgUnitService orgUnitService,
            ISkillService skillService,
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService,
            ITimeService timeService,
            IJobOpeningNoteService jobOpeningNotesService,
            IJobOpeningNotificationService jobOpeningNotificationService,
            IJobProfileService jobProfileService,
            IJobOpeningHistoryService jobOpeningHistoryService)
        {
            _principalProvider = principalProvider;
            _userService = userService;
            _employeeService = employeeService;
            _alertService = alertService;
            _unitOfWorkService = unitOfWorkService;
            _timeService = timeService;
            _classMappingFactory = classMappingFactory;
            _cityService = cityService;
            _orgUnitService = orgUnitService;
            _skillService = skillService;
            _jobOpeningNotesService = jobOpeningNotesService;
            _jobOpeningNotificationService = jobOpeningNotificationService;
            _jobProfileService = jobProfileService;
            _jobOpeningHistoryService = jobOpeningHistoryService;
        }

        public void ApproveJobOpenings([NotNull]ICollection<long> jobOpeningIds)
        {
            if (jobOpeningIds == null || !jobOpeningIds.Any())
            {
                throw new ArgumentException(nameof(jobOpeningIds));
            }

            var now = _timeService.GetCurrentTime();
            var currentEmployeeId = _principalProvider.Current.EmployeeId;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var jobOpenings = unitOfWork.Repositories.JobOpenings.Filtered()
                    .Where(w => jobOpeningIds.Contains(w.Id)
                        && w.Status == JobOpeningStatus.PendingApproval);

                foreach (var jobOpening in jobOpenings)
                {
                    jobOpening.Status = JobOpeningStatus.Active;
                    jobOpening.AcceptingEmployeeId = _principalProvider.Current.EmployeeId;
                    jobOpening.RoleOpenedOn = now;
                    jobOpening.RejectionReason = null;
                    unitOfWork.Repositories.JobOpenings.Edit(jobOpening);

                    SendApprovedReminderEmail(jobOpening);
                }

                unitOfWork.Commit();

                foreach (var jobOpeningId in jobOpeningIds)
                {
                    _jobOpeningHistoryService.LogActivity(jobOpeningId, JobOpeningHistoryEntryType.Approved);
                }
            }
        }

        public void RejectJobOpenings([NotNull]ICollection<long> jobOpeningIds, string rejectionReason)
        {
            if (jobOpeningIds == null || !jobOpeningIds.Any())
            {
                throw new ArgumentException(nameof(jobOpeningIds));
            }

            var currentEmployeeId = _principalProvider.Current.EmployeeId;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var jobOpenings = unitOfWork.Repositories.JobOpenings.Filtered()
                    .Where(w => jobOpeningIds.Contains(w.Id)
                        && w.Status == JobOpeningStatus.PendingApproval).ToList();

                foreach (var jobOpening in jobOpenings)
                {
                    jobOpening.Status = JobOpeningStatus.Rejected;
                    jobOpening.RejectionReason = rejectionReason;
                    unitOfWork.Repositories.JobOpenings.Edit(jobOpening);

                    _jobOpeningNotesService.AddNote(new JobOpeningNoteDto
                    {
                        JobOpeningId = jobOpening.Id,
                        NoteType = NoteType.SystemNote,
                        NoteVisibility = NoteVisibility.Public,
                        Content = string.Format(JobOpeningResources.JobOpeningRejectedNoteMessage, jobOpening.PositionName, jobOpening.RejectionReason)
                    });

                    unitOfWork.Commit();

                    SendRejectedReminderEmail(jobOpening, rejectionReason);
                }

                foreach (var jobOpeningId in jobOpeningIds)
                {
                    _jobOpeningHistoryService.LogActivity(jobOpeningId, JobOpeningHistoryEntryType.Rejected);
                }
            }
        }

        public void SendForApprovalJobOpening(long jobOpeningId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var jobOpening = unitOfWork.Repositories.JobOpenings.Filtered()
                    .Where(w => w.Id == jobOpeningId
                        && (w.Status == JobOpeningStatus.Draft || w.Status == JobOpeningStatus.Rejected || w.Status == JobOpeningStatus.OnHold))
                    .Single();

                jobOpening.Status = JobOpeningStatus.PendingApproval;
                SendForApprovalRemainderEmail(jobOpening);

                unitOfWork.Commit();

                _jobOpeningHistoryService.LogActivity(jobOpeningId, JobOpeningHistoryEntryType.SentForApproval);
            }
        }

        public void CloseJobOpening([NotNull] JobOpeningCloseDto jobOpeningClose)
        {
            if (jobOpeningClose == null)
            {
                return;
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var jobOpening = unitOfWork.Repositories.JobOpenings.GetById(jobOpeningClose.JobOpeningId);

                jobOpening.ClosedComment = jobOpeningClose.ClosedComment;
                jobOpening.ClosedReason = jobOpeningClose.ClosedReason;
                jobOpening.ClosedOn = _timeService.GetCurrentTime();
                jobOpening.Status = JobOpeningStatus.Closed;

                unitOfWork.Commit();

                SendClosedNotificationEmail(jobOpening);
                _jobOpeningHistoryService.LogActivity(jobOpeningClose.JobOpeningId, JobOpeningHistoryEntryType.Closed);
            }
        }

        public void ToggleWatching(ICollection<long> jobOpeningIds, long employeeId, WatchingStatus jobOpeningWatching)
        {
            if (jobOpeningIds == null || !jobOpeningIds.Any())
            {
                throw new ArgumentException(nameof(jobOpeningIds));
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var jobOpenings = unitOfWork.Repositories.JobOpenings.Filtered().GetByIds(jobOpeningIds);
                var employee = unitOfWork.Repositories.Employees.GetById(employeeId);

                WatcherHelper.Toggle(jobOpenings, employee, jobOpeningWatching);

                unitOfWork.Commit();

                foreach (var jobOpeningId in jobOpeningIds)
                {
                    _jobOpeningHistoryService.LogActivity(jobOpeningId,
                            jobOpeningWatching == WatchingStatus.Watch ? JobOpeningHistoryEntryType.Watched : JobOpeningHistoryEntryType.Unwatched);
                }
            }
        }

        public void NotifyAboutRecordChanges(JobOpeningDto before, JobOpeningDto after)
        {
            var changedValues = GetSnapshotChanges(before, after);

            if (changedValues.IsNullOrEmpty())
            {
                return;
            }

            _jobOpeningNotificationService.SendEmailWithChanges(after.Id, changedValues);


            _alertService.AddInformation(JobOpeningResources.JobOpeningUpdateInformation);
        }

        public Dictionary<string, ValueChange> GetSnapshotChanges(JobOpeningDto before, JobOpeningDto after)
        {
            var snapshotComparer = new SnapshotComparer<JobOpeningDto>();

            SetComparedFields(snapshotComparer);
            SetRecordChangeConverters(snapshotComparer);

            snapshotComparer.Compare(before, after);

            return snapshotComparer.GetChangedValues();
        }

        private void SetComparedFields(SnapshotComparer<JobOpeningDto> snapshotComparer)
        {
            snapshotComparer.AddComparedField(j => j.PositionName);
            snapshotComparer.AddComparedField(j => j.CustomerId);
            snapshotComparer.AddComparedField(j => j.CityIds);
            snapshotComparer.AddComparedField(j => j.JobProfileIds);
            snapshotComparer.AddComparedField(j => j.OrgUnitId);
            snapshotComparer.AddComparedField(j => j.CompanyId);
            snapshotComparer.AddComparedField(j => j.BusinessLineId);
            snapshotComparer.AddComparedField(j => j.AcceptingEmployeeId);
            snapshotComparer.AddComparedField(j => j.Priority);
            snapshotComparer.AddComparedField(j => j.HiringMode);
            snapshotComparer.AddComparedField(j => j.VacancyLevel1);
            snapshotComparer.AddComparedField(j => j.VacancyLevel2);
            snapshotComparer.AddComparedField(j => j.VacancyLevel3);
            snapshotComparer.AddComparedField(j => j.VacancyLevel4);
            snapshotComparer.AddComparedField(j => j.VacancyLevel5);
            snapshotComparer.AddComparedField(j => j.VacancyLevelNotApplicable);
            snapshotComparer.AddComparedField(j => j.RecruitersIds);
            snapshotComparer.AddComparedField(j => j.RecruitmentOwnersIds);
            snapshotComparer.AddComparedField(j => j.RecruitmentHelpersIds);
            snapshotComparer.AddComparedField(j => j.IsIntiveResumeRequired);
            snapshotComparer.AddComparedField(j => j.IsPolishSpeakerRequired);
            snapshotComparer.AddComparedField(j => j.IsTechnicalVerificationRequired);
            snapshotComparer.AddComparedField(j => j.NoticePeriod);
            snapshotComparer.AddComparedField(j => j.SalaryRate);
            snapshotComparer.AddComparedField(j => j.EnglishLevel);
            snapshotComparer.AddComparedField(j => j.OtherLanguages);
            snapshotComparer.AddComparedField(j => j.AgencyEmploymentMode);
            snapshotComparer.AddComparedField(j => j.Status);
            snapshotComparer.AddComparedField(j => j.IsHighlyConfidential);
            snapshotComparer.AddComparedField(j => j.DecisionMakerId);
            snapshotComparer.AddComparedField(j => j.ClosedComment);
            snapshotComparer.AddComparedField(j => j.ClosedReason);
            snapshotComparer.AddComparedField(j => j.TechnicalReviewComment);
        }

        private void SetRecordChangeConverters(SnapshotComparer<JobOpeningDto> SnapshotComparer)
        {
            SnapshotComparer.AddDisplayNameConverter<long>(
                j => j.CustomerId,
                id => _employeeService.GetEmployeeOrDefaultById(id)?.FullName);

            SnapshotComparer.AddDisplayNameConverter<long[]>(
                j => j.CityIds,
                ids => string.Join(", ", _cityService.GetCitiesByIds(ids)?.Select(l => l.Name)));

            SnapshotComparer.AddDisplayNameConverter<long>(
                j => j.BusinessLineId,
                id => _orgUnitService.GetOrgUnitOrDefaultById(id)?.FlatName);

            SnapshotComparer.AddCustomObjectConverters<long[]>(
                GetEmployeeProperties(),
                id => string.Join(", ", _employeeService.GetEmployeesById(id)?.Select(l => l.FullName)));

            SnapshotComparer.AddDisplayNameConverter<long[]>(
                 j => j.JobProfileIds,
                 id => string.Join(", ", _jobProfileService.GetJobProfilesByIds(id)?.Select(l => l.Name)));

            SnapshotComparer.AddDisplayNameConverter<long>(
                 j => j.DecisionMakerId,
                 id => _employeeService.GetEmployeeOrDefaultById(id).FullName);

            SnapshotComparer.AddDisplayNameConverter<bool>(
                 j => j.IsTechnicalVerificationRequired,
                 isRequired => isRequired ? JobOpeningResources.Required : JobOpeningResources.NotRequired);

            SnapshotComparer.AddDisplayNameConverter<bool>(
                 j => j.IsPolishSpeakerRequired,
                 isRequired => isRequired ? JobOpeningResources.Required : JobOpeningResources.NotRequired);

            SnapshotComparer.AddDisplayNameConverter<bool>(
                 j => j.IsIntiveResumeRequired,
                 isRequired => isRequired ? JobOpeningResources.Required : JobOpeningResources.NotRequired);

            IEnumerable<Expression<Func<JobOpeningDto, object>>> GetEmployeeProperties()
            {
                yield return j => j.RecruitersIds;
                yield return j => j.RecruitmentHelpersIds;
                yield return j => j.RecruitmentOwnersIds;
            }
        }

        private void SendClosedNotificationEmail(JobOpening jobOpening)
        {
            var recipients = _jobOpeningNotificationService.SendClosedNotificationEmail(jobOpening.Id);

            var recipientsAsString = string.Join(", ", recipients.Select(s => $"{s.FullName} ({s.EmailAddress})"));

            PushAlertInformation(JobOpeningResources.JobOpeningCloseInformation,
              jobOpening.PositionName, recipientsAsString);
        }

        private void SendForApprovalRemainderEmail(JobOpening jobOpening)
        {
            var recipients = _jobOpeningNotificationService.SendForApprovalRemainderEmail(jobOpening.Id);

            var recipientsAsString = string.Join(", ", recipients.Select(s => s.EmailAddress));

            PushAlertInformation(JobOpeningResources.JobOpeningSentAprovalInformation,
              jobOpening.PositionName, recipientsAsString);
        }

        private void SendApprovedReminderEmail(JobOpening jobOpening)
        {
            var recipients = _jobOpeningNotificationService.SendApprovedReminderEmail(jobOpening.Id);

            var recipientsAsString = string.Join(", ", recipients.Select(s => s.EmailAddress));

            PushAlertInformation(JobOpeningResources.JobOpeningAppprovalInformation,
                jobOpening.PositionName, recipientsAsString);
        }

        private void SendRejectedReminderEmail(JobOpening jobOpening, string rejectionReason)
        {
            var recipients = _jobOpeningNotificationService.SendRejectedReminderEmail(jobOpening.Id);

            var recipientsAsString = string.Join(", ", recipients.Select(s => $"{s.FullName} ({s.EmailAddress})"));

            PushAlertInformation(JobOpeningResources.JobOpeningCloseInformation,
                jobOpening.PositionName, recipientsAsString);
        }

        public JobOpeningDto GetJobOpeningOrDefaultById(long jobOpeningId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var jobOpening = unitOfWork.Repositories.JobOpenings.GetByIdOrDefault(jobOpeningId);

                return jobOpening != null
                     ? _classMappingFactory.CreateMapping<JobOpening, JobOpeningDto>().CreateFromSource(jobOpening)
                     : null;
            }
        }

        public bool AddNote(AddNoteDto note)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var jobOpeningNote = new JobOpeningNote
                {
                    JobOpeningId = note.ParentId,
                    Content = note.Content,
                    NoteVisibility = note.NoteVisibility,
                    NoteType = NoteType.UserNote,
                    IsDeleted = false,
                };

                unitOfWork.Repositories.JobOpeningNotes.Add(jobOpeningNote);
                unitOfWork.Commit();

                _jobOpeningHistoryService.LogActivity(note.ParentId, JobOpeningHistoryEntryType.AddedNote);
            }

            return true;
        }

        public BoolResult PutJobOpeningOnHold(long jobOpeningId)
        {
            var changeStatusResult = ChangeStatusOnJobOpening(jobOpeningId, JobOpeningStatus.Active, JobOpeningStatus.OnHold,
                JobOpeningResources.PutOnHoldNoteContent, _jobOpeningNotificationService.SendPutOnHoldRemainderEmail);

            if (changeStatusResult.IsSuccessful)
            {
                _jobOpeningHistoryService.LogActivity(jobOpeningId, JobOpeningHistoryEntryType.PutOnHold);
            }

            return changeStatusResult;
        }

        public BoolResult TakeJobOpeningOffHold(long jobOpeningId)
        {
            var changeStatusResult = ChangeStatusOnJobOpening(jobOpeningId, JobOpeningStatus.OnHold, JobOpeningStatus.Active,
                JobOpeningResources.TakeOffHoldNoteContent, _jobOpeningNotificationService.SendTakeOffHoldRemainderEmail);

            if (changeStatusResult.IsSuccessful)
            {
                _jobOpeningHistoryService.LogActivity(jobOpeningId, JobOpeningHistoryEntryType.TookOffHold);
            }

            return changeStatusResult;
        }

        private BoolResult ChangeStatusOnJobOpening(long jobOpeningId, JobOpeningStatus oldStatus, JobOpeningStatus newStatus,
            string noteContent, Func<long, bool, IList<EmailRecipientDto>> notificationFunction)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var jobOpening = unitOfWork.Repositories.JobOpenings.Filtered().GetById(jobOpeningId);

                var hasHighlyConfidentialProcesses = jobOpening.RecruitmentProcesses
                   .Any(r => RecruitmentProcessBusinessLogic.IsHighlyConfidential.Call(r));

                if (jobOpening.Status != oldStatus)
                {
                    return new BoolResult(false);
                }

                var employee = unitOfWork.Repositories.Employees.GetById(_principalProvider.Current.EmployeeId);

                jobOpening.Status = newStatus;

                if (newStatus == JobOpeningStatus.Active)
                {
                    jobOpening.RoleOpenedOn = _timeService.GetCurrentDate();
                }

                var noteText = string.Format(noteContent, employee.FullName, jobOpening.RoleOpenedOn?.ToShortDateString());

                AddSystemNote(unitOfWork, jobOpening.Id, noteText);

                unitOfWork.Commit();

                notificationFunction.Invoke(jobOpeningId, hasHighlyConfidentialProcesses);

                return new BoolResult(true);
            }
        }

        private void AddSystemNote(IUnitOfWork<IRecruitmentDbScope> unitOfWork, long jobOpeningId, string content)
        {
            var jobOpeningNote = new JobOpeningNote
            {
                JobOpeningId = jobOpeningId,
                NoteVisibility = NoteVisibility.Public,
                Content = content,
                NoteType = NoteType.SystemNote
            };

            unitOfWork.Repositories.JobOpeningNotes.Add(jobOpeningNote);
        }

        private void PushAlertInformation(string information, string positionName, string recipientsAsString)
        {
            _alertService.AddInformation(string.Format(information, positionName, recipientsAsString));
        }

        public long CloneJobOpening(long jobOpeningId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var clonedJobOpeningQuery = unitOfWork.Repositories.JobOpenings
                    .Include(nameof(JobOpening.Cities))
                    .Include(nameof(JobOpening.JobProfiles))
                    .Include(nameof(JobOpening.Recruiters))
                    .Include(nameof(JobOpening.RecruitmentOwners));

                var clonedJobOpening = System.Data.Entity.QueryableExtensions
                    .AsNoTracking(clonedJobOpeningQuery)
                    .GetById(jobOpeningId);

                clonedJobOpening.DecisionMakerEmployeeId = _principalProvider.Current.EmployeeId;
                clonedJobOpening.Status = JobOpeningStatus.Draft;

                unitOfWork.Repositories.JobOpenings.Add(clonedJobOpening);
                unitOfWork.Commit();

                var projectDescriptionsQuery = unitOfWork.Repositories.ProjectDescriptions
                    .Where(s => s.JobOpeningId == jobOpeningId);

                var projectDescriptions = System.Data.Entity.QueryableExtensions
                   .AsNoTracking(projectDescriptionsQuery);

                foreach (var projectDescription in projectDescriptions)
                {
                    projectDescription.JobOpeningId = clonedJobOpening.Id;
                }

                unitOfWork.Repositories.ProjectDescriptions.AddRange(projectDescriptions);
                unitOfWork.Commit();

                _jobOpeningHistoryService.LogActivity(jobOpeningId, JobOpeningHistoryEntryType.Cloned);

                return clonedJobOpening.Id;
            }
        }

        public void ReopenJobOpening(long jobOpeningId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var jobOpening = unitOfWork.Repositories.JobOpenings.GetById(jobOpeningId);

                jobOpening.Status = JobOpeningStatus.Active;
                jobOpening.RoleOpenedOn = _timeService.GetCurrentDate();
                unitOfWork.Commit();
            }

            _jobOpeningNotificationService.SendReopenReminderEmail(jobOpeningId);
            _jobOpeningHistoryService.LogActivity(jobOpeningId, JobOpeningHistoryEntryType.Reopened);
        }
    }
}