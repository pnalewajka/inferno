﻿using System.Linq;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    class InboundEmailPickerCardIndexDataService : InboundEmailCardIndexDataService,
        IInboundEmailPickerCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public InboundEmailPickerCardIndexDataService(ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies,
            IClassMapping<Candidate, CandidateDto> candidateClassMapping,
            IUploadedDocumentHandlingService uploadedDocumentHandlingService,
            IInboundEmailNotificationService inboundEmailNotificationService)
            : base(dependencies, candidateClassMapping, uploadedDocumentHandlingService, inboundEmailNotificationService)
        {
        }

        public override IQueryable<InboundEmail> ApplyContextFiltering(IQueryable<InboundEmail> records, object context)
        {
            var filteredRecords = base.ApplyContextFiltering(records, context);

            if (context is InboundEmailPickerContext inboundEmailPickerContext)
            {
                return ApplyInboundEmailPickerContextFiltering(records, inboundEmailPickerContext);
            }

            return records;
        }

        private IQueryable<InboundEmail> ApplyInboundEmailPickerContextFiltering(IQueryable<InboundEmail> records, InboundEmailPickerContext inboundEmailPickerContext)
        {
            records = inboundEmailPickerContext.AssignedOnly
                ? records.Where(r => r.ProcessedById == PrincipalProvider.Current.EmployeeId)
                : records;

            records = inboundEmailPickerContext.InboundEmailId != null
                ? records.Where(r => r.Id == inboundEmailPickerContext.InboundEmailId)
                : records;

            return records;
        }
    }
}
