﻿using System.Collections.Generic;
using Smt.Atomic.Business.Recruitment.Helpers;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Business.Recruitment.SnapshotComparer;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class RecruitmentProcessHistoryService : IRecruitmentProcessHistoryService
    {
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly ITimeService _timeService;

        public RecruitmentProcessHistoryService(
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService,
            IPrincipalProvider principalProvider,
            ITimeService timeService)
        {
            _unitOfWorkService = unitOfWorkService;
            _principalProvider = principalProvider;
            _timeService = timeService;
        }

        public void LogActivity(long recruitmentProcessId, RecruitmentProcessHistoryEntryType activityType, long? recruitmentProcessStepId = null, Dictionary<string, ValueChange> snapshotChanges = null)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var activity = new RecruitmentProcessHistoryEntry
                {
                    RecruitmentProcessId = recruitmentProcessId,
                    PerformedByUserId = _principalProvider.Current.Id.Value,
                    ActivityType = activityType,
                    PerformedOn = _timeService.GetCurrentTime(),
                    RecruitmentProcessStepId = recruitmentProcessStepId,
                    ChangesDescription = SnapshotComaparerHelper.SerializeSnapshotChanges(snapshotChanges)
                };

                unitOfWork.Repositories.RecruitmentProcessHistoryEntries.Add(activity);
                unitOfWork.Commit();
            }
        }
    }
}