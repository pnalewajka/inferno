﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Consts;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Enums;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Business.Recruitment.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Business.Helpers;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class RecruitmentProcessCardIndexDataService : CardIndexDataService<RecruitmentProcessDto, RecruitmentProcess, IRecruitmentDbScope>, IRecruitmentProcessCardIndexDataService
    {
        private readonly ICardIndexServiceDependencies<IRecruitmentDbScope> _dependencies;
        private readonly IRecruitmentProcessService _recruitmentProcessService;
        private readonly IRecruitmentProcessStepService _recruitmentProcessStepService;
        private readonly ICandidateService _candidateService;
        private readonly IRecruitmentProcessHistoryService _recruitmentProcessHistoryService;
        private readonly IDataActivityLogService _dataActivityLogService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public RecruitmentProcessCardIndexDataService(ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies,
            IRecruitmentProcessService recruitmentProcessService,
            IDataActivityLogService dataActivityLogService,
            IRecruitmentProcessStepService recruitmentProcessStepService,
            ICandidateService candidateService,
            IRecruitmentProcessHistoryService recruitmentProcessDataActivityService)
            : base(dependencies)
        {
            _dependencies = dependencies;
            _recruitmentProcessService = recruitmentProcessService;
            _recruitmentProcessStepService = recruitmentProcessStepService;
            _candidateService = candidateService;
            _recruitmentProcessHistoryService = recruitmentProcessDataActivityService;
            _dataActivityLogService = dataActivityLogService;
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<RecruitmentProcess, RecruitmentProcessDto, IRecruitmentDbScope> eventArgs)
        {
            eventArgs.InputEntity.Watchers = EntityMergingService.CreateEntitiesFromIds<Employee, IRecruitmentDbScope>(eventArgs.UnitOfWork, eventArgs.InputEntity.Watchers.GetIds());
            eventArgs.InputEntity.LastActivityOn = TimeService.GetCurrentTime();
            eventArgs.InputEntity.LastActivityById = _dependencies.PrincipalProvider.Current.EmployeeId;

            var candidate = eventArgs.UnitOfWork.Repositories.Candidates.GetById(eventArgs.InputEntity.CandidateId);

            if (eventArgs.InputEntity.CrmId == null && !CandidateBusinessLogic.HasValidConsent.Call(candidate, TimeService.GetCurrentDate()))
            {
                throw new BusinessException(CandidateResources.CandidateInvalidDataConsent);
            }

            candidate.ConsentCoordinatorId = candidate.ConsentCoordinatorId != 0
                ? candidate.ConsentCoordinatorId
                : PrincipalProvider.Current.EmployeeId;

            var hasMatchingProcesses = eventArgs.UnitOfWork.Repositories.RecruitmentProcesses
                                           .Where(RecruitmentProcessBusinessLogic.IsMatchedProcess.Parametrize(
                                               eventArgs.InputEntity.CandidateId, eventArgs.InputEntity.JobOpeningId))
                                           .Any();

            eventArgs.InputEntity.Screening = new RecruitmentProcessScreening { RecruitmentProcess = eventArgs.InputEntity };

            if (eventArgs.InputEntity.CrmId == null && hasMatchingProcesses)
            {
                eventArgs.AddError(RecruitmentProcessResources.OtherRecruitmentProcessForThisCandidateAndJobOpeningExists);

                return;
            }

            if (eventArgs.InputEntity.JobApplicationId.HasValue)
            {
                var applicationOriginId = eventArgs.UnitOfWork.Repositories.JobApplications
                    .SelectById(eventArgs.InputEntity.JobApplicationId.Value, jo => jo.ApplicationOriginId);

                eventArgs.InputEntity.Final = new RecruitmentProcessFinal
                {
                    RecruitmentProcess = eventArgs.InputEntity,
                    EmploymentSourceId = applicationOriginId
                };
            }

            AddRecruitmentProcessSteps(eventArgs);
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<RecruitmentProcess> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var direction = sortingCriterion.GetSortingDirection();
            var sortingColumnName = sortingCriterion.GetSortingColumnName();

            if (sortingColumnName == nameof(RecruitmentProcessDto.DecisionMakerName))
            {
                orderedQueryBuilder.ApplySortingKey(RecruitmentProcessBusinessLogic.DecisionMakerFullName.AsExpression(), direction);
            }
            else
            {
                base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
            }
        }

        private void AddRecruitmentProcessSteps(RecordAddingEventArgs<RecruitmentProcess, RecruitmentProcessDto, IRecruitmentDbScope> eventArgs)
        {
            var recruitmentProcessDto = eventArgs.InputDto;

            if (recruitmentProcessDto.InitialSteps == null || !recruitmentProcessDto.InitialSteps.Any())
            {
                return;
            }

            var jobOpening = eventArgs.UnitOfWork.Repositories.JobOpenings.Single(j => j.Id == recruitmentProcessDto.JobOpeningId);

            eventArgs.InputEntity.ProcessSteps = recruitmentProcessDto.InitialSteps
                .Select(s => _recruitmentProcessStepService.CreateInitialStep(s, recruitmentProcessDto.RecruiterId, jobOpening.DecisionMakerEmployeeId))
                .ToList();

            if (recruitmentProcessDto.InitialSteps.Contains(RecruitmentProcessStepType.TechnicalReview))
            {
                eventArgs.InputEntity.TechnicalReviews = new List<TechnicalReview> {
                    new TechnicalReview {
                        JobProfiles = jobOpening.JobProfiles,
                        RecruitmentProcessStep = eventArgs.InputEntity.ProcessSteps.Single(p => p.Type == RecruitmentProcessStepType.TechnicalReview)
                    }
                };
            }

            if (recruitmentProcessDto.InitialSteps.Any(e => RecruitmentProcessStepBusinessLogic.IsMakingProcessMatureForHiringManager.Call(e)))
            {
                eventArgs.InputEntity.IsMatureEnoughForHiringManager = true;
            }
        }

        protected override IQueryable<RecruitmentProcess> ConfigureIncludes(IQueryable<RecruitmentProcess> sourceQueryable)
        {
            return base.ConfigureIncludes(sourceQueryable)
                .Include(c => c.Candidate.Languages.Select(l => l.Language))
                .Include(c => c.JobOpening)
                .Include(c => c.JobOpening.DecisionMakerEmployee);
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<RecruitmentProcess, RecruitmentProcessDto, IRecruitmentDbScope> eventArgs)
        {
            if (eventArgs.DbEntity.Status == RecruitmentProcessStatus.Closed)
            {
                throw new InvalidOperationException("Record can't be edited when status is Closed");
            }

            if (eventArgs.InputEntity.JobOpeningId != eventArgs.DbEntity.JobOpeningId)
            {
                var hasMatchingProcesses = eventArgs.UnitOfWork.Repositories.RecruitmentProcesses
                                   .Where(p => p.Status != RecruitmentProcessStatus.Closed
                                               && p.CandidateId == eventArgs.InputEntity.CandidateId
                                               && p.JobOpeningId == eventArgs.InputEntity.JobOpeningId
                                               && p.Id != eventArgs.InputEntity.Id)
                                   .Any();

                if (hasMatchingProcesses)
                {
                    eventArgs.AddError(RecruitmentProcessResources.OtherRecruitmentProcessForThisCandidateAndJobOpeningExists);

                    return;
                }
            }

            eventArgs.InputEntity.LastActivityOn = TimeService.GetCurrentTime();
            eventArgs.InputEntity.LastActivityById = _dependencies.PrincipalProvider.Current.EmployeeId;
            eventArgs.InputEntity.IsMatureEnoughForHiringManager = eventArgs.DbEntity.IsMatureEnoughForHiringManager;

            _recruitmentProcessService.NotifyWatchersAboutRecordChanges(eventArgs.DbEntity, eventArgs.InputEntity);

            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.Watchers, eventArgs.InputEntity.Watchers);

            if (eventArgs.InputEntity.JobApplicationId.HasValue && !eventArgs.DbEntity.JobApplicationId.HasValue && eventArgs.InputEntity.Final != null)
            {
                eventArgs.InputEntity.Final.EmploymentSourceId =
                    eventArgs.UnitOfWork.Repositories.JobApplications.GetById(eventArgs.InputEntity.JobApplicationId.Value).ApplicationOriginId;
            }
        }

        protected override IEnumerable<Expression<Func<RecruitmentProcess, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return r => r.Candidate.FirstName;
            yield return r => r.Candidate.LastName;
            yield return r => r.Candidate.EmailAddress;
            yield return r => r.Candidate.OtherEmailAddress;
            yield return r => r.Candidate.MobilePhone;
            yield return r => r.Candidate.OtherPhone;

            yield return r => r.JobOpening.PositionName;
        }

        protected override NamedFilters<RecruitmentProcess> NamedFilters
        {
            get
            {
                var currentEmployeeId = PrincipalProvider.Current.EmployeeId;
                var currentUserId = PrincipalProvider.Current.Id ?? BusinessLogicHelper.NonExistingId;

                return new NamedFilters<RecruitmentProcess>
                (
                    new[]
                    {
                        new NamedFilter<RecruitmentProcess>(FilterCodes.MyRecords, RecruitmentProcessBusinessLogic.IsOwnRecruitmentProcess.Parametrize(currentUserId, currentEmployeeId), SecurityRoleType.CanFilterRecruitmentProcessesByMyRecords),
                        new NamedFilter<RecruitmentProcess>(FilterCodes.AnyOwner, r => true, SecurityRoleType.CanFilterRecruitmentProcessesByAnyOwner),
                        new NamedFilter<RecruitmentProcess>(FilterCodes.MaturedProcessesOnly, r => r.IsMatureEnoughForHiringManager),
                        new NamedFilter<RecruitmentProcess>(FilterCodes.AnyMaturityWillDo, r => true, SecurityRoleType.CanViewRecruitmentProcessStepsTooEarlyForHiringManager)
                    }
                    .Union(CardIndexServiceHelper.BuildSoftDeletableFilters<RecruitmentProcess>())
                );
            }
        }

        private void HandleWorkflowAction(RecruitmentProcessWorkflowAction action, RecruitmentProcess dbEntity, RecruitmentProcessDto dto)
        {
            switch (action)
            {
                case RecruitmentProcessWorkflowAction.Watch:
                    _recruitmentProcessService.ToggleWatching(new long[] { dbEntity.Id }, PrincipalProvider.Current.EmployeeId, WatchingStatus.Watch);
                    return;

                case RecruitmentProcessWorkflowAction.Unwatch:
                    _recruitmentProcessService.ToggleWatching(new long[] { dbEntity.Id }, PrincipalProvider.Current.EmployeeId, WatchingStatus.Unwatch);
                    return;

                case RecruitmentProcessWorkflowAction.Close:
                    _recruitmentProcessService.CloseRecruitmentProcess(new RecruitmentProcessCloseDto { Id = dbEntity.Id, Reason = dto.WorkflowClosedReason, Comment = dto.WorkflowClosedComment });
                    return;
            }
        }

        public override RecruitmentProcessDto GetDefaultNewRecord()
        {
            var dto = base.GetDefaultNewRecord();

            dto.Status = RecruitmentProcessStatus.Active;
            dto.RecruiterId = PrincipalProvider.Current.EmployeeId;

            return dto;
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<RecruitmentProcess, RecruitmentProcessDto> recordAddedEventArgs)
        {
            _dataActivityLogService.LogCandidateDataActivity(recordAddedEventArgs.InputEntity.CandidateId, ActivityType.CreatedRecord,
                ReferenceType.RecruitmentProcess, recordAddedEventArgs.InputEntity.Id);

            _recruitmentProcessHistoryService.LogActivity(recordAddedEventArgs.InputEntity.Id, RecruitmentProcessHistoryEntryType.Created);

            if (recordAddedEventArgs.InputDto.WorkflowAction.HasValue)
            {
                HandleWorkflowAction(recordAddedEventArgs.InputDto.WorkflowAction.Value, recordAddedEventArgs.InputEntity, recordAddedEventArgs.InputDto);
            }

            var candidateIds = new List<long>
            {
                recordAddedEventArgs.InputEntity.CandidateId
            };

            _candidateService.UpdateLastActivity(recordAddedEventArgs.InputEntity.Candidate.Id, ReferenceType.RecruitmentProcess, recordAddedEventArgs.InputEntity.Id);

            base.OnRecordAdded(recordAddedEventArgs);
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<RecruitmentProcess, RecruitmentProcessDto> recordEditedEventArgs)
        {
            _dataActivityLogService.LogCandidateDataActivity(recordEditedEventArgs.InputEntity.CandidateId, ActivityType.UpdatedRecord,
                ReferenceType.RecruitmentProcess, recordEditedEventArgs.InputEntity.Id);

            var snapshotChanges = _recruitmentProcessService.GetShanpshotChanges(recordEditedEventArgs.OriginalDto, recordEditedEventArgs.InputDto);
            _recruitmentProcessHistoryService.LogActivity(recordEditedEventArgs.InputEntity.Id, RecruitmentProcessHistoryEntryType.Updated, null, snapshotChanges);

            if (recordEditedEventArgs.InputDto.WorkflowAction.HasValue)
            {
                HandleWorkflowAction(recordEditedEventArgs.InputDto.WorkflowAction.Value, recordEditedEventArgs.InputEntity, recordEditedEventArgs.InputDto);
            }

            base.OnRecordEdited(recordEditedEventArgs);
        }

        protected override void OnRecordsDeleted(RecordDeletedEventArgs<RecruitmentProcess> eventArgs)
        {
            foreach (var entitie in eventArgs.DeletedEntities)
            {
                _dataActivityLogService.LogCandidateDataActivity(entitie.CandidateId, ActivityType.RemovedRecord, ReferenceType.RecruitmentProcess, entitie.Id);
                _recruitmentProcessHistoryService.LogActivity(entitie.Id, RecruitmentProcessHistoryEntryType.Removed);
            }

            base.OnRecordsDeleted(eventArgs);
        }
    }
}
