﻿using System.Linq;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    class JobOpeningPickerCardIndexDataService : JobOpeningCardIndexDataService,
        IJobOpeningPickerCardIndexDataService
    {
        private readonly IPrincipalProvider _principalProvider;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public JobOpeningPickerCardIndexDataService(
            ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies,
            IJobOpeningService jobOpeningService,
            IJobOpeningNotificationService jobOpeningNotifcationService,
            ICompanyService companyService,
            ISystemParameterService systemParameterService,
            IPrincipalProvider principalProvider,
            IJobOpeningHistoryService jobOpeningHistoryService)
            : base(dependencies, jobOpeningService, jobOpeningNotifcationService, systemParameterService, companyService, jobOpeningHistoryService)
        {
            _principalProvider = principalProvider;
        }

        public override IQueryable<JobOpening> ApplyContextFiltering(IQueryable<JobOpening> records, object context)
        {
            if (context is JobOpeningPickerContext jobOpeningPickerContext)
            {
                if (jobOpeningPickerContext.OnlyActive)
                {
                    return records.Where(c => c.Status == JobOpeningStatus.Active);
                }

                if (jobOpeningPickerContext.Mode == JobOpeningPickerContext.PickerMode.ReportingHiringManagers)
                {
                    if (_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateRecruitmentReportForAllJobOpenings))
                    {
                        return records.Where(c => true);
                    }
                    else if (_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateRecruitmentReportForMyHiringManagerJobOpenings))
                    {
                        records = records.Where(c => c.DecisionMakerEmployee.LineManagerId == PrincipalProvider.Current.Id);
                    }
                    else if (_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateRecruitmentReportForMyJobOpenings))
                    {
                        records = records.Where(c => c.DecisionMakerEmployeeId == PrincipalProvider.Current.Id);
                    }
                    else
                    {
                        records = records.Where(c => false);
                    }
                }
            }

            return records;
        }
    }
}
