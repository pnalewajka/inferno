﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class RecommendingPersonContactRecordCardIndexDataService : CardIndexDataService<RecommendingPersonContactRecordDto, RecommendingPersonContactRecord, IRecruitmentDbScope>, IRecommendingPersonContactRecordCardIndexDataService
    {
        private readonly IDataActivityLogService _dataActivityLogService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public RecommendingPersonContactRecordCardIndexDataService(ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies,
            IDataActivityLogService dataActivityLogService)
            : base(dependencies)
        {
            _dataActivityLogService = dataActivityLogService;
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<RecommendingPersonContactRecord, RecommendingPersonContactRecordDto> recordAddedEventArgs)
        {
            _dataActivityLogService.LogRecommendingPersonDataActivity(recordAddedEventArgs.InputEntity.RecommendingPersonId, ActivityType.CreatedRecord, ReferenceType.RecommendingPersonContact, recordAddedEventArgs.InputEntity.Id);
        }

        protected override void OnRecordsDeleted(RecordDeletedEventArgs<RecommendingPersonContactRecord> eventArgs)
        {
            foreach (var contact in eventArgs.DeletedEntities)
            {
                _dataActivityLogService.LogCandidateDataActivity(contact.RecommendingPersonId, ActivityType.RemovedRecord, ReferenceType.CandidateContact, contact.Id);
            }
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<RecommendingPersonContactRecord, RecommendingPersonContactRecordDto> recordEditedEventArgs)
        {
            _dataActivityLogService.LogRecommendingPersonDataActivity(recordEditedEventArgs.InputEntity.RecommendingPersonId, ActivityType.UpdatedRecord, ReferenceType.RecommendingPersonContact, recordEditedEventArgs.InputEntity.Id);
        }

        protected override IEnumerable<Expression<Func<RecommendingPersonContactRecord, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return r => r.RecommendingPerson.FirstName;
            yield return r => r.RecommendingPerson.LastName;
            yield return r => r.RecommendingPerson.EmailAddress;
            yield return r => r.RecommendingPerson.PhoneNumber;

            yield return r => r.Comment;
        }

        protected override NamedFilters<RecommendingPersonContactRecord> NamedFilters
        {
            get
            {
                return new NamedFilters<RecommendingPersonContactRecord>(CardIndexServiceHelper.BuildSoftDeletableFilters<RecommendingPersonContactRecord>());
            }
        }

        public override IQueryable<RecommendingPersonContactRecord> ApplyContextFiltering(IQueryable<RecommendingPersonContactRecord> records, object context)
        {
            if (context is ParentIdContext parentIdContext)
            {
                return records.Where(c => c.RecommendingPersonId == parentIdContext.ParentId);
            }

            return base.ApplyContextFiltering(records, context);
        }

        public override RecommendingPersonContactRecordDto GetDefaultNewRecord()
        {
            var now = TimeService.GetCurrentTime();
            var record = base.GetDefaultNewRecord();

            record.ContactedByEmployeeId = PrincipalProvider.Current.EmployeeId;
            record.ContactedOn = now;

            return record;
        }
    }
}
