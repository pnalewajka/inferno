﻿using System.Linq;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class CandidateRecruitmentProcessCardIndexDataService : RecruitmentProcessCardIndexDataService, ICandidateRecruitmentProcessCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public CandidateRecruitmentProcessCardIndexDataService(ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies,
            IRecruitmentProcessService recruitmentProcessService,
            IDataActivityLogService dataActivityLogService,
            IRecruitmentProcessStepService recruitmentProcessStepService,
            ICandidateService candidateService,
            IRecruitmentProcessHistoryService recruitmentProcessHistoryService)
            : base(dependencies, recruitmentProcessService, dataActivityLogService, recruitmentProcessStepService, candidateService, recruitmentProcessHistoryService)
        {
        }

        public override IQueryable<RecruitmentProcess> ApplyContextFiltering(IQueryable<RecruitmentProcess> records, object context)
        {
            if (context is ParentIdContext parentIdContext)
            {
                return records.Where(c => c.CandidateId == parentIdContext.ParentId);
            }

            return base.ApplyContextFiltering(records, context);
        }
    }
}
