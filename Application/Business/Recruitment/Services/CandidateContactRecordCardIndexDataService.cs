﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Consts;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Business.Helpers;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class CandidateContactRecordCardIndexDataService : CardIndexDataService<CandidateContactRecordDto, CandidateContactRecord, IRecruitmentDbScope>, ICandidateContactRecordCardIndexDataService
    {
        private readonly IDataActivityLogService _dataActivityLogService;
        private readonly ICandidateService _candidateService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public CandidateContactRecordCardIndexDataService(ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies,
            ICandidateService candidateService,
            IDataActivityLogService dataActivityLogService)
            : base(dependencies)
        {
            _dataActivityLogService = dataActivityLogService;
            _candidateService = candidateService;
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<CandidateContactRecord, CandidateContactRecordDto, IRecruitmentDbScope> eventArgs)
        {
            eventArgs.InputEntity.RelevantJobOpenings = EntityMergingService.CreateEntitiesFromIds<JobOpening, IRecruitmentDbScope>(eventArgs.UnitOfWork, eventArgs.InputEntity.RelevantJobOpenings.GetIds());

            UpdateCandidateWhereApplicable(eventArgs.InputDto, eventArgs.UnitOfWork);
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<CandidateContactRecord, CandidateContactRecordDto, IRecruitmentDbScope> eventArgs)
        {
            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.RelevantJobOpenings, eventArgs.InputEntity.RelevantJobOpenings);
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<CandidateContactRecord, CandidateContactRecordDto> recordAddedEventArgs)
        {
            _dataActivityLogService.LogCandidateDataActivity(recordAddedEventArgs.InputEntity.CandidateId, ActivityType.CreatedRecord, ReferenceType.CandidateContact, recordAddedEventArgs.InputEntity.Id);

            _candidateService.UpdateLastActivity(recordAddedEventArgs.InputEntity.CandidateId, ReferenceType.CandidateContact, recordAddedEventArgs.InputEntity.Id);
        }

        protected override void OnRecordsDeleted(RecordDeletedEventArgs<CandidateContactRecord> eventArgs)
        {
            foreach (var contact in eventArgs.DeletedEntities)
            {
                _dataActivityLogService.LogCandidateDataActivity(contact.CandidateId, ActivityType.RemovedRecord, ReferenceType.CandidateContact, contact.Id);
            }
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<CandidateContactRecord, CandidateContactRecordDto> recordEditedEventArgs)
        {
            _dataActivityLogService.LogCandidateDataActivity(recordEditedEventArgs.InputEntity.CandidateId, ActivityType.UpdatedRecord, ReferenceType.CandidateContact, recordEditedEventArgs.InputEntity.Id);
        }

        protected override IEnumerable<Expression<Func<CandidateContactRecord, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return r => r.Candidate.FirstName;
            yield return r => r.Candidate.LastName;
            yield return r => r.Candidate.EmailAddress;
            yield return r => r.Candidate.OtherEmailAddress;
            yield return r => r.Candidate.MobilePhone;
            yield return r => r.Candidate.OtherPhone;

            yield return r => r.Comment;
        }

        protected override NamedFilters<CandidateContactRecord> NamedFilters
        {
            get
            {
                var currentEmployeeId = PrincipalProvider.Current.EmployeeId;
                var currentUserId = PrincipalProvider.Current.Id ?? BusinessLogicHelper.NonExistingId;

                return new NamedFilters<CandidateContactRecord>
                (
                    new[]
                    {
                        new NamedFilter<CandidateContactRecord>(FilterCodes.MyRecords,
                            CandidateContactRecordBusinessLogic.IsOwnCandidateContactRecord.Parametrize(currentUserId, currentEmployeeId),
                            SecurityRoleType.CanFilterCandidateContactRecordByMyRecords),
                        new NamedFilter<CandidateContactRecord>(FilterCodes.AnyOwner, r => true, SecurityRoleType.CanFilterCandidateContactRecordByAnyOwner)
                    }
                    .Union(CardIndexServiceHelper.BuildSoftDeletableFilters<CandidateContactRecord>())
                );
            }
        }

        public override IQueryable<CandidateContactRecord> ApplyContextFiltering(IQueryable<CandidateContactRecord> records, object context)
        {
            if (context is ParentIdContext parentIdContext)
            {
                return records.Where(c => c.CandidateId == parentIdContext.ParentId);
            }

            return base.ApplyContextFiltering(records, context);
        }

        public override CandidateContactRecordDto GetDefaultNewRecord()
        {
            var now = TimeService.GetCurrentTime();
            var record = base.GetDefaultNewRecord();

            record.ContactedByEmployeeId = PrincipalProvider.Current.EmployeeId;
            record.ContactedOn = now;

            return record;
        }

        private void UpdateCandidateWhereApplicable(CandidateContactRecordDto inputDto, IUnitOfWork<IRecruitmentDbScope> unitOfWork)
        {
            var candidate = unitOfWork.Repositories.Candidates.GetById(inputDto.CandidateId);

            if (inputDto.CandidateReaction == CandidateReaction.ContactLater)
            {
                candidate.NextFollowUpDate = inputDto.NextContactdOn;
                candidate.ContactCoordinatorId = inputDto.ContactedByEmployeeId;
            }
        }
    }
}
