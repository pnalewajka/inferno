﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Consts;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Business.Recruitment.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class JobApplicationCardIndexDataService : CardIndexDataService<JobApplicationDto, JobApplication, IRecruitmentDbScope>, IJobApplicationCardIndexDataService
    {
        private readonly IDataActivityLogService _dataActivityLogService;
        private readonly IApplicationOriginService _applicationOriginService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public JobApplicationCardIndexDataService(
            IDataActivityLogService dataActivityLogService,
            ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies,
            IApplicationOriginService applicationOriginService)
            : base(dependencies)
        {
            _dataActivityLogService = dataActivityLogService;
            _applicationOriginService = applicationOriginService;
        }

        public override IQueryable<JobApplication> ApplyContextFiltering(IQueryable<JobApplication> records, object context)
        {
            if (context is ParentIdContext parentIdContext)
            {
                return records.Where(c => c.CandidateId == parentIdContext.ParentId);
            }

            return base.ApplyContextFiltering(records, context);
        }

        public JobApplicationDto GetDefaultNewRecord(ApplicationOriginType applicationOriginType)
        {
            var defaultRecord = base.GetDefaultNewRecord();

            defaultRecord.OriginType = applicationOriginType;

            defaultRecord.ApplicationOriginId = SetDefaultApplicationOrgin(applicationOriginType);

            return defaultRecord;
        }

        public ApplicationOriginType GetOriginType(long applicationOriginId)
        {
            using (var service = UnitOfWorkService.Create())
            {
                var applicationOrigin = service.Repositories.ApplicationOrigins.GetById(applicationOriginId);

                return applicationOrigin.OriginType;
            }
        }

        private long SetDefaultApplicationOrgin(ApplicationOriginType applicationOriginType)
        {
            if (applicationOriginType == ApplicationOriginType.Recommendation
                || applicationOriginType == ApplicationOriginType.Event
                || applicationOriginType == ApplicationOriginType.DropOff
                || applicationOriginType == ApplicationOriginType.Other
                || applicationOriginType == ApplicationOriginType.Researching
                || applicationOriginType == ApplicationOriginType.DirectSearch)
            {
                return _applicationOriginService.GetApplicationOriginId(applicationOriginType);
            }

            return 0;
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<JobApplication, JobApplicationDto> recordEditedEventArgs)
        {
            _dataActivityLogService.LogCandidateDataActivity(recordEditedEventArgs.DbEntity.CandidateId, ActivityType.UpdatedRecord,
                ReferenceType.JobApplication, recordEditedEventArgs.DbEntity.Id);

            base.OnRecordEdited(recordEditedEventArgs);
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<JobApplication, JobApplicationDto> recordAddedEventArgs)
        {
            _dataActivityLogService.LogCandidateDataActivity(recordAddedEventArgs.InputEntity.CandidateId, ActivityType.CreatedRecord,
                ReferenceType.JobApplication, recordAddedEventArgs.InputEntity.Id);

            base.OnRecordAdded(recordAddedEventArgs);
        }

        protected override void OnRecordsDeleted(RecordDeletedEventArgs<JobApplication> eventArgs)
        {
            foreach (var entitie in eventArgs.DeletedEntities)
            {
                _dataActivityLogService.LogCandidateDataActivity(entitie.CandidateId, ActivityType.RemovedRecord, ReferenceType.JobApplication, entitie.Id);
            }

            base.OnRecordsDeleted(eventArgs);
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<JobApplication, JobApplicationDto, IRecruitmentDbScope> eventArgs)
        {
            var candidate = eventArgs.UnitOfWork.Repositories.Candidates.GetById(eventArgs.InputEntity.CandidateId);

            if (candidate.CrmId == null && !CandidateBusinessLogic.HasValidConsent.Call(candidate, TimeService.GetCurrentDate()))
            {
                throw new BusinessException(CandidateResources.CandidateInvalidDataConsent);
            }

            eventArgs.InputEntity.JobProfiles = EntityMergingService.CreateEntitiesFromIds<JobProfile, IRecruitmentDbScope>(eventArgs.UnitOfWork, eventArgs.InputEntity.JobProfiles.GetIds());
            eventArgs.InputEntity.Cities = EntityMergingService.CreateEntitiesFromIds<City, IRecruitmentDbScope>(eventArgs.UnitOfWork, eventArgs.InputEntity.Cities.GetIds());
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<JobApplication, JobApplicationDto, IRecruitmentDbScope> eventArgs)
        {
            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.JobProfiles, eventArgs.InputEntity.JobProfiles);
            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.Cities, eventArgs.InputEntity.Cities);
        }

        protected override NamedFilters<JobApplication> NamedFilters
        {
            get
            {
                return new NamedFilters<JobApplication>
                (
                    new[]
                    {
                        new NamedFilter<JobApplication>(FilterCodes.MyRecords, r => r.CreatedById == PrincipalProvider.Current.Id, SecurityRoleType.CanFilterJobApplicationRecordByMyRecords),
                        new NamedFilter<JobApplication>(FilterCodes.AnyOwner, r => true, SecurityRoleType.CanFilterJobApplicationRecordByAnyOwner)
                    }
                    .Union(CardIndexServiceHelper.BuildSoftDeletableFilters<JobApplication>())
                );
            }
        }

        protected override IEnumerable<Expression<Func<JobApplication, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return j => j.Candidate.FirstName;
            yield return j => j.Candidate.LastName;
            yield return j => j.Candidate.EmailAddress;
            yield return j => j.Candidate.MobilePhone;
            yield return j => j.Candidate.SkypeLogin;
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<JobApplication> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var direction = sortingCriterion.GetSortingDirection();

            if (sortingCriterion.GetSortingColumnName() == nameof(JobApplicationDto.CityIds))
            {
                orderedQueryBuilder.ApplySortingKey(j => j.Cities.Select(c => c.Name).FirstOrDefault(), direction);
            }
            else if (sortingCriterion.GetSortingColumnName() == nameof(JobApplicationDto.JobProfileIds))
            {
                orderedQueryBuilder.ApplySortingKey(j => j.JobProfiles.Select(p => p.Name).FirstOrDefault(), direction);
            }
            else
            {
                base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
            }
        }
    }
}