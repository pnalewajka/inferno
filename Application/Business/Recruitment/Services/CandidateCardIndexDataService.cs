﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Castle.Core.Internal;
using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Consts;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.Business.Recruitment.Enums;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Business.Recruitment.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class CandidateCardIndexDataService : CardIndexDataService<CandidateDto, Candidate, IRecruitmentDbScope>, ICandidateCardIndexDataService
    {
        private readonly ICandidateService _candidateService;
        private readonly IClassMappingFactory _classMappingFactory;
        private readonly IDataActivityLogService _dataActivityLogService;
        private readonly ICandidateNotificationService _candidateNotificationService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public CandidateCardIndexDataService(
            ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies,
            IDataActivityLogService dataActivityLogService,
            ICandidateService candidateService,
            ICandidateNotificationService candidateNotificationService)
            : base(dependencies)
        {
            _dataActivityLogService = dataActivityLogService;
            _candidateService = candidateService;
            _candidateNotificationService = candidateNotificationService;
            _classMappingFactory = dependencies.ClassMappingFactory;
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<Candidate, CandidateDto, IRecruitmentDbScope> eventArgs)
        {
            eventArgs.InputEntity.JobProfiles = EntityMergingService.CreateEntitiesFromIds<JobProfile, IRecruitmentDbScope>(eventArgs.UnitOfWork, eventArgs.InputEntity.JobProfiles.GetIds());
            eventArgs.InputEntity.Cities = EntityMergingService.CreateEntitiesFromIds<City, IRecruitmentDbScope>(eventArgs.UnitOfWork, eventArgs.InputEntity.Cities.GetIds());
            eventArgs.InputEntity.Watchers = EntityMergingService.CreateEntitiesFromIds<Employee, IRecruitmentDbScope>(eventArgs.UnitOfWork, eventArgs.InputEntity.Watchers.GetIds());

            var languageValidationResult = ValidateLanguages(eventArgs.UnitOfWork, eventArgs.InputDto.Languages);

            if (!languageValidationResult.IsSuccessful)
            {
                eventArgs.Alerts.AddRange(languageValidationResult.Alerts);
            }

            base.OnRecordAdding(eventArgs);
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<Candidate, IRecruitmentDbScope> eventArgs)
        {
            eventArgs.DeletingRecord.DeletedOn = TimeService.GetCurrentTime();
            eventArgs.UnitOfWork.Repositories.DataOwners.Where(d => d.CandidateId == eventArgs.DeletingRecord.Id).ForEach(d => d.CandidateId = null);

            base.OnRecordDeleting(eventArgs);
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<Candidate, CandidateDto, IRecruitmentDbScope> eventArgs)
        {

            var dbEntity = eventArgs.DbEntity;

            eventArgs.InputEntity.DeletedOn = dbEntity.DeletedOn;

            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, dbEntity.JobProfiles, eventArgs.InputEntity.JobProfiles);
            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, dbEntity.Cities, eventArgs.InputEntity.Cities);
            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, dbEntity.Watchers, eventArgs.InputEntity.Watchers);

            var languageValidationResult = ValidateLanguages(eventArgs.UnitOfWork, eventArgs.InputDto.Languages);

            if (!languageValidationResult.IsSuccessful)
            {
                eventArgs.Alerts.AddRange(languageValidationResult.Alerts);
            }

            MergeLanguages(eventArgs);

            base.OnRecordEditing(eventArgs);
        }

        private void MergeLanguages(RecordEditingEventArgs<Candidate, CandidateDto, IRecruitmentDbScope> eventArgs)
        {
            var languagesToRemove = eventArgs.DbEntity.Languages.Where(d => !eventArgs.InputEntity.Languages.Any(l => l.LanguageId == d.LanguageId && l.Level == d.Level));
            var languagesToAdd = eventArgs.InputEntity.Languages.Where(d => !eventArgs.DbEntity.Languages.Any(l => l.LanguageId == d.LanguageId && l.Level == d.Level));

            eventArgs.UnitOfWork.Repositories.CandidateLanguages.DeleteRange(languagesToRemove, true);
            eventArgs.UnitOfWork.Repositories.CandidateLanguages.AddRange(languagesToAdd);
        }

        private BoolResult ValidateLanguages(IUnitOfWork<IRecruitmentDbScope> unitOfWork, List<CandidateLanguageDto> languagesWithLevel)
        {
            var duplicatedLanguages = languagesWithLevel.GroupBy(l => l.LanguageId)
                .Where(l => l.Count() > 1)
                .Select(l => l.Key);

            if (!duplicatedLanguages.Any())
            {
                return BoolResult.Success;
            }

            var invalidLanguages = unitOfWork.Repositories.Language.Where(s => duplicatedLanguages.Contains(s.Id))
                .Select(s => new LocalizedString { English = s.NameEn, Polish = s.NamePl });

            var invalidLanguagesName = string.Join(", ", invalidLanguages);

            return new BoolResult(false, new[] {
                new AlertDto {
                    Message = string.Format(CandidateResources.InvalidLanguagesSelected, invalidLanguagesName),
                    Type = AlertType.Error
                }
            });
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<Candidate, CandidateDto> recordAddedEventArgs)
        {
            _dataActivityLogService.LogCandidateDataActivity(recordAddedEventArgs.InputEntity.Id, ActivityType.CreatedRecord);

            if (recordAddedEventArgs.InputDto.WorkflowAction.HasValue)
            {
                HandleWorkflowAction(recordAddedEventArgs.InputDto.WorkflowAction.Value, recordAddedEventArgs.InputEntity);
            }

            if (recordAddedEventArgs.InputDto.ShouldAddRelatedDataConsent && recordAddedEventArgs.InputDto.ConsentType.HasValue)
            {
                _candidateService.AddDataConsent(
                    recordAddedEventArgs.InputEntity.Id,
                    recordAddedEventArgs.InputDto.ConsentType.Value,
                    recordAddedEventArgs.InputDto.ConsentScope,
                    recordAddedEventArgs.InputDto.Documents);
            }
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<Candidate, CandidateDto> recordEditedEventArgs)
        {
            _dataActivityLogService.LogCandidateDataActivity(recordEditedEventArgs.InputEntity.Id, ActivityType.UpdatedRecord);

            if (recordEditedEventArgs.InputDto.WorkflowAction.HasValue)
            {
                HandleWorkflowAction(recordEditedEventArgs.InputDto.WorkflowAction.Value, recordEditedEventArgs.InputEntity);
            }

            var entity = recordEditedEventArgs.DbEntity;

            _candidateNotificationService.NotifyAboutRecordChanges(entity.Id, recordEditedEventArgs.OriginalDto);
            _candidateService.UpdateLastActivity(entity.Id, ReferenceType.Candidate, entity.Id);
        }

        protected override void OnRecordsDeleted(RecordDeletedEventArgs<Candidate> eventArgs)
        {
            foreach (var candidate in eventArgs.DeletedEntities)
            {
                _dataActivityLogService.LogCandidateDataActivity(candidate.Id, ActivityType.RemovedRecord);
            }
        }

        protected override IEnumerable<Expression<Func<Candidate, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            if (searchCriteria.Includes(SearchAreaCodes.Name))
            {
                yield return c => c.FirstName;
                yield return c => c.LastName;
            }

            if (searchCriteria.Includes(SearchAreaCodes.Email))
            {
                yield return c => c.EmailAddress;
                yield return c => c.OtherEmailAddress;
            }

            if (searchCriteria.Includes(SearchAreaCodes.Phone))
            {
                yield return c => c.MobilePhone;
                yield return c => c.OtherPhone;
            }

            if (searchCriteria.Includes(SearchAreaCodes.JobProfiles))
            {
                yield return c => c.JobProfiles.Select(t => t.Name);
            }

            if (searchCriteria.Includes(SearchAreaCodes.Cities))
            {
                yield return c => c.Cities.Select(t => t.Name);
            }

            if (searchCriteria.Includes(SearchAreaCodes.Resume))
            {
                yield return c => c.ResumeTextContent;
            }
        }

        protected override NamedFilters<Candidate> NamedFilters => new NamedFilters<Candidate>(new[]
                {
                     new NamedFilter<Candidate>(
                        FilterCodes.CandidateAllCandidates,
                        a => true),
                    new NamedFilter<Candidate>(
                        FilterCodes.CandidateMyCandidates,
                        a => a.ConsentCoordinatorId == PrincipalProvider.Current.EmployeeId || a.ContactCoordinatorId == PrincipalProvider.Current.EmployeeId),
                    new NamedFilter<Candidate>(
                        FilterCodes.CandidateMyProcessCandidates,
                        a => a.RecruitmentProcesses.Any(p => p.RecruiterId == PrincipalProvider.Current.EmployeeId)
                        && a.RecruitmentProcesses.Any(p => p.Status != RecruitmentProcessStatus.Closed)),
                    new NamedFilter<Candidate>(
                        FilterCodes.CandidateProcessedByMeCandidates,
                        a => a.LastActivityById == PrincipalProvider.Current.EmployeeId),
                    new NamedDtoFilter<Candidate, CandidateAdvancedFiltersDto>
                        (FilterCodes.CandidatePropertiesFilters, CandidateBusinessLogic.CandidatePropertiesFiltering.BaseExpression),
                    new NamedFilter<Candidate>(
                        FilterCodes.CandidateMyFavorites,
                        c => c.Favorites.Any(r => r.Id == PrincipalProvider.Current.EmployeeId)),
                }
                .Union(CardIndexServiceHelper.BuildSoftDeletableFilters<Candidate>()));

        public override CandidateDto GetRecordById(long id)
        {
            var candidateFileToCandidateFileDetailsDtoMapping = _classMappingFactory.CreateMapping<CandidateFile, CandidateFileDetailsDto>();
            var dataConsentDocumentToCandidateFileDetailsDtoMapping = _classMappingFactory.CreateMapping<DataConsentDocument, CandidateFileDetailsDto>();
            var recruitmentProcessStepToRecruitmentProcessStepInfoDtoMapping = _classMappingFactory.CreateMapping<RecruitmentProcessStep, RecruitmentProcessStepInfoDto>();
            var candidateNoteMapping = _classMappingFactory.CreateMapping<CandidateNote, CandidateNoteDto>();
            var dataConsentMapping = _classMappingFactory.CreateMapping<DataConsent, CandidateDataConsentDto>();

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var candidate = unitOfWork.Repositories.Candidates
                    .Where(c => c.Id == id)
                    .Filtered()
                    .Include(nameof(Candidate.CandidateNotes))
                    .Include(nameof(Candidate.CandidateContactRecords))
                    .Include(nameof(Candidate.JobApplications))
                    .Include(nameof(Candidate.CandidateFiles))
                    .Include(nameof(Candidate.RecruitmentProcesses))
                    .Include(c => c.RecruitmentProcesses.Select(p => p.TechnicalReviews))
                    .Include(c => c.RecruitmentProcesses.Select(p => p.ProcessSteps))
                    .Single();

                var dto = _classMappingFactory.CreateMapping<Candidate, CandidateDto>().CreateFromSource(candidate);

                var allowedProcessIds = unitOfWork.Repositories
                    .RecruitmentProcesses.Where(p => p.CandidateId == id)
                    .Filtered().GetIds().ToHashSet();

                var allowedStepIds = unitOfWork.Repositories.RecruitmentProcesses
                    .Where(p => p.CandidateId == id)
                    .SelectMany(p => p.ProcessSteps)
                    .Filtered().GetIds().ToHashSet();

                dto.ContactRecordDetails = candidate.CandidateContactRecords
                    .Filtered()
                    .Select(r => new CandidateContactRecordDetailsDto
                    {
                        Id = r.Id,
                        Comment = r.Comment,
                        ContactedOn = r.ContactedOn,
                        CreatedByFullName = r.CreatedBy != null ? UserBusinessLogic.FullName.Call(r.CreatedBy) : null
                    }).ToList();

                dto.JobApplicationDetails = candidate.JobApplications
                    .Filtered()
                    .Select(a => new CandidateJobApplicationDetailsDto
                    {
                        Id = a.Id,
                        CandidateComment = a.CandidateComment,
                        OriginName = a.ApplicationOrigin.Name,
                        OriginComment = a.OriginComment,
                        CreatedOn = a.CreatedOn,
                        CreatedByFullName = a.CreatedBy != null ? UserBusinessLogic.FullName.Call(a.CreatedBy) : null
                    }).ToList();

                dto.FileDetails = candidate.CandidateFiles
                    .Filtered()
                    .Select(candidateFileToCandidateFileDetailsDtoMapping.CreateFromSource)
                    .Concat(
                        unitOfWork.Repositories.DataConsentDocument
                            .Where(d => d.DataConsent.DataOwner.CandidateId == id)
                            .Select(dataConsentDocumentToCandidateFileDetailsDtoMapping.CreateFromSource))
                    .OrderByDescending(f => f.ModifiedOn)
                    .ToList();

                dto.TechnicalReviewDetails = candidate.RecruitmentProcesses
                    .Where(p => allowedProcessIds.Contains(p.Id))
                    .SelectMany(p => p.TechnicalReviews
                        .Select(r => new CandidateTechnicalReviewDetailsDto
                        {
                            Id = r.Id,
                            SeniorityLevelAssessment = r.SeniorityLevelAssessment,
                            JobProfileNames = r.JobProfiles.Select(j => j.Name).ToList(),
                            TechnicalKnowledgeAssessment = r.TechnicalKnowledgeAssessment,
                            TeamProjectSuitabilityAssessment = r.TeamProjectSuitabilityAssessment,
                            EnglishUsageAssessment = r.EnglishUsageAssessment,
                            RiskAssessment = r.RiskAssessment,
                            StrengthAssessment = r.StrengthAssessment,
                            OtherRemarks = r.OtherRemarks,
                            Decision = r.RecruitmentProcessStep.Decision,
                            TechnicalAssignmentAssessment = r.TechnicalAssignmentAssessment,
                            CreatedOn = r.CreatedOn,
                            CreatedByFullName = r.CreatedBy != null ? UserBusinessLogic.FullName.Call(r.CreatedBy) : null
                        })).ToList();

                dto.Notes = candidate.CandidateNotes
                    .Filtered()
                    .OrderByDescending(n => n.CreatedOn)
                    .Select(candidateNoteMapping.CreateFromSource)
                    .ToList();

                dto.Consents = unitOfWork.Repositories.DataConsents.Filtered()
                   .Where(c => c.DataOwner.CandidateId == id)
                   .Select(dataConsentMapping.CreateFromSource)
                   .ToList();

                var steps = candidate.RecruitmentProcesses.SelectMany(rp => rp.ProcessSteps)
                    .Where(s => allowedStepIds.Contains(s.Id))
                    .ToList();

                dto.RecruitmentProcessStepDetails = steps.Where(s => s.Type != RecruitmentProcessStepType.TechnicalReview)
                    .Select(r => new CandidateRecruitmentProcessStepDetailsDto
                    {
                        Id = r.Id,
                        Decision = r.Decision,
                        Type = r.Type,
                        Comment = r.DecisionComment,
                        CreatedOn = r.CreatedOn,
                        CreatedByFullName = r.CreatedBy != null ? UserBusinessLogic.FullName.Call(r.CreatedBy) : null
                    }).ToList();

                dto.TechnicalReviewStepInfo = steps.Where(s => s.Type == RecruitmentProcessStepType.TechnicalReview)
                    .Select(recruitmentProcessStepToRecruitmentProcessStepInfoDtoMapping.CreateFromSource)
                    .ToList();

                return dto;
            }
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<Candidate> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var direction = sortingCriterion.GetSortingDirection();

            if (sortingCriterion.GetSortingColumnName() == nameof(CandidateDto.FullName))
            {
                orderedQueryBuilder.ApplySortingKey(CandidateBusinessLogic.FullName.AsExpression(), direction);
            }
            else if (sortingCriterion.GetSortingColumnName() == nameof(CandidateDto.RecentActivity))
            {
                orderedQueryBuilder.ApplySortingKey(CandidateBusinessLogic.RecentActivityOn.AsExpression(), direction);
            }
            else
            {
                base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
            }
        }

        public override CandidateDto GetDefaultNewRecord()
        {
            var record = base.GetDefaultNewRecord();
            var employeeId = PrincipalProvider.Current.EmployeeId;

            record.ConsentCoordinatorId = employeeId;
            record.ContactCoordinatorId = employeeId;

            return record;
        }

        private void HandleWorkflowAction(CandidateWorkflowAction action, Candidate dbEntity)
        {
            switch (action)
            {
                case CandidateWorkflowAction.Watch:
                    _candidateService.ToggleWatching(new List<long> { dbEntity.Id }, PrincipalProvider.Current.EmployeeId, WatchingStatus.Watch);
                    return;

                case CandidateWorkflowAction.Unwatch:
                    _candidateService.ToggleWatching(new List<long> { dbEntity.Id }, PrincipalProvider.Current.EmployeeId, WatchingStatus.Unwatch);
                    return;
            }
        }
    }
}
