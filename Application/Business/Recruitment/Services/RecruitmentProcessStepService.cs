﻿using System;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Dto.RecruitmentProcessStepAction;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class RecruitmentProcessStepService : IRecruitmentProcessStepService
    {
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IClassMappingFactory _classMappingFactory;
        private readonly IPrincipalProvider _principalProvider;
        private readonly ICalendarService _calendarService;
        private readonly ITimeService _timeService;
        private readonly IRecruitmentProcessHistoryService _recruitmentProcessHistoryService;

        public RecruitmentProcessStepService(
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService,
            ISystemParameterService systemParameterService,
            IClassMappingFactory classMappingFactory,
            IPrincipalProvider principalProvider,
            ICalendarService calendarService,
            ITimeService timeService,
            IRecruitmentProcessHistoryService recruitmentProcessHistoryService)
        {
            _unitOfWorkService = unitOfWorkService;
            _systemParameterService = systemParameterService;
            _classMappingFactory = classMappingFactory;
            _principalProvider = principalProvider;
            _calendarService = calendarService;
            _timeService = timeService;
            _recruitmentProcessHistoryService = recruitmentProcessHistoryService;
        }

        public RecruitmentProcessStep CreateInitialStep(
            RecruitmentProcessStepType recruitmentProcessStepType,
            long recruiterId,
            long jobOpeningDecisionMakerId)
        {
            switch (recruitmentProcessStepType)
            {
                case RecruitmentProcessStepType.TechnicalReview:
                    return CreateRecruitmentProcessStep(recruitmentProcessStepType);

                case RecruitmentProcessStepType.HiringManagerInterview:
                    return CreateRecruitmentProcessStep(recruitmentProcessStepType, jobOpeningDecisionMakerId);

                case RecruitmentProcessStepType.HrCall:
                case RecruitmentProcessStepType.HrInterview:
                    return CreateRecruitmentProcessStep(recruitmentProcessStepType, recruiterId);

                default:
                    throw new NotImplementedException($"Creating step {nameof(recruitmentProcessStepType)} is not implemented via initial steps");
            }
        }

        private RecruitmentProcessStep CreateRecruitmentProcessStep(RecruitmentProcessStepType type, long? assignedEmployeeId = null)
        {
            return new RecruitmentProcessStep
            {
                Type = type,
                AssignedEmployeeId = assignedEmployeeId,
                Status = assignedEmployeeId == null ? RecruitmentProcessStepStatus.Draft : RecruitmentProcessStepStatus.ToDo
            };
        }

        public RecruitmentProcessStepDto GetDefaultNewRecord(RecruitmentProcessStepType type, long? recruitmentProcessId, RecruitmentProcessStepDto newRecord = null)
        {
            if (newRecord == null)
            {
                newRecord = new RecruitmentProcessStepDto();
            }

            newRecord.Type = type;
            newRecord.Status = RecruitmentProcessStepStatus.Draft;

            if (!recruitmentProcessId.HasValue)
            {
                return newRecord;
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                newRecord.RecruitmentProcessId = recruitmentProcessId.Value;

                var recruitmentProcess = unitOfWork.Repositories.RecruitmentProcesses.GetById(recruitmentProcessId.Value);

                switch (type)
                {
                    case RecruitmentProcessStepType.HrInterview:
                    case RecruitmentProcessStepType.HrCall:
                        newRecord.AssignedEmployeeId = recruitmentProcess.RecruiterId;
                        break;

                    case RecruitmentProcessStepType.ContractNegotiations:
                        newRecord.AssignedEmployeeId = recruitmentProcess.RecruiterId;
                        newRecord.StepDetails = new ContractNegotiationsDetailsDto
                        {
                            FinalEmploymentSourceId = recruitmentProcess.JobApplication?.ApplicationOriginId
                        };
                        break;

                    case RecruitmentProcessStepType.HiringManagerResumeApproval:
                        newRecord.AssignedEmployeeId = recruitmentProcess.JobOpening.DecisionMakerEmployeeId;
                        break;

                    case RecruitmentProcessStepType.ClientResumeApproval:
                        newRecord.AssignedEmployeeId = recruitmentProcess.JobOpening.DecisionMakerEmployeeId;

                        newRecord.StepDetails = new ClientResumeApprovalDto
                        {
                            ResumeShownToClientOn = _timeService.GetCurrentTime()
                        };
                        break;

                    case RecruitmentProcessStepType.HiringDecision:
                        newRecord.AssignedEmployeeId = recruitmentProcess.JobOpening.DecisionMakerEmployeeId;
                        break;

                    case RecruitmentProcessStepType.HiringManagerInterview:
                    case RecruitmentProcessStepType.ClientInterview:
                        newRecord.AssignedEmployeeId = recruitmentProcess.JobOpening.DecisionMakerEmployeeId;
                        break;

                    case RecruitmentProcessStepType.TechnicalReview:
                        newRecord.StepDetails = new TechnicalReviewStepDetailsDto
                        {
                            JobProfileIds = recruitmentProcess.JobOpening?.JobProfiles.Select(p => p.Id).ToList()
                        };
                        break;
                }

                return newRecord;
            }
        }

        public RecruitmentProcessStepWorkflowAction[] GetWorkflowActions(RecruitmentProcessStepActionContext context)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recruitmentProcessRulesetProvider = new RecruitmentProcessWorkflowRulesetProvider();
                var currentStep = unitOfWork.Repositories.RecruitmentProcessSteps.GetById(context.RecruitmentProcessStepId);
                var recruitmentProcess = currentStep.RecruitmentProcess;
                var employee = unitOfWork.Repositories.Employees.GetById(context.EmployeeId);

                if (IsNextStepAvaliable(currentStep, recruitmentProcess))
                {
                    return new RecruitmentProcessStepWorkflowAction[0];
                }

                var ruleset = recruitmentProcessRulesetProvider.Ruleset.Single(rs => rs.StepType == currentStep.Type);

                var actions = new RecruitmentProcessStepRule[] { };

                if (IsAssignedEmployee(context.EmployeeId, currentStep))
                {
                    actions = actions.Concat(ruleset.AssigneeActions).ToArray();
                }
                else if (IsRecruiter(employee, recruitmentProcess))
                {
                    actions = actions.Concat(ruleset.RecruiterActions).ToArray();
                }

                foreach (var action in actions)
                {
                    action.EvaluateConditions(recruitmentProcess);
                }

                var availableWorkflowActions = actions.Where(br =>
                {
                    if (br.AddedStepType == null)
                    {
                        return true;
                    }

                    var existingStepTypes = recruitmentProcess.ProcessSteps
                        .Where(s => RecruitmentProcessStepBusinessLogic.IsExistingToDoOrDone.Call(s))
                        .Select(ps => ps.Type);

                    return !existingStepTypes.Contains(br.AddedStepType.Value) || br.CanAddStepIfAlreadyExists;
                });

                var canSeeApproveButtonRegardless = _principalProvider.Current.IsInRole(SecurityRoleType.CanAddRecruitmentStepViaDropdown);

                return availableWorkflowActions
                    .Where(rps =>
                        (rps.WorkflowAction != RecruitmentProcessStepWorkflowActionType.ApproveCandidate)
                        || (rps.WorkflowAction == RecruitmentProcessStepWorkflowActionType.ApproveCandidate
                            && (canSeeApproveButtonRegardless || !availableWorkflowActions.Any(r => r.AddedStepType != null))))
                  .Select(r => new RecruitmentProcessStepWorkflowAction
                  {
                      Rule = r,
                      StepType = currentStep.Type,
                      SameStepCount = recruitmentProcess.ProcessSteps.Count(e => e.Type == r.AddedStepType && !e.IsDeleted)
                  })
                  .OrderBy(a => a.Rule.WorkflowAction)
                  .ToArray();
            }
        }

        public void AddNoteToRecruitmentProcess(AddNoteDto note)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recruitmentProcessStep = unitOfWork.Repositories.RecruitmentProcessSteps.Filtered().GetById(note.ParentId);

                var recruitmentProcessNote = new RecruitmentProcessNote
                {
                    RecruitmentProcessId = recruitmentProcessStep.RecruitmentProcessId,
                    RecruitmentProcessStepId = recruitmentProcessStep.Id,
                    Content = note.Content,
                    NoteVisibility = note.NoteVisibility,
                    NoteType = NoteType.UserNote
                };

                unitOfWork.Repositories.RecruitmentProcessNotes.Add(recruitmentProcessNote);
                unitOfWork.Commit();

                _recruitmentProcessHistoryService.LogActivity(recruitmentProcessStep.RecruitmentProcessId,
                    RecruitmentProcessHistoryEntryType.AddedNote, recruitmentProcessStep.Id);
            }
        }

        public long GetCandidateId(long recruitmentProcessStepId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.RecruitmentProcessSteps
                    .SelectById(recruitmentProcessStepId, s => s.RecruitmentProcess.CandidateId);
            }
        }

        public void RefreshStepModification(long stepId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var step = unitOfWork.Repositories.RecruitmentProcessSteps.GetById(stepId);

                unitOfWork.Repositories.RecruitmentProcessSteps.Edit(step);

                unitOfWork.Commit();
            }
        }

        private bool IsAssignedEmployee(long currentEmployeeId, RecruitmentProcessStep step)
        {
            return currentEmployeeId == step.AssignedEmployeeId
                   || step.OtherAttendingEmployees.Any(e => e.Id == currentEmployeeId);
        }

        private bool IsRecruiter(Employee currentEmployee, RecruitmentProcess process)
        {
            var managerAdGroup = _systemParameterService.GetParameter<string>(ParameterKeys.RecruitmentManagerAdGroup);

            return process.RecruiterId == currentEmployee.Id || EmployeeBusinessLogic.HasProfile.Call(currentEmployee, managerAdGroup);
        }

        private bool IsNextStepAvaliable(RecruitmentProcessStep currentStep, RecruitmentProcess recruitmentProcess)
        {
            return currentStep.Status != RecruitmentProcessStepStatus.ToDo || recruitmentProcess.Status != RecruitmentProcessStatus.Active;
        }
    }
}
