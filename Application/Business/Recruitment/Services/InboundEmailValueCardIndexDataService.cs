﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class InboundEmailValueCardIndexDataService : CardIndexDataService<InboundEmailValueDto, InboundEmailValue, IRecruitmentDbScope>, IInboundEmailValueCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public InboundEmailValueCardIndexDataService(ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies)
            : base(dependencies)
        {
        }
    }
}
