﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class ConsentTypeItemProvider : EnumBasedListItemProvider<DataConsentType>
    {
        private IPrincipalProvider _principalProvider;

        public ConsentTypeItemProvider(IPrincipalProvider principalProvider)
            : base(principalProvider)
        {
            _principalProvider = principalProvider;
        }

        public override IEnumerable<ListItem> GetItems()
        {
            var result = base.GetItems();

            return _principalProvider.Current.IsInRole(SecurityRoleType.CanAddRecommendingPersonConsents)
                ? result 
                : result.Where(i => DataConsentBusinessLogic.IsCandidateConsentType.Call((DataConsentType)i.Id.Value));
        }
    }
}
