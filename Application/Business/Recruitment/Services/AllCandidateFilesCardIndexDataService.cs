﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class AllCandidateFilesCardIndexDataService : CandidateFileCardIndexDataService
    {
        private ICandidateDataConsentFileCardIndexDataService _candidateDataConsentFileCardIndexDataService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public AllCandidateFilesCardIndexDataService(
            ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies,
            IDataActivityLogService dataActivityLogService,
            IUploadedDocumentHandlingService uploadedDocumentHandlingService,
            ICandidateService candidateService,
            ICandidateDataConsentFileCardIndexDataService candidateDataConsentFileCardIndexDataService)
            : base(dependencies, dataActivityLogService, uploadedDocumentHandlingService, candidateService)
        {
            _candidateDataConsentFileCardIndexDataService = candidateDataConsentFileCardIndexDataService;
        }

        public override CardIndexRecords<CandidateFileDto> GetRecords([CanBeNull] QueryCriteria criteria)
        {
            var records = base.GetRecords(criteria);

            var consentFiles = _candidateDataConsentFileCardIndexDataService.GetRecords(GetQueryCriteriaForConesnts(criteria));

            records.Rows.AddRange(consentFiles.Rows);

            return records;
        }

        private QueryCriteria GetQueryCriteriaForConesnts(QueryCriteria criteria)
        {
            criteria.Filters = new List<FilterCodeGroup>();
            criteria.FilterObjects = new Dictionary<string, IFilteringObject>();

            return criteria;
        }
    }
}
