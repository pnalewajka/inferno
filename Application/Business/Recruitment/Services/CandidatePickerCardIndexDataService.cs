﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    class CandidatePickerCardIndexDataService : CandidateCardIndexDataService,
        ICandidatePickerCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public CandidatePickerCardIndexDataService(ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies,
            IDataActivityLogService dataActivityLogService,
            ICandidateService candidateService,
            ICandidateNotificationService candidateNotificationService)
            : base(dependencies, dataActivityLogService, candidateService, candidateNotificationService)
        {
        }

        public override IQueryable<Candidate> ApplyContextFiltering(IQueryable<Candidate> records, object context)
        {
            if (context is CandidatePickerContext candidatePickerContext)
            {
                if (candidatePickerContext.OnlyValidConsent)
                {
                    var today = TimeService.GetCurrentDate();

                    records = records.Where(CandidateBusinessLogic.HasValidConsent.Parametrize(today));
                }

                if (!candidatePickerContext.AllowAnonymous)
                {
                    records = records.Where(c => !c.IsAnonymized);
                }
            }

            if (context is CandidateSearchContext candidateSearchContext)
            {
                if (!string.IsNullOrWhiteSpace(candidateSearchContext.FirstName))
                {
                    records = records.Where(c => c.FirstName.Contains(candidateSearchContext.FirstName));
                }

                if (!string.IsNullOrWhiteSpace(candidateSearchContext.LastName))
                {
                    var lastNameParts = HumanNameHelper.GetLastNameParts(candidateSearchContext.LastName);

                    records = records.Where(c => lastNameParts.Any(p => c.LastName.Contains(p)));
                }

                if (!string.IsNullOrWhiteSpace(candidateSearchContext.EmailAddress))
                {
                    records = records.Where(c => c.EmailAddress == candidateSearchContext.EmailAddress
                                                 || c.OtherEmailAddress == candidateSearchContext.EmailAddress);
                }

                if (!string.IsNullOrWhiteSpace(candidateSearchContext.PhoneNumber))
                {
                    records = records.Where(c => c.MobilePhone == candidateSearchContext.PhoneNumber
                                                 || c.OtherPhone == candidateSearchContext.PhoneNumber);
                }

                if (!string.IsNullOrWhiteSpace(candidateSearchContext.SocialLink))
                {
                    records = records.Where(c => c.SocialLink == candidateSearchContext.SocialLink);
                }
            }

            return records;
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<Candidate> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var direction = sortingCriterion.GetSortingDirection();

            if (sortingCriterion.GetSortingColumnName() == nameof(CandidateDto.FullName))
            {
                orderedQueryBuilder.ApplySortingKey(CandidateBusinessLogic.FullName.AsExpression(), direction);
            }
            else
            {
                base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
            }
        }
    }
}
