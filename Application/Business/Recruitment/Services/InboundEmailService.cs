﻿using System;
using System.Linq;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class InboundEmailService : IInboundEmailService
    {
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;

        public InboundEmailService(
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        public DateTime? GetLastReceivedOnOrDefault()
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.InboundEmails.Max(e => (DateTime?)e.ReceivedOn);
            }
        }

        public void SetEmailColorCategory(long id, InboundEmailColorCategory? inboundEmailColorCategory)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var inboundEmail = unitOfWork.Repositories.InboundEmails.Filtered().GetById(id);

                inboundEmail.EmailColorCategory = inboundEmailColorCategory;

                unitOfWork.Commit();
            }
        }
    }
}
