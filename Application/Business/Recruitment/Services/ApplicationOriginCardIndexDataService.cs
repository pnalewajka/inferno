﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class ApplicationOriginCardIndexDataService : CardIndexDataService<ApplicationOriginDto, ApplicationOrigin, IRecruitmentDbScope>, IApplicationOriginCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public ApplicationOriginCardIndexDataService(ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override IEnumerable<Expression<Func<ApplicationOrigin, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return o => o.Name;
            yield return o => o.OriginType;
        }

        protected override IOrderedQueryable<ApplicationOrigin> GetDefaultOrderBy(IQueryable<ApplicationOrigin> records, QueryCriteria criteria)
        {
            return records.OrderBy(o => o.OriginType).ThenBy(o => o.Name);
        }

        protected override NamedFilters<ApplicationOrigin> NamedFilters
        {
            get
            {
                return new NamedFilters<ApplicationOrigin>(CardIndexServiceHelper.BuildSoftDeletableFilters<ApplicationOrigin>());
            }
        }
    }
}
