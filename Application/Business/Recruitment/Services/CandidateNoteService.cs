﻿using System;
using System.Linq;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class CandidateNoteService : ICandidateNoteService
    {
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;
        private readonly IClassMappingFactory _classMappingFactory;

        public CandidateNoteService(
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService,
            IClassMappingFactory classMappingFactory)
        {
            _unitOfWorkService = unitOfWorkService;
            _classMappingFactory = classMappingFactory;
        }

        public void AddNote(CandidateNoteDto candidateNotesDto)
        {
            if (string.IsNullOrEmpty(candidateNotesDto.Content))
            {
                throw new ArgumentNullException(nameof(candidateNotesDto.Content));
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                if (!unitOfWork.Repositories.Candidates.Any(c => c.Id == candidateNotesDto.CandidateId))
                {
                    throw new ArgumentException(nameof(candidateNotesDto.CandidateId));
                }

                var mapping = _classMappingFactory.CreateMapping<CandidateNoteDto, CandidateNote>();
                var candidateNote = mapping.CreateFromSource(candidateNotesDto);

                unitOfWork.Repositories.CandidateNotes.Add(candidateNote);
                unitOfWork.Commit();
            }
        }
    }
}
