﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class CandidateConsentTypeItemProvider : EnumBasedListItemProvider<DataConsentType>
    {
        public CandidateConsentTypeItemProvider(IPrincipalProvider principalProvider)
            : base(principalProvider)
        {
        }

        public override IEnumerable<ListItem> GetItems()
        {
            return base.GetItems()
                .Where(i => DataConsentBusinessLogic.IsCandidateConsentType.Call((DataConsentType)i.Id.Value));
        }
    }
}
