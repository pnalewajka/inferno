﻿using System.Linq;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Business.Helpers;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class JobOpeningRecruitmentProcessCardIndexDataService : RecruitmentProcessCardIndexDataService, IJobOpeningRecruitmentProcessCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public JobOpeningRecruitmentProcessCardIndexDataService(
            ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies,
            IRecruitmentProcessService recruitmentProcessService,
            IDataActivityLogService dataActivityLogService,
            IRecruitmentProcessStepService recruitmentProcessStepService,
            ICandidateService candidateService,
            IRecruitmentProcessHistoryService recruitmentProcessHistoryService)
            : base(dependencies, recruitmentProcessService, dataActivityLogService, recruitmentProcessStepService, candidateService, recruitmentProcessHistoryService)
        {
        }

        public override IQueryable<RecruitmentProcess> ApplyContextFiltering(IQueryable<RecruitmentProcess> records, object context)
        {
            if (context is ParentIdContext parentIdContext && parentIdContext.ParentId != BusinessLogicHelper.NonExistingId)
            {
                records = records.Where(rp => rp.JobOpeningId == parentIdContext.ParentId);
            }

            return base.ApplyContextFiltering(records, context);
        }
    }
}
