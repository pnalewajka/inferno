﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification;
using Smt.Atomic.Business.Recruitment.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Business.Helpers;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using FilterCodes = Smt.Atomic.Business.Recruitment.Consts.FilterCodes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class JobOpeningCardIndexDataService : CardIndexDataService<JobOpeningDto, JobOpening, IRecruitmentDbScope>, IJobOpeningCardIndexDataService
    {
        private readonly IJobOpeningService _jobOpeningService;
        private readonly IJobOpeningNotificationService _jobOpeningNotificationService;
        private readonly IClassMapping<JobOpeningDto, JobOpening> _entityToDtoMapping;
        private readonly ICompanyService _companyService;
        private readonly IJobOpeningHistoryService _jobOpeningHistoryService;
        private readonly ISystemParameterService _systemParameterService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public JobOpeningCardIndexDataService(
            ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies,
            IJobOpeningService jobOpeningService,
            IJobOpeningNotificationService jobOpeningNotificationService,
            ISystemParameterService systemParameterService,
            ICompanyService companyService,
            IJobOpeningHistoryService jobOpeningHistoryService)
            : base(dependencies)
        {
            _jobOpeningService = jobOpeningService;
            _jobOpeningNotificationService = jobOpeningNotificationService;
            _entityToDtoMapping = dependencies.ClassMappingFactory.CreateMapping<JobOpeningDto, JobOpening>();
            _companyService = companyService;
            _jobOpeningHistoryService = jobOpeningHistoryService;
            _systemParameterService = systemParameterService;
        }

        protected override NamedFilters<JobOpening> NamedFilters
        {
            get
            {
                var currentEmployeeId = PrincipalProvider.Current.EmployeeId;
                var currentUserId = PrincipalProvider.Current.Id ?? BusinessLogicHelper.NonExistingId;

                return new NamedFilters<JobOpening>(new[]
                {
                    new NamedFilter<JobOpening>(FilterCodes.StatusAll, a => true),
                    new NamedDtoFilter<JobOpening, JobOpeningFiltersDto>
                        (FilterCodes.JobOpeningFilters, JobOpeningBusinessLogic.FitlerJobOpenings.BaseExpression),
                   new NamedFilter<JobOpening>(FilterCodes.MyRecords, JobOpeningBusinessLogic.IsMyJobOpening.Parametrize(currentEmployeeId, currentUserId)),
                     new NamedFilter<JobOpening>(FilterCodes.AnyOwner, a => true)

                }
                .Union(CardIndexServiceHelper.BuildEnumBasedFilters<JobOpening, JobOpeningStatus>(u => u.Status))
                .Union(CardIndexServiceHelper.BuildSoftDeletableFilters<JobOpening>()));
            }
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<JobOpening, JobOpeningDto, IRecruitmentDbScope> eventArgs)
        {
            eventArgs.InputEntity.RoleOpenedOn = eventArgs.InputDto.RoleOpenedOn = null;

            var validationResult = Validate(eventArgs.InputDto, eventArgs.UnitOfWork);

            if (!validationResult.IsSuccessful)
            {
                eventArgs.Alerts.AddRange(validationResult.Alerts);

                return;
            }

            eventArgs.InputEntity.JobProfiles = EntityMergingService.CreateEntitiesFromIds<JobProfile, IRecruitmentDbScope>(eventArgs.UnitOfWork, eventArgs.InputEntity.JobProfiles.GetIds());
            eventArgs.InputEntity.Cities = EntityMergingService.CreateEntitiesFromIds<City, IRecruitmentDbScope>(eventArgs.UnitOfWork, eventArgs.InputEntity.Cities.GetIds());
            eventArgs.InputEntity.Recruiters = EntityMergingService.CreateEntitiesFromIds<Employee, IRecruitmentDbScope>(eventArgs.UnitOfWork, eventArgs.InputEntity.Recruiters.GetIds());
            eventArgs.InputEntity.RecruitmentOwners = EntityMergingService.CreateEntitiesFromIds<Employee, IRecruitmentDbScope>(eventArgs.UnitOfWork, eventArgs.InputEntity.RecruitmentOwners.GetIds());
            eventArgs.InputEntity.Watchers = EntityMergingService.CreateEntitiesFromIds<Employee, IRecruitmentDbScope>(eventArgs.UnitOfWork, eventArgs.InputEntity.Watchers.GetIds());
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<JobOpening, JobOpeningDto, IRecruitmentDbScope> eventArgs)
        {
            if (eventArgs.DbEntity.Status == JobOpeningStatus.Closed)
            {
                throw new InvalidOperationException("Record can't be edited when status is Closed");
            }

            eventArgs.InputEntity.RejectionReason = eventArgs.DbEntity.RejectionReason;
            eventArgs.InputEntity.ClosedComment = eventArgs.DbEntity.ClosedComment;
            eventArgs.InputEntity.ClosedReason = eventArgs.DbEntity.ClosedReason;
            eventArgs.InputEntity.ClosedOn = eventArgs.DbEntity.ClosedOn;
            eventArgs.InputEntity.RoleOpenedOn = eventArgs.InputDto.RoleOpenedOn = eventArgs.DbEntity.RoleOpenedOn;

            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.JobProfiles, eventArgs.InputEntity.JobProfiles);
            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.Cities, eventArgs.InputEntity.Cities);
            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.Recruiters, eventArgs.InputEntity.Recruiters);
            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.RecruitmentOwners, eventArgs.InputEntity.RecruitmentOwners);
            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.Watchers, eventArgs.InputEntity.Watchers);

            MergeProjectDescriptions(eventArgs);

            if (eventArgs.DbEntity.DecisionMakerEmployeeId != eventArgs.InputDto.DecisionMakerId)
            {
                UpdateAssignedEmployeeInSubjectedSteps(eventArgs.DbEntity.Id, eventArgs.InputDto.DecisionMakerId);
            }
        }

        private void MergeProjectDescriptions(RecordEditingEventArgs<JobOpening, JobOpeningDto, IRecruitmentDbScope> eventArgs)
        {
            var recordsToAdd = eventArgs.InputEntity.ProjectDescriptions.ToList();
            var recordsToRemove = eventArgs.DbEntity.ProjectDescriptions.ToList();

            eventArgs.UnitOfWork.Repositories.ProjectDescriptions.DeleteRange(recordsToRemove);

            foreach (var item in recordsToAdd)
            {
                item.Id = default(long);
                eventArgs.DbEntity.ProjectDescriptions.Add(item);
            }
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<JobOpening, JobOpeningDto> recordAddedEventArgs)
        {
            if (recordAddedEventArgs.InputDto.WorkflowAction.HasValue)
            {
                HandleWorkflowAction(recordAddedEventArgs.InputDto.WorkflowAction.Value, recordAddedEventArgs.InputEntity, recordAddedEventArgs.InputDto, recordAddedEventArgs.Alerts);
            }

            _jobOpeningHistoryService.LogActivity(recordAddedEventArgs.InputEntity.Id, JobOpeningHistoryEntryType.Created);
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<JobOpening, JobOpeningDto> recordEditedEventArgs)
        {
            if (recordEditedEventArgs.InputDto.WorkflowAction.HasValue)
            {
                HandleWorkflowAction(recordEditedEventArgs.InputDto.WorkflowAction.Value, recordEditedEventArgs.DbEntity, recordEditedEventArgs.InputDto, recordEditedEventArgs.Alerts);
            }

            if (recordEditedEventArgs.InputDto.Status == JobOpeningStatus.Active)
            {
                var currentAssignedRecruiters = recordEditedEventArgs.OriginalDto.RecruitersIds;
                var newlyAssignedRecruiters = recordEditedEventArgs.InputDto.RecruitersIds;

                NotifyAboutAssignment(recordEditedEventArgs.InputDto.Id, currentAssignedRecruiters, newlyAssignedRecruiters);
            }

            _jobOpeningService.NotifyAboutRecordChanges(recordEditedEventArgs.OriginalDto, recordEditedEventArgs.InputDto);
            _jobOpeningHistoryService.LogActivity(recordEditedEventArgs.InputEntity.Id, JobOpeningHistoryEntryType.Updated, 
                _jobOpeningService.GetSnapshotChanges(recordEditedEventArgs.OriginalDto, recordEditedEventArgs.InputDto));
        }

        private void UpdateAssignedEmployeeInSubjectedSteps(long jobOpeningId, long newAssignedEmployeeId)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var recruitmentProcesses = unitOfWork.Repositories.RecruitmentProcessSteps
                    .Where(s => s.RecruitmentProcess.JobOpeningId == jobOpeningId)
                    .Where(RecruitmentProcessStepBusinessLogic.IsActiveStep)
                    .Where(s => RecruitmentProcessStepBusinessLogic.IsClientSpecificStep.Call(s.Type)
                        || RecruitmentProcessStepBusinessLogic.IsHiringManagerSpecificStep.Call(s.Type));

                foreach (var recruitmentProcess in recruitmentProcesses)
                {
                    recruitmentProcess.AssignedEmployeeId = newAssignedEmployeeId;
                }

                unitOfWork.Commit();
            }
        }

        private void NotifyAboutAssignment(long jobOpeningId, long[] currentAssignedRecruiters, long[] newlyAssignedRecruiters)
        {
            var assigned = newlyAssignedRecruiters.Except(currentAssignedRecruiters).ToArray();
            var unassigned = currentAssignedRecruiters.Except(newlyAssignedRecruiters).ToArray();

            if (assigned.Any())
            {
                _jobOpeningNotificationService.SendAssignedRemainderEmail(jobOpeningId, assigned);
            }

            if (unassigned.Any())
            {
                _jobOpeningNotificationService.SendUnassignedRemainderEmail(jobOpeningId, unassigned);
            }
        }

        protected override IEnumerable<Expression<Func<JobOpening, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return j => j.PositionName;
            yield return j => j.DecisionMakerEmployee.FirstName;
            yield return j => j.DecisionMakerEmployee.LastName;
        }

        public JobOpeningDto GetDefaultNewRecord(HiringMode hiringMode)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var currentUser = PrincipalProvider.Current;
                var currentEmployee = unitOfWork.Repositories.Employees.GetById(currentUser.EmployeeId);

                var defaultRecord = base.GetDefaultNewRecord();

                defaultRecord.HiringMode = hiringMode;
                defaultRecord.Status = JobOpeningStatus.Draft;
                defaultRecord.OrgUnitId = currentEmployee.OrgUnitId;
                defaultRecord.DecisionMakerId = currentEmployee.Id;

                defaultRecord.CompanyId = GetDefaultCompanyId();

                defaultRecord.ProjectDescriptions = new List<ProjectDescriptionDto>()
                {
                    new ProjectDescriptionDto()
                };

                switch (hiringMode)
                {
                    case HiringMode.Intive:
                        defaultRecord.IsTechnicalVerificationRequired = true;

                        break;
                }

                return defaultRecord;
            }
        }

        protected override IQueryable<JobOpening> ConfigureIncludes(IQueryable<JobOpening> sourceQueryable)
        {
            return sourceQueryable
                .Include(e => e.Customer)
                .Include(e => e.AcceptingEmployee);
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<JobOpening> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var direction = sortingCriterion.GetSortingDirection();
            var sortingColumnName = sortingCriterion.GetSortingColumnName();

            if (sortingColumnName == nameof(JobOpeningDto.VacancyTotal))
            {
                orderedQueryBuilder.ApplySortingKey(JobOpeningBusinessLogic.VacancyTotal.AsExpression(), direction);
            }
            else if (sortingColumnName == nameof(JobOpeningDto.CityIds))
            {
                orderedQueryBuilder.ApplySortingKey(b => b.Cities.OrderBy(l => l.Name).Select(l => l.Name).FirstOrDefault(), direction);
            }
            else if (sortingColumnName == nameof(JobOpeningDto.JobProfileIds))
            {
                orderedQueryBuilder.ApplySortingKey(b => b.JobProfiles.OrderBy(p => p.Name).Select(p => p.Name).FirstOrDefault(), direction);
            }
            else if (sortingColumnName == nameof(JobOpeningDto.DecisionMakerId))
            {
                orderedQueryBuilder.ApplySortingKey(JobOpeningBusinessLogic.DecisionMakerFullName.AsExpression(), direction);
            }
            else
            {
                base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
            }
        }

        private void HandleWorkflowAction(JobOpeningWorkflowAction action, JobOpening dbEntity, JobOpeningDto inputDto, IList<AlertDto> alerts)
        {
            switch (action)
            {
                case JobOpeningWorkflowAction.SendForApproval:
                    _jobOpeningService.SendForApprovalJobOpening(dbEntity.Id);
                    return;

                case JobOpeningWorkflowAction.Approve:
                    _jobOpeningService.ApproveJobOpenings(new List<long> { dbEntity.Id });
                    return;

                case JobOpeningWorkflowAction.Reject:
                    _jobOpeningService.RejectJobOpenings(new List<long> { dbEntity.Id }, inputDto.WorkflowRejectionReason);
                    return;

                case JobOpeningWorkflowAction.Close:
                    _jobOpeningService.CloseJobOpening(new JobOpeningCloseDto
                    {
                        JobOpeningId = dbEntity.Id,
                        ClosedComment = inputDto.WorkflowClosedComment,
                        ClosedReason = inputDto.WorkflowClosedReason.Value
                    });
                    return;

                case JobOpeningWorkflowAction.Watch:
                    _jobOpeningService.ToggleWatching(new List<long> { dbEntity.Id }, PrincipalProvider.Current.EmployeeId, Enums.WatchingStatus.Watch);
                    return;

                case JobOpeningWorkflowAction.Unwatch:
                    _jobOpeningService.ToggleWatching(new List<long> { dbEntity.Id }, PrincipalProvider.Current.EmployeeId, Enums.WatchingStatus.Unwatch);
                    return;
            }
        }

        private long GetDefaultCompanyId()
        {
            var defaultCompanyCode = SystemParameters.GetParameter<string>(ParameterKeys.RecruitmentDefaultCompanyActiveDirectoryCode);
            var defaultCompanyId = _companyService.GetCompanyIdOrDefaultByActiveDirectorycode(defaultCompanyCode);

            return defaultCompanyId.Value;
        }

        private BoolResult Validate(JobOpeningDto jobOpeningDto, IUnitOfWork<IRecruitmentDbScope> unitOfWork)
        {
            var defaultOrgUnitCode = _systemParameterService.GetParameter<string>(ParameterKeys.OrganizationDefaultOrgUnitCode);
            var defaultOrgUnitId = unitOfWork.Repositories.OrgUnits.Where(ou => ou.Code == defaultOrgUnitCode).Select(s => s.Id).Single();

            if (jobOpeningDto.OrgUnitId == defaultOrgUnitId)
            {
                return new BoolResult(false, new AlertDto[]
                {
                    new AlertDto { Type = AlertType.Error, Message = JobOpeningResources.InvalidOrgUnit }
                });
            }

            return BoolResult.Success;
        }
    }
}