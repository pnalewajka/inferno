﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class CandidateNoteCardIndexDataService : CardIndexDataService<CandidateNoteDto, CandidateNote, IRecruitmentDbScope>, ICandidateNoteCardIndexDataService
    {
        private readonly IDataActivityLogService _dataActivityLogService;
        private readonly ICandidateNoteNotificationService _candidateNoteNotificationService;
        private readonly ICandidateNotificationService _candidateNotificationService;
        private readonly ICandidateService _candidateService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public CandidateNoteCardIndexDataService(ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies,
            IDataActivityLogService dataActivityLogService,
            ICandidateNoteNotificationService candidateNoteNotificationService,
            ICandidateNotificationService candidateNotificationService,
            ICandidateService candidateService)
            : base(dependencies)
        {
            _dataActivityLogService = dataActivityLogService;
            _candidateNoteNotificationService = candidateNoteNotificationService;
            _candidateNotificationService = candidateNotificationService;
            _candidateService = candidateService;
        }

        public override IQueryable<CandidateNote> ApplyContextFiltering(IQueryable<CandidateNote> records, object context)
        {
            if (context is ParentIdContext parentIdContext)
            {
                return records.Where(c => c.CandidateId == parentIdContext.ParentId);
            }

            return base.ApplyContextFiltering(records, context);
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<CandidateNote, CandidateNoteDto> recordAddedEventArgs)
        {
            _dataActivityLogService.LogCandidateDataActivity(recordAddedEventArgs.InputEntity.CandidateId, ActivityType.CreatedRecord, ReferenceType.CandidateNote, recordAddedEventArgs.InputEntity.Id);
            _candidateNoteNotificationService.NotifyAboutAdded(recordAddedEventArgs.InputEntity.Id);

            _candidateService.UpdateLastActivity(recordAddedEventArgs.InputEntity.CandidateId, ReferenceType.CandidateNote, recordAddedEventArgs.InputEntity.Id);
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<CandidateNote, CandidateNoteDto> recordEditedEventArgs)
        {
            _dataActivityLogService.LogCandidateDataActivity(recordEditedEventArgs.InputEntity.CandidateId, ActivityType.UpdatedRecord, ReferenceType.CandidateNote, recordEditedEventArgs.InputEntity.Id);
            _candidateNoteNotificationService.NotifyAboutEdited(recordEditedEventArgs.InputEntity.Id);

            _candidateService.UpdateLastActivity(recordEditedEventArgs.DbEntity.CandidateId, ReferenceType.CandidateNote, recordEditedEventArgs.DbEntity.Id);
        }

        protected override void OnRecordsDeleted(RecordDeletedEventArgs<CandidateNote> eventArgs)
        {
            foreach (var note in eventArgs.DeletedEntities)
            {
                _dataActivityLogService.LogCandidateDataActivity(note.CandidateId, ActivityType.RemovedRecord, ReferenceType.CandidateNote, note.Id);
            }
        }

        protected override NamedFilters<CandidateNote> NamedFilters => new NamedFilters<CandidateNote>(CardIndexServiceHelper.BuildSoftDeletableFilters<CandidateNote>());

        protected override IEnumerable<Expression<Func<CandidateNote, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return n => n.Content;
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<CandidateNote, CandidateNoteDto, IRecruitmentDbScope> eventArgs)
        {
            base.OnRecordAdding(eventArgs);

            AdjustMentionedEmployeeId(eventArgs.InputEntity, eventArgs.UnitOfWork);

            eventArgs.InputEntity.MentionedEmployees = EntityMergingService.CreateEntitiesFromIds<Employee, IRecruitmentDbScope>(eventArgs.UnitOfWork, eventArgs.InputEntity.MentionedEmployees.GetIds());
        }

        private void AdjustMentionedEmployeeId(CandidateNote candidateNote, IUnitOfWork<IRecruitmentDbScope> unitOfWork)
        {
            var employeeIds = MentionItemHelper.GetTokenIds(candidateNote.Content).Distinct();

            candidateNote.MentionedEmployees = unitOfWork.Repositories.Employees.GetByIds(employeeIds).ToList();
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<CandidateNote, CandidateNoteDto, IRecruitmentDbScope> eventArgs)
        {
            base.OnRecordEditing(eventArgs);

            AdjustMentionedEmployeeId(eventArgs.InputEntity, eventArgs.UnitOfWork);

            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.MentionedEmployees, eventArgs.InputEntity.MentionedEmployees);
        }
    }
}