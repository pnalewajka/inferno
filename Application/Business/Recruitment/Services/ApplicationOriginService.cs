﻿using System;
using System.Linq;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Business.Recruitment.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class ApplicationOriginService : IApplicationOriginService
    {
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;

        public ApplicationOriginService(IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        public long GetApplicationOriginId(ApplicationOriginType applicationOriginType)
        {
            using (var service = _unitOfWorkService.Create())
            {
                var dataOrgin = service.Repositories.ApplicationOrigins.SingleOrDefault(o => o.OriginType == applicationOriginType);

                if (dataOrgin != null)
                {
                    return dataOrgin.Id;
                }
            }

            throw new BusinessException(String.Format(AlertResources.NoMatchingApplicationOrigin, applicationOriginType));
        }
    }
}
