﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class JobOpeningNoteCardIndexDataService : CardIndexDataService<JobOpeningNoteDto, JobOpeningNote, IRecruitmentDbScope>, IJobOpeningNoteCardIndexDataService
    {
        private readonly IClassMapping<Employee, EmployeeDto> _employeeToEmployeeDtoClassMapping;
        private readonly IJobOpeningNoteNotificationService _jobOpeningNoteNotificationService;
        private readonly IJobOpeningHistoryService _jobOpeningHistoryService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public JobOpeningNoteCardIndexDataService(ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies,
            IJobOpeningNoteNotificationService jobOpeningNoteNotificationService,
            IClassMappingFactory mappingFactory,
            IJobOpeningHistoryService jobOpeningHistoryService)
            : base(dependencies)
        {
            _employeeToEmployeeDtoClassMapping = mappingFactory.CreateMapping<Employee, EmployeeDto>();
            _jobOpeningNoteNotificationService = jobOpeningNoteNotificationService;
            _jobOpeningHistoryService = jobOpeningHistoryService;
        }

        public override IQueryable<JobOpeningNote> ApplyContextFiltering(IQueryable<JobOpeningNote> records, object context)
        {
            if (context is ParentIdContext parentIdContext)
            {
                return records.Where(c => c.JobOpeningId == parentIdContext.ParentId);
            }

            return base.ApplyContextFiltering(records, context);
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<JobOpeningNote, JobOpeningNoteDto, IRecruitmentDbScope> eventArgs)
        {
            base.OnRecordAdding(eventArgs);

            AdjustMentionedEmployeeId(eventArgs.InputEntity, eventArgs.UnitOfWork);

            eventArgs.InputEntity.MentionedEmployees = EntityMergingService.CreateEntitiesFromIds<Employee, IRecruitmentDbScope>(eventArgs.UnitOfWork, eventArgs.InputEntity.MentionedEmployees.GetIds());
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<JobOpeningNote, JobOpeningNoteDto, IRecruitmentDbScope> eventArgs)
        {
            base.OnRecordEditing(eventArgs);

            AdjustMentionedEmployeeId(eventArgs.InputEntity, eventArgs.UnitOfWork);

            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.MentionedEmployees, eventArgs.InputEntity.MentionedEmployees);
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<JobOpeningNote, JobOpeningNoteDto> recordAddedEventArgs)
        {
            base.OnRecordAdded(recordAddedEventArgs);
            _jobOpeningNoteNotificationService.NotifyAboutNoteAdded(recordAddedEventArgs.InputEntity.Id);
            _jobOpeningHistoryService.LogActivity(recordAddedEventArgs.InputEntity.JobOpeningId, JobOpeningHistoryEntryType.AddedNote);
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<JobOpeningNote, JobOpeningNoteDto> recordEditedEventArgs)
        {
            base.OnRecordEdited(recordEditedEventArgs);
            _jobOpeningNoteNotificationService.NotifyAboutNoteEdited(recordEditedEventArgs.InputEntity.Id);
        }

        private void AdjustMentionedEmployeeId(JobOpeningNote jobOpeningNote, IUnitOfWork<IRecruitmentDbScope> unitOfWork)
        {
            var employeeIds = MentionItemHelper.GetTokenIds(jobOpeningNote.Content).Distinct();

            jobOpeningNote.MentionedEmployees = unitOfWork.Repositories.Employees.GetByIds(employeeIds).ToList();
        }

        protected override NamedFilters<JobOpeningNote> NamedFilters
        {
            get
            {
                return new NamedFilters<JobOpeningNote>(CardIndexServiceHelper.BuildSoftDeletableFilters<JobOpeningNote>());
            }
        }

        protected override IEnumerable<Expression<Func<JobOpeningNote, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return n => n.Content;
        }
    }
}
