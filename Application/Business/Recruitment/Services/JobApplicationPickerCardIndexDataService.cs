﻿using System.Linq;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    class JobApplicationPickerCardIndexDataService : JobApplicationCardIndexDataService, IJobApplicationPickerCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public JobApplicationPickerCardIndexDataService(IDataActivityLogService dataActivityLogService, 
            ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies,
            IApplicationOriginService applicationOriginService)
            : base(dataActivityLogService, dependencies, applicationOriginService)
        {
        }

        public override IQueryable<JobApplication> ApplyContextFiltering(IQueryable<JobApplication> records, object context)
        {
            if (context is ParentIdContext parentIdContext)
            {
                return records.Where(r => r.CandidateId == parentIdContext.ParentId);
            }

            return records;
        }
    }
}
