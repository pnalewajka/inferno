﻿using System.Collections.Generic;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Recruitment.Dto.Reminder.InboundEmailReminder;
using Smt.Atomic.Business.Recruitment.Helpers;
using Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services.Notification
{
    public class InboundEmailNotificationService : IInboundEmailNotificationService
    {
        private readonly IReliableEmailService _reliableEmailService;
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;
        private readonly string _serverAddress;

        public InboundEmailNotificationService(IReliableEmailService reliableEmailService,
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService,
            ISystemParameterService systemParameterService)
        {
            _reliableEmailService = reliableEmailService;
            _unitOfWorkService = unitOfWorkService;
            _serverAddress = systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);
        }

        public void NotifyAboutAssignment(long inboundEmailId)
        {
            NotifyAssignee(TemplateCodes.NotifyAboutInboundEmailAssignmentBody,
                TemplateCodes.NotifyAboutInboundEmailAssignmentSubject, inboundEmailId);
        }

        public void NotifyAboutUnassignment(long inboundEmailId)
        {
            NotifyAssignee(TemplateCodes.NotifyAboutInboundEmailUnassignmentBody,
                TemplateCodes.NotifyAboutInboundEmailUnassignmentSubject, inboundEmailId);
        }

        private void NotifyAssignee(string bodyTemplateCode, string subjectTemplateCode, long inboundEmailId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var inboundEmail = unitOfWork.Repositories.InboundEmails.GetById(inboundEmailId);

                var reminderDto = GetReminderDto(inboundEmail);

                var recipient = new EmailRecipientDto
                {
                    EmailAddress = inboundEmail.ProcessedBy.Email,
                    FullName = EmployeeBusinessLogic.FullName.Call(inboundEmail.ProcessedBy)
                };

                _reliableEmailService.EnqueueBatch(new EmailBatchDto
                {
                    BodyTemplateCode = bodyTemplateCode,
                    SubjectTemplateCode = subjectTemplateCode,
                    Model = reminderDto,
                    Recipients = new List<EmailRecipientDto> { recipient }
                });
            }
        }

        private InboundEmailReminderDto GetReminderDto(InboundEmail inboundEmail)
        {
            return new InboundEmailReminderDto
            {
                Category = inboundEmail.Category.GetDescription(),
                From = inboundEmail.FromEmailAddress,
                InboundEmailUrl = RecruitmentUrlHelper.GetInboundEmailUrl(_serverAddress, inboundEmail.Id),
                Subject = inboundEmail.Subject
            };
        }
    }
}
