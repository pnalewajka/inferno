﻿using System.Data.Entity;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Dto.ReminderDto;
using Smt.Atomic.Business.Recruitment.Helpers;
using Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services.Notification.RecruitmentProcessNoteServices
{
    public class RecruitmentProcessNoteNotificationService : IRecruitmentProcessNoteNotificationService
    {
        private readonly IReliableEmailService _reliableEmailService;
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;
        private readonly IRecruitmentProcessNoteRecipientProvider _recruitmentProcessNoteRecipientsProvider;
        private readonly ISystemParameterService _systemParameterService;
        private readonly string _serverAddress;

        public RecruitmentProcessNoteNotificationService(
            IReliableEmailService reliableEmailService,
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService,
            IRecruitmentProcessNoteRecipientProvider recruitmentProcessNoteRecipientsProvider,
            ISystemParameterService systemParameterService)
        {
            _reliableEmailService = reliableEmailService;
            _unitOfWorkService = unitOfWorkService;
            _systemParameterService = systemParameterService;
            _recruitmentProcessNoteRecipientsProvider = recruitmentProcessNoteRecipientsProvider;
            _serverAddress = systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);
        }

        public void NotifyAboutAddedNoteInRecruitmentProcessNotes(long recruitmentProcessNoteId)
        {
            Notify(TemplateCodes.NotifyAboutAddedNoteInRecruitmentProcessNotesBody,
                TemplateCodes.NotifyAboutAddedNoteInRecruitmentProcessNotesSubject, recruitmentProcessNoteId,
                new RecruitmentProcessRecipientType[] { RecruitmentProcessRecipientType.Watchers, RecruitmentProcessRecipientType.NoteMentionedEmployee, RecruitmentProcessRecipientType.Recruiter });
        }

        public void NotifyAboutEditedNoteInRecruitmentProcessNotes(long recruitmentProcessNoteId)
        {
            Notify(TemplateCodes.NotifyAboutEditedNoteInRecruitmentProcessNotesBody,
                TemplateCodes.NotifyAboutEditedNoteInRecruitmentProcessNotesSubject, recruitmentProcessNoteId,
                new RecruitmentProcessRecipientType[] { RecruitmentProcessRecipientType.Watchers, RecruitmentProcessRecipientType.NoteMentionedEmployee, RecruitmentProcessRecipientType.Recruiter });
        }

        private void Notify(string bodyTemplateCode, string subjectTemplateCode, long processNoteId, RecruitmentProcessRecipientType[] recipients)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recruitmentProcessNote = GetRecruitmentProcessNote(unitOfWork, processNoteId);

                var reminderDto = GetReminderDto(recruitmentProcessNote);

                var recipientsList = _recruitmentProcessNoteRecipientsProvider.GetRecipients(recruitmentProcessNote, recipients);

                _reliableEmailService.EnqueueBatch(new EmailBatchDto
                {
                    BodyTemplateCode = bodyTemplateCode,
                    SubjectTemplateCode = subjectTemplateCode,
                    Model = reminderDto,
                    Recipients = recipientsList
                }, true);
            }
        }

        private RecruitmentProcessNote GetRecruitmentProcessNote(IUnitOfWork<IRecruitmentDbScope> unitOfWork, long id)
        {
            return unitOfWork.Repositories.RecruitmentProcessNotes.Include(r => r.RecruitmentProcess)
                .Include(r => r.RecruitmentProcess)
                .Include(r => r.RecruitmentProcess.Watchers)
                .Include(r => r.RecruitmentProcess.ModifiedBy)
                .Include(r => r.MentionedEmployees)
                .GetById(id);
        }

        private RecruitmentProcessNoteReminderDto GetReminderDto(RecruitmentProcessNote note)
        {
            var recruitmentProcess = note.RecruitmentProcess;

            return new RecruitmentProcessNoteReminderDto
            {
                ProcessUrl = RecruitmentUrlHelper.GetRecruitmentProcessUrl(_serverAddress, recruitmentProcess.Id),
                ProcessName = RecruitmentProcessBusinessLogic.DisplayName.Call(recruitmentProcess),
                ProcessNoteUrl = RecruitmentUrlHelper.GetRecruitmentProcessNoteUrl(_serverAddress, note.Id, recruitmentProcess.Id),
                ModifiedByFullName = $"{note.ModifiedBy.FirstName} {note.ModifiedBy.LastName}",
                NoteContent = note.Content != null ? MentionItemHelper.ReplaceTokens(note.Content, i => i.Label) : null
            };
        }
    }
}
