﻿using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Dto.ReminderDto;
using Smt.Atomic.Business.Recruitment.Helpers;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services.Notification.RecruitmentProcessServices
{
    public class RecruitmentProcessNotificationService : IRecruitmentProcessNotificationService
    {
        private readonly IReliableEmailService _reliableEmailService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;
        private readonly IRecruitmentProcessRecipientsProvider _recruitmentProcessRecipientsProvider;
        private readonly string _serverAddress;

        public RecruitmentProcessNotificationService(
            IReliableEmailService reliableEmailService,
            ISystemParameterService systemParameterService,
            IPrincipalProvider principalProvider,
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService,
            IRecruitmentProcessRecipientsProvider recruitmentProcessRecipientsProvider)
        {
            _reliableEmailService = reliableEmailService;
            _principalProvider = principalProvider;
            _unitOfWorkService = unitOfWorkService;
            _recruitmentProcessRecipientsProvider = recruitmentProcessRecipientsProvider;
            _serverAddress = systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);
        }

        public void NotifyAboutProcessPutOnHold(long processId)
        {
            NotifyOtherDecisionMakersAboutProcessPutOnHold(processId);
            NotifyRecruiterAboutProcessPutOnHold(processId);
            NotifyWatcherAboutProcessPutOnHold(processId);
        }

        public void NotifyOtherDecisionMakersAboutProcessPutOnHold(long processId)
        {
            NorifyDecisionMakers(TemplateCodes.NotifyAboutProcessPutOnHoldBody,
                TemplateCodes.NotifyAboutProcessPutOnHoldSubject, processId);
        }

        public void NotifyOtherDecisionMakersAboutProcessTakenOffHold(long processId)
        {
            NorifyDecisionMakers(TemplateCodes.NotifyAboutProcessTakenOffHoldBody,
                TemplateCodes.NotifyAboutProcessTakenOffHoldSubject, processId);
        }

        public void NotifyRecruiterAboutProcessPutOnHold(long processId)
        {
            NotifyRecruiter(TemplateCodes.NotifyAboutProcessPutOnHoldBody,
                TemplateCodes.NotifyAboutProcessPutOnHoldSubject, processId);
        }

        public void NotifyRecruiterAboutProcessTakenOffHold(long processId)
        {
            NotifyRecruiter(TemplateCodes.NotifyAboutProcessTakenOffHoldBody,
                TemplateCodes.NotifyAboutProcessTakenOffHoldSubject, processId);
        }

        public void NotifyWatcherAboutProcessPutOnHold(long processId)
        {
            NotifyWatchers(TemplateCodes.NotifyAboutProcessPutOnHoldBody,
                TemplateCodes.NotifyAboutProcessPutOnHoldSubject, processId);
        }

        public void NotifyWatcherAboutProcessTakenOffHold(long processId)
        {
            NotifyWatchers(TemplateCodes.NotifyAboutProcessTakenOffHoldBody,
                TemplateCodes.NotifyAboutProcessTakenOffHoldSubject, processId);
        }

        private void NorifyDecisionMakers(string bodyTemplateCode, string subjectTemplateCode, long processId)
        {
            var currentEmployeeId = _principalProvider.Current.EmployeeId;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recruitmentProcess = GetRecruitmentProcess(unitOfWork, processId);

                var recipients = _recruitmentProcessRecipientsProvider
                    .GetRecipients(recruitmentProcess, RecruitmentProcessRecipientType.AllDecisionMakers);

                if (!recipients.Any())
                {
                    return;
                }

                var reminderDto = GetReminderDto(recruitmentProcess);

                _reliableEmailService.EnqueueBatch(new EmailBatchDto
                {
                    BodyTemplateCode = bodyTemplateCode,
                    SubjectTemplateCode = subjectTemplateCode,
                    Model = reminderDto,
                    Recipients = recipients
                }, true);
            }
        }

        private void NotifyRecruiter(string bodyTemplateCode, string subjectTemplateCode, long processId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recruitmentProcess = GetRecruitmentProcess(unitOfWork, processId);

                var recipients = _recruitmentProcessRecipientsProvider
                    .GetRecipients(recruitmentProcess, RecruitmentProcessRecipientType.Recruiter);

                if (!recipients.Any())
                {
                    return;
                }

                var reminderDto = GetReminderDto(recruitmentProcess);

                _reliableEmailService.EnqueueBatch(new EmailBatchDto
                {
                    BodyTemplateCode = bodyTemplateCode,
                    SubjectTemplateCode = subjectTemplateCode,
                    Model = reminderDto,
                    Recipients = recipients
                }, true);
            }
        }

        private void NotifyWatchers(string bodyTemplateCode, string subjectTemplateCode, long processId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recruitmentProcess = GetRecruitmentProcess(unitOfWork, processId);

                var recipients = _recruitmentProcessRecipientsProvider
                    .GetRecipients(recruitmentProcess, RecruitmentProcessRecipientType.Watchers);

                if (!recipients.Any())
                {
                    return;
                }

                var reminderDto = GetReminderDto(recruitmentProcess, true);

                _reliableEmailService.EnqueueBatch(new EmailBatchDto
                {
                    BodyTemplateCode = bodyTemplateCode,
                    SubjectTemplateCode = subjectTemplateCode,
                    Model = reminderDto,
                    Recipients = recipients
                }, true);
            }
        }

        private RecruitmentProcess GetRecruitmentProcess(IUnitOfWork<IRecruitmentDbScope> unitOfWork, long id)
        {
            return unitOfWork.Repositories.RecruitmentProcesses
                .Include(r => r.Recruiter)
                .Include(r => r.ModifiedBy)
                .Include(r => r.Candidate)
                .Include(r => r.JobOpening)
                .Include(r => r.Watchers)
                .GetById(id);
        }

        private RecruitmentProcessReminderDto GetReminderDto(RecruitmentProcess recruitmentProcess, bool isForWatcher = false)
        {
            return new RecruitmentProcessReminderDto
            {
                ProcessUrl = RecruitmentUrlHelper.GetRecruitmentProcessUrl(_serverAddress, recruitmentProcess.Id),
                ProcessName = RecruitmentProcessBusinessLogic.DisplayName.Call(recruitmentProcess),
                ModifiedByFullName = UserBusinessLogic.FullName.Call(recruitmentProcess.ModifiedBy),
                PositionName = recruitmentProcess.JobOpening.PositionName,
                CandidateFullName = CandidateBusinessLogic.FullName.Call(recruitmentProcess.Candidate),
                IsForWatcher = isForWatcher
            };
        }
    }
}
