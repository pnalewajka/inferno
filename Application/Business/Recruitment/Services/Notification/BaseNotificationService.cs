﻿using System.Linq;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services.Notification
{
    public abstract class BaseNotificationService<TEntity, TReminderDto, TRecipient>
    {
        private readonly IRecipientProvider<TEntity, TRecipient> RecipientProvider;
        private readonly IReliableEmailService ReliableEmailService;
        protected readonly IUnitOfWorkService<IRecruitmentDbScope> UnitOfWorkService;
        protected readonly string _serverAddress;

        public BaseNotificationService(
             IReliableEmailService reliableEmailService,
             ISystemParameterService systemParameterService,
             IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService,
             IRecipientProvider<TEntity, TRecipient> recipientProvider)
        {
            ReliableEmailService = reliableEmailService;
            UnitOfWorkService = unitOfWorkService;
            RecipientProvider = recipientProvider;
            _serverAddress = systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);
        }

        protected void Notify(string bodyTemplateCode, string subjectTemplateCode, long entityId, TRecipient[] recipients, params object[] additionalData)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var entity = GetEntity(unitOfWork, entityId);

                var reminderDto = GetReminderDto(entity, additionalData);

                var recipientsList = RecipientProvider.GetRecipients(entity, recipients);

                if (!recipientsList.Any())
                {
                    return;
                }

                ReliableEmailService.EnqueueBatch(new EmailBatchDto
                {
                    BodyTemplateCode = bodyTemplateCode,
                    SubjectTemplateCode = subjectTemplateCode,
                    Model = reminderDto,
                    Recipients = recipientsList
                }, true);
            }
        }

        protected abstract TEntity GetEntity(IUnitOfWork<IRecruitmentDbScope> unitOfWork, long id);

        protected abstract TReminderDto GetReminderDto(TEntity entity, params object[] additionalData);
    }
}
