﻿using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Recruitment.Dto.Reminder.JobOpeningReminder;
using Smt.Atomic.Business.Recruitment.Helpers;
using Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services.Notification
{
    public class JobOpeningNoteNotificationService : IJobOpeningNoteNotificationService
    {
        private readonly IReliableEmailService _reliableEmailService;
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;
        private readonly IJobOpeningNoteRecipientProvider _jobOpeningNoteRecipientProvider;
        private readonly string _serverAddress;

        public JobOpeningNoteNotificationService(
            ISystemParameterService systemParameterService,
            IReliableEmailService reliableEmailService,
            IJobOpeningNoteRecipientProvider jobOpeningNoteRecipientProvider,
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService)
        {
            _reliableEmailService = reliableEmailService;
            _unitOfWorkService = unitOfWorkService;
            _jobOpeningNoteRecipientProvider = jobOpeningNoteRecipientProvider;
            _serverAddress = systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);
        }

        public void NotifyAboutNoteAdded(long noteId)
        {
            NotifyWatchers(TemplateCodes.NotifyAboutAddedNoteInJobOpeningBody, TemplateCodes.NotifyAboutAddedNoteInJobOpeningSubject, noteId);
            NotifyMentionedEmployees(TemplateCodes.NotifyAboutAddedNoteInJobOpeningBody, TemplateCodes.NotifyAboutAddedNoteInJobOpeningSubject, noteId);
        }

        public void NotifyAboutNoteEdited(long noteId)
        {
            NotifyWatchers(TemplateCodes.NotifyAboutEditedNoteInJobOpeningBody, TemplateCodes.NotifyAboutEditedNoteInJobOpeningSubject, noteId);
            NotifyMentionedEmployees(TemplateCodes.NotifyAboutEditedNoteInJobOpeningBody, TemplateCodes.NotifyAboutEditedNoteInJobOpeningSubject, noteId);
        }

        private void NotifyWatchers(string bodyTemplateCode, string subjectTemplateCode, long jobOpeningNoteId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var jobOpeningNote = GetJobOpeningNote(unitOfWork, jobOpeningNoteId);

                var recipients = _jobOpeningNoteRecipientProvider
                    .GetRecipients(jobOpeningNote, JobOpeningRecipientType.Watchers);

                if (!recipients.Any())
                {
                    return;
                }

                var reminderDto = GetReminderDto(jobOpeningNote, true);

                _reliableEmailService.EnqueueBatch(new EmailBatchDto
                {
                    BodyTemplateCode = bodyTemplateCode,
                    SubjectTemplateCode = subjectTemplateCode,
                    Model = reminderDto,
                    Recipients = recipients
                }, true);
            }
        }

        private void NotifyMentionedEmployees(string bodyTemplateCode, string subjectTemplateCode, long jobOpeningNoteId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var jobOpeningNote = GetJobOpeningNote(unitOfWork, jobOpeningNoteId);

                var recipients = _jobOpeningNoteRecipientProvider
                    .GetRecipients(jobOpeningNote, JobOpeningRecipientType.NoteMentionedEmployees);

                if (!recipients.Any())
                {
                    return;
                }

                var reminderDto = GetReminderDto(jobOpeningNote);

                _reliableEmailService.EnqueueBatch(new EmailBatchDto
                {
                    BodyTemplateCode = bodyTemplateCode,
                    SubjectTemplateCode = subjectTemplateCode,
                    Model = reminderDto,
                    Recipients = recipients
                }, true);
            }
        }

        private JobOpeningNoteReminderDto GetReminderDto(JobOpeningNote jobOpeningNote, bool isWatcher = false)
        {
            return new JobOpeningNoteReminderDto
            {
                JobOpeningName = jobOpeningNote.JobOpening.PositionName,
                JobOpeningUrl = RecruitmentUrlHelper.GetJobOpeningUrl(_serverAddress, jobOpeningNote.JobOpeningId),
                JobOpeningNoteUrl = RecruitmentUrlHelper.GetJobOpeningNoteUrl(_serverAddress, jobOpeningNote.Id, jobOpeningNote.JobOpeningId),
                NoteContent = jobOpeningNote.Content != null ? MentionItemHelper.ReplaceTokens(jobOpeningNote.Content, i => i.Label) : null,
                ModifiedByFullName = UserBusinessLogic.FullName.Call(jobOpeningNote.ModifiedBy),
                IsWatcher = isWatcher
            };
        }

        private JobOpeningNote GetJobOpeningNote(IUnitOfWork<IRecruitmentDbScope> unitOfWork, long jobOpeningNoteId)
        {
            return unitOfWork.Repositories.JobOpeningNotes
                .Include(s => s.JobOpening)
                .Include(s => s.JobOpening.Watchers)
                .Include(s => s.MentionedEmployees)
                .GetById(jobOpeningNoteId);
        }
    }
}
