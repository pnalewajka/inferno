﻿using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Dto.ReminderDto;
using Smt.Atomic.Business.Recruitment.Helpers;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Business.Recruitment.Resources;
using Smt.Atomic.Business.Recruitment.Services.Notification.RecruitmentProcessStepServices;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class RecruitmentProcessStepNotificationService : IRecruitmentProcessStepNotificationService
    {
        private readonly IReliableEmailService _reliableEmailService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;
        private readonly IRecruitmentProcessStepRecipientsProvider _recruitmentProcessStepsRecipientsProvider;
        private readonly string _serverAddress;

        public RecruitmentProcessStepNotificationService(
            IReliableEmailService reliableEmailService,
            ISystemParameterService systemParameterService,
            IPrincipalProvider principalProvider,
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService,
            IRecruitmentProcessStepRecipientsProvider recruitmentProcessStepsRecipientsProvider)
        {
            _reliableEmailService = reliableEmailService;
            _principalProvider = principalProvider;
            _unitOfWorkService = unitOfWorkService;
            _recruitmentProcessStepsRecipientsProvider = recruitmentProcessStepsRecipientsProvider;
            _serverAddress = systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);
        }

        public void NotifyAboutApproval(RecruitmentProcessStepType recruitmentProcessStepType, long recruitmentProcessStepId)
        {
            switch (recruitmentProcessStepType)
            {
                case RecruitmentProcessStepType.TechnicalReview:
                    {
                        NotifyRecruiterAboutTechnicalReviewerApproval(recruitmentProcessStepId);
                        NotifyWatcherAboutTechnicalReviewerApproval(recruitmentProcessStepId);
                        break;
                    }

                case RecruitmentProcessStepType.HiringManagerInterview:
                    {
                        NotifyRecruiterAboutHiringManagerApproval(recruitmentProcessStepId);
                        NotifyWatcherAboutHiringManagerApproval(recruitmentProcessStepId);
                        break;
                    }

                case RecruitmentProcessStepType.ClientInterview:
                    {
                        NotifyRecruiterAboutClientApproval(recruitmentProcessStepId);
                        break;
                    }

                case RecruitmentProcessStepType.HiringManagerResumeApproval:
                    {
                        NotifyRecruiterAboutHiringManagerApproval(recruitmentProcessStepId);
                        NotifyWatcherAboutHiringManagerApproval(recruitmentProcessStepId);
                        break;
                    }

                case RecruitmentProcessStepType.ClientResumeApproval:
                    {
                        NotifyWatcherAboutClientApproval(recruitmentProcessStepId);
                        break;
                    }

                case RecruitmentProcessStepType.HrCall:
                case RecruitmentProcessStepType.HrInterview:
                case RecruitmentProcessStepType.HiringDecision:
                case RecruitmentProcessStepType.ContractNegotiations:
                    break;
            }
        }

        public void NotifyAboutRejection(RecruitmentProcessStepType recruitmentProcessStepType, long recruitmentProcessStepId)
        {
            switch (recruitmentProcessStepType)
            {
                case RecruitmentProcessStepType.TechnicalReview:
                    {
                        NotifyRecruiterAboutTechnicalReviewerRejection(recruitmentProcessStepId);
                        NotifyWatcherAboutTechnicalReviewerRejection(recruitmentProcessStepId);
                        break;
                    }

                case RecruitmentProcessStepType.HiringManagerInterview:
                    {
                        NotifyRecruiterAboutHiringManagerRejection(recruitmentProcessStepId);
                        NotifyWatcherAboutHiringManagerRejection(recruitmentProcessStepId);
                        break;
                    }

                case RecruitmentProcessStepType.ClientInterview:
                    {
                        NotifyWatcherAboutClientRejection(recruitmentProcessStepId);
                        NotifyRecruiterAboutClientRejection(recruitmentProcessStepId);
                        break;
                    }

                case RecruitmentProcessStepType.ClientResumeApproval:
                    {
                        NotifyWatcherAboutClientRejection(recruitmentProcessStepId);
                        NotifyRecruiterAboutClientRejection(recruitmentProcessStepId);
                        break;
                    }

                case RecruitmentProcessStepType.HiringManagerResumeApproval:
                    {
                        NotifyRecruiterAboutHiringManagerRejection(recruitmentProcessStepId);
                        NotifyWatcherAboutHiringManagerRejection(recruitmentProcessStepId);
                        break;
                    }

                case RecruitmentProcessStepType.HrCall:
                case RecruitmentProcessStepType.HrInterview:
                case RecruitmentProcessStepType.HiringDecision:
                case RecruitmentProcessStepType.ContractNegotiations:
                    break;
            }
        }

        public void NotifyAllPartiesAboutStepCancelled(long recruitmentProcessStepId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                if (unitOfWork.Repositories.RecruitmentProcessSteps
                    .Any(rp => rp.Id == recruitmentProcessStepId && _principalProvider.Current.EmployeeId == rp.AssignedEmployeeId))
                {
                    return;
                }
            }

            NotifyAssigneeEmployeeAboutStepCancelled(recruitmentProcessStepId);
            NotifyWatcherAboutStepCancelled(recruitmentProcessStepId);
        }

        public void NotifyAboutPlannedStep(RecruitmentProcessStepType stepType, long stepId, long? previousStepId)
        {
            switch (stepType)
            {
                case RecruitmentProcessStepType.TechnicalReview:
                    {
                        NotifyTechnicalReviewerAboutPlannedInterview(stepId, previousStepId);
                        NotifyWatcherAboutPlannedInterview(stepId, previousStepId);
                        break;
                    }

                case RecruitmentProcessStepType.HiringManagerInterview:
                    {
                        NotifyHiringManagerAboutPlannedInterview(stepId, previousStepId);
                        NotifyWatcherAboutPlannedInterview(stepId, previousStepId);
                        break;
                    }

                case RecruitmentProcessStepType.ClientInterview:
                case RecruitmentProcessStepType.HrCall:
                case RecruitmentProcessStepType.HrInterview:
                    {
                        NotifyRecruiterAboutPlannedInterview(stepId, previousStepId);
                        NotifyWatcherAboutPlannedInterview(stepId, previousStepId);
                        break;
                    }

                case RecruitmentProcessStepType.HiringManagerResumeApproval:
                    {
                        NotifyHiringManagerAboutResumeSentForApproval(stepId, previousStepId);
                        NotifyWatcherAboutResumeSentForApproval(stepId, previousStepId);
                        break;
                    }

                case RecruitmentProcessStepType.ClientResumeApproval:
                    {
                        NotifyRecruiterAboutHiringManagerSendingResumeToClientForAppoval(stepId, previousStepId);
                        NotifyWatcherAboutHiringManagerSendingResumeToClientForAppoval(stepId, previousStepId);
                        break;
                    }

                case RecruitmentProcessStepType.HiringDecision:
                    {
                        NotifyHiringManagerAboutHiringDecisionNeeded(stepId, previousStepId);
                        NotifyWatcherAboutHiringDecisionNeeded(stepId, previousStepId);
                        break;
                    }

                case RecruitmentProcessStepType.ContractNegotiations:
                    {
                        NotityRecruiterAboutOfferMadeByHiringManager(stepId, previousStepId);
                        NofityWatcherAboutOfferMadeByHiringManager(stepId, previousStepId);
                        break;
                    }
            }
        }

        public void NotifyAboutProcessPutOnHold(long processStepId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                if (unitOfWork.Repositories.RecruitmentProcessSteps
                    .Any(rp => rp.Id == processStepId && _principalProvider.Current.EmployeeId == rp.AssignedEmployeeId))
                {
                    NotifyAssigneeEmployee(TemplateCodes.NotifyAboutProcessStepPutOnHoldBody, TemplateCodes.NotifyAboutProcessStepPutOnHoldSubject, processStepId);
                }
            }

            NotityRecruiter(TemplateCodes.NotifyAboutProcessStepPutOnHoldBody, TemplateCodes.NotifyAboutProcessStepPutOnHoldSubject, processStepId);
            NotifyWatchers(TemplateCodes.NotifyAboutProcessStepPutOnHoldBody, TemplateCodes.NotifyAboutProcessStepPutOnHoldSubject, processStepId);
        }

        public void NotifyAboutStepUpdate(long processStepId)
        {
            NotifyWatcherAboutStepUpdate(processStepId);
            NotifyAssigneeEmployeeAboutStepUpdate(processStepId);
        }

        private void NotityRecruiterAboutOfferMadeByHiringManager(long contractNegotiationStepId, long? hiringDecisionStepId)
        {
            NotityRecruiter(TemplateCodes.NotifyRecruiterAboutOfferMadeByHiringManagerBody,
                TemplateCodes.NotifyRecruiterAboutOfferMadeByHiringManagerSubject, contractNegotiationStepId, hiringDecisionStepId);
        }

        private void NotifyAssigneeEmployeeAboutStepUpdate(long updatedStepId)
        {
            NotifyAssigneeEmployee(TemplateCodes.NotifyDecisionMakerAboutStepUpdateBody,
                TemplateCodes.NotifyDecisionMakerAboutStepUpdateSubject, updatedStepId);
        }
        private void NotifyWatcherAboutStepUpdate(long updatedStepId)
        {
            NotifyWatchers(TemplateCodes.NotifyDecisionMakerAboutStepUpdateBody,
               TemplateCodes.NotifyDecisionMakerAboutStepUpdateSubject, updatedStepId);
        }

        private void NotifyAssigneeEmployeeAboutStepCancelled(long cancelledStepId)
        {
            NotifyAssigneeEmployee(TemplateCodes.NotifyDecisionMakerAboutStepCancelledBody,
                TemplateCodes.NotifyDecisionMakerAboutStepCancelledSubject, cancelledStepId);
        }

        private void NotifyHiringManagerAboutPlannedInterview(long hiringManagerStepId, long? previousStepId)
        {
            NotifyAssigneeEmployee(TemplateCodes.NotifyHiringManagerAboutPlannedInterviewBody,
                TemplateCodes.NotifyHiringManagerAboutPlannedInterviewSubject, hiringManagerStepId, previousStepId);
        }

        private void NotifyRecruiterAboutPlannedInterview(long stepId, long? previousStepId)
        {
            NotityRecruiter(TemplateCodes.NotifyHiringManagerAboutPlannedInterviewBody,
                TemplateCodes.NotifyHiringManagerAboutPlannedInterviewSubject, stepId, previousStepId);
        }

        private void NotifyHiringManagerAboutResumeSentForApproval(long hiringManagerStepId, long? previousStepId)
        {
            NotifyAssigneeEmployee(TemplateCodes.NotifyHiringManagerAboutResumeSentForApprovalBody,
                TemplateCodes.NotifyHiringManagerAboutResumeSentForApprovalSubject, hiringManagerStepId, previousStepId);
        }

        private void NotifyHiringManagerAboutHiringDecisionNeeded(long hiringManagerStepId, long? previousStepId)
        {
            NotifyAssigneeEmployee(TemplateCodes.NotifyHiringManagerAboutHiringDecisionNeededBody,
                TemplateCodes.NotifyHiringManagerAboutHiringDecisionNeededSubject, hiringManagerStepId, previousStepId);
        }

        private void NotifyTechnicalReviewerAboutPlannedInterview(long technicalReviewStepId, long? previousStepId)
        {
            NotifyAssigneeEmployee(TemplateCodes.NotifyTechnicalReviewerAboutPlannedInterviewBody,
                TemplateCodes.NotifyTechnicalReviewerAboutPlannedInterviewSubject, technicalReviewStepId, previousStepId);
        }

        private void NotifyRecruiterAboutClientApproval(long approvedStepId)
        {
            NotityRecruiter(TemplateCodes.NotifyRecruiterAboutClientApprovalBody,
                TemplateCodes.NotifyRecruiterAboutClientApprovalSubject, approvedStepId);
        }

        private void NotifyRecruiterAboutClientRejection(long rejectedStepId)
        {
            NotityRecruiter(TemplateCodes.NotifyRecruiterAboutClientRejectionBody,
                TemplateCodes.NotifyRecruiterAboutClientRejectionSubject, rejectedStepId);
        }

        private void NotifyRecruiterAboutHiringManagerApproval(long approvedStepId)
        {
            NotityRecruiter(TemplateCodes.NotifyRecruiterAboutHiringManagerApprovalBody,
                TemplateCodes.NotifyRecruiterAboutHiringManagerApprovalSubject, approvedStepId);
        }

        private void NotifyRecruiterAboutHiringManagerRejection(long rejectedStepId)
        {
            NotityRecruiter(TemplateCodes.NotifyRecruiterAboutHiringManagerRejectionBody,
                TemplateCodes.NotifyRecruiterAboutHiringManagerRejectionSubject, rejectedStepId);
        }

        private void NotifyRecruiterAboutHiringManagerSendingResumeToClientForAppoval(long clientStepId, long? previousStepId)
        {
            NotityRecruiter(TemplateCodes.NotifyRecruiterAboutHiringManagerSendingResumeToClientForAppovalBody,
                TemplateCodes.NotifyRecruiterAboutHiringManagerSendingResumeToClientForAppovalSubject, clientStepId, previousStepId);
        }

        private void NotifyRecruiterAboutTechnicalReviewerApproval(long approvedStepId)
        {
            NotityRecruiter(TemplateCodes.NotifyRecruiterAboutTechnicalReviewerApprovalBody,
                TemplateCodes.NotifyRecruiterAboutTechnicalReviewerApprovalSubject, approvedStepId);
        }

        private void NotifyRecruiterAboutTechnicalReviewerRejection(long rejectedStepId)
        {
            NotityRecruiter(TemplateCodes.NotifyRecruiterAboutTechnicalReviewerRejectionBody,
                TemplateCodes.NotifyRecruiterAboutTechnicalReviewerRejectionSubject, rejectedStepId);
        }

        private void NofityWatcherAboutOfferMadeByHiringManager(long contractNegotiationStepId, long? hiringDecisionStepId)
        {
            NotifyWatchers(TemplateCodes.NotifyRecruiterAboutOfferMadeByHiringManagerBody,
                TemplateCodes.NotifyRecruiterAboutOfferMadeByHiringManagerSubject, contractNegotiationStepId, hiringDecisionStepId);
        }

        private void NotifyWatcherAboutStepCancelled(long cancelledStepId)
        {
            NotifyWatchers(TemplateCodes.NotifyDecisionMakerAboutStepCancelledBody,
              TemplateCodes.NotifyDecisionMakerAboutStepCancelledSubject, cancelledStepId);
        }

        private void NotifyWatcherAboutPlannedInterview(long hiringManagerStepId, long? previousStepId)
        {
            NotifyWatchers(TemplateCodes.NotifyHiringManagerAboutPlannedInterviewBody,
                TemplateCodes.NotifyHiringManagerAboutPlannedInterviewSubject, hiringManagerStepId, previousStepId);
        }

        private void NotifyWatcherAboutResumeSentForApproval(long hiringManagerStepId, long? previousStepId)
        {
            NotifyWatchers(TemplateCodes.NotifyHiringManagerAboutResumeSentForApprovalBody,
                TemplateCodes.NotifyHiringManagerAboutResumeSentForApprovalSubject, hiringManagerStepId, previousStepId);
        }

        private void NotifyWatcherAboutHiringDecisionNeeded(long hiringManagerStepId, long? previousStepId)
        {
            NotifyWatchers(TemplateCodes.NotifyHiringManagerAboutHiringDecisionNeededBody,
                  TemplateCodes.NotifyHiringManagerAboutHiringDecisionNeededSubject, hiringManagerStepId, previousStepId);
        }

        private void NotifyWatcherAboutClientApproval(long approvedStepId)
        {
            NotifyWatchers(TemplateCodes.NotifyRecruiterAboutClientApprovalBody,
                TemplateCodes.NotifyRecruiterAboutClientApprovalSubject, approvedStepId);
        }

        private void NotifyWatcherAboutClientRejection(long rejectedStepId)
        {
            NotifyWatchers(TemplateCodes.NotifyRecruiterAboutClientRejectionBody,
                TemplateCodes.NotifyRecruiterAboutClientRejectionSubject, rejectedStepId);
        }

        private void NotifyWatcherAboutHiringManagerApproval(long approvedStepId)
        {
            NotifyWatchers(TemplateCodes.NotifyRecruiterAboutHiringManagerApprovalBody,
                TemplateCodes.NotifyRecruiterAboutHiringManagerApprovalSubject, approvedStepId);
        }

        private void NotifyWatcherAboutHiringManagerRejection(long rejectedStepId)
        {
            NotifyWatchers(TemplateCodes.NotifyRecruiterAboutHiringManagerRejectionBody,
               TemplateCodes.NotifyRecruiterAboutHiringManagerRejectionSubject, rejectedStepId);
        }

        private void NotifyWatcherAboutHiringManagerSendingResumeToClientForAppoval(long clientStepId, long? previousStepId)
        {
            NotifyWatchers(TemplateCodes.NotifyRecruiterAboutHiringManagerSendingResumeToClientForAppovalBody,
               TemplateCodes.NotifyRecruiterAboutHiringManagerSendingResumeToClientForAppovalSubject, clientStepId);
        }


        private void NotifyWatcherAboutTechnicalReviewerApproval(long approvedStepId)
        {
            NotifyWatchers(TemplateCodes.NotifyRecruiterAboutTechnicalReviewerApprovalBody,
                TemplateCodes.NotifyRecruiterAboutTechnicalReviewerApprovalSubject, approvedStepId);
        }

        private void NotifyWatcherAboutTechnicalReviewerRejection(long rejectedStepId)
        {
            NotifyWatchers(TemplateCodes.NotifyRecruiterAboutTechnicalReviewerRejectionBody,
                TemplateCodes.NotifyRecruiterAboutTechnicalReviewerRejectionSubject, rejectedStepId);
        }

        private void NotifyWatchers(string bodyTemplateCode, string subjectTemplateCode, long stepId, long? previousStepId = null)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recruitmentProcessStep = GetRecruitmentProcessStep(unitOfWork, stepId);

                var emailRecipients = _recruitmentProcessStepsRecipientsProvider.GetRecipients(recruitmentProcessStep, RecruitmentProcessRecipientType.Watchers);

                var reminderDto = GetReminderDto(recruitmentProcessStep, true);

                if (previousStepId.HasValue)
                {
                    reminderDto.PreviousProcessStepUrl =
                        RecruitmentUrlHelper.GetRecruitmentProcessStepUrl(_serverAddress, previousStepId.Value, reminderDto.ParentProcessId);
                    reminderDto.PreviousProcessStepName = unitOfWork.Repositories.RecruitmentProcessSteps.SelectById(stepId, r => r.Type).GetDescription();
                }

                var batch = new EmailBatchDto
                {
                    BodyTemplateCode = bodyTemplateCode,
                    SubjectTemplateCode = subjectTemplateCode,
                    Model = reminderDto,
                    Recipients = emailRecipients
                };

                _reliableEmailService.EnqueueBatch(batch, true);
            }
        }

        public void NotifyAssigneeEmployee(string bodyTemplateCode, string subjectTemplateCode, long stepId, long? previousStepId = null)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recruitmentProcessStep = GetRecruitmentProcessStep(unitOfWork, stepId);

                var recipientTypes = new[] { RecruitmentProcessRecipientType.StepAssignedEmployee, RecruitmentProcessRecipientType.OtherAttendees };
                var emailRecipients = _recruitmentProcessStepsRecipientsProvider.GetRecipients(recruitmentProcessStep, recipientTypes);

                var reminderDto = GetReminderDto(recruitmentProcessStep);

                if (previousStepId.HasValue)
                {
                    reminderDto.PreviousProcessStepUrl =
                        RecruitmentUrlHelper.GetRecruitmentProcessStepUrl(_serverAddress, previousStepId.Value, reminderDto.ParentProcessId);
                    reminderDto.PreviousProcessStepName = unitOfWork.Repositories.RecruitmentProcessSteps.SelectById(stepId, r => r.Type).GetDescription();
                }

                var batch = new EmailBatchDto
                {
                    BodyTemplateCode = bodyTemplateCode,
                    SubjectTemplateCode = subjectTemplateCode,
                    Model = reminderDto,
                    Recipients = emailRecipients
                };

                _reliableEmailService.EnqueueBatch(batch, true);
            }
        }

        public void NotityRecruiter(string bodyTemplateCode, string subjectTemplateCode, long stepId, long? previousStepId = null)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recruitmentProcessStep = GetRecruitmentProcessStep(unitOfWork, stepId);

                var reminderDto = GetReminderDto(recruitmentProcessStep);

                var emailRecipients = _recruitmentProcessStepsRecipientsProvider
                    .GetRecipients(recruitmentProcessStep, RecruitmentProcessRecipientType.Recruiter);

                var ccEmailRecipients = _recruitmentProcessStepsRecipientsProvider
                    .GetRecipients(recruitmentProcessStep, RecruitmentProcessRecipientType.RecruitmentLeaders, RecipientType.Cc);

                if (previousStepId.HasValue)
                {
                    reminderDto.PreviousProcessStepUrl = RecruitmentUrlHelper.GetRecruitmentProcessStepUrl(_serverAddress, previousStepId.Value, reminderDto.ParentProcessId);
                    reminderDto.PreviousProcessStepName = unitOfWork.Repositories.RecruitmentProcessSteps.SelectById(stepId, r => r.Type).GetDescription();
                }

                var batch = new EmailBatchDto
                {
                    BodyTemplateCode = bodyTemplateCode,
                    SubjectTemplateCode = subjectTemplateCode,
                    Model = reminderDto,
                    Recipients = emailRecipients.Union(ccEmailRecipients).ToList()
                };

                _reliableEmailService.EnqueueBatch(batch, true);
            }
        }

        private RecruitmentProcessStep GetRecruitmentProcessStep(IUnitOfWork<IRecruitmentDbScope> unitOfWork, long id)
        {
            return unitOfWork.Repositories.RecruitmentProcessSteps
                .Include(r => r.RecruitmentProcess)
                .Include(r => r.RecruitmentProcess.Candidate)
                .Include(r => r.RecruitmentProcess.JobOpening)
                .Include(r => r.ModifiedBy)
                .Include(r => r.AssignedEmployee)
                .Include(r => r.OtherAttendingEmployees)
                .Include(r => r.RecruitmentProcess.Watchers)
                .GetById(id);
        }

        private RecruitmentProcessStepReminderDto GetReminderDto(RecruitmentProcessStep recruitmentProcessStep, bool isForWatcher = false)
        {
            var recruitmentProcess = recruitmentProcessStep.RecruitmentProcess;

            return new RecruitmentProcessStepReminderDto
            {
                ProcessStepUrl = RecruitmentUrlHelper.GetRecruitmentProcessStepUrl(_serverAddress, recruitmentProcessStep.Id, recruitmentProcessStep.RecruitmentProcessId),
                ProcessUrl = RecruitmentUrlHelper.GetRecruitmentProcessUrl(_serverAddress, recruitmentProcessStep.RecruitmentProcessId),
                ProcessStepName = recruitmentProcessStep.Type.GetDescription(),
                ParentProcessId = recruitmentProcessStep.RecruitmentProcessId,
                ProcessName = RecruitmentProcessBusinessLogic.DisplayName.Call(recruitmentProcessStep.RecruitmentProcess),
                ModifiedByFullName = UserBusinessLogic.FullName.Call(recruitmentProcessStep.ModifiedBy),
                AssigneeEmployeeFullName = EmployeeBusinessLogic.FullNameOrDefault.Call(recruitmentProcessStep.AssignedEmployee) ?? RecruitmentProcessStepResources.UnspecifiedEmployee,
                PositionName = recruitmentProcess.JobOpening.PositionName,
                PositionUrl = RecruitmentUrlHelper.GetJobOpeningUrl(_serverAddress, recruitmentProcess.JobOpeningId),
                CandidateFullName = CandidateBusinessLogic.FullName.Call(recruitmentProcess.Candidate),
                Comment = recruitmentProcessStep.DecisionComment,
                PlannedOn = recruitmentProcessStep.PlannedOn,
                IsForWatcher = isForWatcher
            };
        }
    }
}
