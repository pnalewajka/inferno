﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Dto.JobOpeningReminder;
using Smt.Atomic.Business.Recruitment.Dto.Reminder.JobOpeningReminder;
using Smt.Atomic.Business.Recruitment.Helpers;
using Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification;
using Smt.Atomic.Business.Recruitment.SnapshotComparer;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services.Notification
{
    public class JobOpeningNotificationService : IJobOpeningNotificationService
    {
        private readonly ISystemParameterService _systemParameterService;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IEmployeeService _employeeService;
        private readonly IJobOpeningRecipientsProvider _recipientsProvider;

        public JobOpeningNotificationService(ISystemParameterService systemParameterService, IReliableEmailService reliableEmailService,
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService, IPrincipalProvider principalProvider, IEmployeeService employeeService, IJobOpeningRecipientsProvider recipientsProvider)
        {
            _systemParameterService = systemParameterService;
            _reliableEmailService = reliableEmailService;
            _unitOfWorkService = unitOfWorkService;
            _principalProvider = principalProvider;
            _employeeService = employeeService;
            _recipientsProvider = recipientsProvider;
        }

        public IList<EmailRecipientDto> SendApprovedReminderEmail(long jobOpeningId)
        {
            return SendEmail(jobOpeningId, TemplateCodes.JobOpeningApprovedSubject, TemplateCodes.JobOpeningApprovedBody,
                j => new JobOpeningApprovedReminderDto(),
                new[] { JobOpeningRecipientType.RecruitmentInternal, JobOpeningRecipientType.Creator },
                new[] { JobOpeningRecipientType.RecruitmentLeaders, JobOpeningRecipientType.Recruiters,
                    JobOpeningRecipientType.Owners, JobOpeningRecipientType.Creator, JobOpeningRecipientType.Watchers });
        }

        public IList<EmailRecipientDto> SendRejectedReminderEmail(long jobOpeningId)
        {
            return SendEmail(jobOpeningId, TemplateCodes.JobOpeningRejectedSubject, TemplateCodes.JobOpeningRejectedBody,
                j => new JobOpeningRejectedReminderDto
                {
                    Reason = j.RejectionReason
                },
                new[] { JobOpeningRecipientType.Creator },
                null);
        }

        public IList<EmailRecipientDto> SendClosedNotificationEmail(long jobOpeningId)
        {
            return SendEmail(jobOpeningId, TemplateCodes.JobOpeningClosedSubject, TemplateCodes.JobOpeningClosedBody,
                j => new JobOpeningClosedReminderDto
                {
                    ClosedReason = j.ClosedReason.GetDescription(),
                    ClosedComment = j.ClosedComment
                }, new[] { JobOpeningRecipientType.Creator, JobOpeningRecipientType.Recruiters, JobOpeningRecipientType.Watchers, JobOpeningRecipientType.RecruitmentLeaders },
                null);
        }

        public void SendAssignedRemainderEmail(long jobOpeningId, long[] assignedRecruiters)
        {
            SendEmail(jobOpeningId, TemplateCodes.JobOpeningAssignedSubject, TemplateCodes.JobOpeningAssignedBody,
                 jobOpening => new JobOpeningAssignmentReminderDto
                 {
                     ModifiedByFullName = UserBusinessLogic.FullName.Call(jobOpening.ModifiedBy),
                 }, assignedRecruiters, true);
        }

        public void SendUnassignedRemainderEmail(long jobOpeningId, long[] unassignedRecruiters)
        {
            SendEmail(jobOpeningId, TemplateCodes.JobOpeningUnassignedSubject, TemplateCodes.JobOpeningUnassignedBody,
               jobOpening => new JobOpeningAssignmentReminderDto
               {
                   ModifiedByFullName = UserBusinessLogic.FullName.Call(jobOpening.ModifiedBy),
               }, unassignedRecruiters, true);
        }

        public IList<EmailRecipientDto> SendForApprovalRemainderEmail(long jobOpeningId)
        {
            return SendEmail(jobOpeningId, TemplateCodes.JobOpeningApprovalSubject, TemplateCodes.JobOpeningApprovalBody,
                j => new JobOpeningApprovalReminderDto
                {
                    JobOpeningAuthorFullName = j.CreatedBy.FullName
                }, new[] { JobOpeningRecipientType.RecruitmentLeaders },
                null);
        }

        public IList<EmailRecipientDto> SendEmailWithChanges(long jobOpeningId, IDictionary<string, ValueChange> changes)
        {
            return SendEmail(jobOpeningId, TemplateCodes.JobOpeningEditChangesSubject, TemplateCodes.JobOpeningEditChangesBody,
                j => new JobOpeningChangesNotificationDto
                {
                    Changes = changes,
                    ChangedOnDate = j.ModifiedOn.ToString(CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern),
                    ChangedOnTime = j.ModifiedOn.ToString(CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern),
                    ChangedByFullName = j.ModifiedBy.FullName
                },
                new[] { JobOpeningRecipientType.Watchers, JobOpeningRecipientType.RecruitmentLeaders, JobOpeningRecipientType.Recruiters, JobOpeningRecipientType.Owners },
                null,
                false);
        }

        public IList<EmailRecipientDto> SendPutOnHoldRemainderEmail(long jobOpeningId, bool hasHighlyConfidentialProcesses)
        {
            var notConfidentialRecipients = SendEmail(jobOpeningId, TemplateCodes.NotifyJobOpeningPutOnHoldSubject,
                TemplateCodes.NotifyJobOpeningPutOnHoldBody, jo => StatusChangedReminderMapper.Invoke(jo, false),
                new[] { JobOpeningRecipientType.RecruitmentInternal, JobOpeningRecipientType.Creator, JobOpeningRecipientType.Watchers },
                new[] { JobOpeningRecipientType.Watchers });

            if (hasHighlyConfidentialProcesses)
            {
                SendEmail(jobOpeningId, TemplateCodes.NotifyJobOpeningPutOnHoldSubject,
                    TemplateCodes.NotifyJobOpeningPutOnHoldBody, jo => StatusChangedReminderMapper.Invoke(jo, true),
                    new[] { JobOpeningRecipientType.RecruitmentLeaders },
                    null);
            }

            return notConfidentialRecipients;
        }

        public IList<EmailRecipientDto> SendTakeOffHoldRemainderEmail(long jobOpeningId, bool hasHighlyConfidentialProcesses)
        {
            var notConfidentialRecipients = SendEmail(jobOpeningId, TemplateCodes.NotifyJobOpeningTakeOffHoldSubject,
                TemplateCodes.NotifyJobOpeningTakeOffHoldBody, jo => StatusChangedReminderMapper.Invoke(jo, false),
                  new[] { JobOpeningRecipientType.RecruitmentInternal, JobOpeningRecipientType.Creator, JobOpeningRecipientType.Watchers },
                  new[] { JobOpeningRecipientType.Watchers });

            if (hasHighlyConfidentialProcesses)
            {
                SendEmail(jobOpeningId, TemplateCodes.NotifyJobOpeningTakeOffHoldSubject,
                    TemplateCodes.NotifyJobOpeningTakeOffHoldBody, jo => StatusChangedReminderMapper.Invoke(jo, true),
                     new[] { JobOpeningRecipientType.RecruitmentLeaders },
                     null);
            }

            return notConfidentialRecipients;
        }


        public IList<EmailRecipientDto> SendReopenReminderEmail(long jobOpeningId)
        {
            return SendEmail(jobOpeningId, TemplateCodes.NotifyJobOpeningReopenSubject, TemplateCodes.NotifyJobOpeningReopenBody,
                j => new JobOpeningReopenReminderDto
                {
                    JobOpeningReopenedByFullName = j.ModifiedBy.FullName
                }, new[] { JobOpeningRecipientType.Creator, JobOpeningRecipientType.Recruiters, JobOpeningRecipientType.Owners, JobOpeningRecipientType.Watchers },
                null,
                true);
        }

        private Func<JobOpening, bool, JobOpeningStatusChangedReminderDto> StatusChangedReminderMapper =
            (jobOpening, includeHighlyConfidential) => new JobOpeningStatusChangedReminderDto
            {
                RecruitmentProcesses = jobOpening.RecruitmentProcesses.AsQueryable()
                   .Where(rp => includeHighlyConfidential || !RecruitmentProcessBusinessLogic.IsHighlyConfidential.Call(rp))
                   .Where(rp => RecruitmentProcessBusinessLogic.IsAffectedByJobOpeningChanges.Call(rp))
                   .Select(r => new RecruitmentProcessLinkDto
                   {
                       Id = r.Id,
                       Name = RecruitmentProcessBusinessLogic.DisplayName.Call(r)
                   }).ToList()
            };


        private IList<EmailRecipientDto> SendEmail<T>(long jobOpeningId, string subjectTemplate, string bodyTemplate,
            Func<JobOpening, T> mappingToReminderDto, IList<long> recipientEmployeeIds, bool seperateEmails = false)
            where T : BaseJobOpeningReminderDto
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var jobOpening = unitOfWork.Repositories.JobOpenings.GetById(jobOpeningId);

                var reminderDto = ParseJobOpeningToReminderDto(jobOpening, mappingToReminderDto);

                var recipients = unitOfWork.Repositories.Employees.Where(s => recipientEmployeeIds.Contains(s.Id)).Select(e => new EmailRecipientDto
                {
                    EmailAddress = e.Email,
                    FirstName = e.FirstName,
                    LastName = e.LastName
                }).ToList();

                var emailMessageDto = new EmailBatchDto
                {
                    Model = reminderDto,
                    SubjectTemplateCode = subjectTemplate,
                    BodyTemplateCode = bodyTemplate,
                    Recipients = recipients
                };

                _reliableEmailService.EnqueueBatch(emailMessageDto, seperateEmails);

                return recipients;
            }
        }

        private IList<EmailRecipientDto> SendEmail<T>(long jobOpeningId, string subjectTemplate, string bodyTemplate,
            Func<JobOpening, T> mappingToReminderDto, JobOpeningRecipientType[] regularRecipients, JobOpeningRecipientType[] confidentialRecipients, bool seperateEmails = false)
            where T : BaseJobOpeningReminderDto
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var jobOpening = unitOfWork.Repositories.JobOpenings.GetById(jobOpeningId);

                var recipients = (jobOpening.IsHighlyConfidential
                    ? confidentialRecipients
                    : regularRecipients) ?? regularRecipients;

                var recipientsList = _recipientsProvider.GetRecipients(jobOpening, recipients);

                if (!recipientsList.Any())
                {
                    return new List<EmailRecipientDto>();
                }

                var reminderDto = ParseJobOpeningToReminderDto(jobOpening, mappingToReminderDto);

                var emailMessageDto = new EmailBatchDto
                {
                    Model = reminderDto,
                    SubjectTemplateCode = subjectTemplate,
                    BodyTemplateCode = bodyTemplate,
                    Recipients = recipientsList.ToList()
                };

                _reliableEmailService.EnqueueBatch(emailMessageDto, seperateEmails);

                return recipientsList;
            }
        }

        private T ParseJobOpeningToReminderDto<T>(JobOpening jobOpening, Func<JobOpening, T> additionalMapping)
            where T : BaseJobOpeningReminderDto
        {
            var approver = _employeeService.GetEmployeeOrDefaultById(_principalProvider.Current.EmployeeId);
            var serverAddress = _systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);

            var jobOpeningReminderDto = additionalMapping.Invoke(jobOpening);

            jobOpeningReminderDto.JobOpeningApproverFullName = approver.FullName;
            jobOpeningReminderDto.ServerAddress = serverAddress;
            jobOpeningReminderDto.JobOpeningId = jobOpening.Id;
            jobOpeningReminderDto.JobOpeningName = jobOpening.PositionName;
            jobOpeningReminderDto.JobOpeningUrl = RecruitmentUrlHelper.GetJobOpeningUrl(serverAddress, jobOpening.Id);

            return jobOpeningReminderDto;
        }
    }
}