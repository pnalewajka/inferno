﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using Castle.Core.Internal;
using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.Business.Recruitment.Dto.Reminder;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification;
using Smt.Atomic.Business.Recruitment.Services.Notification;
using Smt.Atomic.Business.Recruitment.SnapshotComparer;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Presentation.Common.Interfaces;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class CandidateNotificationService : BaseNotificationService<Candidate, CandidateChangesNotificationDto, CandidateRecipientType>, ICandidateNotificationService
    {
        private readonly IClassMappingFactory _classMappingFactory;
        private readonly IEmployeeService _employeeService;
        private readonly IAlertService _alertService;
        private readonly ICityService _cityService;
        private readonly IJobProfileService _jobProfileService;
        private readonly ITimeService _timeService;

        public CandidateNotificationService(
            IClassMappingFactory classMappingFactory,
            IEmployeeService employeeService,
            IAlertService alertService,
            ICityService cityService,
            IJobProfileService jobProfileService,
            ISystemParameterService systemParameterService,
            IReliableEmailService reliableEmailService,
            ITimeService timeService,
            ICandidateRecipientProvider recipientProvider,
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService) : base(reliableEmailService, systemParameterService, unitOfWorkService, recipientProvider)
        {
            _classMappingFactory = classMappingFactory;
            _employeeService = employeeService;
            _alertService = alertService;
            _cityService = cityService;
            _jobProfileService = jobProfileService;
            _timeService = timeService;
        }

        public void NotifyAboutRecordChanges(long candidateId, CandidateDto orginalDto)
        {
            var changedValues = GetChangedValues(candidateId, orginalDto);

            if (changedValues.IsNullOrEmpty())
            {
                return;
            }

            Notify(TemplateCodes.CandidateEditChangesBody, TemplateCodes.CandidateEditChangesSubject, candidateId,
                new CandidateRecipientType[] { CandidateRecipientType.ContactCoordinator, CandidateRecipientType.Watchers }, changedValues);
        }

        protected override Candidate GetEntity(IUnitOfWork<IRecruitmentDbScope> unitOfWork, long id)
        {
            return unitOfWork.Repositories.Candidates
                .Include(c => c.ContactCoordinator)
                .GetById(id);
        }

        protected override CandidateChangesNotificationDto GetReminderDto(Candidate candidate, params object[] additionalData)
        {
            return new CandidateChangesNotificationDto()
            {
                CandidateId = candidate.Id,
                CandidateFullName = CandidateBusinessLogic.FullName.Call(candidate),
                ServerAddress = _serverAddress,
                Changes = (IDictionary<string, ValueChange>)additionalData[0],
                ChangedOnDate = candidate.ModifiedOn.ToString(CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern),
                ChangedOnTime = candidate.ModifiedOn.ToString(CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern),
                ChangedByFullName = UserBusinessLogic.FullName.Call(candidate.ModifiedBy)
            };
        }

        private IDictionary<string, ValueChange> GetChangedValues(long candidateId, CandidateDto orginalDto)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var candidate = unitOfWork.Repositories.Candidates.GetById(candidateId);

                var mapping = _classMappingFactory.CreateMapping<Candidate, CandidateDto>();
                var sourceDto = mapping.CreateFromSource(candidate);

                var snapshotComparer = new SnapshotComparer<CandidateDto>();

                SetComparedFields(snapshotComparer);
                SetRecordChangeConverters(snapshotComparer);

                snapshotComparer.Compare(orginalDto, sourceDto);

                return snapshotComparer.GetChangedValues();
            }
        }

        private void SetComparedFields(SnapshotComparer<CandidateDto> snapshotComparer)
        {
            snapshotComparer.AddComparedField(c => c.FirstName);
            snapshotComparer.AddComparedField(c => c.LastName);
            snapshotComparer.AddComparedField(c => c.ContactRestriction);
            snapshotComparer.AddComparedField(c => c.MobilePhone);
            snapshotComparer.AddComparedField(c => c.OtherPhone);
            snapshotComparer.AddComparedField(c => c.EmailAddress);
            snapshotComparer.AddComparedField(c => c.OtherEmailAddress);
            snapshotComparer.AddComparedField(c => c.SkypeLogin);
            snapshotComparer.AddComparedField(c => c.SocialLink);
            snapshotComparer.AddComparedField(c => c.NextFollowUpDate);
            snapshotComparer.AddComparedField(c => c.EmployeeStatus);
            snapshotComparer.AddComparedField(c => c.CanRelocate);
            snapshotComparer.AddComparedField(c => c.RelocateDetails);
            snapshotComparer.AddComparedField(c => c.CityIds);
            snapshotComparer.AddComparedField(c => c.JobProfileIds);
            snapshotComparer.AddComparedField(c => c.ResumeTextContent);
            snapshotComparer.AddComparedField(c => c.ContactCoordinatorId);
            snapshotComparer.AddComparedField(c => c.ConsentCoordinatorId);
        }

        private void SetRecordChangeConverters(SnapshotComparer<CandidateDto> snapshotComparer)
        {
            snapshotComparer.AddDisplayNameConverter<long[]>(
                c => c.CityIds,
                ids => string.Join(", ", _cityService.GetCitiesByIds(ids).Select(l => l.Name)));

            snapshotComparer.AddDisplayNameConverter<long[]>(
                c => c.JobProfileIds,
                ids => string.Join(", ", _jobProfileService.GetJobProfilesByIds(ids)?.Select(l => l.Name)));

            snapshotComparer.AddDisplayNameConverter<long>(
                c => c.ContactCoordinatorId,
                id => _employeeService.GetEmployeeOrDefaultById(id).FullName);

            snapshotComparer.AddDisplayNameConverter<long>(
                c => c.ConsentCoordinatorId,
                id => _employeeService.GetEmployeeOrDefaultById(id).FullName);
        }
    }
}
