﻿using System.Data.Entity;
using System.Linq;
using System.Web;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Dto.Reminder;
using Smt.Atomic.Business.Recruitment.Helpers;
using Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services.Notification
{
    public class CandidateNoteNotificationService
        : BaseNotificationService<CandidateNote, CandidateNoteReminderDto, CandidateRecipientType>, ICandidateNoteNotificationService
    {
        public CandidateNoteNotificationService(
            IReliableEmailService reliableEmailService,
            ISystemParameterService systemParameterService,
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService,
            ICandidateNoteRecipientProvider recipientProvider)
            : base(reliableEmailService, systemParameterService, unitOfWorkService, recipientProvider)
        {
        }

        public void NotifyAboutAdded(long candidateNoteId)
        {
            Notify(TemplateCodes.NotifyAboutAddedNoteInCandidateBody, TemplateCodes.NotifyAboutAddedNoteInCandidateSubject,
                candidateNoteId, new CandidateRecipientType[] {
                    CandidateRecipientType.Watchers,
                    CandidateRecipientType.NoteMentionedEmployee,
                    CandidateRecipientType.ContactCoordinator
                });
        }

        public void NotifyAboutEdited(long candidateNoteId)
        {
            Notify(TemplateCodes.NotifyAboutEditedNoteInCandidateBody, TemplateCodes.NotifyAboutEditedNoteInCandidateSubject,
                candidateNoteId, new CandidateRecipientType[] {
                    CandidateRecipientType.Watchers,
                    CandidateRecipientType.NoteMentionedEmployee,
                    CandidateRecipientType.ContactCoordinator
                });
        }

        protected override CandidateNote GetEntity(IUnitOfWork<IRecruitmentDbScope> unitOfWork, long id)
        {
            return unitOfWork.Repositories.CandidateNotes
                        .Include(r => r.Candidate)
                        .Where(r => r.Id == id)
                        .Single();
        }

        protected override CandidateNoteReminderDto GetReminderDto(CandidateNote candidateNote, params object[] additionalData)
        {
            return new CandidateNoteReminderDto
            {
                CandidateName = $"{candidateNote.Candidate.FirstName} {candidateNote.Candidate.LastName}",
                CandidateUrl = RecruitmentUrlHelper.GetCandidateUrl(_serverAddress, candidateNote.CandidateId),
                CandidateNoteUrl = RecruitmentUrlHelper.GetCandidateNoteUrl(_serverAddress, candidateNote.Id, candidateNote.CandidateId),
                ModifiedByFullName = $"{candidateNote.ModifiedBy.FirstName} {candidateNote.ModifiedBy.LastName}",
                NoteContent = HttpUtility.HtmlDecode(MentionItemHelper.ReplaceTokens(candidateNote.Content, i => i.Label)),
            };
        }
    }
}
