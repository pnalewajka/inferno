﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Recruitment.Dto.Reminder.JobOpeningReminder;
using Smt.Atomic.Business.Recruitment.Resources;
using Smt.Atomic.Business.Recruitment.Services.Notification.RecipientsProviders;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Services.Notification
{
    public class JobOpeningRecipientsProvider : BaseRecipientProvider<JobOpening, JobOpeningRecipientType>, IJobOpeningRecipientsProvider
    {
        public JobOpeningRecipientsProvider(ISystemParameterService systemParameterService) : base(systemParameterService)
        {
        }

        public override IList<EmailRecipientDto> GetRecipients(JobOpening jobOpening, JobOpeningRecipientType recipients, RecipientType recipientType = RecipientType.To)
        {
            switch (recipients)
            {
                case JobOpeningRecipientType.Creator:
                    return new List<EmailRecipientDto>
                    {
                        new EmailRecipientDto
                        {
                            EmailAddress = jobOpening.CreatedBy.Email,
                            FullName = jobOpening.CreatedBy.FullName
                        }
                    };

                case JobOpeningRecipientType.RecruitmentLeaders:
                    return new List<EmailRecipientDto> {
                        new EmailRecipientDto
                        {
                            FirstName = JobOpeningResources.RecruitmentLeadersEmailsRecipient,
                            EmailAddress = _systemParameterService.GetParameter<string>(ParameterKeys.JobOpeningRecruitmentLeadersEmailAddress)
                        }
                    };

                case JobOpeningRecipientType.RecruitmentInternal:
                    return new List<EmailRecipientDto> {
                        new EmailRecipientDto
                        {
                            EmailAddress = _systemParameterService.GetParameter<string>(ParameterKeys.JobOpeningRecruitmentInternalEmailAddress)
                        }
                    };

                case JobOpeningRecipientType.Watchers:
                    return jobOpening.Watchers.Select(w => new EmailRecipientDto
                    {
                        EmailAddress = w.Email,
                        FullName = w.DisplayName
                    }).ToList();

                case JobOpeningRecipientType.Owners:
                    return jobOpening.RecruitmentOwners.Select(o => new EmailRecipientDto
                    {
                        EmailAddress = o.Email,
                        FullName = o.DisplayName
                    }).ToList();

                case JobOpeningRecipientType.Recruiters:
                    return jobOpening.Recruiters.Select(r => new EmailRecipientDto
                    {
                        EmailAddress = r.Email,
                        FullName = r.DisplayName
                    }).ToList();

                default:
                    return new List<EmailRecipientDto>();
            }
        }
    }
}
