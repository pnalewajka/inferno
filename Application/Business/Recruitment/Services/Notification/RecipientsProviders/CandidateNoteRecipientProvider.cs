﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Recruitment.Dto.Reminder;
using Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Services.Notification.RecipientsProviders
{
    public class CandidateNoteRecipientProvider
        : BaseRecipientProvider<CandidateNote, CandidateRecipientType>, ICandidateNoteRecipientProvider
    {
        private ICandidateRecipientProvider _candidateNotificationRecipientProvider;

        public CandidateNoteRecipientProvider(
            ICandidateRecipientProvider candidateNotificationRecipientProvider,
            ISystemParameterService systemParameterService)
            : base(systemParameterService)
        {
            _candidateNotificationRecipientProvider = candidateNotificationRecipientProvider;
        }

        public override IList<EmailRecipientDto> GetRecipients(CandidateNote entity, CandidateRecipientType recipient, RecipientType recipientType = RecipientType.To)
        {
            if (recipient == CandidateRecipientType.NoteMentionedEmployee)
            {
                return entity.MentionedEmployees.Select(me => new EmailRecipientDto
                {
                    EmailAddress = me.Email,
                    FirstName = me.FirstName,
                    LastName = me.LastName
                }).ToList();
            }

            return _candidateNotificationRecipientProvider.GetRecipients(entity.Candidate, recipient, recipientType);
        }
    }
}
