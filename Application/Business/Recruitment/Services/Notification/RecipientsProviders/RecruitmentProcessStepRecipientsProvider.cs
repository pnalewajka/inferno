﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Recruitment.Dto.ReminderDto;
using Smt.Atomic.Business.Recruitment.Services.Notification.RecipientsProviders;
using Smt.Atomic.Business.Recruitment.Services.Notification.RecruitmentProcessServices;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Services.Notification.RecruitmentProcessStepServices
{
    public class RecruitmentProcessStepRecipientsProvider : BaseRecipientProvider<RecruitmentProcessStep, RecruitmentProcessRecipientType>, IRecruitmentProcessStepRecipientsProvider
    {
        private readonly IRecruitmentProcessRecipientsProvider _recruitmentProcessRecipientsProvider;

        public RecruitmentProcessStepRecipientsProvider(IRecruitmentProcessRecipientsProvider recruitmentProcessRecipientsProvider,
            ISystemParameterService systemParameterService) : base(systemParameterService)
        {
            _recruitmentProcessRecipientsProvider = recruitmentProcessRecipientsProvider;
        }

        public override IList<EmailRecipientDto> GetRecipients(RecruitmentProcessStep recruitmentProcessStep, RecruitmentProcessRecipientType processRecipientType, RecipientType recipientType = RecipientType.To)
        {
            switch (processRecipientType)
            {
                case RecruitmentProcessRecipientType.StepAssignedEmployee:
                    {
                        var assignedEmployee = recruitmentProcessStep.AssignedEmployee;

                        if (assignedEmployee == null)
                        {
                            return new List<EmailRecipientDto>();
                        }

                        return new List<EmailRecipientDto>
                        {
                            new EmailRecipientDto
                            {
                                EmailAddress = assignedEmployee.Email,
                                FullName = EmployeeBusinessLogic.FullName.Call(assignedEmployee)
                            }
                        };
                    }

                case RecruitmentProcessRecipientType.OtherAttendees:
                    {
                        var stepAssignedEmployees = recruitmentProcessStep
                            .OtherAttendingEmployees
                            .Distinct()
                            .ToList();

                        return stepAssignedEmployees.Select(e => new EmailRecipientDto
                        {
                            EmailAddress = e.Email,
                            FirstName = e.FirstName,
                            LastName = e.LastName
                        }).ToList();
                    }

                default:
                    return _recruitmentProcessRecipientsProvider.GetRecipients(recruitmentProcessStep.RecruitmentProcess, processRecipientType, recipientType);
            }
        }
    }
}
