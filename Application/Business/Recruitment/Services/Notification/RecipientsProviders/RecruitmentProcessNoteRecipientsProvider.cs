﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Recruitment.Dto.ReminderDto;
using Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification;
using Smt.Atomic.Business.Recruitment.Services.Notification.RecruitmentProcessServices;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Services.Notification.RecipientsProviders
{
    public class RecruitmentProcessNoteRecipientsProvider : BaseRecipientProvider<RecruitmentProcessNote, RecruitmentProcessRecipientType>, IRecruitmentProcessNoteRecipientProvider
    {
        private readonly IRecruitmentProcessRecipientsProvider _recruitmentProcessRecipientsProvider;

        public RecruitmentProcessNoteRecipientsProvider(IRecruitmentProcessRecipientsProvider recruitmentProcessRecipientsProvider,
            ISystemParameterService systemParameterService) : base(systemParameterService)
        {
            _recruitmentProcessRecipientsProvider = recruitmentProcessRecipientsProvider;
        }

        public override IList<EmailRecipientDto> GetRecipients(RecruitmentProcessNote recruitmentProcessNote, RecruitmentProcessRecipientType recipients, RecipientType recipientType = RecipientType.To)
        {
            if (recipients == RecruitmentProcessRecipientType.NoteMentionedEmployee)
            {
                var mentionedEmployee = recruitmentProcessNote.MentionedEmployees;

                return mentionedEmployee
                           .Distinct()
                           .Select(s => new EmailRecipientDto
                           {
                               EmailAddress = s.Email,
                               FullName = EmployeeBusinessLogic.FullName.Call(s)
                           }).ToList();
            }

            return _recruitmentProcessRecipientsProvider.GetRecipients(recruitmentProcessNote.RecruitmentProcess, recipients, recipientType);
        }
    }
}
