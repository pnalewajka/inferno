﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Recruitment.Dto.Reminder.JobOpeningReminder;
using Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Services.Notification.RecipientsProviders
{
    public class JobOpeningNoteRecipientProvider : JobOpeningRecipientsProvider, IJobOpeningNoteRecipientProvider
    {
        public JobOpeningNoteRecipientProvider(ISystemParameterService systemParameterService) : base(systemParameterService)
        {
        }

        public IList<EmailRecipientDto> GetRecipients(JobOpeningNote jobOpeningNote,
            JobOpeningRecipientType recipients, RecipientType recipientType = RecipientType.To)
        {
            switch (recipients)
            {
                case JobOpeningRecipientType.NoteMentionedEmployees:
                    return jobOpeningNote.MentionedEmployees.Select(w => new EmailRecipientDto
                    {
                        EmailAddress = w.Email,
                        FullName = w.DisplayName
                    }).ToList();

                default:
                    return GetRecipients(jobOpeningNote.JobOpening, recipients, recipientType);
            }
        }

        public IList<EmailRecipientDto> GetRecipients(JobOpeningNote recruitmentProcessNote, JobOpeningRecipientType[] recipients, RecipientType recipientType = RecipientType.To)
        {
            var emailRecipients = recipients
                .SelectMany(r => GetRecipients(recruitmentProcessNote, r))
                .Distinct()
                .ToList();

            return emailRecipients;
        }
    }
}
