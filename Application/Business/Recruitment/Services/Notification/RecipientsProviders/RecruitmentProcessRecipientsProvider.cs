﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Dto.ReminderDto;
using Smt.Atomic.Business.Recruitment.Services.Notification.RecipientsProviders;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Services.Notification.RecruitmentProcessServices
{
    public class RecruitmentProcessRecipientsProvider : BaseRecipientProvider<RecruitmentProcess, RecruitmentProcessRecipientType>, IRecruitmentProcessRecipientsProvider
    {
        public RecruitmentProcessRecipientsProvider(ISystemParameterService systemParameterService) : base(systemParameterService)
        {
        }

        public override IList<EmailRecipientDto> GetRecipients(RecruitmentProcess recruitmentProcess, RecruitmentProcessRecipientType recipient, RecipientType recipientType = RecipientType.To)
        {
            switch (recipient)
            {
                case RecruitmentProcessRecipientType.AllDecisionMakers:
                    {
                        var decisionMakers = GetDecisionMakers(recruitmentProcess);

                        return decisionMakers
                            .Distinct()
                            .Select(s => new EmailRecipientDto
                            {
                                EmailAddress = s.Email,
                                FullName = EmployeeBusinessLogic.FullName.Call(s)
                            }).ToList();
                    }

                case RecruitmentProcessRecipientType.Recruiter:
                    {
                        var recruiter = recruitmentProcess.Recruiter;

                        return new List<EmailRecipientDto>
                        {
                            new EmailRecipientDto
                            {
                                EmailAddress = recruiter.Email,
                                FullName = EmployeeBusinessLogic.FullName.Call(recruiter)
                            }
                        };
                    }

                case RecruitmentProcessRecipientType.Watchers:
                    {
                        return recruitmentProcess.Watchers.Select(w => new EmailRecipientDto
                        {
                            EmailAddress = w.Email,
                            FullName = w.DisplayName,
                            Type = recipientType
                        }).ToList();
                    }

                case RecruitmentProcessRecipientType.RecruitmentLeaders:
                    {
                        return new List<EmailRecipientDto> {
                            new EmailRecipientDto
                            {
                                EmailAddress = _systemParameterService.GetParameter<string>(ParameterKeys.JobOpeningRecruitmentLeadersEmailAddress),
                                Type = RecipientType.Cc
                            }
                        };
                    }

                default:
                    return new List<EmailRecipientDto>();
            }
        }

        private IList<Employee> GetDecisionMakers(RecruitmentProcess recruitmentProcess)
        {
            var assignedEmployees = recruitmentProcess.ProcessSteps
                .Where(s => RecruitmentProcessStepBusinessLogic.IsSuitableForNotifications.Call(s))
                .ToList()
                .SelectMany(s =>
                {
                    if (s.AssignedEmployee != null)
                    {
                        s.OtherAttendingEmployees.Add(s.AssignedEmployee);
                    }

                    return s.OtherAttendingEmployees;
                })
                .Distinct()
                .ToList();

            var jobOpeningDecisionMakers = recruitmentProcess.JobOpening.DecisionMakerEmployee;

            if (jobOpeningDecisionMakers != null
                && !assignedEmployees.Any(s => s.Email == jobOpeningDecisionMakers.Email))
            {
                assignedEmployees.Add(jobOpeningDecisionMakers);
            }

            return assignedEmployees;
        }
    }
}
