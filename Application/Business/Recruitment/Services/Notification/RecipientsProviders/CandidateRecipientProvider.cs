﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Recruitment.Dto.Reminder;
using Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Services.Notification.RecipientsProviders
{
    public class CandidateRecipientProvider : BaseRecipientProvider<Candidate, CandidateRecipientType>, ICandidateRecipientProvider
    {
        public CandidateRecipientProvider(ISystemParameterService systemParameterService) : base(systemParameterService)
        {
        }

        public override IList<EmailRecipientDto> GetRecipients(Candidate candidate, CandidateRecipientType recipients, RecipientType recipientType = RecipientType.To)
        {
            switch (recipients)
            {
                case CandidateRecipientType.Watchers:
                    {
                        return candidate.Watchers.Select(w => new EmailRecipientDto
                        {
                            EmailAddress = w.Email,
                            FullName = w.DisplayName,
                            Type = recipientType
                        }).ToList();
                    }

                case CandidateRecipientType.ContactCoordinator:
                    {
                        return candidate.ContactCoordinator == null
                                ? new List<EmailRecipientDto>()
                                : new List<EmailRecipientDto>
                                {
                                    new EmailRecipientDto
                                    {
                                        EmailAddress = candidate.ContactCoordinator.Email,
                                        FirstName = candidate.ContactCoordinator.FirstName,
                                        LastName = candidate.ContactCoordinator.LastName
                                    }
                                };
                    }

                default:
                    return new List<EmailRecipientDto> { };
            }
        }
    }
}
