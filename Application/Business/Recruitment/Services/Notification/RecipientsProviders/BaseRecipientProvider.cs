﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.Business.Recruitment.Services.Notification.RecipientsProviders
{
    public abstract class BaseRecipientProvider<TEntity, TRecipient> : IRecipientProvider<TEntity, TRecipient>
    {
        protected readonly ISystemParameterService _systemParameterService;

        public BaseRecipientProvider(ISystemParameterService systemParameterService)
        {
            _systemParameterService = systemParameterService;
        }

        public virtual IList<EmailRecipientDto> GetRecipients(TEntity entity, TRecipient[] recipients, RecipientType recipientType = RecipientType.To)
        {
            var emailRecipients = recipients
              .SelectMany(r => GetRecipients(entity, r))
              .GroupBy(r => r.EmailAddress)
              .Select(r => r.First())
              .ToList();

            return emailRecipients;
        }

        public abstract IList<EmailRecipientDto> GetRecipients(TEntity entity, TRecipient recipient, RecipientType recipientType = RecipientType.To);
    }
}
