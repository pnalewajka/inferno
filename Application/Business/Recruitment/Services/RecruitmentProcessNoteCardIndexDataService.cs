﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Helpers;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Business.Recruitment.Services.Notification.RecruitmentProcessNoteServices;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class RecruitmentProcessNoteCardIndexDataService : CardIndexDataService<RecruitmentProcessNoteDto, RecruitmentProcessNote, IRecruitmentDbScope>, IRecruitmentProcessNoteCardIndexDataService
    {
        private readonly ICardIndexServiceDependencies<IRecruitmentDbScope> _dependencies;
        private readonly IRecruitmentProcessNoteNotificationService _recruitmentProcessNoteNotificationService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public RecruitmentProcessNoteCardIndexDataService(
            ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies,
            IRecruitmentProcessNoteNotificationService recruitmentProcessNoteNotificationService)
            : base(dependencies)
        {
            _dependencies = dependencies;
            _recruitmentProcessNoteNotificationService = recruitmentProcessNoteNotificationService;
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<RecruitmentProcessNote, RecruitmentProcessNoteDto, IRecruitmentDbScope> eventArgs)
        {
            RecruitmentProcessHelper.SetRecruitmentProcessLastActivity(eventArgs.UnitOfWork, eventArgs.InputEntity.RecruitmentProcessId, TimeService.GetCurrentTime(), CurrentEmployeeId);

            base.OnRecordAdding(eventArgs);

            AdjustMentionedEmployeeId(eventArgs.InputEntity, eventArgs.UnitOfWork);

            eventArgs.InputEntity.MentionedEmployees = EntityMergingService.CreateEntitiesFromIds<Employee, IRecruitmentDbScope>(eventArgs.UnitOfWork, eventArgs.InputEntity.MentionedEmployees.GetIds());
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<RecruitmentProcessNote, RecruitmentProcessNoteDto> recordEditedEventArgs)
        {
            _recruitmentProcessNoteNotificationService.NotifyAboutEditedNoteInRecruitmentProcessNotes(recordEditedEventArgs.InputEntity.Id);

            base.OnRecordEdited(recordEditedEventArgs);
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<RecruitmentProcessNote, RecruitmentProcessNoteDto> recordAddedEventArgs)
        {
            _recruitmentProcessNoteNotificationService.NotifyAboutAddedNoteInRecruitmentProcessNotes(recordAddedEventArgs.InputEntity.Id);

            base.OnRecordAdded(recordAddedEventArgs);
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<RecruitmentProcessNote, RecruitmentProcessNoteDto, IRecruitmentDbScope> eventArgs)
        {
            RecruitmentProcessHelper.SetRecruitmentProcessLastActivity(eventArgs.UnitOfWork, eventArgs.InputEntity.RecruitmentProcessId, TimeService.GetCurrentTime(), CurrentEmployeeId);

            base.OnRecordEditing(eventArgs);

            AdjustMentionedEmployeeId(eventArgs.InputEntity, eventArgs.UnitOfWork);

            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.MentionedEmployees, eventArgs.InputEntity.MentionedEmployees);
        }

        public override IQueryable<RecruitmentProcessNote> ApplyContextFiltering(IQueryable<RecruitmentProcessNote> records, object context)
        {
            if (context is ParentIdContext parentIdContext)
            {
                return records.Where(c => c.RecruitmentProcessId == parentIdContext.ParentId);
            }

            return base.ApplyContextFiltering(records, context);
        }

        protected override NamedFilters<RecruitmentProcessNote> NamedFilters
            => new NamedFilters<RecruitmentProcessNote>(CardIndexServiceHelper.BuildSoftDeletableFilters<RecruitmentProcessNote>());

        protected override IEnumerable<Expression<Func<RecruitmentProcessNote, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return n => n.Content;
        }

        private long CurrentEmployeeId => _dependencies.PrincipalProvider.Current.EmployeeId;

        private void AdjustMentionedEmployeeId(RecruitmentProcessNote recruitmentProcessNote, IUnitOfWork<IRecruitmentDbScope> unitOfWork)
        {
            var employeeIds = MentionItemHelper.GetTokenIds(recruitmentProcessNote.Content).Distinct();

            recruitmentProcessNote.MentionedEmployees = unitOfWork.Repositories.Employees.GetByIds(employeeIds).ToList();
        }
    }
}
