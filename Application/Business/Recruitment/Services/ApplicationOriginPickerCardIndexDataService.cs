﻿using System.Linq;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class ApplicationOriginPickerCardIndexDataService : ApplicationOriginCardIndexDataService, IApplicationOriginPickerCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public ApplicationOriginPickerCardIndexDataService(ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies)
            : base(dependencies)
        {
        }

        public override IQueryable<ApplicationOrigin> ApplyContextFiltering(IQueryable<ApplicationOrigin> records, object context)
        {
            if (context is ApplicationOriginPickerContext applicationOriginContext)
            {
                if (applicationOriginContext.OriginType.HasValue)
                {
                    return records.Where(o => o.OriginType == applicationOriginContext.OriginType.Value);
                }

                if (applicationOriginContext.SkipRecommendation)
                {
                    return records.Where(o => o.OriginType != ApplicationOriginType.Recommendation);
                }
            }

            return base.ApplyContextFiltering(records, context);
        }
    }
}
