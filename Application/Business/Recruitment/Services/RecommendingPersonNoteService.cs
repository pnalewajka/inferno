﻿using System;
using System.Linq;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class RecommendingPersonNoteService : IRecommendingPersonNoteService
    {
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;
        private readonly IClassMappingFactory _classMappingFactory;

        public RecommendingPersonNoteService(
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService,
            IClassMappingFactory classMappingFactory)
        {
            _unitOfWorkService = unitOfWorkService;
            _classMappingFactory = classMappingFactory;
        }

        public void AddNote(RecommendingPersonNoteDto recommendingPersonNotesDto)
        {
            if (string.IsNullOrEmpty(recommendingPersonNotesDto.Content))
            {
                throw new ArgumentNullException(nameof(recommendingPersonNotesDto.Content));
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                if (!unitOfWork.Repositories.RecommendingPersons.Any(r => r.Id == recommendingPersonNotesDto.RecommendingPersonId))
                {
                    throw new ArgumentException(nameof(recommendingPersonNotesDto.RecommendingPersonId));
                }

                var mapping = _classMappingFactory.CreateMapping<RecommendingPersonNoteDto, RecommendingPersonNote>();
                var recommendingPersonNote = mapping.CreateFromSource(recommendingPersonNotesDto);

                unitOfWork.Repositories.RecommendingPersonNotes.Add(recommendingPersonNote);
                unitOfWork.Commit();
            }
        }
    }
}
