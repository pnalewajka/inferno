﻿using System.Linq;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class CandidateTechnicalReviewCardIndexDataService : TechnicalReviewCardIndexDataService, ICandidateTechnicalReviewCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public CandidateTechnicalReviewCardIndexDataService(ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies,
            IDataActivityLogService dataActivityLogService)
            : base(dependencies, dataActivityLogService)
        {
        }

        public override IQueryable<TechnicalReview> ApplyContextFiltering(IQueryable<TechnicalReview> records, object context)
        {
            if (context is ParentIdContext parentIdContext)
            {
                return records.Where(c => c.RecruitmentProcess.CandidateId == parentIdContext.ParentId);
            }

            return base.ApplyContextFiltering(records, context);
        }
    }
}
