﻿using System.Linq;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class RecommendingPersonPickerCardIndexDataService : RecommendingPersonCardIndexDataService,
        IRecommendingPersonPickerCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public RecommendingPersonPickerCardIndexDataService(
            ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies,
            IDataActivityLogService dataActivityLogService,
            IEmployeeService employeeService)
            : base(dependencies, dataActivityLogService, employeeService)
        {
        }

        public override IQueryable<RecommendingPerson> ApplyContextFiltering(IQueryable<RecommendingPerson> records, object context)
        {
            if (context is RecommendingPersonPickerContext recommendingPersonPickerContext)
            {
                records = ApplyRecommendingPersonPickerContextFiltering(records, recommendingPersonPickerContext);
            }

            if (context is RecommendingPersonSearchContext recommendingPersonSearchContext)
            {
                records = ApplyRecommendingPersonSearchContextFiltering(records, recommendingPersonSearchContext);
            }

            return records;
        }

        private IQueryable<RecommendingPerson> ApplyRecommendingPersonSearchContextFiltering(IQueryable<RecommendingPerson> records, RecommendingPersonSearchContext context)
        {
            if (!string.IsNullOrWhiteSpace(context.FirstName))
            {
                records = records.Where(c => c.FirstName.Contains(context.FirstName));
            }

            if (!string.IsNullOrWhiteSpace(context.LastName))
            {
                var lastNameParts = HumanNameHelper.GetLastNameParts(context.LastName);

                records = records.Where(c => lastNameParts.Any(p => c.LastName.Contains(p)));
            }

            if (!string.IsNullOrWhiteSpace(context.EmailAddress))
            {
                records = records.Where(c => c.EmailAddress == context.EmailAddress);
            }

            if (!string.IsNullOrWhiteSpace(context.PhoneNumber))
            {
                records = records.Where(c => c.PhoneNumber == context.PhoneNumber);
            }

            return records;
        }

        private IQueryable<RecommendingPerson> ApplyRecommendingPersonPickerContextFiltering(IQueryable<RecommendingPerson> records, RecommendingPersonPickerContext context)
        {
            if (context.OnlyValidConsent)
            {
                var today = TimeService.GetCurrentDate();

                return records.Where(RecommendingPersonBusinessLogic.HasValidConsent.Parametrize(today));
            }

            if (!context.AllowAnonymous)
            {
                records = records.Where(c => !c.IsAnonymized);
            }
            return records;
        }
    }
}
