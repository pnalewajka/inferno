﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.DocumentMappings;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.Business.Recruitment.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class CandidateFileCardIndexDataService : CardIndexDataService<CandidateFileDto, CandidateFile, IRecruitmentDbScope>,
        ICandidateFileCardIndexDataService
    {
        private readonly IUploadedDocumentHandlingService _uploadedDocumentHandlingService;
        private readonly IDataActivityLogService _dataActivityLogService;
        private readonly ICandidateService _candidateService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public CandidateFileCardIndexDataService(ICardIndexServiceDependencies<IRecruitmentDbScope> dependencies,
             IDataActivityLogService dataActivityLogService,
             IUploadedDocumentHandlingService uploadedDocumentHandlingService,
             ICandidateService candidateService)
            : base(dependencies)
        {
            _uploadedDocumentHandlingService = uploadedDocumentHandlingService;
            _dataActivityLogService = dataActivityLogService;
            _candidateService = candidateService;
        }

        public override IQueryable<CandidateFile> ApplyContextFiltering(IQueryable<CandidateFile> records, object context)
        {
            if (context is ParentIdContext parentIdContext)
            {
                return records.Where(c => c.CandidateId == parentIdContext.ParentId);
            }

            return base.ApplyContextFiltering(records, context);
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<CandidateFile, CandidateFileDto, IRecruitmentDbScope> eventArgs)
        {

            var candidate = eventArgs.UnitOfWork.Repositories.Candidates.GetById(eventArgs.InputEntity.CandidateId);

            if (!CandidateBusinessLogic.HasValidConsent.Call(candidate, TimeService.GetCurrentDate()))
            {
                throw new BusinessException(CandidateResources.CandidateInvalidDataConsent);
            }

            if (eventArgs.InputDto.FileSource == FileSource.Disk)
            {
                _uploadedDocumentHandlingService
                    .MergeDocumentCollections(
                        eventArgs.UnitOfWork,
                        eventArgs.InputEntity.File,
                        eventArgs.InputDto.File,
                        p => new CandidateFileDocument
                        {
                            Name = p.DocumentName,
                            ContentType = p.ContentType
                        }
                    );
            }

            if (eventArgs.InputDto.FileSource == FileSource.InboundEmailAttachment)
            {
                var unitOfWork = eventArgs.UnitOfWork;
                var documentId = eventArgs.InputDto.File.First().DocumentId;
                var inboundEmailDocument = unitOfWork.Repositories.InboundEmailDocuments.Single(d => d.Id == documentId);

                eventArgs.InputEntity.File.Add(new CandidateFileDocument
                {
                    ContentType = inboundEmailDocument.ContentType,
                    ContentLength = inboundEmailDocument.ContentLength,
                    Name = inboundEmailDocument.Name,
                    DocumentContent = new CandidateFileDocumentContent
                    {
                        Content = inboundEmailDocument.DocumentContent.Content
                    }
                });
            }
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<CandidateFile, CandidateFileDto> eventArgs)
        {
            _dataActivityLogService.LogCandidateDataActivity(eventArgs.InputEntity.CandidateId, ActivityType.CreatedRecord, ReferenceType.CandidateFile, eventArgs.InputEntity.Id);

            _uploadedDocumentHandlingService
                .PromoteTemporaryDocuments<CandidateFileDocument, CandidateFileDocumentContent, CandidateFileDocumentMapping, IRecruitmentDbScope>(
                    eventArgs.InputDto.File
                );

            if (CandidateFileBusinessLogic.ShouldTrackRecentActivity.Call(eventArgs.InputEntity.DocumentType))
            {
                _candidateService.UpdateLastActivity(eventArgs.InputEntity.CandidateId, ReferenceType.CandidateFile, eventArgs.InputEntity.Id);
            }
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<CandidateFile, CandidateFileDto, IRecruitmentDbScope> eventArgs)
        {
            _uploadedDocumentHandlingService
                .MergeDocumentCollections(
                    eventArgs.UnitOfWork,
                    eventArgs.DbEntity.File,
                    eventArgs.InputDto.File,
                    p => new CandidateFileDocument
                    {
                        Name = p.DocumentName,
                        ContentType = p.ContentType
                    }
                );
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<CandidateFile, CandidateFileDto> eventArgs)
        {
            _dataActivityLogService.LogCandidateDataActivity(eventArgs.InputEntity.CandidateId, ActivityType.UpdatedRecord, ReferenceType.CandidateFile, eventArgs.InputEntity.Id);

            _uploadedDocumentHandlingService
                .PromoteTemporaryDocuments<CandidateFileDocument, CandidateFileDocumentContent, CandidateFileDocumentMapping, IRecruitmentDbScope>(
                    eventArgs.InputDto.File
                );

            if (CandidateFileBusinessLogic.ShouldTrackRecentActivity.Call(eventArgs.InputEntity.DocumentType))
            {
                _candidateService.UpdateLastActivity(eventArgs.InputEntity.CandidateId, ReferenceType.CandidateFile, eventArgs.InputEntity.Id);
            }
        }

        protected override void OnRecordsDeleted(RecordDeletedEventArgs<CandidateFile> eventArgs)
        {
            foreach (var entitie in eventArgs.DeletedEntities)
            {
                _dataActivityLogService.LogCandidateDataActivity(entitie.CandidateId, ActivityType.RemovedRecord, ReferenceType.CandidateFile, entitie.Id);
            }
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<CandidateFile, IRecruitmentDbScope> eventArgs)
        {
            base.OnRecordDeleting(eventArgs);
            eventArgs.UnitOfWork.Repositories.CandidateDocuments.DeleteEach(p => p.CandidateFileId == eventArgs.DeletingRecord.Id);
        }

        protected override NamedFilters<CandidateFile> NamedFilters => new NamedFilters<CandidateFile>(CardIndexServiceHelper.BuildSoftDeletableFilters<CandidateFile>());

        protected override IEnumerable<Expression<Func<CandidateFile, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return n => n.Comment;
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<CandidateFile> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var direction = sortingCriterion.GetSortingDirection();

            if (sortingCriterion.GetSortingColumnName() == nameof(CandidateFileDto.File))
            {
                orderedQueryBuilder.ApplySortingKey(f => f.File.FirstOrDefault().Name, direction);
            }
            else
            {
                base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
            }
        }
    }
}
