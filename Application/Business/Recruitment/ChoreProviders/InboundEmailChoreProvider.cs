﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.Recruitment.ChoreProviders
{
    public class InboundEmailChoreProvider : IChoreProvider
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly ISystemParameterService _systemParameterService;

        public InboundEmailChoreProvider(
            IPrincipalProvider principalProvider,
            ISystemParameterService systemParameterService)
        {
            _principalProvider = principalProvider;
            _systemParameterService = systemParameterService;
        }

        public IQueryable<ChoreDto> GetActiveChores(
            IReadOnlyRepositoryFactory repositoryFactory)
        {
            var inboundEmals = repositoryFactory.Create<InboundEmail>().Filtered();

            var deliveryFailedEmails = GroupToSingleChore(FilterByProfiles(inboundEmals))
                .Select(g => new ChoreDto
                {
                    Type = ChoreType.InboundEmails,
                    DueDate = null,
                    Parameters = g.Count().ToString(),
                });

            return deliveryFailedEmails;
        }

        private IQueryable<IGrouping<int, InboundEmail>> GroupToSingleChore(IQueryable<InboundEmail> inboundEmals)
        {
            return inboundEmals.GroupBy(e => 0);
        }

        private IQueryable<InboundEmail> FilterByProfiles(IQueryable<InboundEmail> inboundEmals)
        {
            var recruitmentFileClerkInboundProfileName = _systemParameterService.GetParameter<string>(ParameterKeys.RecruitmentFileClerkInboundProfileName);
            var recruitmentFileClerkResearchProfileName = _systemParameterService.GetParameter<string>(ParameterKeys.RecruitmentFileClerkResearchProfileName);

            var isClerkInbound = _principalProvider.Current.Profiles.Contains(recruitmentFileClerkInboundProfileName);
            var isClerkResearch = _principalProvider.Current.Profiles.Contains(recruitmentFileClerkResearchProfileName);

            if (isClerkResearch || isClerkInbound)
            {
                if (isClerkInbound)
                {
                    inboundEmals = inboundEmals.Where(i => i.Status != InboundEmailStatus.New && i.Status != InboundEmailStatus.Closed);
                }

                if (isClerkResearch)
                {
                    inboundEmals = inboundEmals.Where(i => i.Status == InboundEmailStatus.SentForResearch);
                }
            }
            else
            {
                inboundEmals = inboundEmals.Where(i => i.Status == InboundEmailStatus.New);
            }

            return inboundEmals;
        }
    }
}
