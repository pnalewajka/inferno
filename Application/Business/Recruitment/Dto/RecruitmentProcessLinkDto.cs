﻿namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecruitmentProcessLinkDto
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }
}
