﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecruitmentProcessStepTimelineDto
    {
        public long Id { get; set; }

        public long CurrentId { get; set; }

        public RecruitmentProcessStepType Type { get; set; }

        public string AssignedEmployeeName { get; set; }

        public DateTime? PlannedOn { get; set; }

        public RecruitmentProcessStepDecision Decision { get; set; }

        public RecruitmentProcessStepStatus Status { get; set; }

        public bool CanBeViewedByCurrentUser { get; set; }
    }
}
