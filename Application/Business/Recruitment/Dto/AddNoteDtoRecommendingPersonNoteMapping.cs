﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class AddNoteDtoRecommendingPersonNoteMapping : ClassMapping<AddNoteDto, RecommendingPersonNote>
    {
        public AddNoteDtoRecommendingPersonNoteMapping()
        {
            Mapping = d => new RecommendingPersonNote
            {
                RecommendingPersonId = d.ParentId,
                Content = d.Content,
                NoteVisibility = d.NoteVisibility,
                IsDeleted = false
            };
        }
    }
}
