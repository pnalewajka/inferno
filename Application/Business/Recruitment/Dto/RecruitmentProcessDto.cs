﻿using System;
using System.Linq;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public partial class RecruitmentProcessDto
    {
        public long Id { get; set; }

        public long CandidateId { get; set; }

        public string CandidateName { get; set; }

        public long JobOpeningId { get; set; }

        public long RecruiterId { get; set; }

        public string RecruiterName { get; set; }

        public string DecisionMakerName { get; set; }

        public string PositionName { get; set; }

        public long? CityId { get; set; }

        public long? JobApplicationId { get; set; }

        public RecruitmentProcessStatus Status { get; set; }

        public DateTime? ResumeShownToClientOn { get; set; }

        public DateTime? ClosedOn { get; set; }

        public RecruitmentProcessClosedReason? ClosedReason { get; set; }

        public string ClosedComment { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public RecruitmentProcessStepType[] InitialSteps { get; set; }

        public RecruitmentProcessStepInfoDto[] StepInfos { get; set; }

        public long[] WatcherIds { get; set; }

        public RecruitmentProcessWorkflowAction? WorkflowAction { get; set; }

        public RecruitmentProcessClosedReason? WorkflowClosedReason { get; set; }

        public string WorkflowClosedComment { get; set; }

        public RecruitmentProcessNoteDto[] Notes { get; set; }

        public HistoryEntryDto[] HistoryEntries { get; set; }

        public Guid? CrmId { get; set; }

        public bool IsReadyToShowScreeningData
            => CanShowDataByStepType(RecruitmentProcessStepType.HrCall, RecruitmentProcessStepType.HrInterview);

        public bool IsReadyToShowTechnicalReviewData
            => CanShowDataByStepType(RecruitmentProcessStepType.TechnicalReview);

        public bool IsReadyToShowOfferData
            => CanShowDataByStepType(RecruitmentProcessStepType.ContractNegotiations);

        public bool IsReadyToShowFinalData
            => CanShowDataByStepType(RecruitmentProcessStepType.ContractNegotiations);

        public bool IsMatureEnoughForHiringManager { get; set; }

        public bool CanBeReopened { get; set; }

        public RequestStatus? OnboardingRequestStatus { get; set; }

        public string DisplayName { get; set; }

        public string CreatedByFullName { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime? LastActivityOn { get; set; }

        public byte[] Timestamp { get; set; }

        private bool CanShowDataByStepType(params RecruitmentProcessStepType[] stepTypes)
        {
            return StepInfos != null
                && StepInfos.Any(s => stepTypes.Contains(s.Type));
        }
    }
}
