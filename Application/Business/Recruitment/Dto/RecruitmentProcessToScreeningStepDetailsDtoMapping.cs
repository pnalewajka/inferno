﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecruitmentProcessToScreeningStepDetailsDtoMapping : ClassMapping<RecruitmentProcessScreening, ScreeningStepDetailsDto>
    {
        public RecruitmentProcessToScreeningStepDetailsDtoMapping()
        {
            Mapping = r => new ScreeningStepDetailsDto
            {
                ScreeningAvailability = r.Availability,
                ScreeningBusinessTripsCanDo = r.BusinessTripsCanDo,
                ScreeningBusinessTripsComment = r.BusinessTripsComment,
                ScreeningContractExpectations = r.ContractExpectations,
                ScreeningCounterOfferCriteria = r.CounterOfferCriteria,
                ScreeningGeneralOpinion = r.GeneralOpinion,
                ScreeningLanguageEnglish = r.LanguageEnglish,
                ScreeningLanguageGerman = r.LanguageGerman,
                ScreeningLanguageOther = r.LanguageOther,
                ScreeningMotivation = r.Motivation,
                ScreeningOtherProcesses = r.OtherProcesses,
                ScreeningRelocationCanDo = r.RelocationCanDo,
                ScreeningRelocationComment = r.RelocationComment,
                ScreeningSkills = r.Skills,
                ScreeningWorkPermitComment = r.WorkPermitComment,
                ScreeningWorkPermitNeeded = r.WorkPermitNeeded,
                ScreeningWorkplaceExpectations = r.WorkplaceExpectations,
                ScreeningEveningWorkCanDo = r.EveningWorkCanDo,
                ScreeningEveningWorkComment = r.EveningWorkComment
            };
        }
    }
}