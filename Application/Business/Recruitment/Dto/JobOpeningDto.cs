﻿using System;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class JobOpeningDto
    {
        public long Id { get; set; }

        public string PositionName { get; set; }

        public long? CustomerId { get; set; }

        public string CustomerName { get; set; }

        public long[] CityIds { get; set; }

        public string[] CityNames { get; set; }

        public long[] JobProfileIds { get; set; }

        public string[] JobProfileNames { get; set; }

        public long OrgUnitId { get; set; }

        public long CompanyId { get; set; }

        public long BusinessLineId { get; set; }

        public long? AcceptingEmployeeId { get; set; }

        public string AcceptingEmployeeFullName { get; set; }

        public int Priority { get; set; }

        public HiringMode HiringMode { get; set; }

        public DateTime? RoleOpenedOn { get; set; }

        public int VacancyLevel1 { get; set; }

        public int VacancyLevel2 { get; set; }

        public int VacancyLevel3 { get; set; }

        public int VacancyLevel4 { get; set; }

        public int VacancyLevel5 { get; set; }

        public int VacancyTotal { get; set; }

        public int VacancyLevelNotApplicable { get; set; }

        public long[] RecruitersIds { get; set; }

        public long[] RecruitmentOwnersIds { get; set; }

        public long[] RecruitmentHelpersIds { get; set; }

        public bool IsTechnicalVerificationRequired { get; set; }

        public bool IsIntiveResumeRequired { get; set; }

        public bool IsPolishSpeakerRequired { get; set; }

        public string NoticePeriod { get; set; }

        public string SalaryRate { get; set; }

        public LanguageReferenceLevel EnglishLevel { get; set; }

        public string OtherLanguages { get; set; }

        public AgencyEmploymentMode? AgencyEmploymentMode { get; set; }

        public JobOpeningStatus Status { get; set; }

        public ICollection<ProjectDescriptionDto> ProjectDescriptions { get; set; }

        public long[] WatcherIds { get; set; }

        public JobOpeningWorkflowAction? WorkflowAction { get; set; }

        public string RejectionReason { get; set; }

        public string WorkflowRejectionReason { get; set; }

        public string WorkflowClosedComment { get; set; }

        public JobOpeningClosedReason? WorkflowClosedReason { get; set; }

        public bool CanBeApprovedByCurrentUser { get; set; }

        public bool IsHighlyConfidential { get; set; }

        public string CreatedByFullName { get; set; }

        public DateTime CreatedOn { get; set; }

        public long DecisionMakerId { get; set; }

        public bool IsWatchedByCurrentUser { get; set; }

        public string ClosedComment { get; set; }

        public JobOpeningClosedReason? ClosedReason { get; set; }

        public Guid? CrmId { get; set; }

        public JobOpeningNoteDto[] Notes { get; set; }

        public HistoryEntryDto[] HistoryEntries { get; set; }

        public byte[] Timestamp { get; set; }

        public string TechnicalReviewComment { get; set; }

        public JobOpeningDto()
        {
            ProjectDescriptions = new ProjectDescriptionDto[0];
        }
    }
}
