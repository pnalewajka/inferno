﻿namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class CompanyContext
    {
        public bool? IsDataAdministrator { get; set; }
    }
}
