﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class ProjectDescriptionToProjectDescriptionDtoMapping : ClassMapping<ProjectDescription, ProjectDescriptionDto>
    {
        public ProjectDescriptionToProjectDescriptionDtoMapping()
        {
            Mapping = e => new ProjectDescriptionDto
            {
                Id = e.Id,
                AboutClientAndProject = e.AboutClient,
                SpecificDuties = e.SpecificDuties,
                ToolsAndEnvironment = e.ToolsAndEnvironment,
                AboutTeam = e.AboutTeam,
                ProjectTimeline = e.ProjectTimeline,
                Methodology = e.Methodology,
                RequiredSkills = e.RequiredSkills,
                AdditionalSkills = e.AdditionalSkills,
                RecruitmentDetails = e.RecruitmentDetails,
                IsTravelRequired = e.IsTravelRequired,
                TravelAndAccomodation = e.TravelAndAccomodation,
                CanWorkRemotely = e.CanWorkRemotely,
                RemoteWorkArrangements = e.RemoteWorkArrangements,
                Comments = e.Comments,
                Timestamp = e.Timestamp
            };
        }
    }
}
