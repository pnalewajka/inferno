﻿using System.Linq;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class TechnicalReviewToTechnicalReviewDtoMapping : ClassMapping<TechnicalReview, TechnicalReviewDto>
    {
        public TechnicalReviewToTechnicalReviewDtoMapping()
        {
            Mapping = e => new TechnicalReviewDto
            {
                Id = e.Id,
                RecruitmentProcessId = e.RecruitmentProcessId,
                RecruitmentProcessStepId = e.RecruitmentProcessStepId,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                JobProfileIds = e.JobProfiles.Select(p => p.Id).ToList(),
                SeniorityLevelAssessment = e.SeniorityLevelAssessment,
                TechnicalAssignmentAssessment = e.TechnicalAssignmentAssessment,
                TechnicalKnowledgeAssessment = e.TechnicalKnowledgeAssessment,
                TeamProjectSuitabilityAssessment = e.TeamProjectSuitabilityAssessment,
                EnglishUsageAssessment = e.EnglishUsageAssessment,
                RiskAssessment = e.RiskAssessment,
                StrengthAssessment = e.StrengthAssessment,
                OtherRemarks = e.OtherRemarks,
                PositionName = TechnicalReviewBusinessLogic.PositionName.Call(e),
                AssigneeFullName = TechnicalReviewBusinessLogic.AssigneeFullName.Call(e),
                PlannedOn = e.RecruitmentProcessStep != null ? e.RecruitmentProcessStep.PlannedOn : null,
                CandidateFirstName = (e.RecruitmentProcess != null && e.RecruitmentProcess.Candidate != null) ? e.RecruitmentProcess.Candidate.FirstName : string.Empty,
                CandidateLastName = (e.RecruitmentProcess != null && e.RecruitmentProcess.Candidate != null) ? e.RecruitmentProcess.Candidate.LastName : string.Empty,
                CandidateFullName = TechnicalReviewBusinessLogic.CandidateFullName.Call(e),
                Decision = e.RecruitmentProcessStep != null ? e.RecruitmentProcessStep.Decision : RecruitmentProcessStepDecision.None,
                JobProfileNames = e.JobProfiles.Select(s => s.Name).ToList()
            };
        }
    }
}
