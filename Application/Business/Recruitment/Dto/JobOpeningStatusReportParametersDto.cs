﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class JobOpeningStatusReportParametersDto
    {
        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }

        public JobOpeningStatus[] JobOpeningStatuses { get; set; }
    }
}
