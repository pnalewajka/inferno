﻿namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecruitmentProcessPickerContext
    {
        public bool OnlyActive { get; set; }
    }
}
