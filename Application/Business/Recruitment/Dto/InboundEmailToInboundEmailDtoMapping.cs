﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class InboundEmailToInboundEmailDtoMapping : ClassMapping<InboundEmail, InboundEmailDto>
    {
        public InboundEmailToInboundEmailDtoMapping(
            IPrincipalProvider principalProvider,
            IClassMapping<InboundEmailValue, InboundEmailValueDto> inboundEmailValueToInboundEmailValueDtoMapping)
        {
            Mapping = e => new InboundEmailDto
            {
                Id = e.Id,
                To = e.To,
                FromEmailAddress = e.FromEmailAddress,
                FromFullName = e.FromFullName,
                Subject = e.Subject,
                Body = e.Body,
                BodyType = e.BodyType,
                EmailColorCategory = e.EmailColorCategory,
                SentOn = e.SentOn,
                ReceivedOn = e.ReceivedOn,
                Status = e.Status,
                ProcessedById = e.ProcessedById,
                ReaderIds = e.Readers != null ? e.Readers.Select(w => w.Id).ToArray() : new long[0],
                HasBeenReadByCurrentUser = e.Readers != null && e.Readers.Any(w => w.Id == principalProvider.Current.Id.Value),
                HasBeenReadByOthers = e.Readers != null && e.Readers.Any(w => w.Id != principalProvider.Current.Id.Value),
                Category = e.Category,
                IsDeleted = e.IsDeleted,
                Documents = e.Attachments
                    .Select(p => new DocumentDto
                    {
                        ContentType = p.ContentType,
                        DocumentName = p.Name,
                        DocumentId = p.Id
                    })
                    .ToArray(),
                Values = e.Values.Select(inboundEmailValueToInboundEmailValueDtoMapping.CreateFromSource).ToList(),
                ResearchingComment = e.ResearchingComment,
                Timestamp = e.Timestamp
            };
        }
    }
}
