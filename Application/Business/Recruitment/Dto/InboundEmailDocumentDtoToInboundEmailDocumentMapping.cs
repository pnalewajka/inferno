﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class InboundEmailDocumentDtoToInboundEmailDocumentMapping : ClassMapping<InboundEmailDocumentDto, InboundEmailDocument>
    {
        public InboundEmailDocumentDtoToInboundEmailDocumentMapping()
        {
            Mapping = d => new InboundEmailDocument
            {
                ContentType = d.ContentType,
                Name = d.DocumentName,
                Id = d.DocumentId.Value
            };
        }
    }
}
