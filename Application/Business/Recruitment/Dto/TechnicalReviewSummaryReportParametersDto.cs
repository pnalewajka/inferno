﻿namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class TechnicalReviewSummaryReportParametersDto
    {
        public long RecruitmentProcessStepId { get; set; }

        public long CustomerId { get; set; }

        public string Comment { get; set; }
    }
}
