﻿using System.Collections.ObjectModel;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class JobOpeningDtoToJobOpeningMapping : ClassMapping<JobOpeningDto, JobOpening>
    {
        public JobOpeningDtoToJobOpeningMapping(
            IClassMappingFactory mappingFactory)
        {
            Mapping = d => new JobOpening
            {
                Id = d.Id,
                CustomerId = d.CustomerId,
                JobProfiles = d.JobProfileIds == null ? new Collection<JobProfile>() : new Collection<JobProfile>(d.JobProfileIds.Select(v => new JobProfile { Id = v }).ToList()),
                Cities = d.CityIds == null ? new Collection<City>() : new Collection<City>(d.CityIds.Select(v => new City { Id = v }).ToList()),
                PositionName = d.PositionName,
                OrgUnitId = d.OrgUnitId,
                CompanyId = d.CompanyId,
                AcceptingEmployeeId = d.AcceptingEmployeeId,
                Priority = d.Priority,
                HiringMode = d.HiringMode,
                VacancyLevel1 = d.VacancyLevel1,
                VacancyLevel2 = d.VacancyLevel2,
                VacancyLevel3 = d.VacancyLevel3,
                VacancyLevel4 = d.VacancyLevel4,
                VacancyLevel5 = d.VacancyLevel5,
                VacancyLevelNotApplicable = d.VacancyLevelNotApplicable,
                IsTechnicalVerificationRequired = d.IsTechnicalVerificationRequired,
                Recruiters = d.RecruitersIds == null ? new Collection<Employee>() : new Collection<Employee>(d.RecruitersIds.Select(v => new Employee { Id = v }).ToList()),
                RecruitmentOwners = d.RecruitmentOwnersIds == null ? new Collection<Employee>() : new Collection<Employee>(d.RecruitmentOwnersIds.Select(v => new Employee { Id = v }).ToList()),
                Watchers = d.WatcherIds == null ? new Collection<Employee>() : new Collection<Employee>(d.WatcherIds.Select(v => new Employee { Id = v }).ToList()),
                IsIntiveResumeRequired = d.IsIntiveResumeRequired,
                IsPolishSpeakerRequired = d.IsPolishSpeakerRequired,
                NoticePeriod = d.NoticePeriod,
                SalaryRate = d.SalaryRate,
                EnglishLevel = d.EnglishLevel,
                OtherLanguages = d.OtherLanguages,
                AgencyEmploymentMode = d.AgencyEmploymentMode,
                Status = d.Status,
                Timestamp = d.Timestamp,
                ProjectDescriptions = d.ProjectDescriptions != null
                    ? d.ProjectDescriptions.Select(mappingFactory.CreateMapping<ProjectDescriptionDto, ProjectDescription>().CreateFromSource).ToArray()
                    : new Collection<ProjectDescription>().ToArray(),
                IsHighlyConfidential = d.IsHighlyConfidential,
                DecisionMakerEmployeeId = d.DecisionMakerId,
                RejectionReason = d.RejectionReason,
                CrmId = d.CrmId,
                TechnicalReviewComment = d.TechnicalReviewComment,
                RoleOpenedOn = d.RoleOpenedOn,
            };
        }
    }
}