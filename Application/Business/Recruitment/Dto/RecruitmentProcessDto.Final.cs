﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public partial class RecruitmentProcessDto
    {
        public long? FinalPositionId { get; set; }

        public long? FinalTrialContractTypeId { get; set; }

        public long? FinalLongTermContractTypeId { get; set; }

        public string FinalTrialSalary { get; set; }

        public string FinalLongTermSalary { get; set; }

        public DateTime? FinalStartDate { get; set; }

        public DateTime? FinalSignedDate { get; set; }

        public long? FinalLocationId { get; set; }

        public string FinalComment { get; set; }

        public long? FinalEmploymentSourceId { get; set; }
    }
}
