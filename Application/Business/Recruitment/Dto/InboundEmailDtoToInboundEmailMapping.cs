﻿using System.Collections.ObjectModel;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class InboundEmailDtoToInboundEmailMapping : ClassMapping<InboundEmailDto, InboundEmail>
    {
        public InboundEmailDtoToInboundEmailMapping(IClassMapping<InboundEmailValueDto, InboundEmailValue> inboundEmailValueDtoToInboundEmailValueMapping)
        {
            Mapping = d => new InboundEmail
            {
                Id = d.Id,
                To = d.To,
                FromEmailAddress = d.FromEmailAddress,
                FromFullName = d.FromFullName,
                Subject = d.Subject,
                Body = d.Body,
                BodyType = d.BodyType,
                SentOn = d.SentOn,
                ReceivedOn = d.ReceivedOn,
                Status = d.Status,
                ProcessedById = d.ProcessedById,
                Category = d.Category,
                IsDeleted = d.IsDeleted,
                Attachments = new Collection<InboundEmailDocument>(),
                Values = d.Values.Select(inboundEmailValueDtoToInboundEmailValueMapping.CreateFromSource).ToList(),
                ResearchingComment = d.ResearchingComment,
                Timestamp = d.Timestamp
            };
        }
    }
}
