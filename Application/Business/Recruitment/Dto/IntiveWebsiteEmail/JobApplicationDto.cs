﻿namespace Smt.Atomic.Business.Recruitment.Dto.IntiveWebsiteEmail
{
    public class JobApplicationDto : BaseJobApplicationDto
    {
        public string JobOffer { get; set; }

        public string ReferenceNumber { get; set; }

        public JobApplicationDto() : base(EmailType.JobApplication)
        {
        }
    }
}
