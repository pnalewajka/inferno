﻿namespace Smt.Atomic.Business.Recruitment.Dto.IntiveWebsiteEmail
{
    public enum EmailType
    {
        JobApplication = 1,

        NinjaJobApplication = 2,

        RecommendationJobApplication = 3,

        RecommendationJobNinja = 4
    }
}
