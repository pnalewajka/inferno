﻿namespace Smt.Atomic.Business.Recruitment.Dto.IntiveWebsiteEmail
{
    public class NinjaJobApplicationDto : BaseJobApplicationDto
    {
        public string WhatDoForUs { get; set; }

        public NinjaJobApplicationDto() : base(EmailType.NinjaJobApplication)
        {
        }
    }
}
