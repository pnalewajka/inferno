﻿namespace Smt.Atomic.Business.Recruitment.Dto.IntiveWebsiteEmail
{
    public class RecommendationJobNinjaDto : RecommendationJobApplicationDto
    {
        public RecommendationJobNinjaDto() : base(EmailType.RecommendationJobNinja)
        {
        }
    }
}
