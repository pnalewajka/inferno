﻿namespace Smt.Atomic.Business.Recruitment.Dto.IntiveWebsiteEmail
{
    public class RecommendationJobApplicationDto : BaseJobApplicationDto
    {
        public string FriendFirstName { get; set; }

        public string FriendLastName { get; set; }

        public string FriendEmail { get; set; }

        public string FriendPhoneNumber { get; set; }

        public string FriendJobPreferences { get; set; }

        public string FriendLocationName { get; set; }

        public RecommendationJobApplicationDto() : this(EmailType.RecommendationJobApplication)
        {
        }

        public RecommendationJobApplicationDto(EmailType type) : base(type)
        {
        }
    }
}
