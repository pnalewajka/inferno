﻿namespace Smt.Atomic.Business.Recruitment.Dto.IntiveWebsiteEmail
{
    public class BaseJobApplicationDto
    {
        public EmailType EmailType { get; private set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string LocationId { get; set; }

        public string LocationName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string Consents { get; set; }

        public BaseJobApplicationDto(EmailType emailType)
        {
            EmailType = emailType;
        }
    }
}
