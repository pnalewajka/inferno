﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class ApplicationOriginPickerContext
    {
        public ApplicationOriginType? OriginType { get; set; }

        public bool SkipRecommendation { get; set; }
    }
}
