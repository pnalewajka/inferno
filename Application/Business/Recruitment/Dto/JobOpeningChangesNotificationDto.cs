﻿using System.Collections.Generic;
using Smt.Atomic.Business.Recruitment.SnapshotComparer;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class JobOpeningChangesNotificationDto : BaseJobOpeningReminderDto
    {
        public string RecipientFirstName { get; set; }

        public string ChangedOnDate { get; set; }

        public string ChangedOnTime { get; set; }

        public string ChangedByFullName { get; set; }

        public IDictionary<string, ValueChange> Changes { get; set; }
    }
}