﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecruitmentProcessStepDto
    {
        public long Id { get; set; }

        public long CreatedById { get; set; }

        public string CreatedByFullName { get; set; }

        public DateTime? CreatedOn { get; set; }

        public long? FromStepId { get; set; }

        public long RecruitmentProcessId { get; set; }

        public RecruitmentProcessStatus RecruitmentProcessStatus { get; set; }

        public RecruitmentProcessStepType Type { get; set; }

        public DateTime? PlannedOn { get; set; }

        public DateTime? DueOn { get; set; }

        public DateTime? DecidedOn { get; set; }

        public long? AssignedEmployeeId { get; set; }

        public long[] OtherAssignedEmployeeIds { get; set; }

        public RecruitmentProcessStepDecision Decision { get; set; }

        public RecruitmentProcessStepStatus Status { get; set; }

        public string DecisionComment { get; set; }

        public string RecruitmentHint { get; set; }

        public object StepDetails { get; set; }

        public CandidateDto Candidate { get; set; }

        public JobOpeningDto JobOpening { get; set; }

        public RecruitmentProcessDto RecruitmentProcess { get; set; }

        public IList<TechnicalReviewDto> TechnicalReviews { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public string ModifiedByFullName { get; set; }

        public long? JobOpeningDecisionMakerId { get; set; }

        public DateTime ModifiedOn { get; set; }

        public RecruitmentProcessStepWorkflowActionType? WorkflowAction { get; set; }

        public long? NextStepId { get; set; }

        public IList<RecruitmentProcessStepTimelineDto> StepTimeline { get; set; }

        public bool CloseCurrentStep { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
