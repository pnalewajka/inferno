﻿namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class ProjectDescriptionDto
    {
        public long Id { get; set; }

        public string AboutClientAndProject { get; set; }

        public string SpecificDuties { get; set; }

        public string ToolsAndEnvironment { get; set; }

        public string AboutTeam { get; set; }

        public string ProjectTimeline { get; set; }

        public string Methodology { get; set; }

        public string RequiredSkills { get; set; }

        public string AdditionalSkills { get; set; }

        public string RecruitmentDetails { get; set; }

        public bool IsTravelRequired { get; set; }

        public string TravelAndAccomodation { get; set; }

        public bool CanWorkRemotely { get; set; }

        public string RemoteWorkArrangements { get; set; }

        public string Comments { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
