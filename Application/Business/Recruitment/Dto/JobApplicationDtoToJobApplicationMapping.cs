﻿using System.Collections.ObjectModel;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class JobApplicationDtoToJobApplicationMapping : ClassMapping<JobApplicationDto, JobApplication>
    {
        public JobApplicationDtoToJobApplicationMapping()
        {
            Mapping = d => new JobApplication
            {
                Id = d.Id,
                CandidateId = d.CandidateId,
                ApplicationOriginId = d.ApplicationOriginId,
                RecommendingPersonId = d.RecommendingPersonId,
                OriginComment = d.OriginComment,
                Cities = d.CityIds == null ? null : new Collection<City>(d.CityIds.Select(v => new City { Id = v }).ToList()),
                JobProfiles = d.JobProfileIds == null ? null : new Collection<JobProfile>(d.JobProfileIds.Select(v => new JobProfile { Id = v }).ToList()),
                SourceEmailId = d.SourceEmailId,
                CandidateComment = d.CandidateComment,
                IsDeleted = d.IsDeleted,
                IsEligibleForReferralProgram = d.IsEligibleForReferralProgram,
                Timestamp = d.Timestamp
            };
        }
    }
}
