﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class ApplicationOriginToApplicationOriginDtoMapping : ClassMapping<ApplicationOrigin, ApplicationOriginDto>
    {
        public ApplicationOriginToApplicationOriginDtoMapping()
        {
            Mapping = e => new ApplicationOriginDto
            {
                Id = e.Id,
                OriginType = e.OriginType,
                Name = e.Name,
                ParserIdentifier = e.ParserIdentifier,
                EmailAddress = e.EmailAddress,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
