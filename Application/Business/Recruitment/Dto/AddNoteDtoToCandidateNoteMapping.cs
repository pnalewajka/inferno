﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class AddNoteDtoToCandidateNoteMapping : ClassMapping<AddNoteDto, CandidateNote>
    {
        public AddNoteDtoToCandidateNoteMapping()
        {
            Mapping = d => new CandidateNote
            {
                CandidateId = d.ParentId,
                Content = d.Content,
                NoteVisibility = d.NoteVisibility,
                IsDeleted = false
            };
        }
    }
}
