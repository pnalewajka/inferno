﻿using System;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class TechnicalReviewDto
    {
        public long Id { get; set; }

        public long RecruitmentProcessId { get; set; }

        public long RecruitmentProcessStepId { get; set; }

        public List<long> JobProfileIds { get; set; }

        public List<string> JobProfileNames { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public SeniorityLevelEnum SeniorityLevelAssessment { get; set; }

        public string TechnicalAssignmentAssessment { get; set; }
        
        public string TechnicalKnowledgeAssessment { get; set; }

        public string TeamProjectSuitabilityAssessment { get; set; }

        public string EnglishUsageAssessment { get; set; }

        public string RiskAssessment { get; set; }

        public string StrengthAssessment { get; set; }

        public string OtherRemarks { get; set; }

        public DateTime? PlannedOn { get; set; }

        public string PositionName { get; set; }

        public string AssigneeFullName { get; set; }

        public string CandidateFirstName { get; set; }

        public string CandidateLastName { get; set; }

        public string CandidateFullName { get; set; }

        public RecruitmentProcessStepDecision Decision { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
