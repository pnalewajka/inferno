﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecruitmentProcessCloseDto
    {
        public long Id { get; set; }

        public RecruitmentProcessClosedReason? Reason { get; set; }

        public string Comment { get; set; }
    }
}
