﻿using System.Collections.Generic;
using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Helpers;
using Smt.Atomic.Business.Recruitment.SnapshotComparer;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecruitmentProcessHistoryEntryToHistoryEntryDtoMapping : ClassMapping<RecruitmentProcessHistoryEntry, HistoryEntryDto>
    {
        public RecruitmentProcessHistoryEntryToHistoryEntryDtoMapping()
        {
            Mapping = d => new HistoryEntryDto
            {
                PerformedByUserFullName = UserBusinessLogic.FullName.Call(d.PerformedByUser),
                ActivityDescription = GetActivityDescription(d),
                PerformedOn = d.PerformedOn,
                SnapshotChanges = GetChangesDescription(d)
            };
        }

        private Dictionary<string, ValueChange> GetChangesDescription(RecruitmentProcessHistoryEntry recruitmentProcessHistory)
        {
            if (recruitmentProcessHistory.ActivityType == RecruitmentProcessHistoryEntryType.Updated)
            {
                return SnapshotComaparerHelper.DeserializeSnapshotChanges(recruitmentProcessHistory.ChangesDescription);
            }

            return null;
        }

        private string GetActivityDescription(RecruitmentProcessHistoryEntry recruitmentProcessHistory)
        {
            var activityDescription = recruitmentProcessHistory.ActivityType.GetLocalizedDescription().ToString();

            if (recruitmentProcessHistory.RecruitmentProcessStepId.HasValue)
            {
                var stepDescription = recruitmentProcessHistory.RecruitmentProcessStep.Type.GetLocalizedDescription().ToString();

                return $"{activityDescription} ({stepDescription})";
            }

            return activityDescription;
        }
    }
}
