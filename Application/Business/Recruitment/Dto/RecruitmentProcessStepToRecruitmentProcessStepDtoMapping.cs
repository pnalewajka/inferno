﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecruitmentProcessStepToRecruitmentProcessStepDtoMapping : ClassMapping<RecruitmentProcessStep, RecruitmentProcessStepDto>
    {
        public RecruitmentProcessStepToRecruitmentProcessStepDtoMapping()
        {
            Mapping = e => new RecruitmentProcessStepDto
            {
                Id = e.Id,
                RecruitmentProcessId = e.RecruitmentProcessId,
                CreatedOn = e.CreatedOn,
                CreatedByFullName = e.CreatedBy == null ? null : UserBusinessLogic.FullName.Call(e.CreatedBy),
                RecruitmentProcessStatus = e.RecruitmentProcess == null ? RecruitmentProcessStatus.Active : e.RecruitmentProcess.Status,
                Type = e.Type,
                PlannedOn = e.PlannedOn,
                DueOn = e.DueOn,
                DecidedOn = e.DecidedOn,
                AssignedEmployeeId = e.AssignedEmployeeId,
                OtherAssignedEmployeeIds = e.OtherAttendingEmployees == null ? new long[0] : e.OtherAttendingEmployees.Select(a => a.Id).ToArray(),
                Decision = e.Decision,
                Status = e.Status,
                DecisionComment = e.DecisionComment,
                RecruitmentHint = e.RecruitmentHint,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedByFullName = e.ModifiedBy == null ? null : UserBusinessLogic.FullName.Call(e.ModifiedBy),
                ModifiedOn = e.ModifiedOn,
                JobOpeningDecisionMakerId = e.RecruitmentProcess == null ? (long?)null : e.RecruitmentProcess.JobOpening.DecisionMakerEmployeeId,
                StepTimeline = new List<RecruitmentProcessStepTimelineDto>(),
                Timestamp = e.Timestamp
            };
        }
    }
}
