﻿namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class InboundEmailPickerContext
    {
        public bool AssignedOnly { get; set; }

        public long? InboundEmailId { get; set; }
    }
}
