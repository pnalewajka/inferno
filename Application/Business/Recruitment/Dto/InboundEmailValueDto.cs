﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class InboundEmailValueDto
    {
        public long Id { get; set; }

        public InboundEmailValueType Key { get; set; }

        public string ValueString { get; set; }

        public long? ValueLong { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
