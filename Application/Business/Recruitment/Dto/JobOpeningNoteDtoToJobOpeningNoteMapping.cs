﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class JobOpeningNoteDtoToJobOpeningNoteMapping : ClassMapping<JobOpeningNoteDto, JobOpeningNote>
    {
        public JobOpeningNoteDtoToJobOpeningNoteMapping()
        {
            Mapping = d => new JobOpeningNote
            {
                Id = d.Id,
                JobOpeningId = d.JobOpeningId,
                NoteType = d.NoteType,
                NoteVisibility = d.NoteVisibility,
                Content = d.Content,
                CreatedById = d.CreatedById,
                ImpersonatedCreatedById = d.ImpersonatedCreatedById,
                CreatedOn = d.CreatedOn,
                IsDeleted = d.IsDeleted,
                Timestamp = d.Timestamp
            };
        }
    }
}
