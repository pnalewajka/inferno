﻿using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class ProcessProgressReportParametersDto
    {
        public List<long> RecruiterIds { get; set; }

        public List<long> CityIds { get; set; }

        public List<long> JobProfileIds { get; set; }

        public List<long> JobOpeningIds { get; set; }

        public List<long> AssignedEmployeeIds { get; set; }

        public RecruitmentProcessStepType[] RecruitmentProcessStepTypes { get; set; }

        public List<long> OrgUnitIds { get; set; }
    }
}
