﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class InboundEmailCloseDto
    {
        public long Id { get; set; }

        public InboundEmailClosedReason? Reason { get; set; }

        public string Comment { get; set; }
    }
}
