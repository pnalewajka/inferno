﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class JobApplicationDto
    {
        public long Id { get; set; }

        public long CandidateId { get; set; }

        public ApplicationOriginType OriginType { get; set; }

        public long ApplicationOriginId { get; set; }

        public long? RecommendingPersonId { get; set; }

        public long? SourceEmailId { get; set; }

        public string OriginComment { get; set; }

        public long[] JobProfileIds { get; set; }

        public long[] CityIds { get; set; }

        public string CandidateComment { get; set; }

        public string CreatedByFullName { get; set; }

        public DateTime CreatedOn { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsEligibleForReferralProgram { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
