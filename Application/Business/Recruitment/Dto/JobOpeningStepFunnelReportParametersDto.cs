﻿using System;
using System.Collections.Generic;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class JobOpeningStepFunnelReportParametersDto
    {
        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }

        public List<long> DecisionMakerIds { get; set; }
    }
}
