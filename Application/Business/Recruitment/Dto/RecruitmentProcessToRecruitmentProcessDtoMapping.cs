﻿using System.Linq;
using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Helpers;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecruitmentProcessToRecruitmentProcessDtoMapping : ClassMapping<RecruitmentProcess, RecruitmentProcessDto>
    {
        private readonly IClassMapping<RecruitmentProcessNote, RecruitmentProcessNoteDto> _recruitmentProcessNoteClassMapping;
        private readonly IClassMapping<RecruitmentProcessHistoryEntry, HistoryEntryDto> _recruitmentProcessHistoryMapping;

        public RecruitmentProcessToRecruitmentProcessDtoMapping(
            IClassMapping<RecruitmentProcessStep,
            RecruitmentProcessStepInfoDto> recruitmentProcessStepClassMapping,
            IClassMapping<RecruitmentProcessNote, RecruitmentProcessNoteDto> recruitmentProcessNoteClassMapping,
            IClassMapping<RecruitmentProcessHistoryEntry, HistoryEntryDto> recruitmentProcessHistoryMapping)
        {
            _recruitmentProcessNoteClassMapping = recruitmentProcessNoteClassMapping;
            _recruitmentProcessHistoryMapping = recruitmentProcessHistoryMapping;

            Mapping = e => new RecruitmentProcessDto
            {
                Id = e.Id,
                CandidateId = e.CandidateId,
                CandidateName = RecruitmentProcessBusinessLogic.CandidateFullName.Call(e),
                JobOpeningId = e.JobOpeningId,
                PositionName = e.JobOpening != null ? e.JobOpening.PositionName : null,
                RecruiterId = e.RecruiterId,
                RecruiterName = e.Recruiter != null ? EmployeeBusinessLogic.FullName.Call(e.Recruiter) : null,
                DecisionMakerName = e.JobOpening != null ? (e.JobOpening.DecisionMakerEmployee != null ? EmployeeBusinessLogic.FullName.Call(e.JobOpening.DecisionMakerEmployee) : null) : null,
                CityId = e.CityId,
                JobApplicationId = e.JobApplicationId,
                Status = e.Status,
                ResumeShownToClientOn = e.ResumeShownToClientOn,
                ClosedOn = e.ClosedOn,
                ClosedReason = e.ClosedReason,
                ClosedComment = e.ClosedComment,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                WatcherIds = e.Watchers != null ? e.Watchers.Select(w => w.Id).ToArray() : new long[0],
                OfferPositionId = e.Offer != null ? e.Offer.PositionId : null,
                OfferComment = e.Offer != null ? e.Offer.Comment : null,
                OfferContractType = e.Offer != null ? e.Offer.ContractType : null,
                OfferLocationId = e.Offer != null ? e.Offer.LocationId : null,
                OfferSalary = e.Offer != null ? e.Offer.Salary : null,
                OfferStartDate = e.Offer != null ? e.Offer.StartDate : null,
                FinalComment = e.Final == null ? null : e.Final.Comment,
                FinalLocationId = e.Final == null ? null : e.Final.LocationId,
                FinalLongTermContractTypeId = e.Final == null ? null : e.Final.LongTermContractTypeId,
                FinalPositionId = e.Final == null ? null : e.Final.PositionId,
                FinalLongTermSalary = e.Final == null ? null : e.Final.LongTermSalary,
                FinalTrialSalary = e.Final == null ? null : e.Final.TrialSalary,
                FinalTrialContractTypeId = e.Final == null ? null : e.Final.TrialContractTypeId,
                FinalStartDate = e.Final == null ? null : e.Final.StartDate,
                FinalSignedDate = e.Final == null ? null : e.Final.SignedDate,
                FinalEmploymentSourceId = e.Final == null ? null : e.Final.EmploymentSourceId,
                ScreeningGeneralOpinion = e.Screening == null ? null : e.Screening.GeneralOpinion,
                ScreeningMotivation = e.Screening == null ? null : e.Screening.Motivation,
                ScreeningWorkplaceExpectations = e.Screening == null ? null : e.Screening.WorkplaceExpectations,
                ScreeningSkills = e.Screening == null ? null : e.Screening.Skills,
                ScreeningLanguageEnglish = e.Screening == null ? null : e.Screening.LanguageEnglish,
                ScreeningLanguageGerman = e.Screening == null ? null : e.Screening.LanguageGerman,
                ScreeningLanguageOther = e.Screening == null ? null : e.Screening.LanguageOther,
                ScreeningContractExpectations = e.Screening == null ? null : e.Screening.ContractExpectations,
                ScreeningAvailability = e.Screening == null ? null : e.Screening.Availability,
                ScreeningOtherProcesses = e.Screening == null ? null : e.Screening.OtherProcesses,
                ScreeningCounterOfferCriteria = e.Screening == null ? null : e.Screening.CounterOfferCriteria,
                ScreeningRelocationCanDo = e.Screening == null ? null : e.Screening.RelocationCanDo,
                ScreeningRelocationComment = e.Screening == null ? null : e.Screening.RelocationComment,
                ScreeningBusinessTripsCanDo = e.Screening == null ? null : e.Screening.BusinessTripsCanDo,
                ScreeningBusinessTripsComment = e.Screening == null ? null : e.Screening.BusinessTripsComment,
                ScreeningEveningWorkCanDo = e.Screening == null ? null : e.Screening.EveningWorkCanDo,
                ScreeningEveningWorkComment = e.Screening == null ? null : e.Screening.EveningWorkComment,
                ScreeningWorkPermitNeeded = e.Screening == null ? null : e.Screening.WorkPermitNeeded,
                ScreeningWorkPermitComment = e.Screening == null ? null : e.Screening.WorkPermitComment,
                StepInfos = e.ProcessSteps != null
                    ? e.ProcessSteps.Where(s => !s.IsDeleted).Select(recruitmentProcessStepClassMapping.CreateFromSource).ToArray()
                    : new RecruitmentProcessStepInfoDto[0],
                CrmId = e.CrmId,
                DisplayName = RecruitmentProcessBusinessLogic.DisplayName.Call(e),
                IsMatureEnoughForHiringManager = e.IsMatureEnoughForHiringManager,
                CanBeReopened = RecruitmentProcessBusinessLogic.CanBeReopened.Call(e),
                OnboardingRequestStatus = e.OnboardingRequest != null ? e.OnboardingRequest.Request.Status : (RequestStatus?)null,
                CreatedOn = e.CreatedOn,
                CreatedByFullName = e.CreatedBy == null ? null : UserBusinessLogic.FullName.Call(e.CreatedBy),
                LastActivityOn = e.LastActivityOn,
                Timestamp = e.Timestamp,
            };
        }

        public override RecruitmentProcessDto CreateFromSource(RecruitmentProcess sourceObject, CreationContext context)
        {
            var result = base.CreateFromSource(sourceObject, context);

            if (context != null && context.RetrievalScenario == RetrievalScenario.SingleRecord)
            {
                result.Notes = sourceObject.RecruitmentProcessNotes != null
                    ? sourceObject.RecruitmentProcessNotes.Filtered()
                        .Select(_recruitmentProcessNoteClassMapping.CreateFromSource).ToArray()
                    : null;

                result.HistoryEntries = sourceObject.RecruitmentProcessHistories != null
                    ? sourceObject.RecruitmentProcessHistories.Filtered().OrderByDescending(h => h.PerformedOn)
                        .Select(_recruitmentProcessHistoryMapping.CreateFromSource).ToArray()
                    : null;
            }

            return result;
        }
    }
}