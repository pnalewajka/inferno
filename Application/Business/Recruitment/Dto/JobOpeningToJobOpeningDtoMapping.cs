﻿using System.Linq;
using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Helpers;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class JobOpeningToJobOpeningDtoMapping : ClassMapping<JobOpening, JobOpeningDto>
    {
        private readonly IClassMapping<JobOpeningNote, JobOpeningNoteDto> _jobOpeningNoteMapping;
        private readonly IClassMapping<JobOpeningHistoryEntry, HistoryEntryDto> _jobOpeningHistoryMapping;
        private readonly IClassMapping<ProjectDescription, ProjectDescriptionDto> _projectDescriptionsMapping;

        public JobOpeningToJobOpeningDtoMapping(
            IPrincipalProvider principalProvider,
            IClassMappingFactory classMappingFactory)
        {
            _projectDescriptionsMapping = classMappingFactory.CreateMapping<ProjectDescription, ProjectDescriptionDto>();
            _jobOpeningNoteMapping = classMappingFactory.CreateMapping<JobOpeningNote, JobOpeningNoteDto>();
            _jobOpeningHistoryMapping = classMappingFactory.CreateMapping<JobOpeningHistoryEntry, HistoryEntryDto>();

            Mapping = e => new JobOpeningDto
            {
                Id = e.Id,
                CustomerId = e.CustomerId,
                CustomerName = e.Customer != null ? e.Customer.ShortName : null,
                JobProfileIds = e.JobProfiles != null ? e.JobProfiles.OrderBy(t => t.Name).Select(t => t.Id).ToArray() : null,
                JobProfileNames = e.JobProfiles != null ? e.JobProfiles.OrderBy(t => t.Name).Select(t => t.Name).ToArray() : null,
                CityIds = e.Cities != null ? e.Cities.OrderBy(l => l.Name).Select(l => l.Id).ToArray() : null,
                CityNames = e.Cities != null ? e.Cities.OrderBy(l => l.Name).Select(l => l.Name).ToArray() : null,
                PositionName = e.PositionName,
                OrgUnitId = e.OrgUnitId,
                CompanyId = e.CompanyId,
                AcceptingEmployeeFullName = e.AcceptingEmployeeId.HasValue ? e.AcceptingEmployee.FullName : null,
                Priority = e.Priority,
                HiringMode = e.HiringMode,
                RoleOpenedOn = e.RoleOpenedOn,
                VacancyLevel1 = e.VacancyLevel1,
                VacancyLevel2 = e.VacancyLevel2,
                VacancyLevel3 = e.VacancyLevel3,
                VacancyLevel4 = e.VacancyLevel4,
                VacancyLevel5 = e.VacancyLevel5,
                VacancyLevelNotApplicable = e.VacancyLevelNotApplicable,
                VacancyTotal = JobOpeningBusinessLogic.VacancyTotal.Call(e),
                RecruitersIds = e.Recruiters.Select(r => r.Id).ToArray(),
                RecruitmentOwnersIds = e.RecruitmentOwners.Select(r => r.Id).ToArray(),
                IsTechnicalVerificationRequired = e.IsTechnicalVerificationRequired,
                IsIntiveResumeRequired = e.IsIntiveResumeRequired,
                IsPolishSpeakerRequired = e.IsPolishSpeakerRequired,
                NoticePeriod = e.NoticePeriod,
                SalaryRate = e.SalaryRate,
                EnglishLevel = e.EnglishLevel,
                OtherLanguages = e.OtherLanguages,
                AgencyEmploymentMode = e.AgencyEmploymentMode,
                Status = e.Status,
                WatcherIds = e.Watchers != null ? e.Watchers.Select(w => w.Id).ToArray() : new long[0],
                Timestamp = e.Timestamp,
                CanBeApprovedByCurrentUser = e.Status == JobOpeningStatus.PendingApproval
                  && principalProvider.Current.IsInRole(SecurityRoleType.CanApproveJobOpenings),
                IsHighlyConfidential = e.IsHighlyConfidential,
                DecisionMakerId = e.DecisionMakerEmployeeId,
                CreatedByFullName = e.CreatedById != null ? UserBusinessLogic.FullName.Call(e.CreatedBy) : null,
                CreatedOn = e.CreatedOn,
                IsWatchedByCurrentUser = e.Watchers != null && e.Watchers.Any(w => w.Id == principalProvider.Current.EmployeeId),
                RejectionReason = e.RejectionReason,
                CrmId = e.CrmId,
                TechnicalReviewComment = e.TechnicalReviewComment
            };
        }

        public override JobOpeningDto CreateFromSource(JobOpening sourceObject, CreationContext context)
        {
            var result = base.CreateFromSource(sourceObject, context);

            if (context != null && context.RetrievalScenario == RetrievalScenario.SingleRecord)
            {
                result.Notes = sourceObject.JobOpeningNotes != null
                  ? sourceObject.JobOpeningNotes.Filtered().Select(_jobOpeningNoteMapping.CreateFromSource).ToArray()
                  : null;

                result.HistoryEntries = sourceObject.JobOpeningHistories != null
                  ? sourceObject.JobOpeningHistories.Filtered().OrderByDescending(h => h.PerformedOn).Select(_jobOpeningHistoryMapping.CreateFromSource).ToArray()
                  : null;

                result.ProjectDescriptions = sourceObject.ProjectDescriptions != null
                  ? sourceObject.ProjectDescriptions.Select(_projectDescriptionsMapping.CreateFromSource).ToArray()
                  : null;
            }

            return result;
        }
    }
}
