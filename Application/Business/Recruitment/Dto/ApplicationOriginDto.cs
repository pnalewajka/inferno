﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class ApplicationOriginDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public ApplicationOriginType OriginType { get; set; }

        public string ParserIdentifier { get; set; }

        public string EmailAddress { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
