﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class JobOpeningNoteDto
    {
        public long Id { get; set; }

        public long JobOpeningId { get; set; }

        public NoteType NoteType { get; set; }

        public NoteVisibility NoteVisibility { get; set; }

        public string Content { get; set; }

        public long? CreatedById { get; set; }

        public long? ImpersonatedCreatedById { get; set; }

        public DateTime CreatedOn { get; set; }

        public bool IsDeleted { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public string CreatedByFullName { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
