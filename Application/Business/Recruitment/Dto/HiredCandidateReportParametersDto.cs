﻿using System;
using System.Collections.Generic;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class HiredCandidateReportParametersDto
    {
        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }

        public List<long> RecruiterIds { get; set; }

        public List<long> JobProfileIds { get; set; }

        public List<long> CityIds { get; set; }
    }
}
