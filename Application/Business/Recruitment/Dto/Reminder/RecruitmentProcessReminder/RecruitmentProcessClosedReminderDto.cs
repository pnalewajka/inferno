﻿namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecruitmentProcessClosedReminderDto
    {
        public string FirstName { get; set; }

        public long RecruitmentProcessId { get; set; }

        public string Reason { get; set; }

        public string Comment { get; set; }

        public string ServerAddress { get; set; }

        public string DisplayName { get; set; }
    }
}
