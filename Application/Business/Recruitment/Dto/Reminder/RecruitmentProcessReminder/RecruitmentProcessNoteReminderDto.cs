﻿namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecruitmentProcessNoteReminderDto
    {
        public string ProcessUrl { get; set; }

        public string ProcessName { get; set; }

        public string ProcessNoteUrl { get; set; }

        public string ModifiedByFullName { get; set; }

        public string NoteContent { get; set; }
    }
}
