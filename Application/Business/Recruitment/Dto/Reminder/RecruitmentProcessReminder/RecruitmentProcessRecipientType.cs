﻿namespace Smt.Atomic.Business.Recruitment.Dto.ReminderDto
{
    public enum RecruitmentProcessRecipientType
    {
        AllDecisionMakers,

        Recruiter,

        Watchers,

        RecruitmentLeaders,

        OtherAttendees,

        StepAssignedEmployee,

        NoteMentionedEmployee
    }
}
