﻿using System;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecruitmentProcessStepReminderDto
    {
        public string ProcessStepUrl { get; set; }

        public string ProcessStepName { get; set; }

        public string ProcessUrl { get; set; }

        public string ProcessName { get; set; }

        public string PreviousProcessStepUrl { get; set; }

        public string PreviousProcessStepName { get; set; }

        public long ParentProcessId { get; set; }

        public string PositionUrl { get; set; }

        public string PositionName { get; set; }

        public string ModifiedByFullName { get; set; }

        public string AssigneeEmployeeFullName { get; set; }

        public string CandidateFullName { get; set; }

        public string Comment { get; set; }

        public DateTime? PlannedOn { get; set; }

        public bool IsForWatcher { get; set; } = false;
    }
}
