﻿namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecruitmentProcessReminderDto
    {
        public string ProcessUrl { get; set; }

        public string ProcessName { get; set; }

        public string PositionUrl { get; set; }

        public string PositionName { get; set; }

        public string ModifiedByFullName { get; set; }

        public string CandidateFullName { get; set; }

        public bool IsForWatcher { get; set; } = false;
    }
}
