﻿namespace Smt.Atomic.Business.Recruitment.Dto.Reminder.InboundEmailReminder
{
    public class InboundEmailReminderDto
    {
        public string InboundEmailUrl { get; set; }
        public string From { get; set; }
        public string Subject { get; set; }
        public string Category { get; set; }
    }
}
