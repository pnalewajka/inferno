﻿namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class CandidateNoteReminderDto
    {
        public string CandidateUrl { get; set; }

        public string CandidateName { get; set; }

        public string CandidateNoteUrl { get; set; }

        public string ModifiedByFullName { get; set; }

        public string NoteContent { get; set; }
    }
}
