﻿namespace Smt.Atomic.Business.Recruitment.Dto.Reminder
{
    public enum CandidateRecipientType
    {
        NoteMentionedEmployee,
        Watchers,
        ContactCoordinator
    }
}
