﻿namespace Smt.Atomic.Business.Recruitment.Dto.Reminder.JobOpeningReminder
{
    public class JobOpeningAssignmentReminderDto : BaseJobOpeningReminderDto
    {
        public string ModifiedByFullName { get; set; }
    }
}
