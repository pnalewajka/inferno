﻿namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class JobOpeningClosedReminderDto : BaseJobOpeningReminderDto
    {
        public string ClosedReason { get; internal set; }
        public string ClosedComment { get; internal set; }
    }
}
