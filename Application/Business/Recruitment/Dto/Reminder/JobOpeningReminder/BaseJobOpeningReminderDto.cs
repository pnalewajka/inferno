﻿namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class BaseJobOpeningReminderDto
    {
        public long JobOpeningId { get; set; }

        public string JobOpeningName { get; set; }

        public string JobOpeningUrl { get; set; }

        public string JobOpeningApproverFullName { get; set; }

        public string ServerAddress { get; set; }
    }
}
