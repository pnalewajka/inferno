﻿namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class JobOpeningApprovalReminderDto : BaseJobOpeningReminderDto
    {
        public string JobOpeningAuthorFullName { get; set; }
    }
}
