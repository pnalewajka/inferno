﻿namespace Smt.Atomic.Business.Recruitment.Dto.JobOpeningReminder
{
    public class JobOpeningReopenReminderDto : BaseJobOpeningReminderDto
    {
        public string JobOpeningReopenedByFullName { get; set; }
    }
}
