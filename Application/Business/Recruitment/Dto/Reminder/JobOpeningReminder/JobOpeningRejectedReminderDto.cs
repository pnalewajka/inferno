﻿namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class JobOpeningRejectedReminderDto : BaseJobOpeningReminderDto
    {
        public string Reason { get; set; }
    }
}
