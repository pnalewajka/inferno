﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Recruitment.Dto.JobOpeningReminder
{
    public class JobOpeningStatusChangedReminderDto : BaseJobOpeningReminderDto
    {
        public IList<RecruitmentProcessLinkDto> RecruitmentProcesses { get; set; }
    }
}
