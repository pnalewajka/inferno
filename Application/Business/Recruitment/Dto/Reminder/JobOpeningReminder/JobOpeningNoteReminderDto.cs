﻿namespace Smt.Atomic.Business.Recruitment.Dto.Reminder.JobOpeningReminder
{
    public class JobOpeningNoteReminderDto
    {
        public string JobOpeningUrl { get; set; }

        public string JobOpeningName { get; set; }

        public string JobOpeningNoteUrl { get; set; }

        public string ModifiedByFullName { get; set; }

        public string NoteContent { get; set; }

        public bool IsWatcher { get; set; }
    }
}
