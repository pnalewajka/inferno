﻿namespace Smt.Atomic.Business.Recruitment.Dto.Reminder.JobOpeningReminder
{
    public enum JobOpeningRecipientType
    {
        Creator,
        RecruitmentLeaders,
        RecruitmentInternal,
        Watchers,
        Owners,
        Recruiters,
        NoteMentionedEmployees
    }
}
