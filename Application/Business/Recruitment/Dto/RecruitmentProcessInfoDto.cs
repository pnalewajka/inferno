﻿using System;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecruitmentProcessInfoDto
    {
        public long Id { get; set; }

        public string PositionName { get; set; }

        public long RecruiterId { get; set; }

        public string RecruiterFullName { get; set; }

        public DateTime? LastActivityOn { get; set; }

        public string LastActivityBy { get; set; }
    }
}
