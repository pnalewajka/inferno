﻿using System;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RejectedCandidateReportParametersDto
    {
        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }

        public List<long> RecruiterIds { get; set; }

        public List<long> RejectingEmployeeIds { get; set; }

        public long? OrgUnitId { get; set; }

        public RecruitmentProcessClosedReason? RejectionReason { get; set; }
    }
}
