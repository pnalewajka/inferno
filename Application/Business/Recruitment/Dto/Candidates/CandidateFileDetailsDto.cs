﻿using System;
using Smt.Atomic.Business.Recruitment.Enums;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class CandidateFileDetailsDto
    {
        public long Id { get; set; }

        public string Comment { get; set; }

        public CandidataFileOrigin CandidateFileOrigin { get; set; }

        public string DocumentName { get; set; }

        public string CreatedByFullName { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime ModifiedOn { get; set; }

        public RecruitmentDocumentType DocumentType { get; set; }
    }
}
