﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class CandidateNoteDtoToCandidateNoteMapping : ClassMapping<CandidateNoteDto, CandidateNote>
    {
        public CandidateNoteDtoToCandidateNoteMapping()
        {
            Mapping = d => new CandidateNote
            {
                Id = d.Id,
                CandidateId = d.CandidateId,
                NoteType = d.NoteType,
                NoteVisibility = d.NoteVisibility,
                Content = d.Content,
                ImpersonatedCreatedById = d.ImpersonatedCreatedById,
                CreatedOn = d.CreatedOn,
                IsDeleted = d.IsDeleted,
                Timestamp = d.Timestamp
            };
        }
    }
}
