﻿namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class CandidatePickerContext
    {
        public bool OnlyValidConsent { get; set; }

        public bool AllowAnonymous { get; set; }
    }
}
