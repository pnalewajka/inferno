﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class CandidateNoteDto
    {
        public long Id { get; set; }

        public long CandidateId { get; set; }

        public NoteType NoteType { get; set; }

        public NoteVisibility NoteVisibility { get; set; }

        public string Content { get; set; }

        public long? ImpersonatedCreatedById { get; set; }

        public DateTime CreatedOn { get; set; }

        public bool IsDeleted { get; set; }

        public string CreatedByFullName { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
