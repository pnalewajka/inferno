﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class CandidateDataConsentDto
    {
        public string DataAdministrator { get; set; }

        public DataConsentType ConsentType { get; set; }

        public string Scope { get; set; }

        public DateTime? ExpiresOn { get; set; }
    }
}