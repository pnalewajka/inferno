﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;

namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class CandidateDtoToCandidateMapping : ClassMapping<CandidateDto, Candidate>
    {
        public CandidateDtoToCandidateMapping()
        {
            Mapping = d => new Candidate
            {
                Id = d.Id,
                FirstName = d.FirstName,
                LastName = d.LastName,
                ContactRestriction = d.ContactRestriction,
                MobilePhone = d.MobilePhone,
                Cities = d.CityIds == null ? null : new List<City>(d.CityIds.Select(v => new City { Id = v })),
                JobProfiles = d.JobProfileIds == null ? null : new List<JobProfile>(d.JobProfileIds.Select(v => new JobProfile { Id = v })),
                Watchers = d.WatcherIds == null ? null : new List<Employee>(d.WatcherIds.Select(v => new Employee { Id = v })),
                Languages = d.Languages.Select(l => new CandidateLanguage
                {
                    CandidateId = d.Id,
                    LanguageId = l.LanguageId,
                    Level = l.Level
                }).ToList(),
                OtherPhone = d.OtherPhone,
                EmailAddress = d.EmailAddress,
                OtherEmailAddress = d.OtherEmailAddress,
                SkypeLogin = d.SkypeLogin,
                SocialLink = d.SocialLink,
                NextFollowUpDate = d.NextFollowUpDate,
                EmployeeStatus = d.EmployeeStatus,
                CanRelocate = d.CanRelocate,
                RelocateDetails = d.RelocateDetails,
                ResumeTextContent = d.ResumeTextContent,
                ContactCoordinatorId = d.ContactCoordinatorId,
                ConsentCoordinatorId = d.ConsentCoordinatorId,
                IsHighlyConfidential = d.IsHighlyConfidential,
                CrmId = d.CrmId,
                OriginalSourceId = d.OriginalSourceId,
                OriginalSourceComment = d.OriginalSourceComment,
                Timestamp = d.Timestamp
            };
        }
    }
}
