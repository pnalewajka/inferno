﻿using System;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class CandidateTechnicalReviewDetailsDto
    {
        public long Id { get; set; }

        public SeniorityLevelEnum SeniorityLevelAssessment { get; set; }

        public SeniorityLevelEnum SeniorityLevelExpectation { get; set; }

        public List<String> JobProfileNames { get; set; }

        public string TechnicalAssignmentAssessment { get; set; }

        public string SolutionDesignAssessment { get; set; }

        public string TechnicalKnowledgeAssessment { get; set; }

        public string TeamProjectSuitabilityAssessment { get; set; }

        public string EnglishUsageAssessment { get; set; }

        public string RiskAssessment { get; set; }

        public string StrengthAssessment { get; set; }

        public string OtherRemarks { get; set; }

        public RecruitmentProcessStepDecision Decision { get; set; }

        public DateTime CreatedOn { get; set; }

        public string CreatedByFullName { get; set; }
    }
}
