﻿using System.Collections.ObjectModel;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class CandidateContactRecordDtoToCandidateContactRecordMapping : ClassMapping<CandidateContactRecordDto, CandidateContactRecord>
    {
        public CandidateContactRecordDtoToCandidateContactRecordMapping()
        {
            Mapping = d => new CandidateContactRecord
            {
                Id = d.Id,
                CandidateId = d.CandidateId,
                ContactType = d.ContactType,
                ContactedOn = d.ContactedOn,
                ContactedById = d.ContactedByEmployeeId,
                RelevantJobOpenings = d.RelevantJobOpeningIds == null ? null : new Collection<JobOpening>(d.RelevantJobOpeningIds.Select(v => new JobOpening { Id = v }).ToList()),
                CandidateReaction = d.CandidateReaction,
                Comment = d.Comment
            };
        }
    }
}
