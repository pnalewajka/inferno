﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.GDPR;

namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class DataConsentToCandidateDataConsentDtoMapping : ClassMapping<DataConsent, CandidateDataConsentDto>
    {
        public DataConsentToCandidateDataConsentDtoMapping()
        {
            Mapping = e => new CandidateDataConsentDto
            {
                DataAdministrator = e.DataAdministrator.Name,
                ExpiresOn = e.ExpiresOn,
                Scope = e.Scope,
                ConsentType = e.Type
            };
        }
    }
}
