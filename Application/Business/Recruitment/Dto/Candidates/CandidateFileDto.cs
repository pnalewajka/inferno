﻿using System;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class CandidateFileDto
    {
        public long Id { get; set; }

        public long CandidateId { get; set; }

        public RecruitmentDocumentType DocumentType { get; set; }

        public string Comment { get; set; }

        public bool IsDeleted { get; set; }

        public DocumentDto[] File { get; set; }

        public byte[] Timestamp { get; set; }

        public FileSource FileSource { get; set; }

        public bool IsDataConsent { get; set; }

        public DateTime ModifiedOn { get; set; }

        public CandidateFileDto()
        {
            File = new DocumentDto[0];
        }
    }
}
