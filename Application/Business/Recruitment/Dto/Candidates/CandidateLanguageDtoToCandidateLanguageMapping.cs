﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class CandidateLanguageDtoToCandidateLanguageMapping : ClassMapping<CandidateLanguageDto, CandidateLanguage>
    {
        public CandidateLanguageDtoToCandidateLanguageMapping()
        {
            Mapping = d => new CandidateLanguage
            {
                LanguageId = d.LanguageId,
                Level = d.Level
            };
        }
    }
}
