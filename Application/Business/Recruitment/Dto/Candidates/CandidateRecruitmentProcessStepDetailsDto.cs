﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class CandidateRecruitmentProcessStepDetailsDto
    {
        public long Id { get; set; }

        public RecruitmentProcessStepDecision Decision { get; set; }

        public RecruitmentProcessStepType Type { get; set; }

        public string Comment { get; set; }

        public DateTime CreatedOn { get; set; }

        public string CreatedByFullName { get; set; }
    }
}
