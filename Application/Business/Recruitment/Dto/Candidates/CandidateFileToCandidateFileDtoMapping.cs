﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class CandidateFileToCandidateFileDtoMapping : ClassMapping<CandidateFile, CandidateFileDto>
    {
        public CandidateFileToCandidateFileDtoMapping()
        {
            Mapping = e => new CandidateFileDto
            {
                Id = e.Id,
                CandidateId = e.CandidateId,
                DocumentType = e.DocumentType,
                Comment = e.Comment,
                IsDeleted = e.IsDeleted,
                File = e.File
                    .Select(p => new DocumentDto
                    {
                        ContentType = p.ContentType,
                        DocumentName = p.Name,
                        DocumentId = p.Id
                    })
                    .ToArray(),
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
