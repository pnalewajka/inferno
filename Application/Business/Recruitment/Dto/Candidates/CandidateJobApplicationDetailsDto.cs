﻿using System;

namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class CandidateJobApplicationDetailsDto
    {
        public long Id { get; set; }

        public string CandidateComment { get; set; }

        public string OriginName { get; set; }

        public string OriginComment { get; set; }

        public DateTime CreatedOn { get; set; }

        public string CreatedByFullName { get; set; }
    }
}
