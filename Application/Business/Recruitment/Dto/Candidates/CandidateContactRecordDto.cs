﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class CandidateContactRecordDto
    {
        public long Id { get; set; }

        public long CandidateId { get; set; }

        public ContactType ContactType { get; set; }

        public DateTime ContactedOn { get; set; }

        public long ContactedByEmployeeId { get; set; }

        public CandidateReaction CandidateReaction { get; set; }

        public long[] RelevantJobOpeningIds { get; set; }

        public string Comment { get; set; }

        public DateTime? NextContactdOn { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
