﻿using System.Collections.Generic;
using Smt.Atomic.Business.Recruitment.SnapshotComparer;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class CandidateChangesNotificationDto
    {
        public long CandidateId { get; set; }

        public string CandidateFullName { get; set; }

        public string ServerAddress { get; set; }

        public string ChangedOnDate { get; set; }

        public string ChangedOnTime { get; set; }

        public string ChangedByFullName { get; set; }

        public IDictionary<string, ValueChange> Changes { get; set; }
    }
}
