﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class CandidateLanguageToCandidateLanguageDtoMapping : ClassMapping<CandidateLanguage, CandidateLanguageDto>
    {
        public CandidateLanguageToCandidateLanguageDtoMapping()
        {
            Mapping = e => new CandidateLanguageDto
            {
                LanguageId = e.LanguageId,
                Language = new LanguageDto
                {
                    Id = e.Language.Id,
                    Name = new LocalizedString
                    {
                        English = e.Language.NameEn,
                        Polish = e.Language.NamePl
                    }
                },
                Level = e.Level
            };
        }
    }
}
