﻿using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class CandidateNoteToCandidateNoteDtoMapping : ClassMapping<CandidateNote, CandidateNoteDto>
    {
        public CandidateNoteToCandidateNoteDtoMapping()
        {
            Mapping = e => new CandidateNoteDto
            {
                Id = e.Id,
                CandidateId = e.CandidateId,
                NoteType = e.NoteType,
                NoteVisibility = e.NoteVisibility,
                Content = e.Content,
                ImpersonatedCreatedById = e.ImpersonatedCreatedById,
                CreatedOn = e.CreatedOn,
                IsDeleted = e.IsDeleted,
                CreatedByFullName = e.CreatedBy != null ? UserBusinessLogic.FullName.Call(e.CreatedBy) : null,
                Timestamp = e.Timestamp
            };
        }
    }
}
