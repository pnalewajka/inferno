﻿namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class CandidateSearchContext
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string EmailAddress { get; set; }

        public string PhoneNumber { get; set; }

        public string SocialLink { get; set; }
    }
}
