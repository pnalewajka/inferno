﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class CandidateContactRecordToCandidateContactRecordDtoMapping : ClassMapping<CandidateContactRecord, CandidateContactRecordDto>
    {
        public CandidateContactRecordToCandidateContactRecordDtoMapping()
        {
            Mapping = e => new CandidateContactRecordDto
            {
                Id = e.Id,
                CandidateId = e.CandidateId,
                ContactType = e.ContactType,
                ContactedOn = e.ContactedOn,
                ContactedByEmployeeId = e.ContactedById,
                CandidateReaction = e.CandidateReaction,
                Comment = e.Comment,
                RelevantJobOpeningIds = e.RelevantJobOpenings.Select(o => o.Id).ToArray(),
                Timestamp = e.Timestamp
            };
        }
    }
}
