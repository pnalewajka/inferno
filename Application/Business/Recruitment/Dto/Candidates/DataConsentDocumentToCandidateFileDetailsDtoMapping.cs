﻿using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Enums;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.GDPR;

namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class DataConsentDocumentToCandidateFileDetailsDtoMapping : ClassMapping<DataConsentDocument, CandidateFileDetailsDto>
    {
        public DataConsentDocumentToCandidateFileDetailsDtoMapping()
        {
            Mapping = d => new CandidateFileDetailsDto
            {
                Id = d.Id,
                DocumentName = d.Name,
                CreatedByFullName = d.ModifiedBy != null ? UserBusinessLogic.FullName.Call(d.ModifiedBy) : null,
                DocumentType = RecruitmentDocumentType.Consent,
                ModifiedOn = d.ModifiedOn,
                CandidateFileOrigin = CandidataFileOrigin.DataConsentFile
            };
        }
    }
}
