﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class CandidateLanguageDto
    {
        public long LanguageId { get; set; }

        public LanguageDto Language { get; set; }

        public LanguageReferenceLevel Level { get; set; }
    }
}
