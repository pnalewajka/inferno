﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.GDPR;

namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class DataConsentDocumentToCandidateFileDtoMapping : ClassMapping<DataConsentDocument, CandidateFileDto>
    {
        public DataConsentDocumentToCandidateFileDtoMapping()
        {
            Mapping = dc => new CandidateFileDto
            {
                Id = dc.Id,
                CandidateId = dc.DataConsent.DataOwner.CandidateId.Value,
                DocumentType = RecruitmentDocumentType.Consent,
                IsDataConsent = true,
                File = new[] {
                        new DocumentDto {
                                ContentType = dc.ContentType,
                                DocumentId = dc.Id,
                                DocumentName = dc.Name
                            }
                        },
                ModifiedOn = dc.ModifiedOn
            };
        }
    }
}
