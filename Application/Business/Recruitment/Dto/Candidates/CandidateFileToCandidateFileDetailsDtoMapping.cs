﻿using System.Linq;
using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class CandidateFileToCandidateFileDetailsDtoMapping : ClassMapping<CandidateFile, CandidateFileDetailsDto>
    {
        public CandidateFileToCandidateFileDetailsDtoMapping()
        {
            Mapping = d => new CandidateFileDetailsDto
            {
                Id = d.Id,
                Comment = d.Comment,
                DocumentName = d.File.Any() ? d.File.Select(f => f.Name).FirstOrDefault() : null,
                CreatedOn = d.CreatedOn,
                CreatedByFullName = d.CreatedBy != null ? UserBusinessLogic.FullName.Call(d.CreatedBy) : null,
                DocumentType = d.DocumentType,
                ModifiedOn = d.ModifiedOn,
                CandidateFileOrigin = CandidataFileOrigin.CandidateFile
            };
        }
    }
}
