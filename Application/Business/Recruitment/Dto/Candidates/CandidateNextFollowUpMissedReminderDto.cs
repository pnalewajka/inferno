﻿using System;

namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class CandidateNextFollowUpMissedReminderDto
    {
        public string RecipientDisplayName { get; set; }

        public string CandidateUrl { get; set; }

        public string CandidateFullName { get; set; }

        public DateTime NextFollowUpDate { get; set; }
    }
}