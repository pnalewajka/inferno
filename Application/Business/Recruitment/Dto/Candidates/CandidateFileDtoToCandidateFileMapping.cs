﻿using System.Collections.ObjectModel;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class CandidateFileDtoToCandidateFileMapping : ClassMapping<CandidateFileDto, CandidateFile>
    {
        public CandidateFileDtoToCandidateFileMapping()
        {
            Mapping = d => new CandidateFile
            {
                Id = d.Id,
                CandidateId = d.CandidateId,
                DocumentType = d.DocumentType,
                Comment = d.Comment,
                IsDeleted = d.IsDeleted,
                File = new Collection<CandidateFileDocument>(),
                Timestamp = d.Timestamp
            };
        }
    }
}
