﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class CandidateToCandidateDtoMapping : ClassMapping<Candidate, CandidateDto>
    {
        public CandidateToCandidateDtoMapping(
            ITimeService timeService,
            IPrincipalProvider principalProvider,
            IClassMappingFactory mappingFactory)
        {
            var recruitmentProcessMapping = mappingFactory.CreateMapping<RecruitmentProcess, RecruitmentProcessInfoDto>();

            Mapping = e => new CandidateDto
            {
                Id = e.Id,
                FirstName = e.FirstName,
                LastName = e.LastName,
                ContactRestriction = e.ContactRestriction,
                MobilePhone = e.MobilePhone,
                OtherPhone = e.OtherPhone,
                CityIds = e.Cities.Select(l => l.Id).ToArray(),
                CityNames = e.Cities.Select(l => l.Name).ToArray(),
                JobProfileIds = e.JobProfiles.Select(p => p.Id).ToArray(),
                JobProfileNames = e.JobProfiles.Select(p => p.Name).ToArray(),
                Languages = e.Languages == null ? null : e.Languages.Select(l => new CandidateLanguageDto
                {
                    LanguageId = l.LanguageId,
                    Language = new LanguageDto
                    {
                        Id = l.LanguageId,
                        Name = new LocalizedString
                        {
                            English = l.Language == null ? null : l.Language.NameEn,
                            Polish = l.Language == null ? null : l.Language.NamePl
                        }
                    },
                    Level = l.Level
                }).ToList(),
                WatcherIds = e.Watchers.Select(w => w.Id).ToArray(),
                EmailAddress = e.EmailAddress,
                OtherEmailAddress = e.OtherEmailAddress,
                SkypeLogin = e.SkypeLogin,
                SocialLink = e.SocialLink,
                NextFollowUpDate = e.NextFollowUpDate,
                EmployeeStatus = e.EmployeeStatus,
                CanRelocate = e.CanRelocate,
                RelocateDetails = e.RelocateDetails,
                ResumeTextContent = e.ResumeTextContent,
                ContactCoordinatorId = e.ContactCoordinatorId,
                ConsentCoordinatorId = e.ConsentCoordinatorId,
                ModifiedById = e.ModifiedById,
                ModifiedByFullName = e.ModifiedBy == null ? null : UserBusinessLogic.FullName.Call(e.ModifiedBy),
                ModifiedOn = e.ModifiedOn,
                HasValidConsent = CandidateBusinessLogic.HasValidConsent.Call(e, timeService.GetCurrentDate()),
                Consents = new List<CandidateDataConsentDto>(),
                LastContactByEmployeeId = e.CandidateContactRecords.Any() ? e.CandidateContactRecords.OrderByDescending(c => c.CreatedOn).FirstOrDefault().Id : (long?)null,
                LastContactBy = e.CandidateContactRecords.Any() ? EmployeeBusinessLogic.FullName.Call(e.CandidateContactRecords.OrderByDescending(c => c.CreatedOn).FirstOrDefault().ContactedBy) : null,
                LastContactOn = e.CandidateContactRecords.Any() ? e.CandidateContactRecords.OrderByDescending(c => c.CreatedOn).FirstOrDefault().ContactedOn : (DateTime?)null,
                LastContactType = e.CandidateContactRecords.Any() ? e.CandidateContactRecords.OrderByDescending(c => c.CreatedOn).FirstOrDefault().ContactType : (ContactType?)null,
                FileDetails = new List<CandidateFileDetailsDto>(),
                JobApplicationDetails = new List<CandidateJobApplicationDetailsDto>(),
                ContactRecordDetails = new List<CandidateContactRecordDetailsDto>(),
                TechnicalReviewDetails = new List<CandidateTechnicalReviewDetailsDto>(),
                RecruitmentProcessStepDetails = new List<CandidateRecruitmentProcessStepDetailsDto>(),
                TechnicalReviewStepInfo = new List<RecruitmentProcessStepInfoDto>(),
                IsAnonymized = e.IsAnonymized,
                IsWatchedByCurrentUser = e.Watchers != null && e.Watchers.Any(w => w.Id == principalProvider.Current.EmployeeId),
                FullName = CandidateBusinessLogic.FullName.Call(e),
                LastProcessInfo = e.RecruitmentProcesses.IsNullOrEmpty() ? null : recruitmentProcessMapping.CreateFromSource(e.RecruitmentProcesses.OrderByDescending(p => p.LastActivityOn).First()),
                IsHighlyConfidential = e.IsHighlyConfidential,
                LastCompanyResumeDocumentId = GetFileIdOrDefault(e.CandidateFiles, RecruitmentDocumentType.CompanyResume),
                LastOriginalResumeDocumentId = GetFileIdOrDefault(e.CandidateFiles, RecruitmentDocumentType.OriginalResume),
                CrmId = e.CrmId,
                Notes = new List<CandidateNoteDto>(),
                OriginalSourceId = e.OriginalSourceId,
                OriginalSourceComment = e.OriginalSourceComment,
                CreatedOn = e.CreatedOn,
                CreatedByFullName = e.CreatedBy == null ? null : UserBusinessLogic.FullName.Call(e.CreatedBy),
                IsFavoriteCandidate = e.Favorites.Any(f => f.Id == principalProvider.Current.EmployeeId),
                RecentActivity = e.LastReferenceId == null || e.LastActivityBy == null || e.LastReferenceType == null || e.LastActivityOn == null
                    ? null
                    : new RecentActivityDto
                    {
                        ReferenceId = e.LastReferenceId.Value,
                        ReferenceType = e.LastReferenceType.Value,
                        UserFullName = e.LastActivityBy != null ? EmployeeBusinessLogic.FullName.Call(e.LastActivityBy) : null,
                        OccuredOn = e.LastActivityOn.Value,
                    },
                Timestamp = e.Timestamp,
            };
        }

        private long? GetFileIdOrDefault(ICollection<CandidateFile> candidateFiles, RecruitmentDocumentType documentType)
        {
            if (candidateFiles == null)
            {
                return null;
            }

            return candidateFiles
                .Where(f => CandidateBusinessLogic.FileIsNotDeletedAndOfGivenType.Call(f, documentType))
                .OrderByDescending(f => f.ModifiedOn)
                .Select(f => (long?)f.Id)
                .FirstOrDefault();
        }
    }
}
