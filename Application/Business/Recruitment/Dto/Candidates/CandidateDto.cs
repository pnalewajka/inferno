﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class CandidateDto
    {
        public long Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public ContactRestriction ContactRestriction { get; set; }

        public string MobilePhone { get; set; }

        public string OtherPhone { get; set; }

        public string EmailAddress { get; set; }

        public string OtherEmailAddress { get; set; }

        public string SkypeLogin { get; set; }

        public string SocialLink { get; set; }

        public DateTime? NextFollowUpDate { get; set; }

        public CandidateEmploymentStatus EmployeeStatus { get; set; }

        public bool CanRelocate { get; set; }

        public string RelocateDetails { get; set; }

        public long[] CityIds { get; set; }

        public string[] CityNames { get; set; }

        public long[] JobProfileIds { get; set; }

        public string[] JobProfileNames { get; set; }

        public List<CandidateLanguageDto> Languages { get; set; }

        public string ResumeTextContent { get; set; }

        public long ContactCoordinatorId { get; set; }

        public long ConsentCoordinatorId { get; set; }

        public long[] WatcherIds { get; set; }

        public long? ModifiedById { get; set; }

        public string ModifiedByFullName { get; set; }

        public DateTime ModifiedOn { get; set; }

        public bool HasValidConsent { get; set; }

        public List<CandidateDataConsentDto> Consents { get; set; }

        public long OriginalSourceId { get; set; }

        public string OriginalSourceComment { get; set; }

        public List<CandidateContactRecordDetailsDto> ContactRecordDetails { get; set; }

        public List<CandidateJobApplicationDetailsDto> JobApplicationDetails { get; set; }

        public List<CandidateFileDetailsDto> FileDetails { get; set; }

        public List<CandidateTechnicalReviewDetailsDto> TechnicalReviewDetails { get; set; }

        public List<RecruitmentProcessStepInfoDto> TechnicalReviewStepInfo { get; set; }

        public List<CandidateRecruitmentProcessStepDetailsDto> RecruitmentProcessStepDetails { get; set; }

        public long? LastContactRecordId { get; set; }

        public long? LastContactByEmployeeId { get; set; }

        public string LastContactBy { get; set; }

        public DateTime? LastContactOn { get; set; }

        public ContactType? LastContactType { get; set; }

        public bool IsAnonymized { get; set; }

        public bool IsHighlyConfidential { get; set; }

        public Guid? CrmId { get; set; }

        public bool IsWatchedByCurrentUser { get; set; }

        public CandidateWorkflowAction? WorkflowAction { get; set; }

        public string FullName { get; set; }

        public RecruitmentProcessInfoDto LastProcessInfo { get; set; }

        public long? LastOriginalResumeDocumentId { get; set; }

        public long? LastCompanyResumeDocumentId { get; set; }

        public List<CandidateNoteDto> Notes { get; set; }

        public bool ShouldAddRelatedDataConsent { get; set; }

        public DataConsentType? ConsentType { get; set; }

        public string ConsentScope { get; set; }

        public DocumentDto[] Documents { get; set; }

        public string CreatedByFullName { get; set; }

        public DateTime CreatedOn { get; set; }

        public bool IsFavoriteCandidate { get; set; }

        public RecentActivityDto RecentActivity { get; set; }

        public byte[] Timestamp { get; set; }

        public CandidateDto()
        {
            Documents = new DocumentDto[0];
        }
    }
}
