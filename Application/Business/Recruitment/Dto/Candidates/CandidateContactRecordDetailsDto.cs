﻿using System;

namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class CandidateContactRecordDetailsDto
    {
        public long Id { get; set; }

        public string CreatedByFullName { get; set; }

        public string Comment { get; set; }

        public DateTime ContactedOn { get; set; }
    }
}
