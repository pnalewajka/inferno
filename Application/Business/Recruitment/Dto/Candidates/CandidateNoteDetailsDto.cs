﻿using System;

namespace Smt.Atomic.Business.Recruitment.Dto.Candidates
{
    public class CandidateNoteDetailsDto
    {
        public long Id { get; set; }

        public string Content { get; set; }

        public DateTime CreatedOn { get; set; }

        public string CreatedByFullName { get; set; }
    }
}
