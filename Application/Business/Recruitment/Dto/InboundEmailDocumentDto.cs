﻿using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class InboundEmailDocumentDto : DocumentDto
    {
        public string To { get; set; }

        public string FromEmailAddress { get; set; }

        public string FromFullName { get; set; }

        public string Subject { get; set; }
    }
}
