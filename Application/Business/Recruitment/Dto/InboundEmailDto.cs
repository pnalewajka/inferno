﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class InboundEmailDto
    {
        public long Id { get; set; }

        public string To { get; set; }

        public string FromEmailAddress { get; set; }

        public string FromFullName { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public EmailBodyType BodyType { get; set; }

        public InboundEmailColorCategory? EmailColorCategory { get; set; }

        public DateTime SentOn { get; set; }

        public DateTime ReceivedOn { get; set; }

        public long? ProcessedById { get; set; }

        public long[] ReaderIds { get; set; }

        public bool HasBeenReadByCurrentUser { get; set; }

        public bool HasBeenReadByOthers { get; set; }

        public InboundEmailStatus Status { get; set; }

        public InboundEmailCategory Category { get; set; }

        public bool IsDeleted { get; set; }

        public DocumentDto[] Documents { get; set; }

        public List<InboundEmailValueDto> Values { get; set; }

        public InboundEmailWorkflowAction? WorkflowAction { get; set; }

        public long? WorkflowProcessedById { get; set; }

        public InboundEmailClosedReason? ClosedReason { get; set; }

        public string ClosedComment { get; set; }

        public CandidateDto MatchingCandidateDto { get; set; }

        public string ResearchingComment { get; set; }

        public byte[] Timestamp { get; set; }

        public InboundEmailDto()
        {
            Documents = new DocumentDto[0];
            Values = new List<InboundEmailValueDto>();
        }
    }
}
