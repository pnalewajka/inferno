﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class ProjectDescriptionDtoToProjectDescriptionMapping : ClassMapping<ProjectDescriptionDto, ProjectDescription>
    {
        public ProjectDescriptionDtoToProjectDescriptionMapping()
        {
            Mapping = d => new ProjectDescription
            {
                Id = d.Id,
                AboutClient = d.AboutClientAndProject,
                SpecificDuties = d.SpecificDuties,
                ToolsAndEnvironment = d.ToolsAndEnvironment,
                AboutTeam = d.AboutTeam,
                ProjectTimeline = d.ProjectTimeline,
                Methodology = d.Methodology,
                RequiredSkills = d.RequiredSkills,
                AdditionalSkills = d.AdditionalSkills,
                RecruitmentDetails = d.RecruitmentDetails,
                IsTravelRequired = d.IsTravelRequired,
                TravelAndAccomodation = d.TravelAndAccomodation,
                CanWorkRemotely = d.CanWorkRemotely,
                RemoteWorkArrangements = d.RemoteWorkArrangements,
                Comments = d.Comments,
                Timestamp = d.Timestamp
            };
        }
    }
}
