﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class TechnicalReviewDtoToTechnicalReviewMapping : ClassMapping<TechnicalReviewDto, TechnicalReview>
    {
        public TechnicalReviewDtoToTechnicalReviewMapping()
        {
            Mapping = d => new TechnicalReview
            {
                Id = d.Id,
                RecruitmentProcessId = d.RecruitmentProcessId,
                RecruitmentProcessStepId = d.RecruitmentProcessStepId,
                Timestamp = d.Timestamp,
                JobProfiles = d.JobProfileIds.Select(t => new JobProfile
                {
                    Id = t
                }).ToList(),
                SeniorityLevelAssessment = d.SeniorityLevelAssessment,
                TechnicalAssignmentAssessment = d.TechnicalAssignmentAssessment,
                TechnicalKnowledgeAssessment = d.TechnicalKnowledgeAssessment,
                TeamProjectSuitabilityAssessment = d.TeamProjectSuitabilityAssessment,
                EnglishUsageAssessment = d.EnglishUsageAssessment,
                RiskAssessment = d.RiskAssessment,
                StrengthAssessment = d.StrengthAssessment,
                OtherRemarks = d.OtherRemarks,
                ModifiedOn = d.ModifiedOn,
            };
        }
    }
}
