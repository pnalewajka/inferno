﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class ContractNegotiationsDetailsDto
    {
        public DateTime? FinalStartDate { get; set; }

        public DateTime? FinalSignedDate { get; set; }

        public long? FinalPositionId { get; set; }

        public long? FinalTrialContractTypeId { get; set; }

        public long? FinalLongTermContractTypeId { get; set; }

        public string FinalTrialSalary { get; set; }

        public string FinalLongTermSalary { get; set; }

        public long? FinalLocationId { get; set; }

        public string FinalComment { get; set; }

        public long? OfferPositionId { get; set; }

        public string OfferContractType { get; set; }

        public string OfferSalary { get; set; }

        public string OfferStartDate { get; set; }

        public long? OfferLocationId { get; set; }

        public string OfferComment { get; set; }

        public long? FinalEmploymentSourceId { get; set; }
    }
}
