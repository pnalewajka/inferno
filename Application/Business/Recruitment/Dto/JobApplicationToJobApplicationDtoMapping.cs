﻿using System.Linq;
using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class JobApplicationToJobApplicationDtoMapping : ClassMapping<JobApplication, JobApplicationDto>
    {
        public JobApplicationToJobApplicationDtoMapping()
        {
            Mapping = e => new JobApplicationDto
            {
                Id = e.Id,
                CandidateId = e.CandidateId,
                OriginType = e.ApplicationOrigin != null ? e.ApplicationOrigin.OriginType : default(ApplicationOriginType),
                ApplicationOriginId = e.ApplicationOriginId,
                RecommendingPersonId = e.RecommendingPersonId,
                OriginComment = e.OriginComment,
                CityIds = e.Cities.Select(l => l.Id).ToArray(),
                JobProfileIds = e.JobProfiles.Select(t => t.Id).ToArray(),
                IsEligibleForReferralProgram = e.IsEligibleForReferralProgram,
                SourceEmailId = e.SourceEmailId,
                CandidateComment = e.CandidateComment,
                CreatedByFullName = e.CreatedBy != null ? UserBusinessLogic.FullName.Call(e.CreatedBy) : null,
                CreatedOn = e.CreatedOn,
                IsDeleted = e.IsDeleted,
                Timestamp = e.Timestamp
            };
        }
    }
}
