﻿using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class JobOpeningNoteToJobOpeningNoteDtoMapping : ClassMapping<JobOpeningNote, JobOpeningNoteDto>
    {
        public JobOpeningNoteToJobOpeningNoteDtoMapping()
        {
            Mapping = e => new JobOpeningNoteDto
            {
                Id = e.Id,
                JobOpeningId = e.JobOpeningId,
                NoteType = e.NoteType,
                NoteVisibility = e.NoteVisibility,
                Content = e.Content,
                CreatedById = e.CreatedById,
                ImpersonatedCreatedById = e.ImpersonatedCreatedById,
                CreatedOn = e.CreatedOn,
                IsDeleted = e.IsDeleted,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                CreatedByFullName = e.CreatedBy != null ? UserBusinessLogic.FullName.Call(e.CreatedBy) : null,
                Timestamp = e.Timestamp
            };
        }
    }
}
