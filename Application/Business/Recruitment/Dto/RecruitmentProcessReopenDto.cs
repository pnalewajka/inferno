﻿namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecruitmentProcessReopenDto
    {
        public long Id { get; set; }

        public string Comment { get; set; }
    }
}
