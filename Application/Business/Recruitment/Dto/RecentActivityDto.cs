﻿using System;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecentActivityDto
    {
        public ReferenceType ReferenceType { get; set; }

        public long ReferenceId { get; set; }

        public string UserFullName { get; set; }

        public DateTime OccuredOn { get; set; }
    }
}
