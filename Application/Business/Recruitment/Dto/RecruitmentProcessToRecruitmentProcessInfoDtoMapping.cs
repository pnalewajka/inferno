﻿using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecruitmentProcessToRecruitmentProcessInfoDtoMapping : ClassMapping<RecruitmentProcess, RecruitmentProcessInfoDto>
    {
        public RecruitmentProcessToRecruitmentProcessInfoDtoMapping()
        {
            Mapping = e => new RecruitmentProcessInfoDto
            {
                Id = e.Id,
                PositionName = e.JobOpening != null ? e.JobOpening.PositionName : null,
                RecruiterId = e.RecruiterId,
                RecruiterFullName = EmployeeBusinessLogic.FullName.Call(e.Recruiter),
                LastActivityOn = e.LastActivityOn,
                LastActivityBy = e.LastActivityBy == null ? null : EmployeeBusinessLogic.FullName.Call(e.LastActivityBy)
            };
        }
    }
}
