﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class VacanciesDto
    {
        public int VacancyNumber { get; set; }

        public SeniorityLevelEnum? Seniority { get; set; }
    }
}
