﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class InboundEmailValueToInboundEmailValueDtoMapping : ClassMapping<InboundEmailValue, InboundEmailValueDto>
    {
        public InboundEmailValueToInboundEmailValueDtoMapping()
        {
            Mapping = e => new InboundEmailValueDto
            {
                Id = e.Id,
                Key = e.Key,
                ValueString = e.ValueString,
                ValueLong = e.ValueLong,
                Timestamp = e.Timestamp
            };
        }
    }
}
