﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class JobOpeningCurrentStepReportParametersDto
    {
        public List<long> JobOpeningIds { get; set; }

        public List<long> HiringManagersIds { get; set; }

        public List<long> CityIds { get; set; }

        public List<long> JobProfileIds { get; set; }
    }
}
