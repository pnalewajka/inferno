﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecruitmentProcessStepInfoDto
    {
        public long Id { get; set; }

        public RecruitmentProcessStepType Type { get; set; }

        public RecruitmentProcessStepStatus Status { get; set; }

        public RecruitmentProcessStepDecision Decision { get; set; }

        public DateTime? PlannedOn { get; set; }

        public DateTime? DueOn { get; set; }

        public DateTime? DecidedOn { get; set; }

        public bool CanBeEdited { get; set; }

        public bool CanBeViewed { get; set; }

        public bool CanViewDoneStep { get; set; }

        public string Hint { get; set; }

        public string Comment { get; set; }

        public string AssigneeFullName { get; set; }
    }
}
