﻿namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecruitmentProcessStepPickerContext
    {
        public int RecruitmentProcessId { get; set; }
    }
}
