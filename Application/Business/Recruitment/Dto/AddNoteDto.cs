﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class AddNoteDto
    {
        public long ParentId { get; set; }

        public NoteVisibility NoteVisibility { get; set; }

        public string Content { get; set; }
    }
}
