﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class JobOpeningCloseDto
    {
        public long JobOpeningId { get; set; }

        public JobOpeningClosedReason ClosedReason { get; set; }

        public string ClosedComment { get; set; }
    }
}
