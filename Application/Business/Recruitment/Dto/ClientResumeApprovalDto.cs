﻿using System;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class ClientResumeApprovalDto
    {
        public DateTime? ResumeShownToClientOn { get; set; }
    }
}
