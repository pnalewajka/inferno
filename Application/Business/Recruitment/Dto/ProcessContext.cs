﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecruitmentProcessContext : ParentIdContext
    {
        public long? ProcessId { get; set; }

        public long? CandidateId { get; set; }

        public long? JobOpeningId { get; set; }

        public Dictionary<string, object> AsDictionary()
        {
            var dictionary = new Dictionary<string, object>();

            foreach (var property in GetType().GetProperties())
            {
                dictionary.Add(NamingConventionHelper.ConvertPascalCaseToHyphenated(property.Name), property.GetValue(this));
            }

            return dictionary;
        }
    }
}
