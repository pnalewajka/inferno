﻿using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecruitmentProcessStepToRecruitmentProcessStepTimelineDtoMapping
        : ClassMapping<RecruitmentProcessStep, RecruitmentProcessStepTimelineDto>
    {
        public RecruitmentProcessStepToRecruitmentProcessStepTimelineDtoMapping()
        {
            Mapping = s => new RecruitmentProcessStepTimelineDto
            {
                Id = s.Id,
                Decision = s.Decision,
                AssignedEmployeeName = s.AssignedEmployee == null ? string.Empty : EmployeeBusinessLogic.FullName.Call(s.AssignedEmployee),
                PlannedOn = s.PlannedOn,
                Status = s.Status,
                Type = s.Type
            };
        }
    }
}
