﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class InboundEmailDocumentToInboundEmailDocumentDtoMapping : ClassMapping<InboundEmailDocument, InboundEmailDocumentDto>
    {
        public InboundEmailDocumentToInboundEmailDocumentDtoMapping()
        {
            Mapping = d => new InboundEmailDocumentDto
            {
                ContentType = d.ContentType,
                DocumentName = d.Name,
                DocumentId = d.Id,
                To = d.InboundEmail.To,
                FromEmailAddress = d.InboundEmail.FromEmailAddress,
                FromFullName = d.InboundEmail.FromFullName,
                Subject = d.InboundEmail.Subject
            };
        }
    }
}
