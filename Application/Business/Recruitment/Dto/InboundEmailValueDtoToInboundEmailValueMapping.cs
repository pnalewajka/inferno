﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class InboundEmailValueDtoToInboundEmailValueMapping : ClassMapping<InboundEmailValueDto, InboundEmailValue>
    {
        public InboundEmailValueDtoToInboundEmailValueMapping()
        {
            Mapping = d => new InboundEmailValue
            {
                Id = d.Id,
                Key = d.Key,
                ValueString = d.ValueString,
                ValueLong = d.ValueLong,
                Timestamp = d.Timestamp
            };
        }
    }
}
