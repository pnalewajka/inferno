﻿namespace Smt.Atomic.Business.Recruitment.Dto
{
    public partial class RecruitmentProcessDto
    {
        public string ScreeningGeneralOpinion { get; set; }

        public string ScreeningMotivation { get; set; }

        public string ScreeningWorkplaceExpectations { get; set; }

        public string ScreeningSkills { get; set; }

        public string ScreeningLanguageEnglish { get; set; }

        public string ScreeningLanguageGerman { get; set; }

        public string ScreeningLanguageOther { get; set; }

        public string ScreeningContractExpectations { get; set; }

        public string ScreeningAvailability { get; set; }

        public string ScreeningOtherProcesses { get; set; }

        public string ScreeningCounterOfferCriteria { get; set; }

        public bool? ScreeningRelocationCanDo { get; set; }

        public string ScreeningRelocationComment { get; set; }

        public bool? ScreeningBusinessTripsCanDo { get; set; }

        public string ScreeningBusinessTripsComment { get; set; }

        public bool? ScreeningWorkPermitNeeded { get; set; }

        public string ScreeningWorkPermitComment { get; set; }

        public bool? ScreeningEveningWorkCanDo { get; set; }

        public string ScreeningEveningWorkComment { get; set; }
    }
}
