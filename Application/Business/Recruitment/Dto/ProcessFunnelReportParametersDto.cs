﻿using System;
using System.Collections.Generic;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class ProcessFunnelReportParametersDto
    {
        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }

        public List<long> RecruiterIds { get; set; }
    }
}
