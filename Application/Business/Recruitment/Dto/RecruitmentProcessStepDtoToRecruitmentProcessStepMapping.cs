﻿using System.Collections.ObjectModel;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecruitmentProcessStepDtoToRecruitmentProcessStepMapping : ClassMapping<RecruitmentProcessStepDto, RecruitmentProcessStep>
    {
        public RecruitmentProcessStepDtoToRecruitmentProcessStepMapping()
        {
            Mapping = d => new RecruitmentProcessStep
            {
                Id = d.Id,
                RecruitmentProcessId = d.RecruitmentProcessId,
                CreatedById = d.CreatedById,
                Type = d.Type,
                PlannedOn = d.PlannedOn,
                DueOn = d.DueOn,
                AssignedEmployeeId = d.AssignedEmployeeId,
                OtherAttendingEmployees = d.OtherAssignedEmployeeIds == null ? new Collection<Employee>() : new Collection<Employee>(d.OtherAssignedEmployeeIds.Select(v => new Employee { Id = v }).ToList()),
                Decision = d.Decision,
                DecisionComment = d.DecisionComment,
                RecruitmentHint = d.RecruitmentHint,
                Status = d.Status,
                Timestamp = d.Timestamp
            };
        }
    }
}
