﻿using System.Collections.ObjectModel;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecruitmentProcessDtoToRecruitmentProcessMapping : ClassMapping<RecruitmentProcessDto, RecruitmentProcess>
    {
        public RecruitmentProcessDtoToRecruitmentProcessMapping()
        {
            Mapping = d => new RecruitmentProcess
            {
                Id = d.Id,
                CandidateId = d.CandidateId,
                JobOpeningId = d.JobOpeningId,
                RecruiterId = d.RecruiterId,
                CityId = d.CityId,
                JobApplicationId = d.JobApplicationId,
                Status = d.Status,
                ResumeShownToClientOn = d.ResumeShownToClientOn,
                ClosedOn = d.ClosedOn,
                ClosedReason = d.ClosedReason,
                ClosedComment = d.ClosedComment,
                Watchers = d.WatcherIds == null ? null : new Collection<Employee>(d.WatcherIds.Select(v => new Employee { Id = v }).ToList()),              
                CrmId = d.CrmId,
                IsMatureEnoughForHiringManager = d.IsMatureEnoughForHiringManager,
                LastActivityOn = d.LastActivityOn,
                Timestamp = d.Timestamp
            };
        }
    }
}
