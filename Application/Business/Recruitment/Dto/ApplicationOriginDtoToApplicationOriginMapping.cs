﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class ApplicationOriginDtoToApplicationOriginMapping : ClassMapping<ApplicationOriginDto, ApplicationOrigin>
    {
        public ApplicationOriginDtoToApplicationOriginMapping()
        {
            Mapping = d => new ApplicationOrigin
            {
                Id = d.Id,
                Name = d.Name,
                OriginType = d.OriginType,
                ParserIdentifier = d.ParserIdentifier,
                EmailAddress = d.EmailAddress,
                Timestamp = d.Timestamp
            };
        }
    }
}
