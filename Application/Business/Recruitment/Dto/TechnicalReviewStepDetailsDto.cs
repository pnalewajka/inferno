﻿using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class TechnicalReviewStepDetailsDto
    {
        public List<long> JobProfileIds { get; set; }

        public VacanciesDto[] Vacancies { get; set; }

        public SeniorityLevelEnum SeniorityLevelAssessment { get; set; }

        public string OtherRemarks { get; set; }

        public string TechnicalAssignmentAssessment { get; set; }

        public string TechnicalKnowledgeAssessment { get; set; }

        public string TeamProjectSuitabilityAssessment { get; set; }

        public string EnglishUsageAssessment { get; set; }

        public string RiskAssessment { get; set; }

        public string StrengthAssessment { get; set; }
    }
}
