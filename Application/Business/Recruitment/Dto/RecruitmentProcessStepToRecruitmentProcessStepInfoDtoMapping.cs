﻿using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecruitmentProcessStepToRecruitmentProcessStepInfoDtoMapping : ClassMapping<RecruitmentProcessStep, RecruitmentProcessStepInfoDto>
    {
        public RecruitmentProcessStepToRecruitmentProcessStepInfoDtoMapping(IPrincipalProvider principalProvider)
        {
            Mapping = e => new RecruitmentProcessStepInfoDto
            {
                Id = e.Id,
                PlannedOn = e.PlannedOn,
                DueOn = e.DueOn,
                DecidedOn = e.DecidedOn,
                Hint = e.RecruitmentHint,
                Comment = e.DecisionComment,
                Type = e.Type,
                Status = e.Status,
                Decision = e.Decision,
                AssigneeFullName = e.AssignedEmployee == null ? null : EmployeeBusinessLogic.FullName.Call(e.AssignedEmployee),
                CanBeEdited = RecruitmentProcessStepBusinessLogic.CanEditStep.Call(e, principalProvider.Current.EmployeeId),
                CanBeViewed = CanViewStep(e, principalProvider.Current),
                CanViewDoneStep = principalProvider.Current.IsInRole(SecurityRoleType.CanViewStepDetailsWhenDone)
            };
        }

        private bool CanViewStep(RecruitmentProcessStep step, IAtomicPrincipal principal)
        {
            var ownProcess = RecruitmentProcessStepBusinessLogic.IsOwnRecruitmentProcess.Call(step, principal.EmployeeId, principal.Id.Value);
            var canViewAllSteps = principal.IsInRole(SecurityRoleType.CanViewAllRecruitmentProcessSteps);

            return ownProcess || canViewAllSteps;
        }
    }
}
