﻿namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class JobOpeningPickerContext
    {
        public bool OnlyActive { get; set; }

        public enum PickerMode
        {
            NoFilter = 0,

            ReportingHiringManagers = 1,
        }

        public PickerMode Mode { get; set; }
    }
}
