﻿using System.Collections.Generic;
using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Helpers;
using Smt.Atomic.Business.Recruitment.SnapshotComparer;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class JobOpeningHistoryEntryToHistoryEntryDtoMapping : ClassMapping<JobOpeningHistoryEntry, HistoryEntryDto>
    {
        public JobOpeningHistoryEntryToHistoryEntryDtoMapping()
        {
            Mapping = d => new HistoryEntryDto
            {
                PerformedByUserFullName = UserBusinessLogic.FullName.Call(d.PerformedByUser),
                ActivityDescription = d.ActivityType.GetDescription(null),
                PerformedOn = d.PerformedOn,
                SnapshotChanges = GetSnapshotChanges(d)
            };            
        }

        private Dictionary<string, ValueChange> GetSnapshotChanges(JobOpeningHistoryEntry jobOpeningHistory)
        {
            if (jobOpeningHistory.ActivityType == JobOpeningHistoryEntryType.Updated)
            {
                return SnapshotComaparerHelper.DeserializeSnapshotChanges(jobOpeningHistory.ChangesDescription);
            }

            return null;
        }
    }
}
