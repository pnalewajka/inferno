﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecruitmentProcessNoteDtoToRecruitmentProcessNoteMapping : ClassMapping<RecruitmentProcessNoteDto, RecruitmentProcessNote>
    {
        public RecruitmentProcessNoteDtoToRecruitmentProcessNoteMapping()
        {
            Mapping = d => new RecruitmentProcessNote
            {
                Id = d.Id,
                RecruitmentProcessId = d.RecruitmentProcessId,
                NoteType = d.NoteType,
                NoteVisibility = d.NoteVisibility,
                Content = d.Content,
                CreatedById = d.CreatedById,
                ImpersonatedCreatedById = d.ImpersonatedCreatedById,
                CreatedOn = d.CreatedOn,
                IsDeleted = d.IsDeleted,
                RecruitmentProcessStepId = d.RecruitmentProcessStepId,
                Timestamp = d.Timestamp
            };
        }
    }
}
