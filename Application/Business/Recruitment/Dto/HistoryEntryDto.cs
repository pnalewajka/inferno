﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Recruitment.SnapshotComparer;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class HistoryEntryDto
    {
        public DateTime PerformedOn { get; set; }

        public string PerformedByUserFullName { get; set; }

        public string ActivityDescription { get; set; }

        public Dictionary<string, ValueChange> SnapshotChanges { get; set; }
    }
}
