﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecruitmentProcessToContractNegotiationsDetailsDtoMapping : ClassMapping<RecruitmentProcess, ContractNegotiationsDetailsDto>
    {
        public RecruitmentProcessToContractNegotiationsDetailsDtoMapping()
        {
            Mapping = r => new ContractNegotiationsDetailsDto
            {
                FinalComment = r.Final == null ? null : r.Final.Comment,
                FinalLocationId = r.Final == null ? null : r.Final.LocationId,
                FinalLongTermContractTypeId = r.Final == null ? null : r.Final.LongTermContractTypeId,
                FinalLongTermSalary = r.Final == null ? null : r.Final.LongTermSalary,
                FinalPositionId = r.Final == null ? null : r.Final.PositionId,
                FinalStartDate = r.Final == null ? null : r.Final.StartDate,
                FinalSignedDate = r.Final == null ? null : r.Final.SignedDate,
                FinalTrialContractTypeId = r.Final == null ? null : r.Final.TrialContractTypeId,
                FinalTrialSalary = r.Final == null ? null : r.Final.TrialSalary,
                FinalEmploymentSourceId = r.Final == null ? null : r.Final.EmploymentSourceId,
                OfferComment = r.Offer == null ? null : r.Offer.Comment,
                OfferContractType = r.Offer == null ? null : r.Offer.ContractType,
                OfferLocationId = r.Offer == null ? null : r.Offer.LocationId,
                OfferPositionId = r.Offer == null ? null : r.Offer.PositionId,
                OfferSalary = r.Offer == null ? null : r.Offer.Salary,
                OfferStartDate = r.Offer == null ? null : r.Offer.StartDate
            };
        }
    }
}
