﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class CandidateAdvancedFiltersDto
    {
        public long[] JobProfileIds { get; set; }

        public long[] CityIds { get; set; }

        public long[] EmployeeStatuses { get; set; }

        public long? LanguageId { get; set; }

        public LanguageReferenceLevel? LanguageReferenceLevelId { get; set; }

        public DateTime? LastContactFrom { get; set; }

        public DateTime? LastContactTo { get; set; }

        public long[] CoordinatorIds { get; set; }
    }
}
