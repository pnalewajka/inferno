﻿namespace Smt.Atomic.Business.Recruitment.Dto
{
    public partial class RecruitmentProcessDto
    {
        public long? OfferPositionId { get; set; }

        public string OfferContractType { get; set; }

        public string OfferSalary { get; set; }

        public string OfferStartDate { get; set; }

        public long? OfferLocationId { get; set; }

        public string OfferComment { get; set; }
    }
}
