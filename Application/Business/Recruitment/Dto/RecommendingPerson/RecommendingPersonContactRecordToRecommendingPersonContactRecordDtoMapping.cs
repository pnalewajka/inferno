﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecommendingPersonContactRecordToRecommendingPersonContactRecordDtoMapping : ClassMapping<RecommendingPersonContactRecord, RecommendingPersonContactRecordDto>
    {
        public RecommendingPersonContactRecordToRecommendingPersonContactRecordDtoMapping()
        {
            Mapping = e => new RecommendingPersonContactRecordDto
            {
                Id = e.Id,
                RecommendingPersonId = e.RecommendingPersonId,
                ContactType = e.ContactType,
                ContactedOn = e.ContactedOn,
                ContactedByEmployeeId = e.ContactedById,
                Comment = e.Comment,
                Timestamp = e.Timestamp
            };
        }
    }
}
