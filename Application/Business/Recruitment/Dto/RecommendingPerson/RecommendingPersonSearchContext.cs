﻿namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecommendingPersonSearchContext
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string EmailAddress { get; set; }

        public string PhoneNumber { get; set; }
    }
}
