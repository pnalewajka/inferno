﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecommendingPersonContactRecordDtoToRecommendingPersonContactRecordMapping : ClassMapping<RecommendingPersonContactRecordDto, RecommendingPersonContactRecord>
    {
        public RecommendingPersonContactRecordDtoToRecommendingPersonContactRecordMapping()
        {
            Mapping = d => new RecommendingPersonContactRecord
            {
                Id = d.Id,
                RecommendingPersonId = d.RecommendingPersonId,
                ContactType = d.ContactType,
                ContactedOn = d.ContactedOn,
                ContactedById = d.ContactedByEmployeeId,
                Comment = d.Comment,
            };
        }
    }
}
