﻿using Smt.Atomic.CrossCutting.Business.Enums;
using System;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecommendingPersonContactRecordDto
    {
        public long Id { get; set; }

        public long RecommendingPersonId { get; set; }

        public ContactType ContactType { get; set; }

        public DateTime ContactedOn { get; set; }

        public long ContactedByEmployeeId { get; set; }

        public string Comment { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
