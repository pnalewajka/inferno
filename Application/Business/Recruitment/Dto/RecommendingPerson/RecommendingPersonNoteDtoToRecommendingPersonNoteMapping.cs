﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecommendingPersonNoteDtoToRecommendingPersonNoteMapping : ClassMapping<RecommendingPersonNoteDto, RecommendingPersonNote>
    {
        public RecommendingPersonNoteDtoToRecommendingPersonNoteMapping()
        {
            Mapping = d => new RecommendingPersonNote
            {
                Id = d.Id,
                RecommendingPersonId = d.RecommendingPersonId,
                NoteType = d.NoteType,
                NoteVisibility = d.NoteVisibility,
                Content = d.Content,
                CreatedById = d.CreatedById,
                ImpersonatedCreatedById = d.ImpersonatedCreatedById,
                CreatedOn = d.CreatedOn,
                IsDeleted = d.IsDeleted,
                Timestamp = d.Timestamp
            };
        }
    }
}
