﻿using System;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecommendingPersonDto
    {
        public long Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string EmailAddress { get; set; }

        public string PhoneNumber { get; set; }

        public bool ShouldAddRelatedDataConsent { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public bool HasValidConsent { get; set; }

        public bool IsAnonymized { get; set; }

        public long CoordinatorId { get; set; }

        public RecommendingPersonNoteDto[] Notes { get; set; }

        public string CreatedByFullName { get; set; }

        public DateTime CreatedOn { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
