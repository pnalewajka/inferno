﻿namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecommendingPersonPickerContext
    {
        public bool OnlyValidConsent { get; set; }

        public bool AllowAnonymous { get; set; }
    }
}
