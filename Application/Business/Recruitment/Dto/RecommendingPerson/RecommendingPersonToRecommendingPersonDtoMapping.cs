﻿using System.Linq;
using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Helpers;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecommendingPersonToRecommendingPersonDtoMapping : ClassMapping<RecommendingPerson, RecommendingPersonDto>
    {
        public RecommendingPersonToRecommendingPersonDtoMapping(ITimeService timeService,
            IClassMappingFactory mappingFactory)
        {
            Mapping = e => new RecommendingPersonDto
            {
                Id = e.Id,
                FirstName = e.FirstName,
                LastName = e.LastName,
                EmailAddress = e.EmailAddress,
                PhoneNumber = e.PhoneNumber,
                IsAnonymized = e.IsAnonymized,
                CoordinatorId = e.CoordinatorId,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                HasValidConsent = RecommendingPersonBusinessLogic.HasValidConsent.Call(e, timeService.GetCurrentDate()),
                Notes = e.RecommendingPersonNotes != null ? e.RecommendingPersonNotes.Filtered().Select(mappingFactory.CreateMapping<RecommendingPersonNote, RecommendingPersonNoteDto>().CreateFromSource).ToArray() : null,
                CreatedByFullName = e.CreatedBy == null ? null : UserBusinessLogic.FullName.Call(e.CreatedBy),
                CreatedOn = e.CreatedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
