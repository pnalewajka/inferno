﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecommendingPersonDtoToRecommendingPersonMapping : ClassMapping<RecommendingPersonDto, RecommendingPerson>
    {
        public RecommendingPersonDtoToRecommendingPersonMapping()
        {
            Mapping = d => new RecommendingPerson
            {
                Id = d.Id,
                FirstName = d.FirstName,
                LastName = d.LastName,
                EmailAddress = d.EmailAddress,
                PhoneNumber = d.PhoneNumber,
                IsAnonymized = d.IsAnonymized,
                CoordinatorId = d.CoordinatorId,
                Timestamp = d.Timestamp
            };
        }
    }
}
