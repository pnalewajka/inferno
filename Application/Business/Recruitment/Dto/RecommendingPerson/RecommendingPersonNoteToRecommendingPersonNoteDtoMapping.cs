﻿using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class RecommendingPersonNoteToRecommendingPersonNoteDtoMapping : ClassMapping<RecommendingPersonNote, RecommendingPersonNoteDto>
    {
        public RecommendingPersonNoteToRecommendingPersonNoteDtoMapping()
        {
            Mapping = e => new RecommendingPersonNoteDto
            {
                Id = e.Id,
                RecommendingPersonId = e.RecommendingPersonId,
                NoteType = e.NoteType,
                NoteVisibility = e.NoteVisibility,
                Content = e.Content,
                CreatedById = e.CreatedById,
                CreatedByFullName = e.CreatedBy != null ? UserBusinessLogic.FullName.Call(e.CreatedBy) : null,
                ImpersonatedCreatedById = e.ImpersonatedCreatedById,
                CreatedOn = e.CreatedOn,
                IsDeleted = e.IsDeleted,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
