﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto
{
    public class JobOpeningFiltersDto
    {
        public long[] JobProfileIds { get; set; }

        public long? OrgUnitId { get; set; }

        public long? DecisionMakerId { get; set; }

        public long[] CityIds { get; set; }

        public int? Priority { get; set; }

        public JobOpeningStatus? Status { get; set; }

        public int[] HiringMode { get; set; }

        public DateTime? OpenedOnFrom { get; set; }

        public DateTime? OpenedOnTo { get; set; }

        public AgencyEmploymentMode? AgencyEmploymentMode { get; set; }
    }
}
