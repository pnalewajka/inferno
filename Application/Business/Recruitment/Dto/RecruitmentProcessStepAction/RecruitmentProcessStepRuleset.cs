﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto.RecruitmentProcessStepAction
{
    public class RecruitmentProcessStepRuleset
    {
        public RecruitmentProcessStepType StepType { get; set; }

        public RecruitmentProcessStepRule[] AssigneeActions { get; set; }

        public RecruitmentProcessStepRule[] RecruiterActions { get; set; }
    }
}
