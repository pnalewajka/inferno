﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Dto.RecruitmentProcessStepAction
{
    public class RecruitmentProcessStepWorkflowAction
    {
        public RecruitmentProcessStepRule Rule { get; set; }

        public int SameStepCount { get; set; }

        public RecruitmentProcessStepType StepType { get; set; }
    }
}
