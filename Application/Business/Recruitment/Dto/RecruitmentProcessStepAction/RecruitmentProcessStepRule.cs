﻿using System;
using System.Linq;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Dto.RecruitmentProcessStepAction
{
    public class RecruitmentProcessStepRule
    {
        private RecruitmentProcessStepType[] MultipleSteps = new [] 
        {
            RecruitmentProcessStepType.TechnicalReview,
            RecruitmentProcessStepType.ClientInterview,
            RecruitmentProcessStepType.HiringManagerInterview
        };

        public Func<RecruitmentProcess, bool> TypicalConditions { get; set; }

        public RecruitmentProcessStepWorkflowActionType WorkflowAction { get; }

        public RecruitmentProcessStepType? AddedStepType { get; set; }

        public bool AllowCloseStepDecision { get; set; }

        public bool CanAddStepIfAlreadyExists => AddedStepType.HasValue && MultipleSteps.Contains(AddedStepType.Value);

        public bool AreConditionsTypical { get; private set; }

        public RecruitmentProcessStepRule(RecruitmentProcessStepWorkflowActionType workflowAction)
        {
            WorkflowAction = workflowAction;
            AreConditionsTypical = true;
        }

        public void EvaluateConditions(RecruitmentProcess recruitmentProcess)
        {
            AreConditionsTypical = TypicalConditions == null || TypicalConditions.Invoke(recruitmentProcess);
        }
    }
}
