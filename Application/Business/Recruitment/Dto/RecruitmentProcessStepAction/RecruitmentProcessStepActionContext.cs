﻿namespace Smt.Atomic.Business.Recruitment.Dto.RecruitmentProcessStepAction
{
    public class RecruitmentProcessStepActionContext
    {
        public long RecruitmentProcessStepId { get; set; }

        public long EmployeeId { get; set; }
    }
}
