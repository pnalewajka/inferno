﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Dto.RecruitmentProcessStepAction;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Interfaces
{
    public interface IRecruitmentProcessStepService
    {
        RecruitmentProcessStep CreateInitialStep(RecruitmentProcessStepType recruitmentProcessStepType, long recruiterId, long jobOpeningDecisionMakerId);

        RecruitmentProcessStepWorkflowAction[] GetWorkflowActions(RecruitmentProcessStepActionContext context);

        RecruitmentProcessStepDto GetDefaultNewRecord(RecruitmentProcessStepType type, long? recruitmentProcessId, RecruitmentProcessStepDto newRecord = null);

        void AddNoteToRecruitmentProcess(AddNoteDto note);

        long GetCandidateId(long recruitmentProcessStepId);

        void RefreshStepModification(long stepId);
    }
}
