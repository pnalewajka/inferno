﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Interfaces
{
    public interface IInboundEmailService
    {
        DateTime? GetLastReceivedOnOrDefault();

        void SetEmailColorCategory(long id, InboundEmailColorCategory? inboundEmailColorCategory);
    }
}
