﻿using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.Recruitment.Interfaces
{
    public interface ICandidateRecentActivityService
    {
        BoolResult RecordContact(long candidateId, long lastContactRecord);

        BoolResult RecordProcessStep(long candidateId, long stepId);
    }
}
