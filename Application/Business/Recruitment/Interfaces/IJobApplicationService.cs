﻿using Smt.Atomic.Business.Recruitment.Dto;

namespace Smt.Atomic.Business.Recruitment.Interfaces
{
    public interface IJobApplicationService
    {
        JobApplicationDto GetJobApplicationById(long jobApplicationId);
    }
}
