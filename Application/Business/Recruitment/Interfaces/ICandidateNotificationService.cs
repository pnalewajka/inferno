﻿using Smt.Atomic.Business.Recruitment.Dto.Candidates;

namespace Smt.Atomic.Business.Recruitment.Interfaces
{
    public interface ICandidateNotificationService
    {
        void NotifyAboutRecordChanges(long candidateId, CandidateDto modifiedCandidateDto);
    }
}