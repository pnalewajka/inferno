﻿namespace Smt.Atomic.Business.Recruitment.Interfaces
{
    public interface ICandidateFileService
    {
        string GetDocumentNameByDocumentId(long candidateDocumentId);

        long GetCandidateFileDocumentIdByFileId(long candidateFileId);

        long GetCandidateIdByDocumentId(long candidateDocumentId);
    }
}
