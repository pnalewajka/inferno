﻿namespace Smt.Atomic.Business.Recruitment.Services.Notification.RecruitmentProcessNoteServices
{
    public interface IRecruitmentProcessNoteNotificationService
    {
        void NotifyAboutAddedNoteInRecruitmentProcessNotes(long recruitmentProcessNoteId);

        void NotifyAboutEditedNoteInRecruitmentProcessNotes(long recruitmentProcessNoteId);
    }
}
