﻿using Smt.Atomic.Business.Recruitment.Dto.Reminder.JobOpeningReminder;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification
{
    public interface IJobOpeningNoteRecipientProvider : IRecipientProvider<JobOpeningNote, JobOpeningRecipientType>
    {
    }
}
