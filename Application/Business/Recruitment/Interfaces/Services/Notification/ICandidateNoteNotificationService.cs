﻿namespace Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification
{
    public interface ICandidateNoteNotificationService
    {
        void NotifyAboutAdded(long candidateNoteId);

        void NotifyAboutEdited(long candidateNoteId);
    }
}
