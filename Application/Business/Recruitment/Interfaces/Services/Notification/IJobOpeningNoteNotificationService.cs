﻿namespace Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification
{
    public interface IJobOpeningNoteNotificationService
    {
        void NotifyAboutNoteAdded(long noteId);

        void NotifyAboutNoteEdited(long noteId);
    }
}
