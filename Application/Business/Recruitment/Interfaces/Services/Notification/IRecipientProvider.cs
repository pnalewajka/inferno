﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Notifications.Dto;

namespace Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification
{
    public interface IRecipientProvider<TEntity, TRecipient>
    {
        IList<EmailRecipientDto> GetRecipients(TEntity entity, TRecipient[] recipients, RecipientType recipientType = RecipientType.To);

        IList<EmailRecipientDto> GetRecipients(TEntity entity, TRecipient recipients, RecipientType recipientType = RecipientType.To);
    }
}
