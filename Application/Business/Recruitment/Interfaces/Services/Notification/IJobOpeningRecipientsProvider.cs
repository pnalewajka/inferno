﻿using Smt.Atomic.Business.Recruitment.Dto.Reminder.JobOpeningReminder;
using Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Services.Notification
{
    public interface IJobOpeningRecipientsProvider : IRecipientProvider<JobOpening, JobOpeningRecipientType>
    {
    }
}