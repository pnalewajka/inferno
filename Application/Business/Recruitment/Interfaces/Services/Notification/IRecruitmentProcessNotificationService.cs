﻿namespace Smt.Atomic.Business.Recruitment.Services.Notification.RecruitmentProcessServices
{
    public interface IRecruitmentProcessNotificationService
    {
        void NotifyAboutProcessPutOnHold(long processId);
        void NotifyRecruiterAboutProcessPutOnHold(long processId);
        void NotifyOtherDecisionMakersAboutProcessPutOnHold(long processId);
        void NotifyWatcherAboutProcessPutOnHold(long processId);
        void NotifyRecruiterAboutProcessTakenOffHold(long processId);
        void NotifyOtherDecisionMakersAboutProcessTakenOffHold(long processId);
        void NotifyWatcherAboutProcessTakenOffHold(long processId);
    }
}
