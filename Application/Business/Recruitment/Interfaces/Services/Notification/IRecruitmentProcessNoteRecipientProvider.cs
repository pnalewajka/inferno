﻿using Smt.Atomic.Business.Recruitment.Dto.ReminderDto;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification
{
    public interface IRecruitmentProcessNoteRecipientProvider : IRecipientProvider<RecruitmentProcessNote, RecruitmentProcessRecipientType>
    {
    }
}
