﻿using Smt.Atomic.Business.Recruitment.Dto.ReminderDto;
using Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Services.Notification.RecruitmentProcessStepServices
{
    public interface IRecruitmentProcessStepRecipientsProvider : IRecipientProvider<RecruitmentProcessStep, RecruitmentProcessRecipientType>
    {
    }
}
