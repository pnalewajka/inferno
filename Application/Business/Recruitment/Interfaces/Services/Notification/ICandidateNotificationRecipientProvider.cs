﻿using Smt.Atomic.Business.Recruitment.Dto.Reminder;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification
{
    public interface ICandidateRecipientProvider : IRecipientProvider<Candidate, CandidateRecipientType>
    {
    }
}
