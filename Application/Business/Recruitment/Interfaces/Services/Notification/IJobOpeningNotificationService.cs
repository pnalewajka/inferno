﻿using System.Collections.Generic;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Recruitment.SnapshotComparer;

namespace Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification
{
    public interface IJobOpeningNotificationService
    {
        IList<EmailRecipientDto> SendApprovedReminderEmail(long jobOpeningId);

        IList<EmailRecipientDto> SendRejectedReminderEmail(long jobOpeningId);

        IList<EmailRecipientDto> SendReopenReminderEmail(long jobOpeningId);

        IList<EmailRecipientDto> SendClosedNotificationEmail(long jobOpeningId);

        IList<EmailRecipientDto> SendForApprovalRemainderEmail(long jobOpeningId);

        IList<EmailRecipientDto> SendEmailWithChanges(long jobOpeningId, IDictionary<string, ValueChange> changes);

        IList<EmailRecipientDto> SendPutOnHoldRemainderEmail(long jobOpeningId, bool hasHighlyConfidentialProcesses);

        IList<EmailRecipientDto> SendTakeOffHoldRemainderEmail(long jobOpeningId, bool hasHighlyConfidentialProcesses);

        void SendAssignedRemainderEmail(long jobOpeningId, long[] assignedRecruiters);

        void SendUnassignedRemainderEmail(long jobOpeningId, long[] unassignedRecruiters);
    }
}