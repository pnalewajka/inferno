﻿namespace Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification
{
    public interface IInboundEmailNotificationService
    {
        void NotifyAboutAssignment(long inboundEmailId);

        void NotifyAboutUnassignment(long inboundEmailId);
    }
}