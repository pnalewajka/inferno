﻿using Smt.Atomic.Business.Recruitment.Dto.ReminderDto;
using Smt.Atomic.Business.Recruitment.Interfaces.Services.Notification;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Services.Notification.RecruitmentProcessServices
{
    public interface IRecruitmentProcessRecipientsProvider : IRecipientProvider<RecruitmentProcess, RecruitmentProcessRecipientType>
    {
    }
}
