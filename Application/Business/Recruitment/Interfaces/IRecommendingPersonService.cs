﻿using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Interfaces
{
    public interface IRecommendingPersonService
    {
        void AnonymizeRecommendingPersonById(long id, bool isAnonymizedPermanently = false);

        bool AddNote(AddNoteDto note);

        bool AddNote(RecommendingPersonNote note);
    }
}
