﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Interfaces
{
    public interface IRecruitmentProcessStepCardIndexDataService : ICardIndexDataService<RecruitmentProcessStepDto>
    {
        RecruitmentProcessStepDto GetDefaultNewRecord(RecruitmentProcessStepType processStepType, long? parentProcessId = null);

        void ReOpenProcessStep(long processStepId);

        RecruitmentProcessStepStatus GetStatus(long id);
    }
}