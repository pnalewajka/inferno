﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Enums;
using Smt.Atomic.Business.Recruitment.SnapshotComparer;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Interfaces
{
    public interface IJobOpeningService
    {
        void ApproveJobOpenings(ICollection<long> jobOpeningIds);

        void RejectJobOpenings(ICollection<long> jobOpeningIds, string reason);

        void SendForApprovalJobOpening(long jobOpeningId);

        void CloseJobOpening(JobOpeningCloseDto jobOpeningClose);

        BoolResult PutJobOpeningOnHold(long jobOpeningId);

        BoolResult TakeJobOpeningOffHold(long jobOpeningId);

        void ToggleWatching(ICollection<long> jobOpeningIds, long employeeId, WatchingStatus jobOpeningWatching);

        void NotifyAboutRecordChanges(JobOpeningDto before, JobOpeningDto after);

        JobOpeningDto GetJobOpeningOrDefaultById(long jobOpeningId);

        bool AddNote(AddNoteDto note);

        long CloneJobOpening(long jobOpeningId);

        void ReopenJobOpening(long jobOpeningId);

        Dictionary<string, ValueChange> GetSnapshotChanges(JobOpeningDto before, JobOpeningDto after);
    }
}
