﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Recruitment.Dto;

namespace Smt.Atomic.Business.Recruitment.Interfaces
{
    public interface IRecommendingPersonPickerCardIndexDataService : ICardIndexDataService<RecommendingPersonDto>
    {
    }
}