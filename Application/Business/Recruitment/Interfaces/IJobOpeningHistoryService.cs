﻿using System.Collections.Generic;
using Smt.Atomic.Business.Recruitment.SnapshotComparer;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Recruitment.Interfaces
{
    public interface IJobOpeningHistoryService
    {
        void LogActivity(long jobOpeningId, JobOpeningHistoryEntryType activityType, Dictionary<string, ValueChange> snapshotChanges = null);
    }
}
