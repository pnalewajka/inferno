﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Interfaces
{
    public interface IJobOpeningCardIndexDataService : ICardIndexDataService<JobOpeningDto>
    {
        JobOpeningDto GetDefaultNewRecord(HiringMode hiringMode);
    }
}