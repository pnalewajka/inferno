﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.Business.Recruitment.Enums;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Recruitment.Interfaces
{
    public interface ICandidateService
    {
        void AnonymizeCandidateById(long id, bool isAnonymizedPermanently = false);

        void ToggleWatching(ICollection<long> candidateIds, long employeeId, WatchingStatus candidateWatching);

        void AddDataConsent(long candidateId, DataConsentType consentType, string consentDetails, DocumentDto[] documents);

        CandidateDto GetCandidateOrDefaultById(long candidateId);

        BoolResult AddNote(AddNoteDto note);

        void UpdateLastActivity(long candidateId, ReferenceType referenceType, long lastReferenceId);

        BoolResult ToogleFavorite(ICollection<long> candidateId, FavoriteStatus candidateFavorite);

        bool IsWatchedByCurrentUser(long candidateId);
    }
}
