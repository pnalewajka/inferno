﻿using Smt.Atomic.Business.Recruitment.Dto;

namespace Smt.Atomic.Business.Recruitment.Interfaces
{
    public interface IJobOpeningNoteService
    {
        void AddNote(JobOpeningNoteDto jobOpeningNoteDto);
    }
}
