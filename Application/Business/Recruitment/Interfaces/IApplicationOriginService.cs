﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Interfaces
{
    public interface IApplicationOriginService
    {
        long GetApplicationOriginId(ApplicationOriginType applicationOriginType);
    }
}
