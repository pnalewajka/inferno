﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Recruitment.Interfaces
{
    public interface IInboundEmailCardIndexDataService : ICardIndexDataService<InboundEmailDto>
    {
        BoolResult AssignMessage(long inboundEmailId, long employeeId);

        BoolResult UnassignMessage(long inboundEmailId);

        BoolResult SendForResearch(long inboundEmailId);

        string GetDocumentNameById(long inboundEmailFileId);

        void LogDataActivity(long inboundEmailId, ActivityType activityType, params string[] details);

        long GetInboundEmailIdByDocumentId(long id);
    }
}

