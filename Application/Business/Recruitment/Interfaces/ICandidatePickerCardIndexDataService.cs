﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;

namespace Smt.Atomic.Business.Recruitment.Interfaces
{
    public interface ICandidatePickerCardIndexDataService : ICardIndexDataService<CandidateDto>
    {
    }
}