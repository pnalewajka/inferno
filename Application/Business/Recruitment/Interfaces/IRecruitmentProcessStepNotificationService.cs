﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Interfaces
{
    public interface IRecruitmentProcessStepNotificationService
    {
        void NotifyAboutApproval(RecruitmentProcessStepType recruitmentProcessStepType, long recruitmentProcessStepId);

        void NotifyAboutRejection(RecruitmentProcessStepType recruitmentProcessStepType, long recruitmentProcessStepId);

        void NotifyAllPartiesAboutStepCancelled(long recruitmentProcessStepId);

        void NotifyAboutPlannedStep(RecruitmentProcessStepType type, long id, long? previousStepId);

        void NotifyAboutProcessPutOnHold(long processStepId);

        void NotifyAboutStepUpdate(long processStepId);
    }
}
