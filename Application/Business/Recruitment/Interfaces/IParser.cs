﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Recruitment.Interfaces
{
    public interface IParser<TSource, TDestination>
    {
        IEnumerable<TDestination> GetParserItems(TSource source);

        bool CanParse(TSource source);
    }
}
