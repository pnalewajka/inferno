﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Recruitment.Interfaces
{
    public interface IJobApplicationCardIndexDataService : ICardIndexDataService<JobApplicationDto>
    {
        JobApplicationDto GetDefaultNewRecord(ApplicationOriginType applicationOriginType);
        ApplicationOriginType GetOriginType(long applicationOriginId);
    }
}