﻿using Smt.Atomic.Business.Recruitment.Dto.Candidates;

namespace Smt.Atomic.Business.Recruitment.Interfaces
{
    public interface ICandidateNoteService
    {
        void AddNote(CandidateNoteDto candidateNoteDto);
    }
}
