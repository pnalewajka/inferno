﻿using Smt.Atomic.Business.Recruitment.Dto;

namespace Smt.Atomic.Business.Recruitment.Interfaces
{
    public interface IRecommendingPersonNoteService
    {
        void AddNote(RecommendingPersonNoteDto recommendingPersonNoteDto);
    }
}
