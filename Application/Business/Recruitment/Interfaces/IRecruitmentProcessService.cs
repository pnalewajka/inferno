﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.Enums;
using Smt.Atomic.Business.Recruitment.SnapshotComparer;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.Interfaces
{
    public interface IRecruitmentProcessService
    {
        void ToggleWatching(long[] recruitmentProcessIds, long employeeId, WatchingStatus recruitmentProcessStatus);

        void CloseRecruitmentProcess(RecruitmentProcessCloseDto dto);

        BoolResult PutRecruitmentProcessOnHold(long id, long? processStepId);

        void TakeRecruitmentProcessOffHold(long id);

        AddRecordResult AddOnboardingRequest(long recruitmentProcessStepId);

        void NotifyWatchersAboutRecordChanges(RecruitmentProcess sourceEntity, RecruitmentProcess imputEntity);

        long? GetOnboardingRequestId(long recruitmentProcessId);

        void AddNoteToRecruitmentProcess(AddNoteDto note);

        long GetCandidateId(long recruitmentProcessId);

        RecruitmentProcessStatus GetStatus(long recruitmentProcessId);

        BoolResult CloneRecruitmentProcess(long recruitmentProcessId, long jobOpeningId);

        BoolResult ReopenProcess(RecruitmentProcessReopenDto dto);

        bool CanProcessBeReopened(long recruitmentProcessId);

        Dictionary<string, ValueChange> GetShanpshotChanges(RecruitmentProcessDto sourceDto, RecruitmentProcessDto inputDto);
    }
}
