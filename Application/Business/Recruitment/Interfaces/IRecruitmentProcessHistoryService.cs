﻿using System.Collections.Generic;
using Smt.Atomic.Business.Recruitment.SnapshotComparer;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Recruitment.Interfaces
{
    public interface IRecruitmentProcessHistoryService
    {
        void LogActivity(long recruitmentProcessId, RecruitmentProcessHistoryEntryType activityType, long? recruitmentProcessStepId = null, Dictionary<string, ValueChange> snapshotChanges = null);
    }
}
