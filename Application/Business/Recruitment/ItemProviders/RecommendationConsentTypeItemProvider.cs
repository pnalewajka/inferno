﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.Business.Recruitment.Services
{
    public class RecommendationConsentTypeItemProvider : EnumBasedListItemProvider<DataConsentType>
    {
        private List<DataConsentType> acceptedDataConsentTypes = new List<DataConsentType>
        {
            DataConsentType.RecommendingPersonConsentRecommendation
        };

        public RecommendationConsentTypeItemProvider(IPrincipalProvider principalProvider)
            : base(principalProvider)
        {
        }

        public override IEnumerable<ListItem> GetItems()
        {
            return base.GetItems()
                .Where(i => acceptedDataConsentTypes.Contains((DataConsentType)i.Id.Value));
        }
    }
}
