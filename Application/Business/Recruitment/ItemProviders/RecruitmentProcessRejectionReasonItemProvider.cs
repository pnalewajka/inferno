﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.Business.Recruitment.ItemProviders
{
    public class RecruitmentProcessRejectionReasonItemProvider : EnumBasedListItemProvider<RecruitmentProcessClosedReason>
    {
        private List<RecruitmentProcessClosedReason> manualClosedReasons = new List<RecruitmentProcessClosedReason>
        {
            RecruitmentProcessClosedReason.RejectedByRecruiter,
            RecruitmentProcessClosedReason.FailedTechnicalReview,
            RecruitmentProcessClosedReason.RejectedByCandidate,
            RecruitmentProcessClosedReason.RejectedByClient,
            RecruitmentProcessClosedReason.RejectedByHiringManager,
            RecruitmentProcessClosedReason.CandidateResigned,
            RecruitmentProcessClosedReason.ClientResigned,
        };

        public RecruitmentProcessRejectionReasonItemProvider(IPrincipalProvider principalProvider)
            : base(principalProvider)
        {
        }

        public override IEnumerable<ListItem> GetItems()
        {
            return base.GetItems()
                .Where(i => manualClosedReasons.Contains((RecruitmentProcessClosedReason)i.Id.Value));
        }
    }
}
