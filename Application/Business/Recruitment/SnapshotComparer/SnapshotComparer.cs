﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Business.Recruitment.SnapshotComparer
{
    public class SnapshotComparer<TDto>
        where TDto : class
    {
        private IDictionary<string, Func<object, string>> _displayNameConverters { get; } = new Dictionary<string, Func<object, string>>();

        private IDictionary<string, SecurityRoleType> _securedFields { get; } = new Dictionary<string, SecurityRoleType>();

        private HashSet<string> _comparableFields { get; } = new HashSet<string>();

        private Dictionary<string, ValueChange> _changedValues { get; } = new Dictionary<string, ValueChange>();

        public void Compare(TDto sourceDto, TDto inputDto)
        {
            if (sourceDto == null)
            {
                throw new ArgumentNullException(nameof(sourceDto));
            }

            if (inputDto == null)
            {
                throw new ArgumentNullException(nameof(inputDto));
            }

            foreach (var propertyInfo in TypeHelper.GetPublicInstanceProperties(sourceDto.GetType()))
            {
                if (_comparableFields.Contains(propertyInfo.Name) && propertyInfo.CanWrite && propertyInfo.CanRead)
                {
                    var beforeValue = propertyInfo.GetValue(sourceDto);
                    var afterValue = propertyInfo.GetValue(inputDto);

                    if (IsGenericCollection(propertyInfo.PropertyType))
                    {
                        continue;
                    }

                    if (IsValueChanged(beforeValue, afterValue))
                    {
                        if (_displayNameConverters.ContainsKey(propertyInfo.Name))
                        {
                            _changedValues.Add(propertyInfo.Name, new ValueChange
                            {
                                Before = _displayNameConverters[propertyInfo.Name].Invoke(beforeValue),
                                After = _displayNameConverters[propertyInfo.Name].Invoke(afterValue),
                                SecurityRole = GetRequiredRole(propertyInfo.Name)
                            });
                        }
                        else
                        {
                            _changedValues.Add(propertyInfo.Name, new ValueChange
                            {
                                Before = GetDefaultDisplayName(beforeValue),
                                After = GetDefaultDisplayName(afterValue),
                                SecurityRole = GetRequiredRole(propertyInfo.Name)
                            });
                        }
                    }
                }
            }
        }

        public void AddDisplayNameConverter<TValue>(Expression<Func<TDto, object>> expression, Func<TValue, string> converterFunc)
        {
            var memberName = GetPropertyName(expression);
            _displayNameConverters.Add(memberName, (o) =>
            {
                if (o == null)
                {
                    return String.Empty;
                }

                return converterFunc((TValue)o);
            });
        }

        public void AddCustomObjectConverters<TValue>(IEnumerable<Expression<Func<TDto, object>>> expressions, Func<TValue, string> converterFunc)
        {
            foreach (var expression in expressions)
            {
                AddDisplayNameConverter(expression, converterFunc);
            }
        }

        public void AddComparedField(Expression<Func<TDto, object>> expression)
        {
            var memberName = GetPropertyName(expression);

            if (!_comparableFields.Contains(memberName))
            {
                _comparableFields.Add(memberName);
            }
        }

        public void AddComparedFields(IEnumerable<Expression<Func<TDto, object>>> expressions)
        {
            foreach (var expression in expressions)
            {
                AddComparedField(expression);
            }
        }

        public void AddSecuredField(Expression<Func<TDto, object>> expression, SecurityRoleType securityRoleType)
        {
            var memberName = GetPropertyName(expression);

            if (!_securedFields.ContainsKey(memberName))
            {
                _securedFields.Add(memberName, securityRoleType);
            }
        }

        public Dictionary<string, ValueChange> GetChangedValues(long? userId, IUserService userService)
        {
            if (!userId.HasValue)
            {
                return GetChangedValues().Where(v => !v.Value.SecurityRole.HasValue)
                    .ToDictionary(v => v.Key, v => v.Value);
            }

            if (userService == null)
            {
                throw new ArgumentNullException(nameof(userService));
            }

            var userRoles = userService.GetUserRolesById(userId.Value);

            return GetChangedValues().Where(v => !v.Value.SecurityRole.HasValue || userRoles.Contains(v.Value.SecurityRole.Value))
                .ToDictionary(v => v.Key, v => v.Value);
        }

        public Dictionary<string, ValueChange> GetChangedValues()
        {
            return _changedValues;
        }

        public virtual string GetDefaultDisplayName(object value)
        {
            switch (value)
            {
                case Enum enumValue:
                    return enumValue.GetDescriptionOrValue();

                case IEnumerable<Int64> arrayLongValue:
                    return string.Join(", ", arrayLongValue.Select(e => e.ToString()));

                default:
                    return Convert.ToString(value);
            }
        }

        private bool IsValueChanged(object sourceValue, object inputValue)
        {
            switch (sourceValue)
            {
                case IEnumerable<Int64> sourceValueIEnumInt64:
                    return (inputValue is null
                        || (inputValue is IEnumerable<Int64> imputValueIEnumInt64
                        && !sourceValueIEnumInt64.OrderBy(s => s).SequenceEqual(imputValueIEnumInt64.OrderBy(s => s))));

                default:
                    return !Equals(sourceValue, inputValue);
            }
        }

        private SecurityRoleType? GetRequiredRole(string fieldName)
        {
            if (_securedFields.TryGetValue(fieldName, out SecurityRoleType securityRoleType))
            {
                return securityRoleType;
            }

            return null;
        }

        private string GetPropertyName(Expression<Func<TDto, object>> expression)
        {
            if (expression.Body is MemberExpression)
            {
                return ((MemberExpression)expression.Body).Member.Name;
            }

            var operand = ((UnaryExpression)expression.Body).Operand;

            return ((MemberExpression)operand).Member.Name;
        }

        private bool IsGenericCollection(Type type)
        {
            return (type.IsGenericType && (type.GetGenericTypeDefinition() == typeof(ICollection<>)));
        }
    }
}
