﻿using System;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Recruitment.SnapshotComparer
{
    [Serializable]
    public class ValueChange
    {
        public string Before { get; set; }

        public string After { get; set; }

        public SecurityRoleType? SecurityRole { get; set; }
    }
}
