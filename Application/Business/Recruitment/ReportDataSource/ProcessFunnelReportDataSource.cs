﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.ReportModel;
using Smt.Atomic.Business.Recruitment.Resources;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.ReportDataSource
{
    [Identifier("DataSource.RecruitmentProcessFunnelReport")]
    [RequireRole(SecurityRoleType.CanGenerateRecruitmentProcessFunnelReport)]
    [DefaultReportDefinition(
        "RecruitmentProcessFunnelReport",
        nameof(RecruitmentReportResources.RecruitmentProcessFunnelReportName),
        nameof(RecruitmentReportResources.RecruitmentProcessFunnelReportDescription),
        MenuAreas.Recruitment,
        MenuGroups.RecruitmentReports,
        ReportingEngine.PivotTable,
        "Smt.Atomic.Business.Recruitment.ReportTemplates.ProcessFunnelPivotTable-config.js",
        typeof(RecruitmentReportResources))]

    public class ProcessFunnelReportDataSource : BaseReportDataSource<ProcessFunnelReportParametersDto>
    {
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;
        private readonly ITimeService _timeService;
        private readonly IPrincipalProvider _principalProvider;

        public ProcessFunnelReportDataSource(
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService,
            ITimeService timeService,
            IPrincipalProvider principalProvider)
        {
            _unitOfWorkService = unitOfWorkService;
            _timeService = timeService;
            _principalProvider = principalProvider;
        }

        public override object GetData(ReportGenerationContext<ProcessFunnelReportParametersDto> reportGenerationContext)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recruitmentProcessData = FilterDataScope(unitOfWork.Repositories, reportGenerationContext.Parameters);

                if (_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateRecruitmentReportsForAllEmployees))
                {
                    recruitmentProcessData = recruitmentProcessData
                        .Where(r => true);
                }
                else if (_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateRecruitmentReportsForMyEmployees))
                {
                    recruitmentProcessData = recruitmentProcessData
                        .Where(r => r.RecruitmentProcess.Recruiter.LineManagerId == _principalProvider.Current.EmployeeId);
                }
                else
                {
                    recruitmentProcessData = recruitmentProcessData
                        .Where(r => false);
                }

                var recruitmentProcessDataInfo = recruitmentProcessData
                    .Select(s => new
                    {
                        RecruiterFirstName = s.RecruitmentProcess.Recruiter.FirstName,
                        RecruiterLastName = s.RecruitmentProcess.Recruiter.LastName,
                        StepInfoType = s.Type
                    }).
                    ToList();

                var recruitmentProcessDataGrouped = recruitmentProcessDataInfo
                    .Select(p => new { Recruiter = $"{p.RecruiterFirstName} {p.RecruiterLastName}", p.StepInfoType })
                    .GroupBy(
                    p => p.Recruiter,
                    p => p.StepInfoType,
                    (key, g) => new { Recruiter = key, ProcessStepInfos = g.ToList() });

                var recruitmentProcessStepTypeAllValues = EnumHelper.GetEnumValues(typeof(RecruitmentProcessStepType))
                    .Select(e => (RecruitmentProcessStepType)e);
                var processFunnelReportModelDataset = new List<ProcessFunnelReportModelData>();

                foreach (var processInfo in recruitmentProcessDataGrouped)
                {
                    foreach (var processStepInfo in processInfo.ProcessStepInfos)
                    {
                        processFunnelReportModelDataset.Add(
                            new ProcessFunnelReportModelData
                            {
                                ProcessStepType = processStepInfo.GetDescription(),
                                RecruiterFullName = processInfo.Recruiter,
                                Value = 1
                            });
                    }

                    foreach (var processStepType in recruitmentProcessStepTypeAllValues.Except(processInfo.ProcessStepInfos))
                    {
                        processFunnelReportModelDataset.Add(
                            new ProcessFunnelReportModelData
                            {
                                ProcessStepType = processStepType.GetDescription(),
                                RecruiterFullName = processInfo.Recruiter,
                                Value = 0
                            });
                    }
                }

                return new ProcessFunnelReportModel
                {
                    Records = processFunnelReportModelDataset,
                    ColumnOrder = recruitmentProcessStepTypeAllValues.OrderBy(e => e).Select(e => e.GetDescription()).ToList()
                };
            }
        }

        public override ProcessFunnelReportParametersDto GetDefaultParameters(ReportGenerationContext<ProcessFunnelReportParametersDto> reportGenerationContext)
        {
            reportGenerationContext.Parameters.DateTo = _timeService.GetCurrentDate();
            reportGenerationContext.Parameters.DateFrom = new DateTime(reportGenerationContext.Parameters.DateTo.Year, 1, 1);

            return reportGenerationContext.Parameters;
        }

        private IQueryable<RecruitmentProcessStep> FilterDataScope(
            IRecruitmentDbScope dbScope,
            ProcessFunnelReportParametersDto processFunnelReportParametersDto)
        {
            var recruitmentProcessSteps = dbScope.RecruitmentProcessSteps
                .Where(s => s.Status == RecruitmentProcessStepStatus.Done)
                .Where(DateRangeHelper.IsDateInRange<RecruitmentProcessStep>(processFunnelReportParametersDto.DateFrom, processFunnelReportParametersDto.DateTo.EndOfDay(), s => s.DecidedOn.Value));

            if (!processFunnelReportParametersDto.RecruiterIds.IsNullOrEmpty())
            {
                recruitmentProcessSteps = recruitmentProcessSteps
                    .Where(r => processFunnelReportParametersDto.RecruiterIds.Contains(r.RecruitmentProcess.RecruiterId));
            }

            return recruitmentProcessSteps;
        }
    }
}
