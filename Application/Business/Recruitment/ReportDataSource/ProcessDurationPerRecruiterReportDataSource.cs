﻿using System;
using System.Globalization;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.ReportModel;
using Smt.Atomic.Business.Recruitment.Resources;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.ReportDataSource
{
    [Identifier("DataSource.RecruitmentProcessDurationPerRecruiterReport")]
    [RequireRole(SecurityRoleType.CanGenerateRecruitmentProcessDurationPerRecruiterReport)]
    [DefaultReportDefinition(
        "RecruitmentProcessDurationPerRecruiterReport",
        nameof(RecruitmentReportResources.RecruitmentProcessDurationPerRecruiterReportName),
        nameof(RecruitmentReportResources.RecruitmentProcessDurationPerRecruiterReportDescription),
        MenuAreas.Recruitment,
        MenuGroups.RecruitmentReports,
        ReportingEngine.PivotTable,
        "Smt.Atomic.Business.Recruitment.ReportTemplates.ProcessDurationPerRecruiterPivotTable-config.js",
        typeof(RecruitmentReportResources))]

    public class ProcessDurationPerRecruiterReportDataSource : BaseReportDataSource<ProcessDurationPerRecruiterReportParametersDto>
    {
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;
        private readonly ITimeService _timeService;
        private readonly IPrincipalProvider _principalProvider;

        public ProcessDurationPerRecruiterReportDataSource(
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService,
            ITimeService timeService,
            IPrincipalProvider principalProvider)
        {
            _unitOfWorkService = unitOfWorkService;
            _timeService = timeService;
            _principalProvider = principalProvider;
        }

        public override object GetData(ReportGenerationContext<ProcessDurationPerRecruiterReportParametersDto> reportGenerationContext)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recruitmentProcessData = FilterDataScope(unitOfWork.Repositories, reportGenerationContext.Parameters);

                if (_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateRecruitmentReportsForAllEmployees))
                {
                    recruitmentProcessData = recruitmentProcessData
                        .Where(r => true);
                }
                else if (_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateRecruitmentReportsForMyEmployees))
                {
                    recruitmentProcessData = recruitmentProcessData
                        .Where(r => r.Recruiter.LineManagerId == _principalProvider.Current.EmployeeId);
                }
                else
                {
                    recruitmentProcessData = recruitmentProcessData
                        .Where(r => false);
                }

                var recruitmentProcessDataInfo = recruitmentProcessData
                    .Select(p => new
                    {
                        RecruiterFullName = EmployeeBusinessLogic.FullName.Call(p.Recruiter),
                        StartDate = p.CreatedOn,
                        HrCallStartDate = p.ProcessSteps.Where(s => s.Type == RecruitmentProcessStepType.HrCall)
                            .OrderBy(s => s.PlannedOn)
                            .Select(s => s.PlannedOn)
                            .FirstOrDefault(),
                        HrInterviewStartDate = p.ProcessSteps.Where(s => s.Type == RecruitmentProcessStepType.HrInterview)
                            .OrderBy(s => s.PlannedOn)
                            .Select(s => s.PlannedOn)
                            .FirstOrDefault(),
                        CloseDate = p.ClosedOn,
                        ContractNegotiationsCloseDate = p.ProcessSteps.Where(s => s.Type == RecruitmentProcessStepType.ContractNegotiations)
                            .OrderByDescending(s => s.DecidedOn)
                            .Select(s => s.DecidedOn)
                            .FirstOrDefault(),
                        ClosedReason = p.ClosedReason
                    }).
                    ToList();

                return recruitmentProcessDataInfo
                    .Select(p => new ProcessDurationPerRecruiterReportModel
                    {
                        ProcessResult = p.ClosedReason.Value == RecruitmentProcessClosedReason.Hired
                            ? OverallRecruitmentProcessResult.Positive.GetDescription(CultureInfo.InvariantCulture)
                            : OverallRecruitmentProcessResult.Negative.GetDescription(CultureInfo.InvariantCulture),
                        RecruiterFullName = p.RecruiterFullName,
                        ProcessDurationInDays = DateHelper.GetDaysInRange(
                            p.HrCallStartDate ?? p.HrInterviewStartDate ?? p.StartDate,
                            p.ContractNegotiationsCloseDate ?? p.CloseDate.Value,
                            false)
                            .Count()
                    });
            }
        }

        public override ProcessDurationPerRecruiterReportParametersDto GetDefaultParameters(ReportGenerationContext<ProcessDurationPerRecruiterReportParametersDto> reportGenerationContext)
        {
            reportGenerationContext.Parameters.DateTo = _timeService.GetCurrentDate();
            reportGenerationContext.Parameters.DateFrom = new DateTime(reportGenerationContext.Parameters.DateTo.Year, 1, 1);

            return reportGenerationContext.Parameters;
        }

        private IQueryable<RecruitmentProcess> FilterDataScope(
            IRecruitmentDbScope dbScope,
            ProcessDurationPerRecruiterReportParametersDto parametersDto)
        {
            var recruitmentProcesses = dbScope.RecruitmentProcesses
                .Where(p => p.Status == RecruitmentProcessStatus.Closed && p.ClosedOn.HasValue && p.ClosedReason.HasValue)
                .Where(p => p.ProcessSteps
                    .Any(s => ((s.Type == RecruitmentProcessStepType.HrCall || s.Type == RecruitmentProcessStepType.HrInterview)
                        && s.PlannedOn >= parametersDto.DateFrom)) || p.CreatedOn >= parametersDto.DateFrom)
                .Where(p => p.ProcessSteps
                    .Any(s => s.Type == RecruitmentProcessStepType.ContractNegotiations && s.DecidedOn <= parametersDto.DateTo)
                        || p.ClosedOn <= parametersDto.DateTo);

            if (!parametersDto.RecruiterIds.IsNullOrEmpty())
            {
                recruitmentProcesses = recruitmentProcesses
                    .Where(p => parametersDto.RecruiterIds.Contains(p.RecruiterId));
            }

            if (parametersDto.ProcessResult.HasValue)
            {
                if (parametersDto.ProcessResult.Value == OverallRecruitmentProcessResult.Positive)
                {
                    recruitmentProcesses = recruitmentProcesses
                        .Where(p => p.ClosedReason == RecruitmentProcessClosedReason.Hired);
                }
                else
                {
                    recruitmentProcesses = recruitmentProcesses
                        .Where(p => p.ClosedReason != RecruitmentProcessClosedReason.Hired);
                }
            }

            return recruitmentProcesses;
        }
    }
}
