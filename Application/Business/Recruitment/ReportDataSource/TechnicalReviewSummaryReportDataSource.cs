﻿using System.Linq;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.ReportModel;
using Smt.Atomic.Business.Recruitment.Resources;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.ReportDataSource
{
    [Identifier("DataSource.TechnicalReviewSummaryReport")]
    [RequireRole(SecurityRoleType.CanGenerateTechnicalReviewSummaryReport)]
    [DefaultReportDefinition(
        "TechnicalReviewSummaryReport",
        nameof(RecruitmentReportResources.TechnicalReviewSummaryReportName),
        nameof(RecruitmentReportResources.TechnicalReviewSummaryReportDescription),
        null,
        null,
        ReportingEngine.MsWord,
        "Smt.Atomic.Business.Recruitment.ReportTemplates.TechnicalReviewSummaryTemplate.docx",
        "TechnicalReviewSummary-@(Model.CandidateFirstName)-@(Model.CandidateLastName).docx",
        typeof(RecruitmentReportResources))]

    public class TechnicalReviewSummaryReportDataSource : BaseReportDataSource<TechnicalReviewSummaryReportParametersDto>
    {
        private const string DocumentName = "TechnicalReviewSummary.docx";
        private const string DateFormat = "MM/dd/yyyy H:mm";

        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;
        private readonly IDataActivityLogService _dataActivityLogService;

        public TechnicalReviewSummaryReportDataSource(
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService,
            IDataActivityLogService dataActivityLogService)
        {
            _unitOfWorkService = unitOfWorkService;
            _dataActivityLogService = dataActivityLogService;
        }

        public override object GetData(ReportGenerationContext<TechnicalReviewSummaryReportParametersDto> reportGenerationContext)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var processStepDetails = unitOfWork.Repositories.TechnicalReviews
                    .Where(r => r.RecruitmentProcessStepId == reportGenerationContext.Parameters.RecruitmentProcessStepId)
                    .Select(t => new
                    {
                        OtherRemarks = t.OtherRemarks,
                        EnglishUsage = t.EnglishUsageAssessment,
                        Risk = t.RiskAssessment,
                        Seniority = t.SeniorityLevelAssessment,
                        Strengths = t.StrengthAssessment,
                        SuitabilityForProject = t.TeamProjectSuitabilityAssessment,
                        TechnicalExercise = t.TechnicalAssignmentAssessment,
                        TechnicalKnowledge = t.TechnicalKnowledgeAssessment,
                        CandidateId = t.RecruitmentProcess.Candidate.Id,
                        CandidateFirstName = t.RecruitmentProcess.Candidate.FirstName,
                        CandidateLastName = t.RecruitmentProcess.Candidate.LastName,
                        InterviewDate = t.RecruitmentProcessStep.PlannedOn,
                        PositionName = t.RecruitmentProcess.JobOpening.PositionName,
                        InterviewerAcronym = t.RecruitmentProcessStep.AssignedEmployee.Acronym,
                        JobProfileNames = t.JobProfiles.Select(j => j.Name)
                    })
                    .Single();

                var customerLegalName = unitOfWork.Repositories.Customers.SelectById(reportGenerationContext.Parameters.CustomerId, c => c.LegalName);

                _dataActivityLogService.LogCandidateDataActivity(
                    processStepDetails.CandidateId,
                    ActivityType.SharedDocumentWithCustomer,
                    GetType().Name,
                    DocumentName,
                    $"{customerLegalName} ({reportGenerationContext.Parameters.CustomerId}) [{reportGenerationContext.Parameters.Comment}]");

                return new TechnicalReviewSummaryReportModel
                {
                    OtherRemarks = processStepDetails.OtherRemarks,
                    EnglishUsage = processStepDetails.EnglishUsage,
                    Strengths = processStepDetails.Strengths,
                    Risk = processStepDetails.Risk,
                    Seniority = ((int)processStepDetails.Seniority).ToString(),
                    Profiles = string.Join(", ", processStepDetails.JobProfileNames),
                    SuitabilityForProject = processStepDetails.SuitabilityForProject,
                    TechnicalExercise = processStepDetails.TechnicalExercise,
                    TechnicalKnowledge = processStepDetails.TechnicalKnowledge,
                    CandidateFirstName = processStepDetails.CandidateFirstName,
                    CandidateLastName = processStepDetails.CandidateLastName,
                    InterviewDate = processStepDetails.InterviewDate.HasValue ? processStepDetails.InterviewDate.Value.ToString(DateFormat) : string.Empty,
                    Interviewer = !string.IsNullOrWhiteSpace(processStepDetails.InterviewerAcronym) ? processStepDetails.InterviewerAcronym.ToUpperInvariant() : string.Empty,
                    PositionName = processStepDetails.PositionName
                };
            }
        }

        public override TechnicalReviewSummaryReportParametersDto GetDefaultParameters(ReportGenerationContext<TechnicalReviewSummaryReportParametersDto> reportGenerationContext)
        {
            return reportGenerationContext.Parameters;
        }
    }
}
