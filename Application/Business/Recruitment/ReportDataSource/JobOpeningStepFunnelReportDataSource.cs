﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.ReportModel;
using Smt.Atomic.Business.Recruitment.Resources;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.ReportDataSource
{
    [Identifier("DataSource.RecruitmentJobOpeningStepFunnelReport")]
    [RequireRole(SecurityRoleType.CanGenerateRecruitmentJobOpeningStepFunnelReport)]
    [DefaultReportDefinition(
        "RecruitmentJobOpeningStepFunnelReport",
        nameof(RecruitmentReportResources.RecruitmentJobOpeningStepFunnelReportName),
        nameof(RecruitmentReportResources.RecruitmentJobOpeningStepFunnelReportDescription),
        MenuAreas.Recruitment,
        MenuGroups.RecruitmentReports,
        ReportingEngine.PivotTable,
        "Smt.Atomic.Business.Recruitment.ReportTemplates.RecruitmentJobOpeningStepFunnelPivotTable-config.js",
        typeof(RecruitmentReportResources))]

    public class JobOpeningStepFunnelReportDataSource : BaseReportDataSource<JobOpeningStepFunnelReportParametersDto>
    {
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;
        private readonly ITimeService _timeService;
        private readonly IPrincipalProvider _principalProvider;

        public JobOpeningStepFunnelReportDataSource(
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService,
            ITimeService timeService,
            IPrincipalProvider principalProvider)
        {
            _unitOfWorkService = unitOfWorkService;
            _timeService = timeService;
            _principalProvider = principalProvider;
        }

        public override object GetData(ReportGenerationContext<JobOpeningStepFunnelReportParametersDto> reportGenerationContext)
        {
            var currentEmployeeId = _principalProvider.Current.EmployeeId;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recruitmentProcessSteps = FilterDataScope(unitOfWork.Repositories, reportGenerationContext.Parameters);

                if (_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateRecruitmentReportForAllJobOpenings))
                {
                    recruitmentProcessSteps = recruitmentProcessSteps.Where(s => true);
                }
                else if (_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateRecruitmentReportForMyHiringManagerJobOpenings))
                {
                    recruitmentProcessSteps = recruitmentProcessSteps.Where(s => s.RecruitmentProcess.JobOpening.DecisionMakerEmployee.LineManagerId == currentEmployeeId);
                }
                else if (_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateRecruitmentReportForMyJobOpenings))
                {
                    recruitmentProcessSteps = recruitmentProcessSteps.Where(s => s.RecruitmentProcess.JobOpening.DecisionMakerEmployeeId == currentEmployeeId);
                }
                else
                {
                    recruitmentProcessSteps = recruitmentProcessSteps.Where(r => false);
                }

                var recruitmentProcessStepsDataInfo = recruitmentProcessSteps
                    .Select(s => new
                    {
                        JobOpeningName = s.RecruitmentProcess.JobOpening.PositionName,
                        DecisionMakerFullName = EmployeeBusinessLogic.FullName.Call(s.RecruitmentProcess.JobOpening.DecisionMakerEmployee),
                        StepInfoType = s.Type
                    }).
                    ToList();

                var recruitmentProcessStepDataGrouped = recruitmentProcessStepsDataInfo
                    .GroupBy(
                    p => p.JobOpeningName,
                    p => new { p.StepInfoType, p.DecisionMakerFullName },
                    (key, g) => new
                    {
                        JobOpeningName = key,
                        ProcessStepInfos = g.Select(s => s.StepInfoType).ToList(),
                        DecisionMakerFullName = g.Select(s => s.DecisionMakerFullName).FirstOrDefault()
                    });

                var recruitmentProcessStepTypeAllValues = EnumHelper.GetEnumValues(typeof(RecruitmentProcessStepType))
                    .Select(e => (RecruitmentProcessStepType)e);

                var jobOpeningStepFunnelReportModelDataset = new List<JobOpeningStepFunnelReportModelData>();

                foreach (var processInfo in recruitmentProcessStepDataGrouped)
                {
                    foreach (var processStepInfo in processInfo.ProcessStepInfos)
                    {
                        jobOpeningStepFunnelReportModelDataset.Add(
                            new JobOpeningStepFunnelReportModelData
                            {
                                ProcessStepType = processStepInfo.GetDescription(),
                                JobOpeningName = processInfo.JobOpeningName,
                                DecisionMakerFullName = processInfo.DecisionMakerFullName,
                                Value = 1
                            });
                    }

                    foreach (var processStepType in recruitmentProcessStepTypeAllValues.Except(processInfo.ProcessStepInfos))
                    {
                        jobOpeningStepFunnelReportModelDataset.Add(
                            new JobOpeningStepFunnelReportModelData
                            {
                                ProcessStepType = processStepType.GetDescription(),
                                JobOpeningName = processInfo.JobOpeningName,
                                DecisionMakerFullName = processInfo.DecisionMakerFullName,
                                Value = 0
                            });
                    }
                }

                return new JobOpeningStepFunnelReportModel
                {
                    Records = jobOpeningStepFunnelReportModelDataset,
                    ColumnOrder = recruitmentProcessStepTypeAllValues.OrderBy(e => e).Select(e => e.GetDescription()).ToList()
                };
            }
        }

        private IQueryable<RecruitmentProcessStep> FilterDataScope(
            IRecruitmentDbScope dbScope,
            JobOpeningStepFunnelReportParametersDto jobOpeningStepFunnelReportParametersDto)
        {
            var recruitmentProcessSteps = dbScope.RecruitmentProcessSteps
                .Where(s => s.Status == RecruitmentProcessStepStatus.Done)
                .Where(DateRangeHelper.IsDateInRange<RecruitmentProcessStep>(
                    jobOpeningStepFunnelReportParametersDto.DateFrom,
                    jobOpeningStepFunnelReportParametersDto.DateTo.EndOfDay(),
                    s => s.DecidedOn.Value));

            if (!jobOpeningStepFunnelReportParametersDto.DecisionMakerIds.IsNullOrEmpty())
            {
                recruitmentProcessSteps = recruitmentProcessSteps
                    .Where(r => jobOpeningStepFunnelReportParametersDto.DecisionMakerIds.Contains(r.RecruitmentProcess.JobOpening.DecisionMakerEmployeeId));
            }

            return recruitmentProcessSteps;
        }

        public override JobOpeningStepFunnelReportParametersDto GetDefaultParameters(
            ReportGenerationContext<JobOpeningStepFunnelReportParametersDto> reportGenerationContext)
        {
            reportGenerationContext.Parameters.DateTo = _timeService.GetCurrentDate();
            reportGenerationContext.Parameters.DateFrom = new DateTime(reportGenerationContext.Parameters.DateTo.Year, 1, 1);

            return reportGenerationContext.Parameters;
        }
    }
}
