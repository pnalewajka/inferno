﻿using System;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.ReportModel;
using Smt.Atomic.Business.Recruitment.Resources;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.ReportDataSource
{
    [Identifier("DataSource.RecruitmentProcessProgressReport")]
    [RequireRole(SecurityRoleType.CanGenerateRecruitmentProcessProgressReport)]
    [DefaultReportDefinition(
        "RecruitmentProcessProgressReport",
        nameof(RecruitmentReportResources.RecruitmentProcessProgressReportName),
        nameof(RecruitmentReportResources.RecruitmentProcessProgressReportDescription),
        MenuAreas.Recruitment,
        MenuGroups.RecruitmentReports,
        ReportingEngine.PivotTable,
        "Smt.Atomic.Business.Recruitment.ReportTemplates.RecruitmentProcessProgressPivotTable-config.js",
        typeof(RecruitmentReportResources))]

    public class ProcessProgressReportDataSource : BaseReportDataSource<ProcessProgressReportParametersDto>
    {
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;
        private readonly IPrincipalProvider _principalProvider;

        public ProcessProgressReportDataSource(
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService,
            IPrincipalProvider principalProvider)
        {
            _unitOfWorkService = unitOfWorkService;
            _principalProvider = principalProvider;
        }

        public override object GetData(ReportGenerationContext<ProcessProgressReportParametersDto> reportGenerationContext)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recruitmentProcesses = FilterDataScope(unitOfWork.Repositories, reportGenerationContext.Parameters);

                if (_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateRecruitmentReportsForAllEmployees))
                {
                    recruitmentProcesses = recruitmentProcesses
                        .Where(r => true);
                }
                else if (_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateRecruitmentReportsForMyEmployees))
                {
                    recruitmentProcesses = recruitmentProcesses
                        .Where(r => r.Recruiter.LineManagerId == _principalProvider.Current.EmployeeId);
                }
                else
                {
                    recruitmentProcesses = recruitmentProcesses
                        .Where(r => false);
                }

                var recruitmentProcessData = recruitmentProcesses
                .Select(t => new
                {
                    CandideFirstName = t.Candidate.FirstName,
                    CandideLastName = t.Candidate.LastName,
                    JobOpening = t.JobOpening.PositionName,
                    RecruiterFirstName = t.Recruiter.FirstName,
                    RecruiterLastName = t.Recruiter.LastName,
                    JobProfileNames = t.JobOpening.JobProfiles.Select(j => j.Name),
                    City = t.City.Name,
                    ProcessStepInfos = t.ProcessSteps
                        .Where(s => s.Status == RecruitmentProcessStepStatus.ToDo)
                        .Where(s => !reportGenerationContext.Parameters.AssignedEmployeeIds.Any()
                            || !s.AssignedEmployeeId.HasValue
                            || reportGenerationContext.Parameters.AssignedEmployeeIds.Contains(s.AssignedEmployeeId.Value))
                        .Where(s => !reportGenerationContext.Parameters.RecruitmentProcessStepTypes.Any()
                            || reportGenerationContext.Parameters.RecruitmentProcessStepTypes.Contains(s.Type))
                        .OrderBy(s => s.Type).ThenBy(s => s.PlannedOn ?? DateTime.MaxValue)
                        .Select(s => new { s.Type, AssignedEmployeeFullName = EmployeeBusinessLogic.FullName.Call(s.AssignedEmployee) }),
                    OrgUnitFlatName = t.JobOpening.OrgUnit.FlatName
                }).
                ToList();

                return recruitmentProcessData
                    .Where(p => p.ProcessStepInfos.Any())
                    .Select(p => new ProcessProgressReportModel
                    {
                        CandidateFullName = $"{p.CandideFirstName} {p.CandideLastName}",
                        CurrentStep = p.ProcessStepInfos.Last().Type.GetDescription(),
                        AssignedEmployeeFullName = p.ProcessStepInfos.Last().AssignedEmployeeFullName,
                        JobOpening = p.JobOpening,
                        JobProfiles = string.Join(", ", p.JobProfileNames),
                        RecruiterFullName = $"{p.RecruiterFirstName} {p.RecruiterLastName}",
                        City = p.City,
                        OrgUnitFlatName = p.OrgUnitFlatName
                    });
            }
        }

        private IQueryable<RecruitmentProcess> FilterDataScope(
            IRecruitmentDbScope dbScope,
            ProcessProgressReportParametersDto processProgressReportParametersDto)
        {
            var recruitmentProcesses = dbScope.RecruitmentProcesses
                    .Where(r => r.Status == RecruitmentProcessStatus.Active);

            if (!processProgressReportParametersDto.RecruiterIds.IsNullOrEmpty())
            {
                recruitmentProcesses = recruitmentProcesses
                    .Where(r => processProgressReportParametersDto.RecruiterIds.Contains(r.RecruiterId));
            }

            if (!processProgressReportParametersDto.CityIds.IsNullOrEmpty())
            {
                recruitmentProcesses = recruitmentProcesses
                    .Where(r => r.CityId.HasValue && processProgressReportParametersDto.CityIds.Contains(r.CityId.Value));
            }

            if (!processProgressReportParametersDto.JobProfileIds.IsNullOrEmpty())
            {
                recruitmentProcesses = recruitmentProcesses
                    .Where(r => r.JobOpening.JobProfiles.Any(j => processProgressReportParametersDto.JobProfileIds.Contains(j.Id)));
            }

            if (!processProgressReportParametersDto.JobOpeningIds.IsNullOrEmpty())
            {
                recruitmentProcesses = recruitmentProcesses
                    .Where(r => processProgressReportParametersDto.JobOpeningIds.Contains(r.JobOpeningId));
            }

            if (!processProgressReportParametersDto.OrgUnitIds.IsNullOrEmpty())
            {
                recruitmentProcesses = recruitmentProcesses
                    .Where(r => processProgressReportParametersDto.OrgUnitIds.Contains(r.JobOpening.OrgUnitId));
            }

            return recruitmentProcesses;
        }

        public override ProcessProgressReportParametersDto GetDefaultParameters(ReportGenerationContext<ProcessProgressReportParametersDto> reportGenerationContext)
        {
            return reportGenerationContext.Parameters;
        }
    }
}
