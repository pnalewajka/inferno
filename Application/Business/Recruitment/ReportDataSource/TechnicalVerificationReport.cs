﻿using Smt.Atomic.Business.Recruitment.Resources;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.ReportDataSource
{
    [Identifier("DataSource.TechnicalVerificationReport")]
    [RequireRole(SecurityRoleType.CanGenerateTechnicalVerificationReport)]
    [DefaultReportDefinition(
        "TechnicalVerificationReport",
        nameof(RecruitmentReportResources.TechnicalVerificationReportName),
        nameof(RecruitmentReportResources.TechnicalVerificationReportDescription),
        MenuAreas.Recruitment,
        MenuGroups.RecruitmentReports,
        ReportingEngine.PivotTable,
        new[]
        {
            "Smt.Atomic.Business.Recruitment.ReportTemplates.TechnicalVerificationReport-config.js",
            "Smt.Atomic.Business.Recruitment.ReportTemplates.TechnicalVerificationReport.manifest",
            "Smt.Atomic.Business.Recruitment.ReportTemplates.TechnicalVerificationReport.sql"
        },
        typeof(RecruitmentReportResources))]

    class TechnicalVerificationReport : SqlBasedDataSource
    {
        public TechnicalVerificationReport(IUnitOfWorkService<IDbScope> unitOfWork, IPrincipalProvider principalProvider, IUnitOfWorkService<IAccountsDbScope> unitOfWorkService)
            : base(unitOfWork, principalProvider, unitOfWorkService)
        {
        }
    }
}
