﻿using System;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.ReportModel;
using Smt.Atomic.Business.Recruitment.Resources;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.ReportDataSource
{
    [Identifier("DataSource.RecruitmentHiredCandidateReport")]
    [RequireRole(SecurityRoleType.CanGenerateRecruitmentHiredCandidateReport)]
    [DefaultReportDefinition(
        "RecruitmentHiredCandidateReport",
        nameof(RecruitmentReportResources.RecruitmentHiredCandidateReportReportName),
        nameof(RecruitmentReportResources.RecruitmentHiredCandidateReportDescription),
        MenuAreas.Recruitment,
        MenuGroups.RecruitmentReports,
        ReportingEngine.PivotTable,
        "Smt.Atomic.Business.Recruitment.ReportTemplates.RecruitmentHiredCandidateReportPivotTable-config.js",
        typeof(RecruitmentReportResources))]

    public class HiredCandidateReportDataSource : BaseReportDataSource<HiredCandidateReportParametersDto>
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly ITimeService _timeService;
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWork;

        public HiredCandidateReportDataSource(
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWork,
            IPrincipalProvider principalProvider,
            ITimeService timeService)
        {
            _principalProvider = principalProvider;
            _timeService = timeService;
            _unitOfWork = unitOfWork;
        }

        public override object GetData(ReportGenerationContext<HiredCandidateReportParametersDto> reportGenerationContext)
        {
            using (var unitOfWork = _unitOfWork.Create())
            {
                var recruitmentProcessData = FilterDataScope(unitOfWork.Repositories, reportGenerationContext.Parameters);

                if (_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateRecruitmentReportsForAllEmployees))
                {
                    recruitmentProcessData = recruitmentProcessData
                        .Where(r => true);
                }
                else if (_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateRecruitmentReportsForMyEmployees))
                {
                    recruitmentProcessData = recruitmentProcessData
                        .Where(r => r.Recruiter.LineManagerId == _principalProvider.Current.EmployeeId);
                }
                else
                {
                    recruitmentProcessData = recruitmentProcessData
                        .Where(r => false);
                }

                var recruitmentProcessDataInfo = recruitmentProcessData
                .Select(s => new
                {
                    Recruiter = EmployeeBusinessLogic.FullName.Call(s.Recruiter),
                    CandidateFullName = CandidateBusinessLogic.FullName.Call(s.Candidate),
                    JobOpening = s.JobOpening.PositionName,
                    JobProfileNames = s.JobOpening.JobProfiles.Select(j => j.Name),
                    FinalPosition = s.Final.Position.NameEn,
                    OrgUnit = s.JobOpening.OrgUnit.Name,
                    TrialContractType = s.Final.TrialContractType.NameEn,
                    LongTremContractType = s.Final.LongTermContractType.NameEn,
                    EmploymentOrigin = s.Final.EmploymentSource.Name,
                    StartDate = s.OnboardingRequest != null ? s.OnboardingRequest.StartDate : (DateTime?)null,
                    RecruitmentLeaderFullName = EmployeeBusinessLogic.FullName.Call(s.Recruiter.LineManager)
                }).
                ToList();

                return recruitmentProcessDataInfo
                    .Select(r => new HiredCandidateReportModel
                    {
                        RecruiterFullName = r.Recruiter,
                        CandidateFullName = r.CandidateFullName,
                        JobOpening = r.JobOpening,
                        JobProfiles = string.Join(", ", r.JobProfileNames),
                        FinalPosition = r.FinalPosition,
                        OrgUnit = r.OrgUnit,
                        TrialContractType = r.TrialContractType,
                        EmploymentOrigin = r.EmploymentOrigin,
                        LongTermContractType = r.LongTremContractType,
                        StartDate = r.StartDate,
                        RecruitmentLeaderFullName = r.RecruitmentLeaderFullName
                    });
            }
        }

        public override HiredCandidateReportParametersDto GetDefaultParameters(ReportGenerationContext<HiredCandidateReportParametersDto> reportGenerationContext)
        {
            reportGenerationContext.Parameters.DateTo = _timeService.GetCurrentDate();
            reportGenerationContext.Parameters.DateFrom = new DateTime(reportGenerationContext.Parameters.DateTo.Year, 1, 1);

            return reportGenerationContext.Parameters;
        }

        private IQueryable<RecruitmentProcess> FilterDataScope(
            IRecruitmentDbScope dbScope,
            HiredCandidateReportParametersDto hiredCandidateReportParameters)
        {
            var recruitmentProcesses = dbScope.RecruitmentProcesses
                .Where(s => s.Status == RecruitmentProcessStatus.Closed && s.ClosedReason == RecruitmentProcessClosedReason.Hired && s.ClosedOn.HasValue)
                .Where(DateRangeHelper.IsDateInRange<RecruitmentProcess>(hiredCandidateReportParameters.DateFrom, hiredCandidateReportParameters.DateTo.EndOfDay(), s => s.ClosedOn.Value));

            if (!hiredCandidateReportParameters.RecruiterIds.IsNullOrEmpty())
            {
                recruitmentProcesses = recruitmentProcesses
                    .Where(r => hiredCandidateReportParameters.RecruiterIds.Contains(r.RecruiterId));
            }

            if (!hiredCandidateReportParameters.JobProfileIds.IsNullOrEmpty())
            {
                recruitmentProcesses = recruitmentProcesses
                    .Where(r => r.JobOpening.JobProfiles.Any(j => hiredCandidateReportParameters.JobProfileIds.Contains(j.Id)));
            }

            if (!hiredCandidateReportParameters.CityIds.IsNullOrEmpty())
            {
                recruitmentProcesses = recruitmentProcesses
                    .Where(r => r.JobOpening.Cities.Any(c => hiredCandidateReportParameters.CityIds.Contains(c.Id)));
            }

            return recruitmentProcesses;
        }
    }
}
