﻿using System;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.ReportModel;
using Smt.Atomic.Business.Recruitment.Resources;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.ReportDataSource
{
    [Identifier("DataSource.RecruitmentJobOpeningStatusReport")]
    [RequireRole(SecurityRoleType.CanGenerateRecruitmentJobOpeningStatusReport)]
    [DefaultReportDefinition(
        "RecruitmentJobOpeningStatusReport",
        nameof(RecruitmentReportResources.RecruitmentJobOpeningStatusReportName),
        nameof(RecruitmentReportResources.RecruitmentJobOpeningStatusReportDescription),
        MenuAreas.Recruitment,
        MenuGroups.RecruitmentReports,
        ReportingEngine.PivotTable,
        "Smt.Atomic.Business.Recruitment.ReportTemplates.RecruitmentJobOpeningStatusPivotTable-config.js",
        typeof(RecruitmentReportResources))]

    public class JobOpeningStatusReportDataSource : BaseReportDataSource<JobOpeningStatusReportParametersDto>
    {
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly ITimeService _timeService;

        public JobOpeningStatusReportDataSource(
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService,
            IPrincipalProvider principalProvider,
            ITimeService timeService)
        {
            _unitOfWorkService = unitOfWorkService;
            _principalProvider = principalProvider;
            _timeService = timeService;
        }

        public override object GetData(ReportGenerationContext<JobOpeningStatusReportParametersDto> reportGenerationContext)
        {
            var currentEmployeeId = _principalProvider.Current.EmployeeId;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var jobOpenings = FilterDataScope(unitOfWork.Repositories, reportGenerationContext.Parameters);

                if (!_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateRecruitmentReportForAllJobOpenings))
                {
                    if (_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateRecruitmentReportForMyHiringManagerJobOpenings))
                    {
                        jobOpenings = jobOpenings.Where(j => j.DecisionMakerEmployee.LineManagerId == currentEmployeeId);
                    }
                    else if (_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateRecruitmentReportForMyJobOpenings))
                    {
                        jobOpenings = jobOpenings.Where(j => j.DecisionMakerEmployeeId == currentEmployeeId);
                    }
                    else
                    {
                        jobOpenings = jobOpenings.Where(r => false);
                    }
                }

                var jobOpeningsData = jobOpenings
                .Select(t => new
                {
                    Position = t.PositionName,
                    DecisionMakerFullName = EmployeeBusinessLogic.FullName.Call(t.DecisionMakerEmployee),
                    JobProfileNames = t.JobProfiles.Select(j => j.Name),
                    CityNames = t.Cities.Select(c => c.Name),
                    Status = t.Status,
                    AcceptedByFullName = EmployeeBusinessLogic.FullName.Call(t.AcceptingEmployee),
                    OpenedOn = t.CreatedOn,
                    ClosedReason = t.ClosedReason,
                    ClosedComment = t.ClosedComment
                }).
                ToList();

                return jobOpeningsData
                    .Select(p => new JobOpeningStatusReportModel
                    {
                        Position = p.Position,
                        DecisionMakerFullName = p.DecisionMakerFullName,
                        Cities = string.Join(", ", p.CityNames),
                        JobProfiles = string.Join(", ", p.JobProfileNames),
                        Status = p.Status.GetDescription(),
                        AcceptedByFullName = p.AcceptedByFullName,
                        OpenedOn = p.OpenedOn,
                        ClosedReason = p.ClosedReason.HasValue ? p.ClosedReason.GetDescription() : null,
                        ClosedComment = p.ClosedComment
                    });
            }
        }

        private IQueryable<JobOpening> FilterDataScope(
            IRecruitmentDbScope dbScope,
            JobOpeningStatusReportParametersDto JobOpeningStatusReportParametersDto)
        {
            var jobOpenings = dbScope.JobOpenings
                    .Where(DateRangeHelper.IsDateInRange<JobOpening>(
                    JobOpeningStatusReportParametersDto.DateFrom,
                    JobOpeningStatusReportParametersDto.DateTo.EndOfDay(),
                    j => j.CreatedOn));

            if (!JobOpeningStatusReportParametersDto.JobOpeningStatuses.IsNullOrEmpty())
            {
                jobOpenings = jobOpenings
                    .Where(j => JobOpeningStatusReportParametersDto.JobOpeningStatuses.Contains(j.Status));
            }

            return jobOpenings;
        }

        public override JobOpeningStatusReportParametersDto GetDefaultParameters(ReportGenerationContext<JobOpeningStatusReportParametersDto> reportGenerationContext)
        {
            reportGenerationContext.Parameters.DateTo = _timeService.GetCurrentDate();
            reportGenerationContext.Parameters.DateFrom = new DateTime(reportGenerationContext.Parameters.DateTo.Year, 1, 1);

            return reportGenerationContext.Parameters;
        }
    }
}
