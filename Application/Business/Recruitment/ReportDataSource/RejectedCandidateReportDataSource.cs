﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.ReportModel;
using Smt.Atomic.Business.Recruitment.Resources;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.ReportDataSource
{
    [Identifier("DataSource.RecruitmentRejectedCandidateReport")]
    [RequireRole(SecurityRoleType.CanGenerateRecruitmentRejectedCandidateReport)]
    [DefaultReportDefinition(
        "RecruitmentRejectedCandidateReport",
        nameof(RecruitmentReportResources.RecruitmentRejectedCandidateReportName),
        nameof(RecruitmentReportResources.RecruitmentRejectedCandidateReportDescription),
        MenuAreas.Recruitment,
        MenuGroups.RecruitmentReports,
        ReportingEngine.PivotTable,
        "Smt.Atomic.Business.Recruitment.ReportTemplates.RecruitmentRejectedCandidatePivotTable-config.js",
        typeof(RecruitmentReportResources))]

    public class RejectedCandidateReportDataSource : BaseReportDataSource<RejectedCandidateReportParametersDto>
    {
        private const string DateFormat = "d";
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;
        private readonly ITimeService _timeService;
        private readonly IPrincipalProvider _principalProvider;

        public RejectedCandidateReportDataSource(
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService,
            ITimeService timeService,
            IPrincipalProvider principalProvider)
        {
            _unitOfWorkService = unitOfWorkService;
            _timeService = timeService;
            _principalProvider = principalProvider;
        }

        public override object GetData(ReportGenerationContext<RejectedCandidateReportParametersDto> reportGenerationContext)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recruitmentProcessData = FilterDataScope(unitOfWork.Repositories, reportGenerationContext.Parameters);

                if (_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateRecruitmentReportsForAllEmployees))
                {
                    recruitmentProcessData = recruitmentProcessData
                        .Where(r => true);
                }
                else if (_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateRecruitmentReportsForMyEmployees))
                {
                    recruitmentProcessData = recruitmentProcessData
                        .Where(r => r.Recruiter.LineManagerId == _principalProvider.Current.EmployeeId);
                }
                else
                {
                    recruitmentProcessData = recruitmentProcessData
                        .Where(r => false);
                }

                var recruitmentProcessDatainfo = recruitmentProcessData
                    .Select(r => new
                    {
                        Candidate = CandidateBusinessLogic.FullName.Call(r.Candidate),
                        ProcessStep = r.ProcessSteps
                            .Where(s => s.Status == RecruitmentProcessStepStatus.Done && s.Decision == RecruitmentProcessStepDecision.Negative && s.DecidedOn.HasValue)
                            .OrderByDescending(s => s.DecidedOn)
                            .Select(s => new { s.Type, s.DecisionComment }),
                        ClosedOn = r.ClosedOn,
                        JobOpening = r.JobOpening.PositionName,
                        Reason = r.ClosedReason,
                    }).
                    ToList();

                var rejectedCandidateReportModels = new List<RejectedCandidateReportModel>();

                foreach (var recruitmentProcess in recruitmentProcessDatainfo)
                {
                    if (recruitmentProcess.ProcessStep.Any())
                    {
                        foreach (var processStep in recruitmentProcess.ProcessStep)
                        {
                            rejectedCandidateReportModels.Add(new RejectedCandidateReportModel
                            {
                                CandidateFullName = recruitmentProcess.Candidate,
                                JobOpening = recruitmentProcess.JobOpening,
                                DecisionDate = recruitmentProcess.ClosedOn.Value,
                                StepName = processStep.Type.GetDescription(),
                                Reason = recruitmentProcess.Reason?.GetDescription(),
                                Comment = processStep.DecisionComment
                            });
                        }
                    }
                    else
                    {
                        rejectedCandidateReportModels.Add(new RejectedCandidateReportModel
                        {
                            CandidateFullName = recruitmentProcess.Candidate,
                            JobOpening = recruitmentProcess.JobOpening,
                            DecisionDate = recruitmentProcess.ClosedOn.Value,
                            Reason = recruitmentProcess.Reason?.GetDescription()
                        });
                    }
                }

                return rejectedCandidateReportModels;
            }
        }

        public override RejectedCandidateReportParametersDto GetDefaultParameters(ReportGenerationContext<RejectedCandidateReportParametersDto> reportGenerationContext)
        {
            reportGenerationContext.Parameters.DateTo = _timeService.GetCurrentDate();
            reportGenerationContext.Parameters.DateFrom = new DateTime(reportGenerationContext.Parameters.DateTo.Year, 1, 1);

            return reportGenerationContext.Parameters;
        }

        private IQueryable<RecruitmentProcess> FilterDataScope(
            IRecruitmentDbScope dbScope,
            RejectedCandidateReportParametersDto rejectedCandidateReportParametersDto)
        {
            var recruitmentProcesses = dbScope.RecruitmentProcesses
                .Where(s => s.Status == RecruitmentProcessStatus.Closed && s.ClosedReason != RecruitmentProcessClosedReason.Hired && s.ClosedOn.HasValue)
                .Where(DateRangeHelper.IsDateInRange<RecruitmentProcess>(rejectedCandidateReportParametersDto.DateFrom, rejectedCandidateReportParametersDto.DateTo.EndOfDay(), s => s.ClosedOn.Value));

            if (!rejectedCandidateReportParametersDto.RecruiterIds.IsNullOrEmpty())
            {
                recruitmentProcesses = recruitmentProcesses
                    .Where(r => rejectedCandidateReportParametersDto.RecruiterIds.Contains(r.RecruiterId));
            }

            if (!rejectedCandidateReportParametersDto.RejectingEmployeeIds.IsNullOrEmpty())
            {
                recruitmentProcesses = recruitmentProcesses
                    .Where(r => r.ProcessSteps
                        .Any(s => s.Status == RecruitmentProcessStepStatus.Done && s.Decision == RecruitmentProcessStepDecision.Negative
                            && (!s.AssignedEmployeeId.HasValue || rejectedCandidateReportParametersDto.RejectingEmployeeIds.Contains(s.AssignedEmployeeId.Value))));
            }

            if (rejectedCandidateReportParametersDto.OrgUnitId.HasValue)
            {
                recruitmentProcesses = recruitmentProcesses
                    .Where(r => r.JobOpening.OrgUnitId == rejectedCandidateReportParametersDto.OrgUnitId.Value);
            }

            if (rejectedCandidateReportParametersDto.RejectionReason.HasValue)
            {
                recruitmentProcesses = recruitmentProcesses
                    .Where(r => r.ClosedReason == rejectedCandidateReportParametersDto.RejectionReason.Value);
            }

            return recruitmentProcesses;
        }
    }
}
