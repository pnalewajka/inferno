﻿using System;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.Business.Recruitment.ReportModel;
using Smt.Atomic.Business.Recruitment.Resources;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.ReportDataSource
{
    [Identifier("DataSource.RecruitmentJobOpeningCurrentStepReport")]
    [RequireRole(SecurityRoleType.CanGenerateRecruitmentJobOpeningCurrentStepReport)]
    [DefaultReportDefinition(
        "RecruitmentJobOpeningCurrentStepReport",
        nameof(RecruitmentReportResources.RecruitmentJobOpeningCurrentStepReportName),
        nameof(RecruitmentReportResources.RecruitmentJobOpeningCurrentStepReportDescription),
        MenuAreas.Recruitment,
        MenuGroups.RecruitmentReports,
        ReportingEngine.PivotTable,
        "Smt.Atomic.Business.Recruitment.ReportTemplates.RecruitmentJobOpeningCurrentStepPivotTable-config.js",
        typeof(RecruitmentReportResources))]

    public class JobOpeningCurrentStepReportDataSource : BaseReportDataSource<JobOpeningCurrentStepReportParametersDto>
    {
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;
        private readonly IPrincipalProvider _principalProvider;

        public JobOpeningCurrentStepReportDataSource(
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService,
            IPrincipalProvider principalProvider)
        {
            _unitOfWorkService = unitOfWorkService;
            _principalProvider = principalProvider;
        }

        public override object GetData(ReportGenerationContext<JobOpeningCurrentStepReportParametersDto> reportGenerationContext)
        {
            var currentEmployeeId = _principalProvider.Current.EmployeeId;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recruitmentProcesses = FilterDataScope(unitOfWork.Repositories, reportGenerationContext.Parameters);

                if (!_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateRecruitmentReportForAllJobOpenings))
                {
                    if (_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateRecruitmentReportForMyHiringManagerJobOpenings))
                    {
                        recruitmentProcesses = recruitmentProcesses.Where(r => r.JobOpening.DecisionMakerEmployee.LineManagerId == currentEmployeeId);
                    }
                    else if (_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateRecruitmentReportForMyJobOpenings))
                    {
                        recruitmentProcesses = recruitmentProcesses.Where(r => r.JobOpening.DecisionMakerEmployeeId == currentEmployeeId);
                    }
                    else
                    {
                        recruitmentProcesses = recruitmentProcesses.Where(r => false);
                    }
                }

                var recruitmentProcessData = recruitmentProcesses
                .Select(t => new
                {
                    CandideFullName = CandidateBusinessLogic.FullName.Call(t.Candidate),
                    JobOpening = t.JobOpening.PositionName,
                    HiringManagerFullName = EmployeeBusinessLogic.FullName.Call(t.Recruiter),
                    JobProfileNames = t.JobOpening.JobProfiles.Select(j => j.Name),
                    City = t.City.Name,
                    StepInfoType = t.ProcessSteps
                        .Where(s => s.Status == RecruitmentProcessStepStatus.ToDo)
                        .OrderBy(s => s.Type).ThenBy(s => s.PlannedOn ?? DateTime.MaxValue)
                        .Select(s => s.Type)
                }).
                ToList();

                return recruitmentProcessData
                    .Where(p => p.StepInfoType.Any())
                    .Select(p => new JobOpeningCurrentStepReportModel
                    {
                        CandidateFullName = p.CandideFullName,
                        CurrentStage = p.StepInfoType.Last().GetDescription(),
                        JobOpening = p.JobOpening,
                        JobProfiles = string.Join(", ", p.JobProfileNames),
                        HiringManagerFullName = p.HiringManagerFullName,
                        City = p.City
                    });
            }
        }

        private IQueryable<RecruitmentProcess> FilterDataScope(
            IRecruitmentDbScope dbScope,
            JobOpeningCurrentStepReportParametersDto JobOpeningCurrentStepReportParametersDto)
        {
            var recruitmentProcesses = dbScope.RecruitmentProcesses
                    .Where(r => r.Status == RecruitmentProcessStatus.Active);

            if (!JobOpeningCurrentStepReportParametersDto.HiringManagersIds.IsNullOrEmpty())
            {
                recruitmentProcesses = recruitmentProcesses
                    .Where(r => JobOpeningCurrentStepReportParametersDto.HiringManagersIds.Contains(r.RecruiterId));
            }

            if (!JobOpeningCurrentStepReportParametersDto.CityIds.IsNullOrEmpty())
            {
                recruitmentProcesses = recruitmentProcesses
                    .Where(r => r.CityId.HasValue && JobOpeningCurrentStepReportParametersDto.CityIds.Contains(r.CityId.Value));
            }

            if (!JobOpeningCurrentStepReportParametersDto.JobProfileIds.IsNullOrEmpty())
            {
                recruitmentProcesses = recruitmentProcesses
                    .Where(r => r.JobOpening.JobProfiles.Any(j => JobOpeningCurrentStepReportParametersDto.JobProfileIds.Contains(j.Id)));
            }

            if (!JobOpeningCurrentStepReportParametersDto.JobOpeningIds.IsNullOrEmpty())
            {
                recruitmentProcesses = recruitmentProcesses
                    .Where(r => JobOpeningCurrentStepReportParametersDto.JobOpeningIds.Contains(r.JobOpening.Id));
            }

            return recruitmentProcesses;
        }

        public override JobOpeningCurrentStepReportParametersDto GetDefaultParameters(ReportGenerationContext<JobOpeningCurrentStepReportParametersDto> reportGenerationContext)
        {
            return reportGenerationContext.Parameters;
        }
    }
}
