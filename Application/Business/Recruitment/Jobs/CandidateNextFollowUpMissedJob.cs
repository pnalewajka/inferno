﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Castle.Core.Logging;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Dto.Candidates;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Jobs
{
    [Identifier("Job.Recruitment.CandidateNextFollowUpMissedJob")]
    [JobDefinition(
        Code = "CandidateNextFollowUpMissedJob",
        Description = "Job sending email about missed candidate next follow-up date",
        ScheduleSettings = "0 30 21 * * *",
        PipelineName = PipelineCodes.Recruitment)]
    public class CandidateNextFollowUpMissedJob : IJob
    {
        private readonly ILogger _logger;
        private readonly ITimeService _timeService;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly ICandidateNoteService _candidateNoteService;
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;
        private const string CanididateUrl = "{0}Recruitment/Candidate/Details/{1}";
        private const string CandidateNoteContent = "Follow-up reminder email was sent to the contact coordinator {0}.";
        private readonly string _serverAddress;

        public CandidateNextFollowUpMissedJob(
            ILogger logger,
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService,
            ITimeService timeService,
            ISystemParameterService systemParameterService,
            IReliableEmailService reliableEmailService,
            IMessageTemplateService messageTemplateService,
            ICandidateNoteService candidateNoteService)
        {
            _logger = logger;
            _timeService = timeService;
            _reliableEmailService = reliableEmailService;
            _messageTemplateService = messageTemplateService;
            _candidateNoteService = candidateNoteService;
            _unitOfWorkService = unitOfWorkService;
            _serverAddress = systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            try
            {
                InternalDoJob(executionContext.CancellationToken);
            }
            catch (Exception exception)
            {
                _logger.Error("Job.Recruitment.FollowUpWithCandidateJob error", exception);

                return JobResult.Fail();
            }

            return JobResult.Success();
        }

        private void InternalDoJob(CancellationToken cancellationToken)
        {
            var currentDate = _timeService.GetCurrentTime().EndOfDay();

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var candidates = unitOfWork.Repositories.Candidates
                    .Where(CandidateBusinessLogic.IsFollowUpNeeded.Parametrize(currentDate))
                    .Select(c => new
                    {
                        RecipientEmailAddress = c.ContactCoordinator.Email,
                        ReceiverFirstName = c.ContactCoordinator.FirstName,
                        RecipientLastName = c.ContactCoordinator.LastName,
                        c.FirstName,
                        c.LastName,
                        c.Id,
                        c.NextFollowUpDate
                    })
                    .ToArray();

                foreach (var candidate in candidates.TakeWhileNotCancelled(cancellationToken))
                {
                    var contactCoordinatorFullName = $"{candidate.ReceiverFirstName} {candidate.RecipientLastName}";

                    var emailRecipientDto = new EmailRecipientDto
                    {
                        EmailAddress = candidate.RecipientEmailAddress,
                        FullName = contactCoordinatorFullName,
                    };

                    var candidateNextFollowUpMissedReminderDto = new CandidateNextFollowUpMissedReminderDto
                    {
                        CandidateFullName = $"{candidate.FirstName} {candidate.LastName}",
                        CandidateUrl = string.Format(CanididateUrl, _serverAddress, candidate.Id),
                        NextFollowUpDate = candidate.NextFollowUpDate.Value,
                        RecipientDisplayName = candidate.ReceiverFirstName
                    };

                    SendEmail(candidateNextFollowUpMissedReminderDto, emailRecipientDto);

                    var candidateToNotify = unitOfWork.Repositories.Candidates.GetById(candidate.Id);
                    candidateToNotify.LastFollowUpReminderDate = currentDate;
                    unitOfWork.Commit();

                    _candidateNoteService.AddNote(new CandidateNoteDto
                    {
                        CandidateId = candidate.Id,
                        NoteType = NoteType.SystemNote,
                        NoteVisibility = NoteVisibility.Public,
                        Content = string.Format(CandidateNoteContent, contactCoordinatorFullName)
                    });
                }
            }
        }

        private void SendEmail(CandidateNextFollowUpMissedReminderDto candidateNextFollowUpMissedReminderDto, EmailRecipientDto emailRecipientDto)
        {
            var emailSubject = _messageTemplateService.ResolveTemplate(TemplateCodes.CandidateNextFollowUpMissedReminderSubject, candidateNextFollowUpMissedReminderDto);
            var emailBody = _messageTemplateService.ResolveTemplate(TemplateCodes.CandidateNextFollowUpMissedReminderBody, candidateNextFollowUpMissedReminderDto);

            var emailMessageDto = new EmailMessageDto
            {
                IsHtml = emailBody.IsHtml,
                Subject = emailSubject.Content,
                Body = emailBody.Content,
                Recipients = new List<EmailRecipientDto> { emailRecipientDto }
            };

            _reliableEmailService.EnqueueMessage(emailMessageDto);
        }
    }
}
