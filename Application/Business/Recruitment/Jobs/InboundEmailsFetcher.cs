﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Web;
using Castle.Core.Logging;
using Microsoft.Exchange.WebServices.Data;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.Business.Recruitment.InboundEmailParser;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Exchange.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Jobs
{
    [Identifier("Job.Recruitment.InboundEmailFetcher")]
    [JobDefinition(
        Code = "InboundEmailFetcher",
        Description = "Job fetching inbound emails for recruitment process",
        ScheduleSettings = "0 0 22 * * *",
        PipelineName = PipelineCodes.Recruitment)]
    public class InboundEmailFetcher : IJob
    {
        private readonly ILogger _logger;
        private readonly IInboundEmailOfficeService _inboundEmailOfficeService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IInboundEmailService _inboundEmailService;
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;

        public InboundEmailFetcher(
            ILogger logger,
            IInboundEmailOfficeService inboundEmailOfficeService,
            ISystemParameterService systemParameterService,
            IInboundEmailService inboundEmailService,
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService)
        {
            _logger = logger;
            _inboundEmailOfficeService = inboundEmailOfficeService;
            _systemParameterService = systemParameterService;
            _inboundEmailService = inboundEmailService;
            _unitOfWorkService = unitOfWorkService;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            try
            {
                FetchEmails(executionContext.CancellationToken);
            }
            catch (Exception exception)
            {
                _logger.Error($"{IdentifierHelper.GetTypeIdentifier(typeof(InboundEmailFetcher))}: error", exception);

                return JobResult.Fail();
            }

            return JobResult.Success();
        }

        private void FetchEmails(CancellationToken cancellationToken)
        {
            var inboundEmailPageSize = _systemParameterService.GetParameter<int>(ParameterKeys.InboundEmailPageSize);
            var inboundEmailGroupNames = _systemParameterService.GetParameter<string[]>(ParameterKeys.InboundEmailGroupNames);
            var lastTimeRecivedOn = _inboundEmailService.GetLastReceivedOnOrDefault() ?? DateTime.MinValue;
            var maxInboundEmailAttachmentSize = _systemParameterService.GetParameter<int>(ParameterKeys.MaxInboundEmailAttachmentSizeInMB) * 1024 * 1024;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var inboundEmailParserExecutor = new InboundEmailParserService(unitOfWork, _logger);

                foreach (var inboundEmailGroupName in inboundEmailGroupNames)
                {
                    _inboundEmailOfficeService.ReadEmails(inboundEmailGroupName, lastTimeRecivedOn, inboundEmailPageSize, email =>
                    {
                        var inboundEmail = CreateInboundEmail(email, maxInboundEmailAttachmentSize, out List<string> parserMessages);

                        if (inboundEmailParserExecutor.TryParse(inboundEmail, out IList<InboundEmailValue> inboundEmailValues))
                        {
                            var inboundEmailMessageType = inboundEmailValues.FirstOrDefault(v => v.Key == InboundEmailValueType.MessageType);

                            if (inboundEmailMessageType != null && inboundEmailMessageType.ValueLong != null)
                            {
                                var emailCategory = (InboundEmailCategory)inboundEmailMessageType.ValueLong.Value;
                                inboundEmail.Category = emailCategory;
                                inboundEmail.Subject = string.IsNullOrEmpty(inboundEmail.Subject) ? "(No Subject)" : inboundEmail.Subject;
                                inboundEmail.Body = string.IsNullOrEmpty(inboundEmail.Body) ? "(No Body)" : inboundEmail.Body;
                            }

                            unitOfWork.Repositories.InboundEmails.Add(inboundEmail);
                            unitOfWork.Commit();

                            foreach (var inboundEmailValue in inboundEmailValues)
                            {
                                inboundEmailValue.InboundEmailId = inboundEmail.Id;
                                unitOfWork.Repositories.InboundEmailValues.Add(inboundEmailValue);
                            }

                            foreach (var parserMessage in parserMessages)
                            {
                                unitOfWork.Repositories.InboundEmailValues.Add(
                                    new InboundEmailValue
                                    {
                                        InboundEmailId = inboundEmail.Id,
                                        Key = InboundEmailValueType.ParserMessage,
                                        ValueString = parserMessage
                                    });
                            }

                            unitOfWork.Commit();
                        }
                        else
                        {
                            unitOfWork.Repositories.InboundEmails.Add(inboundEmail);
                            unitOfWork.Commit();
                        }
                    });
                }
            }
        }

        private InboundEmail CreateInboundEmail(EmailMessage emailMessage, int maxInboundEmailAttachmentSize, out List<string> parserMessages)
        {
            parserMessages = new List<string>();

            var inboundEmail = new InboundEmail
            {
                BodyType = emailMessage.Body.BodyType == BodyType.HTML ? EmailBodyType.Html : EmailBodyType.Text,
                Body = HtmlSanitizeHelper.Sanitize(emailMessage.Body.Text, HtmlSanitizeMethod.Strict),
                Subject = emailMessage.Subject,
                FromEmailAddress = emailMessage.From.Address,
                FromFullName = emailMessage.From.Name,
                ReceivedOn = emailMessage.DateTimeReceived,
                SentOn = emailMessage.DateTimeSent,
                To = string.Join(", ", emailMessage.ToRecipients.Select(a => a.Address))
            };

            if (emailMessage.Attachments.Any())
            {
                inboundEmail.Attachments = new Collection<InboundEmailDocument>();

                foreach (var attachment in emailMessage.Attachments)
                {
                    var content = _inboundEmailOfficeService.GetAttachmentFileContent(attachment);

                    if (content.Length > maxInboundEmailAttachmentSize)
                    {
                        parserMessages.Add($"Attachment {attachment.Name} size exceeded {nameof(ParameterKeys.MaxInboundEmailAttachmentSizeInMB)}");

                        continue;
                    }

                    var mimeType = MimeMapping.GetMimeMapping(attachment.Name.Trim());

                    if (mimeType == MimeHelper.BinaryFile)
                    {
                        _logger.Error($"Couldn't find mime-type for file {attachment.Name}");
                    }

                    foreach (var inboundEmailDocument in ExtractInboundEmailDocumentsFromAttachment(attachment, content))
                    {
                        inboundEmail.Attachments.Add(inboundEmailDocument);
                    }
                }
            }

            return inboundEmail;
        }

        private IEnumerable<InboundEmailDocument> ExtractInboundEmailDocumentsFromAttachment(Attachment attachment, byte[] content)
        {
            var mimeType = MimeMapping.GetMimeMapping(attachment.Name);

            if (mimeType != MimeHelper.CompressedZipFile)
            {
                yield return new InboundEmailDocument
                {
                    Name = attachment.Name,
                    ContentType = MimeMapping.GetMimeMapping(attachment.Name),
                    ContentLength = content.Length,
                    DocumentContent = new InboundEmailDocumentContent
                    {
                        Content = content
                    }
                };

                yield break;
            }

            var inboundEmailDocuments = ZipArchiveHelper.UnzipFilesFromByteArray(content).Select(s => new InboundEmailDocument
            {
                Name = s.FileName,
                ContentType = MimeMapping.GetMimeMapping(s.FileName),
                ContentLength = s.Content.Length,
                DocumentContent = new InboundEmailDocumentContent
                {
                    Content = s.Content
                }
            }).AsEnumerable();

            foreach (var inboundEmailDocument in inboundEmailDocuments)
            {
                yield return inboundEmailDocument;
            }
        }
    }
}
