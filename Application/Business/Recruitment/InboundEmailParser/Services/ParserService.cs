﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Recruitment.Interfaces;

namespace Smt.Atomic.Business.Recruitment.InboundEmailParser
{
    public abstract class ParserService<TParser, TSource, TDestination>
        where TParser : class, IParser<TSource, TDestination>
        where TSource : class
        where TDestination : class
    {
        protected abstract List<TParser> Parsers { get; }

        private TParser SelectApplicableParser(TSource source)
        {
            return Parsers.FirstOrDefault(s => s.CanParse(source));
        }

        protected virtual void AddToValues(IList<TDestination> destinationValues, TDestination destination)
        {
            destinationValues.Add(destination);
        }

        public bool TryParse(TSource source, out IList<TDestination> destinationValues)
        {
            destinationValues = null;

            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            var parser = SelectApplicableParser(source);

            if (parser == null)
            {
                return false;
            }

            destinationValues = new List<TDestination>();

            foreach (var destination in parser.GetParserItems(source))
            {
                AddToValues(destinationValues, destination);
            }

            return true;
        }
    }
}
