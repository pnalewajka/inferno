﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Logging;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.InboundEmailParser
{
    internal class InboundEmailParserService : ParserService<InboundEmailParser, InboundEmail, InboundEmailValue>
    {
        private readonly List<InboundEmailParser> _parsers;

        public InboundEmailParserService(IUnitOfWork<IRecruitmentDbScope> unitOfWork, ILogger logger)
        {
            _parsers = GetParsers(unitOfWork, logger);
        }

        private List<InboundEmailParser> GetParsers(IUnitOfWork<IRecruitmentDbScope> unitOfWork, ILogger logger)
        {
            if (unitOfWork == null)
            {
                throw new ArgumentNullException(nameof(unitOfWork));
            }

            if (logger == null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            var parsers = new Dictionary<string, InboundEmailParser>();

            var parserIdentifiers = unitOfWork.Repositories.ApplicationOrigins
                .Where(o => o.ParserIdentifier != null)
                .Select(o => o.ParserIdentifier)
                .ToList();

            foreach (var identifier in parserIdentifiers)
            {
                if (parsers.ContainsKey(identifier))
                {
                    continue;
                }

                var parserType = default(Type);

                try
                {
                    parserType = IdentifierHelper.GetTypeByIdentifier(identifier);
                }
                catch (Exception ex)
                {
                    logger.Error("Recruitment.InboundEmailParserService error", ex);

                    continue;
                }

                if (ReflectionHelper.CreateInstanceWithIocDependencies(parserType, new { unitOfWork }) is InboundEmailParser parser)
                {
                    parsers.Add(identifier, parser);
                }
            }

            return parsers.Values.ToList();
        }

        protected override List<InboundEmailParser> Parsers => _parsers;
    }
}
