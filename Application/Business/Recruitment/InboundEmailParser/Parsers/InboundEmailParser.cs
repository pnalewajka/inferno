﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Modules.Recruitment;

namespace Smt.Atomic.Business.Recruitment.InboundEmailParser
{
    public abstract class InboundEmailParser : IParser<InboundEmail, InboundEmailValue>
    {
        protected InboundEmailCategory _emailCategory { get; set; }

        protected InboundEmailParser(InboundEmailCategory emailCategory)
        {
            _emailCategory = emailCategory;
        }

        public virtual IEnumerable<InboundEmailValue> GetParserItems(InboundEmail email)
        {
            return GetParserSpecificItems(email)
                .Union(GetParserRequiredItems());
        }

        protected IEnumerable<InboundEmailValue> GetParserRequiredItems()
        {
            yield return new InboundEmailValue { Key = InboundEmailValueType.MessageType, ValueLong = (long)_emailCategory };
        }

        protected virtual IEnumerable<InboundEmailValue> GetParserSpecificItems(InboundEmail email)
        {
            yield break;
        }

        public abstract bool CanParse(InboundEmail inboundEmail);
    }
}
