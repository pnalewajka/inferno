﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Castle.Core.Logging;
using Newtonsoft.Json.Linq;
using Smt.Atomic.Business.Recruitment.Dto.IntiveWebsiteEmail;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.InboundEmailParser.Parsers
{
    [Identifier("Recruitment.IntiveEmailParser")]
    public class IntiveEmailParser : InboundEmailParserByApplicationOrigin
    {
        protected override ApplicationOriginType[] ParserOriginTypes => new[] { ApplicationOriginType.JobAdvertisement };
        private readonly ILogger _logger;

        public IntiveEmailParser(
            IUnitOfWork<IRecruitmentDbScope> unitOfWork,
            ILogger logger)
            : base(unitOfWork, InboundEmailCategory.JobApplication)
        {
            _logger = logger;
        }

        protected override IEnumerable<InboundEmailValue> GetParserSpecificItems(InboundEmail email)
        {
            var parsedItem = ParseJson(email);
            _emailCategory = InboundEmailCategory.JobApplication;

            if (parsedItem == null)
            {
                yield break;
            }

            foreach (var consents in ParseConsents(parsedItem.Consents, InboundEmailValueType.CandidateConsentType))
            {
                yield return consents;
            }

            if (parsedItem.EmailType == EmailType.RecommendationJobApplication
                || parsedItem.EmailType == EmailType.RecommendationJobNinja)
            {
                var recommendationJobApplicationDto = (RecommendationJobApplicationDto)parsedItem;
                _emailCategory = InboundEmailCategory.Recommendation;

                foreach (var inboundEmailValue in ParseRecommendationJobApplicationDto(recommendationJobApplicationDto))
                {
                    yield return inboundEmailValue;
                }

                yield break;
            }

            foreach (var inboundEmailValue in ParseBaseJobApplicationDto(parsedItem))
            {
                yield return inboundEmailValue;
            }

            switch (parsedItem.EmailType)
            {
                case EmailType.JobApplication:
                    var jobApplicationDto = (JobApplicationDto)parsedItem;

                    yield return new InboundEmailValue { Key = InboundEmailValueType.JobApplicationId, ValueString = jobApplicationDto.JobOffer };
                    yield return new InboundEmailValue { Key = InboundEmailValueType.ReferenceNumber, ValueString = jobApplicationDto.ReferenceNumber };
                    break;

                case EmailType.NinjaJobApplication:
                    var ninjaJobApplication = (NinjaJobApplicationDto)parsedItem;

                    yield return new InboundEmailValue { Key = InboundEmailValueType.CandidateAreaOfInterest, ValueString = ninjaJobApplication.WhatDoForUs };
                    break;
            }
        }

        private BaseJobApplicationDto ParseJson(InboundEmail inboundEmail)
        {
            var regExForJson = new Regex(".*.(json)");

            var json = inboundEmail.Attachments.FirstOrDefault(s => regExForJson.Match(s.Name).Success);
            var jsonStr = Encoding.UTF8.GetString(json.DocumentContent.Content);

            var jObject = JObject.Parse(jsonStr);

            return ParseToJobApplication(jObject);
        }

        private BaseJobApplicationDto ParseToJobApplication(JObject jObject)
        {
            const string Type = "Type";

            var type = jObject[Type].Value<string>();

            if (string.IsNullOrEmpty(type))
            {
                throw new Exception("Job Application type was not properly specified in the attached JSON file");
            }

            switch (type)
            {
                case nameof(EmailType.JobApplication):
                    return jObject.ToObject<JobApplicationDto>();

                case nameof(EmailType.NinjaJobApplication):
                    return jObject.ToObject<NinjaJobApplicationDto>();

                case nameof(EmailType.RecommendationJobApplication):
                    return jObject.ToObject<RecommendationJobApplicationDto>();

                case nameof(EmailType.RecommendationJobNinja):
                    return jObject.ToObject<RecommendationJobNinjaDto>();

                default:
                    _logger.Error("Not supported email type from intive website");

                    return null;
            }
        }

        private IEnumerable<InboundEmailValue> ParseRecommendationJobApplicationDto(RecommendationJobApplicationDto dto)
        {
            yield return new InboundEmailValue { Key = InboundEmailValueType.CandidateFirstName, ValueString = dto.FriendFirstName };
            yield return new InboundEmailValue { Key = InboundEmailValueType.CandidateLastName, ValueString = dto.FriendLastName };
            yield return new InboundEmailValue { Key = InboundEmailValueType.CandidateEmailAddress, ValueString = dto.FriendEmail };
            yield return new InboundEmailValue { Key = InboundEmailValueType.CandidatePhoneNumber, ValueString = dto.FriendPhoneNumber };
            yield return new InboundEmailValue { Key = InboundEmailValueType.JobProfileId, ValueString = dto.FriendJobPreferences };
            yield return new InboundEmailValue { Key = InboundEmailValueType.RecommendingPersonFirstName, ValueString = dto.FirstName };
            yield return new InboundEmailValue { Key = InboundEmailValueType.RecommendingPersonLastName, ValueString = dto.LastName };
            yield return new InboundEmailValue { Key = InboundEmailValueType.RecommendingPersonEmailAddress, ValueString = dto.Email };
            yield return new InboundEmailValue { Key = InboundEmailValueType.RecommendingPersonPhoneNumber, ValueString = dto.PhoneNumber };

            yield return ParseCityToDanteValue(dto.EmailType == EmailType.RecommendationJobNinja ? dto.FriendLocationName : dto.LocationName);

            var matchingRecommendingPerson = UnitOfWork.Repositories.RecommendingPersons.Where(s => s.EmailAddress == dto.Email).Select(p => (long?)p.Id).SingleOrDefault();

            if (matchingRecommendingPerson != null)
            {
                yield return new InboundEmailValue { Key = InboundEmailValueType.MatchingRecommendingPersonId, ValueLong = matchingRecommendingPerson };
            }
        }

        private IEnumerable<InboundEmailValue> ParseBaseJobApplicationDto(BaseJobApplicationDto dto)
        {
            yield return new InboundEmailValue { Key = InboundEmailValueType.CandidateFirstName, ValueString = dto.FirstName };
            yield return new InboundEmailValue { Key = InboundEmailValueType.CandidateLastName, ValueString = dto.LastName };
            yield return new InboundEmailValue { Key = InboundEmailValueType.CandidateEmailAddress, ValueString = dto.Email };
            yield return new InboundEmailValue { Key = InboundEmailValueType.CandidatePhoneNumber, ValueString = dto.PhoneNumber };
            yield return ParseCityToDanteValue(dto.LocationName);
        }

        private InboundEmailValue ParseCityToDanteValue(string city)
        {
            var matchingCity = UnitOfWork.Repositories.Cities.FirstOrDefault(s => s.Name == city);

            if (matchingCity == null)
            {
                return new InboundEmailValue { Key = InboundEmailValueType.CandidatePreferedLocation, ValueString = city };
            }

            return new InboundEmailValue { Key = InboundEmailValueType.CityId, ValueLong = matchingCity.Id };
        }

        private IEnumerable<InboundEmailValue> ParseConsents(string consents, InboundEmailValueType valueType)
        {
            var attachedConsensts = consents.Split(';').Select(s => s.Split('_')[0]);

            foreach (var attachedConsent in attachedConsensts)
            {
                var succeeded = EnumHelper.TryParse(attachedConsent, out DataConsentType consentType);

                if (!succeeded)
                {
                    continue;
                }

                yield return new InboundEmailValue { Key = valueType, ValueLong = (long)consentType };
            }
        }
    }
}
