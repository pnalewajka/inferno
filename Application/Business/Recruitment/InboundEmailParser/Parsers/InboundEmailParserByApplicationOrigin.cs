﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.InboundEmailParser
{
    public abstract class InboundEmailParserByApplicationOrigin : InboundEmailParser
    {
        private ApplicationOrigin _applicationOrigin;

        protected readonly IUnitOfWork<IRecruitmentDbScope> UnitOfWork;

        protected abstract ApplicationOriginType[] ParserOriginTypes { get; }

        protected InboundEmailParserByApplicationOrigin(IUnitOfWork<IRecruitmentDbScope> unitOfWork, InboundEmailCategory emailCategory) : base(emailCategory)
        {
            UnitOfWork = unitOfWork;
        }

        private ApplicationOrigin GetApplicationOrigin(InboundEmail inboundEmail)
        {
            if (UnitOfWork == null)
            {
                throw new ArgumentNullException(nameof(UnitOfWork));
            }

            var identifier = IdentifierHelper.GetTypeIdentifier(GetType());

            return UnitOfWork.Repositories.ApplicationOrigins
                .Where(o => o.ParserIdentifier == identifier)
                .Where(o => ParserOriginTypes.Contains(o.OriginType))
                .Where(o => o.EmailAddress == inboundEmail.FromEmailAddress)
                .FirstOrDefault();
        }

        public override IEnumerable<InboundEmailValue> GetParserItems(InboundEmail email)
        {
            return base.GetParserItems(email)
                .Union(GetParserSpecificForAppliactionOrigin());
        }

        private IEnumerable<InboundEmailValue> GetParserSpecificForAppliactionOrigin()
        {
            var applicationOriginId = GetApplicationOriginId();
            var applicationOriginName = GetApplicationOriginName();

            if (applicationOriginId.HasValue)
            {
                yield return new InboundEmailValue { Key = InboundEmailValueType.ApplicationOriginId, ValueLong = applicationOriginId };
            }

            if (!string.IsNullOrEmpty(applicationOriginName))
            {
                yield return new InboundEmailValue { Key = InboundEmailValueType.ApplicationOriginName, ValueString = applicationOriginName };
            }
        }

        public override bool CanParse(InboundEmail inboundEmail)
        {
            _applicationOrigin = GetApplicationOrigin(inboundEmail);

            return _applicationOrigin != default(ApplicationOrigin);
        }

        protected virtual long? GetApplicationOriginId()
        {
            return _applicationOrigin.Id;
        }

        protected virtual string GetApplicationOriginName()
        {
            return _applicationOrigin.Name;
        }
    }
}