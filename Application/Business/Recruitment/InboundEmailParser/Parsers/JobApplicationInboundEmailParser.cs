﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.InboundEmailParser
{
    [Identifier("Recruitment.JobApplicationEmailParser")]
    internal class JobApplicationInboundEmailParser : InboundEmailParserByApplicationOrigin
    {
        private readonly ApplicationOriginType[] _parserOriginTypes = new[]
        {
            ApplicationOriginType.Agency,
            ApplicationOriginType.DropOff,
            ApplicationOriginType.JobAdvertisement,
            ApplicationOriginType.Event,
            ApplicationOriginType.Other,
            ApplicationOriginType.SocialNetwork
        };

        public JobApplicationInboundEmailParser(IUnitOfWork<IRecruitmentDbScope> unitOfWork) : base(unitOfWork, InboundEmailCategory.JobApplication)
        {
        }

        protected override ApplicationOriginType[] ParserOriginTypes => _parserOriginTypes;
    }
}
