﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.InboundEmailParser
{
    [Identifier("Recruitment.RecommendationInboundEmailParser")]
    internal class RecommendationInboundEmailParser : InboundEmailParserByApplicationOrigin
    {
        private readonly ApplicationOriginType[] _parserOriginTypes = new[] { ApplicationOriginType.Recommendation };

        public RecommendationInboundEmailParser(IUnitOfWork<IRecruitmentDbScope> unitOfWork) : base(unitOfWork, InboundEmailCategory.Recommendation)
        {
        }

        protected override ApplicationOriginType[] ParserOriginTypes => _parserOriginTypes;
    }
}
