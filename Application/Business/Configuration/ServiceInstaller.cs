﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Configuration.Services;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.Configuration
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            switch (containerType)
            {
                case ContainerType.WebApi:
                case ContainerType.WebApp:
                case ContainerType.WebServices:
                    container.Register(Component.For<IRazorTemplateService>().ImplementedBy<RazorTemplateService>().LifestyleTransient());
                    container.Register(Component.For<IMessageTemplateService>().ImplementedBy<MessageTemplateService>().LifestyleTransient());
                    container.Register(Component.For<IDefaultParameterContextBuilder>().ImplementedBy<DefaultParameterContextBuilder>().LifestylePerWebRequest());
                    container.Register(Component.For<ISystemParameterService>().ImplementedBy<SystemParameterRetrievalService>().LifestyleTransient());
                    container.Register(Component.For<ISystemParameterCacheService>().ImplementedBy<SystemParameterCacheService>().LifestyleTransient());
                    container.Register(Component.For<ISystemSwitchService>().ImplementedBy<SystemSwitchService>().LifestyleTransient());
                    container.Register(Component.For<ISystemParameterCardIndexService>().ImplementedBy<SystemParameterCardIndexService>().LifestyleTransient());
                    container.Register(Component.For<IMessageTemplateCardIndexService>().ImplementedBy<MessageTemplateCardIndexService>().LifestyleTransient());
                    container.Register(Component.For<ICalendarCardIndexService>().ImplementedBy<CalendarCardIndexService>().LifestyleTransient());
                    container.Register(Component.For<ICalendarEntryCardIndexService>().ImplementedBy<CalendarEntryCardIndexService>().LifestyleTransient());
                    container.Register(Component.For<ICalendarDataService>().ImplementedBy<CalendarDataService>().LifestyleTransient());
                    container.Register(Component.For<ICalendarService>().ImplementedBy<CalendarService>().LifestyleTransient());
                    container.Register(Component.For<IAuditTableCardIndexDataService>().ImplementedBy<AuditTableCardIndexDataService>().LifestyleTransient());
                    container.Register(Component.For<IAuditService>().ImplementedBy<AuditService>().LifestyleTransient());
                    container.Register(Component.For<IDanteParameterContextBuilder>().ImplementedBy<DanteParameterContextBuilder>().LifestyleTransient());
                    break;

                case ContainerType.JobScheduler:
                    container.Register(Component.For<IRazorTemplateService>().ImplementedBy<RazorTemplateService>().LifestyleTransient());
                    container.Register(Component.For<IMessageTemplateService>().ImplementedBy<MessageTemplateService>().LifestyleTransient());
                    container.Register(Component.For<IDefaultParameterContextBuilder>().ImplementedBy<DefaultParameterContextBuilder>().LifestyleTransient());
                    container.Register(Component.For<ISystemParameterService>().ImplementedBy<SystemParameterRetrievalService>().LifestyleTransient());
                    container.Register(Component.For<ISystemParameterCacheService>().ImplementedBy<SystemParameterCacheService>().LifestyleTransient());
                    container.Register(Component.For<ISystemSwitchService>().ImplementedBy<SystemSwitchService>().LifestyleTransient());
                    container.Register(Component.For<ISystemParameterCardIndexService>().ImplementedBy<SystemParameterCardIndexService>().LifestyleTransient());
                    container.Register(Component.For<IMessageTemplateCardIndexService>().ImplementedBy<MessageTemplateCardIndexService>().LifestyleTransient());
                    container.Register(Component.For<ICalendarCardIndexService>().ImplementedBy<CalendarCardIndexService>().LifestyleTransient());
                    container.Register(Component.For<ICalendarEntryCardIndexService>().ImplementedBy<CalendarEntryCardIndexService>().LifestyleTransient());
                    container.Register(Component.For<ICalendarDataService>().ImplementedBy<CalendarDataService>().LifestyleTransient());
                    container.Register(Component.For<ICalendarService>().ImplementedBy<CalendarService>().LifestyleTransient());
                    container.Register(Component.For<IAuditTableCardIndexDataService>().ImplementedBy<AuditTableCardIndexDataService>().LifestyleTransient());
                    container.Register(Component.For<IAuditService>().ImplementedBy<AuditService>().LifestyleTransient());
                    container.Register(Component.For<IDanteParameterContextBuilder>().ImplementedBy<DanteParameterContextBuilder>().LifestyleTransient());
                    break;
            }
        }
    }
}

