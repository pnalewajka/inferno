﻿using System;
using System.Runtime.Serialization;
using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.Configuration.BusinessEvents
{
    [DataContract]
    public class CalendarEntryChangedBusinessEvent : BusinessEvent
    {
        [DataMember]
        public DateTime Day { get; set; }
    }
}