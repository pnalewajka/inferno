﻿using Smt.Atomic.Data.Entities.Modules.Configuration;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Configuration.Dto
{
    public class MessageTemplateToMessageTemplateDtoMapping : ClassMapping<MessageTemplate, MessageTemplateDto>
    {
        public MessageTemplateToMessageTemplateDtoMapping()
        {
            Mapping = e => new MessageTemplateDto
            {
                Code = e.Code,
                Description = e.Description,
                ContentType = e.ContentType,
                Content = e.Content,
                Id = e.Id,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
