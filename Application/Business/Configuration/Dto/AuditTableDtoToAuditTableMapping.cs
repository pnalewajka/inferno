﻿using Smt.Atomic.Data.Entities.Modules.Configuration;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Configuration.Dto
{
    public class AuditTableDtoToAuditTableMapping : ClassMapping<AuditTableDto, AuditTable>
    {
        public AuditTableDtoToAuditTableMapping()
        {
            Mapping = d => new AuditTable
            {
                Id = d.Id,
                TableName = d.TableName,
                Schema = d.Schema,
                IsAuditEnabled = d.IsAuditEnabled
            };
        }
    }
}