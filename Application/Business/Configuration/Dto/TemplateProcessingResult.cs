﻿namespace Smt.Atomic.Business.Configuration.Dto
{
    public class TemplateProcessingResult
    {
        public string Content { get; set; }

        public bool IsHtml { get; set; }
    }
}
