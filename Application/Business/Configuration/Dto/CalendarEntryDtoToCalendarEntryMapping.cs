﻿using Smt.Atomic.Data.Entities.Modules.Configuration;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Configuration.Dto
{
    public class CalendarEntryDtoToCalendarEntryMapping : ClassMapping<CalendarEntryDto, CalendarEntry>
    {
        public CalendarEntryDtoToCalendarEntryMapping()
        {
            Mapping = d => new CalendarEntry
            {
                Id = d.Id,
                EntryOn = d.EntryOn,
                Description = d.Description,
                CalendarEntryType = d.CalendarEntryType,
                WorkingHours = d.WorkingHours,
                Timestamp = d.Timestamp
            };
        }
    }
}
