﻿namespace Smt.Atomic.Business.Configuration.Dto
{
    public class HolidayDetails
    {
        public string Description { get; set; }

        public decimal WorkingHours { get; set; }
    }
}
