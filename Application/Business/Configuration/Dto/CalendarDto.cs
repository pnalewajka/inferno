﻿using System;

namespace Smt.Atomic.Business.Configuration.Dto
{
    public class CalendarDto
    {
        public long Id { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
