﻿using Smt.Atomic.Business.Configuration.Attributes;

namespace Smt.Atomic.Business.Configuration.Dto
{
    public partial class ParameterContext
    {
        [Alias("acronym")]
        public string Acronym { get; set; }

        [Alias("company")]
        public string CompanyCode { get; set; }

        [Alias("location")]
        public string LocationName { get; set; }

        [Alias("lineManager")]
        public string LineManagerLogin { get; set; }
    }
}
