﻿using Smt.Atomic.Data.Entities.Modules.Configuration;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Configuration.Dto
{
    public class AuditTableToAuditTableDtoMapping : ClassMapping<AuditTable, AuditTableDto>
    {
        public AuditTableToAuditTableDtoMapping()
        {
            Mapping = e => new AuditTableDto
            {
                Id = e.Id,
                TableName = e.TableName,
                Schema = e.Schema,
                IsAuditEnabled = e.IsAuditEnabled,
            };
        }
    }
}
