﻿namespace Smt.Atomic.Business.Configuration.Dto
{
    public class AuditTableDto
    {
        public long Id { get; set; }
        
        public string TableName { get; set; }

        public string Schema { get; set; }

        public bool IsAuditEnabled { get; set; }
    }
}
