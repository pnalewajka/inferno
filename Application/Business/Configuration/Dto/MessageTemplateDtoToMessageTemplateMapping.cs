﻿using Smt.Atomic.Data.Entities.Modules.Configuration;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Configuration.Dto
{
    public class MessageTemplateDtoToMessageTemplateMapping : ClassMapping<MessageTemplateDto, MessageTemplate>
    {
        public MessageTemplateDtoToMessageTemplateMapping()
        {
            Mapping = d => new MessageTemplate
            {
                Code = d.Code,
                Description = d.Description,
                ContentType = d.ContentType,
                Content = d.Content,
                Id = d.Id,
                Timestamp = d.Timestamp
            };
        }
    }
}
