﻿using Smt.Atomic.Data.Entities.Modules.Configuration;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Configuration.Dto
{
    public class CalendarDtoToCalendarMapping : ClassMapping<CalendarDto, Calendar>
    {
        public CalendarDtoToCalendarMapping()
        {
            Mapping = d => new Calendar
            {
                Id = d.Id,
                Code = d.Code,
                Description = d.Description,
                Timestamp = d.Timestamp
            };
        }
    }
}
