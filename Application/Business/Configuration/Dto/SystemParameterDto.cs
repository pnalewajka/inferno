﻿using System;
namespace Smt.Atomic.Business.Configuration.Dto
{
    public class SystemParameterDto
    {
        public long Id { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }

        public string ValueType { get; set; }

        public string ContextType { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
