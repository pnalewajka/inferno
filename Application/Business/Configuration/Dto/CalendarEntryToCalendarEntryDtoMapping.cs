﻿using Smt.Atomic.Data.Entities.Modules.Configuration;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Configuration.Dto
{
    public class CalendarEntryToCalendarEntryDtoMapping : ClassMapping<CalendarEntry, CalendarEntryDto>
    {
        public CalendarEntryToCalendarEntryDtoMapping()
        {
            Mapping = e => new CalendarEntryDto
            {
                Id = e.Id,
                EntryOn = e.EntryOn,
                Description = e.Description,
                CalendarEntryType = e.CalendarEntryType,
                WorkingHours = e.WorkingHours,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
