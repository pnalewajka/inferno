﻿using System;
using Newtonsoft.Json;
using Smt.Atomic.Business.Configuration.Attributes;
using Smt.Atomic.Business.Configuration.Services;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.Business.Configuration.Dto
{
    [JsonObject]
    [Identifier(SystemParameterRetrievalService.DefaultParameterContextTypeId)]
    // ReSharper disable once PartialTypeWithSinglePart
    public partial class ParameterContext
    {
        [Alias("user")]
        public ParameterContextUser CurrentUserContext { get; set; }

        [Alias("machine")]
        public string MachineName => Environment.MachineName;

        [Alias("now")]
        public DateTime QueriedOn { get; private set; }

        public ParameterContext(DateTime currentTime)
        {
            // For invalidating cache only when MINUTE changes (not millisecond)
            // If not, big contexts will kill the RAM memory
            QueriedOn = new DateTime(currentTime.Year, currentTime.Month, currentTime.Day, currentTime.Hour, currentTime.Minute, 0);

            CurrentUserContext = new ParameterContextUser(null);
        }
    }
}
