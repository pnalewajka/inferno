﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Business.Configuration.Dto
{
    public class Holidays
    {
        private readonly IDictionary<DateTime, HolidayDetails> _holidays;

        public long CalendarId { get; set; }

        public Holidays(IDictionary<DateTime, HolidayDetails> holidays)
        {
            _holidays = holidays;
        }

        public string GetHolidayDescriptionByDate(DateTime date)
        {
            HolidayDetails details;

            return _holidays.TryGetValue(date, out details) ? details.Description : null;
        }

        public decimal GetHolidayWorkingHoursByDate(DateTime date)
        {
            HolidayDetails details;

            if (_holidays.TryGetValue(date, out details))
            {
                return details.WorkingHours;
            }
            
            throw new ArgumentOutOfRangeException(nameof(date));
        }

        public bool IsHoliday(DateTime date)
        {
            return _holidays.ContainsKey(date);
        }

        public ISet<DateTime> GetHolidays()
        {
            return _holidays.Select(h => h.Key).ToHashSet();
        }

        public ISet<DateTime> GetHolidays(DateTime from, DateTime to)
        {
            return _holidays.Where(h => from <= h.Key && h.Key <= to).Select(h => h.Key).ToHashSet();
        }
    }
}
