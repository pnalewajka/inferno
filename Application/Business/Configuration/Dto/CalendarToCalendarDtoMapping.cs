﻿using Smt.Atomic.Data.Entities.Modules.Configuration;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Configuration.Dto
{
    public class CalendarToCalendarDtoMapping : ClassMapping<Calendar, CalendarDto>
    {
        public CalendarToCalendarDtoMapping()
        {
            Mapping = e => new CalendarDto
            {
                Id = e.Id,
                Code = e.Code,
                Description = e.Description,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
