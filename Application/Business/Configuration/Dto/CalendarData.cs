﻿using System;
using System.Collections.Generic;

namespace Smt.Atomic.Business.Configuration.Dto
{
    public class CalendarData
    {
        public HashSet<DayOfWeek> DefaultDaysOff { get; set; }

        public Holidays KnownHolidays { get; set; }
    }
}