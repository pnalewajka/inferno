﻿using Smt.Atomic.Data.Entities.Modules.Configuration;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Configuration.Dto
{
    public class SystemParameterDtoToSystemParameterMapping : ClassMapping<SystemParameterDto, SystemParameter>
    {
        public SystemParameterDtoToSystemParameterMapping()
        {
            Mapping = d => new SystemParameter
            {
                Id = d.Id,
                Key = d.Key,
                Value = d.Value,
                ValueType = d.ValueType,
                ContextType = d.ContextType,
                Timestamp = d.Timestamp
            };
        }
    }
}
