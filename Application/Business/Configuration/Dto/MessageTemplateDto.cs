﻿using System;
using Smt.Atomic.CrossCutting.Common.Enums;
namespace Smt.Atomic.Business.Configuration.Dto
{
    public class MessageTemplateDto
    {
        public long Id { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public MessageTemplateContentType ContentType { get; set; }

        public string Content { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
