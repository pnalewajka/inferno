﻿using Smt.Atomic.Data.Entities.Modules.Configuration;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Configuration.Dto
{
    public class SystemParameterToSystemParameterDtoMapping : ClassMapping<SystemParameter, SystemParameterDto>
    {
        public SystemParameterToSystemParameterDtoMapping()
        {
            Mapping = e => new SystemParameterDto
            {
                Id = e.Id,
                Key = e.Key,
                Value = e.Value,
                ValueType = e.ValueType,
                ContextType = e.ContextType,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
