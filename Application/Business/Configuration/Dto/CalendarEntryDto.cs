﻿using System;
using Smt.Atomic.CrossCutting.Common.Enums;
namespace Smt.Atomic.Business.Configuration.Dto
{
    public class CalendarEntryDto
    {
        public long Id { get; set; }

        public DateTime EntryOn { get; set; }

        public string Description { get; set; }

        public CalendarEntryType CalendarEntryType { get; set; }

        public decimal WorkingHours { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
