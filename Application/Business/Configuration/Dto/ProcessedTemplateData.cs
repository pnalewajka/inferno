﻿namespace Smt.Atomic.Business.Configuration.Dto
{
    public class ProcessedTemplateData
    {
        public string Content { get; set; }

        public bool IsHtml { get; set; }
    }
}
