﻿using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.Business.Configuration.Dto
{
    [JsonObject]
    public class ParameterContextUser
    {
        public string Login { get; private set; }

        public bool IsAutenticated { get; private set; }

        public ParameterContextUser(IAtomicPrincipal principal)
        {
            if (principal == null)
            {
                return;
            }

            IsAutenticated = principal.IsAuthenticated;

            if (IsAutenticated)
            {
                Login = principal.Login;
            }
        }
    }
}