﻿using System;

namespace Smt.Atomic.Business.Configuration.Services
{
    /// <summary>
    /// Used as a base class for on-demand compiled parameter evaluation types
    /// </summary>
    public abstract class ParameterEvaluation
    {
        /// <summary>
        /// Overwritten in on-demand compiled classes. 
        /// Returns parameter value most suited for given parameter context.
        /// </summary>
        /// <param name="context">Parameter context to evaluate parameter value for.</param>
        /// <returns>Parameter value based on the condition matched with the context</returns>
        public abstract object GetValue(object context);

        protected DateTime D(int year, int month, int day)
        {
            return new DateTime(year, month, day).Date;
        }
    }
}