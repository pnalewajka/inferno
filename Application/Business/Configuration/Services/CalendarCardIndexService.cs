using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Configuration.Resources;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Configuration;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Configuration.Services
{
    public class CalendarCardIndexService : CardIndexDataService<CalendarDto, Calendar, IConfigurationDbScope>, ICalendarCardIndexService
    {
        private readonly IUnitOfWorkService<IAllocationDbScope> _unitOfWorkService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public CalendarCardIndexService(ICardIndexServiceDependencies<IConfigurationDbScope> dependencies, IUnitOfWorkService<IAllocationDbScope> unitOfWorkService)
            : base(dependencies)
        {
            _unitOfWorkService = unitOfWorkService;
            RelatedCacheAreas = new[] {CacheAreas.Calendars};
        }

        protected override IEnumerable<Expression<Func<Calendar, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return t => t.Code;
            yield return t => t.Description;
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<Calendar, IConfigurationDbScope> eventArgs)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                if (unitOfWork.Repositories.Projects.Any(p => p.ProjectSetup.CalendarId == eventArgs.DeletingRecord.Id))
                {
                    eventArgs.Canceled = true;
                    eventArgs.Alerts.Add(new AlertDto
                    {
                        IsDismissable = true,
                        Message = CalendarCardIndexServiceResources.CannotDeleteUsedCalendar,
                        Type = AlertType.Error
                    });
                }
            }
        }
    }
}