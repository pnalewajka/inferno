﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RazorEngine.Configuration;
using RazorEngine.Templating;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.Business.Configuration.Services
{
    internal class RazorTemplateService : IRazorTemplateService
    {
        private const string ModelDirective = "@model ";
        private const string UsingDirective = "@using ";
        private const string TypeNameCodeSuffix = "ModelTypeName";

        private static Lazy<IRazorEngineService> CachedRazorEngineService = new Lazy<IRazorEngineService>(() => CreateDefaultRazorEngine());

        private readonly IClassMappingFactory _classMappingFactory;

        public RazorTemplateService(IClassMappingFactory classMappingFactory)
        {
            _classMappingFactory = classMappingFactory;
        }

        public Type GetTemplateModelType(string content, string templateCode)
        {
            var lines = content.Split('\r', '\n').Select(l => l.Trim(' ', '\t')).ToList();

            return TryFindModelTypeInLoadedAssemblies(lines) ??
                   TryGetTemplateModelTypeUsingRazor(lines, templateCode) ??
                   typeof(object);
        }

        public string TemplateRunAndCompile(string templateCode, string content, object messageModel, object viewBag)
        {
            var resolvedMessageModel = ResolveMessageModel(content, templateCode, messageModel);

            var actualViewBag = viewBag == null ? null : new DynamicViewBag(DictionaryHelper.ToDictionary(viewBag, k => k, v => v));

            try
            {
                return CachedRazorEngineService.Value.RunCompile(content, templateCode, null, resolvedMessageModel, actualViewBag);
            }
            catch (InvalidOperationException ex) when (ex.Message == "The same key was already used for another template!")
            {
                //retry once if the template content changed
                CachedRazorEngineService = new Lazy<IRazorEngineService>(() => CreateDefaultRazorEngine());

                return CachedRazorEngineService.Value.RunCompile(content, templateCode, null, resolvedMessageModel, actualViewBag);
            }
        }

        public IEnumerable<string> Validate(string templateCode, string content)
        {
            var issuesFound = Enumerable.Empty<string>();

            try
            {
                using (var razorEngineService = CreateDefaultRazorEngine())
                {
                    razorEngineService.Compile(content, templateCode);
                }
            }
            catch (TemplateCompilationException templateCompilationException)
            {
                issuesFound = templateCompilationException
                    .CompilerErrors
                    .Select(e => e.ErrorText)
                    .AsEnumerable();
            }

            return issuesFound;
        }

        private static IRazorEngineService CreateDefaultRazorEngine()
        {
            var configuration = new TemplateServiceConfiguration { CachingProvider = new DefaultCachingProvider(t => { }) };

            return RazorEngineService.Create(configuration);
        }

        private object ResolveMessageModel(string content, string templateCode, object messageModel)
        {
            if (messageModel == null)
            {
                return null;
            }

            var requiredType = GetTemplateModelType(content, templateCode);

            return ConvertToRequiredType(requiredType, messageModel);
        }

        private Type TryFindModelTypeInLoadedAssemblies(IEnumerable<string> lines)
        {
            var modelDirectiveLine = lines.SingleOrDefault(line => line.StartsWith(ModelDirective));

            if (modelDirectiveLine == null)
            {
                return null;
            }

            var typeName = modelDirectiveLine.Substring(ModelDirective.Length).Trim();

            return TypeHelper.FindTypeInLoadedAssemblies(typeName);
        }

        private Type TryGetTemplateModelTypeUsingRazor(IEnumerable<string> lines, string templateCode)
        {
            var stringBuilder = new StringBuilder();

            foreach (var line in lines)
            {
                if (line.StartsWith(UsingDirective))
                {
                    stringBuilder.AppendLine(line);
                }
                else if (line.StartsWith(ModelDirective))
                {
                    var typeName = line.Substring(ModelDirective.Length).Trim();
                    stringBuilder.AppendFormat("@typeof({0}).AssemblyQualifiedName", typeName);
                    var content = stringBuilder.ToString();

                    var razorEngineService = CachedRazorEngineService.Value;
                    var assemblyQualifiedName = razorEngineService.RunCompile(content, templateCode + TypeNameCodeSuffix);

                    return Type.GetType(assemblyQualifiedName, true);
                }
            }

            return null;
        }

        private object ConvertToRequiredType(Type requiredType, object model)
        {
            var modelType = model.GetType();

            if (requiredType.IsAssignableFrom(modelType))
            {
                return model;
            }

            var classMapping = _classMappingFactory.CreateMapping(modelType, requiredType);

            return classMapping.CreateFromSource(model);
        }
    }
}