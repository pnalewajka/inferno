using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Configuration.Consts;
using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.Data.Entities.Modules.Configuration;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Configuration.Services
{
    public class SystemParameterCardIndexService : CardIndexDataService<SystemParameterDto, SystemParameter, IConfigurationDbScope>, ISystemParameterCardIndexService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public SystemParameterCardIndexService(ICardIndexServiceDependencies<IConfigurationDbScope> dependencies)
            : base(dependencies)
        {
            RelatedCacheAreas = new[] {CacheAreas.SystemParameters};
        }

        protected override IEnumerable<Expression<Func<SystemParameter, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            if (searchCriteria.Includes(SearchAreaCodes.Name))
            {
                yield return x => x.Key;
            }

            if (searchCriteria.Includes(SearchAreaCodes.Value))
            {
                yield return x => x.Value;
                yield return x => x.ValueType;
            }

            if (searchCriteria.Includes(SearchAreaCodes.ContextType))
            {
                yield return x => x.ContextType;
            }

            yield return u => u.Key;
        }

        protected override IOrderedQueryable<SystemParameter> GetDefaultOrderBy(IQueryable<SystemParameter> records, QueryCriteria criteria)
        {
            return records.OrderBy(p => p.Key);
        }

        public override AddRecordResult AddRecord(SystemParameterDto record, object context = null)
        {
            throw new NotSupportedException("Adding system parameters in runtime is not supported.");
        }

        public override DeleteRecordsResult DeleteRecords(IEnumerable<long> idsToDelete, object context = null, bool isDeletePermanent = false)
        {
            throw new NotSupportedException("Removing system parameters in runtime is not supported.");
        }
    }
}