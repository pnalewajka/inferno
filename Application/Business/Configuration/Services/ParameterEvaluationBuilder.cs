﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Smt.Atomic.Business.Configuration.Attributes;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Business.Configuration.Services
{
    /// <summary>
    /// Creates an on-demand compiled paramater evaluation, with conditions and values based on the 
    /// string representation of the paramterer value
    /// </summary>
    public class ParameterEvaluationBuilder
    {
        private const string TemplateIndent = "            ";
        private const string ParameterSeparator = "=>";

        private static readonly string[] DefaultNamespaces = new[]
            {
                "System.Collections.Generic",
                "System.Linq",
                "System.Text",
                "Smt.Atomic.CrossCutting.Common.Helpers"
            };

        private readonly string _parameterValue;

        private readonly Type _contextType;
        private readonly Type _valueType;

        private ParameterEvaluation _evaluation;

        /// <summary>
        /// Creates instance of a parameter evaluation based on the parameter value and class identifiers for context and value
        /// </summary>
        /// <param name="parameterValue">String representation of the parameter value, optionally with conditions</param>
        /// <param name="valueTypeIdentifier">Parameter value class identifier</param>
        /// <param name="contextTypeIdentifier">Parameter context class identifier</param>
        public ParameterEvaluationBuilder(string parameterValue, string valueTypeIdentifier, string contextTypeIdentifier)
        {
            _parameterValue = parameterValue;

            _valueType = !valueTypeIdentifier.IsNullOrEmpty()
                ? IdentifierHelper.GetTypeByIdentifier(valueTypeIdentifier)
                : null;
            _contextType = IdentifierHelper.GetTypeByIdentifier(contextTypeIdentifier);
        }

        /// <summary>
        /// Creates instance of a parameter evaluation based on the parameter value and types for context and value
        /// </summary>
        /// <param name="parameterValue">String representation of the parameter value, optionally with conditions</param>
        /// <param name="valueType">Parameter value type</param>
        /// <param name="contextType">Parameter context type. When null, a default context type is used</param>
        public ParameterEvaluationBuilder(string parameterValue, Type valueType, Type contextType = null)
        {
            _parameterValue = parameterValue;

            _valueType = valueType;
            _contextType = contextType ?? IdentifierHelper.GetTypeByIdentifier(SystemParameterRetrievalService.DefaultParameterContextTypeId);
        }

        private bool TryParseAndConvert(out object result)
        {
            if (_valueType == typeof(int) && int.TryParse(_parameterValue, NumberStyles.Number, CultureInfo.InvariantCulture, out var intResult))
            {
                result = intResult;

                return true;
            }

            if (_valueType == typeof(long) && long.TryParse(_parameterValue, NumberStyles.Number, CultureInfo.InvariantCulture, out var longResult))
            {
                result = longResult;

                return true;
            }

            if (_valueType == typeof(decimal) && decimal.TryParse(_parameterValue, NumberStyles.Any, CultureInfo.InvariantCulture, out var decimalResult))
            {
                result = decimalResult;

                return true;
            }

            if (_valueType == typeof(string) && !_parameterValue.Contains(ParameterSeparator))
            {
                result = _parameterValue;

                return true;
            }

            result = null;

            return false;
        }

        /// <summary>
        /// Returns parameter value suitable for given context
        /// </summary>
        /// <param name="context">Parameter context describing situation for which the parameter should be evaulated</param>
        /// <returns>Parameter value evaluated for specified context</returns>
        public object GetValue(object context)
        {
            if (TryParseAndConvert(out var result))
            {
                return result;
            }

            var evaluation = GetContextEvaluation();

            return evaluation.GetValue(context);
        }

        public bool IsValid
        {
            get
            {
                try
                {
                    GetContextEvaluation();
                }
                catch
                {
                    return false;
                }

                return true;
            }
        }

        private ParameterEvaluation GetContextEvaluation()
        {
            if (_evaluation != null)
            {
                return _evaluation;
            }

            _evaluation = GetContextEvaluation(_parameterValue, _contextType, _valueType);
            return _evaluation;
        }

        private static ParameterEvaluation GetContextEvaluation(string parameterValue, Type contextType, Type valueType)
        {
            var referencedAssemblies = CompilationHelper.GetLoadedReferences();

            var sourceCode = GetContextSourceCode(parameterValue, contextType, valueType);
            var evaluationAssembly = CompilationHelper.Compile(sourceCode, referencedAssemblies);

            var evaluationType = evaluationAssembly
                .GetTypes()
                .Single(t => typeof(ParameterEvaluation).IsAssignableFrom(t));

            var evaluation = (ParameterEvaluation)Activator.CreateInstance(evaluationType);
            return evaluation;
        }

        private static Dictionary<string, string> GetAliases(Type contextType)
        {
            var properties = TypeHelper.GetPublicInstanceProperties(contextType);
            var aliases = properties
                .Select
                (
                    p => new
                    {
                        p.Name,
                        attribute = AttributeHelper.GetPropertyAttribute<AliasAttribute>(p)
                    }
                )
                .ToDictionary
                (
                    x => x.Name,
                    x => x.attribute == null
                        ? NamingConventionHelper.ConvertPascalCaseToCamelCase(x.Name)
                        : x.attribute.Alias
                );

            return aliases;
        }

        private static Dictionary<string, string> GetValueConditions(string parameterValue, Type valueType)
        {
            const string invalidParameterRepresentation = @"Invalid parameter representation";

            var lines = parameterValue.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

            if (lines.Length == 0)
            {
                throw new ArgumentException(invalidParameterRepresentation, nameof(parameterValue));
            }

            if (lines.Length == 1)
            {
                return new Dictionary<string, string>
                    {
                        { string.Empty, FormatReturnedValue(lines[0], valueType) }
                    };
            }

            var pairs = lines
                .Select(s => s.Split(new[] { ParameterSeparator }, 2, StringSplitOptions.RemoveEmptyEntries))
                .ToList();

            if (pairs.Any(a => a.Length < 2))
            {
                throw new ArgumentException(invalidParameterRepresentation, nameof(parameterValue));
            }

            var result = pairs
                .ToDictionary
                (
                    a => StripBrackets(a[0]),
                    a => FormatReturnedValue(a[1].Trim(), valueType)
                );

            return result;
        }

        private static string StripBrackets(string condition)
        {
            return condition.Trim().TrimStart('[').TrimEnd(']').Trim();
        }

        private static string GetContextSourceCode(string parameterValue, Type contextType, Type valueType)
        {
            var template = new StringBuilder(ResourceHelper.GetResourceAsStringByShortName("ParameterEvaluationTemplate.cs", typeof(ParameterEvaluationBuilder)));

            ReplaceUsings(template, contextType, valueType);
            ReplaceTypeDeclaration(template, contextType);
            ReplaceAliases(template, contextType);
            ReplaceConditions(template, parameterValue, valueType);

            return template.ToString();
        }

        private static void ReplaceUsings(StringBuilder template, Type contextType, Type valueType)
        {
            var namespaces = DefaultNamespaces
                .Union(new [] { "System" })
                .Union(TypeHelper.GetNamespaces(contextType))
                .Union(valueType != null ? TypeHelper.GetNamespaces(valueType) : Enumerable.Empty<string>())
                .Distinct()
                .ToList();

            var usingInsert = new StringBuilder();

            foreach (var name in namespaces)
            {
                usingInsert.AppendLine($"using {name};");
            }

            template.Replace("/* inject-usings */", usingInsert.ToString());
        }

        private static void ReplaceTypeDeclaration(StringBuilder template, Type contextType)
        {
            var typeName = TypeHelper.GetCSharpName(contextType);
            var inheritedTypeName = NamingConventionHelper.ConvertToClassName(typeName + "CompiledEvaluation");

            template.Replace("ParameterEvaluationTemplate", inheritedTypeName);
            template.Replace("(ParameterContext)", $"({typeName})");
        }

        private static void ReplaceAliases(StringBuilder template, Type contextType)
        {
            var aliases = GetAliases(contextType);
            var aliasInsert = new StringBuilder();

            foreach (var alias in aliases)
            {
                aliasInsert.AppendLine(TemplateIndent + "var " + alias.Value + " = context." + alias.Key + ";");
            }

            template.Replace(TemplateIndent + "/* inject-aliases */", aliasInsert.ToString());
        }

        private static void ReplaceConditions(StringBuilder template, string parameterValue, Type valueType)
        {
            var conditions = GetValueConditions(parameterValue, valueType);

            var conditionInsert = new StringBuilder();

            if (valueType != null)
            {
                var typeName = TypeHelper.GetCSharpName(valueType);
                conditionInsert.AppendLine(TemplateIndent + "var _parameterValueType = typeof(" + typeName + ");");
            }
            else
            {
                conditionInsert.AppendLine(TemplateIndent + "Type _parameterValueType = null;");
            }

            conditionInsert.AppendLine();

            foreach (var condition in conditions.Reverse())
            {
                if (condition.Key == string.Empty)
                {
                    var code = TemplateIndent + $"return {condition.Value};";
                    conditionInsert.AppendLine(code);
                }
                else
                {
                    var code = TemplateIndent + $"if ({condition.Key}) {{ return {condition.Value}; }}";
                    conditionInsert.AppendLine(code);
                }
            }

            template.Replace(TemplateIndent + "/* inject-conditions */", conditionInsert.ToString());
        }

        private static string FormatReturnedValue(string value, Type valueType)
        {
            if (value == null)
            {
                return "null";
            }

            if (valueType == typeof(string))
            {
                return $"\"{EscapeQuotes(value)}\"";
            }

            if (JsonHelper.IsJson(value))
            {
                return valueType != null
                    ? $"JsonHelper.Deserialize(\"{EscapeQuotes(value)}\", _parameterValueType)"
                    : $"JsonHelper.Deserialize(\"{EscapeQuotes(value)}\")";
            }

            return value;
        }

        private static string EscapeQuotes(string value)
        {
            return value.Replace("\"", "\\\"");
        }
    }
}