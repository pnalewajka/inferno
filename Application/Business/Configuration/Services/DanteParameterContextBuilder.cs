﻿using Smt.Atomic.Business.Configuration.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.CrossCutting.Common.Caching;
using Castle.Core;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.Business.Configuration.Services
{
    [Interceptor(typeof(CacheInterceptor))]
    class DanteParameterContextBuilder : IDanteParameterContextBuilder
    {
        private readonly IUnitOfWorkService<IAllocationDbScope> _allocationUnitOfWorkService;
        private readonly IDbTimeService _dbTimeService;

        public DanteParameterContextBuilder(IUnitOfWorkService<IAllocationDbScope> allocationUnitOfWorkService, IDbTimeService dbTimeService)
        {
            _allocationUnitOfWorkService = allocationUnitOfWorkService;
            _dbTimeService = dbTimeService;
        }

        [Cached(CacheArea = CacheAreas.SystemParameters, ExpirationInSeconds = 5)]
        public ParameterContext FillCustomParameterContext(long employeeId)
        {
            using (var unitOfWork = _allocationUnitOfWorkService.Create())
            {
                var employee = unitOfWork.Repositories.Employees.GetByIdOrDefault(employeeId);
                var context = new ParameterContext(_dbTimeService.GetCurrentTime());

                if (employee != null)
                {
                    context.Acronym = employee.Acronym;
                    context.CompanyCode = employee.Company?.ActiveDirectoryCode;
                    context.LocationName = employee.Location?.Name;
                    context.LineManagerLogin = employee.LineManager?.User?.Login;
                }

                return context;
            }
        }
    }
}
