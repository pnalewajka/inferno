using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Configuration;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Configuration.Services
{
    public class MessageTemplateCardIndexService : CardIndexDataService<MessageTemplateDto, MessageTemplate, IConfigurationDbScope>, IMessageTemplateCardIndexService
    {
        private readonly IRazorTemplateService _razorTemplateService;
        private readonly IUnitOfWorkService<IConfigurationDbScope> _unitOfWorkService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public MessageTemplateCardIndexService(
            ICardIndexServiceDependencies<IConfigurationDbScope> dependencies,
            IRazorTemplateService razorTemplateService,
            IUnitOfWorkService<IConfigurationDbScope> unitOfWorkService)
            : base(dependencies)
        {
            _razorTemplateService = razorTemplateService;
            _unitOfWorkService = unitOfWorkService;
        }

        public void CreateOrUpdateTemplate(string templateCode, string content, string description, MessageTemplateContentType contentType)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var messageTemplate = unitOfWork.Repositories.MessageTemplates.SingleOrDefault(t => t.Code == templateCode);

                if (messageTemplate == null)
                {
                    messageTemplate = unitOfWork.Repositories.MessageTemplates.CreateEntity();
                    unitOfWork.Repositories.MessageTemplates.Add(messageTemplate);
                }
                else
                {
                    unitOfWork.Repositories.MessageTemplates.Edit(messageTemplate);
                }

                messageTemplate.Code = templateCode;
                messageTemplate.Content = content;
                messageTemplate.Description = description;
                messageTemplate.ContentType = contentType;

                unitOfWork.Commit();
            }
        }

        public byte[] GetZipWithHtmls(IList<long> ids)
        {
           using (var unitOfWork = _unitOfWorkService.Create())
            {
                var records = unitOfWork.Repositories.MessageTemplates.Where(record => ids.Contains(record.Id));

                using (var memoryStream = new MemoryStream())
                {
                    using (var archive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                    {
                        foreach (var record in records)
                        {
                            var zipEntry = archive.CreateEntry(record.Code + ".html");
                            using (var entryStream = zipEntry.Open())
                            using (var streamWriter = new StreamWriter(entryStream))
                            {
                                streamWriter.Write(record.Content);
                            }
                        }
                    }
                    return memoryStream.GetBuffer();
                }
            }
        }
        
        protected override IEnumerable<Expression<Func<MessageTemplate, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return t => t.Code;
            yield return t => t.Description;
            yield return t => t.Content;
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<MessageTemplate, MessageTemplateDto, IConfigurationDbScope> eventArgs)
        {
            if (!ValidateTemplate(eventArgs, eventArgs.InputDto.Code, eventArgs.InputDto.Content))
            {
                return;
            }

            base.OnRecordAdding(eventArgs);
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<MessageTemplate, MessageTemplateDto, IConfigurationDbScope> eventArgs)
        {
            if (!ValidateTemplate(eventArgs, eventArgs.InputDto.Code, eventArgs.InputDto.Content))
            {
                return;
            }

            base.OnRecordEditing(eventArgs);
        }

        private bool ValidateTemplate(CardIndexEventArgs eventArgs, string code, string content)
        {
            var templateIssues = _razorTemplateService.Validate(code, content).ToList();

            if (templateIssues.Any())
            {
                foreach (var templateIssue in templateIssues)
                {
                    eventArgs.AddError(templateIssue);
                }

                return false;
            }

            return true;
        }
    }
}