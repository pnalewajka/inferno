﻿using System.Data.SqlClient;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Configuration.Services
{
    public class AuditService : IAuditService
    {
        private readonly IUnitOfWorkService<IConfigurationDbScope> _unitOfWorkService;

        public AuditService(IUnitOfWorkService<IConfigurationDbScope> unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        public bool IsAuditEnabled()
        {
            using (var unitOfWork = _unitOfWorkService.CreateExtended())
            {
                const string sqlReleaseCommand = @"EXEC [Audit].[atomic_IsAuditEnabled]";
                var result = unitOfWork.SqlQuery<bool>(sqlReleaseCommand).SingleOrDefault();

                return result;
            }
        }

        public void EnableAudit()
        {
            using (var unitOfWork = _unitOfWorkService.CreateExtended())
            {
                const string sqlReleaseCommand = @"EXEC [Audit].[atomic_EnableAudit]";

                unitOfWork.ExecuteSqlCommand(sqlReleaseCommand);
            }
        }

        public void DisableAudit(bool dropTables = false)
        {
            using (var unitOfWork = _unitOfWorkService.CreateExtended())
            {
                const string sqlReleaseCommand = @"EXEC [Audit].[atomic_DisableAudit] @dropAuditTables = @dropTables";

                unitOfWork.ExecuteSqlCommand(sqlReleaseCommand, new SqlParameter("@dropTables", dropTables));
            }
        }

        public void EnableAuditOnTables(long[] ids)
        {
            using (var unitOfWork = _unitOfWorkService.CreateExtended())
            {
                const string sqlReleaseCommand = @"EXEC [Audit].[atomic_EnableAuditOnTable] @id = @id";

                foreach (var i in ids)
                {
                    unitOfWork.ExecuteSqlCommand(sqlReleaseCommand, new SqlParameter("@id", i));
                }
            }
        }

        public void DisableAuditOnTables(long[] ids, bool dropTables = false)
        {
            using (var unitOfWork = _unitOfWorkService.CreateExtended())
            {
                const string sqlReleaseCommand = @"EXEC [Audit].[atomic_DisableAuditOnTable] @id = @id, @dropAuditTable = @dropTable";

                foreach (var i in ids)
                {
                    unitOfWork.ExecuteSqlCommand(sqlReleaseCommand, new SqlParameter("@id", i), new SqlParameter("@dropTable", dropTables));
                }
            }
        }
    }
}
