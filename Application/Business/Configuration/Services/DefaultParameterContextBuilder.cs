﻿using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Configuration.Services
{
    public class DefaultParameterContextBuilder : IDefaultParameterContextBuilder
    {
        private readonly IDbTimeService _dbTimeService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IDanteParameterContextBuilder _danteParameterContextBuilder;

        public DefaultParameterContextBuilder(
            IDbTimeService dbTimeService,
            IPrincipalProvider principalProvider,
            IDanteParameterContextBuilder customParameterContextBuilder)
        {
            _dbTimeService = dbTimeService;
            _principalProvider = principalProvider;
            _danteParameterContextBuilder = customParameterContextBuilder;
        }

        public ParameterContext Build()
        {
            var context = CreateParameterContext(_principalProvider.Current.EmployeeId);

            return context;
        }

        public ParameterContext BuildForEmployee(long employeeId)
        {
            var context = CreateParameterContext(employeeId);

            return context;
        }

        private ParameterContext CreateParameterContext(long employeeId)
        {
            var principal = _principalProvider.Current;

            var context = _danteParameterContextBuilder.FillCustomParameterContext(employeeId);
            context.CurrentUserContext = new ParameterContextUser(principal);

            return context;
        }
    }
}