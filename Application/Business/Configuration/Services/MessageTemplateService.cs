using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Common.Extensions;
using Smt.Atomic.Data.Entities.Modules.Configuration;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Configuration.Services
{
    internal class MessageTemplateService : IMessageTemplateService
    {
        private readonly IRazorTemplateService _razorTemplateService;
        private readonly IReadOnlyUnitOfWorkService<IConfigurationDbScope> _unitOfWorkService;

        public MessageTemplateService(
            IRazorTemplateService razorTemplateService,
            IReadOnlyUnitOfWorkService<IConfigurationDbScope> unitOfWorkService)
        {
            _razorTemplateService = razorTemplateService;
            _unitOfWorkService = unitOfWorkService;
        }

        public TemplateProcessingResult ResolveTemplate(string templateCode, object messageModel, object viewBag, bool useCurrentCulture = false)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var template =
                    unitOfWork
                    .Repositories
                    .MessageTemplates
                    .Single(t => t.Code == templateCode);

                return GetProcessedTemplateData(template, messageModel, viewBag, useCurrentCulture);
            }
        }

        public async Task<TemplateProcessingResult> ResolveTemplateAsync(string templateCode, object messageModel, object viewBag, bool useCurrentCulture = false)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var template = await unitOfWork
                    .Repositories
                    .MessageTemplates
                    .SingleAsync(t => t.Code == templateCode);

                return GetProcessedTemplateData(template, messageModel, viewBag, useCurrentCulture);
            }
        }

        private TemplateProcessingResult GetProcessedTemplateData(MessageTemplate template, object messageModel, object viewBag, bool useCurrentCulture = false)
        {
            var enCulture = CultureInfo.CreateSpecificCulture(CultureCodes.EnglishAmerican);

            var newCulture = useCurrentCulture ? Thread.CurrentThread.CurrentCulture : enCulture;
            var newUICulture = useCurrentCulture ? Thread.CurrentThread.CurrentUICulture : enCulture;

            using (CultureInfoHelper.SetCurrentCulture(newCulture))
            using (CultureInfoHelper.SetCurrentUICulture(newUICulture))
            {
                var content = _razorTemplateService.TemplateRunAndCompile(template.Code, template.Content, messageModel, viewBag);

                if (template.ContentType == MessageTemplateContentType.PlainText)
                {
                    content = HttpUtility.HtmlDecode(content);
                }

                return new TemplateProcessingResult
                {
                    Content = content,
                    IsHtml = template.ContentType == MessageTemplateContentType.Html
                };
            }
        }
    }
}
