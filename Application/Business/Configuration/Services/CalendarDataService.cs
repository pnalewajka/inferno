using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core;
using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.Data.Entities.Modules.Configuration;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Configuration.Services
{
    [Interceptor(typeof(CacheInterceptor))]
    internal class CalendarDataService : ICalendarDataService
    {
        private readonly IUnitOfWorkService<IConfigurationDbScope> _unitOfWorkService;

        public CalendarDataService(IUnitOfWorkService<IConfigurationDbScope> unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        [Cached(CacheArea = CacheAreas.Calendars)]
        public CalendarData GetCalendarDataById(long calendarId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var repository = unitOfWork.Repositories.Calendars;
                var calendar = repository.GetById(calendarId);

                return GetCalendarData(calendar);
            }
        }

        [Cached(CacheArea = CacheAreas.Calendars)]
        public CalendarData GetCalendarDataByCode(string calendarCode)
        {
            if (calendarCode == null)
            {
                throw new ArgumentNullException(nameof(calendarCode));
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var repository = unitOfWork.Repositories.Calendars;
                var calendar = repository.Single(c => c.Code == calendarCode);

                return GetCalendarData(calendar);
            }
        }

        private CalendarData GetCalendarData(Calendar calendar)
        {
            return new CalendarData
            {
                DefaultDaysOff = new HashSet<DayOfWeek>(calendar.GetDefaultDaysOff()),
                KnownHolidays = new Holidays(new Dictionary<DateTime, HolidayDetails>(calendar.Entries.ToDictionary(e => e.EntryOn, e => new HolidayDetails
                {
                    Description = e.Description,
                    WorkingHours = e.WorkingHours
                })))
                {
                    CalendarId = calendar.Id
                }
            };
        }
    }
}