﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Configuration.Services
{
    internal class CalendarService : ICalendarService
    {
        private readonly ICalendarDataService _calendarDataService;
        private readonly IUnitOfWorkService<IConfigurationDbScope> _unitOfWorkService;

        public CalendarService(ICalendarDataService calendarDataService, IUnitOfWorkService<IConfigurationDbScope> unitOfWorkService)
        {
            _calendarDataService = calendarDataService;
            _unitOfWorkService = unitOfWorkService;
        }

        public int GetWorkdayCount(DateTime startDate, DateTime endDate, [NotNull] string calendarCode)
        {
            if (calendarCode == null)
            {
                throw new ArgumentNullException(nameof(calendarCode));
            }

            var calendarData = _calendarDataService.GetCalendarDataByCode(calendarCode);

            return GetWorkdayCountInRange(startDate, endDate, calendarData);
        }

        public int GetWorkdayCount(DateTime startDate, DateTime endDate, long calendarId)
        {
            var calendarData = _calendarDataService.GetCalendarDataById(calendarId);

            return GetWorkdayCountInRange(startDate, endDate, calendarData);
        }

        public bool IsWorkday(DateTime date, [NotNull] string calendarCode)
        {
            if (calendarCode == null)
            {
                throw new ArgumentNullException(nameof(calendarCode));
            }

            var calendarData = _calendarDataService.GetCalendarDataByCode(calendarCode);

            return GetIsWorkday(date, calendarData);
        }

        public bool IsWorkday(DateTime date, long calendarId)
        {
            var calendarData = _calendarDataService.GetCalendarDataById(calendarId);

            return GetIsWorkday(date, calendarData);
        }

        public DateTime AddWorkdays(DateTime date, int days, [NotNull] string calendarCode)
        {
            if (calendarCode == null)
            {
                throw new ArgumentNullException(nameof(calendarCode));
            }

            if (days == 0)
            {
                return date;
            }

            var calendarData = _calendarDataService.GetCalendarDataByCode(calendarCode);

            return GetAddWorkdaysDate(date, days, calendarData);
        }

        public DateTime AddWorkdays(DateTime date, int days, long calendarId)
        {
            if (days == 0)
            {
                return date;
            }

            var calendarData = _calendarDataService.GetCalendarDataById(calendarId);

            return GetAddWorkdaysDate(date, days, calendarData);
        }

        public long? GetCalendarId(string calendarCode)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var calendar = unitOfWork.Repositories.Calendars.SingleOrDefault(c => c.Code == calendarCode);

                return calendar?.Id;
            }
        }

        public ICollection<DateTime> GetDefaultDaysOff(DateTime startDate, DateTime endDate, long calendarId)
        {
            var calendar = _calendarDataService.GetCalendarDataById(calendarId);

            return GetDefaultDaysOff(startDate, endDate, calendar);
        }

        public ICollection<DateTime> GetHolidays(DateTime startDate, DateTime endDate, long calendarId)
        {
            var calendar = _calendarDataService.GetCalendarDataById(calendarId);

            return GetHolidays(startDate, endDate, calendar);
        }

        private int GetWorkdayCountInRange(DateTime startDate, DateTime endDate, CalendarData calendar)
        {
            if (startDate > endDate)
            {
                throw new ArgumentOutOfRangeException();
            }

            var daysOff = GetDaysOff(startDate, endDate, calendar);

            var dayOffCount = daysOff.Count();
            var rangeDayCount = (int)(endDate.Date - startDate.Date).TotalDays + 1;

            return rangeDayCount - dayOffCount;
        }

        private ISet<DateTime> GetDaysOff(DateTime startDate, DateTime endDate, CalendarData calendar)
        {
            var daysOff = GetDefaultDaysOff(startDate, endDate, calendar);
            var holidays = GetHolidays(startDate, endDate, calendar);

            return daysOff.Concat(holidays).Where(d => d >= startDate.StartOfDay() && d <= endDate.EndOfDay()).ToHashSet();
        }

        private ICollection<DateTime> GetDefaultDaysOff(DateTime startDate, DateTime endDate, CalendarData calendar)
        {
            var daysOff = calendar.DefaultDaysOff.Select(d => startDate.StartOfDay().GetNextDayOfWeek(d)).ToList();

            if (daysOff.Count != 0)
            {
                PopulateDefaultDaysOff(daysOff, endDate.EndOfDay());
            }

            return daysOff;
        }

        public ICollection<DateTime> GetHolidays(DateTime startDate, DateTime endDate, CalendarData calendar)
        {
            return calendar.KnownHolidays.GetHolidays(startDate.StartOfDay(), endDate.EndOfDay());
        }

        private static void PopulateDefaultDaysOff(List<DateTime> daysOff, DateTime endDate)
        {
            while (true)
            {
                var index = daysOff.Count - daysOff.Count;

                for (var i = 0; i < daysOff.Count; i++)
                {
                    var date = daysOff[index + i].AddDays(7);

                    if (date > endDate)
                    {
                        return;
                    }

                    daysOff.Add(date);
                }
            }
        }

        private bool GetIsWorkday(DateTime date, CalendarData calendar)
        {
            date = date.Date;

            return calendar.DefaultDaysOff
                .All(d => date.DayOfWeek != d)
                   && !calendar.KnownHolidays.IsHoliday(date);
        }

        private DateTime GetAddWorkdaysDate(DateTime date, int days, CalendarData calendar)
        {
            var referenceDate = date.Date;
            var daysToAdd = Math.Abs(days);

            var referenceDays = Math.Max(daysToAdd * 2, 31);

            var startDate = days > 0 ? referenceDate : referenceDate.AddDays(-referenceDays);
            var endDate = days > 0 ? startDate.AddDays(referenceDays) : referenceDate;

            var daysOff = GetDaysOff(startDate, endDate, calendar);

            var workdaysAdded = 0;
            var dayDelta = days > 0 ? 1 : -1;

            while (workdaysAdded < daysToAdd)
            {
                date = date.AddDays(dayDelta);

                if (!daysOff.Contains(date.Date))
                {
                    workdaysAdded++;
                }
            }

            return date;
        }
    }
}