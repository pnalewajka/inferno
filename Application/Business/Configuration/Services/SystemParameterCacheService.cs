﻿using System.Linq;
using System.Threading.Tasks;
using Castle.Core;
using Castle.Core.Logging;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Common.Extensions;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Configuration.Services
{
    [Interceptor(typeof(CacheInterceptor))]
    internal class SystemParameterCacheService : ISystemParameterCacheService
    {
        private readonly ILogger _logger;
        private readonly IReadOnlyUnitOfWorkService<IConfigurationDbScope> _unitOfWorkService;

        public SystemParameterCacheService(ILogger logger, IReadOnlyUnitOfWorkService<IConfigurationDbScope> unitOfWorkService)
        {
            _logger = logger;
            _unitOfWorkService = unitOfWorkService;
        }

        [Cached(CacheArea = CacheAreas.SystemParameters)]
        public TValue GetParameter<TValue>(string parameterKey, object context)
        {
            var valueTypeId = IdentifierHelper.GetTypeIdentifier(typeof(TValue));

            var contextTypeId = IdentifierHelper.GetTypeIdentifier(context.GetType());
            var isContextDefault = contextTypeId == SystemParameterRetrievalService.DefaultParameterContextTypeId;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var parameter = unitOfWork
                    .Repositories
                    .SystemParameters
                    .SingleOrDefault(p => p.Key == parameterKey
                                          && p.ValueType == valueTypeId
                                          && (p.ContextType == contextTypeId
                                              || (isContextDefault && p.ContextType == null)));

                if (parameter == null)
                {
                    var systemParameterNotFoundException = new SystemParameterNotFoundException(parameterKey, valueTypeId);
                    _logger.FatalFormat(systemParameterNotFoundException, "SystemParameter {0} for key {1} wasn't found", valueTypeId, parameterKey);
                    throw systemParameterNotFoundException;
                }

                var evaluation = new ParameterEvaluationBuilder(parameter.Value, valueTypeId, contextTypeId);
                var value = evaluation.GetValue(context);

                return (TValue)value;
            }
        }

        [Cached(CacheArea = CacheAreas.SystemParameters)]
        public async Task<TValue> GetParameterAsync<TValue>(string parameterKey, object context)
        {
            var valueTypeId = IdentifierHelper.GetTypeIdentifier(typeof(TValue));

            var contextTypeId = IdentifierHelper.GetTypeIdentifier(context.GetType());
            var isContextDefault = contextTypeId == SystemParameterRetrievalService.DefaultParameterContextTypeId;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var parameter = await unitOfWork
                    .Repositories
                    .SystemParameters
                    .SingleOrDefaultAsync(p => p.Key == parameterKey
                                          && p.ValueType == valueTypeId
                                          && (p.ContextType == contextTypeId
                                              || (isContextDefault && p.ContextType == null)));

                if (parameter == null)
                {
                    var systemParameterNotFoundException = new SystemParameterNotFoundException(parameterKey, valueTypeId);
                    _logger.FatalFormat(systemParameterNotFoundException, "SystemParameter {0} for key {1] wasn't found", valueTypeId, parameterKey);
                    throw systemParameterNotFoundException;
                }

                var evaluation = new ParameterEvaluationBuilder(parameter.Value, valueTypeId, contextTypeId);
                var value = evaluation.GetValue(context);

                return (TValue)value;
            }
        }
    }
}
