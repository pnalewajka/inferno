using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Configuration.BusinessEvents;
using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Configuration.Resources;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Dto;
using Smt.Atomic.Data.Entities.Modules.Configuration;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Configuration.Services
{
    public class CalendarEntryCardIndexService : CardIndexDataService<CalendarEntryDto, CalendarEntry, IConfigurationDbScope>, ICalendarEntryCardIndexService
    {
        private readonly ITimeService _timeService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public CalendarEntryCardIndexService(ICardIndexServiceDependencies<IConfigurationDbScope> dependencies, ITimeService timeService) 
            : base(dependencies)
        {
            _timeService = timeService;
            RelatedCacheAreas = new[] {CacheAreas.Calendars};
        }

        protected override IEnumerable<BusinessEvent> GetAddRecordEvents(AddRecordResult result, CalendarEntryDto record)
        {
            var events = base.GetAddRecordEvents(result, record);

            if (result.IsSuccessful)
            {
                events = events.Concat(new[] { new CalendarEntryChangedBusinessEvent { Day = record.EntryOn } });
            }

            return events;
        }

        protected override IEnumerable<BusinessEvent> GetEditRecordEvents(EditRecordResult result, CalendarEntryDto newRecord, CalendarEntryDto originalRecord)
        {
            var events = base.GetEditRecordEvents(result, newRecord, originalRecord);

            if (result.IsSuccessful)
            {
                events = events.Concat(new[] { new CalendarEntryChangedBusinessEvent { Day = originalRecord.EntryOn }});

                if (newRecord.EntryOn.GetFirstDayInMonth() != originalRecord.EntryOn.GetFirstDayInMonth())
                {
                    events = events.Concat(new[] { new CalendarEntryChangedBusinessEvent { Day = newRecord.EntryOn } });
                }
            }

            return events;
        }

        protected override IEnumerable<BusinessEvent> GetDeleteRecordEvents(DeleteRecordsResult result, CalendarEntryDto record)
        {
            var events = base.GetDeleteRecordEvents(result, record);

            if (result.IsSuccessful)
            {
                events = events.Concat(new[] { new CalendarEntryChangedBusinessEvent { Day = record.EntryOn } });
            }

            return events;
        }

        public override IQueryable<CalendarEntry> ApplyContextFiltering(IQueryable<CalendarEntry> records, object context)
        {
            var parentChildContext = context as ParentIdContext;

            if (parentChildContext == null)
            {
                throw new InvalidCastException("Invalid context given for calendar entry query.");
            }

            return records.Where(j => j.CalendarId == parentChildContext.ParentId);            
        }

        public override CalendarEntryDto GetDefaultNewRecord()
        {
            var newRecord = base.GetDefaultNewRecord();

            var today = _timeService.GetCurrentDate();
            newRecord.EntryOn = today.GetFirstDayOfYear();

            return newRecord;
        }

        public IEnumerable<FilterYearDto> GetFilterYears()
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var currentYear = _timeService.GetCurrentDate().Year;
                var entityRepository = unitOfWork.Repositories.CalendarEntries;
                var years = entityRepository.Select(c => c.EntryOn.Year)
                                            .ToHashSet();

                return FiltersHelper.CreateFiltersYears(currentYear, years);
            }
        }

        public BusinessResult CopyEntries(int calendarId, int fromYear, int toYear)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var entityRepository = unitOfWork.Repositories.CalendarEntries;

                var sourceEntries = entityRepository
                    .Where(c => c.CalendarId == calendarId
                                && c.EntryOn.Year == fromYear)
                    .ToList();

                var targetEntries = sourceEntries
                    .Select(
                        c => new CalendarEntry
                        {
                            CalendarId = calendarId,
                            EntryOn = c.EntryOn.AddYears(toYear - fromYear),
                            Description = c.Description
                        }
                    )
                    .ToList();

                targetEntries.ForEach(entityRepository.Add);
                unitOfWork.Commit();
            }

            var result = new BusinessResult();

            var message = string.Format(CalendarEntryCardIndexServiceResources.CalendarEntriesCopiedMessageFormat,
                                        fromYear, toYear);

            result.AddAlert(AlertType.Success, message);

            return result;
        }

        public override void SetContext(CalendarEntry entity, object context)
        {
            var parentChildContext = context as ParentIdContext;

            if (parentChildContext == null)
            {
                throw new InvalidCastException("Invalid context given for calendar entry manipulation.");
            }

            entity.CalendarId = parentChildContext.ParentId;

            base.SetContext(entity, context);
        }

        protected override NamedFilter<CalendarEntry> OnMissingFilterFallback(NamedFilters<CalendarEntry> namedFilters, string filterKey, string filterCode)
        {
            return new NamedFilter<CalendarEntry>(string.Empty, c => false);
        }

        protected override IEnumerable<Expression<Func<CalendarEntry, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return c => c.Description;
        }

        protected override IOrderedQueryable<CalendarEntry> GetDefaultOrderBy(IQueryable<CalendarEntry> records, QueryCriteria criteria)
        {
            return records.OrderBy(c => c.EntryOn);
        }

        protected override NamedFilters<CalendarEntry> NamedFilters
        {
            get
            {
                var filters = new List<NamedFilter<CalendarEntry>>();
                
                foreach (var filterYear in GetFilterYears())
                {
                    var year = filterYear.Year;

                    filters.Add(new NamedFilter<CalendarEntry>(filterYear.GetFilterCode(), c => c.EntryOn.Year == year));
                }

                return new NamedFilters<CalendarEntry>(filters);
            }
        }
    }
}