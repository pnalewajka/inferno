﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Configuration;
using Smt.Atomic.Data.Repositories.Scopes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.Configuration.Services
{
    public class AuditTableCardIndexDataService : ReadOnlyCardIndexDataService<AuditTableDto, AuditTable, IConfigurationDbScope>, IAuditTableCardIndexDataService
    {
        public AuditTableCardIndexDataService(ICardIndexServiceDependencies<IConfigurationDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override IEnumerable<Expression<Func<AuditTable, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return a => a.Schema;
            yield return a => a.TableName;
        }
    }
}
