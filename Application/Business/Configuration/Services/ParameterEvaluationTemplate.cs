﻿/* inject-usings */
namespace Smt.Atomic.Business.Configuration.Services
{
    public class ParameterEvaluationTemplate : ParameterEvaluation
    {
        public override object GetValue(object parameterContext)
        {
            var context = (ParameterContext)parameterContext;

            /* inject-aliases */
            /* inject-conditions */
            throw new System.ArgumentOutOfRangeException("parameterContext", "Context did not match any parameter conditions");
        }
    }
}