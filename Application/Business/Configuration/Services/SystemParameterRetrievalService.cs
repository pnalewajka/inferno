﻿using System.Threading.Tasks;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.Business.Configuration.Services
{
    internal class SystemParameterRetrievalService : ISystemParameterService
    {
        private readonly ISystemParameterCacheService _systemParameterCacheService;
        private readonly IDefaultParameterContextBuilder _defaultParameterContextBuilder;

        internal const string DefaultParameterContextTypeId = "DefaultParameterContext";

        public SystemParameterRetrievalService(
            IDefaultParameterContextBuilder defaultParameterContextBuilder,
            ISystemParameterCacheService systemParameterCacheService)
        {
            _systemParameterCacheService = systemParameterCacheService;
            _defaultParameterContextBuilder = defaultParameterContextBuilder;
        }

        public object GetDefaultContext()
        {
            return _defaultParameterContextBuilder.Build();
        }

        public object GetDefaultContext(long employeeId)
        {
            return _defaultParameterContextBuilder.BuildForEmployee(employeeId);
        }

        public TValue GetParameter<TValue>(string parameterKey)
        {
            var context = _defaultParameterContextBuilder.Build();

            return GetParameter<TValue>(parameterKey, context);
        }

        public TValue GetParameter<TValue>(string parameterKey, object context)
        {
            return _systemParameterCacheService.GetParameter<TValue>(parameterKey, context);
        }

        public async Task<TValue> GetParameterAsync<TValue>(string parameterKey)
        {
            var context = _defaultParameterContextBuilder.Build();

            return await GetParameterAsync<TValue>(parameterKey, context);
        }

        public Task<TValue> GetParameterAsync<TValue>(string parameterKey, object context)
        {
            return _systemParameterCacheService.GetParameterAsync<TValue>(parameterKey, context);
        }
    }
}
