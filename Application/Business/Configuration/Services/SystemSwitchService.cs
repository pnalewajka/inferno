﻿using System;
using System.Linq;
using Castle.Core;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Configuration.Services
{
    [Interceptor(typeof(CacheInterceptor))]
    internal class SystemSwitchService : ISystemSwitchService
    {
        private readonly IReadOnlyUnitOfWorkService<IConfigurationDbScope> _unitOfWorkService;

        public SystemSwitchService(IReadOnlyUnitOfWorkService<IConfigurationDbScope> unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        [Cached(CacheArea = CacheAreas.SystemParameters)]
        public long GetSystemTimeOffset()
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var systemTimeParameter = unitOfWork
                    .Repositories
                    .SystemParameters
                    .SingleOrDefault(p => p.Key == ParameterKeys.SystemTimeOffsetInSeconds);

                if (systemTimeParameter == null)
                {
                    throw new SystemParameterNotFoundException(ParameterKeys.SystemTimeOffsetInSeconds);
                }

                var offsetInSeconds = Convert.ToInt64(systemTimeParameter.Value);

                return offsetInSeconds;
            }
        }
    }
}