﻿namespace Smt.Atomic.Business.Configuration.Consts
{
    public class SearchAreaCodes
    {
        public const string Name = "param-name";
        public const string Value = "param-value";
        public const string ContextType = "param-context";
    }
}
