﻿using System.Threading.Tasks;

namespace Smt.Atomic.Business.Configuration.Interfaces
{
    public interface ISystemParameterCacheService
    {
        TValue GetParameter<TValue>(string parameterKey, object context);

        Task<TValue> GetParameterAsync<TValue>(string parameterKey, object context);
    }
}
