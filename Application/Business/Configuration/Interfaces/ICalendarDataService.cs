using Smt.Atomic.Business.Configuration.Dto;

namespace Smt.Atomic.Business.Configuration.Interfaces
{
    public interface ICalendarDataService
    {
        CalendarData GetCalendarDataById(long calendarId);

        CalendarData GetCalendarDataByCode(string calendarCode);
    }
}