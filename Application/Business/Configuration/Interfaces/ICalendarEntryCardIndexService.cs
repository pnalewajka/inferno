﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Configuration.Dto;

namespace Smt.Atomic.Business.Configuration.Interfaces
{
    public interface ICalendarEntryCardIndexService : ICardIndexDataService<CalendarEntryDto>
    {
        IEnumerable<FilterYearDto> GetFilterYears();

        BusinessResult CopyEntries(int calendarId, int fromYear, int toYear);
    }
}