﻿using System;
using System.Collections.Generic;

namespace Smt.Atomic.Business.Configuration.Interfaces
{
    public interface IRazorTemplateService
    {
        /// <summary>
        /// Returns a type of model required for template resolution
        /// </summary>
        /// <param name="content">Template content, specific to given template engine implementation</param>
        /// <param name="templateCode">Code used for template retrieval. Can be used for internal parser cache</param>
        /// <returns>Type of data required for template resolution</returns>
        Type GetTemplateModelType(string content, string templateCode);

        /// <summary>
        /// Returns a string representation of the template applied to the message model
        /// </summary>
        /// <param name="content">Template content, specific to given template engine implementation</param>
        /// <param name="templateCode">Code used for template retrieval. Can be used for internal parser cache</param>
        /// <param name="messageModel">Data to be used for template resolution</param>
        /// <param name="viewBag">Additional data passed to the template via ViewBag</param>
        /// <returns>Template applied to the message model</returns>
        string TemplateRunAndCompile(string templateCode, string content, object messageModel, object viewBag = null);

        /// <summary>
        /// Validates template (usually before saving to the storage)
        /// </summary>
        /// <param name="content">Template content, specific to given template engine implementation</param>
        /// <param name="templateCode">Code used for template retrieval. Can be used for internal parser cache</param>
        /// <returns>List of issues found. Empty if template is valid</returns>
        IEnumerable<string> Validate(string templateCode, string content);
    }
}