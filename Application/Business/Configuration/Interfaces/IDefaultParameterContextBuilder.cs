﻿using Smt.Atomic.Business.Configuration.Dto;

namespace Smt.Atomic.Business.Configuration.Interfaces
{
    public interface IDefaultParameterContextBuilder
    {
        ParameterContext Build();

        ParameterContext BuildForEmployee(long employeeId);
    }
}
