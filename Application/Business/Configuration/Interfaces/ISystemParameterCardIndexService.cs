﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Configuration.Dto;

namespace Smt.Atomic.Business.Configuration.Interfaces
{
    public interface ISystemParameterCardIndexService : ICardIndexDataService<SystemParameterDto>
    {
    }
}
