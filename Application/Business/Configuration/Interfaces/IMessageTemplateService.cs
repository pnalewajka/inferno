﻿using System.Threading.Tasks;
using Smt.Atomic.Business.Configuration.Dto;

namespace Smt.Atomic.Business.Configuration.Interfaces
{
    public interface IMessageTemplateService
    {
        /// <summary>
        /// Returns template information with the representation of the message with template applied to the message model
        /// </summary>
        /// <param name="templateCode">Code of the template to load from the storage</param>
        /// <param name="messageModel">Data to be used for template resolution</param>
        /// <param name="viewBag">Additional data passed to the template via ViewBag</param>
        /// <param name="useCurrentCulture">Should use current culture instead of default one</param>
        /// <returns>Processed template content and information</returns>
        TemplateProcessingResult ResolveTemplate(string templateCode, object messageModel, object viewBag = null, bool useCurrentCulture = false);

        /// <summary>
        /// Returns template information with the representation of the message with template applied to the message model
        /// </summary>
        /// <param name="templateCode">Code of the template to load from the storage</param>
        /// <param name="messageModel">Data to be used for template resolution</param>
        /// <param name="viewBag">Additional data passed to the template via ViewBag</param>
        /// <param name="useCurrentCulture">Should use current culture instead of default one</param>
        /// <returns>Processed template content and information</returns>
        Task<TemplateProcessingResult> ResolveTemplateAsync(string templateCode, object messageModel, object viewBag = null, bool useCurrentCulture = false);
    }
}