﻿using Smt.Atomic.Business.Configuration.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Configuration.Interfaces
{
    public interface IDanteParameterContextBuilder
    {
        ParameterContext FillCustomParameterContext(long employeeId);
    }
}
