﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Configuration.Interfaces
{
    public interface IMessageTemplateCardIndexService : ICardIndexDataService<MessageTemplateDto>
    {
        byte[] GetZipWithHtmls(IList<long> ids);

        void CreateOrUpdateTemplate(string templateCode, string contentStream, string description, MessageTemplateContentType contentType);
    }
}