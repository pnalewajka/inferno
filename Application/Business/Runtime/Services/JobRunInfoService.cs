﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Castle.Core.Logging;
using Smt.Atomic.Business.Runtime.Dto;
using Smt.Atomic.Business.Runtime.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Runtime;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Runtime.Services
{
    internal class JobRunInfoService : IJobRunInfoService
    {
        private readonly IUnitOfWorkService<IRuntimeDbScope> _unitOfWorkService;
        private readonly IClassMapping<LastJobRunInfo, LastJobRunInfoDto> _lastJobRunInfotoDtoClassMapping;
        private readonly IClassMapping<JobRunInfo, JobRunInfoDto> _jobRunInfoToDtoClassMapping;
        private readonly ILogger _logger;
        private const string DeadlockPattern = "deadlock";

        public JobRunInfoService(IUnitOfWorkService<IRuntimeDbScope> unitOfWorkService,
            IClassMapping<LastJobRunInfo, LastJobRunInfoDto> lastJobRunInfotoDtoClassMapping,
            IClassMapping<JobRunInfo, JobRunInfoDto> jobRunInfoToDtoClassMapping,
            ILogger logger)
        {
            _unitOfWorkService = unitOfWorkService;
            _lastJobRunInfotoDtoClassMapping = lastJobRunInfotoDtoClassMapping;
            _jobRunInfoToDtoClassMapping = jobRunInfoToDtoClassMapping;
            _logger = logger;
        }

        public List<LastJobRunInfoDto> GetLastJobRunInfos(string pipeline)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return
                    unitOfWork
                    .Repositories
                    .LastJobRunInfos
                    .Where(jri => jri.Pipeline == pipeline)
                    .Select(_lastJobRunInfotoDtoClassMapping.CreateFromSource)
                    .ToList();
            }
        }

        public void CleanupExpiredRunInfos([NotNull] string pipeline)
        {
            if (pipeline == null)
            {
                throw new ArgumentNullException(nameof(pipeline));
            }

            const string sqlCommand =
                @"EXEC [Runtime].[atomic_CleanupExpiredRunInfos] @pipelineName = @pipelineName ";

            using (var unitOfWork = _unitOfWorkService.CreateExtended())
            {
                unitOfWork.ExecuteSqlCommand
                    (
                        sqlCommand,
                        new SqlParameter("@pipelineName", pipeline)
                    );

                unitOfWork.Commit();
            }
        }

        

        public JobRunInfoDto AcquireJobRunInfoWithDbLock(long jobDefinitionId, DateTime startedOn, [NotNull] string host, [NotNull] string schedulerName)
        {
            if (host == null)
            {
                throw new ArgumentNullException(nameof(host));
            }

            if (schedulerName == null)
            {
                throw new ArgumentNullException(nameof(schedulerName));
            }

            const string sqlAcquireCommand =
                @"EXEC [Runtime].[atomic_AcquireJobRunInfo] @jobDefinitionId = @jobDefinitionId, @startedOn = @startedOn, @host = @host , @schedulerName = @schedulerName , @newJobRunInfoId = @newJobRunInfoId OUTPUT ";

            using (var unitOfWork = _unitOfWorkService.CreateExtended())
            {
                var outParameter = new SqlParameter("@newJobRunInfoId", 0L) {Direction = ParameterDirection.Output};

                unitOfWork.ExecuteSqlCommand
                    (
                        sqlAcquireCommand,
                        new SqlParameter("@jobDefinitionId", jobDefinitionId),
                        new SqlParameter("@startedOn", startedOn),
                        new SqlParameter("@host", host),
                        new SqlParameter("@schedulerName", schedulerName),
                        outParameter
                    );

                try
                {
                    unitOfWork.Commit();
                }
                catch (Exception exception)
                {
                    if (exception.Message.Contains(DeadlockPattern))
                    {
                        _logger.Warn("Deadlock occured", exception);

                        return null;
                    }

                    throw;
                }               

                if (outParameter.Value == DBNull.Value)
                {
                    return null;
                }

                var newJobRunInfoId = Convert.ToInt64(outParameter.Value);

                return _jobRunInfoToDtoClassMapping.CreateFromSource(unitOfWork.Repositories.JobRunInfos.GetById(newJobRunInfoId));
            }
        }

        public void ReleaseJobRunInfoWithDbLock(long jobRunInfoId, long jobDefinitionId, DateTime finishedOn, JobStatus jobRunStatus, DateTime nextScheduledRun, int? timesFailedInRow)
        {
            const string sqlReleaseCommand =
                @"EXEC [Runtime].[atomic_ReleaseJobRunInfo] @jobDefinitionId = @jobDefinitionId,  @jobRunInfoId = @jobRunInfoId  , @jobStatusEnumId = @jobStatusEnumId, @finishedOn = @finishedOn, @nextRunOn = @nextRunOn, @timesFailedInRow = @timesFailedInRow";

            using (var unitOfWork = _unitOfWorkService.CreateExtended())
            {
                unitOfWork.ExecuteSqlCommand
                    (
                        sqlReleaseCommand,
                        new SqlParameter("@jobDefinitionId", jobDefinitionId),
                        new SqlParameter("@jobRunInfoId", jobRunInfoId),
                        new SqlParameter("@jobStatusEnumId", (long)jobRunStatus),
                        new SqlParameter("@finishedOn", finishedOn),
                        new SqlParameter("@nextRunOn", nextScheduledRun),
                        new SqlParameter("@timesFailedInRow", (object)timesFailedInRow ?? DBNull.Value)
                    );

                try
                {
                    unitOfWork.Commit();
                }
                catch (Exception exception)
                {
                    if (exception.Message.Contains(DeadlockPattern))
                    {
                        _logger.Warn("Deadlock occured", exception);

                        return;
                    }

                    throw;
                }
            }
        }

        public IList<JobRunInfo> GetJobRunInfos(long jobDefinitionId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var infos = unitOfWork
                    .Repositories
                    .JobRunInfos
                    .Where(x => x.JobDefinitionId == jobDefinitionId);

                return infos.ToList();
            }
        }
    }
}