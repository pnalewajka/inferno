﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Runtime.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Runtime;

namespace Smt.Atomic.Business.Runtime.Interfaces
{
    public interface IJobRunInfoService
    {
        /// <summary>
        /// Return list of jobs with last known job run info data
        /// </summary>
        /// <param name="pipeline"></param>
        /// <returns></returns>
        List<LastJobRunInfoDto> GetLastJobRunInfos(string pipeline);

        /// <summary>
        /// Acquire entry in job run info , if possible. If not possible (job is already running), then return null
        /// </summary>
        /// <param name="jobDefinitionId"></param>
        /// <param name="startedOn"></param>
        /// <param name="host"></param>
        /// <param name="schedulerName"></param>
        /// <returns></returns>
        JobRunInfoDto AcquireJobRunInfoWithDbLock(long jobDefinitionId, DateTime startedOn, string host,
            string schedulerName);

        /// <summary>
        /// Release db lock on  job with specific status
        /// </summary>
        /// <param name="jobRunInfoId"></param>
        /// <param name="jobDefinitionId"></param>
        /// <param name="finishedOn"></param>
        /// <param name="jobRunStatus"></param>
        /// <param name="nextScheduledRun"></param>
        /// <param name="timesFailedInRow"></param>
        void ReleaseJobRunInfoWithDbLock(long jobRunInfoId, long jobDefinitionId, DateTime finishedOn,
            JobStatus jobRunStatus, DateTime nextScheduledRun, int? timesFailedInRow);

        /// <summary>
        /// Maintain job run infos, that  might've failed  or didn't terminate properly on other machines";
        /// </summary>
        /// <param name="pipeline"></param>
        void CleanupExpiredRunInfos(string pipeline);

        IList<JobRunInfo> GetJobRunInfos(long jobDefinitionId);
    }
}
