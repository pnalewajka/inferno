﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.Runtime.Interfaces;
using Smt.Atomic.Business.Runtime.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.Runtime
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            if (containerType == ContainerType.JobScheduler)
            {
                container.Register(Component.For<IJobRunInfoService>().ImplementedBy<JobRunInfoService>().LifestyleTransient());
            }
        }
    }
}