﻿using Smt.Atomic.Data.Entities.Modules.Runtime;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Runtime.Dto
{
    public class LastJobRunInfoToLastJobRunInfoDtoMapping : ClassMapping<LastJobRunInfo, LastJobRunInfoDto>
    {
        public LastJobRunInfoToLastJobRunInfoDtoMapping()
        {
            Mapping = e => new LastJobRunInfoDto
            {
                Id = e.Id,
                JobRunInfoId = e.JobRunInfoId,
                Status = e.Status,
                StartedOn = e.StartedOn,
                FinishedOn = e.FinishedOn,
                TimesFailedInRow = e.TimesFailedInRow
            };
        }
    }
}
