﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Runtime;

namespace Smt.Atomic.Business.Runtime.Dto
{
    public class JobRunInfoDtoToJobRunInfoMapping : ClassMapping<JobRunInfoDto, JobRunInfo>
    {
        public JobRunInfoDtoToJobRunInfoMapping()
        {
            Mapping = d => new JobRunInfo
            {
                JobDefinitionId = d.JobDefinitionId,
                StartedOn = d.StartedOn,
                FinishedOn = d.FinishedOn,
                Status = d.Status,
                Host = d.Host,
                SchedulerName = d.SchedulerName,
                Id = d.Id,
                NextRunOn = d.NextRunOn,
                TimesFailedInRow = d.TimesFailedInRow,
                Timestamp = d.Timestamp
            }
            ;
        }
    }
}
