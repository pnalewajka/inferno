﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Runtime;

namespace Smt.Atomic.Business.Runtime.Dto
{
    public class JobRunInfoToJobRunInfoDtoMapping : ClassMapping<JobRunInfo, JobRunInfoDto>
    {
        public JobRunInfoToJobRunInfoDtoMapping()
        {
            Mapping = e => new JobRunInfoDto
            {
                JobDefinitionId = e.JobDefinitionId,
                StartedOn = e.StartedOn,
                FinishedOn = e.FinishedOn,
                Status = e.Status,
                Host = e.Host,
                SchedulerName = e.SchedulerName,
                Id = e.Id,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp,
                NextRunOn = e.NextRunOn,
                TimesFailedInRow = e.TimesFailedInRow
            };
        }
    }
}
