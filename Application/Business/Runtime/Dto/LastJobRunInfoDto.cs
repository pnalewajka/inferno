﻿using System;
namespace Smt.Atomic.Business.Runtime.Dto
{
    public class LastJobRunInfoDto
    {
        public long Id { get; set; }

        public long? JobRunInfoId { get; set; }

        public CrossCutting.Common.Enums.JobStatus? Status { get; set; }

        public DateTime? StartedOn { get; set; }

        public DateTime? FinishedOn { get; set; }

        public int MaxExecutionPeriodInSeconds { get; set; }

        public DateTime? NextRunOn { get; set; }

        public bool ShouldRunNow { get; set; }

        public int? TimesFailedInRow { get; set; }

        public bool IsJobEnabled { get; set; }

    }
}
