﻿using System;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Runtime.Dto
{
    public class JobRunInfoDto
    {
        public long JobDefinitionId { get; set; }

        public DateTime StartedOn { get; set; }

        public DateTime? FinishedOn { get; set; }

        public JobStatus Status { get; set; }

        public string Host { get; set; }

        public string SchedulerName { get; set; }

        public long Id { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public DateTime? NextRunOn { get; set; }

        public int? TimesFailedInRow { get; set; }
    }
}
