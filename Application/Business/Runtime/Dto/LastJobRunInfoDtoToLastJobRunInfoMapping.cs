﻿using Smt.Atomic.Data.Entities.Modules.Runtime;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Runtime.Dto
{
    public class LastJobRunInfoDtoToLastJobRunInfoMapping : ClassMapping<LastJobRunInfoDto, LastJobRunInfo>
    {
        public LastJobRunInfoDtoToLastJobRunInfoMapping()
        {
            Mapping = d => new LastJobRunInfo
            {
                Id = d.Id,
                JobRunInfoId = d.JobRunInfoId,
                Status = d.Status,
                StartedOn = d.StartedOn,
                FinishedOn = d.FinishedOn,
                TimesFailedInRow = d.TimesFailedInRow
            };
        }
    }
}
