﻿using Smt.Atomic.Business.Reports.Enums;

namespace Smt.Atomic.Business.Reports.Interfaces
{
    public interface IUtilizationReportSearch
    {
        long? OrgUnitId { get; }
        long? LocationId { get; }
        long? JobProfileId { get; }
        long? LevelId { get; }
        int Months { get; }
        HourStatus HourStatus { get; }
        UtilizationDisplayType DisplayType { get; }
        bool MustIncludeEntriesWithoutCategory { get; }
    }
}