﻿using System.IO;
using Smt.Atomic.Business.Reports.Models;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Reports.Interfaces
{
    public interface ISeniorityLevelsAllocationReportService
    {
        MemoryStream GenerateReport(SeniorityLevelsReportParameters seniorityLevelsReportParameters);
    }
}
