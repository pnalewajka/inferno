﻿using System.Collections.Generic;
using Smt.Atomic.Business.Reports.Dto;

namespace Smt.Atomic.Business.Reports.Interfaces
{
    public interface ITechnologyRadarItemsService
    {
        IEnumerable<TechnologyRadarItemDto> GetItems();
    }
}