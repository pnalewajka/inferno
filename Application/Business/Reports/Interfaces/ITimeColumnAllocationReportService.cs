﻿using System.IO;
using Smt.Atomic.Business.Reports.Models;

namespace Smt.Atomic.Business.Reports.Interfaces
{
    public interface ITimeColumnAllocationReportService
    {
        MemoryStream GenerateReport(TimeColumnReportParameters timeColumnReportParameters);
    }
}
