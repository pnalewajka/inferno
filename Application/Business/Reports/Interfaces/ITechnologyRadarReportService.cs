﻿using System.Collections.Generic;
using Smt.Atomic.Business.Reports.Dto;

namespace Smt.Atomic.Business.Reports.Interfaces
{
    public interface ITechnologyRadarReportService
    {
        IEnumerable<TechnologyRadarAreaDto> GetTechnologyRadarAreas();
    }
}
