﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Reports.Models;
using Smt.Atomic.Business.Reports.Services;

namespace Smt.Atomic.Business.Reports.Interfaces
{
    public interface IUtilizationReportService
    {
        IEnumerable<UtilizationReportRowModel> GetUtilizationReportRowModels(IUtilizationReportSearch model);

        IEnumerable<UtilizationProjectRowModel> GetUtilizationProjects(IUtilizationReportSearch model, long? utilizationCategoryId, bool? billability, DateTime date);

        IEnumerable<UtilizationProjectRowModel> GetUtilizationProjectsWithoutUtilization(IUtilizationReportSearch model, DateTime date);

        IEnumerable<UtilizationEmployeeRowModel> GetUtilizationProjectEmployees(IUtilizationReportSearch model, long? utilizationCategoryId,
            bool? billability, DateTime date, long projectId);

        IEnumerable<UtilizationEmployeeRowModel> GetUtilizationProjectEmployeesWithoutUtilization(IUtilizationReportSearch model, DateTime date, long projectId);

        IEnumerable<UtilizationCategoryGroupModel> GetUtilizationReportGroupDataModel(IUtilizationReportSearch model,
            DateTime forDate, IReadOnlyCollection<long> utilizationCategories);
    }
}
