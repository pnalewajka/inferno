﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Reports.Models
{
    public class SeniorityLevelsReportParameters
    {
        public ReportScopeEnum ReportScope { get; set; }

        public long? JobMatrixId { get; set; }

        public SeniorityLevelsReportParameters(ReportScopeEnum reportScope, long? jobMatrixId)
        {
            ReportScope = reportScope;
            JobMatrixId = jobMatrixId;
        }
    }
}
