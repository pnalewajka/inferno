﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Reports.Models
{
    public class TimeColumnReportParameters
    {
        public ReportScopeEnum ReportScope { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public TimeColumnReportType TimeColumnReportType { get; set; }
    }
}
