namespace Smt.Atomic.Business.Reports.Models
{
    public class UtilizationEmployeeRowModel
    {
        public long EmployeeId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string DisplayValue { get; set; }
    }
}