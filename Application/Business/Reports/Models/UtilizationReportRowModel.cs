﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Reports.Models
{
    public class UtilizationReportRowModel
    {
        public string Label { get; set; }

        public IEnumerable<UtilizationReportCellModel> Cells { get; set; }

        public bool IsChartRow { get; set; }

        public long? UtilizationCategoryId { get; set; }

        public bool? Billability { get; set; }

        public bool CanDrillData { get; set; }

        public bool CanExportData { get; set; }
    }
}