﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Reports.Models
{
    public class SingleResultFromCountingEmployees
    {
        public long? RowKey { get; set; }

        public AllocationStatus AllocationStatus { get; set; }

        public EmployeeStatus EmployeeStatus { get; set; }

        public DateTime Week { get; set; }

        public long Count { get; set; }
    }
}
