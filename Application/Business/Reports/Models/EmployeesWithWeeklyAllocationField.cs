﻿using System;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;

namespace Smt.Atomic.Business.Reports.Models
{
    public class EmployeesWithWeeklyAllocationField
    {
        public DateTime Week { get; set; }

        public long OrgUnitId { get; set; }

        public long? LocationId { get; set; }

        public EmployeeStatus EmployeeStatus { get; set; }

        public AllocationStatus AllocationStatus { get; set; }

        public ICollection<JobProfile> JobProfiles { get; set; }
    }
}
