﻿using System;

namespace Smt.Atomic.Business.Reports.Models
{
    public class UtilizationReportCellModel
    {
        public decimal Value { get; set; }

        public string TextValue { get; set; }

        public DateTime Date { get; set; }
    }
}