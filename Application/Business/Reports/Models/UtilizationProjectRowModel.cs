﻿namespace Smt.Atomic.Business.Reports.Models
{
    public class UtilizationProjectRowModel
    {
        public long ProjectId { get; set; }

        public string Name { get; set; }

        public string DisplayValue { get; set; }
    }
}