﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Reports.Models
{
    public class AllocationPerProjectReportParameters
    {
        public ProjectFilterTypeEnum ProjectFilterType { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public bool SkipAbsenceDays { get; set; }
    }
}
