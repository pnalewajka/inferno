﻿namespace Smt.Atomic.Business.Reports.Models
{
    public class UtilizationCategoryGroupModel
    {
        public string UtilizationCategoryName { get; set; }
        public decimal? UtilizationCategoryHours { get; set; }
        public string ProjectName { get; set; }
        public decimal? ProjectHours { get; set; }
        public string EmployeeName { get; set; }
        public decimal? EmployeeHours { get; set; }
    }
}