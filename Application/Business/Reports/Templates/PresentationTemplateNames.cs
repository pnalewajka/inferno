﻿namespace Smt.Atomic.Business.Reports.Templates
{
    public static class PresentationTemplateNames
    {
        public const string Description = "Presentation.ProjectDescription";
        public const string BusinessCase = "Presentation.ProjectBusinessCase";
    }
}
