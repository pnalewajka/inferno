﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.Business.Reports.Dto;
using Smt.Atomic.Business.Reports.Interfaces;
using Smt.Atomic.Business.Reports.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.Business.Reports.ReportDataSources
{
    [Identifier("DataSources.UtilizationReportDataSource")]
    [RequireRole(SecurityRoleType.CanGenerateUtilizationReport)]
    [DefaultReportDefinition(
        "UtilizationReport",
        nameof(ReportResources.UtilizationReportName),
        nameof(ReportResources.UtilizationReportDescription),
        MenuAreas.TimeTracking,
        MenuGroups.TimeTrackingReportsUtilization,
        ReportingEngine.MsExcel,
        "Smt.Atomic.Business.Reports.Templates.UtilizationReport.xlsx",
        typeof(ReportResources))]
    public class UtilizationReportDataSource : BaseReportDataSource<UtilizationReportDataSourceParametersDto>
    {
        private readonly IUtilizationReportService _utilizationReportService;
        private readonly IOrgUnitCardIndexDataService _orgUnitCardIndexDataService;
        private readonly ITimeService _timeService;

        public UtilizationReportDataSource(IUtilizationReportService utilizationReportService, IOrgUnitCardIndexDataService orgUnitCardIndexDataService, ITimeService timeService)
        {
            _utilizationReportService = utilizationReportService;
            _orgUnitCardIndexDataService = orgUnitCardIndexDataService;
            _timeService = timeService;
        }

        public override UtilizationReportDataSourceParametersDto GetDefaultParameters(ReportGenerationContext<UtilizationReportDataSourceParametersDto> reportGenerationContext)
        {
            var orgUnit = _orgUnitCardIndexDataService.TopChartOrgUnit(OrgUnitFeatures.ShowInUtilizationReport);
            var monthStart =
                _timeService.GetCurrentDate()
                .AddMonths(-1)
                .GetFirstDayInMonth();

            return new UtilizationReportDataSourceParametersDto
            {
                OrgUnitId = orgUnit?.Id,
                UtilizationMonth = monthStart,
                UtilizationCategoryIds = new List<long>()
            };
        }

        public override object GetData(ReportGenerationContext<UtilizationReportDataSourceParametersDto> reportGenerationContext)
        {
            var parameters = reportGenerationContext.Parameters;
            var items = _utilizationReportService
                .GetUtilizationReportGroupDataModel(parameters, parameters.UtilizationMonth, parameters.UtilizationCategoryIds)
                .ToList();

            return new { Rows = items };
        }
    }
}