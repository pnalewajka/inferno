﻿namespace Smt.Atomic.Business.Reports.Enums
{
    public enum TechnologyRadarStatus
    {
        Assess = 0,
        Rookie = 0,
        Trial = 1,
        Mature = 2,
        Adopt = 2,
        Hold = 3
    }
}