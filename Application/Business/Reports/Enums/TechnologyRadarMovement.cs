﻿namespace Smt.Atomic.Business.Reports.Enums
{
    public enum TechnologyRadarMovement
    {
        Unchanged,
        Changed
    }
}