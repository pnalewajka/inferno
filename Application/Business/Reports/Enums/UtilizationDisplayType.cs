﻿namespace Smt.Atomic.Business.Reports.Enums
{
    public enum UtilizationDisplayType
    {
        Values = 0,
        Percentage = 1
    }
}
