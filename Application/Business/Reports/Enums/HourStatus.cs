﻿using Smt.Atomic.Business.Reports.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.Business.Reports.Enums
{
    public enum HourStatus
    {
        [DescriptionLocalized(nameof(Accepted), typeof(UtilizationReportResources))]
        Accepted,
        [DescriptionLocalized(nameof(All), typeof(UtilizationReportResources))]
        All,
    }
}