﻿namespace Smt.Atomic.Business.Reports.Enums
{
    public enum TechnologyRadarArea
    {
        Platforms = 0,
        PatternsAndPractices = 1,
        LanguagesAndFrameworks = 2,
        Tools = 3
    }
}