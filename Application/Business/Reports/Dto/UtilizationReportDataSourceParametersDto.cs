using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Reports.Enums;
using Smt.Atomic.Business.Reports.Interfaces;

namespace Smt.Atomic.Business.Reports.Dto
{
    public class UtilizationReportDataSourceParametersDto : IUtilizationReportSearch
    {
        public long? OrgUnitId { get; set; }

        public DateTime UtilizationMonth { get; set; }

        public long? LocationId { get; set; }

        public long? JobProfileId { get; set; }

        public long? LevelId { get; set; }

        public HourStatus HourStatus { get; set; }

        public IReadOnlyCollection<long> UtilizationCategoryIds { get; set; } 

        public bool MustIncludeEntriesWithoutCategory { get; set; }

        public UtilizationDisplayType DisplayType { get; set; }

        public int Months { get; set; }
    }
}