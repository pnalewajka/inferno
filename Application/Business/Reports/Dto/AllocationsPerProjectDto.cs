﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Reports.Dto
{
    public class AllocationsPerProjectDto : BaseReportDto
    {
        public string ProjectName { get; set; }

        public string ProjectOrgUnitCode { get; set; }

        public string ProjectOrgUnitName { get; set; }

        public string JobProfileName { get; set; }

        public string EmployeeName { get; set; }

        public string EmployeeOrgUnitCode { get; set; }

        public string EmployeeOrgUnitName { get; set; }

        public string Apn { get; set; }

        public string KupferwerkProjectId { get; set; }

        public IList<decimal> WeekValues { get; set; }

        public AllocationsPerProjectDto()
        {
            WeekValues = new List<decimal>();
        }
    }
}
