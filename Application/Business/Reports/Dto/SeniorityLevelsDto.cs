﻿namespace Smt.Atomic.Business.Reports.Dto
{
    public class SeniorityLevelsDto : BaseReportDto
    {
        public string RowName { get; set; }

        public long Level1 { get; set; }

        public long Level2 { get; set; }

        public long Level3 { get; set; }

        public long Level4 { get; set; }

        public long Level5 { get; set; }

        public SeniorityLevelsDto(int? depth = null) : base(depth)
        {
        }
    }
}
