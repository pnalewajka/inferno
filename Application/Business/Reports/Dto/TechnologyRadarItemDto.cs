﻿using Smt.Atomic.Business.Reports.Enums;

namespace Smt.Atomic.Business.Reports.Dto
{
    public class TechnologyRadarItemDto
    {
        public string Subject { get; set; }

        public string Description { get; set; }

        public TechnologyRadarStatus Status { get; set; }

        public TechnologyRadarArea TechnologyRadarArea { get; set; }
        
        public TechnologyRadarMovement Movement { get; set; }
    }
}