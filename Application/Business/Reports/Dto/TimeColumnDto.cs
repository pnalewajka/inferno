﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Reports.Dto
{
    public class TimeColumnDto : BaseReportDto
    {
        public string RowName { get; set; }

        public string StatusName { get; set; }

        public List<long> WeekValues { get; set; }

        public TimeColumnDto(int? depth = null) : base(depth)
        {
            WeekValues = new List<long>();
        }
    }
}
