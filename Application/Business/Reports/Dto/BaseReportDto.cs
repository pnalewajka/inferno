﻿using Smt.Atomic.Business.Reports.Attributes;

namespace Smt.Atomic.Business.Reports.Dto
{
    public abstract class BaseReportDto
    {
        [NotExportable]
        public int FirstColumnIndent { get; private set; }

        protected BaseReportDto(int? firstColumnIndent = null)
        {
            if (firstColumnIndent.HasValue)
            {
                FirstColumnIndent = firstColumnIndent.Value;
            }
        }
    }
}
