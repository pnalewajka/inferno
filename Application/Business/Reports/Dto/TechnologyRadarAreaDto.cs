﻿using System.Collections.Generic;
using Smt.Atomic.Business.Reports.Enums;

namespace Smt.Atomic.Business.Reports.Dto
{
    public class TechnologyRadarAreaDto
    {
        public TechnologyRadarArea TechnologyRadarArea { get; set; }
        
        public IEnumerable<TechnologyRadarItemDto> AreaItems { get; set; }
    }
}