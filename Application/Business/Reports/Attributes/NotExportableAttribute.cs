﻿using System;

namespace Smt.Atomic.Business.Reports.Attributes
{
    public class NotExportableAttribute : Attribute
    {
    }
}
