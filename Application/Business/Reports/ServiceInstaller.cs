﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.Reports.Interfaces;
using Smt.Atomic.Business.Reports.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.Reports
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            switch (containerType)
            {
                case ContainerType.WebApp:
                    container.Register(Component.For<ISeniorityLevelsPerOrgUnitReportService>().ImplementedBy<SeniorityLevelsPerOrgUnitReportService>().LifestyleTransient());
                    container.Register(Component.For<ISeniorityLevelsPerLocationReportService>().ImplementedBy<SeniorityLevelsPerLocationReportService>().LifestyleTransient());
                    container.Register(Component.For<ISeniorityLevelsPerJobProfileReportService>().ImplementedBy<SeniorityLevelsPerJobProfileReportService>().LifestyleTransient());
                    container.Register(Component.For<ITimeColumnPerOrgUnitReportService>().ImplementedBy<TimeColumnPerOrgUnitReportService>().LifestyleTransient());
                    container.Register(Component.For<ITimeColumnPerLocationReportService>().ImplementedBy<TimeColumnPerLocationReportService>().LifestyleTransient());
                    container.Register(Component.For<ITimeColumnPerJobProfileReportService>().ImplementedBy<TimeColumnPerJobProfileReportService>().LifestyleTransient());
                    container.Register(Component.For<IUtilizationStatusPerOrgUnitReportService>().ImplementedBy<UtilizationStatusPerOrgUnitReportService>().LifestyleTransient());
                    container.Register(Component.For<IUtilizationStatusPerLocationReportService>().ImplementedBy<UtilizationStatusPerLocationReportService>().LifestyleTransient());
                    container.Register(Component.For<IUtilizationStatusPerJobProfileReportService>().ImplementedBy<UtilizationStatusPerJobProfileReportService>().LifestyleTransient());
                    container.Register(Component.For<ITechnologyRadarReportService>().ImplementedBy<TechnologyRadarReportService>().LifestyleTransient());
                    container.Register(Component.For<ITechnologyRadarItemsService>().ImplementedBy<TechnologyRadarItemsService>().LifestyleTransient());
                    container.Register(Component.For<IAllocationsPerProjectReportService>().ImplementedBy<AllocationsPerProjectReportService>().LifestyleTransient());
                    container.Register(Component.For<IUtilizationReportService>().ImplementedBy<UtilizationReportService>().LifestyleTransient());
                    
                    break;
            }
        }        
    }
}

