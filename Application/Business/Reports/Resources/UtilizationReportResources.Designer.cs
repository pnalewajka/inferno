﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Smt.Atomic.Business.Reports.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class UtilizationReportResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal UtilizationReportResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Smt.Atomic.Business.Reports.Resources.UtilizationReportResources", typeof(UtilizationReportResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Accepted.
        /// </summary>
        internal static string Accepted {
            get {
                return ResourceManager.GetString("Accepted", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All.
        /// </summary>
        internal static string All {
            get {
                return ResourceManager.GetString("All", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to  - billed.
        /// </summary>
        internal static string BilledSuffixLabel {
            get {
                return ResourceManager.GetString("BilledSuffixLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to  - free.
        /// </summary>
        internal static string FreeSuffixLabel {
            get {
                return ResourceManager.GetString("FreeSuffixLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Grand total.
        /// </summary>
        internal static string GrandTotalLabel {
            get {
                return ResourceManager.GetString("GrandTotalLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to (no category).
        /// </summary>
        internal static string NoCategoryLabel {
            get {
                return ResourceManager.GetString("NoCategoryLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to % billed.
        /// </summary>
        internal static string PercentOfBilledLabel {
            get {
                return ResourceManager.GetString("PercentOfBilledLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to % billed (without absences).
        /// </summary>
        internal static string PercentOfBilledLabelWithoutAbsences {
            get {
                return ResourceManager.GetString("PercentOfBilledLabelWithoutAbsences", resourceCulture);
            }
        }
    }
}
