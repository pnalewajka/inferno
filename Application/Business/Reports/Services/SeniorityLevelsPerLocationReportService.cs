﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Reports.Dto;
using Smt.Atomic.Business.Reports.Interfaces;
using Smt.Atomic.Business.Reports.Models;
using Smt.Atomic.Business.Reports.Resources;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Reports.Services
{
    public class SeniorityLevelsPerLocationReportService : SeniorityLevelsBaseReportService, ISeniorityLevelsPerLocationReportService
    {
        private readonly IUnitOfWorkService<IDictionariesDbScope> _dictionariesUnitOfWorkService;
        
        private IEnumerable<Location> Locations
        {
            get
            {
                using (var unitOfWork = _dictionariesUnitOfWorkService.Create())
                {
                    return unitOfWork.Repositories.Locations.OrderBy(l => l.Name).ToList();
                }
            }
        }

        public SeniorityLevelsPerLocationReportService(IUnitOfWorkService<IAllocationDbScope> allocationUnitOfWorkService, IOrgUnitService orgUnitService,
            IPrincipalProvider principalProvider, IUnitOfWorkService<IDictionariesDbScope> dictionariesUnitOfWorkService) : base(allocationUnitOfWorkService, orgUnitService, principalProvider)
        {
            _dictionariesUnitOfWorkService = dictionariesUnitOfWorkService;
        }

        protected override ReportDataSource<SeniorityLevelsDto> GetDataSource(SeniorityLevelsReportParameters seniorityLevelsReportParameters)
        {
            var reportDataSource = new ReportDataSource<SeniorityLevelsDto>();
            var locationsDictionary = Locations.ToDictionary(k => k.Id, v => v.Name);

            using (var unitOfWork = AllocationUnitOfWorkService.Create())
            {
                var jobMatrixPerLocation =
                    GetEmployees(unitOfWork, seniorityLevelsReportParameters.ReportScope).Where(e => e.JobMatrixs.Any())
                        .SelectMany(jm => jm.JobMatrixs,
                            (employee, jobMatrix) => new {employee.LocationId, JobMatrixId = jobMatrix.Id})
                        .Join(GetJobMatrixLevels(unitOfWork, seniorityLevelsReportParameters.JobMatrixId),
                            e => e.JobMatrixId, j => j.Id,
                            (e, j) => new {j.Level, e.LocationId})

                        .GroupBy(e => new {e.LocationId}).OrderBy(e => e.Key.LocationId).ToList()
                        .Select(e => new SeniorityLevelsSingleRow
                        {
                            RowKey = e.Key.LocationId ?? -1,
                            Level1 = e.Count(c => c.Level == 1),
                            Level2 = e.Count(c => c.Level == 2),
                            Level3 = e.Count(c => c.Level == 3),
                            Level4 = e.Count(c => c.Level == 4),
                            Level5 = e.Count(c => c.Level == 5),
                        })
                        .ToList();

                reportDataSource.Rows = CreateReportDataSourceRows(locationsDictionary, jobMatrixPerLocation);

                reportDataSource.Headers = new List<string> { ReportResources.LocationsTitle };
                reportDataSource.Headers.AddRange(CreateReportDataSourceHeader(unitOfWork));

                reportDataSource.Sumary = CreateReportDataSourceSummary(jobMatrixPerLocation);
               
                return reportDataSource;
            }
        }
    }
}
