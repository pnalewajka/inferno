﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Reports.Dto;
using Smt.Atomic.Business.Reports.Interfaces;
using Smt.Atomic.Business.Reports.Models;
using Smt.Atomic.Business.Reports.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Reports.Services
{
    public class TimeColumnPerOrgUnitReportService : TimeColumnBaseReportService, ITimeColumnPerOrgUnitReportService
    {
        private readonly IUnitOfWorkService<IOrganizationScope> _organizationUnitOfWorkService;
        
        private IEnumerable<OrgUnit> OrgUnits
        {
            get
            {
                using (var unitOfWork = _organizationUnitOfWorkService.Create())
                {
                    return unitOfWork.Repositories.OrgUnits.OrderBy(o => o.Id).ToList();
                }
            }
        }
        
        public TimeColumnPerOrgUnitReportService(IUnitOfWorkService<IAllocationDbScope> allocationUnitOfWorkService,
            IOrgUnitService orgUnitService, IPrincipalProvider principalProvider, IUnitOfWorkService<IOrganizationScope> organizationUnitOfWorkService, 
            IDateRangeService dateRangeService)
            : base(allocationUnitOfWorkService, orgUnitService, principalProvider, dateRangeService)
        {
            _organizationUnitOfWorkService = organizationUnitOfWorkService;
        }

        protected override ReportDataSource<TimeColumnDto> GetDataSource(TimeColumnReportParameters parameters)
        {
            AssignAllowedEmployeeStatuses(parameters.TimeColumnReportType);
            var reportDataSource = new ReportDataSource<TimeColumnDto>();
            var fromDate = parameters.FromDate.GetPreviousDayOfWeek(DayOfWeek.Monday);
            var weekRanges = DateRangeService.GetAllWeeksInDateRanges(parameters.FromDate, parameters.ToDate).ToList();
            var orgUnitsDictionary = OrgUnits.ToDictionary(k => k.Id, v => v.Name);
            
            using (var unitOfWork = AllocationUnitOfWorkService.Create())
            {
                var countedResultsForCompany = GetEmployeesWithWeeklyAllocation(unitOfWork, parameters.ReportScope, fromDate, parameters.ToDate)
                        .GroupBy(e => new { e.Week, e.OrgUnitId, e.EmployeeStatus })
                        .OrderBy(e => e.Key.OrgUnitId)
                        .ThenBy(e => e.Key.EmployeeStatus)
                        .Select(e => new SingleResultFromCountingEmployees
                        {
                            RowKey = e.Key.OrgUnitId,
                            EmployeeStatus = e.Key.EmployeeStatus,
                            Week = e.Key.Week,
                            Count = e.Count()
                        })
                        .ToList();

                List<TimeColumnDto> countedTotalResultForCompany = CreateRowsForAllCompany(orgUnitsDictionary, countedResultsForCompany, weekRanges);
                
                UpdateEmployeeCountInOrgUnit(countedResultsForCompany);

                reportDataSource.Rows = CreateRowsForEmployeeStatuses(OrgUnits, countedResultsForCompany, weekRanges);
                reportDataSource.Rows.AddRange(countedTotalResultForCompany);
            }

            reportDataSource.Headers = CreateReportDataSourceHeader(ReportResources.OrgUnitTitle, weekRanges);
            reportDataSource.Sumary = new List<object>();

            return reportDataSource;
        }

        private List<TimeColumnDto> CreateRowsForEmployeeStatuses(IEnumerable<OrgUnit> orgUnits,
            List<SingleResultFromCountingEmployees> countedResultsForCompany, List<DateTime> weekRanges)
        {
            var reportDataSourceRows = new List<TimeColumnDto>();

            foreach (var orgUnit in orgUnits)
            {
                var keyValuePair = new KeyValuePair<long, string>(orgUnit.Id, orgUnit.Name);

                foreach (EmployeeStatus employeeStatus in AllowedEmployeeStatus)
                {
                    var singleTimeColumnRow = CreateStatusRowForEmployeeStatus(keyValuePair, employeeStatus, weekRanges, countedResultsForCompany, orgUnit.Depth);
                    reportDataSourceRows.Add(singleTimeColumnRow);
                }

                var singleTimeColumnTotal = CreateTotalRowForEmployeeStatus(keyValuePair, weekRanges, countedResultsForCompany, orgUnit.Depth);
                reportDataSourceRows.Add(singleTimeColumnTotal);
            }

            return reportDataSourceRows;
        }

        private void UpdateEmployeeCountInOrgUnit(List<SingleResultFromCountingEmployees> countedValues)
        {
            var sortedOrgUnit = OrgUnits.OrderByDescending(x => x.NameSortOrder);

            foreach (var row in sortedOrgUnit)
            {
                if (row.ParentId == null) { continue; }

                var childRows = countedValues.Where(l => l.RowKey == row.Id).ToList();

                if (!childRows.Any()) { continue; }

                foreach (var singleChild in childRows)
                {
                    var parentRow =
                        countedValues.Find(c =>
                                c.RowKey == row.ParentId && c.Week == singleChild.Week &&
                                c.EmployeeStatus == singleChild.EmployeeStatus);

                    if (parentRow != null)
                    {
                        countedValues.Find(c =>
                                c.RowKey == row.ParentId.Value && c.Week == singleChild.Week &&
                                c.EmployeeStatus == singleChild.EmployeeStatus).Count += singleChild.Count;
                    }
                    else
                    {
                        var newParent = new SingleResultFromCountingEmployees
                        {
                            EmployeeStatus = singleChild.EmployeeStatus,
                            Week = singleChild.Week,
                            Count = singleChild.Count,
                            RowKey = row.ParentId.Value
                        };

                        countedValues.Add(newParent);
                    }
                }
            }
        }
    }
}
