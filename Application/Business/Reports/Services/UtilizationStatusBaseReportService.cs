﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Reports.Dto;
using Smt.Atomic.Business.Reports.Models;
using Smt.Atomic.Business.Reports.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Reports.Services
{
    public class UtilizationStatusBaseReportService : BaseReportService<TimeColumnDto, TimeColumnReportParameters>
    {
        protected readonly IDateRangeService DateRangeService;

        protected readonly AllocationStatus[] AllowedAllocationStatuses =
        {
            AllocationStatus.Allocated,
            AllocationStatus.Planned, AllocationStatus.Free, AllocationStatus.UnderAllocated,
            AllocationStatus.UnderPlanned, AllocationStatus.AlmostFree, AllocationStatus.Mixed
        };

        public UtilizationStatusBaseReportService(IUnitOfWorkService<IAllocationDbScope> allocationUnitOfWorkService,
            IOrgUnitService orgUnitService, IPrincipalProvider principalProvider, IDateRangeService dateRangeService)
            : base(allocationUnitOfWorkService, orgUnitService, principalProvider)
        {
            DateRangeService = dateRangeService;
        }

        protected IQueryable<EmployeesWithWeeklyAllocationField> GetEmployeesWithWeeklyAllocation(
            IUnitOfWork<IAllocationDbScope> unitOfWork, ReportScopeEnum reportScope, DateTime fromDate, DateTime toDate)
        {
            IQueryable<Employee> employee = unitOfWork.Repositories.Employees;
            if (reportScope == ReportScopeEnum.MyEmployees)
            {
                var currentEmployeeId = PrincipalProvider.Current.EmployeeId;
                var managerOrgUnitsIds = OrgUnitService.GetManagerOrgUnitDescendantIds(currentEmployeeId);

                employee = employee.Where(EmployeeBusinessLogic.IsEmployeeOf.Parametrize(currentEmployeeId, managerOrgUnitsIds));
            }

            var weeklyStatuses = unitOfWork.Repositories.WeeklyAllocationStatus.Where(w =>
                    w.Week >= fromDate && w.Week <= toDate && w.AllocationStatus != AllocationStatus.Unavailable &&
                    w.AllocationStatus != AllocationStatus.InActive);

            return employee.Join(weeklyStatuses, e => e.Id, w => w.EmployeeId,
                (e, w) => new EmployeesWithWeeklyAllocationField
                {
                    Week = w.Week,
                    OrgUnitId = e.OrgUnitId,
                    LocationId = e.LocationId,
                    JobProfiles = e.JobProfiles,
                    AllocationStatus = w.AllocationStatus
                });
        }
        
        protected override ReportDataSource<TimeColumnDto> GetDataSource(TimeColumnReportParameters parameters)
        {
            throw new MethodAccessException("It's base method. Please don't use it!");
        }

        protected List<TimeColumnDto> CreateRowsForEmployeeStatuses(Dictionary<long, string> rowsReportDictionary,
            List<SingleResultFromCountingEmployees> countedResultsForCompany, List<DateTime> weekRanges)
        {
            var reportDataSourceRows = new List<TimeColumnDto>();

            foreach (var rowReportDictionary in rowsReportDictionary)
            {
                foreach (AllocationStatus allocationStatus in AllowedAllocationStatuses)
                {
                    var singleTimeColumnRow = CreateStatusRowForAllocationStatus(rowReportDictionary, allocationStatus, weekRanges, countedResultsForCompany);
                    reportDataSourceRows.Add(singleTimeColumnRow);
                }

                var singleTimeColumnTotal = CreateTotalRowForAllocationStatus(rowReportDictionary, weekRanges, countedResultsForCompany);
                reportDataSourceRows.Add(singleTimeColumnTotal);
            }

            return reportDataSourceRows;
        }

        protected List<TimeColumnDto> CreateRowsForAllCompany(Dictionary<long, string> rowsReportDictionary, List<SingleResultFromCountingEmployees> countedResultsForCompany, List<DateTime> weekRanges)
        {
            var reportDataSourceRows = new List<TimeColumnDto>();
            foreach (AllocationStatus allocationStatus in AllowedAllocationStatuses)
            {
                var singleTimeColumnRow = CreateStatusRowForAllCompany(allocationStatus, weekRanges, countedResultsForCompany);
                reportDataSourceRows.Add(singleTimeColumnRow);
            }

            var singleTotalRowForCompanyTotal = CreateTotalRowForAllCompany(weekRanges, countedResultsForCompany);
            reportDataSourceRows.Add(singleTotalRowForCompanyTotal);

            return reportDataSourceRows;
        }

        protected IList<string> CreateReportDataSourceHeader(string headerTitle, List<DateTime> weekRanges)
        {
            var headerList = new List<string> { headerTitle, ReportResources.AllocationStatusTitle };
            var columnNames = weekRanges.Select(w => w.ToShortDateString());
            headerList.AddRange(columnNames);

            return headerList;
        }

        protected TimeColumnDto CreateTotalRowForAllocationStatus(KeyValuePair<long, string> rowKeyValuePair, List<DateTime> weekRanges,
            List<SingleResultFromCountingEmployees> countedResultsForCompany, int? depth = null)
        {
            var newRowTotal = new TimeColumnDto(depth)
            {
                RowName = rowKeyValuePair.Value,
                StatusName = ReportResources.StatusTotal,
            };

            foreach (var singleValue in weekRanges.Select(week => countedResultsForCompany.Where(c =>
                c.RowKey.HasValue && c.RowKey.Value == rowKeyValuePair.Key && c.Week == week)).Select(l => l.Sum(s => s.Count)))
            {
                newRowTotal.WeekValues.Add(singleValue);
            }

            return newRowTotal;
        }

        protected TimeColumnDto CreateStatusRowForAllocationStatus(KeyValuePair<long, string> rowKeyValuePair, AllocationStatus allocationStatus,
            List<DateTime> weekRanges, List<SingleResultFromCountingEmployees> countedResultsForCompany, int? depth = null)
        {
            var newRow = new TimeColumnDto(depth)
            {
                RowName = rowKeyValuePair.Value,
                StatusName = allocationStatus.GetDescription(),
            };

            foreach (var singleValue in weekRanges.Select(week => countedResultsForCompany.FirstOrDefault(c =>
                c.RowKey.HasValue && c.RowKey.Value == rowKeyValuePair.Key && c.Week == week && c.AllocationStatus == allocationStatus)))
            {
                newRow.WeekValues.Add(singleValue?.Count ?? 0);
            }

            return newRow;
        }

        private TimeColumnDto CreateTotalRowForAllCompany(List<DateTime> weekRanges, List<SingleResultFromCountingEmployees> countedResultsForCompany)
        {
            var newRowTotalTotal = new TimeColumnDto
            {
                RowName = ReportResources.CompanyTotalText,
                StatusName = ReportResources.StatusTotal,
            };

            foreach (var singleValue in weekRanges.Select(week => countedResultsForCompany.Where(c => c.Week == week)).Select(l => l.Sum(s => s.Count)))
            {
                newRowTotalTotal.WeekValues.Add(singleValue);
            }

            return newRowTotalTotal;
        }

        private TimeColumnDto CreateStatusRowForAllCompany(AllocationStatus allocationStatus, List<DateTime> weekRanges,
            List<SingleResultFromCountingEmployees> countedResultsForCompany)
        {
            var newRow = new TimeColumnDto
            {
                RowName = ReportResources.CompanyTotalText,
                StatusName = allocationStatus.GetDescription(),
            };

            foreach (var singleValue in weekRanges.Select(week => countedResultsForCompany.Where(c =>
                c.Week == week && c.AllocationStatus == allocationStatus)).Select(l => l.Sum(s => s.Count)))
            {
                newRow.WeekValues.Add(singleValue);
            }

            return newRow;
        }
    }
}
