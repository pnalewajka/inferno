﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using ClosedXML.Excel;
using CsvHelper;
using CsvHelper.Excel;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Reports.Attributes;
using Smt.Atomic.Business.Reports.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Reports.Services
{
    public abstract class BaseReportService<TDataSourceRow, TInputParameters> where TDataSourceRow : BaseReportDto
    {
        protected readonly IUnitOfWorkService<IAllocationDbScope> AllocationUnitOfWorkService;
        protected readonly IOrgUnitService OrgUnitService;
        protected readonly IPrincipalProvider PrincipalProvider;

        protected abstract ReportDataSource<TDataSourceRow> GetDataSource(TInputParameters parameters);

        private const string CsvDelimiter = ";";

        protected class ReportDataSource<T>
        {
            public IList<string> Headers { get; set; }

            public IList<T> Rows { get; set; }

            public IList<object> Sumary { get; set; }
        }

        protected BaseReportService(IUnitOfWorkService<IAllocationDbScope> allocationUnitOfWorkService,
            IOrgUnitService orgUnitService,
            IPrincipalProvider principalProvider)
        {
            AllocationUnitOfWorkService = allocationUnitOfWorkService;
            OrgUnitService = orgUnitService;
            PrincipalProvider = principalProvider;
        }

        protected IQueryable<Employee> GetEmployees(IUnitOfWork<IAllocationDbScope> unitOfWork, ReportScopeEnum reportScope)
        {
            if (reportScope == ReportScopeEnum.AllEmployees)
            {
                return unitOfWork.Repositories.Employees;
            }

            var currentEmployeeId = PrincipalProvider.Current.EmployeeId;
            var managerOrgUnitsIds = OrgUnitService.GetManagerOrgUnitDescendantIds(currentEmployeeId);

            return unitOfWork.Repositories.Employees.Where(EmployeeBusinessLogic.IsEmployeeOf.Parametrize(currentEmployeeId, managerOrgUnitsIds));
        }
        
        public MemoryStream GenerateReport(TInputParameters parameters)
        {
            var reportDataSource = GetDataSource(parameters);

            using (XLWorkbook xlWorkbook = new XLWorkbook(XLEventTracking.Disabled))
            {
                using (var streamWriter = new ExcelSerializer(xlWorkbook))
                {
                    using (var csvWriter = new CsvWriter(streamWriter))
                    {
                        csvWriter.Configuration.Delimiter = CsvDelimiter;
                        csvWriter.Configuration.CultureInfo = CultureInfo.GetCultureInfo(CultureCodes.EnglishAmerican);

                        foreach (var column in reportDataSource.Headers)
                        {
                            csvWriter.WriteField(column);
                        }

                        csvWriter.NextRecord();

                        foreach (var row in reportDataSource.Rows)
                        {
                            foreach (PropertyInfo propertyInfo in typeof(TDataSourceRow).GetProperties())
                            {
                                bool isNotExportable = propertyInfo.GetCustomAttribute<NotExportableAttribute>() != null;

                                if (isNotExportable)
                                {
                                    continue;
                                }

                                bool isCollection = typeof (IEnumerable).IsAssignableFrom(propertyInfo.PropertyType) &&
                                                    propertyInfo.PropertyType != typeof (string);
                                if (isCollection)
                                {
                                    if (propertyInfo.PropertyType == typeof(List<decimal>) || propertyInfo.PropertyType == typeof(IList<decimal>))
                                    {
                                        var collectionValues = propertyInfo.GetValue(row, null) as List<decimal>;

                                        if (collectionValues != null)
                                        {
                                            foreach (var singleCollectionValue in collectionValues)
                                            {
                                                csvWriter.WriteField(singleCollectionValue);
                                            }
                                        }
                                    }

                                    if (propertyInfo.PropertyType == typeof(List<long>) || propertyInfo.PropertyType == typeof(IList<long>))
                                    {
                                        var collectionValues = propertyInfo.GetValue(row, null) as List<long>;

                                        if (collectionValues != null)
                                        {
                                            foreach (var singleCollectionValue in collectionValues)
                                            {
                                                csvWriter.WriteField(singleCollectionValue);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    csvWriter.WriteField(propertyInfo.GetValue(row, null));
                                }
                            }

                            csvWriter.NextRecord();
                        }

                        if (reportDataSource.Sumary != null)
                        {
                            foreach (var column in reportDataSource.Sumary)
                            {
                                csvWriter.WriteField(column);
                            }
                        }

                        csvWriter.NextRecord();
                    }
                }

                AdjustColumnWidths(xlWorkbook, reportDataSource.Rows);
                var memoryStream = new MemoryStream();
                xlWorkbook.SaveAs(memoryStream);
                memoryStream.Seek(0, SeekOrigin.Begin);

                return memoryStream;
            }
        }

        private void AdjustColumnWidths(XLWorkbook xlWorkbook, IList<TDataSourceRow> parameters)
        {
            var xlWorksheet = xlWorkbook.Worksheet(1);
            int column = 1;
            int row = 0;

            while (!string.IsNullOrWhiteSpace(xlWorksheet.Cell(1, column).GetValue<string>()))
            {
                xlWorksheet.Cell(1, column).Style.Font.Bold = true;
                var xlColumn = xlWorksheet.Column(column);
                xlColumn.AdjustToContents();
                column++;
            }

            foreach (var parameter in parameters)
            {
                var baseReportDto = parameter as BaseReportDto;

                if (baseReportDto != null)
                {
                    xlWorksheet.Cell(row + 2, 1).Style.Alignment.Indent = baseReportDto.FirstColumnIndent;

                    var parameterValue = xlWorksheet.Cell(row + 2, 1).GetValue<string>();
                    if (parameterValue.First() == '"' && parameterValue.Last() == '"')
                    {
                        xlWorksheet.Cell(row + 2, 1).SetValue(parameterValue.Substring(1, parameterValue.Length - 2));
                    }
                }

                row++;
            }
        }
    }
}
