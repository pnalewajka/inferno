﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Reports.Dto;
using Smt.Atomic.Business.Reports.Interfaces;
using Smt.Atomic.Business.Reports.Models;
using Smt.Atomic.Business.Reports.Resources;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Reports.Services
{
    public class TimeColumnPerJobProfileReportService : TimeColumnBaseReportService, ITimeColumnPerJobProfileReportService
    {
        private const int UnassignedJobProfileValue = -1;
        private readonly IUnitOfWorkService<ISkillManagementDbScope> _skillManagementUnitOfWorkService;
        
        private IEnumerable<JobProfile> JobProfiles
        {
            get
            {
                using (var unitOfWork = _skillManagementUnitOfWorkService.Create())
                {
                    return unitOfWork.Repositories.JobProfiles.OrderBy(j => j.Name).ToList();
                }
            }
        }
        
        protected IQueryable<WeeklyAllocationStatus> GetWeeklyAllocationStatus(
            IUnitOfWork<IAllocationDbScope> unitOfWork, DateTime fromDate, DateTime toDate)
        {
            return unitOfWork.Repositories.WeeklyAllocationStatus
                .Where(w => w.Week >= fromDate && w.Week <= toDate && AllowedEmployeeStatus.Contains(w.EmployeeStatus));
        }

        public TimeColumnPerJobProfileReportService(IUnitOfWorkService<IAllocationDbScope> allocationUnitOfWorkService,
            IOrgUnitService orgUnitService, IPrincipalProvider principalProvider,
            IUnitOfWorkService<ISkillManagementDbScope> skillManagementUnitOfWorkService, IDateRangeService dateRangeService)
            : base(allocationUnitOfWorkService, orgUnitService, principalProvider, dateRangeService)
        {
            _skillManagementUnitOfWorkService = skillManagementUnitOfWorkService;
        }

        protected override ReportDataSource<TimeColumnDto> GetDataSource(TimeColumnReportParameters parameters)
        {
            AssignAllowedEmployeeStatuses(parameters.TimeColumnReportType);
            var reportDataSource = new ReportDataSource<TimeColumnDto>();
            var fromDate = parameters.FromDate.GetPreviousDayOfWeek(DayOfWeek.Monday);
            var weekRanges = DateRangeService.GetAllWeeksInDateRanges(parameters.FromDate, parameters.ToDate).ToList();
            var jobProfilesDictionary = JobProfiles.ToDictionary(k => k.Id, v => v.Name);
           
            using (var unitOfWork = AllocationUnitOfWorkService.Create())
            {
                var countedResultsForCompany =
                    GetEmployeesWithWeeklyAllocation(unitOfWork, parameters.ReportScope, fromDate, parameters.ToDate)
                        .SelectMany(jp => jp.JobProfiles.DefaultIfEmpty(),
                            (employee, jobProfile) => 
                                new { JobProfileId = jobProfile != null ? jobProfile.Id : UnassignedJobProfileValue, employee.Week, employee.EmployeeStatus })
                        .GroupBy(c => new {c.Week, c.JobProfileId, c.EmployeeStatus})
                        .OrderBy(c => c.Key.JobProfileId).ThenBy(c => c.Key.EmployeeStatus)
                        .Select(c => new SingleResultFromCountingEmployees
                        {
                            RowKey = c.Key.JobProfileId,
                            EmployeeStatus = c.Key.EmployeeStatus,
                            Week = c.Key.Week,
                            Count = c.Count()
                        })
                        .ToList();

                jobProfilesDictionary.Add(UnassignedJobProfileValue, ReportResources.UnassignedTitle);
                reportDataSource.Rows = CreateRowsForEmployeeStatuses(jobProfilesDictionary, countedResultsForCompany, weekRanges);
                
                var countedTotalResultForCompany =
                                GetEmployees(unitOfWork, parameters.ReportScope)
                                .Join(GetWeeklyAllocationStatus(unitOfWork, fromDate, parameters.ToDate), e => e.Id, w => w.EmployeeId, (e, w) => new { w.Week, w.EmployeeStatus })
                                .GroupBy(c => new { c.Week, c.EmployeeStatus }).OrderBy(c => c.Key.EmployeeStatus)
                                .Select(c => new SingleResultFromCountingEmployees
                                {
                                    EmployeeStatus = c.Key.EmployeeStatus,
                                    Week = c.Key.Week,
                                    Count = c.Count()
                                })
                                .ToList();

                reportDataSource.Rows.AddRange(CreateRowsForAllCompany(jobProfilesDictionary, countedTotalResultForCompany, weekRanges));
            }

            reportDataSource.Headers = CreateReportDataSourceHeader(ReportResources.JobProfileTitle, weekRanges);
            reportDataSource.Sumary = new List<object>();

            return reportDataSource;
        }
    }
}