﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Reports.Dto;
using Smt.Atomic.Business.Reports.Interfaces;
using Smt.Atomic.Business.Reports.Models;
using Smt.Atomic.Business.Reports.Resources;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Reports.Services
{
    public class UtilizationStatusPerLocationReportService : UtilizationStatusBaseReportService, IUtilizationStatusPerLocationReportService
    {
        private readonly IUnitOfWorkService<IDictionariesDbScope> _dictionariesUnitOfWorkService;
        
        private IEnumerable<Location> Locations
        {
            get
            {
                using (var unitOfWork = _dictionariesUnitOfWorkService.Create())
                {
                    return unitOfWork.Repositories.Locations.OrderBy(l => l.Name).ToList();
                }
            }
        }
        
        public UtilizationStatusPerLocationReportService(IUnitOfWorkService<IAllocationDbScope> allocationUnitOfWorkService, IOrgUnitService orgUnitService,
            IPrincipalProvider principalProvider, IUnitOfWorkService<IDictionariesDbScope> dictionariesUnitOfWorkService, IDateRangeService dateRangeService)
            : base(allocationUnitOfWorkService, orgUnitService, principalProvider, dateRangeService)
        {
            _dictionariesUnitOfWorkService = dictionariesUnitOfWorkService;
        }

        protected override ReportDataSource<TimeColumnDto> GetDataSource(TimeColumnReportParameters parameters)
        {
            var reportDataSource = new ReportDataSource<TimeColumnDto>();
            var fromDate = parameters.FromDate.GetPreviousDayOfWeek(DayOfWeek.Monday);
            var weekRanges = DateRangeService.GetAllWeeksInDateRanges(fromDate, parameters.ToDate).ToList();
            var locationsDictionary = Locations.ToDictionary(k => k.Id, v => v.Name);

            using (var unitOfWork = AllocationUnitOfWorkService.Create())
            {
                var countedResultsForCompany = GetEmployeesWithWeeklyAllocation(unitOfWork, parameters.ReportScope, fromDate, parameters.ToDate)
                    .GroupBy(e => new { e.Week, e.LocationId, e.AllocationStatus })
                    .OrderBy(e => e.Key.LocationId)
                    .ThenBy(e => e.Key.AllocationStatus)
                    .Select(e => new SingleResultFromCountingEmployees
                    {
                        RowKey = e.Key.LocationId,
                        AllocationStatus = e.Key.AllocationStatus,
                        Week = e.Key.Week,
                        Count = e.Count()
                    }).ToList();

                reportDataSource.Rows = CreateRowsForEmployeeStatuses(locationsDictionary, countedResultsForCompany, weekRanges);
                reportDataSource.Rows.AddRange(CreateRowsForAllCompany(locationsDictionary, countedResultsForCompany, weekRanges));
            }

            reportDataSource.Headers = CreateReportDataSourceHeader(ReportResources.LocationsTitle, weekRanges);
            reportDataSource.Sumary = new List<object>();

            return reportDataSource;
        }
    }
}
