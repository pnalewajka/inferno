﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Reports.Dto;
using Smt.Atomic.Business.Reports.Models;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Business.Reports.Resources;

namespace Smt.Atomic.Business.Reports.Services
{
    public class SeniorityLevelsBaseReportService : BaseReportService<SeniorityLevelsDto, SeniorityLevelsReportParameters>
    {
        protected class SeniorityLevelsSingleRow
        {
            public long RowKey { get; set; }

            public long Level1 { get; set; }

            public long Level2 { get; set; }

            public long Level3 { get; set; }

            public long Level4 { get; set; }

            public long Level5 { get; set; }
        }
        
        public SeniorityLevelsBaseReportService(IUnitOfWorkService<IAllocationDbScope> allocationUnitOfWorkService,
            IOrgUnitService orgUnitService, IPrincipalProvider principalProvider)
            : base(allocationUnitOfWorkService, orgUnitService, principalProvider)
        {
        }

        protected IEnumerable<JobMatrixLevel> GetJobMatrixLevels(IUnitOfWork<IAllocationDbScope> unitOfWork, long? jobMatrixId = null)
        {
            if (jobMatrixId.HasValue && jobMatrixId.Value != 0)
            {
                return unitOfWork.Repositories.JobMatrixLevels.Where(j => (j.JobMatrixId == jobMatrixId));
            }

            return unitOfWork.Repositories.JobMatrixLevels;
        }


        protected override ReportDataSource<SeniorityLevelsDto> GetDataSource(SeniorityLevelsReportParameters parameters)
        {
            throw new MethodAccessException("It's base method. Please don't use it!");
        }

        protected List<string> CreateReportDataSourceHeader(IUnitOfWork<IAllocationDbScope> unitOfWork)
        {
            return GetJobMatrixLevels(unitOfWork).ToList().Select(j => j.Level).Distinct().OrderBy(j => j)
                    .Select(v => string.Format(ReportResources.JobmatrixLevelTitle, v)).ToList();
        }

        protected IList<SeniorityLevelsDto> CreateReportDataSourceRows(Dictionary<long, string> rowsReportDictionary, List<SeniorityLevelsSingleRow> seniorityLevels)
        {
            return rowsReportDictionary.GroupJoin(seniorityLevels, c => c.Key, p => p.RowKey, (c, ps) => new { c, ps })
                        .SelectMany(@t => @t.ps.DefaultIfEmpty(), (@t, p) =>
                                new SeniorityLevelsDto
                                {
                                    RowName = @t.c.Value,
                                    Level1 = p?.Level1 ?? 0,
                                    Level2 = p?.Level2 ?? 0,
                                    Level3 = p?.Level3 ?? 0,
                                    Level4 = p?.Level4 ?? 0,
                                    Level5 = p?.Level5 ?? 0
                                }).ToList();
        }

        protected List<object> CreateReportDataSourceSummary(List<SeniorityLevelsSingleRow> seniorityLevels)
        {
            return new List<object>
            {
                ReportResources.CompanyTotalText,
                seniorityLevels.Sum(j => j.Level1),
                seniorityLevels.Sum(j => j.Level2),
                seniorityLevels.Sum(j => j.Level3),
                seniorityLevels.Sum(j => j.Level4),
                seniorityLevels.Sum(j => j.Level5)
            };
        }
    }
}
