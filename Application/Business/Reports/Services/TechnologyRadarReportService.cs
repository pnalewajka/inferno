﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Reports.Dto;
using Smt.Atomic.Business.Reports.Enums;
using Smt.Atomic.Business.Reports.Interfaces;

namespace Smt.Atomic.Business.Reports.Services
{
    public class TechnologyRadarReportService : ITechnologyRadarReportService
    {
        private readonly ITechnologyRadarItemsService _technologyRadarItemsService;

        public TechnologyRadarReportService(ITechnologyRadarItemsService technologyRadarItemsService)
        {
            _technologyRadarItemsService = technologyRadarItemsService;
        }

        public IEnumerable<TechnologyRadarAreaDto> GetTechnologyRadarAreas()
        {
            var technologyRadarItems = _technologyRadarItemsService.GetItems().ToList();

            var areas = CreateTechnologyRadarAreaCollection(technologyRadarItems);
            return areas;
        }

        private static IEnumerable<TechnologyRadarAreaDto> CreateTechnologyRadarAreaCollection(IReadOnlyCollection<TechnologyRadarItemDto> technologyRadarItems)
        {
            return new List<TechnologyRadarAreaDto>
            {
                new TechnologyRadarAreaDto
                {
                    TechnologyRadarArea = TechnologyRadarArea.Platforms,
                    AreaItems = technologyRadarItems.Where(i => i.TechnologyRadarArea == TechnologyRadarArea.Platforms)
                },
                new TechnologyRadarAreaDto
                {
                    TechnologyRadarArea = TechnologyRadarArea.PatternsAndPractices,
                    AreaItems = technologyRadarItems.Where(i => i.TechnologyRadarArea == TechnologyRadarArea.PatternsAndPractices)
                },
                new TechnologyRadarAreaDto
                {
                    TechnologyRadarArea = TechnologyRadarArea.LanguagesAndFrameworks,
                    AreaItems = technologyRadarItems.Where(i => i.TechnologyRadarArea == TechnologyRadarArea.LanguagesAndFrameworks)
                },
                new TechnologyRadarAreaDto
                {
                    TechnologyRadarArea = TechnologyRadarArea.Tools,
                    AreaItems = technologyRadarItems.Where(i => i.TechnologyRadarArea == TechnologyRadarArea.Tools)
                }
            };
        }
    }
}
