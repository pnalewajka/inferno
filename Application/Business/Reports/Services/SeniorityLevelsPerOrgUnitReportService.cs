﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Reports.Dto;
using Smt.Atomic.Business.Reports.Interfaces;
using Smt.Atomic.Business.Reports.Models;
using Smt.Atomic.Business.Reports.Resources;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Reports.Services
{
    public class SeniorityLevelsPerOrgUnitReportService : SeniorityLevelsBaseReportService, ISeniorityLevelsPerOrgUnitReportService
    {
        private readonly IUnitOfWorkService<IOrganizationScope> _organizationUnitOfWorkService;
        
        private IEnumerable<OrgUnit> OrgUnits
        {
            get
            {
                using (var unitOfWork = _organizationUnitOfWorkService.Create())
                {
                    return unitOfWork.Repositories.OrgUnits.OrderBy(o => o.Id).ToList();
                }
            }
        }
        
        public SeniorityLevelsPerOrgUnitReportService(IUnitOfWorkService<IAllocationDbScope> allocationUnitOfWorkService, IOrgUnitService orgUnitService,
            IPrincipalProvider principalProvider, IUnitOfWorkService<IOrganizationScope> organizationUnitOfWorkService)
            : base(allocationUnitOfWorkService, orgUnitService, principalProvider)
        {
            _organizationUnitOfWorkService = organizationUnitOfWorkService;
        }

        protected override ReportDataSource<SeniorityLevelsDto> GetDataSource(SeniorityLevelsReportParameters seniorityLevelsReportParameters)
        {
            var reportDataSource = new ReportDataSource<SeniorityLevelsDto>();

            using (var unitOfWork = AllocationUnitOfWorkService.Create())
            {
                var jobMatrixPerOrgUnit =
                    GetEmployees(unitOfWork, seniorityLevelsReportParameters.ReportScope).Where(e => e.JobMatrixs.Any())
                        .SelectMany(jm => jm.JobMatrixs,
                            (employee, jobMatrix) => new {employee.OrgUnitId, JobMatrixId = jobMatrix.Id})
                        .Join(GetJobMatrixLevels(unitOfWork, seniorityLevelsReportParameters.JobMatrixId),
                            e => e.JobMatrixId, j => j.Id,
                            (e, j) => new {j.Level, e.OrgUnitId})
                        .GroupBy(e => new {e.OrgUnitId}).OrderBy(e => e.Key.OrgUnitId).ToList()
                        .Select(e => new SeniorityLevelsSingleRow
                        {
                            RowKey = e.Key.OrgUnitId,
                            Level1 = e.Count(c => c.Level == 1),
                            Level2 = e.Count(c => c.Level == 2),
                            Level3 = e.Count(c => c.Level == 3),
                            Level4 = e.Count(c => c.Level == 4),
                            Level5 = e.Count(c => c.Level == 5),
                        })
                        .ToList();

                reportDataSource.Headers = new List<string> {ReportResources.OrgUnitTitle};
                reportDataSource.Headers.AddRange(CreateReportDataSourceHeader(unitOfWork));
                
                reportDataSource.Sumary = CreateReportDataSourceSummary(jobMatrixPerOrgUnit);

                UpdateEmployeeCountInOrgUnit(jobMatrixPerOrgUnit);

                reportDataSource.Rows =
                    OrgUnits.GroupJoin(jobMatrixPerOrgUnit, c => c.Id, p => p.RowKey,
                        (c, ps) => new {c, ps})
                        .SelectMany(@t => @t.ps.DefaultIfEmpty(),
                            (@t, p) =>
                                new SeniorityLevelsDto(@t.c.Depth)
                                {
                                    RowName = @t.c.Name,
                                    Level1 = p?.Level1 ?? 0,
                                    Level2 = p?.Level2 ?? 0,
                                    Level3 = p?.Level3 ?? 0,
                                    Level4 = p?.Level4 ?? 0,
                                    Level5 = p?.Level5 ?? 0,
                                }).ToList();

                return reportDataSource;
            }
        }
        
        private void UpdateEmployeeCountInOrgUnit(List<SeniorityLevelsSingleRow> jobMatrixPerOrgUnit)
        {
            var sortedOrgUnit = OrgUnits.OrderByDescending(o => o.NameSortOrder);
            foreach (var row in sortedOrgUnit)
            {
                var childRow = jobMatrixPerOrgUnit.Find(j => j.RowKey == row.Id);

                if (childRow == null || row.ParentId == null)
                {
                    continue;
                }

                var parentRow = jobMatrixPerOrgUnit.Find(l => l.RowKey == row.ParentId);
                if (parentRow != null)
                {
                    jobMatrixPerOrgUnit.Find(j => j.RowKey == row.ParentId).Level1 += childRow.Level1;
                    jobMatrixPerOrgUnit.Find(j => j.RowKey == row.ParentId).Level2 += childRow.Level2;
                    jobMatrixPerOrgUnit.Find(j => j.RowKey == row.ParentId).Level3 += childRow.Level3;
                    jobMatrixPerOrgUnit.Find(j => j.RowKey == row.ParentId).Level4 += childRow.Level4;
                    jobMatrixPerOrgUnit.Find(j => j.RowKey == row.ParentId).Level5 += childRow.Level5;
                }
                else
                {
                    jobMatrixPerOrgUnit.Add(new SeniorityLevelsSingleRow
                    {
                        RowKey = row.ParentId.Value,
                        Level1 = childRow.Level1,
                        Level2 = childRow.Level2,
                        Level3 = childRow.Level3,
                        Level4 = childRow.Level4,
                        Level5 = childRow.Level5,
                    });
                }
            }
        }
    }
}