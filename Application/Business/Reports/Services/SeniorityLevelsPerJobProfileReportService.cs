﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Reports.Dto;
using Smt.Atomic.Business.Reports.Interfaces;
using Smt.Atomic.Business.Reports.Models;
using Smt.Atomic.Business.Reports.Resources;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Reports.Services
{
    public class SeniorityLevelsPerJobProfileReportService : SeniorityLevelsBaseReportService, ISeniorityLevelsPerJobProfileReportService
    {
        private const int JobMatrixLevelTotalNumber = 5;
        private const int UnassignedJobProfileValue = -1;

        private IEnumerable<JobProfile> GetAllJobProfiles(IUnitOfWork<IAllocationDbScope> unitOfWork)
        {
            return unitOfWork.Repositories.JobProfiles.OrderBy(j => j.Name).ToList();
        }

        public SeniorityLevelsPerJobProfileReportService(IUnitOfWorkService<IAllocationDbScope> allocationUnitOfWorkService, IOrgUnitService orgUnitService,
            IPrincipalProvider principalProvider) : base(allocationUnitOfWorkService, orgUnitService, principalProvider)
        {
        }

        protected override ReportDataSource<SeniorityLevelsDto> GetDataSource(SeniorityLevelsReportParameters seniorityLevelsReportParameters)
        {
            var reportDataSource = new ReportDataSource<SeniorityLevelsDto>();
            
            using (var unitOfWork = AllocationUnitOfWorkService.Create())
            {
                var jobProfilesDictionary = GetAllJobProfiles(unitOfWork).ToDictionary(k => k.Id, v => v.Name);

                var jobMatrixPerJobProfile =
                    GetEmployees(unitOfWork, seniorityLevelsReportParameters.ReportScope).Where(e => e.JobMatrixs.Any())
                        .SelectMany(jm => jm.JobMatrixs,
                            (employee, jobMatrix) =>
                                new { employee.JobProfiles, JobMatrixId = jobMatrix.Id })
                        .SelectMany(jp => jp.JobProfiles.DefaultIfEmpty(),
                            (employee, jobProfileParam) =>
                            new { employee.JobMatrixId, JobProfileId = jobProfileParam != null ? jobProfileParam.Id : UnassignedJobProfileValue })
                        .Join(GetJobMatrixLevels(unitOfWork, seniorityLevelsReportParameters.JobMatrixId), e => e.JobMatrixId, m => m.Id,
                            (e, m) => new {m.Level, e.JobProfileId})
                        .GroupBy(e => new {e.JobProfileId}).OrderBy(e => e.Key.JobProfileId).ToList()
                        .Select(e => new SeniorityLevelsSingleRow
                        {
                            RowKey = e.Key.JobProfileId,
                            Level1 = e.Count(c => c.Level == 1),
                            Level2 = e.Count(c => c.Level == 2),
                            Level3 = e.Count(c => c.Level == 3),
                            Level4 = e.Count(c => c.Level == 4),
                            Level5 = e.Count(c => c.Level == 5),
                        }).ToList();

                jobProfilesDictionary.Add(UnassignedJobProfileValue, ReportResources.UnassignedTitle);
                reportDataSource.Rows = CreateReportDataSourceRows(jobProfilesDictionary, jobMatrixPerJobProfile);

                var countedTotalResultForCompany =
                    GetEmployees(unitOfWork, seniorityLevelsReportParameters.ReportScope).Where(e => e.JobMatrixs.Any())
                        .SelectMany(jm => jm.JobMatrixs,
                            (employee, jobMatrix) => new { employee.JobProfiles, JobMatrixId = jobMatrix.Id })
                        .Join(GetJobMatrixLevels(unitOfWork, seniorityLevelsReportParameters.JobMatrixId), e => e.JobMatrixId, j => j.Id,
                            (e, j) => new { j.Level })
                        .GroupBy(e => new { e.Level }).OrderBy(e => e.Key.Level)
                        .Select(e => new { JobMatrixLevel = e.Key.Level, Count = e.Count() } )
                        .ToList();

                reportDataSource.Sumary = new List<object> { ReportResources.CompanyTotalText };
                for (int i = 1; i <= JobMatrixLevelTotalNumber; i++)
                {
                    var firstOrDefault = countedTotalResultForCompany.FirstOrDefault(c => c.JobMatrixLevel == i);
                    reportDataSource.Sumary.Add(firstOrDefault?.Count ?? 0);
                }

                reportDataSource.Headers = new List<string> { ReportResources.JobProfileTitle };
                reportDataSource.Headers.AddRange(CreateReportDataSourceHeader(unitOfWork));
            }

            return reportDataSource;
        }
    }
}
