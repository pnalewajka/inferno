﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Reports.Dto;
using Smt.Atomic.Business.Reports.Interfaces;
using Smt.Atomic.Business.Reports.Models;
using Smt.Atomic.Business.Reports.Resources;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Reports.Services
{
    public class TimeColumnPerLocationReportService : TimeColumnBaseReportService, ITimeColumnPerLocationReportService
    {
        private readonly IUnitOfWorkService<IDictionariesDbScope> _dictionariesUnitOfWorkService;

        private IEnumerable<Location> Locations
        {
            get
            {
                using (var unitOfWork = _dictionariesUnitOfWorkService.Create())
                {
                     return unitOfWork.Repositories.Locations.OrderBy(l => l.Name).ToList();
                }
            }
        }

        public TimeColumnPerLocationReportService(IUnitOfWorkService<IAllocationDbScope> allocationUnitOfWorkService,
            IOrgUnitService orgUnitService, IPrincipalProvider principalProvider,
            IUnitOfWorkService<IDictionariesDbScope> dictionariesUnitOfWorkService, IDateRangeService dateRangeService)
            : base(allocationUnitOfWorkService, orgUnitService, principalProvider, dateRangeService)
        {
            _dictionariesUnitOfWorkService = dictionariesUnitOfWorkService;
        }

        protected override ReportDataSource<TimeColumnDto> GetDataSource(TimeColumnReportParameters parameters)
        {
            AssignAllowedEmployeeStatuses(parameters.TimeColumnReportType);
            var reportDataSource = new ReportDataSource<TimeColumnDto>();
            var weekRanges = DateRangeService.GetAllWeeksInDateRanges(parameters.FromDate, parameters.ToDate).ToList();
            var fromDate = parameters.FromDate.GetPreviousDayOfWeek(DayOfWeek.Monday);
            var locationsDictionary = Locations.ToDictionary(k => k.Id, v => v.Name);

            using (var unitOfWork = AllocationUnitOfWorkService.Create())
            {
                var countedResultsForCompany =
                    GetEmployeesWithWeeklyAllocation(unitOfWork, parameters.ReportScope, fromDate, parameters.ToDate)
                        .GroupBy(e => new {e.Week, e.LocationId, e.EmployeeStatus})
                        .OrderBy(e => e.Key.LocationId)
                        .ThenBy(e => e.Key.EmployeeStatus)
                        .Select(e => new SingleResultFromCountingEmployees
                        {
                            RowKey = e.Key.LocationId,
                            EmployeeStatus = e.Key.EmployeeStatus,
                            Week = e.Key.Week,
                            Count = e.Count()
                        })
                        .ToList();
                
                reportDataSource.Rows = CreateRowsForEmployeeStatuses(locationsDictionary, countedResultsForCompany, weekRanges);
                reportDataSource.Rows.AddRange(CreateRowsForAllCompany(locationsDictionary, countedResultsForCompany, weekRanges));
            }

            reportDataSource.Headers = CreateReportDataSourceHeader(ReportResources.LocationsTitle, weekRanges);
            reportDataSource.Sumary = new List<object>();

            return reportDataSource;
        }
    }
}
