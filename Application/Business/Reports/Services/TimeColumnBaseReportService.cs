﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Reports.Dto;
using Smt.Atomic.Business.Reports.Models;
using Smt.Atomic.Business.Reports.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Reports.Services
{
    public class TimeColumnBaseReportService : BaseReportService<TimeColumnDto, TimeColumnReportParameters>
    {
        protected readonly IDateRangeService DateRangeService;

        protected EmployeeStatus[] AllowedEmployeeStatus;
        private readonly EmployeeStatus[] _activeEmployeesAllowedAllocationStatuses = { EmployeeStatus.Active, EmployeeStatus.Inactive, };
        private readonly EmployeeStatus[] _newHireLeaveEmployeesAllowedAllocationStatuses = { EmployeeStatus.NewHire, EmployeeStatus.Leave, };

        public TimeColumnBaseReportService(IUnitOfWorkService<IAllocationDbScope> allocationUnitOfWorkService,
            IOrgUnitService orgUnitService, IPrincipalProvider principalProvider, IDateRangeService dateRangeService)
            : base(allocationUnitOfWorkService, orgUnitService, principalProvider)
        {
            DateRangeService = dateRangeService;
        }

        protected IQueryable<EmployeesWithWeeklyAllocationField> GetEmployeesWithWeeklyAllocation(IUnitOfWork<IAllocationDbScope> unitOfWork, ReportScopeEnum reportScope, DateTime fromDate, DateTime toDate)
        {
            IQueryable<Employee> employees = unitOfWork.Repositories.Employees;
            if (reportScope == ReportScopeEnum.MyEmployees)
            {
                var currentEmployeeId = PrincipalProvider.Current.EmployeeId;
                var managerOrgUnitsIds = OrgUnitService.GetManagerOrgUnitDescendantIds(currentEmployeeId);

                employees = employees.Where(EmployeeBusinessLogic.IsEmployeeOf.Parametrize(currentEmployeeId, managerOrgUnitsIds));
            }

            var weeklyStatuses = unitOfWork.Repositories.WeeklyAllocationStatus.Where(
                    w => w.Week >= fromDate && w.Week <= toDate && AllowedEmployeeStatus.Contains(w.EmployeeStatus));

            return employees.Join(weeklyStatuses, e => e.Id, w => w.EmployeeId, (e, w) => new EmployeesWithWeeklyAllocationField
            { Week = w.Week, OrgUnitId = e.OrgUnitId, LocationId = e.LocationId, JobProfiles = e.JobProfiles, EmployeeStatus = w.EmployeeStatus });
        }
        
        protected override ReportDataSource<TimeColumnDto> GetDataSource(TimeColumnReportParameters parameters)
        {
            throw new MethodAccessException("It's base method. Please don't use it!");
        }

        protected void AssignAllowedEmployeeStatuses(TimeColumnReportType timeColumnReportType)
        {
            AllowedEmployeeStatus = _activeEmployeesAllowedAllocationStatuses;
            if (timeColumnReportType == TimeColumnReportType.NewHiredLeavingEmployee)
            {
                AllowedEmployeeStatus = _newHireLeaveEmployeesAllowedAllocationStatuses;
            }
        }

        protected List<TimeColumnDto> CreateRowsForEmployeeStatuses(Dictionary<long, string> rowsReportDictionary,
            List<SingleResultFromCountingEmployees> countedResultsForCompany, List<DateTime> weekRanges)
        {
            var reportDataSourceRows = new List<TimeColumnDto>();

            foreach (var rowReportDictionary in rowsReportDictionary)
            {
                foreach (EmployeeStatus employeeStatus in AllowedEmployeeStatus)
                {
                    var singleTimeColumnRow = CreateStatusRowForEmployeeStatus(rowReportDictionary, employeeStatus, weekRanges, countedResultsForCompany);
                    reportDataSourceRows.Add(singleTimeColumnRow);
                }

                var singleTimeColumnTotal = CreateTotalRowForEmployeeStatus(rowReportDictionary, weekRanges,countedResultsForCompany);
                reportDataSourceRows.Add(singleTimeColumnTotal);
            }

            return reportDataSourceRows;
        }

        protected List<TimeColumnDto> CreateRowsForAllCompany(Dictionary<long, string> rowsReportDictionary, List<SingleResultFromCountingEmployees> countedResultsForCompany, List<DateTime> weekRanges)
        {
            var reportDataSourceRows = new List<TimeColumnDto>();
            foreach (EmployeeStatus employeeStatus in AllowedEmployeeStatus)
            {
                var singleTimeColumnRow = CreateStatusRowForAllCompany(employeeStatus, weekRanges, countedResultsForCompany);
                reportDataSourceRows.Add(singleTimeColumnRow);
            }

            var singleTotalRowForCompanyTotal = CreateTotalRowForAllCompany(weekRanges, countedResultsForCompany);
            reportDataSourceRows.Add(singleTotalRowForCompanyTotal);

            return reportDataSourceRows;
        }

        protected IList<string> CreateReportDataSourceHeader (string headerTitle, List<DateTime> weekRanges)
        {
            var headerList = new List<string> { headerTitle, ReportResources.AllocationStatusTitle };
            var columnNames = weekRanges.Select(w => w.ToShortDateString());
            headerList.AddRange(columnNames);

            return headerList;
        }


        protected TimeColumnDto CreateTotalRowForEmployeeStatus(KeyValuePair<long, string> rowKeyValuePair, List<DateTime> weekRanges,
            List<SingleResultFromCountingEmployees> countedResultsForCompany, int? depth = null)
        {
            var timeColumn = new TimeColumnDto(depth)
            {
                RowName = rowKeyValuePair.Value,
                StatusName = ReportResources.StatusTotal,
            };

            foreach (var singleValue in weekRanges.Select(week => countedResultsForCompany.Where(c =>
                c.RowKey.HasValue && c.RowKey.Value == rowKeyValuePair.Key && c.Week == week)).Select(l => l.Sum(s => s.Count)))
            {
                timeColumn.WeekValues.Add(singleValue);
            }

            return timeColumn;
        }

        protected TimeColumnDto CreateStatusRowForEmployeeStatus(KeyValuePair<long, string> rowKeyValuePair, EmployeeStatus employeeStatus,
            List<DateTime> weekRanges, List<SingleResultFromCountingEmployees> countedResultsForCompany, int? depth = null)
        {
            var timeColumn = new TimeColumnDto(depth)
            {
                RowName = rowKeyValuePair.Value,
                StatusName = employeeStatus.GetDescription(),
            };

            foreach (var singleValue in weekRanges.Select(week => countedResultsForCompany.FirstOrDefault(c =>
                c.RowKey.HasValue && c.RowKey.Value == rowKeyValuePair.Key && c.Week == week && c.EmployeeStatus == employeeStatus)))
            {
                timeColumn.WeekValues.Add(singleValue?.Count ?? 0);
            }

            return timeColumn;
        }

        private TimeColumnDto CreateTotalRowForAllCompany(List<DateTime> weekRanges, List<SingleResultFromCountingEmployees> countedResultsForCompany)
        {
            var timeColumn = new TimeColumnDto
            {
                RowName = ReportResources.CompanyTotalText,
                StatusName = ReportResources.StatusTotal,
            };

            foreach (var singleValue in weekRanges.Select(week => countedResultsForCompany.Where(c => c.Week == week)).Select(l => l.Sum(s => s.Count)))
            {
                timeColumn.WeekValues.Add(singleValue);
            }

            return timeColumn;
        }

        private TimeColumnDto CreateStatusRowForAllCompany(EmployeeStatus employeeStatus, List<DateTime> weekRanges,
            List<SingleResultFromCountingEmployees> countedResultsForCompany)
        {
            var timeColumn = new TimeColumnDto
            {
                RowName = ReportResources.CompanyTotalText,
                StatusName = employeeStatus.GetDescription(),
            };

            foreach (var singleValue in weekRanges.Select(week => countedResultsForCompany.Where(c =>
                c.Week == week && c.EmployeeStatus == employeeStatus)).Select(l => l.Sum(s => s.Count)))
            {
                timeColumn.WeekValues.Add(singleValue);
            }

            return timeColumn;
        }
    }
}
