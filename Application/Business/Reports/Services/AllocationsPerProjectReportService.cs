﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Reports.Dto;
using Smt.Atomic.Business.Reports.Interfaces;
using Smt.Atomic.Business.Reports.Models;
using Smt.Atomic.Business.Reports.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Reports.Services
{
    public class AllocationsPerProjectReportService : BaseReportService<AllocationsPerProjectDto, AllocationPerProjectReportParameters>, IAllocationsPerProjectReportService
    {
        private readonly IDateRangeService _dateRangeService;

        private class SingleResultFromAllocation
        {
            public long ProjectId { get; set; }

            public string ProjectName { get; set; }

            public string ProjectOrgUnitCode { get; set; }

            public string ProjectOrgUnitName { get; set; }

            public long EmployeeId { get; set; }

            public string EmployeeFullName { get; set; }

            public string EmployeeOrgUnitCode { get; set; }

            public string EmployeeOrgUnitName { get; set; }

            public string JobProfileNames { get; set; }

            public string Apn { get; set; }

            public string KupferwerkProjectId { get; set; }

            public DateTime Day { get; set; }

            public decimal Hours { get; set; }
        }

        private class EmployeeAbsenceSimple
        {
            public long EmployeeId { get; set; }

            public DateTime From { get; set; }

            public DateTime To { get; set; }
        }

        public AllocationsPerProjectReportService(
            IUnitOfWorkService<IAllocationDbScope> allocationUnitOfWorkService,
            IOrgUnitService orgUnitService,
            IPrincipalProvider principalProvider,
            IDateRangeService dateRangeService)
            : base(allocationUnitOfWorkService, orgUnitService, principalProvider)
        {
            _dateRangeService = dateRangeService;
        }

        protected override ReportDataSource<AllocationsPerProjectDto> GetDataSource(AllocationPerProjectReportParameters parameters)
        {
            var reportDataSource = new ReportDataSource<AllocationsPerProjectDto>();

            var dayRanges = _dateRangeService.GetDaysInRange(parameters.FromDate, parameters.ToDate).ToList();



            using (var unitOfWork = AllocationUnitOfWorkService.Create())
            {
                var countedResultsForAllocations =
                    GetAllocationWithfilterProjects(unitOfWork, parameters.ProjectFilterType, parameters.FromDate, parameters.ToDate)
                        .Select(jp => new
                        {
                            JobProfileNames = jp.Employee.JobProfiles.Select(p => p.Name).ToList(),
                            ProjectOrgUnitName = jp.Project.ProjectSetup.OrgUnit.Name,
                            ProjectOrgUnitCode = jp.Project.ProjectSetup.OrgUnit.Code,
                            EmployeeOrgUnitName = jp.Employee.OrgUnit.Name,
                            EmployeeOrgUnitCode = jp.Employee.OrgUnit.Code,
                            jp.ProjectId,
                            ClientProjectName = ProjectBusinessLogic.ClientProjectName.Call(jp.Project),
                            jp.EmployeeId,
                            jp.Employee.FirstName,
                            jp.Employee.LastName,
                            jp.Day,
                            jp.IsWorkDay,
                            jp.Hours,
                            jp.Project.Apn,
                            jp.Project.KupferwerkProjectId,
                        })
                        .ToList()
                        .Select(e => new SingleResultFromAllocation
                        {
                            ProjectId = e.ProjectId,
                            ProjectName = e.ClientProjectName,
                            ProjectOrgUnitCode = e.ProjectOrgUnitCode,
                            ProjectOrgUnitName = e.ProjectOrgUnitName,
                            EmployeeId = e.EmployeeId,
                            EmployeeFullName = $"{e.FirstName} {e.LastName}",
                            EmployeeOrgUnitCode = e.EmployeeOrgUnitCode,
                            EmployeeOrgUnitName = e.EmployeeOrgUnitName,
                            JobProfileNames = string.Join(", ", e.JobProfileNames),
                            Apn = e.Apn ?? "",
                            KupferwerkProjectId = e.KupferwerkProjectId ?? "",
                            Day = e.Day,
                            Hours = e.IsWorkDay ? e.Hours : 0
                        })
                        .OrderBy(e => e.ProjectName)
                        .ThenBy(e => e.JobProfileNames)
                        .ThenBy(e => e.EmployeeFullName)
                        .ToList();

                if (parameters.SkipAbsenceDays)
                {
                    var absences = unitOfWork.Repositories.EmployeeAbsences
                    .Where(a => a.From >= parameters.FromDate || a.To <= parameters.ToDate)
                    .Select(a => new EmployeeAbsenceSimple
                    {
                        From = a.From,
                        To = a.To,
                        EmployeeId = a.EmployeeId
                    })
                    .ToList();

                    countedResultsForAllocations = countedResultsForAllocations.Where(a => IsNotAbsenceDay(absences, a.EmployeeId, a.Day)).ToList();
                }

                reportDataSource.Rows = CreateRowsForEmployeeStatuses(countedResultsForAllocations, dayRanges);
            }

            reportDataSource.Headers = CreateReportDataSourceHeader(dayRanges);

            return reportDataSource;
        }

        private bool IsNotAbsenceDay(List<EmployeeAbsenceSimple> employeeAbsenceShortList, long employeeId, DateTime day)
        {
            return !employeeAbsenceShortList
                .Where(b => b.EmployeeId == employeeId)
                .Where(b => b.From <= day && b.To >= day).Any();
        }

        private IQueryable<AllocationDailyRecord> GetAllocationWithfilterProjects(IUnitOfWork<IAllocationDbScope> unitOfWork, ProjectFilterTypeEnum reportScope,
            DateTime fromDate, DateTime toDate)
        {
            var allocationDailyRecords = unitOfWork.Repositories.AllocationDailyRecords.Where(a => a.Day >= fromDate && a.Day <= toDate);

            if (reportScope == ProjectFilterTypeEnum.AllProjects)
            {
                return allocationDailyRecords;
            }

            var currentEmployeeId = PrincipalProvider.Current.EmployeeId;
            var managerOrgUnitsIds = OrgUnitService.GetManagerOrgUnitDescendantIds(currentEmployeeId);

            return allocationDailyRecords.Where(e => (e.Project.ProjectSetup.ProjectManagerId.HasValue && e.Project.ProjectSetup.ProjectManagerId.Value == currentEmployeeId)
                            || (e.Project.ProjectSetup.OrgUnitId != 0 && managerOrgUnitsIds.Any(id => id == e.Project.ProjectSetup.OrgUnitId)));
        }

        private List<AllocationsPerProjectDto> CreateRowsForEmployeeStatuses(List<SingleResultFromAllocation> countedResultsForAllocations, List<DateTime> dayRanges)
        {
            var reportDataSourceRows = new List<AllocationsPerProjectDto>();

            var groupedReportRows = countedResultsForAllocations.GroupBy(pe => new { pe.ProjectId, JobProfileName = pe.JobProfileNames, pe.EmployeeId });
            foreach (var singleReportRow in groupedReportRows)
            {
                var singleTimeColumnRow = CreateStatusRowForEmployeeStatus(singleReportRow.Key.EmployeeId, dayRanges, singleReportRow.ToList());

                reportDataSourceRows.Add(singleTimeColumnRow);
            }

            return reportDataSourceRows;
        }

        private AllocationsPerProjectDto CreateStatusRowForEmployeeStatus(long currentEmployeeId,
         List<DateTime> dayRanges, List<SingleResultFromAllocation> employeeAllocationList)
        {
            var firstRow = employeeAllocationList.First(c => c.EmployeeId == currentEmployeeId);

            var timeColumn = new AllocationsPerProjectDto
            {
                ProjectName = firstRow.ProjectName,
                JobProfileName = firstRow.JobProfileNames,
                EmployeeName = firstRow.EmployeeFullName,
                ProjectOrgUnitCode = firstRow.ProjectOrgUnitCode,
                ProjectOrgUnitName = firstRow.ProjectOrgUnitName,
                EmployeeOrgUnitCode = firstRow.EmployeeOrgUnitCode,
                EmployeeOrgUnitName = firstRow.EmployeeOrgUnitName,
                Apn = firstRow.Apn,
                KupferwerkProjectId = firstRow.KupferwerkProjectId
            };

            var employeeAllocationDictionary = employeeAllocationList.GroupBy(cr => cr.Day).ToDictionary(cr => cr.Key, c => c.Sum(d => d.Hours));

            foreach (var singleValue in dayRanges)
            {
                timeColumn.WeekValues.Add(employeeAllocationDictionary.ContainsKey(singleValue) ? employeeAllocationDictionary[singleValue] : 0);
            }

            return timeColumn;
        }

        protected IList<string> CreateReportDataSourceHeader(List<DateTime> dayRanges)
        {
            var headerList = new List<string>
            {
                ReportResources.ProjectTitle,
                ReportResources.ProjectOrgUnitCodeTitle,
                ReportResources.ProjectOrgUnitNameTitle,
                ReportResources.JobProfileTitle,
                ReportResources.EmployeeTitle,
                ReportResources.EmployeeOrgUnitCodeTitle,
                ReportResources.EmployeeOrgUnitNameTitle,
                ReportResources.ApnTitle,
                ReportResources.KupferwerkProjectIdTitle
            };

            var columnNames = dayRanges.Select(w => w.ToShortDateString());
            headerList.AddRange(columnNames);

            return headerList;
        }
    }
}
