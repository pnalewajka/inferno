﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlassian.Jira;
using Smt.Atomic.Business.Reports.Dto;
using Smt.Atomic.Business.Reports.Enums;
using Smt.Atomic.Business.Reports.Interfaces;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Jira.Interfaces;

namespace Smt.Atomic.Business.Reports.Services
{
    internal class TechnologyRadarItemsService : ITechnologyRadarItemsService
    {
        private static readonly string _techRadarProjectKey = "TECHRADAR";
        private static readonly string _radarStatusField = "Radar Status";
        private static readonly string _areaField = "Area";
        private static readonly string _technologyType = "Technology";
        private static readonly string _publicDescriptionField = "Public Description";

        private readonly IJiraService _jiraService;

        public TechnologyRadarItemsService(IJiraService jiraService)
        {
            _jiraService = jiraService;
        }
        
        public IEnumerable<TechnologyRadarItemDto> GetItems()
        {
            using (var jiraWrapper = _jiraService.CreateReadOnly())
            {
                var technologyRadarIssues = jiraWrapper.GetIssuesByQuery(x => x.Project == _techRadarProjectKey && x.Type == _technologyType);

                return technologyRadarIssues.Select(MapIssueToTechnologyRadarItem).Where(technologyRadarItem => technologyRadarItem != null);
            }
        }

        private static TechnologyRadarItemDto MapIssueToTechnologyRadarItem(Issue issue)
        {
            try
            {
                TechnologyRadarStatus technologyRadarStatus = GetRadarStatus(issue);
                TechnologyRadarArea technologyRadarArea = GetArea(issue);

                var wasIssueChangedInLastMonth = issue.Updated.HasValue && issue.Updated.Value.Date.AddMonths(1) > DateTime.Now;

                return new TechnologyRadarItemDto
                {
                    Subject = issue.Summary,
                    Description = issue.CustomFields[_publicDescriptionField]?.Values?.FirstOrDefault() ?? string.Empty,
                    Status = technologyRadarStatus,
                    TechnologyRadarArea = technologyRadarArea,
                    Movement = wasIssueChangedInLastMonth ? TechnologyRadarMovement.Changed : TechnologyRadarMovement.Unchanged
                };
            }
            catch (KeyNotFoundException)
            {
                return null;
            }
        }

        private static TechnologyRadarStatus GetRadarStatus(Issue issue)
        {
            var radarStatusString = issue.CustomFields[_radarStatusField].Values[0];

            return EnumHelper.GetEnumValue<TechnologyRadarStatus>(radarStatusString);
        }

        private static TechnologyRadarArea GetArea(Issue issue)
        {
            var areaField = issue.CustomFields[_areaField];

            if (areaField == null)
            {
                throw new KeyNotFoundException();
            }

            var areaString = areaField.Values[0];

            return EnumHelper.GetEnumValue<TechnologyRadarArea>(ExtractAreaName(areaString));
        }

        private static string ExtractAreaName(string areaString)
        {
            return areaString.Replace(" & ", "And");
        }
    }
}