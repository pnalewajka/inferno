﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Reports.Interfaces;
using Smt.Atomic.Business.Reports.Models;
using Smt.Atomic.Business.Reports.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using System.Data.Entity;
using System.Linq.Expressions;
using Smt.Atomic.Business.Reports.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Data.Entities.BusinessLogic;

namespace Smt.Atomic.Business.Reports.Services
{
    public class UtilizationReportService : IUtilizationReportService
    {
        private const string CellValueFormat = "N1";
        private const string CellValuePercentageFormat = "P1";
        private const string NoValueText = "-";

        private class IntermidiateAggregation
        {
            public long Year { get; set; }
            public long Month { get; set; }
            public long? CategoryId { get; set; }
            public long? CategoryOrder { get; set; }
            public bool Billable { get; set; }
            public bool IsAbsence { get; set; }
            public string CategoryName { get; set; }
            public decimal Sum { get; set; }
        }

        private class MonthlyAggregation
        {
            public long Year { get; set; }
            public long Month { get; set; }
            public decimal Sum { get; set; }
        }

        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _timeTrackingUnitOfWorkService;
        private readonly IUnitOfWorkService<IDictionariesDbScope> _dictionariesUnitOfWorkService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly ITimeService _timeService;

        public UtilizationReportService(
            IUnitOfWorkService<ITimeTrackingDbScope> timeTrackingUnitOfWorkService,
            IUnitOfWorkService<IDictionariesDbScope> dictionariesUnitOfWorkService,
            ISystemParameterService systemParameterService,
            ITimeService timeService)
        {
            _timeTrackingUnitOfWorkService = timeTrackingUnitOfWorkService;
            _dictionariesUnitOfWorkService = dictionariesUnitOfWorkService;
            _systemParameterService = systemParameterService;
            _timeService = timeService;
        }

        public IEnumerable<UtilizationCategoryGroupModel> GetUtilizationReportGroupDataModel(IUtilizationReportSearch model, DateTime forDate, IReadOnlyCollection<long> utilizationCategories)
        {
            using (var unitOfWork = _timeTrackingUnitOfWorkService.Create())
            {
                var timeReportData = GetTimeReportRowsByOrgUnitId(unitOfWork, model)
                    .Where(trr => trr.TimeReport.Year == forDate.Year && trr.TimeReport.Month == forDate.Month)
                    .Where(trr => (model.MustIncludeEntriesWithoutCategory && !trr.Project.UtilizationCategoryId.HasValue) || (trr.Project.UtilizationCategoryId.HasValue && utilizationCategories.Contains(trr.Project.UtilizationCategoryId.Value)) || (!model.MustIncludeEntriesWithoutCategory && !utilizationCategories.Any()))
                    .ToList();

                foreach (var category in timeReportData.GroupBy(c => new { c.Project.UtilizationCategoryId, DisplayName = c.Project.UtilizationCategory != null ? c.Project.UtilizationCategory.DisplayName : UtilizationReportResources.NoCategoryLabel }))
                {
                    yield return new UtilizationCategoryGroupModel
                    {
                        UtilizationCategoryName = category.Key.DisplayName,
                        UtilizationCategoryHours = category.Sum(c => c.DailyEntries.Sum(h => h.Hours))
                    };

                    foreach (var projectGroup in category.GroupBy(g => g.Project))
                    {
                        yield return new UtilizationCategoryGroupModel
                        {
                            ProjectName = ProjectBusinessLogic.ProjectName.Call(projectGroup.Key),
                            ProjectHours = projectGroup.Sum(c => c.DailyEntries.Sum(h => h.Hours))
                        };

                        foreach (var employeeGroup in projectGroup.GroupBy(pg => pg.TimeReport.Employee))
                        {
                            yield return new UtilizationCategoryGroupModel
                            {
                                EmployeeName = employeeGroup.Key.FullName,
                                EmployeeHours = employeeGroup.Sum(c => c.DailyEntries.Sum(h => h.Hours))
                            };
                        }
                    }
                }
            }
        }

        public IEnumerable<UtilizationReportRowModel> GetUtilizationReportRowModels(IUtilizationReportSearch model)
        {
            using (var unitOfWork = _timeTrackingUnitOfWorkService.Create())
            {
                var timeReportRows = GetTimeReportRowsByOrgUnitId(unitOfWork, model);
                var result = new List<UtilizationReportRowModel>();
                var startDate = ReportDefaultStartDate(model.Months);

                timeReportRows = timeReportRows = timeReportRows.Where(t => t.TimeReport.Year > startDate.Year || (t.TimeReport.Year == startDate.Year && t.TimeReport.Month >= startDate.Month));

                var rawAggregations = GetIntermidiateAgregations(timeReportRows);

                if (!rawAggregations.Any())
                {
                    return result;
                }

                // grand total row
                var grandTotalData = GetMonthlyAggregations(rawAggregations, r => true);
                var grandTotalRow = BuildUtilizationReportRowModel(grandTotalData, model.DisplayType, model.Months, null, null, UtilizationReportResources.GrandTotalLabel, isChartRow: true);
                result.Add(grandTotalRow);

                // billable commercial
                var commercialUtilizationCategory = GetCommercialUtilizationCategory();
                var billableCommercialData = GetMonthlyAggregations(rawAggregations, r => r.Billable && r.CategoryId == commercialUtilizationCategory.Id);
                var billableCommercialRow = BuildUtilizationReportRowModel(billableCommercialData, model.DisplayType, model.Months, grandTotalRow, commercialUtilizationCategory, billability: true, isChartRow: true, canDrill: true, canExport: false);
                result.Add(billableCommercialRow);

                var nonbillableCommercialData = GetMonthlyAggregations(rawAggregations, r => !r.Billable && r.CategoryId == commercialUtilizationCategory.Id);
                var nonbillableCommercialRow = BuildUtilizationReportRowModel(nonbillableCommercialData, model.DisplayType, model.Months, grandTotalRow, commercialUtilizationCategory, billability: false, canDrill: true, canExport: false);
                result.Add(nonbillableCommercialRow);

                // add rows for rest of utilization categories
                foreach (var category in rawAggregations
                    .Where(r => r.CategoryId != commercialUtilizationCategory.Id)
                    .GroupBy(g => new { g.CategoryName, g.CategoryId, g.CategoryOrder })
                    .OrderBy(g => g.Key.CategoryOrder ?? int.MaxValue - 1))
                {
                    var utilizationCategory = category.Key.CategoryId.HasValue
                        ? new UtilizationCategory
                        {
                            Id = category.Key.CategoryId.Value,
                            DisplayName = category.Key.CategoryName,
                        }
                        : null;

                    var categoryData = GetMonthlyAggregations(rawAggregations, r => r.CategoryId != commercialUtilizationCategory.Id && r.CategoryId == category.Key.CategoryId);
                    var categoryRow = BuildUtilizationReportRowModel(categoryData, model.DisplayType, model.Months, grandTotalRow, utilizationCategory, canDrill: true, canExport: true);
                    result.Add(categoryRow);
                }

                // add row containing percents of billable hours
                var billablePercentageRow = BuildPercentUtilizationReportRowModel(result[1], result[0], UtilizationReportResources.PercentOfBilledLabel);
                result.Add(billablePercentageRow);

                var withoutAbsenceData = GetMonthlyAggregations(rawAggregations, r => !r.IsAbsence);
                var withoutAbsenceRow = BuildUtilizationReportRowModel(withoutAbsenceData, model.DisplayType, model.Months);

                // add row containing percents of billable hours (without absences)
                var withoutAbsencePercentageRow = BuildPercentUtilizationReportRowModel(result[1], withoutAbsenceRow, UtilizationReportResources.PercentOfBilledLabelWithoutAbsences);
                result.Add(withoutAbsencePercentageRow);

                return result;
            }
        }

        private static List<MonthlyAggregation> GetMonthlyAggregations(List<IntermidiateAggregation> rawAggregations, Func<IntermidiateAggregation, bool> filteringPredicate)
        {
            return rawAggregations
                .Where(filteringPredicate)
                .GroupBy(a => new { a.Year, a.Month })
                .Select(g => new MonthlyAggregation
                {
                    Year = g.Key.Year,
                    Month = g.Key.Month,
                    Sum = g.Select(r => r.Sum).DefaultIfEmpty().Sum()
                }).ToList();
        }

        private static List<IntermidiateAggregation> GetIntermidiateAgregations(IQueryable<TimeReportRow> timeReportRows)
        {
            var result =
                from row in timeReportRows
                let utilizationCategory = row.AbsenceType != null
                    ? (row.AbsenceType.AbsenceProject != null
                        ? row.AbsenceType.AbsenceProject.UtilizationCategory
                        : null)
                    : row.Project.UtilizationCategory
                group row by new
                {
                    row.TimeReport.Year,
                    row.TimeReport.Month,
                    CategoryId = utilizationCategory.Id,
                    CategoryOrder = utilizationCategory.Order,
                    CategoryName = utilizationCategory.DisplayName,
                    Billable = !row.AttributeValues.Any(v => v.Features.HasFlag(AttributeValueFeature.NonBillable)),
                    IsAbsence = row.Project.TimeTrackingType == TimeTrackingProjectType.Absence
                } into rowGroup
                select new IntermidiateAggregation
                {
                    Year = rowGroup.Key.Year,
                    Month = rowGroup.Key.Month,
                    CategoryId = rowGroup.Key.CategoryId,
                    Billable = rowGroup.Key.Billable,
                    CategoryName = rowGroup.Key.CategoryName,
                    CategoryOrder = rowGroup.Key.CategoryOrder,
                    IsAbsence = rowGroup.Key.IsAbsence,
                    Sum = rowGroup.SelectMany(g2 => g2.DailyEntries)
                        .Select(e => e.Hours)
                        .DefaultIfEmpty()
                        .Sum()
                };

            return result.ToList();
        }

        private DateTime ReportDefaultStartDate(int monthsInPast)
        {
            if(monthsInPast <= 6)
            {
                monthsInPast = 6;
            }

            if (monthsInPast > 12)
            {
                monthsInPast = 12;
            }

            return _timeService.GetCurrentDate().GetFirstDayInMonth().AddMonths(-monthsInPast);
        }

        private IQueryable<TimeReportRow> GetTimeReportRowsByOrgUnitId(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, IUtilizationReportSearch model)
        {
            if (!model.OrgUnitId.HasValue)
            {
                throw new MissingFieldException(nameof(IUtilizationReportSearch), nameof(IUtilizationReportSearch.OrgUnitId));
            }

            unitOfWork
                .Repositories
                .TimeReportRows
                .SetTimeout(120);

            var orgUnitPathPart = $"/{model.OrgUnitId.Value}/";
            
            var timeReportRows = unitOfWork
                .Repositories
                .TimeReportRows
                .AsNoTracking()
                .Where(
                    r => r.TimeReport.Employee.IsProjectContributor
                         && r.TimeReport.Employee.OrgUnit.Path.Contains(orgUnitPathPart)
                         && r.TimeReport.Employee.EmployeeFeatures.HasFlag(EmployeeFeatures.ShowInUtilizationReport)
                )
                .Where(trr =>
                    !model.LocationId.HasValue || trr.TimeReport.Employee.LocationId == model.LocationId.Value)
                .Where(trr =>
                    !model.JobProfileId.HasValue ||
                    trr.TimeReport.Employee.JobProfiles.Any(jp => jp.Id == model.JobProfileId.Value))
                .Where(trr =>
                    !model.LevelId.HasValue ||
                    trr.TimeReport.Employee.JobMatrixs.Any(jm => jm.Level == model.LevelId.Value))
                .Where(trr => trr.OvertimeVariant != HourlyRateType.OverTimeBank);

            if (model.HourStatus == HourStatus.Accepted)
            {
                timeReportRows = timeReportRows.Where(r => r.TimeReport.Status == TimeReportStatus.Accepted);
            }

            var reportIds = timeReportRows.Select(trr => trr.Id);

            return unitOfWork
                .Repositories
                .TimeReportRows
                .AsNoTracking()
                .Include(tr => tr.TimeReport)
                .Include(tr => tr.DailyEntries)
                .Include(tr => tr.Project.UtilizationCategory)
                .Include(tr => tr.AbsenceType.AbsenceProject)
                .Include(tr => tr.AbsenceType.AbsenceProject.UtilizationCategory)
                .Include(tr => tr.AttributeValues)
                .Join(reportIds, tr => tr.Id, id => id, (tr, id) => tr);
        }

        private static string CalculateCellValueText(decimal value, UtilizationDisplayType displayType, decimal? totalValue)
        {
            if (totalValue.HasValue && displayType == UtilizationDisplayType.Percentage)
            {
                if (totalValue.Value == 0)
                {
                    return NoValueText;
                }

                return (value / totalValue.Value).ToString(CellValuePercentageFormat);
            }

            return value.ToString(CellValueFormat);
        }

        private UtilizationReportRowModel BuildUtilizationReportRowModel(
            IList<MonthlyAggregation> aggregation,
            UtilizationDisplayType displayType,
            int monthsInPast,
            UtilizationReportRowModel grandTotalRow = null,
            UtilizationCategory utilizationCategory = null,
            string customDisplayName = null,
            bool? billability = null,
            bool isChartRow = false,
            bool? canDrill = null,
            bool? canExport = null)
        {
            if (aggregation == null)
            {
                throw new ArgumentNullException(nameof(aggregation));
            }

            var fromDate = ReportDefaultStartDate(monthsInPast);
            var toDate = _timeService.GetCurrentDate().GetFirstDayInMonth();

            var cells = new List<UtilizationReportCellModel>();

            var currentMonth = fromDate;

            while (currentMonth <= toDate)
            {
                var currentMonthValues = aggregation.FirstOrDefault(x => x.Year == currentMonth.Year && x.Month == currentMonth.Month);

                UtilizationReportCellModel cell = new UtilizationReportCellModel
                {
                    Date = currentMonth,
                    Value = currentMonthValues?.Sum ?? 0
                };

                cell.TextValue = CalculateCellValueText(cell.Value, displayType, grandTotalRow?.Cells.FirstOrDefault(x => x.Date.Year == currentMonth.Year && x.Date.Month == currentMonth.Month)?.Value);
                cells.Add(cell);

                currentMonth = currentMonth.AddMonths(1);
            }

            var label = utilizationCategory != null
                        ? utilizationCategory.DisplayName
                        : (string.IsNullOrEmpty(customDisplayName) ? UtilizationReportResources.NoCategoryLabel : customDisplayName);

            if (billability == true)
            {
                label += UtilizationReportResources.BilledSuffixLabel;
            }
            else if (billability == false)
            {
                label += UtilizationReportResources.FreeSuffixLabel;
            }

            var buildUtilizationReportRowModel = new UtilizationReportRowModel
            {
                Label = label,
                Cells = cells,
                IsChartRow = isChartRow,
                UtilizationCategoryId = utilizationCategory?.Id,
                Billability = billability
            };

            if (canDrill.HasValue)
            {
                buildUtilizationReportRowModel.CanDrillData = canDrill.Value;
            }

            if (canExport.HasValue)
            {
                buildUtilizationReportRowModel.CanExportData = canExport.Value;
            }

            return buildUtilizationReportRowModel;
        }

        private Expression<Func<TimeReportRow, bool>> FilterBillability(bool? billability)
        {
            if (!billability.HasValue)
            {
                return trr => true;
            }

            if (billability.Value)
            {
                return trr => trr.AttributeValues.All(va => !va.Features.HasFlag(AttributeValueFeature.NonBillable));
            }

            return trr => trr.AttributeValues.Any(va => va.Features.HasFlag(AttributeValueFeature.NonBillable));
        }

        private UtilizationReportRowModel BuildPercentUtilizationReportRowModel(UtilizationReportRowModel numeratorRow, UtilizationReportRowModel denominatorRow, string label)
        {
            var percentCells = new List<UtilizationReportCellModel>();

            var numeratorCellsArray = numeratorRow.Cells.ToArray();
            var denominatorCellsArray = denominatorRow.Cells.ToArray();

            for (int i = 0; i < denominatorCellsArray.Length; i++)
            {
                var percentCell = new UtilizationReportCellModel { Date = denominatorCellsArray[i].Date };

                if (denominatorCellsArray[i].Value == 0)
                {
                    percentCell.Value = 0;
                    percentCell.TextValue = "-";
                }
                else
                {
                    var value = numeratorCellsArray[i].Value / denominatorCellsArray[i].Value;
                    percentCell.Value = value;
                    percentCell.TextValue = value.ToString(CellValuePercentageFormat);
                }

                percentCells.Add(percentCell);
            }

            return new UtilizationReportRowModel
            {
                Label = label,
                Cells = percentCells
            };
        }

        private UtilizationCategory GetCommercialUtilizationCategory()
        {
            using (var uow = _dictionariesUnitOfWorkService.Create())
            {
                var navisionCodeOfCommercialUtilizationCategory = _systemParameterService.GetParameter<string>(ParameterKeys.NavisionCodeOfCommercialUtilizationCategory);

                return uow.Repositories.UtilizationCategories
                          .Single(x => x.NavisionCode == navisionCodeOfCommercialUtilizationCategory);
            }
        }

        private IQueryable<TimeReportRow> FilterTimeReportRows(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, IUtilizationReportSearch model, long? utilizationCategoryId,
            bool? billability, DateTime date)
        {
            return GetTimeReportRowsByOrgUnitId(unitOfWork, model)
                .Where(trr => trr.TimeReport.Year == date.Year && trr.TimeReport.Month == date.Month)
                .Where(trr => !utilizationCategoryId.HasValue ||
                              (trr.AbsenceType != null && trr.AbsenceType.AbsenceProject != null
                                  ? trr.AbsenceType.AbsenceProject.UtilizationCategoryId == utilizationCategoryId.Value
                                  : trr.Project.UtilizationCategoryId == utilizationCategoryId.Value)
                )
                .Where(FilterBillability(billability))
                .Where(trr => trr.DailyEntries.Any(t => t.Hours > 0));
        }

        private IQueryable<TimeReportRow> FilterTimeReportRowsWithoutUtilization(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, IUtilizationReportSearch model, DateTime date)
        {
            return GetTimeReportRowsByOrgUnitId(unitOfWork, model)
                    .Where(trr => trr.TimeReport.Year == date.Year && trr.TimeReport.Month == date.Month)
                    .Where(trr => trr.AbsenceType != null && trr.AbsenceType.AbsenceProject != null ?
                        !trr.AbsenceType.AbsenceProject.UtilizationCategoryId.HasValue :
                        !trr.Project.UtilizationCategoryId.HasValue
                    )
                    .Where(trr => trr.DailyEntries.Any(t => t.Hours > 0));
        }

        public IEnumerable<UtilizationEmployeeRowModel> GetUtilizationProjectEmployees(IUtilizationReportSearch model, long? utilizationCategoryId, bool? billability, DateTime date, long projectId)
        {
            using (var unitOfWork = _timeTrackingUnitOfWorkService.Create())
            {
                var timeReportRows = FilterTimeReportRows(unitOfWork, model, utilizationCategoryId, billability,
                    date)
                    .Where(trr => trr.ProjectId == projectId);

                return ConvertUtilizationEmployeeRow(timeReportRows, model.DisplayType);
            }
        }


        public IEnumerable<UtilizationEmployeeRowModel> GetUtilizationProjectEmployeesWithoutUtilization(IUtilizationReportSearch model, DateTime date, long projectId)
        {
            using (var unitOfWork = _timeTrackingUnitOfWorkService.Create())
            {
                var timeReportRows = FilterTimeReportRowsWithoutUtilization(unitOfWork, model, date)
                    .Where(trr => trr.ProjectId == projectId);

                return ConvertUtilizationEmployeeRow(timeReportRows, model.DisplayType);
            }
        }

        private static IReadOnlyCollection<UtilizationEmployeeRowModel> ConvertUtilizationEmployeeRow(
            IQueryable<TimeReportRow> rows, UtilizationDisplayType displayType)
        {
            var employeeRows = rows
                    .Select(trr => new
                    {
                        trr.TimeReport.EmployeeId,
                        trr.TimeReport.Employee.FirstName,
                        trr.TimeReport.Employee.LastName,
                        Hours = trr.DailyEntries.Sum(d => d.Hours)
                    });

            var totalSum = displayType == UtilizationDisplayType.Values ? 0 : employeeRows.Sum(e => e.Hours);

            return employeeRows
                    .GroupBy(trr => new
                    {
                        trr.EmployeeId,
                        trr.FirstName,
                        trr.LastName,
                    })
                    .ToList()
                    .Select(trr =>
                        new UtilizationEmployeeRowModel
                        {
                            EmployeeId = trr.Key.EmployeeId,
                            FirstName = trr.Key.FirstName,
                            LastName = trr.Key.LastName,
                            DisplayValue = CalculateCellValueText(trr.Sum(d => d.Hours), displayType, totalSum),
                        }
                    ).ToList();
        }

        private static IReadOnlyCollection<UtilizationProjectRowModel> ConvertUtilizationProjectRow(IQueryable<TimeReportRow> rows, UtilizationDisplayType displayType)
        {
            var utilizationRows = rows
                .Select(trr => new
                {
                    trr.ProjectId,
                    trr.Project,
                    Hours = trr.DailyEntries.Sum(de => de.Hours)
                });

            var hoursSum = displayType == UtilizationDisplayType.Values ? 0 : utilizationRows.Sum(trr => trr.Hours);

            return utilizationRows
                    .GroupBy(trr => new
                    {
                        trr.ProjectId,
                        trr.Project
                    })
                    .ToList()
                    .Select(trr => new UtilizationProjectRowModel
                    {
                        ProjectId = trr.Key.ProjectId,
                        Name = ProjectBusinessLogic.ClientProjectName.Call(trr.Key.Project),
                        DisplayValue = CalculateCellValueText(trr.Sum(g => g.Hours), displayType, hoursSum)
                    })
                    .OrderBy(trr => trr.Name)
                    .ToList();
        }

        public IEnumerable<UtilizationProjectRowModel> GetUtilizationProjectsWithoutUtilization(IUtilizationReportSearch model, DateTime date)
        {
            using (var unitOfWork = _timeTrackingUnitOfWorkService.Create())
            {
                var timeReportRows = FilterTimeReportRowsWithoutUtilization(unitOfWork, model, date);

                return ConvertUtilizationProjectRow(timeReportRows, model.DisplayType);
            }
        }

        public IEnumerable<UtilizationProjectRowModel> GetUtilizationProjects(IUtilizationReportSearch model, long? utilizationCategoryId, bool? billability, DateTime date)
        {
            using (var unitOfWork = _timeTrackingUnitOfWorkService.Create())
            {
                var timeReportRows = FilterTimeReportRows(unitOfWork, model, utilizationCategoryId, billability, date);

                return ConvertUtilizationProjectRow(timeReportRows, model.DisplayType);
            }
        }
    }
}
