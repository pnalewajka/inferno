﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Notifications;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.Notifications.ChoreProviders
{
    public class EmailDeliveryFailedChoreProvider
        : IChoreProvider
    {
        private readonly IPrincipalProvider _principalProvider;

        public EmailDeliveryFailedChoreProvider(
            IPrincipalProvider principalProvider)
        {
            _principalProvider = principalProvider;
        }

        public IQueryable<ChoreDto> GetActiveChores(
            IReadOnlyRepositoryFactory repositoryFactory)
        {
            if (!_principalProvider.Current.IsInRole(SecurityRoleType.CanViewEmails))
            {
                return null;
            }

            var emails = repositoryFactory.Create<Email>().Filtered();

            var deliveryFailedEmails = emails
                .Where(e => e.Status == EmailStatus.DeliveryFailed)
                .GroupBy(e => 0)
                .Select(g => new ChoreDto
                {
                    Type = ChoreType.EmailDeliveryFailed,
                    DueDate = null,
                    Parameters = g.Count().ToString(),
                });

            return deliveryFailedEmails;
        }
    }
}
