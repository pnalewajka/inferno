﻿using System;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Notifications.Dto
{
    public class EmailDto
    {
        public long Id { get; set; }

        public string To { get; set; }

        public string From { get; set; }

        public string Subject { get; set; }

        public EmailStatus Status { get; set; }

        public DateTime? SentOn { get; set; }

        public EmailMessageDto Content { get; set; }

        public string Exception { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public long[] AttachmentIds { get; set; }
    }
}
