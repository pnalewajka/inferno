﻿using System;
using Newtonsoft.Json;
using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.Business.Notifications.Dto
{
    [JsonObject]
    [Identifier("EmailMessageParameterContext")]
    public class EmailMessageParameterContext : ParameterContext
    {
        public string SenderName { get; set; }

        public string SenderEmail { get; set; }

        public EmailMessageParameterContext(DateTime currentTime, EmailMessageDto message) : base(currentTime)
        {
            SenderName = message.From?.FullName ?? string.Empty;
            SenderEmail = message.From?.EmailAddress ?? string.Empty;
        }
    }
}
