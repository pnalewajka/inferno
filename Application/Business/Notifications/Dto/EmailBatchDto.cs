﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Notifications.Dto
{
    public class EmailBatchDto
    {
        public IList<EmailRecipientDto> Recipients { get; set; } = new List<EmailRecipientDto>();

        public object Model { get; set; }

        public string SubjectTemplateCode { get; set; }

        public string BodyTemplateCode { get; set; }
    }
}
