using Smt.Atomic.Data.Entities.Modules.Notifications;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Notifications.Dto
{
    public class EmailAttachmentDtoToEmailAttachmentMapping : ClassMapping<EmailAttachmentDto, EmailAttachment>
    {
        public EmailAttachmentDtoToEmailAttachmentMapping()
        {
            Mapping = d => new EmailAttachment
            {
                Id = d.Id,
                Name = d.Name,
                Content = d.Content,
                EmailId = d.EmailId
            };
        }
    }
}
