﻿using System;
namespace Smt.Atomic.Business.Notifications.Dto
{
    public class EmailAttachmentDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public byte[] Content { get; set; }

        public long EmailId { get; set; }

        public bool IsValid
        {
            get
            {
                var isValid = !string.IsNullOrEmpty(Name) && Content != null && Content.Length > 0;
                return isValid;
            }
        }
    }
}
