﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Notifications;

namespace Smt.Atomic.Business.Notifications.Dto
{
    public class InvitationMessageDto
    {
        public List<EmailRecipientDto> ParticipantEmails { get; set; }

        public DateTime InvitationDateTime { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }
    }
}