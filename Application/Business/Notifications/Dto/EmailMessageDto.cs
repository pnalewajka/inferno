﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Notifications;

namespace Smt.Atomic.Business.Notifications.Dto
{
    public class EmailMessageDto
    {
        [XmlIgnore]
        public IEnumerable<EmailAttachmentDto> Attachments { get; set; }

        public List<EmailRecipientDto> Recipients { get; set; }

        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsHtml { get; set; }

        public EmailRecipientDto From { get; set; }
        public EmailRecipientDto ReplyTo { get; set; }

        public EmailMessageDto()
        {
            Recipients = new List<EmailRecipientDto>();
        }

        public string GetAllRecipients()
        {
            var fieldLength = EntityHelper.GetFieldMaxLength<Email>(e => e.To);
            var fieldValue = string.Join("; ", Recipients.Where(r => !string.IsNullOrEmpty(r.ToString())));

            return fieldValue
                .Substring(0, Math.Min(fieldLength, fieldValue.Length));
        }
    }
}
