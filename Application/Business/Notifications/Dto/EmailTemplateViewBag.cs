﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Notifications.Resources;

namespace Smt.Atomic.Business.Notifications.Dto
{
    public class EmailTemplateViewBag
    {
        private readonly IEnumerable<EmailRecipientDto> _emailRecipients;

        public IEnumerable<EmailRecipientDto> ToRecipients
        {
            get { return _emailRecipients.Where(r => r.Type == RecipientType.To); }
        }

        public IEnumerable<EmailRecipientDto> CcRecipients
        {
            get { return _emailRecipients.Where(r => r.Type == RecipientType.Cc); }
        }

        public EmailRecipientDto FirstToRecipient
        {
            get { return ToRecipients.First(); }
        }

        public string RecipientFirstNames
        {
            get { return GetFormattedNames(r => r.FirstName); }
        }

        public string RecipientFullNames
        {
            get { return GetFormattedNames(r => r.FullName); }
        }

        private string GetFormattedNames(Func<EmailRecipientDto, string> getName)
        {
            var toRecipients = ToRecipients.ToList();
            var toRecipientCount = toRecipients.Count;

            if (toRecipientCount == 1)
            {
                return getName(toRecipients[0]);
            }
            else if (toRecipientCount == 2)
            {
                return string.Format(EmailResource.FirstAndSecond, getName(toRecipients[0]), getName(toRecipients[1]));
            }
            else
            {
                var allButOne = string.Join(", ", toRecipients.Select(getName).Take(toRecipientCount - 1));
                var lastOne = getName(toRecipients[toRecipientCount - 1]);

                return string.Format(EmailResource.AllButOneAndLast, allButOne, lastOne);
            }
        }

        public EmailTemplateViewBag(IEnumerable<EmailRecipientDto> emailRecipients)
        {
            _emailRecipients = emailRecipients;
        }
    }
}
