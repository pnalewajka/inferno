﻿using Smt.Atomic.Data.Entities.Modules.Notifications;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Notifications.Dto
{
    public class EmailAttachmentToEmailAttachmentDtoMapping : ClassMapping<EmailAttachment, EmailAttachmentDto>
    {
        public EmailAttachmentToEmailAttachmentDtoMapping()
        {
            Mapping = e => new EmailAttachmentDto
            {
                Id = e.Id,
                Name = e.Name,
                Content = e.Content,
                EmailId = e.EmailId
            };
        }
    }
}
