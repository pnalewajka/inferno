﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.Notifications;

namespace Smt.Atomic.Business.Notifications.Dto
{
    public class EmailToEmailDtoMapping : ClassMapping<Email, EmailDto>
    {
        public EmailToEmailDtoMapping()
        {
            Mapping = e => new EmailDto
            {
                Id = e.Id,
                To = e.To,
                From = e.From,
                Subject = e.Subject,
                Status = e.Status,
                SentOn = e.SentOn,
                Content = (EmailMessageDto) XmlSerializationHelper.DeserializeFromXml(e.Content, typeof(EmailMessageDto)),
                Exception = e.Exception,
                CreatedBy = e.CreatedBy != null ? e.CreatedBy.Login : null,
                CreatedOn = e.CreatedOn,
                AttachmentIds = e.Attachments == null ? null : e.Attachments.Select(attachment => attachment.Id).ToArray()
            };
        }
    }
}
