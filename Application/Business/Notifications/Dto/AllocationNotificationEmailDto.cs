namespace Smt.Atomic.Business.Notifications.Dto
{

    public class AllocationNotificationEmailDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string ModifiedBy { get; set; }

        public string DateFrom { get; set; }

        public string DateTo { get; set; }

        public string Certainty { get; set; }

        public int Percent { get; set; }

        public string ProjectCode { get; set; }

        public string Client { get; set; }

        public string Comments { get; set; }

        public string Manager { get; set; }

        public bool IsSimple { get; set; }
        
        public decimal SimpleHours { get; set; }

        public decimal MondayHours { get; set; }

        public decimal TuesdayHours { get; set; }

        public decimal WednesdayHours { get; set; }

        public decimal ThursdayHours { get; set; }

        public decimal FridayHours { get; set; }

        public decimal SaturdayHours { get; set; }

        public decimal SundayHours { get; set; }
    }
}