﻿using System;
using System.Linq;
using Smt.Atomic.Business.Common.Enums;

namespace Smt.Atomic.Business.Notifications.Dto
{
    public class EmailRecipientDto
    {
        private string _fullName;

        public string EmailAddress { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName
        {
            get => GetFullName();
            set => SetFullName(value);
        }

        public RecipientType Type { get; set; }

        public EmailRecipientDto()
        {
        }

        public EmailRecipientDto(string emailAddress, string fullName, RecipientType recipientType = RecipientType.To)
        {
            EmailAddress = emailAddress;
            FullName = fullName;
            Type = recipientType;
        }

        public override string ToString()
        {
            return string.IsNullOrWhiteSpace(FullName)
                       ? EmailAddress
                       : $"{FullName} <{EmailAddress}>";
        }

        private string GetFullName()
        {
            if (string.IsNullOrEmpty(_fullName))
            {
                _fullName = $"{FirstName} {LastName}".Trim();
            }

            return _fullName;
        }

        private void SetFullName(string value)
        {
            _fullName = value;

            if (string.IsNullOrEmpty(_fullName))
            {
                FirstName = null;
                LastName = null;
            }
            else
            {
                var parts = _fullName.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                FirstName = parts.Length > 0 ? parts[0] : null;
                LastName = parts.Length > 1 ? string.Join(" ", parts.Skip(1)) : null;
            }
        }
    }
}