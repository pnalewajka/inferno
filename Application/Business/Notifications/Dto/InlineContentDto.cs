﻿namespace Smt.Atomic.Business.Notifications.Dto
{
    public class InlineContentDto
    {
        public string Name { get; set; }

        public byte[] Content { get; set; }

        public string ContentType { get; set; }

        public bool IsValid
        {
            get
            {
                return !string.IsNullOrEmpty(Name) && Content != null && Content.Length > 0;
            }
        }
    }
}
