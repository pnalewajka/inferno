﻿using System.Collections.ObjectModel;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.Notifications;

namespace Smt.Atomic.Business.Notifications.Dto
{
    public class EmailDtoToEmailMapping : ClassMapping<EmailDto, Email>
    {
        public EmailDtoToEmailMapping()
        {
            Mapping = d => new Email
            {
                Id = d.Id,
                To = d.To,
                From = d.From,
                Subject = d.Subject,
                Status = d.Status,
                SentOn = d.SentOn,
                Content = XmlSerializationHelper.SerializeToXml(d.Content, typeof(EmailMessageDto)),
                Exception = d.Exception,
                Attachments = d.AttachmentIds == null ? null : new Collection<EmailAttachment>(d.AttachmentIds.Select(v => new EmailAttachment { Id = v }).ToList())
            };
        }
    }
}
