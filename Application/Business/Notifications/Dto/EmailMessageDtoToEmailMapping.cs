﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.Notifications;

namespace Smt.Atomic.Business.Notifications.Dto
{
    public class EmailMessageDtoToEmailMapping : ClassMapping<EmailMessageDto, Email>
    {
        public EmailMessageDtoToEmailMapping()
        {
            var emailAttachmentDtoToEmailAttachmentMapping = new EmailAttachmentDtoToEmailAttachmentMapping();

            Mapping = d => new Email
            {
                Attachments = d.Attachments == null ? null : d.Attachments.Select(attachment => emailAttachmentDtoToEmailAttachmentMapping.CreateFromSource(attachment)).ToList(),
                To = d.GetAllRecipients(),
                From = d.From.ToString(),
                Subject = d.Subject,
                Status = EmailStatus.Pending,
                Content = XmlSerializationHelper.SerializeToXml(d, typeof(EmailMessageDto))
            };
        }
    }
}