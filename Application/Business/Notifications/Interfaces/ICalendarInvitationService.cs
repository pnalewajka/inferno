﻿using Smt.Atomic.Business.Notifications.Dto;

namespace Smt.Atomic.Business.Notifications.Interfaces
{
    public interface ICalendarInvitationService
    {
        void SendInvitation(InvitationMessageDto message);
    }
}