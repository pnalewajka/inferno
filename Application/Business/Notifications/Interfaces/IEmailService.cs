﻿using System.Threading.Tasks;
using Smt.Atomic.Business.Notifications.Dto;

namespace Smt.Atomic.Business.Notifications.Interfaces
{
    public interface IEmailService
    {
        /// <summary>
        /// Method to sending emails immediately without storing them on DB
        /// </summary>
        /// <param name="message">Email message Dto</param>
        void SendMessage(EmailMessageDto message);

        /// <summary>
        /// Method to async sending emails immediately without storing them on DB
        /// </summary>
        /// <param name="message">Email message Dto</param>
        Task SendMessageAsync(EmailMessageDto message);
    }
}