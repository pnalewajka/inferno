﻿using Smt.Atomic.Business.Notifications.Dto;

namespace Smt.Atomic.Business.Notifications.Interfaces
{
    public interface IInlineContentService
    {
        InlineContentDto GetContent(string contentId);
    }
}
