namespace Smt.Atomic.Business.Notifications.Interfaces
{
    using Enums;
    using Dto;
    using System.Collections.Generic;

    public interface IAllocationNotificationService
    {
        void SendNotification(AllocationNotificationDto allocationNotification, AllocationEvent code, Dictionary<string, string> recipients);
    }
}