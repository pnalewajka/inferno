﻿using Smt.Atomic.Business.Notifications.Dto;

namespace Smt.Atomic.Business.Notifications.Interfaces
{
    public interface IInlineContentResolver
    {
        string Prefix { get; }

        InlineContentDto GetContent(string contentId);
    }
}
