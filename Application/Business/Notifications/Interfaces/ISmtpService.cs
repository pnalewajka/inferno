using System;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Notifications.Interfaces
{
    public interface ISmtpService : IDisposable
    {
        void Send(MailMessage message);

        Task SendMailAsync(MailMessage message);
    }
}