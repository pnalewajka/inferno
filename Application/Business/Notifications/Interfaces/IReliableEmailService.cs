﻿using Smt.Atomic.Business.Notifications.Dto;

namespace Smt.Atomic.Business.Notifications.Interfaces
{
    public interface IReliableEmailService
    {
        /// <summary>
        /// Method to convert EmailMessageDto into EmailDto and store it into db for reliable queue job sender
        /// </summary>
        /// <param name="message">Email message Dto</param>
        void EnqueueMessage(EmailMessageDto message);

        /// <summary>
        /// Method to convert EmailDataDto into EmaiLDto and store it into db for reliable queue job sender
        /// </summary>
        /// <param name="batch">Email batch Dto</param>
        /// <param name="separateToRecipients">Send separate emails per To recipient</param>
        void EnqueueBatch(EmailBatchDto batch, bool separateToRecipients = false);
    }
}