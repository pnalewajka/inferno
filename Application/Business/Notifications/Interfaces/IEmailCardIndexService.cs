﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;

namespace Smt.Atomic.Business.Notifications.Interfaces
{
    public interface IEmailCardIndexService : ICardIndexDataService<EmailDto>
    {
        /// <summary>
        /// Retry sending method is changing email status to pending
        /// </summary>
        /// <param name="emailId">Email entity id to resend</param>
        BusinessResult RetrySending(long emailId);
    }
}