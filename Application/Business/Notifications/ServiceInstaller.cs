﻿using System.Collections.Generic;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Notifications.ChoreProviders;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Notifications.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Presentation.EmailTemplates.Helpers;

namespace Smt.Atomic.Business.Notifications
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            EnforceLoadingAssemblies();

            switch (containerType)
            {
                case ContainerType.WebApi:
                case ContainerType.WebApp:
                case ContainerType.WebServices:
                case ContainerType.JobScheduler:
                    container.Register(Component.For<IAllocationNotificationService>().ImplementedBy<AllocationNotificationService>().LifestyleTransient());
                    container.Register(Component.For<IEmailCardIndexService>().ImplementedBy<EmailCardIndexService>().LifestyleTransient());
                    container.Register(Component.For<ISmtpService>().ImplementedBy<SmtpServiceProxy>().LifestyleTransient());
                    container.Register(Component.For<IEmailService>().ImplementedBy<EmailService>().LifestyleTransient());
                    container.Register(Component.For<IReliableEmailService>().ImplementedBy<ReliableEmailService>().LifestyleTransient());
                    container.Register(Component.For<IInlineContentService>().ImplementedBy<InlineContentService>().LifestyleTransient());
                    container.Register(Classes.FromThisAssembly().BasedOn<IInlineContentResolver>().WithService.Base().LifestyleTransient());
                    container.Register(Component.For<IEnumerable<IInlineContentResolver>>()
                             .UsingFactoryMethod(kernel => kernel.ResolveAll<IInlineContentResolver>())
                             .LifestyleTransient());
                    container.Register(Component.For<ICalendarInvitationService>().ImplementedBy<InvitationService>().LifestyleTransient());

                    break;
            }

            if (containerType == ContainerType.WebApp)
            {
                container.Register(Component.For<IChoreProvider>().ImplementedBy<EmailDeliveryFailedChoreProvider>().LifestylePerWebRequest());
            }
        }

        private void EnforceLoadingAssemblies()
        {
            var _ = EmailTemplateHelper.SharedHead;
        }
    }
}