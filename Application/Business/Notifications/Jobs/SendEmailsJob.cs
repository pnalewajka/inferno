﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Security.Authentication;
using System.Threading;
using Castle.Core.Logging;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Notifications;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Notifications.Jobs
{
    [Identifier("Job.Notifications.SendEmailsJob")]
    [JobDefinition(
        Code = "SendEmails",
        Description = "Send email messages",
        ScheduleSettings = "0 15 * * * *",
        PipelineName = PipelineCodes.Notifications)]
    internal class SendEmailsJob : IJob
    {
        private const int MillisecondMultiplier = 1000;

        private readonly ILogger _logger;
        private readonly IUnitOfWorkService<INotificationsDbScope> _unitOfWorkService;
        private readonly ITimeService _timeService;
        private readonly ISystemParameterService _systemParameters;
        private readonly IEmailService _smtpEmailService;
        private readonly IClassMapping<Email, EmailDto> _emailMapping;
        private readonly IClassMapping<EmailAttachment, EmailAttachmentDto> _emailAttachmentMapping;

        public SendEmailsJob(
            ILogger logger,
            IUnitOfWorkService<INotificationsDbScope> unitOfWorkService,
            ITimeService timeService,
            ISystemParameterService systemParameters,
            IEmailService smtpEmailService,
            IClassMapping<Email, EmailDto> emailMapping,
            IClassMapping<EmailAttachment, EmailAttachmentDto> emailAttachmentMapping)
        {
            _logger = logger;
            _unitOfWorkService = unitOfWorkService;
            _timeService = timeService;
            _systemParameters = systemParameters;
            _smtpEmailService = smtpEmailService;
            _emailMapping = emailMapping;
            _emailAttachmentMapping = emailAttachmentMapping;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            try
            {
                SendMessages(executionContext.CancellationToken);
            }
            catch (Exception exception)
            {
                _logger.Error("SendEmailsJob: SMTP error", exception);

                return JobResult.Fail();
            }

            return JobResult.Success();
        }

        private bool TryToSend(Email email, IUnitOfWork<INotificationsDbScope> unitOfWork)
        {
            var emailDto = GetEmailDto(email);

            try
            {
                email.Exception = null;

                _smtpEmailService.SendMessage(emailDto.Content);

                email.Status = EmailStatus.Sent;
                email.SentOn = _timeService.GetCurrentTime();
            }
            catch (Exception exception)
            {
                if (IsPermanentIssue(exception))
                {
                    throw;
                }

                email.Exception = exception.ToString();
                email.Status = EmailStatus.DeliveryFailed;
            }

            unitOfWork.Repositories.Emails.Edit(email);
            unitOfWork.Commit();

            return email.Status == EmailStatus.Sent;
        }

        private EmailDto GetEmailDto(Email email)
        {
            var emailDto = _emailMapping.CreateFromSource(email);

            if (email.Attachments.Any())
            {
                var attachmentsToSend = new List<EmailAttachmentDto>();

                foreach (var attachment in email.Attachments)
                {
                    attachmentsToSend.Add(_emailAttachmentMapping.CreateFromSource(attachment));
                }
                emailDto.Content.Attachments = attachmentsToSend;
            }

            return emailDto;
        }

        public void SendMessages(CancellationToken cancellationToken)
        {
            var firstDelayInSeconds = _systemParameters.GetParameter<int>(ParameterKeys.SmtpEmailSendingFirstAttemptDelayInSeconds);
            var secondDelayInSeconds = _systemParameters.GetParameter<int>(ParameterKeys.SmtpEmailSendingSecondAttemptDelayInSeconds);
            var chunkSize = _systemParameters.GetParameter<int>(ParameterKeys.SmtpEmailSendingChunkSize);
            var chunkDelayInMilliseconds = _systemParameters.GetParameter<int>(ParameterKeys.SmtpEmailSendingChunkDelayInMilliseconds);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                IEnumerable<Email> emails = unitOfWork.Repositories.Emails.Where(x => x.Status == EmailStatus.Pending).ToList();

                while (!cancellationToken.IsCancellationRequested && emails.Any())
                {
                    var chunk = emails.Take(chunkSize);
                    emails = emails.Skip(chunkSize);

                    foreach (var email in chunk.TakeWhile(i => !cancellationToken.IsCancellationRequested))
                    {
                        if (cancellationToken.IsCancellationRequested)
                        {
                            return;
                        }

                        if (TryToSend(email, unitOfWork))
                        {
                            continue;
                        }

                        if (!cancellationToken.WaitHandle.WaitOne(firstDelayInSeconds * MillisecondMultiplier))
                        {
                            return;
                        }

                        if (TryToSend(email, unitOfWork))
                        {
                            continue;
                        }

                        if (!cancellationToken.WaitHandle.WaitOne(secondDelayInSeconds * MillisecondMultiplier))
                        {
                            return;
                        }

                        if (TryToSend(email, unitOfWork))
                        {
                            continue;
                        }

                        unitOfWork.Repositories.Emails.Edit(email);
                        unitOfWork.Commit();
                    }

                    Thread.Sleep(chunkDelayInMilliseconds);
                }
            }
        }

        private static bool IsPermanentIssue(Exception exception)
        {
            var smtpException = exception as SmtpException;

            if (smtpException != null)
            {
                switch (smtpException.StatusCode)
                {
                    case SmtpStatusCode.ClientNotPermitted:
                    case SmtpStatusCode.CommandNotImplemented:
                    case SmtpStatusCode.CommandParameterNotImplemented:
                    case SmtpStatusCode.CommandUnrecognized:
                    case SmtpStatusCode.GeneralFailure:
                    case SmtpStatusCode.MailboxNameNotAllowed:
                    case SmtpStatusCode.MailboxUnavailable:
                    case SmtpStatusCode.MustIssueStartTlsFirst:
                    case SmtpStatusCode.SyntaxError:
                        return true;
                }
            }

            return exception is AuthenticationException;
        }
    }
}