﻿using System;
using Castle.Core.Logging;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Notifications.Jobs
{
    [Identifier("Job.Notifications.DeleteEmailsJob")]
    [JobDefinition(
        Code = "DeleteEmails",
        Description = "Delete email messages",
        ScheduleSettings = "0 5 * * 1 *",
        PipelineName = PipelineCodes.Notifications)]
    internal class DeleteEmailsJob : IJob
    {
        private readonly ILogger _logger;
        private readonly IUnitOfWorkService<INotificationsDbScope> _unitOfWorkService;
        private readonly ITimeService _timeService;
        private readonly ISystemParameterService _systemParameters;

        public DeleteEmailsJob(
            ILogger logger,
            IUnitOfWorkService<INotificationsDbScope> unitOfWorkService,
            ITimeService timeService,
            ISystemParameterService systemParameters)
        {
            _logger = logger;
            _unitOfWorkService = unitOfWorkService;
            _timeService = timeService;
            _systemParameters = systemParameters;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            try
            {
                DeleteOldMessages();
            }
            catch (Exception exception)
            {
                _logger.Error("SendEmailsJob: SMTP error", exception);

                return JobResult.Fail();
            }

            return JobResult.Success();
        }

        private void DeleteOldMessages()
        {
            var deletionPeriodInDays = _systemParameters.GetParameter<int>(ParameterKeys.SmtpEmailDeletionPeriodInDays);
            var expiredOn = _timeService.GetCurrentDate().AddDays(-deletionPeriodInDays);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                unitOfWork.Repositories.Emails.DeleteEach(x => x.SentOn <= expiredOn);
                unitOfWork.Commit();
            }
        }
    }
}