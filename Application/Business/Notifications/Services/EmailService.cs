using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Castle.Core.Internal;
using Castle.Core.Logging;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.Business.Notifications.Services
{
    internal class EmailService : IEmailService
    {
        private readonly ISystemParameterService _systemParameters;
        private readonly IInlineContentService _inlineContent;
        private readonly ILogger _logger;
        private readonly ISmtpService _smtpClient;
        private readonly ITimeService _timeService;

        public EmailService(
            ISmtpService smtpClient,
            ISystemParameterService systemParameters,
            IInlineContentService inlineContent,
            ILogger logger,
            ITimeService timeService)
        {
            _smtpClient = smtpClient;
            _systemParameters = systemParameters;
            _inlineContent = inlineContent;
            _logger = logger;
            _timeService = timeService;
        }

        public void SendMessage(EmailMessageDto message)
        {
            var mailMessage = GetMessage(message);

            try
            {
                if (mailMessage.To.Any())
                {
                    _smtpClient.Send(mailMessage);
                }
            }
            catch (SmtpException exception)
            {
                _logger.Error("SmtpEmailService: SMTP client error", exception);

                throw;
            }
        }

        public async Task SendMessageAsync(EmailMessageDto message)
        {
            var mailMessage = GetMessage(message);

            try
            {
                if (mailMessage.To.Any())
                {
                    await _smtpClient.SendMailAsync(mailMessage);
                }
            }
            catch (SmtpException exception)
            {
                _logger.Error("SmtpEmailService: SMTP client error", exception);

                throw;
            }
        }

        private MailMessage GetMessage(EmailMessageDto message)
        {
            var mail = new MailMessage();

            var currentTime = _timeService.GetCurrentTime();
            var parameterContext = new EmailMessageParameterContext(currentTime, message);
            var mockEmailRecipient = _systemParameters.GetParameter<string>(ParameterKeys.MockEmailRecipient, parameterContext);

            if (string.IsNullOrEmpty(mockEmailRecipient))
            {
                AddRecipients(message, RecipientType.To, mail.To);
                AddRecipients(message, RecipientType.Cc, mail.CC);
                AddRecipients(message, RecipientType.Bcc, mail.Bcc);

                if (message.ReplyTo != null)
                {
                    mail.ReplyToList.Add(GetAddress(message.ReplyTo));
                }
            }
            else
            {
                mail.To.Clear();
                mail.To.Add(new MailAddress(mockEmailRecipient));
            }

            AddAttachment(message, mail.Attachments);
            AddInlineContent(message, mail.Attachments);

            mail.From = GetAddress(message.From ?? GetDefaultFrom());

            mail.Subject = message.Subject;
            mail.IsBodyHtml = message.IsHtml;
            mail.Body = message.Body;

            return mail;
        }

        private void AddInlineContent(EmailMessageDto message, AttachmentCollection attachmentCollection)
        {
            var cids = GetInlineContentIds(message.Body);

            foreach (var cid in cids)
            {
                try
                {
                    Attachment attachment = GetInlineContentAttachment(cid);

                    attachmentCollection.Add(attachment);
                }
                catch (Exception e)
                {
                    _logger.Error($"Could not found inline content for id: \"{cid}\"", e);
                }
            }
        }

        private Attachment GetInlineContentAttachment(string cid)
        {
            var inlineContent = _inlineContent.GetContent(cid);
            var attachment = new Attachment(new MemoryStream(inlineContent.Content), new ContentType(inlineContent.ContentType))
            {
                Name = inlineContent.Name,
                TransferEncoding = TransferEncoding.Base64,
                ContentId = cid
            };
            attachment.ContentDisposition.Inline = true;

            return attachment;
        }

        private IEnumerable<string> GetInlineContentIds(string messageBody)
        {
            var pattern = "\\ssrc=(\"|\')cid:(?<cid>[^\"\']+)(\"|\')";
            var regex = new Regex(pattern, RegexOptions.ExplicitCapture);
            var matches = regex.Matches(messageBody);

            return matches.Cast<Match>().Select(m => m.Groups["cid"].Value).Distinct();
        }

        private void AddAttachment(EmailMessageDto message, AttachmentCollection attachmentCollection)
        {
            var attachmentsAvailable = message.Attachments != null && message.Attachments.Any() && message.Attachments.All(attachment => attachment.IsValid);

            if (!attachmentsAvailable)
            {
                return;
            }

            var attachments =
                message.Attachments.Select(
                    attachment => new Attachment(new MemoryStream(attachment.Content), attachment.Name)).ToArray();
            attachments.ForEach(attachmentCollection.Add);
        }

        private void AddRecipients(EmailMessageDto message, RecipientType recipientType, MailAddressCollection recipientCollection)
        {
            var recipients = message.Recipients.Where(r => r.Type == recipientType).ToList();

            foreach (var recipient in recipients)
            {
                var recipientAddress = GetAddress(recipient);

                if (!string.IsNullOrEmpty(recipientAddress.Address))
                {
                    recipientCollection.Add(recipientAddress);
                }
            }
        }

        private EmailRecipientDto GetDefaultFrom()
        {
            var emailAddress = _systemParameters.GetParameter<string>(ParameterKeys.SmtpEmailServiceDefaultFromEmailAddress);
            var fullName = _systemParameters.GetParameter<string>(ParameterKeys.SmtpEmailServiceDefaultFromFullName);

            return new EmailRecipientDto
            {
                EmailAddress = emailAddress,
                FullName = fullName
            };
        }

        private MailAddress GetAddress(EmailRecipientDto recipient)
        {
            var setRecipientName = _systemParameters.GetParameter<bool>(ParameterKeys.SmtpEmailServiceSetRecipientName);

            return setRecipientName
                ? new MailAddress(recipient.EmailAddress, recipient.FullName)
                : new MailAddress(recipient.EmailAddress);
        }
    }
}