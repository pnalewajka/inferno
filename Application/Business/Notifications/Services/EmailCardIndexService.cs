using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Notifications.Resources;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Notifications;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Notifications.Services
{
    internal class EmailCardIndexService : ReadOnlyCardIndexDataService<EmailDto, Email, INotificationsDbScope>, IEmailCardIndexService
    {
        private readonly IUnitOfWorkService<INotificationsDbScope> _unitOfWorkService;
        private readonly IClassMapping<Email, EmailDto> _emailToEmailDtoMapping;

        public EmailCardIndexService(
            IUnitOfWorkService<INotificationsDbScope> unitOfWorkService,
            IClassMapping<Email, EmailDto> emailToEmailDtoMapping,
            ICardIndexServiceDependencies<INotificationsDbScope> dependencies)
            : base(dependencies)
        {
            _unitOfWorkService = unitOfWorkService;
            _emailToEmailDtoMapping = emailToEmailDtoMapping;
        }

        protected override IEnumerable<Expression<Func<Email, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return u => u.From;
            yield return u => u.To;
            yield return u => u.Subject;
        }

        protected override NamedFilters<Email> NamedFilters
        {
            get
            {
                return new NamedFilters<Email>(CardIndexServiceHelper.BuildEnumBasedFilters<Email, EmailStatus>(u => u.Status));
            }
        }

        public BusinessResult RetrySending(long id)
        {
            var result = new BusinessResult();

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var email = unitOfWork.Repositories.Emails.GetById(id);

                if (email.Status == EmailStatus.Sent)
                {
                    result.AddAlert(AlertType.Error, EmailResource.EmailResendWrongStatusErrorMessage);
                }
                else
                {
                    email.Status = EmailStatus.Pending;
                    email.Exception = null;

                    unitOfWork.Repositories.Emails.Edit(email);
                    unitOfWork.Commit();

                    result.AddAlert(AlertType.Success, EmailResource.EmailResendResultMessage);
                }

                return result;
            }
        }
    }
}