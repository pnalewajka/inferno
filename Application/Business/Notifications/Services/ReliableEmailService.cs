﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Notifications;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Notifications.Services
{
    internal class ReliableEmailService : IReliableEmailService
    {
        private readonly ISystemParameterService _systemParameters;
        private readonly IUnitOfWorkService<INotificationsDbScope> _unitOfWorkService;
        private readonly IClassMapping<EmailMessageDto, Email> _emailMessageDtoToEmailMapping;
        private readonly IMessageTemplateService _templateService;

        public ReliableEmailService(
            ISystemParameterService systemParameters,
            IUnitOfWorkService<INotificationsDbScope> unitOfWorkService,
            IClassMapping<EmailMessageDto, Email> emailMessageDtoToEmailMapping,
            IMessageTemplateService templateService)
        {
            _systemParameters = systemParameters;
            _unitOfWorkService = unitOfWorkService;
            _emailMessageDtoToEmailMapping = emailMessageDtoToEmailMapping;
            _templateService = templateService;
        }

        public void EnqueueMessage([NotNull] EmailMessageDto emailDto)
        {
            if (emailDto == null)
            {
                throw new NullReferenceException();
            }
            
            // don't enqueue emails without recipients
            if (string.IsNullOrEmpty(emailDto.GetAllRecipients()))
            {
                return;
            }

            if (emailDto.From == null)
            {
                emailDto.From = new EmailRecipientDto
                {
                    EmailAddress = _systemParameters.GetParameter<string>(ParameterKeys.SmtpEmailServiceDefaultFromEmailAddress),
                    FullName = _systemParameters.GetParameter<string>(ParameterKeys.SmtpEmailServiceDefaultFromFullName)
                };
            }

            emailDto.Subject = SanitizeSubject(emailDto.Subject);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                unitOfWork.Repositories.Emails.Add(_emailMessageDtoToEmailMapping.CreateFromSource(emailDto));
                unitOfWork.Commit();
            }
        }

        public void EnqueueBatch([NotNull]EmailBatchDto emailBatchDto, bool separateToRecipients)
        {
            if (emailBatchDto == null)
            {
                throw new ArgumentNullException(nameof(emailBatchDto));
            }

            if (emailBatchDto.Model == null)
            {
                throw new ArgumentNullException(nameof(emailBatchDto.Model));
            }

            if (separateToRecipients)
            {
                var additionalRecipients = emailBatchDto
                    .Recipients
                    .Where(r => r.Type != RecipientType.To)
                    .ToList();

                var toRecipients = emailBatchDto
                    .Recipients
                    .Where(r => r.Type == RecipientType.To)
                    .ToList();

                foreach (var recipient in toRecipients)
                {
                    emailBatchDto.Recipients = additionalRecipients
                        .Concat(new[] { recipient })
                        .ToList();

                    EnqueueBatchInternal(emailBatchDto);
                }
            }
            else
            {
                EnqueueBatchInternal(emailBatchDto);
            }
        }

        private void EnqueueBatchInternal(EmailBatchDto emailDataDto)
        {
            var viewBag = new EmailTemplateViewBag(emailDataDto.Recipients);

            var subject = _templateService.ResolveTemplate(emailDataDto.SubjectTemplateCode, emailDataDto.Model, viewBag);
            var body = _templateService.ResolveTemplate(emailDataDto.BodyTemplateCode, emailDataDto.Model, viewBag);

            var message = new EmailMessageDto
            {
                Body = body.Content,
                IsHtml = body.IsHtml,
                Subject = subject.Content,
                Recipients = emailDataDto.Recipients.ToList()
            };

            EnqueueMessage(message);
        }

        private string SanitizeSubject(string subject)
        {
            subject = subject.Replace('\r', ' ').Replace('\n', ' ');
            subject = Regex.Replace(subject, @"\s+", " ");
            subject = subject.Trim(' ', '\t', '\r', '\n');

            return subject;
        }
    }
}