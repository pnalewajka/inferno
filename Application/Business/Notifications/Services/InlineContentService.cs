﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Logging;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;

namespace Smt.Atomic.Business.Notifications.Services
{
    public class InlineContentService : IInlineContentService
    {
        private static readonly char[] Separators = new char[] { '-' };

        private readonly ILogger _logger;
        private readonly IDictionary<string, IInlineContentResolver> _resolvers;

        public InlineContentService(
            ILogger logger,
            IEnumerable<IInlineContentResolver> resolvers)
        {
            _logger = logger;
            _resolvers = resolvers.ToDictionary(key => key.Prefix, value => value);
        }

        public InlineContentDto GetContent(string contentId)
        {
            var parts = contentId.Split(Separators, 2);

            if (parts.Length != 2)
            {
                _logger.Error($"Invalid content id format: \"{contentId}\"");

                throw new ArgumentException("Invalid content id format", nameof(contentId));
            }

            var resolverName = parts[0];
            var resourceName = parts[1];

            IInlineContentResolver resolver;

            if (_resolvers.TryGetValue(resolverName, out resolver))
            {
                return resolver.GetContent(resourceName);
            }

            _logger.Error($"Unknown resolver: \"{resolverName}\"");

            throw new ArgumentException("Unknown resolver", nameof(contentId));
        }
    }
}
