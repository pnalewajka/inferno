﻿using System;
using System.IO;
using System.Web;
using Castle.Core.Logging;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;

namespace Smt.Atomic.Business.Notifications.Services.InlineContentResolvers
{
    public class StaticContentResolver : IInlineContentResolver
    {
        private const string AssetsPath = "assets\\static";
        
        private readonly ILogger _logger;

        public string Prefix => "static";

        public StaticContentResolver(ILogger logger)
        {
            _logger = logger;
        }

        public InlineContentDto GetContent(string contentId)
        {
            byte[] content;
            string path = Path.Combine(AssetsPath, contentId);

            try
            {
                using (var fileStream = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    var length = fileStream.Length;
                    content = new byte[length];
                    fileStream.Read(content, 0, (int)length);
                }
            }
            catch (Exception e)
            {
                _logger.Error($"Could not get file for content id: \"{contentId}\" in resolver: \"{Prefix}\"");
                throw new ArgumentException("Unknown content id", nameof(contentId), e);
            }
            
            return new InlineContentDto
            {
                Name = contentId,
                Content = content,
                ContentType = MimeMapping.GetMimeMapping(contentId)
            };
        }
    }
}
