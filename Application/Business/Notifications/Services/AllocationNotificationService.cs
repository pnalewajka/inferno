﻿using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Business.Notifications.Services
{
    using Enums;
    using Common.Helpers;
    using CrossCutting.Common.Consts;
    using CrossCutting.Common.Interfaces;
    using Dto;
    using Interfaces;
    using System.Collections.Generic;
    using System.Linq;
    using Configuration.Interfaces;

    public class AllocationNotificationService : IAllocationNotificationService
    {
        private readonly IMessageTemplateService _templateService;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly ISystemParameterService _systemParameters;

        public AllocationNotificationService(IMessageTemplateService templateService, IReliableEmailService reliableEmailService, ISystemParameterService systemParameters)
        {
            _templateService = templateService;
            _reliableEmailService = reliableEmailService;
            _systemParameters = systemParameters;
        }

        public void SendNotification(AllocationNotificationDto allocationNotification, AllocationEvent allocationEvent, Dictionary<string, string> recipients)
        {
            if (!recipients.Any())
            {
                return;
            }

            var allocationNotificationEmailDto = GetAllocationNotificationEmailDto(allocationNotification);
            var chosenSubjectType = GetSubjectType(allocationEvent);
            var chosenBodyType = GetBodyType(allocationEvent);
            var subject = _templateService.ResolveTemplate(chosenSubjectType, allocationNotificationEmailDto);
            var body = _templateService.ResolveTemplate(chosenBodyType, allocationNotificationEmailDto);
            var message = GetEmailMessageDto(body.Content, subject.Content, body.IsHtml);

            foreach (var recipient in recipients)
            {
                message.Recipients.AddIfNotContains(new EmailRecipientDto(recipient.Key, recipient.Value));
            }

            _reliableEmailService.EnqueueMessage(message);
        }

        private string GetBodyType(AllocationEvent allocationEvent)
        {
            switch (allocationEvent)
            {
                case AllocationEvent.OnFixedAdded:
                    return TemplateCodes.AllocationNotificationOnFixedAddedEmailBody;
                case AllocationEvent.OnPlannedAdded:
                    return TemplateCodes.AllocationNotificationOnPlannedAddedEmailBody;
                case AllocationEvent.OnPlannedToFixed:
                    return TemplateCodes.AllocationNotificationOnPlannedToFixedEmailBody;
                case AllocationEvent.OnChanged:
                    return TemplateCodes.AllocationNotificationOnChangedEmailBody;
                case AllocationEvent.OnDeleted:
                    return TemplateCodes.AllocationNotificationOnDeletedEmailBody;
                default:
                    return string.Empty;
            }
        }

        private string GetSubjectType(AllocationEvent allocationEvent)
        {
            switch (allocationEvent)
            {
                case AllocationEvent.OnFixedAdded:
                    return TemplateCodes.AllocationNotificationOnFixedAddedSubject;
                case AllocationEvent.OnPlannedAdded:
                    return TemplateCodes.AllocationNotificationOnPlannedAddedSubject;
                case AllocationEvent.OnPlannedToFixed:
                    return TemplateCodes.AllocationNotificationOnPlannedToFixedSubject;
                case AllocationEvent.OnChanged:
                    return TemplateCodes.AllocationNotificationOnChangedSubject;
                case AllocationEvent.OnDeleted:
                    return TemplateCodes.AllocationNotificationOnDeletedSubject;
                default:
                    return string.Empty;
            }
        }

        private EmailMessageDto GetEmailMessageDto(string body, string subject, bool isHtml)
        {
            var message = new EmailMessageDto
            {
                Body = body,
                Subject = subject,
                IsHtml = isHtml
            };
            return message;
        }

        private AllocationNotificationEmailDto GetAllocationNotificationEmailDto(AllocationNotificationDto data)
        {
            var messageModel = new AllocationNotificationEmailDto
            {
                FirstName = data.FirstName,
                LastName = data.LastName,
                ModifiedBy = data.ModifiedBy,
                DateFrom = data.DateFrom?.ToString("yyyy-MM-dd") ?? string.Empty,
                DateTo = data.DateTo?.ToString("yyyy-MM-dd") ?? string.Empty,
                Certainty = data.Certainty,
                Percent = data.Percent,
                ProjectCode = data.ProjectName,
                Client = data.Client,
                IsSimple = data.IsSimple,
                SimpleHours = GetSimpleHours(data),
                MondayHours = data.MondayHours,
                TuesdayHours = data.TuesdayHours,
                WednesdayHours = data.WednesdayHours,
                ThursdayHours = data.ThursdayHours,
                FridayHours = data.FridayHours,
                SaturdayHours = data.SaturdayHours,
                SundayHours = data.SundayHours,
                Comments = data.Comments
            };
            return messageModel;
        }

        private decimal GetSimpleHours(AllocationNotificationDto data)
        {
            if (data.IsSimple)
            {
                var hours = new List<decimal>
                {
                    data.MondayHours,
                    data.TuesdayHours,
                    data.WednesdayHours,
                    data.ThursdayHours,
                    data.FridayHours,
                    data.MondayHours
                };

                return hours.Max();
            }
            return 0;
        }
    }
}
