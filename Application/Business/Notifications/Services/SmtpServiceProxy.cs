using System.IO;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Castle.Core.Logging;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.Business.Notifications.Services
{
    internal class SmtpServiceProxy : ISmtpService
    {
        private readonly ISystemParameterService _systemParameters;
        private readonly ILogger _logger;

        private SmtpClient _smtpClient;
        private SmtpClient SmtpClient => _smtpClient ?? (_smtpClient = CreateSmtpClient());

        private const int Timeout = 5 * 60 * 1000;

        public SmtpServiceProxy(ISystemParameterService systemParameters, ILogger logger)
        {
            _systemParameters = systemParameters;
            _logger = logger;
        }

        public void Dispose()
        {
            _smtpClient?.Dispose();
        }

        public void Send(MailMessage message)
        {
            SmtpClient?.Send(message);
        }

        public Task SendMailAsync(MailMessage message)
        {
            return SmtpClient?.SendMailAsync(message);
        }

        private SmtpClient CreateSmtpClient()
        {
            var smtpClient = new SmtpClient();

            var host = _systemParameters.GetParameter<string>(ParameterKeys.SmtpEmailServiceHostName);
            var port = _systemParameters.GetParameter<int>(ParameterKeys.SmtpEmailServicePort);
            var userName = _systemParameters.GetParameter<string>(ParameterKeys.SmtpEmailServiceUserName);
            var password = _systemParameters.GetParameter<string>(ParameterKeys.SmtpEmailServicePassword);
            var useSsl = _systemParameters.GetParameter<bool>(ParameterKeys.SmtpEmailServiceUseSsl);

            if (string.IsNullOrEmpty(host) || port == 0)
            {
                var message =
                    $"SmtpEmailService: SMTP server configuration is invalid (host: '{host}', port: '{port}')";

                throw new InvalidDataException(message);
            }

            smtpClient.Host = host;
            smtpClient.Port = port;
            smtpClient.EnableSsl = useSsl;
            smtpClient.Timeout = Timeout;

            if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(password))
            {
                smtpClient.Credentials = new NetworkCredential(userName, password);
            }

            smtpClient.SendCompleted += (s, e) =>
                {
                    if (e.Error != null)
                    {
                        var userMessage = e.UserState as MailMessage;

                        userMessage?.Dispose();

                        _logger.Error("SmtpEmailService: SMTP client error", e.Error);
                    }

                    ((SmtpClient)s).Dispose();
                };

            return smtpClient;
        }
    }
}
