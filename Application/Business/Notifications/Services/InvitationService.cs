using System.Text;
using Ical.Net;
using Ical.Net.CalendarComponents;
using Ical.Net.DataTypes;
using Ical.Net.Serialization;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using System.Collections.Generic;

namespace Smt.Atomic.Business.Notifications.Services
{
    internal class InvitationService : ICalendarInvitationService
    {
        private readonly IReliableEmailService _reliableEmailService;

        public InvitationService(IReliableEmailService reliableEmailService)
        {
            _reliableEmailService = reliableEmailService;
        }

        public void SendInvitation(InvitationMessageDto message)
        {
            var start = message.InvitationDateTime;
            var end = start.AddHours(1);

            //Repeat daily for 1 days
            var recurrenceRule = new RecurrencePattern(FrequencyType.Daily, 1) { Count = 1 };

            var calendarEvent = new CalendarEvent
            {
                Start = new CalDateTime(start),
                End = new CalDateTime(end),
                RecurrenceRules = new List<RecurrencePattern> { recurrenceRule },
            };

            var calendar = new Calendar();
            calendar.Events.Add(calendarEvent);

            var serializer = new CalendarSerializer();
            var serializedCalendar = serializer.SerializeToString(calendar);
            var serializedCalendarByteArray = Encoding.UTF8.GetBytes(serializedCalendar);

            var emailMessageDto = new EmailMessageDto
            {
                Subject = message.Subject,
                Body = message.Body,
                IsHtml = true,
                Attachments = new List<EmailAttachmentDto>
                {
                    new EmailAttachmentDto
                    {
                        Content = serializedCalendarByteArray,
                        Name = "EventDetails.ics"
                    }
                },
                Recipients = message.ParticipantEmails
            };

            _reliableEmailService.EnqueueMessage(emailMessageDto);
        }
    }
}