﻿namespace Smt.Atomic.Business.Notifications.Enums
{
    public enum AllocationEvent
    {
        OnFixedAdded = 1,
        OnPlannedAdded = 2,
        OnPlannedToFixed = 3,
        OnChanged = 4,
        OnDeleted = 5,
    }
}
