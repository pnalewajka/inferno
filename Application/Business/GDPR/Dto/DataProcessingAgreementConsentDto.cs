﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.GDPR.Dto
{
    public class DataProcessingAgreementConsentDto
    {
        public long Id { get; set; }

        public long AgreementId { get; set; }

        public long UserId { get; set; }

        public bool IsApproved { get; set; }

        public List<DocumentDto> Documents { get; set; }

        public List<long> ProjectIds { get; set; }

        public DateTime ModifiedOn { get; set; }
    }
}
