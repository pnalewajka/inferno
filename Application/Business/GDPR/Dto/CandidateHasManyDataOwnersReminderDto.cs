﻿namespace Smt.Atomic.Business.GDPR.Dto
{
    public class CandidateHasManyDataOwnersReminderDto
    {
        public string CandidateUrl { get; set; }

        public string DisplayName { get; set; }

        public string[] DataOwnerUrls { get; set; }
    }
}
