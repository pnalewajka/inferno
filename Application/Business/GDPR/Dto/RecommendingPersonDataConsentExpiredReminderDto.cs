﻿using System;

namespace Smt.Atomic.Business.GDPR.Dto
{
    public class RecommendingPersonDataConsentExpiredReminderDto
    {
        public string RecommendingPersonUrl { get; set; }

        public string DisplayName { get; set; }

        public DateTime ExpiresOn { get; set; }

        public string DataConsentUrl { get; set; }

        public bool IsAnonymized { get; set; }
    }
}