﻿using System.Collections.ObjectModel;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.GDPR;

namespace Smt.Atomic.Business.GDPR.Dto
{
    public class DataProcessingAgreementDtoToDataProcessingAgreementMapping : ClassMapping<DataProcessingAgreementDto, DataProcessingAgreement>
    {
        public DataProcessingAgreementDtoToDataProcessingAgreementMapping()
        {
            Mapping = d => new DataProcessingAgreement
            {
                Id = d.Id,
                ValidFrom = d.ValidFrom,
                ValidTo = d.ValidTo,
                Documents = new Collection<DataProcessingAgreementDocument>(),
                Projects = d.ProjectIds.Select(p => new Project { Id = p }).ToList(),
                RequiredUsers = d.RequiredUserIds.Select(u => new User { Id = u }).ToList(),
                Consents = d.RequiredUserIds.Select(u => new DataProcessingAgreementConsent { AgreementId = d.Id, UserId = u }).ToList(),
            };
        }
    }
}
