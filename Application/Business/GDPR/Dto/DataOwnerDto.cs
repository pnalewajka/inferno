﻿using System;

namespace Smt.Atomic.Business.GDPR.Dto
{
    public class DataOwnerDto
    {
        public long Id { get; set; }

        public long? UserId { get; set; }

        public long? EmployeeId { get; set; }

        public long? CandidateId { get; set; }

        public long? RecommendingPersonId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string EmailAddress { get; set; }

        public string PhoneNumber { get; set; }

        public string SocialLink { get; set; }

        public bool? HasValidCandidateConsent { get; set; }

        public bool? HasValidRecommendingPersonConsent { get; set; }

        public bool? HasValidEmployeeConsent { get; set; }

        public bool? HasValidUserConsent { get; set; }

        public byte[] Timestamp { get; set; }

        public bool IsPotentialDuplicate { get; set; }
    }
}
