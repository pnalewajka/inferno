﻿using System;

namespace Smt.Atomic.Business.GDPR.Dto
{
    public class CandidateDataConsentExpiringReminderDto
    {
        public string CandidateUrl { get; set; }

        public string DisplayName { get; set; }

        public DateTime ExpiresOn { get; set; }

        public string DataConsentUrl { get; set; }
    }
}