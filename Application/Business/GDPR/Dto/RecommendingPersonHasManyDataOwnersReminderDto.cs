﻿namespace Smt.Atomic.Business.GDPR.Dto
{
    public class RecommendingPersonHasManyDataOwnersReminderDto
    {
        public string RecommendingPersonUrl { get; set; }

        public string DisplayName { get; set; }

        public string[] DataOwnerUrls { get; set; }
    }
}