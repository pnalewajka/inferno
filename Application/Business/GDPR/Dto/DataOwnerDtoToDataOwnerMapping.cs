﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.GDPR;

namespace Smt.Atomic.Business.GDPR.Dto
{
    public class DataOwnerDtoToDataOwnerMapping : ClassMapping<DataOwnerDto, DataOwner>
    {
        public DataOwnerDtoToDataOwnerMapping()
        {
            Mapping = d => new DataOwner
            {
                Id = d.Id,
                UserId = d.UserId,
                EmployeeId = d.EmployeeId,
                CandidateId = d.CandidateId,
                RecommendingPersonId = d.RecommendingPersonId,
                IsPotentialDuplicate = d.IsPotentialDuplicate,
                Timestamp = d.Timestamp
            };
        }
    }
}
