﻿using System.Collections.Generic;
using System;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.GDPR.Dto
{
    public class DataProcessingAgreementDto
    {
        public long Id { get; set; }

        public List<long> ProjectIds { get; set; }

        public List<long> RequiredUserIds { get; set; }

        public DateTime ValidFrom { get; set; }

        public DateTime ValidTo { get; set; }

        public List<DocumentDto> Documents { get; set; }

        public DataProcessingAgreementDto()
        {
            ProjectIds = new List<long>();
            RequiredUserIds = new List<long>();
            Documents = new List<DocumentDto>();
        }
    }
}
