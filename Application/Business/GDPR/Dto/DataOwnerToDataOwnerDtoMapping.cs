﻿using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.GDPR.BusinessLogics;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.GDPR;

namespace Smt.Atomic.Business.GDPR.Dto
{
    public class DataOwnerToDataOwnerDtoMapping : ClassMapping<DataOwner, DataOwnerDto>
    {
        public DataOwnerToDataOwnerDtoMapping(ITimeService timeService)
        {
            Mapping = e => new DataOwnerDto
            {
                Id = e.Id,
                UserId = e.UserId,
                EmployeeId = e.EmployeeId,
                CandidateId = e.CandidateId,
                RecommendingPersonId = e.RecommendingPersonId,
                IsPotentialDuplicate = e.IsPotentialDuplicate,
                FirstName = DataOwnerBusinessLogic.FirstName.Call(e),
                LastName = DataOwnerBusinessLogic.LastName.Call(e),
                EmailAddress = DataOwnerBusinessLogic.EmailAddress.Call(e),
                PhoneNumber = DataOwnerBusinessLogic.PhoneNumber.Call(e),
                SocialLink = DataOwnerBusinessLogic.SocialLink.Call(e),
                HasValidCandidateConsent = e.CandidateId != null 
                                           ? CandidateBusinessLogic.HasValidConsent.Call(e.Candidate, timeService.GetCurrentDate()) 
                                           : (bool?)null,
                HasValidRecommendingPersonConsent = e.RecommendingPersonId != null
                                                    ? RecommendingPersonBusinessLogic.HasValidConsent.Call(e.RecommendingPerson, timeService.GetCurrentDate())
                                                    : (bool?)null,
                HasValidEmployeeConsent = e.EmployeeId != null
                                          ? EmployeeBusinessLogic.HasValidConsent.Call(e.Employee, timeService.GetCurrentDate())
                                          : (bool?)null,
                HasValidUserConsent = e.UserId != null
                                      ? UserBusinessLogic.HasValidConsent.Call(e.User, timeService.GetCurrentDate())
                                      : (bool?)null,
                Timestamp = e.Timestamp
            };
        }
    }
}
