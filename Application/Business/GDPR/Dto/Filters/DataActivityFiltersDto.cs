﻿using System;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.GDPR.Dto.Filters
{
    public class DataActivityFiltersDto
    {
        public long? OwnerId { get; set; }

        public ActivityType[] ActivityTypes { get; set; }

        public long? UserId { get; set; }

        public DateTime? From { get; set; }

        public DateTime? To { get; set; }
    }
}
