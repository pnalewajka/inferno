﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.GDPR;

namespace Smt.Atomic.Business.GDPR.Dto
{
    public class DataProcessingAgreementConsentToDataProcessingAgreementConsentDtoMapping : ClassMapping<DataProcessingAgreementConsent, DataProcessingAgreementConsentDto>
    {
        public DataProcessingAgreementConsentToDataProcessingAgreementConsentDtoMapping()
        {
            Mapping = e => new DataProcessingAgreementConsentDto
            {
                Id = e.Id,
                AgreementId = e.AgreementId,
                UserId = e.UserId,
                IsApproved = e.IsApproved,
                ProjectIds = e.Agreement.Projects.GetIds().ToList(),
                Documents = e.Agreement.Documents.Select(p => new DocumentDto
                {
                    DocumentId = p.Id,
                    ContentType = p.ContentType,
                    DocumentName = p.Name,
                }).ToList(),
                ModifiedOn = e.ModifiedOn,
            };
        }
    }
}
