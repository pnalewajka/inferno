﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.GDPR;

namespace Smt.Atomic.Business.GDPR.Dto
{
    public class DataProcessingAgreementConsentDtoToDataProcessingAgreementConsentMapping : ClassMapping<DataProcessingAgreementConsentDto, DataProcessingAgreementConsent>
    {
        public DataProcessingAgreementConsentDtoToDataProcessingAgreementConsentMapping()
        {
            Mapping = d => new DataProcessingAgreementConsent
            {
                Id = d.Id,
                AgreementId = d.AgreementId,
                UserId = d.UserId,
                IsApproved = d.IsApproved,
                ModifiedOn = d.ModifiedOn,
            };
        }
    }
}
