﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.GDPR;

namespace Smt.Atomic.Business.GDPR.Dto
{
    public class DataProcessingAgreementToDataProcessingAgreementDtoMapping : ClassMapping<DataProcessingAgreement, DataProcessingAgreementDto>
    {
        public DataProcessingAgreementToDataProcessingAgreementDtoMapping()
        {
            Mapping = e => new DataProcessingAgreementDto
            {
                Id = e.Id,
                ProjectIds = e.Projects.GetIds().ToList(),
                RequiredUserIds = e.RequiredUsers.GetIds().ToList(),
                ValidFrom = e.ValidFrom,
                ValidTo = e.ValidTo,
                Documents = e.Documents.Select(p => new DocumentDto
                {
                    DocumentId = p.Id,
                    ContentType = p.ContentType,
                    DocumentName = p.Name,
                }).ToList(),
            };
        }
    }
}
