﻿namespace Smt.Atomic.Business.GDPR.Dto
{
    public class ActivityTypeDto
    {
        public long Id { get; set; }

        public string Description { get; set; }
    }
}
