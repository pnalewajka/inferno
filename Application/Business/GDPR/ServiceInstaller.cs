﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Interfaces.GDPR;
using Smt.Atomic.Business.GDPR.Chores;
using Smt.Atomic.Business.GDPR.Interfaces;
using Smt.Atomic.Business.GDPR.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.GDPR
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            if (containerType == ContainerType.WebApp)
            {
                container.Register(Component.For<IDataOwnerCardIndexDataService>().ImplementedBy<DataOwnerCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IDataActivityCardIndexDataService>().ImplementedBy<DataActivityCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IDataConsentCardIndexDataService>().ImplementedBy<DataConsentCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IDataOwnerPickerCardIndexDataService>().ImplementedBy<DataOwnerPickerCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IDataOwnerDataConsentCardIndexDataService>().ImplementedBy<DataOwnerDataConsentCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IDataOwnerDataActivityCardIndexDataService>().ImplementedBy<DataOwnerDataActivityCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ICandidateDataConsentCardIndexDataService>().ImplementedBy<CandidateDataConsentCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ICandidateDataActivityCardIndexDataService>().ImplementedBy<CandidateDataActivityCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IRecommendingPersonDataConsentCardIndexDataService>().ImplementedBy<RecommendingPersonDataConsentCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IRecommendingPersonDataActivityCardIndexDataService>().ImplementedBy<RecommendingPersonDataActivityCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IDataOwnerService>().ImplementedBy<DataOwnerService>().LifestyleTransient());
                container.Register(Component.For<IDataProcessingAgreementService>().ImplementedBy<DataProcessingAgreementService>().LifestyleTransient());
                container.Register(Component.For<IActivityTypeCardIndexDataService>().ImplementedBy<ActivityTypeCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IDataProcessingAgreementCardIndexDataService>().ImplementedBy<DataProcessingAgreementCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IDataProcessingAgreementConsentCardIndexDataService>().ImplementedBy<DataProcessingAgreementConsentCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IChoreProvider>().ImplementedBy<PendingConsentChoreProvider>().LifestylePerWebRequest());
            }
        }
    }
}
