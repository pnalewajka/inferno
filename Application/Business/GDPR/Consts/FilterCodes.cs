﻿namespace Smt.Atomic.Business.GDPR.Consts
{
    public static class FilterCodes
    {
        public const string DataActivityFilters = "data-activity-properties-fiters";

        public const string ConsentApproved = "approved";

        public const string ConsentPending = "pending";

        public const string OwnConsents = "own";

        public const string AllConsents = "all";
    }
}
