﻿using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.GDPR;

namespace Smt.Atomic.Business.GDPR.BusinessLogics
{
    public static class DataOwnerBusinessLogic
    {
        public static readonly BusinessLogic<DataOwner, string> FirstName =
            new BusinessLogic<DataOwner, string>(
                dataOwner => dataOwner.Employee != null ? dataOwner.Employee.FirstName
                : dataOwner.Candidate != null ? dataOwner.Candidate.FirstName
                : dataOwner.RecommendingPerson != null ? dataOwner.RecommendingPerson.FirstName
                : dataOwner.User != null ? dataOwner.User.FirstName
                : null);

        public static readonly BusinessLogic<DataOwner, string> LastName =
            new BusinessLogic<DataOwner, string>(
                dataOwner => dataOwner.Employee != null ? dataOwner.Employee.LastName
                : dataOwner.Candidate != null ? dataOwner.Candidate.LastName
                : dataOwner.RecommendingPerson != null ? dataOwner.RecommendingPerson.LastName
                : dataOwner.User != null ? dataOwner.User.LastName
                : null);

        public static readonly BusinessLogic<DataOwner, string> EmailAddress =
            new BusinessLogic<DataOwner, string>(
                dataOwner => dataOwner.Employee != null ? dataOwner.Employee.Email
                : dataOwner.Candidate != null ? dataOwner.Candidate.EmailAddress
                : dataOwner.RecommendingPerson != null ? dataOwner.RecommendingPerson.EmailAddress
                : dataOwner.User != null ? dataOwner.User.Email
                : null);

        public static readonly BusinessLogic<DataOwner, string> PhoneNumber =
            new BusinessLogic<DataOwner, string>(
                dataOwner => dataOwner.Employee != null ? dataOwner.Employee.PhoneNumber
                : dataOwner.Candidate != null ? dataOwner.Candidate.MobilePhone ?? dataOwner.Candidate.OtherPhone
                : dataOwner.RecommendingPerson != null ? dataOwner.RecommendingPerson.PhoneNumber
                : null);

        public static readonly BusinessLogic<DataOwner, string> SocialLink =
            new BusinessLogic<DataOwner, string>(
                dataOwner => dataOwner.Candidate != null ? dataOwner.Candidate.SocialLink 
                : null);

        public static readonly BusinessLogic<DataOwner, DataOwner, bool> CanBeMerged =
            new BusinessLogic<DataOwner, DataOwner, bool>(
                (sourceOwner, targetOwner) => (sourceOwner.EmployeeId == null || targetOwner.EmployeeId == null || sourceOwner.EmployeeId == targetOwner.EmployeeId)
                    && (sourceOwner.CandidateId == null || targetOwner.CandidateId == null || sourceOwner.CandidateId == targetOwner.CandidateId)
                    && (sourceOwner.RecommendingPersonId == null || targetOwner.RecommendingPersonId == null || sourceOwner.RecommendingPersonId == targetOwner.RecommendingPersonId)
                    && (sourceOwner.UserId == null || targetOwner.UserId == null || sourceOwner.UserId == targetOwner.UserId)
                );
    }
}
