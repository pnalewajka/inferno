﻿using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.GDPR;

namespace Smt.Atomic.Business.GDPR.BusinessLogics
{
    public static class DataProcessingAgreementConsentBusinessLogic
    {
        public static BusinessLogic<DataProcessingAgreementConsent, long, bool> IsUserConsent =
            new BusinessLogic<DataProcessingAgreementConsent, long, bool>(
                (consent, userId) => consent.UserId == userId);

        public static BusinessLogic<DataProcessingAgreementConsent, long, bool> CanBeApprovedByUser =
            new BusinessLogic<DataProcessingAgreementConsent, long, bool>(
                (consent, userId) => !consent.IsApproved && IsUserConsent.Call(consent, userId));
    }
}
