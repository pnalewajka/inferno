﻿using System.Linq;
using Smt.Atomic.Business.GDPR.Dto.Filters;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.GDPR;

namespace Smt.Atomic.Business.GDPR.BusinessLogics
{
    public static class DataActivityBusinessLogic
    {
        public static BusinessLogic<DataActivity, DataActivityFiltersDto, bool> DataActivityPropertiesFiltering =
           new BusinessLogic<DataActivity, DataActivityFiltersDto, bool>(
               (dataActivity, filterDto) =>
               (!filterDto.OwnerId.HasValue || dataActivity.DataOwnerId == filterDto.OwnerId.Value)
               && (!filterDto.ActivityTypes.Any() || filterDto.ActivityTypes.Any(at => at == dataActivity.ActivityType))
               && (!filterDto.UserId.HasValue || dataActivity.PerformedByUserId == filterDto.UserId.Value)
               && (!filterDto.From.HasValue || (dataActivity.PerformedOn != null && dataActivity.PerformedOn >= filterDto.From.Value))
               && (!filterDto.To.HasValue || (dataActivity.PerformedOn != null && dataActivity.PerformedOn <= filterDto.To.Value)));
    }
}
