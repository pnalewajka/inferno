﻿using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.GDPR.Interfaces
{
    public interface IDataProcessingAgreementService
    {
        bool ApproveConsent(long consentId);
    }
}
