﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.GDPR.Dto;

namespace Smt.Atomic.Business.GDPR.Interfaces
{
    public interface IDataOwnerPickerCardIndexDataService : ICardIndexDataService<DataOwnerDto>
    {
    }
}
