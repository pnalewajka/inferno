﻿using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.GDPR.Interfaces
{
    public interface IDataOwnerService
    {
        BoolResult MergeDataOwners(long sourceOwnerId, long targetOwnerId);
    }
}
