﻿using Smt.Atomic.Business.Common.Dto.GDPR;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.GDPR.Interfaces
{
    public interface IDataOwnerDataConsentCardIndexDataService : ICardIndexDataService<DataConsentDto>
    {
        DataConsentDto GetDefaultNewRecord(long dataOwnerId, DataConsentType consentType);
    }
}
