﻿using Smt.Atomic.Business.Common.Dto.GDPR;
using Smt.Atomic.Business.Common.Interfaces;

namespace Smt.Atomic.Business.GDPR.Interfaces
{
    public interface ICandidateDataActivityCardIndexDataService : ICardIndexDataService<DataActivityDto>
    {
        DataActivityDto GetDefaultNewRecord(long candidateId);
    }
}