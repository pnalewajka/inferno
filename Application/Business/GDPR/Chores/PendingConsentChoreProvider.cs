﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.GDPR.Chores
{
    public class PendingConsentChoreProvider
        : IChoreProvider
    {
        private readonly IPrincipalProvider _principalProvider;

        public PendingConsentChoreProvider(
            IPrincipalProvider principalProvider)
        {
            _principalProvider = principalProvider;
        }

        public IQueryable<ChoreDto> GetActiveChores(
            IReadOnlyRepositoryFactory repositoryFactory)
        {
            if (!_principalProvider.Current.IsInRole(SecurityRoleType.CanViewDataProcessingAgreementConsent))
            {
                return null;
            }

            // NOT FILTERED for query performance
            var consents = repositoryFactory.Create<DataProcessingAgreementConsent>();

            var query = consents
                .Where(c => !c.IsApproved)
                .Where(c => c.UserId == _principalProvider.Current.Id)
                .GroupBy(e => 0)
                .Select(g => new ChoreDto
                {
                    Type = ChoreType.PendingDataProcessingAgreementConsent,
                    DueDate = g.Min(c => c.ModifiedOn),
                    Parameters = g.Count().ToString(),
                });

            return query;
        }
    }
}
