﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.GDPR.BusinessLogics;
using Smt.Atomic.Business.GDPR.Dto;
using Smt.Atomic.Business.GDPR.Interfaces;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.GDPR.Services
{
    public class DataOwnerCardIndexDataService : CardIndexDataService<DataOwnerDto, DataOwner, IGDPRDbScope>, IDataOwnerCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public DataOwnerCardIndexDataService(ICardIndexServiceDependencies<IGDPRDbScope> dependencies)
            : base(dependencies)
        {
            DeletionStrategy = DeletionStrategy.Remove;
        }

        protected override IEnumerable<Expression<Func<DataOwner, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return o => o.Candidate.FirstName;
            yield return o => o.Candidate.LastName;
            yield return o => o.Candidate.EmailAddress;
            yield return o => o.Candidate.OtherEmailAddress;
            yield return o => o.Candidate.MobilePhone;
            yield return o => o.Candidate.OtherPhone;

            yield return o => o.RecommendingPerson.FirstName;
            yield return o => o.RecommendingPerson.LastName;
            yield return o => o.RecommendingPerson.EmailAddress;
            yield return o => o.RecommendingPerson.PhoneNumber;

            yield return o => o.Employee.FirstName;
            yield return o => o.Employee.LastName;
            yield return o => o.Employee.Email;
            yield return o => o.Employee.PhoneNumber;

            yield return o => o.User.FirstName;
            yield return o => o.User.LastName;
            yield return o => o.User.Email;
            yield return o => o.User.Login;
        }

        protected override IQueryable<DataOwner> ConfigureIncludes(IQueryable<DataOwner> sourceQueryable)
        {
            return sourceQueryable
                .Include(o => o.User)
                .Include(o => o.Candidate)
                .Include(o => o.RecommendingPerson)
                .Include(o => o.Employee);
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<DataOwner> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var direction = sortingCriterion.GetSortingDirection();

            if (sortingCriterion.GetSortingColumnName() == nameof(DataOwnerDto.FirstName))
            {
                orderedQueryBuilder.ApplySortingKey(DataOwnerBusinessLogic.FirstName.AsExpression(), direction);
            }
            else if (sortingCriterion.GetSortingColumnName() == nameof(DataOwnerDto.LastName))
            {
                orderedQueryBuilder.ApplySortingKey(DataOwnerBusinessLogic.LastName.AsExpression(), direction);
            }
            else if (sortingCriterion.GetSortingColumnName() == nameof(DataOwnerDto.EmailAddress))
            {
                orderedQueryBuilder.ApplySortingKey(DataOwnerBusinessLogic.EmailAddress.AsExpression(), direction);
            }
            else if (sortingCriterion.GetSortingColumnName() == nameof(DataOwnerDto.PhoneNumber))
            {
                orderedQueryBuilder.ApplySortingKey(DataOwnerBusinessLogic.PhoneNumber.AsExpression(), direction);
            }
            else if (sortingCriterion.GetSortingColumnName() == nameof(DataOwnerDto.SocialLink))
            {
                orderedQueryBuilder.ApplySortingKey(DataOwnerBusinessLogic.SocialLink.AsExpression(), direction);
            }
            else
            {
                base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
            }
        }
    }
}
