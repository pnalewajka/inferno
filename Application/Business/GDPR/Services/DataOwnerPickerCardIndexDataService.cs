﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.GDPR.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.GDPR.Services
{
    public class DataOwnerPickerCardIndexDataService : DataOwnerCardIndexDataService,
        IDataOwnerPickerCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public DataOwnerPickerCardIndexDataService(ICardIndexServiceDependencies<IGDPRDbScope> dependencies) : base(dependencies)
        {
        }
    }
}
