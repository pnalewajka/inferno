﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto.GDPR;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.GDPR.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.GDPR.Services
{
    public class RecommendingPersonDataConsentCardIndexDataService : DataConsentCardIndexDataService, IRecommendingPersonDataConsentCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public RecommendingPersonDataConsentCardIndexDataService(
            ICardIndexServiceDependencies<IGDPRDbScope> dependencies,
            IUploadedDocumentHandlingService uploadedDocumentHandlingService,
            IDataActivityLogService dataActivityLogService,
            ICompanyService companyService)
            : base(dependencies, uploadedDocumentHandlingService, dataActivityLogService, companyService)
        {
        }

        public override IQueryable<DataConsent> ApplyContextFiltering(IQueryable<DataConsent> records, object context)
        {
            if (context is ParentIdContext parentIdContext)
            {
                return records.Where(c => c.DataOwner.RecommendingPersonId == parentIdContext.ParentId);
            }

            return base.ApplyContextFiltering(records, context);
        }

        public DataConsentDto GetDefaultNewRecord(long recommendingPersonId, DataConsentType consentType)
        {
            var defaultRecord = base.GetDefaultNewRecord();

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var recommendingPerson = unitOfWork.Repositories.RecommendingPersons.GetById(recommendingPersonId);
                var dataOwner = recommendingPerson.DataOwners.SingleOrDefault();

                if (dataOwner != null)
                {
                    defaultRecord.DataOwnerId = dataOwner.Id;
                }
            }

            return SetDataConsentDtoConsentType(defaultRecord, consentType);
        }
    }
}