﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto.GDPR;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.GDPR.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.GDPR.Services
{
    public class DataOwnerDataConsentCardIndexDataService : DataConsentCardIndexDataService, IDataOwnerDataConsentCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public DataOwnerDataConsentCardIndexDataService(
            ICardIndexServiceDependencies<IGDPRDbScope> dependencies,
            IUploadedDocumentHandlingService uploadedDocumentHandlingService,
            IDataActivityLogService dataActivityLogService,
            ICompanyService companyService)
            : base(dependencies, uploadedDocumentHandlingService, dataActivityLogService, companyService)
        {
        }

        public override IQueryable<DataConsent> ApplyContextFiltering(IQueryable<DataConsent> records, object context)
        {
            if (context is ParentIdContext parentIdContext)
            {
                return records.Where(c => c.DataOwnerId == parentIdContext.ParentId);
            }

            return base.ApplyContextFiltering(records, context);
        }

        public DataConsentDto GetDefaultNewRecord(long dataOwnerId, DataConsentType consentType)
        {
            var defaultRecord = base.GetDefaultNewRecord();

            defaultRecord.DataOwnerId = dataOwnerId;

            return SetDataConsentDtoConsentType(defaultRecord, consentType);
        }
    }
}
