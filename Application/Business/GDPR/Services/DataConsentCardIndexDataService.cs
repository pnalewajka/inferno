﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Dto.GDPR;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Interfaces.GDPR;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.GDPR.DocumentMappings;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.GDPR.Services
{
    public class DataConsentCardIndexDataService : CardIndexDataService<DataConsentDto, DataConsent, IGDPRDbScope>, IDataConsentCardIndexDataService
    {
        private readonly ICardIndexServiceDependencies<IGDPRDbScope> _dependencies;
        private readonly IUploadedDocumentHandlingService _uploadedDocumentHandlingService;
        private readonly IDataActivityLogService _dataActivityLogService;
        private readonly ICompanyService _companyService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public DataConsentCardIndexDataService(
            ICardIndexServiceDependencies<IGDPRDbScope> dependencies,
            IUploadedDocumentHandlingService uploadedDocumentHandlingService,
            IDataActivityLogService dataActivityLogService,
            ICompanyService companyService)
            : base(dependencies)
        {
            _dependencies = dependencies;
            _uploadedDocumentHandlingService = uploadedDocumentHandlingService;
            _dataActivityLogService = dataActivityLogService;
            _companyService = companyService;
        }

        public override IQueryable<DataConsent> ApplyContextFiltering(IQueryable<DataConsent> records, object context)
        {
            var companyPickerContext = context as CompanyContext;

            if (companyPickerContext != null)
            {
                if (companyPickerContext.IsDataAdministrator != null)
                {
                    records = records.Where(r => r.DataAdministrator.IsDataAdministrator == companyPickerContext.IsDataAdministrator.Value);
                }
            }

            return records;
        }

        public long GetDataConsentIdByDataConsentDocumentId(long dataConsentDocumentId)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                return unitOfWork.Repositories.DataConsentDocuments
                    .Filtered()
                    .SelectById(dataConsentDocumentId, c => c.DataConsentId);
            }
        }

        public string GetDocumentNameByDataConsentDocumentId(long dataConsentDocumentId)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                return unitOfWork.Repositories.DataConsentDocuments
                    .Filtered()
                    .SelectById(dataConsentDocumentId, c => c.Name);
            }
        }

        public long GetDataOwnerIdByDataConsentId(long dataConsentId)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                return unitOfWork.Repositories.DataConsents
                    .Filtered()
                    .SelectById(dataConsentId, c => c.DataOwnerId);
            }
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<DataConsent, DataConsentDto, IGDPRDbScope> eventArgs)
        {
            const string defaultHtmlDocumentName = "InboundEmailContent.html";
            const string defaultTxtDocumentName = "InboundEmailContent.txt";

            if (eventArgs.InputDto.FileSource == FileSource.Disk)
            {
                _uploadedDocumentHandlingService
                    .MergeDocumentCollections(
                        eventArgs.UnitOfWork,
                        eventArgs.InputEntity.Attachments,
                        eventArgs.InputDto.Documents,
                        p => new DataConsentDocument
                        {
                            Name = p.DocumentName,
                            ContentType = p.ContentType
                        }
                    );
            }
            else if (eventArgs.InputDto.FileSource == FileSource.InboundEmailAttachment)
            {
                var unitOfWork = eventArgs.UnitOfWork;
                var documentId = eventArgs.InputDto.Documents.Select(d => d.DocumentId).Single();
                var dataConsentDocumentInfo = unitOfWork.Repositories.InboundEmailDocuments.SelectById(documentId.Value,
                    d => new { d.ContentType, d.ContentLength, d.Name, d.DocumentContent.Content });

                eventArgs.InputEntity.Attachments.Add(new DataConsentDocument
                {
                    ContentType = dataConsentDocumentInfo.ContentType,
                    ContentLength = dataConsentDocumentInfo.ContentLength,
                    Name = dataConsentDocumentInfo.Name,
                    DocumentContent = new DataConsentDocumentContent
                    {
                        Content = dataConsentDocumentInfo.Content
                    }
                });
            }
            else if (eventArgs.InputDto.FileSource == FileSource.InboundEmailContent)
            {
                var unitOfWork = eventArgs.UnitOfWork;
                var inboundEmailId = eventArgs.InputDto.Documents.Select(d => d.DocumentId).Single();
                var dataConsentDocumentInfo = unitOfWork.Repositories.InboundEmails.SelectById(inboundEmailId.Value, d => new { d.BodyType, d.Body });
                var content = Encoding.UTF8.GetBytes(dataConsentDocumentInfo.Body);

                eventArgs.InputEntity.Attachments.Add(new DataConsentDocument
                {
                    ContentType = dataConsentDocumentInfo.BodyType == EmailBodyType.Html ? MimeHelper.HtmlFile : MimeHelper.TextFile,
                    ContentLength = content.Length,
                    Name = dataConsentDocumentInfo.BodyType == EmailBodyType.Html ? defaultHtmlDocumentName : defaultTxtDocumentName,
                    DocumentContent = new DataConsentDocumentContent
                    {
                        Content = content
                    }
                });
            }
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<DataConsent, DataConsentDto, IGDPRDbScope> eventArgs)
        {
            _uploadedDocumentHandlingService
                .MergeDocumentCollections(
                    eventArgs.UnitOfWork,
                    eventArgs.DbEntity.Attachments,
                    eventArgs.InputDto.Documents,
                    p => new DataConsentDocument
                    {
                        Name = p.DocumentName,
                        ContentType = p.ContentType
                    }
                );
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<DataConsent, IGDPRDbScope> eventArgs)
        {
            base.OnRecordDeleting(eventArgs);
            eventArgs.UnitOfWork.Repositories.DataConsentDocuments.DeleteEach(p => p.DataConsentId == eventArgs.DeletingRecord.Id);
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<DataConsent, DataConsentDto> eventArgs)
        {
            _dataActivityLogService.LogDataOwnerDataActivity(eventArgs.InputEntity.DataOwnerId, ActivityType.CreatedRecord, GetType().Name);

            _uploadedDocumentHandlingService
                .PromoteTemporaryDocuments<DataConsentDocument, DataConsentDocumentContent, DataConsentDocumentMapping, IGDPRDbScope>(
                    eventArgs.InputDto.Documents
                );

            LogDataActivityIfConsentExpired(eventArgs.InputEntity);
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<DataConsent, DataConsentDto> eventArgs)
        {
            _dataActivityLogService.LogDataOwnerDataActivity(eventArgs.InputEntity.DataOwnerId, ActivityType.UpdatedRecord, GetType().Name);

            _uploadedDocumentHandlingService
                .PromoteTemporaryDocuments<DataConsentDocument, DataConsentDocumentContent, DataConsentDocumentMapping, IGDPRDbScope>(
                    eventArgs.InputDto.Documents
                );

            LogDataActivityIfConsentExpired(eventArgs.DbEntity);
        }

        protected override void OnRecordsDeleted(RecordDeletedEventArgs<DataConsent> eventArgs)
        {
            foreach (var entitie in eventArgs.DeletedEntities)
            {
                _dataActivityLogService.LogDataOwnerDataActivity(entitie.DataOwnerId, ActivityType.RemovedRecord, GetType().Name);
            }

            base.OnRecordsDeleted(eventArgs);
        }

        protected override IEnumerable<Expression<Func<DataConsent, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return c => c.DataOwner.User.FirstName;
            yield return c => c.DataOwner.User.LastName;
            yield return c => c.DataOwner.User.Email;

            yield return c => c.DataOwner.Employee.FirstName;
            yield return c => c.DataOwner.Employee.LastName;
            yield return c => c.DataOwner.Employee.Email;
            yield return c => c.DataOwner.Employee.PhoneNumber;

            yield return c => c.DataOwner.Candidate.FirstName;
            yield return c => c.DataOwner.Candidate.LastName;
            yield return c => c.DataOwner.Candidate.EmailAddress;
            yield return c => c.DataOwner.Candidate.OtherEmailAddress;
            yield return c => c.DataOwner.Candidate.MobilePhone;
            yield return c => c.DataOwner.Candidate.OtherPhone;

            yield return c => c.DataOwner.RecommendingPerson.FirstName;
            yield return c => c.DataOwner.RecommendingPerson.LastName;
            yield return c => c.DataOwner.RecommendingPerson.EmailAddress;
            yield return c => c.DataOwner.RecommendingPerson.PhoneNumber;
        }

        public override DataConsentDto GetDefaultNewRecord()
        {
            var consent = base.GetDefaultNewRecord();
            consent.DataAdministratorId = GetDefaultCompanyId();

            return consent;
        }

        public DataConsentDto GetDefaultNewRecord(DataConsentType consentType)
        {
            var defaultRecord = base.GetDefaultNewRecord();

            return SetDataConsentDtoConsentType(defaultRecord, consentType);
        }

        protected DataConsentDto SetDataConsentDtoConsentType(DataConsentDto dataConsentDto, DataConsentType consentType)
        {
            dataConsentDto.Type = consentType;

            if (DataConsentBusinessLogic.DoesConsentTypeRequireExpirationDate.Call(consentType))
            {
                dataConsentDto.ExpiresOn = _dependencies.TimeService.GetCurrentDate().AddDays(
                    _dependencies.SystemParameters.GetParameter<int>(ParameterKeys.RecruitmentDefaultConsentExpirationDays));
            }

            return dataConsentDto;
        }

        private void LogDataActivityIfConsentExpired(DataConsent dataConsent)
        {
            var today = TimeService.GetCurrentDate();

            if (!DataConsentBusinessLogic.IsExpired.Call(dataConsent, today))
            {
                return;
            }

            _dataActivityLogService.LogDataOwnerDataActivity(dataConsent.DataOwnerId,
                ActivityType.DataConsentExpired,
                ReferenceType.DataConsent,
                dataConsent.Id,
                dataConsent.Type.GetDescription());
        }

        private long GetDefaultCompanyId()
        {
            var defaultCompanyCode = SystemParameters.GetParameter<string>(ParameterKeys.RecruitmentDefaultCompanyActiveDirectoryCode);
            var defaultCompanyId = _companyService.GetCompanyIdOrDefaultByActiveDirectorycode(defaultCompanyCode);

            return defaultCompanyId.Value;
        }
    }
}
