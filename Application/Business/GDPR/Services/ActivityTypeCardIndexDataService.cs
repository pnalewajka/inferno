﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.GDPR.Dto;
using Smt.Atomic.Business.GDPR.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Business.GDPR.Services
{
    public class ActivityTypeCardIndexDataService : EnumBasedCardIndexDataService<ActivityTypeDto, ActivityType>, IActivityTypeCardIndexDataService
    {
        protected override ActivityTypeDto ConvertToDto(ActivityType enumValue)
        {
            return new ActivityTypeDto
            {
                Id = (long)enumValue,
                Description = enumValue.GetDescription()
            };
        }

        protected override IEnumerable<Expression<Func<ActivityTypeDto, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return dto => dto.Description;
        }
    }
}
