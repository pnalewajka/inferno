﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.GDPR.BusinessLogics;
using Smt.Atomic.Business.GDPR.Consts;
using Smt.Atomic.Business.GDPR.Dto;
using Smt.Atomic.Business.GDPR.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.GDPR.Services
{
    public class DataProcessingAgreementConsentCardIndexDataService
        : ReadOnlyCardIndexDataService<DataProcessingAgreementConsentDto, DataProcessingAgreementConsent, IGDPRDbScope>
        , IDataProcessingAgreementConsentCardIndexDataService
    {
        public DataProcessingAgreementConsentCardIndexDataService(
            ICardIndexServiceDependencies<IGDPRDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override IEnumerable<Expression<Func<DataProcessingAgreementConsent, object>>> GetSearchColumns(
            SearchCriteria searchCriteria)
        {
            yield return (DataProcessingAgreementConsent c) => c.User.FirstName;
            yield return (DataProcessingAgreementConsent c) => c.User.LastName;
        }

        protected override NamedFilters<DataProcessingAgreementConsent> NamedFilters =>
            new NamedFilters<DataProcessingAgreementConsent>(new[]
                {
                    new NamedFilter<DataProcessingAgreementConsent>(
                        FilterCodes.ConsentApproved,
                        c => c.IsApproved),

                    new NamedFilter<DataProcessingAgreementConsent>(
                        FilterCodes.ConsentPending,
                        c => !c.IsApproved),

                    new NamedFilter<DataProcessingAgreementConsent>(
                        FilterCodes.OwnConsents,
                        DataProcessingAgreementConsentBusinessLogic.IsUserConsent.Parametrize(PrincipalProvider.Current.Id.Value)),

                    new NamedFilter<DataProcessingAgreementConsent>(
                        FilterCodes.AllConsents,
                        c => true,
                        SecurityRoleType.CanViewAllDataProcessingAgreementConsent)
                });
    }
}
