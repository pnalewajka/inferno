﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto.GDPR;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.GDPR.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.GDPR.Services
{
    class CandidateDataActivityCardIndexDataService : DataActivityCardIndexDataService, ICandidateDataActivityCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public CandidateDataActivityCardIndexDataService(
            ICardIndexServiceDependencies<IGDPRDbScope> dependencies,
            IDataActivityLogService dataActivityLogService)
            : base(dependencies, dataActivityLogService)
        {
        }

        public override IQueryable<DataActivity> ApplyContextFiltering(IQueryable<DataActivity> records, object context)
        {
            if (context is ParentIdContext parentIdContext)
            {
                return records.Where(c => c.DataOwner.CandidateId == parentIdContext.ParentId);
            }

            return base.ApplyContextFiltering(records, context);
        }

        public DataActivityDto GetDefaultNewRecord(long candidateId)
        {
            var defaultRecord = base.GetDefaultNewRecord();

            defaultRecord.ReferenceId = candidateId;
            defaultRecord.ReferenceType = ReferenceType.Candidate;

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var candidate = unitOfWork.Repositories.Candidates.GetById(candidateId);
                var dataOwner = candidate.DataOwners.SingleOrDefault();

                if (dataOwner != null)
                {
                    defaultRecord.DataOwnerId = dataOwner.Id;
                }
            }

            return defaultRecord;
        }
    }
}