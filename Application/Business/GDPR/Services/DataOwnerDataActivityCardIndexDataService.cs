﻿using System.Linq;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.GDPR.Interfaces;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.GDPR.Services
{
    public class DataOwnerDataActivityCardIndexDataService : DataActivityCardIndexDataService, IDataOwnerDataActivityCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public DataOwnerDataActivityCardIndexDataService(
            ICardIndexServiceDependencies<IGDPRDbScope> dependencies,
            IDataActivityLogService dataActivityLogService)
            : base(dependencies, dataActivityLogService)
        {
        }

        public override IQueryable<DataActivity> ApplyContextFiltering(IQueryable<DataActivity> records, object context)
        {
            if (context is ParentIdContext parentIdContext)
            {
                return records.Where(c => c.DataOwnerId == parentIdContext.ParentId);
            }

            return base.ApplyContextFiltering(records, context);
        }
    }
}
