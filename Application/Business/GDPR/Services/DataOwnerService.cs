﻿using Castle.Core.Internal;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.GDPR.BusinessLogics;
using Smt.Atomic.Business.GDPR.Interfaces;
using Smt.Atomic.Business.GDPR.Resources;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using System.Collections.Generic;

namespace Smt.Atomic.Business.GDPR.Services
{
    public class DataOwnerService : IDataOwnerService
    {
        private readonly IUnitOfWorkService<IGDPRDbScope> _unitOfWorkService;

        public DataOwnerService(IUnitOfWorkService<IGDPRDbScope> unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        public BoolResult MergeDataOwners(long dataOwnerId, long mergedDataOwnerId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var dataOwner = unitOfWork.Repositories.DataOwners.GetById(dataOwnerId);
                var mergedDataOwner = unitOfWork.Repositories.DataOwners.GetById(mergedDataOwnerId);

                if (!DataOwnerBusinessLogic.CanBeMerged.Call(dataOwner, mergedDataOwner))
                {
                    return UnmergableDataOwnerResult();
                }

                var mergedDataConsents = mergedDataOwner.DataConsents;
                var mergedDataActivities = mergedDataOwner.DataActivity;

                mergedDataConsents.ForEach(c => c.DataOwnerId = dataOwnerId);
                mergedDataActivities.ForEach(c => c.DataOwnerId = dataOwnerId);

                dataOwner.EmployeeId = dataOwner.EmployeeId ?? mergedDataOwner.EmployeeId;
                dataOwner.CandidateId = dataOwner.CandidateId ?? mergedDataOwner.CandidateId;
                dataOwner.RecommendingPersonId = dataOwner.RecommendingPersonId ?? mergedDataOwner.RecommendingPersonId;
                dataOwner.UserId = dataOwner.UserId ?? mergedDataOwner.UserId;

                unitOfWork.Repositories.DataOwners.Delete(mergedDataOwner);

                unitOfWork.Commit();                
            }

            return BoolResult.Success;
        }
    
        private BoolResult UnmergableDataOwnerResult()
        {
            return new BoolResult(false, new List<AlertDto>
            {
                new AlertDto
                {
                    Type = AlertType.Error,
                    Message = DataOwnerResources.CanNotMergeDataOwners
                }
            });
        }
    }
}
