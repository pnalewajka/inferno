﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Dto.GDPR;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Interfaces.GDPR;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.GDPR.BusinessLogics;
using Smt.Atomic.Business.GDPR.Consts;
using Smt.Atomic.Business.GDPR.Dto.Filters;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.GDPR.Services
{
    public class DataActivityCardIndexDataService : CardIndexDataService<DataActivityDto, DataActivity, IGDPRDbScope>, IDataActivityCardIndexDataService
    {
        private readonly IDataActivityLogService _dataActivityLogService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public DataActivityCardIndexDataService(
            ICardIndexServiceDependencies<IGDPRDbScope> dependencies,
            IDataActivityLogService dataActivityLogService)
            : base(dependencies)
        {
            _dataActivityLogService = dataActivityLogService;
        }

        protected override IEnumerable<Expression<Func<DataActivity, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return a => a.DataOwner.User.FirstName;
            yield return a => a.DataOwner.User.LastName;
            yield return a => a.DataOwner.User.Email;

            yield return a => a.DataOwner.Employee.FirstName;
            yield return a => a.DataOwner.Employee.LastName;
            yield return a => a.DataOwner.Employee.Email;
            yield return a => a.DataOwner.Employee.PhoneNumber;

            yield return a => a.DataOwner.Candidate.FirstName;
            yield return a => a.DataOwner.Candidate.LastName;
            yield return a => a.DataOwner.Candidate.EmailAddress;
            yield return a => a.DataOwner.Candidate.OtherEmailAddress;
            yield return a => a.DataOwner.Candidate.MobilePhone;
            yield return a => a.DataOwner.Candidate.OtherPhone;

            yield return a => a.DataOwner.RecommendingPerson.FirstName;
            yield return a => a.DataOwner.RecommendingPerson.LastName;
            yield return a => a.DataOwner.RecommendingPerson.EmailAddress;
            yield return a => a.DataOwner.RecommendingPerson.PhoneNumber;
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<DataActivity, DataActivityDto> recordAddedEventArgs)
        {
            _dataActivityLogService.LogDataOwnerDataActivity(recordAddedEventArgs.InputEntity.DataOwnerId, ActivityType.CreatedRecord, GetType().Name);

            base.OnRecordAdded(recordAddedEventArgs);
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<DataActivity, DataActivityDto> recordEditedEventArgs)
        {
            _dataActivityLogService.LogDataOwnerDataActivity(recordEditedEventArgs.InputEntity.DataOwnerId, ActivityType.UpdatedRecord, GetType().Name);

            base.OnRecordEdited(recordEditedEventArgs);
        }

        protected override void OnRecordsDeleted(RecordDeletedEventArgs<DataActivity> eventArgs)
        {
            foreach (var entitie in eventArgs.DeletedEntities)
            {
                _dataActivityLogService.LogDataOwnerDataActivity(entitie.DataOwnerId, ActivityType.RemovedRecord, GetType().Name);
            }

            base.OnRecordsDeleted(eventArgs);
        }

        public DataActivityDto GetDefaultNewRecord(ReferenceType referenceType)
        {
            var defaultRecord = base.GetDefaultNewRecord();

            defaultRecord.ReferenceType = referenceType;

            return defaultRecord;
        }

        protected override NamedFilters<DataActivity> NamedFilters =>
            new NamedFilters<DataActivity>(
                new[]
                {
                    new NamedDtoFilter<DataActivity, DataActivityFiltersDto>(FilterCodes.DataActivityFilters, DataActivityBusinessLogic.DataActivityPropertiesFiltering.BaseExpression)
                });
    }
}
