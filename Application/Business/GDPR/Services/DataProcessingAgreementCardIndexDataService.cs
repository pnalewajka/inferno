﻿using System.Linq;
using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.GDPR.DocumentMappings;
using Smt.Atomic.Business.GDPR.Dto;
using Smt.Atomic.Business.GDPR.Interfaces;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.GDPR.Services
{
    public class DataProcessingAgreementCardIndexDataService
        : CardIndexDataService<DataProcessingAgreementDto, DataProcessingAgreement, IGDPRDbScope>
        , IDataProcessingAgreementCardIndexDataService
    {
        private readonly IUploadedDocumentHandlingService _uploadedDocumentHandlingService;

        public DataProcessingAgreementCardIndexDataService(
            ICardIndexServiceDependencies<IGDPRDbScope> dependencies,
            IUploadedDocumentHandlingService uploadedDocumentHandlingService)
            : base(dependencies)
        {
            _uploadedDocumentHandlingService = uploadedDocumentHandlingService;
        }

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public override DataProcessingAgreementDto GetDefaultNewRecord()
        {
            var currentDate = TimeService.GetCurrentDate();

            return new DataProcessingAgreementDto
            {
                ValidFrom = currentDate,
                ValidTo = currentDate,
            };
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<DataProcessingAgreement, DataProcessingAgreementDto, IGDPRDbScope> eventArgs)
        {
            base.OnRecordAdding(eventArgs);

            eventArgs.InputEntity.Projects =
                EntityMergingService.CreateEntitiesFromIds<Project, IGDPRDbScope>(
                    eventArgs.UnitOfWork,
                    eventArgs.InputEntity.Projects.GetIds());

            eventArgs.InputEntity.RequiredUsers =
                EntityMergingService.CreateEntitiesFromIds<User, IGDPRDbScope>(
                    eventArgs.UnitOfWork,
                    eventArgs.InputEntity.RequiredUsers.GetIds());

            _uploadedDocumentHandlingService.MergeDocumentCollections(
                eventArgs.UnitOfWork,
                eventArgs.InputEntity.Documents,
                eventArgs.InputDto.Documents,
                p => new DataProcessingAgreementDocument
                {
                    Name = p.DocumentName,
                    ContentType = p.ContentType,
                }
            );

            eventArgs.InputEntity.Consents =
                eventArgs.InputEntity.RequiredUsers.GetIds().Select(
                    userId => new DataProcessingAgreementConsent
                    {
                        Agreement = eventArgs.InputEntity,
                        UserId = userId,
                    }).ToList();
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<DataProcessingAgreement, DataProcessingAgreementDto> recordAddedEventArgs)
        {
            base.OnRecordAdded(recordAddedEventArgs);

            _uploadedDocumentHandlingService.PromoteTemporaryDocuments<
                DataProcessingAgreementDocument,
                DataProcessingAgreementDocumentContent,
                DataProcessingAgreementDocumentMapping,
                IGDPRDbScope>(recordAddedEventArgs.InputDto.Documents);
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<DataProcessingAgreement, DataProcessingAgreementDto, IGDPRDbScope> eventArgs)
        {
            base.OnRecordEditing(eventArgs);

            // never update the documents here, those MUST be readonly when editing

            EntityMergingService.MergeCollections
                (eventArgs.UnitOfWork,
                eventArgs.DbEntity.Projects,
                eventArgs.InputEntity.Projects);

            EntityMergingService.MergeCollections
                (eventArgs.UnitOfWork,
                eventArgs.DbEntity.RequiredUsers,
                eventArgs.InputEntity.RequiredUsers);

            EntityMergingService.MergeCollections(
                eventArgs.UnitOfWork,
                eventArgs.DbEntity.Consents,
                eventArgs.InputEntity.Consents,
                e => $"{e.AgreementId}-{e.UserId}",
                true);
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<DataProcessingAgreement, IGDPRDbScope> eventArgs)
        {
            base.OnRecordDeleting(eventArgs);

            eventArgs.UnitOfWork.Repositories.DataProcessingAgreementConsents
                .DeleteEach(r => r.AgreementId == eventArgs.DeletingRecord.Id);

            eventArgs.UnitOfWork.Repositories.DataProcessingAgreementDocuments
                .DeleteEach(r => r.Agreement.Id == eventArgs.DeletingRecord.Id);

            eventArgs.UnitOfWork.Repositories.DataProcessingAgreementDocumentContents
                .DeleteEach(r => r.Document.Agreement.Id == eventArgs.DeletingRecord.Id);
        }
    }
}
