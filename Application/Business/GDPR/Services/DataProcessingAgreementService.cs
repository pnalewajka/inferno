﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.GDPR.BusinessLogics;
using Smt.Atomic.Business.GDPR.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.GDPR.Services
{
    public class DataProcessingAgreementService
        : IDataProcessingAgreementService
    {
        private readonly IUnitOfWorkService<IGDPRDbScope> _unitOfWorkService;
        private readonly IPrincipalProvider _principalProvider;

        public DataProcessingAgreementService(
            IUnitOfWorkService<IGDPRDbScope> unitOfWorkService,
            IPrincipalProvider principalProvider)
        {
            _unitOfWorkService = unitOfWorkService;
            _principalProvider = principalProvider;
        }

        public bool ApproveConsent(long consentId)
        {
            var currentUserId = _principalProvider.Current.Id.Value;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var agreement = unitOfWork.Repositories.DataProcessingAgreementConsents
                    .Where(c => c.Id == consentId)
                    .Where(c => DataProcessingAgreementConsentBusinessLogic.CanBeApprovedByUser.Call(c, currentUserId))
                    .SingleOrDefault();

                if (agreement != null)
                {
                    agreement.IsApproved = true;

                    unitOfWork.Commit();

                    return true;
                }
            }

            return false;
        }
    }
}
