﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto.GDPR;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.GDPR.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.GDPR.Services
{
    class RecommendingPersonDataActivityCardIndexDataService : DataActivityCardIndexDataService, IRecommendingPersonDataActivityCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public RecommendingPersonDataActivityCardIndexDataService(
            ICardIndexServiceDependencies<IGDPRDbScope> dependencies,
            IDataActivityLogService dataActivityLogService)
            : base(dependencies, dataActivityLogService)
        {
        }

        public override IQueryable<DataActivity> ApplyContextFiltering(IQueryable<DataActivity> records, object context)
        {
            if (context is ParentIdContext parentIdContext)
            {
                return records.Where(c => c.DataOwner.RecommendingPersonId == parentIdContext.ParentId);
            }

            return base.ApplyContextFiltering(records, context);
        }

        public DataActivityDto GetDefaultNewRecord(long recommendingPersonId)
        {
            var defaultRecord = base.GetDefaultNewRecord();

            defaultRecord.ReferenceId = recommendingPersonId;
            defaultRecord.ReferenceType = ReferenceType.RecommendingPerson;

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var candidate = unitOfWork.Repositories.RecommendingPersons.GetById(recommendingPersonId);
                var dataOwner = candidate.DataOwners.SingleOrDefault();

                if (dataOwner != null)
                {
                    defaultRecord.DataOwnerId = dataOwner.Id;
                }
            }

            return defaultRecord;
        }
    }
}