﻿using Smt.Atomic.Business.Common.Abstracts;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.GDPR.DocumentMappings
{
    [Identifier("Documents.DataConsentDocumentMapping")]
    public class DataConsentDocumentMapping : DocumentMapping<DataConsentDocument, DataConsentDocumentContent, IGDPRDbScope>
    {
        public DataConsentDocumentMapping(IUnitOfWorkService<IGDPRDbScope> unitOfWorkService) 
            : base(unitOfWorkService)
        {
            ContentColumnExpression = o => o.Content;
            NameColumnExpression = o => o.Name;
            ContentTypeColumnExpression = o => o.ContentType;
            ContentLengthColumnExpression = o => o.ContentLength;
        }
    }
}
