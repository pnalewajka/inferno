﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Castle.Core.Logging;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.GDPR.Dto;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Recruitment.BusinessLogics;
using Smt.Atomic.Business.Recruitment.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Recruitment.Jobs
{
    [Identifier("Job.GDPR.DataConsentExpiringJob")]
    [JobDefinition(
        Code = "DataConsentExpiringJob",
        Description = "Job notifying about data consent expiration and anonymize candidate if needed",
        ScheduleSettings = "0 30 22 * * *",
        PipelineName = PipelineCodes.Recruitment)]
    public class DataConsentExpiringJob : IJob
    {
        private readonly ILogger _logger;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IUnitOfWorkService<IRecruitmentDbScope> _unitOfWorkService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly ICandidateService _candidateService;
        private readonly IRecommendingPersonService _recommendingPersonService;
        private const string CanididateUrl = "{0}Recruitment/Candidate/Details/{1}";
        private const string RecommendingPersonUrl = "{0}Recruitment/RecommendingPerson/Details/{1}";
        private const string DataOwnerUrl = "{0}GDPR/DataOwner/Details/{1}";
        private const string DataConsentUrl = "{0}GDPR/DataConsent/Details/{1}";
        private readonly int _daysRemainingToDataConsentExpiration;
        private readonly DateTime _currentDate;
        private readonly DateTime _anonymizationDateThreshold;
        private readonly string _serverAddress;

        public DataConsentExpiringJob(
            ILogger logger,
            ITimeService timeService,
            ISystemParameterService systemParameterService,
            IUnitOfWorkService<IRecruitmentDbScope> unitOfWorkService,
            IMessageTemplateService messageTemplateService,
            IReliableEmailService reliableEmailService,
            ICandidateService candidateService,
            IRecommendingPersonService recommendingPersonService)
        {
            _logger = logger;
            _systemParameterService = systemParameterService;
            _unitOfWorkService = unitOfWorkService;
            _messageTemplateService = messageTemplateService;
            _reliableEmailService = reliableEmailService;
            _candidateService = candidateService;
            _recommendingPersonService = recommendingPersonService;
            _daysRemainingToDataConsentExpiration = _systemParameterService.GetParameter<int>(ParameterKeys.DaysRemainingToDataConsentExpiration);
            _currentDate = timeService.GetCurrentDate();
            _anonymizationDateThreshold = _currentDate.AddYears(_systemParameterService.GetParameter<int>(ParameterKeys.YearsAfterPermanentAnonymizationPossible) * (-1));
            _serverAddress = _systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            try
            {
                AnonymizePermanentlyCandidate(executionContext.CancellationToken);
                AnonymizePermanentlyRecommendingPerson(executionContext.CancellationToken);
                NotifyAboutCandidateConsentExpiration(executionContext.CancellationToken);
                NotifyAboutRecommendingPersonConsentExpiration(executionContext.CancellationToken);
            }
            catch (Exception exception)
            {
                _logger.Error("Job.GDPR.DataConsentExpiringJob error", exception);

                return JobResult.Fail();
            }

            return JobResult.Success();
        }

        private void AnonymizePermanentlyCandidate(
            CancellationToken cancellationToken)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var candidatesIds = unitOfWork.Repositories.Candidates
                    .Where(c => c.IsDeleted && !c.IsAnonymized && c.DeletedOn < _anonymizationDateThreshold)
                    .Select(c => c.Id);

                foreach (var candidateId in candidatesIds.TakeWhileNotCancelled(cancellationToken))
                {
                    _candidateService.AnonymizeCandidateById(candidateId, true);
                }
            }
        }

        private void AnonymizePermanentlyRecommendingPerson(
            CancellationToken cancellationToken)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recommendingPersonIds = unitOfWork.Repositories.RecommendingPersons
                    .Where(c => c.IsDeleted && !c.IsAnonymized && c.DeletedOn < _anonymizationDateThreshold)
                    .Select(c => c.Id);

                foreach (var recommendingPersonId in recommendingPersonIds.TakeWhileNotCancelled(cancellationToken))
                {
                    _recommendingPersonService.AnonymizeRecommendingPersonById(recommendingPersonId, true);
                }
            }
        }

        private void NotifyAboutCandidateConsentExpiration(
            CancellationToken cancellationToken)
        {
            var expirationDate = _currentDate.AddDays(_daysRemainingToDataConsentExpiration);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var isExpiredCandidateConsent = DataConsentBusinessLogic.IsExpiredCandidateConsent.Parametrize(expirationDate);
                var isNotExpiredCandidateConsent = DataConsentBusinessLogic.IsNotExpiredCandidateConsent.Parametrize(_currentDate);

                var candidates = unitOfWork.Repositories.Candidates
                    .Where(c => !c.IsDeleted)
                    .Select(c => new
                    {
                        DataOwnerIds = c.DataOwners.Select(o => o.Id),
                        CandiddateId = c.Id,
                        CoordinatorFirstName = c.ConsentCoordinator.FirstName,
                        CoordinatorLastName = c.ConsentCoordinator.LastName,
                        CoordinatorEmail = c.ConsentCoordinator.Email,
                        ExpiringDataConsents = c.DataOwners
                            .SelectMany(o =>
                                o.DataConsents.AsQueryable()
                                .Where(isExpiredCandidateConsent)
                                .Select(d => new { d.Id, ExpiresOn = d.ExpiresOn.Value, d.Type })),
                        ValidDataConsentTypes = c.DataOwners
                            .SelectMany(o =>
                                o.DataConsents.AsQueryable()
                                .Where(isNotExpiredCandidateConsent)
                                .Select(d => d.Type)),
                        HasValidConsent = c.DataOwners
                            .Any(o => o.DataConsents.AsQueryable().Any(isNotExpiredCandidateConsent))
                    })
                    .Where(r => r.ExpiringDataConsents.Count() > 0 || r.DataOwnerIds.Count() > 1);

                foreach (var candidate in candidates.TakeWhileNotCancelled(cancellationToken))
                {
                    var candidateUrl = string.Format(CanididateUrl, _serverAddress, candidate.CandiddateId);
                    var emailRecipientDto = PrepareEmailRecipientDto(candidate.CoordinatorEmail, $"{candidate.CoordinatorFirstName} {candidate.CoordinatorLastName}");

                    if (candidate.DataOwnerIds.Count() > 1)
                    {
                        SendCandidateHasManyOwnersReminder(
                            candidateUrl,
                            candidate.DataOwnerIds.Select(o => string.Format(DataOwnerUrl, _serverAddress, o)).ToArray(),
                            emailRecipientDto);

                        continue;
                    }

                    var isAnonymized = false;

                    if (!candidate.HasValidConsent)
                    {
                        _candidateService.AnonymizeCandidateById(candidate.CandiddateId);
                        isAnonymized = true;
                    }

                    var validConsentTypes = candidate.ValidDataConsentTypes.ToHashSet();

                    foreach (var dataConsent in candidate.ExpiringDataConsents)
                    {
                        if (CanBeOverridenByAny(dataConsent.Type, validConsentTypes))
                        {
                            continue;
                        }

                        if (dataConsent.ExpiresOn < _currentDate)
                        {
                            SendExpiredCandidateConsentReminder(
                                candidateUrl,
                                candidate.CoordinatorFirstName,
                                dataConsent.Id,
                                dataConsent.ExpiresOn,
                                isAnonymized,
                                emailRecipientDto);
                        }
                        else
                        {
                            SendExpiringCandidateConsentReminder(
                                candidateUrl,
                                candidate.CoordinatorFirstName,
                                dataConsent.Id,
                                dataConsent.ExpiresOn,
                                emailRecipientDto);
                        }
                    }
                }
            }
        }

        private bool CanBeOverridenByAny(DataConsentType consentType, HashSet<DataConsentType> consentTypes)
        {
            return consentTypes.Any(overridingType => DataConsentBusinessLogic.CanOverrideConsent(overridingType, consentType));
        }

        private void NotifyAboutRecommendingPersonConsentExpiration(
            CancellationToken cancellationToken)
        {
            var expirationDate = _currentDate.AddDays(_daysRemainingToDataConsentExpiration);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var isExpiredRecommendingPersonConsent = DataConsentBusinessLogic.IsExpiredRecommendingPersonConsent.Parametrize(expirationDate);
                var isNotExpiredRecommendingPersonConsent = DataConsentBusinessLogic.IsNotExpiredRecommendingPersonConsent.Parametrize(_currentDate);

                var recommendingPersons = unitOfWork.Repositories.RecommendingPersons
                    .Where(c => !c.IsDeleted)
                    .Select(c => new
                    {
                        DataOwnerIds = c.DataOwners.Select(o => o.Id),
                        RecommendingPersonId = c.Id,
                        CoordinatorFirstName = c.Coordinator.FirstName,
                        CoordinatorLastName = c.Coordinator.LastName,
                        CoordinatorEmail = c.Coordinator.Email,
                        DataConsents = c.DataOwners
                            .SelectMany(o => o.DataConsents.AsQueryable()
                                .Where(isExpiredRecommendingPersonConsent)
                                .Select(d => new { d.Id, ExpiresOn = d.ExpiresOn.Value })),
                        HasValidConsent = c.DataOwners
                            .Any(o => o.DataConsents.AsQueryable().Any(isNotExpiredRecommendingPersonConsent))
                    })
                    .Where(r => r.DataConsents.Count() > 0 || r.DataOwnerIds.Count() > 1);

                foreach (var recommendingPerson in recommendingPersons.TakeWhileNotCancelled(cancellationToken))
                {
                    var recommendingPersonUrl = string.Format(RecommendingPersonUrl, _serverAddress, recommendingPerson.RecommendingPersonId);
                    var emailRecipientDto = PrepareEmailRecipientDto(recommendingPerson.CoordinatorEmail, $"{recommendingPerson.CoordinatorFirstName} {recommendingPerson.CoordinatorLastName}");

                    if (recommendingPerson.DataOwnerIds.Count() > 1)
                    {
                        SendRecommendingPersonHasManyOwnersReminder(
                            recommendingPersonUrl,
                            recommendingPerson.DataOwnerIds.Select(o => string.Format(DataOwnerUrl, _serverAddress, o)).ToArray(),
                            emailRecipientDto);

                        continue;
                    }

                    var isAnonymized = false;

                    if (!recommendingPerson.HasValidConsent)
                    {
                        isAnonymized = true;
                        _recommendingPersonService.AnonymizeRecommendingPersonById(recommendingPerson.RecommendingPersonId);
                    }

                    foreach (var dataConsent in recommendingPerson.DataConsents)
                    {
                        if (dataConsent.ExpiresOn < _currentDate)
                        {
                            SendExpiredRecommendingPersonConsentReminder(
                                recommendingPersonUrl,
                                recommendingPerson.CoordinatorFirstName,
                                dataConsent.Id,
                                dataConsent.ExpiresOn,
                                isAnonymized,
                                emailRecipientDto);
                        }
                        else
                        {
                            SendExpiringRecommendingPersonConsentReminder(
                                recommendingPersonUrl,
                                recommendingPerson.CoordinatorFirstName,
                                dataConsent.Id,
                                dataConsent.ExpiresOn,
                                emailRecipientDto);
                        }
                    }
                }
            }
        }

        private EmailRecipientDto PrepareEmailRecipientDto(string emailAdress, string fullName)
        {
            return new EmailRecipientDto
            {
                EmailAddress = emailAdress,
                FullName = fullName
            };
        }

        private void SendExpiringCandidateConsentReminder(CandidateDataConsentExpiringReminderDto dataConsentExpiringReminderDto, EmailRecipientDto emailRecipientDto)
        {
            var emailSubject = _messageTemplateService.ResolveTemplate(TemplateCodes.CandidateDataConsentExpirationReminderSubject, dataConsentExpiringReminderDto);
            var emailBody = _messageTemplateService.ResolveTemplate(TemplateCodes.CandidateDataConsentExpirationReminderBody, dataConsentExpiringReminderDto);

            EnqueueMessage(emailSubject, emailBody, emailRecipientDto);
        }

        private void SendExpiringCandidateConsentReminder(
            string candidateUrl,
            string displayName,
            long dataConsentId,
            DateTime expiresOn,
            EmailRecipientDto emailRecipientDto)
        {
            var candidateDataConsentExpiringReminderDto = new CandidateDataConsentExpiringReminderDto
            {
                CandidateUrl = candidateUrl,
                DisplayName = displayName,
                DataConsentUrl = string.Format(DataConsentUrl, _serverAddress, dataConsentId),
                ExpiresOn = expiresOn
            };

            SendExpiringCandidateConsentReminder(candidateDataConsentExpiringReminderDto, emailRecipientDto);
        }

        private void SendExpiredCandidateConsentReminder(
            string candidateUrl,
            string displayName,
            long dataConsentId,
            DateTime expiresOn,
            bool isAnonymized,
            EmailRecipientDto emailRecipientDto)
        {
            var candidateDataConsentExpiredReminderDto = new CandidateDataConsentExpiredReminderDto
            {
                CandidateUrl = candidateUrl,
                DisplayName = displayName,
                DataConsentUrl = string.Format(DataConsentUrl, _serverAddress, dataConsentId),
                ExpiresOn = expiresOn,
                IsAnonymized = isAnonymized
            };

            SendExpiredCandidateConsentReminder(candidateDataConsentExpiredReminderDto, emailRecipientDto);
        }

        private void SendExpiredCandidateConsentReminder(CandidateDataConsentExpiredReminderDto dataConsentExpiredReminderDto, EmailRecipientDto emailRecipientDto)
        {
            var emailSubject = _messageTemplateService.ResolveTemplate(TemplateCodes.CandidateDataConsentExpiredReminderSubject, dataConsentExpiredReminderDto);
            var emailBody = _messageTemplateService.ResolveTemplate(TemplateCodes.CandidateDataConsentExpiredReminderBody, dataConsentExpiredReminderDto);

            EnqueueMessage(emailSubject, emailBody, emailRecipientDto);
        }

        private void SendCandidateHasManyOwnersReminder(string candidateUrl, string[] dataOwnerUrls, EmailRecipientDto emailRecipientDto)
        {
            var candidateHasManyDataOwnersReminderDto = new CandidateHasManyDataOwnersReminderDto
            {
                CandidateUrl = candidateUrl,
                DataOwnerUrls = dataOwnerUrls
            };

            SendCandidateHasManyOwnersReminder(candidateHasManyDataOwnersReminderDto, emailRecipientDto);
        }

        private void SendCandidateHasManyOwnersReminder(CandidateHasManyDataOwnersReminderDto candidateHasManyDataOwnersReminderDto, EmailRecipientDto emailRecipientDto)
        {
            var emailSubject = _messageTemplateService.ResolveTemplate(TemplateCodes.CandidateWithManyDataOwnersReminderSubject, candidateHasManyDataOwnersReminderDto);
            var emailBody = _messageTemplateService.ResolveTemplate(TemplateCodes.CandidateWithManyDataOwnersReminderBody, candidateHasManyDataOwnersReminderDto);

            EnqueueMessage(emailSubject, emailBody, emailRecipientDto);
        }

        private void SendRecommendingPersonHasManyOwnersReminder(string recommendingPersonUrl, string[] dataOwnerUrls, EmailRecipientDto emailRecipientDto)
        {
            var recommendingPersonHasManyDataOwnersReminderDto = new RecommendingPersonHasManyDataOwnersReminderDto
            {
                RecommendingPersonUrl = recommendingPersonUrl,
                DataOwnerUrls = dataOwnerUrls
            };

            SendRecommendingPersonHasManyOwnersReminder(recommendingPersonHasManyDataOwnersReminderDto, emailRecipientDto);
        }

        private void SendRecommendingPersonHasManyOwnersReminder(RecommendingPersonHasManyDataOwnersReminderDto recommendingPersonHasManyDataOwnersReminderDto, EmailRecipientDto emailRecipientDto)
        {
            var emailSubject = _messageTemplateService.ResolveTemplate(TemplateCodes.CandidateWithManyDataOwnersReminderSubject, recommendingPersonHasManyDataOwnersReminderDto);
            var emailBody = _messageTemplateService.ResolveTemplate(TemplateCodes.CandidateWithManyDataOwnersReminderBody, recommendingPersonHasManyDataOwnersReminderDto);

            EnqueueMessage(emailSubject, emailBody, emailRecipientDto);
        }

        private void SendExpiringRecommendingPersonConsentReminder(RecommendingPersonDataConsentExpiringReminderDto dataConsentExpiringReminderDto, EmailRecipientDto emailRecipientDto)
        {
            var emailSubject = _messageTemplateService.ResolveTemplate(TemplateCodes.RecommendingPersonDataConsentExpirationReminderSubject, dataConsentExpiringReminderDto);
            var emailBody = _messageTemplateService.ResolveTemplate(TemplateCodes.RecommendingPersonDataConsentExpirationReminderBody, dataConsentExpiringReminderDto);

            EnqueueMessage(emailSubject, emailBody, emailRecipientDto);
        }

        private void SendExpiredRecommendingPersonConsentReminder(string recommendingPersonUrl,
            string displayName,
            long dataConsentId,
            DateTime expiresOn,
            bool isAnonymized,
            EmailRecipientDto emailRecipientDto)
        {
            var recommendingPersonDataConsentExpiredReminderDto = new RecommendingPersonDataConsentExpiredReminderDto
            {
                RecommendingPersonUrl = recommendingPersonUrl,
                DisplayName = displayName,
                DataConsentUrl = string.Format(DataConsentUrl, _serverAddress, dataConsentId),
                ExpiresOn = expiresOn,
                IsAnonymized = isAnonymized
            };

            SendExpiredRecommendingPersonConsentReminder(recommendingPersonDataConsentExpiredReminderDto, emailRecipientDto);
        }

        private void SendExpiringRecommendingPersonConsentReminder(string recommendingPersonUrl,
            string displayName,
            long dataConsentId,
            DateTime expiresOn,
            EmailRecipientDto emailRecipientDto)
        {
            var recommendingPersonDataConsentExpiringReminderDto = new RecommendingPersonDataConsentExpiringReminderDto
            {
                RecommendingPersonUrl = recommendingPersonUrl,
                DisplayName = displayName,
                DataConsentUrl = string.Format(DataConsentUrl, _serverAddress, dataConsentId),
                ExpiresOn = expiresOn
            };

            SendExpiringRecommendingPersonConsentReminder(recommendingPersonDataConsentExpiringReminderDto, emailRecipientDto);
        }

        private void SendExpiredRecommendingPersonConsentReminder(RecommendingPersonDataConsentExpiredReminderDto dataConsentExpiredReminderDto, EmailRecipientDto emailRecipientDto)
        {
            var emailSubject = _messageTemplateService.ResolveTemplate(TemplateCodes.RecommendingPersonDataConsentExpiredReminderSubject, dataConsentExpiredReminderDto);
            var emailBody = _messageTemplateService.ResolveTemplate(TemplateCodes.RecommendingPersonDataConsentExpiredReminderBody, dataConsentExpiredReminderDto);

            EnqueueMessage(emailSubject, emailBody, emailRecipientDto);
        }

        private void EnqueueMessage(TemplateProcessingResult emailSubject, TemplateProcessingResult emailBody, EmailRecipientDto emailRecipientDto)
        {
            var emailMessageDto = new EmailMessageDto
            {
                IsHtml = emailBody.IsHtml,
                Subject = emailSubject.Content,
                Body = emailBody.Content,
                Recipients = new List<EmailRecipientDto> { emailRecipientDto }
            };

            _reliableEmailService.EnqueueMessage(emailMessageDto);
        }
    }
}
