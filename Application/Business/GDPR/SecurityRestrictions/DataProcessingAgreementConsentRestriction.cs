﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.GDPR.SecurityRestrictions
{
    public class DataProcessingAgreementConsentRestriction
        : EntitySecurityRestriction<DataProcessingAgreementConsent>
    {
        public DataProcessingAgreementConsentRestriction(IPrincipalProvider principalProvider)
            : base(principalProvider)
        {
        }

        public override Expression<Func<DataProcessingAgreementConsent, bool>> GetRestriction()
        {
            if (!Roles.Contains(SecurityRoleType.CanViewDataProcessingAgreementConsent))
            {
                return NoRecords();
            }

            if (Roles.Contains(SecurityRoleType.CanViewAllDataProcessingAgreementConsent))
            {
                return AllRecords();
            }

            return c => c.UserId == CurrentPrincipal.Id.Value;
        }
    }
}
