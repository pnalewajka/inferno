﻿using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.GDPR.SecurityRestrictions
{
    public class DataProcessingAgreementDocumentRestriction
        : RelatedEntitySecurityRestriction<DataProcessingAgreementDocument, DataProcessingAgreement>
    {
        public DataProcessingAgreementDocumentRestriction(IPrincipalProvider principalProvider)
            : base(principalProvider)
        {
        }

        protected override BusinessLogic<DataProcessingAgreementDocument, DataProcessingAgreement> RelatedEntitySelector =>
            new BusinessLogic<DataProcessingAgreementDocument, DataProcessingAgreement>(d => d.Agreement);
    }
}
