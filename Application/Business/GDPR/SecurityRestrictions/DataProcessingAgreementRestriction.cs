﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.GDPR.SecurityRestrictions
{
    public class DataProcessingAgreementRestriction
        : EntitySecurityRestriction<DataProcessingAgreement>
    {
        public DataProcessingAgreementRestriction(IPrincipalProvider principalProvider)
            : base(principalProvider)
        {
        }

        public override Expression<Func<DataProcessingAgreement, bool>> GetRestriction()
        {
            if (!Roles.Contains(SecurityRoleType.CanViewDataProcessingAgreement))
            {
                return NoRecords();
            }

            if (Roles.Contains(SecurityRoleType.CanViewAllDataProcessingAgreement))
            {
                return AllRecords();
            }

            return c => c.RequiredUsers
                .Any(u => u.Id == CurrentPrincipal.Id.Value);
        }
    }
}
