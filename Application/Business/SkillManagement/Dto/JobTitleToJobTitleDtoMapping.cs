﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;

namespace Smt.Atomic.Business.SkillManagement.Dto
{
    public class JobTitleToJobTitleDtoMapping : ClassMapping<JobTitle, JobTitleDto>
    {
        public JobTitleToJobTitleDtoMapping()
        {
            Mapping = e => new JobTitleDto
            {
                Id = e.Id,
                Name = new LocalizedString
                {
                    English = e.NameEn,
                    Polish = e.NamePl
                },
                JobMatrixLevelId = e.JobMatrixLevelId,
                DefaultProfileIds = e.DefaultProfiles != null ? e.DefaultProfiles.Select(p => p.Id).ToArray() : null,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                IsActive = e.IsActive,
                Timestamp = e.Timestamp
            };
        }
    }
}
