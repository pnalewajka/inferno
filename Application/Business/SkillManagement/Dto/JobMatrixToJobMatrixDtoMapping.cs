﻿using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.SkillManagement.Dto
{
    public class JobMatrixToJobMatrixDtoMapping : ClassMapping<JobMatrix, JobMatrixDto>
    {
        public JobMatrixToJobMatrixDtoMapping()
        {
            Mapping = e => new JobMatrixDto
            {
                Name = e.Name,
                Code = e.Code,
                Description = e.Description,
                Id = e.Id,
                Timestamp = e.Timestamp
            };
        }
    }
}
