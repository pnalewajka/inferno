﻿using System;
namespace Smt.Atomic.Business.SkillManagement.Dto
{
    public class SkillDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public long[] SkillTagIds { get; set; }
    }
}
