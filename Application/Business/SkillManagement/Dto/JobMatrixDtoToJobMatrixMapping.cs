﻿using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.SkillManagement.Dto
{
    public class JobMatrixDtoToJobMatrixMapping : ClassMapping<JobMatrixDto, JobMatrix>
    {
        public JobMatrixDtoToJobMatrixMapping()
        {
            Mapping = d => new JobMatrix
            {
                Name = d.Name,
                Code = d.Code,
                Description = d.Description,
                Id = d.Id,
                Timestamp = d.Timestamp
            };
        }
    }
}
