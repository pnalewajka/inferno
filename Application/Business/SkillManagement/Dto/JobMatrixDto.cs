﻿namespace Smt.Atomic.Business.SkillManagement.Dto
{
    public class JobMatrixDto
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public long Id { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
