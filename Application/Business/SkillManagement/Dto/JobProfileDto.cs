﻿using System;
namespace Smt.Atomic.Business.SkillManagement.Dto
{
    public class JobProfileDto
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public long? CustomOrder { get; set; }

        public long Id { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public string[] SkillTagNames { get; set; }
    }
}
