﻿using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.SkillManagement.Dto
{
    public class JobMatrixLevelToJobMatrixLevelDtoMapping : ClassMapping<JobMatrixLevel, JobMatrixLevelDto>
    {
        public JobMatrixLevelToJobMatrixLevelDtoMapping()
        {
            Mapping = e => new JobMatrixLevelDto
            {
                Code = e.Code,
                Level = e.Level,
                Description = e.Description,
                JobMatrixId = e.JobMatrixId,
                Id = e.Id,
                Timestamp = e.Timestamp
            };
        }
    }
}
