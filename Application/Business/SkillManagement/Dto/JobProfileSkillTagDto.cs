﻿namespace Smt.Atomic.Business.SkillManagement.Dto
{
    public class JobProfileSkillTagDto
    {
        public long Id { get; set; }

        public long JobProfileId { get; set; }

        public long SkillTagId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public long CustomOrder { get; set; }        
    }
}
