﻿using System.Linq;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.SkillManagement.Dto
{
    public class SkillToSkillDtoMapping : ClassMapping<Skill, SkillDto>
    {
        public SkillToSkillDtoMapping()
        {
            Mapping = e => new SkillDto
            {
                Id = e.Id,
                Name = e.Name,
                Description = e.Description,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp,
                SkillTagIds = e.SkillTags.Select(s => s.Id).ToArray()
            };
        }
    }
}
