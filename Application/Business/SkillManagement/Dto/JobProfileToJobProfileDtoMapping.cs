﻿using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using System.Linq;

namespace Smt.Atomic.Business.SkillManagement.Dto
{
    public class JobProfileToJobProfileDtoMapping : ClassMapping<JobProfile, JobProfileDto>
    {
        public JobProfileToJobProfileDtoMapping()
        {
            Mapping = e => new JobProfileDto
            {
                Name = e.Name,
                Description = e.Description,
                CustomOrder = e.CustomOrder,
                Id = e.Id,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp,
                SkillTagNames = e.SkillTags == null ? null : e.SkillTags.OrderBy(v => v.CustomOrder).ThenBy(v=>v.Id).Select(v => v.SkillTag.Name).ToArray()
            };
        }
    }
}
