﻿using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using System.Linq;

namespace Smt.Atomic.Business.SkillManagement.Dto
{
    public class SkillDtoToSkillMapping : ClassMapping<SkillDto, Skill>
    {
        public SkillDtoToSkillMapping()
        {
            Mapping = d => new Skill
            {
                Id = d.Id,
                Name = d.Name,
                Description = d.Description,
                Timestamp = d.Timestamp
            };
        }
    }
}
