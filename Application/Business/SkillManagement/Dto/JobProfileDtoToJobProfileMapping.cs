﻿using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.SkillManagement.Dto
{
    public class JobProfileDtoToJobProfileMapping : ClassMapping<JobProfileDto, JobProfile>
    {
        public JobProfileDtoToJobProfileMapping()
        {
            Mapping = d => new JobProfile
            {
                Name = d.Name,
                Description = d.Description,
                CustomOrder = d.CustomOrder,
                Id = d.Id,
                Timestamp = d.Timestamp
            };
        }
    }
}