﻿using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.SkillManagement.Dto
{
    public class JobProfileSkillTagDtoToJobProfileSkillTagMapping : ClassMapping<JobProfileSkillTagDto, JobProfileSkillTag>
    {
        public JobProfileSkillTagDtoToJobProfileSkillTagMapping()
        {
            Mapping = d => new JobProfileSkillTag
            {
                Id = d.Id,
                JobProfileId = d.JobProfileId,
                SkillTagId = d.SkillTagId,
                CustomOrder = d.CustomOrder
            };
        }
    }
}