﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.SkillManagement.Dto
{
    public class SkillTagDto
    {
        public long Id { get; set; }

        public LocalizedString Name { get; set; }

        public string Description { get; set; }

        public byte[] Timestamp { get; set; }

        public SkillTagType TagType { get; set; }
    }
}
