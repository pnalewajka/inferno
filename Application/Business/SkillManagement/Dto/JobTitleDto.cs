﻿using System;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.SkillManagement.Dto
{
    public class JobTitleDto
    {
        public long Id { get; set; }

        public LocalizedString Name { get; set; }

        public long? JobMatrixLevelId { get; set; }

        public long[] DefaultProfileIds { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public bool IsActive { get; set; }
    }
}
