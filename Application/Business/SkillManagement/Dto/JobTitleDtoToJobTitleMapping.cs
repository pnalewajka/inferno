﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;

namespace Smt.Atomic.Business.SkillManagement.Dto
{
    public class JobTitleDtoToJobTitleMapping : ClassMapping<JobTitleDto, JobTitle>
    {
        public JobTitleDtoToJobTitleMapping()
        {
            Mapping = d => new JobTitle
            {
                Id = d.Id,
                NameEn = d.Name.English,
                NamePl = d.Name.Polish,
                JobMatrixLevelId = d.JobMatrixLevelId,
                DefaultProfiles = d.DefaultProfileIds != null ? d.DefaultProfileIds.Select(id => new SecurityProfile { Id = id }).ToList() : null,
                IsActive = d.IsActive,
                Timestamp = d.Timestamp
            };
        }
    }
}
