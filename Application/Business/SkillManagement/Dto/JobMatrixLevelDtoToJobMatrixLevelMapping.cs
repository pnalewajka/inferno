﻿using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.SkillManagement.Dto
{
    public class JobMatrixLevelDtoToJobMatrixLevelMapping : ClassMapping<JobMatrixLevelDto, JobMatrixLevel>
    {
        public JobMatrixLevelDtoToJobMatrixLevelMapping()
        {
            Mapping = d => new JobMatrixLevel
            {
                Code = d.Code,
                Level = d.Level,
                Description = d.Description,
                JobMatrixId = d.JobMatrixId,
                Id = d.Id,
                Timestamp = d.Timestamp
            };
        }
    }
}
