﻿namespace Smt.Atomic.Business.SkillManagement.Dto
{
    public class JobMatrixLevelDto
    {
        public string Code { get; set; }

        public int Level { get; set; }

        public string Description { get; set; }

        public long? JobMatrixId { get; set; }

        public long Id { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
