﻿using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.SkillManagement.Dto
{
    public class SkillTagDtoToSkillTagMapping : ClassMapping<SkillTagDto, SkillTag>
    {
        public SkillTagDtoToSkillTagMapping()
        {
            Mapping = d => new SkillTag
            {
                Id = d.Id,
                NameEn = d.Name.English,
                NamePl = d.Name.Polish,
                Description = d.Description,
                Timestamp = d.Timestamp,
                TagType = d.TagType
            };
        }
    }
}
