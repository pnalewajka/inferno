﻿using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.SkillManagement.Dto
{
    public class JobProfileSkillTagToJobProfileSkillTagDtoMapping : ClassMapping<JobProfileSkillTag, JobProfileSkillTagDto>
    {
        public JobProfileSkillTagToJobProfileSkillTagDtoMapping()
        {
            Mapping = e => new JobProfileSkillTagDto
            {
                Id = e.Id,
                JobProfileId = e.JobProfileId,
                SkillTagId = e.SkillTagId,
                CustomOrder = e.CustomOrder,
                Description = e.SkillTag.Description,  
                Name = e.SkillTag.Name         
            };
        }
    }
}