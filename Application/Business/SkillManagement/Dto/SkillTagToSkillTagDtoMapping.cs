﻿using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.SkillManagement.Dto
{
    public class SkillTagToSkillTagDtoMapping : ClassMapping<SkillTag, SkillTagDto>
    {
        public SkillTagToSkillTagDtoMapping()
        {
            Mapping = e => new SkillTagDto
            {
                Id = e.Id,
                Name = new LocalizedString
                {
                    English = e.NameEn,
                    Polish = e.NamePl
                },
                Description = e.Description,
                Timestamp = e.Timestamp,
                TagType = e.TagType
            };
        }
    }
}
