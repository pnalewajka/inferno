﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.Business.SkillManagement.Resources;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.SkillManagement.Services
{
    public class JobMatrixLevelCardIndexDataService : CardIndexDataService<JobMatrixLevelDto, JobMatrixLevel, ISkillManagementDbScope>, IJobMatrixLevelCardIndexDataService
    {
        private readonly IReadOnlyUnitOfWorkService<ISkillManagementDbScope> _unitOfWorkSkillManagementScope;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public JobMatrixLevelCardIndexDataService(ICardIndexServiceDependencies<ISkillManagementDbScope> dependencies,
            IReadOnlyUnitOfWorkService<ISkillManagementDbScope> unitOfWorkSkillManagementScope)
            : base(dependencies)
        {
            _unitOfWorkSkillManagementScope = unitOfWorkSkillManagementScope;
        }
        protected override IEnumerable<Expression<Func<JobMatrixLevel, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return a => a.Code;
        }

        public override IQueryable<JobMatrixLevel> ApplyContextFiltering(IQueryable<JobMatrixLevel> records, object context)
        {
            if (context == null) return records;
            var parentIdContext = (ParentIdContext)context;
            return records.Where(r => r.JobMatrixId == parentIdContext.ParentId);
        }

        public override void SetContext(JobMatrixLevel entity, object context)
        {
            if (context == null) return;
            var parentIdContext = (ParentIdContext)context;
            Debug.Assert(parentIdContext != null, "parentIdContext != null");
            entity.JobMatrixId = parentIdContext.ParentId;
        }

        protected override void OnRecordAdding(
            RecordAddingEventArgs<JobMatrixLevel, JobMatrixLevelDto, ISkillManagementDbScope> recordAddingEventArgs)
        {
            using (var unitOfWorkSkillManagementScope = _unitOfWorkSkillManagementScope.Create())
            {
                var jobMatrixLevelIds = (from x in unitOfWorkSkillManagementScope.Repositories.JobMatrixLevels
                                         where x.JobMatrixId == recordAddingEventArgs.InputDto.JobMatrixId
                                         select x.Id).ToList();

                if (jobMatrixLevelIds.Count() >= 10)
                {
                    recordAddingEventArgs.Alerts.Add(new AlertDto
                    {
                        Message = String.Format(AlertResources.AddingMaxTenJobMatrixLevelsAlert),
                        Type = AlertType.Error

                    });
                }
            }
        }
    }
}
