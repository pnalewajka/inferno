﻿using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.SkillManagement.Services
{
    public class JobTitleService : IJobTitleService
    {
        private readonly IUnitOfWorkService<ISkillManagementDbScope> _unitOfWorkService;
        private readonly IClassMapping<JobTitle, JobTitleDto> _jobTitleToJobTitleDtoClassMapping;

        public JobTitleService(
            IUnitOfWorkService<ISkillManagementDbScope> unitOfWorkService,
            IClassMapping<JobTitle, JobTitleDto> jobTitleToJobTitleDtoClassMapping)
        {
            _unitOfWorkService = unitOfWorkService;
            _jobTitleToJobTitleDtoClassMapping = jobTitleToJobTitleDtoClassMapping;
        }

        [Cached(CacheArea = CacheAreas.JobTitles, ExpirationInSeconds = 600)]
        public JobTitleDto GetJobTitleOrDefaultById(long id)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var jobTitle = unitOfWork.Repositories.JobTitles.GetByIdOrDefault(id);

                return jobTitle != null ? _jobTitleToJobTitleDtoClassMapping.CreateFromSource(jobTitle) : null;
            }
        }
    }
}
