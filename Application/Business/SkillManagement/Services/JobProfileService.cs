﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;

namespace Smt.Atomic.Business.SkillManagement.Services
{
    public class JobProfileService : IJobProfileService
    {
        private readonly IUnitOfWorkService<IAllocationDbScope> _unitOfWorkService;
        private readonly IClassMapping<JobProfile, JobProfileDto> _entityToDtoMapping;

        public JobProfileService(
            IUnitOfWorkService<IAllocationDbScope> unitOfWorkService,
            IClassMapping<JobProfile, JobProfileDto> entityToDtoMapping)
        {
            _unitOfWorkService = unitOfWorkService;
            _entityToDtoMapping = entityToDtoMapping;
        }

        [Cached(CacheArea = CacheAreas.JobProfiles, ExpirationInSeconds = 600)]
        public IList<JobProfileDto> GetAllJobProfiles()
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.JobProfiles
                    .AsNoTracking()
                    .AsEnumerable()
                    .Select(_entityToDtoMapping.CreateFromSource)
                    .ToList();
            }
        }

        [Cached(CacheArea = CacheAreas.JobProfiles, ExpirationInSeconds = 600)]
        public IList<JobProfileDto> GetJobProfilesByIds(long[] ids)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.JobProfiles
                    .GetByIds(ids)
                    .AsNoTracking()
                    .AsEnumerable()
                    .Select(_entityToDtoMapping.CreateFromSource)
                    .ToList();
            }
        }
    }
}
