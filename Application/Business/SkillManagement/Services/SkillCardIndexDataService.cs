﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.SkillManagement.BusinessEvents;
using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.Data.Entities.Dto;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.SkillManagement.Services
{
    public class SkillCardIndexDataService : CardIndexDataService<SkillDto, Skill, ISkillManagementDbScope>, ISkillCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public SkillCardIndexDataService(ICardIndexServiceDependencies<ISkillManagementDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override NamedFilters<Skill> NamedFilters
        {
            get
            {
                return new NamedFilters<Skill>(new[]
                {
                    new NamedFilter<Skill, long[]>(FilterCodes.SkillTagFilter,
                        (e, skillTagIds) =>
                            (!skillTagIds.Any() || e.SkillTags.Any(s => skillTagIds.Contains(s.Id)))
                        ),
                });
            }
        }

        protected override IEnumerable<Expression<Func<Skill, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return a => a.Name;
            yield return a => a.Description;
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<Skill, SkillDto, ISkillManagementDbScope> eventArgs)
        {
            eventArgs.InputEntity.SkillTags = eventArgs.UnitOfWork.Repositories.SkillTags.GetByIds(eventArgs.InputDto.SkillTagIds).ToArray();
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<Skill, SkillDto, ISkillManagementDbScope> eventArgs)
        {
            eventArgs.InputEntity.SkillTags = eventArgs.UnitOfWork.Repositories.SkillTags.GetByIds(eventArgs.InputDto.SkillTagIds).ToArray();
            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.SkillTags, eventArgs.InputEntity.SkillTags);
        }

        protected override IEnumerable<BusinessEvent> GetEditRecordEvents(EditRecordResult result, SkillDto newRecord, SkillDto originalRecord)
        {
            var events = base.GetEditRecordEvents(result, newRecord, originalRecord);

            if (result.IsSuccessful)
            {
                var removedTechnologyTagIds = originalRecord.SkillTagIds.Except(newRecord.SkillTagIds);
                var addedTechnologyTagIds = newRecord.SkillTagIds.Except(originalRecord.SkillTagIds);
                var changedTechnologyTagIds = removedTechnologyTagIds.Concat(addedTechnologyTagIds);

                if (changedTechnologyTagIds.Any())
                {
                    events = events.Concat(new[]
                    {
                        new TechnologyTagsChangedForTechnologyBusinessEvent
                        {
                            TechnologyId = newRecord.Id,
                            TechnologyTagIds = changedTechnologyTagIds.ToArray()
                        }
                    });
                }
            }

            return events;
        }

        protected override IEnumerable<BusinessEvent> GetDeleteRecordEvents(DeleteRecordsResult result, SkillDto record)
        {
            var events = base.GetDeleteRecordEvents(result, record);

            if (result.IsSuccessful && record.SkillTagIds.Any())
            {
                events = events.Concat(new[]
                {
                    new TechnologyWithTechnologyTagsRemovedBusinessEvent
                    {
                        TechnologyId = record.Id,
                        TechnologyTagIds = record.SkillTagIds
                    }
                });
            }

            return events;
        }
    }
}
