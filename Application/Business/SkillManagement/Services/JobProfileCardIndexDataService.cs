﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.Business.SkillManagement.Resources;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.SkillManagement.Services
{
    public class JobProfileCardIndexDataService : CardIndexDataService<JobProfileDto, JobProfile, ISkillManagementDbScope>, IJobProfileCardIndexDataService
    {
        private readonly IClassMapping<JobProfile, JobProfileDto> _classMappingJobProfiles;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public JobProfileCardIndexDataService(ICardIndexServiceDependencies<ISkillManagementDbScope> dependencies,
            IClassMapping<JobProfile, JobProfileDto> classMappingJobProfiles)
            : base(dependencies)
        {
            _classMappingJobProfiles = classMappingJobProfiles;

            RelatedCacheAreas = new[] { CacheAreas.JobProfiles };
        }

        protected override NamedFilters<JobProfile> NamedFilters
        {
            get
            {
                return new NamedFilters<JobProfile>(new[]
                {
                    new NamedFilter<JobProfile, long[]>(FilterCodes.SkillTagFilter,
                        (e, skillTagIds) =>
                            (!skillTagIds.Any() || e.SkillTags.Any(s => skillTagIds.Contains(s.SkillTagId)))
                        ),
                });
            }
        }

        protected override IOrderedQueryable<JobProfile> GetDefaultOrderBy(IQueryable<JobProfile> records, QueryCriteria criteria)
        {
            return records.OrderBy(jp => jp.CustomOrder ?? long.MaxValue);
        }

        protected override IEnumerable<Expression<Func<JobProfile, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return a => a.Name;
            yield return a => a.Description;
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<JobProfile, ISkillManagementDbScope> eventArgs)
        {
            if (eventArgs.DeletingRecord.UsedInEmployees.Any())
            {
                eventArgs.Canceled = true;
                eventArgs.AddError(JobProfileResources.ErrorUsedInEmployees);

                return;
            }

            if (eventArgs.DeletingRecord.UsedInAddEmployeeRequests.Any())
            {
                eventArgs.Canceled = true;
                eventArgs.AddError(JobProfileResources.ErrorUsedInAddEmployeeRequests);

                return;
            }

            if (eventArgs.DeletingRecord.UsedInEmployeeDataChangeRequests.Any())
            {
                eventArgs.Canceled = true;
                eventArgs.AddError(JobProfileResources.ErrorUsedInEmployeeDataChangeRequests);

                return;
            }

            base.OnRecordDeleting(eventArgs);
        }
    }
}
