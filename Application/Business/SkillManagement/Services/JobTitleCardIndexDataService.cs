﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.Business.SkillManagement.Resources;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.SkillManagement.Services
{
    public class JobTitleCardIndexDataService : CardIndexDataService<JobTitleDto, JobTitle, ISkillManagementDbScope>, IJobTitleCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public JobTitleCardIndexDataService(ICardIndexServiceDependencies<ISkillManagementDbScope> dependencies)
            : base(dependencies)
        {
            RelatedCacheAreas = new[] { CacheAreas.JobTitles };
        }

        protected override AddRecordResult InternalAddRecord(JobTitleDto record, object context)
        {
            ThrowIfNameIsUsed(record);

            return base.InternalAddRecord(record, context);
        }

        protected override EditRecordResult InternalEditRecord(JobTitleDto newRecord, out JobTitleDto originalRecord, object context)
        {
            ThrowIfNameIsUsed(newRecord);

            return base.InternalEditRecord(newRecord, out originalRecord, context);
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<JobTitle, JobTitleDto, ISkillManagementDbScope> eventArgs)
        {
            eventArgs.InputEntity.DefaultProfiles = EntityMergingService.CreateEntitiesFromIds<SecurityProfile, ISkillManagementDbScope>(eventArgs.UnitOfWork, eventArgs.InputEntity.DefaultProfiles.GetIds());

            base.OnRecordAdding(eventArgs);
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<JobTitle, JobTitleDto, ISkillManagementDbScope> eventArgs)
        {
            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.DefaultProfiles, eventArgs.InputEntity.DefaultProfiles);

            base.OnRecordEditing(eventArgs);
        }

        protected override IEnumerable<Expression<Func<JobTitle, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return a => a.NameEn;
            yield return a => a.NamePl;
            yield return a => a.JobMatrixLevel.Code;
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<JobTitle> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var direction = sortingCriterion.GetSortingDirection();

            if (sortingCriterion.GetSortingColumnName() == nameof(JobTitleDto.DefaultProfileIds))
            {
                orderedQueryBuilder.ApplySortingKey(t => t.DefaultProfiles
                    .OrderBy(p => p.Name)
                    .Select(p => p.Name)
                    .FirstOrDefault(), direction);
            }
            else
            {
                base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
            }
        }

        public void ThrowIfNameIsUsed(JobTitleDto newRecord)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                if (unitOfWork.Repositories.JobTitles
                    .Any(p => p.Id != newRecord.Id && (p.NameEn == newRecord.Name.English || p.NamePl == newRecord.Name.Polish)))
                {
                    throw new BusinessException(JobTitleResources.NameUsedError);
                }
            }
        }

        public override IQueryable<JobTitle> ApplyContextFiltering(IQueryable<JobTitle> records, object context)
        {
            var filteredJobTitlePickerContext = context as JobTitleContext;

            if (filteredJobTitlePickerContext != null && filteredJobTitlePickerContext.OnlyActive)
            {
                records = records.Where(t => t.IsActive);
            }

            return base.ApplyContextFiltering(records, context);
        }
    }
}