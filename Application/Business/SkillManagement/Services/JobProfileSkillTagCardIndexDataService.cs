﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.SkillManagement.Services
{
    public class JobProfileSkillTagCardIndexDataService : CardIndexDataService<JobProfileSkillTagDto, JobProfileSkillTag, ISkillManagementDbScope>, IJobProfileSkillTagCardIndexDataService
    {
        private readonly IUnitOfWorkService<ISkillManagementDbScope> _skillManagementUnitOfWork;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public JobProfileSkillTagCardIndexDataService(
            IUnitOfWorkService<ISkillManagementDbScope> skillManagementUnitOfWork,
            ICardIndexServiceDependencies<ISkillManagementDbScope> dependencies)
            : base(dependencies)
        {
            _skillManagementUnitOfWork = skillManagementUnitOfWork;
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<JobProfileSkillTag, ISkillManagementDbScope> eventArgs)
        {
            var jobProfileSkillTag = eventArgs.DeletingRecord;
            RenumerateAfterDelete(eventArgs.UnitOfWork, eventArgs.DeletingRecord);
        }

        protected override IOrderedQueryable<JobProfileSkillTag> GetDefaultOrderBy(IQueryable<JobProfileSkillTag> records, QueryCriteria criteria)
        {
            return records.OrderBy(x => x.CustomOrder);
        }        

        public override IQueryable<JobProfileSkillTag> ApplyContextFiltering(IQueryable<JobProfileSkillTag> records, object context)
        {
            var jobProfileSkillTagItemsContext = context as ParentIdContext;

            if (jobProfileSkillTagItemsContext != null)
            {
                return records.Where(r => r.JobProfileId == jobProfileSkillTagItemsContext.ParentId).OrderBy(x=>x.CustomOrder);
            }

            return records;
        }

        public void AddSkillTags(long parentId, IList<long> skillTagIds)
        {
            if (skillTagIds != null && skillTagIds.Count > 0)
            {
                using (var unitOfWork = _skillManagementUnitOfWork.Create())
                {
                    foreach (var skillTagId in skillTagIds)
                    {
                        if (!unitOfWork.Repositories.JobProfileSkillTags.Any(q => q.JobProfileId == parentId && q.SkillTagId == skillTagId))
                        {
                            unitOfWork.Repositories.JobProfileSkillTags.Add(new JobProfileSkillTag()
                            {
                                JobProfileId = parentId,
                                CustomOrder = GetNextSkillTagOrder(unitOfWork, parentId),
                                SkillTagId = skillTagId
                            });
                        }
                    }

                    unitOfWork.Commit();
                }
            }
        }

        public void MoveSkillTagDownOrder(long jobProfileSkillTagId)
        {
            using (var unitOfWork = _skillManagementUnitOfWork.Create())
            {
                var jobProfileSkillTagToMoveDown = unitOfWork.Repositories.JobProfileSkillTags.GetById(jobProfileSkillTagId);

                if (CanMoveSkillTagDown(unitOfWork, jobProfileSkillTagToMoveDown))
                {
                    var jobProfileSkillTagToMoveUp = unitOfWork.Repositories.JobProfileSkillTags.Single(
                        x =>
                            x.JobProfileId == jobProfileSkillTagToMoveDown.JobProfileId &&
                            x.CustomOrder == jobProfileSkillTagToMoveDown.CustomOrder + 1
                            );

                    jobProfileSkillTagToMoveUp.CustomOrder--;
                    jobProfileSkillTagToMoveDown.CustomOrder++;

                    unitOfWork.Commit();
                }
            }
        }

        public void MoveSkillTagUpOrder(long jobProfileSkillTagId)
        {
            using (var unitOfWork = _skillManagementUnitOfWork.Create())
            {
                var jobProfileSkillTagToMoveUp = unitOfWork.Repositories.JobProfileSkillTags.GetById(jobProfileSkillTagId);

                if (CanMovSkillTagUp(unitOfWork, jobProfileSkillTagToMoveUp))
                {
                    var jobProfileSkillTagToMoveDown = unitOfWork.Repositories.JobProfileSkillTags.Single(
                        x =>
                            x.JobProfileId == jobProfileSkillTagToMoveUp.JobProfileId &&
                            x.CustomOrder == jobProfileSkillTagToMoveUp.CustomOrder - 1
                            );

                    jobProfileSkillTagToMoveUp.CustomOrder--;
                    jobProfileSkillTagToMoveDown.CustomOrder++;

                    unitOfWork.Commit();
                }
            }
        }

        private bool CanMovSkillTagUp(IUnitOfWork<ISkillManagementDbScope> unitOfWork, JobProfileSkillTag jobProfileSkillTag)
        {
            return jobProfileSkillTag.CustomOrder > 1;
        }

        private bool CanMoveSkillTagDown(IUnitOfWork<ISkillManagementDbScope> unitOfWork, JobProfileSkillTag jobProfileSkillTag)
        {
            var maxOrder = unitOfWork.Repositories.JobProfileSkillTags
                .Where(x => x.JobProfileId == jobProfileSkillTag.JobProfileId)
                .Max(x => x.CustomOrder);

            return jobProfileSkillTag.CustomOrder < maxOrder;
        }

        private long GetNextSkillTagOrder(IUnitOfWork<ISkillManagementDbScope> unitOfWork, long jobProfileId)
        {
            var jobProfile = unitOfWork.Repositories.JobProfiles.GetById(jobProfileId);
            return jobProfile.SkillTags.Count > 0 ? jobProfile.SkillTags.Max(s => s.CustomOrder) + 1 : 1;
        }

        private void RenumerateAfterDelete(IUnitOfWork<ISkillManagementDbScope> unitOfWork, JobProfileSkillTag deletedJobProfileSkillTag)
        {
            var jobProfileSkillTags = unitOfWork.Repositories.JobProfileSkillTags.Where(
                x =>
                    x.JobProfileId == deletedJobProfileSkillTag.JobProfileId &&
                    x.CustomOrder > deletedJobProfileSkillTag.CustomOrder
                    );

            foreach (var jobProfileSkillTag in jobProfileSkillTags)
            {
                jobProfileSkillTag.CustomOrder--;
            }
        }       
    }
}
