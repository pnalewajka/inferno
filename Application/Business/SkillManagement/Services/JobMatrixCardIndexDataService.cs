﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.Business.SkillManagement.Resources;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.SkillManagement.Services
{
    public class JobMatrixCardIndexDataService : CardIndexDataService<JobMatrixDto, JobMatrix, ISkillManagementDbScope>, IJobMatrixCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public JobMatrixCardIndexDataService(ICardIndexServiceDependencies<ISkillManagementDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override IEnumerable<Expression<Func<JobMatrix, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return a => a.Code;
            yield return a => a.Name;
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<JobMatrix, ISkillManagementDbScope> eventArgs)
        {
            if (eventArgs.DeletingRecord.Levels.Any())
            {
                eventArgs.Canceled = true;
                eventArgs.AddError(AlertResources.MustDeleteLevelsFirst);

                return;
            }

            base.OnRecordDeleting(eventArgs);
        }
    }
}