﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.SkillManagement.BusinessEvents;
using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Dto;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.SkillManagement.Services
{
    public class SkillTagCardIndexDataService : CardIndexDataService<SkillTagDto, SkillTag, ISkillManagementDbScope>, ISkillTagCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public SkillTagCardIndexDataService(ICardIndexServiceDependencies<ISkillManagementDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override IEnumerable<Expression<Func<SkillTag, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return a => a.NameEn;
            yield return a => a.NamePl;
            yield return a => a.Description;
        }

        protected override IEnumerable<BusinessEvent> GetEditRecordEvents(EditRecordResult result, SkillTagDto newRecord, SkillTagDto originalRecord)
        {
            var events = base.GetEditRecordEvents(result, newRecord, originalRecord);

            var isNewRecordKeyTechnology = newRecord.TagType.HasFlag(SkillTagType.KeyTechnology);
            var isOriginalRecordKeyTechnology = originalRecord.TagType.HasFlag(SkillTagType.KeyTechnology);

            if (result.IsSuccessful && isNewRecordKeyTechnology != isOriginalRecordKeyTechnology)
            {
                events = events.Concat(new[]
                {
                    new TechnologyTagKeyTechnologyAssignmentChangedBusinessEvent
                    {
                        TechnologyTagId = newRecord.Id
                    }
                });
            }

            return events;
        }

        protected override NamedFilters<SkillTag> NamedFilters
        {
            get
            {
                return new NamedFilters<SkillTag>(new[]
                {
                    new NamedFilter<SkillTag>(FilterCodes.KeyTechnologyTagFilter, s =>
                        s.TagType.HasFlag(SkillTagType.KeyTechnology)),

                    new NamedFilter<SkillTag>(FilterCodes.ResumeTechnicalSkillsTagFilter, s =>
                        s.TagType.HasFlag(SkillTagType.ResumeTechnicalSkillsGroup))
            });
            }
        }
    }
}
