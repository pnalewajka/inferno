﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.SkillManagement.Services
{
    public class JobMatrixListItemProvider : IListItemProvider
    {
        private readonly IUnitOfWorkService<ISkillManagementDbScope> _unitOfWorkService;

        public JobMatrixListItemProvider(IUnitOfWorkService<ISkillManagementDbScope> unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        public IEnumerable<ListItem> GetItems()
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.JobMatrix.Select(x => new ListItem
                {
                    Id = x.Id,
                    DisplayName = x.Name,
                }).ToList();
            }
        }
    }
}
