﻿using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using System.Linq;

namespace Smt.Atomic.Business.SkillManagement.Services
{
    public class TechnologyService : ITechnologyService
    {
        private readonly IUnitOfWorkService<ISkillManagementDbScope> _unitOfWorkService;

        public TechnologyService(IUnitOfWorkService<ISkillManagementDbScope> unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        public void UpdateKeyTechnologyInProjects(long keyTechnologyId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var technologyTag = unitOfWork.Repositories.SkillTags.SingleOrDefault(t => t.Id == keyTechnologyId);

                if (technologyTag == null)
                {
                    return;
                }

                if (technologyTag.TagType.HasFlag(SkillTagType.KeyTechnology))
                {
                    var projectsToAddKeyTechnology = unitOfWork.Repositories.Projects 
                        .Where(p => !p.ProjectSetup.KeyTechnologies.Any(kt => kt.Id == technologyTag.Id))
                        .Where(p => p.ProjectSetup.Technologies.Any(t => t.SkillTags.Any(tag => tag.Id == technologyTag.Id)))
                        .ToList();

                    foreach (var project in projectsToAddKeyTechnology)
                    {
                        project.ProjectSetup.KeyTechnologies.Add(technologyTag);
                    }
                }
                else
                {
                    var projectsToRemoveKeyTechnology = technologyTag.ProjectSetups.ToList();

                    foreach (var projectSetup in projectsToRemoveKeyTechnology)
                    {
                        projectSetup.KeyTechnologies.Remove(technologyTag);
                    }
                }

                unitOfWork.Commit();
            }
        }

        public void UpdateKeyTechnologyInProjects(long keyTechnologyId, long technologyId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var technologyTag = unitOfWork.Repositories.SkillTags.SingleOrDefault(t => t.Id == keyTechnologyId);

                if (technologyTag == null)
                {
                    return;
                }

                if (technologyTag.Skills.Any(x => x.Id == technologyId))
                {
                    var projectsToAddKeyTechnology = unitOfWork.Repositories.Projects
                        .Where(p => !p.ProjectSetup.KeyTechnologies.Any(kt => kt.Id == technologyTag.Id))
                        .Where(p => p.ProjectSetup.Technologies.Any(t => t.Id == technologyId))
                        .ToList();

                    foreach (Project project in projectsToAddKeyTechnology)
                    {
                        project.ProjectSetup.KeyTechnologies.Add(technologyTag);
                    }
                }
                else
                {
                    var projectsToRemoveKeyTechnology = technologyTag.ProjectSetups
                        .Where(p => !p.Technologies.Any(t => t.SkillTags.Any(tag => tag.Id == technologyTag.Id)))
                        .ToList();

                    foreach (var projectSetup in projectsToRemoveKeyTechnology)
                    {
                        projectSetup.KeyTechnologies.Remove(technologyTag);
                    }
                }

                unitOfWork.Commit();
            }
        }
    }
}
