﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.SkillManagement.Services
{
    public class SkillService : ISkillService
    {
        private readonly IUnitOfWorkService<ISkillManagementDbScope> _unitOfWorkService;
        private readonly IClassMapping<JobMatrixLevel, JobMatrixLevelDto> _jobMatrixLevelTojobMatrixLevelDtoClassMapping;
        private readonly IClassMapping<JobProfile, JobProfileDto> _jobProfileToJobProfileDtoClassMapping;
        private readonly IClassMapping<JobTitle, JobTitleDto> _jobTitleToJobTitleDtoClassMapping;
        private readonly IClassMapping<Skill, SkillDto> _skillToSkillDtoClassMapping;

        public SkillService(IUnitOfWorkService<ISkillManagementDbScope> unitOfWorkService,
            IClassMapping<JobMatrixLevel, JobMatrixLevelDto> jobMatrixLevelTojobMatrixLevelDtoClassMapping,
            IClassMapping<JobProfile, JobProfileDto> jobProfileToJobProfileDtoClassMapping,
            IClassMapping<JobTitle, JobTitleDto> jobTitleToJobTitleDtoClassMapping,
            IClassMapping<Skill, SkillDto> skillToSkillDtoClassMapping)
        {
            _unitOfWorkService = unitOfWorkService;
            _jobMatrixLevelTojobMatrixLevelDtoClassMapping = jobMatrixLevelTojobMatrixLevelDtoClassMapping;
            _jobProfileToJobProfileDtoClassMapping = jobProfileToJobProfileDtoClassMapping;
            _jobTitleToJobTitleDtoClassMapping = jobTitleToJobTitleDtoClassMapping;
            _skillToSkillDtoClassMapping = skillToSkillDtoClassMapping;
        }

        public IList<JobMatrixLevelDto> GetAllJobMatrixLevels()
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.JobMatrixLevels.AsNoTracking()
                    .AsEnumerable()
                    .Select(_jobMatrixLevelTojobMatrixLevelDtoClassMapping.CreateFromSource)
                    .ToList();
            }
        }

        public IList<JobProfileDto> GetAllJobProfiles()
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.JobProfiles.AsNoTracking()
                    .AsEnumerable()
                    .Select(_jobProfileToJobProfileDtoClassMapping.CreateFromSource)
                    .ToList();
            }
        }

        public IList<JobTitleDto> GetAllJobTitles()
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.JobTitles.AsNoTracking()
                    .AsEnumerable()
                    .Select(_jobTitleToJobTitleDtoClassMapping.CreateFromSource)
                    .ToList();
            }
        }

        public IList<SkillDto> GetSkillsByIds(long[] ids)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Skills.GetByIds(ids).AsNoTracking()
                    .AsEnumerable()
                    .Select(_skillToSkillDtoClassMapping.CreateFromSource)
                    .ToList();
            }
        }
    }
}
