﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.EventSourcing.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Organization.Services;
using Smt.Atomic.Business.SkillManagement.BusinessEventHandlers;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.Business.SkillManagement.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.SkillManagement
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            if (containerType == ContainerType.WebApp)
            {
                container.Register(Component.For<IJobMatrixCardIndexDataService>().ImplementedBy<JobMatrixCardIndexDataService>().LifestyleTransient());
                //    container.Register(Component.For<IJobMatrixImporterCardIndexDataService>().ImplementedBy<JobMatrixImporterCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ITreeStructureService<ISkillManagementDbScope>>().ImplementedBy<TreeStructureService<ISkillManagementDbScope>>().LifestyleTransient());
                container.Register(Component.For<ISkillCardIndexDataService>().ImplementedBy<SkillCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IJobProfileCardIndexDataService>().ImplementedBy<JobProfileCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IJobMatrixLevelCardIndexDataService>().ImplementedBy<JobMatrixLevelCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ISkillTagCardIndexDataService>().ImplementedBy<SkillTagCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IJobTitleCardIndexDataService>().ImplementedBy<JobTitleCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IJobProfileService>().ImplementedBy<JobProfileService>().LifestyleTransient());
                container.Register(Component.For<IJobProfileSkillTagCardIndexDataService>().ImplementedBy<JobProfileSkillTagCardIndexDataService>().LifestyleTransient());
            }

            switch (containerType)
            {
                case ContainerType.JobScheduler:
                case ContainerType.WebApi:
                case ContainerType.WebApp:
                case ContainerType.WebServices:
                case ContainerType.UnitTests:
                    container.Register(Component.For<ISkillService>().ImplementedBy<SkillService>().LifestyleTransient());
                    container.Register(Component.For<IJobTitleService>().ImplementedBy<JobTitleService>().LifestyleTransient());
                    break;
            }

            if (containerType == ContainerType.JobScheduler)
            {
                container.Register(Component.For<ITechnologyService>().ImplementedBy<TechnologyService>().LifestyleTransient());
                container.Register(Component.For<IBusinessEventHandler>().ImplementedBy<TechnologyTagsChangedForTechnologyEventHandler>().LifestyleTransient());
                container.Register(Component.For<IBusinessEventHandler>().ImplementedBy<TechnologyWithTechnologyTagsRemovedEventHandler>().LifestyleTransient());
                container.Register(Component.For<IBusinessEventHandler>().ImplementedBy<TechnologyTagKeyTechnologyAssignmentChangedEventHandler>().LifestyleTransient());
                container.Register(Component.For<IJobTitleCardIndexDataService>().ImplementedBy<JobTitleCardIndexDataService>().LifestyleTransient());
            }
        }
    }
}



