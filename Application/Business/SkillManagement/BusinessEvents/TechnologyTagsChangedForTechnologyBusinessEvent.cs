﻿using Smt.Atomic.Data.Entities.Dto;
using System.Runtime.Serialization;

namespace Smt.Atomic.Business.SkillManagement.BusinessEvents
{
    [DataContract]
    public class TechnologyTagsChangedForTechnologyBusinessEvent : BusinessEvent
    {
        [DataMember]
        public long TechnologyId { get; set; }

        [DataMember]
        public long[] TechnologyTagIds { get; set; }
    }
}
