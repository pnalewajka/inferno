﻿using Smt.Atomic.Data.Entities.Dto;
using System.Runtime.Serialization;

namespace Smt.Atomic.Business.SkillManagement.BusinessEvents
{
    [DataContract]
    public class TechnologyTagKeyTechnologyAssignmentChangedBusinessEvent : BusinessEvent
    {
        [DataMember]
        public long TechnologyTagId { get; set; }
    }
}
