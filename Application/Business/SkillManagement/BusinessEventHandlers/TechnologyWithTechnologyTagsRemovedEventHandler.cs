﻿using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Business.SkillManagement.BusinessEvents;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.SkillManagement.BusinessEventHandlers
{
    public class TechnologyWithTechnologyTagsRemovedEventHandler : BusinessEventHandler<TechnologyWithTechnologyTagsRemovedBusinessEvent>
    {
        private readonly ITechnologyService _technologyService;

        public TechnologyWithTechnologyTagsRemovedEventHandler(ITechnologyService technologyService)
        {
            _technologyService = technologyService;
        }

        public override void Handle(TechnologyWithTechnologyTagsRemovedBusinessEvent businessEvent)
        {
            foreach (var technologyTagId in businessEvent.TechnologyTagIds)
            {
                _technologyService.UpdateKeyTechnologyInProjects(businessEvent.TechnologyId, technologyTagId);
            }
        }
    }
}
