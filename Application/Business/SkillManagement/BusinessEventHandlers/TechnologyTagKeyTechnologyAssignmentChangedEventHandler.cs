﻿using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Business.SkillManagement.BusinessEvents;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.SkillManagement.BusinessEventHandlers
{
    public class TechnologyTagKeyTechnologyAssignmentChangedEventHandler : BusinessEventHandler<TechnologyTagKeyTechnologyAssignmentChangedBusinessEvent>
    {
        private readonly ITechnologyService _technologyService;

        public TechnologyTagKeyTechnologyAssignmentChangedEventHandler(ITechnologyService technologyService)
        {
            _technologyService = technologyService;
        }

        public override void Handle(TechnologyTagKeyTechnologyAssignmentChangedBusinessEvent businessEvent)
        {
            _technologyService.UpdateKeyTechnologyInProjects(businessEvent.TechnologyTagId);
        }
    }
}
