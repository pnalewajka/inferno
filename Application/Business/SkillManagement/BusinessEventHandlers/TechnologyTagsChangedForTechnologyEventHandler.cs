﻿using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Business.SkillManagement.BusinessEvents;
using Smt.Atomic.Business.SkillManagement.Interfaces;

namespace Smt.Atomic.Business.SkillManagement.BusinessEventHandlers
{
    public class TechnologyTagsChangedForTechnologyEventHandler : BusinessEventHandler<TechnologyTagsChangedForTechnologyBusinessEvent>
    {
        private readonly ITechnologyService _technologyService;

        public TechnologyTagsChangedForTechnologyEventHandler(ITechnologyService technologyService)
        {
            _technologyService = technologyService;
        }

        public override void Handle(TechnologyTagsChangedForTechnologyBusinessEvent businessEvent)
        {
            foreach (var technologyTagId in businessEvent.TechnologyTagIds)
            {
                _technologyService.UpdateKeyTechnologyInProjects(businessEvent.TechnologyId, technologyTagId);
            }
        }
    }
}
