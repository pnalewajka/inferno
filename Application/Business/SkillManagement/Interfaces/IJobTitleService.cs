﻿using Smt.Atomic.Business.SkillManagement.Dto;

namespace Smt.Atomic.Business.SkillManagement.Interfaces
{
    public interface IJobTitleService
    {
        JobTitleDto GetJobTitleOrDefaultById(long id);
    }
}
