﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;

namespace Smt.Atomic.Business.SkillManagement.Interfaces
{
    public interface IJobProfileCardIndexDataService : ICardIndexDataService<JobProfileDto>
    {
    }
}

