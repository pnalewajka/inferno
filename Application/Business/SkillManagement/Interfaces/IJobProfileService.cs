﻿using System.Collections.Generic;
using Smt.Atomic.Business.SkillManagement.Dto;

namespace Smt.Atomic.Business.SkillManagement.Interfaces
{
    public interface IJobProfileService
    {
        IList<JobProfileDto> GetAllJobProfiles();

        IList<JobProfileDto> GetJobProfilesByIds(long[] ids);
    }
}
