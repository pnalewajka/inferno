﻿namespace Smt.Atomic.Business.SkillManagement.Interfaces
{
    public interface ITechnologyService
    {
        void UpdateKeyTechnologyInProjects(long keyTechnologyId);
        void UpdateKeyTechnologyInProjects(long keyTechnologyId, long technologyId);
    }
}
