﻿using System.Collections.Generic;
using Smt.Atomic.Business.SkillManagement.Dto;

namespace Smt.Atomic.Business.SkillManagement.Interfaces
{
    public interface ISkillService
    {
        IList<JobMatrixLevelDto> GetAllJobMatrixLevels();

        IList<JobProfileDto> GetAllJobProfiles();

        IList<JobTitleDto> GetAllJobTitles();

        IList<SkillDto> GetSkillsByIds(long[] ids);
    }
}
