﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.SkillManagement.Interfaces
{
    public static class FilterCodes
    {
        public const string SkillTagFilter = "skill-tag";
        public const string KeyTechnologyTagFilter = "key-technology";
        public const string ResumeTechnicalSkillsTagFilter = "resume-technical-skill";
    }
}
