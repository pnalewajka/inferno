﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.SkillManagement.Dto;

namespace Smt.Atomic.Business.SkillManagement.Interfaces
{
    public interface IJobMatrixCardIndexDataService : ICardIndexDataService<JobMatrixDto>
    {
    }
}

