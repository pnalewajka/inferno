﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.SkillManagement.Dto;
using System.Collections.Generic;

namespace Smt.Atomic.Business.SkillManagement.Interfaces
{
    public interface IJobProfileSkillTagCardIndexDataService : ICardIndexDataService<JobProfileSkillTagDto>
    {
        void MoveSkillTagUpOrder(long jobProfileSkillTagId);
        void MoveSkillTagDownOrder(long jobProfileSkillTagId);
        void AddSkillTags(long parentId, IList<long> skillTagIds);
    }
}