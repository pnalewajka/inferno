﻿using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.Common.Interfaces;

namespace Smt.Atomic.Business.BusinessTrips.Interfaces
{
    public interface IBusinessTripSettlementsCardIndexDataService : ICardIndexDataService<BusinessTripSettlementDto>
    {
    }
}
