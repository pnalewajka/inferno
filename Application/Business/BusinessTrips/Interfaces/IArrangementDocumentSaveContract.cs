﻿using System;

namespace Smt.Atomic.Business.BusinessTrips.Interfaces
{
    public interface IArrangementDocumentSaveContract
    {
        long? DocumentId { get; }

        Guid? TemporaryDocumentId { get; }

        string ContentType { get; }

        string DocumentName { get; }
    }
}