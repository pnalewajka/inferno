﻿using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;

namespace Smt.Atomic.Business.BusinessTrips.Interfaces
{
    public interface IBusinessTripAdvancedPaymentCardIndexDataService : ICardIndexDataService<BusinessTripAdvancedPaymentRequestDto>
    {
    }
}