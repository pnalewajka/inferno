﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.BusinessTrips.Dto;

namespace Smt.Atomic.Business.BusinessTrips.Interfaces
{
    public interface IBusinessTripParticipantCardIndexDataService : ICardIndexDataService<BusinessTripParticipantDto>
    {
        BusinessTripParticipantDto GetDefaultNewRecord(long businessTripId);
    }
}
