﻿using System;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.BusinessTrips.Interfaces
{
    public interface IBusinessTripArrangementSaveContract
    {
        long? Id { get; }

        ArrangementType ArrangementType { get; }

        string Name { get; }

        DateTime IssuedOn { get; set; }

        IList<long> ParticipantIds { get; }

        string Details { get; }

        long? CurrencyId { get; }

        decimal? Value { get; }

        IList<IArrangementDocumentSaveContract> ArrangementDocuments { get; }

        byte[] Timestamp { get; }
    }
}