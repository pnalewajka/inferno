﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.BusinessTrips.Services;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;

namespace Smt.Atomic.Business.BusinessTrips.Interfaces
{
    public interface IBusinessTripService
    {
        long CreateBusinessTripFromRequest(BusinessTripRequestDto businessTripRequest, long voucherEmployeeId);

        BusinessTripDto GetBusinessTripOrDefaultById(long id);

        bool IsBusinessTripAccessible(long id);

        long AddBusinessTripMessage(long businessTripId, long authorEmployeeId, string content);

        IEnumerable<BusinessTripMessageDto> GetBusinessTripMessagesOrDefaultById(long id);

        IEnumerable<long> GetBusinessTripParticipantIdsOrDefaultById(long businessTripId, Expression<Func<BusinessTripParticipant, bool>> predicate);

        ArrangementTrackingParticipantDto[] GetArrangementTrackingEntries(long businessTripId);

        string GetArrangementTrackingRemarks(long businessTripId);

        void SaveArrangementTrackingRemarks(long businessTripId, string remarks);

        void SaveArrangementTrackingStatuses(long businessTripId, ArrangementTrackingParticipantDto[] participants);

        IEnumerable<ArrangementTrackingEntryDto> CreateArrangementTrackingEntries(BusinessTripParticipantDto participant);

        IEnumerable<ArrangementTrackingEntryDto> CreateArrangementTrackingEntries(BusinessTripRequestDto request);

        void AssignFrontDeskResponsibility(long[] businessTripIds, long employeeId);

        ICollection<AlertDto> ValidateParticipantEmployeesEmploymentPeriods(DateTime btStartDate, DateTime btEndDate, ICollection<long> employeeIds);

        bool IsParticipantBusinessTripInternational(long participantId);

        bool SendArrangedNotificationEmailIfNeeded(long businessTripId);
    }
}
