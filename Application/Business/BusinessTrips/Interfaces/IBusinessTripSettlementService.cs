﻿using System.Collections.Generic;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.BusinessTrips.Interfaces
{
    public interface IBusinessTripSettlementService
    {
        BusinessTripSettlementRequestDto GetSettlementRequestByIdOrDefault(long settlementRequestId);

        ICollection<BusinessTripSettlementRequestMealDto> GetMeals(BusinessTripSettlementRequestDto dto);

        SettlementSectionsFeatureType GetSettlementConfiguration(long? participantId);

        bool IsRequestAvailableAfterFiltering(long settlementRequestId);

        bool HasAnySettlementRequest(long businessTripId);

        void UpdateControllingStatus(long[] settlementRequestIds, BusinessTripSettlementControllingStatus status);

        void CreateEmptyRequest(long participantId);
    }
}
