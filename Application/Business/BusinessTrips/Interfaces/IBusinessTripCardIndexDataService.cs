﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.BusinessTrips.Dto;

namespace Smt.Atomic.Business.BusinessTrips.Interfaces
{
    public interface IBusinessTripCardIndexDataService : ICardIndexDataService<BusinessTripDto>
    {
    }
}

