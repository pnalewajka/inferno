﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.BusinessTrips.Interfaces
{
    public interface IBusinessTripAcceptanceConditionsService
    {
        BusinessTripAcceptanceConditionsDto GetAcceptanceConditionsForBusinessTrip(long businessTripId);

        BoolResult ChangeAcceptanceConditionsForBusinessTrip(long businessTripId, BusinessTripAcceptanceConditionsDto conditions);
    }
}
