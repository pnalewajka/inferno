﻿using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.Common.Dto;
using System.Collections.Generic;

namespace Smt.Atomic.Business.BusinessTrips.Interfaces
{
    public interface IBusinessTripArrangementsService
    {
        IEnumerable<BusinessTripArrangementDto> GetBusinessTripArrangements(long businessTripId);

        IEnumerable<BusinessTripArrangementDto> SaveBusinessTripArrangements(long businessTripId,
            IReadOnlyCollection<IBusinessTripArrangementSaveContract> arrangements);

        BoolResult RemoveArrangement(long arrangementId);
    }
}