﻿using System.Collections.Generic;
using Smt.Atomic.Business.BusinessTrips.Dto;

namespace Smt.Atomic.Business.BusinessTrips.Interfaces
{
    public interface IBusinessTripParticipantsService
    {
        bool ShouldParticipantSpecifyDestinationType(long participantId);

        string GetSettlementReportCodeByParticipantId(long participantId);

        BusinessTripParticipantDto GetBusinessTripParticipantById(long participantId);

        IEnumerable<BusinessTripParticipantDto> GetBusinessTripParticipants(long businessTripId);

        IEnumerable<long> GetParticipantIdsByEmployeeId(long employeeId);

        long GetEmployeeIdByParticipantId(long participantId);

        long GetUserIdByParticipantId(long participantId);

        IEnumerable<BusinessTripParticipantDto> GetBusinessTripParticipantsWithUnsettledBusinessTripToNotify(long businessTripId);
    }
}
