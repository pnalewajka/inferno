﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security;
using Smt.Atomic.Business.BusinessTrips.BusinessLogics;
using Smt.Atomic.Business.BusinessTrips.Consts;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.BusinessTrips.Filters;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Workflows.BusinessLogics;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.BusinessTrips.Services
{
    public class BusinessTripCardIndexDataService : CardIndexDataService<BusinessTripDto, BusinessTrip, IBusinessTripsDbScope>, IBusinessTripCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public BusinessTripCardIndexDataService(ICardIndexServiceDependencies<IBusinessTripsDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override NamedFilters<BusinessTrip> NamedFilters
        {
            get
            {
                var currentUserId = PrincipalProvider.Current.Id;
                var currentDate = TimeService.GetCurrentDate();

                if (!currentUserId.HasValue)
                {
                    return null;
                }

                return new NamedFilters<BusinessTrip>(
                    new NamedFilter<BusinessTrip>[]
                    {
                        FrontDeskAsigneeFilter.BusinessTripFilter,
                        CompanyFilter.EmployeeFilter,
                        DateRangeFilter.BusinessTripFilter,
                        DestinationPlaceFilter.BusinessTripFilter,
                        DeparturePlaceFilter.BusinessTripFilter,
                        FrontDeskArrangementFilter.BusinessTripArrangedFilter,
                        FrontDeskArrangementFilter.BusinessTripInArrangementFilter,
                        ScopeFilter.AllFilter,
                        ScopeFilter.MyTripsFilter(currentUserId.Value),
                        ScopeFilter.ApprovedByMeFilter(currentUserId.Value, currentDate),
                        ScopeFilter.WatchedByMeFilter(currentUserId.Value),
                    }
                    .Union(CardIndexServiceHelper.BuildEnumBasedFilters<BusinessTrip, BusinessTripStatus>(
                        new BusinessLogic<BusinessTrip, long?>(
                            b => (long)BusinessTripBusinessLogic.GetStatus.Call(b, currentDate))))
                    .Union(CardIndexServiceHelper.BuildEnumBasedFilters<BusinessTrip, BusinessTripSettlementControllingStatus>(
                        new BusinessLogic<BusinessTrip, IEnumerable<long?>>(
                            t => t.Participants.SelectMany(
                                p => p.SettlementRequests.AsQueryable()
                                    .Where(r => RequestBusinessLogic.IsApprovedOrCompleted.Call(r.Request))
                                    .Select(r => r.ControllingStatus))
                                    .Cast<long?>()))));
            }
        }

        protected override IEnumerable<Expression<Func<BusinessTrip, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return t => "BT-" + t.Id;

            if (searchCriteria.Includes(SearchAreaCodes.Apn))
            {
                yield return t => t.Projects.Select(p => p.Apn);
            }

            if (searchCriteria.Includes(SearchAreaCodes.Participants))
            {
                yield return t => t.Participants.Select(p => p.Employee.FirstName);
                yield return t => t.Participants.Select(p => p.Employee.LastName);
            }

            if (searchCriteria.Includes(SearchAreaCodes.Projects))
            {
                yield return t => t.Projects.Select(p => p.ProjectSetup.GenericName);
                yield return t => t.Projects.Select(p => p.ProjectSetup.ClientShortName);
                yield return t => t.Projects.Select(p => p.ProjectSetup.ProjectShortName);
                yield return t => t.Projects.Select(p => p.SubProjectShortName);
            }

            if (searchCriteria.Includes(SearchAreaCodes.DepartureCity))
            {
                yield return t => t.Participants.Select(p => p.DepartureCity.Name);
            }

            if (searchCriteria.Includes(SearchAreaCodes.DestinationCity))
            {
                yield return t => t.DestinationCity.Name;
            }
        }

        public override IQueryable<BusinessTrip> ApplyContextFiltering(IQueryable<BusinessTrip> records, object context)
        {
            var businessTripContext = context as BusinessTripContext;

            if (businessTripContext?.Mode == BusinessTripPickerMode.OnlyFinished)
            {
                var currentDate = TimeService.GetCurrentDate();
                var isFinished = new BusinessLogic<BusinessTrip, bool>
                    (b => BusinessTripBusinessLogic.GetStatus.Call(b, currentDate) == BusinessTripStatus.Finished);

                records = records.Where(isFinished);
            }

            if (businessTripContext?.Mode == BusinessTripPickerMode.NotAccounted)
            {
                var currentDate = TimeService.GetCurrentDate();
                var notAccounted = new BusinessLogic<BusinessTrip, bool>(
                    b => BusinessTripBusinessLogic.GetStatus.Call(b, currentDate) != BusinessTripStatus.Accounted);

                records = records.Where(notAccounted);
            }

            if (businessTripContext == null || businessTripContext.Month == 0 || businessTripContext.Year == 0)
            {
                return base.ApplyContextFiltering(records, context);
            }

            return records.Where(x => x.StartedOn.Month == businessTripContext.Month &&
                                      x.StartedOn.Year == businessTripContext.Year);
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<BusinessTrip> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var direction = sortingCriterion.GetSortingDirection();

            switch (sortingCriterion.GetSortingColumnName())
            {
                case nameof(BusinessTripDto.Identifier):
                    orderedQueryBuilder.ApplySortingKey(b => b.Id, direction);
                    break;

                case nameof(BusinessTripDto.CompanyNames):
                    orderedQueryBuilder.ApplySortingKey(b => b.Participants.Select(p => p.Employee.Company.Name).FirstOrDefault(), direction);
                    break;

                case nameof(BusinessTripDto.DepartureCityNames):
                    orderedQueryBuilder.ApplySortingKey(b => b.Participants.Select(p => p.DepartureCity.Name).FirstOrDefault(), direction);
                    break;

                case nameof(BusinessTripDto.DestinationCityName):
                    orderedQueryBuilder.ApplySortingKey(b => b.DestinationCity.Name, direction);
                    break;

                case nameof(BusinessTripDto.Status):
                    var currentDate = TimeService.GetCurrentDate();
                    orderedQueryBuilder.ApplySortingKey<BusinessTripStatus>(BusinessTripBusinessLogic.GetStatus.Parametrize(currentDate), direction);
                    break;

                case nameof(BusinessTripDto.Settlements):
                    orderedQueryBuilder.ApplySortingKey(b => b.Request != null, direction);
                    break;

                default:
                    base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
                    break;
            }
        }

        public override AddRecordResult AddRecord(BusinessTripDto record, object context = null)
        {
            throw new SecurityException("Use Request to add a BusinessTrip");
        }

        public override DeleteRecordsResult DeleteRecords(IEnumerable<long> idsToDelete, object context = null, bool isDeletePermanent = false)
        {
            throw new SecurityException("Use Request to delete the BusinessTrip");
        }

        protected override NamedFilter<BusinessTrip> OnMissingFilterFallback(NamedFilters<BusinessTrip> namedFilters, string filterKey, string filterCode)
        {
            if (filterCode == Consts.FilterCodes.AllMode || filterCode == Consts.FilterCodes.MonthlyMode)
            {
                return new NamedFilter<BusinessTrip>(string.Empty, c => true);
            }
            
            return base.OnMissingFilterFallback(namedFilters, filterKey, filterCode);
        }
    }
}
