﻿using System.Linq;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.Business.BusinessTrips.Resources;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Business.Extensions;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.BusinessTrips.Services
{
    public class BusinessTripAcceptanceConditionsService : IBusinessTripAcceptanceConditionsService
    {
        private readonly ICardIndexServiceDependencies<IBusinessTripsDbScope> _dependencies;

        public BusinessTripAcceptanceConditionsService(ICardIndexServiceDependencies<IBusinessTripsDbScope> dependencies)
        {
            _dependencies = dependencies;
        }

        public BoolResult ChangeAcceptanceConditionsForBusinessTrip(long businessTripId, BusinessTripAcceptanceConditionsDto conditions)
        {
            using (var unitOfWork = _dependencies.UnitOfWorkService.Create())
            {
                var settlementCondition = unitOfWork.Repositories
                    .BusinessTripAcceptanceConditions
                    .Filtered()
                    .Where(c => c.BusinessTrip.Id == businessTripId)
                    .Single();

                settlementCondition.CopyFields(conditions);

                unitOfWork.Commit();

                return new BoolResult(true, new[] {
                    new AlertDto {
                        Message = BusinessTripResources.AcceptanceConditionChanged,
                        Type = AlertType.Success, IsDismissable = true
                    }
                });
            }
        }

        public BusinessTripAcceptanceConditionsDto GetAcceptanceConditionsForBusinessTrip(long businessTripId)
        {
            var mapping = _dependencies.ClassMappingFactory.CreateMapping<BusinessTripAcceptanceConditions, BusinessTripAcceptanceConditionsDto>();
            using (var unitOfWork = _dependencies.UnitOfWorkService.Create())
            {
                return unitOfWork.Repositories
                    .BusinessTripAcceptanceConditions
                    .Filtered()
                    .Where(c => c.BusinessTrip.Id == businessTripId)
                    .Select(mapping.CreateFromSource)
                    .Single();
            }
        }
    }
}
