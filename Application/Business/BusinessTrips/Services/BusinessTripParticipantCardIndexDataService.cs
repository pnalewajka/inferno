﻿using System;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.Business.BusinessTrips.Resources;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.BusinessTrips.Services
{
    public class BusinessTripParticipantCardIndexDataService
        : BusinessTripTabCardIndexDataService<BusinessTripParticipantDto, BusinessTripParticipant>, IBusinessTripParticipantCardIndexDataService
    {
        private readonly ICityService _cityService;
        private readonly IEmployeeService _employeeService;
        private readonly IBusinessTripService _businessTripService;
        private readonly IClassMapping<ArrangementTrackingEntryDto, ArrangementTrackingEntry> _arrangementTrackingEntryDtoToArrangementTrackingEntryMapping;
        private readonly IClassMapping<BusinessTripParticipant, BusinessTripParticipantDto> _participantToDtoMapping;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        protected override BusinessLogic<BusinessTripParticipant, BusinessTrip> BusinessTripSelector
            => new BusinessLogic<BusinessTripParticipant, BusinessTrip>(p => p.BusinessTrip);

        public BusinessTripParticipantCardIndexDataService(
            ICardIndexServiceDependencies<IBusinessTripsDbScope> dependencies,
            ICityService cityService,
            IEmployeeService employeeService,
            IBusinessTripService businessTripService)
            : base(dependencies)
        {
            _cityService = cityService;
            _employeeService = employeeService;
            _businessTripService = businessTripService;
            _arrangementTrackingEntryDtoToArrangementTrackingEntryMapping = dependencies.ClassMappingFactory.CreateMapping<ArrangementTrackingEntryDto, ArrangementTrackingEntry>();
            _participantToDtoMapping = dependencies.ClassMappingFactory.CreateMapping<BusinessTripParticipant, BusinessTripParticipantDto>();
        }

        protected override BusinessLogic<BusinessTripParticipant, bool> ApplyEntityContextFiltering(object context)
        {
            if (context is ParticipantPickerContext participantPicker)
            {
                if (participantPicker.PickerFilter == ParticipantPickerContext.PickerMode.ManageSettlements
                    && !PrincipalProvider.Current.IsInRole(SecurityRoleType.CanManageOthersBusinessTripSettlements))
                {
                    return new BusinessLogic<BusinessTripParticipant, bool>(a => a.EmployeeId == PrincipalProvider.Current.EmployeeId);
                }

                if (participantPicker.PickerFilter == ParticipantPickerContext.PickerMode.SettlementReport
                    && !PrincipalProvider.Current.IsInRole(SecurityRoleType.CanGenerateAllBusinessTripSettlementReport))
                {
                    return new BusinessLogic<BusinessTripParticipant, bool>(a => a.EmployeeId == PrincipalProvider.Current.EmployeeId);
                }
            }

            return base.ApplyEntityContextFiltering(context);
        }

        public BusinessTripParticipantDto GetDefaultNewRecord(long businessTripId)
        {
            var record = GetDefaultNewRecord();

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var businessTripData = unitOfWork.Repositories.BusinessTrips
                    .Where(bt => bt.Id == businessTripId)
                    .Select(bt => new
                    {
                        bt.StartedOn,
                        bt.EndedOn,
                        bt.Request.TransportationPreferences,
                        bt.Request.AccommodationPreferences
                    })
                    .Single();

                record.StartedOn = businessTripData.StartedOn;
                record.EndedOn = businessTripData.EndedOn;
                record.TransportationPreferences = businessTripData.TransportationPreferences;
                record.AccommodationPreferences = businessTripData.AccommodationPreferences;
                record.LuggageDeclarations = new DeclaredLuggageDto[0];
            }

            return record;
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<BusinessTripParticipant, BusinessTripParticipantDto, IBusinessTripsDbScope> eventArgs)
        {
            var isParticipantAdded = eventArgs.UnitOfWork.Repositories.BusinessTripParticipants.Any(p =>
                p.EmployeeId == eventArgs.InputDto.EmployeeId && p.BusinessTripId == eventArgs.InputDto.BusinessTripId);

            if (isParticipantAdded)
            {
                throw new BusinessException(BusinessTripResources.ParticipantAlreadyAddedError);
            }

            var participant = eventArgs.InputEntity;
            var departureCity = eventArgs.UnitOfWork.Repositories.Cities.GetByIdOrDefault(participant.DepartureCityId);
            var businessTrip = eventArgs.UnitOfWork.Repositories.BusinessTrips.Filtered().GetById(participant.BusinessTripId);

            ClearRedundantVouchers(businessTrip, eventArgs.InputDto, participant, departureCity);

            Validate(eventArgs.UnitOfWork, eventArgs.InputDto, businessTrip.DestinationCityId);
            UpdateBusinessTripIncludingParticipant(eventArgs.UnitOfWork, businessTrip, participant);
            UpdateEmployee(eventArgs.UnitOfWork, eventArgs.InputDto);

            base.OnRecordAdding(eventArgs);
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<BusinessTripParticipant, BusinessTripParticipantDto, IBusinessTripsDbScope> eventArgs)
        {
            eventArgs.InputDto.BusinessTripId = eventArgs.DbEntity.BusinessTripId;
            eventArgs.InputEntity.BusinessTripId = eventArgs.DbEntity.BusinessTripId;

            eventArgs.DbEntity.LuggageDeclarations = eventArgs.InputEntity.LuggageDeclarations;

            var participant = eventArgs.InputEntity;
            var departureCity = eventArgs.UnitOfWork.Repositories.Cities.GetByIdOrDefault(participant.DepartureCityId);
            var businessTrip = eventArgs.UnitOfWork.Repositories.BusinessTrips.GetById(participant.BusinessTripId);

            ClearRedundantVouchers(businessTrip, eventArgs.InputDto, participant, departureCity);

            Validate(eventArgs.UnitOfWork, eventArgs.InputDto, businessTrip.DestinationCityId);
            UpdateBusinessTripExcludingParticipant(eventArgs.UnitOfWork, businessTrip, participant.Id);
            UpdateBusinessTripIncludingParticipant(eventArgs.UnitOfWork, businessTrip, participant);
            UpdateEmployee(eventArgs.UnitOfWork, eventArgs.InputDto);

            base.OnRecordEditing(eventArgs);
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<BusinessTripParticipant, IBusinessTripsDbScope> eventArgs)
        {
            var participant = eventArgs.DeletingRecord;
            var businessTrip = eventArgs.UnitOfWork.Repositories.BusinessTrips.GetById(participant.BusinessTripId);

            if (!businessTrip.Participants.Where(p => p.Id != participant.Id).Any())
            {
                throw new BusinessException(BusinessTripResources.CannotDeleteLastParticipantError);
            }

            if (ParticipantHasSettlementRequests(eventArgs.UnitOfWork, participant))
            {
                throw new BusinessException(BusinessTripResources.CannotDeleteParticipantWithSettlementRequestsError);
            }

            eventArgs.UnitOfWork.Repositories.LuggageDeclarations.DeleteRange(eventArgs.DeletingRecord.LuggageDeclarations);

            UpdateBusinessTripExcludingParticipant(eventArgs.UnitOfWork, businessTrip, participant.Id);

            base.OnRecordDeleting(eventArgs);
        }

        private void ClearRedundantVouchers(BusinessTrip businessTrip, BusinessTripParticipantDto participantDto, BusinessTripParticipant participant, City departureCity)
        {
            participant.DepartureCityTaxiVouchers = participant.TransportationPreferences.HasFlag(MeansOfTransport.Taxi) && departureCity.IsVoucherServiceAvailable
                ? participant.DepartureCityTaxiVouchers
                : 0;
            participantDto.DepartureCityTaxiVouchers = participant.DepartureCityTaxiVouchers;

            participant.DestinationCityTaxiVouchers = participant.TransportationPreferences.HasFlag(MeansOfTransport.Taxi) && businessTrip.DestinationCity.IsVoucherServiceAvailable
                ? participant.DestinationCityTaxiVouchers
                : 0;
            participantDto.DestinationCityTaxiVouchers = participant.DestinationCityTaxiVouchers;
        }

        private bool ParticipantHasSettlementRequests(IUnitOfWork<IBusinessTripsDbScope> unitOfWork, BusinessTripParticipant participant)
        {
            return unitOfWork.Repositories.BusinessTripSettlementRequests.Any(r => r.BusinessTripParticipantId == participant.Id);
        }

        private void UpdateBusinessTripIncludingParticipant(IUnitOfWork<IBusinessTripsDbScope> unitOfWork, BusinessTrip businessTrip, BusinessTripParticipant participant)
        {
            businessTrip.StartedOn = DateHelper.Min(participant.StartedOn, businessTrip.StartedOn);
            businessTrip.EndedOn = DateHelper.Max(participant.EndedOn, businessTrip.EndedOn);

            UpdateArrangementTrackingEntries(unitOfWork, businessTrip, participant);
            AddNewParticipantAsWatcherToRequest(unitOfWork, businessTrip.Request.Request, participant.EmployeeId);
        }

        private void UpdateBusinessTripExcludingParticipant(IUnitOfWork<IBusinessTripsDbScope> unitOfWork, BusinessTrip businessTrip, long participantId)
        {
            var participants = unitOfWork.Repositories.BusinessTripParticipants
                .Where(p => p.Id != participantId && p.BusinessTripId == businessTrip.Id)
                .Select(p => new { p.StartedOn, p.EndedOn, p.DepartureCityTaxiVouchers, p.DestinationCityTaxiVouchers })
                .ToList();

            businessTrip.StartedOn = participants.Select(p => p.StartedOn).DefaultIfEmpty(DateTime.MaxValue).Min();
            businessTrip.EndedOn = participants.Select(p => p.StartedOn).DefaultIfEmpty(DateTime.MinValue).Max();
        }

        private void AddNewParticipantAsWatcherToRequest(IUnitOfWork<IBusinessTripsDbScope> unitOfWork, Request request, long employeeId)
        {
            var user = unitOfWork.Repositories.Employees
                .Where(e => e.Id == employeeId)
                .Select(e => e.User)
                .Single();

            request.Watchers.Add(user);
        }

        protected void UpdateArrangementTrackingEntries(
            IUnitOfWork<IBusinessTripsDbScope> unitOfWork,
            BusinessTrip businessTrip,
            BusinessTripParticipant participant)
        {
            var participantDto = _participantToDtoMapping.CreateFromSource(participant);
            var dbParticipant = businessTrip.Participants.SingleOrDefault(p => p.Id == participant.Id);

            var newTrackingEntries = _businessTripService.CreateArrangementTrackingEntries(participantDto)
                .Select(_arrangementTrackingEntryDtoToArrangementTrackingEntryMapping.CreateFromSource)
                .ToArray();

            // if editing participant, update old tracking entries
            if (dbParticipant != null)
            {
                EntityMergingService.MergeCollections(
                    unitOfWork,
                    dbParticipant.ArrangementTrackingEntries,
                    newTrackingEntries,
                    e => e.ArrangementType,
                    true);
            }
            else
            {
                participant.ArrangementTrackingEntries = newTrackingEntries;
            }
        }

        private void UpdateEmployee(IUnitOfWork<IBusinessTripsDbScope> unitOfWork, BusinessTripParticipantDto participant)
        {
            var employee = unitOfWork.Repositories.Employees.GetById(participant.EmployeeId);

            if (!employee.DateOfBirth.HasValue)
            {
                employee.DateOfBirth = participant.DateOfBirth;
            }

            if (string.IsNullOrEmpty(employee.PersonalNumber))
            {
                employee.PersonalNumber = participant.PersonalNumber;
            }

            if (!employee.PersonalNumberType.HasValue)
            {
                employee.PersonalNumberType = participant.PersonalNumberType;
            }
        }

        private void Validate(IUnitOfWork<IBusinessTripsDbScope> unitOfWork, BusinessTripParticipantDto participant, long destinationCityId)
        {
            var departureCity = _cityService.GetCityOrDefaultById(participant.DepartureCityId);
            var destinationCity = _cityService.GetCityOrDefaultById(destinationCityId);
            var employee = unitOfWork.Repositories.Employees.GetById(participant.EmployeeId);
            var company = employee.EmploymentPeriods
                .Where(p => EmploymentPeriodBusinessLogic.ForDate.Call(p, participant.StartedOn))
                .Select(p => p.Company).Single();

            if ((participant.TransportationPreferences & company.AllowedMeansOfTransport) != participant.TransportationPreferences)
            {
                throw new BusinessException(string.Format(BusinessTripResources.NotAllowedMeansOfTransport, company.Name));
            }

            if ((participant.AccommodationPreferences & company.AllowedAccomodations) != participant.AccommodationPreferences)
            {
                throw new BusinessException(string.Format(BusinessTripResources.NotAllowedAccomodations, company.Name));
            }

            if (employee.CurrentEmployeeStatus == EmployeeStatus.Terminated)
            {
                throw new BusinessException(BusinessTripResources.TerminatedParticipantError);
            }

            if (participant.AccommodationPreferences.HasFlag(AccommodationType.LumpSum) && participant.AccommodationPreferences != AccommodationType.LumpSum)
            {
                throw new BusinessException(BusinessTripResources.NotOnlyLumpSumError);
            }

            if (participant.AccommodationPreferences.HasFlag(AccommodationType.CompanyApartment) && !destinationCity.IsCompanyApartmentAvailable)
            {
                throw new BusinessException(string.Format(BusinessTripResources.CompanyApartmentUnavailableError, destinationCity.Name));
            }

            if (participant.TransportationPreferences.HasFlag(MeansOfTransport.Taxi) && participant.DepartureCityTaxiVouchers > 0 && !departureCity.IsVoucherServiceAvailable)
            {
                throw new BusinessException(string.Format(BusinessTripResources.VoucherServiceUnavailableError, BusinessTripRequestResources.DepartureCities));
            }

            if (participant.TransportationPreferences.HasFlag(MeansOfTransport.Taxi) && participant.DestinationCityTaxiVouchers > 0 && !destinationCity.IsVoucherServiceAvailable)
            {
                throw new BusinessException(string.Format(BusinessTripResources.VoucherServiceUnavailableError, destinationCity.Name));
            }

            foreach (var alert in _businessTripService.ValidateParticipantEmployeesEmploymentPeriods(
                participant.StartedOn, participant.EndedOn, new[] { participant.EmployeeId }))
            {
                throw new BusinessException(alert.Message);
            }
        }
    }
}
