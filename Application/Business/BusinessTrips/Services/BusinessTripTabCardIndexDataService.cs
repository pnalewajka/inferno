﻿using System;
using System.Linq;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.BusinessTrips.Services
{
    public abstract class BusinessTripTabCardIndexDataService<TDto, TEntity>
        : CardIndexDataService<TDto, TEntity, IBusinessTripsDbScope>
        where TDto : class
        where TEntity : class, IEntity
    {
        protected BusinessTripTabCardIndexDataService(
            ICardIndexServiceDependencies<IBusinessTripsDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected abstract BusinessLogic<TEntity, BusinessTrip> BusinessTripSelector { get; }

        protected virtual BusinessLogic<TEntity, bool> ApplyEntityContextFiltering(object context)
        {
            return new BusinessLogic<TEntity, bool>(e => true);
        }

        public sealed override IQueryable<TEntity> ApplyContextFiltering(IQueryable<TEntity> records, object context)
        {
            var id = (context as ParentIdContext)?.ParentId;

            if (id == null)
            {
                throw new ArgumentNullException(nameof(ParentIdContext.ParentId));
            }

            var entityCondition = ApplyEntityContextFiltering(context);

            var condition = new BusinessLogic<TEntity, bool>(e => BusinessTripSelector.Call(e).Id == id && entityCondition.Call(e));

            return records.Where(condition.AsExpression());
        }
    }
}
