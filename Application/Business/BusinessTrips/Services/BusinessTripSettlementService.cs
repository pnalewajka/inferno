﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.BusinessTrips.BusinessLogics;
using Smt.Atomic.Business.BusinessTrips.EmailModels;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Workflows.BusinessLogics;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.BusinessTrips.Services
{
    public class BusinessTripSettlementService : IBusinessTripSettlementService
    {
        private readonly IClassMapping<BusinessTripSettlementRequest, BusinessTripSettlementRequestDto> _entityToDtoMapping;
        private readonly IUnitOfWorkService<IBusinessTripsDbScope> _unitOfWorkService;
        private readonly ITimeService _timeService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IRequestCardIndexDataService _requestCardIndexDataService;
        private readonly IRequestWorkflowService _requestWorkflowService;

        public BusinessTripSettlementService(
            IClassMapping<BusinessTripSettlementRequest, BusinessTripSettlementRequestDto> entityToDtoMapping,
            IUnitOfWorkService<IBusinessTripsDbScope> unitOfWorkService,
            ITimeService timeService,
            IMessageTemplateService messageTemplateService,
            IReliableEmailService reliableEmailService,
            ISystemParameterService systemParameterService,
            IRequestCardIndexDataService requestCardIndexDataService,
            IRequestWorkflowService requestWorkflowService)
        {
            _entityToDtoMapping = entityToDtoMapping;
            _unitOfWorkService = unitOfWorkService;
            _timeService = timeService;
            _messageTemplateService = messageTemplateService;
            _reliableEmailService = reliableEmailService;
            _systemParameterService = systemParameterService;
            _requestCardIndexDataService = requestCardIndexDataService;
            _requestWorkflowService = requestWorkflowService;
        }

        public BusinessTripSettlementRequestDto GetSettlementRequestByIdOrDefault(long settlementRequestId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var settlementRequest = unitOfWork.Repositories.BusinessTripSettlementRequests.GetById(settlementRequestId);

                return settlementRequest != null
                    ? _entityToDtoMapping.CreateFromSource(settlementRequest)
                    : null;
            }
        }

        public bool IsRequestAvailableAfterFiltering(long settlementRequestId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Requests.Filtered().Any(r => r.Id == settlementRequestId);
            }
        }

        public ICollection<BusinessTripSettlementRequestMealDto> GetMeals(BusinessTripSettlementRequestDto dto)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                if (dto.BusinessTripParticipantId != default(long))
                {
                    var participant = unitOfWork.Repositories.BusinessTripParticipants
                        .Where(p => p.Id == dto.BusinessTripParticipantId)
                        .Single();

                    var contractType = unitOfWork.Repositories.EmploymentPeriods
                        .Where(p => EmploymentPeriodBusinessLogic.OfEmployeeInDate.Call(
                            p, participant.EmployeeId, participant.BusinessTrip.StartedOn))
                        .Select(p => p.ContractType)
                        .Single();

                    if (contractType.UseDailySettlementMeals)
                    {
                        return GetDailyMeals(dto).ToArray();
                    }

                    return GetCountryMeals(unitOfWork, participant, dto).ToArray();
                }
            }

            return new BusinessTripSettlementRequestMealDto[0];
        }

        private IEnumerable<BusinessTripSettlementRequestMealDto> GetDailyMeals(
            BusinessTripSettlementRequestDto dto)
        {
            if (!dto.DepartureDateTime.HasValue || !dto.ArrivalDateTime.HasValue)
            {
                yield break;
            }

            var days = DateHelper.GetDaysInRange(dto.DepartureDateTime.Value, dto.ArrivalDateTime.Value);

            foreach (var day in days)
            {
                yield return new BusinessTripSettlementRequestMealDto
                {
                    Day = day,
                };
            }
        }

        private IEnumerable<BusinessTripSettlementRequestMealDto> GetCountryMeals(
            IUnitOfWork<IBusinessTripsDbScope> unitOfWork,
            BusinessTripParticipant participant,
            BusinessTripSettlementRequestDto dto)
        {
            var mealCountryIds = new HashSet<long>(dto.AbroadTimeEntries.Select(e => e.DepartureCountryId))
            {
                participant.DepartureCity.CountryId,
                participant.BusinessTrip.DestinationCity.CountryId
            };

            foreach (var countryId in mealCountryIds.Where(id => id != default(long)))
            {
                yield return new BusinessTripSettlementRequestMealDto
                {
                    CountryId = countryId,
                    CountryName = unitOfWork.Repositories.Countries.Where(c => c.Id == countryId).Select(c => c.Name).Single(),
                };
            }
        }

        public SettlementSectionsFeatureType GetSettlementConfiguration(long? participantId)
        {
            if (!participantId.HasValue)
            {
                return default(SettlementSectionsFeatureType);
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var participantContext = unitOfWork.Repositories
                    .BusinessTripParticipants
                    .Where(p => p.Id == participantId)
                    .Select(p => new { p.EmployeeId, p.StartedOn })
                    .Single();

                var sectionFeatures = unitOfWork.Repositories.EmploymentPeriods
                    .Where(EmploymentPeriodBusinessLogic.ForDate.Parametrize(participantContext.StartedOn))
                    .Where(p => p.EmployeeId == participantContext.EmployeeId)
                    .Select(p => p.ContractType.SettlementSectionsFeatures)
                    .SingleOrDefault();

                return sectionFeatures;
            }
        }

        public bool HasAnySettlementRequest(long businessTripId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.BusinessTripSettlementRequests.Any(r => r.BusinessTripParticipant.BusinessTripId == businessTripId);
            }
        }

        public void UpdateControllingStatus(long[] settlementRequestIds, BusinessTripSettlementControllingStatus status)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var settlementRequests = unitOfWork.Repositories.Requests.Filtered()
                    .Where(RequestBusinessLogic.IsApprovedOrCompleted)
                    .Select(r => r.BusinessTripSettlementRequest)
                    .Where(r => r.ControllingStatus != status)
                    .GetByIds(settlementRequestIds)
                    .ToList();

                if (settlementRequests.Any())
                {
                    foreach (var request in settlementRequests)
                    {
                        request.ControllingStatus = status;
                    }

                    unitOfWork.Commit();

                    // send notification to participants
                    foreach (var group in settlementRequests.GroupBy(r => r.BusinessTripParticipant.BusinessTripId))
                    {
                        SendControllingStatusChangedEmail(group.Key, settlementRequestIds, group, status);
                    }
                }
            }
        }

        private void SendControllingStatusChangedEmail(
            long businessTripId,
            long[] settlementRequestIds,
            IEnumerable<BusinessTripSettlementRequest> requests,
            BusinessTripSettlementControllingStatus status)
        {
            var serverAddress = _systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);

            var recipients = requests
                .Select(r => r.BusinessTripParticipant.Employee)
                .DistinctBy(e => e.Id)
                .Select(e => new EmailRecipientDto(e.Email, e.FullName))
                .ToList();

            var model = new SettlementControllingStatusChangedEmailModel
            {
                BusinessTripId = businessTripId,
                SettlementRequestIds = settlementRequestIds,
                BusinessTripSettlementListUrl = $"{serverAddress}BusinessTrips/BusinessTripSettlements/List?parent-id={businessTripId}",
                NewControllingStatus = status,
            };

            var subject = _messageTemplateService.ResolveTemplate(TemplateCodes.SettlementControllingStatusChangedSubject, model).Content;
            var body = _messageTemplateService.ResolveTemplate(TemplateCodes.SettlementControllingStatusChangedBody, model).Content;

            var email = new EmailMessageDto
            {
                IsHtml = true,
                Recipients = recipients,
                Subject = subject,
                Body = body,
            };

            _reliableEmailService.EnqueueMessage(email);
        }

        public void CreateEmptyRequest(long participantId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var businessTripId = unitOfWork.Repositories.BusinessTripParticipants
                    .Where(p => p.Id == participantId)
                    .Select(p => p.BusinessTripId)
                    .Single();

                var result = _requestCardIndexDataService.CreateRequestsExternally(
                    RequestType.BusinessTripSettlementRequest,
                    new List<BusinessTripSettlementRequestDto>
                    {
                        new BusinessTripSettlementRequestDto
                        {
                            BusinessTripId = businessTripId,
                            BusinessTripParticipantId = participantId,
                            IsEmpty = true,
                        }
                    });

                _requestWorkflowService.Submit(result.AddedRecordIds.Single());
            }
        }
    }
}
