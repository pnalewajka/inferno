﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using Castle.Core.Internal;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.BusinessTrips.BusinessEvents;
using Smt.Atomic.Business.BusinessTrips.BusinessLogics;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.BusinessTrips.EmailModels;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.Business.BusinessTrips.Resources;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Dictionaries.BusinessLogics;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Business.Extensions;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Presentation.Common.Interfaces;

namespace Smt.Atomic.Business.BusinessTrips.Services
{
    public class ArrangementTrackingParticipantDto
    {
        public long ParticipantId { get; set; }

        public string ParticipantName { get; set; }

        public IDictionary<TrackedArrangement, ArrangementTrackingStatus> Statuses { get; set; }
    }

    public class BusinessTripService : IBusinessTripService
    {
        private readonly ITimeService _timeService;
        private readonly IUnitOfWorkService<IBusinessTripsDbScope> _unitOfWorkService;
        private readonly IClassMapping<BusinessTrip, BusinessTripDto> _businessTripToBusinessTripDtoClassMapping;
        private readonly IClassMapping<BusinessTripRequest, BusinessTripRequestDto> _businessTripRequestToBusinessTripRequestDtoClassMapping;
        private readonly IClassMapping<BusinessTripMessage, BusinessTripMessageDto> _businessTripMessageToBusinessTripMessageDtoClassMapping;
        private readonly IClassMapping<ArrangementTrackingEntryDto, ArrangementTrackingEntry> _arrangementTrackingEntryDtoToArrangementTrackingEntryMapping;
        private readonly IAlertService _alertService;
        private readonly IBusinessTripParticipantsService _businessTripParticipantsService;
        private readonly IEmployeeService _employeeService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IBusinessEventPublisher _businessEventPublisher;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly IMessageTemplateService _messageTemplateService;

        public BusinessTripService(
            ITimeService timeService,
            IUnitOfWorkService<IBusinessTripsDbScope> unitOfWorkService,
            IAlertService alertService,
            IClassMappingFactory mappingFactory,
            IBusinessTripParticipantsService businessTripParticipantsService,
            IEmployeeService employeeService,
            ISystemParameterService systemParameterService,
            IReliableEmailService reliableEmailService,
            IMessageTemplateService messageTemplateService,
            IPrincipalProvider principalProvider,
            IBusinessEventPublisher businessEventPublisher)
        {
            _timeService = timeService;
            _unitOfWorkService = unitOfWorkService;
            _businessTripToBusinessTripDtoClassMapping = mappingFactory.CreateMapping<BusinessTrip, BusinessTripDto>();
            _businessTripRequestToBusinessTripRequestDtoClassMapping = mappingFactory.CreateMapping<BusinessTripRequest, BusinessTripRequestDto>();
            _businessTripMessageToBusinessTripMessageDtoClassMapping = mappingFactory.CreateMapping<BusinessTripMessage, BusinessTripMessageDto>();
            _arrangementTrackingEntryDtoToArrangementTrackingEntryMapping = mappingFactory.CreateMapping<ArrangementTrackingEntryDto, ArrangementTrackingEntry>();
            _alertService = alertService;
            _businessTripParticipantsService = businessTripParticipantsService;
            _employeeService = employeeService;
            _systemParameterService = systemParameterService;
            _reliableEmailService = reliableEmailService;
            _messageTemplateService = messageTemplateService;
            _principalProvider = principalProvider;
            _businessEventPublisher = businessEventPublisher;
        }

        public long CreateBusinessTripFromRequest(BusinessTripRequestDto businessTripRequest, long voucherEmployeeId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var request = unitOfWork.Repositories.BusinessTripRequests.GetById(businessTripRequest.Id);
                var projects = unitOfWork.Repositories.Projects.GetByIds(businessTripRequest.ProjectIds).ToList();
                var employees = unitOfWork.Repositories.Employees
                    .GetByIds(businessTripRequest.ParticipantEmployeeIds)
                    .ToList();

                if (employees.Any(e => e.Location?.City == null))
                {
                    throw new BusinessException(BusinessTripResources.EmployeeWithoutLocationError);
                }

                var acceptanceConditionsProject = projects.FirstOrDefault();

                request.BusinessTrip = new BusinessTrip
                {
                    Projects = projects,
                    BusinessTripAcceptanceConditions = CreateAcceptanceConditionsFromProject(acceptanceConditionsProject),
                    Participants = employees.Select(e =>
                    {
                        var company = e.EmploymentPeriods
                            .Where(p => EmploymentPeriodBusinessLogic.ForDate.Call(p, businessTripRequest.StartedOn))
                            .Select(p => p.Company).Single();

                        return new BusinessTripParticipant
                        {
                            EmployeeId = e.Id,
                            StartedOn = businessTripRequest.StartedOn,
                            EndedOn = businessTripRequest.EndedOn,
                            IdCardNumber = e.IdCardNumber,
                            PhoneNumber = e.PhoneNumber,
                            HomeAddress = e.HomeAddress,
                            AccommodationPreferences = businessTripRequest.AccommodationPreferences & company.AllowedAccomodations,
                            TransportationPreferences = businessTripRequest.TransportationPreferences & company.AllowedMeansOfTransport,
                            DepartureCityTaxiVouchers = e.Id == voucherEmployeeId ? businessTripRequest.DepartureCityTaxiVouchers : 0,
                            DestinationCityTaxiVouchers = e.Id == voucherEmployeeId ? businessTripRequest.DestinationCityTaxiVouchers : 0,
                            DepartureCityId = CalculateDepartureCity(businessTripRequest, employees, e),
                            LuggageDeclarations = new Collection<DeclaredLuggage>
                            {
                                businessTripRequest.LuggageDeclarations.Where(d => d.EmployeeId == e.Id).Select(d => new DeclaredLuggage
                                {
                                    EmployeeId = e.Id,
                                    HandLuggageItems = d.HandLuggageItems,
                                    RegisteredLuggageItems = d.RegisteredLuggageItems
                                }).SingleOrDefault()
                            },
                            ArrangementTrackingEntries = CreateArrangementTrackingEntries(businessTripRequest)
                              .Select(_arrangementTrackingEntryDtoToArrangementTrackingEntryMapping.CreateFromSource)
                              .ToArray(),
                        };
                    }).ToList(),
                    StartedOn = businessTripRequest.StartedOn,
                    EndedOn = businessTripRequest.EndedOn,
                    DestinationCityId = businessTripRequest.DestinationCityId,
                    TravelExplanation = businessTripRequest.TravelExplanation,
                    ItineraryDetails = businessTripRequest.ItineraryDetails,
                    FrontDeskAssigneeEmployeeId = null,
                };

                unitOfWork.Commit();

                // assign front-desk responsible via service (sends emails etc)
                if (businessTripRequest.AreTravelArrangementsNeeded)
                {
                    var assignee = employees
                        .GroupBy(e => e.Company.FrontDeskAssigneeEmployeeId)
                        .OrderByDescending(e => e.Count())
                        .Select(e => e.Key)
                        .FirstOrDefault();

                    if (assignee.HasValue)
                    {
                        AssignFrontDeskResponsibility(new[] { request.BusinessTrip.Id }, assignee.Value);
                    }
                }

                if (acceptanceConditionsProject != null)
                {
                    _alertService.AddInformation(string.Format(BusinessTripResources.CreatedWithDefaultAcceptanceConditionsMessageFormat, ProjectBusinessLogic.ProjectName.Call(acceptanceConditionsProject), $"/BusinessTrips/BusinessTripAcceptanceConditions/Edit?parent-id={request.BusinessTrip.Id}"));
                }

                // send email if automatically arranged
                SendArrangedNotificationEmailIfNeeded(request.BusinessTrip.Id);

                return request.BusinessTrip.Id;
            }
        }

        private long CalculateDepartureCity(
            BusinessTripRequestDto request,
            ICollection<Employee> employees,
            Employee employee)
        {
            // single participant, single departure city
            if (request.ParticipantEmployeeIds.IsSingle())
            {
                return request.DepartureCityIds.First();
            }

            var cityId = employee.Location?.CityId;

            // city from participant is one of the departures
            if (cityId.HasValue && request.DepartureCityIds.Contains(cityId.Value))
            {
                return cityId.Value;
            }

            // participant has no location, but there is 1 unassigned departure city
            var notAssignedCities = request.DepartureCityIds
                .Except(employees
                    .Where(e => e.LocationId.HasValue)
                    .Where(e => e.Location.CityId.HasValue)
                    .Select(e => e.Location.CityId.Value))
                .ToArray();

            if (notAssignedCities.Length == 1)
            {
                return notAssignedCities.Single();
            }

            // fallback (as it was)
            return cityId.Value;
        }

        private bool IsInsuranceNeeded(BusinessTripRequestDto request)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var departureCountryIds = unitOfWork.Repositories.Cities.GetByIds(request.DepartureCityIds).Select(c => c.CountryId);
                var destinationCountryId = unitOfWork.Repositories.Cities.Where(c => c.Id == request.DestinationCityId).Select(c => c.CountryId).Single();

                return departureCountryIds.Any(c => c != destinationCountryId);
            }
        }

        private bool IsInsuranceNeeded(long businessTripId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.BusinessTrips
                    .Where(b => b.Id == businessTripId)
                    .Any(b => b.Participants.Any(c => c.DepartureCity.CountryId != b.DestinationCity.CountryId));
            }
        }

        public IEnumerable<ArrangementTrackingEntryDto> CreateArrangementTrackingEntries(BusinessTripParticipantDto participant)
        {
            return CreateArrangementTrackingEntries(
                participant.TransportationPreferences,
                participant.AccommodationPreferences,
                IsInsuranceNeeded(participant.BusinessTripId));
        }

        public IEnumerable<ArrangementTrackingEntryDto> CreateArrangementTrackingEntries(BusinessTripRequestDto request)
        {
            return CreateArrangementTrackingEntries(
                request.TransportationPreferences,
                request.AccommodationPreferences,
                IsInsuranceNeeded(request));
        }

        private BusinessTripAcceptanceConditions CreateAcceptanceConditionsFromProject(Project project)
        {
            if (project == null)
            {
                return null;
            }

            return new BusinessTripAcceptanceConditions().CopyFields(project.ProjectSetup);
        }

        private IEnumerable<ArrangementTrackingEntryDto> CreateArrangementTrackingEntries(
            MeansOfTransport meansOfTransport,
            AccommodationType accomodation,
            bool isInsuranceNeeded)
        {
            if (meansOfTransport.HasFlag(MeansOfTransport.Train))
            {
                yield return new ArrangementTrackingEntryDto
                {
                    ArrangementType = TrackedArrangement.Train,
                    Status = ArrangementTrackingStatus.ToDo,
                };
            }

            if (meansOfTransport.HasFlag(MeansOfTransport.Ship))
            {
                yield return new ArrangementTrackingEntryDto
                {
                    ArrangementType = TrackedArrangement.Ship,
                    Status = ArrangementTrackingStatus.ToDo,
                };
            }

            if (meansOfTransport.HasFlag(MeansOfTransport.Plane))
            {
                yield return new ArrangementTrackingEntryDto
                {
                    ArrangementType = TrackedArrangement.Plane,
                    Status = ArrangementTrackingStatus.ToDo,
                };
            }

            if (meansOfTransport.HasFlag(MeansOfTransport.AirportTransport))
            {
                yield return new ArrangementTrackingEntryDto
                {
                    ArrangementType = TrackedArrangement.AirportTransport,
                    Status = ArrangementTrackingStatus.ToDo,
                };
            }

            if (accomodation.HasFlag(AccommodationType.CompanyApartment))
            {
                yield return new ArrangementTrackingEntryDto
                {
                    ArrangementType = TrackedArrangement.CompanyApartment,
                    Status = ArrangementTrackingStatus.ToDo,
                };
            }

            if (accomodation.HasFlag(AccommodationType.Hotel))
            {
                yield return new ArrangementTrackingEntryDto
                {
                    ArrangementType = TrackedArrangement.Hotel,
                    Status = ArrangementTrackingStatus.ToDo,
                };
            }

            if (isInsuranceNeeded)
            {
                yield return new ArrangementTrackingEntryDto
                {
                    ArrangementType = TrackedArrangement.Insurance,
                    Status = ArrangementTrackingStatus.ToDo,
                };
            }

            if (meansOfTransport.HasFlag(MeansOfTransport.Taxi))
            {
                yield return new ArrangementTrackingEntryDto
                {
                    ArrangementType = TrackedArrangement.Taxi,
                    Status = ArrangementTrackingStatus.ToDo,
                };
            }

            if (meansOfTransport.HasFlag(MeansOfTransport.Other) || accomodation.HasFlag(AccommodationType.Other))
            {
                yield return new ArrangementTrackingEntryDto
                {
                    ArrangementType = TrackedArrangement.Other,
                    Status = ArrangementTrackingStatus.ToDo,
                };
            }
        }

        public BusinessTripDto GetBusinessTripOrDefaultById(long id)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var businessTrip = unitOfWork.Repositories.BusinessTrips.Filtered().GetByIdOrDefault(id);

                return businessTrip != null
                    ? _businessTripToBusinessTripDtoClassMapping.CreateFromSource(businessTrip)
                    : null;
            }
        }

        public IEnumerable<long> GetBusinessTripParticipantIdsOrDefaultById(long businessTripId, Expression<Func<BusinessTripParticipant, bool>> predicate)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var result = unitOfWork.Repositories.BusinessTrips.Filtered()
                    .Where(t => t.Id == businessTripId)
                    .SelectMany(t => t.Participants)
                    .Where(predicate)
                    .Select(t => t.Id)
                    .ToList();

                return result;
            }
        }

        public bool IsBusinessTripAccessible(long id)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var result = unitOfWork.Repositories.BusinessTrips.Filtered().Any(bt => bt.Id == id);

                return result;
            }
        }

        public long AddBusinessTripMessage(long businessTripId, long authorEmployeeId, string content)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employeeIds = MentionItemHelper.GetTokenIds(content).Distinct();

                var businessTripMessage = new BusinessTripMessage
                {
                    AuthorEmployeeId = authorEmployeeId,
                    AddedOn = _timeService.GetCurrentTime(),
                    Content = content,
                    BusinessTripId = businessTripId,
                    MentionedEmployees = unitOfWork.Repositories.Employees.GetByIds(employeeIds).ToList()
                };

                unitOfWork.Repositories.BusinessTripMessages.Add(businessTripMessage);
                unitOfWork.Commit();

                _businessEventPublisher.PublishBusinessEvent(new BusinessTripMessageAddedEvent
                {
                    BusinessTripMessageId = businessTripMessage.Id,
                });

                return businessTripMessage.Id;
            }
        }

        public IEnumerable<BusinessTripMessageDto> GetBusinessTripMessagesOrDefaultById(long id)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var messages = unitOfWork.Repositories.BusinessTripMessages
                    .Filtered()
                    .Where(m => m.BusinessTripId == id)
                    .AsEnumerable()
                    .Select(_businessTripMessageToBusinessTripMessageDtoClassMapping.CreateFromSource)
                    .ToList();

                return messages;
            }
        }

        public ArrangementTrackingParticipantDto[] GetArrangementTrackingEntries(long businessTripId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var arrangementTrackingEntries = unitOfWork.Repositories.BusinessTrips
                    .Filtered()
                    .Where(e => e.Id == businessTripId)
                    .SelectMany(e => e.Participants)
                    .SelectMany(e => e.ArrangementTrackingEntries)
                    .GroupBy(e => e.ParticipantId)
                    .ToList()
                    .Select(g => new ArrangementTrackingParticipantDto
                    {
                        ParticipantId = g.Key,
                        ParticipantName = EmployeeBusinessLogic.FullName.Call(g.First().Participant.Employee),
                        Statuses = g.ToDictionary(r => r.ArrangementType, r => r.Status)
                    });

                return arrangementTrackingEntries.ToArray();
            }
        }

        public string GetArrangementTrackingRemarks(long businessTripId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.BusinessTrips
                    .Filtered()
                    .Where(e => e.Id == businessTripId)
                    .Select(e => e.ArrangementTrackingRemarks)
                    .Single();
            }
        }

        public void SaveArrangementTrackingRemarks(long businessTripId, string remarks)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var businessTrip = unitOfWork.Repositories.BusinessTrips
                    .Filtered()
                    .Single(e => e.Id == businessTripId);

                businessTrip.ArrangementTrackingRemarks = remarks;

                unitOfWork.Commit();
            }
        }

        public void SaveArrangementTrackingStatuses(long businessTripId, ArrangementTrackingParticipantDto[] participants)
        {
            var participantIds = participants.Select(p => p.ParticipantId).ToArray();

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var dbParticipants = unitOfWork.Repositories.ArrangementTrackingEntries.Filtered()
                    .Where(e => participantIds.Contains(e.ParticipantId)).ToList();

                foreach (var participant in participants)
                {
                    foreach (var arrangement in participant.Statuses)
                    {
                        var dbParticipant = dbParticipants.Single(
                            p => p.ParticipantId == participant.ParticipantId
                                && p.ArrangementType == arrangement.Key);

                        dbParticipant.Status = arrangement.Value;
                    }
                }

                unitOfWork.Commit();
            }
        }

        public void AssignFrontDeskResponsibility(long[] businessTripIds, long employeeId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var userRoles = unitOfWork.Repositories.Employees
                    .Where(e => e.Id == employeeId).Select(e => e.User).Single()
                    .GetAllRoleTypes();

                var canManageFrontDesk = userRoles.Contains(SecurityRoleType.CanManageBusinessTripFrontDesk);

                if (!canManageFrontDesk)
                {
                    throw new InvalidOperationException("Employee has no required role (CanManageBusinessTripFrontDesk)");
                }

                var businessTrips = unitOfWork.Repositories.BusinessTrips.Filtered().GetByIds(businessTripIds).ToList();

                foreach (var businessTrip in businessTrips)
                {
                    businessTrip.FrontDeskAssigneeEmployeeId = employeeId;
                }

                unitOfWork.Commit();

                if (!CollectionExtensions.IsNullOrEmpty(businessTrips))
                {
                    _businessEventPublisher.PublishBusinessEvent(new FrontDeskResponsabilityAssignedEvent
                    {
                        FrontDeskAssigneeEmployeeId = employeeId,
                        BusinessTripIds = businessTrips.GetIds(),
                    });
                }
            }
        }

        private ICollection<AlertDto> ValidateParticipantEmployeesEmploymentPeriods(DateTime btStartDate, DateTime btEndDate, IQueryable<Employee> employees)
        {
            var result = new List<AlertDto>();

            var isEmploymentPeriodInRange = EmploymentPeriodBusinessLogic.OverlapingRange
                    .Parametrize(btStartDate, btEndDate);

            foreach (var employee in employees)
            {
                var employmentPeriods = employee.EmploymentPeriods.AsQueryable().Where(isEmploymentPeriodInRange);

                if (!EmploymentPeriodBusinessLogic.IsActiveInPeriod
                    .Call(employmentPeriods, btStartDate, btEndDate))
                {
                    result.Add(new AlertDto
                    {
                        Message = string.Format(
                            BusinessTripResources.ParticipantHasNoActiveEmploymentPeriod,
                            $"{employee.FirstName} {employee.LastName}"),
                        Type = AlertType.Error
                    });
                }
            }

            return result;
        }

        public ICollection<AlertDto> ValidateParticipantEmployeesEmploymentPeriods(DateTime btStartDate, DateTime btEndDate, ICollection<long> employeeIds)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employees = unitOfWork.Repositories.Employees
                    .Where(p => employeeIds.Contains(p.Id));

                return ValidateParticipantEmployeesEmploymentPeriods(btStartDate, btEndDate, employees);
            }
        }

        public bool IsParticipantBusinessTripInternational(long participantId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork
                    .Repositories
                    .BusinessTripParticipants
                    .Where(p => p.Id == participantId)
                    .Select(BusinessTripBusinessLogic.IsInternational.AsExpression())
                    .Single();
            }
        }

        public bool SendArrangedNotificationEmailIfNeeded(long businessTripId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var businessTrip = unitOfWork.Repositories.BusinessTrips.GetById(businessTripId);

                return SendArrangedNotificationEmailIfNeeded(businessTrip);
            }
        }

        private bool SendArrangedNotificationEmailIfNeeded(BusinessTrip businessTrip)
        {
            if (!BusinessTripBusinessLogic.IsArranged.Call(businessTrip))
            {
                return false;
            }

            const string BusinessTripUrlFormat = "{0}BusinessTrips/BusinessTripArrangements?parent-id={1}";

            var recipients = new List<EmailRecipientDto>();
            var employees = businessTrip.Participants.Select(p => new { p.Employee.Email, p.Employee.FullName });

            foreach (var employee in employees)
            {
                recipients.Add(new EmailRecipientDto { EmailAddress = employee.Email, FullName = employee.FullName });
            }

            var serverAddress = _systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);

            var businessTripArrangedReminderDto = new BusinessTripArrangedReminderDto
            {
                BusinessTripName = $"BT-{businessTrip.Id}",
                BusinessTripUrl = string.Format(BusinessTripUrlFormat, serverAddress, businessTrip.Id)
            };

            var emailSubject = _messageTemplateService.ResolveTemplate(TemplateCodes.BusinessTripsArrangedSubject, businessTripArrangedReminderDto);
            var emailBody = _messageTemplateService.ResolveTemplate(TemplateCodes.BusinessTripsArrangedBody, businessTripArrangedReminderDto);

            var emailMessageDto = new EmailMessageDto
            {
                IsHtml = emailBody.IsHtml,
                Subject = emailSubject.Content,
                Body = emailBody.Content,
                Recipients = recipients
            };

            _reliableEmailService.EnqueueMessage(emailMessageDto);

            return true;
        }
    }
}
