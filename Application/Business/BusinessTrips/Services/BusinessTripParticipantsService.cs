﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.BusinessTrips.BusinessLogics;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.BusinessTrips.Helpers;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.Business.BusinessTrips.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.BusinessTrips.Services
{
    internal class BusinessTripParticipantsService : IBusinessTripParticipantsService
    {
        private readonly IClassMappingFactory _classMappingFactory;
        private readonly IUnitOfWorkService<IBusinessTripsDbScope> _unitOfWorkService;
        private readonly ITimeService _timeService;

        public BusinessTripParticipantsService(
            IClassMappingFactory classMappingFactory,
            IUnitOfWorkService<IBusinessTripsDbScope> unitOfWorkService,
            ITimeService timeService)
        {
            _classMappingFactory = classMappingFactory;
            _unitOfWorkService = unitOfWorkService;
            _timeService = timeService;
        }

        public bool ShouldParticipantSpecifyDestinationType(long participantId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.BusinessTripParticipants
                    .Where(p => p.Id == participantId)
                    .SelectMany(p => p.Employee.EmploymentPeriods
                        .Where(ep => EmploymentPeriodBusinessLogic.ForDate.Call(ep, p.BusinessTrip.StartedOn))
                        .Select(ep => ep.ContractType.HasToSpecifyDestinationType))
                    .SingleOrDefault();
            }
        }

        public string GetSettlementReportCodeByParticipantId(long participantId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var businessTripCalculator = unitOfWork.Repositories.BusinessTripParticipants
                    .Where(p => p.Id == participantId)
                    .SelectMany(p => p.Employee.EmploymentPeriods
                        .Where(ep => EmploymentPeriodBusinessLogic.ForDate.Call(ep, p.BusinessTrip.StartedOn))
                        .Select(ep => ep.ContractType.BusinessTripCompensationCalculator))
                    .SingleOrDefault();

                switch (businessTripCalculator)
                {
                    case BusinessTripCompensationCalculators.HourlyRateBasedPL:
                    case BusinessTripCompensationCalculators.EmploymentContractPL:
                        return BusinessTripSettlementReportHelper.ReportDataSource;

                    case BusinessTripCompensationCalculators.EmploymentContractDE:
                        return BusinessTripSettlementReportHelper.IntiveGmbhReportDataSource;

                    default:
                        throw new BusinessException(ReportsResources.ReportNotAvailable);
                }
            }
        }

        public BusinessTripParticipantDto GetBusinessTripParticipantById(long participantId)
        {
            var participantsClassMapping = _classMappingFactory.CreateMapping<BusinessTripParticipant, BusinessTripParticipantDto>();

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork
                    .Repositories
                    .BusinessTripParticipants
                    .Where(p => p.Id == participantId)
                    .Select(participantsClassMapping.CreateFromSource)
                    .Single();
            }
        }

        public IEnumerable<BusinessTripParticipantDto> GetBusinessTripParticipants(long businessTripId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var participantsClassMapping = _classMappingFactory.CreateMapping<BusinessTripParticipant, BusinessTripParticipantDto>();

                return unitOfWork
                    .Repositories
                    .BusinessTripParticipants
                    .Filtered()
                    .Where(p => p.BusinessTripId == businessTripId)
                    .Select(participantsClassMapping.CreateFromSource)
                    .ToList();
            }
        }

        /// <remarks>
        /// This method is used by JobScheduler so the request shouldn't be filtered.
        /// </remarks>
        public IEnumerable<BusinessTripParticipantDto> GetBusinessTripParticipantsWithUnsettledBusinessTripToNotify(long businessTripId)
        {
            var currentDate = _timeService.GetCurrentDate();
            var isCurrentEmploymentPeriod = EmploymentPeriodBusinessLogic.ForDate
                .Parametrize(currentDate);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var participantsClassMapping = _classMappingFactory.CreateMapping<BusinessTripParticipant, BusinessTripParticipantDto>();

                return unitOfWork
                    .Repositories
                    .BusinessTripParticipants
                    .Where(p =>
                        p.Employee.EmploymentPeriods.AsQueryable()
                            .Where(isCurrentEmploymentPeriod)
                            .Any(ep => ep.ContractType.NotifyMissingBusinessTripSettlement)
                        && p.BusinessTripId == businessTripId
                        && BusinessTripParticipantBusinessLogic.HasPendingSettlements.Call(p, currentDate))
                    .Select(participantsClassMapping.CreateFromSource)
                    .ToList();
            }
        }

        public IEnumerable<long> GetParticipantIdsByEmployeeId(long employeeId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.BusinessTripParticipants.Where(p => p.EmployeeId == employeeId).Select(p => p.Id).ToList();
            }
        }

        public long GetEmployeeIdByParticipantId(long participantId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.BusinessTripParticipants.SelectById(participantId, p => p.EmployeeId);
            }
        }

        public long GetUserIdByParticipantId(long participantId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.BusinessTripParticipants.SelectById(participantId, p => p.Employee.UserId.Value);
            }
        }
    }
}
