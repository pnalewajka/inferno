﻿using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.BusinessTrips.Services
{
    public class BusinessTripSettlementsCardIndexDataService
        : BusinessTripTabCardIndexDataService<BusinessTripSettlementDto, BusinessTripParticipant>, IBusinessTripSettlementsCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public BusinessTripSettlementsCardIndexDataService(
            ICardIndexServiceDependencies<IBusinessTripsDbScope> dependencies) 
            : base(dependencies)
        {
        }

        protected override BusinessLogic<BusinessTripParticipant, BusinessTrip> BusinessTripSelector
            => new BusinessLogic<BusinessTripParticipant, BusinessTrip>(p => p.BusinessTrip);

        protected override IQueryable<BusinessTripParticipant> ConfigureIncludes(IQueryable<BusinessTripParticipant> sourceQueryable)
        {
            return base.ConfigureIncludes(sourceQueryable)
                .Include(a => a.SettlementRequests.Select(r => r.Request.ApprovalGroups.Select(g => g.Approvals)));
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<BusinessTripParticipant> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var direction = sortingCriterion.GetSortingDirection();

            if (sortingCriterion.GetSortingColumnName() == nameof(BusinessTripParticipantDto.EmployeeFirstName))
            {
                orderedQueryBuilder.ApplySortingKey(p => p.Employee.LastName, direction);
            }
            else if (sortingCriterion.GetSortingColumnName() == nameof(BusinessTripParticipantDto.EmployeeLastName))
            {
                orderedQueryBuilder.ApplySortingKey(p => p.Employee.FirstName, direction);
            }
            else
            {
                base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
            }
        }
    }
}
