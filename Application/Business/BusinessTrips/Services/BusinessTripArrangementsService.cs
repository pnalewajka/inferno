﻿using Castle.Core.Logging;
using Smt.Atomic.Business.BusinessTrips.DocumentMappings;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Common.Resources;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;

namespace Smt.Atomic.Business.BusinessTrips.Services
{
    internal class BusinessTripArrangementsService : IBusinessTripArrangementsService
    {
        private readonly IClassMappingFactory _classMappingFactory;
        private readonly IUnitOfWorkService<IBusinessTripsDbScope> _unitOfWorkService;
        private readonly IUploadedDocumentHandlingService _uploadedDocumentHandlingService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly ICalculatorResolver _calculatorResolver;
        private readonly ILogger _logger;

        public BusinessTripArrangementsService(IUnitOfWorkService<IBusinessTripsDbScope> unitOfWorkService,
            IUploadedDocumentHandlingService uploadedDocumentHandlingService,
            IPrincipalProvider principalProvider,
            ICalculatorResolver calculatorResolver,
            IClassMappingFactory classMappingFactory,
            ILogger logger
            )
        {
            _unitOfWorkService = unitOfWorkService;
            _uploadedDocumentHandlingService = uploadedDocumentHandlingService;
            _classMappingFactory = classMappingFactory;
            _principalProvider = principalProvider;
            _calculatorResolver = calculatorResolver;
            _logger = logger;
        }

        public IEnumerable<BusinessTripArrangementDto> GetBusinessTripArrangements(long businessTripId)
        {
            var currencyClassMapping = _classMappingFactory.CreateMapping<Currency, CurrencyDto>();

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var arrangements = unitOfWork.Repositories.Arrangements
                    .Include(a => a.Participants)
                    .Include(a => a.ArrangementDocuments)
                    .Include(a => a.Currency)
                    .Filtered()
                    .Where(a => a.BusinessTripId == businessTripId)
                    .ToList();

                return arrangements.Select(a => new BusinessTripArrangementDto
                {
                    Id = a.Id,
                    ArrangementType = a.ArrangementType,
                    Name = a.Name,
                    IssuedOn = a.IssuedOn,
                    Participants =
                        a.Participants?.Select(p => new BusinessTripDisplayParticipantDto
                        {
                            Id = p.Id,
                            EmployeeId = p.EmployeeId,
                            EmployeeFirstName = p.Employee.FirstName,
                            EmployeeLastName = p.Employee.LastName,
                        }).ToList(),
                    ArrangementDocuments =
                        a.ArrangementDocuments?.Select(d => new ArrangementDocumentDto
                        {
                            Id = d.Id,
                            Name = d.Name
                        }).ToList(),
                    Currency = a.Currency != null ? currencyClassMapping.CreateFromSource(a.Currency) : null,
                    Value = a.Value,
                    Details = a.Details,
                    Timestamp = a.Timestamp
                }).ToList();
            }
        }

        public IEnumerable<BusinessTripArrangementDto> SaveBusinessTripArrangements(long businessTripId,
            IReadOnlyCollection<IBusinessTripArrangementSaveContract> arrangements)
        {
            var arrangementIds = arrangements.Where(a => a.Id.HasValue).Select(a => a.Id.Value).ToList();

            using (var transactionScope = TransactionHelper.CreateDefaultTransactionScope())
            {
                using (var unitOfWork = _unitOfWorkService.Create())
                {
                    var canManageArrangementIds = unitOfWork.Repositories.Arrangements
                        .Filtered()
                        .Where(a => a.BusinessTripId == businessTripId)
                        .GetIds();

                    var castedDocuments = new List<DocumentDto>();

                    foreach (var contract in arrangements.Where(a => !a.Id.HasValue))
                    {
                        var addArrangement = UpdateArrangement(unitOfWork,
                            new Arrangement
                            {
                                BusinessTripId = businessTripId,
                                CreatedByUserId = _principalProvider.Current.Id.Value,
                                ArrangementDocuments = new Collection<ArrangementDocument>()
                            }, contract);

                        unitOfWork.Repositories.Arrangements.Add(addArrangement);
                        castedDocuments.AddRange(MergeDocuments(unitOfWork, addArrangement, contract));
                    }

                    var toEdit =
                        unitOfWork.Repositories.Arrangements
                        .Filtered()
                        .Where(a =>
                            a.BusinessTripId == businessTripId
                            && canManageArrangementIds.Contains(a.Id))
                            .ToDictionary(a => a.Id);

                    foreach (var editArrangement in arrangements)
                    {
                        if (editArrangement.Id.HasValue && toEdit.ContainsKey(editArrangement.Id.Value))
                        {
                            UpdateArrangement(unitOfWork, toEdit[editArrangement.Id.Value], editArrangement);
                        }
                    }

                    unitOfWork.Commit();

                    if (RecalculateSettlementsRequired(unitOfWork, businessTripId))
                    {
                        unitOfWork.Commit();
                    }

                    _uploadedDocumentHandlingService
                        .PromoteTemporaryDocuments
                        <ArrangementDocument, ArrangementDocumentContent, ArrangementDocumentMapping,
                            IBusinessTripsDbScope>(castedDocuments);

                    transactionScope.Complete();
                }
            }

            return GetBusinessTripArrangements(businessTripId);
        }

        public BoolResult RemoveArrangement(long arrangementId)
        {
            using (var transactionScope = TransactionHelper.CreateDefaultTransactionScope())
            {
                using (var unitOfWork = _unitOfWorkService.Create())
                {
                    var businessTripId = unitOfWork.Repositories.Arrangements
                        .Filtered()
                        .SelectById(arrangementId, a => a.BusinessTripId);

                    unitOfWork.Repositories.ArrangementDocumentContents.DeleteEach(
                        c => c.Document.Arrangement.Id == arrangementId);

                    unitOfWork.Repositories.ArrangementDocuments.DeleteEach(
                        c => c.ArrangementId == arrangementId);

                    unitOfWork.Repositories.Arrangements.DeleteEach(
                        a => a.Id == arrangementId);

                    unitOfWork.Commit();

                    if (RecalculateSettlementsRequired(unitOfWork, businessTripId))
                    {
                        unitOfWork.Commit();
                    }

                    transactionScope.Complete();
                    
                    _logger.Warn($"EmployeeId={_principalProvider.Current.EmployeeId}, Login={_principalProvider.Current.Login}: Removed arrangement from business trip Id={businessTripId}");

                    return BoolResult.Success;
                }
            }
        }

        private bool RecalculateSettlementsRequired(IUnitOfWork<IBusinessTripsDbScope> unitOfWork, long businessTripId)
        {
            var settlementRequests = unitOfWork.Repositories
                .BusinessTripSettlementRequests
                .Where(r => r.BusinessTripParticipant.BusinessTripId == businessTripId)
                .AsEnumerable();

            var calculationChanged = false;

            foreach (var settlementRequest in settlementRequests)
            {
                var settlementCalculator = _calculatorResolver.GetCalculatorBySettlementRequestId(settlementRequest.Id);
                var calculator = _calculatorResolver.GetErrorCalculator(settlementCalculator);
                var calculationResult = calculator.Calculate();

                var newCalculation = JsonHelper.Serialize(calculationResult);

                calculationChanged = calculationChanged || newCalculation != settlementRequest.CalculationResult;

                settlementRequest.CalculationResult = newCalculation;
            }

            return calculationChanged;
        }

        private IEnumerable<DocumentDto> MergeDocuments(IUnitOfWork<IBusinessTripsDbScope> unitOfWork, Arrangement arrangement, IBusinessTripArrangementSaveContract contract)
        {
            var documentDtos = contract.ArrangementDocuments
                .Select(d => new DocumentDto
                {
                    TemporaryDocumentId = d.TemporaryDocumentId,
                    DocumentId = d.DocumentId,
                    ContentType = d.ContentType,
                    DocumentName = d.DocumentName
                }).ToList();

            _uploadedDocumentHandlingService.MergeDocumentCollections(unitOfWork,
                        arrangement.ArrangementDocuments
                        , documentDtos,
                        p => new ArrangementDocument
                        {
                            Name = p.DocumentName,
                            ContentType = p.ContentType,
                            Arrangement = arrangement
                        });

            return documentDtos;
        }

        private Arrangement UpdateArrangement
            (
            IUnitOfWork<IBusinessTripsDbScope> unitOfWork,
            Arrangement arrangement,
            IBusinessTripArrangementSaveContract contract)
        {
            if (arrangement.Timestamp != null && string.Join("", arrangement.Timestamp) != string.Join("", contract.Timestamp))
            {
                throw new DatabaseConcurrencyException(Exceptions.ConcurrencyExceptionMessage);
            }

            arrangement.ArrangementType = contract.ArrangementType;
            arrangement.Name = contract.Name;
            arrangement.Details = contract.Details;
            arrangement.Value = contract.Value;
            arrangement.IssuedOn = contract.IssuedOn;

            arrangement.Currency = contract.CurrencyId.HasValue
                ? unitOfWork.Repositories.Currencies.GetById(contract.CurrencyId.Value)
                : unitOfWork.Repositories.BusinessTripParticipants
                    .Where(p => p.BusinessTripId == arrangement.BusinessTripId)
                    .Select(p => p.Employee.Company.DefaultAdvancedPaymentCurrency)
                    .First();

            arrangement.Participants?.Clear();
            arrangement.Participants = contract.ParticipantIds != null && contract.ParticipantIds.Any()
                ? unitOfWork.Repositories.BusinessTripParticipants.GetByIds(contract.ParticipantIds).ToList()
                : null;

            return arrangement;
        }
    }
}