﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.BusinessTrips.Services
{
    public class BusinessTripContext
    {
        [UrlFieldName("year")]
        public int Year { get; set; }

        [UrlFieldName("month")]
        public byte Month { get; set; }

        public BusinessTripPickerMode Mode { get; set; }
    }
}
