﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.BusinessTrips.Consts;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.Business.BusinessTrips.Resources;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.BusinessTrips.Services
{
    public class DailyAllowanceCardIndexDataService : CardIndexDataService<DailyAllowanceDto, DailyAllowance, IBusinessTripsDbScope>, IDailyAllowanceCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public DailyAllowanceCardIndexDataService(ICardIndexServiceDependencies<IBusinessTripsDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override NamedFilters<DailyAllowance> NamedFilters
        {
            get
            {
                return new NamedFilters<DailyAllowance>(new NamedFilter<DailyAllowance>[]
                {
                    new NamedFilter<DailyAllowance, long>(
                        FilterCodes.DailyAllowanceCountry,
                        (allowance, countryId) => countryId == allowance.EmploymentCountryId),

                    new NamedFilter<DailyAllowance, DateTime>(
                        FilterCodes.DailyAllowanceValidForDate,
                        (a, validFor) =>
                            (a.ValidFrom != null && a.ValidTo != null && a.ValidFrom <= validFor && a.ValidTo >= validFor) ||
                            (a.ValidFrom == null && a.ValidTo >= validFor) ||
                            (a.ValidTo == null && a.ValidFrom <= validFor)
                    )
                });
            }
        }

        protected override AddRecordResult InternalAddRecord(DailyAllowanceDto record, object context)
        {
            var alerts = ValidateRecord(record);
            return alerts != null && alerts.Any()
                ? new AddRecordResult(false, 0, null, alerts)
                : base.InternalAddRecord(record, context);
        }

        protected override EditRecordResult InternalEditRecord(DailyAllowanceDto newRecord,
            out DailyAllowanceDto originalRecord, object context)
        {
            var alerts = ValidateRecord(newRecord, true);
            originalRecord = null;
            return alerts != null && alerts.Any()
                ? new EditRecordResult(false, 0, null, alerts)
                : base.InternalEditRecord(newRecord, out originalRecord, context);
        }

        protected override IEnumerable<Expression<Func<DailyAllowance, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return a => a.TravelCountry.Name;
            yield return a => a.TravelCountry.IsoCode;
        }

        private List<AlertDto> ValidateRecord(DailyAllowanceDto record, bool isEditing = false)
        {
            var alerts = new List<AlertDto>();

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var existingRecords = unitOfWork.Repositories.DailyAllowances
                    .Where(d => d.EmploymentCountryId == record.EmploymentCountryId
                        && ((record.TravelCountryId.HasValue && d.TravelCountryId == record.TravelCountryId)
                            || (record.TravelCityId.HasValue && d.TravelCityId == record.TravelCityId)))
                    .ToList();

                if (!existingRecords.Any())
                {
                    return null;
                }

                if (isEditing)
                {
                    existingRecords.Remove(existingRecords.First(r => r.Id == record.Id));
                }

                if (existingRecords.Any(r => r.ValidFrom == null || r.ValidTo == null))
                {
                    alerts.Add(new AlertDto
                    {
                        Message = DailyAllowanceResources.PreviousEntryNotClosedErrorMessage,
                        IsDismissable = false,
                        Type = AlertType.Error
                    });
                }
                else if (
                    (record.ValidFrom != null && existingRecords.Any(r =>
                         record.ValidFrom.IsBetweenDates(r.ValidFrom.Value, r.ValidTo.Value))) ||
                    (record.ValidTo != null && existingRecords.Any(r =>
                         record.ValidTo.IsBetweenDates(r.ValidFrom.Value, r.ValidTo.Value))))
                {
                    alerts.Add(new AlertDto
                    {
                        Message = DailyAllowanceResources.IncorrectDatesErrorMessage,
                        IsDismissable = false,
                        Type = AlertType.Error
                    });
                }
            }

            return alerts;
        }
    }
}
