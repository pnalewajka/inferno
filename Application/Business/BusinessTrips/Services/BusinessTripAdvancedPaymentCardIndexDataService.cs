﻿using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.BusinessTrips.Services
{
    public class BusinessTripAdvancedPaymentCardIndexDataService
        : BusinessTripTabCardIndexDataService<BusinessTripAdvancedPaymentRequestDto, AdvancedPaymentRequest>, 
        IBusinessTripAdvancedPaymentCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        protected override BusinessLogic<AdvancedPaymentRequest, BusinessTrip> BusinessTripSelector
            => new BusinessLogic<AdvancedPaymentRequest, BusinessTrip>(p => p.BusinessTrip);

        public BusinessTripAdvancedPaymentCardIndexDataService(ICardIndexServiceDependencies<IBusinessTripsDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<AdvancedPaymentRequest> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var direction = sortingCriterion.GetSortingDirection();

            if (sortingCriterion.GetSortingColumnName() == nameof(BusinessTripAdvancedPaymentRequestDto.ParticipantFullName))
            {
                var sortingExpression = new BusinessLogic<AdvancedPaymentRequest, string>(
                    r => EmployeeBusinessLogic.FullNameReversed.Call(r.Participant.Employee)
                );

                orderedQueryBuilder.ApplySortingKey(sortingExpression.AsExpression(), direction);
            }
            else
            {
                base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
            }
        }
    }
}