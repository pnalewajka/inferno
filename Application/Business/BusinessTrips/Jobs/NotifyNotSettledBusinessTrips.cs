﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Castle.Core.Logging;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.BusinessTrips.BusinessLogics;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.BusinessTrips.Jobs
{
    [Identifier("Job.BusinessTrips.NotifyNotSettledBusinessTrips")]
    [JobDefinition(
        Code = "NotifyNotSettledBusinessTrips",
        Description = "Notify not settled business trips",
        ScheduleSettings = "0 23 * * * *",
        PipelineName = PipelineCodes.Notifications)]
    public sealed class NotifyNotSettledBusinessTrips : IJob
    {
        private readonly ILogger _logger;
        private readonly IUnitOfWorkService<IBusinessTripsDbScope> _unitOfWorkService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IBusinessTripParticipantsService _businessTripParticipantsService;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IEmployeeService _employeeService;
        private readonly ITimeService _timeService;

        public NotifyNotSettledBusinessTrips(
            ILogger logger,
            IUnitOfWorkService<IBusinessTripsDbScope> unitOfWorkService,
            ITimeService timeService,
            IBusinessTripParticipantsService businessTripParticipantsService,
            ISystemParameterService systemParameterService,
            IReliableEmailService reliableEmailService,
            IMessageTemplateService messageTemplateService,
            IEmployeeService employeeService)
        {
            _logger = logger;
            _unitOfWorkService = unitOfWorkService;
            _systemParameterService = systemParameterService;
            _businessTripParticipantsService = businessTripParticipantsService;
            _systemParameterService = systemParameterService;
            _reliableEmailService = reliableEmailService;
            _messageTemplateService = messageTemplateService;
            _employeeService = employeeService;
            _timeService = timeService;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            try
            {
                SendMessages(executionContext.CancellationToken);
            }
            catch (Exception exception)
            {
                _logger.Error("Job.BusinessTrips.NotifyNotSettledBusinessTrips error", exception);

                return JobResult.Fail();
            }

            return JobResult.Success();
        }

        private void SendMessages(CancellationToken cancellationToken)
        {
            var daysAfterWhichRemindOfUnsettledBusinessTripsInDays = _systemParameterService.GetParameter<int>(ParameterKeys.DaysAfterWhichRemindOfUnsettledBusinessTripsInDays);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var dateToCompare = _timeService.GetCurrentDate().AddDays(daysAfterWhichRemindOfUnsettledBusinessTripsInDays * -1);

                var businessTrips = unitOfWork.Repositories.BusinessTrips
                    .Where(BusinessTripBusinessLogic.IsNotAccounted)
                    .Where(b => b.EndedOn < dateToCompare).ToList();

                foreach (var businessTrip in businessTrips.TakeWhileNotCancelled(cancellationToken))
                {
                    if (cancellationToken.IsCancellationRequested)
                    {
                        return;
                    }

                    SendEmail(businessTrip, daysAfterWhichRemindOfUnsettledBusinessTripsInDays);
                }
            }
        }

        private void SendEmail(BusinessTrip businessTrip, int daysAfterFinished)
        {
            const string BusinessTripUrlFormat = "{0}BusinessTrips/BusinessTripSettlements/List?parent-id={1}";

            var recipients = new List<EmailRecipientDto>();

            var employeesIds = _businessTripParticipantsService.GetBusinessTripParticipantsWithUnsettledBusinessTripToNotify(businessTrip.Id)
                .Select(p => p.EmployeeId);

            foreach (var employeeDto in _employeeService.GetEmployeesById(employeesIds))
            {
                recipients.Add(new EmailRecipientDto { EmailAddress = employeeDto.Email, FullName = employeeDto.FullName });
            }

            if (recipients.Any())
            {
                var serverAddress = _systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);

                var businessTripNotSettledReminderDto = new BusinessTripNotSettledReminderDto
                {
                    BusinessTripName = $"BT-{businessTrip.Id}",
                    BusinessTripUrl = string.Format(BusinessTripUrlFormat, serverAddress, businessTrip.Id),
                    DaysAfterFinished = daysAfterFinished
                };

                var emailSubject = _messageTemplateService.ResolveTemplate(TemplateCodes.BusinessTripNotSettledReminderSubject, businessTripNotSettledReminderDto);
                var emailBody = _messageTemplateService.ResolveTemplate(TemplateCodes.BusinessTripNotSettledReminderBody, businessTripNotSettledReminderDto);

                var emailMessageDto = new EmailMessageDto
                {
                    IsHtml = emailBody.IsHtml,
                    Subject = emailSubject.Content,
                    Body = emailBody.Content,
                    Recipients = recipients
                };

                _reliableEmailService.EnqueueMessage(emailMessageDto);
            }
        }
    }
}