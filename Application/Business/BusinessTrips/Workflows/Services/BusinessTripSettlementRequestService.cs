﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.Business.BusinessTrips.Resources;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Compensation.BusinessEvents;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.Business.Dictionaries.BusinessLogics;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Enums;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.Business.Workflows.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Business.Helpers;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.BusinessTrips.Workflows.Services
{
    internal class BusinessTripSettlementRequestService
        : RequestServiceBase<BusinessTripSettlementRequest, BusinessTripSettlementRequestDto>
    {
        private readonly IUnitOfWorkService<IBusinessTripsDbScope> _unitOfWorkService;
        private readonly IApprovingPersonService _approvingPersonService;
        private readonly IBusinessTripSettlementService _businessTripSettlementService;
        private readonly IUserService _userService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IBusinessEventPublisher _businessEventPublisher;
        private readonly ICalculatorResolver _calculatorResolver;
        private readonly IEntityMergingService _entityMergingService;
        private readonly IBusinessTripParticipantsService _businessTripParticipantsService;
        private readonly ITimeService _timeService;
        private readonly IEmployeeService _employeeService;

        public BusinessTripSettlementRequestService(
            IActionSetService actionSetService,
            IPrincipalProvider principalProvider,
            IClassMappingFactory classMappingFactory,
            IWorkflowNotificationService workflowNotificationService,
            IUnitOfWorkService<IBusinessTripsDbScope> unitOfWorkService,
            IApprovingPersonService approvingPersonService,
            IBusinessTripSettlementService businessTripSettlementService,
            ISystemParameterService systemParameterService,
            IUserService userService,
            ICalculatorResolver calculatorResolver,
            IBusinessEventPublisher businessEventPublisher,
            IBusinessTripParticipantsService businessTripParticipantsService,
            ITimeService timeService,
            IRequestWorkflowService requestWorkflowService,
            IEntityMergingService entityMergingService,
            IEmployeeService employeeService)
            : base(actionSetService, principalProvider, classMappingFactory, workflowNotificationService, requestWorkflowService, systemParameterService)
        {
            _unitOfWorkService = unitOfWorkService;
            _approvingPersonService = approvingPersonService;
            _businessTripSettlementService = businessTripSettlementService;
            _userService = userService;
            _systemParameterService = systemParameterService;
            _businessEventPublisher = businessEventPublisher;
            _calculatorResolver = calculatorResolver;
            _businessTripParticipantsService = businessTripParticipantsService;
            _timeService = timeService;
            _entityMergingService = entityMergingService;
            _employeeService = employeeService;
        }

        public override string RequestName => WorkflowResources.BusinessTripSettlementRequestTitle;

        public override RequestType RequestType => RequestType.BusinessTripSettlementRequest;

        public override RequestTypeGroup RequestTypeGroup => RequestTypeGroup.Travel;

        public override SecurityRoleType? RequiredRoleType => SecurityRoleType.CanRequestBusinessTrip;

        public override bool ShouldStartAsDraft => true;

        public override string RejectionNotificationEmailBodyTemplateCode => TemplateCodes.SettlementRequestRejectedMessageBody;

        public override IQueryable<Request> ConfigureIncludes(IQueryable<Request> repository)
        {
            return repository.Include(r => r.BusinessTripSettlementRequest);
        }

        public override IEnumerable<long> GetBusinessWatchersUserIds(RequestDto request, BusinessTripSettlementRequestDto requestDto)
        {
            yield return _businessTripParticipantsService.GetUserIdByParticipantId(requestDto.BusinessTripParticipantId);
        }

        public override void Execute(RequestDto request, BusinessTripSettlementRequestDto requestDto)
        {
            _businessEventPublisher.PublishBusinessEvent(new PaymentApprovedEvent(requestDto));
            _businessEventPublisher.PublishBusinessEvent(new CompensationChangedEvent(
                _businessTripParticipantsService.GetEmployeeIdByParticipantId(requestDto.BusinessTripParticipantId),
                requestDto.SettlementDate.Value.Year,
                (byte)requestDto.SettlementDate.Value.Month));
        }

        public override IEnumerable<ApproverGroupDto> GetApproverGroups(RequestDto request, BusinessTripSettlementRequestDto requestDto)
        {
            if (requestDto.BusinessTripParticipantId == BusinessLogicHelper.NonExistingId)
            {
                yield break;
            }

            if (requestDto.IsEmpty)
            {
                yield break;
            }

            var firstApproverIds = _approvingPersonService
                .GetBusinessTripSettlementApprovers(requestDto.BusinessTripParticipantId);

            if (firstApproverIds.Any())
            {
                yield return new ApproverGroupDto(ApprovalGroupMode.Or, firstApproverIds);
            }

            long employeeId;
            long[] projectIds;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                employeeId = unitOfWork.Repositories.BusinessTripParticipants
                    .Where(p => p.Id == requestDto.BusinessTripParticipantId)
                    .Select(p => p.EmployeeId).Single();

                projectIds = unitOfWork.Repositories.BusinessTrips
                    .Where(t => t.Id == requestDto.BusinessTripId)
                    .SelectMany(t => t.Projects.Select(p => p.Id))
                    .ToArray();
            }

            var secondApproverIds = _approvingPersonService.GetStandardCostApprovers(new[] { employeeId }, projectIds);

            if (secondApproverIds.Any())
            {
                yield return new ApproverGroupDto(
                    ApprovalGroupMode.And,
                    secondApproverIds);
            }
        }

        public override string GetRequestDescription(RequestDto request, BusinessTripSettlementRequestDto data)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var participantName = unitOfWork.Repositories.BusinessTripParticipants
                    .Where(p => p.Id == data.BusinessTripParticipantId)
                    .Select(p => p.Employee)
                    .Select(p => p.FirstName + " " + p.LastName)
                    .Single();

                return string.Format(BusinessTripSettlementRequestResources.RequestDescriptionFormat, data.BusinessTripId, participantName);
            }
        }

        private void StoreRequestCalculations(Request request)
        {
            var settlementRequest = request.BusinessTripSettlementRequest;

            if (settlementRequest == null)
            {
                throw new Exception($"Request Id: {request.Id} has no {nameof(Request.BusinessTripSettlementRequest)}");
            }

            var settlementCalculator = _calculatorResolver.GetCalculatorBySettlementRequestId(settlementRequest.Id);
            var calculator = _calculatorResolver.GetErrorCalculator(settlementCalculator);
            var calculationResult = calculator.Calculate();

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var dbSettlementRequest = unitOfWork.Repositories.BusinessTripSettlementRequests.GetById(settlementRequest.Id);

                // avoids concurrency problems, sets the exact same data on both entities
                settlementRequest.CalculationResult
                    = dbSettlementRequest.CalculationResult
                    = JsonHelper.Serialize(calculationResult);

                unitOfWork.Commit();
            }
        }

        private void InitializeSettlementDate(Request request)
        {
            if (request.Status == RequestStatus.Pending || request.Status == RequestStatus.Draft || request.Status == RequestStatus.Rejected)
            {
                request.BusinessTripSettlementRequest.SettlementDate = _timeService.GetCurrentTime();
            }
        }

        public override void OnAdding(RecordAddingEventArgs<Request, RequestDto, IWorkflowsDbScope> addingEventArgs)
        {
            base.OnAdding(addingEventArgs);

            InitializeSettlementDate(addingEventArgs.InputEntity);
        }

        public override void OnEditing(RecordEditingEventArgs<Request, RequestDto, IWorkflowsDbScope> editingEventArgs)
        {
            base.OnEditing(editingEventArgs);

            _entityMergingService.MergeCollections(editingEventArgs.UnitOfWork,
                        editingEventArgs.DbEntity.BusinessTripSettlementRequest.AbroadTimeEntries,
                        editingEventArgs.InputEntity.BusinessTripSettlementRequest.AbroadTimeEntries,
                        a => a.Id,
                        true
                    );

            _entityMergingService.MergeCollections(editingEventArgs.UnitOfWork,
                        editingEventArgs.DbEntity.BusinessTripSettlementRequest.OwnCosts,
                        editingEventArgs.InputEntity.BusinessTripSettlementRequest.OwnCosts,
                        a => a.Id,
                        true
                    );

            _entityMergingService.MergeCollections(editingEventArgs.UnitOfWork,
                        editingEventArgs.DbEntity.BusinessTripSettlementRequest.CompanyCosts,
                        editingEventArgs.InputEntity.BusinessTripSettlementRequest.CompanyCosts,
                        a => a.Id,
                        true
                    );

            _entityMergingService.MergeCollections(editingEventArgs.UnitOfWork,
                        editingEventArgs.DbEntity.BusinessTripSettlementRequest.Meals,
                        editingEventArgs.InputEntity.BusinessTripSettlementRequest.Meals,
                        a => new { a.BusinessTripSettlementRequestId, a.CountryId },
                        true
                    );

            _entityMergingService.MergeCollections(editingEventArgs.UnitOfWork,
                       editingEventArgs.DbEntity.BusinessTripSettlementRequest.AdditionalAdvancePayments,
                       editingEventArgs.InputEntity.BusinessTripSettlementRequest.AdditionalAdvancePayments,
                       a => a.Id,
                       true
                   );

            #region REFACTORE THIS ASAP PLEASE

            if (editingEventArgs.InputEntity.BusinessTripSettlementRequest.PrivateVehicleMileage == null && editingEventArgs.DbEntity.BusinessTripSettlementRequest.PrivateVehicleMileage != null)
            {
                editingEventArgs.UnitOfWork.Repositories.BusinessTripSettlementRequestVehicleMileages.Delete(editingEventArgs.DbEntity.BusinessTripSettlementRequest.PrivateVehicleMileage);
            }
            else if (editingEventArgs.InputEntity.BusinessTripSettlementRequest.PrivateVehicleMileage != null)
            {
                if (editingEventArgs.DbEntity.BusinessTripSettlementRequest.PrivateVehicleMileage == null)
                {
                    editingEventArgs.DbEntity.BusinessTripSettlementRequest.PrivateVehicleMileage = new BusinessTripSettlementRequestVehicleMileage { };
                }

                editingEventArgs.DbEntity.BusinessTripSettlementRequest.PrivateVehicleMileage.InboundFromCityId = editingEventArgs.InputEntity.BusinessTripSettlementRequest.PrivateVehicleMileage.InboundFromCityId;
                editingEventArgs.DbEntity.BusinessTripSettlementRequest.PrivateVehicleMileage.InboundKilometers = editingEventArgs.InputEntity.BusinessTripSettlementRequest.PrivateVehicleMileage.InboundKilometers;
                editingEventArgs.DbEntity.BusinessTripSettlementRequest.PrivateVehicleMileage.InboundToCityId = editingEventArgs.InputEntity.BusinessTripSettlementRequest.PrivateVehicleMileage.InboundToCityId;
                editingEventArgs.DbEntity.BusinessTripSettlementRequest.PrivateVehicleMileage.IsCarEngineCapacityOver900cc = editingEventArgs.InputEntity.BusinessTripSettlementRequest.PrivateVehicleMileage.IsCarEngineCapacityOver900cc;
                editingEventArgs.DbEntity.BusinessTripSettlementRequest.PrivateVehicleMileage.OutboundFromCityId = editingEventArgs.InputEntity.BusinessTripSettlementRequest.PrivateVehicleMileage.OutboundFromCityId;
                editingEventArgs.DbEntity.BusinessTripSettlementRequest.PrivateVehicleMileage.OutboundKilometers = editingEventArgs.InputEntity.BusinessTripSettlementRequest.PrivateVehicleMileage.OutboundKilometers;
                editingEventArgs.DbEntity.BusinessTripSettlementRequest.PrivateVehicleMileage.OutboundToCityId = editingEventArgs.InputEntity.BusinessTripSettlementRequest.PrivateVehicleMileage.OutboundToCityId;
                editingEventArgs.DbEntity.BusinessTripSettlementRequest.PrivateVehicleMileage.RegistrationNumber = editingEventArgs.InputEntity.BusinessTripSettlementRequest.PrivateVehicleMileage.RegistrationNumber;
                editingEventArgs.DbEntity.BusinessTripSettlementRequest.PrivateVehicleMileage.VehicleType = editingEventArgs.InputEntity.BusinessTripSettlementRequest.PrivateVehicleMileage.VehicleType;
            }

            #endregion

            InitializeSettlementDate(editingEventArgs.DbEntity);
        }

        public override void OnAdded(RecordAddedEventArgs<Request, RequestDto> addedEventArgs)
        {
            StoreRequestCalculations(addedEventArgs.InputEntity);

            base.OnAdded(addedEventArgs);
        }

        public override void OnEdited(RecordEditedEventArgs<Request, RequestDto> editedEventArgs)
        {
            StoreRequestCalculations(editedEventArgs.InputEntity);

            base.OnEdited(editedEventArgs);
        }

        public override IEnumerable<AlertDto> Validate(RequestDto request, BusinessTripSettlementRequestDto requestDto)
        {
            var alerts = new List<AlertDto>(base.Validate(request, requestDto));

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                // check that participant belongs to the business trip (extra security)
                if (!unitOfWork.Repositories.BusinessTripParticipants.Any(
                    p => p.Id == requestDto.BusinessTripParticipantId
                    && p.BusinessTripId == requestDto.BusinessTripId))
                {
                    alerts.Add(AlertDto.CreateError(BusinessTripSettlementRequestResources.ErrorIncorrectParticipantBusinessTrip));
                }

                // V1: check if settlement for participant already created
                if (request.Id == BusinessLogicHelper.NonExistingId && unitOfWork.Repositories.BusinessTripSettlementRequests.Any(s => s.BusinessTripParticipantId == requestDto.BusinessTripParticipantId))
                {
                    alerts.Add(AlertDto.CreateError(BusinessTripSettlementRequestResources.ErrorSettlementAlreadyCreated));
                }

                // If request is empty, avoid further validations
                if (requestDto.IsEmpty)
                {
                    return alerts;
                }

                var businessTrip = unitOfWork.Repositories.BusinessTrips.Single(b => b.Id == requestDto.BusinessTripId);
                var margin = _systemParameterService.GetParameter<int>(ParameterKeys.BusinessTripSettlementRequestDaysMargin);
                var startDateWithMargin = businessTrip.StartedOn.AddDays(-margin).StartOfDay();
                var endDateWithMargin = businessTrip.EndedOn.AddDays(margin).EndOfDay();
                var isDayInBusinessTripRange = DateRangeHelper.IsDateInRange<BusinessTrip>(
                    e => startDateWithMargin, e => endDateWithMargin, DateComparer.LeftClosed | DateComparer.RightClosed);

                var ownCostMargin = _systemParameterService.GetParameter<int>(ParameterKeys.BusinessTripSettlementRequestOwnCostDaysMargin);
                var startDateWithOwnCostMargin = businessTrip.StartedOn.AddDays(-ownCostMargin).StartOfDay();
                var endDateWithOwnCostMargin = businessTrip.EndedOn.AddDays(ownCostMargin).EndOfDay();

                var settings = _businessTripSettlementService.GetSettlementConfiguration(requestDto.BusinessTripParticipantId);
                var isDepartureArrivalRequired = settings.HasFlag(SettlementSectionsFeatureType.DepartureArrival);

                if (isDepartureArrivalRequired)
                {
                    if (!requestDto.ArrivalDateTime.HasValue)
                    {
                        alerts.Add(AlertDto.CreateError(BusinessTripSettlementRequestResources.ArrivalDateIsRequiredMessage));

                        return alerts; // fatal error, further validations depends on this field
                    }

                    if (!requestDto.DepartureDateTime.HasValue)
                    {
                        alerts.Add(AlertDto.CreateError(BusinessTripSettlementRequestResources.DepartureDateIsRequiredMessage));

                        return alerts; // fatal error, further validations depends on this field
                    }
                }

                // departure date
                if (requestDto.DepartureDateTime.HasValue
                    && !isDayInBusinessTripRange.Call(businessTrip, requestDto.DepartureDateTime.Value))
                {
                    alerts.Add(AlertDto.CreateError(
                        string.Format(BusinessTripSettlementRequestResources.ErrorDepartureDateInRange, startDateWithMargin, endDateWithMargin)));

                    return alerts; // fatal error, further validation makes no sense (i.e. invalid ranges)
                }

                // arrival date
                if (requestDto.ArrivalDateTime.HasValue
                    && !isDayInBusinessTripRange.Call(businessTrip, requestDto.ArrivalDateTime.Value))
                {
                    alerts.Add(AlertDto.CreateError(
                        string.Format(BusinessTripSettlementRequestResources.ErrorArrivalDateInRange, startDateWithMargin, endDateWithMargin)));
                }

                // departure date before arrival date
                if (requestDto.DepartureDateTime.HasValue && requestDto.ArrivalDateTime.HasValue
                    && requestDto.DepartureDateTime >= requestDto.ArrivalDateTime)
                {
                    alerts.Add(AlertDto.CreateError(BusinessTripSettlementRequestResources.ErrorDepartureAfterArrival));
                }

                var isTimeEntryInSettlementRange = DateRangeHelper.IsDateInRange<BusinessTripSettlementRequestDto>(
                    e => (e.DepartureDateTime ?? businessTrip.StartedOn.StartOfDay()),
                    e => (e.ArrivalDateTime ?? businessTrip.EndedOn.EndOfDay()),
                    DateComparer.LeftClosed | DateComparer.RightClosed);

                // abroad time entries in settlement range
                if (requestDto.AbroadTimeEntries.Any(e => !isTimeEntryInSettlementRange.Call(requestDto, e.DepartureDateTime)))
                {
                    alerts.Add(AlertDto.CreateError(string.Format(BusinessTripSettlementRequestResources.ErrorAbroadTimeEntriesInRange,
                        requestDto.DepartureDateTime.Value, requestDto.ArrivalDateTime.Value)));
                }

                var participantLocation = unitOfWork.Repositories.BusinessTripParticipants
                    .SelectById(requestDto.BusinessTripParticipantId, p => p.Employee.Location);
                var lastAbroadTimeEntryCountry = requestDto.AbroadTimeEntries
                    .OrderByDescending(e => e.DepartureDateTime)
                    .Select(e => (long?)e.DepartureCountryId)
                    .FirstOrDefault();

                // because only intive GmbH employees have to specify destination type
                var isIntiveGmbhEmployee =
                    _businessTripParticipantsService.ShouldParticipantSpecifyDestinationType(requestDto.BusinessTripParticipantId);

                if (!isIntiveGmbhEmployee)
                {
                    // proper country as last abroad time entry (only for non-DE)
                    if (lastAbroadTimeEntryCountry.HasValue && lastAbroadTimeEntryCountry != LocationBusinessLogic.CountryId.Call(participantLocation))
                    {
                        alerts.Add(AlertDto.CreateError(string.Format(BusinessTripSettlementRequestResources.ErrorLastTimeEntryCountry,
                            LocationBusinessLogic.Country.Call(participantLocation)?.Name)));
                    }
                }

                var isDayInSettlementRange = DateRangeHelper.IsDateInRange<BusinessTripSettlementRequestDto>(
                     e => (e.DepartureDateTime ?? businessTrip.StartedOn).StartOfDay(),
                     e => (e.ArrivalDateTime ?? businessTrip.EndedOn).EndOfDay(),
                     DateComparer.LeftClosed | DateComparer.RightClosed);

                var isDayInSettlementRangeForOwnCosts = DateRangeHelper.GetIsDateInRangeBusinessLogic(startDateWithOwnCostMargin, endDateWithOwnCostMargin);

                // company costs
                if (requestDto.CompanyCosts.Where(c => !c.HasInvoiceIssued).Any(c => !isDayInSettlementRange.Call(requestDto, c.TransactionDate)))
                {
                    alerts.Add(AlertDto.CreateError(string.Format(BusinessTripSettlementRequestResources.ErrorCompanyCostsInRange,
                        (requestDto.DepartureDateTime ?? businessTrip.StartedOn).StartOfDay(),
                        (requestDto.ArrivalDateTime ?? businessTrip.EndedOn).EndOfDay())));
                }

                // own costs
                if (requestDto.OwnCosts.Where(c => !c.HasInvoiceIssued).Any(c => !isDayInSettlementRangeForOwnCosts.Call(c.TransactionDate)))
                {
                    alerts.Add(AlertDto.CreateError(string.Format(BusinessTripSettlementRequestResources.ErrorOwnCostsInRange,
                        startDateWithOwnCostMargin, endDateWithOwnCostMargin)));
                }

                // invoice issuance dates
                var invoiceIssuanceDates = requestDto.OwnCosts.Where(c => c.HasInvoiceIssued).Select(c => c.TransactionDate)
                    .Concat(requestDto.CompanyCosts.Where(c => c.HasInvoiceIssued).Select(c => c.TransactionDate))
                    .ToArray();

                if (!invoiceIssuanceDates.IsNullOrEmpty())
                {
                    if (isIntiveGmbhEmployee && !request.Documents.Any())
                    {
                        alerts.Add(AlertDto.CreateError(BusinessTripSettlementRequestResources.ErrorDocsRequired));
                    }

                    var alert = ValidateInvoiceIssuanceDates(invoiceIssuanceDates, businessTrip);

                    if (alert != null)
                    {
                        alerts.Add(alert);
                    }
                }

                var validMeals = _businessTripSettlementService.GetMeals(requestDto);

                // secure meals countries
                var mealsCountries = validMeals
                    .Where(m => m.CountryId.HasValue)
                    .Select(m => m.CountryId.Value);

                requestDto.Meals = requestDto.Meals
                    .Where(m => !m.CountryId.HasValue || mealsCountries.Contains(m.CountryId.Value))
                    .ToList();

                // secure meals days
                var mealDays = validMeals
                    .Where(m => m.Day.HasValue)
                    .Select(m => m.Day.Value);

                requestDto.Meals = requestDto.Meals
                    .Where(m => !m.Day.HasValue || mealDays.Contains(m.Day.Value))
                    .ToList();
            }

            return alerts;
        }

        public override bool CanBeDeleted(RequestDto requestDto, long byUserId)
        {
            if (_userService.GetUserRolesById(byUserId).Contains(SecurityRoleType.CanForceDeleteAllBusinessTripSettlementRequests))
            {
                return true;
            }

            var settlementRequestDto = (BusinessTripSettlementRequestDto)requestDto.RequestObject;

            if (settlementRequestDto.IsEmpty)
            {
                return false;
            }

            return base.CanBeDeleted(requestDto, byUserId);
        }

        public override bool CanBeEdited(RequestDto requestDto, long byUserId)
        {
            var settlementRequestDto = (BusinessTripSettlementRequestDto)requestDto.RequestObject;

            if (settlementRequestDto.IsEmpty)
            {
                return false;
            }

            if (requestDto.Status == RequestStatus.Rejected || requestDto.Status == RequestStatus.Draft)
            {
                var currentPrincipal = PrincipalProvider.Current;

                var canManageOwnBusinessTripSettlements = currentPrincipal.IsInRole(SecurityRoleType.CanManageOwnBusinessTripSettlements);
                var canManageOthersBusinessTripSettlements = currentPrincipal.IsInRole(SecurityRoleType.CanManageOthersBusinessTripSettlements);

                if ((canManageOwnBusinessTripSettlements && requestDto.RequestingUserId == byUserId) || canManageOthersBusinessTripSettlements)
                {
                    return true;
                }
            }

            return base.CanBeEdited(requestDto, byUserId);
        }

        public override bool CanBeSubmitted(RequestDto requestDto, long byUserId)
        {
            if (requestDto.Status != RequestStatus.Draft)
            {
                return false;
            }

            var currentPrincipal = PrincipalProvider.Current;
            var canManageOthersBusinessTripSettlements = currentPrincipal.IsInRole(SecurityRoleType.CanManageOthersBusinessTripSettlements);

            if (canManageOthersBusinessTripSettlements)
            {
                return true;
            }

            var settlementRequestDto = (BusinessTripSettlementRequestDto)requestDto.RequestObject;

            if (settlementRequestDto.BusinessTripParticipantId != BusinessLogicHelper.NonExistingId)
            {
                var participantUserId = _businessTripParticipantsService.GetUserIdByParticipantId(settlementRequestDto.BusinessTripParticipantId);

                if (participantUserId == byUserId)
                {
                    return true;
                }
            }

            return base.CanBeSubmitted(requestDto, byUserId);
        }

        public override BusinessTripSettlementRequestDto GetDraft(IDictionary<string, object> parameters)
        {
            const string participantIdName = "participant-id";
            const string originalRequestIdName = "base-on";

            var newRequest = base.GetDraft(parameters);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                if (parameters.ContainsKey(participantIdName))
                {
                    var participantId = long.Parse(parameters[participantIdName].ToString());

                    var participant = unitOfWork.Repositories.BusinessTripParticipants
                        .Include(p => p.BusinessTrip)
                        .Include(p => p.BusinessTrip.DestinationCity)
                        .Include(p => p.DepartureCity)
                        .Include(p => p.Employee.Company.DefaultAdvancedPaymentCurrency)
                        .Filtered()
                        .GetById(participantId);

                    var settings = _businessTripSettlementService.GetSettlementConfiguration(participantId);

                    newRequest.BusinessTripParticipantId = participant.Id;
                    newRequest.BusinessTripId = participant.BusinessTripId;

                    var defaultCurrency = participant.Employee.Company?.DefaultAdvancedPaymentCurrency;
                    newRequest.DefaultCurrencyId = defaultCurrency?.Id;
                    newRequest.DefaultCurrencyName = defaultCurrency?.DisplayName;

                    newRequest.DefaultCardHolderEmployeeId = participant.Employee?.Id;
                    newRequest.DefaultCardHolderEmployeeName = participant.Employee?.DisplayName;

                    if (settings.HasFlag(SettlementSectionsFeatureType.DepartureArrival))
                    {
                        newRequest.DepartureDateTime = participant.StartedOn;
                        newRequest.ArrivalDateTime = participant.EndedOn;

                        if (_businessTripParticipantsService.ShouldParticipantSpecifyDestinationType(participantId))
                        {
                            newRequest.AbroadTimeEntries = new[]
                            {
                                new BusinessTripSettlementRequestAbroadTimeEntryDto
                                {
                                    DepartureCountryId = participant.BusinessTrip.DestinationCity.CountryId,
                                    DepartureCityId = participant.BusinessTrip.DestinationCityId,
                                    DepartureDateTime = participant.StartedOn
                                }
                            };
                        }
                        else if (participant.DepartureCity.CountryId != participant.BusinessTrip.DestinationCity.CountryId)
                        {
                            newRequest.AbroadTimeEntries = new[]
                            {
                                new BusinessTripSettlementRequestAbroadTimeEntryDto
                                {
                                    DepartureCountryId = participant.BusinessTrip.DestinationCity.CountryId,
                                    DepartureCityId = participant.BusinessTrip.DestinationCityId,
                                    DepartureDateTime = participant.StartedOn
                                },
                                new BusinessTripSettlementRequestAbroadTimeEntryDto
                                {
                                    DepartureCountryId = participant.DepartureCity.CountryId,
                                    DepartureCityId = participant.DepartureCityId,
                                    DepartureDateTime = participant.EndedOn
                                }
                            };
                        }
                    }

                    if (settings.HasFlag(SettlementSectionsFeatureType.Meals))
                    {
                        newRequest.Meals = _businessTripSettlementService.GetMeals(newRequest).ToArray();
                    }

                    if (parameters.ContainsKey(originalRequestIdName))
                    {
                        var baseSettlementRequestId = long.Parse(parameters[originalRequestIdName].ToString());

                        var mealsClassMapping = ClassMappingFactory.CreateMapping<BusinessTripSettlementRequestMeal, BusinessTripSettlementRequestMealDto>();
                        var abroadTimeClassMapping = ClassMappingFactory.CreateMapping<BusinessTripSettlementRequestAbroadTimeEntry, BusinessTripSettlementRequestAbroadTimeEntryDto>();

                        var baseSettlementRequest = unitOfWork
                            .Repositories
                            .BusinessTripSettlementRequests
                            .GetById(baseSettlementRequestId);

                        if (newRequest.BusinessTripId != BusinessLogicHelper.NonExistingId
                            && newRequest.BusinessTripId != baseSettlementRequest.BusinessTripParticipant.BusinessTripId)
                        {
                            throw new SecurityException("Participant and SettlementRequest don't belong to the same BusinessTrip");
                        }

                        newRequest.BusinessTripId = baseSettlementRequest.BusinessTripParticipant.BusinessTripId;

                        if (settings.HasFlag(SettlementSectionsFeatureType.DepartureArrival))
                        {
                            newRequest.DepartureDateTime = baseSettlementRequest.DepartureDateTime;
                            newRequest.ArrivalDateTime = baseSettlementRequest.ArrivalDateTime;
                        }

                        newRequest.AbroadTimeEntries = baseSettlementRequest.AbroadTimeEntries.Select(abroadTimeClassMapping.CreateFromSource).ToList();
                    }
                }

                return newRequest;
            }
        }

        public override BoolResult OnDeleting(Request request, RequestDto requestDto, IUnitOfWork<IWorkflowsDbScope> unitOfWork)
        {
            unitOfWork.Repositories.BusinessTripSettlementRequestMeals
                .DeleteEach(m => m.Request.Request.Id == request.Id);

            unitOfWork.Repositories.BusinessTripSettlemenRequestAbroadTimeEntries
                 .DeleteEach(m => m.Request.Request.Id == request.Id);

            unitOfWork.Repositories.BusinessTripSettlementRequestOwnCosts
                 .DeleteEach(m => m.Request.Request.Id == request.Id);

            unitOfWork.Repositories.BusinessTripSettlementRequestCompanyCosts
                 .DeleteEach(m => m.Request.Request.Id == request.Id);

            unitOfWork.Repositories.BusinessTripSettlementRequestAdvancePayments
                .DeleteEach(m => m.Request.Request.Id == request.Id);

            unitOfWork.Repositories.BusinessTripSettlementRequestVehicleMileages
                .DeleteEach(m => m.Request.Request.Id == request.Id);

            return base.OnDeleting(request, requestDto, unitOfWork);
        }

        private AlertDto ValidateInvoiceIssuanceDates(IEnumerable<DateTime> invoiceIssuanceDates, BusinessTrip businessTrip)
        {
            var invoiceMargin = _systemParameterService.GetParameter<int>(ParameterKeys.BusinessTripSettlementRequestInvoiceDaysMargin);
            var isDayInRange = DateRangeHelper.IsDateInRange<BusinessTrip>(
                e => e.StartedOn.AddDays(-invoiceMargin).StartOfDay(), e => e.EndedOn.AddDays(invoiceMargin).EndOfDay(),
                DateComparer.LeftClosed | DateComparer.RightClosed);

            if (invoiceIssuanceDates.Any(d => !isDayInRange.Call(businessTrip, d)))
            {
                var startDateWithMargin = businessTrip.StartedOn.AddDays(-invoiceMargin).StartOfDay();
                var endDateWithMargin = businessTrip.EndedOn.AddDays(invoiceMargin).EndOfDay();

                return AlertDto.CreateError(string.Format(
                    BusinessTripSettlementRequestResources.ErrorInvoiceIssuanceDateNotInRange, startDateWithMargin, endDateWithMargin));
            }

            return null;
        }

        public override string PendingApprovalNotificationAdditionalContent(RequestDto request)
        {
            var baseUrl = _systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);
            var url = baseUrl + "BusinessTrips/BusinessTrip/List";

            return $"<a href=\"{url}\">{BusinessTripRequestResources.PendingApprovalAdditionalContentLink}</a>";
        }
    }
}
