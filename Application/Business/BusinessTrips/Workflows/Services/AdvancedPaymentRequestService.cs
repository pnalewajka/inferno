using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.Business.BusinessTrips.Resources;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.Business.Workflows.Services;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.BusinessTrips.Workflows.Services
{
    internal class AdvancedPaymentRequestService : RequestServiceBase<AdvancedPaymentRequest, AdvancedPaymentRequestDto>
    {
        private readonly ICurrencyService _currencyService;
        private readonly IReadOnlyUnitOfWorkService<IBusinessTripsDbScope> _unitOfWorkService;
        private readonly IApprovingPersonService _approvingPersonService;
        private readonly IBusinessTripService _businessTripService;

        public AdvancedPaymentRequestService(
            IActionSetService actionSetService, 
            IPrincipalProvider principalProvider,
            IClassMappingFactory classMappingFactory,
            IWorkflowNotificationService workflowNotificationService,
            ICurrencyService currencyService,
            IReadOnlyUnitOfWorkService<IBusinessTripsDbScope> unitOfWorkService,
            IApprovingPersonService approvingPersonService,
            IBusinessTripService businessTripService,
            IRequestWorkflowService requestWorkflowService,
            ISystemParameterService systemParameterService)
            : base(actionSetService,
                  principalProvider,
                  classMappingFactory,
                  workflowNotificationService,
                  requestWorkflowService,
                  systemParameterService)
        {
            _currencyService = currencyService;
            _unitOfWorkService = unitOfWorkService;
            _approvingPersonService = approvingPersonService;
            _businessTripService = businessTripService;
        }

        public override RequestType RequestType => RequestType.AdvancedPaymentRequest;
        public override string RequestName => WorkflowResources.AdvancedPaymentRequestName;

        public override RequestTypeGroup RequestTypeGroup => RequestTypeGroup.Travel;

        public override SecurityRoleType? RequiredRoleType => SecurityRoleType.CanRequestAdvancedPaymentRequest;

        public override AdvancedPaymentRequestDto GetDraft(IDictionary<string, object> parameters)
        {
            var draft = base.GetDraft(parameters);

            string businessTripIdPropertyName = NamingConventionHelper.ConvertPascalCaseToHyphenated(nameof(AdvancedPaymentRequestDto.BusinessTripId));

            if (parameters.ContainsKey(businessTripIdPropertyName))
            {
                draft.BusinessTripId = int.Parse(parameters[businessTripIdPropertyName] as string);
            }

            return draft;
        }

        public override void Execute(RequestDto request, AdvancedPaymentRequestDto requestDto)
        {
        }

        public override IEnumerable<long> GetBusinessWatchersUserIds(RequestDto request, AdvancedPaymentRequestDto requestDto)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var participantUserId = unitOfWork.Repositories.BusinessTripParticipants
                    .First(p => p.Id == requestDto.ParticipantId).Employee.UserId;

                return request.RequestingUserId != participantUserId
                    ? new List<long>() {participantUserId.Value}
                    : Enumerable.Empty<long>();
            }
        }

        public override string GetRequestDescription(RequestDto request, AdvancedPaymentRequestDto data)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return string.Format(
                    BusinessTripRequestResources.AdvancedPaymentDescriptionFormat,
                    data.Amount,
                    data.CurrencyDisplayName,
                    data.BusinessTripId,
                    unitOfWork.Repositories.BusinessTripParticipants
                        .First(p => p.Id == data.ParticipantId).Employee
                        .DisplayName);
            }
        }

        public override IEnumerable<ApproverGroupDto> GetApproverGroups(RequestDto request, AdvancedPaymentRequestDto requestDto)
        {
            if (requestDto.ParticipantId == default(long))
            {
                yield break;
            }

            if (requestDto.BusinessTripId != default(long))
            {
                long employeeId;
                long[] projectIds;

                using (var unitOfWork = _unitOfWorkService.Create())
                {
                    employeeId = unitOfWork.Repositories.BusinessTripParticipants
                        .Where(p => p.Id == requestDto.ParticipantId)
                        .Select(p => p.EmployeeId).Single();

                    projectIds = unitOfWork.Repositories.BusinessTrips
                        .Where(t => t.Id == requestDto.BusinessTripId)
                        .SelectMany(t => t.Projects.Select(p => p.Id))
                        .ToArray();
                }

                var standardApprovers = _approvingPersonService.GetStandardCostApprovers(new[] { employeeId }, projectIds);
                
                yield return new ApproverGroupDto(ApprovalGroupMode.And, standardApprovers);
            }

            var approver = _approvingPersonService.GetAdvancePaymentRequestApprover(requestDto.ParticipantId);

            if (approver.HasValue)
            {
                yield return new ApproverGroupDto(ApprovalGroupMode.Or, new[] {approver.Value});
            }
        }

        public override IQueryable<Request> ConfigureIncludes(IQueryable<Request> repository)
        {
            return repository.Include(r => r.AdvancedPaymentRequest);
        }

        public override IEnumerable<AlertDto> Validate(RequestDto request, AdvancedPaymentRequestDto requestDto)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                // check that participant belongs to the business trip (extra security)
                if (!unitOfWork.Repositories.BusinessTripParticipants.Any(
                    p => p.Id == requestDto.ParticipantId
                    && p.BusinessTripId == requestDto.BusinessTripId))
                {
                    yield return AlertDto.CreateError(BusinessTripSettlementRequestResources.ErrorIncorrectParticipantBusinessTrip);
                }
            }

            var allowedCurrencies = _currencyService.GetAllowedAdvancedPaymentCurrenciesByBtParticipantId(requestDto.ParticipantId);
            var currentCurrency = allowedCurrencies.SingleOrDefault(c => c.Currency.Id == requestDto.CurrencyId);

            if (currentCurrency == null)
            {
                yield return AlertDto.CreateError(BusinessTripAdvancedPaymentRequestResources.NotAllowedCurrency);
            }
            else if (requestDto.Amount < currentCurrency.MinimumValue)
            {
                yield return AlertDto.CreateError(string.Format(
                    BusinessTripAdvancedPaymentRequestResources.MinimumNotReached, currentCurrency.MinimumValue));
            }
        }        
    }
}