﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Castle.Core.Internal;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.BusinessTrips.BusinessLogics;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.Business.BusinessTrips.Resources;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.Business.Workflows.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.BusinessTrips.Workflows.Services
{
    internal class BusinessTripRequestService : RequestServiceBase<BusinessTripRequest, BusinessTripRequestDto>
    {
        private readonly ICityService _cityService;
        private readonly ITimeService _timeService;
        private readonly IUserService _userService;
        private readonly IEmployeeService _employeeService;
        private readonly IBusinessTripService _businessTripService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IUnitOfWorkService<IBusinessTripsDbScope> _unitOfWorkService;
        private readonly IApprovingPersonService _approvingPersonService;
        private readonly ICompanyService _companyService;

        public BusinessTripRequestService(
            ICityService cityService,
            ITimeService timeService,
            IUserService userService,
            IEmployeeService employeeService,
            IActionSetService actionSetService,
            IBusinessTripService businessTripService,
            ISystemParameterService systemParameterService,
            IWorkflowNotificationService workflowNotificationService,
            IPrincipalProvider principalProvider,
            IUnitOfWorkService<IBusinessTripsDbScope> unitOfWorkService,
            IApprovingPersonService approvingPersonService,
            IRequestWorkflowService requestWorkflowService,
            IClassMappingFactory classMappingFactory,
            ICompanyService companyService)
            : base(actionSetService, principalProvider, classMappingFactory, workflowNotificationService, requestWorkflowService, systemParameterService)
        {
            _cityService = cityService;
            _timeService = timeService;
            _userService = userService;
            _employeeService = employeeService;
            _businessTripService = businessTripService;
            _systemParameterService = systemParameterService;
            _unitOfWorkService = unitOfWorkService;
            _approvingPersonService = approvingPersonService;
            _companyService = companyService;
        }

        public override RequestType RequestType => RequestType.BusinessTripRequest;

        public override string RequestName => WorkflowResources.BusinessTripRequest;

        public override RequestTypeGroup RequestTypeGroup => RequestTypeGroup.Travel;

        public override SecurityRoleType? RequiredRoleType => SecurityRoleType.CanRequestBusinessTrip;

        public override IEnumerable<long> GetBusinessWatchersUserIds(RequestDto request, BusinessTripRequestDto requestDto)
        {
            return _employeeService.GetUserIdsByEmployeeIds(requestDto.ParticipantEmployeeIds);
        }

        public override EmailRecipientDto[] GetExtraWorkflowNotificationRecipients(RequestDto request, BusinessTripRequestDto requestDto)
        {
            return _employeeService.GetEmployeesById(requestDto.ParticipantEmployeeIds)
                .Select(e => new EmailRecipientDto
                {
                    FullName = e.FullName,
                    EmailAddress = e.Email,
                    Type = RecipientType.Cc
                }).ToArray();
        }

        public override bool CanBeDeleted(RequestDto requestDto, long byUserId)
        {
            if (_userService.GetUserRolesById(byUserId).Contains(SecurityRoleType.CanForceDeleteAllBusinessTripRequests))
            {
                return true;
            }

            if (requestDto.RequestingUserId == byUserId)
            {
                using (var unitOfWork = _unitOfWorkService.Create())
                {
                    var businessTripRequest = unitOfWork.Repositories.BusinessTripRequests.GetByIdOrDefault(requestDto.Id);
                    var businessTrip = businessTripRequest?.BusinessTrip;

                    if (businessTrip != null)
                    {
                        var currentDate = _timeService.GetCurrentDate();
                        var status = BusinessTripBusinessLogic.GetStatus.Call(businessTrip, currentDate);

                        return status == BusinessTripStatus.ArrangementInProgress;
                    }
                }
            }

            return false;
        }

        public override IQueryable<Request> ConfigureIncludes(IQueryable<Request> repository)
        {
            return repository.Include(r => r.BusinessTripRequest);
        }

        public override IEnumerable<ApproverGroupDto> GetApproverGroups(
            RequestDto request,
            BusinessTripRequestDto requestData)
        {
            var approvers = _approvingPersonService.GetStandardCostApprovers(requestData.ParticipantEmployeeIds, requestData.ProjectIds);

            if (approvers.Any())
            {
                yield return new ApproverGroupDto(ApprovalGroupMode.And, approvers);
            }
        }

        public override BusinessTripRequestDto GetDraft(IDictionary<string, object> parameters)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var currentDate = _timeService.GetCurrentDate();

                var currentEmployee =
                    unitOfWork.Repositories.Employees.GetById(PrincipalProvider.Current.EmployeeId);

                var projectIds = currentEmployee.AllocationRequests
                    .Where(ar => ar.StartDate <= currentDate && (!ar.EndDate.HasValue || ar.EndDate >= currentDate))
                    .Select(ar => ar.ProjectId)
                    .ToArray();

                long? suggestedProjectId = null;

                // if user is allocated to exactly 1 project, we suggest it
                if (projectIds?.Length == 1)
                {
                    suggestedProjectId = projectIds.First();
                }
                else
                {
                    var lastBusinessTrip = unitOfWork.Repositories.BusinessTripRequests
                        .Where(r => r.Request.RequestingUser.Employees.FirstOrDefault().Id == currentEmployee.Id)
                        .OrderByDescending(r => r.StartedOn)
                        .Select(r => new { Request = r, ProjectCount = r.Projects.Count(), ProjectId = r.Projects.Select(p => p.Id).FirstOrDefault() })
                        .FirstOrDefault();

                    if (lastBusinessTrip?.ProjectCount == 1)
                    {
                        suggestedProjectId = lastBusinessTrip.ProjectId;
                    }
                }

                var draft = new BusinessTripRequestDto
                {
                    ProjectIds = suggestedProjectId != null ? new[] { suggestedProjectId.Value } : Array.Empty<long>(),
                    ParticipantEmployeeIds = new[] { currentEmployee.Id },
                    DepartureCityIds = currentEmployee.Location != null && currentEmployee.Location.CityId.HasValue
                        ? new[] { currentEmployee.Location.CityId.Value }
                        : Array.Empty<long>(),
                    AreTravelArrangementsNeeded = true,
                    LuggageDeclarations = currentEmployee == null
                        ? Array.Empty<DeclaredLuggageDto>()
                        : new[] { new DeclaredLuggageDto { EmployeeId = currentEmployee.Id } }
                };

                RemoveDefaultProjectIfMissingData(draft);

                return draft;
            }
        }

        private void RemoveDefaultProjectIfMissingData(BusinessTripRequestDto dto)
        {
            if (dto.ProjectIds.Any())
            {
                try
                {
                    GetApproverGroups(null, dto).ToArray();
                }
                catch (BusinessException)
                {
                    dto.ProjectIds = new long[0];
                }
            }
        }

        public override string GetRequestDescription(RequestDto request, BusinessTripRequestDto requestDto)
        {
            var destinationCity = _cityService.GetCityOrDefaultById(requestDto.DestinationCityId);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var departureCities = unitOfWork.Repositories.Cities.GetByIds(requestDto.DepartureCityIds)
                    .GroupBy(c => c.Country.Name);
                var departureCitiesTextAll = new List<string>();

                foreach (var cities in departureCities)
                {
                    departureCitiesTextAll.Add($"{string.Join(", ", cities.Select(c => c.Name))} ({cities.Key})");
                }

                return string.Format(BusinessTripRequestResources.RequestDescriptionFormat,
                    request.Id != default(long) ? $"[BT-{request.Id}] " : string.Empty,
                    $"{string.Join(", ", departureCitiesTextAll)} ({requestDto.StartedOn:d})",
                    $"{destinationCity.Name} ({destinationCity.CountryName}) ({requestDto.EndedOn:d})");
            }
        }

        public override void Execute(RequestDto request, BusinessTripRequestDto requestDto)
        {
            var requestingEmployeeId = _employeeService.GetEmployeeIdByUserId(request.RequestingUserId);
            var vouchersEmployeeId = requestDto.ParticipantEmployeeIds.Any(id => id == requestingEmployeeId)
                ? requestingEmployeeId.Value
                : requestDto.ParticipantEmployeeIds.First();

            _businessTripService.CreateBusinessTripFromRequest(requestDto, vouchersEmployeeId);
        }

        public override IEnumerable<AlertDto> Validate(RequestDto request, BusinessTripRequestDto requestDto)
        {
            var alerts = new List<AlertDto>(base.Validate(request, requestDto));
            var maximumDaysInPast = _systemParameterService.GetParameter<int>(ParameterKeys.BusinessTripsMaximumDaysInPast);

            if (requestDto.StartedOn.AddDays(maximumDaysInPast) < _timeService.GetCurrentDate())
            {
                alerts.Add(new AlertDto
                {
                    Message = string.Format(BusinessTripRequestResources.MaximumDaysInPastExceededError, maximumDaysInPast),
                    Type = AlertType.Error
                });
            }

            var terminatedParticipants = _employeeService.GetEmployees(e =>
                requestDto.ParticipantEmployeeIds.Contains(e.Id) && e.CurrentEmployeeStatus == EmployeeStatus.Terminated);

            if (terminatedParticipants.Any())
            {
                var terminatedParticipantNames = string.Join(", ", terminatedParticipants.Select(p => p.FullName));

                alerts.Add(new AlertDto
                {
                    Message = string.Format(BusinessTripRequestResources.TerminatedParticipantsError, terminatedParticipantNames),
                    Type = AlertType.Error
                });
            }

            if (requestDto.AccommodationPreferences.HasFlag(AccommodationType.LumpSum) && requestDto.AccommodationPreferences != AccommodationType.LumpSum)
            {
                alerts.Add(new AlertDto
                {
                    Message = BusinessTripRequestResources.NotOnlyLumpSumError,
                    Type = AlertType.Error
                });
            }

            var destinationCity = _cityService.GetCityOrDefaultById(requestDto.DestinationCityId);

            if (requestDto.AccommodationPreferences.HasFlag(AccommodationType.CompanyApartment) && !destinationCity.IsCompanyApartmentAvailable)
            {
                alerts.Add(new AlertDto
                {
                    Message = string.Format(BusinessTripRequestResources.CompanyApartmentUnavailableError, destinationCity.Name),
                    Type = AlertType.Error
                });
            }

            if (requestDto.TransportationPreferences.HasFlag(MeansOfTransport.Taxi)
                && requestDto.DepartureCityTaxiVouchers > 0
                && _cityService.GetCitiesByIds(requestDto.DepartureCityIds).All(c => !c.IsVoucherServiceAvailable))
            {
                alerts.Add(new AlertDto
                {
                    Message = string.Format(BusinessTripRequestResources.VoucherServiceUnavailableError, BusinessTripRequestResources.DepartureCities),
                    Type = AlertType.Error
                });
            }

            if (requestDto.TransportationPreferences.HasFlag(MeansOfTransport.Taxi)
                && requestDto.DestinationCityTaxiVouchers > 0
                && !destinationCity.IsVoucherServiceAvailable)
            {
                alerts.Add(new AlertDto
                {
                    Message = string.Format(BusinessTripRequestResources.VoucherServiceUnavailableError, destinationCity.Name),
                    Type = AlertType.Error
                });
            }

            if (requestDto.DepartureCityIds.Count() > requestDto.ParticipantEmployeeIds.Count())
            {
                alerts.Add(new AlertDto
                {
                    Message = BusinessTripRequestResources.MaximumDepartureCitiesExeededError,
                    Type = AlertType.Error
                });
            }

            if (requestDto.DepartureCityIds.IsNullOrEmpty())
            {
                alerts.Add(new AlertDto
                {
                    Message = BusinessTripRequestResources.DepartureCitiesAreRequiredError,
                    Type = AlertType.Error
                });
            }

            if (!requestDto.ParticipantEmployeeIds.Contains(PrincipalProvider.Current.EmployeeId)
                && !PrincipalProvider.Current.IsInRole(SecurityRoleType.CanRequestBusinessTripOnBehalf))
            {
                alerts.Add(new AlertDto
                {
                    Message = BusinessTripRequestResources.UnauthorizedRequestOnBehalfError,
                    Type = AlertType.Error
                });
            }

            alerts.AddRange(
                _businessTripService.ValidateParticipantEmployeesEmploymentPeriods(
                    requestDto.StartedOn, requestDto.EndedOn, requestDto.ParticipantEmployeeIds));

            alerts.AddRange(ValidateByCompanyData(requestDto));

            return alerts;
        }

        public override BoolResult OnDeleting(Request request, RequestDto requestDto, IUnitOfWork<IWorkflowsDbScope> unitOfWork)
        {
            // remove general request-related luggages
            unitOfWork.Repositories.DeclaredLuggages.DeleteEach(l => l.RequestId == request.Id);

            // it's crucial to do it here before querying db for business trip entity
            unitOfWork.Repositories.BusinessTripSettlementRequests
                .Where(r => r.BusinessTripParticipant.BusinessTrip.Request.Id == request.Id)
                .Select(r => r.Id)
                .ToList()
                .ForEach(id => RequestWorkflowService.Delete(id));

            // remove business trip related entities
            var businessTrip = unitOfWork.Repositories.BusinessTrips.SingleOrDefault(b => b.Request.Id == request.Id);

            if (businessTrip != null)
            {
                foreach (var participant in businessTrip.Participants.ToList())
                {
                    participant.AdvancedPaymentRequests.ToList().ForEach(r => RequestWorkflowService.Delete(r.Request.Id));

                    unitOfWork.Repositories.DeclaredLuggages.DeleteRange(participant.LuggageDeclarations);
                    unitOfWork.Repositories.ArrangementTrackingEntries.DeleteRange(participant.ArrangementTrackingEntries);
                    unitOfWork.Repositories.Arrangements.DeleteRange(participant.Arrangements);

                    unitOfWork.Repositories.BusinessTripParticipants.Delete(participant);
                }

                unitOfWork.Repositories.Arrangements.DeleteEach(a => a.BusinessTripId == businessTrip.Id);
                unitOfWork.Repositories.BusinessTripAcceptanceConditions.DeleteEach(c => c.BusinessTrip.Id == businessTrip.Id);
                unitOfWork.Repositories.BusinessTripMessage.DeleteEach(m => m.BusinessTripId == businessTrip.Id);

                unitOfWork.Repositories.BusinessTrips.Delete(businessTrip);
            }

            return base.OnDeleting(request, requestDto, unitOfWork);
        }

        public override string ApprovalNotificationAdditionalContent(RequestDto request)
        {
            var baseUri = new Uri(_systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress));
            var businessTripListUrl = new Uri(baseUri, "BusinessTrips/BusinessTrip/List");

            return $"<a href='{businessTripListUrl}'>Click here</a> to open your business trip list and track the arrangement progress. "
                + "You will be notified by email once the arrangement is completed.";
        }

        private IEnumerable<AlertDto> ValidateByCompanyData(BusinessTripRequestDto businessTripRequestDto)
        {
            var alerts = new List<AlertDto>();
            var companies = businessTripRequestDto.ParticipantEmployeeIds
                .Select(employeeId => _companyService.GetCompanyOrDefaultByEmployeeId(employeeId)).DistinctBy(c => c.Id).ToList();

            if (companies.IsNullOrEmpty())
            {
                return alerts;
            }

            foreach (var company in companies)
            {
                var wrongMeansOfTransportation = new List<string>();

                foreach (var meanOfTransport in EnumHelper.GetFlags(businessTripRequestDto.TransportationPreferences))
                {
                    if (!company.AllowedMeansOfTransport.HasFlag(meanOfTransport))
                    {
                        wrongMeansOfTransportation.Add(meanOfTransport.GetDescription());
                    }
                }

                if (!wrongMeansOfTransportation.IsNullOrEmpty())
                {
                    alerts.Add(new AlertDto()
                    {
                        Message = string.Format(BusinessTripRequestResources.WrongTransportationPreferences,
                            company.Name, string.Join<string>(", ", wrongMeansOfTransportation)),
                        Type = AlertType.Error
                    });
                }

                var wrongAccomodations = new List<string>();

                foreach (var accomodation in EnumHelper.GetFlags(businessTripRequestDto.AccommodationPreferences))
                {
                    if (!company.AllowedAccomodations.HasFlag(accomodation))
                    {
                        wrongAccomodations.Add(accomodation.GetDescription());
                    }
                }

                if (!wrongAccomodations.IsNullOrEmpty())
                {
                    alerts.Add(new AlertDto()
                    {
                        Message = string.Format(BusinessTripRequestResources.WrongAllocationPreferences,
                            company.Name, string.Join<string>(", ", wrongAccomodations)),
                        Type = AlertType.Error
                    });
                }
            }

            return alerts;
        }

        public override string PendingApprovalNotificationAdditionalContent(RequestDto request)
        {
            var baseUrl = _systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);
            var url = baseUrl + "BusinessTrips/BusinessTrip/List";

            return $"<a href=\"{url}\">{BusinessTripRequestResources.PendingApprovalAdditionalContentLink}</a>";
        }
    }
}