﻿using System;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;

namespace Smt.Atomic.Business.BusinessTrips.BusinessLogics
{
    public static class BusinessTripParticipantBusinessLogic
    {
        public static readonly BusinessLogic<BusinessTripParticipant, DateTime, bool> HasPendingSettlements
            = new BusinessLogic<BusinessTripParticipant, DateTime, bool>(
                (participant, date) =>
                    BusinessTripBusinessLogic.IsFinished.Call(participant.BusinessTrip, date)
                    && (!participant.SettlementRequests.Any()
                        || participant.SettlementRequests.Any(
                            s => s.Request.Status != RequestStatus.Pending && s.Request.Status != RequestStatus.Completed)));
    }
}
