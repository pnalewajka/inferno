﻿using System;
using System.Linq;
using Smt.Atomic.Business.Workflows.BusinessLogics;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;

namespace Smt.Atomic.Business.BusinessTrips.BusinessLogics
{
    public static class BusinessTripBusinessLogic
    {
        public static readonly BusinessLogic<BusinessTrip, long, bool> IsRequestingUser =
            new BusinessLogic<BusinessTrip, long, bool>(
                (businessTrip, userId) => RequestBusinessLogic.IsRequester.Call(businessTrip.Request.Request, userId));

        public static readonly BusinessLogic<BusinessTrip, long, bool> IsWatcherUser =
            new BusinessLogic<BusinessTrip, long, bool>(
                (businessTrip, userId) => RequestBusinessLogic.IsWatcher.Call(businessTrip.Request.Request, userId));

        public static readonly BusinessLogic<BusinessTrip, long, DateTime, bool> IsApprovingUser =
            new BusinessLogic<BusinessTrip, long, DateTime, bool>(
                (businessTrip, userId, date) => RequestBusinessLogic.IsApprover.Call(businessTrip.Request.Request, userId, date));

        public static readonly BusinessLogic<BusinessTrip, long, DateTime, bool> IsSettlementApprovingUser =
            new BusinessLogic<BusinessTrip, long, DateTime, bool>(
                (businessTrip, userId, date) => businessTrip.Participants
                    .SelectMany(p => p.SettlementRequests)
                    .Any(r => RequestBusinessLogic.IsApprover.Call(r.Request, userId, date)));

        public static readonly BusinessLogic<BusinessTrip, long, bool> IsEmployeeParticipant =
            new BusinessLogic<BusinessTrip, long, bool>(
                (businessTrip, employeeId) => businessTrip.Participants.Any(p => p.EmployeeId == employeeId));

        public static readonly BusinessLogic<BusinessTrip, long, bool> IsEmployeeMentioned =
            new BusinessLogic<BusinessTrip, long, bool>(
                (businessTrip, employeeId) => businessTrip.Messages.Any(m => m.MentionedEmployees.Any(e => e.Id == employeeId)));

        public static readonly BusinessLogic<BusinessTrip, bool> IsArranged =
            new BusinessLogic<BusinessTrip, bool>(
                businessTrip => businessTrip.Participants.All(
                    p => p.ArrangementTrackingEntries.All(e => e.Status == ArrangementTrackingStatus.Done)));

        public static readonly BusinessLogic<BusinessTrip, DateTime, bool> IsOngoing =
            new BusinessLogic<BusinessTrip, DateTime, bool>(
                (businessTrip, currentDate) => currentDate >= businessTrip.StartedOn);

        public static readonly BusinessLogic<BusinessTrip, DateTime, bool> IsFinished =
            new BusinessLogic<BusinessTrip, DateTime, bool>(
                (businessTrip, currentDate) => currentDate > businessTrip.EndedOn);

        public static readonly BusinessLogic<BusinessTrip, bool> IsAccounted =
            new BusinessLogic<BusinessTrip, bool>(
                businessTrip => businessTrip.Participants.All(
                    p => p.SettlementRequests.Any() && p.SettlementRequests.All(s => s.Request.Status == RequestStatus.Completed)));

        public static readonly BusinessLogic<BusinessTrip, bool> IsNotAccounted =
            new BusinessLogic<BusinessTrip, bool>(
                businessTrip => !businessTrip.Participants.All(
                    p => p.SettlementRequests.Any() && p.SettlementRequests.All(s => s.Request.Status == RequestStatus.Completed)));

        public static readonly BusinessLogic<BusinessTrip, DateTime, BusinessTripStatus> GetStatus =
            new BusinessLogic<BusinessTrip, DateTime, BusinessTripStatus>(
                (businessTrip, currentDate) =>
                    IsAccounted.Call(businessTrip) ? BusinessTripStatus.Accounted
                    : IsFinished.Call(businessTrip, currentDate) ? BusinessTripStatus.Finished
                    : IsOngoing.Call(businessTrip, currentDate) ? BusinessTripStatus.Ongoing
                    : IsArranged.Call(businessTrip) ? BusinessTripStatus.Arranged
                    : BusinessTripStatus.ArrangementInProgress);

        public static readonly BusinessLogic<BusinessTrip, long, bool> IsEmployeeFrontDeskAsignee =
            new BusinessLogic<BusinessTrip, long, bool>(
                (businessTrip, employeeId) => businessTrip.FrontDeskAssigneeEmployeeId != null ? businessTrip.FrontDeskAssigneeEmployeeId.Value == employeeId : false);              

        public static readonly BusinessLogic<BusinessTrip, long[], bool> BelongsToAnyOfCompanies =
            new BusinessLogic<BusinessTrip, long[], bool>(
                (businessTrip, companyIds) => businessTrip.Participants.Any(t => t.Employee.CompanyId.HasValue && companyIds.Contains(t.Employee.CompanyId.Value)));
                
        public static readonly BusinessLogic<BusinessTrip, DateTime, DateTime, bool> IsBetweenBusinessTripRange =
            new BusinessLogic<BusinessTrip, DateTime, DateTime, bool>(
                (businessTrip, from, to) => businessTrip.StartedOn >= from && businessTrip.EndedOn <= to);

        public static readonly BusinessLogic<BusinessTrip, long, bool> HasDestinationCity =
            new BusinessLogic<BusinessTrip, long, bool>(
                (businessTrip, cityId) => businessTrip.DestinationCityId == cityId);

        public static readonly BusinessLogic<BusinessTrip, long, bool> HasDepartureCity =
            new BusinessLogic<BusinessTrip, long, bool>(
                (businessTrip, cityId) => businessTrip.Participants.Any(p => p.DepartureCityId == cityId));

        public static readonly BusinessLogic<BusinessTrip, bool> IsBusinessTripFrontDeskArranged =
            new BusinessLogic<BusinessTrip, bool>(
                businessTrip => !businessTrip.Participants
                .Any(participant => participant.ArrangementTrackingEntries
                .Any(arrangementTrackingEntry => arrangementTrackingEntry.Status != ArrangementTrackingStatus.Done)));

        public static readonly BusinessLogic<BusinessTrip, bool> IsBusinessTripFrontDeskInArrangement =
            new BusinessLogic<BusinessTrip, bool>(
                businessTrip => businessTrip.Participants
                .Any(participant => participant.ArrangementTrackingEntries
                .Any(arrangementTrackingEntry => arrangementTrackingEntry.Status != ArrangementTrackingStatus.Done)));

        public static readonly BusinessLogic<BusinessTripParticipant, bool> IsInternational =
            new BusinessLogic<BusinessTripParticipant, bool>(
                (participant) => participant.Employee.Location.City.CountryId != participant.BusinessTrip.DestinationCity.CountryId);
    }
}
