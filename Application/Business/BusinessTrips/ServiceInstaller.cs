﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.BusinessTrips.ChoreProviders;
using Smt.Atomic.Business.BusinessTrips.Interfaces;
using Smt.Atomic.Business.BusinessTrips.Services;
using Smt.Atomic.Business.BusinessTrips.Workflows.Services;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.EventSourcing.Interfaces;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.BusinessTrips
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            if (containerType == ContainerType.WebApp)
            {
                container.Register(Component.For<IBusinessTripCardIndexDataService>().ImplementedBy<BusinessTripCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IBusinessTripParticipantCardIndexDataService>().ImplementedBy<BusinessTripParticipantCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IBusinessTripAdvancedPaymentCardIndexDataService>().ImplementedBy<BusinessTripAdvancedPaymentCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IBusinessTripService>().ImplementedBy<BusinessTripService>().LifestyleTransient());
                container.Register(Component.For<IRequestService>().ImplementedBy<BusinessTripRequestService>().LifestyleTransient());
                container.Register(Component.For<IRequestService>().ImplementedBy<AdvancedPaymentRequestService>().LifestyleTransient());
                container.Register(Component.For<IRequestService>().ImplementedBy<BusinessTripSettlementRequestService>().LifestyleTransient());
                container.Register(Component.For<IBusinessTripSettlementService>().ImplementedBy<BusinessTripSettlementService>().LifestyleTransient());
                container.Register(Component.For<IBusinessTripArrangementsService>().ImplementedBy<BusinessTripArrangementsService>().LifestyleTransient());
                container.Register(Component.For<IBusinessTripParticipantsService>().ImplementedBy<BusinessTripParticipantsService>().LifestyleTransient());
                container.Register(Component.For<IBusinessTripSettlementsCardIndexDataService>().ImplementedBy<BusinessTripSettlementsCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IDailyAllowanceCardIndexDataService>().ImplementedBy<DailyAllowanceCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IBusinessTripAcceptanceConditionsService>().ImplementedBy<BusinessTripAcceptanceConditionsService>().LifestyleTransient());
                container.Register(Component.For<IChoreProvider>().ImplementedBy<UnsettledBusinessTripChoreProvider>().LifestylePerWebRequest());
            }

            if (containerType == ContainerType.JobScheduler)
            {
                container.Register(Component.For<IBusinessTripSettlementService>().ImplementedBy<BusinessTripSettlementService>().LifestylePerThread());
                container.Register(Component.For<IBusinessTripService>().ImplementedBy<BusinessTripService>().LifestyleTransient());
                container.Register(Component.For<IRequestService>().ImplementedBy<BusinessTripRequestService>().LifestyleTransient());
                container.Register(Component.For<IRequestService>().ImplementedBy<AdvancedPaymentRequestService>().LifestyleTransient());
                container.Register(Component.For<IRequestService>().ImplementedBy<BusinessTripSettlementRequestService>().LifestyleTransient());
                container.Register(Component.For<IBusinessTripParticipantsService>().ImplementedBy<BusinessTripParticipantsService>().LifestyleTransient());
            }

            container.Register(Classes.FromThisAssembly().BasedOn<IBusinessEventHandler>().WithService.FromInterface().LifestyleTransient());
        }
    }
}
