﻿namespace Smt.Atomic.Business.BusinessTrips.Consts
{
    public static class SearchAreaCodes
    {
        public const string Apn = "apn";
        public const string Participants = "participants";
        public const string Projects = "projects";
        public const string DepartureCity = "departure";
        public const string DestinationCity = "destination";
    }
}