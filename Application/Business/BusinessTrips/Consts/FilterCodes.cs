﻿namespace Smt.Atomic.Business.BusinessTrips.Consts
{
    public static class FilterCodes
    {
        public const string DailyAllowanceValidForDate = "valid-for-date";
        public const string DailyAllowanceCountry = "employee-country";
        public const string FrontDeskAsigneeFilter = "front-desk-assignee";
        public const string DateRangeFilter = "date-range";
        public const string DestinationPlaceFilter = "city";
        public const string DeparturePlaceFilter = "departure-city";
        public const string FrontDeskArrangedFilter = "front-desk-arranged";
        public const string FrontDeskInArrangementFilter = "front-desk-in-arrangement";
        public const string AllMode = "all";
        public const string MonthlyMode = "monthly";
        public const string AllTrips = "all-trips";
        public const string MyTrips = "my-trips";
        public const string ApprovedByMe = "approved-by-me";
        public const string WatchedByMe = "watched-by-me";
    }
}
