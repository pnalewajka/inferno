﻿using Smt.Atomic.Business.Workflows.Dto;

namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class BusinessTripSettlementDto
    {
        public RequestDto Request { get; set; }

        public BusinessTripParticipantDto Participant { get; set; }
    }
}
