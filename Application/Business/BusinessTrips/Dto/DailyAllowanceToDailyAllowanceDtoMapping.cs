﻿using Smt.Atomic.Business.Dictionaries.BusinessLogics;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;

namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class DailyAllowanceToDailyAllowanceDtoMapping : ClassMapping<DailyAllowance, DailyAllowanceDto>
    {
        public DailyAllowanceToDailyAllowanceDtoMapping()
        {
            Mapping = e => new DailyAllowanceDto
            {
                Id = e.Id,
                ValidFrom = e.ValidFrom,
                ValidTo = e.ValidTo,
                EmploymentCountryId = e.EmploymentCountryId,
                EmploymentCountryName = e.EmploymentCountry == null ? null : e.EmploymentCountry.Name,
                TravelCityId = e.TravelCityId,
                TravelCityName = e.TravelCity == null ? null : CityBusinessLogic.DisplayName.Call(e.TravelCity),
                TravelCountryId = e.TravelCountryId,
                TravelCountryName = e.TravelCountry == null ? null : e.TravelCountry.Name,
                CurrencyId = e.CurrencyId,
                Allowance = e.Allowance,
                DepartureAndArrivalDayAllowance = e.DepartureAndArrivalDayAllowance,
                AccommodationLumpSum = e.AccommodationLumpSum,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
