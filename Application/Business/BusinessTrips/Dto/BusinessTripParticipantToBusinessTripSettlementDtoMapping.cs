﻿using System.Linq;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class BusinessTripParticipantToBusinessTripSettlementDtoMapping : ClassMapping<BusinessTripParticipant, BusinessTripSettlementDto>
    {
        private readonly IClassMapping<Request, RequestDto> _requestClasssMapping;
        private readonly IClassMapping<BusinessTripParticipant, BusinessTripParticipantDto> _participantClassMapping;

        public BusinessTripParticipantToBusinessTripSettlementDtoMapping(
            IClassMappingFactory classMappingFactory)
        {
            _requestClasssMapping = classMappingFactory.CreateMapping<Request, RequestDto>();
            _participantClassMapping = classMappingFactory.CreateMapping<BusinessTripParticipant, BusinessTripParticipantDto>();

            Mapping = e => CreateSettlementDto(e);
        }

        private BusinessTripSettlementDto CreateSettlementDto(BusinessTripParticipant e)
        {
            var latestRequest = e.SettlementRequests
                .OrderByDescending(r => r.Request.CreatedOn)
                .Select(r => r.Request)
                .FirstOrDefault();

            return new BusinessTripSettlementDto
            {
                Request = latestRequest == null ? null : _requestClasssMapping.CreateFromSource(latestRequest),
                Participant = _participantClassMapping.CreateFromSource(e)
            };
        }
    }
}
