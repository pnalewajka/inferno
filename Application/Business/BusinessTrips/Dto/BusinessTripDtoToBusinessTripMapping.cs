﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class BusinessTripDtoToBusinessTripMapping : ClassMapping<BusinessTripDto, BusinessTrip>
    {
        public BusinessTripDtoToBusinessTripMapping()
        {
            Mapping = d => new BusinessTrip
            {
                Id = d.Id,
            };
        }
    }
}
