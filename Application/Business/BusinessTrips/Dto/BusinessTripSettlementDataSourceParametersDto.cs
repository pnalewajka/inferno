﻿namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class BusinessTripSettlementDataSourceParametersDto
    {
        public long BusinessTripId { get; set; }

        public long BusinessTripParticipantId { get; set; }
    }
}
