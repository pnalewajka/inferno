﻿using System;

namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class BusinessTripMessageDto
    {
        public long Id { get; set; }

        public long AuthorEmployeeId { get; set; }

        public DateTime AddedOn { get; set; }

        public string Content { get; set; }

        public long BusinessTripId { get; set; }

        public string AuthorEmail { get; set; }

        public string AuthorFullName { get; set; }

        public long[] MentionedEmployeeIds { get; set; }
    }
}
