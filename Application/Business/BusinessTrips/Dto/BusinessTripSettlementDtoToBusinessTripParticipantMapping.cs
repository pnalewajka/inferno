﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;

namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class BusinessTripSettlementDtoToBusinessTripParticipantMapping : ClassMapping<BusinessTripSettlementDto, BusinessTripParticipant>
    {
        public BusinessTripSettlementDtoToBusinessTripParticipantMapping()
        {
            Mapping = d => new BusinessTripParticipant { };
        }
    }
}
