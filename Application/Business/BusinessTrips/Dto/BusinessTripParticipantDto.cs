﻿using System;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class BusinessTripParticipantDto : BusinessTripDisplayParticipantDto
    {

        public long BusinessTripId { get; set; }

        public long DepartureCityId { get; set; }

        public DateTime StartedOn { get; set; }

        public DateTime EndedOn { get; set; }

        public MeansOfTransport TransportationPreferences { get; set; }

        public AccommodationType AccommodationPreferences { get; set; }

        public int DepartureCityTaxiVouchers { get; set; }

        public int DestinationCityTaxiVouchers { get; set; }

        public DeclaredLuggageDto[] LuggageDeclarations { get; set; }

        public string ItineraryDetails { get; set; }

        public string IdCardNumber { get; set; }

        public string PhoneNumber { get; set; }

        public string HomeAddress { get; set; }

        public long? CompanyId { get; set; }

        public long? ContractTypeId { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string PersonalNumber { get; set; }

        public PersonalNumberType? PersonalNumberType { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
