﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Business.Interfaces;

namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class BusinessTripAcceptanceConditionsDto: IAcceptanceConditions
    {
        public long Id { get; set; }

        public bool ReinvoicingToCustomer { get; set; }

        public long? ReinvoiceCurrencyId { get; set; }

        public CostApprovalPolicy CostApprovalPolicy { get; set; }

        public long? MaxCostsCurrencyId { get; set; }

        public decimal? MaxHotelCosts { get; set; }

        public decimal? MaxTotalFlightsCosts { get; set; }

        public decimal? MaxOtherTravelCosts { get; set; }
    }
}
