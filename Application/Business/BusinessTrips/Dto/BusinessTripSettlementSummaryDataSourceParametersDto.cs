﻿namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class BusinessTripSettlementSummaryDataSourceParametersDto
    {
        public int Year { get; set; }

        public int Month { get; set; }
    }
}
