﻿namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class ArrangementDocumentDto
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }
}