﻿using Smt.Atomic.Business.Common.Services;

namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class ParticipantPickerContext : ParentIdContext
    {
        public enum PickerMode
        {
            None = 0,
            ManageSettlements,
            SettlementReport,
        }

        public PickerMode PickerFilter { get; set; }
    }
}
