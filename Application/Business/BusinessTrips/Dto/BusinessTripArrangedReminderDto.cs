﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class BusinessTripArrangedReminderDto
    {
        public string BusinessTripUrl { get; set; }

        public string BusinessTripName { get; set; }
    }
}
