﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class BusinessTripArrangementDto
    {
        public long Id { get; set; }

        public ArrangementType ArrangementType { get; set; }

        public DateTime IssuedOn { get; set; }

        public string Name { get; set; }

        public IList<BusinessTripDisplayParticipantDto> Participants { get; set; }

        public string Details { get; set; }

        public CurrencyDto Currency { get; set; }

        public decimal? Value { get; set; }

        public IList<ArrangementDocumentDto> ArrangementDocuments { get; set; }

        public byte[] Timestamp { get; set; }
    }
}