﻿using System;

namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class DailyAllowanceDto
    {
        public long Id { get; set; }

        public DateTime? ValidFrom { get; set; }

        public DateTime? ValidTo { get; set; }

        public long EmploymentCountryId { get; set; }

        public string EmploymentCountryName { get; set; }

        public long? TravelCityId { get; set; }

        public string TravelCityName { get; set; }

        public long? TravelCountryId { get; set; }

        public string TravelCountryName { get; set; }

        public long CurrencyId { get; set; }

        public decimal Allowance { get; set; }

        public decimal? DepartureAndArrivalDayAllowance { get; set; }

        public decimal AccommodationLumpSum { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
