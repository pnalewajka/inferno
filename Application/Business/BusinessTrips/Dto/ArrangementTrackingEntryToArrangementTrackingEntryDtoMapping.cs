﻿using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class ArrangementTrackingEntryToArrangementTrackingEntryDtoMapping : ClassMapping<ArrangementTrackingEntry, ArrangementTrackingEntryDto>
    {
        public ArrangementTrackingEntryToArrangementTrackingEntryDtoMapping()
        {
            Mapping = e => new ArrangementTrackingEntryDto
            {
                Id = e.Id,
                ParticipantId = e.ParticipantId,
                ArrangementType = e.ArrangementType,
                Status = e.Status,
            };
        }
    }
}
