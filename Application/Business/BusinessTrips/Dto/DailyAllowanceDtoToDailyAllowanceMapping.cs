﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;

namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class DailyAllowanceDtoToDailyAllowanceMapping : ClassMapping<DailyAllowanceDto, DailyAllowance>
    {
        public DailyAllowanceDtoToDailyAllowanceMapping()
        {
            Mapping = d => new DailyAllowance
            {
                Id = d.Id,
                ValidFrom = d.ValidFrom,
                ValidTo = d.ValidTo,
                EmploymentCountryId = d.EmploymentCountryId,
                TravelCityId = d.TravelCityId,
                TravelCountryId = d.TravelCountryId,
                CurrencyId = d.CurrencyId,
                Allowance = d.Allowance,
                DepartureAndArrivalDayAllowance = d.DepartureAndArrivalDayAllowance,
                AccommodationLumpSum = d.AccommodationLumpSum,
                Timestamp = d.Timestamp
            };
        }
    }
}
