﻿using System;
using System.Linq;
using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.BusinessTrips.BusinessLogics;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Business.Helpers;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Repositories.Helpers;

namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class BusinessTripToBusinessTripDtoMapping : ClassMapping<BusinessTrip, BusinessTripDto>
    {
        public BusinessTripToBusinessTripDtoMapping(
            ITimeService timeService,
            IClassMappingFactory classMappingFactory,
            IPrincipalProvider principalProvider)
        {
            var settlementMapping = classMappingFactory.CreateMapping<BusinessTripParticipant, BusinessTripSettlementDto>();
            var participantMapping = classMappingFactory.CreateMapping<BusinessTripParticipant, BusinessTripParticipantDto>();
            var currentUserId = principalProvider?.Current?.Id ?? BusinessLogicHelper.NonExistingId;

            Mapping = e => new BusinessTripDto
            {
                Id = e.Id,
                Identifier = $"BT-{e.Id}",
                ProjectIds = e.Projects.Select(p => p.Id).ToArray(),
                ProjectNames = e.Projects.AsQueryable().Select<Project, string>(ProjectBusinessLogic.ClientProjectName).ToArray(),
                ParticipantIds = e.Participants.Select(p => p.Id).ToArray(),
                StartedOn = e.StartedOn,
                EndedOn = e.EndedOn,
                Status = BusinessTripBusinessLogic.GetStatus.Call(e, timeService.GetCurrentDate()),
                DepartureCityNames = e.Participants.Select(p => $"{p.DepartureCity.Name} ({p.DepartureCity.Country.Name})").Distinct().ToArray(),
                DestinationCityId = e.DestinationCityId,
                DestinationCityName = $"{e.DestinationCity.Name} ({e.DestinationCity.Country.Name})",
                TravelExplanation = e.TravelExplanation,
                ItineraryDetails = e.ItineraryDetails,
                AcceptingPersonFullNames = e.Request.Request.ApprovalGroups
                    .SelectMany(g => g.Approvals)
                    .Select(a => UserBusinessLogic.FullName.Call(a.User))
                    .ToArray(),
                ParticipantFullNames = e.Participants.Select(p => EmployeeBusinessLogic.FullName.Call(p.Employee)).ToArray(),
                ArrangementTrackingRemarks = e.ArrangementTrackingRemarks,
                FrontDeskAssigneeEmployeeId = e.FrontDeskAssigneeEmployeeId,
                CompanyNames = e.Participants.Select(p => p.Employee.Company.Name).Distinct().ToArray(),
                Settlements = e.Participants.Select(settlementMapping.CreateFromSource).ToArray(),
                Participants = e.Participants.Select(participantMapping.CreateFromSource).ToArray(),
                Attachments = GetAttachments(e, currentUserId),
                ReinvoicingToCustomer = e.BusinessTripAcceptanceConditions != null
                    ? e.BusinessTripAcceptanceConditions.ReinvoicingToCustomer
                    : false,
            };
        }

        private static DocumentDto[] GetAttachments(BusinessTrip e, long currentUserId)
        {
            //keep as a separate method to avoid known issue with ConfigureIncludes
            return e.Arrangements
                .Filtered()
                .SelectMany(a => a.ArrangementDocuments)
                .Select(a => new DocumentDto
                {
                    ContentType = a.ContentType,
                    DocumentName = a.Name,
                    DocumentId = a.Id
                }).ToArray();
        }
    }
}
