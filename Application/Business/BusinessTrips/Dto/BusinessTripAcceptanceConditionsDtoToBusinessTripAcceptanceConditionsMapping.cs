﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;

namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class BusinessTripAcceptanceConditionsDtoToBusinessTripAcceptanceConditionsMapping : ClassMapping<BusinessTripAcceptanceConditionsDto, BusinessTripAcceptanceConditions>
    {
        public BusinessTripAcceptanceConditionsDtoToBusinessTripAcceptanceConditionsMapping()
        {
            Mapping = d => new BusinessTripAcceptanceConditions
            {
                Id = d.Id,
                ReinvoicingToCustomer = d.ReinvoicingToCustomer,
                ReinvoiceCurrencyId = d.ReinvoiceCurrencyId,
                CostApprovalPolicy = d.CostApprovalPolicy,
                MaxCostsCurrencyId = d.MaxCostsCurrencyId,
                MaxHotelCosts = d.MaxHotelCosts,
                MaxTotalFlightsCosts = d.MaxTotalFlightsCosts,
                MaxOtherTravelCosts = d.MaxOtherTravelCosts
            };
        }
    }
}
