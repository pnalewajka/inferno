﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;

namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class BusinessTripAcceptanceConditionsToBusinessTripAcceptanceConditionsDtoMapping : ClassMapping<BusinessTripAcceptanceConditions, BusinessTripAcceptanceConditionsDto>
    {
        public BusinessTripAcceptanceConditionsToBusinessTripAcceptanceConditionsDtoMapping()
        {
            Mapping = e => new BusinessTripAcceptanceConditionsDto
            {
                Id = e.Id,
                ReinvoicingToCustomer = e.ReinvoicingToCustomer,
                ReinvoiceCurrencyId = e.ReinvoiceCurrencyId,
                CostApprovalPolicy = e.CostApprovalPolicy,
                MaxCostsCurrencyId = e.MaxCostsCurrencyId,
                MaxHotelCosts = e.MaxHotelCosts,
                MaxTotalFlightsCosts = e.MaxTotalFlightsCosts,
                MaxOtherTravelCosts = e.MaxOtherTravelCosts
            };
        }
    }
}
