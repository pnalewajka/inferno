﻿using System.Linq;
using Smt.Atomic.Business.Dictionaries.BusinessLogics;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;

namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class BusinessTripParticipantToBusinessTripParticipantDtoMapping : ClassMapping<BusinessTripParticipant, BusinessTripParticipantDto>
    {
        public BusinessTripParticipantToBusinessTripParticipantDtoMapping()
        {
            Mapping = e => new BusinessTripParticipantDto
            {
                Id = e.Id,
                BusinessTripId = e.BusinessTripId,
                EmployeeId = e.EmployeeId,
                DepartureCityId = e.DepartureCityId,
                StartedOn = e.StartedOn,
                EndedOn = e.EndedOn,
                TransportationPreferences = e.TransportationPreferences,
                AccommodationPreferences = e.AccommodationPreferences,
                DepartureCityTaxiVouchers = e.DepartureCityTaxiVouchers,
                DestinationCityTaxiVouchers = e.DestinationCityTaxiVouchers,
                LuggageDeclarations = e.LuggageDeclarations.Select(l => new DeclaredLuggageDto
                {
                    Id = l.Id,
                    EmployeeId = l.EmployeeId,
                    HandLuggageItems = l.HandLuggageItems,
                    RegisteredLuggageItems = l.RegisteredLuggageItems
                }).ToArray(),
                ItineraryDetails = e.ItineraryDetails,
                CompanyId = e.Employee != null && e.Employee.GetCurrentEmploymentPeriod() != null
                    ? e.Employee.GetCurrentEmploymentPeriod().CompanyId
                    : null,
                ContractTypeId = e.Employee != null && e.Employee.GetCurrentEmploymentPeriod() != null
                    ? e.Employee.GetCurrentEmploymentPeriod().ContractTypeId
                    : (long?)null,
                DateOfBirth = e.Employee != null ? e.Employee.DateOfBirth : null,
                PersonalNumber = e.Employee != null ? e.Employee.PersonalNumber : null,
                PersonalNumberType = e.Employee != null ? e.Employee.PersonalNumberType : null,
                IdCardNumber = e.IdCardNumber,
                PhoneNumber = e.PhoneNumber,
                HomeAddress = e.HomeAddress,
                EmployeeFirstName = e.Employee != null ? e.Employee.FirstName : null,
                EmployeeLastName = e.Employee != null ? e.Employee.LastName : null,
                Timestamp = e.Timestamp,
                EmployeeOrgUnitName = e.Employee != null && e.Employee.OrgUnitId != default(long) ? e.Employee.OrgUnit.Name : null,
                EmployeeOrgUnitCode = e.Employee != null && e.Employee.OrgUnitId != default(long) ? e.Employee.OrgUnit.Code : null,
            };
        }
    }
}
