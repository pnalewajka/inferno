﻿namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class BusinessTripDisplayParticipantDto
    {
        public long Id { get; set; }

        public long EmployeeId { get; set; }

        public string EmployeeFirstName { get; set; }

        public string EmployeeLastName { get; set; }

        public string EmployeeName => $"{EmployeeFirstName} {EmployeeLastName}";

        public string EmployeeOrgUnitName { get; set; }

        public string EmployeeOrgUnitCode { get; set; }
    }
}