﻿namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class BusinessTripNotSettledReminderDto
    {
        public string BusinessTripUrl { get; set; }

        public string BusinessTripName { get; set; }

        public int DaysAfterFinished { get; set; }
    }
}
