﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class BusinessTripParticipantDtoToBusinessTripParticipantMapping : ClassMapping<BusinessTripParticipantDto, BusinessTripParticipant>
    {
        public BusinessTripParticipantDtoToBusinessTripParticipantMapping()
        {
            Mapping = d => new BusinessTripParticipant
            {
                Id = d.Id,
                BusinessTripId = d.BusinessTripId,
                EmployeeId = d.EmployeeId,
                StartedOn = d.StartedOn,
                EndedOn = d.EndedOn,
                DepartureCityId = d.DepartureCityId,
                TransportationPreferences = d.TransportationPreferences,
                AccommodationPreferences = d.AccommodationPreferences,
                DepartureCityTaxiVouchers = d.DepartureCityTaxiVouchers,
                DestinationCityTaxiVouchers = d.DestinationCityTaxiVouchers,
                LuggageDeclarations = d.LuggageDeclarations.Select(l => new DeclaredLuggage
                {
                    Id = l.Id,
                    EmployeeId = l.EmployeeId,
                    HandLuggageItems = l.HandLuggageItems,
                    RegisteredLuggageItems = l.RegisteredLuggageItems
                }).ToList(),
                ItineraryDetails = d.ItineraryDetails,
                IdCardNumber = d.IdCardNumber,
                PhoneNumber = d.PhoneNumber,
                HomeAddress = d.HomeAddress,
                Timestamp = d.Timestamp
            };
        }
    }
}
