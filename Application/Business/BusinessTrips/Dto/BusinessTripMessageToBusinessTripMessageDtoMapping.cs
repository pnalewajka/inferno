﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;

namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class BusinessTripMessageToBusinessTripMessageDtoMapping : ClassMapping<BusinessTripMessage, BusinessTripMessageDto>
    {
        public BusinessTripMessageToBusinessTripMessageDtoMapping()
        {
            Mapping = e => new BusinessTripMessageDto
            {
                Id = e.Id,
                AuthorEmployeeId = e.AuthorEmployeeId,
                AddedOn = e.AddedOn,
                Content = e.Content,
                BusinessTripId = e.BusinessTripId,
                AuthorEmail = e.Author != null ? e.Author.Email : "",
                AuthorFullName = e.Author != null ? (e.Author.FirstName + " " + e.Author.LastName) : "",
                MentionedEmployeeIds = e.MentionedEmployees.Select(m => m.Id).ToArray()
            };
        }
    }
}
