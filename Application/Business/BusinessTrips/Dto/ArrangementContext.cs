﻿using Smt.Atomic.Business.Common.Services;

namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class ArrangementContext : ParentIdContext
    {
        public bool RecalculateApproved { get; set; }
    }
}
