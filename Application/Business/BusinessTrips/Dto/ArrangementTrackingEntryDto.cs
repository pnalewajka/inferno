﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class ArrangementTrackingEntryDto
    {
        public long Id { get; set; }

        public long ParticipantId { get; set; }

        public TrackedArrangement ArrangementType { get; set; }

        public ArrangementTrackingStatus Status { get; set; }
    }
}
