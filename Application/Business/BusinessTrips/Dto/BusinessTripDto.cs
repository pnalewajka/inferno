﻿using System;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class BusinessTripDto
    {
        public long Id { get; set; }       

        public string Identifier { get; set; }

        public long RequestId { get; set; }

        public long[] ProjectIds { get; set; }
        
        public string[] ProjectNames { get; set; }

        public long[] ParticipantIds { get; set; }

        public DateTime StartedOn { get; set; }

        public DateTime EndedOn { get; set; }

        public BusinessTripStatus Status { get; set; }

        public long DestinationCityId { get; set; }

        public string TravelExplanation { get; set; }

        public string[] DepartureCityNames { get; set; }

        public string DestinationCityName { get; set; }

        public string ItineraryDetails { get; set; }

        public string ArrangementTrackingRemarks { get; set; }

        public long? FrontDeskAssigneeEmployeeId { get; set; }

        public string[] CompanyNames { get; set; }

        public string[] AcceptingPersonFullNames { get; set; }

        public string[] ParticipantFullNames { get; set; }

        public BusinessTripSettlementDto[] Settlements { get; set; }

        public byte[] Timestamp { get; set; }

        public DocumentDto[] Attachments { get; set; }

        public BusinessTripParticipantDto[] Participants { get; set; }

        public bool ReinvoicingToCustomer { get; set; }
    }
}
