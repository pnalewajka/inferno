﻿using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.BusinessTrips.Dto
{
    public class ArrangementTrackingEntryDtoToArrangementTrackingEntryMapping : ClassMapping<ArrangementTrackingEntryDto, ArrangementTrackingEntry>
    {
        public ArrangementTrackingEntryDtoToArrangementTrackingEntryMapping()
        {
            Mapping = d => new ArrangementTrackingEntry
            {
                Id = d.Id,
                ParticipantId = d.ParticipantId,
                ArrangementType = d.ArrangementType,
                Status = d.Status,
            };
        }
    }
}
