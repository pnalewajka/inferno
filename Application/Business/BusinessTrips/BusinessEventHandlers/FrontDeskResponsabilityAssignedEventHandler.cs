﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.BusinessTrips.BusinessEvents;
using Smt.Atomic.Business.BusinessTrips.EmailModels;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.BusinessTrips.BusinessEventHandlers
{
    public class FrontDeskResponsabilityAssignedEventHandler
        : BusinessEventHandler<FrontDeskResponsabilityAssignedEvent>
    {
        private readonly IUnitOfWorkService<IBusinessTripsDbScope> _unitOfWorkService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IReliableEmailService _reliableEmailService;

        public FrontDeskResponsabilityAssignedEventHandler(
            IUnitOfWorkService<IBusinessTripsDbScope> unitOfWorkService,
            ISystemParameterService systemParameterService,
            IMessageTemplateService messageTemplateService,
            IReliableEmailService reliableEmailService)
        {
            _unitOfWorkService = unitOfWorkService;
            _systemParameterService = systemParameterService;
            _messageTemplateService = messageTemplateService;
            _reliableEmailService = reliableEmailService;
        }

        public override void Handle(FrontDeskResponsabilityAssignedEvent businessEvent)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var assignee = unitOfWork.Repositories.Employees.GetByIdOrDefault(businessEvent.FrontDeskAssigneeEmployeeId);
                var businessTrips = unitOfWork.Repositories.BusinessTrips
                    .AsNoTracking()
                    .GetByIds(businessEvent.BusinessTripIds)
                    .ToList();

                SendAssignmentEmail(assignee, businessTrips);
            }
        }

        private void SendAssignmentEmail(Employee assignee, List<BusinessTrip> businessTrips)
        {
            const string businessTripUrlFormat = "{0}BusinessTrips/BusinessTripArrangements?parent-id={1}";
            const string allBusinessTripsUrlFormat = "{0}BusinessTrips/BusinessTrip/List";

            var serverAddress = _systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);
            var emailModel = new BusinessTripAssignmentEmailModel
            {
                BusinessTripsUrl = string.Format(allBusinessTripsUrlFormat, serverAddress),
                EmployeeFullName = assignee.FullName,
                BusinessTripIdToUrlDictionary = businessTrips.ToDictionary(d => $"BT-{d.Id}",
                    d => string.Format(businessTripUrlFormat, serverAddress, d.Id))
            };

            _reliableEmailService.EnqueueMessage(new EmailMessageDto()
            {
                Body = _messageTemplateService.ResolveTemplate(TemplateCodes.BusinessTripAssignmentChangedBody, emailModel).Content,
                Subject = _messageTemplateService.ResolveTemplate(TemplateCodes.BusinessTripAssignmentChangeSubject, businessTrips.Count > 1).Content,
                IsHtml = true,
                Recipients = new List<EmailRecipientDto>() { new EmailRecipientDto(assignee.Email, assignee.FullName) }
            });
        }
    }
}
