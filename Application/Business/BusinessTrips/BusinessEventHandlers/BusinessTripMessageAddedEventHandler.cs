﻿using System.Linq;
using Smt.Atomic.Business.BusinessTrips.BusinessEvents;
using Smt.Atomic.Business.BusinessTrips.EmailModels;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.BusinessTrips.BusinessEventHandlers
{
    public class BusinessTripMessageAddedEventHandler : BusinessEventHandler<BusinessTripMessageAddedEvent>
    {
        private readonly IUnitOfWorkService<IBusinessTripsDbScope> _unitOfWorkService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly IMessageTemplateService _messageTemplateService;

        public BusinessTripMessageAddedEventHandler(
            IUnitOfWorkService<IBusinessTripsDbScope> unitOfWorkService,
            ISystemParameterService systemParameterService,
            IReliableEmailService reliableEmailService,
            IMessageTemplateService messageTemplateService)
        {
            _unitOfWorkService = unitOfWorkService;
            _systemParameterService = systemParameterService;
            _reliableEmailService = reliableEmailService;
            _messageTemplateService = messageTemplateService;
        }

        public override void Handle(BusinessTripMessageAddedEvent businessEvent)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var message = unitOfWork.Repositories
                    .BusinessTripMessages
                    .GetById(businessEvent.BusinessTripMessageId);

                var recipients = unitOfWork.Repositories
                    .BusinessTrips
                    .Where(b => b.Id == message.BusinessTripId)
                    .SelectMany(b => b.Participants.Select(p => p.Employee)
                        .Union(b.Messages.Select(m => m.Author))
                        .Union(b.Messages.SelectMany(m => m.MentionedEmployees))
                        .Union(new[] { b.FrontDeskAssignee }))
                    .Where(e => e != null && e.Id != message.AuthorEmployeeId && !string.IsNullOrEmpty(e.Email))
                    .DistinctBy(e => e.Id)
                    .Select(e => new EmailRecipientDto
                    {
                        EmailAddress = e.Email,
                        FullName = e.FullName,
                        Type = RecipientType.To
                    });

                var content = MentionItemHelper.ReplaceTokens(message.Content, i => i.Label);
                var emailModel = new BusinessTripCommunicationAddedEmailModel
                {
                    Message = HtmlHelper.DecodeAndStripHtmlTags(content),
                    EmployeeFullName = message.Author.DisplayName,
                    BusinessTripsUrl = BusinessTripListUrl(),
                    CommunicationUrl = BusinessTripCommunicationUrl(message.BusinessTripId, businessEvent.BusinessTripMessageId),
                };

                _reliableEmailService.EnqueueMessage(new EmailMessageDto
                {
                    Body = _messageTemplateService.ResolveTemplate(TemplateCodes.BusinessTripCommunicationMessageAddedBody, emailModel).Content,
                    Subject = _messageTemplateService.ResolveTemplate(TemplateCodes.BusinessTripCommunicationMessageAddedSubject, emailModel).Content,
                    IsHtml = true,
                    Recipients = recipients.ToList(),
                });
            }
        }

        private string BusinessTripListUrl()
        {
            var serverAddress = _systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);

            return $"{serverAddress}BusinessTrips/BusinessTrip/List";
        }

        private string BusinessTripCommunicationUrl(long businessTripId, long communicationId)
        {
            var serverAddress = _systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);

            return $"{serverAddress}BusinessTrips/BusinessTripCommunication/List?parent-id={businessTripId}#{communicationId}";
        }
    }
}
