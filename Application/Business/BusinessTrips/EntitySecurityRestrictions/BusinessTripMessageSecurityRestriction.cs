﻿using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.BusinessTrips.EntitySecurityRestrictions
{
    public class BusinessTripMessageSecurityRestriction : RelatedEntitySecurityRestriction<BusinessTripMessage, BusinessTrip>
    {
        public BusinessTripMessageSecurityRestriction(IPrincipalProvider principalProvider)
            : base(principalProvider)
        {
        }

        protected override BusinessLogic<BusinessTripMessage, BusinessTrip> RelatedEntitySelector =>
            new BusinessLogic<BusinessTripMessage, BusinessTrip>(businessTripMessage => businessTripMessage.BusinessTrip);
    }
}
