﻿using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.BusinessTrips.EntitySecurityRestrictions
{
    public class BusinessTripParticipantSecurityRestriction : RelatedEntitySecurityRestriction<BusinessTripParticipant, BusinessTrip>
    {
        public BusinessTripParticipantSecurityRestriction(IPrincipalProvider principalProvider)
            : base(principalProvider)
        {
        }

        protected override BusinessLogic<BusinessTripParticipant, BusinessTrip> RelatedEntitySelector =>
            new BusinessLogic<BusinessTripParticipant, BusinessTrip>(businessTripParticipant => businessTripParticipant.BusinessTrip);
    }
}
