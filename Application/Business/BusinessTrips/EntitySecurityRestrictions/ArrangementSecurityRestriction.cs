﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.BusinessTrips.EntitySecurityRestrictions
{
    public class ArrangementSecurityRestriction : RelatedEntitySecurityRestriction<Arrangement, BusinessTrip>
    {
        public ArrangementSecurityRestriction(IPrincipalProvider principalProvider) 
            : base(principalProvider)
        {
        }

        protected override BusinessLogic<Arrangement, BusinessTrip> RelatedEntitySelector 
            => new BusinessLogic<Arrangement, BusinessTrip>(a => a.BusinessTrip);

        protected override BusinessLogic<Arrangement, bool> GetEntityRestriction()
        {
            if (Roles.Contains(SecurityRoleType.CanManageAllBusinessTripArrangements))
            {
                return new BusinessLogic<Arrangement, bool>(AllRecords());   
            }

            if (Roles.Contains(SecurityRoleType.CanManageBusinessTripArrangements))
            {
                return new BusinessLogic<Arrangement, bool>(a => !a.Participants.Any()
                    || a.Participants.Any(p => p.EmployeeId == CurrentPrincipal.EmployeeId));
            }

            return new BusinessLogic<Arrangement, bool>(NoRecords());
        }
    }
}
