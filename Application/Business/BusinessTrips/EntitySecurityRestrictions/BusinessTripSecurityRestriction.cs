﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.Business.BusinessTrips.BusinessLogics;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.FilteringQueries;
using Smt.Atomic.Data.Repositories.Helpers;

namespace Smt.Atomic.Business.BusinessTrips.EntitySecurityRestrictions
{
    public class BusinessTripSecurityRestriction : EntitySecurityRestriction<BusinessTrip>
    {
        private readonly ITimeService _timeService;

        public BusinessTripSecurityRestriction(
            IPrincipalProvider principalProvider,
            ITimeService timeService)
            : base(principalProvider)
        {
            _timeService = timeService;

            RequiresAuthentication = true;
        }

        public override Expression<Func<BusinessTrip, bool>> GetRestriction()
        {
            if (Roles.Contains(SecurityRoleType.CanViewAllBusinessTrips))
            {
                return AllRecords();
            }

            if (Roles.Contains(SecurityRoleType.CanViewMyBusinessTrips))
            {
                var currentDate = _timeService.GetCurrentTime();
                var currentUserId = CurrentPrincipal.Id.Value;
                var currentEmployeeId = CurrentPrincipal.EmployeeId;

                return new BusinessLogic<BusinessTrip, bool>(r =>
                    BusinessTripBusinessLogic.IsEmployeeParticipant.Call(r, currentEmployeeId)
                    || BusinessTripBusinessLogic.IsWatcherUser.Call(r, currentUserId)
                    || BusinessTripBusinessLogic.IsRequestingUser.Call(r, currentUserId)
                    || BusinessTripBusinessLogic.IsApprovingUser.Call(r, currentUserId, currentDate)
                    || BusinessTripBusinessLogic.IsSettlementApprovingUser.Call(r, currentUserId, currentDate)
                    || BusinessTripBusinessLogic.IsEmployeeMentioned.Call(r, currentEmployeeId));
            }

            return GetRequestRestriction();
        }

        private Expression<Func<BusinessTrip, bool>> GetRequestRestriction()
        {
            var requestRestriction = EntitySecurityRestrictionsHelper.GetSecurityRestriction<Request>();
            var requestBusinessLogic = new BusinessLogic<Request, bool>(requestRestriction.GetRestriction());

            return new BusinessLogic<BusinessTrip, bool>(c => requestBusinessLogic.Call(c.Request.Request));
        }
    }
}
