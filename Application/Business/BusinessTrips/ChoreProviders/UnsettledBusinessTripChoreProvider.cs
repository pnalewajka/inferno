﻿using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.BusinessTrips.BusinessLogics;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.BusinessTrips.ChoreProviders
{
    public class UnsettledBusinessTripChoreProvider
        : IChoreProvider
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly ITimeService _timeService;

        public UnsettledBusinessTripChoreProvider(
            IPrincipalProvider principalProvider,
            ITimeService timeService)
        {
            _principalProvider = principalProvider;
            _timeService = timeService;
        }

        public IQueryable<ChoreDto> GetActiveChores(IReadOnlyRepositoryFactory repositoryFactory)
        {
            if (!_principalProvider.Current.IsInRole(SecurityRoleType.CanRequestBusinessTrip))
            {
                return null;
            }

            var currentDate = _timeService.GetCurrentDate();
            var isCurrentEmploymentPeriod = EmploymentPeriodBusinessLogic.ForDate.Parametrize(currentDate);
            var hasPendingSettlements = BusinessTripParticipantBusinessLogic.HasPendingSettlements.Parametrize(currentDate);
            var participants = repositoryFactory.Create<BusinessTripParticipant>().Filtered();

            return participants
                .Where(p => p.EmployeeId == _principalProvider.Current.EmployeeId)
                .Where(p => p.Employee.EmploymentPeriods.AsQueryable()
                    .Where(isCurrentEmploymentPeriod)
                    .Select(ep => ep.ContractType.NotifyMissingBusinessTripSettlement)
                    .FirstOrDefault())
                .Where(hasPendingSettlements)
                .GroupBy(p => p.BusinessTripId)
                .Select(b => new ChoreDto
                {
                    Type = ChoreType.UnsettledBusinessTrip,
                    DueDate = null,
                    Parameters = b.Key.ToString()
                });
        }
    }
}
