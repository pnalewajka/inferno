﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Smt.Atomic.Business.BusinessTrips.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class ReportsResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ReportsResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Smt.Atomic.Business.BusinessTrips.Resources.ReportsResources", typeof(ReportsResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Business Trip Settlement for intive GmbH.
        /// </summary>
        internal static string BusinessTripSettlementIntiveGmbhReportDescription {
            get {
                return ResourceManager.GetString("BusinessTripSettlementIntiveGmbhReportDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Business Trip Settlement for intive GmbH.
        /// </summary>
        internal static string BusinessTripSettlementIntiveGmbhReportName {
            get {
                return ResourceManager.GetString("BusinessTripSettlementIntiveGmbhReportName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Business Trip Settlement.
        /// </summary>
        internal static string BusinessTripSettlementReportDescription {
            get {
                return ResourceManager.GetString("BusinessTripSettlementReportDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Business Trip Settlement.
        /// </summary>
        internal static string BusinessTripSettlementReportName {
            get {
                return ResourceManager.GetString("BusinessTripSettlementReportName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Business Trip Settlement Summary.
        /// </summary>
        internal static string BusinessTripSettlementSummaryReportDescription {
            get {
                return ResourceManager.GetString("BusinessTripSettlementSummaryReportDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Business Trip Settlement Summary.
        /// </summary>
        internal static string BusinessTripSettlementSummaryReportName {
            get {
                return ResourceManager.GetString("BusinessTripSettlementSummaryReportName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Domestic.
        /// </summary>
        internal static string DomesticValue {
            get {
                return ResourceManager.GetString("DomesticValue", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Foreign.
        /// </summary>
        internal static string ForeignValue {
            get {
                return ResourceManager.GetString("ForeignValue", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to NO.
        /// </summary>
        internal static string No {
            get {
                return ResourceManager.GetString("No", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Report cannot be generated without business trip settlement request.
        /// </summary>
        internal static string NoSettlementForReportError {
            get {
                return ResourceManager.GetString("NoSettlementForReportError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This report is not available for given contract type.
        /// </summary>
        internal static string ReportNotAvailable {
            get {
                return ResourceManager.GetString("ReportNotAvailable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Settlements.
        /// </summary>
        internal static string SettlementsZipFile {
            get {
                return ResourceManager.GetString("SettlementsZipFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to TO PAY:.
        /// </summary>
        internal static string ToPay {
            get {
                return ResourceManager.GetString("ToPay", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to TO RETURN:.
        /// </summary>
        internal static string ToReturn {
            get {
                return ResourceManager.GetString("ToReturn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This report is available only for calculations in EUR currency.
        /// </summary>
        internal static string WrongCurrencyEurForReportError {
            get {
                return ResourceManager.GetString("WrongCurrencyEurForReportError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This report is available only for calculations in PLN currency.
        /// </summary>
        internal static string WrongCurrencyPlnForReportError {
            get {
                return ResourceManager.GetString("WrongCurrencyPlnForReportError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to YES.
        /// </summary>
        internal static string Yes {
            get {
                return ResourceManager.GetString("Yes", resourceCulture);
            }
        }
    }
}
