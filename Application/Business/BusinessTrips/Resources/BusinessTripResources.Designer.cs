﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Smt.Atomic.Business.BusinessTrips.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class BusinessTripResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal BusinessTripResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Smt.Atomic.Business.BusinessTrips.Resources.BusinessTripResources", typeof(BusinessTripResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Acceptance conditions for business trip changed.
        /// </summary>
        internal static string AcceptanceConditionChanged {
            get {
                return ResourceManager.GetString("AcceptanceConditionChanged", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to There has to be at least one participant in business trip.
        /// </summary>
        internal static string CannotDeleteLastParticipantError {
            get {
                return ResourceManager.GetString("CannotDeleteLastParticipantError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot delete participant with settlement requests.
        /// </summary>
        internal static string CannotDeleteParticipantWithSettlementRequestsError {
            get {
                return ResourceManager.GetString("CannotDeleteParticipantWithSettlementRequestsError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Company apartment is unavailable in {0}.
        /// </summary>
        internal static string CompanyApartmentUnavailableError {
            get {
                return ResourceManager.GetString("CompanyApartmentUnavailableError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Business trip was created with default acceptance conditions taken from project {0}. &lt;a class=&quot;alert-link&quot; href=&quot;{1}&quot;&gt;Click here&lt;/a&gt; to change this conditions.
        /// </summary>
        internal static string CreatedWithDefaultAcceptanceConditionsMessageFormat {
            get {
                return ResourceManager.GetString("CreatedWithDefaultAcceptanceConditionsMessageFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to At least one of the participants has no location set.
        /// </summary>
        internal static string EmployeeWithoutLocationError {
            get {
                return ResourceManager.GetString("EmployeeWithoutLocationError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Selected accomodations aren&apos;t available for &apos;{0}&apos;.
        /// </summary>
        internal static string NotAllowedAccomodations {
            get {
                return ResourceManager.GetString("NotAllowedAccomodations", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Selected means of transport aren&apos;t available for &apos;{0}&apos;.
        /// </summary>
        internal static string NotAllowedMeansOfTransport {
            get {
                return ResourceManager.GetString("NotAllowedMeansOfTransport", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lump Sum cannot be selected with other options.
        /// </summary>
        internal static string NotOnlyLumpSumError {
            get {
                return ResourceManager.GetString("NotOnlyLumpSumError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Given employee is already a participant of this business trip.
        /// </summary>
        internal static string ParticipantAlreadyAddedError {
            get {
                return ResourceManager.GetString("ParticipantAlreadyAddedError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Participant {0} is inactive and cannot participate in business trip.
        /// </summary>
        internal static string ParticipantHasNoActiveEmploymentPeriod {
            get {
                return ResourceManager.GetString("ParticipantHasNoActiveEmploymentPeriod", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Selected employee is inactive and cannot participate in business trip.
        /// </summary>
        internal static string TerminatedParticipantError {
            get {
                return ResourceManager.GetString("TerminatedParticipantError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Voucher service is unavailable in {0}.
        /// </summary>
        internal static string VoucherServiceUnavailableError {
            get {
                return ResourceManager.GetString("VoucherServiceUnavailableError", resourceCulture);
            }
        }
    }
}
