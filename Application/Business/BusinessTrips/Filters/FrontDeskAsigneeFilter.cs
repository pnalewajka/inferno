﻿using Smt.Atomic.Business.BusinessTrips.BusinessLogics;
using Smt.Atomic.Business.BusinessTrips.Consts;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;

namespace Smt.Atomic.Business.BusinessTrips.Filters
{
    public static class FrontDeskAsigneeFilter
    {
        public static NamedFilter<BusinessTrip> BusinessTripFilter => new NamedFilter<BusinessTrip, long>(
            FilterCodes.FrontDeskAsigneeFilter,
            BusinessTripBusinessLogic.IsEmployeeFrontDeskAsignee);
    }
}
