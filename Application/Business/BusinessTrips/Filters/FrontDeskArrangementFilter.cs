﻿using Smt.Atomic.Business.BusinessTrips.BusinessLogics;
using Smt.Atomic.Business.BusinessTrips.Consts;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;

namespace Smt.Atomic.Business.BusinessTrips.Filters
{
    public static class FrontDeskArrangementFilter
    {
        public static NamedFilter<BusinessTrip> BusinessTripInArrangementFilter => new NamedFilter<BusinessTrip>(
            FilterCodes.FrontDeskInArrangementFilter,
            BusinessTripBusinessLogic.IsBusinessTripFrontDeskInArrangement);

        public static NamedFilter<BusinessTrip> BusinessTripArrangedFilter => new NamedFilter<BusinessTrip>(
            FilterCodes.FrontDeskArrangedFilter,
            BusinessTripBusinessLogic.IsBusinessTripFrontDeskArranged);
    }
}
