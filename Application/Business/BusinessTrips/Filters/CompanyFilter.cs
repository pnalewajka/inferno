﻿using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.BusinessTrips.BusinessLogics;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;

namespace Smt.Atomic.Business.BusinessTrips.Filters
{
    public static class CompanyFilter
    {
        public static NamedFilter<BusinessTrip> EmployeeFilter => new NamedFilter<BusinessTrip, long[]>(
            FilterCodes.CompanyFilter,
            BusinessTripBusinessLogic.BelongsToAnyOfCompanies.BaseExpression);
    }
}
