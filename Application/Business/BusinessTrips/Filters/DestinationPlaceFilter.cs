﻿using Smt.Atomic.Business.BusinessTrips.BusinessLogics;
using Smt.Atomic.Business.BusinessTrips.Consts;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;

namespace Smt.Atomic.Business.BusinessTrips.Filters
{
    public static class DestinationPlaceFilter
    {
        public static NamedFilter<BusinessTrip> BusinessTripFilter => new NamedFilter<BusinessTrip, long>(
           FilterCodes.DestinationPlaceFilter,
           BusinessTripBusinessLogic.HasDestinationCity);
    }
}
