﻿using System;
using System.Linq;
using Smt.Atomic.Business.BusinessTrips.BusinessLogics;
using Smt.Atomic.Business.BusinessTrips.Consts;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;

namespace Smt.Atomic.Business.BusinessTrips.Filters
{
    public static class ScopeFilter
    {
        public static NamedFilter<BusinessTrip> AllFilter => new NamedFilter<BusinessTrip>(FilterCodes.AllTrips, e => true)
        {
            RequiredRole = SecurityRoleType.CanViewAllBusinessTrips
        };

        public static NamedFilter<BusinessTrip> MyTripsFilter(long userId)
        {
            return new NamedFilter<BusinessTrip>(
                FilterCodes.MyTrips,
                bt => bt.Participants.Any(p => p.Employee.UserId == userId));
        }

        public static NamedFilter<BusinessTrip> ApprovedByMeFilter(long userId, DateTime date)
        {
            return new NamedFilter<BusinessTrip>(
                FilterCodes.ApprovedByMe,
                BusinessTripBusinessLogic.IsApprovingUser.Parametrize(userId, date));
        }

        public static NamedFilter<BusinessTrip> WatchedByMeFilter(long userId)
        {
            return new NamedFilter<BusinessTrip>(
                FilterCodes.WatchedByMe,
                BusinessTripBusinessLogic.IsWatcherUser.Parametrize(userId));
        }
    }
}
