﻿using System;
using System.Globalization;
using System.Resources;
using Smt.Atomic.Business.BusinessTrips.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Business.BusinessTrips.ReportModels
{
    public abstract class BusinessTripSettlementBaseReportModel
    {
        protected static ResourceManager ResourceManager = ResourceManagerHelper.GetResourceManager<ReportsResources>();

        public abstract CultureInfo Culture { get; }

        public long BusinessTripId { get; set; }

        public long SettlementId { get; set; }

        public long EmployeeId { get; set; }

        public string BusinessTripIdentifier => BusinessTripId == default(long) || SettlementId == default(long) ? null 
            : $"BT-{BusinessTripId}/{SettlementId}";

        public string EmployeeFullName { get; set; }

        public string ProjectApn { get; set; }

        public bool? IsDomestic { get; set; }

        public string SettlementType => IsDomestic == null ? null : ResourceManager.GetString(IsDomestic.Value
            ? nameof(ReportsResources.DomesticValue)
            : nameof(ReportsResources.ForeignValue), Culture);

        public string DepartureCity { get; set; }

        public string DestinationCity { get; set; }

        public string Description { get; set; }

        public DateTime DepartureDate { get; set; }

        public DateTime ArrivalDate { get; set; }

        public decimal LumpSumValue { get; set; }

        public CostModel[] OwnCosts { get; set; }

        public decimal OwnCostsValue { get; set; }

        public CostModel[] CompanyCosts { get; set; }

        public decimal CompanyCostsValue { get; set; }

        public decimal VehicleMileageValue { get; set; }

        public string TotalLabel => ResourceManager.GetString(TotalSum > 0m
            ? nameof(ReportsResources.ToPay)
            : nameof(ReportsResources.ToReturn), Culture);

        public decimal TotalSum { get; set; }

        public decimal TotalSumToShow => Math.Abs(TotalSum);

        public string ProjectManagers { get; set; }

        public RequestStatus? Status { get; set; }

        public string StatusText => Status == null ? null
            : AttributeHelper.GetEnumFieldAttribute<DescriptionLocalizedAttribute>(Status)?.GetDescription(Culture);

        public DateTime SettlementDate { get; set; }

        public string CalculationResult { get; set; }

        public class CostModel
        {
            public CultureInfo Culture { get; set; }

            public string Name { get; set; }

            public decimal Value { get; set; }

            public string Currency { get; set; }

            public bool? HasInvoiceIssued { get; set; }

            public string HasInvoiceIssuedText => HasInvoiceIssued == null ? null : ResourceManager.GetString(HasInvoiceIssued.Value
                ? nameof(ReportsResources.Yes)
                : nameof(ReportsResources.No), Culture);

            public BusinessTripSettlementPaymentMethod? PaymentMethod { get; set; }

            public string CardHolder { get; set; }

            public string PaymentMethodText =>
                AttributeHelper.GetEnumFieldAttribute<DescriptionLocalizedAttribute>(PaymentMethod)?.GetDescription(Culture);

            public DateTime Date { get; set; }
        }
    }
}
