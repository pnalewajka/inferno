﻿using System;
using System.Globalization;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.Business.BusinessTrips.ReportModels
{
    public class BusinessTripSettlementIntiveGmbhReportModel : BusinessTripSettlementBaseReportModel
    {
        public override CultureInfo Culture { get; } = new CultureInfo(CultureCodes.English);

        public DailyAllowanceModel[] DailyAllowances { get; set; }

        public decimal DailyAllowancesValue { get; set; }

        public decimal? VehicleMileageKilometers { get; set; }

        public decimal? VehicleMileageRate { get; set; }

        public class DailyAllowanceModel
        {
            public DateTime Date { get; set; }

            public decimal Allowance { get; set; }

            public decimal MealsValue { get; set; }

            public decimal CalculatedValue { get; set; }

            public string Currency { get; set; }
        }
    }
}
