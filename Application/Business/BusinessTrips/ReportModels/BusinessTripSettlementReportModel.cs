﻿using System;
using System.Globalization;
using System.Resources;
using Smt.Atomic.Business.BusinessTrips.Resources;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.Business.BusinessTrips.ReportModels
{
    public class BusinessTripSettlementReportModel : BusinessTripSettlementBaseReportModel
    {
        public override CultureInfo Culture { get; } = new CultureInfo(CultureCodes.Polish);

        public string EmployeeAddress { get; set; }

        public string EmployeeFullNameInMileage => !string.IsNullOrEmpty(RegistrationNumber) ? EmployeeFullName : null;

        public DateTime? BorderCrossingDepartureDate { get; set; }

        public DateTime? BorderCrossingArrivalDate { get; set; }

        public decimal DomesticDailyAllowances { get; set; }

        public decimal DomesticDailyAllowancesValue { get; set; }

        public decimal ForeignDailyAllowances { get; set; }

        public decimal ForeignDailyAllowancesValue { get; set; }

        public decimal DailyAllowancesValue { get; set; }

        public long Breakfasts { get; set; }

        public long Lunches { get; set; }

        public long Dinners { get; set; }

        public string RegistrationNumber { get; set; }

        public bool? IsCarEngineCapacityOver900cc { get; set; }

        public string IsCarEngineCapacityOver900ccText => IsCarEngineCapacityOver900cc == null ? null : ResourceManager.GetString(IsCarEngineCapacityOver900cc.Value
            ? nameof(ReportsResources.Yes)
            : nameof(ReportsResources.No), Culture);

        public string InboundFromCity { get; set; }

        public string InboundToCity { get; set; }

        public decimal? InboundKilometers { get; set; }

        public decimal? InboundRate { get; set; }

        public decimal? InboundValue { get; set; }

        public string OutboundFromCity { get; set; }

        public string OutboundToCity { get; set; }

        public decimal? OutboundKilometers { get; set; }

        public decimal? OutboundRate { get; set; }

        public decimal? OutboundValue { get; set; }

        public decimal AdvancePaymentValue { get; set; }
    }
}
