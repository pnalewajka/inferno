﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Smt.Atomic.Business.BusinessTrips.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Business.BusinessTrips.ReportModels
{
    public class BusinessTripSettlementSummaryReportModel
    {
        public ICollection<EntryReportModel> Settlements { get; set; }

        public class EntryReportModel
        {
            private static readonly CultureInfo Culture = new CultureInfo(CultureCodes.EnglishAmerican);

            public long BusinessTripId { get; set; }

            public long EmployeeId { get; set; }

            public long SettlementId { get; set; }

            public string EmployeeFullName { get; set; }

            public string EmployeeAcronym { get; set; }

            public string CompanyName { get; set; }

            public string ContractTypeName { get; set; }

            public string ProjectApn { get; set; }

            public string Description { get; set; }

            public string BusinessTripIdentifier => BusinessTripId != default(long) ? $"BT-{BusinessTripId}" : null;

            public DateTime? StartDate { get; set; }

            public DateTime? EndDate { get; set; }

            public DateTime? SettlementDate { get; set; }

            public bool? IsDomestic { get; set; }

            public string SettlementType => IsDomestic == null ? null : (ResourceManagerHelper.GetResourceManager<ReportsResources>()
                .GetString(IsDomestic.Value ? nameof(ReportsResources.DomesticValue) : nameof(ReportsResources.ForeignValue), Culture));

            public string Destination { get; set; }

            public string TransportName { get; set; }

            public decimal? TransportValue { get; set; }

            public string AccommodationName { get; set; }

            public decimal? AccommodationValue { get; set; }

            public string OtherName { get; set; }

            public decimal? OtherValue { get; set; }

            public string OwnCostName { get; set; }

            public decimal? OwnCostForeignValue { get; set; }

            public string OwnCostForeignCurrency { get; set; }

            public decimal? OwnCostValue { get; set; }

            public string CompanyCostName { get; set; }

            public decimal? CompanyCostForeignValue { get; set; }

            public string CompanyCostForeignCurrency { get; set; }

            public decimal? CompanyCostValue { get; set; }

            public decimal? MileageValue { get; set; }

            public decimal? AllowanceUnits { get; set; }

            public decimal? AllowanceValue { get; set; }

            public bool RequiresReinvoicing { get; set; }

            public decimal? ReinvoicingForeignValue { get; set; }

            public string ReinvoicingForeignCurrency { get; set; }

            public decimal? ReinvoicingExchangeRate { get; set; }

            public decimal? ReinvoicingValue { get; set; }

            public decimal? AdvancePaymentValue { get; set; }

            public decimal? TotalValue { get; set; }

            public RequestStatus? Status { get; set; }

            public RequestStatus? MonthlyStatus { get; set; }

            public string StatusText => Status == null ? null
                : AttributeHelper.GetEnumFieldAttribute<DescriptionLocalizedAttribute>(Status)?.GetDescription(Culture);

            public string MonthlyStatusText => MonthlyStatus == null ? null
                : AttributeHelper.GetEnumFieldAttribute<DescriptionLocalizedAttribute>(MonthlyStatus)?.GetDescription(Culture);

            public string AcceptingPersons { get; set; }

            public string CalculationResult { get; set; }

            public IList<EntryReportModel> RelatedEntries { get; set; } = new List<EntryReportModel>();
        }
    }
}
