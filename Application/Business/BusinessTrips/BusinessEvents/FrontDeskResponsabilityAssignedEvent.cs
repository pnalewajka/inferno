﻿using System.Runtime.Serialization;
using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.BusinessTrips.BusinessEvents
{
    [DataContract]
    [KnownType(typeof(long[]))]
    public class FrontDeskResponsabilityAssignedEvent : BusinessEvent
    {
        [DataMember]
        public long[] BusinessTripIds { get; set; }

        [DataMember]
        public long FrontDeskAssigneeEmployeeId { get; set; }
    }
}
