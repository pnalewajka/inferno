﻿using System.Runtime.Serialization;
using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.BusinessTrips.BusinessEvents
{
    [DataContract]
    public class BusinessTripMessageAddedEvent : BusinessEvent
    {
        [DataMember]
        public long BusinessTripMessageId { get; set; }
    }
}
