﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.BusinessTrips.ReportModels;
using Smt.Atomic.Business.BusinessTrips.Resources;
using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.DiagnosticItems;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.Business.Compensation.Services;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

using static Smt.Atomic.Business.BusinessTrips.ReportModels.BusinessTripSettlementSummaryReportModel;

namespace Smt.Atomic.Business.BusinessTrips.ReportDataSources
{
    [Identifier("DataSources.BusinessTripSettlementSummary")]
    [RequireRole(SecurityRoleType.CanGenerateBusinessTripSettlementSummaryReport)]
    [DefaultReportDefinition(
        "BusinessTripSettlementSummary",
        nameof(ReportsResources.BusinessTripSettlementSummaryReportName),
        nameof(ReportsResources.BusinessTripSettlementSummaryReportDescription),
        MenuAreas.Workflow,
        MenuGroups.WorkflowReports,
        ReportingEngine.MsExcel,
        "Smt.Atomic.Business.BusinessTrips.ReportTemplates.BusinessTripSettlementSummary.xlsx",
        typeof(ReportsResources))]
    public class BusinessTripSettlementSummaryDataSource
         : BaseReportDataSource<BusinessTripSettlementSummaryDataSourceParametersDto>
    {
        private Lazy<IList<PropertyInfo>> _propertiesToOverride = new Lazy<IList<PropertyInfo>>(() => InitializePropertiesToOverride());

        private readonly ITimeService _timeService;
        private readonly ICurrencyExchangeRateService _nbpCurrencyExchangeRateService;
        private readonly IUnitOfWorkService<IBusinessTripsDbScope> _unitOfWorkService;

        public BusinessTripSettlementSummaryDataSource(
            ITimeService timeService,
            NbpCurrencyExchangeRateService nbpCurrencyExchangeRateService,
            IUnitOfWorkService<IBusinessTripsDbScope> unitOfWorkService)
        {
            _timeService = timeService;
            _nbpCurrencyExchangeRateService = nbpCurrencyExchangeRateService;
            _unitOfWorkService = unitOfWorkService;
        }

        public override object GetData(ReportGenerationContext<BusinessTripSettlementSummaryDataSourceParametersDto> reportGenerationContext)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var parameters = reportGenerationContext.Parameters;

                var settlements = unitOfWork.Repositories.BusinessTripSettlementRequests
                    .Where(s => (s.ArrivalDateTime ?? s.BusinessTripParticipant.BusinessTrip.EndedOn).Year == parameters.Year
                        && (s.ArrivalDateTime ?? s.BusinessTripParticipant.BusinessTrip.EndedOn).Month == parameters.Month)
                    .Select(s => new EntryReportModel
                    {
                        BusinessTripId = s.BusinessTripParticipant.BusinessTripId,
                        EmployeeId = s.BusinessTripParticipant.EmployeeId,
                        SettlementId = s.Id,
                        EmployeeFullName = s.BusinessTripParticipant.Employee.FirstName + " " + s.BusinessTripParticipant.Employee.LastName,
                        EmployeeAcronym = s.BusinessTripParticipant.Employee.Acronym,
                        Description = s.BusinessTripParticipant.BusinessTrip.TravelExplanation,
                        StartDate = s.DepartureDateTime ?? s.BusinessTripParticipant.BusinessTrip.StartedOn,
                        EndDate = s.ArrivalDateTime ?? s.BusinessTripParticipant.BusinessTrip.EndedOn,
                        SettlementDate = s.SettlementDate,
                        IsDomestic = s.BusinessTripParticipant.Employee.Location.City.CountryId == s.BusinessTripParticipant.BusinessTrip.DestinationCity.CountryId,
                        Destination = s.BusinessTripParticipant.BusinessTrip.DestinationCity.Name + ", " + s.BusinessTripParticipant.BusinessTrip.DestinationCity.Country.Name,
                        RequiresReinvoicing = s.BusinessTripParticipant.BusinessTrip.BusinessTripAcceptanceConditions != null
                            ? s.BusinessTripParticipant.BusinessTrip.BusinessTripAcceptanceConditions.ReinvoicingToCustomer
                            : false,
                        ReinvoicingForeignCurrency = s.BusinessTripParticipant.BusinessTrip.BusinessTripAcceptanceConditions != null
                            ? s.BusinessTripParticipant.BusinessTrip.BusinessTripAcceptanceConditions.ReinvoiceCurrency.IsoCode
                            : null,
                        Status = s.Request.Status,
                        CalculationResult = s.CalculationResult
                    })
                    .ToList();

                foreach (var settlement in settlements.ToList())
                {
                    var employmentPeriodData = unitOfWork.Repositories.EmploymentPeriods
                        .Where(p => p.EmployeeId == settlement.EmployeeId)
                        .Where(EmploymentPeriodBusinessLogic.ForDate.Parametrize(settlement.StartDate.Value))
                        .Select(p => new { CompanyName = p.Company.Name, ContractTypeName = p.ContractType.NameEn })
                        .FirstOrDefault();

                    settlement.CompanyName = employmentPeriodData.CompanyName;
                    settlement.ContractTypeName = employmentPeriodData.ContractTypeName;
                }

                FillAdditionalMultiRowData(unitOfWork, settlements);
                FillCalculationResultData(settlements);

                return new BusinessTripSettlementSummaryReportModel
                {
                    Settlements = settlements
                };
            }
        }

        public override BusinessTripSettlementSummaryDataSourceParametersDto GetDefaultParameters(ReportGenerationContext<BusinessTripSettlementSummaryDataSourceParametersDto> reportGenerationContext)
        {
            var currentDate = _timeService.GetCurrentDate();

            return new BusinessTripSettlementSummaryDataSourceParametersDto
            {
                Year = currentDate.Year,
                Month = currentDate.Month,
            };
        }

        private void FillAdditionalMultiRowData(IUnitOfWork<IBusinessTripsDbScope> unitOfWork,
            IList<EntryReportModel> settlements)
        {
            var businessTripids = settlements.Select(s => s.BusinessTripId).ToHashSet();
            var settlementIds = settlements.Select(s => s.SettlementId).ToHashSet();

            var acceptingPersons = unitOfWork.Repositories.BusinessTripSettlementRequests
                 .Where(s => settlementIds.Contains(s.Id))
                 .Select(s => new
                 {
                     SettlementId = s.Id,
                     AcceptingPersons = s.Request.ApprovalGroups
                         .SelectMany(g => g.Approvals)
                         .Select(a => a.User.FirstName + " " + a.User.LastName)
                 })
                 .ToDictionary(r => r.SettlementId, r => r.AcceptingPersons);
            var projectApns = unitOfWork.Repositories.BusinessTrips
                .Where(t => businessTripids.Contains(t.Id))
                .Select(t => new
                {
                    BusinessTripId = t.Id,
                    Apns = t.Projects.Select(p => p.Apn)
                })
                .ToDictionary(r => r.BusinessTripId, r => r.Apns);
            var monthlyStatuses = unitOfWork.Repositories.BusinessTripSettlementRequests
                .Where(s => settlementIds.Contains(s.Id))
                .GroupBy(s => s.BusinessTripParticipant.EmployeeId)
                .ToDictionary(g => g.Key, g => g.Min(s => s.Request.Status));

            foreach (var settlement in settlements.ToList())
            {
                settlement.MonthlyStatus = monthlyStatuses[settlement.EmployeeId];

                settlement.ProjectApn = string.Join(", ", projectApns[settlement.BusinessTripId]);
                settlement.AcceptingPersons = string.Join(", ", acceptingPersons[settlement.SettlementId]);
            }
        }

        private void FillCalculationResultData(IList<EntryReportModel> settlements)
        {
            for (int index = 0; index < settlements.Count; ++index)
            {
                var settlement = settlements[index];

                if (settlement.CalculationResult == null)
                {
                    continue;
                }

                var calculationResult = JsonHelper.Deserialize<CalculationResult>(settlement.CalculationResult);

                FillArrangements(settlement, calculationResult);
                FillAllowance(settlement, calculationResult);
                FillMileage(settlement, calculationResult);
                FillOwnCosts(settlement, calculationResult);
                FillCompanyCosts(settlement, calculationResult);
                FillAdvancePayment(settlement, calculationResult);
                FillReinvoicing(settlement, calculationResult);
                FillTotalValue(settlement, calculationResult);

                int firstIndex = index;

                foreach (var relatedEntry in settlement.RelatedEntries)
                {
                    if (!Enumerable.Range(firstIndex, index - firstIndex + 1).Any(i => MergeModels(relatedEntry, settlements[i])))
                    {
                        settlements.Insert(++index, relatedEntry);
                    }
                }
            }
        }

        private bool MergeModels(EntryReportModel source, EntryReportModel target)
        {
            var properties = _propertiesToOverride.Value.Where(p => p.GetValue(source) != null && p.GetValue(target) == null);

            if (!properties.Any())
            {
                return false;
            }

            foreach (var property in properties)
            {
                property.SetValue(target, property.GetValue(source));
            }

            return true;
        }

        private void FillArrangements(EntryReportModel settlement, CalculationResult calculationResult)
        {
            var arrangementsCostItems = calculationResult.CostItems.Where(i => i.Group == CostItemGroupType.Arrangements).ToList();

            if (arrangementsCostItems.Any())
            {
                var diagnosticItems = arrangementsCostItems.SelectMany(i => i.DiagnosticItems).Where(d => d.Group == DiagnosticGroupType.Arrangements);

                foreach (var diagnosticItem in diagnosticItems.OfType<TransportArrangementCostDiagnosticItem>())
                {
                    var newSettlement = new EntryReportModel()
                    {
                        TransportName = diagnosticItem.Name,
                        TransportValue = diagnosticItem.CalculatedValue.Amount
                    };

                    settlement.RelatedEntries.Add(newSettlement);
                }

                foreach (var diagnosticItem in diagnosticItems.OfType<AccommodationArrangementCostDiagnosticItem>())
                {
                    var newSettlement = new EntryReportModel()
                    {
                        AccommodationName = diagnosticItem.Name,
                        AccommodationValue = diagnosticItem.CalculatedValue.Amount
                    };

                    settlement.RelatedEntries.Add(newSettlement);
                }

                foreach (var diagnosticItem in diagnosticItems.OfType<OtherArrangementCostDiagnosticItem>())
                {
                    var newSettlement = new EntryReportModel()
                    {
                        OtherName = diagnosticItem.Name,
                        OtherValue = diagnosticItem.CalculatedValue.Amount
                    };

                    settlement.RelatedEntries.Add(newSettlement);
                }
            }
        }

        private void FillAllowance(EntryReportModel settlement, CalculationResult calculationResult)
        {
            var hoursWorkedCostItems = calculationResult.CostItems.Where(i => i.Group == CostItemGroupType.HoursWorkedForClientBonus).ToList();
            var dailyAllowancesCostItems = calculationResult.CostItems.Where(i => i.Group == CostItemGroupType.DailyAllowances).ToList();

            if (hoursWorkedCostItems.Any())
            {
                var hours = hoursWorkedCostItems
                    .SelectMany(i => i.DiagnosticItems)
                    .Where(i => i.Group == DiagnosticGroupType.HoursWorkedForClientBonus)
                    .OfType<HoursMultiplicationDiagnosticItem>()
                    .Select(i => i.Hours)
                    .Sum();

                settlement.AllowanceUnits = hours;
                settlement.AllowanceValue = hoursWorkedCostItems.Sum(i => i.Value);
            }
            else if (dailyAllowancesCostItems.Any())
            {
                var units = dailyAllowancesCostItems.SelectMany(i => i.DiagnosticItems)
                    .Where(d => d.Group == DiagnosticGroupType.DailyAllowances)
                    .OfType<DailyAllowanceValueDiagnosticItem>()
                    .Sum(i => i.AllowanceUnits);

                settlement.AllowanceUnits = units;
                settlement.AllowanceValue = dailyAllowancesCostItems.Sum(i => i.Value);
            }
        }

        private void FillMileage(EntryReportModel settlement, CalculationResult calculationResult)
        {
            var mileageCostItems = calculationResult.CostItems.Where(i => i.Group == CostItemGroupType.PrivateVehicleMileage).ToList();

            if (mileageCostItems.Any())
            {
                settlement.MileageValue = mileageCostItems.Sum(i => i.Value);
            }
        }

        private void FillOwnCosts(EntryReportModel settlement, CalculationResult calculationResult)
        {
            var ownCostItems = calculationResult.CostItems.Where(i => i.Group == CostItemGroupType.OwnCosts).ToList();

            if (ownCostItems.Any())
            {
                var diagnosticItems = ownCostItems.SelectMany(i => i.DiagnosticItems
                    .Where(d => d.Group == DiagnosticGroupType.OwnCosts)
                    .OfType<CostCurrencyRateMultiplicationDiagnosticItem>())
                    .ToList();

                foreach (var diagnosticItem in diagnosticItems)
                {
                    var newSettlement = new EntryReportModel
                    {
                        OwnCostName = diagnosticItem.Name,
                        OwnCostValue = diagnosticItem.CalculatedValue.Amount,
                        OwnCostForeignValue = diagnosticItem.CostValue.Amount,
                        OwnCostForeignCurrency = diagnosticItem.CostValue.CurrencyIsoCode
                    };

                    settlement.RelatedEntries.Add(newSettlement);
                }
            }
        }

        private void FillCompanyCosts(EntryReportModel settlement, CalculationResult calculationResult)
        {
            var companyCostItems = calculationResult.CostItems.Where(i => i.Group == CostItemGroupType.CompanyCosts).ToList();

            if (companyCostItems.Any())
            {
                var diagnosticItems = companyCostItems.SelectMany(i => i.DiagnosticItems)
                    .Where(d => d.Group == DiagnosticGroupType.CompanyCosts)
                    .OfType<CostCurrencyRateMultiplicationDiagnosticItem>()
                    .ToList();

                foreach (var diagnosticItem in diagnosticItems)
                {
                    var newSettlement = new EntryReportModel()
                    {
                        CompanyCostName = diagnosticItem.Name,
                        CompanyCostValue = diagnosticItem.CalculatedValue.Amount,
                        CompanyCostForeignValue = diagnosticItem.CostValue.Amount,
                        CompanyCostForeignCurrency = diagnosticItem.CostValue.CurrencyIsoCode
                    };

                    settlement.RelatedEntries.Add(newSettlement);
                }
            }
        }

        private void FillAdvancePayment(EntryReportModel settlement, CalculationResult calculationResult)
        {
            var advancePaymentCostItems = calculationResult.CostItems
                .Where(i => i.Group == CostItemGroupType.AdditionalAdvancePayment || i.Group == CostItemGroupType.AdvancePayments)
                .ToList();

            if (advancePaymentCostItems.Any())
            {
                settlement.AdvancePaymentValue = advancePaymentCostItems.Sum(i => i.Value);
            }
        }

        private void FillReinvoicing(EntryReportModel settlement, CalculationResult calculationResult)
        {
            var totalCostItems = calculationResult.CostItems.Where(i => i.Group == CostItemGroupType.BusinessTripExpensesTotal).ToList();

            if (settlement.RequiresReinvoicing && totalCostItems.Any())
            {
                const decimal sameCurrencyExchangeRate = 1m;

                var exchangeRate = settlement.ReinvoicingForeignCurrency != CurrencyIsoCodes.PolishZloty
                    ? _nbpCurrencyExchangeRateService.GetExchangeRateForDay(settlement.ReinvoicingForeignCurrency, settlement.SettlementDate.Value)
                    : null;

                settlement.ReinvoicingValue = totalCostItems.Sum(i => i.Value);
                settlement.ReinvoicingExchangeRate = exchangeRate != null ? exchangeRate.Rate : sameCurrencyExchangeRate;
                settlement.ReinvoicingForeignValue = settlement.ReinvoicingValue / settlement.ReinvoicingExchangeRate;
            }
        }

        private void FillTotalValue(EntryReportModel settlement, CalculationResult calculationResult)
        {
            var totalCostItems = calculationResult.CostItems.Where(i => i.Group == CostItemGroupType.BusinessTripCompensationTotal).ToList();

            if (totalCostItems.Any())
            {
                settlement.TotalValue = totalCostItems.Sum(i => i.Value);
            }
        }

        private static IList<PropertyInfo> InitializePropertiesToOverride()
        {
            var propertyNamesToOverride = new[]
            {
                nameof(EntryReportModel.TransportName),
                nameof(EntryReportModel.TransportValue),
                nameof(EntryReportModel.AccommodationName),
                nameof(EntryReportModel.AccommodationValue),
                nameof(EntryReportModel.OtherName),
                nameof(EntryReportModel.OtherValue),
                nameof(EntryReportModel.OwnCostName),
                nameof(EntryReportModel.OwnCostValue),
                nameof(EntryReportModel.OwnCostForeignCurrency),
                nameof(EntryReportModel.OwnCostForeignValue),
                nameof(EntryReportModel.CompanyCostName),
                nameof(EntryReportModel.CompanyCostValue),
                nameof(EntryReportModel.CompanyCostForeignCurrency),
                nameof(EntryReportModel.CompanyCostForeignValue)
            };

            return typeof(EntryReportModel).GetProperties().Where(p => propertyNamesToOverride.Contains(p.Name)).ToList();
        }
    }
}
