﻿using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.BusinessTrips.Helpers;
using Smt.Atomic.Business.BusinessTrips.ReportModels;
using Smt.Atomic.Business.BusinessTrips.Resources;
using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.DiagnosticItems;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.BusinessTrips.ReportDataSources
{
    [Identifier("DataSources.BusinessTripSettlementIntiveGmbh")]
    [RequireRole(SecurityRoleType.CanGenerateBusinessTripSettlementReport)]
    [DefaultReportDefinition(
        BusinessTripSettlementReportHelper.IntiveGmbhReportDataSource,
        nameof(ReportsResources.BusinessTripSettlementIntiveGmbhReportName),
        nameof(ReportsResources.BusinessTripSettlementIntiveGmbhReportDescription),
        MenuAreas.Workflow,
        MenuGroups.WorkflowReports,
        ReportingEngine.MsExcel,
        "Smt.Atomic.Business.BusinessTrips.ReportTemplates.BusinessTripSettlementIntiveGmbh.xlsx",
        "@(Model.SettlementId)-@(Model.EmployeeFullName).xlsx",
        typeof(ReportsResources))]
    public class BusinessTripSettlementIntiveGmbhDataSource : BaseReportDataSource<BusinessTripSettlementDataSourceParametersDto>
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly IApprovingPersonService _approvingPersonService;
        private readonly IUnitOfWorkService<IBusinessTripsDbScope> _unitOfWorkService;

        public BusinessTripSettlementIntiveGmbhDataSource(
            IPrincipalProvider principalProvider,
            IApprovingPersonService approvingPersonService,
            IUnitOfWorkService<IBusinessTripsDbScope> unitOfWorkService)
        {
            _principalProvider = principalProvider;
            _approvingPersonService = approvingPersonService;
            _unitOfWorkService = unitOfWorkService;
        }

        public override object GetData(ReportGenerationContext<BusinessTripSettlementDataSourceParametersDto> reportGenerationContext)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var model = unitOfWork.Repositories.BusinessTripSettlementRequests
                    .Where(s => s.SettlementDate.HasValue
                        && s.BusinessTripParticipantId == reportGenerationContext.Parameters.BusinessTripParticipantId)
                    .Select(s => new BusinessTripSettlementIntiveGmbhReportModel
                    {
                        BusinessTripId = s.BusinessTripParticipant.BusinessTripId,
                        SettlementId = s.Id,
                        EmployeeId = s.BusinessTripParticipant.EmployeeId,
                        EmployeeFullName = EmployeeBusinessLogic.FullName.Call(s.BusinessTripParticipant.Employee),
                        Description = s.BusinessTripParticipant.BusinessTrip.TravelExplanation,
                        IsDomestic = s.BusinessTripParticipant.Employee.Location.City.CountryId == s.BusinessTripParticipant.BusinessTrip.DestinationCity.CountryId,
                        DepartureCity = s.BusinessTripParticipant.Employee.Location.City.Name + ", " + s.BusinessTripParticipant.Employee.Location.City.Country.Name,
                        DestinationCity = s.BusinessTripParticipant.BusinessTrip.DestinationCity.Name + ", " + s.BusinessTripParticipant.BusinessTrip.DestinationCity.Country.Name,
                        DepartureDate = s.DepartureDateTime ?? s.BusinessTripParticipant.BusinessTrip.StartedOn,
                        ArrivalDate = s.ArrivalDateTime ?? s.BusinessTripParticipant.BusinessTrip.EndedOn,
                        SettlementDate = s.SettlementDate.Value,
                        Status = s.Request.Status,
                        CalculationResult = s.CalculationResult,
                    })
                    .FirstOrDefault();

                if (model == null)
                {
                    throw new BusinessException(ReportsResources.NoSettlementForReportError);
                }

                var currentPrincipal = _principalProvider.Current;

                if (ShouldDataBeLimitedToOwnSettlements(currentPrincipal) && model.EmployeeId != currentPrincipal.EmployeeId)
                {
                    throw new MissingRequiredRoleException(new[] { SecurityRoleType.CanGenerateAllBusinessTripSettlementReport.ToString() });
                }

                FillAdditionalMultiRowData(unitOfWork, model);
                FillCalculationResultData(model);

                return model;
            }
        }

        public override BusinessTripSettlementDataSourceParametersDto GetDefaultParameters(ReportGenerationContext<BusinessTripSettlementDataSourceParametersDto> reportGenerationContext)
        {
            return new BusinessTripSettlementDataSourceParametersDto();
        }

        private void FillAdditionalMultiRowData(IUnitOfWork<IBusinessTripsDbScope> unitOfWork, BusinessTripSettlementIntiveGmbhReportModel model)
        {
            var projects = unitOfWork.Repositories.BusinessTrips
                .Where(t => t.Id == model.BusinessTripId)
                .SelectMany(t => t.Projects.Select(p => new { p.Id, p.Apn }))
                .ToList();

            var approverIds = _approvingPersonService.GetStandardCostApprovers(new[] { model.EmployeeId }, projects.Select(p => p.Id));

            var projectManagers = unitOfWork.Repositories.Employees
                .Where(e => approverIds.Contains(e.Id))
                .Select(EmployeeBusinessLogic.FullName.AsExpression())
                .ToList();

            var ownCosts = unitOfWork.Repositories.BusinessTripSettlementRequests
                .Where(s => s.Id == model.SettlementId)
                .SelectMany(s => s.OwnCosts.Select(c => new BusinessTripSettlementBaseReportModel.CostModel
                {
                    Name = c.Description,
                    Value = c.Amount,
                    Currency = CurrencyIsoCodes.Euro,
                    HasInvoiceIssued = c.HasInvoiceIssued,
                    Date = c.TransactionDate
                }))
                .AsEnumerable()
                .Select(c => { c.Culture = model.Culture; return c; })
                .ToArray();

            var companyCosts = unitOfWork.Repositories.BusinessTripSettlementRequests
                .Where(s => s.Id == model.SettlementId)
                .SelectMany(s => s.CompanyCosts.Select(c => new BusinessTripSettlementBaseReportModel.CostModel
                {
                    Name = c.Description,
                    Value = c.Amount,
                    PaymentMethod = c.PaymentMethod,
                    CardHolder = c.CardHolder == null ? null : c.CardHolder.FirstName + " " + c.CardHolder.LastName,
                    Date = c.TransactionDate
                }))
                .AsEnumerable()
                .Select(c => { c.Culture = model.Culture; return c; })
                .ToArray();

            model.OwnCosts = ownCosts;
            model.CompanyCosts = companyCosts;

            model.ProjectApn = string.Join(", ", projects.Select(p => p.Apn));
            model.ProjectManagers = string.Join(", ", projectManagers);
        }

        private void FillCalculationResultData(BusinessTripSettlementIntiveGmbhReportModel model)
        {
            if (model.CalculationResult == null)
            {
                return;
            }

            var calculationResult = JsonHelper.Deserialize<CalculationResult>(model.CalculationResult);

            if (calculationResult.CostItems.Any(i => i.CurrencyIsoCode != CurrencyIsoCodes.Euro))
            {
                throw new BusinessException(ReportsResources.WrongCurrencyEurForReportError);
            }

            FillDailyAllowances(model, calculationResult);
            FillLumpSum(model, calculationResult);
            FillOwnCosts(model, calculationResult);
            FillCompanyCosts(model, calculationResult);
            FillVehicleMileage(model, calculationResult);
            FillTotalSum(model, calculationResult);
        }

        private void FillDailyAllowances(BusinessTripSettlementIntiveGmbhReportModel model, CalculationResult calculationResult)
        {
            var dailyAllowanceItems = calculationResult.CostItems.Where(i => i.Group == CostItemGroupType.DailyAllowances).ToList();

            if (dailyAllowanceItems.Any())
            {
                model.DailyAllowancesValue = dailyAllowanceItems.Sum(i => i.Value);
                model.DailyAllowances = dailyAllowanceItems
                    .SelectMany(i => i.DiagnosticItems)
                    .OfType<IntiveGmbhDailyAllowanceValueDiagnosticItem>()
                    .Select(i => new BusinessTripSettlementIntiveGmbhReportModel.DailyAllowanceModel
                    {
                        Date = i.Date,
                        Allowance = i.Allowance.Amount,
                        MealsValue = i.MealsValue.Amount,
                        CalculatedValue = i.CalculatedValue.Amount,
                        Currency = i.CalculatedValue.CurrencyIsoCode
                    })
                    .ToArray();
            }
        }

        private void FillLumpSum(BusinessTripSettlementIntiveGmbhReportModel model, CalculationResult calculationResult)
        {
            var lumpSumCostItems = calculationResult.CostItems.Where(i => i.Group == CostItemGroupType.LumpSum).ToList();

            if (lumpSumCostItems.Any())
            {
                model.LumpSumValue = lumpSumCostItems.Sum(i => i.Value);
            }
        }

        private void FillOwnCosts(BusinessTripSettlementIntiveGmbhReportModel model, CalculationResult calculationResult)
        {
            var ownCostItems = calculationResult.CostItems.Where(i => i.Group == CostItemGroupType.OwnCosts).ToList();

            if (ownCostItems.Any())
            {
                model.OwnCostsValue = ownCostItems.Sum(i => i.Value);

                var diagnosticItems = ownCostItems.SelectMany(i => i.DiagnosticItems).OfType<IntiveGmbhCostCurrencyRateMultiplicationDiagnosticItem>();

                foreach (var diagnosticItem in diagnosticItems)
                {
                    if (diagnosticItem.CostValue.CurrencyIsoCode != CurrencyIsoCodes.Euro)
                    {
                        var ownCost = model.OwnCosts.First(c => c.Name == diagnosticItem.Name && c.Value == diagnosticItem.CostValue.Amount);

                        ownCost.Value = diagnosticItem.CalculatedValue.Amount;
                    }
                }
            }
        }

        private void FillCompanyCosts(BusinessTripSettlementIntiveGmbhReportModel model, CalculationResult calculationResult)
        {
            var companyCostItems = calculationResult.CostItems.Where(i => i.Group == CostItemGroupType.CompanyCosts).ToList();

            if (companyCostItems.Any())
            {
                model.CompanyCostsValue = companyCostItems.Sum(i => i.Value);

                var diagnosticItems = companyCostItems.SelectMany(i => i.DiagnosticItems).OfType<IntiveGmbhCostCurrencyRateMultiplicationDiagnosticItem>();

                foreach (var diagnosticItem in diagnosticItems)
                {
                    if (diagnosticItem.CostValue.CurrencyIsoCode != CurrencyIsoCodes.Euro)
                    {
                        var companyCost = model.CompanyCosts.First(c => c.Name == diagnosticItem.Name && c.Value == diagnosticItem.CostValue.Amount);

                        companyCost.Value = diagnosticItem.CalculatedValue.Amount;
                    }
                }
            }
        }

        private void FillVehicleMileage(BusinessTripSettlementIntiveGmbhReportModel model, CalculationResult calculationResult)
        {
            var vehicleMileageCostItems = calculationResult.CostItems.Where(i => i.Group == CostItemGroupType.PrivateVehicleMileage).ToList();

            if (vehicleMileageCostItems.Any())
            {
                var diagnosticItems = vehicleMileageCostItems
                    .SelectMany(i => i.DiagnosticItems)
                    .OfType<KilometersMultiplicationDiagnosticItem>()
                    .ToList();

                model.VehicleMileageKilometers = diagnosticItems.Sum(i => i.Kilometers);
                model.VehicleMileageRate = diagnosticItems.Select(i => i.Rate.Amount).First();
                model.VehicleMileageValue = diagnosticItems.Sum(i => i.CalculatedValue.Amount);
            }
        }

        private void FillTotalSum(BusinessTripSettlementIntiveGmbhReportModel model, CalculationResult calculationResult)
        {
            var totalCostItems = calculationResult.CostItems.Where(i => i.Group == CostItemGroupType.BusinessTripCompensationTotal).ToList();

            model.TotalSum = totalCostItems.Sum(i => i.Value);
        }

        public static bool ShouldDataBeLimitedToOwnSettlements(IAtomicPrincipal currentPrincipal)
        {
            if (!currentPrincipal.IsInRole(SecurityRoleType.CanGenerateBusinessTripSettlementReport))
            {
                throw new MissingRequiredRoleException(new[] { SecurityRoleType.CanGenerateBusinessTripSettlementReport.ToString() });
            }

            if (currentPrincipal.IsInRole(SecurityRoleType.CanGenerateAllBusinessTripSettlementReport))
            {
                return false;
            }

            if (currentPrincipal.IsInRole(SecurityRoleType.CanGenerateMyBusinessTripSettlementReport))
            {
                return true;
            }

            throw new MissingRequiredRoleException(new[] {
                SecurityRoleType.CanGenerateAllBusinessTripSettlementReport.ToString(),
                SecurityRoleType.CanGenerateMyBusinessTripSettlementReport.ToString()}, allRolesRequired: false);
        }
    }
}
