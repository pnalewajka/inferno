﻿using System;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.BusinessTrips.Dto;
using Smt.Atomic.Business.BusinessTrips.Helpers;
using Smt.Atomic.Business.BusinessTrips.ReportModels;
using Smt.Atomic.Business.BusinessTrips.Resources;
using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.DiagnosticItems;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.BusinessTrips.ReportDataSources
{
    [Identifier("DataSources.BusinessTripSettlement")]
    [RequireRole(SecurityRoleType.CanGenerateBusinessTripSettlementReport)]
    [DefaultReportDefinition(
        BusinessTripSettlementReportHelper.ReportDataSource,
        nameof(ReportsResources.BusinessTripSettlementReportName),
        nameof(ReportsResources.BusinessTripSettlementReportDescription),
        MenuAreas.Workflow,
        MenuGroups.WorkflowReports,
        ReportingEngine.MsExcel,
        "Smt.Atomic.Business.BusinessTrips.ReportTemplates.BusinessTripSettlement.xlsx",
        "@(Model.SettlementId)-@(Model.EmployeeFullName).xlsx",
        typeof(ReportsResources))]
    public class BusinessTripSettlementDataSource : BaseReportDataSource<BusinessTripSettlementDataSourceParametersDto>
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly IApprovingPersonService _approvingPersonService;
        private readonly IUnitOfWorkService<IBusinessTripsDbScope> _unitOfWorkService;

        public BusinessTripSettlementDataSource(
            IPrincipalProvider principalProvider,
            IApprovingPersonService approvingPersonService,
            IUnitOfWorkService<IBusinessTripsDbScope> unitOfWorkService)
        {
            _principalProvider = principalProvider;
            _approvingPersonService = approvingPersonService;
            _unitOfWorkService = unitOfWorkService;
        }

        public override object GetData(ReportGenerationContext<BusinessTripSettlementDataSourceParametersDto> reportGenerationContext)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var model = unitOfWork.Repositories.BusinessTripSettlementRequests
                    .Where(s => s.SettlementDate.HasValue
                        && s.BusinessTripParticipantId == reportGenerationContext.Parameters.BusinessTripParticipantId)
                    .Select(s => new BusinessTripSettlementReportModel
                    {
                        BusinessTripId = s.BusinessTripParticipant.BusinessTripId,
                        SettlementId = s.Id,
                        EmployeeId = s.BusinessTripParticipant.EmployeeId,
                        EmployeeFullName = s.BusinessTripParticipant.Employee.FirstName + " " + s.BusinessTripParticipant.Employee.LastName,
                        EmployeeAddress = s.BusinessTripParticipant.HomeAddress,
                        Description = s.BusinessTripParticipant.BusinessTrip.TravelExplanation,
                        IsDomestic = s.BusinessTripParticipant.Employee.Location.City.CountryId == s.BusinessTripParticipant.BusinessTrip.DestinationCity.CountryId,
                        DepartureCity = s.BusinessTripParticipant.Employee.Location.City.Name + ", " + s.BusinessTripParticipant.Employee.Location.City.Country.Name,
                        DestinationCity = s.BusinessTripParticipant.BusinessTrip.DestinationCity.Name + ", " + s.BusinessTripParticipant.BusinessTrip.DestinationCity.Country.Name,
                        DepartureDate = s.DepartureDateTime ?? s.BusinessTripParticipant.BusinessTrip.StartedOn,
                        ArrivalDate = s.ArrivalDateTime ?? s.BusinessTripParticipant.BusinessTrip.EndedOn,
                        BorderCrossingDepartureDate = s.AbroadTimeEntries.Any() ? s.AbroadTimeEntries.Min(e => e.DepartureDateTime) : (DateTime?)null,
                        BorderCrossingArrivalDate = s.AbroadTimeEntries.Any() ? s.AbroadTimeEntries.Max(e => e.DepartureDateTime) : (DateTime?)null,
                        Breakfasts = s.Meals.Any() ? s.Meals.Sum(m => m.BreakfastCount) : 0,
                        Lunches = s.Meals.Any() ? s.Meals.Sum(m => m.LunchCount) : 0,
                        Dinners = s.Meals.Any() ? s.Meals.Sum(m => m.DinnerCount) : 0,
                        RegistrationNumber = s.PrivateVehicleMileage.RegistrationNumber,
                        IsCarEngineCapacityOver900cc = s.PrivateVehicleMileage.IsCarEngineCapacityOver900cc,
                        InboundFromCity = s.PrivateVehicleMileage.InboundFromCity == null ? null
                            : s.PrivateVehicleMileage.InboundFromCity.Name + ", " + s.PrivateVehicleMileage.InboundFromCity.Country.Name,
                        InboundToCity = s.PrivateVehicleMileage.InboundToCity == null ? null
                            : s.PrivateVehicleMileage.InboundToCity.Name + ", " + s.PrivateVehicleMileage.InboundToCity.Country.Name,
                        InboundKilometers = s.PrivateVehicleMileage.InboundKilometers,
                        OutboundFromCity = s.PrivateVehicleMileage.OutboundFromCity == null ? null
                            : s.PrivateVehicleMileage.OutboundFromCity.Name + ", " + s.PrivateVehicleMileage.OutboundFromCity.Country.Name,
                        OutboundToCity = s.PrivateVehicleMileage.OutboundToCity == null ? null
                            : s.PrivateVehicleMileage.OutboundToCity.Name + ", " + s.PrivateVehicleMileage.OutboundToCity.Country.Name,
                        OutboundKilometers = s.PrivateVehicleMileage.OutboundKilometers,
                        SettlementDate = s.SettlementDate.Value,
                        Status = s.Request.Status,
                        CalculationResult = s.CalculationResult
                    })
                    .FirstOrDefault();

                if (model == null)
                {
                    throw new BusinessException(ReportsResources.NoSettlementForReportError);
                }

                var currentPrincipal = _principalProvider.Current;

                if (ShouldDataBeLimitedToOwnSettlements(currentPrincipal) && model.EmployeeId != currentPrincipal.EmployeeId)
                {
                    throw new MissingRequiredRoleException(new[] { SecurityRoleType.CanGenerateAllBusinessTripSettlementReport.ToString() });
                }

                FillAdditionalMultiRowData(unitOfWork, model);
                FillCalculationResultData(model);

                model.EmployeeAddress = !string.IsNullOrEmpty(model.RegistrationNumber)
                    ? model.EmployeeAddress?.NormalizeEndLinesToUnix()?.Replace("\n", "; ")
                    : null;

                return model;
            }
        }

        public override BusinessTripSettlementDataSourceParametersDto GetDefaultParameters(ReportGenerationContext<BusinessTripSettlementDataSourceParametersDto> reportGenerationContext)
        {
            return new BusinessTripSettlementDataSourceParametersDto();
        }

        private void FillAdditionalMultiRowData(IUnitOfWork<IBusinessTripsDbScope> unitOfWork, BusinessTripSettlementReportModel model)
        {
            var projects = unitOfWork.Repositories.BusinessTrips
                .Where(t => t.Id == model.BusinessTripId)
                .SelectMany(t => t.Projects.Select(p => new { p.Id, p.Apn }))
                .ToList();

            var approverIds = _approvingPersonService.GetStandardCostApprovers(new[] { model.EmployeeId }, projects.Select(p => p.Id));

            var projectManagers = unitOfWork.Repositories.Employees
                .Where(e => approverIds.Contains(e.Id))
                .Select(EmployeeBusinessLogic.FullName.AsExpression())
                .ToList();

            var ownCosts = unitOfWork.Repositories.BusinessTripSettlementRequests
                .Where(s => s.Id == model.SettlementId)
                .SelectMany(s => s.OwnCosts.Select(c => new BusinessTripSettlementBaseReportModel.CostModel
                {
                    Name = c.Description,
                    Value = c.Amount,
                    Currency = CurrencyIsoCodes.PolishZloty,
                    HasInvoiceIssued = c.HasInvoiceIssued,
                    Date = c.TransactionDate
                }))  
                .AsEnumerable()
                .Select(c => { c.Culture = model.Culture; return c; })
                .ToArray();

            var companyCosts = unitOfWork.Repositories.BusinessTripSettlementRequests
                .Where(s => s.Id == model.SettlementId)
                .SelectMany(s => s.CompanyCosts.Select(c => new BusinessTripSettlementBaseReportModel.CostModel
                {
                    Name = c.Description,
                    Value = c.Amount,
                    PaymentMethod = c.PaymentMethod,
                    CardHolder = c.CardHolder == null ? null : c.CardHolder.FirstName + " " + c.CardHolder.LastName,
                    Date = c.TransactionDate
                }))
                .AsEnumerable()
                .Select(c => { c.Culture = model.Culture; return c; })
                .ToArray();

            model.OwnCosts = ownCosts;
            model.CompanyCosts = companyCosts;

            model.ProjectApn = string.Join(", ", projects.Select(p => p.Apn));
            model.ProjectManagers = string.Join(", ", projectManagers);
        }

        private void FillCalculationResultData(BusinessTripSettlementReportModel model)
        {
            if (model.CalculationResult == null)
            {
                return;
            }

            var calculationResult = JsonHelper.Deserialize<CalculationResult>(model.CalculationResult);

            if (calculationResult.CostItems.Any(i => i.CurrencyIsoCode != CurrencyIsoCodes.PolishZloty))
            {
                throw new BusinessException(ReportsResources.WrongCurrencyPlnForReportError);
            }

            FillDailyAllowances(model, calculationResult);
            FillLumpSum(model, calculationResult);
            FillOwnCosts(model, calculationResult);
            FillCompanyCosts(model, calculationResult);
            FillVehicleMileage(model, calculationResult);
            FillAdvancePayment(model, calculationResult);
            FillTotalSum(model, calculationResult);
        }

        private void FillDailyAllowances(BusinessTripSettlementReportModel model, CalculationResult calculationResult)
        {
            var dailyAllowancesItems = calculationResult.CostItems.Where(i => i.Group == CostItemGroupType.DailyAllowances).ToList();

            if (dailyAllowancesItems.Any())
            {
                const string domesticCountry = "Poland";

                var diagnosticItems = dailyAllowancesItems
                    .SelectMany(i => i.DiagnosticItems)
                    .OfType<DailyAllowanceValueDiagnosticItem>()
                    .ToList();

                var domesticItems = diagnosticItems.Where(i => i.Country == domesticCountry);
                var foreignItems = diagnosticItems.Where(i => i.Country != domesticCountry);

                model.DailyAllowancesValue = dailyAllowancesItems.Sum(i => i.Value);
                model.DomesticDailyAllowances = domesticItems.Sum(i => i.AllowanceUnits);
                model.ForeignDailyAllowances = foreignItems.Sum(i => i.AllowanceUnits);
                model.DomesticDailyAllowancesValue = domesticItems.Sum(i => i.CalculatedValue.Amount);
                model.ForeignDailyAllowancesValue = foreignItems.Sum(i => i.CalculatedValue.Amount);
            }
        }

        private void FillLumpSum(BusinessTripSettlementReportModel model, CalculationResult calculationResult)
        {
            var lumpSumCostItems = calculationResult.CostItems.Where(i => i.Group == CostItemGroupType.LumpSum).ToList();

            if (lumpSumCostItems.Any())
            {
                var lumpSumValue = lumpSumCostItems.Sum(i => i.Value);

                model.LumpSumValue = lumpSumValue;
                model.DailyAllowancesValue += lumpSumValue;
            }
        }

        private void FillOwnCosts(BusinessTripSettlementReportModel model, CalculationResult calculationResult)
        {
            var ownCostItems = calculationResult.CostItems.Where(i => i.Group == CostItemGroupType.OwnCosts).ToList();

            if (ownCostItems.Any())
            {
                model.OwnCostsValue = ownCostItems.Sum(i => i.Value);

                var diagnosticItems = ownCostItems.SelectMany(i => i.DiagnosticItems).OfType<CostCurrencyRateMultiplicationDiagnosticItem>();

                foreach (var diagnosticItem in diagnosticItems)
                {
                    if (diagnosticItem.CostValue.CurrencyIsoCode != CurrencyIsoCodes.PolishZloty)
                    {
                        var ownCost = model.OwnCosts.First(c => c.Name == diagnosticItem.Name && c.Value == diagnosticItem.CostValue.Amount);

                        ownCost.Value = diagnosticItem.CalculatedValue.Amount;
                    }
                }
            }
        }

        private void FillCompanyCosts(BusinessTripSettlementReportModel model, CalculationResult calculationResult)
        {
            var companyCostItems = calculationResult.CostItems.Where(i => i.Group == CostItemGroupType.CompanyCosts).ToList();

            if (companyCostItems.Any())
            {
                model.CompanyCostsValue = companyCostItems.Sum(i => i.Value);

                var diagnosticItems = companyCostItems.SelectMany(i => i.DiagnosticItems).OfType<CostCurrencyRateMultiplicationDiagnosticItem>();

                foreach (var diagnosticItem in diagnosticItems)
                {
                    if (diagnosticItem.CostValue.CurrencyIsoCode != CurrencyIsoCodes.PolishZloty)
                    {
                        var companyCost = model.CompanyCosts.First(c => c.Name == diagnosticItem.Name && c.Value == diagnosticItem.CostValue.Amount);

                        companyCost.Value = diagnosticItem.CalculatedValue.Amount;
                    }
                }
            }
        }

        private void FillVehicleMileage(BusinessTripSettlementReportModel model, CalculationResult calculationResult)
        {
            var vehicleMileageCostItems = calculationResult.CostItems.Where(i => i.Group == CostItemGroupType.PrivateVehicleMileage).ToList();

            if (vehicleMileageCostItems.Any())
            {
                var diagnosticItems = vehicleMileageCostItems
                    .SelectMany(i => i.DiagnosticItems)
                    .OfType<KilometersMultiplicationDiagnosticItem>()
                    .ToList();

                model.VehicleMileageValue = vehicleMileageCostItems.Sum(i => i.Value);

                model.InboundRate = diagnosticItems[0].Rate.Amount;
                model.InboundValue = diagnosticItems[0].CalculatedValue.Amount;
                model.OutboundRate = diagnosticItems[1].Rate.Amount;
                model.OutboundValue = diagnosticItems[1].CalculatedValue.Amount;
            }
        }

        private void FillAdvancePayment(BusinessTripSettlementReportModel model, CalculationResult calculationResult)
        {
            var advancePaymentCostItems = calculationResult.CostItems
                .Where(i => i.Group == CostItemGroupType.AdditionalAdvancePayment || i.Group == CostItemGroupType.AdvancePayments)
                .ToList();

            if (advancePaymentCostItems.Any())
            {
                model.AdvancePaymentValue = advancePaymentCostItems.Sum(i => i.Value);
            }
        }

        private void FillTotalSum(BusinessTripSettlementReportModel model, CalculationResult calculationResult)
        {
            var totalCostItems = calculationResult.CostItems.Where(i => i.Group == CostItemGroupType.BusinessTripCompensationTotal).ToList();

            model.TotalSum = totalCostItems.Sum(i => i.Value);
        }

        public static bool ShouldDataBeLimitedToOwnSettlements(IAtomicPrincipal currentPrincipal)
        {
            if (!currentPrincipal.IsInRole(SecurityRoleType.CanGenerateBusinessTripSettlementReport))
            {
                throw new MissingRequiredRoleException(new[] { SecurityRoleType.CanGenerateBusinessTripSettlementReport.ToString() });
            }

            if (currentPrincipal.IsInRole(SecurityRoleType.CanGenerateAllBusinessTripSettlementReport))
            {
                return false;
            }

            if (currentPrincipal.IsInRole(SecurityRoleType.CanGenerateMyBusinessTripSettlementReport))
            {
                return true;
            }

            throw new MissingRequiredRoleException(new[] {
                SecurityRoleType.CanGenerateAllBusinessTripSettlementReport.ToString(),
                SecurityRoleType.CanGenerateMyBusinessTripSettlementReport.ToString()}, allRolesRequired: false);
        }
    }
}
