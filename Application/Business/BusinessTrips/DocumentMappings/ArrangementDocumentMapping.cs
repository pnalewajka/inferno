using Smt.Atomic.Business.Common.Abstracts;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.BusinessTrips.DocumentMappings
{
    [Identifier("Documents.ArrangementDocumentMapping")]
    public class ArrangementDocumentMapping :
        DocumentMapping<ArrangementDocument, ArrangementDocumentContent, IBusinessTripsDbScope>
    {
        public ArrangementDocumentMapping(IUnitOfWorkService<IBusinessTripsDbScope> unitOfWorkService)
            : base(unitOfWorkService)
        {
            NameColumnExpression = e => e.Name;
            ContentTypeColumnExpression = e => e.ContentType;
            ContentLengthColumnExpression = e => e.ContentLength;
            ContentColumnExpression = e => e.Content;
        }
    }
}