﻿using Smt.Atomic.Business.BusinessTrips.Resources;

namespace Smt.Atomic.Business.BusinessTrips.Helpers
{
    public static class BusinessTripSettlementReportHelper
    {
        public static readonly string ZipFileName = ReportsResources.SettlementsZipFile;

        public const string ReportDataSource = "BusinessTripSettlement";
        public const string IntiveGmbhReportDataSource = "BusinessTripSettlementIntiveGmbh";
    }
}
