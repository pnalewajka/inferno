﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.BusinessTrips.EmailModels
{
    public class SettlementControllingStatusChangedEmailModel
    {
        public long BusinessTripId { get; set; }

        public long[] SettlementRequestIds { get; set; }

        public string BusinessTripSettlementListUrl { get; set; }

        public BusinessTripSettlementControllingStatus NewControllingStatus { get; set; }
    }
}
