﻿namespace Smt.Atomic.Business.BusinessTrips.EmailModels
{
    public class BusinessTripCommunicationAddedEmailModel
    {
        public string EmployeeFullName { get; set; }

        public string Message { get; set; }

        public string CommunicationUrl { get; set; }

        public string BusinessTripsUrl { get; set; }
    }
}
