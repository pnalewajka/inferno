﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.BusinessTrips.EmailModels
{
    public class BusinessTripAssignmentEmailModel
    {
        public BusinessTripAssignmentEmailModel()
        {
            BusinessTripIdToUrlDictionary = new Dictionary<string, string>();
        }

        public Dictionary<string, string> BusinessTripIdToUrlDictionary { get; set; }

        public string BusinessTripsUrl { get; set; }

        public string EmployeeFullName { get; set; }
    }
}
