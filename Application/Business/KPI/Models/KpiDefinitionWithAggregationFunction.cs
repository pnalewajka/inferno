﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.KPI.Models
{
    public class KpiDefinitionWithAggregationFunction
    {
        private static readonly long moduloAggregationFunctionValue = (long)AggregationFunction.Avg + 1;

        public KpiDefinitionWithAggregationFunction()
        {

        }

        public KpiDefinitionWithAggregationFunction(long kpiDefinitionId, AggregationFunction aggregationFunction)
        {
            KpiDefinitionId = kpiDefinitionId;
            AggregationFunction = aggregationFunction;
        }

        public long KpiDefinitionId { get; set; }

        public AggregationFunction AggregationFunction { get; set; }

        public long CombinedId => moduloAggregationFunctionValue * KpiDefinitionId + (int)AggregationFunction;

        public static KpiDefinitionWithAggregationFunction FromCombinedId(long id)
        {
            return new KpiDefinitionWithAggregationFunction()
            {
                KpiDefinitionId = (id - (id % moduloAggregationFunctionValue)) / moduloAggregationFunctionValue,
                AggregationFunction = (AggregationFunction)(id % moduloAggregationFunctionValue),
            };
        }
    }
}
