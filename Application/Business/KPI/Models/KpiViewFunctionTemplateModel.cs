﻿using System.Collections.Generic;
using System.Linq;
using RazorEngine.Text;
using Smt.Atomic.Business.KPI.Dto;

namespace Smt.Atomic.Business.KPI.Models
{
    public class KpiViewFunctionTemplateModel
    {
        private const string _columnSeparator = ",\n";
        private const string _schemaName = "KPI";
        private readonly string _entityName;

        public string[] Columns { get; set; }

        public IEnumerable<KpiDefinitionDto> KpiDefinitions { get; set; }

        public string ViewName => $"{_schemaName}.{_entityName}KPI";

        public string FunctionName => $"{_schemaName}.Calculate{_entityName}KPIs";

        public string ProcedureName => $"{FunctionName}ForDateRange";

        public string ResultTableName => $"Calculate{_entityName}KPIsForDateRangeResult";

        public string Table { get; set; }

        public string Alias { get; set; }

        public KpiViewFunctionTemplateModel(
            string[] columns,
            string tableName,
            string alias,
            IEnumerable<KpiDefinitionDto> kpiDefinitions,
            string entityName)
        {
            Table = tableName;
            Alias = alias;
            Columns = columns;
            KpiDefinitions = kpiDefinitions;
            _entityName = entityName;
        }

        public RawString GetSelectColumns()
        {
            var tableColumns = GetTableColumns();
            var kpiColumns = GetKpiColumns();

            return new RawString((
                    tableColumns
                    + (string.IsNullOrWhiteSpace(tableColumns) || string.IsNullOrWhiteSpace(kpiColumns) ? "" : _columnSeparator)
                    + kpiColumns)
                .Replace("'", "''"));
        }

        private string GetTableColumns()
        {
            return string.Join(_columnSeparator, Columns.Select(t => Alias + '.' + t));
        }

        private string GetKpiColumns()
        {
            return string.Join(
                _columnSeparator,
                KpiDefinitions.Select(k => GetSelectFromKpiDefinition(k)));
        }

        private string GetSelectFromKpiDefinition(KpiDefinitionDto kpiDefinition)
        {
            return $"({kpiDefinition.SqlFormula}) AS '{kpiDefinition.Name}'";
        }
    }
}
