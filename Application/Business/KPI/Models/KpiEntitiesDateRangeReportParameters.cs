﻿using System;
using Smt.Atomic.Business.KPI.Dto;

namespace Smt.Atomic.Business.KPI.Models
{
    public class KpiEntitiesDateRangeReportParameters : KpiEntityParametersBaseDto
    {
        public DateTime ReportStartDate { get; set; }

        public DateTime ReportEndDate { get; set; }
    }
}
