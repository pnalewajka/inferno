﻿using System;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Helpers;

namespace Smt.Atomic.Business.KPI.Extensions
{
    public static class QueryableEmployeeExtensions
    {
        public static IQueryable<Employee> FilterByIds(this IQueryable<Employee> query, long[] ids)
        {
            return query.AddFilterConditionally(
                !ids.IsNullOrEmpty(),
                q => q.GetByIds(ids));
        }

        public static IQueryable<Employee> FilterByStatus(this IQueryable<Employee> query, EmployeeStatus? status)
        {
            return query.AddFilterConditionally(
                status.HasValue,
                q => q.Where(e => e.CurrentEmployeeStatus == status));
        }

        public static IQueryable<Employee> FilterByOrgUnitIds(this IQueryable<Employee> query, long[] orgUnitIds)
        {
            return query.AddFilterConditionally(
                !orgUnitIds.IsNullOrEmpty(),
                q => q.Where(e => orgUnitIds.Contains(e.OrgUnitId)));
        }

        public static IQueryable<Employee> FilterByLineManagerId(this IQueryable<Employee> query, long? lineManagerId)
        {
            return query.AddFilterConditionally(
                lineManagerId.HasValue,
                q => q.Where(e => e.LineManagerId.HasValue && lineManagerId.Value == e.LineManagerId.Value));
        }

        public static IQueryable<Employee> FilterByLocationIds(this IQueryable<Employee> query, long[] locationIds)
        {
            return query.AddFilterConditionally(
                !locationIds.IsNullOrEmpty(),
                q => q.Where(e => e.LocationId.HasValue && locationIds.Contains(e.LocationId.Value)));
        }

        public static IQueryable<Employee> FilterByContractTypeIds(this IQueryable<Employee> query, DateTime date, long[] contractTypeIds)
        {
            var contractTypeExpression = EmploymentPeriodBusinessLogic.HasContractType.Parametrize(date, contractTypeIds);

            return query.AddFilterConditionally(
                !contractTypeIds.IsNullOrEmpty(),
                q => q.Where(e =>
                e.EmploymentPeriods.AsQueryable().Any(contractTypeExpression)));
        }
    }
}
