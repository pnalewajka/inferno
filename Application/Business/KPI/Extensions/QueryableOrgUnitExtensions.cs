﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Repositories.Helpers;

namespace Smt.Atomic.Business.KPI.Extensions
{
    public static class QueryableOrgUnitExtensions
    {
        public static IQueryable<OrgUnit> FilterByIds(this IQueryable<OrgUnit> query, long[] ids)
        {
            return query.AddFilterConditionally(
                !ids.IsNullOrEmpty(),
                q => q.GetByIds(ids));
        }

        public static IQueryable<OrgUnit> FilterByManagerIds(this IQueryable<OrgUnit> query, long[] unitManagerIds)
        {
            return query.AddFilterConditionally(
                !unitManagerIds.IsNullOrEmpty(),
                q => q.Where(o => o.EmployeeId.HasValue && unitManagerIds.Contains(o.EmployeeId.Value)));
        }
    }
}
