﻿using System;
using System.Linq;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Business.KPI.Extensions
{
    public static class QueryableExtensions
    {
        public static IQueryable<TEntity> AddFilterConditionally<TEntity>(
            this IQueryable<TEntity> query,
            bool condition,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> filteringExpression) where TEntity : IEntity
        {
            if (condition)
            {
                return filteringExpression.Invoke(query);
            }

            return query;
        }
    }
}
