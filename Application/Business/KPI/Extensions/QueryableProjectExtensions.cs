﻿using System;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Helpers;

namespace Smt.Atomic.Business.KPI.Extensions
{
    public static class QueryableProjectExtensions
    {
        public static IQueryable<Project> FilterByIds(this IQueryable<Project> query, long[] ids)
        {
            return query.AddFilterConditionally(
                !ids.IsNullOrEmpty(),
                q => q.GetByIds(ids));
        }

        public static IQueryable<Project> FilterByProjectManagerId(this IQueryable<Project> query, long? projectManagerId)
        {
            return query.AddFilterConditionally(
                projectManagerId.HasValue,
                q => q.Where(p => p.ProjectSetup.ProjectManagerId.HasValue && projectManagerId.Value == p.ProjectSetup.ProjectManagerId.Value));
        }

        public static IQueryable<Project> FilterByOrgUnitIds(this IQueryable<Project> query, long[] orgUnitIds)
        {
            return query.AddFilterConditionally(
                !orgUnitIds.IsNullOrEmpty(),
                q => q.Where(p => orgUnitIds.Contains(p.ProjectSetup.OrgUnitId)));
        }

        public static IQueryable<Project> FilterByProjectDate(this IQueryable<Project> query, DateTime? projectDate)
        {
            return query.AddFilterConditionally(
                projectDate.HasValue,
                q => q.Where(p => p.StartDate.HasValue && p.StartDate.Value <= projectDate.Value
                    && (!p.EndDate.HasValue || p.EndDate.Value <= projectDate.Value)));
        }
    }
}
