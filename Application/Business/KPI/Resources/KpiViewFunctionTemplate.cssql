﻿@model Smt.Atomic.Business.KPI.Models.KpiViewFunctionTemplateModel

BEGIN TRANSACTION

DROP FUNCTION IF EXISTS @Model.FunctionName

EXEC
('
    CREATE FUNCTION @Model.FunctionName
    (
        @@date DATETIME
    )
    RETURNS TABLE
    AS
    RETURN
    (
        SELECT
        @Model.GetSelectColumns()
        FROM
        @Model.Table @(Model.Alias)
    )
')

IF @Raw("@@")error <> 0
BEGIN
    ROLLBACK TRANSACTION
    RETURN
END

DROP VIEW IF EXISTS @Model.ViewName

EXEC
('
    CREATE VIEW @Model.ViewName AS SELECT * FROM @(Model.FunctionName)(GETDATE())
')

DROP PROCEDURE IF EXISTS @Model.ProcedureName

EXEC
('
    CREATE PROCEDURE @Model.ProcedureName
        @@startDate DATE,
        @@endDate DATE,
        @@entityIds CollectionItems READONLY
    AS
    BEGIN
        DROP TABLE IF EXISTS ##@Model.ResultTableName
        SELECT *, @@startDate AS ReportRecordDate INTO ##@Model.ResultTableName FROM @(Model.FunctionName)(@@startDate) WHERE Id IN (SELECT * FROM @@entityIds)

        WHILE (@@startDate < @@endDate)
        BEGIN
            SET @@startDate = DATEADD(day, 1, @@startDate)
            INSERT INTO ##@Model.ResultTableName SELECT *, @@startDate AS ReportRecordDate FROM @(Model.FunctionName)(@@startDate) WHERE Id IN (SELECT * FROM @@entityIds)
        END
    END
')

COMMIT