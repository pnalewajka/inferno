﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.Business.KPI.Services
{
    public class KpiDefinitionContext
    {
        [UrlFieldName("entity")]
        public KpiEntity Entity { get; set; }
    }
}
