﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.KPI.Dto;
using Smt.Atomic.Business.KPI.Interfaces;
using Smt.Atomic.Business.KPI.Models;
using Smt.Atomic.Business.KPI.Resources;
using Smt.Atomic.CrossCutting.Business.Attributes;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.KPI;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.KPI.Services
{
    public class KpiSqlService : IKpiSqlService
    {
        private IRazorTemplateService _razorTemplateService;
        private IUnitOfWorkService<IKPIDbScope> _unitOfWork;

        public KpiSqlService(IUnitOfWorkService<IKPIDbScope> unitOfWork, IRazorTemplateService razorTemplateService)
        {
            _unitOfWork = unitOfWork;
            _razorTemplateService = razorTemplateService;
        }

        public string RecordDateColumnName => "ReportRecordDate";

        public void UpdateKpiViewAndFunction(KpiEntity entity, IEnumerable<KpiDefinitionDto> kpiDefinitions)
        {
            var kpiEntityAttribute = AttributeHelper.GetEnumFieldAttribute<KpiEntityAttribute>(entity);

            var model = new KpiViewFunctionTemplateModel(
                kpiEntityAttribute.Columns,
                kpiEntityAttribute.Table,
                kpiEntityAttribute.Alias,
                kpiDefinitions,
                entity.ToString());

            var sqlCommand = _razorTemplateService.TemplateRunAndCompile(
                "kpiViewFunctionTemplate",
                KpiDefinitionTemplateResources.KpiViewFunctionTemplate,
                model);

            using (var extendedUnitOfWork = _unitOfWork.CreateExtended())
            {
                extendedUnitOfWork.ExecuteSqlCommand(sqlCommand);
            }
        }

        public KpiFunctionResultDto GetKpiFunctionResultForAggregatedDateRange(KpiDefinitionWithAggregationFunction[] kpiDefinitions, KpiEntity entity, DateTime reportStartDate, DateTime reportEndDate, long[] entityInstancesId)
        {
            if (entityInstancesId.IsNullOrEmpty())
            {
                return EmptyResult();
            }

            var outerColumns = GetAggregationColumns(kpiDefinitions, entity);
            var columns = GetSelectColumns(entity, kpiDefinitions.Select(k => k.KpiDefinitionId).ToArray());

            var queries = GenerateQueriesForDateRange(reportStartDate, reportEndDate, columns, entity, entityInstancesId);
            var query = $"SELECT {string.Join(", ", outerColumns)} FROM ({string.Join(" UNION ", queries)}) AS Subresults GROUP BY {string.Join(", ", GetEntityColumns(entity))}";
            var result = ExecuteSqlQuery(query, new Dictionary<string, object>());

            return result;
        }

        public KpiFunctionResultDto GetKpiFunctionResultForDateRange(long[] kpiDefinitionIds, KpiEntity entity, DateTime reportStartDate, DateTime reportEndDate, long[] entityInstancesId)
        {
            if (entityInstancesId.IsNullOrEmpty())
            {
                return EmptyResult();
            }

            var columns = GetSelectColumns(entity, kpiDefinitionIds);
            var query = GetStoredProcedureQuery(entityInstancesId, entity, reportStartDate, reportEndDate, columns, false);
            var result = ExecuteSqlQuery(query, new Dictionary<string, object>());

            return result;
        }

        public KpiFunctionResultDto GetKpiFunctionResultForDateRange(KpiDefinitionWithAggregationFunction[] kpiDefinitions, KpiEntity entity, DateTime reportStartDate, DateTime reportEndDate, long[] entityInstancesId)
        {
            if (entityInstancesId.IsNullOrEmpty())
            {
                return EmptyResult();
            }

            var columns = GetKpiAggregationColumns(kpiDefinitions);
            var query = GetStoredProcedureQuery(entityInstancesId, entity, reportStartDate, reportEndDate, columns, true);
            var result = ExecuteSqlQuery(query, new Dictionary<string, object>());

            return result;
        }

        public KpiFunctionResultDto GetKpiFunctionResult(long[] kpiDefinitionIds, KpiEntity entity, DateTime reportDate, long[] entityInstancesId)
        {
            if (entityInstancesId.IsNullOrEmpty())
            {
                return EmptyResult();
            }

            var functionName = $"KPI.Calculate{entity.ToString()}KPIs('{reportDate.ToDefaultDateFormat()}')";

            return ExecuteSqlQuery($"SELECT {string.Join(", ", GetSelectColumns(entity, kpiDefinitionIds))} FROM {functionName} WHERE Id IN ({string.Join(", ", entityInstancesId)})", new Dictionary<string, object>());
        }

        public ICollection<string> GetKpiColumnNames(ICollection<long> kpiDefinitionIds)
        {
            using (var unitOfWork = _unitOfWork.Create())
            {
                return unitOfWork.Repositories.KpiDefinitions
                            .Where(k => kpiDefinitionIds.Contains(k.Id)).Select(k => k.Name).ToList();
            }
        }

        public ICollection<string> GetKpiAggregationColumnNames(KpiDefinitionWithAggregationFunction[] kpiDefinitions)
        {
            using (var unitOfWork = _unitOfWork.Create())
            {
                return kpiDefinitions.Select(kpi =>
                {
                    var kpiDefinition = GetKpiDefinitionById(unitOfWork, kpi.KpiDefinitionId);

                    return GetKpiAlias(kpiDefinition.Name, kpi.AggregationFunction);
                }).ToList();
            }
        }

        private string GetStoredProcedureQuery(long[] entityInstancesId, KpiEntity entity, DateTime reportStartDate, DateTime reportEndDate, List<string> columns, bool groupByDate)
        {
            const int maxInserts = 1000;
            var inserts = new List<string>();

            for (int i = 0; i < entityInstancesId.Length; i += maxInserts)
            {
                inserts.Add($"INSERT INTO @entityIds VALUES {string.Join(", ", entityInstancesId.Skip(i).Take(maxInserts).Select(k => $"({k})"))}");
            }

            return "BEGIN TRANSACTION "
                + "DECLARE @entityIds CollectionItems "
                + $"{string.Join(" ", inserts)}"
                + $"EXEC KPI.Calculate{entity.ToString()}KPIsForDateRange '{reportStartDate.ToDefaultDateFormat()}', '{reportEndDate.ToDefaultDateFormat()}', @entityIds "
                + $"SELECT {RecordDateColumnName}, {string.Join(", ", columns)} FROM ##Calculate{entity.ToString()}KPIsForDateRangeResult {(groupByDate ? $"GROUP BY {RecordDateColumnName} " : "")}"
                + $"ORDER BY {(columns.Contains("Id") ? "Id," : "")} {RecordDateColumnName} "
                + "COMMIT";
        }

        private List<string> GenerateQueriesForDateRange(DateTime startDate, DateTime endDate, IEnumerable<string> columns, KpiEntity entity, long[] entityInstancesId)
        {
            var selectQuery = string.Join(", ", columns);
            var functionName = $"KPI.Calculate{entity.ToString()}KPIs(";
            var entityIds = string.Join(", ", entityInstancesId);
            var entitiesWhere = string.IsNullOrEmpty(entityIds) ? "" : $"WHERE Id IN({entityIds})";
            var queries = new List<string>();

            for (var reportDate = startDate; reportDate <= endDate; reportDate = reportDate.AddDays(1))
            {
                var reportDateFormatted = reportDate.ToDefaultDateFormat();
                queries.Add($"SELECT '{reportDateFormatted}' AS {RecordDateColumnName}, {selectQuery} FROM {functionName}'{reportDateFormatted}') {entitiesWhere}");
            }

            return queries;
        }

        private KpiFunctionResultDto ExecuteSqlQuery(string sqlQuery, IDictionary<string, object> parameters)
        {
            using (var extendedUnitOfWork = _unitOfWork.CreateExtended())
            {
                var queryResult = extendedUnitOfWork.SqlQueryToDataSet(sqlQuery, parameters);

                return SingleTableDatasetToKpiFunctionResultDto(queryResult);
            }
        }

        private KpiFunctionResultDto SingleTableDatasetToKpiFunctionResultDto(DataSet dataSet)
        {
            return new KpiFunctionResultDto()
            {
                Records = dataSet.Tables[0].AsEnumerable()
                     .Select(r => (IDictionary<string, object>)r.Table.Columns.Cast<DataColumn>().ToDictionary(c => c.ColumnName, c => r[c]))
                     .ToList(),
            };
        }

        private KpiFunctionResultDto EmptyResult()
        {
            return new KpiFunctionResultDto
            {
                Records = new List<IDictionary<string, object>>(),
            };
        }

        private List<string> GetSelectColumns(KpiEntity entity, long[] kpiDefinitionIds)
        {
            using (var unitOfWork = _unitOfWork.Create())
            {
                var entityColumns = GetEntityColumns(entity);
                var kpiColumnNames = GetKpiColumnNames(kpiDefinitionIds);

                return entityColumns
                    .Union(kpiColumnNames.Select(k => $"[{k}]")).ToList();
            }
        }

        private List<string> GetKpiAggregationColumns(KpiDefinitionWithAggregationFunction[] kpiDefinitions)
        {
            using (var unitOfWork = _unitOfWork.Create())
            {
                return kpiDefinitions.Select(kpi =>
                {
                    var kpiDefinition = GetKpiDefinitionById(unitOfWork, kpi.KpiDefinitionId);

                    return GetAggregationColumn(kpi.AggregationFunction, kpiDefinition.Name);
                }).ToList();
            }
        }

        private List<string> GetAggregationColumns(KpiDefinitionWithAggregationFunction[] kpiDefinitions, KpiEntity entity)
        {
            var entityColumns = GetEntityColumns(entity);

            return GetKpiAggregationColumns(kpiDefinitions).Union(entityColumns).ToList();
        }

        private string[] GetEntityColumns(KpiEntity entity)
        {
            return AttributeHelper.GetEnumFieldAttribute<KpiEntityAttribute>(entity).Columns;
        }

        private KpiDefinition GetKpiDefinitionById(IUnitOfWork<IKPIDbScope> unitOfWork, long id)
        {
            return unitOfWork.Repositories.KpiDefinitions.Filtered().GetById(id);
        }

        private string GetAggregationColumn(AggregationFunction aggregationFunction, string columnName)
        {
            return $"{aggregationFunction.ToString()}(CAST([{columnName}] AS FLOAT)) AS '{GetKpiAlias(columnName, aggregationFunction)}'";
        }

        private string GetKpiAlias(string columnName, AggregationFunction aggregationFunction)
        {
            return $"{columnName} ({aggregationFunction.ToString()})";
        }
    }
}
