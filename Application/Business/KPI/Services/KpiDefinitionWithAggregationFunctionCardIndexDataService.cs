﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.KPI.Dto;
using Smt.Atomic.Business.KPI.Interfaces;
using Smt.Atomic.Business.KPI.Models;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.KPI.Services
{
    public class KpiDefinitionWithAggregationFunctionCardIndexDataService : CustomDataCardIndexDataService<KpiDefinitionWithAggregationFunctionDto>, IKpiDefinitionWithAggregationFunctionCardIndexDataService
    {
        private readonly IUnitOfWorkService<IKPIDbScope> _unitOfWorkKpi;

        public KpiDefinitionWithAggregationFunctionCardIndexDataService(IUnitOfWorkService<IKPIDbScope> unitOfWorkKpi)
        {
            _unitOfWorkKpi = unitOfWorkKpi;
        }

        public override KpiDefinitionWithAggregationFunctionDto GetRecordById(long id)
        {
            return GetAllRecords().Single(r => r.Id == id);
        }

        public override KpiDefinitionWithAggregationFunctionDto GetRecordByIdOrDefault(long id)
        {
            return GetAllRecords().SingleOrDefault(r => r.Id == id);
        }

        protected override IList<KpiDefinitionWithAggregationFunctionDto> GetAllRecords(object context = null)
        {
            using (var unitOfWork = _unitOfWorkKpi.Create())
            {
                var kpiDefinitions = unitOfWork.Repositories.KpiDefinitions.AsNoTracking().ToList();
                var aggregateFunctions = EnumHelper.GetEnumValues<AggregationFunction>();

                return kpiDefinitions
                    .SelectMany(kpi => aggregateFunctions
                        .Select(function =>
                            new KpiDefinitionWithAggregationFunctionDto()
                            {
                                Id = new KpiDefinitionWithAggregationFunction(kpi.Id, function).CombinedId,
                                NameAndFunction = $"{kpi.Name} ({function.ToString()})",
                                Entity = kpi.Entity,
                            })).OrderBy(k => k.NameAndFunction).ToList();
            }
        }

        protected override IList<KpiDefinitionWithAggregationFunctionDto> ApplyContextFiltering(IList<KpiDefinitionWithAggregationFunctionDto> records, object context)
        {
            if (!(context is KpiDefinitionContext))
            {
                return base.ApplyContextFiltering(records, context);
            }
            var kpiDefinitionContext = (KpiDefinitionContext)context;

            return records.Where(r => r.Entity == kpiDefinitionContext.Entity).ToList();
        }

        protected override IEnumerable<Expression<Func<KpiDefinitionWithAggregationFunctionDto, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return kpi => kpi.NameAndFunction;
        }
    }
}