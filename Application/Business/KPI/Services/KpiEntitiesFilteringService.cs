﻿using System;
using System.Linq;
using Smt.Atomic.Business.KPI.Dto;
using Smt.Atomic.Business.KPI.Extensions;
using Smt.Atomic.Business.KPI.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.KPI.Services
{
    public class KpiEntitiesFilteringService : IKpiEntitiesFilteringService
    {
        private readonly IUnitOfWorkService<IAllocationDbScope> _allocationUnitOfWork;
        private readonly IUnitOfWorkService<IOrganizationScope> _organizationUnitOfWork;
        private readonly ITimeService _timeService;

        public KpiEntitiesFilteringService(IUnitOfWorkService<IAllocationDbScope> allocationUnitOfWork,
            IUnitOfWorkService<IOrganizationScope> organizationUnitOfWork,
            ITimeService timeService)
        {
            _allocationUnitOfWork = allocationUnitOfWork;
            _organizationUnitOfWork = organizationUnitOfWork;
            _timeService = timeService;
        }

        public long[] GetEntityIds(KpiEntityParametersBaseDto parameters)
        {
            switch (parameters.Entity)
            {
                case KpiEntity.Employee: return GetEmployeeEntityIds(parameters);
                case KpiEntity.Project: return GetProjectEntityIds(parameters);
                case KpiEntity.OrgUnit: return GetOrgUnitEntityIds(parameters);
            }

            throw new InvalidOperationException("Unrecognized KpiEntity");
        }

        private long[] GetEmployeeEntityIds(KpiEntityParametersBaseDto parameters)
        {
            using (var unitOfWork = _allocationUnitOfWork.Create())
            {
                var currentDate = _timeService.GetCurrentDate();

                return unitOfWork.Repositories.Employees.AsQueryable()
                    .FilterByIds(parameters.EmployeeIds)
                    .FilterByStatus(parameters.EmployeeStatus)
                    .FilterByOrgUnitIds(parameters.EmployeeOrgUnitIds)
                    .FilterByLineManagerId(parameters.EmployeeLineManagerId)
                    .FilterByLocationIds(parameters.EmployeeLocationIds)
                    .FilterByContractTypeIds(currentDate, parameters.EmployeeContractTypeIds)
                    .Select(e => e.Id)
                    .ToArray();
            }
        }

        private long[] GetProjectEntityIds(KpiEntityParametersBaseDto parameters)
        {
            using (var unitOfWork = _allocationUnitOfWork.Create())
            {
                return unitOfWork.Repositories.Projects.AsQueryable()
                    .FilterByIds(parameters.ProjectIds)
                    .FilterByProjectManagerId(parameters.ProjectManagerId)
                    .FilterByOrgUnitIds(parameters.ProjectOrgUnitIds)
                    .FilterByProjectDate(parameters.ProjectDate)
                    .Select(p => p.Id)
                    .ToArray();
            }
        }

        private long[] GetOrgUnitEntityIds(KpiEntityParametersBaseDto parameters)
        {
            using (var unitOfWork = _organizationUnitOfWork.Create())
            {
                return unitOfWork.Repositories.OrgUnits.AsQueryable()
                    .FilterByIds(parameters.OrgUnitIds)
                    .FilterByManagerIds(parameters.OrgUnitManagerIds)
                    .Select(o => o.Id)
                    .ToArray();
            }
        }
    }
}
