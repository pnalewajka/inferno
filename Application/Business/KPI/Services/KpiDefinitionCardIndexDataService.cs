﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.KPI.Dto;
using Smt.Atomic.Business.KPI.Interfaces;
using Smt.Atomic.Business.KPI.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.KPI;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.KPI.Services
{
    public class KpiDefinitionCardIndexDataService : CardIndexDataService<KpiDefinitionDto, KpiDefinition, IKPIDbScope>, IKpiDefinitionCardIndexDataService
    {
        private IKpiSqlService _kpiSqlService;
        private IUserService _userService;
        private IPrincipalProvider _principalProvider;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public KpiDefinitionCardIndexDataService(ICardIndexServiceDependencies<IKPIDbScope> dependencies, IKpiSqlService kpiSqlService, IUserService userService, IPrincipalProvider principalProvider)
            : base(dependencies)
        {
            _kpiSqlService = kpiSqlService;
            _userService = userService;
            _principalProvider = principalProvider;
        }

        public override IQueryable<KpiDefinition> ApplyContextFiltering(IQueryable<KpiDefinition> records, object context)
        {
            if (!(context is KpiDefinitionContext))
            {
                return base.ApplyContextFiltering(records, context);
            }

            var kpiDefinitionContext = (KpiDefinitionContext)context;

            var result = records.Filtered().Where(r => r.Entity == kpiDefinitionContext.Entity);

            return result;
        }

        protected override IQueryable<KpiDefinition> GetFilteredRecords(IQueryable<KpiDefinition> records)
        {
            return records;
        }

        protected override IEnumerable<Expression<Func<KpiDefinition, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return k => k.Name;
            yield return k => k.Description;
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<KpiDefinition, KpiDefinitionDto, IKPIDbScope> eventArgs)
        {
            eventArgs.InputEntity.Roles = EntityMergingService.CreateEntitiesFromIds<SecurityRole, IKPIDbScope>(eventArgs.UnitOfWork, eventArgs.InputEntity.Roles.Select(r => r.Id).ToArray());
            var entity = eventArgs.InputEntity.Entity;
            var kpiDefinitions = GetKpiDefinitions(entity);
            kpiDefinitions.Add(eventArgs.InputDto);

            HandleKpiDefinitionChange(eventArgs, entity, kpiDefinitions);
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<KpiDefinition, IKPIDbScope> eventArgs)
        {
            var entity = eventArgs.DeletingRecord.Entity;
            var kpiDefinitions = GetKpiDefinitions(entity);
            kpiDefinitions.RemoveAt(kpiDefinitions.FindIndex(k => k.Id == eventArgs.DeletingRecord.Id));

            HandleKpiDefinitionChange(eventArgs, entity, kpiDefinitions);
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<KpiDefinition, KpiDefinitionDto, IKPIDbScope> eventArgs)
        {
            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.Roles, eventArgs.InputEntity.Roles);
            var oldEntity = eventArgs.DbEntity.Entity;
            var newEntity = eventArgs.InputEntity.Entity;
            var isKpiEntityChanged = oldEntity != newEntity;
            var kpiDefinitions = GetKpiDefinitions(newEntity);

            if (isKpiEntityChanged)
            {
                kpiDefinitions.Add(eventArgs.InputDto);
            }
            else
            {
                kpiDefinitions[kpiDefinitions.FindIndex(k => k.Id == eventArgs.InputDto.Id)] = eventArgs.InputDto;
            }

            HandleKpiDefinitionChange(eventArgs, newEntity, kpiDefinitions);

            if (isKpiEntityChanged)
            {
                kpiDefinitions = GetKpiDefinitions(oldEntity);
                kpiDefinitions.RemoveAt(kpiDefinitions.FindIndex(k => k.Id == eventArgs.DbEntity.Id));
                HandleKpiDefinitionChange(eventArgs, oldEntity, kpiDefinitions);
            }
        }

        protected override NamedFilters<KpiDefinition> NamedFilters
        {
            get
            {
                return new NamedFilters<KpiDefinition>(
                    new[]
                    {
                        new NamedFilter<KpiDefinition>("project", k => k.Entity == KpiEntity.Project),
                        new NamedFilter<KpiDefinition>("employee", k => k.Entity == KpiEntity.Employee),
                        new NamedFilter<KpiDefinition>("orgunit", k => k.Entity == KpiEntity.OrgUnit),
                    });
            }
        }

        private List<KpiDefinitionDto> GetKpiDefinitions(KpiEntity entity)
        {
            var query = GetRecords(new QueryCriteria()
            {
                Filters = new List<FilterCodeGroup>()
                {
                    new FilterCodeGroup()
                    {
                        Codes = new List<string>()
                        {
                            entity.ToString().ToLowerInvariant(),
                        }
                    }
                }
            });

            return query.Rows.ToList();
        }

        private void HandleKpiDefinitionChange(CardIndexEventArgs eventArgs, KpiEntity entity, List<KpiDefinitionDto> kpiDefinitions)
        {
            var result = HandleKpiDefinitionChange(entity, kpiDefinitions);

            if (result.ContainsErrors)
            {
                eventArgs.AddError(result.GetMessages());
            }
        }

        private BusinessResult HandleKpiDefinitionChange(KpiEntity entity, IEnumerable<KpiDefinitionDto> kpiDefinitions)
        {
            var result = new BusinessResult();

            try
            {
                _kpiSqlService.UpdateKpiViewAndFunction(entity, kpiDefinitions);
            }
            catch (SqlException e)
            {
                result.AddAlert(AlertType.Error, KpiDefinitionServiceResources.SqlError);
                result.AddAlert(AlertType.Error, e.Message);
            }
            catch
            {
                result.AddAlert(AlertType.Error, KpiDefinitionServiceResources.UnknownError);
            }

            return result;
        }
    }
}
