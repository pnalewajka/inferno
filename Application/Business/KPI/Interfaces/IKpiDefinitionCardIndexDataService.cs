﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.KPI.Dto;

namespace Smt.Atomic.Business.KPI.Interfaces
{
    public interface IKpiDefinitionCardIndexDataService : ICardIndexDataService<KpiDefinitionDto>
    {
    }
}
