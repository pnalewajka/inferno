﻿using Smt.Atomic.Business.KPI.Dto;

namespace Smt.Atomic.Business.KPI.Interfaces
{
    public interface IKpiEntitiesFilteringService
    {
        long[] GetEntityIds(KpiEntityParametersBaseDto parameters);
    }
}
