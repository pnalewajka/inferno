﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.KPI.Dto;
using Smt.Atomic.Business.KPI.Models;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.KPI.Interfaces
{
    public interface IKpiSqlService
    {
        string RecordDateColumnName { get; }

        void UpdateKpiViewAndFunction(KpiEntity entity, IEnumerable<KpiDefinitionDto> kpiDefinitions);

        ICollection<string> GetKpiColumnNames(ICollection<long> kpiDefinitionIds);

        ICollection<string> GetKpiAggregationColumnNames(KpiDefinitionWithAggregationFunction[] kpiDefinitions);

        KpiFunctionResultDto GetKpiFunctionResult(long[] kpiDefinitionIds, KpiEntity entity, DateTime reportDate, long[] entityInstancesId);

        KpiFunctionResultDto GetKpiFunctionResultForDateRange(long[] kpiDefinitionIds, KpiEntity entity, DateTime reportStartDate, DateTime reportEndDate, long[] entityInstancesId);

        KpiFunctionResultDto GetKpiFunctionResultForDateRange(KpiDefinitionWithAggregationFunction[] kpiDefinitions, KpiEntity entity, DateTime reportStartDate, DateTime reportEndDate, long[] entityInstancesId);

        KpiFunctionResultDto GetKpiFunctionResultForAggregatedDateRange(KpiDefinitionWithAggregationFunction[] kpiDefinitions, KpiEntity entity, DateTime reportStartDate, DateTime reportEndDate, long[] entityInstancesId);
    }
}
