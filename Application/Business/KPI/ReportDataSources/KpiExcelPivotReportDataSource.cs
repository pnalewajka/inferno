﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.KPI.Dto;
using Smt.Atomic.Business.KPI.Interfaces;
using Smt.Atomic.Business.KPI.Resources;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.CrossCutting.Business.Attributes;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.KPI.ReportDataSources
{
    [Identifier("DataSource.KpiExcelPivotReportDataSource")]
    [RequireRole(SecurityRoleType.CanViewKpiDefinitions)]
    [DefaultReportDefinition(
        "KpiExcelPivotReport",
        nameof(ReportsResources.KpiExcelPivotReportName),
        nameof(ReportsResources.KpiExcelPivotReportDescription),
        MenuAreas.Organization,
        MenuGroups.KpiReports,
        ReportingEngine.MsExcelNoTemplate,
        new string[0],
        "KpiExcelPivotReport.xlsx",
        typeof(ReportsResources))]

    public class KpiExcelPivotReportDataSource : BaseReportDataSource<KpiExcelPivotReportParametersDto>
    {
        private IUnitOfWorkService<IKPIDbScope> _unitOfWork;
        private IKpiSqlService _kpiSqlService;
        private ITimeService _timeService;
        private IKpiEntitiesFilteringService _filteringService;

        public KpiExcelPivotReportDataSource(IUnitOfWorkService<IKPIDbScope> unitOfWork, IKpiSqlService kpiSqlService, ITimeService timeService, IKpiEntitiesFilteringService filteringService)
        {
            _unitOfWork = unitOfWork;
            _kpiSqlService = kpiSqlService;
            _timeService = timeService;
            _filteringService = filteringService;
        }

        public override object GetData(ReportGenerationContext<KpiExcelPivotReportParametersDto> reportGenerationContext)
        {
            var parameters = reportGenerationContext.Parameters;
            var entityInstanceIds = _filteringService.GetEntityIds(parameters);
            var startDate = parameters.ReportStartDate;
            var endDate = parameters.ReportEndDate;
            var entityAttribute = AttributeHelper.GetEnumFieldAttribute<KpiEntityAttribute>(parameters.Entity);

            const string separator = ",";

            var pivotFormattingModel = new ExcelPivotTableFormattingModel
            {
                PivotDefinition = new ExcelFormattingPivotDefinition
                {
                    Columns = string.Join(separator, entityAttribute.Columns),
                    DataSourceTableName = nameof(KpiFunctionResultDto.Records),
                    Rows = _kpiSqlService.RecordDateColumnName,
                    Values = string.Join(separator, _kpiSqlService.GetKpiColumnNames(parameters.KpiDefinitionIds)),
                },
            };

            var serializedFormattingModel = XmlSerializationHelper.SerializeToXml(pivotFormattingModel);

            var reportTemplateDocument = new DocumentDto
            {
                ContentType = MimeHelper.XmlFile,
                DocumentName = "PivotTable-formatting.xml",
            };

            reportGenerationContext.AddOrUpdateTemplateFile(reportTemplateDocument, serializedFormattingModel);

            return _kpiSqlService.GetKpiFunctionResultForDateRange(parameters.KpiDefinitionIds, parameters.Entity, startDate, endDate, entityInstanceIds);
        }

        public override KpiExcelPivotReportParametersDto GetDefaultParameters(ReportGenerationContext<KpiExcelPivotReportParametersDto> reportGenerationContext)
        {
            return new KpiExcelPivotReportParametersDto
            {
                Entity = KpiEntity.Employee,
                KpiDefinitionIds = new long[0],
                EmployeeIds = new long[0],
                ProjectIds = new long[0],
                OrgUnitIds = new long[0],
                ReportStartDate = _timeService.GetCurrentDate(),
                ReportEndDate = _timeService.GetCurrentDate(),
            };
        }
    }
}