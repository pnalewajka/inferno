﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Smt.Atomic.Business.KPI.Dto;
using Smt.Atomic.Business.KPI.Interfaces;
using Smt.Atomic.Business.KPI.Models;
using Smt.Atomic.Business.KPI.Resources;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.CrossCutting.Business.Attributes;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.KPI.ReportDataSources
{
    [Identifier("DataSource.KpiChartReportDataSource")]
    [RequireRole(SecurityRoleType.CanPerformKpiReports)]
    [DefaultReportDefinition(
        "KpiChartReport",
        nameof(ReportsResources.KpiChartReportName),
        nameof(ReportsResources.KpiChartReportDescription),
        MenuAreas.Organization,
        MenuGroups.KpiReports,
        ReportingEngine.Chart,
        "Smt.Atomic.Business.KPI.ReportTemplates.KpiReportChart-config.js",
        typeof(ReportsResources))]

    public class KpiChartReportDataSource : BaseReportDataSource<KpiChartReportParametersDto>
    {
        private IUnitOfWorkService<IKPIDbScope> _unitOfWork;
        private IKpiSqlService _kpiSqlService;
        private ITimeService _timeService;
        private IKpiEntitiesFilteringService _filteringService;

        public KpiChartReportDataSource(IUnitOfWorkService<IKPIDbScope> unitOfWork, IKpiSqlService kpiSqlService, ITimeService timeService, IKpiEntitiesFilteringService filteringService)
        {
            _unitOfWork = unitOfWork;
            _kpiSqlService = kpiSqlService;
            _timeService = timeService;
            _filteringService = filteringService;
        }

        public override object GetData(ReportGenerationContext<KpiChartReportParametersDto> reportGenerationContext)
        {
            var parameters = reportGenerationContext.Parameters;

            var entityAttribute = AttributeHelper.GetEnumFieldAttribute<KpiEntityAttribute>(parameters.Entity);
            var kpiColumnNames = GetKpiColumnNames(parameters);
            var kpiPropertyNames = GetKpiPropertyNames(parameters, kpiColumnNames);
            var entityInstanceIds = _filteringService.GetEntityIds(parameters);

            var startDate = parameters.ReportStartDate;
            var endDate = parameters.ReportEndDate;
            KpiChartReportFunctionResultDto result;

            if (parameters.ReportType == KpiChartReportType.AggregatedKpisForManyPeriodsOverEntities)
            {
                result = new KpiChartReportFunctionResultDto(
                    _kpiSqlService.GetKpiFunctionResultForAggregatedDateRange(parameters.KpiDefinitions, parameters.Entity, startDate, endDate, entityInstanceIds))
                {
                    LabelPropertyNames = entityAttribute.Columns.Where(c => c.Contains("Name"))
                };
            }
            else if (parameters.ReportType == KpiChartReportType.OneKpiForManyEntitiesOverPeriods)
            {
                result = new KpiChartReportFunctionResultDto(
                    _kpiSqlService.GetKpiFunctionResultForDateRange(new long[] { parameters.KpiDefinitionId.Value }, parameters.Entity, startDate, endDate, entityInstanceIds))
                {
                    LabelPropertyNames = new string[] { _kpiSqlService.RecordDateColumnName }
                };
            }
            else
            {
                result = new KpiChartReportFunctionResultDto(
                    _kpiSqlService.GetKpiFunctionResultForDateRange(parameters.KpiDefinitions, parameters.Entity, startDate, endDate, entityInstanceIds))
                {
                    LabelPropertyNames = new string[] { _kpiSqlService.RecordDateColumnName }
                };
            }

            result.IsKpiSerie = parameters.ReportType != KpiChartReportType.OneKpiForManyEntitiesOverPeriods;
            result.KpiColumnNames = kpiPropertyNames;

            return result;
        }

        public override KpiChartReportParametersDto GetDefaultParameters(ReportGenerationContext<KpiChartReportParametersDto> reportGenerationContext)
        {
            return new KpiChartReportParametersDto()
            {
                Entity = KpiEntity.Employee,
                KpiDefinitions = new KpiDefinitionWithAggregationFunction[0],
                EmployeeIds = new long[0],
                ProjectIds = new long[0],
                OrgUnitIds = new long[0],
                ReportStartDate = _timeService.GetCurrentDate(),
                ReportEndDate = _timeService.GetCurrentDate(),
            };
        }

        private ICollection<string> GetKpiPropertyNames(KpiChartReportParametersDto parameters, ICollection<string> kpiColumnNames)
        {
            if (parameters.ReportType != KpiChartReportType.OneKpiForManyEntitiesOverPeriods)
            {
                return _kpiSqlService.GetKpiAggregationColumnNames(parameters.KpiDefinitions);
            }

            return kpiColumnNames;
        }

        private ICollection<string> GetKpiColumnNames(KpiChartReportParametersDto parameters)
        {
            using (var unitOfWork = _unitOfWork.Create())
            {
                if (parameters.ReportType == KpiChartReportType.OneKpiForManyEntitiesOverPeriods)
                {
                    return _kpiSqlService.GetKpiColumnNames(new long[] { parameters.KpiDefinitionId.Value });
                }

                return _kpiSqlService.GetKpiColumnNames(parameters.KpiDefinitions.Select(k => k.KpiDefinitionId).ToArray());
            }
        }
    }
}