﻿using Smt.Atomic.Business.KPI.Dto;
using Smt.Atomic.Business.KPI.Interfaces;
using Smt.Atomic.Business.KPI.Resources;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.CrossCutting.Business.Attributes;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.KPI.ReportDataSources
{
    [Identifier("DataSource.KpiReportDataSource")]
    [RequireRole(SecurityRoleType.CanPerformKpiReports)]
    [DefaultReportDefinition(
        "KpiReportExcel",
        nameof(ReportsResources.KpiExcelReportName),
        nameof(ReportsResources.KpiExcelReportDescription),
        MenuAreas.Organization,
        MenuGroups.KpiReports,
        ReportingEngine.MsExcelNoTemplate,
        new string[0],
        "KpiExcelReport.xlsx",
        typeof(ReportsResources))]
    [DefaultReportDefinition(
        "KpiReport",
        nameof(ReportsResources.KpiReportName),
        nameof(ReportsResources.KpiReportDescription),
        MenuAreas.Organization,
        MenuGroups.KpiReports,
        ReportingEngine.PivotTable,
        "Smt.Atomic.Business.KPI.ReportTemplates.KpiReport-config.js",
        typeof(ReportsResources))]

    public class KpiReportDataSource : BaseReportDataSource<KpiReportParametersDto>
    {
        private IUnitOfWorkService<IKPIDbScope> _unitOfWork;
        private IKpiSqlService _kpiSqlService;
        private ITimeService _timeService;
        private IKpiEntitiesFilteringService _filteringService;

        public KpiReportDataSource(IUnitOfWorkService<IKPIDbScope> unitOfWork, IKpiSqlService kpiSqlService, ITimeService timeService, IKpiEntitiesFilteringService filteringService)
        {
            _unitOfWork = unitOfWork;
            _kpiSqlService = kpiSqlService;
            _timeService = timeService;
            _filteringService = filteringService;
        }

        public override object GetData(ReportGenerationContext<KpiReportParametersDto> reportGenerationContext)
        {
            var parameters = reportGenerationContext.Parameters;
            var entityAttribute = AttributeHelper.GetEnumFieldAttribute<KpiEntityAttribute>(parameters.Entity);
            var entityInstanceIds = _filteringService.GetEntityIds(parameters);

            return _kpiSqlService.GetKpiFunctionResult(parameters.KpiDefinitionIds, parameters.Entity, parameters.ReportDate, entityInstanceIds);
        }

        public override KpiReportParametersDto GetDefaultParameters(ReportGenerationContext<KpiReportParametersDto> reportGenerationContext)
        {
            return new KpiReportParametersDto()
            {
                Entity = KpiEntity.Employee,
                KpiDefinitionIds = new long[0],
                ReportDate = _timeService.GetCurrentDate(),
            };
        }
    }
}