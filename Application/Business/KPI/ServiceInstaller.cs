﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.KPI.Interfaces;
using Smt.Atomic.Business.KPI.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.KPI
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            if (containerType == ContainerType.WebApp)
            {
                container.Register(Component.For<IKpiDefinitionCardIndexDataService>().ImplementedBy<KpiDefinitionCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IKpiSqlService>().ImplementedBy<KpiSqlService>().LifestyleTransient());
                container.Register(Component.For<IKpiDefinitionWithAggregationFunctionCardIndexDataService>().ImplementedBy<KpiDefinitionWithAggregationFunctionCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IKpiEntitiesFilteringService>().ImplementedBy<KpiEntitiesFilteringService>().LifestyleTransient());
            }
        }
    }
}
