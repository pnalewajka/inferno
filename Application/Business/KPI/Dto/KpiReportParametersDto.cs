﻿using System;

namespace Smt.Atomic.Business.KPI.Dto
{
    public class KpiReportParametersDto : KpiEntityParametersBaseDto
    {
        public long[] KpiDefinitionIds { get; set; }

        public DateTime ReportDate { get; set; }
    }
}
