﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.KPI.Dto
{
    public class KpiDefinitionWithAggregationFunctionDto
    {
        public long Id { get; set; }

        public string NameAndFunction { get; set; }

        public KpiEntity Entity { get; set; }
    }
}
