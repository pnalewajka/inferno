﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.KPI;

namespace Smt.Atomic.Business.KPI.Dto
{
    public class KpiDefinitionToKpiDefinitionDtoMapping : ClassMapping<KpiDefinition, KpiDefinitionDto>
    {
        public KpiDefinitionToKpiDefinitionDtoMapping()
        {
            Mapping = e => new KpiDefinitionDto
            {
                Id = e.Id,
                Name = e.Name,
                Description = e.Description,
                Entity = e.Entity,
                RoleIds = e.Roles.Select(r => r.Id).ToArray(),
                SqlFormula = e.SqlFormula,
                Timestamp = e.Timestamp
            };
        }
    }
}
