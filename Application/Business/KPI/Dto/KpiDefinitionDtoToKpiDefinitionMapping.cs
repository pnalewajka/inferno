﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.KPI;

namespace Smt.Atomic.Business.KPI.Dto
{
    public class KpiDefinitionDtoToKpiDefinitionMapping : ClassMapping<KpiDefinitionDto, KpiDefinition>
    {
        public KpiDefinitionDtoToKpiDefinitionMapping()
        {
            Mapping = d => new KpiDefinition
            {
                Id = d.Id,
                Name = d.Name,
                Description = d.Description,
                Entity = d.Entity,
                Roles = d.RoleIds == null ? null : d.RoleIds.Select(r => new SecurityRole { Id = r }).ToList(),
                SqlFormula = d.SqlFormula,
                Timestamp = d.Timestamp
            };
        }
    }
}
