﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.KPI.Dto
{
    public class KpiFunctionResultDto
    {
        public ICollection<IDictionary<string, object>> Records { get; set; }
    }
}