﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Business.KPI.Dto
{
    public class KpiDefinitionDto : SimpleEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public KpiEntity Entity { get; set; }

        public string SqlFormula { get; set; }

        public long[] RoleIds { get; set; }
    }
}
