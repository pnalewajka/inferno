﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.KPI.Dto
{
    public class KpiEntityParametersBaseDto
    {
        public KpiEntity Entity { get; set; }

        public long[] ProjectIds { get; set; }

        public long? ProjectManagerId { get; set; }

        public long[] ProjectOrgUnitIds { get; set; }

        public DateTime? ProjectDate { get; set; }

        public long[] EmployeeIds { get; set; }

        public EmployeeStatus? EmployeeStatus { get; set; }

        public long[] EmployeeOrgUnitIds { get; set; }

        public long? EmployeeLineManagerId { get; set; }

        public long[] EmployeeLocationIds { get; set; }

        public long[] EmployeeContractTypeIds { get; set; }

        public long[] OrgUnitIds { get; set; }

        public long[] OrgUnitManagerIds { get; set; }
    }
}
