﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.KPI.Dto
{
    public class KpiChartReportFunctionResultDto : KpiFunctionResultDto
    {
        public KpiChartReportFunctionResultDto(KpiFunctionResultDto functionResult)
        {
            Records = functionResult.Records;
        }

        public bool IsKpiSerie { get; set; }

        public IEnumerable<string> LabelPropertyNames { get; set; }

        public IEnumerable<string> KpiColumnNames { get; set; }
    }
}
