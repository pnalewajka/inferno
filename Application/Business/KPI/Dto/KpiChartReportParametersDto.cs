﻿using Smt.Atomic.Business.KPI.Models;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.KPI.Dto
{
    public class KpiChartReportParametersDto : KpiEntitiesDateRangeReportParameters
    {
        public KpiChartReportType ReportType { get; set; }

        public long? KpiDefinitionId { get; set; }

        public KpiDefinitionWithAggregationFunction[] KpiDefinitions { get; set; }
    }
}
