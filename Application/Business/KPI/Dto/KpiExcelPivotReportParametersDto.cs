﻿using Smt.Atomic.Business.KPI.Models;

namespace Smt.Atomic.Business.KPI.Dto
{
    public class KpiExcelPivotReportParametersDto : KpiEntitiesDateRangeReportParameters
    {
        public long[] KpiDefinitionIds { get; set; }
    }
}
