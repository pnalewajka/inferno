﻿﻿pivot.dataConverter = function (data) {
     if (data.Records.length > 0) {
         pivot.configurations[0].config.rows = Object.keys(data.Records[0]);
     }
     
     return data.Records;
};

pivot.configurations = [
    {
        name: "KPI Report",
        code: "KPI",
        config: {
            cols: [],
            vals: [],
            aggregatorName: "First",
            rowOrder: "key_a_to_z",
            colOrder: "key_a_to_z",
        }
    },
];