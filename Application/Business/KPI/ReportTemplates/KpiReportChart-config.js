var kpiColumnNames = [];
var labelPropertyNames = [];
var isKpiSerie = false;

var ReportDataChart = function () {

    var data = function () {
        kpiColumnNames = reportData.KpiColumnNames;
        labelPropertyNames = reportData.LabelPropertyNames;
        isKpiSerie = reportData.IsKpiSerie;

        return {
            Rows: reportData.Records,
        }
    }();

    return {
        Data: data
    }
}();

function getConfigurationForKpiDefinition(kpiNames) {
    var configuration = {
        name: 'KPI Chart Report',
        config: {
            type: 'bar',
            data: {
                labels: ReportDataChart.Data.Rows.map(r => labelPropertyNames.map(l => r[l]).join(' ')),
                datasets: [],
            },
            options: {
                animation: {
                    duration: 0,
                },
                elements: {
                    line: {
                        tension: 0,
                    }
                },
                hover: {
                    animationDuration: 0,
                },
                responsiveAnimationDuration: 0,
                responsive: true,
                title: {
                    display: kpiNames.length === 1,
                    text: kpiNames[0],
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true,
                },
                scales: {
                    xAxes: [{
                        type: labelPropertyNames[0] === 'ReportRecordDate' ? 'time' : 'category',
                        display: true,
                    }],
                    yAxes: [{
                        id: 'primary',
                        display: true,
                        position: 'left',
                        offset: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'KPI Value'
                        }
                    },
                    {
                        id: 'secondary',
                        display: true,
                        position: 'right',
                        scaleLabel: {
                            display: true,
                            labelString: 'Secondary KPI Value'
                        }
                    }]
                }
            }
        },
    };
    ReportDataChart.Data.Rows.forEach((record, rowIndex) => {
        kpiNames.forEach((kpiName, kpiIndex) => {
            var dataset = configuration.config.data.datasets.find(d => d.id === (isKpiSerie ? kpiName : record.Id));
            if (dataset) {
                dataset.data.push({
                    x: labelPropertyNames.map(l => record[l]).join(' '),
                    y: record[kpiName],
                })
            } else {
                configuration.config.data.datasets.push({
                    id: isKpiSerie ? kpiName : record.Id,
                    label: isKpiSerie ? kpiName : Object.keys(record).filter(k => k.includes('Name')).map(k => record[k]).join(' '),
                    backgroundColor: ChartReport.getColor(rowIndex + kpiIndex),
                    borderColor: ChartReport.getColor(rowIndex + kpiIndex),
                    data: [{
                        x: labelPropertyNames.map(l => record[l]).join(' '),
                        y: record[kpiName],
                    }],
                    type: 'line',
                    fill: false,
                    spanGaps: false,
                });
            }
        });        
    });

    return configuration;
}

function changeChartType(datasetId) {
    var dataset = configurations[0].config.data.datasets.find(d => d.id === datasetId);
    dataset.type = dataset.type === 'bar' ? 'line' : 'bar';
    ChartReport.changeConfiguration(configurations[0].config);
}

function changeChartAxis(datasetId, isSecondary) {
    var dataset = configurations[0].config.data.datasets.find(d => d.id === datasetId);
    dataset.yAxisID = isSecondary ? 'secondary' : 'primary';
    ChartReport.changeConfiguration(configurations[0].config);
}

function configureChangeButton() {
    var $changeButton = $('<button type="button" id="change-chart-type-menu" class="btn dropdown-toggle btn-default" data-toggle="dropdown">Change type<span class="caret"></span></button>');
    var $buttonGroup = $('.content > .btn-group').last();
    $changeButton.appendTo($buttonGroup);
    var $dropdownMenu = $('<ul id="predefined-config-menu" class="dropdown-menu filter-menu" role="menu"></ul>');
    $dropdownMenu.appendTo($buttonGroup);
    configurations[0].config.data.datasets.forEach(dataset => {
        var $dropdownOption = $('<li>');
        var $dropdownButton = $('<a href="#"><span>' + dataset.label + '</span></a>');
        var $dropdownCheckboxContainer = $('<div style="float: right; padding-right: 20px;"></div>');
        var $dropdownCheckbox = $('<input class="form-check-input" type="checkbox"><span>Secondary axis</span></input>');
        $dropdownButton.click(() => {
            changeChartType(dataset.id);
        });
        $dropdownCheckbox.change(() => {
            changeChartAxis(dataset.id, $dropdownCheckbox.prop('checked'));
        });
        $dropdownButton.appendTo($dropdownOption);
        $dropdownCheckboxContainer.appendTo($dropdownOption);
        $dropdownCheckbox.appendTo($dropdownCheckboxContainer);
        $dropdownOption.appendTo($dropdownMenu);
        var $dropdownDivider = $('<li class="sudo divider"></li>');
        $dropdownDivider.appendTo($dropdownMenu);
    });
}

var configurations = [];
configurations.push(getConfigurationForKpiDefinition(kpiColumnNames));
configureChangeButton()