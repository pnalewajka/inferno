﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.EventSourcing;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.EventSourcing.ChoreProviders
{
    public class FailedBusinessEventsChoreProvider
        : IChoreProvider
    {
        private readonly IPrincipalProvider _principalProvider;

        public FailedBusinessEventsChoreProvider(
            IPrincipalProvider principalProvider)
        {
            _principalProvider = principalProvider;
        }

        public IQueryable<ChoreDto> GetActiveChores(IReadOnlyRepositoryFactory repositoryFactory)
        {
            if (!_principalProvider.Current.IsInRole(SecurityRoleType.CanViewPublishedEventQueueItem))
            {
                return null;
            }

            var eventItems = repositoryFactory.Create<PublishedEventQueueItem>().Filtered();

            return eventItems
                .Where(e => e.Exception != null)
                .GroupBy(e => 0)
                .Select(g => new ChoreDto
                {
                    Type = ChoreType.FailedBusinessEvents,
                    DueDate = null,
                    Parameters = g.Count().ToString(),
                });
        }
    }
}
