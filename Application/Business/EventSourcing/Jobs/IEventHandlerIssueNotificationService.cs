﻿using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.EventSourcing.Jobs
{
    public interface IEventHandlerIssueNotificationService
    {
        void NotifyAboutEventHandlingError(BusinessEvent businessEvent, bool lastNotificationForThisEvent);
    }
}