﻿using System.Linq;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Dto;
using Smt.Atomic.Business.Configuration.Interfaces;

namespace Smt.Atomic.Business.EventSourcing.Jobs
{
    public class EventHandlerIssueNotificationService : IEventHandlerIssueNotificationService
    {
        private readonly IReliableEmailService _reliableEmailService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly ISystemParameterService _systemParameterService;

        public EventHandlerIssueNotificationService(IReliableEmailService reliableEmailService, IMessageTemplateService messageTemplateService, ISystemParameterService systemParameterService)
        {
            _reliableEmailService = reliableEmailService;
            _messageTemplateService = messageTemplateService;
            _systemParameterService = systemParameterService;
        }

        public void NotifyAboutEventHandlingError(BusinessEvent businessEvent, bool lastNotificationForThisEvent)
        {
            var body =
                _messageTemplateService.ResolveTemplate(
                    lastNotificationForThisEvent
                        ? TemplateCodes.EventSourcingEventHandlingFailedNoMoreAttemptsBody
                        : TemplateCodes.EventSourcingEventHandlingFailedMoreAttemptsBody, 
                    businessEvent);
            var subject = _messageTemplateService.ResolveTemplate(TemplateCodes.EventSourcingEventHandlingFailedSubject, businessEvent);
            var recipients = _systemParameterService.GetParameter<string[]>(ParameterKeys.AdminEmailRecipients).Select(r => new EmailRecipientDto(r, r)).ToList();

            _reliableEmailService.EnqueueMessage(new EmailMessageDto
            {
                Body = body.Content,
                Subject = subject.Content,
                IsHtml = body.IsHtml,
                Recipients = recipients
            });
        }
    }
}