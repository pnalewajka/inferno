﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Logging;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.Business.EventSourcing.Dto;
using Smt.Atomic.Business.EventSourcing.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.EventSourcing.Jobs
{
    [Identifier("Job.EventSourcing.EventProcessing")]
    [JobDefinition(
        Code = "EventProcessingJob",
        Description = "Process events from event source",
        ScheduleSettings = "* * * * * *",
        PipelineName = PipelineCodes.EventSource,
        MaxExecutioPeriodInSeconds = 30,
        MaxRunAttemptsInBulk = 1,
        MaxBulkRunAttempts = 1)]
    internal class EventProcessingJob : IJob
    {
        private const int MaximumRetriesCount = 3;
        private readonly IBusinessEventQueueService _businessEventQueueService;
        private readonly ILogger _logger;
        private readonly IEventHandlerIssueNotificationService _eventHandlerIssueNotificationService;

        public EventProcessingJob(
            IBusinessEventQueueService businessEventQueueService, ILogger logger, IEventHandlerIssueNotificationService eventHandlerIssueNotificationService)
        {
            _businessEventQueueService = businessEventQueueService;
            _logger = logger;
            _eventHandlerIssueNotificationService = eventHandlerIssueNotificationService;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            while (!executionContext.CancellationToken.IsCancellationRequested)
            {
                var item = _businessEventQueueService.GetNextBusinessEvent();

                if (item == null)
                {
                    break;
                }

                var businessEvent = item.BusinessEvent;
                var handlers = ReflectionHelper.ResolveAll<IBusinessEventHandler>();

                if (handlers.Any())
                {
                    HandleEvent(handlers, businessEvent, item);
                }
                else
                {
                    _logger.InfoFormat("No handlers for business event of type '{0}'.", businessEvent.GetType().FullName);
                }

                _businessEventQueueService.DeleteBusinessEvent(item);
            }

            return JobResult.Success();
        }

        private void HandleEvent(IBusinessEventHandler[] handlers, BusinessEvent businessEvent, BusinessEventQueueItem item)
        {
            bool hadTransientException = true;
            bool finishedSucessfully = false;

            for (var attempt = 0; attempt < MaximumRetriesCount && hadTransientException; ++attempt)
            {
                hadTransientException = false;

                try
                {
                    RunHandlers(handlers, businessEvent);
                    finishedSucessfully = true;
                }
                catch (Exception exception) when (ExceptionHelper.IsTransientException(exception))
                {
                    hadTransientException = true;
                    _logger.Error(
                        $"Business event handler for {businessEvent.GetType().Name} (type: id: {businessEvent.Id}) threw a transient exception on {attempt + 1}. attempt: {exception}");

                    _eventHandlerIssueNotificationService.NotifyAboutEventHandlingError(businessEvent, attempt == MaximumRetriesCount - 1);
                }
                catch (Exception exception)
                {
                    _logger.Error(
                        $"Business event handler for {businessEvent.GetType().Name} (type: id: {businessEvent.Id}) threw an exception: {exception}");
                    _businessEventQueueService.MarkWithException(item, exception);
                }
            }
            
            if (!finishedSucessfully && !hadTransientException)
            {
                _eventHandlerIssueNotificationService.NotifyAboutEventHandlingError(businessEvent, true);
            }
        }

        private void RunHandlers(IEnumerable<IBusinessEventHandler> handlers, BusinessEvent businessEvent)
        {
            foreach (var handler in handlers)
            {
                if (handler.GetBusinessEventType() == businessEvent.GetType())
                {
                    try
                    {
                        handler.Handle(businessEvent);
                    }
                    catch (Exception)
                    {
                        _logger.Error($"Error handling business event (ID = {businessEvent.Id}) in handler {handler.GetBusinessEventType().Name}");                        
                        throw;
                    }
                }
            }
        }
    }
}