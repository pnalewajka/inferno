﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.EventSourcing.ChoreProviders;
using Smt.Atomic.Business.EventSourcing.Interfaces;
using Smt.Atomic.Business.EventSourcing.Jobs;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Business.EventSourcing
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            switch (containerType)
            {
                case ContainerType.WebApp:
                case ContainerType.WebApi:
                case ContainerType.JobScheduler:
                case ContainerType.WebServices:
                    container.Register(Component
                        .For<IBusinessEventPublisher, IBusinessEventQueueService>()
                        .ImplementedBy<MsSqlBusinessEventQueueService>()
                        .LifestyleTransient());
                    container.Register(Classes.FromThisAssembly().BasedOn<IJob>().LifestylePerThread());
                    break;
            }

            if (containerType == ContainerType.WebApp)
            {
                container.Register(Component.For<IPublishedEventQueueItemCardIndexDataService>().ImplementedBy<PublishedEventQueueItemCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IChoreProvider>().ImplementedBy<FailedBusinessEventsChoreProvider>().LifestylePerWebRequest());
            }
            else if (containerType == ContainerType.JobScheduler)
            {
                container.Register(Component.For<IEventHandlerIssueNotificationService>().ImplementedBy<EventHandlerIssueNotificationService>().LifestyleTransient());
            }
        }
    }
}