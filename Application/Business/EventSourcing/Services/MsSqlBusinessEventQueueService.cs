using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.EventSourcing.Dto;
using Smt.Atomic.Business.EventSourcing.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Dto;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.EventSourcing;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.EventSourcing.Services
{
    public class MsSqlBusinessEventQueueService : IBusinessEventQueueService
    {
        private static readonly Encoding EncodingForSerialization = Encoding.UTF8;
        private readonly IUnitOfWorkService<IEventSourcingDataContext> _unitOfWorkService;
        private readonly IDbTimeService _dbTimeService;
        private readonly ISystemParameterService _systemParameterService;

        public MsSqlBusinessEventQueueService(
            IUnitOfWorkService<IEventSourcingDataContext> unitOfWorkService,
            IDbTimeService dbTimeService,
            ISystemParameterService systemParameterService)
        {
            _unitOfWorkService = unitOfWorkService;
            _dbTimeService = dbTimeService;
            _systemParameterService = systemParameterService;
        }

        public void PublishBusinessEvent(BusinessEvent businessEvent)
        {
            businessEvent.CreatedOn = _dbTimeService.GetCurrentTime();
            businessEvent.Id = Guid.NewGuid();

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var processedEventQueueItem = new PublishedEventQueueItem
                {
                    PublishedOn = _dbTimeService.GetCurrentTime(),
                    LeasedOn = null,
                    BusinessEvent = GetSerializedBusinessEvent(businessEvent),
                    BusinessEventId = businessEvent.Id,
                    Exception = null
                };

                unitOfWork.Repositories.PublishedEventQueue.Add(processedEventQueueItem);
                unitOfWork.Commit();
            }
        }

        public BusinessEventQueueItem GetNextBusinessEvent()
        {
            var currentTime = _dbTimeService.GetCurrentTime();

            var leaseTimeInSeconds = _systemParameterService.GetParameter<int>(ParameterKeys.BusinessEventLeaseTimeInSeconds);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var publishedItem = unitOfWork
                    .Repositories
                    .PublishedEventQueue
                    .OrderBy(i => i.PublishedOn)
                    .FirstOrDefault(i => (
                        i.LeasedOn == null || DbFunctionsHelper.AddSeconds(i.LeasedOn, leaseTimeInSeconds) < currentTime) 
                        && i.Exception == null);

                if (publishedItem == null)
                {
                    return null;
                }

                var businessEvent = GetDeserializedBusinessEvent(publishedItem);

                publishedItem.LeasedOn = _dbTimeService.GetCurrentTime();
                unitOfWork.Repositories.PublishedEventQueue.Edit(publishedItem);

                unitOfWork.Commit();

                return new BusinessEventQueueItem(businessEvent, publishedItem.Timestamp);
            }
        }

        public void DeleteBusinessEvent(BusinessEventQueueItem businessEventQueueItem)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var publishedItem =
                    unitOfWork.Repositories.PublishedEventQueue.Single(
                        i =>
                            i.BusinessEventId == businessEventQueueItem.BusinessEvent.Id 
                            && i.Timestamp == businessEventQueueItem.LeaseToken
                            && i.Exception == null);

                if (publishedItem.LeasedOn == null)
                {
                    throw new InvalidDataException("Leasing time has not been set");
                }

                var processedItem = new ProcessedEventQueueItem
                {
                    BusinessEvent = publishedItem.BusinessEvent,
                    BusinessEventId = publishedItem.BusinessEventId,
                    ProcessingStartedOn = publishedItem.LeasedOn.Value,
                    ProcessedFinishedOn = _dbTimeService.GetCurrentTime(),
                    PublishedOn = publishedItem.PublishedOn,
                };

                unitOfWork.Repositories.ProcessedEventQueue.Add(processedItem);
                unitOfWork.Repositories.PublishedEventQueue.Delete(publishedItem);

                unitOfWork.Commit();
            }
        }

        public void CancelBusinessEventLease(BusinessEventQueueItem businessEventQueueItem)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var item =
                    unitOfWork.Repositories.PublishedEventQueue.Single(
                        i =>
                            i.BusinessEventId == businessEventQueueItem.BusinessEvent.Id 
                            && i.Timestamp == businessEventQueueItem.LeaseToken
                            && i.Exception == null);

                if (item.LeasedOn == null)
                {
                    throw new InvalidDataException("Leasing time has not been set");
                }

                item.LeasedOn = null;

                unitOfWork.Repositories.PublishedEventQueue.Edit(item);
                unitOfWork.Commit();
            }
        }

        public void MarkWithException(BusinessEventQueueItem businessEventQueueItem, Exception exception)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var item =
                    unitOfWork.Repositories.PublishedEventQueue.Single(
                        i =>
                            i.BusinessEventId == businessEventQueueItem.BusinessEvent.Id
                            && i.Timestamp == businessEventQueueItem.LeaseToken);

                if (item.LeasedOn == null)
                {
                    throw new InvalidDataException("Leasing time has not been set");
                }

                item.LeasedOn = null;

                var exceptionAsString = exception.ToString();
                var maxLength = EntityHelper.GetFieldMaxLength<PublishedEventQueueItem>(i => item.Exception);
                item.Exception = exceptionAsString.Substring(0, Math.Min(exceptionAsString.Length, maxLength));

                unitOfWork.Repositories.PublishedEventQueue.Edit(item);
                unitOfWork.Commit();
            }
        }

        private static string GetSerializedBusinessEvent(BusinessEvent businessEvent)
        {
            var businessEventTypes = TypeHelper.GetAllSubClassesOf(typeof (BusinessEvent), false);
            var serializedBusinessEvent = DataContractSerializationHelper.SerializeToXml(businessEvent, typeof (BusinessEvent), businessEventTypes);

            return serializedBusinessEvent;
        }

        private static BusinessEvent GetDeserializedBusinessEvent(PublishedEventQueueItem publishedItem)
        {
            var businessEventTypes = TypeHelper.GetAllSubClassesOf(typeof (BusinessEvent), false);
            var businessEvent = (BusinessEvent)DataContractSerializationHelper.DeserializeFromXml(publishedItem.BusinessEvent, typeof (BusinessEvent), businessEventTypes);
            
            return businessEvent;
        }
    }
}