﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.EventSourcing.Dto;
using Smt.Atomic.Business.EventSourcing.Interfaces;
using Smt.Atomic.Business.EventSourcing.Resources;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.EventSourcing;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.EventSourcing.Services
{
    public class PublishedEventQueueItemCardIndexDataService : CardIndexDataService<PublishedEventQueueItemDto, PublishedEventQueueItem, IEventSourcingDataContext>, IPublishedEventQueueItemCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public PublishedEventQueueItemCardIndexDataService(ICardIndexServiceDependencies<IEventSourcingDataContext> dependencies)
            : base(dependencies)
        {
        }

        protected override NamedFilters<PublishedEventQueueItem> NamedFilters
        {
            get
            {
                return new NamedFilters<PublishedEventQueueItem>(new[]
                {
                    new NamedFilter<PublishedEventQueueItem>(PublishedEventQueueItemFilterCodes.Active, x => x.Exception == null),
                    new NamedFilter<PublishedEventQueueItem>(PublishedEventQueueItemFilterCodes.Failed, x => x.Exception != null),
                });
            }
        }

        public BusinessResult ReExecute(long id)
        {
            var result = new BusinessResult();

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var repository = unitOfWork.Repositories.PublishedEventQueue;
                var queueItem = repository.GetById(id);

                if (string.IsNullOrWhiteSpace(queueItem.Exception))
                {
                    result.AddAlert(AlertType.Error, PublishedEventQueueItemResource.QueueItemIsActiveErrorMessage);
                }
                else
                {
                    queueItem.Exception = null;

                    repository.Edit(queueItem);
                    unitOfWork.Commit();

                    result.AddAlert(AlertType.Success, PublishedEventQueueItemResource.QueueItemReExecuteResultMessage);
                }

                return result;
            }
        }

        protected override IEnumerable<Expression<Func<PublishedEventQueueItem, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return x => x.BusinessEvent;
            yield return x => x.Exception;
            yield return x => x.BusinessEventId;
        }
    }
}
