using System;
using Smt.Atomic.Business.EventSourcing.Interfaces;
using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.EventSourcing.Services
{
    public abstract class BusinessEventHandler<TBusinessEvent> : 
        IBusinessEventHandler<TBusinessEvent>,
        IBusinessEventHandler
        where TBusinessEvent : BusinessEvent
    {
        public abstract void Handle(TBusinessEvent businessEvent);

        void IBusinessEventHandler.Handle(BusinessEvent businessEvent)
        {
            Handle((TBusinessEvent)businessEvent);
        }

        public Type GetBusinessEventType()
        {
            return typeof (TBusinessEvent);
        }
    }
}