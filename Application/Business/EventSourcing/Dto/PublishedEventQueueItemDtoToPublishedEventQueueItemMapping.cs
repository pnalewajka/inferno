﻿using Smt.Atomic.Data.Entities.Modules.EventSourcing;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.EventSourcing.Dto
{
    public class PublishedEventQueueItemDtoToPublishedEventQueueItemMapping : ClassMapping<PublishedEventQueueItemDto, PublishedEventQueueItem>
    {
        public PublishedEventQueueItemDtoToPublishedEventQueueItemMapping()
        {
            Mapping = d => new PublishedEventQueueItem
            {
                Id = d.Id,
                Timestamp = d.Timestamp,
                BusinessEventId = d.BusinessEventId,
                PublishedOn = d.PublishedOn,
                LeasedOn = d.LeasedOn,
                BusinessEvent = d.BusinessEvent,
                Exception = d.Exception
            };
        }
    }
}
