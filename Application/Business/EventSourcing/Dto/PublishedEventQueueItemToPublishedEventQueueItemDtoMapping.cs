﻿using Smt.Atomic.Data.Entities.Modules.EventSourcing;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.EventSourcing.Dto
{
    public class PublishedEventQueueItemToPublishedEventQueueItemDtoMapping : ClassMapping<PublishedEventQueueItem, PublishedEventQueueItemDto>
    {
        public PublishedEventQueueItemToPublishedEventQueueItemDtoMapping()
        {
            Mapping = e => new PublishedEventQueueItemDto
            {
                Id = e.Id,
                Timestamp = e.Timestamp,
                BusinessEventId = e.BusinessEventId,
                PublishedOn = e.PublishedOn,
                LeasedOn = e.LeasedOn,
                BusinessEvent = e.BusinessEvent,
                Exception = e.Exception
            };
        }
    }
}
