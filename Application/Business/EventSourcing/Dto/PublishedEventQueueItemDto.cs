﻿using System;
namespace Smt.Atomic.Business.EventSourcing.Dto
{
    public class PublishedEventQueueItemDto
    {
        public long Id { get; set; }

        public byte[] Timestamp { get; set; }

        public Guid BusinessEventId { get; set; }

        public DateTime PublishedOn { get; set; }

        public DateTime? LeasedOn { get; set; }

        public string BusinessEvent { get; set; }

        public string Exception { get; set; }
    }
}
