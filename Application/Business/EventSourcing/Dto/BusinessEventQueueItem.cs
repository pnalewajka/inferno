﻿using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.EventSourcing.Dto
{
    public class BusinessEventQueueItem
    {
        public BusinessEvent BusinessEvent { get; private set; }

        public byte[] LeaseToken { get; private set; }

        public BusinessEventQueueItem(BusinessEvent businessEvent, byte[] leaseToken)
        {
            LeaseToken = leaseToken;
            BusinessEvent = businessEvent;
        }
    }
}