using System;
using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.EventSourcing.Interfaces
{
    public interface IBusinessEventHandler<in TBusinessEvent>
        where TBusinessEvent : BusinessEvent
    {
        void Handle(TBusinessEvent businessEvent);
    }

    public interface IBusinessEventHandler
    {
        void Handle(BusinessEvent businessEvent);

        Type GetBusinessEventType();
    }
}