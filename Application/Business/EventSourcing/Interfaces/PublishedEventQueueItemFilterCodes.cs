﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.EventSourcing.Interfaces
{
    public static class PublishedEventQueueItemFilterCodes
    {
        public const string Active = "active";
        public const string Failed = "failed";
    }
}
