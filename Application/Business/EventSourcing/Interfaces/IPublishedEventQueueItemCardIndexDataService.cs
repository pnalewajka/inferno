﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.EventSourcing.Dto;

namespace Smt.Atomic.Business.EventSourcing.Interfaces
{
    public interface IPublishedEventQueueItemCardIndexDataService : ICardIndexDataService<PublishedEventQueueItemDto>
    {
        BusinessResult ReExecute(long id);
    }
}

