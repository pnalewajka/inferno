﻿using System;
using Smt.Atomic.Business.EventSourcing.Dto;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Business.EventSourcing.Interfaces
{
    public interface IBusinessEventQueueService : IBusinessEventPublisher
    {
        /// <summary>
        /// Gets the first available business event from the business event processing queue
        /// This starts leasing time for the given business event. The consumer is supposed to 
        /// finish the processing within the given lease time, otherwise the event will be available
        /// for processing for other consumers
        /// </summary>
        /// <returns></returns>
        BusinessEventQueueItem GetNextBusinessEvent();

        /// <summary>
        /// Deletes the business event from the business event processing queue.
        /// If event is not leased by the given client it should throw the exception
        /// </summary>
        void DeleteBusinessEvent(BusinessEventQueueItem businessEventQueueItem);


        /// <summary>
        /// Cancel business event processing and return it to the business event processing queue
        /// </summary>
        void CancelBusinessEventLease(BusinessEventQueueItem businessEventQueueItem);

        /// <summary>
        /// Mark that processing of the given business event resulted in exception
        /// </summary>
        /// <param name="businessEventQueueItem"></param>
        /// <param name="exception"></param>
        void MarkWithException(BusinessEventQueueItem businessEventQueueItem, Exception exception);
    }
}