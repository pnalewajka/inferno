﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.ServiceOperations.Dto;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.ServiceOperations.Interfaces
{
    public interface ITimeReportSupportService
    {
        IList<TimeReportRow> GetTimeReportsToChange(TimeReportChangeServiceDto model);

        BoolResult ChangeProjectForTimeReport(TimeReportChangeServiceDto model);
    }
}
