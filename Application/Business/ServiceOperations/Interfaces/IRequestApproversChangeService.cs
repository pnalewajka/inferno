﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.ServiceOperations.Dto;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.ServiceOperations.Interfaces
{
    public interface IRequestApproversChangeService
    {
        ICollection<ApprovalGroupDto> GetApprovalsDtoToChange(long requestId);
        BoolResult ChangeApproverForRequest(RequestApproversChangeDto approversChangeServiceDto);
    }
}