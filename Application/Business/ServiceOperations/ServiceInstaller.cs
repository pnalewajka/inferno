﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.ServiceOperations.Interfaces;
using Smt.Atomic.Business.ServiceOperations.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.ServiceOperations
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            switch (containerType)
            {
                case ContainerType.WebApp:
                    container.Register(Component.For<ITimeReportSupportService>().ImplementedBy<TimeReportSupportService>().LifestyleTransient());
                    container.Register(Component.For<IRequestApproversChangeService>().ImplementedBy<RequestApproversChangeService>().LifestyleTransient());
                    break;
            }
        }
    }
}

