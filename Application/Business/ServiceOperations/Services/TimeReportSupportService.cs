﻿using Smt.Atomic.Business.ServiceOperations.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.Business.ServiceOperations.Dto;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Castle.Core.Logging;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Business.ServiceOperations.Resources;
using System.Data.Entity;

namespace Smt.Atomic.Business.ServiceOperations.Services
{
    public class TimeReportSupportService : ITimeReportSupportService
    {
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService;
        private readonly ILogger _logger;

        public TimeReportSupportService(IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
            ILogger logger)
        {
            _unitOfWorkService = unitOfWorkService;
            _logger = logger;
        }

        public IList<TimeReportRow> GetTimeReportsToChange(TimeReportChangeServiceDto modifyProject)
        {
            var predicate = GetPredicateForTimeReportRows(modifyProject);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var timeReportsRows = unitOfWork.Repositories.TimeReportRows.Include(u => u.TimeReport.Employee).Where(predicate).ToList();

                return timeReportsRows;
            }
        }

        public BoolResult ChangeProjectForTimeReport(TimeReportChangeServiceDto modifyProject)
        {
            var predicate = GetPredicateForTimeReportRows(modifyProject);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var timeReportsRows = unitOfWork.Repositories.TimeReportRows.Where(predicate).ToList();

                foreach (var timeReportRow in timeReportsRows)
                {
                    timeReportRow.ProjectId = modifyProject.ProjectDestinationId;
                }

                unitOfWork.Commit();
                _logger.Warn(PrepareLogMessage(modifyProject));

                return new BoolResult(true, AddNewAlert(AlertType.Success, TimeReportSupportResources.SuccessfulChanged));
            }
        }

        private string PrepareLogMessage(TimeReportChangeServiceDto model)
        {
            var messageToLogs = new StringBuilder();

            var projectMessage = $"Change logged hours from projectId: '{model.ProjectSourceId}' to projectId: '{model.ProjectDestinationId}'";
            var employeeMessage = model.EmployeeIds.Length > 0 ? $", for employeeIDs: '{string.Join(", ", model.EmployeeIds)}'" : ", for all employees";
            var periodMessage = (model.Year.HasValue && model.Month.HasValue) ? $", for time reports in: '{model.Month}.{model.Year}'" : ", for all TR ";
            var trStatusMessage = (model.TimeReportStatus.HasValue) ? $", for TR with status: '{model.TimeReportStatus}'" : ", for TR with any status";

            messageToLogs.Append(projectMessage).Append(employeeMessage).Append(periodMessage).Append(trStatusMessage);

            return messageToLogs.ToString();
        }

        private Func<TimeReportRow, bool> GetPredicateForTimeReportRows(TimeReportChangeServiceDto modifyProject)
        {
            Func<TimeReportRow, bool> predicate = x => x.ProjectId == modifyProject.ProjectSourceId;

            if (modifyProject.EmployeeIds.Length > 0)
            {
                predicate = predicate.And(x => modifyProject.EmployeeIds.Contains(x.TimeReport.EmployeeId));
            }

            if (modifyProject.Year.HasValue && modifyProject.Month.HasValue)
            {
                predicate = predicate.And(x => x.TimeReport.Month == modifyProject.Month.Value &&
                                             x.TimeReport.Year == modifyProject.Year.Value);
            }

            if (modifyProject.TimeReportStatus.HasValue)
            {
                predicate = predicate.And(x => x.TimeReport.Status == modifyProject.TimeReportStatus.Value);
            }

            return predicate;
        }

        private List<AlertDto> AddNewAlert(AlertType type, string message)
        {
            var alerts = new List<AlertDto>();

            alerts.Insert(0, new AlertDto
            {
                Type = type,
                Message = message,
                IsDismissable = true
            });

            return alerts;
        }
    }
}
