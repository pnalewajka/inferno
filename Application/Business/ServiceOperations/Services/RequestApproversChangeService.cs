﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using Castle.Core.Logging;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.ServiceOperations.Dto;
using Smt.Atomic.Business.ServiceOperations.Interfaces;
using Smt.Atomic.Business.ServiceOperations.Resources;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.ServiceOperations.Services
{
    public class RequestApproversChangeService : IRequestApproversChangeService
    {
        private readonly ILogger _logger;
        private readonly IClassMapping<Request, RequestDto> _requestToDtoMapper;
        private readonly IUnitOfWorkService<IWorkflowsDbScope> _unitOfWorkService;

        public RequestApproversChangeService(IUnitOfWorkService<IWorkflowsDbScope> unitOfWorkService,
            ILogger logger,
            IClassMapping<Request, RequestDto> requestToDtoMapper)
        {
            _unitOfWorkService = unitOfWorkService;
            _logger = logger;
            _requestToDtoMapper = requestToDtoMapper;
        }

        public ICollection<ApprovalGroupDto> GetApprovalsDtoToChange(long requestId)
        {
            return GetRequestDto(requestId).ApprovalGroups;
        }

        public BoolResult ChangeApproverForRequest(RequestApproversChangeDto approversChangeServiceDto)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var approval = unitOfWork.Repositories.Approvals.GetById(approversChangeServiceDto.ApprovalId);

                if (approval.ApprovalGroup.RequestId != approversChangeServiceDto.RequestId)
                {
                    return new BoolResult(false, AddNewAlert(AlertType.Error, RequestApproversChangeServiceResources.FailedChanged));
                }
              
                var userId = unitOfWork.Repositories.Employees.GetById(approversChangeServiceDto.EmployeeId).UserId;

                if (userId != null)
                {
                    approval.UserId = userId.Value;
                }

                unitOfWork.Commit();
                _logger.Warn(PrepareLogMessage(approversChangeServiceDto));

                return new BoolResult(true, AddNewAlert(AlertType.Success, RequestApproversChangeServiceResources.SuccessfulChanged));
            }
        }

        private RequestDto GetRequestDto(long requestId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var request = unitOfWork.Repositories.Requests
                    .Include(r => r.RequestingUser)
                    .Include(r => r.ApprovalGroups)
                    .GetById(requestId);

                return _requestToDtoMapper.CreateFromSource(request);
            }
        }

        private string PrepareLogMessage(RequestApproversChangeDto approversChangeServiceDto)
        {
            return $"Approver {approversChangeServiceDto.ApprovalId} changed to {approversChangeServiceDto.EmployeeId} ";
        }

        private List<AlertDto> AddNewAlert(AlertType type, string message)
        {
            var alerts = new List<AlertDto>();

            alerts.Insert(0, new AlertDto
            {
                Type = type,
                Message = message,
                IsDismissable = true
            });

            return alerts;
        }
    }
}