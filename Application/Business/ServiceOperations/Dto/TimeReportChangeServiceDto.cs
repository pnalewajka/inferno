﻿using Smt.Atomic.CrossCutting.Business.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.ServiceOperations.Dto
{
    public class TimeReportChangeServiceDto
    {
        public int? Month { get; set; }
        
        public int? Year { get; set; }

        public long ProjectSourceId { get; set; }

        public long ProjectDestinationId { get; set; }
        
        public long[] EmployeeIds { get; set; }

        public TimeReportStatus? TimeReportStatus { get; set; }
    }
}
