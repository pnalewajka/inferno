﻿namespace Smt.Atomic.Business.ServiceOperations.Dto
{
    public class RequestApproversChangeDto
    {
        public long RequestId { get; set; }

        public long ApprovalId { get; set; }

        public long EmployeeId { get; set; }
    }
}