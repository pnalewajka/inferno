﻿using System;

namespace Smt.Atomic.Business.CustomerPortal.Interfaces
{
    public interface IResourceFormApproveData
    {
        string Rate { get; }

        long[] SeniorityLevels { get; }

        long MainTechnology { get; }

        long Region { get; }

        long ResourceFormId { get; }
    }
}
