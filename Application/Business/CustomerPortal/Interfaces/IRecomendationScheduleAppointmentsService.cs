using System.Collections.Generic;
using Smt.Atomic.Business.CustomerPortal.Dto;

namespace Smt.Atomic.Business.CustomerPortal.Interfaces
{
    public interface IRecomendationScheduleAppointmentsService
    {
        IEnumerable<ScheduleAppointmentDto> GetAppointments();

        IEnumerable<ChallengeDto> GetChallenges();

        long? ApproveAppointment(long appointmentId);

        long? ReSendNotification(long appointmentId);
    }
}