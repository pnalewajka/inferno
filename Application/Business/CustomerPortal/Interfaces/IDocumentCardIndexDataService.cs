﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.CustomerPortal.Dto;

namespace Smt.Atomic.Business.CustomerPortal.Interfaces
{
    public interface IDocumentCardIndexDataService : ICardIndexDataService<DocumentDto>
    {
        /// <summary>
        /// Gets an array of documents which are belong to specified recommendation
        /// </summary>
        /// <param name="recommendationId">A recommendation Id</param>
        /// <returns>An array of documents</returns>
        DocumentDto[] GetDocumentsByRecommendationId(long recommendationId);

        void UpdateDocumentsContent(long[] documentIds);

        DocumentContentDto GetContentById(long id);

        IEnumerable<DocumentContentDto> GetContentsByIds(long[] documentIds);
    }
}

