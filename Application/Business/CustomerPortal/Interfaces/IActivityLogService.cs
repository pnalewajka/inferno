﻿using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.Interfaces
{
    public interface IActivityLogService
    {
        IActivityLogEntryService GetNewRecommendationActivityService(IUnitOfWork<ICustomerPortalDbScope> unitOfWork);

        IActivityLogEntryService GetResourceFormApprovedActivityService(IUnitOfWork<ICustomerPortalDbScope> unitOfWork);

        IActivityLogEntryService GetResourceFormQuestionActivityService(IUnitOfWork<ICustomerPortalDbScope> unitOfWork);

        IActivityLogEntryService GetAppointmentScheduledActivityService(IUnitOfWork<ICustomerPortalDbScope> unitOfWork);

        IActivityLogEntryService GetChallengeAnsweredActivityService(IUnitOfWork<ICustomerPortalDbScope> unitOfWork);
    }
}
