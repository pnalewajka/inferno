using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.CustomerPortal.Dto;

namespace Smt.Atomic.Business.CustomerPortal.Interfaces
{
    public interface IQuestionCardIndexDataService : ICardIndexDataService<QuestionDto>
    {
        /// <summary>
        /// Gets question records which are ready to sent to the SDM
        /// </summary>
        /// <returns>An array of Question records</returns>
        QuestionDto[] GetReadyToSendRecords();

        /// <summary>
        /// Marks records with specified Ids as send (WasSent = true)
        /// </summary>
        /// <param name="questionIds">An array of Question record ids</param>
        /// <returns></returns>
        BoolResult MarkAsSendRecords(IReadOnlyCollection<long> questionIds);
    }
}

