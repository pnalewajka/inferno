using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.CustomerPortal.Dto;

namespace Smt.Atomic.Business.CustomerPortal.Interfaces
{
    public interface IRecommendationCardIndexDataService : ICardIndexDataService<RecommendationDto>
    {
        bool CanAccesDocument(long recommendationId, long documentId);

        /// <summary>
        ///     Get an array of records, that are not archived and withdrawn for given userId and neededResourceId
        /// </summary>
        /// <returns>An array of Recommendation records</returns>
        RecommendationDto[] GetRecords(long userId, long? neededResourceId);

        BusinessResult RejectRecommendation(long recommendationId, string comment);

        BusinessResult RefreshRecommendations(IEnumerable<long> recommendationIds);

        long? GetRecommendationByCrmId(Guid crmId);

        RecommendationDto GetRecommendationById(long id);
    }
}