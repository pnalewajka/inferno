﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.CustomerPortal.Dto;

namespace Smt.Atomic.Business.CustomerPortal.Interfaces
{
    public interface IChallengeCardIndexDataService : ICardIndexDataService<ChallengeDto>
    {
        void SendAssignmentToCustomer(long id, string comment);
        ChallengeDto GetByRecommendationId(long recommendationId);
        DocumentContentDto GetAnswerContentById(long documentId);
    }
}

