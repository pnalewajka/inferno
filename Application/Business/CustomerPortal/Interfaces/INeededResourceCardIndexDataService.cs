﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.CustomerPortal.Dto;

namespace Smt.Atomic.Business.CustomerPortal.Interfaces
{
    public interface INeededResourceCardIndexDataService : ICardIndexDataService<NeededResourceDto>
    {
        /// <summary>
        /// Get a record by crmId
        /// </summary>
        /// <param name="crmId"></param>
        /// <returns>NeededResource record</returns>
        NeededResourceDto GetRecordByCrmId(Guid crmId);

        /// <summary>
        /// Get an array of records by crmIds
        /// </summary>
        /// <returns>An array of NeededResource records</returns>
        NeededResourceDto[] GetRecordsByCrmIds(Guid[] crmIds);

        /// <summary>
        /// Get an array of dashboard items by user
        /// </summary>
        /// <param name="userId">Dante user id</param>
        /// <returns>An array of dashboard itoms</returns>
        NeededResourceDto[] GetVacanciesByUserId(long userId);

        /// <summary>
        /// Set OnHold status to needed resource
        /// </summary>
        /// <param name="neededResourceId"></param>
        /// <param name="reason"></param>
        bool PutOnHold(long neededResourceId, string reason);

        /// <summary>
        /// Resume needed resource from OnHold state
        /// </summary>
        /// <param name="neededResourceId"></param>
        void Resume(long neededResourceId, string reason);

        /// <summary>
        /// Create business event for refreshing needed resources
        /// </summary>
        /// <param name="neededResourceIds"></param>
        void TriggerNeededResourcesRefresh(IEnumerable<long> neededResourceIds);

        IReadOnlyCollection<ActivityLogDto> GetActivityLogs();

        void DiscardActivity(long activityId);
    }
}

