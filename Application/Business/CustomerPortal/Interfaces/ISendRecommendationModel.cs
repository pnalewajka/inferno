﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.CustomerPortal.Interfaces
{
    public interface ISendRecommendationModel
    {
        long Id { get; }

        string Rate { get; }

        string Availability { get; }

        string Note { get; }

        int? Years { get; }
    }
}
