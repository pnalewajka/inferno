using System.Collections.Generic;
using System.Threading.Tasks;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.Notifications.Dto;

namespace Smt.Atomic.Business.CustomerPortal.Interfaces
{
    public interface IRecommendationEmailMessageFactory
    {
        Task<EmailMessageDto> CreateNewRecommendationEmailMessageAsync(RecommendationDto recommendationDto, IEnumerable<DocumentDto> documentDtos);

        Task<EmailMessageDto> CreateRecommendationStateChangedEmailMessageAsync(RecommendationDto recommendationDto);

        Task<EmailMessageDto> CreateRecommendationQuestionEmailMessageAsync(RecommendationDto recommendationDto, QuestionDto questionDto);

        Task<IEnumerable<EmailMessageDto>> CreateRecommendationInPendingStateSummaryEmailMessagesAsync(IEnumerable<RecommendationDto> recommendationDtos);
    }
}
