﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.CustomerPortal.Dto;

namespace Smt.Atomic.Business.CustomerPortal.Interfaces
{
    public interface INoteCardIndexDataService : ICardIndexDataService<NoteDto>
    {
        NoteDto[] GetNotesByRecommendationId(long recommendationId);
        NoteDto[] GetNotesToSaveInCRM();
    }
}

