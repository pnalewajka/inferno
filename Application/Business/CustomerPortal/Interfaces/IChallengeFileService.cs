﻿using System;
using Smt.Atomic.Business.CustomerPortal.Dto;

namespace Smt.Atomic.Business.CustomerPortal.Interfaces
{
    public interface IChallengeFileService
    {
        Guid AddTemporaryChallengeFile(ChallengeFileDto challengeFileDto);
    }
}
