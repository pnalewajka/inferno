﻿using System;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.Accounts.Dto;

namespace Smt.Atomic.Business.CustomerPortal.Interfaces
{
    public interface IClientDataCardIndexDataService : ICardIndexDataService<ClientDataDto>
    {
        AccountManagerDto GetAccountManagerForClient(long clientId);
    }
}

