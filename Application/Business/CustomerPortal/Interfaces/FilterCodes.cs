﻿namespace Smt.Atomic.Business.CustomerPortal.Interfaces
{
    public static class FilterCodes
    {
        public const string OnHoldRequestsFilter = "onHold";
        public const string ActiveRequestsFilter = "active";
        public const string HasPendingAppointments = "pending-appointments";
        public const string HasChallenges = "challenges";
        public const string RecommendationStatusFilter = "status";
        public const string RecommendationSeenByCustomer = "seen";
        public const string ResourceFormState = "form-state";
        public const string ResourceFormInCRM = "in-crm";
        public const string NoAnswers = "no-answers";
        public const string HasAnswers = "has-answers";
        public const string AppointmentStatus = "appointment-status";
        public const string ManagerUser = "manager-user";
        public const string ClientId = "client-id";
    }
}
