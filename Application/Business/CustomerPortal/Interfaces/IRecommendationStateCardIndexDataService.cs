using Smt.Atomic.Business.CustomerPortal.Dto;

namespace Smt.Atomic.Business.CustomerPortal.Interfaces
{
    public interface IRecommendationStateCardIndexDataService
    {
        RecommendationDto ScheduleRecomendationNextStep(IRecomendationSchedule nextStepData);
        RecommendationDto ScheduleChallengeNextStep(IRecomendationChallenge nextStepData);

        void ApproveRecommendation(IRecommendationApproved approveData);
    }
}