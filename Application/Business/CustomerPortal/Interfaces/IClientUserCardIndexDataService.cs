﻿using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Common.Interfaces;

namespace Smt.Atomic.Business.CustomerPortal.Interfaces
{
    public interface IClientUserCardIndexDataService : ICardIndexDataService<UserDto>
    {
    }
}
