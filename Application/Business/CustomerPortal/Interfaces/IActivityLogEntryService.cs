using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.Interfaces
{
    public interface IActivityLogEntryService
    {
        void RegisterActivity(long entityId);

        void ClearActivity(long entityId);
    }
}