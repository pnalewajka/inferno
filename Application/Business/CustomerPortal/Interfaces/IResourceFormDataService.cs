﻿using System.Collections.Generic;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.Common.Interfaces;

namespace Smt.Atomic.Business.CustomerPortal.Interfaces
{
    public interface IResourceFormDataService : ICardIndexDataService<ResourceFormDto>
    {
        IReadOnlyCollection<ResourceFormDto> GetCurrentUserResources();

        IReadOnlyCollection<LanguageOptionDto> GetLanguageOptions();

        ResourceFormDto ApproveResourceForm(long id, IResourceFormApproveData formData);

        void AskForDetails(long resourceFormId, string question);

        ResourceFormDto GetResourceFormById(long id);
    }
}