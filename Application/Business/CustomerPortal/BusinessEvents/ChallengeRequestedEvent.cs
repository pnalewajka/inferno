﻿using System;
using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEvents
{
    public class ChallengeRequestedEvent : BusinessEvent
    {
        public int FilesCount { get; set; }
        public Guid[] FileIds { get; set; }
        public long RecommendationId { get; set; }
        public DateTime Deadline { get; set; }
        public Guid ResourceId { get; set; }
    }
}
