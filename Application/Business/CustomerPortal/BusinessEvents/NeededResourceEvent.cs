﻿using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEvents
{
    public abstract class NeededResourceEvent : BusinessEvent
    {
        public long ClientUserId { get; set; }
        public string NeededResourceName { get; set; }
        public string Reason { get; set; }
    }
}
