﻿using System;
using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEvents
{
    public class RecommendationRejectedEvent : BusinessEvent
    {
        public Guid ResourceId { get; set; }
        public DateTime RejectDateTime { get; set; }
        public string Comment { get; set; }
    }
}