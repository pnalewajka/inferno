﻿using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Data.Entities.Dto;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEvents
{
    [DataContract]
    public class AddRecruitmentActivityEvent : BusinessEvent
    {
        [DataMember]
        public Guid ResourceId { get; set; }

        [DataMember]
        public IEnumerable<SuggestedDate> SuggestedDates { get; set; }
    }
}
