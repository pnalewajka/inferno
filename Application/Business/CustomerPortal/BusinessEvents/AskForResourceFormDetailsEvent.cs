﻿using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEvents
{
    public class AskForResourceFormDetailsEvent : BusinessEvent
    {
        public long ResourceFormId { get; set; }
        public string Question { get; set; }
    }
}