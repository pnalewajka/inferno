﻿using System;
using System.Runtime.Serialization;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEvents
{
    [DataContract]
    public class SuggestedDate
    {
        [DataMember]
        public DateTime StartsOn { get; set; }

        [DataMember]
        public DateTime EndsOn { get; set; }
    }
}
