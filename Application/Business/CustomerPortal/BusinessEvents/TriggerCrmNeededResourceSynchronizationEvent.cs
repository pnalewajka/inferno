using System;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEvents
{
    public class TriggerCrmNeededResourceSynchronizationEvent : TriggerCrmClientSynchronizationEvent
    {
        public Guid NeededResourceId { get; set; }
    }
}