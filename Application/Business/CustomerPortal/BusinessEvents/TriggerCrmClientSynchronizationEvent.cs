﻿using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEvents
{
    public class TriggerCrmClientSynchronizationEvent : BusinessEvent
    {
        public long ClientDataId { get; set; }
    }
}