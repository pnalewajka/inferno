﻿using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Data.Entities.Dto;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEvents
{
    [DataContract]
    public class NotifyAboutInterviewScheduledEvent : BusinessEvent
    {
        [DataMember]
        public string ResourceName { get; set; }

        [DataMember]
        public string NeededResourceName { get; set; }

        [DataMember]
        public string ContactName { get; set; }

        [DataMember]
        public string Comment { get; set; }

        [DataMember]
        public long NeededResourceId { get; set; }

        [DataMember]
        public long OperationManagerId { get; set; }

        [DataMember]
        public IEnumerable<SuggestedDate> SuggestedDates { get; set; }
    }
}
