﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEvents
{
    public class SeenByCustomerEvent: BusinessEvent
    {
        public Guid RecommendationCrmId { get; set; }

        public long UserId { get; set; }
    }
}
