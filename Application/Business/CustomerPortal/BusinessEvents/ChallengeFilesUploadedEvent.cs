﻿using Smt.Atomic.Data.Entities.Dto;
using System;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEvents
{
    public class ChallengeFilesUploadedEvent : BusinessEvent
    {
        public long RecommendationId { get; set; }
        public DateTime Deadline { get; set; }
    }
}
