namespace Smt.Atomic.Business.CustomerPortal.BusinessEvents
{
    public class TriggerRecommendationDocumentSynchronizationEvent : TriggerCrmRecruitmentActivitySynchronizationEvent
    {
        public string DocumentUrl { get; set; }
    }
}