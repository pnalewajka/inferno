﻿using System;
using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEvents
{
    public class NotifyScheduleAppointmentEvent : BusinessEvent
    {
        public long OperationManagerId { get; set; }

        public long RecommendationId { get; set; }

        public DateTime AppointmentStartDateTime { get; set; }

        public DateTime AppointmentEndDateTime { get; set; }
    }
}