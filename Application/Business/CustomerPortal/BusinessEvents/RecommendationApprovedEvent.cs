﻿using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEvents
{
    public class RecommendationApprovedEvent : BusinessEvent
    {
        public long RecommendationId { get; set; }

        public long ClientUserId { get; set; }

        public string Comment { get; set; }
    }
}
