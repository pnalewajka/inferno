﻿using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEvents
{
    public class AssignmentSentBusinessEvent : BusinessEvent
    {
        public string Comment { get; set; }
        public long RecommendationId { get; set; }
    }
}
