using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEvents
{
    public class DownloadSharepointDocumentEvent : BusinessEvent
    {
        public long RecommendationId { get; set; }

        public string AbsoluteUrlPath { get; set; }
    }
}