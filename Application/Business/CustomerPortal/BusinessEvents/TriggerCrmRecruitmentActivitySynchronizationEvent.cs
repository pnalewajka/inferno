﻿using System;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEvents
{
    public class TriggerCrmRecruitmentActivitySynchronizationEvent : TriggerCrmNeededResourceSynchronizationEvent
    {
        public Guid RecruitmentActivityId { get; set; }
    }
}