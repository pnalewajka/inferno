﻿using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEvents
{
    public class ResourceRequestSubmittedEvent : BusinessEvent
    {
        public string AdditionalComment { get; set; }
        public long? CustomerUserId { get; set; }
        public string LanguageComment { get; set; }
        public string Location { get; set; }
        public string PlannedStart { get; set; }
        public string PositionRequirements { get; set; }
        public string ProjectDescription { get; set; }
        public string ProjectDuration { get; set; }
        public string RecruitmentPhases { get; set; }
        public long? RequestId { get; set; }
        public int ResourceCount { get; set; }
        public string ResourceName { get; set; }
        public string RoleDescription { get; set; }
    }
}
