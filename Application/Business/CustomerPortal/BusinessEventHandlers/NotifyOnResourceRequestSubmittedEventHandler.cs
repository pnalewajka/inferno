﻿using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.CustomerPortal.BusinessEvents;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Crm.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using System;
using System.Linq;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEventHandlers
{
    public class NotifyOnResourceRequestSubmittedEventHandler : BusinessEventHandler<ResourceRequestSubmittedEvent>
    {
        private readonly IClientDataCardIndexDataService _clientDataCardIndexDataService;
        private readonly ICrmService _crmService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IUnitOfWorkService<ICustomerPortalDbScope> _unitOfWorkService;
        private readonly IUserService _userService;

        public NotifyOnResourceRequestSubmittedEventHandler(
            ICrmService crmService,
            IUserService userService,
            IClientDataCardIndexDataService clientDataCardIndexDataService,
            IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService,
            IMessageTemplateService messageTemplateService,
            IReliableEmailService reliableEmailService,
            ISystemParameterService systemParameterService)
        {
            _crmService = crmService;
            _userService = userService;
            _unitOfWorkService = unitOfWorkService;
            _clientDataCardIndexDataService = clientDataCardIndexDataService;
            _messageTemplateService = messageTemplateService;
            _reliableEmailService = reliableEmailService;
            _systemParameterService = systemParameterService;
        }
        public override void Handle(ResourceRequestSubmittedEvent businessEvent)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var serverHost = _systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);
                var scheduleAppointmentUrlFormat = _systemParameterService.GetParameter<string>(ParameterKeys.CustomerPortalReviewFormUrlFormat);
                var reviewFormUrl = string.Format(scheduleAppointmentUrlFormat, serverHost, businessEvent.RequestId);

                var request = unitOfWork.Repositories.ResourceForms.GetById(businessEvent.RequestId.Value);
                var requiredLanguages = string.Join(", ", request.LanguageOption.Select(l => $"{l.LanguageName}: {l.LevelName}").ToArray());

                var clientData = unitOfWork.Repositories.ClientDatas
                    .FirstOrDefault(cd => cd.ClientUserId == businessEvent.CustomerUserId);

                if (clientData == null)
                {
                    throw new BusinessException($"Cannot retrieve customer data from CRM because user [ID:${businessEvent.CustomerUserId}]");
                }

                var customerContact = _crmService.GetClientContactInfo(clientData.ClientContact.CrmId);
                var resourceRequestSubmittedEmailDto = new ResourceRequestSubmittedEmailDto
                {
                    ResourceName = businessEvent.ResourceName,
                    CustomerName = customerContact.CompanyName,
                    ResourceCount = businessEvent.ResourceCount,
                    RoleDescription = businessEvent.RoleDescription,
                    PositionRequirements = businessEvent.PositionRequirements,
                    ProjectDescription = businessEvent.ProjectDescription,
                    ProjectDuration = businessEvent.ProjectDuration,
                    PlannedStart = businessEvent.PlannedStart,
                    Location = businessEvent.Location,
                    RecruitmentPhases = businessEvent.RecruitmentPhases,
                    RequiredLanguages = requiredLanguages,
                    LanguageComment = businessEvent.LanguageComment,
                    AdditionalComment = businessEvent.AdditionalComment,
                    ReviewFormUrl = reviewFormUrl,
                    FooterHtml = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalNotificationsFooter, null).Content
                };

                var body = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalResourceRequestSubmittedBody, resourceRequestSubmittedEmailDto);
                var subject = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalResourceRequestSubmittedSubject, resourceRequestSubmittedEmailDto);

                _reliableEmailService.EnqueueMessage(new EmailMessageDto
                {
                    Body = body.Content,
                    Subject = subject.Content,
                    IsHtml = body.IsHtml,
                    Recipients = new[] { new EmailRecipientDto(clientData.ManagerUser.Email, clientData.ManagerUser.FullName ) }.ToList()
                });
            }

        }
    }
}
