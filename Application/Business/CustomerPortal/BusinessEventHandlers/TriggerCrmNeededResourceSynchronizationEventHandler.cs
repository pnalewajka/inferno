using System.Linq;
using Smt.Atomic.Business.CustomerPortal.BusinessEvents;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Data.Crm.Interfaces;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEventHandlers
{
    public class TriggerCrmNeededResourceSynchronizationEventHandler : BusinessEventHandler<
        TriggerCrmNeededResourceSynchronizationEvent>
    {
        private readonly IBusinessEventPublisher _businessEventPublisher;
        private readonly ICrmNeededResourcesService _crmNeededResourcesService;
        private readonly IUnitOfWorkService<ICustomerPortalDbScope> _unitOfWorkService;

        public TriggerCrmNeededResourceSynchronizationEventHandler(
            ICrmNeededResourcesService crmNeededResourcesService,
            IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService,
            IBusinessEventPublisher businessEventPublisher)
        {
            _crmNeededResourcesService = crmNeededResourcesService;
            _unitOfWorkService = unitOfWorkService;
            _businessEventPublisher = businessEventPublisher;
        }

        public override void Handle(TriggerCrmNeededResourceSynchronizationEvent businessEvent)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recruitmentActivitiesGuids =
                    _crmNeededResourcesService.LoadRecruitmentActivitiesGuids(businessEvent.NeededResourceId)
                        .ToList();

                if (!recruitmentActivitiesGuids.Any())
                {
                    return;
                }

                var neededResourceDto = _crmNeededResourcesService.LoadNeededResource(businessEvent.NeededResourceId);

                var neededResource =
                    unitOfWork.Repositories.NeededResources.SingleOrDefault(
                        nr => nr.CrmId == businessEvent.NeededResourceId);

                if (neededResource == null)
                {
                    unitOfWork.Repositories.NeededResources.Add(new NeededResource
                    {
                        CrmId = businessEvent.NeededResourceId,
                        Name = neededResourceDto.Name
                    });
                }
                else
                {
                    neededResource.Name = neededResourceDto.Name;
                }

                unitOfWork.Commit();

                foreach (var recruitmentActivitiesGuid in recruitmentActivitiesGuids)
                {
                    _businessEventPublisher.PublishBusinessEvent(new TriggerCrmRecruitmentActivitySynchronizationEvent
                    {
                        ClientDataId = businessEvent.ClientDataId,
                        NeededResourceId = businessEvent.NeededResourceId,
                        RecruitmentActivityId = recruitmentActivitiesGuid
                    });
                }
            }
        }
    }
}