﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.CustomerPortal.BusinessEvents;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Crm.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Business.Configuration.Interfaces;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEventHandlers
{
    public class RecommendationRejectedEventHandler : BusinessEventHandler<RecommendationRejectedEvent>
    {
        private readonly ICrmService _crmService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IUnitOfWorkService<ICustomerPortalDbScope> _unitOfWorkService;
        private readonly IReliableEmailService _emailService;


        public RecommendationRejectedEventHandler(
            ICrmService crmService,
            IMessageTemplateService messageTemplateService,
            IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService,
            IReliableEmailService emailService)
        {
            _crmService = crmService;
            _messageTemplateService = messageTemplateService;
            _unitOfWorkService = unitOfWorkService;
            _emailService = emailService;
        }

        public override void Handle(RecommendationRejectedEvent businessEvent)
        {
            var message = GetEmailDto(businessEvent.ResourceId, businessEvent.Comment);

            UpdateRecruitmentActivityDecision(businessEvent);

            UpdateCrmNote(businessEvent);

            _emailService.EnqueueMessage(message);
        }

        private void UpdateRecruitmentActivityDecision(RecommendationRejectedEvent businessEvent)
        {
            _crmService.UpdateRecruitmentActivityClientDecision(
                businessEvent.ResourceId,
                businessEvent.RejectDateTime,
                true,
                businessEvent.Comment
                );
        }

        private void UpdateCrmNote(RecommendationRejectedEvent businessEvent)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var resource = unitOfWork.Repositories.Recommendations.First(r => r.ResourceId == businessEvent.ResourceId);
                var clientData = unitOfWork.Repositories.ClientDatas.Single(cd => cd.ClientUserId == resource.UserId);

                _crmService.AddRecruitmentActivityNote(businessEvent.ResourceId, "Rejection info", $"{clientData.ClientContact.CompanyName} has rejected the candidate: {businessEvent.Comment}");
            }
        }

        private EmailMessageDto GetEmailDto(Guid resourceId, string note)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var resource = unitOfWork.Repositories.Recommendations.First(r => r.ResourceId == resourceId);
                var clientData = unitOfWork.Repositories.ClientDatas.Single(cd => cd.ClientUserId == resource.UserId);

                var emailDto = new RecommendationRejectEmailDto
                {
                    ResourceName = resource.Name,
                    NeededResourceName = resource.NeededResource.Name,
                    CommentLines = StringHelper.BreakString(note),
                    ContactName = clientData.ClientContact.Name,
                    CompanyName = clientData.ClientContact.CompanyName,
                    CrmUrl = _crmService.RecruitmentActivityUrl(resourceId),
                };
                
                var subject = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalNotificationsOnRecommendationRejectedSubject, emailDto);
                var body = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalNotificationsOnRecommendationRejectedBody, emailDto);

                return new EmailMessageDto
                {
                    IsHtml = body.IsHtml,
                    Subject = subject.Content,
                    Body = body.Content,
                    Recipients = new List<EmailRecipientDto> { new EmailRecipientDto(clientData.ManagerUser.Email, clientData.ManagerUser.FullName) }
                };
            }
        }
    }
}
