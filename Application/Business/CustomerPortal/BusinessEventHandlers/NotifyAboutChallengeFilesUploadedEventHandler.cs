﻿using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.CustomerPortal.BusinessEvents;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using System.Collections.Generic;
using System.Linq;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEventHandlers
{
    public class NotifyAboutChallengeFilesUploadedEventHandler : BusinessEventHandler<ChallengeFilesUploadedEvent>
    {
        private readonly IClientDataCardIndexDataService _clientDataCardIndexDataService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IUnitOfWorkService<ICustomerPortalDbScope> _unitOfWorkService;
        private readonly IUserService _userService;

        public NotifyAboutChallengeFilesUploadedEventHandler(
            IUserService userService,
            IMessageTemplateService messageTemplateService,
            IReliableEmailService reliableEmailService,
            ISystemParameterService systemParameterService,
            IClientDataCardIndexDataService clientDataCardIndexDataService,
            IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService)
        {
            _userService = userService;
            _unitOfWorkService = unitOfWorkService;
            _clientDataCardIndexDataService = clientDataCardIndexDataService;
            _messageTemplateService = messageTemplateService;
            _reliableEmailService = reliableEmailService;
            _systemParameterService = systemParameterService;
        }
        public override void Handle(ChallengeFilesUploadedEvent businessEvent)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recommendation = unitOfWork.Repositories.Recommendations.GetById(businessEvent.RecommendationId);
                var challenge = unitOfWork.Repositories.Challenges.SingleOrDefault(c => c.RecommendationId == businessEvent.RecommendationId);
                var operationManager = _clientDataCardIndexDataService.GetAccountManagerForClient(recommendation.UserId.Value);
                var challengeRequestedEmailDto = new ChallengeRequestedEmailDto
                {
                    ResourceName = recommendation.Name,
                    NeededResourceName = recommendation.NeededResource.Name,
                    Deadline = challenge.Deadline,
                    RecommendationUrl = GetResourceUrl(recommendation.Id, recommendation.NeededResourceId.Value),
                    FooterHtml = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalNotificationsFooter, null).Content
                };

                var body = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalChallengeRequestBody, challengeRequestedEmailDto);
                var subject = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalChallengeRequestSubject, challengeRequestedEmailDto);

                var attachments = new List<EmailAttachmentDto>();
                foreach (var document in challenge.ChallengeDocuments)
                {
                    attachments.Add(
                        new EmailAttachmentDto
                        {
                            Content = document.DocumentContent.Content,
                            Name = document.Name
                        });
                }

                _reliableEmailService.EnqueueMessage(new EmailMessageDto
                {
                    Body = body.Content,
                    Subject = subject.Content,
                    IsHtml = true,
                    Recipients = new[] { new EmailRecipientDto(operationManager.Email, operationManager.Name) }.ToList(),
                    Attachments = attachments
                });
            }
        }

        private string GetResourceUrl(long recommendationId, long neededResourceId)
        {
            var serverHost = _systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);
            var scheduleAppointmentUrlFormat = _systemParameterService.GetParameter<string>(ParameterKeys.CustomerPortalChallengeViewUrlFormat);
            var recommendationUrl = string.Format(scheduleAppointmentUrlFormat, serverHost, recommendationId, neededResourceId);

            return recommendationUrl;
        }
    }
}
