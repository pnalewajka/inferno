﻿using System.Linq;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Crm.BusinessEvents;
using Smt.Atomic.Data.Crm.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEventHandlers
{
    public class NotifyAboutResourceRequestApprovedEventHandler : BusinessEventHandler<CreateNeededResourceEvent>
    {
        private readonly IUnitOfWorkService<ICustomerPortalDbScope> _unitOfWorkService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly ICrmService _crmService;

        public NotifyAboutResourceRequestApprovedEventHandler(
            ICrmService crmService,
            IMessageTemplateService messageTemplateService,
            IReliableEmailService reliableEmailService,
            ISystemParameterService systemParameterService,
            IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService)
        {
            _crmService = crmService;
            _unitOfWorkService = unitOfWorkService;
            _messageTemplateService = messageTemplateService;
            _reliableEmailService = reliableEmailService;
            _systemParameterService = systemParameterService;
        }
        public override void Handle(CreateNeededResourceEvent businessEvent)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var resourceForm = unitOfWork.Repositories.ResourceForms.GetById(businessEvent.ResourceFormId);
                var customerUser = _crmService.GetClientContactInfo(businessEvent.ContactClientGuid);
                var operationManager = unitOfWork.Repositories.Users.GetById(businessEvent.OperationManagerId.Value);

                var resourceRequestAcceptedConfirmationEmailDto = new ResourceRequestAcceptedConfirmationEmailDto
                {
                    CustomerFirstName = customerUser.FirstName,
                    ResourceName = resourceForm.ResourceName,
                    SdmName = operationManager.FullName,
                    FooterHtml = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalNotificationsFooter, null).Content
                };

                var body = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalResourceRequestAcceptedBody, resourceRequestAcceptedConfirmationEmailDto);
                var subject = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalResourceRequestAcceptedSubject, resourceRequestAcceptedConfirmationEmailDto);

                _reliableEmailService.EnqueueMessage(new EmailMessageDto
                {
                    Body = body.Content,
                    Subject = subject.Content,
                    IsHtml = body.IsHtml,
                    Recipients = new[] {
                        new EmailRecipientDto(customerUser.Email, $"{customerUser.FirstName} {customerUser.LastName}"),
                        new EmailRecipientDto(operationManager.Email, operationManager.FullName, RecipientType.Cc),
                    }.ToList(),
                });
            }
        }
    }
}
