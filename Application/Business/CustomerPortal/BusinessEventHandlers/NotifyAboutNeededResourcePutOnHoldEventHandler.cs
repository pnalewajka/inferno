﻿using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.CustomerPortal.BusinessEvents;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using System.Linq;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEventHandlers
{
    public class NotifyAboutNeededResourcePutOnHoldEventHandler : BusinessEventHandler<NeededResourcePutOnHoldEvent>
    {
        private readonly IUnitOfWorkService<ICustomerPortalDbScope> _unitOfWorkService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IClientDataCardIndexDataService _clientDataCardIndexDataService;

        public NotifyAboutNeededResourcePutOnHoldEventHandler(
            IMessageTemplateService messageTemplateService,
            IReliableEmailService reliableEmailService,
            ISystemParameterService systemParameterService,
            IClientDataCardIndexDataService clientDataCardIndexDataService,
            IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService)
        {
            _clientDataCardIndexDataService = clientDataCardIndexDataService;
            _unitOfWorkService = unitOfWorkService;
            _messageTemplateService = messageTemplateService;
            _reliableEmailService = reliableEmailService;
            _systemParameterService = systemParameterService;
        }

        public override void Handle(NeededResourcePutOnHoldEvent businessEvent)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var client = unitOfWork.Repositories.Users.GetById(businessEvent.ClientUserId);
                var operationManager = _clientDataCardIndexDataService.GetAccountManagerForClient(businessEvent.ClientUserId);

                var neededResourceStatusChanged = new NeededResourceStatusChanged
                {
                    CustomerName = client.FullName,
                    NeededResource = businessEvent.NeededResourceName,
                    Reason = businessEvent.Reason,
                    FooterHtml = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalNotificationsFooter, null).Content
                };

                var body = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalNeededResourcePutOnHoldBody, neededResourceStatusChanged);
                var subject = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalNeededResourcePutOnHoldSubject, neededResourceStatusChanged);

                _reliableEmailService.EnqueueMessage(new EmailMessageDto
                {
                    Body = body.Content,
                    Subject = subject.Content,
                    IsHtml = body.IsHtml,
                    Recipients = new[] {
                        new EmailRecipientDto(operationManager.Email, operationManager.Name),
                    }.ToList()
                });
            }
        }
    }
}
