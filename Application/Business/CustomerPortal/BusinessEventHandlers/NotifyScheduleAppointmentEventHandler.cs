﻿using System;
using Smt.Atomic.Business.CustomerPortal.BusinessEvents;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Crm.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using System.Linq;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEventHandlers
{
    public class NotifyScheduleAppointmentEventHandler : BusinessEventHandler<NotifyScheduleAppointmentEvent>
    {
        private readonly IUnitOfWorkService<ICustomerPortalDbScope> _unitOfWorkService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly ICrmService _crmService;
        private readonly IUserService _userService;

        public NotifyScheduleAppointmentEventHandler(
            ICrmService crmService,
            IUserService userService,
            IMessageTemplateService messageTemplateService,
            IReliableEmailService reliableEmailService,
            ISystemParameterService systemParameterService,
            IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService)
        {
            _crmService = crmService;
            _userService = userService;
            _unitOfWorkService = unitOfWorkService;
            _messageTemplateService = messageTemplateService;
            _reliableEmailService = reliableEmailService;
            _systemParameterService = systemParameterService;
        }

        public override void Handle(NotifyScheduleAppointmentEvent businessEvent)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var operationManager = unitOfWork.Repositories.Users.GetById(businessEvent.OperationManagerId);
                var recommendation = unitOfWork.Repositories.Recommendations.GetById(businessEvent.RecommendationId);
                var customerUser = unitOfWork.Repositories.Users.GetById(recommendation.UserId.Value);
                var interviewer = _crmService.GetClientContactInfo(new Guid(customerUser.CrmId));
                var interviewInvitationEmailDto = new InterviewInvitationEmailDto
                {
                    CustomerName = interviewer.CompanyName,
                    Interviewer = customerUser.FullName,
                    Interviewee = recommendation.Name,
                    NeededResource = recommendation.NeededResource.Name,
                    OperationManager = operationManager.FullName,
                    StartsOn = businessEvent.AppointmentStartDateTime,
                    FooterHtml = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalNotificationsFooter, null).Content
                };

                var body = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalInterviewInvitationBody, interviewInvitationEmailDto);
                var subject = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalInterviewInvitationSubject, interviewInvitationEmailDto);

                _reliableEmailService.EnqueueMessage(new EmailMessageDto
                {
                    Body = body.Content,
                    Subject = subject.Content,
                    IsHtml = body.IsHtml,
                    Recipients = new[] {
                        new EmailRecipientDto(customerUser.Email, customerUser.FullName),
                        new EmailRecipientDto(operationManager.Email, operationManager.FullName),
                    }.ToList()
                });
            }
        }
    }
}