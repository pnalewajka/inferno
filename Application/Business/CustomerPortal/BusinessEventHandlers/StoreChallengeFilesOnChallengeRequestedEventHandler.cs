﻿using Smt.Atomic.Business.CustomerPortal.BusinessEvents;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using System.Linq;
using System.Transactions;
using Smt.Atomic.CrossCutting.Common.Exceptions;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEventHandlers
{
    public class StoreChallengeFilesOnChallengeRequestedEventHandler : BusinessEventHandler<ChallengeRequestedEvent>
    {
        private readonly IBusinessEventPublisher _businessEventPublisher;
        private readonly IUnitOfWorkService<ICustomerPortalDbScope> _unitOfWorkService;

        public StoreChallengeFilesOnChallengeRequestedEventHandler(
            IBusinessEventPublisher businessEventPublisher,
            IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService)
        {
            _businessEventPublisher = businessEventPublisher;
            _unitOfWorkService = unitOfWorkService;
        }

        public override void Handle(ChallengeRequestedEvent businessEvent)
        {
            using (var transaction = new TransactionScope())
            {
                using (var unitOfWork = _unitOfWorkService.Create())
                {
                    MoveUplodedFilesToDocument(businessEvent, unitOfWork);

                    _businessEventPublisher.PublishBusinessEvent(new ChallengeFilesUploadedEvent
                    {
                        Deadline = businessEvent.Deadline,
                        RecommendationId = businessEvent.RecommendationId,
                    });

                    transaction.Complete();
                }
            }
        }

        private void MoveUplodedFilesToDocument(ChallengeRequestedEvent businessEvent, IUnitOfWork<ICustomerPortalDbScope> unitOfWork)
        {
            var challengeFiles = unitOfWork.Repositories.ChallengeFiles.Where(cf => businessEvent.FileIds.Contains(cf.Identifier));
            var challenge = unitOfWork.Repositories.Challenges.SingleOrDefault(c => c.RecommendationId == businessEvent.RecommendationId);

            if (challenge == null)
            {
                throw new BusinessException($"Challenge for Recommendation Id={businessEvent.RecommendationId} was not created");
            }

            foreach (var file in challengeFiles)
            {
                var document = new ChallengeDocument
                {
                    ContentLength = file.ContentLength,
                    ContentType = file.ContentType,
                    ChallengeId = challenge.Id,
                    Name = file.Name,
                    DocumentContent = new ChallengeDocumentContent
                    {
                        Content = file.Data
                    },
                };

                challenge.ChallengeDocuments.Add(document);
            }

            unitOfWork.Repositories.ChallengeFiles.DeleteRange(challengeFiles);
            unitOfWork.Commit();
        }
    }
}
