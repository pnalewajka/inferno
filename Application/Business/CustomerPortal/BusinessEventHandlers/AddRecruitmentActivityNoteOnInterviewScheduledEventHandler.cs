﻿using Smt.Atomic.Business.CustomerPortal.BusinessEvents;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Data.Crm.Interfaces;
using System.Linq;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEventHandlers
{
    public class AddRecruitmentActivityNoteOnInterviewScheduledEventHandler : BusinessEventHandler<AddRecruitmentActivityEvent>
    {
        private readonly ICrmService _crmService;

        public AddRecruitmentActivityNoteOnInterviewScheduledEventHandler(ICrmService crmService)
        {
            _crmService = crmService;
        }

        public override void Handle(AddRecruitmentActivityEvent businessEvent)
        {
            var noteTitle = "Next step interview suggested";
            var scheduleNextStepCrmBodyFormat = "Interview dates: {0}";

            var suggestedDates = businessEvent.SuggestedDates.Select(sd => $"{sd.StartsOn} - {sd.EndsOn}");
            var text = string.Format(scheduleNextStepCrmBodyFormat, string.Join(", ", suggestedDates));

            _crmService.AddRecruitmentActivityNote(businessEvent.ResourceId, noteTitle, text);
        }
    }
}
