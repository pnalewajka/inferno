﻿using System.Linq;
using Smt.Atomic.Business.CustomerPortal.BusinessEvents;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.Data.Crm.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEventHandlers
{
    public class SeenByCustomerEventHandler : BusinessEventHandler<SeenByCustomerEvent>
    {
        private readonly ICrmService _crmService;
        private readonly IUnitOfWorkService<ICustomerPortalDbScope> _unitOfWorkService;

        public SeenByCustomerEventHandler(ICrmService crmService,
            IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService)
        {
            _crmService = crmService;
            _unitOfWorkService = unitOfWorkService;
        }

        public override void Handle(SeenByCustomerEvent businessEvent)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var clientData = unitOfWork.Repositories.ClientDatas.FirstOrDefault(f => f.ClientUserId == businessEvent.UserId);

                if (clientData == null)
                {
                    throw new BusinessException($"User with Id={businessEvent.UserId} has no Client data defined");
                }

                _crmService.AddRecruitmentActivityNote(businessEvent.RecommendationCrmId, "Profile visited by customer",
                    $"User {clientData.ClientContact.Name} ({clientData.ClientContact.CompanyName}) visited candidate's profile for the first time");
            }
        }
    }
}