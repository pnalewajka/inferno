﻿using Smt.Atomic.Business.CustomerPortal.BusinessEvents;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Data.Crm.Interfaces;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEventHandlers
{
    public class TriggerCrmClientSynchronizationEventHandler : BusinessEventHandler<TriggerCrmClientSynchronizationEvent>
    {
        private readonly IBusinessEventPublisher _businessEventPublisher;
        private readonly ICrmNeededResourcesService _crmNeededResourcesService;
        private readonly IUnitOfWorkService<ICustomerPortalDbScope> _unitOfWorkService;

        public TriggerCrmClientSynchronizationEventHandler(
            ICrmNeededResourcesService crmNeededResourcesService,
            IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService,
            IBusinessEventPublisher businessEventPublisher)
        {
            _crmNeededResourcesService = crmNeededResourcesService;
            _unitOfWorkService = unitOfWorkService;
            _businessEventPublisher = businessEventPublisher;
        }

        public override void Handle(TriggerCrmClientSynchronizationEvent businessEvent)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var clientData = unitOfWork.Repositories.ClientDatas.GetById(businessEvent.ClientDataId);

                var neededResources =
                    _crmNeededResourcesService.LoadCustomerNeededResourcesCodes(clientData.ClientContact.CrmId);

                foreach (var neededResourceId in neededResources)
                    _businessEventPublisher.PublishBusinessEvent(
                        new TriggerCrmNeededResourceSynchronizationEvent
                        {
                            ClientDataId = businessEvent.ClientDataId,
                            NeededResourceId = neededResourceId
                        });
            }
        }
    }
}