﻿using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.CustomerPortal.BusinessEvents;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using System.Linq;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEventHandlers
{
    public class NotifyAboutInterviewScheduledEventHandler : BusinessEventHandler<NotifyAboutInterviewScheduledEvent>
    {
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IUserService _userService;

        public NotifyAboutInterviewScheduledEventHandler(
            IUserService userService,
            IMessageTemplateService messageTemplateService,
            IReliableEmailService reliableEmailService,
            ISystemParameterService systemParameterService)
        {
            _userService = userService;
            _messageTemplateService = messageTemplateService;
            _reliableEmailService = reliableEmailService;
            _systemParameterService = systemParameterService;
        }
        public override void Handle(NotifyAboutInterviewScheduledEvent businessEvent)
        {
            var serverHost = _systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);
            var scheduleAppointmentUrlFormat = _systemParameterService.GetParameter<string>(ParameterKeys.CustomerPortalScheduleAppointmentUrlFormat);
            var approveUrl = string.Format(scheduleAppointmentUrlFormat, serverHost, businessEvent.NeededResourceId);

            var scheduleNewInterviewEmailDto = new ScheduleNewInterviewEmailDto
            {
                ResourceName = businessEvent.ResourceName,
                NeededResourceName = businessEvent.NeededResourceName,
                ContactName = businessEvent.ContactName,
                ApproveUrl = approveUrl,
                Comment = businessEvent.Comment,
                Interviews = businessEvent.SuggestedDates.Select(sd => $"{sd.StartsOn} - {sd.EndsOn}").ToArray(),
                FooterHtml = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalNotificationsFooter, null).Content
            };

            var body = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalNewInterviewOptionsBody, scheduleNewInterviewEmailDto);
            var subject = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalNewInterviewOptionsSubject, scheduleNewInterviewEmailDto);

            var operationManager = _userService.GetUserById(businessEvent.OperationManagerId);
            _reliableEmailService.EnqueueMessage(new EmailMessageDto
            {
                Body = body.Content,
                Subject = subject.Content,
                IsHtml = body.IsHtml,
                Recipients = new[] { new EmailRecipientDto(operationManager.Email, operationManager.GetFullName()) }.ToList()
            });
        }
    }
}
