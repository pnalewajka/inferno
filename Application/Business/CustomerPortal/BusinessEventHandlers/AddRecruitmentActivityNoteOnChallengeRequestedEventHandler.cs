﻿using Smt.Atomic.Business.CustomerPortal.BusinessEvents;
using Smt.Atomic.Business.CustomerPortal.Resources;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Data.Crm.Interfaces;
using System;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEventHandlers
{
    public class AddRecruitmentActivityNoteOnChallengeRequestedEventHandler : BusinessEventHandler<ChallengeRequestedEvent>
    {
        private readonly ICrmService _crmService;

        public AddRecruitmentActivityNoteOnChallengeRequestedEventHandler(ICrmService crmService)
        {
            _crmService = crmService;
        }

        public override void Handle(ChallengeRequestedEvent businessEvent)
        {
            _crmService.AddRecruitmentActivityNote(
                businessEvent.ResourceId,
                CustomerPortalResources.ScheduleNextStepCrmTitle,
                string.Format(CustomerPortalResources.ScheduleNextStepCrmBodyFormat, businessEvent.FilesCount, businessEvent.Deadline));
        }
    }
}
