﻿using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.CustomerPortal.BusinessEvents;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using System;
using System.Linq;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEventHandlers
{
    public class NotifyAboutAssignmentWasFinnishedEventHandler : BusinessEventHandler<AssignmentSentBusinessEvent>
    {
        private readonly IClientDataCardIndexDataService _clientDataCardIndexDataService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IUnitOfWorkService<ICustomerPortalDbScope> _unitOfWorkService;
        private readonly IUserService _userService;

        public NotifyAboutAssignmentWasFinnishedEventHandler(
            IUserService userService,
            IMessageTemplateService messageTemplateService,
            IReliableEmailService reliableEmailService,
            ISystemParameterService systemParameterService,
            IClientDataCardIndexDataService clientDataCardIndexDataService,
            IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService)
        {
            _userService = userService;
            _unitOfWorkService = unitOfWorkService;
            _clientDataCardIndexDataService = clientDataCardIndexDataService;
            _messageTemplateService = messageTemplateService;
            _reliableEmailService = reliableEmailService;
            _systemParameterService = systemParameterService;
        }

        public override void Handle(AssignmentSentBusinessEvent businessEvent)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recommendation = unitOfWork.Repositories.Recommendations.GetById(businessEvent.RecommendationId);
                var client = unitOfWork.Repositories.Users.GetById(recommendation.UserId.Value);
                var challengeRequestedEmailDto = new AssignmentFinishedEmailDto
                {
                    ResourceName = recommendation.Name,
                    NeededResourceName = recommendation.NeededResource.Name,
                    Comment = businessEvent.Comment,
                    RecommendationUrl = GetResourceUrl(recommendation.Id, recommendation.NeededResourceId.Value),
                    FooterHtml = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalNotificationsFooter, null).Content
                };

                var body = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalAssignmentFinishedBody, challengeRequestedEmailDto);
                var subject = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalAssignmentFinishedSubject, challengeRequestedEmailDto);

                _reliableEmailService.EnqueueMessage(new EmailMessageDto
                {
                    Body = body.Content,
                    Subject = subject.Content,
                    IsHtml = true,
                    Recipients = new[] { new EmailRecipientDto(client.Email, client.FullName) }.ToList()
                });
            }
        }

        private string GetResourceUrl(long recommendationId, long neededResourceId)
        {
            var serverHost = _systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);
            var scheduleAppointmentUrlFormat = _systemParameterService.GetParameter<string>(ParameterKeys.CustomerPortalCandidateDetailsUrlFormat);
            var recommendationUrl = string.Format(scheduleAppointmentUrlFormat, serverHost, neededResourceId, recommendationId);

            return recommendationUrl;
        }
    }
}
