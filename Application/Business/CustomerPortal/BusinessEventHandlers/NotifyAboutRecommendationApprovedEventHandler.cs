﻿using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.CustomerPortal.BusinessEvents;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Crm.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using System.Linq;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEventHandlers
{
    public class NotifyAboutRecommendationApprovedEventHandler : BusinessEventHandler<RecommendationApprovedEvent>
    {
        private readonly IUnitOfWorkService<ICustomerPortalDbScope> _unitOfWorkService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly ICrmService _crmService;
        private readonly ICrmConnectionStringProvider _crmConnectionStringProvider;

        public NotifyAboutRecommendationApprovedEventHandler(
            ICrmService crmService,
            IMessageTemplateService messageTemplateService,
            IReliableEmailService reliableEmailService,
            ISystemParameterService systemParameterService,
            ICrmConnectionStringProvider crmConnectionStringProvider,
            IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService)
        {
            _crmService = crmService;
            _crmConnectionStringProvider = crmConnectionStringProvider;
            _unitOfWorkService = unitOfWorkService;
            _messageTemplateService = messageTemplateService;
            _reliableEmailService = reliableEmailService;
            _systemParameterService = systemParameterService;
        }
        public override void Handle(RecommendationApprovedEvent businessEvent)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recommendation = unitOfWork.Repositories.Recommendations.GetById(businessEvent.RecommendationId);
                var clientContactDto = _crmService.GetClientContactsByRecruitmentActivityIds(new[] { recommendation.ResourceId }).Single();
                var candidate = _crmService.GetCandidate(recommendation.ResourceId);
                var customerData = unitOfWork.Repositories.ClientDatas.Where(c => c.ClientUserId == businessEvent.ClientUserId).Single();
                var operationManager = unitOfWork.Repositories.Users.GetById(customerData.ManagerUserId);
                
                var neededResourceStatusChanged = new CustomerDecisionEmailDto
                {
                    ResourceName = candidate?.FullName,
                    NeededResourceName = recommendation.NeededResource.Name,
                    CommentLines = new[] { businessEvent.Comment },
                    ContactName = candidate?.ContactName,
                    ContactFirstName = candidate?.ContactFirstName,
                    ContactEmail = candidate?.ContactEmail,
                    SdmEmail = candidate?.SdmEmail,
                    SdmName = candidate?.SdmName,
                    SdmFirstName = candidate?.SdmFirstName,
                    CompanyName = clientContactDto. CompanyName,
                    CrmUrl = _crmService.RecruitmentActivityUrl(recommendation.ResourceId),
                };
                
                var body = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalNotificationsOnRecommendationAcceptedBody, neededResourceStatusChanged);
                var subject = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalNotificationsOnRecommendationAcceptedSubject, neededResourceStatusChanged);

                _reliableEmailService.EnqueueMessage(new EmailMessageDto
                {
                    Body = body.Content,
                    Subject = subject.Content,
                    IsHtml = body.IsHtml,
                    Recipients = new[] {
                        new EmailRecipientDto(operationManager.Email, operationManager.FullName),
                    }.ToList()
                });
            }
        }
    }
}
