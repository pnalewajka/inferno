﻿using System.Linq;
using Smt.Atomic.Business.CustomerPortal.BusinessEvents;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Data.SharePoint.Interfaces;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEventHandlers
{
    public class DownloadSharepointDocumentEventHandler : BusinessEventHandler<DownloadSharepointDocumentEvent>
    {
        private readonly ISharePointService _sharePointService;
        private readonly IUnitOfWorkService<ICustomerPortalDbScope> _unitOfWorkService;

        public DownloadSharepointDocumentEventHandler(ISharePointService sharePointService,
            IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService)
        {
            _sharePointService = sharePointService;
            _unitOfWorkService = unitOfWorkService;
        }

        public override void Handle(DownloadSharepointDocumentEvent businessEvent)
        {
            var file = _sharePointService.GetCandidateDocuments(new[] {businessEvent.AbsoluteUrlPath}).FirstOrDefault();

            if (file?.Content == null)
            {
                throw new BusinessException($"Can't download document for url '{businessEvent.AbsoluteUrlPath}'");
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var document =
                    unitOfWork.Repositories.Documents.SingleOrDefault(
                        d => d.Url == businessEvent.AbsoluteUrlPath &&
                             d.RecommendationId == businessEvent.RecommendationId);

                if (document != null)
                {
                    unitOfWork.Repositories.Documents.Delete(document);
                }

                unitOfWork.Repositories.Documents.Add(new Document
                {
                    ContentLength = file.Content.Length,
                    ContentType = "pdf/application",
                    RecommendationId = businessEvent.RecommendationId,
                    Name = file.Name,
                    Url = businessEvent.AbsoluteUrlPath,
                    DocumentContent = new DocumentContent
                    {
                        Content = file.Content
                    }
                });

                unitOfWork.Commit();
            }
        }
    }
}