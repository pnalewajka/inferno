﻿using System.Linq;
using Smt.Atomic.Business.CustomerPortal.BusinessEvents;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEventHandlers
{
    public class AskForResourceFormDetailsEventHandler : BusinessEventHandler<AskForResourceFormDetailsEvent>
    {
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IUnitOfWorkService<ICustomerPortalDbScope> _unitOfWorkService;

        public AskForResourceFormDetailsEventHandler(
            IMessageTemplateService messageTemplateService,
            IReliableEmailService reliableEmailService,
            ISystemParameterService systemParameterService,
            IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService)
        {
            _messageTemplateService = messageTemplateService;
            _reliableEmailService = reliableEmailService;
            _systemParameterService = systemParameterService;
            _unitOfWorkService = unitOfWorkService;
        }

        public override void Handle(AskForResourceFormDetailsEvent businessEvent)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var resourceForm = unitOfWork.Repositories.ResourceForms.GetById(businessEvent.ResourceFormId);

                var frontEndUrl = _systemParameterService.GetParameter<string>(ParameterKeys.CustomerPortalFrontEndUrl);

                var askForDetailsEmailDto = new AskForResourceFormDetailsEmailDto
                {
                    ResourceFormName = resourceForm.ResourceName,
                    Question = businessEvent.Question,
                    ResourceFormUrl = $"{frontEndUrl}/vacancies/request/{businessEvent.ResourceFormId}",
                    FooterHtml = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalNotificationsFooter, null).Content
                };

                var body = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalAskForResourceFormDetailsBody, askForDetailsEmailDto);
                var subject = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalAskForResourceFormDetailsSubject, askForDetailsEmailDto);

                _reliableEmailService.EnqueueMessage(new EmailMessageDto
                {
                    Body = body.Content,
                    Subject = subject.Content,
                    IsHtml = body.IsHtml,
                    Recipients = new[] { new EmailRecipientDto(resourceForm.CreatedBy.Email, resourceForm.CreatedBy.FullName) }.ToList()
                });
            }
        }
    }
}
