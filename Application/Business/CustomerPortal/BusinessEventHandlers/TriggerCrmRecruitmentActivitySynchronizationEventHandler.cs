﻿using System.Linq;
using Smt.Atomic.Business.CustomerPortal.BusinessEvents;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Crm.Interfaces;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.BusinessEventHandlers
{
    public class TriggerCrmRecruitmentActivitySynchronizationEventHandler : BusinessEventHandler<
        TriggerCrmRecruitmentActivitySynchronizationEvent>
    {

        private readonly IBusinessEventPublisher _businessEventPublisher;
        private readonly ICrmNeededResourcesService _crmNeededResourcesService;
        private readonly IUnitOfWorkService<ICustomerPortalDbScope> _unitOfWorkService;
        private readonly ITimeService _timeService;

        public TriggerCrmRecruitmentActivitySynchronizationEventHandler(
            ICrmNeededResourcesService crmNeededResourcesService,
            IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService,
            ITimeService timeService,
            IBusinessEventPublisher businessEventPublisher)
        {
            _crmNeededResourcesService = crmNeededResourcesService;
            _unitOfWorkService = unitOfWorkService;
            _timeService = timeService;
            _businessEventPublisher = businessEventPublisher;
        }

        public override void Handle(TriggerCrmRecruitmentActivitySynchronizationEvent businessEvent)
        {
            var activity = _crmNeededResourcesService.LoadRecruitmentActivitie(businessEvent.RecruitmentActivityId);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recommendation = unitOfWork.Repositories.Recommendations.SingleOrDefault(
                    r => r.ResourceId == businessEvent.RecruitmentActivityId);

                if (recommendation == null)
                {
                    var clientData =
                        unitOfWork.Repositories.ClientDatas.GetById(businessEvent.ClientDataId);

                    var neededResource =
                        unitOfWork.Repositories.NeededResources.SingleOrDefault(
                            nr => nr.CrmId == businessEvent.NeededResourceId);

                    recommendation = new Recommendation
                    {
                        NeededResource = neededResource,
                        ResourceId = activity.CrmId,
                        Name = activity.Name,
                        UserId = clientData.ClientUserId,
                        AddedOn = _timeService.GetCurrentTime(),
                    };

                    unitOfWork.Repositories.Recommendations.Add(recommendation);
                }
                else
                {
                    recommendation.Name = activity.Name;
                }

                unitOfWork.Commit();

                //var documents =
                //    _crmNeededResourcesService.ResolveRecruitmentActivitieDocumentUrls(businessEvent.RecruitmentActivityId).ToList();

                //foreach (var document in documents)
                //{
                //    _businessEventPublisher.PublishBusinessEvent(new DownloadSharepointDocumentEvent
                //    {
                //        RecommendationId = recommendation.Id,
                //        AbsoluteUrlPath = document
                //    });
                //}
            }
        }
    }
}
