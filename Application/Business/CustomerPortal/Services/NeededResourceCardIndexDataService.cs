﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Transactions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.CustomerPortal.BusinessEvents;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.Services
{
    public class NeededResourceCardIndexDataService : CardIndexDataService<NeededResourceDto, NeededResource, ICustomerPortalDbScope>, INeededResourceCardIndexDataService
    {
        private readonly IClassMapping<NeededResource, NeededResourceDto> _entityToDtoMapping;
        private readonly ICardIndexServiceDependencies<ICustomerPortalDbScope> _cardIndexServiceDependencies;
        private readonly IBusinessEventPublisher _buinBusinessEventPublisher;
        private readonly IClassMappingFactory _classMappingFactory;
        private readonly IPrincipalProvider _principalProvider;
        private readonly ITimeService _timeService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public NeededResourceCardIndexDataService(
            ICardIndexServiceDependencies<ICustomerPortalDbScope> dependencies,
            IBusinessEventPublisher buinBusinessEventPublisher,
            IClassMappingFactory classMappingFactory,
            IPrincipalProvider principalProvider,
            ITimeService timeService,
            IClassMapping<NeededResource, NeededResourceDto> entityToDtoMapping)
            : base(dependencies)
        {
            _cardIndexServiceDependencies = dependencies;
            _buinBusinessEventPublisher = buinBusinessEventPublisher;
            _classMappingFactory = classMappingFactory;
            _principalProvider = principalProvider;
            _timeService = timeService;
            _entityToDtoMapping = entityToDtoMapping;
        }

        protected override IEnumerable<Expression<Func<NeededResource, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return e => e.Name;
        }

        public NeededResourceDto GetRecordByCrmId(Guid crmId)
        {
            if (crmId == null)
            {
                throw new ArgumentNullException(nameof(crmId));
            }

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var neededResource = unitOfWork.Repositories.NeededResources.SingleOrDefault(n => n.CrmId == crmId);

                return _entityToDtoMapping.CreateFromSource(neededResource);
            }
        }

        public NeededResourceDto[] GetRecordsByCrmIds(Guid[] crmIds)
        {
            if (crmIds == null)
            {
                throw new ArgumentNullException(nameof(crmIds));
            }

            if (crmIds.Length == 0)
            {
                return Array.Empty<NeededResourceDto>();
            }

            NeededResourceDto[] result;

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var query = from neededResource in unitOfWork.Repositories.NeededResources
                            where crmIds.Contains(neededResource.CrmId)
                            select neededResource;

                var entities = query.ToArray();
                result = entities.Select(e => _entityToDtoMapping.CreateFromSource(e)).ToArray();
            }

            return result;
        }

        public IReadOnlyCollection<ActivityLogDto> GetActivityLogs()
        {
            var mapping = _classMappingFactory.CreateMapping<ActivityLog, ActivityLogDto>();

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var currentUser = _principalProvider.Current.Id;

                if (!currentUser.HasValue)
                {
                    throw new BusinessException("Only loged in users can get activity logs");
                }

                return GetNewRecommendationActivityLogs(unitOfWork, currentUser.Value)
                    .Union(GetApprovedFormsActivityLogs(unitOfWork, currentUser.Value))
                    .Union(GetFormsQuestionAcitvityLogs(unitOfWork, currentUser.Value))
                    .Union(GetScheduledAppointmentsAcitvityLogs(unitOfWork, currentUser.Value))
                    .Where(al => !al.SeenByUserOn.HasValue)
                    .ToList()
                    .Select(mapping.CreateFromSource)
                    .ToList();
            }
        }

        public void DiscardActivity(long activityId)
        {
            var userId = _principalProvider.Current.Id;

            if (!userId.HasValue)
            {
                throw new BusinessException("Only authorized users can discard activities");
            }

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var activity = unitOfWork.Repositories.ActivityLogs.SingleOrDefault(al => al.Id == activityId && (
                    (al.Recommendation != null && al.Recommendation.UserId == userId.Value) ||
                    (al.ResourceForm != null && al.ResourceForm.CreatedById == userId.Value)
                    ));

                if (activity != null)
                {
                    activity.SeenByUserOn = _timeService.GetCurrentTime();
                    unitOfWork.Commit();
                }
            }
        }

        public bool PutOnHold(long neededResourceId, string reason)
        {
            using (var transaction = new TransactionScope())
            {
                using (var unitOfWork = UnitOfWorkService.Create())
                {
                    var neededResource = unitOfWork.Repositories.NeededResources.GetByIdOrDefault(neededResourceId);
                    var clientUserId = _cardIndexServiceDependencies.PrincipalProvider.Current.Id.Value;

                    if (neededResource == null)
                    {
                        return false;
                    }

                    if (neededResource.IsOnHold)
                    {
                        return false;
                    }

                    neededResource.IsOnHold = true;
                    neededResource.HoldReason = reason;
                    unitOfWork.Commit();

                    PublishBusinessEvents(new[]{ new NeededResourcePutOnHoldEvent
                    {
                        NeededResourceName = neededResource.Name,
                        Reason = reason,
                        ClientUserId = clientUserId
                    }});
                    transaction.Complete();

                    return true;
                }
            }
        }

        public void Resume(long neededResourceId, string reason)
        {
            using (var transaction = new TransactionScope())
            {
                using (var unitOfWork = UnitOfWorkService.Create())
                {
                    var neededResource = unitOfWork.Repositories.NeededResources.GetById(neededResourceId);
                    var clientUserId = _cardIndexServiceDependencies.PrincipalProvider.Current.Id.Value;

                    if (!neededResource.IsOnHold)
                    {
                        return;
                    }
                    neededResource.IsOnHold = false;
                    neededResource.HoldReason = null;
                    unitOfWork.Commit();

                    PublishBusinessEvents(new[] { new NeededResourceResumedEvent
                    {
                        NeededResourceName = neededResource.Name,
                        Reason = reason,
                        ClientUserId = clientUserId
                    }});
                    transaction.Complete();
                }
            }
        }

        public void TriggerNeededResourcesRefresh(IEnumerable<long> neededResourceIds)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var neededResources = unitOfWork.Repositories.NeededResources.GetByIds(neededResourceIds);
                foreach (var neededResource in neededResources)
                {
                    _buinBusinessEventPublisher.PublishBusinessEvent(new TriggerCrmNeededResourceSynchronizationEvent { NeededResourceId = neededResource.CrmId });
                }
            }
        }

        public NeededResourceDto[] GetVacanciesByUserId(long userId)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var neededResources = unitOfWork.Repositories.NeededResources
                    .Where(nr =>
                        nr.Recommendations
                        .Any(r =>
                            r.UserId == userId &&
                            r.Status != RecommendationStatus.Withdrawn &&
                            r.Status != RecommendationStatus.None &&
                            r.Status != RecommendationStatus.Archived) ||
                        (nr.ResourceForm != null && nr.ResourceForm.CreatedById == userId)
                    )
                    .Select(MapEntityToDto)
                    .ToArray();

                return neededResources;
            }
        }

        private NeededResourceDto MapEntityToDto(NeededResource nr)
        {
            return new NeededResourceDto
            {
                Name = nr.Name,
                Id = nr.Id,
                RecommendationsCount =
                    nr.Recommendations.Count(
                        r =>
                            r.Status != RecommendationStatus.None && r.Status != RecommendationStatus.Withdrawn &&
                            r.Status != RecommendationStatus.Archived),
                RejectedRecommendationsCount = nr.Recommendations.Count(r => r.Status == RecommendationStatus.Rejected),
                InvitedRecommendationsCount = nr.Recommendations.Count(r => r.Status == RecommendationStatus.Invited),
                ApprovedRecommendationsCount = nr.Recommendations.Count(r => r.Status == RecommendationStatus.Approved),
                IsOnHold = nr.IsOnHold,
                ResourceFormId = nr.ResourceFormId
            };
        }

        private static IQueryable<ActivityLog> GetNewRecommendationActivityLogs(IUnitOfWork<ICustomerPortalDbScope> unitOfWork, long userId)
        {
            return unitOfWork
                .Repositories
                .ActivityLogs
                .Include(a => a.Recommendation)
                .Include(a => a.Recommendation.NeededResource)
                .Where(al =>
                    al.Recommendation.UserId == userId &&
                    al.ActivityType == ActivityLogType.NewRecommendation
                    );
        }

        private static IQueryable<ActivityLog> GetApprovedFormsActivityLogs(
            IUnitOfWork<ICustomerPortalDbScope> unitOfWork, long userId)
        {
            return unitOfWork
                .Repositories
                .ActivityLogs
                .Where(al =>
                    al.ResourceForm.CreatedById == userId &&
                    al.ActivityType == ActivityLogType.RequestApproved
                );
        }

        private static IQueryable<ActivityLog> GetFormsQuestionAcitvityLogs(
            IUnitOfWork<ICustomerPortalDbScope> unitOfWork, long userId)
        {
            return unitOfWork
                .Repositories
                .ActivityLogs
                .Where(al =>
                    al.ResourceForm.CreatedById == userId &&
                    al.ActivityType == ActivityLogType.RequestQuestion
                );
        }

        private static IQueryable<ActivityLog> GetScheduledAppointmentsAcitvityLogs(
    IUnitOfWork<ICustomerPortalDbScope> unitOfWork, long userId)
        {
            return unitOfWork
                .Repositories
                .ActivityLogs
                .Include(a => a.Recommendation)
                .Include(a => a.Recommendation.NeededResource)
                .Where(al =>
                    al.Recommendation.UserId == userId &&
                    al.ActivityType == ActivityLogType.AppointmentScheduled
                    );
        }

        private static IQueryable<ActivityLog> GetChallengeAnsweredAcitvityLogs(
IUnitOfWork<ICustomerPortalDbScope> unitOfWork, long userId)
        {
            return unitOfWork
                .Repositories
                .ActivityLogs
                .Include(a => a.Recommendation)
                .Include(a => a.Recommendation.NeededResource)
                .Where(al =>
                    al.Recommendation.UserId == userId &&
                    al.ActivityType == ActivityLogType.ChallengeAnswered
                    );
        }

        protected override NamedFilters<NeededResource> NamedFilters
        {
            get
            {
                return new NamedFilters<NeededResource>(new[]
                {
                    new NamedFilter<NeededResource, long>(
                        FilterCodes.ClientId,
                        (neededResource, userId) => neededResource.Recommendations.Any(r => r.UserId == userId)),
                    new NamedFilter<NeededResource>(
                        FilterCodes.OnHoldRequestsFilter,
                        neededResource => neededResource.IsOnHold),
                    new NamedFilter<NeededResource>(
                        FilterCodes.ActiveRequestsFilter,
                        neededResource => !neededResource.IsOnHold),
                    new NamedFilter<NeededResource>(
                        FilterCodes.HasPendingAppointments,
                        neededResource=>neededResource.Recommendations.Any(
                            r=>r.ScheduleAppointments.Any(
                                a=>a.AppointmentStatus==AppointmentStatusType.Pending))),
                    new NamedFilter<NeededResource>(
                        FilterCodes.HasChallenges,
                        neededResource=>neededResource.Recommendations.Any(
                            r=>r.Challenges.Any(
                                c=>!c.AnswerDocuments.Any())))
                });
            }
        }
    }
}
