﻿using System.Linq;
using Castle.Core.Logging;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.Services
{
    public class NoteCardIndexDataService : CardIndexDataService<NoteDto, Note, ICustomerPortalDbScope>, INoteCardIndexDataService
    {
        private readonly IClassMapping<Note, NoteDto> _entityToDtoMapping;
        private readonly ILogger _logger;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public NoteCardIndexDataService(
            ICardIndexServiceDependencies<ICustomerPortalDbScope> dependencies,
            ILogger logger)
            : base(dependencies)
        {
            _entityToDtoMapping = dependencies.ClassMappingFactory.CreateMapping<Note, NoteDto>();
            _logger = logger;
        }

        public NoteDto[] GetNotesByRecommendationId(long recommendationId)
        {
            NoteDto[] result;

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var entities = unitOfWork.Repositories.Notes.Where(n => n.RecommendationId == recommendationId).ToArray();
                result = entities.Select(e => _entityToDtoMapping.CreateFromSource(e)).ToArray();
            }

            return result;
        }

        public NoteDto[] GetNotesToSaveInCRM()
        {
            NoteDto[] result;

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var entities = unitOfWork.Repositories.Notes.Where(n => !n.SavedInCRM).ToArray();
                result = entities.Select(e => _entityToDtoMapping.CreateFromSource(e)).ToArray();
            }

            return result;
        }
    }
}
