﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.Services
{
    public class ClientUserCardIndexDataService : CardIndexDataService<UserDto, User, ICustomerPortalDbScope>, IClientUserCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public ClientUserCardIndexDataService(ICardIndexServiceDependencies<ICustomerPortalDbScope> dependencies)
            : base(dependencies)
        {
        }

        public override IQueryable<User> ApplyContextFiltering(IQueryable<User> records, object context)
        {
            IEnumerable<long> managerIds = null;

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                managerIds = unitOfWork.Repositories.ClientDatas
                    .Select(r => r.ClientUserId)
                    .Distinct()
                    .ToList();
            }

            return records.Where(r => managerIds.Any(id => id == r.Id));
        }

        protected override NamedFilters<User> NamedFilters
        {
            get
            {
                return new NamedFilters<User>(new[] {
                    new NamedFilter<User>("Existing", user => user != null)
                });
            }
        }
    }
}
