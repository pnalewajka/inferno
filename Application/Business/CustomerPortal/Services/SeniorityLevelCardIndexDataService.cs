﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Business.CustomerPortal.Services
{
    internal class SeniorityLevelCardIndexDataService :
        EnumBasedCardIndexDataService<SeniorityLevelDto, SeniorityLevelEnum>, ISeniorityLevelCardIndexDataService
    {
        protected override IEnumerable<Expression<Func<SeniorityLevelDto, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield break;
        }

        protected override SeniorityLevelDto ConvertToDto(SeniorityLevelEnum enumValue)
        {
            return new SeniorityLevelDto
            {
                Id = (int) enumValue,
                Name = enumValue.ToString(),
                Description = enumValue.GetDescription()
            };
        }
    }
}