using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.Services
{
    public class QuestionCardIndexDataService : CardIndexDataService<QuestionDto, Question, ICustomerPortalDbScope>, IQuestionCardIndexDataService
    {
        private readonly IClassMapping<Question, QuestionDto> _entityToDtoMapping;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public QuestionCardIndexDataService(ICardIndexServiceDependencies<ICustomerPortalDbScope> dependencies)
            : base(dependencies)
        {
            var classMappingFactory = dependencies.ClassMappingFactory;
            _entityToDtoMapping = classMappingFactory.CreateMapping<Question, QuestionDto>();
        }

        /// <summary>
        /// Gets question records which are ready to sent to the SDM
        /// </summary>
        /// <returns>An array of Recommendation records</returns>
        public QuestionDto[] GetReadyToSendRecords()
        {
            QuestionDto[] result;

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var query = from question in unitOfWork.Repositories.Questions
                            where question.WasSent == false
                            select question;

                var questions = query.ToArray();
                result = questions.Select(e => _entityToDtoMapping.CreateFromSource(e)).ToArray();
            }

            return result;
        }

        public BoolResult MarkAsSendRecords(IReadOnlyCollection<long> questionIds)
        {
            if (questionIds == null)
            {
                throw new ArgumentNullException(nameof(questionIds));
            }

            if (!questionIds.Any())
            {
                return new BoolResult(true);
            }

            using (var transactionScope = TransactionHelper.CreateDefaultTransactionScope())
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var questions = unitOfWork.Repositories.Questions.Where(e => questionIds.Contains(e.Id)).ToArray();

                foreach (var question in questions)
                {
                    question.WasSent = true;
                }

                unitOfWork.Commit();
                transactionScope.Complete();
            }

            return new BoolResult(true);
        }
    }
}
