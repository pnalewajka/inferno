﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.Services
{
    public class BusinesUnitOptionsCardIndexDataService : ReadOnlyCardIndexDataService<BusinesUnitOptionsDto, BusinesUnitOptions, ICustomerPortalDbScope>, IBusinesUnitOptionsCardIndexDataService
    {
        public BusinesUnitOptionsCardIndexDataService(ICardIndexServiceDependencies<ICustomerPortalDbScope> dependencies)
            : base(dependencies)
        {
        }
    }
}
