﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.CustomerPortal.BusinessEvents;
using Smt.Atomic.Business.CustomerPortal.DocumentMappings;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Crm.BusinessEvents;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.Services
{
    internal class ResourceFormDataService : CardIndexDataService<ResourceFormDto, ResourceForm, ICustomerPortalDbScope>, IResourceFormDataService
    {
        private readonly IClassMappingFactory _classMappingFactory;
        private readonly IPrincipalProvider _principalProvider;
        private readonly ITimeService _timeSerivce;
        private readonly IUnitOfWorkService<ICustomerPortalDbScope> _unitOfWorkService;
        private readonly IActivityLogService _activityLogService;
        private readonly IBusinessEventPublisher _businessEventPublisher;
        private readonly IUploadedDocumentHandlingService _uploadedDocumentHandlingService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public ResourceFormDataService(
            IActivityLogService activityLogService,
            ICardIndexServiceDependencies<ICustomerPortalDbScope> dependencies,
            IUploadedDocumentHandlingService uploadedDocumentHandlingService,
            IBusinessEventPublisher businessEventPublisher) : base(dependencies)
        {
            _activityLogService = activityLogService;
            _businessEventPublisher = businessEventPublisher;
            _classMappingFactory = dependencies.ClassMappingFactory;
            _principalProvider = dependencies.PrincipalProvider;
            _unitOfWorkService = dependencies.UnitOfWorkService;
            _timeSerivce = dependencies.TimeService;
            _uploadedDocumentHandlingService = uploadedDocumentHandlingService;
        }

        protected override IEnumerable<Expression<Func<ResourceForm, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return r => r.ResourceName;
            yield return r => r.Location;
        }

        protected override NamedFilters<ResourceForm> NamedFilters
        {
            get
            {
                return new NamedFilters<ResourceForm>(new[]
                {
                    new NamedFilter<ResourceForm>(
                        FilterCodes.ResourceFormState,
                        (resource) => resource.State == ResourceFormState.Submitted),
                    new NamedFilter<ResourceForm>(
                        FilterCodes.ResourceFormInCRM,
                        resource => resource.NeededResourceCrmCode.HasValue)
                });
            }
        }

        public IReadOnlyCollection<ResourceFormDto> GetCurrentUserResources()
        {
            var mapping = _classMappingFactory.CreateMapping<ResourceForm, ResourceFormDto>();

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories
                    .ResourceForms
                    .Filtered()
                    .Where(rf => rf.CreatedById == _principalProvider.Current.Id)
                    .Select(mapping.CreateFromSource)
                    .ToList();
            }
        }

        public IReadOnlyCollection<LanguageOptionDto> GetLanguageOptions()
        {
            var mapping = _classMappingFactory.CreateMapping<LanguageOption, LanguageOptionDto>();
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.LanguageOptions
                    .Select(mapping.CreateFromSource)
                    .ToList();
            }
        }

        public ResourceFormDto ApproveResourceForm(long id, IResourceFormApproveData formData)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var mapping = _classMappingFactory.CreateMapping<ResourceForm, ResourceFormDto>();
                var resourceForm = unitOfWork.Repositories.ResourceForms.GetById(id);

                resourceForm.ApprovedById = _principalProvider.Current.Id;
                resourceForm.ApprovedOn = _timeSerivce.GetCurrentTime();
                resourceForm.State = ResourceFormState.Approved;

                _activityLogService.GetResourceFormApprovedActivityService(unitOfWork).RegisterActivity(id);

                var formClientData =
                    unitOfWork.Repositories.ClientDatas.SingleOrDefault(cd => cd.ClientUserId == resourceForm.CreatedById);

                if (formClientData == null)
                {
                    throw new BusinessException("Customer has no Client data entity");
                }

                var createNeededResourceEvent = new CreateNeededResourceEvent
                {
                    OperationManagerId = formClientData.ManagerUserId,
                    ResourceName = $"{formClientData.ClientContact.CompanyName}/{resourceForm.ResourceName}/{resourceForm.Location}",
                    ResourceFormId = resourceForm.Id,
                    NumberOfPeople = resourceForm.ResourceCount,
                    TechnologyDepartmentId = formData.MainTechnology,
                    RegionId = formData.Region,
                    AccountGuid = formClientData.ClientContact.CompanyGuidId,
                    ContactClientGuid = formClientData.ClientContact.CrmId,
                    Rate = formData.Rate,
                    Seniority1 = formData.SeniorityLevels.Contains((int)SeniorityLevelEnum.Seniority1),
                    Seniority2 = formData.SeniorityLevels.Contains((int)SeniorityLevelEnum.Seniority2),
                    Seniority3 = formData.SeniorityLevels.Contains((int)SeniorityLevelEnum.Seniority3),
                    Seniority4 = formData.SeniorityLevels.Contains((int)SeniorityLevelEnum.Seniority4),
                    Seniority5 = formData.SeniorityLevels.Contains((int)SeniorityLevelEnum.Seniority5)
                };

                PublishBusinessEvents(new[] { createNeededResourceEvent });
                unitOfWork.Commit();

                return mapping.CreateFromSource(resourceForm);
            }
        }

        public void AskForDetails(long resourceFormId, string question)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var resourceForm = unitOfWork.Repositories.ResourceForms.GetById(resourceFormId);

                resourceForm.Question = question;
                resourceForm.State = ResourceFormState.Draft;

                _activityLogService
                    .GetResourceFormQuestionActivityService(unitOfWork)
                    .RegisterActivity(resourceFormId);

                unitOfWork.Commit();

                BusinessEventPublisher.PublishBusinessEvent(new AskForResourceFormDetailsEvent
                {
                    CreatedOn = _timeSerivce.GetCurrentTime(),
                    ResourceFormId = resourceFormId,
                    Question = question
                });
            }
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<ResourceForm, ResourceFormDto> recordAddedEventArgs)
        {
            if (recordAddedEventArgs.InputDto.Files != null)
            {
                _uploadedDocumentHandlingService.PromoteTemporaryDocuments<ResourceFormFile, ResourceFormFileContent, ResourceFormFileMapping, ICustomerPortalDbScope>(recordAddedEventArgs.InputDto.Files);
            }

            if (recordAddedEventArgs.InputEntity.State == ResourceFormState.Submitted)
            {
                var classMapping = _classMappingFactory.CreateMapping<ResourceForm, ResourceFormDto>();
                SendEventWhenResourceRequestChanged(classMapping.CreateFromSource(recordAddedEventArgs.InputEntity));
            }
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<ResourceForm, ResourceFormDto> recordEditedEventArgs)
        {
            if (recordEditedEventArgs.InputDto.Files != null)
            {
                _uploadedDocumentHandlingService.PromoteTemporaryDocuments<ResourceFormFile, ResourceFormFileContent, ResourceFormFileMapping, ICustomerPortalDbScope>(recordEditedEventArgs.InputDto.Files);
            }

            if (recordEditedEventArgs.InputEntity.State == ResourceFormState.Submitted)
            {
                var classMapping = _classMappingFactory.CreateMapping<ResourceForm, ResourceFormDto>();
                SendEventWhenResourceRequestChanged(classMapping.CreateFromSource(recordEditedEventArgs.InputEntity));
            }
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<ResourceForm, ResourceFormDto, ICustomerPortalDbScope> eventArgs)
        {
            if (!_principalProvider.Current.Id.HasValue)
            {
                throw new UnauthorizedException("Unauthorized access");
            }

            base.OnRecordAdding(eventArgs);

            eventArgs.InputEntity.CreatedById = _principalProvider.Current.Id.Value;
            eventArgs.InputEntity.CreatedOn = _timeSerivce.GetCurrentTime();

            if (eventArgs.InputDto.State == ResourceFormState.Submitted)
            {
                eventArgs.InputEntity.SubmittedById = _principalProvider.Current.Id.Value;
                eventArgs.InputEntity.SubmittedOn = _timeSerivce.GetCurrentTime();
                eventArgs.InputEntity.Question = null;
                _activityLogService.GetResourceFormQuestionActivityService(eventArgs.UnitOfWork).ClearActivity(eventArgs.InputEntity.Id);
            }

            if (eventArgs.InputDto.LanguageOptions != null)
            {
                if (ContainsUnknownLanguageId(eventArgs.UnitOfWork, eventArgs.InputDto.LanguageOptions.Select(l => l.Id)))
                {
                    throw new BusinessException("Languages contains unkown values");
                }

                eventArgs.InputEntity.LanguageOption = eventArgs.UnitOfWork.Repositories.LanguageOptions.GetByIds(
                    eventArgs.InputDto.LanguageOptions.Select(l => l.Id)).ToList();
            }
            if (eventArgs.InputDto.Files != null)
            {
                eventArgs.InputEntity.Files = new Collection<ResourceFormFile>();

                _uploadedDocumentHandlingService.MergeDocumentCollections(eventArgs.UnitOfWork, eventArgs.InputEntity.Files, eventArgs.InputDto.Files,
                   p => new ResourceFormFile
                   {
                       Name = p.DocumentName,
                       ContentType = p.ContentType
                   });
            }
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<ResourceForm, ICustomerPortalDbScope> eventArgs)
        {
            if (eventArgs.DeletingRecord.State == ResourceFormState.Approved)
            {
                throw new BusinessException($"Can't remove {nameof(ResourceFormState.Approved)} resource request");
            }

            if (eventArgs.DeletingRecord.CreatedById != _principalProvider.Current.Id)
            {
                throw new UnauthorizedAccessException();
            }

            base.OnRecordDeleting(eventArgs);
        }

        protected override IQueryable<ResourceForm> ConfigureIncludes(IQueryable<ResourceForm> sourceQueryable)
        {
            return sourceQueryable.Include(a => a.Files);
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<ResourceForm, ResourceFormDto, ICustomerPortalDbScope> eventArgs)
        {
            base.OnRecordEditing(eventArgs);

            if (eventArgs.DbEntity.CreatedById != _principalProvider.Current.Id)
            {
                throw new UnauthorizedAccessException();
            }

            eventArgs.InputEntity.CreatedOn = eventArgs.DbEntity.CreatedOn;
            eventArgs.InputEntity.CreatedById = eventArgs.DbEntity.CreatedById;

            if (eventArgs.InputDto.State != eventArgs.DbEntity.State && eventArgs.InputDto.State == ResourceFormState.Submitted)
            {
                eventArgs.DbEntity.SubmittedById = _principalProvider.Current.Id.Value;
                eventArgs.DbEntity.SubmittedOn = _timeSerivce.GetCurrentTime();
                _activityLogService.GetResourceFormQuestionActivityService(eventArgs.UnitOfWork).ClearActivity(eventArgs.InputEntity.Id);
            }

            if (eventArgs.InputDto.LanguageOptions != null)
            {
                if (ContainsUnknownLanguageId(eventArgs.UnitOfWork, eventArgs.InputDto.LanguageOptions.Select(l => l.Id)))
                {
                    throw new BusinessException("Languages contains unkown values");
                }

                eventArgs.InputEntity.LanguageOption = eventArgs.UnitOfWork.Repositories.LanguageOptions.GetByIds(
                    eventArgs.InputDto.LanguageOptions.Select(l => l.Id)).ToList();
            }
            else
            {
                eventArgs.InputEntity.LanguageOption = new List<LanguageOption>();
            }

            if (eventArgs.InputDto.Files != null)
            {
                _uploadedDocumentHandlingService.MergeDocumentCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.Files, eventArgs.InputDto.Files,
                    p => new ResourceFormFile
                    {
                        Name = p.DocumentName,
                        ContentType = p.ContentType
                    });
            }

            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.LanguageOption, eventArgs.InputEntity.LanguageOption);
        }

        private void SendEventWhenResourceRequestChanged(ResourceFormDto dto)
        {
            var eventMapper = _classMappingFactory.CreateMapping<ResourceFormDto, ResourceRequestSubmittedEvent>();
            var resourceRequestSubmittedEvent = eventMapper.CreateFromSource(dto);
            resourceRequestSubmittedEvent.CustomerUserId = _principalProvider.Current.Id;
            _businessEventPublisher.PublishBusinessEvent(resourceRequestSubmittedEvent);
        }

        private static bool ContainsUnknownLanguageId(IUnitOfWork<ICustomerPortalDbScope> unitOfWork, IEnumerable<long> requiredLanguages)
        {
            var existingLanguages = unitOfWork.Repositories.LanguageOptions.Select(l => l.Id);

            return requiredLanguages.Except(existingLanguages).Any();
        }

        public ResourceFormDto GetResourceFormById(long id)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var entity = unitOfWork
                    .Repositories
                    .ResourceForms
                    .Filtered()
                    .GetById(id);

                var classMapping = _classMappingFactory.CreateMapping<ResourceForm, ResourceFormDto>();

                _activityLogService.GetResourceFormApprovedActivityService(unitOfWork).ClearActivity(id);
                unitOfWork.Commit();

                return classMapping.CreateFromSource(entity);
            }
        }
    }
}
