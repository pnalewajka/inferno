using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.CustomerPortal.BusinessEvents;
using Smt.Atomic.Business.CustomerPortal.DocumentMappings;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.Business.CustomerPortal.Resources;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.Services
{
    public class RecommendationCardIndexDataService :
        CardIndexDataService<RecommendationDto, Recommendation, ICustomerPortalDbScope>,
        IRecommendationCardIndexDataService
    {
        private readonly IClassMapping<Recommendation, RecommendationDto> _entityToDtoMapping;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IBusinessEventPublisher _businessEventPublisher;
        private readonly IActivityLogService _activityLogService;
        private readonly ITimeService _timeService;
        private readonly IUploadedDocumentHandlingService _uploadedDocumentHandlingService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public RecommendationCardIndexDataService(
            ITimeService timeService,
            IPrincipalProvider principalProvider,
            IBusinessEventPublisher businessEventPublisher,
            IActivityLogService activityLogService,
            ICardIndexServiceDependencies<ICustomerPortalDbScope> dependencies,
            IUploadedDocumentHandlingService uploadedDocumentHandlingService)
            : base(dependencies)
        {
            _timeService = timeService;
            _principalProvider = principalProvider;
            _businessEventPublisher = businessEventPublisher;
            _activityLogService = activityLogService;
            _uploadedDocumentHandlingService = uploadedDocumentHandlingService;

            var classMappingFactory = dependencies.ClassMappingFactory;
            _entityToDtoMapping = classMappingFactory.CreateMapping<Recommendation, RecommendationDto>();
        }

        /// <summary>
        ///     Get an array of records, that are not archived and withdrawn for given <paramref name="userId" />
        /// </summary>
        /// <returns>An array of Recommendation records</returns>
        public RecommendationDto[] GetRecords(long userId, long? neededResourceId)
        {
            if (neededResourceId == null)
                return Array.Empty<RecommendationDto>();

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Recommendations
                    .Where(r => r.UserId == userId &&
                                r.NeededResourceId == neededResourceId &&
                                r.Status != RecommendationStatus.None &&
                                r.Status != RecommendationStatus.Withdrawn &&
                                r.Status != RecommendationStatus.Archived)
                    .ToList()
                    .Select(e => _entityToDtoMapping.CreateFromSource(e))
                    .ToArray();
            }
        }

        public BusinessResult RejectRecommendation(long recommendationId, string comment)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var recommendation = unitOfWork.Repositories.Recommendations.GetById(recommendationId);

                if (recommendation.UserId != _principalProvider.Current.Id)
                    throw new BusinessException($"You don't have access to recommendation {recommendationId}");

                recommendation.Status = RecommendationStatus.Rejected;
                recommendation.RejectedOn = _timeService.GetCurrentTime();

                BusinessEventPublisher.PublishBusinessEvent(new RecommendationRejectedEvent
                {
                    ResourceId = recommendation.ResourceId,
                    RejectDateTime = _timeService.GetCurrentTime(),
                    Comment = comment
                });

                unitOfWork.Commit();

                return new BusinessResult(AlertType.Success, string.Format(CustomerPortalResources.RecommendationRejectedMessageFormat, recommendation.Name));
            }
        }

        public BusinessResult RefreshRecommendations(IEnumerable<long> recommendationIds)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var recommendations = unitOfWork.Repositories.Recommendations.GetByIds(recommendationIds).Include(a => a.NeededResource);
                foreach (var recommendation in recommendations)
                {
                    _businessEventPublisher.PublishBusinessEvent(new TriggerCrmRecruitmentActivitySynchronizationEvent
                    {
                        RecruitmentActivityId = recommendation.ResourceId,
                        NeededResourceId = recommendation.NeededResource.CrmId,
                    });
                }
            }

            return new BusinessResult(AlertType.Success, RecommendationResource.RefreshDocumentsMessage);
        }

        public long? GetRecommendationByCrmId(Guid crmId)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Recommendations.SingleOrDefault(r => r.ResourceId == crmId)?.Id;
            }
        }

        public RecommendationDto GetRecommendationById(long id)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var currentUserId = _principalProvider.Current.Id;

                var recommendation = unitOfWork.Repositories.Recommendations
                    .Include(r => r.Notes)
                    .Include(r => r.Notes.Select(n => n.User))
                    .GetById(id);
                
                if (!currentUserId.HasValue || !recommendation.UserId.HasValue ||
                    recommendation.UserId.Value != currentUserId.Value)
                {
                    throw new BusinessException($"Can't find recommendation with Id={id} for user with Id={currentUserId}");
                }

                if (!recommendation.SeenByCustomer)
                {
                    recommendation.SeenByCustomer = true;

                    _activityLogService.GetNewRecommendationActivityService(unitOfWork).ClearActivity(id);
                    _activityLogService.GetAppointmentScheduledActivityService(unitOfWork).ClearActivity(id);
                    _activityLogService.GetChallengeAnsweredActivityService(unitOfWork).ClearActivity(id);

                    _businessEventPublisher.PublishBusinessEvent(
                        new SeenByCustomerEvent
                        {
                            RecommendationCrmId = recommendation.ResourceId,
                            UserId = currentUserId.Value
                        });

                    unitOfWork.Commit();
                }

                return _entityToDtoMapping.CreateFromSource(recommendation);
            }
        }

        public bool CanAccesDocument(long recommendationId, long documentId)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                if (!_principalProvider.Current.Id.HasValue)
                    throw new UnauthorizedException("Only authorized users can open documents");

                return unitOfWork.Repositories
                    .Documents
                    .Any(d =>
                        d.Recommendation.UserId == _principalProvider.Current.Id.Value &&
                        d.RecommendationId == recommendationId &&
                        d.Id == documentId &&
                        d.Recommendation.Status != RecommendationStatus.None &&
                        d.Recommendation.Status != RecommendationStatus.Withdrawn &&
                        d.Recommendation.Status != RecommendationStatus.Archived
                    );
            }
        }

        protected override IEnumerable<Expression<Func<Recommendation, object>>> GetSearchColumns(
            SearchCriteria searchCriteria)
        {
            yield return r => r.Name;
        }

        protected override NamedFilters<Recommendation> NamedFilters
        {
            get
            {
                return new NamedFilters<Recommendation>(new[]
                {
                    new NamedFilter<Recommendation,RecommendationStatus>(
                        FilterCodes.RecommendationStatusFilter,
                        (recommendation, status) => recommendation.Status == status),
                    new NamedFilter<Recommendation>(
                        FilterCodes.RecommendationSeenByCustomer,
                        recommendation => recommendation.SeenByCustomer
                        )
                });
            }
        }

        public override IQueryable<Recommendation> ApplyContextFiltering(IQueryable<Recommendation> records,
            object context)
        {
            var parentContext = context as ParentIdContext;
            return parentContext == null
                ? base.ApplyContextFiltering(records, context)
                : records.Where(r => r.NeededResourceId == parentContext.ParentId);
        }

        /// <summary>
        ///     Gets recommendations that are in pending state - awaiting for acceptance or rejection.
        /// </summary>
        /// <returns>An array of Recommendation records</returns>
        public RecommendationDto[] GetRecordsInPendingState()
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Recommendations
                    .Where(r => !r.IsOutside &&
                                r.Status != RecommendationStatus.Invited &&
                                r.Status != RecommendationStatus.Rejected &&
                                r.Status != RecommendationStatus.Archived &&
                                r.Status != RecommendationStatus.Withdrawn)
                    .Select(e => _entityToDtoMapping.CreateFromSource(e)).ToArray();
            }
        }

        /// <summary>
        ///     Called during record adding, just before adding to repository
        /// </summary>
        protected override void OnRecordAdding(
            RecordAddingEventArgs<Recommendation, RecommendationDto, ICustomerPortalDbScope> eventArgs)
        {
            eventArgs.InputEntity.Documents =
                EntityMergingService.CreateEntitiesFromIds<Document, ICustomerPortalDbScope>(eventArgs.UnitOfWork,
                    eventArgs.InputEntity.Documents.GetIds());

            eventArgs.InputEntity.Status = RecommendationStatus.None;
            eventArgs.InputEntity.AddedOn = _timeService.GetCurrentTime();
        }

        /// <summary>
        ///     Called during record editing, just before adding to repository marked  as 'edit'
        /// </summary>
        protected override void OnRecordEditing(
            RecordEditingEventArgs<Recommendation, RecommendationDto, ICustomerPortalDbScope> eventArgs)
        {
            _uploadedDocumentHandlingService.MergeDocumentCollections(eventArgs.UnitOfWork,
                eventArgs.DbEntity.Documents,
                eventArgs.InputDto.Documents,
                p => new Document
                {
                    Name = p.DocumentName,
                    ContentType = p.ContentType
                });

            eventArgs.InputEntity.ModifiedOn = _timeService.GetCurrentTime();
            eventArgs.InputEntity.ModifiedById = _principalProvider.Current.Id;

            eventArgs.InputEntity.AddedOn = eventArgs.DbEntity.AddedOn;

            if (eventArgs.InputEntity.Status == RecommendationStatus.None)
            {
                eventArgs.InputEntity.Status = RecommendationStatus.Added;

                _activityLogService
                    .GetNewRecommendationActivityService(eventArgs.UnitOfWork)
                    .RegisterActivity(eventArgs.InputEntity.Id);
            }
        }

        protected override void OnRecordEdited(
            RecordEditedEventArgs<Recommendation, RecommendationDto> recordEditedEventArgs)
        {
            _uploadedDocumentHandlingService
                .PromoteTemporaryDocuments<Document, DocumentContent, RecommendationDocumentMapping,
                    ICustomerPortalDbScope>(recordEditedEventArgs.InputDto.Documents);

            base.OnRecordEdited(recordEditedEventArgs);
        }
    }
}