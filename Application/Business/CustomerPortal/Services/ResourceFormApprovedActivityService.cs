﻿using System.Linq;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.Services
{
    internal class ResourceFormApprovedActivityService : ResourceFormActivityBaseService, IActivityLogEntryService
    {
        public ResourceFormApprovedActivityService(IUnitOfWork<ICustomerPortalDbScope> unitOfWork, ITimeService timeService) 
            : base(unitOfWork, timeService, ActivityLogType.RequestApproved)
        {
        }

        protected override IQueryable<ActivityLog> EntitiesToClear(long entityId)
        {
            return UnitOfWork
                .Repositories
                .ActivityLogs
                .Where(d => d.ResourceFormId == entityId &&
                            (d.ActivityType == ActivityLogType.RequestApproved));
        }
    }
}
