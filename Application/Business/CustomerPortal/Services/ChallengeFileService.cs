﻿using System;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.Services
{
    internal class ChallengeFileService : IChallengeFileService
    {
        private readonly IUnitOfWorkService<ICustomerPortalDbScope> _unitOfWorkService;

        public ChallengeFileService(IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        public Guid AddTemporaryChallengeFile(ChallengeFileDto challengeFileDto)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var identifier = Guid.NewGuid();

                var newFile = new ChallengeFile
                {
                    Data = challengeFileDto.Data,
                    Name = challengeFileDto.Name,
                    ContentLength = challengeFileDto.Data.Length,
                    ContentType = challengeFileDto.ContentType,
                    Identifier = identifier
                };

                unitOfWork.Repositories.ChallengeFiles.Add(newFile);
                unitOfWork.Commit();

                return identifier;
            }
        }
    }
}