using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Castle.Core.Internal;
using Castle.Core.Logging;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Data.SharePoint.Interfaces;
using Smt.Atomic.Data.SharePoint.Models;
using DocumentDto = Smt.Atomic.Business.CustomerPortal.Dto.DocumentDto;

namespace Smt.Atomic.Business.CustomerPortal.Services
{
    public class DocumentCardIndexDataService : CardIndexDataService<DocumentDto, Document, ICustomerPortalDbScope>, IDocumentCardIndexDataService
    {
        private readonly IClassMapping<Document, DocumentDto> _entityToDtoMapping;
        private readonly ISharePointService _sharePointService;
        private readonly ILogger _logger;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public DocumentCardIndexDataService(
            ICardIndexServiceDependencies<ICustomerPortalDbScope> dependencies,
            ISharePointService sharePointService,
            ILogger logger)
            : base(dependencies)
        {
            var classMappingFactory = dependencies.ClassMappingFactory;
            _entityToDtoMapping = classMappingFactory.CreateMapping<Document, DocumentDto>();
            _sharePointService = sharePointService;
            _logger = logger;
        }

        public DocumentDto[] GetDocumentsByRecommendationId(long recommendationId)
        {
            DocumentDto[] result;

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var entities = unitOfWork.Repositories.Documents.Where(d => d.RecommendationId == recommendationId).ToArray();
                result = entities.Select(e => _entityToDtoMapping.CreateFromSource(e)).ToArray();
            }

            return result;
        }

        public void UpdateDocumentsContent(long[] documentIds)
        {
            if (documentIds == null || !documentIds.Any())
            {
                return;
            }

            var records = GetRecordsByIds(documentIds);
            
            try
            {
                var task = GetSharePointFilesAsync(records.Values);
                task.Wait();
                var documentContentDtos = task.Result.Select(i => new DocumentContentDto { Id = i.Key, Content = i.Value.Content, Name = i.Value.Name }).ToArray();
                UpdateDocumentsWithContent(documentContentDtos);
            }
            catch (AggregateException ex)
            {
                _logger.Error("Update documents content failed.");
                ex.Flatten().InnerExceptions.ForEach(e => _logger.Error(ex.ToString));
            }
        }

        public DocumentContentDto GetContentById(long id)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var document = unitOfWork.Repositories.Documents.SingleOrDefault(d => d.Id == id);

                return document == null ? null : new DocumentContentDto { Id = document.Id, Content = document.DocumentContent?.Content, Name = document.Name };
            }
        }

        public IEnumerable<DocumentContentDto> GetContentsByIds(long[] documentIds)
        {
            if (documentIds == null || !documentIds.Any())
            {
                return new DocumentContentDto[0];
            }

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var query = from document in unitOfWork.Repositories.Documents
                            where documentIds.Contains(document.Id)
                            let content = document.DocumentContent == null ? null : document.DocumentContent.Content
                            select new DocumentContentDto { Id = document.Id, Content = content, Name = document.Name };

                var documents = query.ToArray();
                return documents;
            }
        }

        private Task<IDictionary<long, SharePointFile>> GetSharePointFilesAsync(ICollection<DocumentDto> documentDtos)
        {
            var sharePointFiles = new ConcurrentDictionary<long, SharePointFile>();
            var parent = Task<IDictionary<long, SharePointFile>>.Factory.StartNew(() =>
            {
                documentDtos.ForEach(d =>
                {
                    var document = d;
                    Task.Factory.StartNew(() =>
                    {
                        var sharePointFile = _sharePointService.GetCandidateDocument(document.UniqueId);
                        sharePointFiles.TryAdd(document.Id, sharePointFile);

                    }, TaskCreationOptions.AttachedToParent);
                });

                return sharePointFiles;
            });

            return parent;
        }

        private void UpdateDocumentsWithContent(DocumentContentDto[] documentContentDtos)
        {
            if (documentContentDtos == null || !documentContentDtos.Any())
            {
                return;
            }

            if (!documentContentDtos.All(c => c.Id > 0))
            {
                throw new InvalidOperationException("Document content id is missing.");
            }

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var documentIds = documentContentDtos.Select(dc => dc.Id).ToArray();

                var documentsToUpdate = unitOfWork.Repositories.Documents.Where(d => documentIds.Contains(d.Id)).ToArray();
                foreach (var document in documentsToUpdate)
                {
                    var documentDtoWithContent = documentContentDtos.Single(d => d.Id == document.Id);
                    if (document.DocumentContent == null)
                    {
                        document.DocumentContent = new DocumentContent { Id = document.Id, Content = documentDtoWithContent.Content };
                    }
                    else
                    {
                        document.DocumentContent.Content = documentDtoWithContent.Content;
                    }
                }

                unitOfWork.Commit();
            }
        }
    }
}
