﻿using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.Services
{
    internal class ResourceFormQuestionActivityService : ResourceFormActivityBaseService, IActivityLogEntryService
    {
        public ResourceFormQuestionActivityService(IUnitOfWork<ICustomerPortalDbScope> unitOfWork, ITimeService timeService) 
            : base(unitOfWork, timeService, ActivityLogType.RequestQuestion)
        {
        }
    }
}
