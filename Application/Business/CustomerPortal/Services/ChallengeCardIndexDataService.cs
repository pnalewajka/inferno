﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.CustomerPortal.BusinessEvents;
using Smt.Atomic.Business.CustomerPortal.DocumentMappings;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.Services
{
    public class ChallengeCardIndexDataService : CardIndexDataService<ChallengeDto, Challenge, ICustomerPortalDbScope>, IChallengeCardIndexDataService
    {
        private readonly IUploadedDocumentHandlingService _uploadedDocumentHandlingService;
        private readonly IClassMappingFactory _classMappingFactory;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public ChallengeCardIndexDataService(
            IUploadedDocumentHandlingService uploadedDocumentHandlingService,
            ICardIndexServiceDependencies<ICustomerPortalDbScope> dependencies)
            : base(dependencies)
        {
            _uploadedDocumentHandlingService = uploadedDocumentHandlingService;
            _classMappingFactory = dependencies.ClassMappingFactory;
        }

        protected override NamedFilters<Challenge> NamedFilters
        {
            get
            {
                return new NamedFilters<Challenge>(new[]
                {
                    new NamedFilter<Challenge>(
                        FilterCodes.NoAnswers,
                        challenge => !challenge.AnswerDocuments.Any()),
                    new NamedFilter<Challenge>(
                        FilterCodes.HasAnswers,
                        challenge => challenge.AnswerDocuments.Any())
                });
            }
        }

        protected override IEnumerable<Expression<Func<Challenge, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return c => c.Recommendation.Name;
        }

        public override IQueryable<Challenge> ApplyContextFiltering(IQueryable<Challenge> records, object context)
        {
            var parentContext = context as ParentIdContext;
            return parentContext == null ?
                base.ApplyContextFiltering(records, context) :
                records.Where(r => r.Recommendation.NeededResourceId == parentContext.ParentId);
        }

        public void SendAssignmentToCustomer(long id, string comment)
        {
            BusinessEventPublisher.PublishBusinessEvent(new AssignmentSentBusinessEvent
            {
                RecommendationId = id,
                Comment = comment
            });
        }

        public ChallengeDto GetByRecommendationId(long recommendationId)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var challenge = unitOfWork.Repositories.Challenges.SingleOrDefault(c => c.RecommendationId == recommendationId);

                if (challenge == null)
                {
                    return null;
                }

                var mapper = _classMappingFactory.CreateMapping<Challenge, ChallengeDto>();

                return mapper.CreateFromSource(challenge);
            }
        }

        public DocumentContentDto GetAnswerContentById(long documentId)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var document = unitOfWork.Repositories.AnswerDocuments.GetById(documentId);

                return document == null ? null : new DocumentContentDto
                {
                    Id = document.Id,
                    Content = document.DocumentContent?.Content,
                    Name = document.Name
                };
            }
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<Challenge, ChallengeDto, ICustomerPortalDbScope> eventArgs)
        {
            bool firstAnswerDocuments = !eventArgs.DbEntity.AnswerDocuments.Any() && eventArgs.InputDto.AnswerDocuments.Any();

            if (firstAnswerDocuments)
            {
                BusinessEventPublisher.PublishBusinessEvent(new AssignmentSentBusinessEvent
                {
                    RecommendationId = eventArgs.DbEntity.Id,
                    Comment = eventArgs.InputDto.AnswerComment
                });
            }

            _uploadedDocumentHandlingService
                 .MergeDocumentCollections(
                     eventArgs.UnitOfWork,
                     eventArgs.DbEntity.AnswerDocuments,
                     eventArgs.InputDto.AnswerDocuments,
                     p => new AnswerDocument
                     {
                         Name = p.DocumentName,
                         ContentType = p.ContentType
                     }
                 );
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<Challenge, ChallengeDto> recordEditedEventArgs)
        {
            _uploadedDocumentHandlingService
                  .PromoteTemporaryDocuments<AnswerDocument, AnswerDocumentContent, AnswerDocumentMapping, ICustomerPortalDbScope>(
                      recordEditedEventArgs.InputDto.AnswerDocuments
                  );
        }
    }
}
