﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.Services
{
    internal class ScheduleAppointmentCardIndexDataService : CardIndexDataService<ScheduleAppointmentDto, ScheduleAppointment, ICustomerPortalDbScope>, IScheduleAppointmentCardIndexDataService
    {
        public ScheduleAppointmentCardIndexDataService(ICardIndexServiceDependencies<ICustomerPortalDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public override IQueryable<ScheduleAppointment> ApplyContextFiltering(IQueryable<ScheduleAppointment> records, object context)
        {
            records = records.Where(r => r.AppointmentStatus != AppointmentStatusType.NotUsed);

            var parentContext = context as ParentIdContext;
            return parentContext == null ?
                base.ApplyContextFiltering(records, context) :
                records.Where(r => r.Recommendation.NeededResourceId == parentContext.ParentId);
        }

        protected override IEnumerable<Expression<Func<ScheduleAppointment, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return a => a.Recommendation.Name;
        }

        protected override NamedFilters<ScheduleAppointment> NamedFilters
        {
            get
            {
                return new NamedFilters<ScheduleAppointment>(new[]
                {
                    new NamedFilter<ScheduleAppointment,AppointmentStatusType>(
                        FilterCodes.AppointmentStatus,
                        (appointment, status) => appointment.AppointmentStatus == status),
                });
            }
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<ScheduleAppointment> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            if (sortingCriterion.GetSortingColumnName() == "DisplayDate")
            {
                var direction = sortingCriterion.GetSortingDirection();
                orderedQueryBuilder.ApplySortingKey(r => r.AppointmentDate, direction);
            }
            else
            {
                base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
            }
        }
    }
}
