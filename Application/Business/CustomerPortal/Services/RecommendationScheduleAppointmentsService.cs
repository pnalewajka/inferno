﻿using Smt.Atomic.Business.CustomerPortal.BusinessEvents;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Crm.BusinessEvents;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.CustomerPortal.Services
{
    class RecommendationScheduleAppointmentsService : IRecomendationScheduleAppointmentsService
    {
        private readonly IUnitOfWorkService<ICustomerPortalDbScope> _unitOfWorkService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IClassMappingFactory _classMappingFactory;
        private readonly ITimeService _timeService;
        private readonly IBusinessEventPublisher _businessEventPublisher;
        private readonly IActivityLogService _activityLogService;

        public RecommendationScheduleAppointmentsService(
            IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService,
            IPrincipalProvider principalProvider,
            IClassMappingFactory classMappingFactory,
            IActivityLogService activityLogService,
            ITimeService timeService,
            IBusinessEventPublisher businessEventPublisher)
        {
            _unitOfWorkService = unitOfWorkService;
            _principalProvider = principalProvider;
            _classMappingFactory = classMappingFactory;
            _timeService = timeService;
            _activityLogService = activityLogService;
            _businessEventPublisher = businessEventPublisher;
        }

        public IEnumerable<ScheduleAppointmentDto> GetAppointments()
        {
            var classMapping = _classMappingFactory.CreateMapping<ScheduleAppointment, ScheduleAppointmentDto>();
            var currentUserId = _principalProvider.Current.Id;
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var appointments =
                    unitOfWork.Repositories.ScheduleAppointments.Where(
                        sa =>
                            sa.Recommendation.Status != RecommendationStatus.None &&
                            sa.Recommendation.Status != RecommendationStatus.Withdrawn &&
                            sa.Recommendation.UserId == currentUserId &&
                            sa.AppointmentStatus != AppointmentStatusType.NotUsed);

                return appointments.Select(classMapping.CreateFromSource).ToList();
            }
        }

        public IEnumerable<ChallengeDto> GetChallenges()
        {
            var classMapping = _classMappingFactory.CreateMapping<Challenge, ChallengeDto>();
            var currentUserId = _principalProvider.Current.Id;
            var currentDate = _timeService.GetCurrentTime();

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var challenges = unitOfWork.Repositories.Challenges.Where(
                    c =>
                        c.Recommendation.Status != RecommendationStatus.None &&
                        c.Recommendation.Status != RecommendationStatus.Withdrawn &&
                        c.Recommendation.UserId == currentUserId &&
                        c.Deadline > currentDate
                );

                return challenges.Select(classMapping.CreateFromSource).ToList();
            }
            throw new System.NotImplementedException();
        }

        public long? ApproveAppointment(long appointmentId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var appointment = unitOfWork.Repositories.ScheduleAppointments.GetById(appointmentId);

                if (appointment.AppointmentStatus != AppointmentStatusType.Pending)
                {
                    throw new BusinessException($"Appointment is already approved");
                }

                appointment.AppointmentStatus = AppointmentStatusType.Accepted;

                var otherAppointmentsForRecomendation = unitOfWork.Repositories.ScheduleAppointments.Where(
                    a => a.RecommendationId == appointment.RecommendationId && a.Id != appointment.Id)
                    .ToList();

                foreach (var scheduleAppointment in otherAppointmentsForRecomendation)
                {
                    scheduleAppointment.AppointmentStatus = AppointmentStatusType.NotUsed;
                }

                _activityLogService
                    .GetAppointmentScheduledActivityService(unitOfWork)
                    .RegisterActivity(appointment.RecommendationId);

                PublishAppointmentScheduled(appointment);
                SendAppointmentNotification(appointment);

                unitOfWork.Commit();

                return appointment.Recommendation.NeededResourceId;
            }
        }

        public long? ReSendNotification(long appointmentId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var appointment = unitOfWork.Repositories.ScheduleAppointments.GetById(appointmentId);

                if (appointment.AppointmentStatus != AppointmentStatusType.Accepted)
                {
                    throw new BusinessException($"Appointment is not approved");
                }

                SendAppointmentNotification(appointment);

                return appointment.Recommendation.NeededResourceId;
            }
        }

        public void PublishAppointmentScheduled(ScheduleAppointment appointment)
        {
            _businessEventPublisher.PublishBusinessEvent(new ScheduleAppointmentEvent
            {
                RecommendationId = appointment.RecommendationId,
                AppointmentStartDateTime = appointment.AppointmentDate,
                AppointmentEndDateTime = appointment.AppointmentEndDate,
                CreatedOn = _timeService.GetCurrentTime(),
            });
        }

        public void SendAppointmentNotification(ScheduleAppointment appointment)
        {
            _businessEventPublisher.PublishBusinessEvent(new NotifyScheduleAppointmentEvent
            {
                OperationManagerId = _principalProvider.Current.Id.Value,
                RecommendationId = appointment.RecommendationId,
                AppointmentStartDateTime = appointment.AppointmentDate,
                AppointmentEndDateTime = appointment.AppointmentEndDate,
                CreatedOn = _timeService.GetCurrentTime(),
            });
        }
    }
}
