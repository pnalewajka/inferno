using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Castle.Core.Internal;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Helpers;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Crm.Dto;
using Smt.Atomic.Data.Crm.Interfaces;
using EmailDto = Smt.Atomic.Business.CustomerPortal.Dto.EmailDto;

namespace Smt.Atomic.Business.CustomerPortal.Services
{
    public class RecommendationEmailMessageFactory : IRecommendationEmailMessageFactory
    {
        private const string SenderName = "intive Customer Portal";

        private readonly ICrmConnectionStringProvider _crmConnectionStringProvider;
        private readonly ICrmService _crmService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IDocumentCardIndexDataService _documentService;
        private readonly IRecommendationStateCardIndexDataService _recommendationStateService;
        private readonly IUserCardIndexService _userService;
        private readonly string CrmUrlFormat;

        public RecommendationEmailMessageFactory(
            IMessageTemplateService messageTemplateService,
            ICrmService crmService,
            ICrmConnectionStringProvider crmConnectionStringProvider,
            IPrincipalProvider principalProvider,
            ISystemParameterService systemParameterService,
            IDocumentCardIndexDataService documentService,
            IRecommendationStateCardIndexDataService recommendationStateService,
            IUserCardIndexService userService)
        {
            _crmService = crmService;
            _crmConnectionStringProvider = crmConnectionStringProvider;
            _principalProvider = principalProvider;
            _systemParameterService = systemParameterService;
            _messageTemplateService = messageTemplateService;
            _documentService = documentService;
            _recommendationStateService = recommendationStateService;
            _userService = userService;

            CrmUrlFormat = $"{_crmConnectionStringProvider.GetUrl()}/main.aspx?etn={Data.Crm.CrmModel.smt_recruitmentactivity.EntityLogicalName}&id={{0}}&pagetype=entityrecord";
        }

        public async Task<EmailMessageDto> CreateNewRecommendationEmailMessageAsync(RecommendationDto recommendationDto, IEnumerable<DocumentDto> documentDtos)
        {
            if (recommendationDto == null)
            {
                throw new ArgumentNullException(nameof(recommendationDto), "Recommendation dto cannot be null.");
            }

            var candidate = await _crmService.GetCandidateAsync(recommendationDto.ResourceId, loadCandidateContacts: true);

            var user = GetUserWhichAddedRecommendation(recommendationDto);

            var recommendationEmailDto = new RecommendationEmailDto
            {
                CommentLines = StringHelper.BreakString(recommendationDto.Note),
                ContactName = candidate?.ContactName,
                ContactFirstName = candidate?.ContactFirstName,
                ContactEmail = candidate?.ContactEmail,
                NeededResourceName = candidate?.NeededResourceName,
                ResourceName = candidate?.FullName,
                ResourceUrl = GetResourceUrl(recommendationDto.Id),
                Documents = documentDtos,
                SdmName = GetFullNameStartingWithLastName(user),
                SdmFirstName = user?.FirstName,
                SdmEmail = user?.Email
            };

            if (!recommendationEmailDto.IsValid)
            {
                throw new InvalidOperationException("Recommendation email dto is not valid.");
            }

            var message = new EmailMessageDto
            {
                IsHtml = true
            };

            SetMessageSender(message);
            SetMessageRecipient(message, recommendationEmailDto.ContactEmail, recommendationEmailDto.ContactName);

            SetMessageCcRecipient(message, recommendationEmailDto.SdmEmail, recommendationEmailDto.SdmName);
            SetMessageReplyTo(message, recommendationEmailDto.SdmEmail, recommendationEmailDto.SdmName);

            if (!AreEmailsEqual(recommendationEmailDto.SdmEmail, candidate?.SdmEmail))
            {
                SetMessageCcRecipient(message, candidate?.SdmEmail, candidate?.SdmName);
            }

            var subjectTask = _messageTemplateService.ResolveTemplateAsync(TemplateCodes.CustomerPortalNotificationsOnRecommendationSentSubject, recommendationEmailDto);
            var bodyTask = ResolveBodyAsync(TemplateCodes.CustomerPortalNotificationsOnRecommendationSentBody, recommendationEmailDto);
            await Task.WhenAll(subjectTask, bodyTask);

            message.Subject = subjectTask.Result.Content;
            message.Body = bodyTask.Result.Content;
            message.IsHtml = bodyTask.Result.IsHtml;

            return message;
        }

        public async Task<EmailMessageDto> CreateRecommendationStateChangedEmailMessageAsync(RecommendationDto recommendationDto)
        {
            if (recommendationDto == null)
            {
                throw new ArgumentNullException(nameof(recommendationDto), "Recommendation dto cannot be null.");
            }

            var candidateTask = _crmService.GetCandidateAsync(recommendationDto.ResourceId, loadCandidateContacts: true);
            var clientTask = _crmService.GetClientContactsByRecruitmentActivityIdsAsync(new[] { recommendationDto.ResourceId });
            await Task.WhenAll(candidateTask, clientTask);

            var candidate = candidateTask.Result;
            var client = clientTask.Result.FirstOrDefault();
            var emailDto = CreateEmailDto(recommendationDto, candidate, client, recommendationDto.Status);

            if (!emailDto.IsValid)
            {
                throw new InvalidOperationException("Customer decision email dto is not valid.");
            }

            var message = new EmailMessageDto
            {
                IsHtml = true,
            };

            SetMessageSender(message);

            string subjectTemplateCode;
            string bodyTemplateCode;

            switch (recommendationDto.Status)
            {
                case RecommendationStatus.Invited:
                    subjectTemplateCode = TemplateCodes.CustomerPortalNotificationsOnRecommendationAcceptedSubject;
                    bodyTemplateCode = TemplateCodes.CustomerPortalNotificationsOnRecommendationAcceptedBody;
                    SetMessageRecipient(message, emailDto.SdmEmail, emailDto.SdmName);
                    SetMessageReplyTo(message, emailDto.ContactEmail, emailDto.ContactName);
                    break;

                case RecommendationStatus.Rejected:
                    subjectTemplateCode = TemplateCodes.CustomerPortalNotificationsOnRecommendationRejectedSubject;
                    bodyTemplateCode = TemplateCodes.CustomerPortalNotificationsOnRecommendationRejectedBody;
                    SetMessageRecipient(message, emailDto.SdmEmail, emailDto.SdmName);
                    SetMessageReplyTo(message, emailDto.ContactEmail, emailDto.ContactName);
                    break;

                case RecommendationStatus.Withdrawn:
                    subjectTemplateCode = TemplateCodes.CustomerPortalNotificationsOnRecommendationWithdrawnSubject;
                    bodyTemplateCode = TemplateCodes.CustomerPortalNotificationsOnRecommendationWithdrawnBody;
                    SetMessageRecipient(message, emailDto.ContactEmail, emailDto.ContactName);
                    SetMessageReplyTo(message, emailDto.SdmEmail, emailDto.SdmName);
                    break;

                default:
                    throw new InvalidOperationException("Invalid recommendation status");
            }

            var subjectTask = _messageTemplateService.ResolveTemplateAsync(subjectTemplateCode, emailDto);
            var bodyTask = ResolveBodyAsync(bodyTemplateCode, emailDto);
            await Task.WhenAll(subjectTask, bodyTask);

            message.Subject = subjectTask.Result.Content;
            message.Body = bodyTask.Result.Content;
            message.IsHtml = bodyTask.Result.IsHtml;

            return message;
        }

        public async Task<EmailMessageDto> CreateRecommendationQuestionEmailMessageAsync(RecommendationDto recommendationDto, QuestionDto questionDto)
        {
            if (recommendationDto == null)
            {
                throw new ArgumentNullException(nameof(recommendationDto), @"Recommendation dto cannot be null.");
            }

            if (questionDto == null)
            {
                throw new ArgumentNullException(nameof(questionDto), @"Question dto cannot be null.");
            }

            var candidateTask = _crmService.GetCandidateAsync(recommendationDto.ResourceId, loadCandidateContacts: true);
            var clientTask = _crmService.GetClientContactsByRecruitmentActivityIdsAsync(new[] { recommendationDto.ResourceId });
            await Task.WhenAll(candidateTask, clientTask);

            var candidate = candidateTask.Result;
            var client = clientTask.Result.FirstOrDefault();

            var questionEmail = new QuestionEmailDto
            {
                ResourceName = candidate?.FullName,
                NeededResourceName = candidate?.NeededResourceName,
                QuestionLines = StringHelper.BreakString(questionDto.Query),
                ContactName = candidate?.ContactName,
                ContactFirstName = candidate?.ContactFirstName,
                ContactEmail = candidate?.ContactEmail,
                SdmEmail = candidate?.SdmEmail,
                SdmName = candidate?.SdmName,
                SdmFirstName = candidate?.SdmFirstName,
                CompanyName = client?.CompanyName
            };

            if (!questionEmail.IsValid)
            {
                throw new InvalidOperationException("Question email dto is not valid.");
            }

            var message = new EmailMessageDto
            {
                IsHtml = true,
            };

            SetMessageSender(message);
            SetMessageRecipient(message, questionEmail.SdmEmail, questionEmail.SdmName);
            SetMessageReplyTo(message, questionEmail.ContactEmail, questionEmail.ContactName);

            var subjectTask = _messageTemplateService.ResolveTemplateAsync(TemplateCodes.CustomerPortalNotificationsAskAQuestionSubject, questionEmail);
            var bodyTask = ResolveBodyAsync(TemplateCodes.CustomerPortalNotificationsAskAQuestionBody, questionEmail);
            await Task.WhenAll(subjectTask, bodyTask);

            message.Subject = subjectTask.Result.Content;
            message.Body = bodyTask.Result.Content;
            message.IsHtml = bodyTask.Result.IsHtml;

            return message;
        }

        public async Task<IEnumerable<EmailMessageDto>> CreateRecommendationInPendingStateSummaryEmailMessagesAsync(IEnumerable<RecommendationDto> recommendationDtos)
        {
            if (recommendationDtos.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(recommendationDtos), "RecommendationDtos cannot be null or empty.");
            }

            var recommendationsPerClients = recommendationDtos
                                           .OrderByDescending(x => x.CreatedOn)
                                           .GroupBy(x => x.UserId);

            var emailMessagesDtos = new List<EmailMessageDto>();

            foreach (var recommendationsPerClient in recommendationsPerClients)
            {
                var recommendationPerCustomerEmailMessageDto =
                    await CreateRecommendationInPendingStateSummaryForClient(recommendationsPerClient.ToList());

                emailMessagesDtos.Add(recommendationPerCustomerEmailMessageDto);
            }

            return emailMessagesDtos;
        }

        private async Task<EmailMessageDto> CreateRecommendationInPendingStateSummaryForClient(IEnumerable<RecommendationDto> recommendations)
        {
            var resourceId = recommendations.First().ResourceId;
            var clientContactDto = (await _crmService.GetClientContactsByRecruitmentActivityIdsAsync(new[] { resourceId })).Single();
            var candidate = await _crmService.GetCandidateAsync(resourceId, true);

            var model = new RecommendationsInPendingStateEmailDto
            {
                ClientFirstName = clientContactDto.FirstName,
                ClientName = clientContactDto.Name,
                ClientEmail = clientContactDto.Email,
                SdmName = candidate.SdmName,
                SdmEmail = candidate.SdmEmail,
                FooterHtml = GetFooterHtml()
            };

            foreach (var recommendationPerNeededResource in recommendations.GroupBy(r => r.NeededResourceName).OrderBy(g => g.Key))
            {
                var neededResourceName = NeededResourceNameHelper.PrepareNeededResourceNiceName(recommendationPerNeededResource.Key);

                var recommendedCandidatesPerNeededResource =
                         recommendationPerNeededResource
                        .OrderBy(r => r.CreatedOn.Date).ThenBy(r => r.Name)
                        .Select(r => new RecommendedCandidateDto
                        {
                            CandidateName = r.Name,
                            RecommendationDate = r.CreatedOn,
                            ResourceUrl = GetResourceUrl(r.Id),
                        })
                        .ToList();

                model.RecommendedCandidatesPerNeededResource.Add(neededResourceName, recommendedCandidatesPerNeededResource);
            }

            if (!model.IsValid)
            {
                throw new InvalidOperationException("RecommendationsInPendingStateEmailDto is not valid.");
            }

            var subjectTask = _messageTemplateService.ResolveTemplateAsync(
                TemplateCodes.CustomerPortalNotificationsRecommendationsInPendingStateEmailTemplateSubject, model);
            var bodyTask = _messageTemplateService.ResolveTemplateAsync(
                TemplateCodes.CustomerPortalNotificationsRecommendationsInPendingStateEmailTemplateBody, model);
            await Task.WhenAll(subjectTask, bodyTask);

            var message = new EmailMessageDto
            {
                IsHtml = bodyTask.Result.IsHtml,
                Subject = subjectTask.Result.Content,
                Body = bodyTask.Result.Content
            };

            SetMessageSender(message);
            SetMessageRecipient(message, model.ClientEmail, model.ClientName);
            SetMessageReplyTo(message, model.SdmEmail, model.SdmName);

            return message;
        }

        private void SetMessageSender(EmailMessageDto message)
        {
            message.From = new EmailRecipientDto
            {
                EmailAddress = _systemParameterService.GetParameter<string>(ParameterKeys.SmtpEmailServiceDefaultFromEmailAddress),
                FullName = SenderName
            };
        }

        private void SetMessageRecipient(EmailMessageDto message, string recipientEmail, string recipientName)
        {
            CheckIfNameAndEmailAreNotEmpty(recipientEmail, recipientName);
            message.Recipients.Add(new EmailRecipientDto(recipientEmail, recipientName));
        }

        private void SetMessageCcRecipient(EmailMessageDto message, string ccRecipientEmail, string ccRecipientName)
        {
            CheckIfNameAndEmailAreNotEmpty(ccRecipientEmail, ccRecipientName);
            message.Recipients.Add(new EmailRecipientDto(ccRecipientEmail, ccRecipientName, RecipientType.Cc));
        }

        private void SetMessageReplyTo(EmailMessageDto message, string replyToEmail, string replyToName)
        {
            CheckIfNameAndEmailAreNotEmpty(replyToEmail, replyToName);
            message.ReplyTo = new EmailRecipientDto(replyToEmail, replyToName);
        }

        private void CheckIfNameAndEmailAreNotEmpty(string emailAddress, string name)
        {
            if (string.IsNullOrEmpty(emailAddress))
            {
                throw new ArgumentNullException(nameof(emailAddress), "email address cannot be null.");
            }
            if (string.IsNullOrEmpty(emailAddress))
            {
                throw new ArgumentNullException(nameof(name), "name cannot be null.");
            }
        }

        private EmailDto CreateEmailDto(
            RecommendationDto recommendationDto,
            CrmCandidateDto candidate, Data.Crm.Dto.ClientContactDto client, RecommendationStatus status)
        {
            switch (status)
            {
                case RecommendationStatus.Invited:
                case RecommendationStatus.Rejected:
                    return new CustomerDecisionEmailDto
                    {
                        ResourceName = candidate?.FullName,
                        NeededResourceName = candidate?.NeededResourceName,
                        // TODO: Add note
                        CommentLines = StringHelper.BreakString(""),
                        ContactName = candidate?.ContactName,
                        ContactFirstName = candidate?.ContactFirstName,
                        ContactEmail = candidate?.ContactEmail,
                        SdmEmail = candidate?.SdmEmail,
                        SdmName = candidate?.SdmName,
                        SdmFirstName = candidate?.SdmFirstName,
                        CompanyName = client?.CompanyName,
                        CrmUrl = string.Format(CrmUrlFormat, recommendationDto.ResourceId.ToString("B"))
                    };
                case RecommendationStatus.Withdrawn:
                    return new WithdrawRecommendationEmailDto
                    {
                        ResourceName = candidate?.FullName,
                        NeededResourceName = candidate?.NeededResourceName,
                        // TODO: Add note
                        CommentLines = StringHelper.BreakString(""),
                        ContactName = candidate?.ContactName,
                        ContactFirstName = candidate?.ContactFirstName,
                        ContactEmail = candidate?.ContactEmail,
                        SdmEmail = candidate?.SdmEmail,
                        SdmName = candidate?.SdmName,
                        SdmFirstName = candidate?.SdmFirstName
                    };
                default:
                    throw new InvalidOperationException("Invalid recommendation status");
            }
        }

        private UserDto GetUserWhichAddedRecommendation(RecommendationDto recommendationDto)
        {
            return null;
        }

        private string GetResourceUrl(long recommendationId)
        {
            var serverAddress = _systemParameterService.GetParameter<string>(ParameterKeys.CustomerPortalServerAddress);
            var resourceUrlFormat = _systemParameterService.GetParameter<string>(ParameterKeys.CustomerPortalRecommendationUrlFormat);
            var resourceUrl = string.Format(resourceUrlFormat, serverAddress, recommendationId);

            return resourceUrl;
        }

        private string GetFullNameStartingWithLastName(UserDto userDto)
        {
            return $"{userDto.LastName} {userDto.FirstName}".Trim();
        }

        private bool AreEmailsEqual(string emailAddress1, string emailAddress2)
        {
            return string.Equals(emailAddress1, emailAddress2, StringComparison.OrdinalIgnoreCase);
        }

        private Task<TemplateProcessingResult> ResolveBodyAsync(string templateCode, EmailDto messageModel)
        {
            messageModel.FooterHtml = GetFooterHtml();
            return _messageTemplateService.ResolveTemplateAsync(templateCode, messageModel);
        }

        private string GetFooterHtml()
        {
            return _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalNotificationsFooter, null).Content;
        }
    }
}
