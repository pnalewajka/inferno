﻿using System.Linq;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.Services
{
    internal abstract class ResourceFormActivityBaseService {

        protected readonly IUnitOfWork<ICustomerPortalDbScope> UnitOfWork;
        private readonly ITimeService _timeService;
        private readonly ActivityLogType _logType;

        protected ResourceFormActivityBaseService(IUnitOfWork<ICustomerPortalDbScope> unitOfWork, ITimeService timeService, ActivityLogType logType)
        {
            UnitOfWork = unitOfWork;
            _timeService = timeService;
            _logType = logType;
        }

        public virtual void RegisterActivity(long entityId)
        {
            UnitOfWork.Repositories.ActivityLogs.Add(new ActivityLog
            {
                ActivityType = _logType,
                ResourceFormId = entityId
            });
        }

        protected virtual IQueryable<ActivityLog> EntitiesToClear(long entityId)
        {
            return UnitOfWork
                .Repositories
                .ActivityLogs
                .Where(d => d.ResourceFormId == entityId &&
                            (d.ActivityType == ActivityLogType.RequestApproved ||
                             d.ActivityType == ActivityLogType.RequestQuestion));
        }

        public virtual void ClearActivity(long entityId)
        {
            var elements = EntitiesToClear(entityId)
                .ToList();

            var currentDateTime = _timeService.GetCurrentTime();

            foreach (var element in elements)
            {
                element.SeenByUserOn = currentDateTime;
            }
        }
    }
}
