﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.CustomerPortal.BusinessEvents;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.Business.CustomerPortal.Resources;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.Services
{
    public class ClientDataCardIndexDataService : CardIndexDataService<ClientDataDto, ClientData, ICustomerPortalDbScope>, IClientDataCardIndexDataService
    {
        private readonly ICardIndexServiceDependencies<ICustomerPortalDbScope> _cardIndexServiceDependencies;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public ClientDataCardIndexDataService(ICardIndexServiceDependencies<ICustomerPortalDbScope> dependencies)
            : base(dependencies)
        {
            _cardIndexServiceDependencies = dependencies;
        }

        protected override IEnumerable<Expression<Func<ClientData, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return r => r.ManagerUser.LastName;
            yield return r => r.ManagerUser.FirstName;
            yield return r => r.ClientUser.LastName;
            yield return r => r.ClientUser.FirstName;
            yield return r => r.ClientContact.CompanyName;
        }

        public AccountManagerDto GetAccountManagerForClient(long clientId)
        {
            using (var unitOfWork = _cardIndexServiceDependencies.UnitOfWorkService.Create())
            {
                var clientData = unitOfWork.Repositories.ClientDatas.SingleOrDefault(c => c.ClientUserId == clientId);

                if (clientData == null)
                {
                    throw new BusinessException(CustomerPortalResources.ClientHasNoOMAssignedException);
                }

                var user = unitOfWork.Repositories.Users.GetById(clientData.ManagerUserId);

                return new AccountManagerDto
                {
                    Id = user.Id,
                    Name = user.FullName,
                    Email = user.Email,
                    Phone = clientData.ContactPersonPhone
                };
            }
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<ClientData, ClientDataDto> recordAddedEventArgs)
        {
            BusinessEventPublisher.PublishBusinessEvent(new TriggerCrmClientSynchronizationEvent { ClientDataId = recordAddedEventArgs.InputEntity.Id });

            base.OnRecordAdded(recordAddedEventArgs);
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<ClientData, ClientDataDto, ICustomerPortalDbScope> eventArgs)
        {
            if (eventArgs.InputDto.ClientUserId != eventArgs.DbEntity.ClientUserId)
            {
                throw new BusinessException(CustomerPortalResources.CannotChangeClientInClientDataExceptionMessage);
            }
        }

        protected override NamedFilters<ClientData> NamedFilters
        {
            get
            {
                return new NamedFilters<ClientData>(new[]
                {
                    new NamedFilter<ClientData, long>(
                        FilterCodes.ManagerUser,
                        (clientData, managerUserId) => clientData.ManagerUserId == managerUserId)
                });
            }
        }
    }
}
