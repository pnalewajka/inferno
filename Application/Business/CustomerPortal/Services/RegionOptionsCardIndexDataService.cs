﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.Services
{
    public class RegionOptionsCardIndexDataService : ReadOnlyCardIndexDataService<RegionOptionsDto, RegionOptions, ICustomerPortalDbScope>, IRegionOptionsCardIndexDataService
    {
        public RegionOptionsCardIndexDataService(ICardIndexServiceDependencies<ICustomerPortalDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<RegionOptions> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            orderedQueryBuilder.ApplySortingKey(r => r.DisplayValue, SortingDirection.Ascending);
            base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
        }

        protected override IEnumerable<Expression<Func<RegionOptions, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return r => r.DisplayValue;
        }
    }
}
