﻿using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using System;
using System.Linq;

namespace Smt.Atomic.Business.CustomerPortal.Services
{
    public class AppointmentScheduledActivityService : IActivityLogEntryService
    {
        private readonly ITimeService _timeService;
        private readonly IUnitOfWork<ICustomerPortalDbScope> _unitOfWork;

        public AppointmentScheduledActivityService(IUnitOfWork<ICustomerPortalDbScope> unitOfWork, ITimeService timeService)
        {
            _unitOfWork = unitOfWork;
            _timeService = timeService;
        }

        public void ClearActivity(long entityId)
        {
            var elements = _unitOfWork.Repositories.ActivityLogs
                .Where(d => d.RecommendationId == entityId && d.ActivityType == ActivityLogType.AppointmentScheduled)
                .ToList();

            var currentDateTime = _timeService.GetCurrentTime();

            foreach (var activityLog in elements)
            {
                activityLog.SeenByUserOn = currentDateTime;
            }
        }

        public void RegisterActivity(long entityId)
        {
            _unitOfWork.Repositories.ActivityLogs.Add(new ActivityLog
            {
                ActivityType = ActivityLogType.AppointmentScheduled,
                RecommendationId = entityId
            });
        }
    }
}
