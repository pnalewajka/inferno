using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.CustomerPortal.BusinessEvents;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Scopes;
using System;
using System.Linq;
using System.Transactions;

namespace Smt.Atomic.Business.CustomerPortal.Services
{
    public class RecommendationStateCardIndexDataService : IRecommendationStateCardIndexDataService
    {
        private readonly IBusinessEventPublisher _businessEventPublisher;
        private readonly ICardIndexServiceDependencies<ICustomerPortalDbScope> _dependencies;
        private readonly IPrincipalProvider _principalProvider;
        private readonly ITimeService _timeService;

        public RecommendationStateCardIndexDataService(
            ICardIndexServiceDependencies<ICustomerPortalDbScope> dependencies,
            IPrincipalProvider principalProvider,
            ITimeService timeService,
            IBusinessEventPublisher businessEventPublisher)
        {
            _dependencies = dependencies;
            _principalProvider = principalProvider;
            _timeService = timeService;
            _businessEventPublisher = businessEventPublisher;
        }

        public void ApproveRecommendation(IRecommendationApproved approveData)
        {
            using (var transaction = new TransactionScope())
            {
                using (var unitOfWork = _dependencies.UnitOfWorkService.Create())
                {
                    var recomendation = unitOfWork.Repositories.Recommendations.GetById(approveData.RecomendationId);

                    recomendation.Status = RecommendationStatus.Approved;
                    unitOfWork.Commit();

                    _businessEventPublisher.PublishBusinessEvent(new RecommendationApprovedEvent
                    {
                        RecommendationId = approveData.RecomendationId,
                        ClientUserId = _principalProvider.Current.Id.Value,
                        Comment = approveData.Comment,
                    });

                    transaction.Complete();
                }
            }
        }

        public RecommendationDto ScheduleRecomendationNextStep(IRecomendationSchedule nextStepData)
        {
            using (var transaction = new TransactionScope())
            {
                using (var unitOfWork = _dependencies.UnitOfWorkService.Create())
                {
                    var mapping = _dependencies.ClassMappingFactory.CreateMapping<Recommendation, RecommendationDto>();
                    var recomendation = unitOfWork.Repositories.Recommendations.GetById(nextStepData.RecomendationId);

                    recomendation.Status = RecommendationStatus.Invited;

                    var recomendationDto = mapping.CreateFromSource(recomendation);

                    foreach (var suggestedDate in nextStepData.SuggestedDates)
                    {
                        unitOfWork.Repositories.ScheduleAppointments.Add(new ScheduleAppointment
                        {
                            AppointmentDate = suggestedDate.StartsOn,
                            AppointmentEndDate = suggestedDate.EndsOn,
                            AppointmentStatus = AppointmentStatusType.Pending,
                            Recommendation = recomendation,
                            InterviewType = nextStepData.InterviewType
                        });
                    }

                    var sugestedDates = nextStepData.SuggestedDates.Select(s => new SuggestedDate { StartsOn = s.StartsOn, EndsOn = s.EndsOn });
                    var createdOn = _timeService.GetCurrentTime();

                    unitOfWork.Repositories.Notes.Add(new Note
                    {
                        RecommendationId = recomendation.Id,
                        Content = $"Suggested interview slots: { string.Join(", ", sugestedDates.Select(s => $"from { s.StartsOn } to { s.EndsOn }")) }",
                        SavedInCRM = false,
                        UserId = _principalProvider.Current.Id,
                        CreatedOn = createdOn
                    });

                    unitOfWork.Commit();

                    _businessEventPublisher.PublishBusinessEvent(new AddRecruitmentActivityEvent
                    {
                        CreatedOn = createdOn,
                        ResourceId = recomendation.ResourceId,
                        SuggestedDates = sugestedDates,
                    });

                    _businessEventPublisher.PublishBusinessEvent(new NotifyAboutInterviewScheduledEvent
                    {
                        Comment = nextStepData.Note,
                        CreatedOn = createdOn,
                        ContactName = $"{_principalProvider.Current.FirstName} {_principalProvider.Current.LastName}",
                        NeededResourceId = recomendation.NeededResourceId.Value,
                        NeededResourceName = recomendation.NeededResource.Name,
                        ResourceName = recomendation.Name,
                        SuggestedDates = sugestedDates,
                        OperationManagerId = recomendation.UserId.Value,
                    });

                    transaction.Complete();

                    return recomendationDto;
                }
            }
        }

        public RecommendationDto ScheduleChallengeNextStep(IRecomendationChallenge nextStepData)
        {
            using (var transaction = new TransactionScope())
            {
                using (var unitOfWork = _dependencies.UnitOfWorkService.Create())
                {
                    var recomendation = unitOfWork.Repositories.Recommendations.GetById(nextStepData.RecomendationId);

                    unitOfWork.Repositories.Challenges.Add(new Challenge
                    {
                        RecommendationId = recomendation.Id,
                        Deadline = nextStepData.Deadline,
                        ChallengeComment = nextStepData.Note,
                    });

                    recomendation.Status = RecommendationStatus.Invited;

                    var mapping = _dependencies.ClassMappingFactory.CreateMapping<Recommendation, RecommendationDto>();
                    var recommendationDto = mapping.CreateFromSource(recomendation);

                    unitOfWork.Commit();

                    _businessEventPublisher.PublishBusinessEvent(new ChallengeRequestedEvent
                    {
                        ResourceId = recomendation.ResourceId,
                        FilesCount = nextStepData.Files.Length,
                        FileIds = nextStepData.Files,
                        RecommendationId = nextStepData.RecomendationId,
                        Deadline = nextStepData.Deadline
                    });

                    transaction.Complete();

                    return recommendationDto;
                }
            }
        }


    }
}