﻿using System.Linq;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.Services
{
    internal class NewRecommendationActivityService : IActivityLogEntryService
    {
        private readonly IUnitOfWork<ICustomerPortalDbScope> _unitOfWork;
        private readonly ITimeService _timeService;

        public NewRecommendationActivityService(IUnitOfWork<ICustomerPortalDbScope> unitOfWork, ITimeService timeService)
        {
            _unitOfWork = unitOfWork;
            _timeService = timeService;
        }

        public void RegisterActivity(long entityId)
        {
            _unitOfWork.Repositories.ActivityLogs.Add(new ActivityLog
            {
                ActivityType = ActivityLogType.NewRecommendation,
                RecommendationId = entityId
            });
        }

        public void ClearActivity(long entityId)
        {
            var elements = _unitOfWork
                .Repositories
                .ActivityLogs
                .Where(d => d.RecommendationId == entityId && d.ActivityType == ActivityLogType.NewRecommendation)
                .ToList();

            var currentDateTime = _timeService.GetCurrentTime();

            foreach (var activityLog in elements)
            {
                activityLog.SeenByUserOn = currentDateTime;
            }
        }
    }
}
