﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.Services
{
    public class MainTechnologyOptionsCardIndexDataService : ReadOnlyCardIndexDataService<MainTechnologyOptionsDto, MainTechnologyOptions, ICustomerPortalDbScope>, IMainTechnologyOptionsCardIndexDataService
    {
        public MainTechnologyOptionsCardIndexDataService(ICardIndexServiceDependencies<ICustomerPortalDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<MainTechnologyOptions> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var direction = sortingCriterion.GetSortingDirection();

            if (sortingCriterion.GetSortingColumnName() == nameof(MainTechnologyOptionsDto.DisplayValue))
            {
                orderedQueryBuilder.ApplySortingKey(r => r.DisplayValue, direction);
            }
            else
            {
                base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
            }
        }

        protected override IEnumerable<Expression<Func<MainTechnologyOptions, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return r => r.DisplayValue;
        }
    }
}
