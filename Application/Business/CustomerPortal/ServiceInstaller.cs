﻿using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.Business.CustomerPortal.Services;
using Smt.Atomic.Business.EventSourcing.Interfaces;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.CustomerPortal
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            if (containerType == ContainerType.WebApp || containerType == ContainerType.WebApi || containerType == ContainerType.JobScheduler)
            {
                container.Register(Component.For<IRecommendationCardIndexDataService>().ImplementedBy<RecommendationCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IDocumentCardIndexDataService>().ImplementedBy<DocumentCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IRecommendationEmailMessageFactory>().ImplementedBy<RecommendationEmailMessageFactory>().LifestyleTransient());
                container.Register(Component.For<IRecommendationStateCardIndexDataService>().ImplementedBy<RecommendationStateCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<INeededResourceCardIndexDataService>().ImplementedBy<NeededResourceCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IQuestionCardIndexDataService>().ImplementedBy<QuestionCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<INoteCardIndexDataService>().ImplementedBy<NoteCardIndexDataService>().LifestyleTransient());

                container.Register(
                    Component.For<IRecomendationScheduleAppointmentsService>()
                        .ImplementedBy<RecommendationScheduleAppointmentsService>()
                        .LifestyleTransient());

                container.Register(
                    Component.For<IChallengeFileService>()
                        .ImplementedBy<ChallengeFileService>()
                        .LifestyleTransient());

                container.Register(
                    Component.For<IResourceFormDataService>()
                        .ImplementedBy<ResourceFormDataService>()
                        .LifestyleTransient());

                container.Register(
                    Component.For<IScheduleAppointmentCardIndexDataService>()
                        .ImplementedBy<ScheduleAppointmentCardIndexDataService>()
                        .LifestyleTransient());

                container.Register(
                    Component.For<IClientUserCardIndexDataService>()
                    .ImplementedBy<ClientUserCardIndexDataService>()
                    .LifestyleTransient());

                container.Register(
                    Classes.FromThisAssembly()
                    .BasedOn<IBusinessEventHandler>()
                    .WithService
                    .FromInterface()
                    .LifestyleTransient());

                container.Register(
                    Component.For<ISeniorityLevelCardIndexDataService>()
                    .ImplementedBy<SeniorityLevelCardIndexDataService>()
                    .LifestyleTransient());

                container.Register(Component.For<IMainTechnologyOptionsCardIndexDataService>()
                    .ImplementedBy<MainTechnologyOptionsCardIndexDataService>()
                    .LifestyleTransient());

                container.Register(Component.For<IRegionOptionsCardIndexDataService>()
                    .ImplementedBy<RegionOptionsCardIndexDataService>()
                    .LifestyleTransient());

                container.Register(Component.For<IBusinesUnitOptionsCardIndexDataService>()
                    .ImplementedBy<BusinesUnitOptionsCardIndexDataService>()
                    .LifestyleTransient());

                container.Register(Component.For<IClientDataCardIndexDataService>()
                    .ImplementedBy<ClientDataCardIndexDataService>()
                    .LifestyleTransient());

                container.Register(Component.For<IClientContactCardIndexDataService>()
                    .ImplementedBy<ClientContactCardIndexDataService>()
                    .LifestyleTransient());

                container.Register(Component.For<IChallengeCardIndexDataService>()
                    .ImplementedBy<ChallengeCardIndexDataService>()
                    .LifestyleTransient());


                container.Register(Component.For<IActivityLogEntryService>()
                    .ImplementedBy<NewRecommendationActivityService>()
                    .Named("NewRecommendationActivityService")
                    .LifestyleTransient());

                container.Register(Component.For<IActivityLogEntryService>()
                    .ImplementedBy<ResourceFormApprovedActivityService>()
                    .Named("ResourceFormApprovedActivityService")
                    .LifestyleTransient());

                container.Register(Component.For<IActivityLogEntryService>()
                    .ImplementedBy<ResourceFormQuestionActivityService>()
                    .Named("ResourceFormQuestionActivityService")
                    .LifestyleTransient());

                container.Register(Component.For<IActivityLogEntryService>()
                    .ImplementedBy<AppointmentScheduledActivityService>()
                    .Named("AppointmentScheduledActivityService")
                    .LifestyleTransient());

                container.Register(Component.For<IActivityLogEntryService>()
                    .ImplementedBy<ChallengeAnsweredActivityService>()
                    .Named("ChallengeAnsweredActivityService")
                    .LifestyleTransient()); 

                container.Register(Component.For<IActivityLogService>().AsFactory().LifestyleTransient());
            }
        }
    }
}











