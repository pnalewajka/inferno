﻿namespace Smt.Atomic.Business.CustomerPortal.Helpers
{
    public static class NeededResourceNameHelper
    {
        public static string PrepareNeededResourceNiceName(string neededResourceName)
        {
            var neededResourceNameElements = neededResourceName.Split('/');
            if (neededResourceNameElements.Length == 5)
            {
                return neededResourceNameElements[1].Trim() + " / " + neededResourceNameElements[2].Trim();
            }
            return neededResourceName;
        }
    }
}
