﻿using Smt.Atomic.Business.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using System.Linq.Expressions;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;

namespace Smt.Atomic.Business.CustomerPortal.Jobs
{
    [Identifier("Job.CustomerPortal.ChallengeDeadlineReminderJob")]

    [JobDefinition(
    Code = "ChallengeDeadlineReminderTARecommendation",
    Description = "Remind about challenge deadline to OM",
    ScheduleSettings = "0 0 9 * * *",
    MaxExecutioPeriodInSeconds = 600,
    PipelineName = PipelineCodes.CustomerPortal)]
    public class ChallengeDeadlineReminderJob : IJob
    {
        private readonly IClientDataCardIndexDataService _clientDataCardIndexDataService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly ITimeService _timeService;
        private readonly IUnitOfWorkService<ICustomerPortalDbScope> _unitOfWorkService;

        public ChallengeDeadlineReminderJob(
            ITimeService timeService,
            IMessageTemplateService messageTemplateService,
            IReliableEmailService reliableEmailService,
            IClientDataCardIndexDataService clientDataCardIndexDataService,
            ISystemParameterService systemParameterService,
            IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService)
        {
            _timeService = timeService;
            _clientDataCardIndexDataService = clientDataCardIndexDataService;
            _reliableEmailService = reliableEmailService;
            _messageTemplateService = messageTemplateService;
            _systemParameterService = systemParameterService;
            _unitOfWorkService = unitOfWorkService;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var tommorowStart = _timeService.GetCurrentDate().AddDays(1);
                var tommorowEnd = tommorowStart.AddDays(1).AddMilliseconds(-1);
                Expression<Func<Challenge, bool>> isDeadlineTommorow = challenge => challenge.Deadline > tommorowStart && challenge.Deadline < tommorowEnd;

                var challenges = unitOfWork.Repositories.Challenges
                    .Include(c => c.Recommendation.NeededResource)
                    .Where(isDeadlineTommorow).Where(c => !c.AnswerDocuments.Any());
                foreach (var challenge in challenges)
                {
                    var operationManager = _clientDataCardIndexDataService.GetAccountManagerForClient(challenge.Recommendation.UserId.Value);
                    var challengeReminderEmailDto = new ChallengeReminderEmailDto
                    {
                        ResourceName = challenge.Recommendation.Name,
                        NeededResourceName = challenge.Recommendation.NeededResource.Name,
                        Deadline = challenge.Deadline,
                        ChallengeUrl = GetChallengeUrl(challenge.RecommendationId, challenge.Recommendation.NeededResourceId.Value),
                        FooterHtml = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalNotificationsFooter, null).Content
                    };

                    var body = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalChallengeDeadlineBody, challengeReminderEmailDto);
                    var subject = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalChallengeDeadlineSubject, challengeReminderEmailDto);

                    _reliableEmailService.EnqueueMessage(new EmailMessageDto
                    {
                        Body = body.Content,
                        Subject = subject.Content,
                        IsHtml = body.IsHtml,
                        Recipients = new[] { new EmailRecipientDto(operationManager.Email, operationManager.Name) }.ToList()
                    });
                }
            }

            return JobResult.Success();
        }

        private string GetChallengeUrl(long recommendationId, long neededResourceId)
        {
            var serverHost = _systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);
            var scheduleAppointmentUrlFormat = _systemParameterService.GetParameter<string>(ParameterKeys.CustomerPortalChallengeViewUrlFormat);
            var recommendationUrl = string.Format(scheduleAppointmentUrlFormat, serverHost, recommendationId, neededResourceId);

            return recommendationUrl;
        }
    }
}
