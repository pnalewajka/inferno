﻿using System.Linq;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Crm.Interfaces;

namespace Smt.Atomic.Business.CustomerPortal.Jobs
{
    [Identifier("Job.CustomerPortal.ScheduleCandidateContactAlert")]
    [JobDefinition(
        Code = "ScheduleCandidateContactAlert",
        Description = "Schedule again contact with candidate email alert",
        ScheduleSettings = "0 0 4 * * *",
        MaxExecutioPeriodInSeconds = 600,
        PipelineName = PipelineCodes.CustomerPortal)]
    public class ScheduleCandidateContactAlertJob : IJob
    {
        private readonly ICrmService _crmService;
        private readonly ITimeService _timeService;

        public ScheduleCandidateContactAlertJob(ICrmService crmService,
            ITimeService timeService)
        {
            _crmService = crmService;
            _timeService = timeService;
        }
        public JobResult DoJob(JobExecutionContext executionContext)
        {
            var candidateIds = _crmService.GetGuidsOfCandidatesToContactOn(_timeService.GetCurrentDate());

            if (candidateIds.Any())
            {
                _crmService.UpdateCandidateContactDateAlert(candidateIds);
            }

            return JobResult.Success();
        }
    }
}
