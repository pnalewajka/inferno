﻿using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Crm.Interfaces;

namespace Smt.Atomic.Business.CustomerPortal.Jobs
{
    [Identifier("Job.CustomerPortal.ReminderAboutOutstandingRecommendationRecruitmentActivities")]
    [JobDefinition(
        Code = "ReminderAboutOutstandingRecommendationRecruitmentActivities",
        Description = "Reminder about outstanding recommendation recruitment activities",
        ScheduleSettings = "0 0 4 * * *",
        MaxExecutioPeriodInSeconds = 600,
        PipelineName = PipelineCodes.CustomerPortal)]
    public class ReminderAboutOutstandingRecommendationRecruitmentActivities : IJob
    {
        private readonly ICrmRecruitmentActivityService _crmRecruitmentActivityService;
        private readonly ITimeService _timeService;

        public ReminderAboutOutstandingRecommendationRecruitmentActivities(ICrmRecruitmentActivityService crmRecruitmentActivityService,
            ITimeService timeService)
        {
            _crmRecruitmentActivityService = crmRecruitmentActivityService;
            _timeService = timeService;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            _crmRecruitmentActivityService.TriggerEmailAlertToCreateReminderForOwnerRA();

            return JobResult.Success();
        }
    }
}