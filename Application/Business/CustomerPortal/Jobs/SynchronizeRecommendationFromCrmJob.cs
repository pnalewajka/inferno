﻿using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.Business.CustomerPortal.BusinessEvents;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using System.Linq;

namespace Smt.Atomic.Business.CustomerPortal.Jobs
{
    [Identifier("Job.CustomerPortal.SynchronizeRecommendationFromCrm")]
    [JobDefinition(
        Code = "SynchronizeRecommendationFromCrm",
        Description = "Processes TA Customer Portal recommendation sent from the CRM",
        ScheduleSettings = "0 0 0 * * *",
        MaxExecutioPeriodInSeconds = 600,
        PipelineName = PipelineCodes.CustomerPortal)]
    public class SynchronizeRecommendationFromCrmJob: IJob
    {
        private readonly IBusinessEventPublisher _businessEventPublisher;
        private readonly IUnitOfWorkService<ICustomerPortalDbScope> _unitOfWorkService;
        public SynchronizeRecommendationFromCrmJob(IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService, IBusinessEventPublisher businessEventPublisher)
        {
            _unitOfWorkService = unitOfWorkService;
            _businessEventPublisher = businessEventPublisher;
        }
        public JobResult DoJob(JobExecutionContext executionContext)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var clientDataIds = unitOfWork.Repositories.ClientDatas.Select(c => c.Id).ToList();

                foreach(var clientDataId in clientDataIds)
                {
                    _businessEventPublisher.PublishBusinessEvent(new TriggerCrmClientSynchronizationEvent { ClientDataId = clientDataId });
                }
            }

            return JobResult.Success();
        }
    }
}
