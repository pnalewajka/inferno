using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using Castle.Core.Logging;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.Data.Crm.Interfaces;

namespace Smt.Atomic.Business.CustomerPortal.Jobs
{
    [Identifier("Job.CustomerPortal.RecommendationQuestionJob")]

    [JobDefinition(
        Code = "AskAQuestionTARecommendation",
        Description = "Asks a question about recommended candidate",
        ScheduleSettings = "15,45 * * * * *",
        MaxExecutioPeriodInSeconds = 600,
        PipelineName = PipelineCodes.CustomerPortal)]
    public class RecommendationQuestionJob : IJob
    {
        private readonly ILogger _logger;
        private readonly ICrmService _crmService;
        private readonly IUserService _userService;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly IQuestionCardIndexDataService _questionService;
        private readonly IRecommendationCardIndexDataService _recommendationService;
        private readonly IRecommendationEmailMessageFactory _recommendationEmailMessageFactory;

        public RecommendationQuestionJob(
            ILogger logger,
            ICrmService crmService,
            IUserService userService,
            IReliableEmailService reliableEmailService,
            IQuestionCardIndexDataService questionService,
            IRecommendationCardIndexDataService recommendationService,
            IRecommendationEmailMessageFactory recommendationEmailMessageFactory)
        {
            _logger = logger;
            _crmService = crmService;
            _userService = userService;
            _reliableEmailService = reliableEmailService;
            _questionService = questionService;
            _recommendationService = recommendationService;
            _recommendationEmailMessageFactory = recommendationEmailMessageFactory;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            try
            {
                _logger.Info("Collecting a list of question records ready to send.");
                var questions = _questionService.GetReadyToSendRecords();
                if (!questions.Any())
                {
                    _logger.Info("There is no questions which could be send.");
                    return JobResult.Success();
                }

                _logger.Info("Preparing recommendation data.");
                var recommendationsSet = PrepareRecommendationData(questions);

                _logger.Info("Updating note in CRM.");
                UpdateNoteInCrm(questions, recommendationsSet);
                
                _logger.Info("Sending an email to SDM about question asked by client.");
                questions.ForEach(q => SendEmail(recommendationsSet[q.RecommendationId], q));

                _logger.Info("Changing question status to sent.");
                UpdateQuestionWasSentFlag(questions.Where(q => q.WasSent).ToArray());
                
                _logger.Info("Recommendation questions processed successfully.");
                return JobResult.Success();
            }
            catch (Exception ex)
            {
                _logger.Error("Error during executing RecommendationQuestionJob.", ex);
                return JobResult.Fail();
            }
        }

        private void UpdateQuestionWasSentFlag(IReadOnlyCollection<QuestionDto> questions)
        {
            if (!questions.Any())
            {
                return;
            }

            var questionIds = questions.Select(q => q.Id).ToArray();

            try
            {
                _questionService.MarkAsSendRecords(questionIds);
            }
            catch (Exception ex)
            {
                _logger.Error($"Updating WasSent flag in questions table failed (ids: {string.Join(",", questionIds)}).", ex);
                throw;
            }

            _logger.Info($"Updating WasSent flag in questions table completed successfully (ids: {string.Join(", ", questionIds)}).");
        }

        private IDictionary<long, RecommendationDto> PrepareRecommendationData(IReadOnlyCollection<QuestionDto> questions)
        {
            if (questions == null)
            {
                throw new ArgumentNullException(nameof(questions));
            }

            var recommendationIds = questions.Select(q => q.RecommendationId).Distinct().ToArray();
            var recommendations = _recommendationService.GetRecordsByIds(recommendationIds);
            var recommendationPairs = recommendations.Values.ToDictionary(key => key.Id, val => val);
            return recommendationPairs;
        }

        private void UpdateNoteInCrm(IReadOnlyCollection<QuestionDto> questions, IDictionary<long, RecommendationDto> recommendationPairs)
        {
            if (!recommendationPairs.Any())
            {
                return;
            }

            var userIds = questions.Select(q => q.UserId).Distinct().ToArray();
            var userNamePairs = _userService.GetUsersByIds(userIds).ToDictionary(key => key.Id, val => val.GetFullName());

            foreach (var question in questions)
            {
                var resourceId = recommendationPairs[question.RecommendationId].ResourceId;

                try
                {
                    var userFullName = userNamePairs[question.UserId];
                    var subject = $"Question from customer ({userFullName})";
                    var body = question.Query;
                    
                    _crmService.AddRecruitmentActivityNote(resourceId, subject, body);
                }
                catch (CrmException ex)
                {
                    _logger.Error($"Updating note in CRM failed (RecruitmentActivityId = {resourceId}).", ex);
                }
            }

            _logger.Info("Notes in CRM updated");
        }

        private void SendEmail(RecommendationDto recommendation, QuestionDto question)
        {
            try
            {
                var emailTask = _recommendationEmailMessageFactory.CreateRecommendationQuestionEmailMessageAsync(recommendation, question);
                emailTask.Wait();

                var emailDto = emailTask.Result;
                _reliableEmailService.EnqueueMessage(emailDto);
                question.WasSent = true;
            }
            catch (AggregateException aex)
            {
                _logger.Error($"Creating an email with question about recommended candidate (RecommendationId={recommendation.Id}) failed.", aex.InnerException);
            }
            catch (Exception ex)
            {
                _logger.Error($"Sending an email with question about recommended candidate (RecommendationId={recommendation.Id}) failed.", ex);
            }
        }
    }
}