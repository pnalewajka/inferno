﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.Jobs
{
    [Identifier("Job.CustomerPortal.ResourceInterviewReviewReminderJob")]
    [JobDefinition(
        Code = "ResourceInterviewReviewReminderJob",
        Description = "Notify customers about pending interview reviews",
        ScheduleSettings = "0 0 6 * * *",
        MaxExecutioPeriodInSeconds = 1800,
        PipelineName = PipelineCodes.CustomerPortal)]
    public class ResourceInterviewReviewReminderJob : IJob
    {
        private readonly ITimeService _timeService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IReliableEmailService _emailService;
        private readonly IUnitOfWorkService<ICustomerPortalDbScope> _unitOfWorkService;

        public ResourceInterviewReviewReminderJob(
            IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService,
            ITimeService timeService,
            ISystemParameterService systemParameterService,
            IMessageTemplateService messageTemplateService,
            IReliableEmailService emailService)
        {
            _unitOfWorkService = unitOfWorkService;
            _timeService = timeService;
            _systemParameterService = systemParameterService;
            _messageTemplateService = messageTemplateService;
            _emailService = emailService;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var currentDateTime = _timeService.GetCurrentTime();

                var pendingRecommendationAppointments = unitOfWork.Repositories.ScheduleAppointments
                    .Where(
                        sa =>
                            sa.AppointmentStatus == AppointmentStatusType.Accepted &&
                            currentDateTime > sa.AppointmentEndDate)
                    .Where(sa => sa.Recommendation.Status == RecommendationStatus.Invited)
                    .ToList();

                foreach (var appointment in pendingRecommendationAppointments)
                {
                    if (!RecomendationExecutedToday(appointment) && RecomendationNeedToExecute(appointment) && appointment.Recommendation.UserId.HasValue)
                    {
                        appointment.ReminderTriggered = _timeService.GetCurrentTime();

                        var clientUser = unitOfWork.Repositories.Users.GetById(appointment.Recommendation.UserId.Value);
                        var frontEndUrl = _systemParameterService.GetParameter<string>(ParameterKeys.CustomerPortalFrontEndUrl);

                        var emailDto = new AppointmentReviewReminderEmailDto
                        {
                            ResourceName = appointment.Recommendation.Name,
                            CustomerName = clientUser.FullName,
                            CandidateDetailsUrl = $"{frontEndUrl}/candidates/{appointment.Recommendation.NeededResourceId}/details/{appointment.Recommendation.Id}",
                            FooterHtml = _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalNotificationsFooter, null).Content
                        };

                        var subject =
                            _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalAppointmentReviewReminderSubject,
                                emailDto);
                        var body =
                            _messageTemplateService.ResolveTemplate(TemplateCodes.CustomerPortalAppointmentReviewReminderBody,
                                emailDto);

                        _emailService.EnqueueMessage(new EmailMessageDto
                        {
                            Recipients = new List<EmailRecipientDto>
                            {
                                new EmailRecipientDto { EmailAddress = clientUser.Email, FullName = clientUser.FullName, Type = RecipientType.To }
                            },
                            Subject = subject.Content,
                            Body = body.Content,
                            IsHtml = body.IsHtml
                        });

                        unitOfWork.Commit();
                    }
                }
            }

            return JobResult.Success();
        }

        private bool RecomendationExecutedToday(ScheduleAppointment appointment)
        {
            var currentDate = _timeService.GetCurrentDate().Date;
            return appointment.ReminderTriggered.HasValue && currentDate == appointment.ReminderTriggered.Value.Date;
        }

        private bool RecomendationNeedToExecute(ScheduleAppointment appointment)
        {
            var currentDate = _timeService.GetCurrentDate().Date;
            return appointment.AppointmentEndDate.AddDays(1).Date == currentDate
                || appointment.AppointmentEndDate.AddDays(2).Date == currentDate;
        }
    }
}