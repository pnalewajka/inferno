﻿using System;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class NeededResourceDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public int RecommendationsCount { get; set; }

        public int RejectedRecommendationsCount { get; set; }

        public int InvitedRecommendationsCount { get; set; }

        public int ApprovedRecommendationsCount { get; set; }

        public Guid CrmId { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public bool IsOnHold { get; set; }

        public bool HasAppointments { get; set; }

        public bool HasChallenges { get; set; }

        public long? ResourceFormId { get; set; }
    }
}
