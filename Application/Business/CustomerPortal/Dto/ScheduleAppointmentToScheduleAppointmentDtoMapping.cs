﻿using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class ScheduleAppointmentToScheduleAppointmentDtoMapping : ClassMapping<ScheduleAppointment, ScheduleAppointmentDto>
    {
        public ScheduleAppointmentToScheduleAppointmentDtoMapping()
        {
            Mapping = e => new ScheduleAppointmentDto
            {
                Id = e.Id,
                RecomendationId = e.RecommendationId,
                RecomendationName = e.Recommendation.Name,
                NeededResourceName = e.Recommendation.NeededResource.Name,
                NeededResourceId = e.Recommendation.NeededResource.Id,
                AppointmentDate = e.AppointmentDate,
                AppointmentEndDate = e.AppointmentEndDate,
                AppointmentStatus = e.AppointmentStatus,
                InterviewType = e.InterviewType,
                RecommendationId = e.RecommendationId,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
