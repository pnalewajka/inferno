﻿using System;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class ClientDataDto
    {
        public long Id { get; set; }

        public long ClientUserId { get; set; }

        public long ManagerUserId { get; set; }

        public long ClientContactId { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public string ContactPersonPhone { get; set; }
    }
}
