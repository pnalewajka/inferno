﻿namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class AppointmentReviewReminderEmailDto
    {
        public string ResourceName { get; set; }

        public string CustomerName { get; set; }

        public string CandidateDetailsUrl { get; set; }

        public string FooterHtml { get; set; }
    }
}