﻿using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using System.Linq;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class NeededResourceToNeededResourceDtoMapping : ClassMapping<NeededResource, NeededResourceDto>
    {
        public NeededResourceToNeededResourceDtoMapping()
        {
            Mapping = e => new NeededResourceDto
            {
                Name = e.Name,
                Id = e.Id,
                CrmId = e.CrmId,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp,
                IsOnHold = e.IsOnHold,
                HasAppointments = e.Recommendations.Any(
                    r => r.ScheduleAppointments.Any(
                        a => a.AppointmentStatus == AppointmentStatusType.Pending)),
                HasChallenges = e.Recommendations.Any(
                    r => r.Challenges.Any(
                        c => !c.AnswerDocuments.Any())),
                ResourceFormId = e.ResourceFormId,
                RecommendationsCount =
                    e.Recommendations.Count(
                        r =>
                            r.Status != RecommendationStatus.None && r.Status != RecommendationStatus.Withdrawn &&
                            r.Status != RecommendationStatus.Archived),
                RejectedRecommendationsCount = e.Recommendations.Count(r => r.Status == RecommendationStatus.Rejected),
                InvitedRecommendationsCount = e.Recommendations.Count(r => r.Status == RecommendationStatus.Invited),
                ApprovedRecommendationsCount = e.Recommendations.Count(r => r.Status == RecommendationStatus.Approved),
            };
        }
    }
}
