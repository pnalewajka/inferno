﻿using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public interface IRecomendationSchedule
    {
        long RecomendationId { get; }

        string Note { get; }

        InterviewType InterviewType { get;}

        ISuggestedDate[] SuggestedDates { get; }
    }
}

