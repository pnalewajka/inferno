﻿namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class NeededResourceStatusChanged
    {
        public string NeededResource { get; set; }
        public string CustomerName { get; set; }
        public string Reason { get; set; }
        public string FooterHtml { get; set; }
    }
}
