﻿using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using System.Linq;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class ChallengeToChallengeDtoMapping : ClassMapping<Challenge, ChallengeDto>
    {
        public ChallengeToChallengeDtoMapping()
        {
            Mapping = e => new ChallengeDto
            {
                Id = e.Id,
                Name = e.Recommendation.Name,
                RecommendationId = e.RecommendationId,
                ChallengeDocuments = e.ChallengeDocuments == null ? null : e.ChallengeDocuments.Select(c => new Common.Dto.DocumentDto
                {
                    ContentType = c.ContentType,
                    DocumentId = c.Id,
                    DocumentName = c.Name
                }).ToArray(),
                AnswerDocuments = e.AnswerDocuments == null ? null : e.AnswerDocuments.Select(c => new Common.Dto.DocumentDto
                {
                    ContentType = c.ContentType,
                    DocumentId = c.Id,
                    DocumentName = c.Name
                }).ToArray(),
                ChallengeComment = e.ChallengeComment,
                AnswerComment = e.AnswerComment,
                Deadline = e.Deadline,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
