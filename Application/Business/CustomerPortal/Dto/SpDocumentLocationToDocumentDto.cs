﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Crm.CrmModel;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class SpDocumentLocationToDocumentDto : ClassMapping<SharePointDocumentLocation, DocumentDto>
    {
        public SpDocumentLocationToDocumentDto()
        {
            Mapping = d => new DocumentDto
            {
                Name = d.Name,
                Url = d.AbsoluteURL,
            };
        }
    }
}
