﻿using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class NeededResourceDtoToNeededResourceMapping : ClassMapping<NeededResourceDto, NeededResource>
    {
        public NeededResourceDtoToNeededResourceMapping()
        {
            Mapping = d => new NeededResource
            {
                Name = d.Name,
                Id = d.Id,
                CrmId = d.CrmId,
                Timestamp = d.Timestamp,
                ResourceFormId = d.ResourceFormId
            };
        }
    }
}
