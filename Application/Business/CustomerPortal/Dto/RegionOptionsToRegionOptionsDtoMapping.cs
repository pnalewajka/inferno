﻿using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class RegionOptionsToRegionOptionsDtoMapping : ClassMapping<RegionOptions, RegionOptionsDto>
    {
        public RegionOptionsToRegionOptionsDtoMapping()
        {
            Mapping = e => new RegionOptionsDto
            {
                Id = e.Id,
                CrmId = e.CrmId,
                DisplayValue = e.DisplayValue
            };
        }
    }
}
