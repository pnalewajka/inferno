﻿using System.Linq;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class QuestionEmailDto : EmailDto
    {
        public string[] QuestionLines { get; set; }

        public string CompanyName { get; set; }

        public override bool IsValid
        {
            get
            {
                var isValid = base.IsValid && QuestionLines.Any(line => !string.IsNullOrEmpty(line)) && !string.IsNullOrEmpty(CompanyName); 
                return isValid;
            }
        }
    }
}