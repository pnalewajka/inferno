﻿using System.Collections.Generic;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using System.Linq;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class ResourceFormToResourceFormDtoMapping : ClassMapping<ResourceForm, ResourceFormDto>
    {
        public ResourceFormToResourceFormDtoMapping()
        {
            Mapping = e => new ResourceFormDto
            {
                Id = e.Id,
                CreatedBy = e.CreatedById,
                ResourceName = e.ResourceName,
                ResourceCount = e.ResourceCount,
                PlannedStart = e.PlannedStart,
                ProjectDuration = e.ProjectDuration,
                Location = e.Location,
                ProjectDescription = e.ProjectDescription,
                PositionRequirements = e.PositionRequirements,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp,
                State = e.State,
                AdditionalComment = e.AdditionalComment,
                LanguageComment = e.LanguageComment,
                RecruitmentPhases = e.RecruitmentPhases,
                RoleDescription = e.RoleDescription,
                LanguageOptions = e.LanguageOption == null ?
                new List<LanguageOptionDto>() :
                e.LanguageOption.Select(l => new LanguageOptionDto
                {
                    Id = l.Id,
                    LanguageName = l.LanguageName,
                    LevelName = l.LevelName,
                    LanguageId = l.LanguageId,
                    LevelId = l.LevelId
                }).ToList(),
                NeededResourceCrmCode = e.NeededResourceCrmCode,
                Question = e.Question,
                Files = e.Files == null 
                        ? new Common.Dto.DocumentDto[0] 
                        : e.Files
                            .Select(f => new Common.Dto.DocumentDto { DocumentId = f.Id, ContentType = f.ContentType, DocumentName = f.Name })
                            .ToArray(),
                RequestType = e.RequestType
            };
        }
    }
}
