﻿using System;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public interface IRecomendationChallenge
    {
        long RecomendationId { get; set; }

        string Note { get; set; }

        DateTime Deadline { get; set; }

        Guid[] Files { get; set; }
    }
}