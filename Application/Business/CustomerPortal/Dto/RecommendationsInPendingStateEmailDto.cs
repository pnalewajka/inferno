﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class RecommendationsInPendingStateEmailDto
    {
        public RecommendationsInPendingStateEmailDto()
        {
            RecommendedCandidatesPerNeededResource = new Dictionary<string, IEnumerable<RecommendedCandidateDto>>();
        }

        public string ClientFirstName { get; set; }

        public string ClientName { get; set; }

        public string ClientEmail { get; set; }

        public string SdmName { get; set; }

        public string SdmEmail { get; set; }

        public string FooterHtml { get; set; }

        public Dictionary<string, IEnumerable<RecommendedCandidateDto>> RecommendedCandidatesPerNeededResource { get; set; }

        public bool IsValid =>
            !string.IsNullOrEmpty(ClientFirstName) &&
            !string.IsNullOrEmpty(ClientName) &&
            !string.IsNullOrEmpty(ClientEmail) &&
            !string.IsNullOrEmpty(SdmName) &&
            !string.IsNullOrEmpty(SdmEmail) &&
            CandidatesAreValid();


        private bool CandidatesAreValid()
        {
            return
                RecommendedCandidatesPerNeededResource.Any() &&
                RecommendedCandidatesPerNeededResource.All(
                    rc => !string.IsNullOrEmpty(rc.Key) && rc.Value.All(v => v.IsValid));
        }
    }

    public class RecommendedCandidateDto
    {
        public string CandidateName { get; set; }

        public string ResourceUrl { get; set; }

        public DateTime RecommendationDate { get; set; }        

        public bool IsValid =>
            !string.IsNullOrEmpty(CandidateName) &&
            !string.IsNullOrEmpty(ResourceUrl) &&
            RecommendationDate != default(DateTime);
    }
}