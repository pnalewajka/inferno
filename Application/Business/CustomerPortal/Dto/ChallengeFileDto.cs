﻿using System;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class ChallengeFileDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string ContentType { get; set; }

        public Guid Identifier { get; set; }

        public long ContentLength { get; set; }

        public byte[] Data { get; set; }

        public long? RecommendationId { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
