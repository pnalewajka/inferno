﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class ResourceRequestSubmittedEmailDto
    {
        public string NeededResource { get; set; }
        public string CustomerName { get; set; }
        public string FooterHtml { get; set; }
        public string ResourceName { get; set; }
        public int ResourceCount { get; set; }
        public string AdditionalComment { get; set; }
        public string LanguageComment { get; set; }
        public string RecruitmentPhases { get; set; }
        public string Location { get; set; }
        public string PlannedStart { get; set; }
        public string ProjectDuration { get; set; }
        public string ProjectDescription { get; set; }
        public string PositionRequirements { get; set; }
        public string RoleDescription { get; set; }
        public string RequiredLanguages { get; internal set; }
        public string ReviewFormUrl { get; internal set; }
    }
}
