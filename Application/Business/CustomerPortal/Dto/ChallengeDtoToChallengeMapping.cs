﻿using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using System.Collections.ObjectModel;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class ChallengeDtoToChallengeMapping : ClassMapping<ChallengeDto, Challenge>
    {
        public ChallengeDtoToChallengeMapping()
        {
            Mapping = d => new Challenge
            {
                Id = d.Id,
                RecommendationId = d.RecommendationId,
                Deadline = d.Deadline,
                AnswerComment = d.AnswerComment,
                ChallengeDocuments = new Collection<ChallengeDocument>(),
                AnswerDocuments = new Collection<AnswerDocument>(),
                Timestamp = d.Timestamp
            };
        }
    }
}
