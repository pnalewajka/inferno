﻿namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class DocumentContentDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public byte[] Content { get; set; }
    }
}
