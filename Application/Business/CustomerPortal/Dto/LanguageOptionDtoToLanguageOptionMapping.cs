﻿using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class LanguageOptionDtoToLanguageOptionMapping : ClassMapping<LanguageOptionDto, LanguageOption>
    {
        public LanguageOptionDtoToLanguageOptionMapping()
        {
            Mapping = d => new LanguageOption
            {
                Id = d.Id,
                LevelId = d.LevelId,
                LanguageId = d.LanguageId,
                LevelName = d.LevelName,
                LanguageName = d.LanguageName,
            };
        }
    }
}
