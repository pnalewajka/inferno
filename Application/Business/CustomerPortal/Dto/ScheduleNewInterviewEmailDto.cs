﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class ScheduleNewInterviewEmailDto: EmailDto
    {
        public string Comment { get; set; }
        public string[] Interviews { get; set; }
        public string ApproveUrl { get; set; }
    }
}
