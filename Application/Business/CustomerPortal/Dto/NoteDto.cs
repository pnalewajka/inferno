﻿using System;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class NoteDto
    {
        public long Id { get; set; }

        public string Content { get; set; }

        public bool SavedInCRM { get; set; }

        public long? UserId { get; set; }

        public string UserFullName { get; set; }

        public DateTime CreatedOn { get; set; }

        public long? RecommendationId { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
