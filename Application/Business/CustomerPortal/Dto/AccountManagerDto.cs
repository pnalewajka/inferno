﻿namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class AccountManagerDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}
