﻿using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class ClientContactDtoToClientContactMapping : ClassMapping<ClientContactDto, ClientContact>
    {
        public ClientContactDtoToClientContactMapping()
        {
            Mapping = d => new ClientContact
            {
                Id = d.Id,
                CreatedOn = d.CreatedOn,
                CrmId = d.CrmId,
                Name = d.Name,
                Email = d.Email,
                CompanyName = d.CompanyName,
                CompanyGuidId = d.CompenyGuidId
            };
        }
    }
}
