﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
using System.Collections.Generic;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class ResourceFormDto
    {
        public long Id { get; set; }

        public string ResourceName { get; set; }

        public int ResourceCount { get; set; }

        public string PlannedStart { get; set; }

        public string ProjectDuration { get; set; }

        public string Location { get; set; }

        public string ProjectDescription { get; set; }

        public string PositionRequirements { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public ResourceFormState State { get; set; }

        public DateTime ModifiedOn { get; set; }

        public long CreatedBy { get; set; }

        public string RoleDescription { get; set; }

        public string LanguageComment { get; set; }

        public string RecruitmentPhases { get; set; }

        public string AdditionalComment { get; set; }

        public IEnumerable<LanguageOptionDto> LanguageOptions { get; set; }

        public byte[] Timestamp { get; set; }

        public string Question { get; set; }

        public Guid? NeededResourceCrmCode { get; set; }

        public Common.Dto.DocumentDto[] Files { get; set; }

        public ResourceRequestType RequestType { get; set; }
    }
}
