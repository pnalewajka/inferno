﻿using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class ClientDataDtoToClientDataMapping : ClassMapping<ClientDataDto, ClientData>
    {
        public ClientDataDtoToClientDataMapping()
        {
            Mapping = d => new ClientData
            {
                Id = d.Id,
                ClientUserId = d.ClientUserId,
                ManagerUserId = d.ManagerUserId,
                ClientContactId = d.ClientContactId,
                Timestamp = d.Timestamp,
                ContactPersonPhone = d.ContactPersonPhone
            };
        }
    }
}
