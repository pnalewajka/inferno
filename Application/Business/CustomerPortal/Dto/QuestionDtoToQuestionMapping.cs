using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class QuestionDtoToQuestionMapping : ClassMapping<QuestionDto, Question>
    {
        public QuestionDtoToQuestionMapping()
        {
            Mapping =
                d =>
                    new Question
                    {
                        Id = d.Id,
                        RecommendationId = d.RecommendationId,
                        UserId = d.UserId,
                        Query = d.Query,
                        QueryDate = d.QueryDate,
                        WasSent = d.WasSent,
                        Timestamp = d.Timestamp
                    };
        }
    }
}
