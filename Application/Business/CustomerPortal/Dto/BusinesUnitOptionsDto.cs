﻿using System;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class BusinesUnitOptionsDto
    {
        public long Id { get; set; }
        public int CrmId { get; set; }
        public string DisplayValue { get; set; }
    }
}
