﻿using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class ChallengeFileToChallengeFileDtoMapping : ClassMapping<ChallengeFile, ChallengeFileDto>
    {
        public ChallengeFileToChallengeFileDtoMapping()
        {
            Mapping = e => new ChallengeFileDto
            {
                Id = e.Id,
                Name = e.Name,
                ContentType = e.ContentType,
                Identifier = e.Identifier,
                ContentLength = e.ContentLength,
                Data = e.Data,
                RecommendationId = e.RecommendationId,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
