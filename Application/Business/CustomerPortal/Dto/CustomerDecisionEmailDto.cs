﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class CustomerDecisionEmailDto : EmailDto
    {
        public string CompanyName { get; set; }
        
        public string CrmUrl { get; set; }
        
        public string[] CommentLines { get; set; }

        public bool CommentAvailable
        {
            get
            {
                return CommentLines.Any(line => !string.IsNullOrEmpty(line));
            }
        }

        public override bool IsValid
        {
            get
            {
                var isValid = base.IsValid &&
                              !string.IsNullOrEmpty(CompanyName) &&
                              !string.IsNullOrEmpty(CrmUrl);

                return isValid;
            }
        }
    }
}
