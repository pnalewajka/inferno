﻿using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class ChallengeFileDtoToChallengeFileMapping : ClassMapping<ChallengeFileDto, ChallengeFile>
    {
        public ChallengeFileDtoToChallengeFileMapping()
        {
            Mapping = d => new ChallengeFile
            {
                Id = d.Id,
                Name = d.Name,
                ContentType = d.ContentType,
                Identifier = d.Identifier,
                ContentLength = d.ContentLength,
                Data = d.Data,
                RecommendationId = d.RecommendationId,
                Timestamp = d.Timestamp
            };
        }
    }
}
