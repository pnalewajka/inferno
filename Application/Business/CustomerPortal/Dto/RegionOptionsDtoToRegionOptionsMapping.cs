﻿using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class RegionOptionsDtoToRegionOptionsMapping : ClassMapping<RegionOptionsDto, RegionOptions>
    {
        public RegionOptionsDtoToRegionOptionsMapping()
        {
            Mapping = d => new RegionOptions
            {
                Id = d.Id,
                CrmId = d.CrmId,
                DisplayValue = d.DisplayValue
            };
        }
    }
}
