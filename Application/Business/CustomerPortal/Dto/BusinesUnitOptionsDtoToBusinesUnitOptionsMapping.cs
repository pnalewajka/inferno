﻿using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class BusinesUnitOptionsDtoToBusinesUnitOptionsMapping : ClassMapping<BusinesUnitOptionsDto, BusinesUnitOptions>
    {
        public BusinesUnitOptionsDtoToBusinesUnitOptionsMapping()
        {
            Mapping = d => new BusinesUnitOptions
            {
                Id = d.Id,
                CrmId = d.CrmId,
                DisplayValue = d.DisplayValue
            };
        }
    }
}
