﻿using System;
using System.Runtime.Serialization;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    [DataContract]
    public class DocumentDto
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public long Id { get; set; }

        public long? RecommendationId { get; set; }

        public Guid UniqueId { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
