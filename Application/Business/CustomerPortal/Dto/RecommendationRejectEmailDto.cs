﻿using Smt.Atomic.Business.CustomerPortal.Helpers;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class RecommendationRejectEmailDto
    {
        public string ResourceName { get; set; }
        public string ContactName { get; set; }
        public string CompanyName { get; set; }
        public string NeededResourceName { get; set; }
        public string[] CommentLines { get; set; }
        public string CrmUrl { get; set; }
        public string FooterHtml { get; set; }

        public string NeededResourceShortName
            => NeededResourceNameHelper.PrepareNeededResourceNiceName(NeededResourceName);
    }
}