﻿using Smt.Atomic.CrossCutting.Common.Enums;
using System;
using System.Collections.Generic;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class RecommendationDto
    {
        public Guid ResourceId { get; set; }

        public string Name { get; set; }

        public string Rate { get; set; }

        public string Availability { get; set; }

        public string Note { get; set; }

        public NoteDto[] Notes { get; set; }

        public IEnumerable<Common.Dto.DocumentDto> Documents { get; set; }

        public int? Years { get; set; }

        public long Id { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public long? UserId { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public long? NeededResourceId { get; set; }

        public DateTime CreatedOn { get; set; }

        public string NeededResourceName { get; set; }

        public bool SeenByCustomer { get; set; }

        public bool IsOutside { get; set; }

        public DateTime? ChallengeDeadline { get; set; }

        public int ActiveDaysCount { get; set; }

        public DateTime? NextAppointmentDate { get; set; }

        public bool InterviewCompleted { get; set; }

        public RecommendationStatus Status { get; set; }

        public DateTime? RejectedOn { get; set; }
    }
}
