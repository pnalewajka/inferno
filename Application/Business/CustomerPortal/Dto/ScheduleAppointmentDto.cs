﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class ScheduleAppointmentDto
    {
        public long Id { get; set; }

        public long RecomendationId { get; set; }

        public long NeededResourceId { get; set; }

        public string RecomendationName { get; set; }

        public string NeededResourceName { get; set; }

        public DateTime AppointmentDate { get; set; }

        public DateTime AppointmentEndDate { get; set; }

        public AppointmentStatusType AppointmentStatus { get; set; }

        public InterviewType InterviewType { get; set; }

        public long RecommendationId { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
