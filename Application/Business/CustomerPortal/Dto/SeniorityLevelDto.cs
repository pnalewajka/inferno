﻿namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class SeniorityLevelDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}