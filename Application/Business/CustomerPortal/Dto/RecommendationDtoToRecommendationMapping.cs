using System.Collections.ObjectModel;
using System.Linq;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class RecommendationDtoToRecommendationMapping : ClassMapping<RecommendationDto, Recommendation>
    {
        public RecommendationDtoToRecommendationMapping()
        {
            Mapping = d => new Recommendation
            {
                Id = d.Id,
                ResourceId = d.ResourceId,
                Name = d.Name,
                Rate = d.Rate,
                Availability = d.Availability,
                Note = d.Note,
                Years = d.Years,
                UserId = d.UserId,
                Documents = new Collection<Document>(),
                ImpersonatedById = d.ImpersonatedById,
                ModifiedById = d.ModifiedById,
                ModifiedOn = d.ModifiedOn,
                Timestamp = d.Timestamp,
                NeededResourceId = d.NeededResourceId,
                IsOutside = d.IsOutside,
                SeenByCustomer = d.SeenByCustomer,
                Status = d.Status,
                RejectedOn = d.RejectedOn,
            };
        }
    }
}
