﻿using System;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public interface ISuggestedDate
    {
        DateTime StartsOn { get; set; }

        DateTime EndsOn { get; set; }
    }
}