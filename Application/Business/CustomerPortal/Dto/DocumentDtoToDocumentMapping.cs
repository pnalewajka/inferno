﻿using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class DocumentDtoToDocumentMapping : ClassMapping<DocumentDto, Document>
    {
        public DocumentDtoToDocumentMapping()
        {
            Mapping = d => new Document
            {
                Name = d.Name,
                Url = d.Url,
                UniqueId = d.UniqueId,
                Id = d.Id,
                RecommendationId = d.RecommendationId,
                Timestamp = d.Timestamp
            };
        }
    }
}
