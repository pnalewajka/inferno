using System;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class QuestionDto
    {
        public long Id { get; set; }

        public long RecommendationId { get; set; }

        public long UserId { get; set; }

        public string Query { get; set; }

        public DateTime QueryDate { get; set; }

        public bool WasSent { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
