﻿using Smt.Atomic.Business.CustomerPortal.BusinessEvents;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class ResourceFormDtoToResourceRequestSubmittedEventMapping : ClassMapping<ResourceFormDto, ResourceRequestSubmittedEvent>
    {
        public ResourceFormDtoToResourceRequestSubmittedEventMapping()
        {
            Mapping = model => new ResourceRequestSubmittedEvent
            {
                RequestId = model.Id,
                ResourceName = model.ResourceName,
                PlannedStart = model.PlannedStart,
                PositionRequirements = model.PositionRequirements,
                ProjectDescription = model.ProjectDescription,
                ProjectDuration = model.ProjectDuration,
                ResourceCount = model.ResourceCount,
                Location = model.Location,
                AdditionalComment = model.AdditionalComment,
                LanguageComment = model.LanguageComment,
                RecruitmentPhases = model.RecruitmentPhases,
                RoleDescription = model.RoleDescription
            };
        }
    }
}
