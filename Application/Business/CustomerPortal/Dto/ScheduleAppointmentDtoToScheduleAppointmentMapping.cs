﻿using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class ScheduleAppointmentDtoToScheduleAppointmentMapping : ClassMapping<ScheduleAppointmentDto, ScheduleAppointment>
    {
        public ScheduleAppointmentDtoToScheduleAppointmentMapping()
        {
            Mapping = d => new ScheduleAppointment
            {
                Id = d.Id,
                AppointmentDate = d.AppointmentDate,
                AppointmentEndDate = d.AppointmentEndDate,
                AppointmentStatus = d.AppointmentStatus,
                RecommendationId = d.RecommendationId,
                InterviewType = d.InterviewType,
                Timestamp = d.Timestamp
            };
        }
    }
}
