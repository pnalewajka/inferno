﻿using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class NoteDtoToNoteMapping : ClassMapping<NoteDto, Note>
    {
        public NoteDtoToNoteMapping()
        {
            Mapping = d => new Note
            {
                Id = d.Id,
                Content = d.Content,
                SavedInCRM = d.SavedInCRM,
                UserId = d.UserId,
                CreatedOn = d.CreatedOn,
                RecommendationId = d.RecommendationId,
                Timestamp = d.Timestamp
            };
        }
    }
}
