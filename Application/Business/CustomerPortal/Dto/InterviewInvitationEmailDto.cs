﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class InterviewInvitationEmailDto
    {
        public DateTime StartsOn { get; set; }
        public string Interviewer { get; set; }
        public string Interviewee { get; set; }
        public string OperationManager { get; set; }
        public string NeededResource { get; set; }
        public string CustomerName { get; set; }
        public string Account { get; set; }
        public string FooterHtml { get; set; }
    }
}
