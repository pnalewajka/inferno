﻿namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public interface IRecommendationApproved
    {
        long RecomendationId { get; }

        string Comment { get; }
    }
}
