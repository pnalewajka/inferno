﻿using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class LanguageOptionToLanguageOptionDtoMapping : ClassMapping<LanguageOption, LanguageOptionDto>
    {
        public LanguageOptionToLanguageOptionDtoMapping()
        {
            Mapping = e => new LanguageOptionDto
            {
                Id = e.Id,
                LevelId = e.LevelId,
                LanguageId = e.LanguageId,
                LevelName = e.LevelName,
                LanguageName = e.LanguageName,
            };
        }
    }
}
