﻿using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class ClientDataToClientDataDtoMapping : ClassMapping<ClientData, ClientDataDto>
    {
        public ClientDataToClientDataDtoMapping()
        {
            Mapping = e => new ClientDataDto
            {
                Id = e.Id,
                ClientUserId = e.ClientUserId,
                ManagerUserId = e.ManagerUserId,
                ClientContactId = e.ClientContactId,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp,
                ContactPersonPhone = e.ContactPersonPhone
            };
        }
    }
}
