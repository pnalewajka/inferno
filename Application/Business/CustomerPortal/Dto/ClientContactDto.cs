﻿using System;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class ClientContactDto
    {
        public long Id { get; set; }

        public DateTime CreatedOn { get; set; }

        public Guid CrmId { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public Guid CompenyGuidId { get; set; }

        public string CompanyName { get; set; }
    }
}
