﻿using System;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class LanguageOptionDto
    {
        public long Id { get; set; }

        public int LevelId { get; set; }

        public int LanguageId { get; set; }

        public string LevelName { get; set; }

        public string LanguageName { get; set; }
    }
}
