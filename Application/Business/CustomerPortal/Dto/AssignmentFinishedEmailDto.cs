﻿namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class AssignmentFinishedEmailDto
    {
        public string ResourceName { get; set; }
        public string NeededResourceName { get; set; }
        public string RecommendationUrl { get; set; }
        public string Comment { get; set; }
        public string FooterHtml { get; set; }
    }
}
