﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class RecommendationToRecommendationDtoMapping : ClassMapping<Recommendation, RecommendationDto>
    {
        public RecommendationToRecommendationDtoMapping(IClassMappingFactory classMappingFactory)
        {
            var noteMapping = classMappingFactory.CreateMapping<Note, NoteDto>();
            Mapping = e => new RecommendationDto
            {
                ResourceId = e.ResourceId,
                Name = e.Name,
                Rate = e.Rate,
                Availability = e.Availability,
                Note = e.Note,
                Years = (int?)e.Years,
                Id = e.Id,
                UserId = e.UserId,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp,
                Documents = e.Documents == null ? new List<Common.Dto.DocumentDto>() : e.Documents.Select(d => new Common.Dto.DocumentDto
                {
                    DocumentId = d.Id,
                    DocumentName = d.Name,
                    ContentType = d.ContentType,
                }),
                Notes = e.Notes == null ? new NoteDto[0] : e.Notes.Select(noteMapping.CreateFromSource).ToArray(),
                NeededResourceId = e.NeededResourceId,
                IsOutside = e.IsOutside,
                CreatedOn = e.AddedOn,
                NeededResourceName = e.NeededResource != null ? e.NeededResource.Name : null,
                SeenByCustomer = e.SeenByCustomer,
                Status = e.Status,
                RejectedOn = e.RejectedOn,
                ActiveDaysCount = Math.Max((e.AddedOn.AddDays(30) - DateTime.Now).Days, 0),
                InterviewCompleted = e.ScheduleAppointments != null && e.ScheduleAppointments.Any(a => a.AppointmentStatus == AppointmentStatusType.Accepted && a.AppointmentEndDate < DateTime.Now),
                NextAppointmentDate = e.ScheduleAppointments == null ? null : (e.ScheduleAppointments.Any(a => a.AppointmentStatus == AppointmentStatusType.Accepted) ?
                e.ScheduleAppointments.First(a => a.AppointmentStatus == AppointmentStatusType.Accepted).AppointmentDate :
                (DateTime?)null)
            };
        }
    }
}
