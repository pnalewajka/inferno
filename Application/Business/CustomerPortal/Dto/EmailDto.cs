﻿using Smt.Atomic.Business.CustomerPortal.Helpers;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public abstract class EmailDto
    {
        public string ContactName { get; set; }

        public string ContactFirstName { get; set; }

        public string ContactEmail { get; set; }

        public string NeededResourceName { get; set; }

        public string ResourceName { get; set; }

        public string SdmName { get; set; }

        public string SdmFirstName { get; set; }

        public string SdmEmail { get; set; }

        public string FooterHtml { get; set; }

        public string NeededResourceShortName => NeededResourceNameHelper.PrepareNeededResourceNiceName(NeededResourceName);

        public virtual bool IsValid
        {
            get
            {
                var isValid = !string.IsNullOrEmpty(ContactName) &&
                              !string.IsNullOrEmpty(ContactEmail) &&
                              !string.IsNullOrEmpty(NeededResourceName) &&
                              !string.IsNullOrEmpty(ResourceName) &&
                              !string.IsNullOrEmpty(SdmName) &&
                              !string.IsNullOrEmpty(SdmEmail);

                return isValid;
            }
        }
    }
}
