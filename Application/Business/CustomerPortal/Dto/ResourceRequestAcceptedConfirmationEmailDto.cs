﻿namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class ResourceRequestAcceptedConfirmationEmailDto
    {
        public string CustomerFirstName { get; set; }
        public string FooterHtml { get; set; }
        public string ResourceName { get; set; }
        public string SdmName { get; set; }
    }
}
