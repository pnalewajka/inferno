﻿using System;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class ChallengeRequestedEmailDto
    {
        public string ResourceName { get; set; }
        public string NeededResourceName { get; set; }
        public DateTime Deadline { get; set; }
        public string FooterHtml { get; set; }
        public string RecommendationUrl { get; set; }
    }
}
