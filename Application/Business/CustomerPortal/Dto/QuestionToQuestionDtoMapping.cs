using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class QuestionToQuestionDtoMapping : ClassMapping<Question, QuestionDto>
    {
        public QuestionToQuestionDtoMapping()
        {
            Mapping =
                e =>
                    new QuestionDto
                    {
                        Id = e.Id,
                        RecommendationId = e.RecommendationId,
                        UserId = e.UserId,
                        Query = e.Query,
                        QueryDate = e.QueryDate,
                        WasSent = e.WasSent,
                        ImpersonatedById = e.ImpersonatedById,
                        ModifiedById = e.ModifiedById,
                        ModifiedOn = e.ModifiedOn,
                        Timestamp = e.Timestamp
                    };
        }
    }
}
