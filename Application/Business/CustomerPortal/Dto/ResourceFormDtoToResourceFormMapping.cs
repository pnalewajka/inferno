﻿using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using System.Linq;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class ResourceFormDtoToResourceFormMapping : ClassMapping<ResourceFormDto, ResourceForm>
    {
        public ResourceFormDtoToResourceFormMapping()
        {
            Mapping = d => new ResourceForm
            {
                Id = d.Id,
                ResourceName = d.ResourceName,
                ResourceCount = d.ResourceCount,
                PlannedStart = d.PlannedStart,
                ProjectDuration = d.ProjectDuration,
                Location = d.Location,
                ProjectDescription = d.ProjectDescription,
                PositionRequirements = d.PositionRequirements,
                Timestamp = d.Timestamp,
                AdditionalComment = d.AdditionalComment,
                LanguageComment = d.LanguageComment,
                RecruitmentPhases = d.RecruitmentPhases,
                RoleDescription = d.RoleDescription,
                State = d.State,
                NeededResourceCrmCode = d.NeededResourceCrmCode,
                Question = d.Question,
                RequestType = d.RequestType
            };
        }
    }
}
