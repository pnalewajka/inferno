﻿using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class ActivityLogToActivityLogDtoMapping : ClassMapping<ActivityLog, ActivityLogDto>
    {
        public ActivityLogToActivityLogDtoMapping()
        {
            Mapping = e => new ActivityLogDto
            {
                Id = e.Id,
                ActivityType = e.ActivityType,
                RecommendationId = e.RecommendationId,
                NeededResourceId = e.Recommendation == null ? null : e.Recommendation.NeededResourceId,
                ResourceFormId = e.ResourceFormId,
                RecommendationName = e.Recommendation == null ? string.Empty : e.Recommendation.Name,
                NeededResourceName = e.Recommendation == null || e.Recommendation.NeededResource == null ? string.Empty : e.Recommendation.NeededResource.Name,
                ResourceFormName = e.ResourceForm == null ? string.Empty : e.ResourceForm.ResourceName,
                Date = e.ModifiedOn
            };
        }
    }
}
