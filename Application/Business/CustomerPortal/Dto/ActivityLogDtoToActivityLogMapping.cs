﻿using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class ActivityLogDtoToActivityLogMapping : ClassMapping<ActivityLogDto, ActivityLog>
    {
        public ActivityLogDtoToActivityLogMapping()
        {
            Mapping = d => new ActivityLog
            {
                Id = d.Id,
                ActivityType = d.ActivityType,
                RecommendationId = d.RecommendationId,
                ResourceFormId = d.ResourceFormId,
            };
        }
    }
}
