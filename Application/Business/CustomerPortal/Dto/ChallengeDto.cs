﻿using System;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class ChallengeDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public long RecommendationId { get; set; }

        public DateTime Deadline { get; set; }

        public string ChallengeComment { get; set; }

        public string AnswerComment { get; set; }

        public Common.Dto.DocumentDto[] ChallengeDocuments { get; set; }

        public Common.Dto.DocumentDto[] AnswerDocuments { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
