﻿using System.Linq;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class WithdrawRecommendationEmailDto : EmailDto
    {
        public bool CommentAvailable
        {
            get
            {
                return CommentLines.Any(line => !string.IsNullOrEmpty(line));
            }
        }

        public string[] CommentLines { get; set; }
    }
}
