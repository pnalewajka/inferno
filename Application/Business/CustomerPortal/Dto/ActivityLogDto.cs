﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
using System.Collections;
using System.Collections.Generic;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class ActivityLogDto
    {
        public long Id { get; set; }

        public ActivityLogType ActivityType { get; set; }

        public long? RecommendationId { get; set; }

        public long? NeededResourceId { get; set; }

        public long? ResourceFormId { get; set; }

        public string RecommendationName { get; set; }

        public string NeededResourceName { get; set; }

        public string ResourceFormName { get; set; }

        public DateTime Date { get; set; }
    }
}
