﻿using System;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class ChallengeReminderEmailDto
    {
        public string ResourceName { get; set; }
        public string NeededResourceName { get; set; }
        public DateTime Deadline { get; set; }
        public string ChallengeUrl { get; set; }
        public string FooterHtml { get; set; }
    }
}
