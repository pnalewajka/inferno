﻿using System;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class RegionOptionsDto
    {
        public long Id { get; set; }
        public Guid CrmId { get; set; }
        public string DisplayValue { get; set; }
    }
}
