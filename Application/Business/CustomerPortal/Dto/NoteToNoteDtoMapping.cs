﻿using System;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class NoteToNoteDtoMapping : ClassMapping<Note, NoteDto>
    {
        public NoteToNoteDtoMapping()
        {
            Mapping = e => new NoteDto
            {
                Id = e.Id,
                Content = e.Content,
                SavedInCRM = e.SavedInCRM,
                UserId = e.UserId,
                UserFullName = e.User == null ? string.Empty : e.User.FullName,
                CreatedOn = e.CreatedOn,
                RecommendationId = e.RecommendationId,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
