﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class DashboardItemDto
    {
        public string NeededResourceName { get; set; }

        public long NeededResourceId { get; set; }

        public int RecommendationsCount { get; set; }

        public int RequestedInterviewCount { get; set; }
    }
}
