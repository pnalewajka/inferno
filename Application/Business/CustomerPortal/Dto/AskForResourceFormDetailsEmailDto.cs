﻿namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class AskForResourceFormDetailsEmailDto : EmailDto
    {
        public string ResourceFormName { get; set; }

        public string Question { get; set; }

        public string ResourceFormUrl { get; set; }
    }
}