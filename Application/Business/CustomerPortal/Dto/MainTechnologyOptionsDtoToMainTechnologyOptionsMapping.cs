﻿using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class MainTechnologyOptionsDtoToMainTechnologyOptionsMapping : ClassMapping<MainTechnologyOptionsDto, MainTechnologyOptions>
    {
        public MainTechnologyOptionsDtoToMainTechnologyOptionsMapping()
        {
            Mapping = d => new MainTechnologyOptions
            {
                Id = d.Id,
                CrmId = d.CrmId,
                DisplayValue = d.DisplayValue
            };
        }
    }
}
