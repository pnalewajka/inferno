﻿using System.Linq;
using System.Collections.Generic;

namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class RecommendationEmailDto : EmailDto
    {
        public string ResourceUrl { get; set; }
        
        public string[] CommentLines { get; set; }

        public bool CommentAvailable
        {
            get
            {
                return CommentLines.Any(line => !string.IsNullOrEmpty(line));
            }
        }

        public IEnumerable<DocumentDto> Documents { get; set; }

        public override bool IsValid
        {
            get
            {
                var isValid = base.IsValid &&
                              !string.IsNullOrEmpty(ResourceUrl);
                              
                return isValid;
            }
        }
    }
}