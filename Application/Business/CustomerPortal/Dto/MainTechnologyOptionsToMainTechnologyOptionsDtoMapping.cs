﻿using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class MainTechnologyOptionsToMainTechnologyOptionsDtoMapping : ClassMapping<MainTechnologyOptions, MainTechnologyOptionsDto>
    {
        public MainTechnologyOptionsToMainTechnologyOptionsDtoMapping()
        {
            Mapping = e => new MainTechnologyOptionsDto
            {
                Id = e.Id,
                CrmId = e.CrmId,
                DisplayValue = e.DisplayValue
            };
        }
    }
}
