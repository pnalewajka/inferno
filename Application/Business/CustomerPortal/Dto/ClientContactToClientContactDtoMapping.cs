﻿using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class ClientContactToClientContactDtoMapping : ClassMapping<ClientContact, ClientContactDto>
    {
        public ClientContactToClientContactDtoMapping()
        {
            Mapping = e => new ClientContactDto
            {
                Id = e.Id,
                CreatedOn = e.CreatedOn,
                CrmId = e.CrmId,
                Name = e.Name,
                Email = e.Email,
                CompanyName = e.CompanyName,
                CompenyGuidId = e.CompanyGuidId
            };
        }
    }
}
