﻿using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class DocumentToDocumentDtoMapping : ClassMapping<Document, DocumentDto>
    {
        public DocumentToDocumentDtoMapping()
        {
            Mapping = e => new DocumentDto
            {
                Name = e.Name,
                Url = e.Url,
                Id = e.Id,
                RecommendationId = e.RecommendationId,
                UniqueId = e.UniqueId,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
