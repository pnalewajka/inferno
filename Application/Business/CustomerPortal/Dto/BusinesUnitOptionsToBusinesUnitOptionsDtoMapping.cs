﻿using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.CustomerPortal.Dto
{
    public class BusinesUnitOptionsToBusinesUnitOptionsDtoMapping : ClassMapping<BusinesUnitOptions, BusinesUnitOptionsDto>
    {
        public BusinesUnitOptionsToBusinesUnitOptionsDtoMapping()
        {
            Mapping = e => new BusinesUnitOptionsDto
            {
                Id = e.Id,
                CrmId = e.CrmId,
                DisplayValue = e.DisplayValue
            };
        }
    }
}
