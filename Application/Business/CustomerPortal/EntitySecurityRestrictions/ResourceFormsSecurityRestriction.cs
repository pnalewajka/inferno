﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.CustomerPortal.EntitySecurityRestrictions
{
    public class ResourceFormsSecurityRestriction : EntitySecurityRestriction<ResourceForm>
    {
        public ResourceFormsSecurityRestriction(IPrincipalProvider principalProvider)
            : base(principalProvider)
        {
        }

        public override Expression<Func<ResourceForm, bool>> GetRestriction()
        {
            if (Roles.Contains(SecurityRoleType.CanManageResourceForms))
            {
                return AllRecords();
            }

            return r => r.CreatedById == CurrentPrincipal.Id.Value;
        }
    }
}
