﻿using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.CustomerPortal.EntitySecurityRestrictions
{
    public class ResourceFormFileSecurityRestriction : RelatedEntitySecurityRestriction<ResourceFormFile, ResourceForm>
    {
        public ResourceFormFileSecurityRestriction(IPrincipalProvider principalProvider) 
            : base(principalProvider)
        {
        }

        protected override BusinessLogic<ResourceFormFile, ResourceForm> RelatedEntitySelector 
            => new BusinessLogic<ResourceFormFile, ResourceForm>(r => r.ResourceForm);
    }
}
