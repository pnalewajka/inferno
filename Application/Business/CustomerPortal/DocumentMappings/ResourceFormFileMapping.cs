﻿using Smt.Atomic.Business.Common.Abstracts;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.DocumentMappings
{
    [Identifier("Documents.ResourceFormFile")]
    public class ResourceFormFileMapping : DocumentMapping<ResourceFormFile, ResourceFormFileContent, ICustomerPortalDbScope>
    {
        public ResourceFormFileMapping(IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService) : base(unitOfWorkService)
        {
            NameColumnExpression = f => f.Name;
            ContentTypeColumnExpression = f => f.ContentType;
            ContentLengthColumnExpression = f => f.ContentLength;
            ContentColumnExpression = f => f.Content;
        }
    }
}
