﻿using Smt.Atomic.Business.Common.Abstracts;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.CustomerPortal.DocumentMappings
{
    [Identifier("Documents.AnswerDocumentMapping")]
    public class AnswerDocumentMapping : DocumentMapping<AnswerDocument, AnswerDocumentContent, ICustomerPortalDbScope>
    {
        public AnswerDocumentMapping(IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService)
            : base(unitOfWorkService)
        {
            ContentColumnExpression = o => o.Content;
            NameColumnExpression = o => o.Name;
            ContentTypeColumnExpression = o => o.ContentType;
            ContentLengthColumnExpression = o => o.ContentLength;
        }
    }
}
