﻿using Smt.Atomic.Business.Common.Abstracts;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Data.Entities.Modules.CustomerPortal;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.CustomerPortal.DocumentMappings
{
    [Identifier("Documents.ChallengeDocumentMapping")]
    public class ChallengeDocumentMapping : DocumentMapping<ChallengeDocument, ChallengeDocumentContent, ICustomerPortalDbScope>
    {
        public ChallengeDocumentMapping(IUnitOfWorkService<ICustomerPortalDbScope> unitOfWorkService)
            : base(unitOfWorkService)
        {
            ContentColumnExpression = o => o.Content;
            NameColumnExpression = o => o.Name;
            ContentTypeColumnExpression = o => o.ContentType;
            ContentLengthColumnExpression = o => o.ContentLength;
        }
    }
}
