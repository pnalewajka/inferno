﻿using System;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Resumes;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.Resumes.ChoreProviders
{
    public class OutdatedResumeChoreProvider
        : IChoreProvider
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly ISystemParameterService _systemParameterService;
        private readonly ITimeService _timeService;

        public OutdatedResumeChoreProvider(
            IPrincipalProvider principalProvider,
            ISystemParameterService systemParameterService,
            ITimeService timeService)
        {
            _principalProvider = principalProvider;
            _systemParameterService = systemParameterService;
            _timeService = timeService;
        }

        public IQueryable<ChoreDto> GetActiveChores(IReadOnlyRepositoryFactory repositoryFactory)
        {
            if (!_principalProvider.Current.IsInRole(SecurityRoleType.CanEditMyResume))
            {
                return null;
            }

            var outdatedResumeDaysOffset = _systemParameterService.GetParameter<int>(ParameterKeys.OutdatedResumeDaysOffset);
            var deadlineDate = _timeService.GetCurrentDate().AddDays(-outdatedResumeDaysOffset);

            return repositoryFactory.Create<Employee>().Filtered()
                .Include(e => e.Resumes)
                .Where(e => e.Id == _principalProvider.Current.EmployeeId)
                .Select(e => new { Resumes = e.Resumes.Filtered().AsQueryable() })
                .Select(e => e.Resumes.Any() ? e.Resumes.Max(r => r.ModifiedOn) : (DateTime?)null)
                .Where(d => !d.HasValue || d.Value < deadlineDate)
                .Select(d => new ChoreDto
                {
                    Type = ChoreType.OutdatedResume,
                    DueDate = d,
                    Parameters = null,
                });
        }
    }
}
