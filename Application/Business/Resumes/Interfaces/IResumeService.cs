﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Resumes.Dto;
using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.Data.Entities.Modules.Resumes;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Resumes.Interfaces
{
    public interface IResumeService
    {
        Resume GetEmployeeResume(long employeeId, IUnitOfWork<IResumesDbScope> unitOfWork);

        MyResumeDto GetEmployeeResumeDto(long id);

        IList<IndustryDto> GetIndustries();

        IList<SkillDto> GetSkills();

        IList<EmployeeSkillTagDto> GetEmployeeSkillTags(long employeeId);

        IList<LanguageDto> GetLanguages();

        IList<CompanyDto> GetCompanies();

        IList<ProjectDto> GetProjects(int topProjects, string nameFilter);

        string GetProjectDescription(int projectId);

        void SubmitResume(MyResumeDto myResumeDto);

        string CalculateYearsExperience(ICollection<ResumeCompany> resumeCompanies, CultureInfo culture);

        SuggestedSkillsGroupDto GetEmployeeSuggestedSkillsGroup(long employeeId);

        Guid CreateResumeShareToken(long sharerEmployeeId, long employeeId, string reason);

        long DownloadFromResumeShareToken(Guid token);

        Guid? GetResumeShareToken(long sharerEmployeeId, long ownerEmployeeId);
    }
}
