﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Resumes.Interfaces
{
    public interface IResumeDto
    {
        long Id { get; set; }
    }
}
