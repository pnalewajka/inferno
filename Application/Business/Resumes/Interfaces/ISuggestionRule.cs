﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.Business.Resumes.Dto;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Resumes.Interfaces
{
    public interface ISuggestionRule
    {
        SuggestionRuleResultDto SuggestSkills(long employeeId, IEnumerable<long> allowedSkillsIds);
    }
}
