﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Resumes;

namespace Smt.Atomic.Business.Resumes.Dto
{
    public class ResumeCompanyDtoToResumeCompanyMapping : ClassMapping<ResumeCompanyDto, ResumeCompany>
    {
        public ResumeCompanyDtoToResumeCompanyMapping()
        {
            Mapping = d => new ResumeCompany
            {
                Id = d.Id,
                Name = d.Name,
                From = d.From,
                To = d.To,
                Timestamp = d.Timestamp
            };
        }
    }
}
