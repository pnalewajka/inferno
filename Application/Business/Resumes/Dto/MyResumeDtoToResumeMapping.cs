﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Resumes;

namespace Smt.Atomic.Business.Resumes.Dto
{
    public class MyResumeDtoToResumeMapping : ClassMapping<MyResumeDto, Resume>
    {
        public MyResumeDtoToResumeMapping()
        {
            Mapping = d => new Resume
            {
                Id = d.Id,
                EmployeeId = d.EmployeeId,
                AvailabilityInDays = d.AvailabilityInDays,
                Summary = d.Summary,
                CivoResumeVersionId = d.CivoResumeVersionId,
                Timestamp = d.Timestamp,
                IsConsentClauseForDataProcessingChecked = d.IsConsentClauseForDataProcessingChecked
            };
        }
    }
}
