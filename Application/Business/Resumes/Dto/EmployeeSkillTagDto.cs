﻿namespace Smt.Atomic.Business.Resumes.Dto
{
    public class EmployeeSkillTagDto
    {
        public long SkillTagId { get; set; }

        public string Name { get; set; }

        public long CustomOrder { get; set; }
    }
}
