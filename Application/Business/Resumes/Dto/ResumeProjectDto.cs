﻿using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.SkillManagement.Dto;
using System;
using System.Collections.Generic;

namespace Smt.Atomic.Business.Resumes.Dto
{
    public class ResumeProjectDto
    {
        public long Id { get; set; }

        public DateTime From { get; set; }

        public DateTime? To { get; set; }

        public string Roles { get; set; }

        public string Duties { get; set; }

        public string ProjectDescription { get; set; }

        public bool IsProjectConfidential { get; set; }

        public string Name { get; set; }

        public long? ProjectId { get; set; }

        public ResumeProjectSkillDto[] TechnicalSkills { get; set; }

        public ResumeProjectIndustryDto[] Industries { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
