﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Resumes.Dto
{
    public class ResumeProjectSkillDto
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }
}
