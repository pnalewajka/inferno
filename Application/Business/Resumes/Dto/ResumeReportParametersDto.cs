﻿namespace Smt.Atomic.Business.Resumes.Dto
{
    public class ResumeReportParametersDto
    {
        public long EmployeeId { get; set; }
    }
}
