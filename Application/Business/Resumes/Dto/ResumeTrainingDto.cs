﻿using Smt.Atomic.CrossCutting.Business.Enums;
using System;

namespace Smt.Atomic.Business.Resumes.Dto
{
    public class ResumeTrainingDto : Interfaces.IResumeDto
    {
        public long Id { get; set; }

        public string Title { get; set; }

        public DateTime Date { get; set; }

        public string Institution { get; set; }

        public TrainingType Type { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
