﻿using Smt.Atomic.CrossCutting.Business.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Resumes.Dto
{
    public class ResumeLanguageDto : Interfaces.IResumeDto
    {
        public long Id { get; set; }

        public long LanguageId { get; set; }

        public string LanguageName { get; set; }

        public byte[] Timestamp { get; set; }

        public LanguageReferenceLevel Level { get; set; }
    }
}
