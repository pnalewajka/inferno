﻿using System;
using Smt.Atomic.Data.Entities.Modules.Resumes;
namespace Smt.Atomic.Business.Resumes.Dto
{
    public class ResumeAdditionalSkillDto : Interfaces.IResumeDto
    {
        public long Id { get; set; }

        public long ResumeId { get; set; }

        public Resume Resume { get; set; }

        public string Name { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
