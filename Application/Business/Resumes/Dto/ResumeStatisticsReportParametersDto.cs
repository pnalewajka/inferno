﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Resumes.Dto
{
    public class ResumeStatisticsReportParametersDto
    {
        public long[] LocationIds { get; set; }

        public long[] LineManagerIds { get; set; }

        public long[] OrgUnitIds { get; set; }

        public long[] SkillIds { get; set; }

        public long[] JobProfileIds { get; set; }
    }
}
