﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Resumes.Dto
{
    public class MyResumeDto 
    {
        public long Id { get; set; }

        public long? EmployeeId { get; set; }

        public string EmlpoyeeName { get; set; }

        public int AvailabilityInDays { get; set; }

        public string Summary { get; set; }

        public long? CivoResumeVersionId { get; set; }

        public byte[] Timestamp { get; set; }

        public IList<ResumeTechnicalSkillDto> ResumeTechnicalSkills { get; set; }

        public IList<ResumeCompanyDto> ResumeCompanies { get; set; }

        public IList<ResumeLanguageDto> ResumeLanguages { get; set; }

        public IList<ResumeTrainingDto> ResumeTrainings { get; set; }

        public IList<ResumeEducationDto> ResumeEducations { get; set; }

        public IList<ResumeAdditionalSkillDto> ResumeAdditionalSkills { get; set; }

        public string YearsOfExperience { get; set; }

        public string Position { get; set; }

        public bool IsConsentClauseForDataProcessingChecked { get; set; }

        public IList<long> ResumeRejectedSkillSuggestions { get; set; }
    }
}
