﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Resumes;

namespace Smt.Atomic.Business.Resumes.Dto
{
    public class ResumeProjectDtoToResumeProjectMapping : ClassMapping<ResumeProjectDto, ResumeProject>
    {
        public ResumeProjectDtoToResumeProjectMapping()
        {
            Mapping = d => new ResumeProject
            {
                Id = d.Id,
                Duties = d.Duties,
                From = d.From,
                To = d.To,
                Roles = d.Roles,
                Name = d.Name,
                ProjectId = d.ProjectId,
                ProjectDescription = d.ProjectDescription,
                Timestamp = d.Timestamp
            };
        }
    }
}
