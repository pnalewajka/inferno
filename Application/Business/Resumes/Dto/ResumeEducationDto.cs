﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Resumes.Dto
{
    public class ResumeEducationDto : Interfaces.IResumeDto
    {
        public long Id { get; set; }

        public string UniversityName { get; set; }

        public DateTime From { get; set; }

        public DateTime? To { get; set; }

        public string Degree { get; set; }

        public byte[] Timestamp { get; set; }

        public string Specialization { get; set; }
    }
}
