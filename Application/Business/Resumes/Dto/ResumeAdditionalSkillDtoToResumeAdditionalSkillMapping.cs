﻿using Smt.Atomic.Data.Entities.Modules.Resumes;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Resumes.Dto
{
    public class ResumeAdditionalSkillDtoToResumeAdditionalSkillMapping : ClassMapping<ResumeAdditionalSkillDto, ResumeAdditionalSkill>
    {
        public ResumeAdditionalSkillDtoToResumeAdditionalSkillMapping()
        {
            Mapping = d => new ResumeAdditionalSkill
            {
                Id = d.Id,
                Name = d.Name,
                Timestamp = d.Timestamp
            };
        }
    }
}