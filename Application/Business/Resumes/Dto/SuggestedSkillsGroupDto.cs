﻿using System.Collections.Generic;
using Smt.Atomic.Business.SkillManagement.Dto;

namespace Smt.Atomic.Business.Resumes.Dto
{
    public class SuggestedSkillsGroupDto
    {
        public string SuggestionReason { get; set; }

        public IList<SuggestedSkillDto> SkillSuggestions { get; set; }

        public static SuggestedSkillsGroupDto Empty()
        {
            return new SuggestedSkillsGroupDto
            {
                SuggestionReason = string.Empty,
                SkillSuggestions = new List<SuggestedSkillDto>()
            };
        }
    }
}