﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Resumes.Dto
{
    public class SuggestionRuleResultDto
    {
        public string SuggestionReason { get; set; }

        public IList<long> SuggestedSkills { get; set; }

        public SuggestionRuleResultDto()
        {
            SuggestedSkills = new List<long>();
        }

        public static SuggestionRuleResultDto NoSuggestions()
        {
            return new SuggestionRuleResultDto();
        }
    }
}
