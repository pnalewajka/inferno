﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Resumes;

namespace Smt.Atomic.Business.Resumes.Dto
{
    public class ResumeEducationDtoToResumeEducationMapping : ClassMapping<ResumeEducationDto, ResumeEducation>
    {
        public ResumeEducationDtoToResumeEducationMapping()
        {
            Mapping = d => new ResumeEducation
            {
                Id = d.Id,
                UniversityName = d.UniversityName,
                From = d.From,
                To = d.To,
                Degree = d.Degree,
                Specialization = d.Specialization,
                Timestamp = d.Timestamp
            };
        }
    }
}
