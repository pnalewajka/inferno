﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Resumes;

namespace Smt.Atomic.Business.Resumes.Dto
{
    public class ResumeTrainingDtoToResumeTrainingMapping : ClassMapping<ResumeTrainingDto, ResumeTraining>
    {
        public ResumeTrainingDtoToResumeTrainingMapping()
        {
            Mapping = d => new ResumeTraining
            {
                Id = d.Id,
                Title = d.Title,
                Date = d.Date,
                Institution = d.Institution,
                Type = d.Type,
                Timestamp = d.Timestamp
            };
        }
    }
}
