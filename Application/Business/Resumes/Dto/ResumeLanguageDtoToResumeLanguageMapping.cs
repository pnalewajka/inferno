﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Resumes;

namespace Smt.Atomic.Business.Resumes.Dto
{
    public class ResumeLanguageDtoToResumeLanguageMapping : ClassMapping<ResumeLanguageDto, ResumeLanguage>
    {
        public ResumeLanguageDtoToResumeLanguageMapping()
        {
            Mapping = d => new ResumeLanguage
            {
                Id = d.Id,
                LanguageId = d.LanguageId,
                Level = d.Level,
                Timestamp = d.Timestamp
            };
        }
    }
}
