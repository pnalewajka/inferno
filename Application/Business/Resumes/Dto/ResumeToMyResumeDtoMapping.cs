﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Resumes;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using System;
using System.Linq;

namespace Smt.Atomic.Business.Resumes.Dto
{
    public class ResumeToMyResumeDtoMapping : ClassMapping<Resume, MyResumeDto>
    {
        public ResumeToMyResumeDtoMapping()
        {
            Func<SkillTag, string> getTechnicalSkillTagName = (skillTag) => skillTag == null ? string.Empty : skillTag.Name;

            Mapping = r => new MyResumeDto
            {
                Id = r.Id,
                EmployeeId = r.EmployeeId,
                EmlpoyeeName = $"{r.Employee.FirstName} {r.Employee.LastName}",
                AvailabilityInDays = r.AvailabilityInDays,
                Summary = r.Summary,
                CivoResumeVersionId = r.CivoResumeVersionId,
                Timestamp = r.Timestamp,
                IsConsentClauseForDataProcessingChecked = r.IsConsentClauseForDataProcessingChecked,
                ResumeTechnicalSkills = r.ResumeTechnicalSkills == null ? null : r.ResumeTechnicalSkills.Select(t => new ResumeTechnicalSkillDto
                {
                    Id = t.Id,
                    ExpertiseLevel = t.ExpertiseLevel,
                    ExpertisePeriod = t.ExpertisePeriod,
                    TechnicalSkillId = t.TechnicalSkillId,
                    Name = t.TechnicalSkill.Name,
                    SkillTagName = getTechnicalSkillTagName(t.SkillTag),
                    SkillTagId = t.SkillTagId.HasValue ? t.SkillTagId.Value : 0,
                    Timestamp = t.Timestamp,
                    CreatedOn = t.CreatedOn
                }).ToList(),
                ResumeEducations = r.ResumeEducations == null ? null : r.ResumeEducations.Select(e => new ResumeEducationDto
                {
                    Id = e.Id,
                    Degree = e.Degree,
                    From = e.From,
                    To = e.To,
                    Specialization = e.Specialization,
                    UniversityName = e.UniversityName,
                    Timestamp = e.Timestamp
                }).ToList(),
                ResumeAdditionalSkills = r.ResumeAdditionalSkills == null ? null : r.ResumeAdditionalSkills.Select(a => new ResumeAdditionalSkillDto
                {
                    Id = a.Id,
                    Name = a.Name,
                    Timestamp = a.Timestamp
                }).ToList(),
                ResumeLanguages = r.ResumeLanguages == null ? null : r.ResumeLanguages.Select(l => new ResumeLanguageDto
                {
                    Id = l.Id,
                    LanguageId = l.LanguageId,
                    LanguageName = l.Language.NameEn,
                    Level = l.Level,
                    Timestamp = l.Timestamp
                }).ToList(),
                ResumeTrainings = r.ResumeTrainings == null ? null : r.ResumeTrainings.Select(t => new ResumeTrainingDto
                {
                    Id = t.Id,
                    Title = t.Title,
                    Date = t.Date,
                    Institution = t.Institution,
                    Type = t.Type,
                    Timestamp = t.Timestamp
                }).ToList(),
                ResumeCompanies = r.ResumeCompanies == null ? null : r.ResumeCompanies.Select(c => new ResumeCompanyDto
                {
                    Id = c.Id,
                    Name = c.Name,
                    From = c.From,
                    To = c.To,
                    Timestamp = c.Timestamp,
                    ResumeProjects = c.ResumeProjects == null ? null : c.ResumeProjects.Select(p => new ResumeProjectDto
                    {
                        Id = p.Id,
                        From = p.From,
                        To = p.To,
                        Name = p.Name,
                        ProjectId = p.ProjectId,
                        IsProjectConfidential = p.Project == null ? false : p.Project.ProjectSetup.Disclosures != ProjectDisclosures.WeCanTalkAboutProject,
                        ProjectDescription = p.Project != null && string.IsNullOrWhiteSpace(p.ProjectDescription) ? p.Project.Description : p.ProjectDescription,
                        Roles = p.Roles,
                        Duties = p.Duties,
                        Industries = p.Industries == null ? null : p.Industries.Select(i => new ResumeProjectIndustryDto { Id = i.Id, Name = i.Name }).ToArray(),
                        TechnicalSkills = p.TechnicalSkills == null ? null : p.TechnicalSkills.Select(t => new ResumeProjectSkillDto { Id = t.Id, Name = t.Name }).ToArray(),
                        Timestamp = p.Timestamp
                    }).ToList()
                }).ToList(),
                ResumeRejectedSkillSuggestions = r.ResumeRejectedSkillSuggestion == null ? null : r.ResumeRejectedSkillSuggestion.Select(s => s.Id).ToList()
            };
        }
    }
}
