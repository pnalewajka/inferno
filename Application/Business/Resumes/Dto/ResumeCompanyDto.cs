﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Resumes.Dto
{
    public class ResumeCompanyDto : Interfaces.IResumeDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public DateTime From { get; set; }

        public DateTime? To { get; set; }

        public byte[] Timestamp { get; set; }

        public IList<ResumeProjectDto> ResumeProjects { get; set; }
    }
}
