﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Resumes.Dto
{
    public class ResumeTechnicalSkillDto : Interfaces.IResumeDto
    {
        public long Id { get; set; }

        public long TechnicalSkillId { get; set; }

        public SkillExpertiseLevel ExpertiseLevel { get; set; }

        public SkillExpertisePeriod ExpertisePeriod { get; set; }

        public string Name { get; set; }

        public string SkillTagName { get; set; }

        public long SkillTagId { get; set; }

        public byte[] Timestamp { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
