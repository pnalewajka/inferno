﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Resumes;

namespace Smt.Atomic.Business.Resumes.Dto
{
    public class ResumeTechnicalSkillDtoToResumeTechnicalSkillMapping : ClassMapping<ResumeTechnicalSkillDto, ResumeTechnicalSkill>
    {
        public ResumeTechnicalSkillDtoToResumeTechnicalSkillMapping()
        {
            Mapping = d => new ResumeTechnicalSkill
            {
                Id = d.Id,
                ExpertiseLevel = d.ExpertiseLevel,
                ExpertisePeriod = d.ExpertisePeriod,
                TechnicalSkillId = d.TechnicalSkillId,
                SkillTagId = d.SkillTagId,
                Timestamp = d.Timestamp,
                CreatedOn = d.CreatedOn
            };
        }
    }
}
