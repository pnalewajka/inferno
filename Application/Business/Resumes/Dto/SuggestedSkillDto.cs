﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Resumes.Dto
{
    public class SuggestedSkillDto
    {
        public long SkillId { get; set; }

        public string Name { get; set; }

        public string SkillTagName { get; set; }

        public long SkillTagId { get; set; }

        public SkillExpertiseLevel ExpertiseLevel { get; set; }

        public SkillExpertisePeriod ExpertisePeriod { get; set; }
    }
}