pivot.dataConverter = function (data) {
    return data.ResumeStatistics.map(function (a) {
        return {
            "Organization Unit": a.OrganizationUnit,
            "Employees Count": a.FullyCompletedCount+a.PartiallyCompletedCount+a.NotCompletedCount,
            "Number Fully Completed": a.FullyCompletedCount,
            "Number Partially Completed": a.PartiallyCompletedCount,
            "Number Not Completed": a.NotCompletedCount,
            "Percentage Fully Completed": a.FullyCompletedProportionalWithTotal,
            "Percentage Partially Completed": a.PartiallyCompletedProportionalWithTotal,
            "Percentage Not Completed": a.NotCompletedProportionalWithTotal
       };
    });
};

pivot.configurations = [
    {
        name: "Percentage Fully Completed (Column: Total) by Org Unit",
        code: "perc-complete-orgunit",
        config: {
            "rows":["Organization Unit","Employees Count","Number Fully Completed","Number Partially Completed","Number Not Completed"],
            "vals":["Number Fully Completed","Employees Count"],
            "aggregatorName": "Sum over Sum",
            "rendererName":"Heatmap"
        },
    }
];
