﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Resumes.ChoreProviders;
using Smt.Atomic.Business.Resumes.Interfaces;
using Smt.Atomic.Business.Resumes.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.Resumes
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            if (containerType == ContainerType.WebApp)
            {
                container.Register(Component.For<ISuggestionRule>().ImplementedBy<NewSkillSuggestionRule>().LifestyleTransient());
                container.Register(Component.For<ISuggestionRule>().ImplementedBy<ProjectColleaguesSuggestionRule>().LifestyleTransient());
                container.Register(Component.For<IResumeService>().ImplementedBy<ResumeService>().LifestyleTransient());
                container.Register(Component.For<IChoreProvider>().ImplementedBy<OutdatedResumeChoreProvider>().LifestylePerWebRequest());
            }
        }
    }
}

