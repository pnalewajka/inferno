﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Resumes.ReportModels
{
    public class ResumeReportModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Position { get; set; }

        public string YearsOfExperience { get; set; }

        public string Summary { get; set; }

        public List<ResumeTechnicalSkillsReportModel> TechnicalSkills { get; set; }

        public List<ResumeAdditionalSkillsReportModel> AdditionalSkills { get; set; }

        public List<ResumeCompanyReportModel> Companies { get; set; }

        public List<ResumeLanguageReportModel> Languages { get; set; }

        public List<ResumeEducationReportModel> Educations { get; set; }

        public List<ResumeTrainingReportModel> Trainings { get; set; }

        public bool IsConsentClauseForDataProcessingChecked { get; set; }

        public string IntiveIs { get; set; }

        public string ConsentClauseForDataProcessingText { get; set; }
    }
}
