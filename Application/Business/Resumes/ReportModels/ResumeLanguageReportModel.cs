﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Resumes.ReportModels
{
    public class ResumeLanguageReportModel
    {
        public string Name { get; set; }

        public string Level { get; set; }
    }
}
