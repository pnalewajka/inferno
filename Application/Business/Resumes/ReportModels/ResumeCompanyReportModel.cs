﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Resumes.ReportModels
{
    public class ResumeCompanyReportModel
    {
        public string Name { get; set; }

        public string TimePeriod { get; set; }

        public List<ResumeProjectReportModel> Projects { get; set; }
    }
}
