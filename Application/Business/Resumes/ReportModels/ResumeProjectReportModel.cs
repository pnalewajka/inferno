﻿namespace Smt.Atomic.Business.Resumes.ReportModels
{
    public class ResumeProjectReportModel
    {
        public string Name { get; set; }

        public string DurationInMonths { get; set; }

        public string Roles { get; set; }

        public string Duties { get; set; }

        public string Description { get; set; }

        public string Skillset { get; set; }

        public string BranchOfIndustry { get; set; }
    }
}