﻿namespace Smt.Atomic.Business.Resumes.ReportModels
{
    public class ResumeEducationReportModel
    {
        public string UniversityName { get; set; }

        public string TimePeriod { get; set; }

        public string Specialization { get; set; }

        public string Degree { get; set; }
    }
}
