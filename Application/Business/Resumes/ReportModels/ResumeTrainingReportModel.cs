﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Resumes.ReportModels
{
    public class ResumeTrainingReportModel
    {
        public string Year { get; set; }

        public string Institution { get; set; }

        public string Title { get; set; }
    }
}
