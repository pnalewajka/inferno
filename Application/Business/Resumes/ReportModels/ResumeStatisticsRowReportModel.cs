﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Resumes.ReportModels
{
    public class ResumeStatisticsRowReportModel
    {
        public string OrganizationUnit { get; set; }

        public int FullyCompletedCount { get; set; }

        public int PartiallyCompletedCount { get; set; }

        public int NotCompletedCount { get; set; }

        public decimal Total => FullyCompletedCount + PartiallyCompletedCount + NotCompletedCount;

        public decimal FullyCompletedProportionalWithTotal => Total > 0 ? FullyCompletedCount / Total : 0;

        public decimal PartiallyCompletedProportionalWithTotal => Total > 0 ? PartiallyCompletedCount / Total : 0;

        public decimal NotCompletedProportionalWithTotal => Total > 0 ? NotCompletedCount / Total : 0;
    }
}
