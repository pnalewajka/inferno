﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Resumes.ReportModels
{
    public class ResumeTechnicalSkillsReportModel
    {
        public string Title { get; set; }
        
        public string Content { get; set; }

        public long CustomOrder { get; set; }
    }
}
