﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.Business.Resumes.Dto;
using Smt.Atomic.Business.Resumes.Interfaces;
using Smt.Atomic.Business.Resumes.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Resumes.Services
{
    public class NewSkillSuggestionRule : ISuggestionRule
    {
        private readonly IUnitOfWorkService<IResumesDbScope> _unitOfWorkResumeService;

        public NewSkillSuggestionRule(IUnitOfWorkService<IResumesDbScope> unitOfWorkResumeService)
        {
            _unitOfWorkResumeService = unitOfWorkResumeService;
        }

        public SuggestionRuleResultDto SuggestSkills(long employeeId, IEnumerable<long> allowedSkillsIds)
        {
            using (var unitOfWork = _unitOfWorkResumeService.Create())
            {
                var resumeModificationDate = unitOfWork.Repositories.Resumes.First(r => r.EmployeeId == employeeId).ModifiedOn;

                var result = unitOfWork.Repositories
                       .Skills
                       .Include(s => s.SkillTags)
                       .Where(s =>
                       allowedSkillsIds.Contains(s.Id) &&
                       s.CreatedOn > resumeModificationDate)
                       .Select(s => s.Id)
                       .ToList();

                return new SuggestionRuleResultDto
                {
                    SuggestionReason = ResumeReportResource.ReasonNewSkills,
                    SuggestedSkills = result
                };
            }
        }
    }
}
