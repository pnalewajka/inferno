﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Security;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Resumes.Dto;
using Smt.Atomic.Business.Resumes.Interfaces;
using Smt.Atomic.Business.Resumes.Resources;
using Smt.Atomic.Business.SkillManagement.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Abstracts;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.Resumes;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Resumes.Services
{
    public class ResumeService : IResumeService
    {
        private readonly IUnitOfWorkService<IResumesDbScope> _unitOfWorkResumeService;
        private readonly IUnitOfWorkService<IAllocationDbScope> _unitOfWorkAllocationService;
        private readonly IClassMapping<Resume, MyResumeDto> _resumeToResumeDtoClassMapping;
        private readonly IClassMapping<MyResumeDto, Resume> _resumeDtoToResumeClassMapping;
        private readonly IClassMapping<ResumeLanguageDto, ResumeLanguage> _resumeLanguageDtoToResumeLanguageClassMapping;
        private readonly IClassMapping<ResumeEducationDto, ResumeEducation> _resumeEducationDtoToResumeEducationClassMapping;
        private readonly IClassMapping<ResumeAdditionalSkillDto, ResumeAdditionalSkill> _resumeAdditionalSkillDtoToResumeAdditionalSkillClassMapping;
        private readonly IClassMapping<ResumeCompanyDto, ResumeCompany> _resumeCompanyDtoToResumeCompanyClassMapping;
        private readonly IClassMapping<ResumeTrainingDto, ResumeTraining> _resumeTrainingDtoToResumeTrainingClassMapping;
        private readonly IClassMapping<ResumeTechnicalSkillDto, ResumeTechnicalSkill> _resumeTechnicalSkillDtoToResumeTechnicalSkillClassMapping;
        private readonly IClassMapping<ResumeProjectDto, ResumeProject> _resumeProjectDtoToResumeProjectClassMapping;
        private readonly IClassMapping<Industry, IndustryDto> _industryToIndustryDtoClassMapping;
        private readonly IClassMapping<Skill, SkillDto> _skillToSkillDtoClassMapping;
        private readonly IClassMapping<SkillTag, SkillTagDto> _skillTagToSkillTagDtoClassMapping;
        private readonly IClassMapping<Language, LanguageDto> _languageToLanguageDtoClassMapping;
        private readonly IClassMapping<Company, CompanyDto> _companyToCopanyDtoClassMapping;
        private readonly IClassMapping<Project, ProjectDto> _projectToProjectDtoClassMapping;
        private readonly IClassMapping<JobProfileSkillTag, JobProfileSkillTagDto> _jobProfileSkillTagDtoClassMapping;
        private readonly IUnitOfWorkService<IDictionariesDbScope> _unitOfWorkDictionaries;
        private readonly IUnitOfWorkService<ISkillManagementDbScope> _unitOfWorkSkills;
        private readonly ITimeService _timeService;
        private readonly ISuggestionRule[] _suggestionRules;
        private readonly IDataActivityLogService _dataActivityLogService;

        public ResumeService(
            IUnitOfWorkService<IResumesDbScope> unitOfWorkResumeService,
            IUnitOfWorkService<IAllocationDbScope> unitOfWorkAllocationService,
            IClassMapping<Resume, MyResumeDto> resumeToResumeDtoClassMapping,
            IClassMapping<MyResumeDto, Resume> resumeDtoToResumeClassMapping,
            IClassMapping<ResumeLanguageDto, ResumeLanguage> resumeLanguageDtoToResumeLanguageClassMapping,
            IClassMapping<ResumeEducationDto, ResumeEducation> resumeEducationDtoToResumeEducationClassMapping,
            IClassMapping<ResumeAdditionalSkillDto, ResumeAdditionalSkill> resumeAdditionalSkillDtoToResumeAdditionalSkillClassMapping,
            IClassMapping<ResumeCompanyDto, ResumeCompany> resumeCompanyDtoToResumeCompanyClassMapping,
            IClassMapping<ResumeTrainingDto, ResumeTraining> resumeTrainingDtoToResumeTrainingClassMapping,
            IClassMapping<ResumeTechnicalSkillDto, ResumeTechnicalSkill> resumeTechnicalSkillDtoToResumeTechnicalSkillClassMapping,
            IClassMapping<ResumeProjectDto, ResumeProject> resumeProjectDtoToResumeProjectClassMapping,
            IClassMapping<Industry, IndustryDto> industryToIndustryDtoClassMapping,
            IClassMapping<Skill, SkillDto> skillToSkillDtoClassMapping,
            IClassMapping<SkillTag, SkillTagDto> skillTagToSkillTagDtoClassMapping,
            IClassMapping<Language, LanguageDto> languageToLanguageDtoClassMapping,
            IClassMapping<Company, CompanyDto> companyToCopanyDtoClassMapping,
            IClassMapping<Project, ProjectDto> projectToProjectDtoClassMapping,
            IClassMapping<JobProfileSkillTag, JobProfileSkillTagDto> jobProfileSkillTagDtoClassMapping,
            IUnitOfWorkService<IDictionariesDbScope> unitOfWorkDictionaries,
            IUnitOfWorkService<ISkillManagementDbScope> unitOfWorkSkills,
            ITimeService timeService,
            ISuggestionRule[] suggestionRules,
            IDataActivityLogService dataActivityLogService)
        {
            _unitOfWorkResumeService = unitOfWorkResumeService;
            _unitOfWorkAllocationService = unitOfWorkAllocationService;
            _resumeToResumeDtoClassMapping = resumeToResumeDtoClassMapping;
            _resumeDtoToResumeClassMapping = resumeDtoToResumeClassMapping;
            _resumeLanguageDtoToResumeLanguageClassMapping = resumeLanguageDtoToResumeLanguageClassMapping;
            _resumeEducationDtoToResumeEducationClassMapping = resumeEducationDtoToResumeEducationClassMapping;
            _resumeAdditionalSkillDtoToResumeAdditionalSkillClassMapping = resumeAdditionalSkillDtoToResumeAdditionalSkillClassMapping;
            _resumeCompanyDtoToResumeCompanyClassMapping = resumeCompanyDtoToResumeCompanyClassMapping;
            _resumeTrainingDtoToResumeTrainingClassMapping = resumeTrainingDtoToResumeTrainingClassMapping;
            _resumeTechnicalSkillDtoToResumeTechnicalSkillClassMapping = resumeTechnicalSkillDtoToResumeTechnicalSkillClassMapping;
            _resumeProjectDtoToResumeProjectClassMapping = resumeProjectDtoToResumeProjectClassMapping;
            _industryToIndustryDtoClassMapping = industryToIndustryDtoClassMapping;
            _skillToSkillDtoClassMapping = skillToSkillDtoClassMapping;
            _skillTagToSkillTagDtoClassMapping = skillTagToSkillTagDtoClassMapping;
            _languageToLanguageDtoClassMapping = languageToLanguageDtoClassMapping;
            _companyToCopanyDtoClassMapping = companyToCopanyDtoClassMapping;
            _projectToProjectDtoClassMapping = projectToProjectDtoClassMapping;
            _unitOfWorkDictionaries = unitOfWorkDictionaries;
            _unitOfWorkSkills = unitOfWorkSkills;
            _timeService = timeService;
            _suggestionRules = suggestionRules;
            _jobProfileSkillTagDtoClassMapping = jobProfileSkillTagDtoClassMapping;
            _dataActivityLogService = dataActivityLogService;
        }

        public Resume GetEmployeeResume(long employeeId, IUnitOfWork<IResumesDbScope> unitOfWork)
        {
            bool resumeExists = unitOfWork.Repositories.Resumes.Any(r => r.EmployeeId.HasValue && r.EmployeeId.Value == employeeId);

            if (!resumeExists)
            {
                unitOfWork.Repositories.Resumes.Add(new Resume { EmployeeId = employeeId });
                unitOfWork.Commit();
            }

            var resume = unitOfWork.Repositories
                                   .Resumes
                                   .Include(x => x.Employee)
                                   .Include(x => x.ResumeCompanies)
                                   .Include(x => x.ResumeCompanies
                                                  .Select(c => c.ResumeProjects))
                                    .Include(x => x.ResumeCompanies
                                        .Select(c => c.ResumeProjects
                                            .Select(p => p.Project)))
                                   .Include(x => x.ResumeEducations)
                                   .Include(x => x.ResumeLanguages)
                                   .Include(x => x.ResumeLanguages
                                                  .Select(l => l.Language))
                                   .Include(x => x.ResumeTechnicalSkills)
                                   .Include(x => x.ResumeTechnicalSkills.Select(t => t.SkillTag))
                                   .Include(x => x.ResumeTechnicalSkills
                                                  .Select(s => s.TechnicalSkill))
                                   .Include(x => x.ResumeAdditionalSkills)
                                   .Include(x => x.ResumeTrainings)
                                   .SingleOrDefault(x => x.EmployeeId.HasValue
                                                         && x.EmployeeId.Value == employeeId);

            return resume;
        }

        public MyResumeDto GetEmployeeResumeDto(long employeeId)
        {
            using (var unitOfWork = _unitOfWorkResumeService.Create())
            {
                var resume = GetEmployeeResume(employeeId, unitOfWork);

                var resumeDto = _resumeToResumeDtoClassMapping.CreateFromSource(resume);
                resumeDto.YearsOfExperience = CalculateYearsExperience(resume.ResumeCompanies, CultureInfo.CurrentCulture);

                using (var unitOfWorkEmpl = _unitOfWorkAllocationService.Create())
                {
                    var position = unitOfWorkEmpl.Repositories.Employees.SingleOrDefault(e => e.Id == employeeId).JobTitle;
                    resumeDto.Position = position?.NameEn;
                }

                return resumeDto;
            }
        }

        public SuggestedSkillsGroupDto GetEmployeeSuggestedSkillsGroup(long employeeId)
        {
            using (var unitOfWork = _unitOfWorkResumeService.Create())
            {
                var employeeData = unitOfWork.Repositories
                    .Resumes
                    .Include(r => r.ResumeRejectedSkillSuggestion)
                    .Include(r => r.ResumeTechnicalSkills)
                    .Where(r => r.EmployeeId == employeeId)
                    .Select(r => new
                    {
                        CurrentResumeeSkills = r.ResumeTechnicalSkills.Select(s => s.TechnicalSkillId),
                        ResumeRejectedSkillSuggestion = r.ResumeRejectedSkillSuggestion.Select(s => s.Id),
                        SkillTags = r.Employee.JobProfiles.SelectMany(p => p.SkillTags).Select(t => t.SkillTagId)
                    }).FirstOrDefault();

                //in case resume does not exists (new employees?)
                if (employeeData != null)
                {
                    var allowedSkills = unitOfWork.Repositories
                        .Skills
                        .Include(s => s.SkillTags)
                        .Where(s =>
                            !employeeData.CurrentResumeeSkills.Contains(s.Id) &&
                            !employeeData.ResumeRejectedSkillSuggestion.Contains(s.Id) &&
                            s.SkillTags.Any(st => employeeData.SkillTags.Contains(st.Id)))
                            .ToList();

                    return FindSuggestion(employeeId, allowedSkills, employeeData.SkillTags);
                }
            }

            return SuggestedSkillsGroupDto.Empty();
        }

        private SuggestedSkillsGroupDto FindSuggestion(long employeeId, IEnumerable<Skill> allowedSkills, IEnumerable<long> allowedSkillTagsIds)
        {
            foreach (var suggestionRule in _suggestionRules)
            {
                var suggestionsFound = new List<SuggestedSkillDto>();
                var suggestionRuleResult = suggestionRule.SuggestSkills(employeeId, allowedSkills.Select(s => s.Id));

                foreach (var skill in allowedSkills.Where(s => suggestionRuleResult.SuggestedSkills.Contains(s.Id)))
                {
                    var skillTag = skill.SkillTags.First(st => allowedSkillTagsIds.Contains(st.Id));

                    suggestionsFound.Add(new SuggestedSkillDto
                    {
                        SkillId = skill.Id,
                        Name = skill.Name,
                        SkillTagId = skillTag.Id,
                        SkillTagName = skillTag.Name,
                        ExpertiseLevel = SkillExpertiseLevel.Basic,
                        ExpertisePeriod = SkillExpertisePeriod.LessThanOne
                    });
                }

                if (suggestionsFound.Any())
                {
                    return new SuggestedSkillsGroupDto
                    {
                        SuggestionReason = suggestionRuleResult.SuggestionReason,
                        SkillSuggestions = suggestionsFound
                    };
                }
            }

            return SuggestedSkillsGroupDto.Empty();
        }

        public void ValidateResume(MyResumeDto myResumeDto)
        {
            var duplicateSkills = myResumeDto.ResumeTechnicalSkills?.GroupBy(s => s.TechnicalSkillId)
                .Where(s => s.Count() > 1)
                .Select(s => new
                {
                    duplicateSkillNames = s.First().Name
                });

            if (!duplicateSkills.IsNullOrEmpty())
            {
                throw new BusinessException(string.Format(ResumeReportResource.DuplicateSkills,
                    string.Join(", ", duplicateSkills.Select(x => x.duplicateSkillNames))));
            }
        }

        public void SubmitResume(MyResumeDto myResumeDto)
        {
            ValidateResume(myResumeDto);

            using (var unitOfWork = _unitOfWorkResumeService.Create())
            {
                var resume = _resumeDtoToResumeClassMapping.CreateFromSource(myResumeDto);

                unitOfWork.Repositories.Resumes.Edit(resume);

                SubmitResumeDependency<ResumeLanguageDto, ResumeLanguage>(resume, _resumeLanguageDtoToResumeLanguageClassMapping,
                    myResumeDto.ResumeLanguages, unitOfWork.Repositories.ResumeLanguages);

                SubmitResumeDependency<ResumeEducationDto, ResumeEducation>(resume, _resumeEducationDtoToResumeEducationClassMapping,
                    myResumeDto.ResumeEducations, unitOfWork.Repositories.ResumeEducations);

                SubmitResumeDependency<ResumeAdditionalSkillDto, ResumeAdditionalSkill>(resume, _resumeAdditionalSkillDtoToResumeAdditionalSkillClassMapping,
                   myResumeDto.ResumeAdditionalSkills, unitOfWork.Repositories.ResumeAdditionalSkills);

                SubmitResumeDependency<ResumeTrainingDto, ResumeTraining>(resume, _resumeTrainingDtoToResumeTrainingClassMapping,
                    myResumeDto.ResumeTrainings, unitOfWork.Repositories.ResumeTrainings);

                SubmitResumeDependency<ResumeTechnicalSkillDto, ResumeTechnicalSkill>(resume, _resumeTechnicalSkillDtoToResumeTechnicalSkillClassMapping,
                    myResumeDto.ResumeTechnicalSkills, unitOfWork.Repositories.ResumeTechnicalSkills);

                AppendResumeRejectedSkillSuggestions(resume, myResumeDto.ResumeRejectedSkillSuggestions, unitOfWork.Repositories.Resumes, unitOfWork.Repositories.Skills);

                var companyAndCompanyDtoPairs = SubmitResumeDependency<ResumeCompanyDto, ResumeCompany>(resume,
                    _resumeCompanyDtoToResumeCompanyClassMapping, myResumeDto.ResumeCompanies, unitOfWork.Repositories.ResumeCompanies);

                //there are projects in companies
                var projectIds = myResumeDto.ResumeCompanies != null
                    ? myResumeDto.ResumeCompanies.Where(c => c.ResumeProjects != null).SelectMany(c => c.ResumeProjects).Select(p => p.Id).ToList()
                    : new List<long>();
                unitOfWork.Repositories.ResumeProjects.DeleteEach(p => p.Company.ResumeId == resume.Id && !projectIds.Contains(p.Id));

                SubmitResumeCompanyProjects(companyAndCompanyDtoPairs, unitOfWork);

                resume.CalculateResumeCompleteness();

                resume.IsConsentClauseForDataProcessingChecked = myResumeDto.IsConsentClauseForDataProcessingChecked;

                unitOfWork.Commit();
            }
        }

        private Dictionary<TResume, TResumeDto> SubmitResumeDependency<TResumeDto, TResume>(Resume resume, IClassMapping<TResumeDto, TResume> mapping,
            IList<TResumeDto> dtoList, IRepository<TResume> repository)
            where TResumeDto : class, IResumeDto
            where TResume : ModificationTrackedEntity, IResume
        {
            List<TResume> resumeEntities = dtoList != null ? new List<TResume>() : null;
            Dictionary<TResume, TResumeDto> entityDtoPairs = new Dictionary<TResume, TResumeDto>();

            if (dtoList != null)
            {
                foreach (var dto in dtoList)
                {
                    var entity = mapping.CreateFromSource(dto);
                    resumeEntities.Add(entity);
                    entityDtoPairs.Add(entity, dto);
                }
            }

            var entityIds = dtoList != null ? dtoList.Select(l => l.Id).ToList() : new List<long>();
            repository.DeleteEach(l => l.ResumeId == resume.Id && !entityIds.Contains(l.Id));

            if (resumeEntities != null)
            {
                resumeEntities.ForEach(l => { l.Resume = resume; l.ResumeId = resume.Id; });

                foreach (var entity in resumeEntities)
                {
                    if (entity.Id > 0)
                    {
                        repository.Edit(entity);
                    }
                    else
                    {
                        repository.Add(entity);
                    }
                }
            }

            return entityDtoPairs;
        }

        private void AppendResumeRejectedSkillSuggestions(Resume resume, IList<long> rejectedSkillsIds, IRepository<Resume> resumeRepository, IRepository<Skill> skillRepository)
        {
            if (rejectedSkillsIds != null && rejectedSkillsIds.Any())
            {
                var currentRejections = resumeRepository.Include(r => r.ResumeRejectedSkillSuggestion).Single(r => r.Id == resume.Id).ResumeRejectedSkillSuggestion.Select(s => s.Id).ToList();

                var rejectedSuggestionsToSave = rejectedSkillsIds.Union(currentRejections);
                var newlyRejectedSkills = skillRepository.Where(s => rejectedSuggestionsToSave.Contains(s.Id)).AsNoTracking().ToList();
                resume.ResumeRejectedSkillSuggestion = newlyRejectedSkills;
            }
        }

        private void SubmitResumeCompanyProjects(Dictionary<ResumeCompany, ResumeCompanyDto> companyAndCompanyDtoPairs,
            IUnitOfWork<IResumesDbScope> unitOfWork)
        {
            if (companyAndCompanyDtoPairs != null)
            {
                var internalCompanyProjects = new List<Project>();

                using (var unitOfWorkProjects = _unitOfWorkAllocationService.Create())
                {
                    var projectIds = companyAndCompanyDtoPairs.Where(c => c.Value != null && c.Value.ResumeProjects != null)
                        .Select(c => c.Value)
                        .SelectMany(c => c.ResumeProjects)
                        .Select(p => p.ProjectId).ToList();

                    internalCompanyProjects = unitOfWorkProjects.Repositories.Projects.Where(p => projectIds.Contains(p.Id)).AsNoTracking().ToList();
                }

                foreach (var companyPair in companyAndCompanyDtoPairs)
                {
                    var companyDto = companyPair.Value;
                    var company = companyPair.Key;

                    if (companyDto.ResumeProjects != null)
                    {
                        foreach (var projectDto in companyDto.ResumeProjects)
                        {
                            var project = _resumeProjectDtoToResumeProjectClassMapping.CreateFromSource(projectDto);
                            project.Company = company;
                            project.CompanyId = company.Id;

                            if (internalCompanyProjects.Any(p => p.Id == project.ProjectId) &&
                                ReplaceNewLines(internalCompanyProjects.First(p => p.Id == project.ProjectId)
                                    .Description) == ReplaceNewLines(project.ProjectDescription))
                            {
                                project.ProjectDescription = null;
                            }


                            var industriesIdsDto = projectDto.Industries.Select(i => i.Id).AsEnumerable();
                            var skillsIdsDto = projectDto.TechnicalSkills.Select(i => i.Id).AsEnumerable();

                            var industriesToAdd = industriesIdsDto;
                            var skillsToAdd = skillsIdsDto;

                            if (project.Id > 0)
                            {
                                unitOfWork.Repositories.ResumeProjects.Edit(project);
                                unitOfWork.Repositories.ResumeProjects.Where(p => p.Id == project.Id)
                                    .Include(i => i.Industries)
                                    .Include(s => s.TechnicalSkills).FirstOrDefault();

                                industriesToAdd = industriesIdsDto.Except(project.Industries.Select(x => x.Id));
                                skillsToAdd = skillsIdsDto.Except(project.TechnicalSkills.Select(x => x.Id));

                                var industriesToRemove = project.Industries.Select(i => i.Id).Except(industriesIdsDto);
                                var skillsToRemove = project.TechnicalSkills.Select(i => i.Id).Except(skillsIdsDto);

                                foreach (var industry in project.Industries.Where(i => industriesToRemove.Contains(i.Id)).ToList())
                                {
                                    project.Industries.Remove(industry);
                                }
                                foreach (var skill in project.TechnicalSkills.Where(i => skillsToRemove.Contains(i.Id)).ToList())
                                {
                                    project.TechnicalSkills.Remove(skill);
                                }
                            }
                            else
                            {
                                unitOfWork.Repositories.ResumeProjects.Add(project);
                                project.Industries = new Collection<Industry>();
                                project.TechnicalSkills = new Collection<Skill>();
                            }

                            foreach (var industryId in industriesToAdd.Distinct())
                            {
                                project.Industries.Add(unitOfWork.Repositories.Industries.First(i => i.Id == industryId));
                            }

                            foreach (var skillId in skillsToAdd.Distinct())
                            {
                                project.TechnicalSkills.Add(unitOfWork.Repositories.Skills.First(i => i.Id == skillId));
                            }
                        }
                    }
                }
            }
        }

        private string ReplaceNewLines(string text)
        {
            return text.Replace(Environment.NewLine, "").Replace("\r", "").Replace("\n", "").Trim();
        }

        public string CalculateYearsExperience(ICollection<ResumeCompany> resumeCompanies, CultureInfo culture)
        {
            if (!resumeCompanies.Any())
            {
                return ResumeReportResource.None;
            }

            var currentDate = _timeService.GetCurrentDate();

            var monthOfExperience = 0;
            ISet<DateTime> workedMonths = new HashSet<DateTime>();

            foreach (var company in resumeCompanies)
            {
                workedMonths.UnionWith(DateHelper.AllMonthsInRange(company.From, company.To ?? DateHelper.BeginningOfMonth(DateTime.Now)));
            }

            monthOfExperience = workedMonths.Count;

            using (CultureInfoHelper.SetCurrentUICulture(culture))
            {
                int years = monthOfExperience / 12;
                int months = monthOfExperience % 12;

                var appendixMonths = FormatYearsOfExpirience(months,
                    ResumeReportResource.Month, ResumeReportResource.Months, ResumeReportResource.MonthLabelFor2Items);

                var appendixYears = FormatYearsOfExpirience(years,
                    ResumeReportResource.Year, ResumeReportResource.Years, ResumeReportResource.YearLabelFor2Items);

                var value = "";

                if (years > 0)
                {
                    value = $"{years} {appendixYears}";
                }

                if (months > 0)
                {
                    if (!string.IsNullOrWhiteSpace(value))
                    {
                        value += ", ";
                    }

                    value += $"{months} {appendixMonths}";
                }

                return value;
            }
        }

        private string FormatYearsOfExpirience(int number, string one, string twoToFour, string others)
        {
            if (number > 20 && number % 10 == 1)
                return others;

            if (number == 1)
            {
                return one;
            }
            else if ((number == 2 || number == 3 || number == 4) ||
                    (number > 20 && number % 10 == 2) || (number > 20 && number % 10 == 3) || (number > 20 && number % 10 == 4))
            {
                return twoToFour;
            }
            else
            {
                return others;
            }
        }

        public IList<IndustryDto> GetIndustries()
        {
            using (var unitOfWork = _unitOfWorkDictionaries.Create())
            {
                var industries = unitOfWork.Repositories.Industries.AsNoTracking().ToList();

                var industriesDto = industries.Select(i => _industryToIndustryDtoClassMapping.CreateFromSource(i));

                return industriesDto.ToList();
            }
        }

        public IList<SkillDto> GetSkills()
        {
            using (var unitOfWork = _unitOfWorkSkills.Create())
            {
                var skills = unitOfWork.Repositories.Skills
                    .Where(s => s.SkillTags.Any(t => t.TagType.HasFlag(SkillTagType.ResumeTechnicalSkillsGroup)))
                    .AsNoTracking().ToList();

                var skillsDto = skills.Select(i => _skillToSkillDtoClassMapping.CreateFromSource(i));

                return skillsDto.ToList();
            }
        }

        public IList<EmployeeSkillTagDto> GetEmployeeSkillTags(long employeeId)
        {

            if (employeeId == -1)
            {
                throw new BusinessException(ResumeReportResource.MyResumeNotAvailable);
            }

            using (var unitOfWork = _unitOfWorkResumeService.Create())
            {
                var employeeSkillTag = unitOfWork.Repositories.Employees.First(e => e.Id == employeeId)
                    .JobProfiles.SelectMany(p => p.SkillTags).ToList();

                var employeeSkillTagIds = employeeSkillTag.Select(p => p.SkillTagId).ToList();

                var existingSkillTagIds = unitOfWork.Repositories.Resumes.Where(r => r.EmployeeId == employeeId)
                     .SelectMany(r => r.ResumeTechnicalSkills)
                     .SelectMany(s => s.TechnicalSkill.SkillTags)
                     .Select(t => t.Id).Distinct().ToList();

                employeeSkillTagIds.AddRange(existingSkillTagIds.Except(employeeSkillTagIds));

                var skillTags = unitOfWork.Repositories.SkillTags
                    .Where(t => t.TagType.HasFlag(SkillTagType.ResumeTechnicalSkillsGroup)
                    && employeeSkillTagIds.Contains(t.Id)).ToList();

                var profileSkillTag = employeeSkillTag
                    .Where(x => skillTags.Any(z => z.Id == x.SkillTagId)).GroupBy(x => x.SkillTagId)
                    .Select(group => new EmployeeSkillTagDto
                    {
                        CustomOrder = group.First().CustomOrder,
                        Name = group.First().SkillTag.Name,
                        SkillTagId = group.First().SkillTagId
                    }
                    ).ToList();

                foreach (var skillTag in skillTags.Where(x => !profileSkillTag.Any(z => z.SkillTagId == x.Id)))
                {
                    profileSkillTag.Add(
                        new EmployeeSkillTagDto
                        {
                            CustomOrder = profileSkillTag.Max(z => z.CustomOrder) + 1,
                            Name = skillTag.Name,
                            SkillTagId = skillTag.Id
                        });
                }

                return profileSkillTag;
            }
        }

        public IList<CompanyDto> GetCompanies()
        {
            using (var unitOfWork = _unitOfWorkDictionaries.Create())
            {
                var companies = unitOfWork.Repositories.Companies.AsNoTracking().ToList();

                var companiesDto = companies.Select(i => _companyToCopanyDtoClassMapping.CreateFromSource(i));

                return companiesDto.ToList();
            }
        }

        public IList<ProjectDto> GetProjects(int topProjects, string nameFilter)
        {
            using (var unitOfWork = _unitOfWorkAllocationService.Create())
            {
                var projectsQuery = unitOfWork.Repositories.Projects.AsQueryable();

                if (!string.IsNullOrWhiteSpace(nameFilter))
                {
                    nameFilter = nameFilter.ToLower().Trim();
                    projectsQuery = projectsQuery.Where(p =>
                        p.ProjectSetup.ProjectShortName.ToLower().Contains(nameFilter) ||
                        p.SubProjectShortName.ToLower().Contains(nameFilter) ||
                        p.ProjectSetup.ClientShortName.ToLower().Contains(nameFilter) ||
                        p.PetsCode.ToLower().Contains(nameFilter)
                    );
                }

                if (topProjects > 0)
                {
                    projectsQuery = projectsQuery.Take(topProjects);
                }

                var projects = projectsQuery.OrderBy(e => ProjectBusinessLogic.ProjectName.Call(e)).AsNoTracking().ToList();

                var projectsDto = projects.Select(i => _projectToProjectDtoClassMapping.CreateFromSource(i));

                return projectsDto.ToList();
            }
        }

        public string GetProjectDescription(int projectId)
        {
            using (var unitOfWork = _unitOfWorkAllocationService.Create())
            {
                var project = unitOfWork.Repositories.Projects.First(p => p.Id == projectId);

                return project.Description;
            }
        }

        public IList<LanguageDto> GetLanguages()
        {
            using (var unitOfWork = _unitOfWorkDictionaries.Create())
            {
                var languages = unitOfWork.Repositories.Languages.AsNoTracking().ToList();

                var languagesDto = languages.Select(i => _languageToLanguageDtoClassMapping.CreateFromSource(i));

                return languagesDto.ToList();
            }
        }

        public Guid? GetResumeShareToken(long sharerEmployeeId, long ownerEmployeeId)
        {
            using (var unitOfWork = _unitOfWorkResumeService.Create())
            {
                return unitOfWork.Repositories.ResumeShareToken
                    .Where(r => r.SharerEmployeeId == sharerEmployeeId)
                    .Where(r => r.OwnerEmployeeId == ownerEmployeeId)
                    .Select(r => (Guid?)r.Token)
                    .SingleOrDefault();
            }
        }

        public Guid CreateResumeShareToken(long sharerEmployeeId, long ownerEmployeeId, string reason)
        {
            using (var unitOfWork = _unitOfWorkResumeService.Create())
            {
                var token = unitOfWork.Repositories.ResumeShareToken.CreateEntity();

                token.Token = Guid.NewGuid();
                token.OwnerEmployeeId = ownerEmployeeId;
                token.SharerEmployeeId = sharerEmployeeId;
                token.Reason = reason;
                
                unitOfWork.Repositories.ResumeShareToken.Add(token);
                unitOfWork.Commit();

                _dataActivityLogService.LogEmployeeDataActivity(
                    ownerEmployeeId,
                    ActivityType.ResumeShareTokenGenerated,
                    token.Sharer.FullName,
                    reason);

                return token.Token;
            }
        }

        public long DownloadFromResumeShareToken(Guid token)
        {
            using (var unitOfWork = _unitOfWorkResumeService.Create())
            {
                var shareToken = unitOfWork.Repositories.ResumeShareToken
                    .Where(t => t.Token == token)
                    .SingleOrDefault();

                if (shareToken == null)
                {
                    throw new SecurityException("Invalid token");
                }

                _dataActivityLogService.LogEmployeeDataActivity(
                    shareToken.OwnerEmployeeId,
                    ActivityType.ResumeGenerated,
                    shareToken.Reason);

                return shareToken.OwnerEmployeeId;
            }
        }
    }
}
