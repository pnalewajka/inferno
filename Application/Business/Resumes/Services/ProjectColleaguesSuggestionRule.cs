﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.Business.Resumes.Dto;
using Smt.Atomic.Business.Resumes.Interfaces;
using Smt.Atomic.Business.Resumes.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Resumes.Services
{
    public class ProjectColleaguesSuggestionRule : ISuggestionRule
    {
        private readonly IUnitOfWorkService<IResumesDbScope> _unitOfWorkResumeService;
        private readonly ITimeService _timeService;
        private readonly ISystemParameterService _systemParameterService;

        public ProjectColleaguesSuggestionRule(
            IUnitOfWorkService<IResumesDbScope> unitOfWorkResumeService,
            ITimeService timeService,
            ISystemParameterService systemParameterService)
        {
            _unitOfWorkResumeService = unitOfWorkResumeService;
            _timeService = timeService;
            _systemParameterService = systemParameterService;
        }

        public string SuggestionReason => ResumeReportResource.ReasonProjectColleaguesAdded;

        public SuggestionRuleResultDto SuggestSkills(long employeeId, IEnumerable<long> allowedSkillsIds)
        {
            using (var unitOfWork = _unitOfWorkResumeService.Create())
            {
                var currentDate = _timeService.GetCurrentDate();

                var employee = unitOfWork.Repositories.Employees
                    .Where(e => e.Id == employeeId)
                    .Select(e => new
                    {
                        Projects = e.AllocationRequests.Select(ar => ar.Project),
                        SameProfileEmployeesIds = e.JobProfiles
                        .SelectMany(jobProfile => jobProfile.UsedInEmployees.Select(uie => uie.Id))
                    })
                    .Single();

                var currentProjects = employee.Projects
                    .Where(pr => pr.AllocationRequests
                           .Any(ar => ar.EmployeeId == employeeId &&
                                      ar.StartDate <= currentDate &&
                                      (!ar.EndDate.HasValue || ar.EndDate >= currentDate)))
                    .GroupBy(allocation => allocation.Id)
                    .Select(group => group.First())
                    .ToList();

                foreach (var project in currentProjects)
                {
                    var suggestionFound = GetProjectSuggestions(
                        employeeId, allowedSkillsIds, employee.SameProfileEmployeesIds,
                        unitOfWork, project, true);

                    if (suggestionFound.SuggestedSkills.Any())
                    {
                        return suggestionFound;
                    }
                }

                var otherProjects = employee.Projects
                    .Where(pr => currentProjects.All(c => c.Id != pr.Id))
                    .GroupBy(allocation => allocation.Id)
                    .Select(group => group.First())
                    .ToList();

                foreach (var project in otherProjects)
                {
                    var suggestionFound = GetProjectSuggestions(
                        employeeId, allowedSkillsIds, employee.SameProfileEmployeesIds,
                        unitOfWork, project, false);

                    if (suggestionFound.SuggestedSkills.Any())
                    {
                        return suggestionFound;
                    }
                }

                return SuggestionRuleResultDto.NoSuggestions();
            }
        }

        private SuggestionRuleResultDto GetProjectSuggestions(
            long employeeId, IEnumerable<long> allowedSkillsIds, IEnumerable<long> allowedEmployeesIds,
            IUnitOfWork<IResumesDbScope> unitOfWork, Project project, bool isCurrentProject)
        {
            var currentDate = _timeService.GetCurrentDate();

            var skills = unitOfWork.Repositories.ResumeProjects
                .Where(r => r.ProjectId == project.Id)
                .SelectMany(r => r.TechnicalSkills.Select(skill => skill.Id))
                .Where(id => allowedSkillsIds.Contains(id))
                .Distinct()
                .ToList();

            if (isCurrentProject)
            {
                var colleaguesIds = project.AllocationRequests
                .Where(ar => ar.EmployeeId != employeeId &&
                             allowedEmployeesIds.Contains(ar.EmployeeId))
                .Select(ar => ar.EmployeeId)
                .Distinct()
                .ToList();

                var limitDays = _systemParameterService
                    .GetParameter<int>(ParameterKeys.ResumeDaysToSuggestSkillsFromColleaguesResumees);
                var dateLimit = currentDate.AddDays(-limitDays);

                var resumeSkills = unitOfWork.Repositories
                        .Resumes
                        .Where(r => r.EmployeeId.HasValue &&
                                    colleaguesIds.Contains(r.EmployeeId.Value))
                        .SelectMany(r => r.ResumeTechnicalSkills
                                    .Where(s => s.CreatedOn >= dateLimit)
                                    .Select(s => s.TechnicalSkillId))
                        .Where(id => allowedSkillsIds.Contains(id))
                        .Distinct();

                skills = skills.Union(resumeSkills).ToList();
            }

            if (skills.Any())
            {
                return new SuggestionRuleResultDto
                {
                    SuggestionReason = string.Format(
                        ResumeReportResource.ReasonProjectColleaguesAdded,
                        ProjectBusinessLogic.ProjectName.Call(project)),
                    SuggestedSkills = skills
                };
            }

            return SuggestionRuleResultDto.NoSuggestions();
        }
    }
}