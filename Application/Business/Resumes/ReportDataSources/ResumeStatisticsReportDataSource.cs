﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.Business.Resumes.Dto;
using Smt.Atomic.Business.Resumes.Interfaces;
using Smt.Atomic.Business.Resumes.ReportModels;
using Smt.Atomic.Business.Resumes.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Resumes.ReportDataSources
{
    [Identifier("DataSources.ResumeStatisticsReportDataSource")]
    [DefaultReportDefinition(
        "ResumeStatistics",
        nameof(ResumeReportResource.ResumeStatistics),
        nameof(ResumeReportResource.ResumeStatisticsDescription),
        MenuAreas.Competence,
        MenuGroups.CompetenceReports,
        ReportingEngine.MsExcel,
        "Smt.Atomic.Business.Resumes.ReportTemplates.ResumeStatisticsTemplate.xlsx",
        typeof(ResumeReportResource))]
    [DefaultReportDefinition(
        "ResumeStatisticsPivot",
        nameof(ResumeReportResource.ResumeStatisticsPivot),
        nameof(ResumeReportResource.ResumeStatisticsDescription),
        MenuAreas.Competence,
        MenuGroups.CompetenceReports,
        ReportingEngine.PivotTable,
        "Smt.Atomic.Business.Resumes.ReportTemplates.ResumeStatisticsPivotTable-config.js",
        typeof(ResumeReportResource))]
    [RequireRole(SecurityRoleType.CanGenerateResumeReport)]
    public class ResumeStatisticsReportDataSource : BaseReportDataSource<ResumeStatisticsReportParametersDto>
    {
        private readonly IUnitOfWorkService<IResumesDbScope> _unitOfWorkResume;
        private readonly IOrgUnitService _organizationService;
        private readonly IResumeService _resumeService;

        public ResumeStatisticsReportDataSource(IUnitOfWorkService<IResumesDbScope> unitOfWorkServiceResume,
            IOrgUnitService organizationService,
            IResumeService resumeService)
        {
            _unitOfWorkResume = unitOfWorkServiceResume;
            _organizationService = organizationService;
            _resumeService = resumeService;
        }

        public override ResumeStatisticsReportParametersDto GetDefaultParameters(ReportGenerationContext<ResumeStatisticsReportParametersDto> reportGenerationContext)
        {
            return new ResumeStatisticsReportParametersDto();
        }

        public override object GetData(ReportGenerationContext<ResumeStatisticsReportParametersDto> reportGenerationContext)
        {
            using (var unitOfWork = _unitOfWorkResume.Create())
            {
                var expression = BuildQuery(reportGenerationContext.Parameters);

                var employees = unitOfWork.Repositories.Employees.Include(e => e.OrgUnit)
                    .Where(expression)
                    .Select(e => new { EmployeeId = e.Id, OrganizationUnit = e.OrgUnit == null ? "" : e.OrgUnit.Name }).ToList();

                var employeeIds = employees.Select(e => e.EmployeeId).ToList();
                
                var resumeStatistics = unitOfWork.Repositories.Resumes.Where(r => r.EmployeeId.HasValue && employeeIds.Contains(r.EmployeeId.Value))
                    .Select(x => new
                    {
                        EmployeeId = x.EmployeeId.Value,
                        OrganizationUnit = x.Employee.OrgUnit == null ? "" : x.Employee.OrgUnit.Name,
                        FillPercentage = x.FillPercentage
                    }).ToList();
                
                foreach (var employee in employees)
                {
                    if (!resumeStatistics.Any(r => r.EmployeeId == employee.EmployeeId))
                    {
                        resumeStatistics.Add(new { EmployeeId = employee.EmployeeId, OrganizationUnit = employee.OrganizationUnit, FillPercentage = 0 });
                    }
                }

                return new ResumeStatisticsReportModel
                {
                    ResumeStatistics = resumeStatistics.GroupBy(r => r.OrganizationUnit).Select(x =>
                                          new ResumeStatisticsRowReportModel
                                          {
                                              OrganizationUnit = x.Key,
                                              FullyCompletedCount = x.Count(r => r.FillPercentage == 100),
                                              PartiallyCompletedCount = x.Count(r => r.FillPercentage > 0 && r.FillPercentage < 100),
                                              NotCompletedCount = x.Count(r => r.FillPercentage == 0)
                                          }).ToList()
                };
            }
        }

        private Expression<Func<Employee, bool>> BuildQuery(ResumeStatisticsReportParametersDto parameters)
        {
            Expression<Func<Employee, bool>> expression = r => r.CurrentEmployeeStatus == CrossCutting.Business.Enums.EmployeeStatus.Active;

            if (parameters.LocationIds.Any())
            {
                expression = expression.And(e => e.LocationId.HasValue && parameters.LocationIds.Contains(e.LocationId.Value));
            }

            if (parameters.LineManagerIds.Any())
            {
                expression = expression.And(e => e.LineManagerId.HasValue && parameters.LineManagerIds.Contains(e.LineManagerId.Value));
            }

            if (parameters.JobProfileIds.Any())
            {
                expression = expression.And(e => parameters.JobProfileIds.Intersect(e.JobProfiles.Select(p => p.Id)).Any());
            }

            if (parameters.SkillIds.Any())
            {
                expression = expression.And(e => parameters.SkillIds.Intersect(e.Resumes.SelectMany(r => r.ResumeTechnicalSkills.Select(s => s.TechnicalSkillId))).Any());
            }

            if (parameters.OrgUnitIds.Any())
            {
                var organizationsWithDescendantsIds = parameters.OrgUnitIds.ToList();

                foreach (var parentOrgUnitId in parameters.OrgUnitIds)
                {
                    var descendantIds = _organizationService.GetDescendantOrgUnitsIds(parentOrgUnitId);
                    organizationsWithDescendantsIds.AddRange(descendantIds);
                }

                expression = expression.And(r => organizationsWithDescendantsIds.Distinct().Contains(r.OrgUnitId));
            }

            return expression;
        }
    }
}
