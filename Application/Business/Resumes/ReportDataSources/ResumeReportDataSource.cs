﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.Business.Resumes.Dto;
using Smt.Atomic.Business.Resumes.Interfaces;
using Smt.Atomic.Business.Resumes.ReportModels;
using Smt.Atomic.Business.Resumes.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Resumes;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Resumes.ReportDataSources
{
    [Identifier("DataSources.ResumeReportDataSource")]
    [DefaultReportDefinition(
        "ResumeReportDataSource",
        nameof(ResumeReportResource.Resume),
        nameof(ResumeReportResource.EmployeeResume),
        null,
        null,
        ReportingEngine.MsWord,
        "Smt.Atomic.Business.Resumes.ReportTemplates.ResumeTemplate.docx",
        "INTIVE-@(Model.FirstName)-@(Model.LastName)-CV-EN.docx",
        typeof(ResumeReportResource))]
    [RequireRole(SecurityRoleType.CanGenerateResumeReport)]
    public class ResumeReportDataSource : BaseReportDataSource<ResumeReportParametersDto>, IRequiredRolesProvider
    {
        private const string Separator = ", ";

        private readonly IUnitOfWorkService<IResumesDbScope> _unitOfWorkServiceResume;
        private readonly IUnitOfWorkService<ISkillManagementDbScope> _unitOfWorkServiceSkills;
        private readonly IPrincipalProvider _principalProvider;
        private readonly ITimeService _timeService;
        private readonly IResumeService _resumeService;
        private readonly ISystemParameterService _systemParameterService;

        public ResumeReportDataSource(IUnitOfWorkService<IResumesDbScope> unitOfWorkServiceResume,
            IUnitOfWorkService<ISkillManagementDbScope> unitOfWorkServiceSkills,
            IPrincipalProvider principalProvider,
            ITimeService timeService,
            IResumeService resumeService,
            ISystemParameterService systemParameterService)
        {
            _unitOfWorkServiceResume = unitOfWorkServiceResume;
            _unitOfWorkServiceSkills = unitOfWorkServiceSkills;
            _principalProvider = principalProvider;
            _timeService = timeService;
            _resumeService = resumeService;
            _systemParameterService = systemParameterService;
        }

        public override ResumeReportParametersDto GetDefaultParameters(ReportGenerationContext<ResumeReportParametersDto> reportGenerationContext)
        {
            return new ResumeReportParametersDto();
        }

        public override object GetData(ReportGenerationContext<ResumeReportParametersDto> reportGenerationContext)
        {
            using (CultureInfoHelper.SetCurrentUICulture(CultureInfo.InvariantCulture))
            {
                using (var unitOfWork = _unitOfWorkServiceResume.Create())
                {
                    var resume = _resumeService.GetEmployeeResume(reportGenerationContext.Parameters.EmployeeId, unitOfWork);
                    return GetResumeModel(resume);
                }
            }
        }

        public IEnumerable<SecurityRoleType> GetRolesForReportConfiguration()
        {
            yield return SecurityRoleType.CanGenerateResumeReport;
            yield return SecurityRoleType.CanGenerateOwnResumeReport;
        }

        public IEnumerable<SecurityRoleType> GetRolesForReportExecution(object parameters)
        {
            yield return SecurityRoleType.CanGenerateResumeReport;

            var parametersDto = parameters as ResumeReportParametersDto;
            var employeeId = _principalProvider.Current.EmployeeId;

            if (employeeId == parametersDto?.EmployeeId)
            {
                yield return SecurityRoleType.CanGenerateOwnResumeReport;
            }
        }

        private ResumeReportModel GetResumeModel(Resume resume)
        {
            var cultureDependedParameterContext =
                new CultureDependedParameterContext { CurrentCulture = CultureInfo.GetCultureInfo("en-GB").Name };

            return new ResumeReportModel
            {
                FirstName = resume?.Employee?.FirstName ?? string.Empty,
                LastName = resume?.Employee?.LastName ?? string.Empty,
                Position = resume?.Employee?.JobTitle?.NameEn ?? string.Empty,
                YearsOfExperience = resume != null ? _resumeService.CalculateYearsExperience(resume.ResumeCompanies, CultureInfo.InvariantCulture) : ResumeReportResource.None,
                Summary = resume?.Summary ?? string.Empty,
                Companies = resume != null ? GetCompanies(resume.ResumeCompanies) : new List<ResumeCompanyReportModel>(),
                Languages = resume != null ? GetLanguages(resume.ResumeLanguages) : new List<ResumeLanguageReportModel>(),
                Educations = resume != null ? GetEducation(resume.ResumeEducations) : new List<ResumeEducationReportModel>(),
                Trainings = resume != null ? GetTrainings(resume.ResumeTrainings) : new List<ResumeTrainingReportModel>(),
                TechnicalSkills = resume != null ? GetTechnicalSkills(resume.ResumeTechnicalSkills, resume.Employee) : new List<ResumeTechnicalSkillsReportModel>(),
                AdditionalSkills = resume != null ? GetAdditionalSkills(resume.ResumeAdditionalSkills) : new List<ResumeAdditionalSkillsReportModel>(), 
                IsConsentClauseForDataProcessingChecked = resume?.IsConsentClauseForDataProcessingChecked ?? false,
                IntiveIs = _systemParameterService.GetParameter<string>(ParameterKeys.ResumeIntiveIs, cultureDependedParameterContext),
                ConsentClauseForDataProcessingText = _systemParameterService.GetParameter<string>(ParameterKeys.ResumeConsentClauseForDataProcessing, cultureDependedParameterContext)
            };
        }

        private List<ResumeTechnicalSkillsReportModel> GetTechnicalSkills(ICollection<ResumeTechnicalSkill> resumeTechnicalSkills, Employee employee)
        {      
            using (var unitOfWork = _unitOfWorkServiceSkills.Create())
            {
                using (CultureInfoHelper.SetCurrentUICulture(CultureInfo.InvariantCulture))
                {
                    var technicalSkills = resumeTechnicalSkills.Where(t => t.ExpertiseLevel != SkillExpertiseLevel.None)
                                                                            .SelectMany(x => x.TechnicalSkill.SkillTags, (t, s) => new { Skill = t, SkillTagName = s.NameEn, TagType = s.TagType, SkillTagId = s.Id })
                                                                            .Where(x => x.TagType.HasFlag(SkillTagType.ResumeTechnicalSkillsGroup))
                                                                            .GroupBy(x => new { x.SkillTagName, x.SkillTagId }, s => s.Skill);

                    var employeeSkillTags = employee.JobProfiles.SelectMany(p => p.SkillTags).GroupBy(x => x.SkillTagId).SelectMany(group => group).ToList();

                    Func<List<JobProfileSkillTag>, List<ResumeTechnicalSkillsReportModel>, long, long> getCustomOrder = (skillTags, reportModels, skillTagId) =>
                    {
                        var employeeSkillTag = skillTags.FirstOrDefault(x => x.SkillTagId == skillTagId);
                        return employeeSkillTag != null ? employeeSkillTag.CustomOrder : reportModels.Count() == 0 ? employeeSkillTags.Max(z => z.CustomOrder) + 1 : reportModels.Max(z => z.CustomOrder) + 1;
                    };                   

                    var result = new List<ResumeTechnicalSkillsReportModel>();                    

                    foreach (var skillTag in technicalSkills)
                    {
                        var skillGroups = skillTag.GroupBy(t => t.ExpertiseLevel).OrderByDescending(t => t.Key);

                        string content = "";

                        foreach (var sg in skillGroups)
                        {
                            var experienceLevel = EnumExtensions.GetDescription(sg.Key);
                            var experienceValue = string.Join(", ", sg.Select(t => $"{t.TechnicalSkill.Name}"));
                            content += $"{experienceLevel}: {experienceValue}\r";
                        }

                        result.Add(new ResumeTechnicalSkillsReportModel { Title = skillTag.Key.SkillTagName, Content = content.Trim(), CustomOrder = getCustomOrder(employeeSkillTags, result, skillTag.Key.SkillTagId) });                                          
                    }

                    return result.OrderBy(x => x.CustomOrder).ToList();
                }
            }
        }

        private List<ResumeTrainingReportModel> GetTrainings(ICollection<ResumeTraining> resumeTrainings)
        {
            return resumeTrainings
                    .OrderByDescending(x => x.Date.Year)
                    .Select(
                        x =>
                            new ResumeTrainingReportModel
                            {
                                Institution = x.Institution,
                                Title = x.Title,
                                Year = x.Date.Year.ToString()
                            })
                    .ToList();
        }

        private List<ResumeEducationReportModel> GetEducation(ICollection<ResumeEducation> resumeEducations)
        {
            return resumeEducations
                    .OrderByDescending(x => x.From)
                    .Select(
                        x =>
                            new ResumeEducationReportModel
                            {
                                UniversityName = x.UniversityName,
                                TimePeriod = GetTimePeriodInYearFormat(x.From, x.To),
                                Specialization = x.Specialization,
                                Degree = x.Degree
                            })
                    .ToList();
        }

        private List<ResumeAdditionalSkillsReportModel> GetAdditionalSkills(ICollection<ResumeAdditionalSkill> resumeAdditionalSkills)
        {
            return resumeAdditionalSkills
                    .Select(
                        x =>
                            new ResumeAdditionalSkillsReportModel()
                            {
                                Name = x.Name
                            })
                    .ToList();
        }

        private List<ResumeLanguageReportModel> GetLanguages(ICollection<ResumeLanguage> resumeLanguages)
        {
            return resumeLanguages.Select(
                    x =>
                        new ResumeLanguageReportModel
                        {
                            Level = x.Level.ToString(),
                            Name = x.Language.NameEn
                        }
                    ).ToList();
        }

        private List<ResumeCompanyReportModel> GetCompanies(ICollection<ResumeCompany> resumeCompanies)
        {
            return resumeCompanies
                    .OrderByDescending(c => c.To ?? DateTime.MaxValue).ThenByDescending(c => c.From)
                    .Select(
                        x =>
                            new ResumeCompanyReportModel
                            {
                                Name = x.Name,
                                TimePeriod = GetTimePeriodInMonthYearFormat(x.From, x.To),
                                Projects = GetProjects(x.ResumeProjects)
                            })
                    .ToList();
        }

        private List<ResumeProjectReportModel> GetProjects(ICollection<ResumeProject> resumeProjects)
        {
            return resumeProjects
                    .OrderByDescending(x => x.From)
                    .Select(
                        x =>
                            new ResumeProjectReportModel
                            {
                                Name = x.Project != null ? ProjectBusinessLogic.ClientProjectName.Call(x.Project) : x.Name,
                                Roles = x.Roles,
                                Duties = ClearTextFormating(x.Duties),
                                Description = x.Project != null && string.IsNullOrWhiteSpace(x.ProjectDescription) ? x.Project.Description : x.ProjectDescription,
                                DurationInMonths = CalculateProjectDuration(x.From, x.To),
                                Skillset = ListToInlineValue(x.TechnicalSkills.Select(s => s.Name)),
                                BranchOfIndustry = ListToInlineValue(x.Industries.Select(i => i.Name))
                            })
                    .ToList();
        }

        private string ListToInlineValue(IEnumerable<string> listOfValues)
        {
            return string.Join(Separator, listOfValues);
        }

        private string ClearTextFormating(string text)
        {
            return text.Replace("\t", " ");
        }

        private string CalculateProjectDuration(DateTime from, DateTime? to)
        {
            var dateTo = to.HasValue ? to.Value : _timeService.GetCurrentDate();
            var duration = (dateTo.Year - from.Year) * 12 + dateTo.Month - from.Month + 1;
            var appendix = duration == 1 ? ResumeReportResource.Month : ResumeReportResource.Months;

            return $"{duration} {appendix}";
        }

        private string GetTimePeriodInMonthYearFormat(DateTime from, DateTime? to)
        {
            return string.Format("{0} - {1}", from.ToString("MM yyyy")
                                            , to.HasValue ? to.Value.ToString("MM yyyy") : ResumeReportResource.Now);
        }

        private string GetTimePeriodInYearFormat(DateTime from, DateTime? to)
        {
            return string.Format("{0} - {1}", from.ToString("yyyy")
                                            , to.HasValue ? to.Value.ToString("yyyy") : ResumeReportResource.Now);
        }
    }
}
