﻿using Smt.Atomic.Business.Common.Dto;
using System;

namespace Smt.Atomic.Business.Common.Interfaces
{
    public interface IDtoPropertyNameCardIndexDataService : ICardIndexDataService<PropertyNameDto>
    {
        string GetPropertyNameById(long id, Type viewModelType);
    }
}
