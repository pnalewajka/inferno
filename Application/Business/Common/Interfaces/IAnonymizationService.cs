﻿using System;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.Common.Interfaces
{
    public interface IAnonymizationService
    {
        /// <summary>
        /// Anonymize all properties, for which RoleRequired attribute was used, but respective roles are not matched with required ones
        /// </summary>
        /// <param name="model"></param>
        void Anonymize(object model);

        /// <summary>
        /// Check if any of public properties has RoleRequiredAttribute defined against them.
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <returns></returns>
        bool ShouldBeAnonymized<TModel>();

        /// <summary>
        /// Check if any of public properties has RoleRequiredAttribute defined against them.
        /// </summary>
        /// <param name="modelType"></param>
        /// <returns></returns>
        bool ShouldBeAnonymized(Type modelType);

        /// <summary>
        /// Return list of read restricted properties on specific model, restricted upon current user roles
        /// </summary>
        /// <param name="modelType"></param>
        /// <returns></returns>
        IEnumerable<string> GetReadRestrictedProperties(Type modelType);

        /// <summary>
        /// Get restricted fields in related object
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TDestination"></typeparam>
        /// <param name="classMapping"></param>
        /// <param name="readRestrictedSourceProperties"></param>
        /// <returns></returns>
        IEnumerable<string> GetReadRestrictedPropertiesForRelatedObject<TSource, TDestination>(
            IClassMapping<TDestination, TSource> classMapping, HashSet<string> readRestrictedSourceProperties)
            where TSource : class
            where TDestination : class;

        /// <summary>
        /// Get restricted fields in related object
        /// </summary>
        /// <param name="classMapping"></param>
        /// <param name="readRestrictedSourceProperties"></param>
        /// <param name="sourceType"></param>
        /// <param name="destinationType"></param>
        /// <returns></returns>
        IEnumerable<string> GetReadRestrictedPropertiesForRelatedObject(IClassMapping classMapping,
            HashSet<string> readRestrictedSourceProperties, Type sourceType, Type destinationType);

        /// <summary>
        /// Get missing roles on specific fields
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="fields"></param>
        /// <returns></returns>
        IEnumerable<string> GetMissingRoles<TModel>(IEnumerable<string> fields);

        /// <summary>
        /// Get missing roles on specific fields
        /// </summary>
        /// <param name="modelType"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        IEnumerable<string> GetMissingRoles(Type modelType, IEnumerable<string> fields);
    }
}
