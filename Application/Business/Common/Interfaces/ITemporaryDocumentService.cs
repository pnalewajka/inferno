using System;
using System.IO;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.Common.Interfaces
{
    public interface ITemporaryDocumentService
    {
        /// <summary>
        /// Create temporary document header, should be removed after specific amount of time if broken, or unused
        /// Pass temporaryDocumentId if one is available already
        /// </summary>
        /// <param name="documentName"></param>
        /// <param name="contentType"></param>
        /// <param name="contentLength"></param>
        /// <param name="temporaryDocumentId"></param>
        /// <returns></returns>
        Guid CreateTemporaryDocument(string documentName, string contentType, long contentLength, Guid? temporaryDocumentId);

        /// <summary>
        /// Mark temporary document as completed, no more chunks will be added
        /// </summary>
        /// <param name="temporaryDocumentId"></param>
        /// <returns></returns>
        bool CompleteTemporaryDocument(Guid temporaryDocumentId);

        /// <summary>
        /// Add chunk to open temporary document 
        /// </summary>
        /// <param name="temporaryDocumentId"></param>
        /// <param name="dataStream"></param>
        void AddChunk(Guid temporaryDocumentId, Stream dataStream);

        /// <summary>
        /// Get contents of temporary file
        /// </summary>
        /// <param name="temporaryDocumentId"></param>
        /// <returns></returns>
        Stream GetContentStream(Guid temporaryDocumentId);

        /// <summary>
        /// Clean up storage by removing unused entries older than specific age, as defined by system parameter System.TemporaryDocumentService.TimeToLiveInSeconds
        /// </summary>
        /// <param name="maxAgeInSeconds"></param>
        void CleanUpTemporaryDocuments(int maxAgeInSeconds);

        /// <summary>
        /// Get temporary document metadata
        /// </summary>
        /// <param name="temporaryDocumentId"></param>
        /// <returns></returns>
        TemporaryDocumentDto GetDocumentMetadata(Guid temporaryDocumentId);
    }
}