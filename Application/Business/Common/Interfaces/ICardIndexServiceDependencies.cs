﻿using Castle.Core.Logging;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.Common.Interfaces
{
    public interface ICardIndexServiceDependencies<TDbScope> 
        where TDbScope : IDbScope
    {
        IEntityMergingService EntityMergingService { get; }
        IClassMappingFactory ClassMappingFactory { get; }
        IPrincipalProvider PrincipalProvider { get; }
        IAnonymizationService AnonymizationService { get; }
        IUnitOfWorkService<TDbScope> UnitOfWorkService { get; }
        ITimeService TimeService { get; }
        IBusinessEventPublisher BusinessEventPublisher { get; }
        ISystemParameterService SystemParameters { get; }
        ICacheInvalidationService CacheInvalidationService { get; }
        ILogger Logger { get; }
    }
}