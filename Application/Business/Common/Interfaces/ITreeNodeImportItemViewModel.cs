﻿namespace Smt.Atomic.Business.Common.Interfaces
{
    public interface ITreeNodeImportItemViewModel
    {
        string ParentCode { get; set; }

        string Code { get; set; }

        string Name { get; set; }
    }
}