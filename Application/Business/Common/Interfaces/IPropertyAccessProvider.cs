﻿using System.Reflection;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.CrossCutting.Common.Annotations;

namespace Smt.Atomic.Business.Common.Interfaces
{
    public interface IPropertyAccessProvider
    {
        int Priority { get; }

        AccessRoles GetAccessRoles([NotNull] MemberInfo memberInfo);
    }
}
