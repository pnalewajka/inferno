using System;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.Common.Interfaces
{
    public interface IDocumentMapping
    {
        /// <summary>
        /// Return file with metadata and contents
        /// </summary>
        /// <param name="id">ID of document</param>
        /// <param name="secret">Secret which might be used to bypass security restrictions for given document</param>
        /// <returns></returns>
        DocumentDownloadDto DownloadDocument(long id, Guid? secret = null);
    }
}