﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Business.Common.Interfaces
{
    /// <summary>
    /// File persist service
    /// </summary>
    public interface ITemporaryDocumentPromotionService : ITemporaryDocumentService
    {
        /// <summary>
        /// Copy temporary document contents and metadata into specific entity provided by developer
        /// Developer is responsible over creation target entity, as well as for creation of unitofwork and commiting transaction 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="temporaryDocumentId"></param>
        /// <param name="targetEntity"></param>
        /// <param name="nameColumnExpression"></param>
        /// <param name="mimeTypeColumnExpression"></param>
        /// <param name="contentLengthColumnExpression"></param>
        /// <param name="contentColumnExpression"></param>
        void PromoteToEntity<TEntity>(Guid temporaryDocumentId, TEntity targetEntity,
            Expression<Func<TEntity, object>> nameColumnExpression,
            Expression<Func<TEntity, object>> mimeTypeColumnExpression,
            Expression<Func<TEntity, object>> contentLengthColumnExpression,
            Expression<Func<TEntity, object>> contentColumnExpression) where TEntity : class, IEntity;

        /// <summary>
        /// Copy temporary document contents and metadata into specific entity provided by developer
        /// Developer is responsible over creation target meta entity, as well as for creation of unitofwork and commiting transaction 
        /// System will create targetContent entity if none provided
        /// </summary>
        /// <typeparam name="TMetaEntity"></typeparam>
        /// <typeparam name="TContentEntity"></typeparam>
        /// <param name="temporaryDocumentId"></param>
        /// <param name="targetMetaEntity"></param>
        /// <param name="nameColumnExpression"></param>
        /// <param name="mimeTypeColumnExpression"></param>
        /// <param name="contentLengthColumnExpression"></param>
        /// <param name="targetContentEntity"></param>
        /// <param name="contentColumnExpression"></param>
        void PromoteToEntity<TMetaEntity, TContentEntity>(Guid temporaryDocumentId,
            TMetaEntity targetMetaEntity,
            Expression<Func<TMetaEntity, object>> nameColumnExpression,
            Expression<Func<TMetaEntity, object>> mimeTypeColumnExpression,
            Expression<Func<TMetaEntity, object>> contentLengthColumnExpression,
            TContentEntity targetContentEntity,
            Expression<Func<TContentEntity, object>> contentColumnExpression
            )
            where TMetaEntity : class, IEntity
            where TContentEntity : class, IEntity;
    }
}
