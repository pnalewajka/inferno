﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.Common.Interfaces
{
    public interface IUserChoreProvider
    {
        int GetActiveChoreCount();

        IList<ChoreDto> GetActiveChores();
    }
}
