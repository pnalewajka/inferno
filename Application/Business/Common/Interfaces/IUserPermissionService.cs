﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Common.Interfaces
{
    public interface IUserPermissionService
    {
        void GrantSecurityRolesToUser(long userId, IEnumerable<long> roleIds);
        void RevokeSecurityRolesFromUser(long userId, IEnumerable<long> roleIds);
        void AssignUsersToProfile(long profileId, IEnumerable<long> userIds);
        void AssignSecurityProfilesToUser(long userId, IEnumerable<long> profileIds, bool removeOtherProfiles = false);
        void AssignSecurityProfilesToUserBasedOnActiveDirectory(long userId, string activeDirectoryNames);
        void RemoveSecurityProfilesFromUser(long userId, IEnumerable<long> profileIds);
        void RemoveSecurityProfilesFromUserBasedOnActiveDirectory(long userId, string activeDirectoryNames);
        void RemoveUsersFromProfile(long profileId, IEnumerable<long> userIds);
        void RemoveAllSecurityProfilesFromUser(long userId);
        IEnumerable<long> GetSecurityProfileIdsFromActiveDirectoryGroups(IEnumerable<string> activeDirectoryGroupNames);
    }
}