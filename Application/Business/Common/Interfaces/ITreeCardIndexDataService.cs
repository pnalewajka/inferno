﻿namespace Smt.Atomic.Business.Common.Interfaces
{
    public interface ITreeCardIndexDataService
    {
        /// <summary>
        /// Is a tree renderer in collapsible mode
        /// </summary>
        bool IsCollapsibleTreeMode { get; set; }
    }
}
