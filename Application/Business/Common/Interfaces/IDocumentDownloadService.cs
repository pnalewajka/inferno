﻿using System;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.Common.Interfaces
{
    public interface IDocumentDownloadService
    {
        DocumentDownloadDto GetDocument(string documentMappingIdentifier, long documentId, Guid? secret = null);
    }
}