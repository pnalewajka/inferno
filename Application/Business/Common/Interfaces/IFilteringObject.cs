﻿using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.Common.Interfaces
{
    /// <summary>
    /// Interface indicates that object provides parameters / dto for filtering purposes
    /// </summary>
    public interface IFilteringObject 
    {
        /// <summary>
        /// Get filter parameters (dictionary of parameters)
        /// </summary>
        /// <returns></returns>
        FilterParameters GetFilterParameters();

        /// <summary>
        /// Provide specific dto instance
        /// </summary>
        /// <returns></returns>
        object GetFilterDto();
    }
}
