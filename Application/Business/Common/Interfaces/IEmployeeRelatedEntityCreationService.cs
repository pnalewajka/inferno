﻿namespace Smt.Atomic.Business.Common.Interfaces
{
    public interface IEmployeeRelatedEntityCreationService
    {
        long AddRecommendingPersonBasedOnEmployeeId(long employeeId);

        void AddEmployeeDataConsentBasedOnEmployeeId(long employeeId);
    }
}
