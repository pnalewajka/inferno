﻿namespace Smt.Atomic.Business.Common.Interfaces
{
    public interface IParentIdContext
    {
        long ParentId { get; set; }
    }
}