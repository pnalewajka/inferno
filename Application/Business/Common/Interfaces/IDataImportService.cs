﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Common.Interfaces
{
    public interface IDataImportService
    {
        CardIndexRecords<object> GetRecordsAsObjects(QueryCriteria criteria);

        ImportRecordResult ImportRecord(object dto, ImportBehavior importBehavior);

        void BeforeBulkImportFinished(BeforeBulkImportFinishedEventArgs eventArgs);
    }
}