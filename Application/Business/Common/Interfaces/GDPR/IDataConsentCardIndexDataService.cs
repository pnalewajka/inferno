﻿using Smt.Atomic.Business.Common.Dto.GDPR;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Common.Interfaces.GDPR
{
    public interface IDataConsentCardIndexDataService : ICardIndexDataService<DataConsentDto>
    {
        long GetDataConsentIdByDataConsentDocumentId(long dataConsentDocumentId);

        string GetDocumentNameByDataConsentDocumentId(long dataConsentDocumentId);

        long GetDataOwnerIdByDataConsentId(long dataConsentId);

        DataConsentDto GetDefaultNewRecord(DataConsentType consentType);
    }
}