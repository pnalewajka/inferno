﻿using Smt.Atomic.Business.Common.Dto.GDPR;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Common.Interfaces.GDPR
{
    public interface IDataActivityCardIndexDataService : ICardIndexDataService<DataActivityDto>
    {
        DataActivityDto GetDefaultNewRecord(ReferenceType referenceType);
    }
}
