﻿namespace Smt.Atomic.Business.Common.Interfaces
{
    public interface ITreeNodeImportDto : ITreeNodeDto
    {
        string ParentCode { get; set; }
    }
}