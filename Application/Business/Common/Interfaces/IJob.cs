﻿using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Models;

namespace Smt.Atomic.Business.Common.Interfaces
{
    public interface IJob
    {
        /// <summary>
        /// Do job of given kind, objects implementing this interface are instantiated in main loop of Scheduler windows service 
        /// </summary>
        /// <returns>whether job was done successfully</returns>
        /// <param name="executionContext"></param>
        JobResult DoJob(JobExecutionContext executionContext);
    }
}
