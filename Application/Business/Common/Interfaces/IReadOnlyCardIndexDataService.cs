﻿using Smt.Atomic.Business.Common.Dto;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Contexts;

namespace Smt.Atomic.Business.Common.Interfaces
{
    /// <summary>
    /// Interface allows only get data for card index
    /// </summary>
    /// <typeparam name="TDto"></typeparam>
    public interface IReadOnlyCardIndexDataService<TDto>
    {
        /// <summary>
        /// Get single record with given id.
        /// Throws exception if entity not found.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TDto GetRecordById(long id);

        /// <summary>
        /// Get single record with given id or default if entity not found.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TDto GetRecordByIdOrDefault(long id);

        /// <summary>
        /// Get all records with given ids.
        /// Data will be filtered or not depending on the filter parameter.
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="filtered"></param>
        /// <returns></returns>
        IDictionary<long, TDto> GetRecordsByIds(long[] ids, bool filtered = true);

        /// <summary>
        /// Gets all records with the given sorting criteria
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        CardIndexRecords<TDto> GetRecords(QueryCriteria criteria = null);

        /// <summary>
        /// Gets disposable queryable collection of all records
        /// </summary>
        /// <returns></returns>
        DisposableQueryable<TDto> GetQueryableRecords(QueryCriteria criteria = null);

        /// <summary>
        /// Indicates if ICardIndexDataService has any search columns defined
        /// </summary>
        bool SupportsSearchByColumns { get; }

        /// <summary>
        /// Indicates if ICardIndexDataService has any named filters defined
        /// </summary>
        bool SupportsNamedFilters { get; }

        /// <summary>
        /// Gets the id of the given record
        /// Since we do not require for the Dto and ViewModel to have explicit Id field the entity Id is used for that
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        long GetRecordId(TDto dto);
    }
}
