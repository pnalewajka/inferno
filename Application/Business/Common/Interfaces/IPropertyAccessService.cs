﻿using System.Collections.Generic;
using System.Reflection;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Annotations;

namespace Smt.Atomic.Business.Common.Interfaces
{
    public interface IPropertyAccessService
    {
        /// <summary>
        /// Get property access mode for specific property
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <param name="userRoles"></param>
        /// <returns></returns>
        PropertyAccessMode GetPropertyAccessMode(
            [NotNull] PropertyInfo propertyInfo,
            [NotNull] HashSet<string> userRoles);

        /// <summary>
        /// Return list of missing roles for specific property
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <param name="userRoles"></param>
        /// <returns></returns>
        IEnumerable<string> GetPropertyMissingRoles(
            [NotNull] PropertyInfo propertyInfo,
            [NotNull] HashSet<string> userRoles);
    }
}
