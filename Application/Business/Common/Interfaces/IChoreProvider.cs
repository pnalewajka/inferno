﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.Common.Interfaces
{
    public interface IChoreProvider
    {
        IQueryable<ChoreDto> GetActiveChores(IReadOnlyRepositoryFactory repositoryFactory);
    }
}
