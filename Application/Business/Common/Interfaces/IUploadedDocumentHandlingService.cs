﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Abstracts;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.Common.Interfaces
{
    /// <summary>
    /// Methods for supporting uploaded files handling
    /// </summary>
    public interface IUploadedDocumentHandlingService
    {
        /// <summary>
        /// Merge files from Dto collection in dbcollection of entity, using mapping function for entity creation
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TDbScope"></typeparam>
        /// <param name="unitOfWork"></param>
        /// <param name="dbCollection"></param>
        /// <param name="inputDocumentCollection"></param>
        /// <param name="mappingFunction"></param>
        void MergeDocumentCollections<TEntity, TDbScope>(IUnitOfWork<TDbScope> unitOfWork,
            ICollection<TEntity> dbCollection, IEnumerable<DocumentDto> inputDocumentCollection,
            Func<DocumentDto, TEntity> mappingFunction)
            where TEntity : class, IEntity
            where TDbScope : IDbScope;

        /// <summary>
        /// Update entity from DocumentDto, using mapping function for entity creation
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TDbScope"></typeparam>
        /// <param name="dbDocument"></param>
        /// <param name="inputDocument"></param>
        /// <param name="unitOfWork"></param>
        /// <param name="mappingFunction"></param>
        TEntity UpdateRelatedDocument<TEntity, TDbScope>(IUnitOfWork<TDbScope> unitOfWork, TEntity dbDocument, DocumentDto inputDocument, Func<DocumentDto, TEntity> mappingFunction)
            where TEntity : class, IEntity
            where TDbScope : IDbScope;

        /// <summary>
        /// Promote file Dto collection to entity objects (meta for filename and contentype, content for actual file contents
        /// </summary>
        /// <typeparam name="TMetaEntity"></typeparam>
        /// <typeparam name="TContentEntity"></typeparam>
        /// <param name="inputDocumentCollection"></param>
        /// <param name="nameColumnExpression"></param>
        /// <param name="contentTypeColumnExpression"></param>
        /// <param name="contentLengthColumnExpression"></param>
        /// <param name="contentColumnExpression"></param>
        [Obsolete]
        void PromoteTemporaryDocuments<TMetaEntity, TContentEntity>(IEnumerable<DocumentDto> inputDocumentCollection,
            Expression<Func<TMetaEntity, object>> nameColumnExpression,
            Expression<Func<TMetaEntity, object>> contentTypeColumnExpression,
            Expression<Func<TMetaEntity, object>> contentLengthColumnExpression,
            Expression<Func<TContentEntity, object>> contentColumnExpression
            )
            where TMetaEntity : class, IEntity
            where TContentEntity : class, IEntity;

        /// <summary>
        /// Promote file Dto collection to entity objects passing file mapping object
        /// TMetaEntity can be equal to TContentEntity if programmer wishes to use the same entity for meta and storage (not recommended due to performance of sql server)
        /// </summary>
        /// <typeparam name="TMetaEntity">Meta entity: location of filename content type columns </typeparam>
        /// <typeparam name="TContentEntity">Content enitity: location of file content column</typeparam>
        /// <typeparam name="TMapping"></typeparam>
        /// <typeparam name="TDbScope"></typeparam>
        /// <param name="inputDocumentCollection"></param>
        void PromoteTemporaryDocuments<TMetaEntity, TContentEntity, TMapping, TDbScope>(IEnumerable<DocumentDto> inputDocumentCollection)
            where TMetaEntity : class, IEntity
            where TContentEntity : class, IEntity
            where TMapping : DocumentMapping<TMetaEntity, TContentEntity, TDbScope>
            where TDbScope : IDbScope;
    }
}
