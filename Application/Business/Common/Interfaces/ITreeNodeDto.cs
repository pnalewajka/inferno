﻿namespace Smt.Atomic.Business.Common.Interfaces
{
    public interface ITreeNodeDto
    {
        long Id { get; set; }

        string Name { get; set; }

        string Code { get; set; }

        long? ParentId { get; set; }

        string Path { get; }

        long NameSortOrder { get; }

        long DescendantCount { get; }

        int? Depth { get; set; }

        long? CustomOrder { get; set; }
    }
}