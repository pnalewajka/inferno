using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Common.Interfaces
{
    public interface ICardIndexDataService<TDto> : IDataImportService, IReadOnlyCardIndexDataService<TDto>
    {
        /// <summary>
        /// Add a new record
        /// </summary>
        /// <param name="record">New record to be added</param>
        /// <param name="context">Context of new record to be added</param>
        /// <returns></returns>
        AddRecordResult AddRecord(TDto record, object context = null);

        /// <summary>
        /// Edit record
        /// </summary>
        /// <param name="record">Record to be edited</param>
        /// <param name="context">Record's context</param>
        /// <returns></returns>
        EditRecordResult EditRecord(TDto record, object context = null);

        /// <summary>
        /// Bulk edit records
        /// </summary>
        /// <param name="dto">Info of records to be edited</param>
        /// <param name="context">Record's context</param>
        /// <returns></returns>
        BulkEditRecordResult BulkEditRecords(BulkEditDto dto, object context = null);
        
        /// <summary>
        /// Delete all records with given ids and limited to the given context
        /// </summary>
        /// <param name="idsToDelete"></param>
        /// <param name="context"></param>
        /// <param name="isDeletePermanent">skip soft delete</param>
        /// <returns></returns>
        DeleteRecordsResult DeleteRecords(IEnumerable<long> idsToDelete, object context = null, bool isDeletePermanent = false);
        
        /// <summary>
        /// Returns default new record which should be used for adding a new record
        /// </summary>
        /// <returns></returns>
        TDto GetDefaultNewRecord();

        /// <summary>
        /// Returns list of service-level-defined roles for filters, if any.
        /// (Please note this will be refactored soon.)
        /// </summary>
        /// <returns></returns>
        IEnumerable<KeyValuePair<string, SecurityRoleType>> GetFilterRoles();
    }
}