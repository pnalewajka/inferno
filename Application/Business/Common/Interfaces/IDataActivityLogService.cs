﻿using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Common.Interfaces
{
    public interface IDataActivityLogService
    {
        void LogEmployeeDataActivity(long employeeId, ActivityType activityType, params string[] details);

        void LogEmployeeDataActivity(long employeeId, ActivityType activityType, ReferenceType referenceType, long referenceId, params string[] details);

        void LogUserDataActivity(long userId, ActivityType activityType, params string[] details);

        void LogUserDataActivity(long userId, ActivityType activityType, ReferenceType referenceType, long referenceId, params string[] details);

        void LogCandidateDataActivity(long candidateId, ActivityType activityType, params string[] details);

        void LogCandidateDataActivity(long candidateId, ActivityType activityType, ReferenceType referenceType, long referenceId, params string[] details);

        void LogRecommendingPersonDataActivity(long recomendingPersonId, ActivityType activityType, params string[] details);

        void LogRecommendingPersonDataActivity(long recomendingPersonId, ActivityType activityType, ReferenceType referenceType, long referenceId, params string[] details);

        void LogDataOwnerDataActivity(long dataOwnerId, ActivityType activityType, params string[] details);

        void LogDataOwnerDataActivity(long dataOwnerId, ActivityType activityType, ReferenceType referenceType, long referenceId, params string[] details);

        void OnEmployeeAdded(long employeeId);
    }
}
