namespace Smt.Atomic.Business.Common.Interfaces
{
    public interface ITreeNodeItemViewModel
    {
        long Id { get; set; }

        string Code { get; set; }

        string Name { get; set; }

        long? ParentId { get; set; }

        int? Depth { get; set; }

        long DescendantCount { get; }

        string GetRowCssClass();
    }
}
