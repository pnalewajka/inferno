﻿namespace Smt.Atomic.Business.Common.Interfaces
{
    public interface IDanteCalendarService
    {
        long GetCalendarIdByOrgUnitId(long? orgUnitId);
        long GetDefaultCalendarId();
    }
}