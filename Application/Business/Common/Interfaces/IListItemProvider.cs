﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.Common.Interfaces
{
    /// <summary>
    /// Interface for objects providing list items for dropdowns
    /// </summary>
    public interface IListItemProvider
    {
        /// <summary>
        /// Returns list of items to be used in a dropdown
        /// </summary>
        /// <returns>List of items. It might contain empty item (with null key)
        /// If empty item is not present it will be added automatically</returns>
        IEnumerable<ListItem> GetItems();
    }
}