﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.Common
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            switch (containerType)
            {
                case ContainerType.JobScheduler:
                case ContainerType.WebApi:
                case ContainerType.WebApp:
                    container.Register(Component.For<ITemporaryDocumentService, ITemporaryDocumentPromotionService>().ImplementedBy<DbTemporaryDocumentService>().Named("FullService").LifestyleTransient());
                    container.Register(Component.For<IUploadedDocumentHandlingService>().ImplementedBy<UploadedDocumentHandlingService>().LifestyleTransient());
                    container.Register(Component.For<IDocumentDownloadService>().ImplementedBy<DocumentDownloadService>().LifestyleTransient());
                    container.Register(Component.For<IAnonymizationService>().ImplementedBy<AnonymizationService>().LifestyleTransient());
                    container.Register(Component.For<IPropertyAccessProvider>().ImplementedBy<PropertyAccessProvider>().LifestyleTransient());
                    container.Register(Component.For<IDanteCalendarService>().ImplementedBy<DanteCalendarService>().LifestyleTransient());
                    container.Register(Component.For<IDtoPropertyNameCardIndexDataService>().ImplementedBy<DtoPropertyNameCardIndexDataService>().LifestyleTransient());
                    container.Register(Component.For<IDataActivityLogService>().ImplementedBy<DataActivityLogService>().LifestyleTransient());
                    container.Register(Component.For<IUserChoreProvider>().ImplementedBy<UserChoreProvider>().LifestyleTransient());
                    break;
            }

            if (containerType == ContainerType.WebApp)
            {
                container.Register(Component.For<IEmployeeRelatedEntityCreationService>().ImplementedBy<EmployeeRelatedEntityCreationService>().LifestylePerWebRequest());
            }

            if (containerType == ContainerType.JobScheduler)
            {
                container.Register(Component.For<IEmployeeRelatedEntityCreationService>().ImplementedBy<EmployeeRelatedEntityCreationService>().LifestylePerThread());
            }

            container.Register(Component.For(typeof(ICardIndexServiceDependencies<>)).ImplementedBy(typeof(CardIndexDataServiceDependencies<>)).LifestyleTransient());
        }
    }
}