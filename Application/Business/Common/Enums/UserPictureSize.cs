﻿namespace Smt.Atomic.Business.Common.Enums
{
    /// <summary>
    /// User picture sizes (used for different presentation purposes)
    /// </summary>
    public enum UserPictureSize
    {
        Small = 20,
        Normal = 70,
        Big = 240
    }
}