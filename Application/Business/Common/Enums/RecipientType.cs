﻿namespace Smt.Atomic.Business.Common.Enums
{
    public enum RecipientType
    {
        To,
        Cc,
        Bcc
    }
}