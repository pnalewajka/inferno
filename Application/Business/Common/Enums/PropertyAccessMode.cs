namespace Smt.Atomic.Business.Common.Enums
{
    /// <summary>
    /// Property access mode
    /// </summary>
    public enum PropertyAccessMode
    {
        /// <summary>
        /// No access at all
        /// </summary>
        NoAccess,

        /// <summary>
        /// User can only read property value
        /// </summary>
        ReadOnly,
        
        /// <summary>
        /// User can modify property value
        /// </summary>
        ReadWrite
    }
}