﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.Common.Abstracts
{
    /// <summary>
    /// Defines mappings for file name, content type and content for generic extraction & handling
    /// TMetaEntity can be equal to TContentEntity if programmer wishes to use the same entity for meta and storage (not recommended due to performance of sql server)
    /// </summary>
    /// <typeparam name="TMetaEntity">Meta entity: location of filename content type columns </typeparam>
    /// <typeparam name="TContentEntity">Content enitity: location of file content column</typeparam>
    /// <typeparam name="TDbScope"></typeparam>
    public abstract class DocumentMapping<TMetaEntity, TContentEntity, TDbScope> : IDocumentMapping
        where TMetaEntity : class, IEntity
        where TContentEntity : class, IEntity
        where TDbScope : IDbScope
    {
        private readonly IUnitOfWorkService<TDbScope> _unitOfWorkService;

        protected DocumentMapping(IUnitOfWorkService<TDbScope> unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        public Expression<Func<TMetaEntity, object>> NameColumnExpression { get; set; }
        public Expression<Func<TMetaEntity, object>> ContentTypeColumnExpression { get; set; }
        public Expression<Func<TMetaEntity, object>> ContentLengthColumnExpression { get; set; }
        public Expression<Func<TContentEntity, object>> ContentColumnExpression { get; set; }

        public DocumentDownloadDto DownloadDocument(long id, Guid? secret)
        {
            var resultDto = new DocumentDownloadDto();

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var repositoryFactory = ((IRepositoryFactory)unitOfWork.Repositories);
                var metaEntityRepository = repositoryFactory.Create<TMetaEntity>();
                var metaEntity = GetMetaEntity(metaEntityRepository, id, secret);

                resultDto.DocumentName = (string)NameColumnExpression.Compile().Invoke(metaEntity);
                resultDto.ContentType = (string)ContentTypeColumnExpression.Compile().Invoke(metaEntity);
                resultDto.ContentLength = (long)ContentLengthColumnExpression.Compile().Invoke(metaEntity);

                if (typeof(TMetaEntity) == typeof(TContentEntity))
                {
                    resultDto.Content =
                        (byte[])ContentColumnExpression.Compile().Invoke(metaEntity as TContentEntity);
                }
                else
                {
                    var contentEntityRepository = repositoryFactory.Create<TContentEntity>();
                    var contentEntity = contentEntityRepository.GetById(id);
                    resultDto.Content = (byte[])ContentColumnExpression.Compile().Invoke(contentEntity);
                }

                return resultDto;
            }
        }

        protected virtual TMetaEntity GetMetaEntity(IRepository<TMetaEntity> metaEntityRepository, long id, Guid? secret)
        {
            return metaEntityRepository.Filtered().GetById(id);
        }
    }
}
