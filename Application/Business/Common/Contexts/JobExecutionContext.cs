﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Smt.Atomic.Business.Common.Contexts
{
    /// <summary>
    /// Job execution context
    /// </summary>
    public class JobExecutionContext
    {
        public JobExecutionContext(CancellationToken cancellationToken, IDictionary<string,string> parameters, string host, string schedulerName, string pipeline, long jobDefinitionId, bool isLastAttempt, TimeSpan maxExecutionTime)
        {
            CancellationToken = cancellationToken;
            Parameters = parameters;
            Host = host;
            SchedulerName = schedulerName;
            Pipeline = pipeline;
            JobDefinitionId = jobDefinitionId;
            IsLastAttempt = isLastAttempt;
            MaxExecutionTime = maxExecutionTime;
            ExecutionId = Guid.NewGuid();
        }

        public CancellationToken CancellationToken { get; private set; }

        /// <summary>
        /// Job parameters
        /// </summary>
        public IDictionary<string, string> Parameters { get; private set; }

        /// <summary>
        /// Host name, on which scheduler is running
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// Scheduler name
        /// </summary>
        public string SchedulerName { get; set; }

        /// <summary>
        /// Pipeline
        /// </summary>
        public string Pipeline { get; set; }

        /// <summary>
        /// Db id of current job
        /// </summary>
        public long JobDefinitionId { get; set; }

        /// <summary>
        /// Guid of current job execution
        /// </summary>
        public Guid ExecutionId { get; }

        /// <summary>
        /// Will job execution be dropped if fails?
        /// </summary>
        public bool IsLastAttempt { get; }

        /// <summary>
        /// Max job execution time
        /// </summary>
        public TimeSpan MaxExecutionTime { get; set; }

        /// <summary>
        /// Get preamble for log message
        /// </summary>
        /// <returns></returns>
        public string GetMessagePreamble()
        {
            return $"Pipeline {Pipeline}, job {JobDefinitionId}";
        }

        /// <summary>
        /// Get preamble for log message with thread info
        /// </summary>
        /// <returns></returns>
        public string GetMessagePreambleWithThreadInfo()
        {
            return $"Pipeline {Pipeline}, job {JobDefinitionId}, thread {Thread.CurrentThread.ManagedThreadId}";
        }

    }
}
