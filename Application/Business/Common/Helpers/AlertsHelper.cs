﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.Common.Helpers
{
    public static class AlertsHelper
    {
        public static string SerializeToString(IEnumerable<AlertDto> alerts)
        {
            return string.Join(Environment.NewLine, alerts.Select(SerializeToString));
        }

        public static string SerializeToString(AlertDto alert)
        {
            return $"{alert.Type}: {alert.Message}";
        }
    }
}
