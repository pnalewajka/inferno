﻿using System;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Annotations;

namespace Smt.Atomic.Business.Common.Helpers
{
    public static class HumanNameHelper
    {
        public static char[] LastNameSeparators = new[] { ' ', '-' };

        public static bool IsComplexLastName([NotNull]string lastName)
        {
            if (lastName == null)
            {
                throw new ArgumentNullException(nameof(lastName));
            }

            return LastNameSeparators.Any(c => lastName.Contains(c));
        }

        public static string[] GetLastNameParts([NotNull]string lastName)
        {
            if (lastName == null)
            {
                throw new ArgumentNullException(nameof(lastName));
            }

            return lastName.Split(LastNameSeparators, StringSplitOptions.RemoveEmptyEntries);
        }

        public static string SanitizeName(string name)
        {
            return (name ?? string.Empty).Trim();
        }
    }
}
