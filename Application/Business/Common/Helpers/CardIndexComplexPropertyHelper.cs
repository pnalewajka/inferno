﻿using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Common.Helpers
{
    public class CardIndexComplexPropertyHelper
    {
        /// <summary>
        /// Receives nasted path element for complex property types. Used in each cases when we need to base on one of the complex type property like ordering sorting or searching.
        /// </summary>
        /// <returns>Property name</returns>
        public static string GetNestedPath<T>(string propertyName)
        {
            var propertyType = PropertyHelper.GetPropertyTypeByNameOrDefault<T>(propertyName);

            if (propertyType.BaseType == typeof(LocalizedStringBase))
            {
                return LocalizedStringHelper.GetLocalizedPropertyInfo(propertyType).Name;
            }

            return string.Empty;
        }
    }
}
