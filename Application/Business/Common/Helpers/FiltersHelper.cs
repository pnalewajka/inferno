﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.Common.Helpers
{
    public static class FiltersHelper
    {
        private const string AndFilterGroupSeparator = "*";
        private const string OrFilterGroupSeparator = " ";
        private const char ListSeparator = ',';

        public static List<FilterCodeGroup> ParseFilterString(string filtersString, HashSet<string> readRestrictedFilterCodes = null)
        {
            var filterGroups = new List<FilterCodeGroup>();

            if(filtersString == null)
            {
                return filterGroups;
            }

            readRestrictedFilterCodes = readRestrictedFilterCodes ?? new HashSet<string>();
            var groupExpressions = filtersString.Split(new[] { ListSeparator }, StringSplitOptions.RemoveEmptyEntries).ToList();

            foreach (var groupExpression in groupExpressions)
            {
                var filterCodes = new FilterCodeGroup
                {
                    IsConjunction = groupExpression.Contains(AndFilterGroupSeparator)
                };

                var separator = filterCodes.IsConjunction ? AndFilterGroupSeparator : OrFilterGroupSeparator;
                var codes = groupExpression.Split(new[] { separator }, StringSplitOptions.None);

                filterCodes.Codes = codes.Where(c => !readRestrictedFilterCodes.Contains(c)).ToList();

                if (filterCodes.Codes.Any())
                {
                    filterGroups.Add(filterCodes);
                }
            }

            return filterGroups;
        }

        public static string CreateFilterString(IList<FilterCodeGroup> filterGroups)
        {
            var filterStringBuilder = new StringBuilder();

            foreach (var filterGroup in filterGroups)
            {
                var groupExpression = string.Join(filterGroup.IsConjunction ? AndFilterGroupSeparator : "+", filterGroup.Codes);

                if (filterStringBuilder.Length > 0)
                {
                    filterStringBuilder.Append(ListSeparator);
                }

                filterStringBuilder.Append(groupExpression);
            }

            return filterStringBuilder.ToString();
        }

        /// <summary>
        /// Creates list of years used in filter
        /// </summary>
        /// <param name="currentYear">Current year</param>
        /// <param name="years">Years witch should be added do the filter</param>
        /// <returns></returns>
        public static IEnumerable<FilterYearDto> CreateFiltersYears(int currentYear, HashSet<int> years)
        {
            var result = new List<FilterYearDto>();

            if (years.Any())
            {
                var minYear = years.Min();
                var maxYear = years.Max();

                if (minYear > currentYear)
                {
                    minYear = currentYear;
                }

                if (maxYear < currentYear)
                {
                    maxYear = currentYear;
                }

                for (var y = minYear - 1; y <= maxYear + 1; y++)
                {
                    result.Add(new FilterYearDto { Year = y, HasEntries = years.Contains(y) });
                }
            }
            else
            {
                result.Add(new FilterYearDto { Year = currentYear - 1 });
                result.Add(new FilterYearDto { Year = currentYear });
                result.Add(new FilterYearDto { Year = currentYear + 1 });
            }

            return result.AsEnumerable();
        }
    }
}
