﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Common.Helpers
{
    public static class StringExtender
    {
        public static string NormalizeEndLinesToUnix(this string s)
        {
            if (!string.IsNullOrEmpty(s))
            {
                return s.Replace("\r\n", "\n");
            }
            else
            {
                return s;
            }
        }

        public static string ShortDateString(this DateTime? date)
        {
            if (date.HasValue)
                return date.Value.ToString("yyyy-MM-dd");
            return string.Empty;
        }

        public static string ShortDateString(this DateTime date)
        {
            return date.ToString("yyyy-MM-dd");
        }
    }
}
