﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Common.Helpers
{
    public static class ClassMappingHelper
    {
        public static IEnumerable<PropertyGroup> TranslateToDestinationPropertyNames<TSource, TDestination>(
            IEnumerable<PropertyGroup> propertyGroups, 
            IClassMapping classMapping)
        {
            return TranslateToDestinationPropertyNames(propertyGroups, classMapping, typeof(TSource), typeof(TDestination));
        }

        public static IEnumerable<PropertyGroup> TranslateToDestinationPropertyNames(
            IEnumerable<PropertyGroup> propertyGroups, 
            IClassMapping classMapping, 
            Type sourceType, 
            Type destinationType)
        {
            var mappings = classMapping.GetFieldMappings(sourceType, destinationType);

            foreach (var sourceProperty in propertyGroups)
            {
                var destinationGroup = new PropertyGroup();

                foreach (var property in sourceProperty.PropertyNames)
                {
                    List<string> propertyNames;
                    var hasValue = mappings.TryGetValue(property, out propertyNames);

                    if (!hasValue)
                    {
                        var message = $"Value not found in key: {property}  in mapping: {classMapping}.";

                        throw new Exception(message);
                    }

                    destinationGroup.PropertyNames.AddRange(propertyNames.Where(v => !string.IsNullOrEmpty(v)));
                }

                yield return destinationGroup;
            }
        }
    }
}
