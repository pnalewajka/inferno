﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Business.Common.Helpers
{
    public static class SecurityHelper
    {
        /// <summary>
        /// Get property access mode for specific property
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <param name="userRoles"></param>
        /// <returns></returns>
        public static PropertyAccessMode GetPropertyAccessMode([NotNull] MemberInfo memberInfo, [NotNull] HashSet<string> userRoles)
        {
            if (memberInfo == null)
            {
                throw new ArgumentNullException(nameof(memberInfo));
            }

            if (userRoles == null)
            {
                throw new ArgumentNullException(nameof(userRoles));
            }

            var accessRoles = GetAccessRoles(memberInfo);

            return accessRoles.GetAccessMode(userRoles);
        }

        /// <summary>
        /// Return list of missing roles for specific property
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <param name="userRoles"></param>
        /// <returns></returns>
        public static IEnumerable<string> GetPropertyMissingRoles([NotNull] MemberInfo memberInfo,
            [NotNull] HashSet<string> userRoles)
        {
            if (memberInfo == null)
            {
                throw new ArgumentNullException(nameof(memberInfo));
            }

            if (userRoles == null)
            {
                throw new ArgumentNullException(nameof(userRoles));
            }

            var roleRequiredAttribute = AttributeHelper.GetMemberAttribute(memberInfo, (RoleRequiredAttribute)null);

            if (roleRequiredAttribute == null)
            {
                return Enumerable.Empty<string>();
            }

            var accessRoles = GetAccessRoles(memberInfo);

            return accessRoles.GetMissingRoles(userRoles).Select(r => r.ToString());
        }

        private static AccessRoles GetAccessRoles(MemberInfo memberInfo)
        {
            var accessRoles = ReflectionHelper
                .ResolveAll<IPropertyAccessProvider>()
                .OrderByDescending(p => p.Priority)
                .Select(p => p.GetAccessRoles(memberInfo));

            var result = new AccessRoles();

            foreach (var accessRole in accessRoles)
            {
                result.RoleForRead = result.RoleForRead ?? accessRole.RoleForRead;
                result.RoleForWrite = result.RoleForWrite ?? accessRole.RoleForWrite;
            }

            return result;
        }
    }
}
