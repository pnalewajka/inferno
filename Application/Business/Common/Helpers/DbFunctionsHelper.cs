﻿using System;
using System.Data.Entity;

namespace Smt.Atomic.Business.Common.Helpers
{
    public static class DbFunctionsHelper
    {
        [DbFunction("Edm", "AddSeconds")]
        public static DateTime? AddSeconds(DateTime? time, int? seconds)
        {
            if (time == null || seconds == null)
                return null;

            return time.Value.AddSeconds(seconds.Value);
        }
    }
}
