﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Business.Common.Helpers
{
    /// <summary>
    /// Provides helper methods for easier card index configuration
    /// </summary>
    public static class CardIndexServiceHelper
    {
        public static IEnumerable<NamedFilter<TEntity>> BuildEnumBasedFilters<TEntity, TEnum>(
            Expression<Func<TEntity, object>> filteringColumnExpression,
            Func<TEnum, bool> valuePredicate = null)
            where TEntity : class, IEntity
        {
            if (valuePredicate == null)
            {
                valuePredicate = v => true;
            }

            var enumValues = EnumHelper.GetEnumValues<TEnum>().Where(valuePredicate);
            var filters = enumValues.Select(v => new NamedFilter<TEntity>(
                code: v.ToString(),
                condition: DynamicQueryHelper.BuildEnumFilteringCondition(filteringColumnExpression, (Enum)(object)v)));

            return filters;
        }

        public static IEnumerable<NamedFilter<TEntity>> BuildEnumBasedFilters<TEntity, TEnum>(
            Expression<Func<TEntity, long?>> filteringColumnExpression,
            Func<TEnum, bool> valuePredicate = null)
            where TEntity : class, IEntity
        {
            if (valuePredicate == null)
            {
                valuePredicate = v => true;
            }

            var isFlagged = TypeHelper.IsFlaggedEnum(typeof(TEnum));
            var enumValues = EnumHelper.GetEnumValues<TEnum>().Where(valuePredicate);
            var filters = enumValues.Select(v => new NamedFilter<TEntity>(
                code: v.ToString(),
                condition: DynamicQueryHelper.BuildEnumFilteringCondition(filteringColumnExpression, Convert.ToInt64(v), isFlagged)));

            return filters;
        }

        public static IEnumerable<NamedFilter<TEntity>> BuildEnumBasedFilters<TEntity, TEnum>(
            Expression<Func<TEntity, IEnumerable<long?>>> filteringColumnExpression,
            Func<TEnum, bool> valuePredicate = null)
            where TEntity : class, IEntity
        {
            if (valuePredicate == null)
            {
                valuePredicate = v => true;
            }

            var enumValues = EnumHelper.GetEnumValues<TEnum>().Where(valuePredicate);
            var filters = enumValues.Select(v => new NamedFilter<TEntity>(
                code: v.ToString(),
                condition: DynamicQueryHelper.BuildEnumFilteringCondition(filteringColumnExpression, Convert.ToInt64(v))));

            return filters;
        }

        public static IEnumerable<NamedFilter<TEntity>> BuildEnumBasedFilters<TEntity, TEnum>(
            Expression<Func<TEntity, object>> filteringColumnExpression,
            SecurityRoleType requiredRole,
            Func<TEnum, bool> valuePredicate = null)
            where TEntity : class, IEntity
        {
            if (valuePredicate == null)
            {
                valuePredicate = v => true;
            }

            var enumValues = EnumHelper.GetEnumValues<TEnum>().Where(valuePredicate);
            var filters = enumValues.Select(v => new NamedFilter<TEntity>(
                code: v.ToString(),
                condition: DynamicQueryHelper.BuildEnumFilteringCondition(filteringColumnExpression, (Enum)(object)v),
                requiredRole: requiredRole));

            return filters;
        }

        public static IEnumerable<SoftDeletableNamedFilter<TEntity>> BuildSoftDeletableFilters<TEntity>(
            Func<SoftDeletableStateFilter, bool> valuePredicate = null)
            where TEntity : class, IEntity, ISoftDeletable
        {
            if (valuePredicate == null)
            {
                valuePredicate = v => true;
            }

            var filters = new[]
            {
                new SoftDeletableNamedFilter<TEntity>(SoftDeletableStateFilter.Existing, null),
                new SoftDeletableNamedFilter<TEntity>(SoftDeletableStateFilter.Deleted, SecurityRoleType.CanViewSoftDeletedEntities),
                new SoftDeletableNamedFilter<TEntity>(SoftDeletableStateFilter.All, SecurityRoleType.CanViewSoftDeletedEntities),
            };

            return filters.Where(f => valuePredicate(f.State));
        }
    }
}
