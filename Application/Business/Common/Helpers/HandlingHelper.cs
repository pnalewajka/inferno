﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;

namespace Smt.Atomic.Business.Common.Helpers
{
    public static class HandlingHelper
    {
        /// <summary>
        /// General handler for catching business exceptions and converting them to alerts
        /// </summary>
        /// <param name="innerFunction"></param>
        /// <returns></returns>
        public  static BoolResult ExecuteWithExceptionHandling(Func<BoolResult> innerFunction)
        {
            try
            {
                return innerFunction();
            }
            catch (BusinessException businessException)
            {
                var alerts = new List<AlertDto>
                {
                    new AlertDto
                    {
                        Type = AlertType.Warning,
                        IsDismissable = true,
                        Message = businessException.Message
                    }
                };

                return new BoolResult(false, alerts);
            }
        }

        /// <summary>
        /// General handler for catching business exceptions and converting them to alerts, async version
        /// </summary>
        /// <param name="innerFunction"></param>
        /// <returns></returns>
        public static async Task<BoolResult> ExecuteWithExceptionHandlingAsync(Func<Task<BoolResult>> innerFunction)
        {
            return await ExecuteWithExceptionHandlingAsync(innerFunction, (s, a) => new BoolResult(s, a));
        }

        /// <summary>
        /// General handler for catching business exceptions and converting them to alerts, async version
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="innerFunction"></param>
        /// <param name="failedResult"></param>
        /// <returns></returns>
        public static async Task<TResult> ExecuteWithExceptionHandlingAsync<TResult>(Func<Task<TResult>> innerFunction, Func<bool, IEnumerable<AlertDto>, TResult> failedResult) where TResult : BoolResult
        {
            try
            {
                return await innerFunction();
            }
            catch (BusinessException businessException)
            {
                var alerts = new List<AlertDto>
                {
                    new AlertDto
                    {
                        Type = AlertType.Warning,
                        IsDismissable = true,
                        Message = businessException.Message
                    }
                };

                return failedResult(false, alerts);
            }
        }

    }
}
