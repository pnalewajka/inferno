﻿using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;

namespace Smt.Atomic.Business.Common.Helpers
{
    public static class ImageExtensions
    {
        /// <summary>
        /// Converts Image to byte array
        /// </summary>
        /// <param name="image">Image object</param>
        /// <returns>Image converted to byte array</returns>
        public static byte[] ToByteArray(this Image image)
        {
            var imageConverter = new ImageConverter();

            return (byte[])imageConverter.ConvertTo(image, typeof(byte[]));
        }

        /// <summary>
        /// Gets Image's content type
        /// </summary>
        /// <param name="image">Image object</param>
        /// <returns>Image's content type</returns>
        public static string GetContentType(this Image image)
        {
            var format = image.RawFormat;
            var codec = ImageCodecInfo.GetImageDecoders().First(c => c.FormatID == format.Guid);

            return codec.MimeType;
        }
    }
}
