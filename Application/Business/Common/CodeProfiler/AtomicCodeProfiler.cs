﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StackExchange.Profiling;

namespace Smt.Atomic.Business.Common.CodeProfiler
{
    public class AtomicCodeProfiler
    {
        public static IDisposable Profile(string name)
        {
            var profiler = MiniProfiler.Current;

            return profiler != null
                ? new AtomicCodeProfilerResult(profiler.Step(name))
                : new AtomicCodeProfilerResult(null);
        }
    }
}
