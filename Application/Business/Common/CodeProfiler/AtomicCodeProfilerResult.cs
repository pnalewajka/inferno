﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Common.CodeProfiler
{
    internal class AtomicCodeProfilerResult : IDisposable
    {
        private readonly IDisposable _step = null;

        public AtomicCodeProfilerResult(IDisposable step)
        {
            _step = step;
        }

        public void Dispose()
        {
            _step?.Dispose();
        }
    }
}
