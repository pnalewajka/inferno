﻿using Smt.Atomic.Data.Entities.Dto;
using System.Runtime.Serialization;

namespace Smt.Atomic.Business.Common.BusinessEvents
{
    [DataContract]
    [KnownType(typeof(long[]))]
    public class BulkEditEvent : BusinessEvent
    {
        [DataMember]
        public long[] RecordIds { get; set; }

        [DataMember]
        public string DtoType { get; set; }

        [DataMember]
        public string PropertyName { get; set; }

        [DataMember]
        public object PropertyValue { get; set; }

        [DataMember]
        public string CardIndexDataServiceType { get; set; }

        [DataMember]
        public long ModifyingUserId { get; set; }
    }
}
