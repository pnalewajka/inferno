namespace Smt.Atomic.Business.Common.Services
{
    public class RequiredRoleContext
    {
        public string Role { get; set; }
    }
}