﻿using System;
using System.IO;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Business.Common.Services
{
    internal class DocumentDownloadService : IDocumentDownloadService
    {
        public DocumentDownloadDto GetDocument([NotNull] string documentMappingIdentifier, long documentId, Guid? secret)
        {
            if (documentMappingIdentifier == null)
            {
                throw new ArgumentNullException(nameof(documentMappingIdentifier));
            }

            var documentMappingType = IdentifierHelper.GetTypeByIdentifier(documentMappingIdentifier);

            var documentMapping = ReflectionHelper.CreateInstanceWithIocDependencies(documentMappingType) as IDocumentMapping;

            if (documentMapping == null)
            {
                throw new InvalidDataException($"Cannot instantiate mapping for {documentMappingType.Name}");
            }

            return documentMapping.DownloadDocument(documentId, secret);
        }
    }
}
