﻿using System.Linq;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.GDPR;
using Smt.Atomic.Data.Entities.Modules.Recruitment;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Common.Services
{
    public class EmployeeRelatedEntityCreationService : IEmployeeRelatedEntityCreationService
    {
        private readonly IUnitOfWorkService<IGDPRDbScope> _unitOfWorkService;
        private readonly ISystemParameterService _systemParameterService;

        public EmployeeRelatedEntityCreationService(
            IUnitOfWorkService<IGDPRDbScope> unitOfWorkService,
            ISystemParameterService systemParameterService)
        {
            _unitOfWorkService = unitOfWorkService;
            _systemParameterService = systemParameterService;
        }

        public long AddRecommendingPersonBasedOnEmployeeId(long employeeId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employee = unitOfWork.Repositories.Employees.GetById(employeeId);
                var defaultCoordinatorId = GetDefaultCoordinatorId();

                var recommendingPerson = new RecommendingPerson
                {
                    CoordinatorId = defaultCoordinatorId,
                    EmailAddress = employee.Email,
                    FirstName = employee.FirstName,
                    LastName = employee.LastName
                };

                unitOfWork.Repositories.RecommendingPersons.Add(recommendingPerson);
                unitOfWork.Commit();

                return recommendingPerson.Id;
            }
        }

        public void AddEmployeeDataConsentBasedOnEmployeeId(long employeeId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var companyId = unitOfWork.Repositories.Employees.GetById(employeeId).CompanyId;

                if (!companyId.HasValue)
                {
                    companyId = GetDefaultCompanyId();
                }

                var dataConsent = new DataConsent
                {
                    Type = DataConsentType.RecommendingEmployeeConsent,
                    DataAdministratorId = GetDefaultCompanyId(),
                    DataOwnerId = GetDataOwnerIdForEmployee(employeeId)
                };

                unitOfWork.Repositories.DataConsents.Add(dataConsent);
                unitOfWork.Commit();
            }
        }

        private long GetDefaultCompanyId()
        {
            var defaultCompanyActiveDirectoryCode = _systemParameterService.GetParameter<string>(ParameterKeys.RecruitmentDefaultCompanyActiveDirectoryCode);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Companies
                    .Where(c => c.ActiveDirectoryCode == defaultCompanyActiveDirectoryCode)
                    .Select(c => c.Id)
                    .Single();
            }
        }

        private long GetDefaultCoordinatorId()
        {
            var defaultCoordinatorEmail = _systemParameterService.GetParameter<string>(ParameterKeys.RecruitmentDefaultCoordinatorEmail);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Employees
                    .Where(c => c.Email == defaultCoordinatorEmail)
                    .Select(c => c.Id)
                    .Single();
            }
        }

        private long GetDataOwnerIdForEmployee(long employeeId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.DataOwners
                    .Where(c => c.EmployeeId == employeeId && c.IsPotentialDuplicate == false)
                    .Select(c => c.Id)
                    .Single();
            }
        }
    }
}
