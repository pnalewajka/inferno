﻿using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Business.Common.Services
{
    class PropertyAccessProvider : IPropertyAccessProvider
    {
        public int Priority => 0;

        public AccessRoles GetAccessRoles(MemberInfo memberInfo)
        {
            var result = new AccessRoles();

            var roleRequiredAttribute = AttributeHelper.GetMemberAttribute<RoleRequiredAttribute>(memberInfo);

            if (roleRequiredAttribute != null)
            {
                result.RoleForRead = roleRequiredAttribute.ForRead;
                result.RoleForWrite = roleRequiredAttribute.ForWrite;
            }

            return result;
        }
    }
}
