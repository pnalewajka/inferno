﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Business.Common.Services
{
    /// <summary>
    /// Returns list of items for a dropdown of identifiable sub-classes of a given type.
    /// Sorted by identifier, so id is simply an index.
    /// </summary>
    /// <typeparam name="TClass">Base class for list items</typeparam>
    public class IdentifiableTypeListItemProvider<TClass> : IListItemProvider
        where TClass : class
    {
        public IEnumerable<ListItem> GetItems()
        {
            var subClasses = TypeHelper.GetAllSubClassesOf(typeof(TClass), true);

            return subClasses
                .Where(t => !t.IsAbstract)
                .Select(t => IdentifierHelper.GetTypeIdentifier(t, false))
                .Where(s => s != null)
                .OrderBy(s => s)
                .WithIndex()
                .Select(i => new ListItem { Id = i.Index, DisplayName = i.Item });
        }
    }
}