﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;

namespace Smt.Atomic.Business.Common.Services
{
    public class MonthsListItemProvider : IListItemProvider
    {
        public IEnumerable<ListItem> GetItems()
        {
            var formatInfo = CultureInfo.CurrentUICulture.DateTimeFormat;

            return Enumerable.Range(1, 12).Select(n => new ListItem
            {
                Id = n,
                DisplayName = formatInfo.GetMonthName(n),
            });
        }
    }
}
