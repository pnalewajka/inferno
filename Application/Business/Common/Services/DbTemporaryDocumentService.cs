﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Runtime;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Common.Services
{
    internal class DbTemporaryDocumentService : ITemporaryDocumentPromotionService
    {
        private readonly IUnitOfWorkService<IRuntimeDbScope> _unitOfWorkService;
        private readonly IPrincipalProvider _principalProvider;

        public DbTemporaryDocumentService(IUnitOfWorkService<IRuntimeDbScope> unitOfWorkService, IPrincipalProvider principalProvider)
        {
            _unitOfWorkService = unitOfWorkService;
            _principalProvider = principalProvider;
        }

        public Guid CreateTemporaryDocument(string documentName, string contentType, long contentLength, Guid? temporaryDocumentId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var newDocument = new TemporaryDocument
                {
                    Identifier = temporaryDocumentId ?? Guid.NewGuid(),
                    ContentLength = contentLength,
                    Name = documentName,
                    ContentType = contentType,
                    State = TemporaryDocumentState.Started,
                };

                unitOfWork.Repositories.TemporaryDocuments.Add(newDocument);

                unitOfWork.Commit();

                return newDocument.Identifier;
            }
        }

        public bool CompleteTemporaryDocument(Guid temporaryDocumentId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var temporaryDocument = unitOfWork.Repositories.TemporaryDocuments.Single(t => t.Identifier == temporaryDocumentId);

                if (temporaryDocument.State != TemporaryDocumentState.Failed)
                {
                    temporaryDocument.State = TemporaryDocumentState.Completed;
                    unitOfWork.Commit();

                    return true;
                }
            }

            return false;
        }

        public void AddChunk(Guid temporaryDocumentId, [NotNull] Stream dataStream)
        {
            if (dataStream == null)
            {
                throw new ArgumentNullException(nameof(dataStream));
            }

            var buffer = new byte[dataStream.Length];
            dataStream.Read(buffer, 0, (int)dataStream.Length);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var temporaryDocument = unitOfWork.Repositories.TemporaryDocuments.Single(t => t.Identifier == temporaryDocumentId);

                var newChunk = new TemporaryDocumentPart
                {
                    DocumentHeader = temporaryDocument,
                    Data = buffer
                };

                temporaryDocument.DocumentParts.Add(newChunk);

                unitOfWork.Commit();
            }
        }

        private byte[] Combine(IEnumerable<TemporaryDocumentPart> documentParts)
        {
            var temporaryDocumentParts = documentParts.ToList();

            var newBuffer = new byte[temporaryDocumentParts.Sum(p => p.Data.Length)];
            var offset = 0;

            foreach (var documentPart in temporaryDocumentParts)
            {
                Buffer.BlockCopy(documentPart.Data, 0, newBuffer, offset, documentPart.Data.Length);
                offset += documentPart.Data.Length;
            }

            return newBuffer;
        }

        public Stream GetContentStream(Guid temporaryDocumentId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var temporaryDocument = unitOfWork.Repositories.TemporaryDocuments.Single(t => t.Identifier == temporaryDocumentId);

                return new MemoryStream(Combine(temporaryDocument.DocumentParts));
            }
        }

        public void PromoteToEntity<TEntity>(Guid temporaryDocumentId, TEntity targetEntity,
            Expression<Func<TEntity, object>> nameColumnExpression,
            Expression<Func<TEntity, object>> mimeTypeColumnExpression,
            Expression<Func<TEntity, object>> contentLengthColumnExpression,
            Expression<Func<TEntity, object>> contentColumnExpression
            ) where TEntity : class, IEntity
        {
            PromoteToEntity(temporaryDocumentId, targetEntity, nameColumnExpression, mimeTypeColumnExpression, contentLengthColumnExpression, targetEntity, contentColumnExpression);
        }

        public void PromoteToEntity<TMetaEntity, TContentEntity>(Guid temporaryDocumentId,
            TMetaEntity targetMetaEntity,
            Expression<Func<TMetaEntity, object>> nameColumnExpression,
            Expression<Func<TMetaEntity, object>> mimeTypeColumnExpression,
            Expression<Func<TMetaEntity, object>> contentLengthColumnExpression,
            TContentEntity targetContentEntity,
            Expression<Func<TContentEntity, object>> contentColumnExpression
            )
            where TMetaEntity : class, IEntity
            where TContentEntity : class, IEntity
        {
            const string sqlPromoteCommand =
                @"EXEC [Runtime].[atomic_PromoteTemporaryDocumentToTuple] @CurrentUserId, @TargetMetaTableSchema, @TargetMetaTableName, @TargetMetaTableId, @TargetDataTableSchema, @TargetDataTableName, @TargetDataTableId, @DataColumnName, @MimeTypeColumnName, @ContentLengthColumnName, @NameColumnName, @TemporaryDocumentId";

            using (var unitOfWork = _unitOfWorkService.CreateExtended())
            {
                unitOfWork.ExecuteSqlCommand
                    (
                        sqlPromoteCommand,
                        new SqlParameter("@CurrentUserId", _principalProvider.Current.Id.Value),
                        new SqlParameter("@TargetMetaTableSchema", EntityHelper.GetTableSchema<TMetaEntity>()),
                        new SqlParameter("@TargetMetaTableName", EntityHelper.GetTableName<TMetaEntity>()),
                        new SqlParameter("@TargetMetaTableId", targetMetaEntity.Id),
                        new SqlParameter("@TargetDataTableSchema", EntityHelper.GetTableSchema<TContentEntity>()),
                        new SqlParameter("@TargetDataTableName", EntityHelper.GetTableName<TContentEntity>()),
                        new SqlParameter("@TargetDataTableId", targetContentEntity != null ? (object)(targetContentEntity.Id) : DBNull.Value),
                        new SqlParameter("@DataColumnName", PropertyHelper.GetPropertyName(contentColumnExpression)),
                        new SqlParameter("@MimeTypeColumnName", PropertyHelper.GetPropertyName(mimeTypeColumnExpression)),
                        new SqlParameter("@ContentLengthColumnName", PropertyHelper.GetPropertyName(contentLengthColumnExpression)),
                        new SqlParameter("@NameColumnName", PropertyHelper.GetPropertyName(nameColumnExpression)),
                        new SqlParameter("@TemporaryDocumentId", temporaryDocumentId)
                    );

                unitOfWork.Commit();
            }
        }

        public void CleanUpTemporaryDocuments(int maxAgeInSeconds)
        {
            const string sqlDeleteCommand = @"EXEC [Runtime].[atomic_CleanUpTemporaryDocuments] @AgeInSeconds";

            using (var unitOfWork = _unitOfWorkService.CreateExtended())
            {
                unitOfWork.ExecuteSqlCommand(sqlDeleteCommand,
                    new SqlParameter("@AgeInSeconds", maxAgeInSeconds));

                unitOfWork.Commit();
            }
        }

        public TemporaryDocumentDto GetDocumentMetadata(Guid temporaryDocumentId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var temporaryDocumentEntity = unitOfWork.Repositories.TemporaryDocuments.Single(t => t.Identifier == temporaryDocumentId);

                return new TemporaryDocumentDto
                {
                    ContentType = temporaryDocumentEntity.ContentType,
                    TemporaryDocumentId = temporaryDocumentEntity.Identifier,
                    Name = temporaryDocumentEntity.Name,
                    State = temporaryDocumentEntity.State
                };
            }
        }
    }
}
