﻿using System;
using System.Data.SqlClient;
using System.Linq;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Common.Services
{
    public class DataActivityLogService : IDataActivityLogService
    {
        private readonly IUnitOfWorkService<IGDPRDbScope> _unitOfWorkService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IEmployeeRelatedEntityCreationService _employeeRelatedEntityCreationService;
        private readonly ITimeService _timeService;

        const string LogEmployeeDataActivityStorageProcedureName = "LogEmployeeDataActivity";
        const string LogUserDataActivityStorageProcedureName = "LogUserDataActivity";
        const string LogCandidateDataActivityStorageProcedureName = "LogCandidateDataActivity";
        const string LogRecommendingPersonDataActivityStorageProcedureName = "LogRecommendingPersonDataActivity";
        const string LogDataOwnerDataActivityStorageProcedureName = "LogDataOwnerDataActivity";

        public DataActivityLogService(
            IUnitOfWorkService<IGDPRDbScope> unitOfWorkService,
            IPrincipalProvider principalProvider,
            IEmployeeRelatedEntityCreationService employeeRelatedEntityCreationService,
            ITimeService timeService)
        {
            _unitOfWorkService = unitOfWorkService;
            _principalProvider = principalProvider;
            _employeeRelatedEntityCreationService = employeeRelatedEntityCreationService;
            _timeService = timeService;
        }

        public void LogEmployeeDataActivity(long employeeId, ActivityType activityType, params string[] details)
        {
            InternalLogDataActivity(LogEmployeeDataActivityStorageProcedureName, employeeId, activityType, ReferenceType.Employee, employeeId, details);
        }

        public void LogEmployeeDataActivity(long employeeId, ActivityType activityType, ReferenceType referenceType, long referenceId, params string[] details)
        {
            InternalLogDataActivity(LogEmployeeDataActivityStorageProcedureName, employeeId, activityType, referenceType, referenceId, details);
        }

        public void LogUserDataActivity(long userId, ActivityType activityType, params string[] details)
        {
            InternalLogDataActivity(LogUserDataActivityStorageProcedureName, userId, activityType, ReferenceType.User, userId, details);
        }

        public void LogUserDataActivity(long userId, ActivityType activityType, ReferenceType referenceType, long referenceId, params string[] details)
        {
            InternalLogDataActivity(LogUserDataActivityStorageProcedureName, userId, activityType, referenceType, referenceId, details);
        }

        public void LogCandidateDataActivity(long candidateId, ActivityType activityType, params string[] details)
        {
            InternalLogDataActivity(LogCandidateDataActivityStorageProcedureName, candidateId, activityType, ReferenceType.Candidate, candidateId, details);
        }

        public void LogCandidateDataActivity(long candidateId, ActivityType activityType, ReferenceType referenceType, long referenceId, params string[] details)
        {
            InternalLogDataActivity(LogCandidateDataActivityStorageProcedureName, candidateId, activityType, referenceType, referenceId, details);
        }

        public void LogRecommendingPersonDataActivity(long recomendingPersonId, ActivityType activityType, params string[] details)
        {
            InternalLogDataActivity(LogRecommendingPersonDataActivityStorageProcedureName, recomendingPersonId, activityType, ReferenceType.RecommendingPerson, recomendingPersonId, details);
        }

        public void LogRecommendingPersonDataActivity(long recomendingPersonId, ActivityType activityType, ReferenceType referenceType, long referenceId, params string[] details)
        {
            InternalLogDataActivity(LogRecommendingPersonDataActivityStorageProcedureName, recomendingPersonId, activityType, referenceType, referenceId, details);
        }

        public void LogDataOwnerDataActivity(long dataOwnerId, ActivityType activityType, params string[] details)
        {
            InternalLogDataActivity(LogDataOwnerDataActivityStorageProcedureName, dataOwnerId, activityType, ReferenceType.DataOwner, dataOwnerId, details);
        }

        public void LogDataOwnerDataActivity(long dataOwnerId, ActivityType activityType, ReferenceType referenceType, long referenceId, params string[] details)
        {
            InternalLogDataActivity(LogDataOwnerDataActivityStorageProcedureName, dataOwnerId, activityType, referenceType, referenceId, details);
        }

        public void OnEmployeeAdded(long employeeId)
        {
            LogEmployeeDataActivity(employeeId, ActivityType.CreatedRecord);

            var recommendingPersonId = _employeeRelatedEntityCreationService.AddRecommendingPersonBasedOnEmployeeId(employeeId);
            LogRecommendingPersonDataActivity(recommendingPersonId, ActivityType.CreatedRecord);

            _employeeRelatedEntityCreationService.AddEmployeeDataConsentBasedOnEmployeeId(employeeId);
        }

        private void InternalLogDataActivity(string storedProcedureName, long identity, ActivityType activityType, ReferenceType referenceType, long referenceId, params string[] details)
        {
            if (!_principalProvider.IsSet)
            {
                return;
            }

            var detailsCount = GetDetailsCount(details);

            AddDataActivity(
                _principalProvider.Current.Id,
                storedProcedureName,
                identity,
                activityType,
                referenceType,
                referenceId,
                detailsCount > 0 ? details[0] : null,
                detailsCount > 1 ? details[1] : null,
                detailsCount > 2 ? details[2] : null);
        }

        private static int GetDetailsCount(string[] details)
        {
            const int maxDetailsCount = 3;
            var detailsCount = details.IsNullOrEmpty() ? 0 : details.Count();

            if (detailsCount > maxDetailsCount)
            {
                throw new ArgumentException("Incorrect details parameter count");
            }

            return detailsCount;
        }

        private void AddDataActivity(long? currentUserId, string procedureName, long identity, ActivityType activityType, ReferenceType referenceType, long referenceId, string detail1, string detail2, string detail3)
        {
            using (var unitOfWork = _unitOfWorkService.CreateExtended())
            {
                string sqlCommand = $"EXEC [GDPR].[{procedureName}] @identity, @activityType, @referenceType, @referenceId, @detail1, @detail2, @detail3, @currentUserId";
                unitOfWork.ExecuteSqlCommand(sqlCommand,
                    new SqlParameter("@identity", identity),
                    new SqlParameter("@activityType", activityType),
                    new SqlParameter("@referenceType", referenceType),
                    new SqlParameter("@referenceId", referenceId),
                    new SqlParameter("@detail1", (object)detail1 ?? DBNull.Value),
                    new SqlParameter("@detail2", (object)detail2 ?? DBNull.Value),
                    new SqlParameter("@detail3", (object)detail3 ?? DBNull.Value),
                    new SqlParameter("@currentUserId", currentUserId)
                    );
            }
        }
    }
}
