using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Business.Common.Services
{
    public abstract class EnumBasedCardIndexDataService<TDto, TEnum> : CustomDataCardIndexDataService<TDto> 
        where TEnum : struct
        where TDto : class
    {
        protected abstract TDto ConvertToDto(TEnum enumValue);

        protected override IList<TDto> GetAllRecords(object context = null)
        {
            var values = EnumHelper.GetEnumValues<TEnum>();
            IList<TDto> records = values.Select(ConvertToDto).ToList();

            return records;
        }

        public override TDto GetRecordById(long id)
        {
            return GetRecordInternal(id);
        }

        public override TDto GetRecordByIdOrDefault(long id)
        {
            return GetRecordInternal(id);
        }

        private TDto GetRecordInternal(long id)
        {
            return ConvertToDto((TEnum)Enum.ToObject(typeof(TEnum), id));
        }
    }
}