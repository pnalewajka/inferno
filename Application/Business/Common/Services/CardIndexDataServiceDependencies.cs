using Castle.Core.Logging;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.Common.Services
{
    public class CardIndexDataServiceDependencies<TDbScope> : ICardIndexServiceDependencies<TDbScope> 
        where TDbScope : IDbScope
    {
        public IEntityMergingService EntityMergingService { get; }
        public IClassMappingFactory ClassMappingFactory { get; }
        public IPrincipalProvider PrincipalProvider { get; }
        public IAnonymizationService AnonymizationService { get; }
        public IUnitOfWorkService<TDbScope> UnitOfWorkService { get; }
        public ITimeService TimeService { get; }
        public IBusinessEventPublisher BusinessEventPublisher { get; }
        public ISystemParameterService SystemParameters { get; }
        public ICacheInvalidationService CacheInvalidationService { get; }
        public ILogger Logger { get; }

        public CardIndexDataServiceDependencies(
            IEntityMergingService entityMergingService,
            IClassMappingFactory classMappingFactory,
            IPrincipalProvider principalProvider,
            IAnonymizationService anonymizationService,
            IUnitOfWorkService<TDbScope> unitOfWorkService,
            ITimeService timeService, 
            IBusinessEventPublisher businessEventPublisher,
            ISystemParameterService systemParameters,
            ICacheInvalidationService cacheInvalidationService,
            ILogger logger)
        {
            BusinessEventPublisher = businessEventPublisher;
            EntityMergingService = entityMergingService;
            ClassMappingFactory = classMappingFactory;
            PrincipalProvider = principalProvider;
            AnonymizationService = anonymizationService;
            UnitOfWorkService = unitOfWorkService;
            TimeService = timeService;
            SystemParameters = systemParameters;
            CacheInvalidationService = cacheInvalidationService;
            Logger = logger;
        }
    }
}