using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.Business.Common.Services
{
    /// <summary>
    /// Returns list of items for a dropdown based on the enum.
    /// Ids are based on the enum values, display name is taken from description attribute
    /// </summary>
    /// <typeparam name="TEnum"></typeparam>
    public class EnumBasedListItemProvider<TEnum> : IListItemProvider
        where TEnum : struct
    {
        private IPrincipalProvider _principalProvider;

        public EnumBasedListItemProvider(IPrincipalProvider principalProvider)
        {
            _principalProvider = principalProvider;
        }

        public virtual IEnumerable<ListItem> GetItems()
        {
            var isFlaggedEnum = TypeHelper.IsFlaggedEnum(typeof(TEnum));

            return EnumHelper
                .GetEnumValues(typeof(TEnum))
                .Where(enumValue => !isFlaggedEnum || IsPowerOfTwo(Convert.ToInt64(enumValue)))
                .Where(IsAccessibleForCurrentUser)
                .Select(enumValue => new ListItem
                {
                    Id = Convert.ToInt64(enumValue),
                    DisplayName = enumValue.GetDescriptionOrValue()
                });
        }

        private static bool IsPowerOfTwo(long n)
        {
            return n != 0 && (n & (n - 1)) == 0;
        }

        private bool IsAccessibleForCurrentUser(Enum value)
        {
            var attributes = AttributeHelper.GetEnumFieldAttributes<RoleRequiredAttribute>(value);

            foreach (var attribute in attributes)
            {
                if (!_principalProvider.Current.IsInRole(attribute.ForRead))
                {
                    return false;
                }
            }

            return true;
        }
    }
}