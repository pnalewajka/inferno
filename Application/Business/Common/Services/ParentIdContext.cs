using Smt.Atomic.Business.Common.Interfaces;

namespace Smt.Atomic.Business.Common.Services
{
    public class ParentIdContext : IParentIdContext
    {
        public long ParentId { get; set; }
    }
}