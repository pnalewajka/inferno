﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Business.Common.Services
{
    public class BooleanBasedListItemProvider<TResourceType> : IListItemProvider
    {
        private const string TrueDisplayNameKey = "TrueDisplayName";
        private const string FalseDisplayNameKey = "FalseDisplayName";

        public IEnumerable<ListItem> GetItems()
        {
            var resourceManager = ResourceManagerHelper.GetResourceManager(typeof(TResourceType));

            return new[]
            {
                new ListItem
                {
                    Id = 1,
                    // ReSharper disable once ResourceItemNotResolved
                    DisplayName = resourceManager.GetString(TrueDisplayNameKey)
                },
                new ListItem
                {
                    Id = 0,
                    // ReSharper disable once ResourceItemNotResolved
                    DisplayName = resourceManager.GetString(FalseDisplayNameKey)
                }
            };
        }
    }
}
