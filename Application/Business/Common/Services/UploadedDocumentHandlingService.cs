using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Abstracts;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.Common.Services
{
    internal class UploadedDocumentHandlingService : IUploadedDocumentHandlingService
    {
        private readonly ITemporaryDocumentPromotionService _temporaryDocumentService;

        public UploadedDocumentHandlingService(ITemporaryDocumentPromotionService temporaryDocumentService)
        {
            _temporaryDocumentService = temporaryDocumentService;
        }

        public void MergeDocumentCollections<TEntity, TDbScope>(IUnitOfWork<TDbScope> unitOfWork, ICollection<TEntity> dbCollection, IEnumerable<DocumentDto> inputDocumentCollection, Func<DocumentDto, TEntity> mappingFunction)
            where TEntity : class, IEntity
            where TDbScope : IDbScope
        {
            var picturesList = dbCollection.GetIds();
            var inputFileCollection = inputDocumentCollection.ToList();
            var modifiedList = inputFileCollection.Where(p => !p.IsTemporary).Select(p => p.DocumentId).OfType<long>().ToList();
            var removalList = picturesList.Except(modifiedList);

            var repositoryFactory = ((IRepositoryFactory)unitOfWork.Repositories);
            var repository = repositoryFactory.Create<TEntity>();

            foreach (var fileId in removalList)
            {
                var entity = dbCollection.Single(p => p.Id == fileId);
                dbCollection.Remove(entity);
                repository.Delete(entity);
            }

            foreach (var file in inputFileCollection.Where(f => f.IsTemporary))
            {
                var newMetaEntity = mappingFunction(file);

                file.TargetEntity = newMetaEntity;
                dbCollection.Add(newMetaEntity);
            }
        }

        public TEntity UpdateRelatedDocument<TEntity, TDbScope>(IUnitOfWork<TDbScope> unitOfWork, TEntity dbDocument, DocumentDto inputDocument, Func<DocumentDto, TEntity> mappingFunction)
            where TEntity : class, IEntity
            where TDbScope : IDbScope
        {

            var repositoryFactory = ((IRepositoryFactory)unitOfWork.Repositories);
            var repository = repositoryFactory.Create<TEntity>();

            if (dbDocument != null && (inputDocument == null || inputDocument.IsTemporary))
            {
                repository.Delete(dbDocument);
                dbDocument = null;
            }

            if (inputDocument != null && inputDocument.IsTemporary)
            {
                var newMetaEntity = mappingFunction(inputDocument);

                inputDocument.TargetEntity = newMetaEntity;
                dbDocument = newMetaEntity;
            }

            return dbDocument;
        }

        public void PromoteTemporaryDocuments<TMetaEntity, TContentEntity, TMapping, TDbScope>(IEnumerable<DocumentDto> inputDocumentCollection)
            where TMetaEntity : class, IEntity
            where TContentEntity : class, IEntity where TMapping : DocumentMapping<TMetaEntity, TContentEntity, TDbScope>
            where TDbScope : IDbScope
        {
            var documentMapping = ReflectionHelper.CreateInstanceWithIocDependencies<TMapping>();

            PromoteTemporaryDocuments(inputDocumentCollection, documentMapping.NameColumnExpression, documentMapping.ContentTypeColumnExpression, documentMapping.ContentLengthColumnExpression, documentMapping.ContentColumnExpression);
        }


        public void PromoteTemporaryDocuments<TMetaEntity, TContentEntity>(IEnumerable<DocumentDto> inputDocumentCollection,
            Expression<Func<TMetaEntity, object>> nameColumnExpression,
            Expression<Func<TMetaEntity, object>> contentTypeColumnExpression,
            Expression<Func<TMetaEntity, object>> contentLengthColumnExpression,
            Expression<Func<TContentEntity, object>> contentColumnExpression
            )
            where TMetaEntity : class, IEntity
            where TContentEntity : class, IEntity
        {
            if (inputDocumentCollection == null)
            {
                return;
            }

            foreach (var file in inputDocumentCollection.Where(f => f.IsTemporary))
            {
                if (file.TemporaryDocumentId == null)
                {
                    throw new InvalidDataException();
                }

                // ReSharper disable once PossibleInvalidOperationException
                _temporaryDocumentService.PromoteToEntity(file.TemporaryDocumentId.Value, (TMetaEntity)file.TargetEntity, nameColumnExpression,
                    contentTypeColumnExpression, contentLengthColumnExpression, null, contentColumnExpression);
            }
        }
    }
}