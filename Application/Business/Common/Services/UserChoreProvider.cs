﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Data.Common.Extensions;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.Common.Services
{
    public class UserChoreProvider
        : IUserChoreProvider
    {
        private readonly IReadOnlyUnitOfWorkService<IDbScope> _unitOfWorkService;
        private readonly IEnumerable<IChoreProvider> _choreProviders;

        public UserChoreProvider(
            IReadOnlyUnitOfWorkService<IDbScope> unitOfWorkService,
            IEnumerable<IChoreProvider> choreProviders)
        {
            _unitOfWorkService = unitOfWorkService;
            _choreProviders = choreProviders;
        }

        public int GetActiveChoreCount()
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var queries = GetActiveChoreQueries(unitOfWork);

                if (!queries.Any())
                {
                    return 0;
                }

                var choreCounts = queries
                    .Select(q => q.CountAsync())
                    .ToArray();

                Task.WaitAll(choreCounts);

                var sum = choreCounts.Sum(c => c.Result);

                return sum;
            }
        }

        public IList<ChoreDto> GetActiveChores()
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var queries = GetActiveChoreQueries(unitOfWork);

                if (!queries.Any())
                {
                    return new ChoreDto[0];
                }

                var chores = queries
                    .Select(q => q.ToListAsync())
                    .ToArray();

                Task.WaitAll(chores);

                var choreDtos = chores
                    .SelectMany(c => c.Result)
                    .ToList();

                // Populate IDs
                for (var i = 0; i < choreDtos.Count; ++i)
                {
                    choreDtos[i].Id = i + 1;
                }

                return choreDtos;
            }
        }

        private ICollection<IQueryable<ChoreDto>> GetActiveChoreQueries(IReadOnlyUnitOfWork<IDbScope> unitOfWork)
        {
            var repositoryFactory = (IReadOnlyRepositoryFactory)unitOfWork.Repositories;
            var queries = new List<IQueryable<ChoreDto>>();

            foreach (var choreProvider in _choreProviders)
            {
                var query = choreProvider.GetActiveChores((IReadOnlyRepositoryFactory)unitOfWork.Repositories);

                if (query != null)
                {
                    queries.Add(query);
                }
            }

            return queries;
        }
    }
}
