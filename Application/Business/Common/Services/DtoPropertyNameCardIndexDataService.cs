﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Business.Common.Services
{
    public class DtoPropertyNameCardIndexDataService : CustomDataCardIndexDataService<PropertyNameDto>, IDtoPropertyNameCardIndexDataService
    {
        private IList<PropertyNameDto> _records;

        public string GetPropertyNameById(long id, Type viewModelType)
        {
            return Initialize(viewModelType).Single(p => p.Id == id).PropertyName;
        }

        public override PropertyNameDto GetRecordById(long id)
        {
            return _records.Single(i => i.Id == id);
        }

        public override PropertyNameDto GetRecordByIdOrDefault(long id)
        {
            return _records.SingleOrDefault(i => i.Id == id);
        }

        protected override IList<PropertyNameDto> GetAllRecords(object context = null)
        {
            var modelFieldNameContext = context as DtoPropertyCardIndexContext;

            if (modelFieldNameContext != null)
            {
                var modelType = IdentifierHelper.GetTypeByIdentifier(modelFieldNameContext.ViewModelIdentifier);
                _records = Initialize(modelType);
            }

            return _records;
        }

        protected override IEnumerable<Expression<Func<PropertyNameDto, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return model => model.PropertyName;
        }

        private IList<PropertyNameDto> Initialize(Type modelType)
        {
            if (_records == null)
            {
                _records = TypeHelper.GetPublicInstanceProperties(modelType)
                                    .Where(property => property.CanWrite)
                                    .Where(property=> AttributeHelper.GetPropertyAttribute<AllowBulkEditAttribute>(property) != null)
                                    .Select(property => new
                                    {
                                        PropertyName = property.Name,
                                        Property = property
                                    })
                                    .OrderBy(identity => identity.PropertyName)
                                    .Select((identity, index) => new PropertyNameDto
                                    {
                                        Id = index + 1,
                                        PropertyName = identity.PropertyName,
                                        ViewModelType = modelType
                                    }).ToList();
            }

            return _records.ToList();
        }
    }
}
