﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Resources;

namespace Smt.Atomic.Business.Common.Services
{
    public class YesNoItemProvider : IListItemProvider
    {
        public IEnumerable<ListItem> GetItems()
        {
            return new[]
            {
                new ListItem { Id = 1, DisplayName = CommonResources.Yes },
                new ListItem { Id = 0, DisplayName = CommonResources.No }
            };
        }
    }
}
