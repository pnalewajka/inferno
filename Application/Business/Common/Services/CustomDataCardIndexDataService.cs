using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Business.Common.Services
{
    public abstract class CustomDataCardIndexDataService<TDto> : ICardIndexDataService<TDto> where TDto : class
    {
        protected abstract IList<TDto> GetAllRecords(object context = null);

        public CardIndexRecords<TDto> GetRecords(QueryCriteria criteria = null)
        {
            criteria = criteria ?? new QueryCriteria();

            var allRecords = GetAllRecords(criteria.Context);
            var paginationData = ApplyFiltering(allRecords, criteria);

            return new CardIndexRecords<TDto>(paginationData.Records, criteria.CurrentPageIndex, paginationData.LastPageIndex, paginationData.RecordCount.Result);
        }

        public DisposableQueryable<TDto> GetQueryableRecords([CanBeNull] QueryCriteria criteria)
        {
            criteria = criteria ?? new QueryCriteria();

            var allRecords = GetAllRecords(criteria.Context);
            var paginationData = ApplyFiltering(allRecords, criteria);

            return new DisposableQueryable<TDto>(paginationData.Records.AsQueryable());
        }

        private IList<TDto> ApplyFilteringCriteria(IList<TDto> records, List<FilterCodeGroup> filterGroups, Dictionary<string, IFilteringObject> filterObjects)
        {
            if (filterGroups.IsNullOrEmpty())
            {
                return records;
            }

            foreach (var filterGroup in filterGroups)
            {
                Expression<Func<TDto, bool>> filterGroupExpression = null;

                foreach (var filterCode in filterGroup.Codes)
                {
                    var filter = NamedFilters[filterCode];

                    IFilteringObject filterObject = null;

                    if (filterObjects.ContainsKey(filterCode))
                    {
                        filterObject = filterObjects[filterCode];
                    }

                    if (filterGroup.IsConjunction)
                    {
                        filterGroupExpression = filterGroupExpression == null
                            ? filter.GetExpression(filterObject)
                            : filterGroupExpression.And(filter.GetExpression(filterObject));
                    }
                    else
                    {
                        filterGroupExpression = filterGroupExpression == null
                            ? filter.GetExpression(filterObject)
                            : filterGroupExpression.Or(filter.GetExpression(filterObject));
                    }
                }

                if (filterGroupExpression == null)
                {
                    throw new InvalidDataException();
                }

                records = records.AsQueryable().Where(filterGroupExpression).ToList();
            }

            return records;
        }

        private IList<TDto> ApplyOrderByCriteria(IList<TDto> records, IReadOnlyCollection<SortingCriterion> orderByList)
        {
            if (!orderByList.IsNullOrEmpty())
            {
                IOrderedEnumerable<TDto> orderedRecords = records.OrderBy(r => 0);
                var index = 0;
                
                foreach (var orderBy in orderByList)
                {
                    var dtoColumnName = orderBy.GetSortingColumnName();
                    var ascending = orderBy.Ascending;
                    // ReSharper disable once LoopCanBeConvertedToQuery

                    var orderByExpression = DynamicQueryHelper.BuildOrderByExpression<TDto>(dtoColumnName).Compile();

                    if (index++ == 0)
                    {
                        orderedRecords = ascending
                            ? records.OrderBy(orderByExpression)
                            : records.OrderByDescending(orderByExpression);
                    }
                    else
                    {
                        orderedRecords = ascending
                            ? orderedRecords.ThenBy(orderByExpression)
                            : orderedRecords.ThenByDescending(orderByExpression);
                    }
                }

                records = orderedRecords.ToList();
            }

            return records;
        }

        private IList<TDto> ApplySearchTextCriteria(IList<TDto> records, string searchText, QueryCriteria criteria)
        {
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                var words = searchText.ToLower().Split(new[] { " ", "\t" }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var word in words)
                {
                    Expression<Func<TDto, bool>> allConditionJoinedByOr = null;

                    // ReSharper disable once LoopCanBeConvertedToQuery
                    foreach (var searchColumn in GetSearchColumns(criteria.SearchCriteria))
                    {
                        var condition = DynamicQueryHelper.BuildSearchCondition(searchColumn, word);
                        allConditionJoinedByOr = allConditionJoinedByOr == null
                            ? condition
                            : condition.Or(allConditionJoinedByOr);
                    }

                    if (allConditionJoinedByOr == null)
                    {
                        throw new InvalidOperationException();
                    }

                    records = records.Where(allConditionJoinedByOr.Compile()).ToList();
                }
            }

            return records;
        }

        protected virtual IList<TDto> ApplyContextFiltering(IList<TDto> records, object context)
        {
            return records;
        }

        public abstract TDto GetRecordById(long id);

        public abstract TDto GetRecordByIdOrDefault(long id);

        public IDictionary<long, TDto> GetRecordsByIds(long[] ids, bool filtered = true)
        {
            return ids.Distinct().ToDictionary(i => i, GetRecordById);
        }

        public AddRecordResult AddRecord(TDto record, object context = null)
        {
            throw new NotSupportedException();
        }

        public EditRecordResult EditRecord(TDto record, object context = null)
        {
            throw new NotSupportedException();
        }

        public BulkEditRecordResult BulkEditRecords(BulkEditDto dto, object context = null)
        {
            throw new NotSupportedException();
        }

        public DeleteRecordsResult DeleteRecords(IEnumerable<long> idsToDelete, object context, bool isDeletePermanent = false)
        {
            throw new NotSupportedException();
        }

        public TDto GetDefaultNewRecord()
        {
            throw new NotSupportedException();
        }

        protected virtual NamedFilters<TDto> NamedFilters => null;

        protected abstract IEnumerable<Expression<Func<TDto, object>>> GetSearchColumns(SearchCriteria searchCriteria);

        public bool SupportsSearchByColumns => !GetSearchColumns(new SearchCriteria()).IsNullOrEmpty();

        public bool SupportsNamedFilters => !(NamedFilters == null || NamedFilters.IsNullOrEmpty());

        public long GetRecordId(TDto dto)
        {
            throw new InvalidOperationException("This method should not be called for enum-based services");
        }

        CardIndexRecords<object> IDataImportService.GetRecordsAsObjects(QueryCriteria criteria)
        {
            var result = GetRecords(criteria);

            return new CardIndexRecords<object>(result.Rows.Cast<object>().ToList(), result.CurrentPageIndex, result.LastPageIndex, result.RecordCount, result.Alerts);
        }

        public ImportRecordResult ImportRecord(object dto, ImportBehavior importBehavior)
        {
            throw new NotImplementedException();
        }
        
        public virtual void BeforeBulkImportFinished(BeforeBulkImportFinishedEventArgs eventArgs)
        {
            
        }

        public IEnumerable<KeyValuePair<string, SecurityRoleType>> GetFilterRoles()
        {
            return NamedFilters.GetRoles();
        }
        
        private PaginationData<IList<TDto>> ApplyFiltering(IList<TDto> records, QueryCriteria criteria)
        {
            criteria.CurrentPageIndex = criteria.CurrentPageIndex ?? 0;

            records = ApplyContextFiltering(records, criteria.Context);
            records = ApplyFilteringCriteria(records, criteria.Filters, criteria.FilterObjects ?? new Dictionary<string, IFilteringObject>());
            records = ApplySearchTextCriteria(records, criteria.SearchPhrase, criteria);

            var recordCount = records.Count();

            records = ApplyOrderByCriteria(records, criteria.OrderBy);

            if (criteria.CurrentPageIndex.Value != 0)
            {
                Debug.Assert(criteria.PageSize != null, "criteria.PageSize != null");

                int recordsToSkip = criteria.CurrentPageIndex.Value*criteria.PageSize.Value;
                records = records.Skip(recordsToSkip).ToList();
            }

            if (criteria.PageSize.HasValue)
            {
                records = records.Take(criteria.PageSize.Value).ToList();
            }

            return new PaginationData<IList<TDto>>
            {
                Records = records,
                PageSize = criteria.PageSize,
                RecordCount = Task.FromResult(recordCount),
            };
        }
    }
}