﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.Business.Common.Services
{
    internal class AnonymizationService : IAnonymizationService
    {
        private readonly IPrincipalProvider _principalProvider;
        private HashSet<string> _userRoles;

        private HashSet<string> UserRoles => _userRoles ?? (_userRoles = _principalProvider.Current.Roles.ToHashSet());

        public AnonymizationService(IPrincipalProvider principalProvider)
        {
            _principalProvider = principalProvider;
        }

        public void Anonymize(object model)
        {
            if (model == null)
            {
                return;
            }

            var modelType = model.GetType();

            if (!ShouldBeAnonymized(modelType))
            {
                return;
            }

            var securableProperties = TypeHelper.GetPublicInstancePropertiesWithAttribute<RoleRequiredAttribute>(modelType).ToList();

            foreach (var securableProperty in securableProperties)
            {
                var accessMode = SecurityHelper.GetPropertyAccessMode(securableProperty, UserRoles);

                if (accessMode != PropertyAccessMode.NoAccess)
                {
                    continue;
                }

                var actualValue = securableProperty.GetValue(model);

                if (securableProperty.PropertyType.IsEnum)
                {
                    securableProperty.SetValue(model, AnonymizeEnumProperty(securableProperty, actualValue));
                }
                else if (Nullable.GetUnderlyingType(securableProperty.PropertyType)!=null)
                {
                    securableProperty.SetValue(model, null);
                }
                else if (securableProperty.PropertyType == typeof (string))
                {
                    securableProperty.SetValue(model, AnonymizeStringProperty(securableProperty, actualValue));
                }
                else if (securableProperty.PropertyType == typeof(DateTime))
                {
                    securableProperty.SetValue(model, AnonymizeDateTimeProperty(securableProperty, actualValue));
                }
                else if (TypeHelper.IsNumeric(securableProperty.PropertyType, false))
                {
                    securableProperty.SetValue(model,
                        Convert.ChangeType(AnonymizeNumberProperty(securableProperty, actualValue),
                            securableProperty.PropertyType));
                }
                else if (!securableProperty.PropertyType.IsValueType && !securableProperty.PropertyType.IsInterface)
                {
                    securableProperty.SetValue(model, AnonymizeComplexTypeProperty(securableProperty, actualValue));
                }
            }
        }

        public bool ShouldBeAnonymized<TModel>()
        {
            return ShouldBeAnonymized(typeof(TModel));
        }

        public bool ShouldBeAnonymized(Type modelType)
        {
            return TypeHelper.GetPublicInstancePropertiesWithAttribute<RoleRequiredAttribute>(modelType).Any();
        }

        public IEnumerable<string> GetReadRestrictedProperties(Type modelType)
        {
            var securableProperties =
                TypeHelper.GetPublicInstancePropertiesWithAttribute<RoleRequiredAttribute>(modelType);

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var securableProperty in securableProperties)
            {
                if (SecurityHelper.GetPropertyAccessMode(securableProperty, UserRoles) == PropertyAccessMode.NoAccess)
                {
                    yield return securableProperty.Name;
                }
            }
        }

        public IEnumerable<string> GetReadRestrictedPropertiesForRelatedObject<TSource, TDestination>(IClassMapping<TDestination, TSource> classMapping,
            HashSet<string> readRestrictedSourceProperties) where TSource : class where TDestination : class
        {
            return GetReadRestrictedPropertiesForRelatedObject(classMapping, readRestrictedSourceProperties, typeof (TDestination), typeof (TSource));
        }

        public IEnumerable<string> GetReadRestrictedPropertiesForRelatedObject(IClassMapping classMapping,
            HashSet<string> readRestrictedSourceProperties, Type sourceType, Type destinationType)
        {
            var fieldMappings = classMapping.GetFieldMappings(sourceType, destinationType);

            return fieldMappings.Where(f => readRestrictedSourceProperties.Contains(f.Key)).SelectMany(fieldMapping => fieldMapping.Value);
        }

        public IEnumerable<string> GetMissingRoles<TModel>([NotNull] IEnumerable<string> fields)
        {
            if (fields == null)
            {
                throw new ArgumentNullException(nameof(fields));
            }

            return GetMissingRoles(typeof (TModel), fields);
        }

        public IEnumerable<string> GetMissingRoles(Type modelType, [NotNull] IEnumerable<string> fields)
        {
            if (fields == null)
            {
                throw new ArgumentNullException(nameof(fields));
            }

            var securableProperties =
                TypeHelper.GetPublicInstancePropertiesWithAttribute<RoleRequiredAttribute>(modelType);

            var userRoles = _principalProvider.Current.Roles.ToHashSet();

            var fieldsHashSet = fields.ToHashSet();

            return
                securableProperties.Where(s => fieldsHashSet.Contains(s.Name))
                    .SelectMany(
                        s => SecurityHelper.GetPropertyMissingRoles(s, userRoles));
        }

        /// <summary>
        /// Anonymize value returning specific enum
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <param name="actualValue"></param>
        /// <returns></returns>
        protected virtual object AnonymizeEnumProperty(PropertyInfo propertyInfo, object actualValue)
        {
            return EnumHelper.GetOutOfRangeValue(propertyInfo.PropertyType);
        }

        /// <summary>
        /// Anonymize value returning string
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <param name="actualValue"></param>
        /// <returns></returns>
        protected virtual string AnonymizeStringProperty(PropertyInfo propertyInfo, object actualValue)
        {
            return string.Empty;
        }

        /// <summary>
        /// Anonymize value returning specific numeric value
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <param name="actualValue"></param>
        /// <returns></returns>
        protected virtual long AnonymizeNumberProperty(PropertyInfo propertyInfo, object actualValue)
        {
            return 0L;
        }

        /// <summary>
        /// Anonymize value returning specific datetime
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <param name="actualValue"></param>
        /// <returns></returns>
        protected virtual DateTime AnonymizeDateTimeProperty(PropertyInfo propertyInfo, object actualValue)
        {
            return DateTime.MinValue;
        }

        /// <summary>
        /// Anonymize value returning new instance of complex object
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <param name="actualValue"></param>
        /// <returns></returns>
        protected virtual object AnonymizeComplexTypeProperty(PropertyInfo propertyInfo, object actualValue)
        {
            return ReflectionHelper.CreateInstance(propertyInfo.PropertyType);
        }
    }
}
