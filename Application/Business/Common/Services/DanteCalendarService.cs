using System.IO;
using System.Linq;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Common.Services
{
    public class DanteCalendarService : IDanteCalendarService
    {
        private readonly IUnitOfWorkService<IOrganizationScope> _organizationUnitOfWorkService;
        private readonly IUnitOfWorkService<IConfigurationDbScope> _configurationUnitOfWorkService;
        private readonly ISystemParameterService _systemParameterService;

        public DanteCalendarService(
            IUnitOfWorkService<IOrganizationScope> organizationUnitOfWorkService,
            IUnitOfWorkService<IConfigurationDbScope> configurationUnitOfWorkService,
            ISystemParameterService systemParameterService)
        {
            _organizationUnitOfWorkService = organizationUnitOfWorkService;
            _configurationUnitOfWorkService = configurationUnitOfWorkService;
            _systemParameterService = systemParameterService;
        }

        public long GetCalendarIdByOrgUnitId(long? orgUnitId)
        {
            if (!orgUnitId.HasValue)
            {
                return GetDefaultCalendarId();
            }

            using (var unitOfWork = _organizationUnitOfWorkService.Create())
            {
                var orgUnit = unitOfWork.Repositories.OrgUnits.FirstOrDefault(x => x.Id == orgUnitId.Value);

                if (orgUnit == null)
                {
                    return GetDefaultCalendarId();
                }

                while (orgUnit != null)
                {
                    if (orgUnit.Calendar != null)
                    {
                        return orgUnit.Calendar.Id;
                    }

                    orgUnit = orgUnit.Parent;
                }

                return GetDefaultCalendarId();
            }
        }

        public long GetDefaultCalendarId()
        {
            using (var scope = _configurationUnitOfWorkService.Create())
            {
                var defaultCalendarCode = _systemParameterService.GetParameter<string>(ParameterKeys.AllocationDefaultCalendarCode);
                var calendar = scope.Repositories.Calendars.FirstOrDefault(c => c.Code == defaultCalendarCode);

                if (calendar == null)
                {
                    throw new InvalidDataException("Unknown parameter: " + ParameterKeys.AllocationDefaultCalendarCode);
                }

                return calendar.Id;
            }
        }
    }
}
