﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Castle.Core.Logging;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Common.Extensions;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using QueryableExtensions = System.Data.Entity.QueryableExtensions;

namespace Smt.Atomic.Business.Common.Services
{
    public abstract class ReadOnlyCardIndexDataService<TDto, TEntity, TDbScope> : ICardIndexDataService<TDto>
        where TEntity : class, IEntity
        where TDto : class
        where TDbScope : IDbScope
    {
        private readonly IClassMappingFactory _mappingFactory;
        private readonly IClassMapping<TEntity, TDto> _entityToDtoMapping;
        private readonly ILogger _logger;
        private NamedFilters<TEntity> _namedFiltersInternal;
        private readonly string[] _softDeletableStateFilter = Enum.GetNames(typeof(SoftDeletableStateFilter)).Select(s => s.ToLowerInvariant()).ToArray();

        protected IEntityMergingService EntityMergingService { get; set; }
        protected IPrincipalProvider PrincipalProvider { get; set; }
        protected IAnonymizationService AnonymizationService { get; set; }
        protected IUnitOfWorkService<TDbScope> UnitOfWorkService { get; set; }
        protected ITimeService TimeService { get; set; }
        protected ISystemParameterService SystemParameters { get; set; }

        protected ReadOnlyCardIndexDataService(ICardIndexServiceDependencies<TDbScope> dependencies)
        {
            _mappingFactory = dependencies.ClassMappingFactory;
            _entityToDtoMapping = _mappingFactory.CreateMapping<TEntity, TDto>();
            _logger = dependencies.Logger;

            EntityMergingService = dependencies.EntityMergingService;
            PrincipalProvider = dependencies.PrincipalProvider;
            AnonymizationService = dependencies.AnonymizationService;
            UnitOfWorkService = dependencies.UnitOfWorkService;
            SystemParameters = dependencies.SystemParameters;

            TimeService = dependencies.TimeService;
        }

        private NamedFilters<TEntity> NamedFiltersInternal
        {
            get
            {
                if (_namedFiltersInternal == null)
                {
                    InitializeNamedFiltersInternal();
                }

                return _namedFiltersInternal;
            }

            set { _namedFiltersInternal = value; }
        }

        public virtual IQueryable<TEntity> ApplyContextFiltering(IQueryable<TEntity> records, object context)
        {
            return records;
        }

        protected virtual IQueryable<TEntity> GetFilteredRecords(IQueryable<TEntity> records)
        {
            return records.Filtered();
        }

        public virtual void SetContext([NotNull] TEntity entity, [CanBeNull] object context)
        {
        }

        /// <summary>
        /// Configure EF calls by adding "includes" - relevant dependent database objects, to be loaded
        /// eagerly rather than using lazy loading approach
        /// </summary>
        /// <param name="sourceQueryable"></param>
        /// <returns></returns>
        protected virtual IQueryable<TEntity> ConfigureIncludes(IQueryable<TEntity> sourceQueryable)
        {
            var navigationProperties = _entityToDtoMapping.NavigationProperties;

            return navigationProperties.Aggregate(sourceQueryable, QueryableExtensions.Include);
        }

        public virtual CardIndexRecords<TDto> GetRecords([CanBeNull] QueryCriteria criteria)
        {
            criteria = AdjustPageSize(criteria);

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var repositoryFactory = ((IRepositoryFactory)unitOfWork.Repositories);

                var records = GetFilteredRecords(repositoryFactory.Create<TEntity>());
                records = ConfigureIncludes(records);
                records = ApplyBaseFilter(records);

                var paginationData = ApplyFiltering(records, criteria);
                var entities = paginationData.Records.AsNoTracking().ToList();

                var recordsRetrievedEventArgs = new RecordsRetrievedEventArgs<TEntity>(entities, criteria.Context, RetrievalScenario.MultipleRecords);
                OnAfterRecordsRetrieved(recordsRetrievedEventArgs);

                var creationContext = new CreationContext(RetrievalScenario.MultipleRecords, criteria.Context);
                var dtos = entities.Select(e => _entityToDtoMapping.CreateFromSource(e, creationContext)).ToList();

                return new CardIndexRecords<TDto>(dtos, criteria.CurrentPageIndex, paginationData.LastPageIndex, paginationData.RecordCount.Result);
            }
        }

        protected virtual IQueryable<TEntity> ApplyBaseFilter(IQueryable<TEntity> records)
        {
            return records;
        }

        protected virtual QueryCriteria AdjustPageSize(QueryCriteria criteria)
        {
            var defaultPageSize = SystemParameters.GetParameter<int>(ParameterKeys.DefaultPageSize);

            criteria = criteria ?? new QueryCriteria();
            criteria.PageSize = criteria.PageSize ?? PageSize ?? defaultPageSize;

            return criteria;
        }

        public DisposableQueryable<TDto> GetQueryableRecords([CanBeNull] QueryCriteria criteria)
        {
            criteria = criteria ?? new QueryCriteria();
            var unitOfWork = UnitOfWorkService.Create();

            try
            {
                var repositoryFactory = ((IRepositoryFactory)unitOfWork.Repositories);

                var records = GetFilteredRecords(repositoryFactory.Create<TEntity>());
                records = ConfigureIncludes(records);
                records = ApplyFiltering(records, criteria).Records.AsNoTracking();

                var dtos = records.Select(_entityToDtoMapping.Mapping);

                return new DisposableQueryable<TDto>(dtos, unitOfWork);
            }
            catch (Exception)
            {
                unitOfWork.Dispose();
                throw;
            }
        }

        protected virtual NamedFilter<TEntity> OnMissingFilterFallback(NamedFilters<TEntity> namedFilters, string filterKey, string filterCode)
        {
            if (typeof(ISoftDeletable).IsAssignableFrom(typeof(TEntity))
                && _softDeletableStateFilter.Contains(filterCode.ToLower()))
            {
                throw new FilterNotFoundException($"ApplyContextFiltering method should be overridden in CardIndexDataService interface of type {typeof(TEntity)}");
            }
            else
            {
                return new NamedFilter<TEntity>(string.Empty, c => false);
            }
        }

        protected IQueryable<TEntity> ApplyFilteringCriteria(IQueryable<TEntity> records, List<FilterCodeGroup> filterGroups, Dictionary<string, IFilteringObject> filterObjects)
        {
            if (filterGroups.IsNullOrEmpty())
            {
                return records;
            }

            foreach (var filterGroup in filterGroups)
            {
                Expression<Func<TEntity, bool>> filterGroupExpression = null;

                foreach (var filterCode in filterGroup.Codes)
                {
                    var filter = NamedFiltersInternal[filterCode]
                                 ?? OnMissingFilterFallback(NamedFiltersInternal, filterCode.ToUpperInvariant(), filterCode);

                    AuthorizeFilterUsage(filter);
                    IFilteringObject filterObject = null;

                    if (filterObjects.ContainsKey(filterCode))
                    {
                        filterObject = filterObjects[filterCode];
                    }

                    if (filterGroup.IsConjunction)
                    {
                        filterGroupExpression = filterGroupExpression == null
                            ? filter.GetExpression(filterObject)
                            : filterGroupExpression.And(filter.GetExpression(filterObject));
                    }
                    else
                    {
                        filterGroupExpression = filterGroupExpression == null
                            ? filter.GetExpression(filterObject)
                            : filterGroupExpression.Or(filter.GetExpression(filterObject));
                    }
                }

                if (filterGroupExpression == null)
                {
                    throw new InvalidDataException();
                }

                records = records.Where(filterGroupExpression);
            }

            return records;
        }

        protected static bool IsPropertyReadRestricted(Expression<Func<TEntity, object>> searchColumnExpression, HashSet<string> readRestrictedProperties)
        {
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var memberInfo in PropertyHelper.GetPropertyPathMemberInfo(searchColumnExpression))
            {
                var propertyInfo = memberInfo as PropertyInfo;

                if (propertyInfo != null)
                {
                    return readRestrictedProperties.Contains(propertyInfo.Name);
                }
            }

            return false;
        }

        protected virtual IQueryable<TEntity> ApplySearchTextCriteria(IQueryable<TEntity> records, QueryCriteria criteria)
        {
            var readRestrictedEntityProperties =
                AnonymizationService.GetReadRestrictedPropertiesForRelatedObject(_entityToDtoMapping,
                    criteria.ReadRestrictedDtoProperties).ToHashSet();

            var searchText = criteria.SearchPhrase;

            if (string.IsNullOrWhiteSpace(searchText))
            {
                return records;
            }

            var words = SearchPhraseHelper.GetPhrases(searchText).ToList();

            foreach (var word in words)
            {
                var allConditionJoinedByOr = GetCustomSearchExpression(words);

                // ReSharper disable once LoopCanBeConvertedToQuery
                foreach (var searchColumn in GetSearchColumns(criteria.SearchCriteria))
                {
                    var expressionColumn = new BusinessLogic<TEntity, object>(searchColumn).AsExpression();

                    if (IsPropertyReadRestricted(expressionColumn, readRestrictedEntityProperties))
                    {
                        continue;
                    }

                    var condition = DynamicQueryHelper.BuildSearchCondition<TEntity>(expressionColumn, word);

                    if (condition != null)
                    {
                        allConditionJoinedByOr = allConditionJoinedByOr == null
                            ? condition
                            : condition.Or(allConditionJoinedByOr);
                    }
                }

                allConditionJoinedByOr = AlterSearchPhraseCondition(criteria.SearchCriteria, allConditionJoinedByOr, word);

                if (allConditionJoinedByOr == null)
                {
                    throw new InvalidOperationException();
                }

                records = records.Where(allConditionJoinedByOr);
            }

            return records;
        }

        protected virtual Expression<Func<TEntity, bool>> AlterSearchPhraseCondition(SearchCriteria criteria, Expression<Func<TEntity, bool>> condition, string word)
        {
            return condition;
        }

        protected virtual IEnumerable<BusinessLogic<TEntity, int>> GetSearchRelevanceFactors(QueryCriteria criteria)
        {
            yield break;
        }

        protected virtual IOrderedQueryable<TEntity> GetDefaultOrderBy(IQueryable<TEntity> records, QueryCriteria criteria)
        {
            return records.OrderByDescending(r => r.Id);
        }

        protected List<FilterCodeGroup> AddDefaultSoftDeletableFilter(IQueryable<TEntity> records, List<FilterCodeGroup> filterGroups)
        {
            if (!filterGroups.IsNullOrEmpty())
            {
                foreach (var filterCodeGroup in filterGroups)
                {
                    foreach (var filterCode in filterCodeGroup.Codes)
                    {
                        var filter = NamedFiltersInternal[filterCode]
                                     ?? OnMissingFilterFallback(NamedFiltersInternal, filterCode.ToUpperInvariant(), filterCode);

                        if (TypeHelper.IsSubclassOfRawGeneric(filter.GetType(), typeof(SoftDeletableNamedFilter<>)))
                        {
                            return filterGroups;
                        }
                    }
                }
            }
            else
            {
                filterGroups = new List<FilterCodeGroup>();
            }

            filterGroups.Add(new FilterCodeGroup
            {
                Codes = new List<string> { SoftDeletableStateFilter.Existing.ToString() }
            });

            return filterGroups;
        }

        protected virtual IQueryable<TEntity> ApplyOrderByCriteria(IQueryable<TEntity> records, QueryCriteria criteria)
        {
            if (criteria.OrderBy.IsNullOrEmpty())
            {
                var orderByQuery = !criteria.SearchPhrase.IsNullOrEmpty()
                    ? DynamicQueryHelper.OrderByRelevanceFactors(records, GetSearchRelevanceFactors(criteria))
                    : GetDefaultOrderBy(records, criteria);

                return orderByQuery.ThenByDescending(e => e.Id);
            }

            var orderedQueryBuilder = new OrderedQueryBuilder<TEntity>(records);

            foreach (var orderBy in criteria.OrderBy)
            {
                ApplySortingCriterion(orderedQueryBuilder, orderBy);
            }

            var result = orderedQueryBuilder.Result ?? GetDefaultOrderBy(records, criteria);

            return result.ThenByDescending(e => e.Id);
        }

        protected virtual void ApplySortingCriterion(
            OrderedQueryBuilder<TEntity> orderedQueryBuilder,
            SortingCriterion sortingCriterion)
        {
            var dtoColumnName = sortingCriterion.GetSortingColumnName();
            var sortingDirection = sortingCriterion.GetSortingDirection();
            var entityPropertyPath = GetOrderByEntityPropertyPath(dtoColumnName);

            orderedQueryBuilder.ApplySortingKey(entityPropertyPath, sortingDirection);
        }

        public virtual TDto GetRecordById(long id)
        {
            return GetRecordInternal(id, QueryableEntityExtensions.GetById);
        }

        public TDto GetRecordByIdOrDefault(long id)
        {
            return GetRecordInternal(id, QueryableEntityExtensions.GetByIdOrDefault);
        }

        protected TDto GetRecordInternal(long id, Func<IQueryable<TEntity>, long, TEntity> recordExtractionFunc)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var repositoryFactory = ((IRepositoryFactory)unitOfWork.Repositories);
                var entityRepository = repositoryFactory.Create<TEntity>();
                var records = GetFilteredRecords(entityRepository);
                records = ConfigureIncludes(records);

                if (typeof(ISoftDeletable).IsAssignableFrom(typeof(TEntity)))
                {
                    records = FilterSoftDeletedRecordsIfUnauthorized(records);
                }

                var entity = recordExtractionFunc(records, id);

                if (entity == null)
                {
                    return null;
                }

                var recordsRetrievedEventArgs = new RecordsRetrievedEventArgs<TEntity>(entity, null);
                OnAfterRecordsRetrieved(recordsRetrievedEventArgs);

                var creationContext = new CreationContext(RetrievalScenario.SingleRecord, null);
                var dto = _entityToDtoMapping.CreateFromSource(entity, creationContext);

                return dto;
            }
        }

        private PropertyPath GetOrderByEntityPropertyPath(string dtoColumnName)
        {
            var fieldMappings = _entityToDtoMapping.GetFieldMappings(typeof(TEntity), typeof(TDto));
            var propertyPathElement = CardIndexComplexPropertyHelper.GetNestedPath<TDto>(dtoColumnName);
            var mappingKey = PropertyHelper.CombinePropertyPath(dtoColumnName, propertyPathElement);

            return new PropertyPath(fieldMappings[mappingKey].AsEnumerable().Reverse());
        }

        private IQueryable<TEntity> FilterSoftDeletedRecordsIfUnauthorized(IQueryable<TEntity> records)
        {
            var filters = GetSoftDeletableFilters();
            var filterAll = filters.SingleOrDefault(f => f.Code == NamingConventionHelper.ConvertPascalCaseToHyphenated(SoftDeletableStateFilter.All.ToString()));
            var filterExisting = filters.SingleOrDefault(f => f.Code == NamingConventionHelper.ConvertPascalCaseToHyphenated(SoftDeletableStateFilter.Existing.ToString()));

            var filter = filterAll != null && IsAccessibleToCurrentUser(filterAll.RequiredRole)
                ? filterAll
                : filterExisting;

            if (filter != null)
            {
                if (!IsAccessibleToCurrentUser(filter.RequiredRole))
                {
                    throw new UnauthorizedFilterUsageException(filter.Code, filter.RequiredRole.ToString());
                }

                records = records.Where(filter.Expression);
            }

            return records;
        }

        public virtual IDictionary<long, TDto> GetRecordsByIds(long[] ids, bool filtered = true)
        {
            if (ids == null || ids.Length == 0)
            {
                return new Dictionary<long, TDto>();
            }

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var repositoryFactory = ((IRepositoryFactory)unitOfWork.Repositories);
                var entityRepository = repositoryFactory.Create<TEntity>();

                var records = ConfigureIncludes(entityRepository);
                var entities = (filtered ? GetFilteredRecords(records).GetByIds(ids) : records.GetByIds(ids)).ToList();

                var recordsRetrievedEventArgs = new RecordsRetrievedEventArgs<TEntity>(entities, null, RetrievalScenario.MultipleRecords);
                OnAfterRecordsRetrieved(recordsRetrievedEventArgs);

                var creationContext = new CreationContext(RetrievalScenario.MultipleRecords, null);
                var dtos = entities.ToDictionary(e => e.Id, e => _entityToDtoMapping.CreateFromSource(e, creationContext));

                return dtos;
            }
        }

        public virtual TDto GetDefaultNewRecord()
        {
            var currentTime = TimeService.GetCurrentTime();
            var entity = ReflectionHelper.CreateInstance<TEntity>();

            var modifiedEntity = entity as IModificationTracked;

            if (modifiedEntity != null)
            {
                modifiedEntity.ModifiedOn = currentTime;
            }

            var createdEntity = entity as ICreationTracked;

            if (createdEntity != null)
            {
                createdEntity.CreatedOn = currentTime;
            }

            InitializeNavigationProperties(entity);

            var creationContext = new CreationContext(RetrievalScenario.SingleRecord);

            return _entityToDtoMapping.CreateFromSource(entity, creationContext);
        }

        protected virtual void InitializeNavigationProperties(TEntity entity)
        {
            var writablePropertyInfos = TypeHelper.GetPublicInstanceProperties(typeof(TEntity))
                                                  .Where(p => p.GetSetMethod() != null);

            foreach (var propertyInfo in writablePropertyInfos)
            {
                if (propertyInfo.PropertyType.IsGenericType && propertyInfo.PropertyType.GenericTypeArguments.Length == 1)
                {
                    var collectionElementType = propertyInfo.PropertyType.GenericTypeArguments[0];

                    if (collectionElementType.IsClass)
                    {
                        var navigationPropertyType = typeof(Collection<>).MakeGenericType(collectionElementType);

                        if (propertyInfo.PropertyType.IsAssignableFrom(navigationPropertyType))
                        {
                            var emptyCollection = ReflectionHelper.CreateInstance(navigationPropertyType);
                            propertyInfo.SetValue(entity, emptyCollection);
                        }
                    }
                }
            }
        }

        public bool SupportsSearchByColumns => !GetSearchColumns(new SearchCriteria()).IsNullOrEmpty();

        public bool SupportsNamedFilters => !(NamedFilters == null || NamedFilters.IsNullOrEmpty());

        public long GetRecordId(TDto dto)
        {
            var dtoToEntityMapping = _mappingFactory.CreateMapping<TDto, TEntity>();

            return dtoToEntityMapping.CreateFromSource(dto).Id;
        }

        protected virtual int? PageSize => null;

        protected virtual NamedFilters<TEntity> NamedFilters => null;

        protected virtual Type GetSystemFilterTypeParameter()
        {
            return null;
        }

        protected virtual IEnumerable<Expression<Func<TEntity, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield break;
        }

        /// <summary>
        /// Fallback method called every time any records are retrieved from repository
        /// </summary>
        /// <param name="entites"></param>
        protected virtual void OnAfterRecordsRetrieved(RecordsRetrievedEventArgs<TEntity> eventArgs)
        {
        }

        /// <summary>
        /// Default search condition
        /// </summary>
        /// <param name="wordCollection">Search word collection</param>
        /// <returns>Search expression</returns>
        protected virtual Expression<Func<TEntity, bool>> GetCustomSearchExpression(
            IReadOnlyCollection<string> wordCollection)
        {
            return null;
        }

        protected virtual bool IsNewRecord(TEntity entity)
        {
            if (entity.Id <= 0)
            {
                return true;
            }

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var repositoryFactory = ((IRepositoryFactory)unitOfWork.Repositories);
                var entityRepository = repositoryFactory.Create<TEntity>();

                return entityRepository.GetByIdOrDefault(entity.Id) == null;
            }
        }

        CardIndexRecords<object> IDataImportService.GetRecordsAsObjects(QueryCriteria criteria)
        {
            var result = GetRecords(criteria);

            return new CardIndexRecords<object>(result.Rows.Cast<object>().ToList(), result.CurrentPageIndex, result.LastPageIndex, result.RecordCount, result.Alerts);
        }

        public virtual AddRecordResult AddRecord(TDto record, object context = null)
        {
            throw new NotSupportedException();
        }

        public virtual EditRecordResult EditRecord(TDto record, object context = null)
        {
            throw new NotSupportedException();
        }

        public virtual BulkEditRecordResult BulkEditRecords(BulkEditDto dto, object context = null)
        {
            throw new NotSupportedException();
        }

        public virtual DeleteRecordsResult DeleteRecords(IEnumerable<long> idsToDelete, object context = null, bool isDeletePermanent = false)
        {
            throw new NotSupportedException();
        }

        public virtual ImportRecordResult ImportRecord(object dto, ImportBehavior importBehavior)
        {
            throw new NotSupportedException();
        }

        public IEnumerable<KeyValuePair<string, SecurityRoleType>> GetFilterRoles()
        {
            return NamedFilters.GetRoles();
        }

        public virtual void BeforeBulkImportFinished(BeforeBulkImportFinishedEventArgs eventArgs)
        {
            throw new NotSupportedException();
        }

        protected PaginationData<IQueryable<TEntity>> ApplyFiltering(IQueryable<TEntity> records, QueryCriteria criteria)
        {
            criteria.CurrentPageIndex = criteria.CurrentPageIndex ?? 0;

            if (typeof(ISoftDeletable).IsAssignableFrom(typeof(TEntity)))
            {
                criteria.Filters = AddDefaultSoftDeletableFilter(records, criteria.Filters);
            }

            records = ApplyContextFiltering(records, criteria.Context);
            records = ApplyFilteringCriteria(records, criteria.Filters, criteria.FilterObjects ?? new Dictionary<string, IFilteringObject>());
            records = ApplySearchTextCriteria(records, criteria);

            var recordCount = records.CountAsync();

            records = ApplyOrderByCriteria(records, criteria);

            if (criteria.CurrentPageIndex.Value != 0)
            {
                Debug.Assert(criteria.PageSize != null, "criteria.PageSize != null");

                int recordsToSkip = criteria.CurrentPageIndex.Value * criteria.PageSize.Value;
                records = QueryableExtensions.Skip(records, () => recordsToSkip);
            }

            if (criteria.PageSize.HasValue)
            {
                records = QueryableExtensions.Take(records, () => criteria.PageSize.Value);
            }

            return new PaginationData<IQueryable<TEntity>>
            {
                Records = records,
                RecordCount = recordCount,
                PageSize = criteria.PageSize,
            };
        }

        private void AuthorizeFilterUsage(NamedFilter<TEntity> filter)
        {
            if (filter.RequiredRole.HasValue
                && (PrincipalProvider.Current == null || !PrincipalProvider.Current.IsInRole(filter.RequiredRole.Value)))
            {
                throw new UnauthorizedFilterUsageException(filter.Code, filter.RequiredRole.Value.ToString());
            }
        }

        private void InitializeNamedFiltersInternal()
        {
            NamedFiltersInternal = NamedFilters == null
                ? NamedFilters<TEntity>.Empty()
                : new NamedFilters<TEntity>(NamedFilters);

            if (typeof(ISoftDeletable).IsAssignableFrom(typeof(TEntity)))
            {
                NamedFiltersInternal.AppendUniqueFilters(GetSystemFilters());
            }
        }

        private NamedFilters<TEntity> GetSystemFilters()
        {
            var genericFilter = typeof(SoftDeletableNamedFilter<>);
            var filterTypeParameter = GetSystemFilterTypeParameter();

            if (filterTypeParameter != null)
            {
                var filterType = genericFilter.MakeGenericType(filterTypeParameter);

                return
                    new NamedFilters<TEntity>(new[]
                    {
                        ReflectionHelper.CreateInstance<NamedFilter<TEntity>>(filterType,
                            SoftDeletableStateFilter.Existing, null)
                    });
            }

            return NamedFilters<TEntity>.Empty();
        }

        private IList<NamedFilter<TEntity>> GetSoftDeletableFilters()
        {
            if (NamedFilters == null)
            {
                return new List<NamedFilter<TEntity>>();
            }

            return NamedFilters.Items
                .Where(f => TypeHelper.IsSubclassOfRawGeneric(f.GetType(), typeof(SoftDeletableNamedFilter<>)))
                .ToList();
        }

        private bool IsAccessibleToCurrentUser(SecurityRoleType? requiredRole)
        {
            return requiredRole == null || PrincipalProvider.Current.IsInRole(requiredRole.Value);
        }
    }
}
