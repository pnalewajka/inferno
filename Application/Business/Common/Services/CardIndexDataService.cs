using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using Smt.Atomic.Business.Common.BusinessEvents;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Resources;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Dto;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.Common.Services
{
    public abstract class CardIndexDataService<TDto, TEntity, TDbScope>
        : ReadOnlyCardIndexDataService<TDto, TEntity, TDbScope>
          where TEntity : class, IEntity
          where TDto : class
          where TDbScope : IDbScope
    {
        private readonly IClassMapping<TDto, TEntity> _dtoToEntityMapping;
        private readonly IClassMapping<TEntity, TDto> _entityToDtoMapping;
        private readonly ICacheInvalidationService _cacheInvalidationService;
        public DeletionStrategy DeletionStrategy = DeletionStrategy.Delete;

        protected abstract bool ShouldUseDownwardMappingForFieldResolution { get; }

        /// <summary>
        /// This field contains list of related cache area <see cref="CacheAreas"/> and it will be
        /// used to clear the cache areas when some entities are changed (add, edit or delete).
        /// </summary>
        protected string[] RelatedCacheAreas;

        protected IBusinessEventPublisher BusinessEventPublisher { get; }

        protected CardIndexDataService(
            ICardIndexServiceDependencies<TDbScope> dependencies)
            : base(dependencies)
        {
            var classMappingFactory = dependencies.ClassMappingFactory;
            _dtoToEntityMapping = classMappingFactory.CreateMapping<TDto, TEntity>();
            _entityToDtoMapping = classMappingFactory.CreateMapping<TEntity, TDto>();
            _cacheInvalidationService = dependencies.CacheInvalidationService;

            BusinessEventPublisher = dependencies.BusinessEventPublisher;
        }

        protected void PublishBusinessEvents(IEnumerable<BusinessEvent> events)
        {
            foreach (var businessEvent in events)
            {
                BusinessEventPublisher.PublishBusinessEvent(businessEvent);
            }
        }

        public override AddRecordResult AddRecord(TDto record, object context = null)
        {
            var getEventMethodName = MethodHelper.GetMethodName(() => GetAddRecordEvents(null, null));
            var shouldGenerateBusinessEvent = TypeHelper.IsMethodOverridden(getEventMethodName, GetType());

            if (shouldGenerateBusinessEvent)
            {
                using (var transactionScope = TransactionHelper.CreateDefaultTransactionScope())
                {
                    var result = InternalAddRecord(record, context);
                    PublishBusinessEvents(GetAddRecordEvents(result, record));
                    transactionScope.Complete();

                    return result;
                }
            }

            return InternalAddRecord(record, context);
        }

        protected virtual AddRecordResult InternalAddRecord(TDto record, object context)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var repositoryFactory = ((IRepositoryFactory)unitOfWork.Repositories);
                var entityRepository = repositoryFactory.Create<TEntity>();

                TEntity entity = _dtoToEntityMapping.CreateFromSource(record);
                SetContext(entity, context);

                var recordAddingEventArgs = new RecordAddingEventArgs<TEntity, TDto, TDbScope>(unitOfWork, entity,
                    record, context);
                var violations = entityRepository.GetUniqueKeyViolations(recordAddingEventArgs.InputEntity).ToList();

                var alerts = new List<AlertDto>();

                if (violations.Any())
                {
                    recordAddingEventArgs.Canceled = true;
                }
                else
                {
                    OnRecordAdding(recordAddingEventArgs);
                    alerts.AddRange(recordAddingEventArgs.Alerts);

                    entityRepository.Add(entity);

                    if (!recordAddingEventArgs.Canceled)
                    {
                        unitOfWork.Commit();

                        var recordAddedEventArgs = new RecordAddedEventArgs<TEntity, TDto>(entity, record, context);
                        OnRecordAdded(recordAddedEventArgs);
                        RecordsChanged(new RecordChangedEventArgs<TEntity>(entity));
                        alerts.AddRange(recordAddedEventArgs.Alerts);
                    }
                }

                var propertyNames = ClassMappingHelper.TranslateToDestinationPropertyNames<TEntity, TDto>(
                    violations, _entityToDtoMapping);

                return new AddRecordResult(!recordAddingEventArgs.Canceled, entity.Id, propertyNames, alerts);
            }
        }

        public override EditRecordResult EditRecord(TDto record, object context = null)
        {
            TDto originalRecord;
            var getEventMethodName = MethodHelper.GetMethodName(() => GetEditRecordEvents(null, null, null));
            var shouldGenerateBusinessEvent = TypeHelper.IsMethodOverridden(getEventMethodName, GetType());

            if (shouldGenerateBusinessEvent)
            {
                using (var transactionScope = TransactionHelper.CreateDefaultTransactionScope())
                {
                    var result = InternalEditRecord(record, out originalRecord, context);
                    PublishBusinessEvents(GetEditRecordEvents(result, record, originalRecord));
                    transactionScope.Complete();

                    return result;
                }
            }

            return InternalEditRecord(record, out originalRecord, context);
        }

        protected virtual EditRecordResult InternalEditRecord(TDto newRecord, out TDto originalRecord, object context)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var repositoryFactory = ((IRepositoryFactory)unitOfWork.Repositories);
                var entityRepository = repositoryFactory.Create<TEntity>();

                TEntity inputEntity = _dtoToEntityMapping.CreateFromSource(newRecord);
                TEntity dbEntity = GetFilteredRecords(entityRepository).GetByIdOrDefault(inputEntity.Id);

                if (dbEntity == null)
                {
                    originalRecord = null;
                    return new EditRecordResult(false,
                        inputEntity.Id,
                        null,
                        new List<AlertDto>
                        {
                            new AlertDto
                            {
                                Message = string.Format(CardIndexDataResources.RecordIdNotExistsFormat, inputEntity.Id),
                                Type = AlertType.Error,
                                IsDismissable = true
                            }
                        });
                }

                var creationContext = new CreationContext(RetrievalScenario.SingleRecord);
                originalRecord = _entityToDtoMapping.CreateFromSource(dbEntity, creationContext);

                var recordEditingEventArgs = new RecordEditingEventArgs<TEntity, TDto, TDbScope>(unitOfWork, dbEntity,
                    inputEntity, newRecord, context);
                var violations = entityRepository.GetUniqueKeyViolations(recordEditingEventArgs.InputEntity).ToList();

                var alerts = new List<AlertDto>();

                if (violations.Any())
                {
                    recordEditingEventArgs.Canceled = true;
                }
                else
                {
                    OnRecordEditing(recordEditingEventArgs);
                    ApplyChanges(dbEntity, inputEntity);
                    alerts.AddRange(recordEditingEventArgs.Alerts);

                    if (!recordEditingEventArgs.Canceled)
                    {
                        unitOfWork.Commit();

                        var recordEditedEventArgs = new RecordEditedEventArgs<TEntity, TDto>(dbEntity, inputEntity, newRecord, originalRecord, context);
                        OnRecordEdited(recordEditedEventArgs);
                        RecordsChanged(new RecordChangedEventArgs<TEntity>(dbEntity));
                        alerts.AddRange(recordEditedEventArgs.Alerts);
                    }
                }

                var propertyNames = ClassMappingHelper.TranslateToDestinationPropertyNames<TEntity, TDto>(violations,
                    _entityToDtoMapping);

                return new EditRecordResult(!recordEditingEventArgs.Canceled, inputEntity.Id, propertyNames, alerts);
            }
        }

        protected virtual void ApplyChanges(TEntity dbEntity, TEntity inputEntity)
        {
            HashSet<string> mappedProperties;
            
            if (ShouldUseDownwardMappingForFieldResolution)
            {
                mappedProperties = _dtoToEntityMapping.GetFieldMappings(typeof(TDto), inputEntity.GetType())
                                                          .SelectMany(m => m.Value)
                                                          .ToHashSet();
            }
            else
            {
                mappedProperties = _entityToDtoMapping.GetFieldMappings(inputEntity.GetType(), typeof(TDto))
                                                          .SelectMany(m => m.Value)
                                                          .ToHashSet();
            }

            EntityHelper.ShallowCopy(inputEntity, dbEntity, mappedProperties);
        }

        protected virtual CardIndexEventArgs OnBulkEditing(BulkEditDto dto, object context = null)
        {
            return new CardIndexEventArgs(context)
            {
                Canceled = false
            };
        }

        public override BulkEditRecordResult BulkEditRecords(BulkEditDto dto, object context = null)
        {
            var validationResult = OnBulkEditing(dto);

            var currentPrincipal = PrincipalProvider.Current;
            var currentPrincipalId = currentPrincipal.Id;

            if (!validationResult.Canceled)
            {
                var serializablePropertyValue = dto.PropertyValue?.GetType().IsEnum ?? false
                    ? (int)dto.PropertyValue
                    : dto.PropertyValue;

                if (currentPrincipalId != null)
                {
                    PublishBusinessEvents(new List<BulkEditEvent>
                    {
                        new BulkEditEvent
                        {
                            DtoType = dto.DtoType.FullName,
                            PropertyName = dto.PropertyName,
                            PropertyValue = serializablePropertyValue,
                            RecordIds = dto.RecordIds,
                            CardIndexDataServiceType = dto.CardIndexDataServiceType.FullName,
                            ModifyingUserId = currentPrincipalId.Value
                        }
                    });
                }
            }

            return new BulkEditRecordResult(
                isSuccessful: !validationResult.Canceled,
                editedRecordIds: dto.RecordIds,
                uniqueKeyViolations: null,
                alerts: validationResult.Alerts);
        }

        public override DeleteRecordsResult DeleteRecords(IEnumerable<long> idsToDelete, object context = null,
            bool isDeletePermanent = false)
        {
            IEnumerable<TDto> deletedRecords;
            var getEventMethodName = MethodHelper.GetMethodName(() => GetDeleteRecordEvents(null, null));
            var shouldGenerateBusinessEvent = TypeHelper.IsMethodOverridden(getEventMethodName, GetType());

            if (shouldGenerateBusinessEvent)
            {
                using (var transactionScope = TransactionHelper.CreateDefaultTransactionScope())
                {
                    var result = InternalDeleteRecords(idsToDelete, out deletedRecords, context, isDeletePermanent);

                    foreach (TDto deletedRecord in deletedRecords)
                    {
                        PublishBusinessEvents(GetDeleteRecordEvents(result, deletedRecord));
                    }

                    transactionScope.Complete();

                    return result;
                }
            }

            return InternalDeleteRecords(idsToDelete, out deletedRecords, context, isDeletePermanent);
        }

        protected virtual DeleteRecordsResult InternalDeleteRecords(
            IEnumerable<long> idsToDelete,
            out IEnumerable<TDto> deletedRecords,
            object context,
            bool isDeletePermanent)
        {
            if (idsToDelete == null)
            {
                throw new ArgumentNullException(nameof(idsToDelete));
            }

            var ids = idsToDelete.ToArray();

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var repositoryFactory = ((IRepositoryFactory)unitOfWork.Repositories);
                var entityRepository = repositoryFactory.Create<TEntity>();

                var entities = GetFilteredRecords(entityRepository).Where(x => ids.Contains(x.Id)).ToList();
                deletedRecords = entities.Select(_entityToDtoMapping.CreateFromSource).ToList();

                foreach (TEntity record in entities)
                {
                    var recordDeletingEventArgs = new RecordDeletingEventArgs<TEntity, TDbScope>(unitOfWork, record,
                        context);
                    OnRecordDeleting(recordDeletingEventArgs);

                    if (recordDeletingEventArgs.Canceled)
                    {
                        var deleteResult = new DeleteRecordsResult(false, 0, record.Id);
                        deleteResult.Alerts.AddRange(recordDeletingEventArgs.Alerts);

                        return deleteResult;
                    }

                    if (DeletionStrategy == DeletionStrategy.Delete)
                    {
                        entityRepository.Delete(record, isDeletePermanent);
                    }
                    else
                    {
                        entityRepository.Remove(record);
                    }
                }

                unitOfWork.Commit();
                var recordDeletedEventArgs = new RecordDeletedEventArgs<TEntity>(entities, context);
                OnRecordsDeleted(recordDeletedEventArgs);
                RecordsChanged(new RecordChangedEventArgs<TEntity>(entities));

                return new DeleteRecordsResult(true, entities.Count);
            }
        }

        public override ImportRecordResult ImportRecord(object dto, ImportBehavior importBehavior)
        {
            var entity = (TEntity)_dtoToEntityMapping.CreateFromSource(dto);

            if (importBehavior == ImportBehavior.Insert || importBehavior == ImportBehavior.InsertOrUpdate)
            {
                if (IsNewRecord(entity))
                {
                    var addRecordResult = AddRecord((TDto)dto);

                    return new ImportRecordResult(addRecordResult.UniqueKeyViolations, addRecordResult.IsSuccessful,
                        addRecordResult.Alerts);
                }
            }

            if (importBehavior == ImportBehavior.InsertOrUpdate)
            {
                var editRecordResult = EditRecord((TDto)dto);

                return new ImportRecordResult(editRecordResult.UniqueKeyViolations, editRecordResult.IsSuccessful, editRecordResult.Alerts);
            }

            return new ImportRecordResult(null, true);
        }

        public override void BeforeBulkImportFinished(BeforeBulkImportFinishedEventArgs eventArgs)
        {
        }

        /// <summary>
        /// Returns business events which should be published when the new record is added
        /// </summary>
        protected virtual IEnumerable<BusinessEvent> GetAddRecordEvents(AddRecordResult result, TDto record)
        {
            yield break;
        }

        /// <summary>
        /// Returns business events which should be published when the record is edited
        /// </summary>
        protected virtual IEnumerable<BusinessEvent> GetEditRecordEvents(EditRecordResult result, TDto newRecord,
            TDto originalRecord)
        {
            yield break;
        }

        /// <summary>
        /// Returns business events which should be published when the record is deleted
        /// </summary>
        protected virtual IEnumerable<BusinessEvent> GetDeleteRecordEvents(DeleteRecordsResult result, TDto record)
        {
            yield break;
        }

        /// <summary>
        /// Called during record adding, just before adding to repository
        /// </summary>
        protected virtual void OnRecordAdding(RecordAddingEventArgs<TEntity, TDto, TDbScope> eventArgs)
        {
        }

        /// <summary>
        /// Called after record successfully added
        /// </summary>
        /// <param name="recordAddedEventArgs"></param>
        protected virtual void OnRecordAdded(RecordAddedEventArgs<TEntity, TDto> recordAddedEventArgs)
        {
        }

        /// <summary>
        /// Called during record editing, just before adding to repository marked  as 'edit'
        /// </summary>
        protected virtual void OnRecordEditing(RecordEditingEventArgs<TEntity, TDto, TDbScope> eventArgs)
        {
        }

        /// <summary>
        /// Called after record is edited
        /// </summary>
        /// <param name="recordEditedEventArgs"></param>
        protected virtual void OnRecordEdited(RecordEditedEventArgs<TEntity, TDto> recordEditedEventArgs)
        {
        }

        /// <summary>
        /// Called before record is deleted, allows for canceling the operation
        /// </summary>
        /// <param name="eventArgs"></param>
        protected virtual void OnRecordDeleting(RecordDeletingEventArgs<TEntity, TDbScope> eventArgs)
        {
        }

        /// <summary>
        /// Called when record deleted
        /// </summary>
        /// <param name="eventArgs"></param>
        protected virtual void OnRecordsDeleted(RecordDeletedEventArgs<TEntity> eventArgs)
        {
        }

        /// <summary>
        /// Called when records are added/edited/deleted
        /// </summary>
        protected virtual void OnRecordsChanged(RecordChangedEventArgs<TEntity> eventArgs)
        {
        }

        private void RecordsChanged(RecordChangedEventArgs<TEntity> eventArgs)
        {
            RelatedCacheAreas?.ForEach(ca => _cacheInvalidationService.InvalidateArea(ca));
            OnRecordsChanged(eventArgs);
        }
    }
}