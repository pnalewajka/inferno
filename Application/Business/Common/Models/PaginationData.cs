﻿using System;
using System.Collections;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Common.Models
{
    public class PaginationData<TRecordIEnumerableType>
        where TRecordIEnumerableType : IEnumerable
    {
        public TRecordIEnumerableType Records { get; set; }

        public Task<int> RecordCount { get; set; }

        public long? PageSize { get; set; }

        public int? LastPageIndex => PageSize.HasValue
            ? Math.Max(0, (int)Math.Ceiling((double)RecordCount.Result / PageSize.Value) - 1)
            : (int?)null;
    }
}
