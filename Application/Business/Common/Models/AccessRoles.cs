using System.Collections.Generic;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Common.Models
{
    public class AccessRoles
    {
        public SecurityRoleType? RoleForRead { get; set; }

        public SecurityRoleType? RoleForWrite { get; set; }

        public PropertyAccessMode GetAccessMode(HashSet<string> userRoles)
        {
            var access = PropertyAccessMode.NoAccess;

            if (HasReadRole(userRoles))
            {
                access = PropertyAccessMode.ReadOnly;

                if (HasWriteRole(userRoles))
                {
                    access = PropertyAccessMode.ReadWrite;
                }
            }

            return access;
        }

        public IEnumerable<SecurityRoleType> GetMissingRoles(HashSet<string> userRoles)
        {
            var missingRoles = new List<SecurityRoleType>(2);

            if (!HasReadRole(userRoles))
            {
                missingRoles.Add(RoleForRead.Value);
            }

            if (!HasWriteRole(userRoles))
            {
                missingRoles.Add(RoleForWrite.Value);
            }

            return missingRoles;
        }

        private bool HasReadRole(HashSet<string> userRoles)
        {
            return RoleForRead == null || userRoles.Contains(RoleForRead.Value.ToString());
        }

        private bool HasWriteRole(HashSet<string> userRoles)
        {
            return RoleForWrite == null || userRoles.Contains(RoleForWrite.Value.ToString());
        }
    }
}