﻿namespace Smt.Atomic.Business.Common.Models
{
    public class JobResult
    {
        public bool IsJobFinishedGracefully { get; set; }

        public static JobResult Success()
        {
            return new JobResult { IsJobFinishedGracefully = true };
        }

        public static JobResult Fail()
        {
            return new JobResult { IsJobFinishedGracefully = false };
        }
    }
}
