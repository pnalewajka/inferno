using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.Business.Common.Dto
{
    public class FilterYearDto
    {
        public int Year { get; set; }
        public bool HasEntries { get; set; }

        public string GetFilterCode()
        {
            return $"y{Year}";
        }

        public static int? TryGetYear(string code)
        {
            var year = 0;

            var result = code != null
                         && code.Length == 5
                         && code.ToLowerInvariant()[0] == 'y'
                         && int.TryParse(code.Substring(1), out year);

            result = result && (year >= SystemConstraints.MinCalendarYear
                                && year <= SystemConstraints.MaxCalendarYear);

            return result ? year : (int?) null;
        }
    }
}