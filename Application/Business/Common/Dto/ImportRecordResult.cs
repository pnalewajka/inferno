﻿using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Common.Dto
{
    public class ImportRecordResult : RecordOperationResult
    {
        public ImportRecordResult()
        {
        }

        public ImportRecordResult(IEnumerable<PropertyGroup> uniqueKeyViolations, bool isSuccessful,
            IEnumerable<AlertDto> alerts = null) : base(uniqueKeyViolations, isSuccessful, alerts)
        {
        }
    }
}