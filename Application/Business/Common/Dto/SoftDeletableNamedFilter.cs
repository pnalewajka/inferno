﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Business.Common.Dto
{
    public class SoftDeletableNamedFilter<TSoftDeletableEntity> : NamedFilter<TSoftDeletableEntity>
        where TSoftDeletableEntity : class, IEntity, ISoftDeletable
    {
        public SoftDeletableStateFilter State { get; private set; }

        public SoftDeletableNamedFilter(SoftDeletableStateFilter softDeletableStateFilter,
            SecurityRoleType? requiredRole = SecurityRoleType.CanViewSoftDeletedEntities)
            : base(softDeletableStateFilter.ToString(), ExtractExpression(softDeletableStateFilter), requiredRole)
        {
            State = softDeletableStateFilter;
        }

        private static Expression<Func<TSoftDeletableEntity, bool>> ExtractExpression(SoftDeletableStateFilter filter)
        {
            switch (filter)
            {
                case SoftDeletableStateFilter.All:
                    return e => true;

                case SoftDeletableStateFilter.Deleted:
                    return e => e.IsDeleted;

                default:
                    return e => !e.IsDeleted;
            }
        }
    }
}
