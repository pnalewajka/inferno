using System;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Business.Common.Dto
{
    public class NamedFilter<TEntity>
        where TEntity : class
    {
        private string _code;

        public string Code
        {
            get { return _code; }
            set { _code = NamingConventionHelper.ConvertPascalCaseToHyphenated(value); }
        }

        public Expression<Func<TEntity, bool>> Expression { get; }

        /// <summary>
        /// Security role that must be matched to present filter to user
        /// Null if role check isn't required (default)
        /// It is copied to the client-side filters
        /// </summary>
        public SecurityRoleType? RequiredRole { get; set; }

        public NamedFilter(string code, Expression<Func<TEntity, bool>> condition, SecurityRoleType? requiredRole = null)
        {
            Code = code;
            Expression = condition;
            RequiredRole = requiredRole;
        }

        public virtual Expression<Func<TEntity, bool>> GetExpression(IFilteringObject filteringObject)
        {
            return Expression;
        }
    }

    public class NamedFilter<TEntity, T1> : NamedFilter<TEntity>
        where TEntity : class, IEntity
    {
        private readonly Expression<Func<TEntity, T1, bool>> _condition;

        public NamedFilter(string code, Expression<Func<TEntity, T1, bool>> condition, SecurityRoleType? requiredRole = null)
            : base(code, null, requiredRole)
        {
            _condition = condition;
        }

        public override Expression<Func<TEntity, bool>> GetExpression(IFilteringObject filteringObject)
        {
            var visitor = new FilterParameterReplacementVisitor<Func<TEntity, bool>>(filteringObject.GetFilterParameters());

            return visitor.VisitAndModify(_condition);
        }
    }

    public class NamedFilter<TEntity, T1, T2> : NamedFilter<TEntity>
        where TEntity : class, IEntity
    {
        private readonly Expression<Func<TEntity, T1, T2, bool>> _condition;

        public NamedFilter(string code, Expression<Func<TEntity, T1, T2, bool>> condition, SecurityRoleType? requiredRole = null)
            : base(code, null, requiredRole)
        {
            _condition = condition;
        }

        public override Expression<Func<TEntity, bool>> GetExpression(IFilteringObject filteringObject)
        {
            var visitor = new FilterParameterReplacementVisitor<Func<TEntity, bool>>(filteringObject.GetFilterParameters());

            return visitor.VisitAndModify(_condition);
        }
    }

    public class NamedFilter<TEntity, T1, T2, T3> : NamedFilter<TEntity>
        where TEntity : class, IEntity
    {
        private readonly Expression<Func<TEntity, T1, T2, T3, bool>> _condition;

        public NamedFilter(string code, Expression<Func<TEntity, T1, T2, T3, bool>> condition, SecurityRoleType? requiredRole = null)
            : base(code, null, requiredRole)
        {
            _condition = condition;
        }

        public override Expression<Func<TEntity, bool>> GetExpression(IFilteringObject filteringObject)
        {
            var visitor = new FilterParameterReplacementVisitor<Func<TEntity, bool>>(filteringObject.GetFilterParameters());

            return visitor.VisitAndModify(_condition);
        }
    }

    public class NamedFilter<TEntity, T1, T2, T3, T4> : NamedFilter<TEntity>
        where TEntity : class, IEntity
    {
        private readonly Expression<Func<TEntity, T1, T2, T3, T4, bool>> _condition;

        public NamedFilter(string code, Expression<Func<TEntity, T1, T2, T3, T4, bool>> condition, SecurityRoleType? requiredRole = null)
            : base(code, null, requiredRole)
        {
            _condition = condition;
        }

        public override Expression<Func<TEntity, bool>> GetExpression(IFilteringObject filteringObject)
        {
            var visitor = new FilterParameterReplacementVisitor<Func<TEntity, bool>>(filteringObject.GetFilterParameters());

            return visitor.VisitAndModify(_condition);
        }
    }

    public class NamedFilter<TEntity, T1, T2, T3, T4, T5> : NamedFilter<TEntity>
        where TEntity : class, IEntity
    {
        private readonly Expression<Func<TEntity, T1, T2, T3, T4, T5, bool>> _condition;

        public NamedFilter(string code, Expression<Func<TEntity, T1, T2, T3, T4, T5, bool>> condition, SecurityRoleType? requiredRole = null)
            : base(code, null, requiredRole)
        {
            _condition = condition;
        }

        public override Expression<Func<TEntity, bool>> GetExpression(IFilteringObject filteringObject)
        {
            var visitor = new FilterParameterReplacementVisitor<Func<TEntity, bool>>(filteringObject.GetFilterParameters());

            return visitor.VisitAndModify(_condition);
        }
    }

    public class NamedFilter<TEntity, T1, T2, T3, T4, T5, T6> : NamedFilter<TEntity>
        where TEntity : class, IEntity
    {
        private readonly Expression<Func<TEntity, T1, T2, T3, T4, T5, T6, bool>> _condition;

        public NamedFilter(string code, Expression<Func<TEntity, T1, T2, T3, T4, T5, T6, bool>> condition, SecurityRoleType? requiredRole = null)
            : base(code, null, requiredRole)
        {
            _condition = condition;
        }

        public override Expression<Func<TEntity, bool>> GetExpression(IFilteringObject filteringObject)
        {
            var visitor = new FilterParameterReplacementVisitor<Func<TEntity, bool>>(filteringObject.GetFilterParameters());

            return visitor.VisitAndModify(_condition);
        }
    }

    public class NamedFilter<TEntity, T1, T2, T3, T4, T5, T6, T7> : NamedFilter<TEntity>
        where TEntity : class, IEntity
    {
        private readonly Expression<Func<TEntity, T1, T2, T3, T4, T5, T6, T7, bool>> _condition;

        public NamedFilter(string code, Expression<Func<TEntity, T1, T2, T3, T4, T5, T6, T7, bool>> condition, SecurityRoleType? requiredRole = null)
            : base(code, null, requiredRole)
        {
            _condition = condition;
        }

        public override Expression<Func<TEntity, bool>> GetExpression(IFilteringObject filteringObject)
        {
            var visitor = new FilterParameterReplacementVisitor<Func<TEntity, bool>>(filteringObject.GetFilterParameters());

            return visitor.VisitAndModify(_condition);
        }
    }

    public class NamedFilter<TEntity, T1, T2, T3, T4, T5, T6, T7, T8> : NamedFilter<TEntity>
        where TEntity : class, IEntity
    {
        private readonly Expression<Func<TEntity, T1, T2, T3, T4, T5, T6, T7, T8, bool>> _condition;

        public NamedFilter(string code, Expression<Func<TEntity, T1, T2, T3, T4, T5, T6, T7, T8, bool>> condition, SecurityRoleType? requiredRole = null)
            : base(code, null, requiredRole)
        {
            _condition = condition;
        }

        public override Expression<Func<TEntity, bool>> GetExpression(IFilteringObject filteringObject)
        {
            var visitor = new FilterParameterReplacementVisitor<Func<TEntity, bool>>(filteringObject.GetFilterParameters());

            return visitor.VisitAndModify(_condition);
        }
    }

    public class NamedFilter<TEntity, T1, T2, T3, T4, T5, T6, T7, T8, T9> : NamedFilter<TEntity>
        where TEntity : class, IEntity
    {
        private readonly Expression<Func<TEntity, T1, T2, T3, T4, T5, T6, T7, T8, T9, bool>> _condition;

        public NamedFilter(string code, Expression<Func<TEntity, T1, T2, T3, T4, T5, T6, T7, T8, T9, bool>> condition, SecurityRoleType? requiredRole = null)
            : base(code, null, requiredRole)
        {
            _condition = condition;
        }

        public override Expression<Func<TEntity, bool>> GetExpression(IFilteringObject filteringObject)
        {
            var visitor = new FilterParameterReplacementVisitor<Func<TEntity, bool>>(filteringObject.GetFilterParameters());

            return visitor.VisitAndModify(_condition);
        }
    }

    public class NamedFilter<TEntity, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> : NamedFilter<TEntity>
        where TEntity : class, IEntity
    {
        private readonly Expression<Func<TEntity, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, bool>> _condition;

        public NamedFilter(string code, Expression<Func<TEntity, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, bool>> condition, SecurityRoleType? requiredRole = null)
            : base(code, null, requiredRole)
        {
            _condition = condition;
        }

        public override Expression<Func<TEntity, bool>> GetExpression(IFilteringObject filteringObject)
        {
            var visitor = new FilterParameterReplacementVisitor<Func<TEntity, bool>>(filteringObject.GetFilterParameters());

            return visitor.VisitAndModify(_condition);
        }
    }

    public class NamedFilter<TEntity, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> : NamedFilter<TEntity>
        where TEntity : class, IEntity
    {
        private readonly Expression<Func<TEntity, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, bool>> _condition;

        public NamedFilter(string code, Expression<Func<TEntity, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, bool>> condition, SecurityRoleType? requiredRole = null)
            : base(code, null, requiredRole)
        {
            _condition = condition;
        }

        public override Expression<Func<TEntity, bool>> GetExpression(IFilteringObject filteringObject)
        {
            var visitor = new FilterParameterReplacementVisitor<Func<TEntity, bool>>(filteringObject.GetFilterParameters());

            return visitor.VisitAndModify(_condition);
        }
    }

    public class NamedFilter<TEntity, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> : NamedFilter<TEntity>
        where TEntity : class, IEntity
    {
        private readonly Expression<Func<TEntity, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, bool>> _condition;

        public NamedFilter(string code, Expression<Func<TEntity, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, bool>> condition, SecurityRoleType? requiredRole = null)
            : base(code, null, requiredRole)
        {
            _condition = condition;
        }

        public override Expression<Func<TEntity, bool>> GetExpression(IFilteringObject filteringObject)
        {
            var visitor = new FilterParameterReplacementVisitor<Func<TEntity, bool>>(filteringObject.GetFilterParameters());

            return visitor.VisitAndModify(_condition);
        }
    }

    public class NamedFilter<TEntity, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> : NamedFilter<TEntity>
        where TEntity : class, IEntity
    {
        private readonly Expression<Func<TEntity, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, bool>> _condition;

        public NamedFilter(string code, Expression<Func<TEntity, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, bool>> condition, SecurityRoleType? requiredRole = null)
            : base(code, null, requiredRole)
        {
            _condition = condition;
        }

        public override Expression<Func<TEntity, bool>> GetExpression(IFilteringObject filteringObject)
        {
            var visitor = new FilterParameterReplacementVisitor<Func<TEntity, bool>>(filteringObject.GetFilterParameters());

            return visitor.VisitAndModify(_condition);
        }
    }

    public class NamedFilter<TEntity, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> : NamedFilter<TEntity>
        where TEntity : class, IEntity
    {
        private readonly Expression<Func<TEntity, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, bool>> _condition;

        public NamedFilter(string code, Expression<Func<TEntity, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, bool>> condition, SecurityRoleType? requiredRole = null)
            : base(code, null, requiredRole)
        {
            _condition = condition;
        }

        public override Expression<Func<TEntity, bool>> GetExpression(IFilteringObject filteringObject)
        {
            var visitor = new FilterParameterReplacementVisitor<Func<TEntity, bool>>(filteringObject.GetFilterParameters());

            return visitor.VisitAndModify(_condition);
        }
    }

    public class NamedFilter<TEntity, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> : NamedFilter<TEntity>
        where TEntity : class, IEntity
    {
        private readonly Expression<Func<TEntity, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, bool>> _condition;

        public NamedFilter(string code, Expression<Func<TEntity, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, bool>> condition, SecurityRoleType? requiredRole = null)
            : base(code, null, requiredRole)
        {
            _condition = condition;
        }

        public override Expression<Func<TEntity, bool>> GetExpression(IFilteringObject filteringObject)
        {
            var visitor = new FilterParameterReplacementVisitor<Func<TEntity, bool>>(filteringObject.GetFilterParameters());

            return visitor.VisitAndModify(_condition);
        }
    }
}