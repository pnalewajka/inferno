using System.Collections.Generic;

namespace Smt.Atomic.Business.Common.Dto
{
    public class RecordChangedEventArgs<TEntity>
    {
        public IList<TEntity> ChangedEntities { get; }

        public RecordChangedEventArgs(TEntity changedEntity)
        {
            ChangedEntities = new List<TEntity> { changedEntity };
        }

        public RecordChangedEventArgs(IList<TEntity> changedEntities)
        {
            ChangedEntities = changedEntities;
        }
    }
}