using System.Collections.Generic;
using Smt.Atomic.Business.Common.Interfaces;

namespace Smt.Atomic.Business.Common.Dto
{
    public class QueryCriteria : SortingCriteria
    {
        public QueryCriteria()
        {
            Filters = new List<FilterCodeGroup>();
            FilterObjects = new Dictionary<string, IFilteringObject>();
            SearchCriteria = new SearchCriteria();
        }
        /// <summary>
        /// Additional information about context, for example parent id for SubCardIndex
        /// </summary>
        public object Context { get; set; }

        /// <summary>
        /// Text entered in the card index search box
        /// </summary>
        public string SearchPhrase { get; set; }

        /// <summary>
        /// Named filters which are turned on
        /// </summary>
        public List<FilterCodeGroup> Filters { get; set; }

        /// <summary>
        /// Parameters (dictionary from viewmodel) for advanced filters
        /// </summary>
        public Dictionary<string, IFilteringObject> FilterObjects { get; set; }

        /// <summary>
        /// Current page index (number of pages to skip)
        /// </summary>
        public int? CurrentPageIndex { get; set; }

        /// <summary>
        /// Page size which will override page size specified in the service
        /// </summary>
        public int? PageSize { get; set; }

        /// <summary>
        /// Query string parameters
        /// </summary>
        public IDictionary<string, object> QueryStringFieldValues { get; set; }

        /// <summary>
        /// Handles areas to search
        /// </summary>
        public SearchCriteria SearchCriteria { get; set; }
    }
}