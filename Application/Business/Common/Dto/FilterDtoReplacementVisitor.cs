using System;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Smt.Atomic.Business.Common.Dto
{
    /// <summary>
    /// This is expression visitor for removal of Dto based parameters from expression tree
    /// For each parameter that comes out of specific dto, MemberAccess in expression is replaced with actual value from dto
    /// </summary>
    /// <typeparam name="TOutputExpression"></typeparam>
    internal class FilterDtoReplacementVisitor<TOutputExpression> : ExpressionVisitor
    {
        private readonly object _filterParameters;
        private readonly Type _objectType;
        private Expression _rootExpression;

        public FilterDtoReplacementVisitor(object filterParameters)
        {
            if (filterParameters == null)
            {
                throw new ArgumentNullException(nameof(filterParameters));
            }

            _filterParameters = filterParameters;
            _objectType = _filterParameters.GetType();
        }

        public Expression<TOutputExpression> VisitAndModify<TExpression>(Expression<TExpression> rootExpression)
        {
            _rootExpression = rootExpression;

            return (Expression<TOutputExpression>)VisitLambda(rootExpression);
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            var result = base.VisitMember(node);

            if (node.NodeType != ExpressionType.MemberAccess || node.Expression == null 
                || node.Expression.NodeType != ExpressionType.Parameter || node.Expression.Type != _objectType)
            {
                return result;
            }

            var member = node.Member as PropertyInfo;

            if (member == null) 
            {
                return result;
            }

            var propertyValue = member.GetValue(_filterParameters);

            if (member.PropertyType.IsArray)
            {
                var elementType = member.PropertyType.GetElementType();

                if (elementType == null)
                {
                    throw new InvalidDataException();
                }

                var values = (Array)propertyValue ?? Array.CreateInstance(elementType, 0);

                return Expression.NewArrayInit(elementType, values.OfType<object>().Select(Expression.Constant));
            }

            return Expression.Constant(propertyValue, member.PropertyType);
        }

        protected override Expression VisitLambda<T>(Expression<T> node)
        {
            bool isNestedLambda = _rootExpression != node;

            var expression = Visit(node.Body);

            var parameters = node.Parameters.Where(p => p.Type != _objectType).ToList();

            if (isNestedLambda)
            {
                return Expression.Lambda<T>(expression, parameters);
            }

            return Expression.Lambda<TOutputExpression>(expression, parameters);
        }
    }
}