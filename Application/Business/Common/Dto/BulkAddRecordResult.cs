﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Common.Dto
{
    public class BulkAddRecordResult : RecordOperationResult
    {
        public IList<long> AddedRecordIds { get; set; }

        public BulkAddRecordResult(bool isSuccessful, IList<long> addedRecordIds, IEnumerable<PropertyGroup> uniqueKeyViolations, IEnumerable<AlertDto> alerts = null)
            : base(uniqueKeyViolations, isSuccessful, alerts)
        {
            AddedRecordIds = addedRecordIds;
        }

        public static BulkAddRecordResult Default => new BulkAddRecordResult(true, new List<long>(), Enumerable.Empty<PropertyGroup>() );
        public static BulkAddRecordResult Fail => new BulkAddRecordResult(false, new List<long>(), null);
    }
}
