﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Common.Dto
{
    public class CardIndexRecords<TDto> 
    {
        public IList<TDto> Rows { get; private set; }
        public long RecordCount { get; private set; }
        public int? CurrentPageIndex { get; private set; }
        public int? LastPageIndex { get; private set; }
        public IReadOnlyCollection<AlertDto> Alerts { get; set; }

        public CardIndexRecords(IList<TDto> rows, int? currentPageIndex, int? lastPageIndex, long recordCount, IReadOnlyCollection<AlertDto> alerts = null)
        {
            Rows = rows;
            RecordCount = recordCount;
            CurrentPageIndex = currentPageIndex;
            LastPageIndex = lastPageIndex;
            Alerts = alerts ?? new List<AlertDto>();
        }
    }
}