using System;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Business.Common.Dto
{
    public class NamedDtoFilter<TEntity, TDto> : NamedFilter<TEntity> where TEntity : class, IEntity
    {
        private readonly Expression<Func<TEntity, TDto, bool>> _condition;

        public NamedDtoFilter(string code, Expression<Func<TEntity, TDto, bool>> condition) :base(code, null)
        {
            _condition = condition;
        }

        public override Expression<Func<TEntity, bool>> GetExpression(IFilteringObject filterObject)
        {
            var dto = filterObject.GetFilterDto();
            var visitor = new FilterDtoReplacementVisitor<Func<TEntity, bool>>(dto);

            return visitor.VisitAndModify(_condition);
        }
    }
}