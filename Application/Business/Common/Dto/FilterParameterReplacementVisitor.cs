using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Business.Common.Dto
{
    /// <summary>
    /// This is expression visitor for removal of constant parameters from expression:
    /// Assuming following expression: 
    /// &lt;User, DateTime, DateTime&gt; (u, from, to) =&gt; u.ModifiedOn &gt; from && u.ModifiedOn &lt; to
    /// And having list of values for 'DateTime from' and 'DateTime to'
    /// following visitor will replace occurences of 'to' and 'from' parameters in expression body with constant values leaving 
    /// with expressio similar to
    /// &lt;User&gt; u=&gt; u.ModifiedOn > '2012-01-01' && u.ModifiedOn &lt; '2015-01-01'
    /// </summary>
    /// <typeparam name="TOutputExpression"></typeparam>
    internal class FilterParameterReplacementVisitor<TOutputExpression> : ExpressionVisitor
    {
        private readonly Dictionary<string, FilterParameter> _filterParameters;
        private readonly HashSet<string> _replacedParameterNames = new HashSet<string>();
        private Expression _rootExpression;

        public FilterParameterReplacementVisitor([NotNull]FilterParameters filterParameters)
        {
            if (filterParameters == null)
            {
                throw new ArgumentNullException(nameof(filterParameters));
            }

            _filterParameters = filterParameters.ToDictionary(
                k => NamingConventionHelper.ConvertHyphenatedToCamelCase(UrlFieldsHelper.GetUnprefixedUrlFieldName(k.Key)),
                v => v.Value);
        }

        /// <summary>
        /// Adjust incoming expression to required form of TOutputExpression by replacing parameters
        /// Expression form will be transformed like in case below
        /// Func&lt;User,DateTime,DateTime,bool&gt; is requested to change its form to Func&lt;User,bool&gt;
        /// </summary>
        /// <typeparam name="TExpression"></typeparam>
        /// <param name="rootExpression"></param>
        /// <returns></returns>
        public Expression<TOutputExpression> VisitAndModify<TExpression>(Expression<TExpression> rootExpression)
        {
            _rootExpression = rootExpression;

            return (Expression<TOutputExpression>)VisitLambda(rootExpression);
        }

        protected override Expression VisitParameter(ParameterExpression node)
        {
            //Is parameter requested for replacement?
            if (!_filterParameters.ContainsKey(node.Name))
            {
                return base.VisitParameter(node);
            }

            var filterParameter = _filterParameters[node.Name];
            _replacedParameterNames.Add(node.Name);

            if (filterParameter.Type.IsArray)
            {
                var elementType = filterParameter.Type.GetElementType();

                if (elementType == null)
                {
                    throw new InvalidDataException();
                }

                var values = (Array)filterParameter.Value ?? Array.CreateInstance(elementType, 0);

                return Expression.NewArrayInit(elementType, values.OfType<object>().Select(Expression.Constant));
            }

            return Expression.Constant(filterParameter.Value, filterParameter.Type);
        }

        protected override Expression VisitLambda<T>(Expression<T> node)
        {
            bool isNestedLambda = _rootExpression != node;

            var expression = Visit(node.Body);

            //Provide a list of parameters that were not replaced
            var parameters = node.Parameters
                .Where(p => !_replacedParameterNames.Contains(p.Name))
                .ToList();

            if (isNestedLambda)
            {
                return Expression.Lambda<T>(expression, parameters);
            }

            // Help for Exception: Incorrect number of parameters supplied for lambda declaration
            //   - Expression parameter names (lambda parameters) and the viewmodel property names must be equal

            return Expression.Lambda<TOutputExpression>(expression, parameters);
        }
    }
}