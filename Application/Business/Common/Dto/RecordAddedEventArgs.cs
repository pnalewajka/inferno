using System.Collections.Generic;

namespace Smt.Atomic.Business.Common.Dto
{
    public class RecordAddedEventArgs<TEntity, TDto>
    {
        public object Context { get; set; }
        public TEntity InputEntity { get; set; }
        public TDto InputDto { get; set; }
        public IList<AlertDto> Alerts { get; private set; }

        public RecordAddedEventArgs(TEntity inputEntity, TDto inputDto, object context)
        {
            Context = context;
            InputEntity = inputEntity;
            InputDto = inputDto;
            Alerts = new List<AlertDto>();
        }
    }
}