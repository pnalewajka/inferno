﻿using System;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Business.Common.Dto
{
    public class DocumentDto
    {
        public long? DocumentId { get; set; }
        public Guid? TemporaryDocumentId { get; set; }
        public string ContentType { get; set; }
        public string DocumentName { get; set; }

        public IEntity TargetEntity { get; set; }

        public bool IsTemporary => TemporaryDocumentId.HasValue;
    }
}
