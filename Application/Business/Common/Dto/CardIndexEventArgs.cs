using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Common.Dto
{
    public class CardIndexEventArgs
    {
        public bool Canceled { get; set; }
        public object Context { get; private set; }
        public IList<AlertDto> Alerts { get; private set; }

        public CardIndexEventArgs(object context)
        {
            Context = context;
            Canceled = false;

            var alerts = new ObservableCollection<AlertDto>();
            alerts.CollectionChanged += AlertsOnCollectionChanged;
            Alerts = alerts;            
        }

        public void AddInformation(string message, bool isDismissable = true)
        {
            AddAlert(AlertType.Information, message, isDismissable);
        }

        public void AddWarning(string message, bool isDismissable = true)
        {
            AddAlert(AlertType.Warning, message, isDismissable);
        }

        public void AddSuccess(string message, bool isDismissable = true)
        {
            AddAlert(AlertType.Success, message, isDismissable);
        }

        public void AddError(string message, bool isDismissable = true)
        {
            AddAlert(AlertType.Error, message, isDismissable);
        }

        private void AddAlert(AlertType type, string message, bool isDismissable)
        {
            var alertDto = new AlertDto { Message = message, IsDismissable = isDismissable, Type = type };
            Alerts.Add(alertDto);
        }

        private void AlertsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            if (notifyCollectionChangedEventArgs.NewItems.OfType<AlertDto>().Any(a => a.Type == AlertType.Error))
            {
                Canceled = true;
            }
        }
    }
}