﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Dto.GDPR;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.GDPR;

namespace Smt.Atomic.Business.GDPR.Dto
{
    public class DataConsentToDataConsentDtoMapping : ClassMapping<DataConsent, DataConsentDto>
    {
        public DataConsentToDataConsentDtoMapping()
        {
            Mapping = e => new DataConsentDto
            {
                Id = e.Id,
                Type = e.Type,
                ExpiresOn = e.ExpiresOn,
                DataAdministratorId = e.DataAdministratorId,
                Scope = e.Scope,
                Documents = e.Attachments.Select(p => new DocumentDto
                {
                    ContentType = p.ContentType,
                    DocumentName = p.Name,
                    DocumentId = p.Id
                })
                .ToArray(),
                DataOwnerId = e.DataOwnerId
            };
        }
    }
}
