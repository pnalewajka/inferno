﻿using Smt.Atomic.Business.Common.Dto.GDPR;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.GDPR;

namespace Smt.Atomic.Business.GDPR.Dto
{
    public class DataActivityToDataActivityDtoMapping : ClassMapping<DataActivity, DataActivityDto>
    {
        public DataActivityToDataActivityDtoMapping()
        {
            Mapping = e => new DataActivityDto
            {
                Id = e.Id,
                DataOwnerId = e.DataOwnerId,
                PerformedOn = e.PerformedOn,
                PerformedByUserId = e.PerformedByUserId,
                ActivityType = e.ActivityType,
                ReferenceId = e.ReferenceId,
                ReferenceType = e.ReferenceType,
                S1 = e.S1,
                S2 = e.S2,
                S3 = e.S3,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
