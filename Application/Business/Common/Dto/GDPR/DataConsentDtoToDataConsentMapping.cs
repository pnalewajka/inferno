﻿using System.Collections.ObjectModel;
using Smt.Atomic.Business.Common.Dto.GDPR;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.GDPR;

namespace Smt.Atomic.Business.GDPR.Dto
{
    public class DataConsentDtoToDataConsentMapping : ClassMapping<DataConsentDto, DataConsent>
    {
        public DataConsentDtoToDataConsentMapping()
        {
            Mapping = d => new DataConsent
            {
                Id = d.Id,
                Type = d.Type,
                ExpiresOn = d.ExpiresOn,
                DataAdministratorId = d.DataAdministratorId,
                Scope = d.Scope,
                Attachments = new Collection<DataConsentDocument>(),
                DataOwnerId = d.DataOwnerId
            };
        }
    }
}
