﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Common.Dto.GDPR
{
    public class DataConsentDto
    {
        public long Id { get; set; }

        public DataConsentType Type { get; set; }

        public DateTime? ExpiresOn { get; set; }

        public long DataAdministratorId { get; set; }

        public DocumentDto[] Documents { get; set; }

        public FileSource FileSource { get; set; }

        public string Scope { get; set; }

        public long DataOwnerId { get; set; }

        public long ApplicantId { get; set; }

        public DataConsentDto()
        {
            Documents = new DocumentDto[0];
        }
    }
}
