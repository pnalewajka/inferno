﻿using System;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Common.Dto.GDPR

{
    public class DataActivityDto
    {
        public long Id { get; set; }

        public long DataOwnerId { get; set; }

        public DateTime PerformedOn { get; set; }

        public long PerformedByUserId { get; set; }

        public ActivityType ActivityType { get; set; }

        public long? ReferenceId { get; set; }

        public ReferenceType? ReferenceType { get; set; }

        public string S1 { get; set; }

        public string S2 { get; set; }

        public string S3 { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
