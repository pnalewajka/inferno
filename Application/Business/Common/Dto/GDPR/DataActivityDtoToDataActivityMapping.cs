﻿using Smt.Atomic.Business.Common.Dto.GDPR;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.GDPR;

namespace Smt.Atomic.Business.GDPR.Dto
{
    public class DataActivityDtoToDataActivityMapping : ClassMapping<DataActivityDto, DataActivity>
    {
        public DataActivityDtoToDataActivityMapping()
        {
            Mapping = d => new DataActivity
            {
                Id = d.Id,
                DataOwnerId = d.DataOwnerId,
                PerformedOn = d.PerformedOn,
                PerformedByUserId = d.PerformedByUserId,
                ActivityType = d.ActivityType,
                ReferenceId = d.ReferenceId,
                ReferenceType = d.ReferenceType,
                S1 = d.S1,
                S2 = d.S2,
                S3 = d.S3,
                Timestamp = d.Timestamp
            };
        }
    }
}
