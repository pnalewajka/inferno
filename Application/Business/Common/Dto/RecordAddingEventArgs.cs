using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.Common.Dto
{
    public class RecordAddingEventArgs<TEntity, TDto, TDbScope> : CardIndexEventArgs
        where TDbScope : IDbScope
    {
        public IUnitOfWork<TDbScope> UnitOfWork { get; private set; }
        public TEntity InputEntity { get; set; }
        public TDto InputDto { get; set; }

        public RecordAddingEventArgs(IUnitOfWork<TDbScope> unitOfWork, TEntity inputEntity, TDto inputDto, object context) : base(context)
        {
            UnitOfWork = unitOfWork;
            InputEntity = inputEntity;
            InputDto = inputDto;
        }
    }
}