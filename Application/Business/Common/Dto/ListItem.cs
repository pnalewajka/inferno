﻿using System.Diagnostics;

namespace Smt.Atomic.Business.Common.Dto
{
    [DebuggerDisplay("{DisplayName} ({Id})")]
    public class ListItem
    {
        public long? Id { get; set; }

        public string DisplayName { get; set; }

        public string OptionId => $"{Id}";
    }
}