using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Common.Dto
{
    public class BusinessResult
    {
        public IList<AlertDto> Alerts { get; private set; }

        public BusinessResult()
            : this(null)
        {
        }

        public BusinessResult(AlertType type, [Localizable(true)] string message, bool isDismissable = true)
            : this(new[] {new AlertDto {Type = type, Message = message, IsDismissable = isDismissable}})
        {
        }

        public BusinessResult(IEnumerable<AlertDto> alerts)
        {
            var collection = new ObservableCollection<AlertDto>(alerts ?? Enumerable.Empty<AlertDto>());
            collection.CollectionChanged += AlertsOnCollectionChanged;

            Alerts = collection;
        }

        public void AddAlert(AlertType type, [Localizable(true)]string message, bool isDismissable = true)
        {
            Alerts.Add(new AlertDto { Type = type, Message = message, IsDismissable = isDismissable });
        }

        protected virtual void AlertsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
        }

        public bool ContainsErrors => Alerts.Any(a => a.Type == AlertType.Error);

        public string GetMessages()
        {
            return string.Join(" ", Alerts.Select(a => a.Message));
        }
    }
}