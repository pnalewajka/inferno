using System.Collections.Generic;

namespace Smt.Atomic.Business.Common.Dto
{
    public class RecordEditedEventArgs<TEntity, TDto>
    {
        public object Context { get; private set; }
        public TEntity InputEntity { get; private set; }
        public TEntity DbEntity { get; private set; }
        public TDto InputDto { get; private set; }
        public TDto OriginalDto { get; private set; }
        public IList<AlertDto> Alerts { get; private set; }

        public RecordEditedEventArgs(TEntity dbEntity, TEntity inputEntity, TDto inputDto, TDto originalDto, object context)
        {
            Context = context;
            DbEntity = dbEntity;
            InputEntity = inputEntity;
            InputDto = inputDto;
            OriginalDto = originalDto;
            Alerts = new List<AlertDto>();
        }
    }
}