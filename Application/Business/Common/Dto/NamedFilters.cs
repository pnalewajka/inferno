﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Common.Dto
{
    public class NamedFilters<TEntity> where TEntity : class
    {
        private readonly IList<NamedFilter<TEntity>> _items;
        
        public IEnumerable<NamedFilter<TEntity>> Items => _items;

        public NamedFilter<TEntity> this[string code] => GetFilterByCode(code);

        public NamedFilters()
        {
            _items = new List<NamedFilter<TEntity>>();
        }

        public NamedFilters(IEnumerable<NamedFilter<TEntity>> items)
        {
            _items = items.ToList();
        }

        public NamedFilters(NamedFilters<TEntity> namedFilters)
        {
            _items = namedFilters.Items.ToList();
        }

        /// <summary>
        /// Append filters with unique codes
        /// </summary>
        /// <param name="namedFilters"></param>
        public void AppendUniqueFilters(NamedFilters<TEntity> namedFilters)
        {
            foreach (var namedFilter in namedFilters.Items)
            {
                if (_items.All(f => f.Code != namedFilter.Code))
                {
                    _items.Add(namedFilter);
                }
            }
        }

        public NamedFilter<TEntity> GetFilterByCode(string code)
        {
            code = code.ToUpperInvariant();
            return _items.FirstOrDefault(f => f.Code.ToUpperInvariant() == code);
        }

        public IEnumerable<KeyValuePair<string, SecurityRoleType>> GetRoles()
        {
            return _items.Where(f => f.RequiredRole.HasValue)
                    .Select(f => new KeyValuePair<string, SecurityRoleType>(f.Code, f.RequiredRole.Value));
        }

        public IEnumerable<string> GetFilterCodes()
        {
            return _items.Select(f => f.Code);
        }

        public bool IsNullOrEmpty()
        {
            return Items == null || !Items.Any();
        }
        
        public static NamedFilters<TEntity> Empty()
        {
            return new NamedFilters<TEntity>(Enumerable.Empty<NamedFilter<TEntity>>());
        }
    }
}
