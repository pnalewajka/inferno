﻿using System;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Common.Dto
{
    public class TemporaryDocumentDto
    {
        public string Name { get; set; }
        public string ContentType { get; set; }
        public TemporaryDocumentState State { get; set; }
        public Guid TemporaryDocumentId { get; set; }
    }
}
