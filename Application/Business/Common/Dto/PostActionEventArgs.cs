namespace Smt.Atomic.Business.Common.Dto
{
    public class PostActionEventArgs
    {
        public object Context { get; private set; }

        public PostActionEventArgs(object context)
        {
            Context = context;
        }
    }
}