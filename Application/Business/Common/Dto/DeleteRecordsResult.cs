namespace Smt.Atomic.Business.Common.Dto
{
    public class DeleteRecordsResult : BoolResult
    {
        public long? FailedRecordId { get; set; }
        public long DeletedCount { get; set; }

        public DeleteRecordsResult(bool isSuccessful, long deletedCount, long? failedRecordId = null) 
            : base(isSuccessful)
        {
            FailedRecordId = failedRecordId;
            DeletedCount = deletedCount;
        }
    }
}