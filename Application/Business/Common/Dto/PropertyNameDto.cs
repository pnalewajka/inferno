﻿using System;

namespace Smt.Atomic.Business.Common.Dto
{
    public class PropertyNameDto
    {
        public int Id { get; set; }

        public string PropertyName { get; set; }

        public Type ViewModelType { get; set; }

        public override string ToString()
        {
            return PropertyName;
        }
    }
}
