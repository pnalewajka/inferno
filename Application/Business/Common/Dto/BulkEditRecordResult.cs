﻿using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Common.Dto
{
    public class BulkEditRecordResult : RecordOperationResult
    {
        public long[] EditedRecordIds { get; set; }

        public BulkEditRecordResult(bool isSuccessful, long[] editedRecordIds, IEnumerable<PropertyGroup> uniqueKeyViolations, IEnumerable<AlertDto> alerts = null)
            : base(uniqueKeyViolations, isSuccessful, alerts)
        {
            EditedRecordIds = editedRecordIds;
        }
    }
}
