using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.Common.Dto
{
    public class RecordEditingEventArgs<TEntity, TDto, TDbScope> : CardIndexEventArgs
        where TDbScope : IDbScope
    {
        public TEntity InputEntity { get; set; }
        public IUnitOfWork<TDbScope> UnitOfWork { get; private set; }
        public TEntity DbEntity { get; set; }
        public TDto InputDto { get; set; }

        public RecordEditingEventArgs(IUnitOfWork<TDbScope> unitOfWork, TEntity dbEntity, TEntity inputEntity, TDto inputDto, object context) : base(context)
        {
            UnitOfWork = unitOfWork;
            DbEntity = dbEntity;
            InputEntity = inputEntity;
            InputDto = inputDto;
        }
    }
}