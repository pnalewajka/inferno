﻿using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Common.Dto
{
    public class RecordOperationResult : BoolResult
    {
        public IEnumerable<PropertyGroup> UniqueKeyViolations { get; set; }

        public RecordOperationResult()
        {
        }

        public RecordOperationResult(IEnumerable<PropertyGroup> uniqueKeyViolations, bool isSuccessful, IEnumerable<AlertDto> alerts = null)
            : base(isSuccessful, alerts)
        {
            UniqueKeyViolations = uniqueKeyViolations;
        }
    }
}
