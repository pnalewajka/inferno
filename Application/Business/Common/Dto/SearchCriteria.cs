﻿using System.Collections.Generic;
using System.Linq;

namespace Smt.Atomic.Business.Common.Dto
{
    /// <summary>
    /// Holds areas in which records should be searched
    /// </summary>
    public class SearchCriteria
    {
        public ISet<string> Codes { get; }

        public SearchCriteria()
        {
            Codes = new HashSet<string>();
        }

        public bool Includes(string code)
        {
            return !Codes.Any() || Codes.Contains(code);
        }
    }
}