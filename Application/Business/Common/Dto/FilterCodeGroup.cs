using System.Collections.Generic;

namespace Smt.Atomic.Business.Common.Dto
{
    /// <summary>
    /// Collection of filter codes selected by the user within a given filter group
    /// </summary>
    public class FilterCodeGroup
    {
        /// <summary>
        /// Should the filters be used in conjuction?
        /// </summary>
        public bool IsConjunction { get; set; }

        /// <summary>
        /// What codes were selected in this group?
        /// </summary>
        public List<string> Codes { get; set; }

        public FilterCodeGroup()
        {
            Codes = new List<string>();
        }
    }
}