using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Common.Dto
{
    public class RecordsRetrievedEventArgs<TEntity>
    {
        public object Context { get; set; }
        public IList<TEntity> RetrievedEntities { get; set; }
        public RetrievalScenario Scenario { get; set; }

        public RecordsRetrievedEventArgs(IList<TEntity> entities, object context, RetrievalScenario scenario)
        {
            RetrievedEntities = entities;
            Context = context;
            Scenario = scenario;
        }

        public RecordsRetrievedEventArgs(TEntity entity, object context)
            : this(new List<TEntity> { entity }, context, RetrievalScenario.SingleRecord)
        {
        }
    }
}