﻿namespace Smt.Atomic.Business.Common.Dto
{
    public class DocumentDownloadDto
    {
        public string DocumentName { get; set; }
        public string ContentType { get; set; }
        public long ContentLength { get; set; }
        public byte[] Content { get; set; }
    }
}
