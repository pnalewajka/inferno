﻿namespace Smt.Atomic.Business.Common.Dto
{
    public class GeographyPointDto
    {
        public double Longtitude { get; set; }

        public double Latitude { get; set; }
    }
}
