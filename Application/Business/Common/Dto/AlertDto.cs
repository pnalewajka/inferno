using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Common.Dto
{
    public class AlertDto
    {
        public AlertType Type { get; set; }
        public string Message { get; set; }
        public bool IsDismissable { get; set; }

        public static AlertDto CreateError(string message)
        {
            return new AlertDto
            {
                Type = AlertType.Error,
                Message = message
            };
        }
    }
}