﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Common.Dto
{
    public class DynamicDto
    {
        public string Definition { get; set; }

        public IDictionary<string, object> Data { get; set; }

        public DynamicDto()
        {
            Data = new Dictionary<string, object>();
        }

        public DynamicDto(string definition)
        {
            Definition = definition;
            Data = new Dictionary<string, object>();
        }
    }
}
