using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Common.Dto
{
    public class EditRecordResult : RecordOperationResult
    {
        public long EditedRecordId { get; set; }

        public EditRecordResult(bool isSuccessful, long editedRecordId, IEnumerable<PropertyGroup> uniqueKeyViolations, IEnumerable<AlertDto> alerts = null)
            : base(uniqueKeyViolations, isSuccessful, alerts)
        {
            EditedRecordId = editedRecordId;
        }
    }
}