using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Business.Common.Dto
{
    public class SortingCriterion
    {
        private const string VirtualColumnNamePrefix = @"Virtual";

        public bool Ascending { get; private set; }

        private string ColumnName { get; set; }
        private string ViewModelColumnName { get; set; }
        private string VirtualColumnName { get; set; }
        private bool IsVirtual { get; set; }

        public SortingCriterion(string columnName, bool ascending)
            : this(columnName, columnName, ascending)
        {
        }        
        
        public SortingCriterion(string columnName, string viewModelColumnName, bool ascending)
        {
            ColumnName = columnName;
            ViewModelColumnName = viewModelColumnName;
            Ascending = ascending;

            if (columnName != null && columnName.StartsWith(VirtualColumnNamePrefix))
            {
                ColumnName = columnName.TrimStart(VirtualColumnNamePrefix.ToCharArray());
                VirtualColumnName = columnName;
                IsVirtual = true;
            }
        }

        public string GetSortingColumnName()
        {
            return ColumnName;
        }

        public string GetPresentationColumnName()
        {
            return IsVirtual ? VirtualColumnName : ViewModelColumnName;
        }

        public SortingDirection GetSortingDirection()
        {
            return Ascending ? SortingDirection.Ascending : SortingDirection.Descending;
        }

        public static SortingCriterion ForProperty<TDto>(Expression<Func<TDto, object>> propertyNameExpression, bool ascending = true)
        {
            return new SortingCriterion(PropertyHelper.GetPropertyName(propertyNameExpression), ascending);
        }

        /// <summary>
        /// Sorting criterion column name is restricted for current user
        /// </summary>
        /// <param name="readRestrictedProperties"></param>
        /// <returns></returns>
        public bool IsReadRestricted(HashSet<string> readRestrictedProperties)
        {
            if (readRestrictedProperties == null)
            {
                return false;
            }

            return readRestrictedProperties.Contains(ColumnName);
        }
    }
}