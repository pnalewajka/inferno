using System;

namespace Smt.Atomic.Business.Common.Dto
{
    /// <summary>
    /// Filter parameter type and value
    /// </summary>
    public class FilterParameter
    {
        /// <summary>
        /// Parameter type (resolved if nullable)
        /// </summary>
        public Type Type { get; private set; }

        /// <summary>
        /// Value
        /// </summary>
        public object Value { get; private set; }

        /// <summary>
        /// Object property name
        /// </summary>
        public string PropertyName { get; set; }

        /// <summary>
        /// Url field name
        /// </summary>
        public string UrlFieldName { get; private set; }

        public FilterParameter(Type type, object value, string propertyName, string urlFieldName)
        {
            Value = value;
            PropertyName = propertyName;
            UrlFieldName = urlFieldName;
            Type = type;
        }
    }
}