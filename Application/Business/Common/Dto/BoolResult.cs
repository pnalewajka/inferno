using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Common.Dto
{
    public class BoolResult : BusinessResult
    {
        public bool IsSuccessful { get; set; }
        
        public BoolResult()
        {
        }

        public BoolResult(bool isSuccessful, IEnumerable<AlertDto> alerts = null)
            : base(alerts)
        {
            IsSuccessful = isSuccessful;
        }

        protected override void AlertsOnCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            if (notifyCollectionChangedEventArgs.NewItems.OfType<AlertDto>().Any(a => a.Type == AlertType.Error))
            {
                IsSuccessful = false;
            }
        }

        public static BoolResult Success => new BoolResult(true);

        public static BoolResult Failure(string reason) => new BoolResult(false, new[] { AlertDto.CreateError(reason) });
    }
}