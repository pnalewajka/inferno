using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Common.Dto
{
    public class AddRecordResult : RecordOperationResult
    {
        public long AddedRecordId { get; set; }

        public AddRecordResult()
        {
        }

        public AddRecordResult(bool isSuccessful, long addedRecordId, IEnumerable<PropertyGroup> uniqueKeyViolations, IEnumerable<AlertDto> alerts = null)
            : base(uniqueKeyViolations, isSuccessful, alerts)
        {
            AddedRecordId = addedRecordId;
        }

        public static AddRecordResult Fail => new AddRecordResult(false, 0, null);
    }
}