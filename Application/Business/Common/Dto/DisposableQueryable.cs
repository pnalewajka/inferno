﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Smt.Atomic.Business.Common.Dto
{
    public class DisposableQueryable<T> : IQueryable<T>, IDisposable
    {
        private readonly IQueryable<T> _queryable;
        private readonly IDisposable _disposable;

        public DisposableQueryable(IQueryable<T> queryable, IDisposable disposable = null)
        {
            _queryable = queryable;
            _disposable = disposable;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _queryable.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public Expression Expression => _queryable.Expression;

        public Type ElementType => _queryable.ElementType;

        public IQueryProvider Provider => _queryable.Provider;

        public virtual void Dispose()
        {
            if (_disposable != null)
            {
                _disposable.Dispose();
            }
        }
    }
}
