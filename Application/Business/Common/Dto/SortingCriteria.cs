using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Business.Common.Dto
{
    public class SortingCriteria
    {
        public SortingCriteria()
        {
            ReadRestrictedDtoProperties = new HashSet<string>();
        }

        /// <summary>
        /// Sorting criteria which is a list of  pairs: column name and asc/desc info
        /// </summary>
        public IReadOnlyCollection<SortingCriterion> OrderBy { get; set; }

        /// <summary>
        /// Restricted properties on dto
        /// </summary>
        public HashSet<string> ReadRestrictedDtoProperties { get; set; }

        public SortingDirection GetSortingDirectionForColumn([NotNull] string columnName)
        {
            if (columnName == null)
            {
                throw new ArgumentNullException(nameof(columnName));
            }

            if (OrderBy.IsNullOrEmpty())
            {
                return SortingDirection.Unspecified;
            }

            var sortingCriterion = OrderBy.FirstOrDefault(o => o.GetPresentationColumnName() == columnName);

            if (sortingCriterion == null)
            {
                return SortingDirection.Unspecified;
            }

            return sortingCriterion.GetSortingDirection();
        }
    }
}