using System.Collections.Generic;

namespace Smt.Atomic.Business.Common.Dto
{
    public class RecordDeletedEventArgs<TEntity>
    {
        public object Context { get; set; }
        public IList<TEntity> DeletedEntities { get; set; }

        public RecordDeletedEventArgs(IList<TEntity> entities, object context)
        {
            DeletedEntities = entities;
            Context = context;
        }
    }
}