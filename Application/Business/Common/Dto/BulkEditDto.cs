﻿using System;

namespace Smt.Atomic.Business.Common.Dto
{
    public class BulkEditDto
    {
        public long[] RecordIds { get; set; }

        public Type DtoType { get; set; }

        public string PropertyName { get; set; }

        public object PropertyValue { get; set; }

        public Type CardIndexDataServiceType { get; set; }
    }
}
