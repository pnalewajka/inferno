using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.Common.Dto
{
    public class RecordDeletingEventArgs<TEntity, TDbScope> : CardIndexEventArgs
        where TDbScope : IDbScope
    {
        public IUnitOfWork<TDbScope> UnitOfWork { get; set; }
        public TEntity DeletingRecord { get; set; }

        public RecordDeletingEventArgs(IUnitOfWork<TDbScope> unitOfWork, TEntity entity, object context)
            : base(context)
        {
            UnitOfWork = unitOfWork;
            DeletingRecord = entity;
        }
    }
}