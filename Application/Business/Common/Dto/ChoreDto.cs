﻿using System;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Common.Dto
{
    public class ChoreDto
    {
        public ChoreType Type { get; set; }

        public long Id { get; set; }

        public DateTime? DueDate { get; set; }

        public string Parameters { get; set; }
    }
}
