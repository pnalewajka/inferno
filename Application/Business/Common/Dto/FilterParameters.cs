using System.Collections.Generic;

namespace Smt.Atomic.Business.Common.Dto
{
    /// <summary>
    /// Filter parameters : filter view model property values represented as dictionary
    /// </summary>
    public class FilterParameters : Dictionary<string, FilterParameter>
    {
    }
}