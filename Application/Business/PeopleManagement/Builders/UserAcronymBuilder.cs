﻿using System;
using System.Linq;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.PeopleManagement.Interfaces;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Business.PeopleManagement.Builders
{
    public class UserAcronymBuilder
    {
        private readonly string _firstName;
        private readonly string _lastName;
        private readonly Func<string, bool> _acronymAvailabilityChecker;

        public UserAcronymBuilder(Func<string, bool> acronymAvailabilityChecker, string firstName, string lastName)
        {
            _acronymAvailabilityChecker = acronymAvailabilityChecker;

            _firstName = StringHelper.RemoveDiacritics(firstName);
            _lastName = StringHelper.RemoveDiacritics(lastName);
        }

        public string BuildAcronym()
        {
            var strategy = GetBuildingStrategy(_lastName);
            
            return strategy.BuildAcronym(_acronymAvailabilityChecker, _firstName, _lastName);
        }

        private static IUserAcronymBuildingStrategy GetBuildingStrategy(string lastName)
        {
            return HumanNameHelper.IsComplexLastName(lastName)
                ? (IUserAcronymBuildingStrategy)new ComplexSurnameAcronymBuildingStrategy()
                : new SimpleSurnameAcronymBuildingStrategy();
        }
    }
}
