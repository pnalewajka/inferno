using System;
using System.Linq;
using Smt.Atomic.Business.Common.Helpers;

namespace Smt.Atomic.Business.PeopleManagement.Builders
{
    internal sealed class ComplexSurnameAcronymBuildingStrategy : BaseAcronymBuildingStrategy
    {
        public override string BuildAcronym(Func<string, bool> acronymAvailabilityChecker, string firstName, string lastName)
        {
            var lastNameParts = HumanNameHelper.GetLastNameParts(lastName);

            var firstNameLetters = GetLetters(firstName);
            var firstLastNameLetters = GetLetters(lastNameParts[0]);
            var remainingLastNameLetters = GetLetters(string.Concat(lastNameParts.Skip(1)));

            foreach (var firstCharacter in firstNameLetters)
            {
                foreach (var secondCharacter in firstLastNameLetters)
                {
                    foreach (var thirdCharacter in remainingLastNameLetters)
                    {
                        var acronym = new string(new[] { firstCharacter, secondCharacter, thirdCharacter });

                        if (acronymAvailabilityChecker(acronym))
                        {
                            return acronym;
                        }
                    }
                }
            }

            return base.BuildAcronym(acronymAvailabilityChecker, firstName, lastName);
        }
    }
}
