﻿using System;

namespace Smt.Atomic.Business.PeopleManagement.Builders
{
    internal sealed class SimpleSurnameAcronymBuildingStrategy : BaseAcronymBuildingStrategy
    {
        public override string BuildAcronym(Func<string, bool> acronymAvailabilityChecker, string firstName, string lastName)
        {
            var firstNameLetters = GetLetters(firstName);
            var lastNameLetters = GetLetters(lastName);

            foreach (var firstCharacter in firstNameLetters)
            {
                for (var firstIndexer = 0; firstIndexer < lastNameLetters.Length; ++firstIndexer)
                {
                    var secondIndexerLimit = lastNameLetters.Length + firstIndexer + 1;

                    for (var secondIndexer = firstIndexer + 1; secondIndexer < secondIndexerLimit; ++secondIndexer)
                    {
                        var acronym = new string(new[]
                        {
                            firstCharacter,
                            lastNameLetters[firstIndexer],
                            lastNameLetters[secondIndexer % lastNameLetters.Length]
                        });

                        if (acronymAvailabilityChecker(acronym))
                        {
                            return acronym;
                        }
                    }
                }
            }

            return base.BuildAcronym(acronymAvailabilityChecker, firstName, lastName);
        }
    }
}
