﻿using System;
using System.Linq;
using Smt.Atomic.Business.PeopleManagement.Exceptions;
using Smt.Atomic.Business.PeopleManagement.Interfaces;

namespace Smt.Atomic.Business.PeopleManagement.Builders
{
    internal abstract class BaseAcronymBuildingStrategy : IUserAcronymBuildingStrategy
    {
        public virtual string BuildAcronym(Func<string, bool> acronymAvailabilityChecker, string firstName, string lastName)
        {
            throw new AcronymGenerationException(firstName, lastName);
        }

        protected string GetLetters(string baseLetters)
        {
            var alphabeth = Enumerable.Range('a', 'z' - 'a' + 1).Select(l => (char)l);

            return new string(baseLetters.ToLower().AsEnumerable().Concat(alphabeth).Distinct().ToArray());
        }
    }
}
