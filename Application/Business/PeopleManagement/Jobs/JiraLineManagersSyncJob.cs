﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Logging;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Jira.Dto;
using Smt.Atomic.Data.Jira.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.PeopleManagement.Jobs
{
    [Identifier("Job.PeopleManagement.JiraLineManagersSync")]
    [JobDefinition(
        Code = "JiraLineManagersSync",
        Description = "Updates current employee line manager and accepting person in Jira",
        ScheduleSettings = "0 0 0 * * *",
        MaxExecutioPeriodInSeconds = 3600,
        PipelineName = PipelineCodes.PeopleManagement,
        IsEnabled = false)]
    public sealed class JiraLineManagersSyncJob : IJob
    {
        private readonly ILogger _logger;
        private readonly IJiraService _jiraService;
        private readonly IApprovingPersonService _approvingPersonService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IUnitOfWorkService<IAllocationDbScope> _unitOfWorkService;

        public JiraLineManagersSyncJob(
            ILogger logger,
            IJiraService jiraService,
            IApprovingPersonService approvingPersonService,
            ISystemParameterService systemParameterService,
            IUnitOfWorkService<IAllocationDbScope> unitOfWorkService)
        {
            _logger = logger;
            _jiraService = jiraService;
            _approvingPersonService = approvingPersonService;
            _systemParameterService = systemParameterService;
            _unitOfWorkService = unitOfWorkService;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            IEnumerable<long> ids;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                try
                {
                    ids = unitOfWork.Repositories.Employees
                        .Where(e => e.CurrentEmployeeStatus != EmployeeStatus.Terminated)
                        .Select(e => e.Id)
                        .ToList();
                }
                catch (Exception exception)
                {
                    _logger.Error("Retrieving employee IDs failed", exception);

                    return JobResult.Fail();
                }

                using (var context = _jiraService.CreateReadWrite())
                {
                    var jqlTemplate = _systemParameterService.GetParameter<string>(ParameterKeys.JiraLineManagerSyncJobQuery);

                    foreach (var id in ids.TakeWhileNotCancelled(executionContext.CancellationToken).TakeIfEnoughTime(executionContext.MaxExecutionTime))
                    {
                        try
                        {
                            const string inactiveUserLoginPrefix = "AD_";

                            var employee = unitOfWork.Repositories.Employees
                                .Select(e => new
                                {
                                    Id = e.Id,
                                    Login = e.User.Login,
                                    LineManagerLogin = e.LineManager.User.Login
                                })
                                .Single(e => e.Id == id);

                            if (employee.Login.StartsWith(inactiveUserLoginPrefix))
                            {
                                continue;
                            }

                            var issues = context.GetIssuesByJql(string.Format(jqlTemplate, employee.Login));

                            if (!issues.Any())
                            {
                                _logger.Warn($"No Jira issues found for employee (ID={id})");

                                continue;
                            }

                            var acceptingPersonId = _approvingPersonService.GetStandardHRApprover(id);
                            var acceptingPersonLogin = unitOfWork.Repositories.Employees
                                .Where(e => e.Id == acceptingPersonId)
                                .Select(e => e.User.Login)
                                .SingleOrDefault();

                            foreach (var key in issues.Select(i => i.Key.Value))
                            {
                                var updateIssueDto = new UpdateIssueDto();

                                if (!string.IsNullOrEmpty(employee.LineManagerLogin))
                                {
                                    updateIssueDto.CustomFields.Add("customfield_16023", new[] { employee.LineManagerLogin });
                                }
                                else
                                {
                                    _logger.Warn($"Could not find line manager login for employee (ID={id})");
                                }

                                if (!string.IsNullOrEmpty(acceptingPersonLogin))
                                {
                                    updateIssueDto.CustomFields.Add("customfield_16012", new[] { acceptingPersonLogin });
                                }
                                else
                                {
                                    _logger.Error($"Could not find accepting person login for employee (ID={id})");
                                }

                                context.UpdateIssue(key, updateIssueDto);
                            }
                        }
                        catch (Exception exception)
                        {
                            _logger.Error($"Updating employee (ID={id}) in Jira failed", exception);
                        }
                    }
                }
            }

            return JobResult.Success();
        }
    }
}
