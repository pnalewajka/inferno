﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.PeopleManagement.Dto;
using Smt.Atomic.Business.PeopleManagement.Interfaces;
using Smt.Atomic.Business.PeopleManagement.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.Data.Entities.Modules.PeopleManagement;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.PeopleManagement.Services
{
    public class AssetTypeCardIndexDataService : CardIndexDataService<AssetTypeDto, AssetType, IPeopleManagementDbScope>, IAssetTypeCardIndexService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public AssetTypeCardIndexDataService(ICardIndexServiceDependencies<IPeopleManagementDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override IEnumerable<Expression<Func<AssetType, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return r => r.NameEn;
            yield return r => r.NamePl;
            yield return r => r.Description;
        }

        protected override NamedFilters<AssetType> NamedFilters
        {
            get
            {
                return new NamedFilters<AssetType>(new[]
                {
                    new NamedFilter<AssetType>(FilterCodes.AssetTypeActive, a => a.IsActive),
                    new NamedFilter<AssetType>(FilterCodes.AssetTypeInactive, a => !a.IsActive)
                }
                .Union(
                    CardIndexServiceHelper.BuildEnumBasedFilters<AssetType, AssetTypeCategory>(
                        r => r.Category)));
            }
        }

        public override AssetTypeDto GetDefaultNewRecord()
        {
            // Ensures that newly created records will have null value in Category property
            return new AssetTypeDto();
        }

        protected override EditRecordResult InternalEditRecord(AssetTypeDto newRecord, out AssetTypeDto originalRecord, object context)
        {
            IsNameInUse(newRecord);

            return base.InternalEditRecord(newRecord, out originalRecord, context);
        }

        protected override AddRecordResult InternalAddRecord(AssetTypeDto record, object context)
        {
            IsNameInUse(record);

            return base.InternalAddRecord(record, context);
        }

        public override IQueryable<AssetType> ApplyContextFiltering(IQueryable<AssetType> records, object context)
        {
            var currentContext = context as FilteredAssetTypePickerContext;

            if (currentContext != null)
            {
                if (currentContext.Category.HasValue)
                {
                    records = records.Where(d => d.Category == currentContext.Category.Value);
                }

                if (currentContext.ShouldBeActive)
                {
                    records = records.Where(d => d.IsActive);
                }
            }

            return base.ApplyContextFiltering(records, context);
        }

        public void IsNameInUse(AssetTypeDto newRecord)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                if (unitOfWork.Repositories.AssetTypes.Any(p => p.Id != newRecord.Id && p.NameEn == newRecord.Name.English))
                {
                    throw new BusinessException(String.Format(AssetTypeResources.FieldIsInUse, "Name.English"));
                }

                if (unitOfWork.Repositories.AssetTypes.Any(p => p.Id != newRecord.Id && p.NamePl == newRecord.Name.Polish))
                {
                    throw new BusinessException(String.Format(AssetTypeResources.FieldIsInUse, "Name.Polish"));
                }
            }
        }
    }
}
