﻿using System.Linq;
using Smt.Atomic.Business.ActiveDirectory.Interfaces;
using Smt.Atomic.Business.PeopleManagement.Builders;
using Smt.Atomic.Business.PeopleManagement.Dto;
using Smt.Atomic.Business.PeopleManagement.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.PeopleManagement;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.PeopleManagement.Services
{
    public class UserAcronymService : IUserAcronymService
    {
        private readonly IActiveDirectorySearchService _activeDirectorySearchService;
        private readonly IUnitOfWorkService<IPeopleManagementDbScope> _unitOfWorkService;
        private readonly IClassMapping<UserAcronymDto, UserAcronym> _userAcronymDtoToUserAcronymMapping;

        public UserAcronymService(
            IActiveDirectorySearchService activeDirectorySearchService,
            IUnitOfWorkService<IPeopleManagementDbScope> unitOfWorkService,
            IClassMapping<UserAcronymDto, UserAcronym> userAcronymDtoToUserAcronymMapping)
        {
            _activeDirectorySearchService = activeDirectorySearchService;
            _unitOfWorkService = unitOfWorkService;
            _userAcronymDtoToUserAcronymMapping = userAcronymDtoToUserAcronymMapping;
        }

        public string GenerateAcronym(string firstName, string lastName)
        {
            var builder = new UserAcronymBuilder(IsAcronymAvailable, firstName, lastName);

            return builder.BuildAcronym();
        }

        public void AddAcronym(UserAcronymDto userAcronymDto)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var userAcronym = _userAcronymDtoToUserAcronymMapping.CreateFromSource(userAcronymDto);

                unitOfWork.Repositories.UserAcronyms.Add(userAcronym);
                unitOfWork.Commit();
            }
        }

        private bool IsAcronymAvailable(string acronym)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return !unitOfWork.Repositories.UserAcronyms.Any(a => a.Acronym == acronym)
                    && !_activeDirectorySearchService.CheckIfUsed(acronym);
            }
        }
    }
}
