﻿using Smt.Atomic.Business.PeopleManagement.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.PeopleManagement;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.PeopleManagement.Interfaces
{
    public class EmploymentTerminationReasonService : IEmploymentTerminationReasonService
    {
        private readonly IUnitOfWorkService<IPeopleManagementDbScope> _unitOfWorkService;
        private readonly IClassMapping<EmploymentTerminationReason, EmploymentTerminationReasonDto> _employmentTerminationReasonToEmploymentTerminationReasonDtoMapping;

        public EmploymentTerminationReasonService(
            IUnitOfWorkService<IPeopleManagementDbScope> unitOfWorkService,
            IClassMapping<EmploymentTerminationReason, EmploymentTerminationReasonDto> employmentTerminationReasonToEmploymentTerminationReasonDtoMapping)
        {
            _unitOfWorkService = unitOfWorkService;
            _employmentTerminationReasonToEmploymentTerminationReasonDtoMapping = employmentTerminationReasonToEmploymentTerminationReasonDtoMapping;
        }

        public EmploymentTerminationReasonDto GetEmploymentTerminationReasonOrDefaultById(long id)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employmentTerminationReason = unitOfWork.Repositories.EmploymentTerminationReasons.GetById(id);

                if (employmentTerminationReason == null)
                {
                    return null;
                }

                var employmentTerminationReasonDto = _employmentTerminationReasonToEmploymentTerminationReasonDtoMapping.CreateFromSource(employmentTerminationReason);

                return employmentTerminationReasonDto;
            }
        }
    }
}