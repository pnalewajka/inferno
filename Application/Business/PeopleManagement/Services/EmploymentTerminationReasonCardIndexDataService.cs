﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.PeopleManagement.Dto;
using Smt.Atomic.Business.PeopleManagement.Interfaces;
using Smt.Atomic.Data.Entities.Modules.PeopleManagement;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.PeopleManagement.Services
{
    public class EmploymentTerminationReasonCardIndexDataService : CardIndexDataService<EmploymentTerminationReasonDto, EmploymentTerminationReason, IPeopleManagementDbScope>, IEmploymentTerminationReasonCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public EmploymentTerminationReasonCardIndexDataService(ICardIndexServiceDependencies<IPeopleManagementDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override IEnumerable<Expression<Func<EmploymentTerminationReason, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return r => r.NameEn;
            yield return r => r.NamePl;
            yield return r => r.DescriptionEn;
            yield return r => r.DescriptionPl;
        }
    }
}