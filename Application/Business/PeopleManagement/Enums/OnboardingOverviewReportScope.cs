﻿using Smt.Atomic.Business.PeopleManagement.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.PeopleManagement.Enums
{
    public enum OnboardingOverviewReportScope
    {
        [RoleRequired(SecurityRoleType.CanGenerateOnboardingOverviewReport)]
        [DescriptionLocalized(nameof(OnboardingRequestResources.ScopeMe), typeof(OnboardingRequestResources))]
        Me,

        [RoleRequired(SecurityRoleType.CanGenerateOnboardingOverviewReport)]
        [RoleRequired(SecurityRoleType.CanGenerateMyEmployeesOnboardingOverviewReport)]
        [DescriptionLocalized(nameof(OnboardingRequestResources.ScopeMyEmployees), typeof(OnboardingRequestResources))]
        MyEmployees,

        [RoleRequired(SecurityRoleType.CanGenerateOnboardingOverviewReport)]
        [RoleRequired(SecurityRoleType.CanGenerateAllEmployeesOnboardingOverviewReport)]
        [DescriptionLocalized(nameof(OnboardingRequestResources.ScopeAllEmployees), typeof(OnboardingRequestResources))]
        AllEmployees,
    }
}
