﻿using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.PeopleManagement.Enums;
using Smt.Atomic.Business.PeopleManagement.ReportModels;
using Smt.Atomic.Business.PeopleManagement.ReportParameters;
using Smt.Atomic.Business.PeopleManagement.Resources;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.PeopleManagement.ReportDataSources
{
    [Identifier("Report.OnboardingOverviewDataSource")]
    [RequireRole(SecurityRoleType.CanGenerateOnboardingOverviewReport)]
    [DefaultReportDefinition(
        "OnboardingOverviewPivot",
        nameof(OnboardingRequestResources.OnboardingOverviewPivotReportName),
        "Recruiters/onboarding overview",
        MenuAreas.Organization,
        MenuGroups.PeopleManagement,
        ReportingEngine.PivotTable,
        "Smt.Atomic.Business.PeopleManagement.ReportTemplates.OnboardingOverview-config.js",
        typeof(OnboardingRequestResources))]
    public class OnboardingOverviewReportDataSource
        : BaseReportDataSource<OnboardingOverviewReportParametersDto>
    {
        private readonly IUnitOfWorkService<IPeopleManagementDbScope> _unitOfWorkService;
        private readonly ITimeService _timeService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IOrgUnitService _orgUnitService;

        public OnboardingOverviewReportDataSource(
            IUnitOfWorkService<IPeopleManagementDbScope> unitOfWorkService,
            ITimeService timeService,
            IPrincipalProvider principalProvider,
            IOrgUnitService orgUnitService)
        {
            _unitOfWorkService = unitOfWorkService;
            _timeService = timeService;
            _principalProvider = principalProvider;
            _orgUnitService = orgUnitService;
        }

        public override object GetData(
            ReportGenerationContext<OnboardingOverviewReportParametersDto> context)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var onboardingRequests = ApplyFilters(
                    unitOfWork.Repositories.OnboardingRequests,
                    context.Parameters).ToList();

                return new OnboardingOverviewReportModel
                {
                    OnboardingRequests = onboardingRequests.Select(r => new OnboardingOverviewReportModel.OnboardingRequestModel
                    {
                        Recruiter = r.Recruiter?.FullName ?? string.Empty,
                        Requester = r.Request.RequestingUser.FullName,
                        NewHire = (!r.FirstName.IsNullOrEmpty() && !r.LastName.IsNullOrEmpty())
                            ? r.FirstName + " " + r.LastName
                            : string.Empty,
                        RecruiterOrgUnit = r.Recruiter?.OrgUnit?.Name ?? string.Empty,
                        NewHireOrgUnit = r.OrgUnit?.Name ?? string.Empty,
                        NewHireLineManager = r.LineManager?.FullName ?? string.Empty,
                        StartDate = r.StartDate,
                        OnboardingDate = r.OnboardingDate,
                        IsForeignEmployee = r.IsForeignEmployee,
                        IsWelcomeAnnounceEmailInfoDone = !r.WelcomeAnnounceEmailInformation.IsNullOrEmpty(),
                        IsPhotoDocumentsDone = r.WelcomeAnnounceEmailPhotos.Any(),
                        IsPayrollDocumentsDone = r.PayrollDocuments.Any(),
                        IsForeignEmployeeDocumentsDone = !r.IsForeignEmployee || r.ForeignEmployeeDocuments.Any(),
                        RequestStatus = r.Request.Status,
                        AssetsRequestStatus = r.AssetsRequest.SingleOrDefault()?.Request?.Status,
                    }).ToArray(),
                };
            }
        }

        public override OnboardingOverviewReportParametersDto GetDefaultParameters(ReportGenerationContext<OnboardingOverviewReportParametersDto> reportGenerationContext)
        {
            var from = _timeService.GetCurrentDate();

            return new OnboardingOverviewReportParametersDto
            {
                From = from,
                // To = from.AddMonths(3),
                Scope = OnboardingOverviewReportScope.Me,
                Statuses = EnumHelper.GetEnumValues<RequestStatus>(),
            };
        }

        private IQueryable<OnboardingRequest> ApplyFilters(
            IQueryable<OnboardingRequest> requests,
            OnboardingOverviewReportParametersDto parameters)
        {
            requests = requests.Filtered()
                .Where(DateRangeHelper.IsDateInRange<OnboardingRequest>(
                    parameters.From, parameters.To, r => r.StartDate));

            if (!parameters.Statuses.IsNullOrEmpty())
            {
                requests = requests.Where(r => parameters.Statuses.Contains(r.Request.Status));
            }

            var currentEmployeeId = _principalProvider.Current.EmployeeId;

            switch (parameters.Scope)
            {
                case OnboardingOverviewReportScope.Me:
                    {
                        requests = requests.Where(r => r.RecruiterEmployeeId == currentEmployeeId);

                        break;
                    }

                case OnboardingOverviewReportScope.MyEmployees:
                    {
                        var managerOrgUnitsIds = _orgUnitService.GetManagerOrgUnitDescendantIds(currentEmployeeId);
                        var isMyRecruiter = new BusinessLogic<OnboardingRequest, bool>(
                            r => EmployeeBusinessLogic.IsEmployeeOf.Call(
                                r.Recruiter, currentEmployeeId, managerOrgUnitsIds));

                        requests = requests.Where(isMyRecruiter);

                        break;
                    }
            }

            return requests;
        }
    }
}
