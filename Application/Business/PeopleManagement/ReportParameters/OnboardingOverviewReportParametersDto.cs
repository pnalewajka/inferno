﻿using System;
using Smt.Atomic.Business.PeopleManagement.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.PeopleManagement.ReportParameters
{
    public class OnboardingOverviewReportParametersDto
    {
        public DateTime From { get; set; }

        public DateTime? To { get; set; }

        public RequestStatus[] Statuses { get; set; }

        public OnboardingOverviewReportScope Scope { get; set; }
    }
}
