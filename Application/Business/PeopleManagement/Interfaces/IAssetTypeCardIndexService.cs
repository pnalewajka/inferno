﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.PeopleManagement.Dto;

namespace Smt.Atomic.Business.PeopleManagement.Interfaces
{
    public interface IAssetTypeCardIndexService: ICardIndexDataService<AssetTypeDto>
    {
    }
}
