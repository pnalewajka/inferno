﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Workflows.Dto;

namespace Smt.Atomic.Business.PeopleManagement.Interfaces
{
    public interface IAssetsRequestService
    {
        IList<AlertDto> OnEditingOnboarding(OnboardingRequestDto oldOnboardingRequestDto,
            OnboardingRequestDto newOnboardingRequestDto,
            string onboardingRequestViewModelTypeName);
    }
}
