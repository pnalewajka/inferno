﻿using System;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.PeopleManagement.Dto;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;

namespace Smt.Atomic.Business.PeopleManagement.Interfaces
{
    public interface IOnboardingRequestService : IRequestService
    {
        OnboardingRequestAccessDescription GetAccessDescription(RequestDto request);

        DocumentDto[] GetForeignEmployeeDocuments(long requestId, Guid secret);

        DocumentDto[] GetPayrollDocuments(long requestId, Guid secret);
        
        DocumentDto[] GetWelcomeAnnounceEmailPhotos(long requestId, Guid secret);
    }
}
