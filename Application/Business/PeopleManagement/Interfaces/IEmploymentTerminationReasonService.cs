﻿using Smt.Atomic.Business.PeopleManagement.Dto;

namespace Smt.Atomic.Business.PeopleManagement.Interfaces
{
    public interface IEmploymentTerminationReasonService
    {
        EmploymentTerminationReasonDto GetEmploymentTerminationReasonOrDefaultById(long id);
    }
}