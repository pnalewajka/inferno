﻿namespace Smt.Atomic.Business.PeopleManagement.Interfaces
{
    public static class FilterCodes
    {
        public const string AssetTypeActive = "active";
        public const string AssetTypeInactive = "inactive";
    }
}
