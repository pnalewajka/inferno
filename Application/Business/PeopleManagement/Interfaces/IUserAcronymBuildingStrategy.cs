using System;

namespace Smt.Atomic.Business.PeopleManagement.Interfaces
{
    public interface IUserAcronymBuildingStrategy
    {
        string BuildAcronym(Func<string, bool> acronymAvailabilityChecker, string firstName, string lastName);
    }
}