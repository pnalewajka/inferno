﻿using Smt.Atomic.Business.PeopleManagement.Dto;

namespace Smt.Atomic.Business.PeopleManagement.Interfaces
{
    public interface IUserAcronymService
    {
        void AddAcronym(UserAcronymDto userAcronymDto);

        string GenerateAcronym(string firstName, string lastName);
    }
}
