﻿using System;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Business.PeopleManagement.Helpers
{
    public static class UserLoginHelper
    {
        public static string GetSuggestedLogin(string firstName, string lastName)
        {
            if (string.IsNullOrEmpty(firstName))
            {
                throw new ArgumentNullException(firstName);
            }

            if (string.IsNullOrEmpty(lastName))
            {
                throw new ArgumentNullException(lastName);
            }

            const int maxLoginLength = 20;

            var login = $"{NormalizeName(firstName)}.{NormalizeName(lastName)}";

            return login.Substring(0, Math.Min(login.Length, maxLoginLength));
        }

        private static string NormalizeName(string name)
        {
            return new string(StringHelper.RemoveDiacritics(name).ToLowerInvariant().Where(IsAllowedCharacter).ToArray());
        }

        private static bool IsAllowedCharacter(char character)
        {
            return character == '-' || char.IsLetter(character);
        }
    }
}
