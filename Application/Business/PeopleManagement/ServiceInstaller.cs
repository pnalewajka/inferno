﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.PeopleManagement.Interfaces;
using Smt.Atomic.Business.PeopleManagement.Services;
using Smt.Atomic.Business.PeopleManagement.Workflows.Services;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.PeopleManagement
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            if (containerType == ContainerType.WebApp)
            {
                container.Register(Component.For<IUserAcronymService>().ImplementedBy<UserAcronymService>().LifestyleTransient());
                container.Register(Component.For<IEmploymentTerminationReasonService>().ImplementedBy<EmploymentTerminationReasonService>().LifestyleTransient());
                container.Register(Component.For<IRequestService>().ImplementedBy<AddEmployeeRequestService>().LifestyleTransient());
                container.Register(Component.For<IRequestService>().ImplementedBy<AddHomeOfficeRequestService>().LifestyleTransient());
                container.Register(Component.For<IRequestService>().ImplementedBy<EmployeeDataChangeRequestService>().LifestyleTransient());
                container.Register(Component.For<IRequestService>().ImplementedBy<EmploymentDataChangeRequestService>().LifestyleTransient());
                container.Register(Component.For<IRequestService>().ImplementedBy<EmploymentTerminationRequestService>().LifestyleTransient());
                container.Register(Component.For<IRequestService, IOnboardingRequestService>().ImplementedBy<OnboardingRequestService>().LifestyleTransient());
                container.Register(Component.For<IEmploymentTerminationReasonCardIndexDataService>().ImplementedBy<EmploymentTerminationReasonCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IAssetTypeCardIndexService>().ImplementedBy<AssetTypeCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IRequestService, IAssetsRequestService>().ImplementedBy<AssetsRequestService>().LifestyleTransient());
            }

            if (containerType == ContainerType.JobScheduler)
            {
                container.Register(Component.For<IUserAcronymService>().ImplementedBy<UserAcronymService>().LifestyleTransient());
                container.Register(Component.For<IEmploymentTerminationReasonService>().ImplementedBy<EmploymentTerminationReasonService>().LifestyleTransient());
                container.Register(Component.For<IRequestService>().ImplementedBy<AddEmployeeRequestService>().LifestyleTransient());
                container.Register(Component.For<IRequestService>().ImplementedBy<AddHomeOfficeRequestService>().LifestyleTransient());
                container.Register(Component.For<IRequestService>().ImplementedBy<EmployeeDataChangeRequestService>().LifestyleTransient());
                container.Register(Component.For<IRequestService>().ImplementedBy<EmploymentDataChangeRequestService>().LifestyleTransient());
                container.Register(Component.For<IRequestService>().ImplementedBy<EmploymentTerminationRequestService>().LifestyleTransient());
                container.Register(Component.For<IRequestService, IOnboardingRequestService>().ImplementedBy<OnboardingRequestService>().LifestyleTransient());
                container.Register(Component.For<IRequestService>().ImplementedBy<AssetsRequestService>().LifestyleTransient());
            }
        }
    }
}