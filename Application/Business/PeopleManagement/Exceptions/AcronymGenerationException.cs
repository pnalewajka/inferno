﻿using System;

namespace Smt.Atomic.Business.PeopleManagement.Exceptions
{
    public class AcronymGenerationException : Exception
    {
        public AcronymGenerationException(string firstName, string lastName)
            : base($"Unable to generate the acronym for {firstName} {lastName}.")
        {
        }
    }
}