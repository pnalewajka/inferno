﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Modules.PeopleManagement;

namespace Smt.Atomic.Business.PeopleManagement.Dto
{
    public class AssetTypeToAssetTypeDtoMapping: ClassMapping<AssetType, AssetTypeDto>
    {
        public AssetTypeToAssetTypeDtoMapping()
        {
            Mapping = d => new AssetTypeDto
            {
                Id = d.Id,
                Name =  new LocalizedString()
                {
                    Polish = d.NamePl,
                    English = d.NameEn
                },
                Category = d.Category,
                DetailsRequired = d.DetailsRequired,
                DetailsHint = new LocalizedString()
                {
                    Polish = d.DetailsHintPl,
                    English = d.DetailsHintEn
                },
                IsActive = d.IsActive,
                Description = d.Description,
                Timestamp = d.Timestamp
            };
        }
    }
}
