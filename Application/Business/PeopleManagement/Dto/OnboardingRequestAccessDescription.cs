﻿using System.Collections.Generic;
using System.Linq;

namespace Smt.Atomic.Business.PeopleManagement.Dto
{
    public class OnboardingRequestAccessDescription
    {
        private readonly IDictionary<long, OnboardingRequestUserType> _employeeUserTypes;

        public OnboardingRequestAccessDescription(
            long? requesterEmployeeId,
            long? recruiterEmployeeId,
            long? teamLeaderOfRecruiterEmployeeId,
            long? headOfRecruitmentEmployeeId,
            long? lineManagerEmployeeId,
            long? hiringManagerEmployeeId,
            IEnumerable<long> lineManagerAssignerEmployeeIds)
        {
            _employeeUserTypes = new Dictionary<long, OnboardingRequestUserType>();

            if (requesterEmployeeId.HasValue)
            {
                SetAccess(requesterEmployeeId.Value, OnboardingRequestUserType.Requester);
            }

            if (recruiterEmployeeId.HasValue)
            {
                SetAccess(recruiterEmployeeId.Value, OnboardingRequestUserType.Recruiter);
            }

            if (teamLeaderOfRecruiterEmployeeId.HasValue)
            {
                SetAccess(teamLeaderOfRecruiterEmployeeId.Value, OnboardingRequestUserType.TeamLeaderOfRecruiter);
            }

            if (headOfRecruitmentEmployeeId.HasValue)
            {
                SetAccess(headOfRecruitmentEmployeeId.Value, OnboardingRequestUserType.HeadOfRecruitment);
            }

            if (lineManagerEmployeeId.HasValue)
            {
                SetAccess(lineManagerEmployeeId.Value, OnboardingRequestUserType.LineManager);
            }

            if (hiringManagerEmployeeId.HasValue)
            {
                SetAccess(hiringManagerEmployeeId.Value, OnboardingRequestUserType.HiringManager);
            }

            foreach (var employeeId in lineManagerAssignerEmployeeIds)
            {
                SetAccess(employeeId, OnboardingRequestUserType.LineManagerAssigner);
            }
        }

        public long? GetEmployeeIdByUserType(OnboardingRequestUserType userType)
        {
            return _employeeUserTypes.Where(p => p.Value.HasFlag(userType)).Select(p => p.Key).SingleOrDefault();
        }

        public bool HasAccess(long employeeId)
        {
            return _employeeUserTypes.ContainsKey(employeeId);
        }

        public bool HasAccess(long employeeId, OnboardingRequestUserType requiredUserType)
        {
            OnboardingRequestUserType userType;

            return _employeeUserTypes.TryGetValue(employeeId, out userType)
                ? (userType & requiredUserType) != OnboardingRequestUserType.None
                : false;
        }

        private void SetAccess(long employeeId, OnboardingRequestUserType userType)
        {
            if (!_employeeUserTypes.ContainsKey(employeeId))
            {
                _employeeUserTypes.Add(employeeId, userType);

                return;
            }

            _employeeUserTypes[employeeId] |= userType;
        }
    }
}
