﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.PeopleManagement;

namespace Smt.Atomic.Business.PeopleManagement.Dto
{
    public class AssetTypeDtoToAssetTypeMapping: ClassMapping<AssetTypeDto, AssetType>
    {
        public AssetTypeDtoToAssetTypeMapping()
        {
            Mapping = d => new AssetType
            {
                Id = d.Id,
                NamePl = d.Name.Polish,
                NameEn = d.Name.English,
                Category = d.Category.Value,
                DetailsRequired = d.DetailsRequired,
                DetailsHintEn = d.DetailsHint.English,
                DetailsHintPl = d.DetailsHint.Polish,
                IsActive = d.IsActive,
                Description = d.Description,
                Timestamp = d.Timestamp
            };
        }
    }
}
