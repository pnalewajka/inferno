﻿namespace Smt.Atomic.Business.PeopleManagement.Dto
{
    public class UserAcronymDto
    {
        public long Id { get; set; }

        public string Acronym { get; set; }

        public byte[] Timestamp { get; set; }
    }
}