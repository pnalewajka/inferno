﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.PeopleManagement;

namespace Smt.Atomic.Business.PeopleManagement.Dto
{
    public class UserAcronymDtoToUserAcronymMapping : ClassMapping<UserAcronymDto, UserAcronym>
    {
        public UserAcronymDtoToUserAcronymMapping()
        {
            Mapping = d => new UserAcronym
            {
                Id = d.Id,
                Acronym = d.Acronym,
                Timestamp = d.Timestamp
            };
        }
    }
}
