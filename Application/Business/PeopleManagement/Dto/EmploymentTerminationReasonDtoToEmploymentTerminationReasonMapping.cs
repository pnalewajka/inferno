﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.PeopleManagement;

namespace Smt.Atomic.Business.PeopleManagement.Dto
{
    public class EmploymentTerminationReasonDtoToEmploymentTerminationReasonMapping : ClassMapping<EmploymentTerminationReasonDto, EmploymentTerminationReason>
    {
        public EmploymentTerminationReasonDtoToEmploymentTerminationReasonMapping()
        {
            Mapping = d => new EmploymentTerminationReason
            {
                Id = d.Id,
                NameEn = d.Name.English,
                NamePl = d.Name.Polish,
                DescriptionEn = d.Description.English,
                DescriptionPl = d.Description.Polish,
                IsVoluntarilyTurnover = d.IsVoluntarilyTurnover,
                IsOtherDepartureReasons = d.IsOtherDepartureReasons,
                Timestamp = d.Timestamp
            };
        }
    }
}