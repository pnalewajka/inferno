﻿using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.PeopleManagement.Dto
{
    public class EmploymentTerminationReasonDto
    {
        public long Id { get; set; }

        public LocalizedString Name { get; set; }

        public LocalizedString Description { get; set; }

        public bool IsVoluntarilyTurnover { get; set; }

        public bool IsOtherDepartureReasons { get; set; }

        public byte[] Timestamp { get; set; }
    }
}