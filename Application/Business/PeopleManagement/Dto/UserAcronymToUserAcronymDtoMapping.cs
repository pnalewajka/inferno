﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.PeopleManagement;

namespace Smt.Atomic.Business.PeopleManagement.Dto
{
    public class UserAcronymToUserAcronymDtoMapping : ClassMapping<UserAcronym, UserAcronymDto>
    {
        public UserAcronymToUserAcronymDtoMapping()
        {
            Mapping = e => new UserAcronymDto
            {
                Id = e.Id,
                Acronym = e.Acronym,
                Timestamp = e.Timestamp
            };
        }
    }
}
