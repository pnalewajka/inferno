﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.PeopleManagement.Dto
{
    public class FilteredAssetTypePickerContext
    {
        public AssetTypeCategory? Category { get; set; }

        public bool ShouldBeActive { get; set; }
    }
}
