﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Abstracts;

namespace Smt.Atomic.Business.PeopleManagement.Dto
{
    public class AssetTypeDto: SimpleEntity
    {
        public LocalizedString Name { get; set; }

        public AssetTypeCategory? Category { get; set; }

        public bool DetailsRequired { get; set; }

        public LocalizedString DetailsHint { get; set; }

        public bool IsActive { get; set; }

        public string Description { get; set; }
    }
}
