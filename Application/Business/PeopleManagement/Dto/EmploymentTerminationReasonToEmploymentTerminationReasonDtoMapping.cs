﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Modules.PeopleManagement;

namespace Smt.Atomic.Business.PeopleManagement.Dto
{
    public class EmploymentTerminationReasonToEmploymentTerminationReasonDtoMapping : ClassMapping<EmploymentTerminationReason, EmploymentTerminationReasonDto>
    {
        public EmploymentTerminationReasonToEmploymentTerminationReasonDtoMapping()
        {
            Mapping = e => new EmploymentTerminationReasonDto
            {
                Id = e.Id,
                Name = new LocalizedString
                {
                    English = e.NameEn,
                    Polish = e.NamePl
                },
                Description = new LocalizedString
                {
                    English = e.DescriptionEn,
                    Polish = e.DescriptionPl
                },
                IsVoluntarilyTurnover = e.IsVoluntarilyTurnover,
                IsOtherDepartureReasons = e.IsOtherDepartureReasons,
                Timestamp = e.Timestamp
            };
        }
    }
}
