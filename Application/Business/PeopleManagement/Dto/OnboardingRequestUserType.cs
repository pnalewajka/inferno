﻿using System;

namespace Smt.Atomic.Business.PeopleManagement.Dto
{
    [Flags]
    public enum OnboardingRequestUserType
    {
        None = 0,

        Requester = 1 << 0,

        Recruiter = 1 << 1,

        TeamLeaderOfRecruiter = 1 << 2,

        HeadOfRecruitment = 1 << 3,

        LineManager = 1 << 4,

        HiringManager = 1 << 5,

        LineManagerAssigner = 1 << 6,

        Recruiters = Requester | Recruiter | TeamLeaderOfRecruiter | HeadOfRecruitment,

        Managers = LineManager | HiringManager,

        RecruitersAndManagers = Recruiters | Managers
    }
}
