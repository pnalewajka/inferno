﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.PeopleManagement.Resources;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.Business.Workflows.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Jira.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.PeopleManagement.Workflows.Services
{
    internal class EmploymentDataChangeRequestService : RequestServiceBase<EmploymentDataChangeRequest, EmploymentDataChangeRequestDto>
    {
        private readonly IJiraService _jiraService;
        private readonly ITimeService _timeService;
        private readonly IOrgUnitService _orgUnitService;
        private readonly IEmployeeService _employeeService;
        private readonly IApprovingPersonService _approvingPersonService;
        private readonly IUnitOfWorkService<IAllocationDbScope> _unitOfWorkService;

        public EmploymentDataChangeRequestService(
            IJiraService jiraService,
            ITimeService timeService,
            IOrgUnitService orgUnitService,
            IEmployeeService employeeService,
            IApprovingPersonService approvingPersonService,
            IUnitOfWorkService<IAllocationDbScope> unitOfWorkService,
            IWorkflowNotificationService workflowNotificationService,
            IPrincipalProvider principalProvider,
            IClassMappingFactory classMappingFactory,
            IActionSetService actionSetService,
            IRequestWorkflowService requestWorkflowService,
            ISystemParameterService systemParameterService)
            : base(actionSetService, principalProvider, classMappingFactory, workflowNotificationService, requestWorkflowService, systemParameterService)
        {
            _jiraService = jiraService;
            _timeService = timeService;
            _orgUnitService = orgUnitService;
            _employeeService = employeeService;
            _approvingPersonService = approvingPersonService;
            _unitOfWorkService = unitOfWorkService;
        }

        public override RequestType RequestType => RequestType.EmploymentDataChangeRequest;

        public override string RequestName => WorkflowResources.EmploymentDataChangeRequestName;

        public override RequestTypeGroup RequestTypeGroup => RequestTypeGroup.Manager;


        public override SecurityRoleType? RequiredRoleType => SecurityRoleType.CanRequestEmploymentDataChange;

        public override bool CanBeDeleted(RequestDto requestDto, long byUserId)
        {
            if (requestDto.Status == RequestStatus.Draft || requestDto.Status == RequestStatus.Pending || requestDto.Status == RequestStatus.Approved)
            {
                return requestDto.RequestingUserId == byUserId || base.CanBeDeleted(requestDto, byUserId);
            }

            return base.CanBeDeleted(requestDto, byUserId);
        }

        public override EmploymentDataChangeRequestDto GetDraft(IDictionary<string, object> parameters)
        {
            return new EmploymentDataChangeRequestDto
            {
                EffectiveOn = _timeService.GetCurrentDate()
            };
        }

        public override IEnumerable<ApproverGroupDto> GetApproverGroups(RequestDto request, EmploymentDataChangeRequestDto employmentDataChangeRequest)
        {
            var requestingEmployeeId = _employeeService.GetEmployeeIdByUserId(request.RequestingUserId);
            var approverId = _approvingPersonService.GetStandardHRApprover(employmentDataChangeRequest.EmployeeId, requestingEmployeeId.Value);

            if (approverId.HasValue && approverId.Value != requestingEmployeeId)
            {
                yield return new ApproverGroupDto(ApprovalGroupMode.Or, new[] { approverId.Value });
            }
        }

        public override void Execute(RequestDto request, EmploymentDataChangeRequestDto employmentDataChangeRequest)
        {
            // TODO in v2 of this request
        }

        public override void OnAdding(RecordAddingEventArgs<Request, RequestDto, IWorkflowsDbScope> addingEventArgs)
        {
            var employmentDataChangeRequest = (EmploymentDataChangeRequestDto)addingEventArgs.InputDto.RequestObject;

            addingEventArgs.InputDto.DeferredUntil = employmentDataChangeRequest.EffectiveOn;
            addingEventArgs.InputEntity.DeferredUntil = employmentDataChangeRequest.EffectiveOn;

            base.OnAdding(addingEventArgs);
        }

        public override BoolResult OnDeleting(Request request, RequestDto requestDto, IUnitOfWork<IWorkflowsDbScope> unitOfWork)
        {
            var employmentDataChangeRequest = (EmploymentDataChangeRequestDto)requestDto.RequestObject;

            if (request.Status == RequestStatus.Approved)
            {
                using (var context = _jiraService.CreateReadWrite())
                {
                    var employee = _employeeService.GetEmployeeOrDefaultById(employmentDataChangeRequest.EmployeeId);
                    var comment = string.Format(EmploymentDataChangeRequestResources.RequestDeletedJiraComment, employee.LineManagerFullName);

                    foreach (var entry in request.HistoryEntries.Where(e => e.Type == RequestHistoryEntryType.JiraIssueCreated))
                    {
                        context.AddCommentToIssue(entry.JiraIssueKey, comment);
                    }
                }
            }

            return base.OnDeleting(request, requestDto, unitOfWork);
        }

        public override IEnumerable<AlertDto> Validate(RequestDto request, EmploymentDataChangeRequestDto employmentDataChangeRequest)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var managerId = PrincipalProvider.Current.EmployeeId;
                var managerOrgUnitIds = _orgUnitService.GetManagerOrgUnitDescendantIds(managerId);
                var employee = unitOfWork.Repositories.Employees.GetById(employmentDataChangeRequest.EmployeeId);

                if (!EmployeeBusinessLogic.HasGivenLineManager.Call(employee, managerId)
                    && !EmployeeBusinessLogic.BelongsToOneOfOrgUnits.Call(employee, managerOrgUnitIds))
                {
                    var message = string.Format(EmploymentDataChangeRequestResources.PermissionError, employee.FullName);

                    yield return new AlertDto { Message = message, Type = AlertType.Error };
                }
            }
        }

        public override string GetRequestDescription(RequestDto request, EmploymentDataChangeRequestDto data)
        {
            var employee = _employeeService.GetEmployeeOrDefaultById(data.EmployeeId);

            if (employee == null)
            {
                return null;
            }

            return string.Format(EmployeeDataChangeRequestResources.RequestDescriptionFormat, employee.FullName);
        }

        public override IQueryable<Request> ConfigureIncludes(IQueryable<Request> repository)
        {
            return repository.Include(r => r.EmploymentDataChangeRequest);
        }
    }
}
