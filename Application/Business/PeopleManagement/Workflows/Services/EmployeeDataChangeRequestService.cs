﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.ActiveDirectory.Dto;
using Smt.Atomic.Business.ActiveDirectory.Interfaces;
using Smt.Atomic.Business.Allocation.BusinessEvents;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.PeopleManagement.Resources;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.Business.Workflows.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Business.Helpers;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Jira.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.PeopleManagement.Workflows.Services
{
    internal class EmployeeDataChangeRequestService : RequestServiceBase<EmployeeDataChangeRequest, EmployeeDataChangeRequestDto>
    {
        private readonly IJiraService _jiraService;
        private readonly IOrgUnitService _orgUnitService;
        private readonly IEmployeeService _employeeService;
        private readonly ILocationService _locationService;
        private readonly IEntityMergingService _entityMergingService;
        private readonly IApprovingPersonService _approvingPersonService;
        private readonly IUnitOfWorkService<IAllocationDbScope> _unitOfWorkService;
        private readonly IActiveDirectoryWriter _activeDirectoryWriter;
        private readonly IBusinessEventPublisher _businessEventPublisher;

        public EmployeeDataChangeRequestService(
            IJiraService jiraService,
            IOrgUnitService orgUnitService,
            IEmployeeService employeeService,
            ILocationService locationService,
            IEntityMergingService entityMergingService,
            IApprovingPersonService approvingPersonService,
            IUnitOfWorkService<IAllocationDbScope> unitOfWorkService,
            IWorkflowNotificationService workflowNotificationService,
            IPrincipalProvider principalProvider,
            IActiveDirectoryWriter activeDirectoryWriter,
            IActionSetService actionSetService,
            IClassMappingFactory classMappingFactory,
            IRequestWorkflowService requestWorkflowService,
            IBusinessEventPublisher businessEventPublisher,
            ISystemParameterService systemParameterService)
            : base(actionSetService, principalProvider, classMappingFactory, workflowNotificationService, requestWorkflowService, systemParameterService)
        {
            _jiraService = jiraService;
            _orgUnitService = orgUnitService;
            _employeeService = employeeService;
            _locationService = locationService;
            _entityMergingService = entityMergingService;
            _approvingPersonService = approvingPersonService;
            _unitOfWorkService = unitOfWorkService;
            _activeDirectoryWriter = activeDirectoryWriter;
            _businessEventPublisher = businessEventPublisher;
        }

        public override RequestType RequestType => RequestType.EmployeeDataChangeRequest;

        public override string RequestName => WorkflowResources.EmployeeDataChangeRequestName;

        public override RequestTypeGroup RequestTypeGroup => RequestTypeGroup.Manager;

        public override SecurityRoleType? RequiredRoleType => SecurityRoleType.CanRequestEmployeeDataChange;

        public override bool CanBeDeleted(RequestDto requestDto, long byUserId)
        {
            if (requestDto.Status == RequestStatus.Draft || requestDto.Status == RequestStatus.Pending || requestDto.Status == RequestStatus.Approved)
            {
                return requestDto.RequestingUserId == byUserId || base.CanBeDeleted(requestDto, byUserId);
            }

            return base.CanBeDeleted(requestDto, byUserId);
        }

        public override IEnumerable<ApproverGroupDto> GetApproverGroups(RequestDto request, EmployeeDataChangeRequestDto employeeDataChangeRequest)
        {
            if ((employeeDataChangeRequest.LineManager?.IsSet ?? false) || (employeeDataChangeRequest.OrgUnit?.IsSet ?? false))
            {
                var requestingEmployeeId = _employeeService.GetEmployeeIdByUserId(request.RequestingUserId);
                var approverIds = employeeDataChangeRequest.EmployeeIds
                    .Select(e => _approvingPersonService.GetStandardHRApprover(e, requestingEmployeeId.Value))
                    .Where(id => id != null && id != requestingEmployeeId)
                    .Cast<long>().Distinct().ToArray();

                if (approverIds.Any())
                {
                    yield return new ApproverGroupDto(ApprovalGroupMode.And, approverIds);
                }
            }
        }

        public override void OnAdding(RecordAddingEventArgs<Request, RequestDto, IWorkflowsDbScope> addingEventArgs)
        {
            var employeeDataChangeRequest = (EmployeeDataChangeRequestDto)addingEventArgs.InputDto.RequestObject;

            addingEventArgs.InputDto.DeferredUntil = employeeDataChangeRequest.EffectiveOn;
            addingEventArgs.InputEntity.DeferredUntil = employeeDataChangeRequest.EffectiveOn;

            base.OnAdding(addingEventArgs);
        }

        public override BoolResult OnDeleting(Request request, RequestDto requestDto, IUnitOfWork<IWorkflowsDbScope> unitOfWork)
        {
            var employeeDataChangeRequest = (EmployeeDataChangeRequestDto)requestDto.RequestObject;

            if (request.Status == RequestStatus.Approved)
            {
                using (var context = _jiraService.CreateReadWrite())
                {
                    foreach (var entry in request.HistoryEntries.Where(e => e.Type == RequestHistoryEntryType.JiraIssueCreated))
                    {
                        context.AddCommentToIssue(entry.JiraIssueKey, EmployeeDataChangeRequestResources.RequestDeletedJiraComment);
                    }
                }
            }

            return base.OnDeleting(request, requestDto, unitOfWork);
        }

        public override void Execute(RequestDto request, EmployeeDataChangeRequestDto employeeDataChangeRequest)
        {
            foreach (var employeeId in employeeDataChangeRequest.EmployeeIds)
            {
                using (var unitOfWork = _unitOfWorkService.Create())
                {
                    var employee = unitOfWork.Repositories.Employees.Include(e => e.EmploymentPeriods).GetById(employeeId);

                    var activeDirectoryUserChangeDescription = new ActiveDirectoryUserChangeDescriptionDto
                    {
                        EmployeeId = employee.Id
                    };

                    if (employeeDataChangeRequest.JobProfile.IsSet && employeeDataChangeRequest.JobProfile.Value.HasValue)
                    {
                        var jobProfileIds = new[] { employeeDataChangeRequest.JobProfile.Value.Value };
                        var jobProfiles = unitOfWork.Repositories.JobProfiles.GetByIds(jobProfileIds).ToList();

                        _entityMergingService.MergeCollections(unitOfWork, employee.JobProfiles, jobProfiles);
                        activeDirectoryUserChangeDescription.JobProfileIds = jobProfileIds;
                    }

                    if (employeeDataChangeRequest.OrgUnit.IsSet && employeeDataChangeRequest.OrgUnit.Value.HasValue)
                    {
                        employee.OrgUnitId = employeeDataChangeRequest.OrgUnit.Value.Value;
                        activeDirectoryUserChangeDescription.OrgUnitId = employee.OrgUnitId;
                    }

                    if (employeeDataChangeRequest.Location.IsSet && employeeDataChangeRequest.Location.Value.HasValue)
                    {
                        var currentEmployeePeriod = employee.GetCurrentEmploymentPeriod();

                        if (currentEmployeePeriod != null)
                        {
                            var newEmploymentPeriod = new EmploymentPeriod();
                            var effectiveOn = employeeDataChangeRequest.EffectiveOn.Value;

                            EntityHelper.ShallowCopy(currentEmployeePeriod, newEmploymentPeriod);

                            newEmploymentPeriod.Id = BusinessLogicHelper.NonExistingId;
                            newEmploymentPeriod.LocationId = employeeDataChangeRequest.Location.Value;
                            newEmploymentPeriod.StartDate = effectiveOn;

                            unitOfWork.Repositories.EmploymentPeriods.Add(newEmploymentPeriod);

                            currentEmployeePeriod.EndDate = effectiveOn.AddDays(-1);

                            _businessEventPublisher.PublishBusinessEvent(new EmploymentPeriodChangedBusinessEvent
                            {
                                EmployeeId = employee.Id,
                                From = currentEmployeePeriod.StartDate,
                                To = newEmploymentPeriod.EndDate
                            });
                        }

                        activeDirectoryUserChangeDescription.LocationId = employee.LocationId.Value;
                    }

                    if (employeeDataChangeRequest.PlaceOfWork.IsSet && employeeDataChangeRequest.PlaceOfWork.Value.HasValue)
                    {
                        employee.PlaceOfWork = employeeDataChangeRequest.PlaceOfWork.Value;
                        activeDirectoryUserChangeDescription.PlaceOfWork = employee.PlaceOfWork.Value;
                    }

                    if (employeeDataChangeRequest.LineManager.IsSet)
                    {
                        employee.LineManagerId = employeeDataChangeRequest.LineManager.Value;
                        activeDirectoryUserChangeDescription.LineManagerId = employee.LineManagerId;
                    }

                    _activeDirectoryWriter.ModifyActiveDirectoryUser(activeDirectoryUserChangeDescription);

                    unitOfWork.Commit();
                }
            }
        }

        public override IEnumerable<AlertDto> Validate(RequestDto request, EmployeeDataChangeRequestDto employeeDataChangeRequest)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var managerId = PrincipalProvider.Current.EmployeeId;
                var managerOrgUnitIds = _orgUnitService.GetManagerOrgUnitDescendantIds(managerId);

                foreach (var employeeId in employeeDataChangeRequest.EmployeeIds)
                {
                    var employee = unitOfWork.Repositories.Employees.GetById(employeeId);

                    if (!EmployeeBusinessLogic.HasGivenLineManager.Call(employee, managerId)
                        && !EmployeeBusinessLogic.BelongsToOneOfOrgUnits.Call(employee, managerOrgUnitIds))
                    {
                        var message = string.Format(EmployeeDataChangeRequestResources.PermissionError, employee.FullName);

                        yield return new AlertDto { Message = message, Type = AlertType.Error };
                    }
                }
            }

            if (employeeDataChangeRequest.Location.IsSet && employeeDataChangeRequest.PlaceOfWork.IsSet)
            {
                if (!_locationService.CheckIfPlaceOfWorkIsAllowedForLocation(employeeDataChangeRequest.Location.Value.Value, employeeDataChangeRequest.PlaceOfWork.Value.Value))
                {
                    yield return new AlertDto { Message = EmployeeDataChangeRequestResources.WrongPlaceOfWorkError, Type = AlertType.Error };
                }
            }
        }

        public override string GetRequestDescription(RequestDto request, EmployeeDataChangeRequestDto data)
        {
            if (data.EmployeeIds.Any())
            {
                var employees = _employeeService.GetEmployeesById(data.EmployeeIds);
                var names = employees.Select(e => e.FullName).ToList();

                return string.Format(EmployeeDataChangeRequestResources.RequestDescriptionFormat, string.Join(", ", names));
            }

            return null;
        }

        public override IQueryable<Request> ConfigureIncludes(IQueryable<Request> repository)
        {
            return repository.Include(r => r.EmployeeDataChangeRequest);
        }
    }
}