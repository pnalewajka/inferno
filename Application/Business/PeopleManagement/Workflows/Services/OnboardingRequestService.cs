﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Castle.Core.Logging;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.ActiveDirectory.Dto;
using Smt.Atomic.Business.ActiveDirectory.Interfaces;
using Smt.Atomic.Business.Allocation.BusinessEvents;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Helpers;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.MeetMe.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.PeopleManagement.Dto;
using Smt.Atomic.Business.PeopleManagement.Interfaces;
using Smt.Atomic.Business.PeopleManagement.Resources;
using Smt.Atomic.Business.SkillManagement.Interfaces;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Exceptions;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.Business.Workflows.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Jira.Dto;
using Smt.Atomic.Data.Jira.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.PeopleManagement.Workflows.Services
{
    internal class OnboardingRequestService : RequestServiceBase<OnboardingRequest, OnboardingRequestDto>, IOnboardingRequestService
    {
        private readonly ILogger _logger;
        private readonly IActiveDirectoryWriter _activeDirectoryWriter;
        private readonly IBusinessEventPublisher _businessEventPublisher;
        private readonly IRequestServiceResolver _requestServiceResolver;
        private readonly IJiraService _jiraService;
        private readonly ITimeService _timeService;
        private readonly IUserService _userService;
        private readonly ICompanyService _companyService;
        private readonly IMeetingService _meetingService;
        private readonly IEmployeeService _employeeService;
        private readonly IJobTitleService _jobTitleService;
        private readonly ILocationService _locationService;
        private readonly IUserAcronymService _userAcronymService;
        private readonly IContractTypeService _contractTypeService;
        private readonly IOrgUnitService _orgUnitService;
        private readonly IUserPermissionService _userPermissionService;
        private readonly IRequestCardIndexDataService _requestCardIndexDataService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IEmploymentPeriodService _employmentPeriodService;
        private readonly IActiveDirectorySearchService _activeDirectorySearchService;
        private readonly IUnitOfWorkService<IWorkflowsDbScope> _unitOfWorkService;

        public OnboardingRequestService(
            IJiraService jiraService,
            ITimeService timeService,
            IUserService userService,
            ICompanyService companyService,
            IMeetingService meetingService,
            IEmployeeService employeeService,
            IJobTitleService jobTitleService,
            ILocationService locationService,
            IUserAcronymService userAcronymService,
            IContractTypeService contractTypeService,
            IUserPermissionService userPermissionService,
            IRequestCardIndexDataService requestCardIndexDataService,
            ISystemParameterService systemParameterService,
            IEmploymentPeriodService employmentPeriodService,
            IActiveDirectorySearchService activeDirectorySearchService,
            ILogger logger,
            IPrincipalProvider principalProvider,
            IActiveDirectoryWriter activeDirectoryWriter,
            IOrgUnitService orgUnitService,
            IWorkflowNotificationService workflowNotificationService,
            IRequestWorkflowService requestWorkflowService,
            IActionSetService actionSetService,
            IUnitOfWorkService<IWorkflowsDbScope> unitOfWorkService,
            IClassMappingFactory classMappingFactory,
            IBusinessEventPublisher businessEventPublisher,
            IRequestServiceResolver requestServiceResolver)
            : base(actionSetService, principalProvider, classMappingFactory, workflowNotificationService, requestWorkflowService, systemParameterService)
        {
            _logger = logger;
            _activeDirectoryWriter = activeDirectoryWriter;
            _businessEventPublisher = businessEventPublisher;
            _requestServiceResolver = requestServiceResolver;
            _jiraService = jiraService;
            _timeService = timeService;
            _userService = userService;
            _companyService = companyService;
            _meetingService = meetingService;
            _employeeService = employeeService;
            _jobTitleService = jobTitleService;
            _locationService = locationService;
            _userAcronymService = userAcronymService;
            _requestCardIndexDataService = requestCardIndexDataService;
            _contractTypeService = contractTypeService;
            _orgUnitService = orgUnitService;
            _userPermissionService = userPermissionService;
            _systemParameterService = systemParameterService;
            _employmentPeriodService = employmentPeriodService;
            _activeDirectorySearchService = activeDirectorySearchService;
            _unitOfWorkService = unitOfWorkService;
        }

        public override RequestType RequestType => RequestType.OnboardingRequest;

        public override string RequestName => WorkflowResources.OnboardingRequestName;

        public override RequestTypeGroup RequestTypeGroup => RequestTypeGroup.Manager;

        public override SecurityRoleType? RequiredRoleType => SecurityRoleType.CanRequestOnboarding;

        public override bool ShouldStartAsDraft => true;

        public override OnboardingRequestDto GetDraft(IDictionary<string, object> parameters)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var recruiterGroupName = _systemParameterService.GetParameter<string>(ParameterKeys.RecruitmentSpecialistAdGroup);
                var isCurrentUserRecruiter = unitOfWork.Repositories.Users
                    .Any(u => u.Id == PrincipalProvider.Current.Id && u.Profiles.Any(p => p.ActiveDirectoryGroupName == recruiterGroupName));

                return new OnboardingRequestDto
                {
                    RecruiterEmployeeId = isCurrentUserRecruiter ? PrincipalProvider.Current.EmployeeId : default(long),
                    SignedOn = _timeService.GetCurrentDate()
                };
            }
        }

        public override IEnumerable<ApproverGroupDto> GetApproverGroups(RequestDto request, OnboardingRequestDto onboardingRequest)
        {
            var orgUnitManagerEmployeeId = _orgUnitService.GetOrgUnitManagerId(onboardingRequest.OrgUnitId);

            if (orgUnitManagerEmployeeId.HasValue)
            {
                yield return new ApproverGroupDto(ApprovalGroupMode.Or, new[] { orgUnitManagerEmployeeId.Value });
            }
        }

        public override void OnAdding(RecordAddingEventArgs<Request, RequestDto, IWorkflowsDbScope> addingEventArgs)
        {
            var onboardingRequest = (OnboardingRequestDto)addingEventArgs.InputDto.RequestObject;

            if (onboardingRequest.LineManagerEmployeeId.HasValue)
            {
                ThrowInvalidPropertyModificationException(nameof(OnboardingRequestDto.LineManagerEmployeeId));
            }

            addingEventArgs.InputDto.DeferredUntil = onboardingRequest.StartDate;
            addingEventArgs.InputEntity.DeferredUntil = onboardingRequest.StartDate;

            base.OnAdding(addingEventArgs);
        }

        public override bool CanBeEdited(RequestDto requestDto, long byUserId)
        {
            if (requestDto.Status == RequestStatus.Draft || requestDto.Status == RequestStatus.Pending || requestDto.Status == RequestStatus.Approved)
            {
                var employeeId = _employeeService.GetEmployeeIdByUserId(byUserId);

                if (!employeeId.HasValue)
                {
                    return false;
                }

                var accessDescription = GetAccessDescription(requestDto);

                return accessDescription.HasAccess(employeeId.Value);
            }

            return false;
        }

        public override void OnEditing(RecordEditingEventArgs<Request, RequestDto, IWorkflowsDbScope> eventArgs)
        {
            var oldRequest = eventArgs.DbEntity;
            var newRequest = eventArgs.InputEntity;

            newRequest.OnboardingRequest.ResultingUserId = oldRequest.OnboardingRequest.ResultingUserId;
            newRequest.OnboardingRequest.WelcomeAnnounceEmailPhotosSecret = oldRequest.OnboardingRequest.WelcomeAnnounceEmailPhotosSecret;
            newRequest.OnboardingRequest.PayrollDocumentsSecret = oldRequest.OnboardingRequest.PayrollDocumentsSecret;
            newRequest.OnboardingRequest.ForeignEmployeeDocumentsSecret = oldRequest.OnboardingRequest.ForeignEmployeeDocumentsSecret;

            var entityToDtoMapping = ClassMappingFactory.CreateMapping<OnboardingRequest, OnboardingRequestDto>();

            var oldOnboardingRequestDto = entityToDtoMapping.CreateFromSource(oldRequest.OnboardingRequest);
            var newOnboardingRequestDto = (OnboardingRequestDto)eventArgs.InputDto.RequestObject;

            newOnboardingRequestDto.ResultingUserId = oldOnboardingRequestDto.ResultingUserId;

            var modifiedProperties = oldOnboardingRequestDto.GetType().GetProperties()
                .Where(p => !p.Name.EndsWith("Secret"))
                .Where(p => p.PropertyType != typeof(DocumentDto[])
                    ? !Equals(p.GetValue(oldOnboardingRequestDto), p.GetValue(newOnboardingRequestDto))
                    : !DocumentsEquals((DocumentDto[])p.GetValue(oldOnboardingRequestDto), (DocumentDto[])p.GetValue(newOnboardingRequestDto)))
                .Select(p => p.Name)
                .ToHashSet();

            if (modifiedProperties.Any())
            {
                var employeeId = PrincipalProvider.Current.EmployeeId;
                var accessDescription = GetAccessDescription(eventArgs.InputDto);

                if (oldRequest.Status == RequestStatus.Draft)
                {
                    if (modifiedProperties.Contains(nameof(OnboardingRequestDto.LineManagerEmployeeId)))
                    {
                        ThrowInvalidPropertyModificationException(nameof(OnboardingRequestDto.LineManagerEmployeeId));
                    }

                    if (!accessDescription.HasAccess(employeeId, OnboardingRequestUserType.Requester | OnboardingRequestUserType.Recruiter))
                    {
                        ThrowInvalidPropertyModificationException(modifiedProperties.First());
                    }
                }
                else if (oldRequest.Status == RequestStatus.Pending)
                {
                    if (modifiedProperties.Contains(nameof(OnboardingRequestDto.OrgUnitId)))
                    {
                        ThrowInvalidPropertyModificationException(nameof(OnboardingRequestDto.OrgUnitId));
                    }

                    CanBeModified(modifiedProperties, accessDescription, employeeId, OnboardingRequestUserType.HiringManager | OnboardingRequestUserType.LineManagerAssigner, nameof(OnboardingRequestDto.LineManagerEmployeeId));

                    if (modifiedProperties.Any())
                    {
                        CanBeModified(modifiedProperties, accessDescription, employeeId, OnboardingRequestUserType.Recruiters, modifiedProperties.First());
                    }
                }
                else if (oldRequest.Status == RequestStatus.Approved)
                {
                    var differingPropertiesCount = modifiedProperties.Count;

                    CanBeModified(modifiedProperties, accessDescription, employeeId, OnboardingRequestUserType.RecruitersAndManagers, nameof(OnboardingRequestDto.StartDate));
                    CanBeModified(modifiedProperties, accessDescription, employeeId, OnboardingRequestUserType.RecruitersAndManagers, nameof(OnboardingRequestDto.LocationId));
                    CanBeModified(modifiedProperties, accessDescription, employeeId, OnboardingRequestUserType.RecruitersAndManagers, nameof(OnboardingRequestDto.PlaceOfWork));

                    CanBeModified(modifiedProperties, accessDescription, employeeId, OnboardingRequestUserType.Recruiters, nameof(OnboardingRequestDto.CompanyId));
                    CanBeModified(modifiedProperties, accessDescription, employeeId, OnboardingRequestUserType.Recruiters, nameof(OnboardingRequestDto.JobTitleId));
                    CanBeModified(modifiedProperties, accessDescription, employeeId, OnboardingRequestUserType.Recruiters, nameof(OnboardingRequestDto.JobProfileId));

                    var isRequiringModification = modifiedProperties.Count != differingPropertiesCount;

                    CanBeModified(modifiedProperties, accessDescription, employeeId, OnboardingRequestUserType.RecruitersAndManagers, nameof(OnboardingRequestDto.OnboardingDate));
                    CanBeModified(modifiedProperties, accessDescription, employeeId, OnboardingRequestUserType.RecruitersAndManagers, nameof(OnboardingRequestDto.OnboardingLocationId));

                    CanBeModified(modifiedProperties, accessDescription, employeeId, OnboardingRequestUserType.Recruiters, nameof(OnboardingRequestDto.WelcomeAnnounceEmailInformation));
                    CanBeModified(modifiedProperties, accessDescription, employeeId, OnboardingRequestUserType.Recruiters, nameof(OnboardingRequestDto.WelcomeAnnounceEmailPhotos));
                    CanBeModified(modifiedProperties, accessDescription, employeeId, OnboardingRequestUserType.Recruiters, nameof(OnboardingRequestDto.PayrollDocuments));
                    CanBeModified(modifiedProperties, accessDescription, employeeId, OnboardingRequestUserType.Recruiters, nameof(OnboardingRequestDto.ForeignEmployeeDocuments));

                    if (modifiedProperties.Contains(nameof(OnboardingRequestDto.LineManagerEmployeeId))
                        && accessDescription.HasAccess(employeeId, OnboardingRequestUserType.HiringManager))
                    {
                        modifiedProperties.Remove(nameof(OnboardingRequestDto.LineManagerEmployeeId));
                    }

                    if (modifiedProperties.Any())
                    {
                        ThrowInvalidPropertyModificationException(modifiedProperties.First());
                    }

                    if (isRequiringModification)
                    {
                        UpdateJiraDueDate(eventArgs.DbEntity, oldOnboardingRequestDto, newOnboardingRequestDto);

                        var employee = eventArgs.UnitOfWork.Repositories.Employees.Single(e => e.UserId == newOnboardingRequestDto.ResultingUserId);

                        employee.LocationId = newOnboardingRequestDto.LocationId;
                        employee.PlaceOfWork = newOnboardingRequestDto.PlaceOfWork;
                        employee.CompanyId = newOnboardingRequestDto.CompanyId;
                        employee.JobTitleId = newOnboardingRequestDto.JobTitleId;

                        var jobMatrices = eventArgs.UnitOfWork.Repositories.JobTitles
                            .Where(t => t.Id == newOnboardingRequestDto.JobTitleId)
                            .Select(t => t.JobMatrixLevel)
                            .ToList();
                        var jobProfiles = eventArgs.UnitOfWork.Repositories.JobProfiles
                            .Where(p => p.Id == newOnboardingRequestDto.JobProfileId)
                            .ToList();

                        employee.JobMatrixs.Clear();
                        employee.JobProfiles.Clear();

                        foreach (var jobMatrix in jobMatrices)
                        {
                            employee.JobMatrixs.Add(jobMatrix);
                        }

                        foreach (var jobProfile in jobProfiles)
                        {
                            employee.JobProfiles.Add(jobProfile);
                        }

                        var employmentPeriod = employee.EmploymentPeriods.First();
                        var originalEmploymentPeriod = new EmploymentPeriod();

                        EntityHelper.ShallowCopy(employmentPeriod, originalEmploymentPeriod, new[]
                        {
                            nameof(EmploymentPeriod.EmployeeId),
                            nameof(EmploymentPeriod.StartDate),
                            nameof(EmploymentPeriod.SignedOn),
                            nameof(EmploymentPeriod.NoticePeriod)
                        }.ToHashSet());

                        employmentPeriod.StartDate = newOnboardingRequestDto.StartDate;
                        employmentPeriod.CompanyId = newOnboardingRequestDto.CompanyId;
                        employmentPeriod.JobTitleId = newOnboardingRequestDto.JobTitleId;

                        var firstMeeting = employee.Meetings != null && employee.Meetings.Count == 1 ? employee.Meetings.First() : null;

                        if (firstMeeting?.Status == MeetingStatus.Pending)
                        {
                            var interval = _systemParameterService.GetParameter<int>(ParameterKeys.FirstMeetMeWeeksInterval);

                            firstMeeting.MeetingDueDate = DateHelper.AddWeeks(employmentPeriod.StartDate, interval);
                        }

                        _businessEventPublisher.PublishBusinessEvent(
                            AllocationHelper.CreateEventFromEmploymentPeriods(employmentPeriod, originalEmploymentPeriod));

                        _businessEventPublisher.PublishBusinessEvent(new EmploymentPeriodChangedBusinessEvent
                        {
                            EmployeeId = employmentPeriod.EmployeeId,
                            From = employmentPeriod.StartDate,
                            To = employmentPeriod.EndDate
                        });

                        var activeDirectoryUserChangeDescription = new ActiveDirectoryUserChangeDescriptionDto
                        {
                            EmployeeId = employee.Id,
                            StartDate = newOnboardingRequestDto.StartDate,
                            LocationId = newOnboardingRequestDto.LocationId,
                            PlaceOfWork = newOnboardingRequestDto.PlaceOfWork,
                            CompanyId = newOnboardingRequestDto.CompanyId,
                            JobTitleId = newOnboardingRequestDto.JobTitleId,
                            JobProfileIds = new[] { newOnboardingRequestDto.JobProfileId }
                        };

                        _activeDirectoryWriter.ModifyActiveDirectoryUser(activeDirectoryUserChangeDescription);
                    }

                    var assetRequestService = (IAssetsRequestService)_requestServiceResolver.GetByServiceType(RequestType.AssetsRequest);
                    var alertDtoList = assetRequestService.OnEditingOnboarding(oldOnboardingRequestDto, newOnboardingRequestDto, ViewModelTypeName);

                    if (alertDtoList != null)
                    {
                        eventArgs.Alerts.AddRange(alertDtoList);
                    }
                }
            }

            base.OnEditing(eventArgs);

            eventArgs.InputDto.Status = oldRequest.Status;
            newRequest.Status = oldRequest.Status;
            newRequest.DeferredUntil = newOnboardingRequestDto.StartDate;
        }

        public override bool CanBeDeleted(RequestDto requestDto, long byUserId)
        {
            if (requestDto.Status == RequestStatus.Draft)
            {
                return true;
            }

            if (requestDto.Status == RequestStatus.Pending || requestDto.Status == RequestStatus.Approved)
            {
                var onboardingRequest = (OnboardingRequestDto)requestDto.RequestObject;

                if (_timeService.GetCurrentDate() >= onboardingRequest.StartDate)
                {
                    return false;
                }

                var employeeId = _employeeService.GetEmployeeIdByUserId(byUserId);

                if (!employeeId.HasValue)
                {
                    return false;
                }

                var accessDescription = GetAccessDescription(requestDto);

                return accessDescription.HasAccess(employeeId.Value, OnboardingRequestUserType.Recruiters);
            }

            return false;
        }

        public override BoolResult OnDeleting(Request request, RequestDto requestDto, IUnitOfWork<IWorkflowsDbScope> unitOfWork)
        {
            var onboardingRequestDto = (OnboardingRequestDto)requestDto.RequestObject;

            unitOfWork.Repositories.Requests
                .Where(r => r.AssetsRequest.OnboardingRequestId == onboardingRequestDto.Id)
                .Select(r => r.Id)
                .ToList()
                .ForEach(id => RequestWorkflowService.Delete(id));

            unitOfWork.Repositories.EmployeePhotoDocuments.DeleteEach(d => d.OnboardingRequestId == onboardingRequestDto.Id);
            unitOfWork.Repositories.PayrollDocuments.DeleteEach(d => d.OnboardingRequestId == onboardingRequestDto.Id);
            unitOfWork.Repositories.ForeignEmployeeDocuments.DeleteEach(d => d.OnboardingRequestId == onboardingRequestDto.Id);

            var relatedRecruitmentProcess = unitOfWork.Repositories.RecruitmentProcesses.SingleOrDefault(d => d.OnboardingRequestId == onboardingRequestDto.Id);

            if (relatedRecruitmentProcess != null)
            {
                relatedRecruitmentProcess.OnboardingRequestId = null;
            }

            _logger.Warn($"Deleting onboarding request with status {request.Status}");

            if (request.Status == RequestStatus.Approved)
            {
                var employmentPeriods = unitOfWork.Repositories.EmploymentPeriods
                    .Where(p => p.Employee.UserId.HasValue && p.Employee.UserId == onboardingRequestDto.ResultingUserId)
                    .ToList();

                if (!employmentPeriods.Any())
                {
                    _logger.Warn($"Could not find any employment period for user with ID={onboardingRequestDto.ResultingUserId}");
                }

                foreach (var employmentPeriod in employmentPeriods)
                {
                    unitOfWork.Repositories.EmploymentPeriods.Delete(employmentPeriod);

                    _businessEventPublisher.PublishBusinessEvent(AllocationHelper.CreateEventFromEmploymentPeriod(employmentPeriod));

                    _businessEventPublisher.PublishBusinessEvent(new EmploymentPeriodChangedBusinessEvent
                    {
                        EmployeeId = employmentPeriod.EmployeeId,
                        From = employmentPeriod.StartDate,
                        To = employmentPeriod.EndDate
                    });
                }

                var recruiter = _employeeService.GetEmployeeOrDefaultById(onboardingRequestDto.RecruiterEmployeeId);
                var comment = string.Format(OnboardingRequestResources.RequestDeletedJiraComment, recruiter.FullName, recruiter.Login);

                ActionSetService.AddCommentToJiraIssues(request, comment);
                ActionSetService.SendNotificationToEmailRecipients(request, requestDto,
                    TemplateCodes.OnboardingRequestDeletedSubject,
                    TemplateCodes.OnboardingRequestDeletedBody);
            }

            return base.OnDeleting(request, requestDto, unitOfWork);
        }

        public override void OnAllApproved(Request request, RequestDto requestDto)
        {
            var onboardingRequestDto = (OnboardingRequestDto)requestDto.RequestObject;

            if (!onboardingRequestDto.LineManagerEmployeeId.HasValue)
            {
                throw new RequestMissingDataForStatusChangeBusinessException(OnboardingRequestResources.ApprovingWithoutLineManagerError);
            }

            base.OnAllApproved(request, requestDto);

            var email = GenerateEmail(onboardingRequestDto);
            var acronym = _userAcronymService.GenerateAcronym(onboardingRequestDto.FirstName, onboardingRequestDto.LastName);

            var activeDirectoryId = CreateActiveDirectoryUser(onboardingRequestDto, email, acronym);
            var userId = CreateUser(onboardingRequestDto, email, activeDirectoryId);
            var employeeId = CreateEmployee(onboardingRequestDto, userId, email, acronym);

            CreateEmploymentPeriod(onboardingRequestDto, employeeId);
            CreateAssetsRequest(onboardingRequestDto, employeeId);

            request.OnboardingRequest.ResultingUserId = userId;
            _meetingService.ScheduleFirstMeeting(employeeId, onboardingRequestDto.StartDate);
            _userAcronymService.AddAcronym(new UserAcronymDto { Acronym = acronym });
        }

        public override void Execute(RequestDto request, OnboardingRequestDto onboardingRequest)
        {
        }

        public override IEnumerable<AlertDto> Validate(RequestDto request, OnboardingRequestDto requestDto)
        {
            if (requestDto.IsCreatedAutomatically)
            {
                yield break;
            }

            if (!requestDto.PlaceOfWork.HasValue || !_locationService.CheckIfPlaceOfWorkIsAllowedForLocation(requestDto.LocationId, requestDto.PlaceOfWork.Value))
            {
                yield return new AlertDto { Message = OnboardingRequestResources.WrongPlaceOfWorkError, Type = AlertType.Error };
            }

            if (!requestDto.OnboardingDate.HasValue)
            {
                yield return AlertDto.CreateError(OnboardingRequestResources.OnboardingDateRequiredError);
            }

            if (!requestDto.OnboardingLocationId.HasValue)
            {
                yield return AlertDto.CreateError(OnboardingRequestResources.OnboardingLocationRequiredError);
            }

            if (!requestDto.SignedOn.HasValue)
            {
                yield return new AlertDto { Message = OnboardingRequestResources.SignedOnRequiredError, Type = AlertType.Error };
            }

            if (requestDto.IsComingFromReferralProgram && string.IsNullOrEmpty(requestDto.ReferralProgramDetails))
            {
                yield return new AlertDto { Message = OnboardingRequestResources.ReferralProgramDetailsRequiredError, Type = AlertType.Error };
            }

            if (requestDto.IsRelocationPackageNeeded && string.IsNullOrEmpty(requestDto.RelocationPackageDetails))
            {
                yield return new AlertDto { Message = OnboardingRequestResources.RelocationPackageDetailsRequiredError, Type = AlertType.Error };
            }

            var defaultOrgUnitCode = _systemParameterService.GetParameter<string>(ParameterKeys.OrganizationDefaultOrgUnitCode);
            var defaultOrgUnit = _orgUnitService.GetOrgUnitByCode(defaultOrgUnitCode);

            if (requestDto.OrgUnitId == defaultOrgUnit.Id)
            {
                yield return new AlertDto { Message = OnboardingRequestResources.DefaultOrgUnitError, Type = AlertType.Error };
            }

            if (request.Status == RequestStatus.Draft || request.Status == RequestStatus.Pending)
            {
                using (var unitOfWork = _unitOfWorkService.Create())
                {
                    if (unitOfWork.Repositories.Users.Any(u => u.Login == requestDto.Login) || _activeDirectorySearchService.CheckIfUsed(requestDto.Login))
                    {
                        yield return new AlertDto { Message = OnboardingRequestResources.LoginExistsError, Type = AlertType.Error };
                    }

                    var isOtherRequestActive = unitOfWork.Repositories.OnboardingRequests
                        .Any(r => r.Id != request.Id && r.FirstName == requestDto.FirstName && r.LastName == requestDto.LastName
                            && r.Request.Status != RequestStatus.Rejected && r.Request.Status != RequestStatus.Completed);

                    if (request.Status == RequestStatus.Draft && isOtherRequestActive)
                    {
                        yield return new AlertDto
                        {
                            Message = string.Format(OnboardingRequestResources.DuplicateWarning, requestDto.FirstName, requestDto.LastName),
                            Type = AlertType.Warning
                        };
                    }
                }
            }
            else if (request.Status == RequestStatus.Approved)
            {
                if (!requestDto.LineManagerEmployeeId.HasValue)
                {
                    yield return new AlertDto { Message = OnboardingRequestResources.LineManagerCannotBeEmptyError, Type = AlertType.Error };
                }
            }
        }

        public override string GetRequestDescription(RequestDto request, OnboardingRequestDto onboardingRequest)
        {
            return string.Format(OnboardingRequestResources.RequestDescriptionFormat, onboardingRequest.FirstName, onboardingRequest.LastName);
        }

        public override IQueryable<Request> ConfigureIncludes(IQueryable<Request> repository)
        {
            return repository.Include(r => r.OnboardingRequest);
        }

        public OnboardingRequestAccessDescription GetAccessDescription(RequestDto request)
        {
            var onboardingRequest = (OnboardingRequestDto)request.RequestObject;

            var requesterEmployeeId = _employeeService.GetEmployeeIdByUserId(request.RequestingUserId);

            var recruiter = _employeeService.GetEmployeeOrDefaultById(onboardingRequest.RecruiterEmployeeId);
            var recruiterOrgUnitManagerEmployeeId = _orgUnitService.GetOrgUnitManagerId(recruiter?.OrgUnitId ?? default(long));

            var recruitmentOrgUnitCode = _systemParameterService.GetParameter<string>(ParameterKeys.RecruitmentOrgUnitCode);
            var recruitmentOrgUnit = _orgUnitService.GetOrgUnitByCode(recruitmentOrgUnitCode);

            var orgUnitManagerEmployeeId = _orgUnitService.GetOrgUnitManagerId(onboardingRequest.OrgUnitId);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var lineManagerAssignerEmployeeIds = unitOfWork.Repositories.Employees
                    .Where(e =>
                        e.User.Roles.Any(r => r.Id == (long)SecurityRoleType.CanAssignLineManagerInOnboardingRequest) ||
                        e.User.Profiles.Any(p => p.Roles.Any(r => r.Id == (long)SecurityRoleType.CanAssignLineManagerInOnboardingRequest)))
                    .Select(e => e.Id)
                    .ToList();

                return new OnboardingRequestAccessDescription(
                    requesterEmployeeId: requesterEmployeeId,
                    recruiterEmployeeId: onboardingRequest.RecruiterEmployeeId,
                    teamLeaderOfRecruiterEmployeeId: recruiterOrgUnitManagerEmployeeId,
                    headOfRecruitmentEmployeeId: recruitmentOrgUnit.EmployeeId,
                    lineManagerEmployeeId: onboardingRequest.LineManagerEmployeeId,
                    hiringManagerEmployeeId: orgUnitManagerEmployeeId,
                    lineManagerAssignerEmployeeIds: lineManagerAssignerEmployeeIds);
            }
        }

        public DocumentDto[] GetForeignEmployeeDocuments(long requestId, Guid secret)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.OnboardingRequests
                    .Where(r => r.Id == requestId && r.ForeignEmployeeDocumentsSecret == secret)
                    .SelectMany(r => r.ForeignEmployeeDocuments.Select(d => new DocumentDto
                    {
                        DocumentId = d.Id,
                        ContentType = d.ContentType,
                        DocumentName = d.Name
                    }))
                    .ToArray();
            }
        }

        public DocumentDto[] GetPayrollDocuments(long requestId, Guid secret)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.OnboardingRequests
                    .Where(r => r.Id == requestId && r.PayrollDocumentsSecret == secret)
                    .SelectMany(r => r.PayrollDocuments.Select(d => new DocumentDto
                    {
                        DocumentId = d.Id,
                        ContentType = d.ContentType,
                        DocumentName = d.Name
                    }))
                    .ToArray();
            }
        }

        public DocumentDto[] GetWelcomeAnnounceEmailPhotos(long requestId, Guid secret)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.OnboardingRequests
                    .Where(r => r.Id == requestId && r.WelcomeAnnounceEmailPhotosSecret == secret)
                    .SelectMany(r => r.WelcomeAnnounceEmailPhotos.Select(p => new DocumentDto
                    {
                        DocumentId = p.Id,
                        ContentType = p.ContentType,
                        DocumentName = p.Name
                    }))
                    .ToArray();
            }
        }

        private void UpdateJiraDueDate(Request request, OnboardingRequestDto oldOnboardingRequestDto, OnboardingRequestDto newOnboardingRequestDto)
        {
            if (oldOnboardingRequestDto.OnboardingDate != newOnboardingRequestDto.OnboardingDate)
            {
                using (var context = _jiraService.CreateReadWrite())
                {
                    foreach (var entry in request.HistoryEntries.Where(e => e.Type == RequestHistoryEntryType.JiraIssueCreated))
                    {
                        context.UpdateIssue(entry.JiraIssueKey, new UpdateIssueDto { DueDate = newOnboardingRequestDto.OnboardingDate });
                    }
                }
            }
        }

        private string GenerateEmail(OnboardingRequestDto request)
        {
            var company = _companyService.GetCompanyOrDefaultById(request.CompanyId);

            if (string.IsNullOrEmpty(company?.EmailDomain))
            {
                var defaultDomain = _systemParameterService.GetParameter<string>(ParameterKeys.UserDefaultEmailDomain);

                return $"{request.Login}@{defaultDomain}";
            }

            return $"{request.Login}@{company.EmailDomain}";
        }

        private Guid? CreateActiveDirectoryUser(OnboardingRequestDto request, string email, string acronym)
        {
            var activeDirectoryUserCreationDto = new ActiveDirectoryUserCreationDto
            {
                Email = email,
                Acronym = acronym,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Login = request.Login,
                JobTitleId = request.JobTitleId,
                JobProfileId = request.JobProfileId,
                OrgUnitId = request.OrgUnitId,
                LocationId = request.LocationId,
                PlaceOfWork = request.PlaceOfWork.Value,
                CompanyId = request.CompanyId,
                LineManagerId = request.LineManagerEmployeeId.Value,
                StartDate = request.StartDate,
                EffectiveDate = request.SignedOn,
                ContractTypeId = request.ProbationPeriodContractTypeId
            };

            return _activeDirectoryWriter.CreateActiveDirectoryUser(activeDirectoryUserCreationDto);
        }

        private long CreateUser(OnboardingRequestDto request, string email, Guid? activeDirectoryId)
        {
            var userId = _userService.CreateUser(new UserDto
            {
                Email = email,
                Login = request.Login,
                FirstName = request.FirstName,
                LastName = request.LastName,
                IsActive = true,
                ActiveDirectoryId = activeDirectoryId
            });

            var jobTitle = _jobTitleService.GetJobTitleOrDefaultById(request.JobTitleId);

            if (jobTitle != null)
            {
                _userPermissionService.AssignSecurityProfilesToUser(userId, jobTitle.DefaultProfileIds);
            }

            return userId;
        }

        private long CreateEmployee(OnboardingRequestDto request, long userId, string email, string acronym)
        {
            var orgUnit = _orgUnitService.GetOrgUnitOrDefaultById(request.OrgUnitId);
            var jobTitle = _jobTitleService.GetJobTitleOrDefaultById(request.JobTitleId);
            var contractType = _contractTypeService.GetContractTypeOrDefaultById(request.ProbationPeriodContractTypeId);

            var employee = new EmployeeDto
            {
                UserId = userId,
                Email = email,
                Acronym = acronym,
                FirstName = request.FirstName,
                LastName = request.LastName,
                JobTitleId = request.JobTitleId,
                MinimumOvertimeBankBalance = contractType.MinimunOvertimeBankBalance,
                LocationId = request.LocationId,
                PlaceOfWork = request.PlaceOfWork,
                CurrentEmployeeStatus = EmployeeStatus.NewHire,
                IsProjectContributor = _orgUnitService.GetDefaultProjectContributorForOrgUnit(request.OrgUnitId),
                JobProfileIds = new[] { request.JobProfileId },
                JobMatrixIds = jobTitle?.JobMatrixLevelId != null
                    ? new[] { jobTitle.JobMatrixLevelId.Value }
                    : null,
                CompanyId = request.CompanyId,
                LineManagerId = request.LineManagerEmployeeId,
                OrgUnitId = request.OrgUnitId,
                EmployeeFeatures = (orgUnit?.OrgUnitFeatures ?? default(OrgUnitFeatures)).HasFlag(OrgUnitFeatures.ShowInUtilizationReport)
                    ? EmployeeFeatures.ShowInUtilizationReport
                    : default(EmployeeFeatures)
            };

            return _employeeService.CreateEmployee(employee);
        }

        private void CreateEmploymentPeriod(OnboardingRequestDto request, long employeeId)
        {
            const decimal defaultHours = 8M;

            var employmentPeriod = new EmploymentPeriodDto
            {
                IsActive = true,
                EmployeeId = employeeId,
                StartDate = request.StartDate,
                SignedOn = request.SignedOn.Value,
                CompanyId = request.CompanyId,
                JobTitleId = request.JobTitleId,
                ContractTypeId = request.ProbationPeriodContractTypeId,
                NoticePeriod = NoticePeriod.OneMonth,
                TimeReportingMode = TimeReportingMode.Detailed,
                MondayHours = defaultHours,
                TuesdayHours = defaultHours,
                WednesdayHours = defaultHours,
                ThursdayHours = defaultHours,
                FridayHours = defaultHours,
                VacationHourToDayRatio = defaultHours,
                LocationId = request.LocationId
            };

            _employmentPeriodService.CreateEmploymentPeriod(employmentPeriod);
        }

        private void CreateAssetsRequest(OnboardingRequestDto request, long employeeId)
        {
            var lineManagerEmployee = _employeeService.GetEmployeeOrDefaultById(request.LineManagerEmployeeId.Value);

            var assetsRequestDto = new RequestDto
            {
                RequestingUserId = lineManagerEmployee.UserId.Value,
                RequestObject = new AssetsRequestDto
                {
                    EmployeeId = employeeId,
                    OnboardingRequestId = request.Id,
                    IsCreatedAutomatically = true
                },
                RequestType = RequestType.AssetsRequest
            };

            _requestCardIndexDataService.AddRecord(assetsRequestDto);
        }

        private void ThrowInvalidPropertyModificationException(string propertyName)
        {
            var viewModelType = TypeHelper.FindTypeInLoadedAssemblies(ViewModelTypeName);
            var viewModelMapping = ClassMappingFactory.CreateMapping(typeof(OnboardingRequestDto), viewModelType);
            var fieldMappings = viewModelMapping.GetFieldMappings(typeof(OnboardingRequestDto), viewModelType);

            var field = fieldMappings[propertyName].FirstOrDefault();

            if (field == null)
            {
                throw new InvalidOperationException($"Could not find field in view model for {propertyName} field.");
            }

            var propertyInfo = viewModelType.GetProperty(field);
            var label = PropertyHelper.GetPropertyFriendlyName(propertyInfo);

            throw new BusinessException(string.Format(OnboardingRequestResources.BlockedFieldError, label));
        }

        private void CanBeModified(ISet<string> modifiedProperties, OnboardingRequestAccessDescription accessDescription,
            long employeeId, OnboardingRequestUserType userTypes, string propertyName)
        {
            if (modifiedProperties.Contains(propertyName))
            {
                modifiedProperties.Remove(propertyName);

                if (!accessDescription.HasAccess(employeeId, userTypes))
                {
                    ThrowInvalidPropertyModificationException(propertyName);
                }
            }
        }

        private static bool DocumentsEquals(DocumentDto[] firstCollection, DocumentDto[] secondCollection)
        {
            var temporaryDocuments = firstCollection.Where(d => d.IsTemporary).Select(d => d.TemporaryDocumentId).ToHashSet();

            temporaryDocuments.SymmetricExceptWith(secondCollection.Where(d => d.IsTemporary).Select(d => d.TemporaryDocumentId));

            if (temporaryDocuments.Any())
            {
                return false;
            }

            var promotedDocuments = firstCollection.Where(d => !d.IsTemporary).Select(d => d.DocumentId).ToHashSet();

            promotedDocuments.SymmetricExceptWith(secondCollection.Where(d => !d.IsTemporary).Select(d => d.DocumentId));

            if (promotedDocuments.Any())
            {
                return false;
            }

            return true;
        }
    }
}
