﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.ActiveDirectory.Dto;
using Smt.Atomic.Business.ActiveDirectory.Interfaces;
using Smt.Atomic.Business.Allocation.BusinessEvents;
using Smt.Atomic.Business.Allocation.Helpers;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.PeopleManagement.Resources;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Exceptions;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.Business.Workflows.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Jira.Dto;
using Smt.Atomic.Data.Jira.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.PeopleManagement.Workflows.Services
{
    internal class EmploymentTerminationRequestService : RequestServiceBase<EmploymentTerminationRequest, EmploymentTerminationRequestDto>
    {
        private readonly IJiraService _jiraService;
        private readonly ITimeService _timeService;
        private readonly IUserService _userService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IEmployeeService _employeeService;
        private readonly IActiveDirectoryWriter _activeDirectoryWriter;
        private readonly IBusinessEventPublisher _businessEventPublisher;
        private readonly IOrgUnitService _orgUnitService;
        private readonly IDefaultParameterContextBuilder _defaultParameterContextBuilder;
        private readonly IUnitOfWorkService<IWorkflowsDbScope> _unitOfWorkService;

        public EmploymentTerminationRequestService(
            IJiraService jiraService,
            ITimeService timeService,
            IUserService userService,
            ISystemParameterService systemParameterService,
            IEmployeeService employeeService,
            IOrgUnitService orgUnitService,
            IActiveDirectoryWriter activeDirectoryWriter,
            IBusinessEventPublisher businessEventPublisher,
            IPrincipalProvider principalProvider,
            IDefaultParameterContextBuilder defaultParameterContextBuilder,
            IUnitOfWorkService<IWorkflowsDbScope> unitOfWorkService,
            IWorkflowNotificationService workflowNotificationService,
            IActionSetService actionSetService,
            IClassMappingFactory classMappingFactory,
            IRequestWorkflowService requestWorkflowService)
            : base(actionSetService, principalProvider, classMappingFactory, workflowNotificationService, requestWorkflowService, systemParameterService)
        {
            _jiraService = jiraService;
            _timeService = timeService;
            _userService = userService;
            _systemParameterService = systemParameterService;
            _employeeService = employeeService;
            _orgUnitService = orgUnitService;
            _activeDirectoryWriter = activeDirectoryWriter;
            _businessEventPublisher = businessEventPublisher;
            _defaultParameterContextBuilder = defaultParameterContextBuilder;
            _unitOfWorkService = unitOfWorkService;
        }

        public override RequestType RequestType => RequestType.EmploymentTerminationRequest;

        public override string RequestName => WorkflowResources.EmploymentTerminationRequestName;

        public override RequestTypeGroup RequestTypeGroup => RequestTypeGroup.Manager;

        public override SecurityRoleType? RequiredRoleType => SecurityRoleType.CanRequestEmploymentTermination;

        public override EmploymentTerminationRequestDto GetDraft(IDictionary<string, object> parameters)
        {
            var currentDate = _timeService.GetCurrentDate();

            return new EmploymentTerminationRequestDto
            {
                TerminationDate = currentDate,
                EndDate = currentDate
            };
        }

        public override IEnumerable<ApproverGroupDto> GetApproverGroups(RequestDto request, EmploymentTerminationRequestDto employmentTerminationRequest)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var context = _defaultParameterContextBuilder.BuildForEmployee(employmentTerminationRequest.EmployeeId);
                var approverAcronyms = _systemParameterService.GetParameter<string[]>(ParameterKeys.EmploymentTerminationHRApproverAcronyms, context);
                var approverIds = unitOfWork.Repositories.Employees
                    .Where(e => approverAcronyms.Contains(e.Acronym))
                    .Select(e => e.Id)
                    .ToList();

                yield return new ApproverGroupDto(ApprovalGroupMode.Or, approverIds);
            }
        }

        public override void OnAdding(RecordAddingEventArgs<Request, RequestDto, IWorkflowsDbScope> addingEventArgs)
        {
            var employmentTerminationRequest = (EmploymentTerminationRequestDto)addingEventArgs.InputDto.RequestObject;

            addingEventArgs.InputDto.DeferredUntil = employmentTerminationRequest.EndDate;
            addingEventArgs.InputEntity.DeferredUntil = employmentTerminationRequest.EndDate;

            base.OnAdding(addingEventArgs);
        }

        public override bool CanBeEdited(RequestDto requestDto, long byUserId)
        {
            return CanBeModified(requestDto, byUserId) || base.CanBeEdited(requestDto, byUserId);
        }

        public override void OnEditing(RecordEditingEventArgs<Request, RequestDto, IWorkflowsDbScope> eventArgs)
        {
            var oldEmploymentTerminationRequest = eventArgs.DbEntity.EmploymentTerminationRequest;
            var newEmploymentTerminationRequest = eventArgs.InputEntity.EmploymentTerminationRequest;

            if (oldEmploymentTerminationRequest.EmployeeId != newEmploymentTerminationRequest.EmployeeId
                || oldEmploymentTerminationRequest.TerminationDate != newEmploymentTerminationRequest.TerminationDate)
            {
                throw new BusinessException(EmploymentTerminationRequestResources.UneditableFieldError);
            }

            if (eventArgs.DbEntity.Status == RequestStatus.Approved && oldEmploymentTerminationRequest.EndDate != newEmploymentTerminationRequest.EndDate)
            {
                using (var context = _jiraService.CreateReadWrite())
                {
                    foreach (var entry in eventArgs.DbEntity.HistoryEntries.Where(e => e.Type == RequestHistoryEntryType.JiraIssueCreated))
                    {
                        context.UpdateIssue(entry.JiraIssueKey, new UpdateIssueDto { DueDate = newEmploymentTerminationRequest.EndDate });
                    }
                }
            }

            eventArgs.InputEntity.Status = eventArgs.DbEntity.Status;
            eventArgs.InputEntity.DeferredUntil = newEmploymentTerminationRequest.EndDate;

            newEmploymentTerminationRequest.BackupData = oldEmploymentTerminationRequest.BackupData;

            if (eventArgs.InputEntity.Status == RequestStatus.Approved)
            {
                SubmitChanges((EmploymentTerminationRequestDto)eventArgs.InputDto.RequestObject);
            }

            base.OnEditing(eventArgs);
        }

        public override bool CanBeDeleted(RequestDto requestDto, long byUserId)
        {
            return CanBeModified(requestDto, byUserId) || base.CanBeDeleted(requestDto, byUserId);
        }

        public override BoolResult OnDeleting(Request request, RequestDto requestDto, IUnitOfWork<IWorkflowsDbScope> unitOfWork)
        {
            var employmentTerminationRequest = (EmploymentTerminationRequestDto)requestDto.RequestObject;

            if (request.Status == RequestStatus.Approved)
            {
                var employee = _employeeService.GetEmployeeOrDefaultById(employmentTerminationRequest.EmployeeId);
                var comment = string.Format(EmploymentTerminationRequestResources.RequestDeletedJiraComment, employee.LineManagerFullName);

                ActionSetService.AddCommentToJiraIssues(request, comment);
                ActionSetService.SendNotificationToEmailRecipients(request, requestDto,
                    TemplateCodes.EmploymentTerminationRequestDeletedSubject,
                    TemplateCodes.EmploymentTerminationRequestDeletedBody);
            }

            if (!string.IsNullOrEmpty(employmentTerminationRequest.BackupData))
            {
                var employee = unitOfWork.Repositories.Employees.GetById(employmentTerminationRequest.EmployeeId);
                var employmentPeriod = employee.GetLastEmploymentPeriod();
                var backupData = JsonHelper.Deserialize<EmploymentTerminationBackupDataDto>(employmentTerminationRequest.BackupData);

                employmentPeriod.EndDate = backupData.EndDate;
                employmentPeriod.TerminatedOn = backupData.TerminationDate;
                employmentPeriod.TerminationReasonId = backupData.TerminationReasonId;

                _businessEventPublisher.PublishBusinessEvent(new EmploymentPeriodChangedBusinessEvent
                {
                    EmployeeId = employmentPeriod.EmployeeId,
                    From = employmentPeriod.StartDate,
                    To = employmentPeriod.EndDate
                });

                _businessEventPublisher.PublishBusinessEvent(AllocationHelper.CreateEventFromEmploymentPeriod(employmentPeriod));

                var activeDirectoryUserChangeDescription = new ActiveDirectoryUserChangeDescriptionDto
                {
                    EmployeeId = employmentTerminationRequest.EmployeeId,
                    EndDate = backupData.EndDate
                };

                _activeDirectoryWriter.ModifyActiveDirectoryUser(activeDirectoryUserChangeDescription);
            }

            return base.OnDeleting(request, requestDto, unitOfWork);
        }

        private bool CanBeModified(RequestDto request, long byUserId)
        {
            if (request.Status == RequestStatus.Draft || request.Status == RequestStatus.Pending || request.Status == RequestStatus.Approved)
            {
                if (_userService.GetUserRolesById(byUserId).Contains(SecurityRoleType.CanModifyApprovedEmploymentTerminationRequest))
                {
                    return true;
                }

                if (request.Status != RequestStatus.Approved)
                {
                    if (request.RequestingUserId == byUserId)
                    {
                        return true;
                    }

                    if (request.ApprovalGroups != null)
                    {
                        return request.ApprovalGroups.Any(g => g.Approvals.Any(a => a.UserId == byUserId));
                    }

                    var employee = _employeeService.GetEmployeeOrDefaultByUserId(byUserId);
                    var approverGroups = GetApproverGroups(request, (EmploymentTerminationRequestDto)request.RequestObject);

                    return approverGroups.Any(g => g.Approvers.Any(a => a.EmployeeId == employee?.Id));
                }
            }

            return false;
        }

        public override void OnAllApproved(Request request, RequestDto requestDto)
        {
            base.OnAllApproved(request, requestDto);

            var backupData = SubmitChanges((EmploymentTerminationRequestDto)requestDto.RequestObject);

            request.EmploymentTerminationRequest.BackupData = JsonHelper.Serialize(backupData);
        }

        private EmploymentTerminationBackupDataDto SubmitChanges(EmploymentTerminationRequestDto employmentTerminationRequest)
        {
            EmploymentTerminationBackupDataDto backupData;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employmentPeriod = unitOfWork.Repositories.EmploymentPeriods
                    .Where(p => p.EmployeeId == employmentTerminationRequest.EmployeeId)
                    .OrderByDescending(p => p.StartDate)
                    .First();

                if (employmentPeriod.StartDate >= employmentTerminationRequest.EndDate)
                {
                    throw new RequestMissingDataForStatusChangeBusinessException(EmploymentTerminationRequestResources.NewerEmploymentPeriodError);
                }

                backupData = new EmploymentTerminationBackupDataDto
                {
                    EndDate = employmentPeriod.EndDate,
                    TerminationDate = employmentPeriod.TerminatedOn,
                    TerminationReasonId = employmentPeriod.TerminationReasonId
                };

                employmentPeriod.Employee.CurrentEmployeeStatus = EmployeeStatus.Leave;

                employmentPeriod.EndDate = employmentTerminationRequest.EndDate;
                employmentPeriod.TerminatedOn = employmentTerminationRequest.TerminationDate;
                employmentPeriod.TerminationReasonId = employmentTerminationRequest.ReasonId;

                var activeDirectoryUserChangeDescription = new ActiveDirectoryUserChangeDescriptionDto
                {
                    EmployeeId = employmentTerminationRequest.EmployeeId,
                    EndDate = employmentTerminationRequest.EndDate
                };

                _activeDirectoryWriter.ModifyActiveDirectoryUser(activeDirectoryUserChangeDescription);

                unitOfWork.Repositories.TechnicalInterviewers
                    .DeleteEach(i => i.EmployeeId == employmentTerminationRequest.EmployeeId);

                unitOfWork.Repositories.PricingEngineers
                    .DeleteEach(i => i.EmployeeId == employmentTerminationRequest.EmployeeId);

                unitOfWork.Commit();

                _businessEventPublisher.PublishBusinessEvent(AllocationHelper.CreateEventFromEmploymentPeriod(employmentPeriod));
            }

            return backupData;
        }

        public override void Execute(RequestDto request, EmploymentTerminationRequestDto requestDto)
        {
        }

        public override IEnumerable<AlertDto> Validate(RequestDto request, EmploymentTerminationRequestDto requestDto)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var isOtherRequestActive = unitOfWork.Repositories.EmploymentTerminationRequests
                    .Any(r => r.Id != request.Id && r.EmployeeId == requestDto.EmployeeId
                        && r.Request.Status != RequestStatus.Rejected && r.Request.Status != RequestStatus.Completed);

                if (isOtherRequestActive)
                {
                    yield return new AlertDto { Message = EmploymentTerminationRequestResources.DuplicateError, Type = AlertType.Error };
                }
            }

            if (request.RequestingUserId != PrincipalProvider.Current.Id
                || PrincipalProvider.Current.IsInRole(SecurityRoleType.CanTerminateAllEmployees))
            {
                yield break;
            }

            if (PrincipalProvider.Current.IsInRole(SecurityRoleType.CanTerminateMyEmployees))
            {
                var employee = _employeeService.GetEmployeeOrDefaultById(requestDto.EmployeeId);
                var managerOrgUnitsIds = _orgUnitService.GetManagerOrgUnitDescendantIds(PrincipalProvider.Current.EmployeeId);

                if (employee.LineManagerId != PrincipalProvider.Current.EmployeeId
                    && !managerOrgUnitsIds.Contains(employee.OrgUnitId))
                {
                    var message = string.Format(EmploymentTerminationRequestResources.OneRoleMissingError,
                        SecurityRoleType.CanTerminateAllEmployees);

                    yield return new AlertDto { Message = message, Type = AlertType.Error };
                }
            }
            else
            {
                var message = string.Format(EmploymentTerminationRequestResources.SomeOfRolesAreMissingError,
                    SecurityRoleType.CanTerminateMyEmployees + ", " + SecurityRoleType.CanTerminateAllEmployees);

                yield return new AlertDto { Message = message, Type = AlertType.Error };
            }
        }

        public override string GetRequestDescription(RequestDto request, EmploymentTerminationRequestDto data)
        {
            var employee = _employeeService.GetEmployeeOrDefaultById(data.EmployeeId);

            if (employee == null)
            {
                return null;
            }

            return string.Format(EmploymentTerminationRequestResources.RequestDescriptionFormat, employee.FullName, data.EndDate.ToShortDateString());
        }

        public override IQueryable<Request> ConfigureIncludes(IQueryable<Request> repository)
        {
            return repository.Include(r => r.EmploymentTerminationRequest);
        }
    }
}