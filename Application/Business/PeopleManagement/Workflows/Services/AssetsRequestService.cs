﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.PeopleManagement.Interfaces;
using Smt.Atomic.Business.PeopleManagement.Resources;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Exceptions;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.Business.Workflows.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Jira.Dto;
using Smt.Atomic.Data.Jira.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.PeopleManagement.Workflows.Services
{
    internal class AssetsRequestService : RequestServiceBase<AssetsRequest, AssetsRequestDto>, IAssetsRequestService
    {
        private readonly IUnitOfWorkService<IPeopleManagementDbScope> _peopleManagementUnitOfWorkService;
        private readonly IEmployeeService _employeeService;
        private readonly IApprovingPersonService _approvingPersonService;
        private readonly IJiraService _jiraService;

        public AssetsRequestService(
            IPrincipalProvider principalProvider,
            IWorkflowNotificationService workflowNotificationService,
            IActionSetService actionSetService,
            IClassMappingFactory classMappingFactory,
            IUnitOfWorkService<IPeopleManagementDbScope> peopleManagementUnitOfWorkService,
            IEmployeeService employeeService,
            IApprovingPersonService approvingPersonService,
            IJiraService jiraService,
            IRequestWorkflowService requestWorkflowService,
            ISystemParameterService systemParameterService)
            : base(actionSetService,
                  principalProvider,
                  classMappingFactory,
                  workflowNotificationService,
                  requestWorkflowService,
                  systemParameterService)
        {
            _peopleManagementUnitOfWorkService = peopleManagementUnitOfWorkService;
            _employeeService = employeeService;
            _approvingPersonService = approvingPersonService;
            _jiraService = jiraService;
        }

        public override RequestType RequestType => RequestType.AssetsRequest;

        public override RequestTypeGroup RequestTypeGroup => RequestTypeGroup.Manager;

        public override string RequestName => WorkflowResources.AssetsRequestName;

        public override SecurityRoleType? RequiredRoleType => SecurityRoleType.CanRequestAssets;

        public override IQueryable<Request> ConfigureIncludes(IQueryable<Request> repository)
        {
            return repository.Include(r => r.AssetsRequest);
        }

        public override bool ShouldStartAsDraft => true;

        public override void Execute(RequestDto request, AssetsRequestDto requestDto)
        {
        }

        public override IEnumerable<ApproverGroupDto> GetApproverGroups(RequestDto request, AssetsRequestDto requestDto)
        {
            yield break;
        }

        public override string GetRequestDescription(RequestDto request, AssetsRequestDto data)
        {
            return string.Format(AssetsRequestResources.AssetsDescriptionFormat, _employeeService.GetEmployeeOrDefaultById(data.EmployeeId).FullName);
        }

        public override IEnumerable<AlertDto> Validate(RequestDto request, AssetsRequestDto requestDto)
        {
            var alert = ValidateCommentRequirement(requestDto).Where(a => a != null).FirstOrDefault();

            if (alert != null)
            {
                yield return alert;
            }

            if (!requestDto.IsCreatedAutomatically)
            {
                var isAnyAssetTypeChosen = (requestDto.AssetTypeOtherIds ?? Enumerable.Empty<long>())
                    .Concat(requestDto.AssetTypeOptionalHardwareIds ?? Enumerable.Empty<long>())
                    .Cast<long?>()
                    .Concat(new[]
                    {
                        requestDto.AssetTypeBackpackId,
                        requestDto.AssetTypeHardwareSetId,
                        requestDto.AssetTypeHeadsetId,
                        requestDto.AssetTypeSoftwareId
                    })
                    .Any(id => id.HasValue);

                if (!isAnyAssetTypeChosen)
                {
                    yield return AlertDto.CreateError(AssetsRequestResources.NoAssetError);
                }
            }
        }

        public override void OnEditing(RecordEditingEventArgs<Request, RequestDto, IWorkflowsDbScope> eventArgs)
        {
            var newRequestObjectDto = (AssetsRequestDto)eventArgs.InputDto.RequestObject;
            var oldRequestObject = (AssetsRequest)eventArgs.DbEntity.RequestObject;

            if (!newRequestObjectDto.OnboardingRequestId.HasValue && newRequestObjectDto.EmployeeId != oldRequestObject.EmployeeId)
            {
                throw new BusinessException(AssetsRequestResources.EmployeeFieldIsReadonly);
            }

            base.OnEditing(eventArgs);
        }

        public override BoolResult OnDeleting(Request request, RequestDto requestDto, IUnitOfWork<IWorkflowsDbScope> unitOfWork)
        {
            var assetsRequest = (AssetsRequestDto)requestDto.RequestObject;

            if (request.Status == RequestStatus.Completed)
            {
                using (var context = _jiraService.CreateReadWrite())
                {
                    var recruiterEmployeeId = unitOfWork.Repositories.OnboardingRequests
                        .Where(o => o.Id == assetsRequest.OnboardingRequestId)
                        .Select(o => o.RecruiterEmployeeId)
                        .Single();
                    var recruiter = _employeeService.GetEmployeeOrDefaultById(recruiterEmployeeId);
                    var employee = _employeeService.GetEmployeeOrDefaultById(assetsRequest.EmployeeId);
                    var comment = string.Format(AssetsRequestResources.RequestDeletedJiraComment,
                        employee.FullName,
                        employee.Login,
                        recruiter.FullName,
                        recruiter.Login);

                    foreach (var entry in request.HistoryEntries.Where(e => e.Type == RequestHistoryEntryType.JiraIssueCreated))
                    {
                        context.AddCommentToIssue(entry.JiraIssueKey, comment);
                    }
                }
            }

            return base.OnDeleting(request, requestDto, unitOfWork);
        }

        public IList<AlertDto> OnEditingOnboarding(
            OnboardingRequestDto oldOnboardingRequestDto, 
            OnboardingRequestDto newOnboardingRequestDto, 
            string onboardingRequestViewModelTypeName)
        {
            if (oldOnboardingRequestDto.OnboardingDate != newOnboardingRequestDto.OnboardingDate
                || oldOnboardingRequestDto.LocationId != newOnboardingRequestDto.LocationId
                || oldOnboardingRequestDto.LineManagerEmployeeId != newOnboardingRequestDto.LineManagerEmployeeId
                || oldOnboardingRequestDto.StartDate != newOnboardingRequestDto.StartDate
                || oldOnboardingRequestDto.OnboardingLocationId != newOnboardingRequestDto.OnboardingLocationId)
            {
                using (var unitOfWork = _peopleManagementUnitOfWorkService.Create())
                using (var context = _jiraService.CreateReadWrite())
                {
                    var requestId = unitOfWork.Repositories.AssetRequest.Where(a => a.OnboardingRequestId == newOnboardingRequestDto.Id).Select(a => a.Id).Single();
                    var request = unitOfWork.Repositories.Request.GetById(requestId);

                    foreach (var entry in request.HistoryEntries.Where(e => e.Type == RequestHistoryEntryType.JiraIssueCreated))
                    {
                        const string viewersCustomeField = "customfield_10260";
                        var updateIssueDto = new UpdateIssueDto { DueDate = newOnboardingRequestDto.OnboardingDate };

                        if (oldOnboardingRequestDto.LineManagerEmployeeId != newOnboardingRequestDto.LineManagerEmployeeId)
                        {
                            var oldValue = context.GetValueFromCustomField(entry.JiraIssueKey, viewersCustomeField);
                            var newValue = new List<string>(oldValue);

                            if (oldOnboardingRequestDto.LineManagerEmployeeId.HasValue)
                            {
                                newValue.RemoveAll(a => a == _employeeService.GetEmployeeOrDefaultByUserId(oldOnboardingRequestDto.LineManagerEmployeeId.Value).Login);
                            }

                            if (newOnboardingRequestDto.LineManagerEmployeeId.HasValue)
                            {
                                newValue.Add(_employeeService.GetEmployeeOrDefaultByUserId(oldOnboardingRequestDto.LineManagerEmployeeId.Value).Login);
                            }

                            updateIssueDto.CustomFields.Add(viewersCustomeField, newValue.Distinct().ToArray());
                        }

                        context.UpdateIssue(entry.JiraIssueKey, updateIssueDto);
                    }

                    var viewModelType = TypeHelper.FindTypeInLoadedAssemblies(onboardingRequestViewModelTypeName);

                    return ActionSetService.UpdateActions(request, oldOnboardingRequestDto, newOnboardingRequestDto, viewModelType)?.Alerts;
                }
            }

            return null;
        }

        private IEnumerable<AlertDto> ValidateCommentRequirement(AssetsRequestDto requestDto)
        {
            yield return ValidateCommentRequirement(requestDto.AssetTypeHardwareSetId, requestDto.AssetTypeHardwareSetComment);
            yield return ValidateCommentRequirement(requestDto.AssetTypeHeadsetId, requestDto.AssetTypeHeadsetComment);
            yield return ValidateCommentRequirement(requestDto.AssetTypeBackpackId, requestDto.AssetTypeBackpackComment);
            yield return ValidateCommentRequirement(requestDto.AssetTypeOptionalHardwareIds, requestDto.AssetTypeOptionalHardwareComment);
            yield return ValidateCommentRequirement(requestDto.AssetTypeSoftwareId, requestDto.AssetTypeSoftwareComment);
            yield return ValidateCommentRequirement(requestDto.AssetTypeOtherIds, requestDto.AssetTypeOtherComment);
        }

        private AlertDto ValidateCommentRequirement(long? assetTypeId, string assetTypeComment)
        {
            return assetTypeId.HasValue
                ? ValidateCommentRequirement(new[] { assetTypeId.Value }, assetTypeComment)
                : null;
        }

        private AlertDto ValidateCommentRequirement(long[] assetTypeIds, string assetTypeComment)
        {
            if ((assetTypeIds?.Any() ?? false) && string.IsNullOrEmpty(assetTypeComment))
            {
                using (var unityOfWork = _peopleManagementUnitOfWorkService.Create())
                {
                    if (unityOfWork.Repositories.AssetTypes.Any(d => assetTypeIds.Contains(d.Id) && d.DetailsRequired))
                    {
                        return AlertDto.CreateError(AssetsRequestResources.CommentsRequiredError);
                    }
                }
            }

            return null;
        }
    }
}
