﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.PeopleManagement.Resources;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.Business.Workflows.Services;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Exchange.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.PeopleManagement.Workflows.Services
{
    internal class AddHomeOfficeRequestService : RequestServiceBase<AddHomeOfficeRequest, AddHomeOfficeRequestDto>
    {
        private readonly ICalendarOfficeService _calendarOfficeService;
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService;
        private readonly ITimeService _timeService;
        private readonly IApprovingPersonService _approvingPersonService;
        private readonly IUserService _userService;

        public AddHomeOfficeRequestService(IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
            IWorkflowNotificationService workflowNotificationService,
            ICalendarOfficeService calendarOfficeService,
            IPrincipalProvider principalProvider,
            ITimeService timeService,
            IApprovingPersonService approvingPersonService,
            IUserService userService,
            IClassMappingFactory classMappingFactory,
            IActionSetService actionSetService,
            ISystemParameterService systemParameterService,
            IRequestWorkflowService requestWorkflowService)
            : base(actionSetService,
                  principalProvider,
                  classMappingFactory,
                  workflowNotificationService,
                  requestWorkflowService,
                  systemParameterService)
        {
            _unitOfWorkService = unitOfWorkService;
            _calendarOfficeService = calendarOfficeService;
            _timeService = timeService;
            _approvingPersonService = approvingPersonService;
            _userService = userService;
        }

        public override RequestType RequestType => RequestType.AddHomeOfficeRequest;

        public override string RequestName => WorkflowResources.AddHomeOfficeRequesName;

        public override RequestTypeGroup RequestTypeGroup => RequestTypeGroup.TimeTracking;

        public override SecurityRoleType? RequiredRoleType => SecurityRoleType.CanRequestHomeOffice;

        public override IEnumerable<AlertDto> Validate(RequestDto request, AddHomeOfficeRequestDto addHomeOfficeRequest)
        {
            IList<AlertDto> alerts = new List<AlertDto>(base.Validate(request, addHomeOfficeRequest));

            var approverGroups = GetApproverGroups(request, addHomeOfficeRequest).ToList();

            if (!approverGroups.Any() || !approverGroups.Any(a => a.Approvers.Any()))
            {
                alerts.Add(new AlertDto
                {
                    Message = AddHomeOfficeRequestResources.ApproversError,
                    Type = AlertType.Error
                });
            }

            return alerts;
        }

        public override AddHomeOfficeRequestDto GetDraft(IDictionary<string, object> parameters)
        {
            ValidateCurrentPrincipalHasEmployee();

            var defaultHomeOfficeDate = _timeService.GetCurrentDate().AddDays(1);

            return new AddHomeOfficeRequestDto
            {
                From = defaultHomeOfficeDate,
                To = defaultHomeOfficeDate,
            };
        }

        public override string GetRequestDescription(RequestDto request, AddHomeOfficeRequestDto homeOfficeRequest)
        {
            return string.Format(AddHomeOfficeRequestResources.HomeOfficeDescription, homeOfficeRequest.From, homeOfficeRequest.To);
        }

        public override IEnumerable<ApproverGroupDto> GetApproverGroups(RequestDto request, AddHomeOfficeRequestDto addHomeOfficeRequest)
        {
            long employeeId;
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                employeeId = GetEmployee(unitOfWork, request).Id;
            }

            var approvers =
                _approvingPersonService.GetProjectApprovers(employeeId, addHomeOfficeRequest.From,
                    addHomeOfficeRequest.To).ToList();
            var mainApprover = _approvingPersonService.GetEmployeeMainManager(employeeId);

            if (mainApprover.HasValue)
            {
                approvers.Add(mainApprover.Value);
            }

            yield return new ApproverGroupDto(ApprovalGroupMode.And, approvers);
        }

        public override BoolResult OnDeleting(Request request, RequestDto requestDto, IUnitOfWork<IWorkflowsDbScope> unitOfWork)
        {
            DeleteCalendarEntries(unitOfWork, requestDto);

            return base.OnDeleting(request, requestDto, unitOfWork);
        }

        private void DeleteCalendarEntries(IUnitOfWork<IWorkflowsDbScope> unitOfWork, RequestDto requestDto)
        {
            var calendarEntries =
                unitOfWork.Repositories.AbsenceCalendarEntries.Where(ce => ce.RequestId == requestDto.Id).ToList();

            foreach (var calendarEntry in calendarEntries)
            {
                try
                {
                    _calendarOfficeService.DeleteAppointment(calendarEntry.ExchangeId);
                    unitOfWork.Repositories.AbsenceCalendarEntries.Delete(calendarEntry);
                    unitOfWork.Commit();
                }
                catch (Exception)
                {
                    // ignored
                }
            }
        }

        public override bool CanBeDeleted(RequestDto requestDto, long byUserId)
        {
            if (_userService.GetUserRolesById(byUserId).Contains(SecurityRoleType.CanDeleteHomeOfficeRequests))
            {
                return true;
            }

            var homeOfficeRequest = (AddHomeOfficeRequestDto)requestDto.RequestObject;
            var currentDate = _timeService.GetCurrentDate();

            return requestDto.RequestingUserId == byUserId
                   && homeOfficeRequest.From > currentDate;
        }

        public override void Execute(RequestDto request, AddHomeOfficeRequestDto addHomeOfficeRequest)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employee = GetEmployee(unitOfWork, request);
                var calendars = GetAbsenceCalendars(employee);

                foreach (var calendar in calendars)
                {
                    string appointmentId;

                    try
                    {
                        appointmentId = _calendarOfficeService.CreateAppointment(
                            calendar.OfficeGroupEmail,
                            CalendarAppointmentType.HomeOffice,
                            "Home office",
                            addHomeOfficeRequest.From.StartOfDay(),
                            addHomeOfficeRequest.To.EndOfDay(),
                            request.Description.English,
                            employee.FullName,
                            new[] { employee.Email });
                    }
                    catch (Exception)
                    {
                        appointmentId = null;
                    }

                    if (!appointmentId.IsNullOrEmpty() && !unitOfWork.Repositories.AbsenceCalendarEntries.Any(e => e.ExchangeId == appointmentId))
                    {
                        unitOfWork.Repositories.AbsenceCalendarEntries.Add(new AbsenceCalendarEntry
                        {
                            ExchangeId = appointmentId,
                            RequestId = request.Id
                        });

                        unitOfWork.Commit();
                    }
                }
            }
        }

        public List<CalendarAbsenceDto> GetAbsenceCalendars(Employee employee)
        {
            var calendarAbsenceToDtoMapping = ClassMappingFactory.CreateMapping<CalendarAbsence, CalendarAbsenceDto>();

            return employee.CalendarAbsences.Select(c => calendarAbsenceToDtoMapping.CreateFromSource(c)).ToList();
        }

        private Employee GetEmployee(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, RequestDto request)
        {
            return unitOfWork.Repositories.Employees.Single(e => e.UserId == request.RequestingUserId);
        }

        public override IQueryable<Request> ConfigureIncludes(IQueryable<Request> repository)
        {
            return repository.Include(r => r.AddHomeOfficeRequest);
        }
    }
}