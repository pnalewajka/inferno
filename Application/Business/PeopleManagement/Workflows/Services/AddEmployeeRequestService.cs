﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.ActiveDirectory.Dto;
using Smt.Atomic.Business.ActiveDirectory.Interfaces;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.MeetMe.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.PeopleManagement.Dto;
using Smt.Atomic.Business.PeopleManagement.Interfaces;
using Smt.Atomic.Business.PeopleManagement.Resources;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.Business.Workflows.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.PeopleManagement.Workflows.Services
{
    internal class AddEmployeeRequestService : RequestServiceBase<AddEmployeeRequest, AddEmployeeRequestDto>
    {
        private readonly ITimeService _timeService;
        private readonly IUserService _userService;
        private readonly IOrgUnitService _orgUnitService;
        private readonly IEmployeeService _employeeService;
        private readonly ILocationService _locationService;
        private readonly IUserAcronymService _userAcronymService;
        private readonly IActiveDirectoryWriter _activeDirectoryWriter;
        private readonly IUserPermissionService _userPermissionService;
        private readonly IEmploymentPeriodService _employmentPeriodService;
        private readonly IActiveDirectorySearchService _activeDirectorySearchService;
        private readonly IUnitOfWorkService<IWorkflowsDbScope> _unitOfWorkService;
        private readonly IMeetingService _meetingService;

        public AddEmployeeRequestService(
            ITimeService timeService,
            IUserService userService,
            IOrgUnitService orgUnitService,
            IEmployeeService employeeService,
            ILocationService locationService,
            IPrincipalProvider principalProvider,
            IUserAcronymService userAcronymService,
            IUserPermissionService userPermissionService,
            IActiveDirectoryWriter activeDirectoryWriter,
            IEmploymentPeriodService employmentPeriodService,
            IWorkflowNotificationService workflowNotificationService,
            IActiveDirectorySearchService activeDirectorySearchService,
            IUnitOfWorkService<IWorkflowsDbScope> unitOfWorkService,
            IActionSetService actionSetService,
            IClassMappingFactory classMappingFactory,
            IMeetingService meetingService,
            IRequestWorkflowService requestWorkflowService,
            ISystemParameterService systemParameterService)
            : base(actionSetService,
                  principalProvider,
                  classMappingFactory,
                  workflowNotificationService,
                  requestWorkflowService,
                  systemParameterService)
        {
            _timeService = timeService;
            _userService = userService;
            _orgUnitService = orgUnitService;
            _employeeService = employeeService;
            _locationService = locationService;
            _userAcronymService = userAcronymService;
            _userPermissionService = userPermissionService;
            _activeDirectoryWriter = activeDirectoryWriter;
            _employmentPeriodService = employmentPeriodService;
            _activeDirectorySearchService = activeDirectorySearchService;
            _unitOfWorkService = unitOfWorkService;
            _meetingService = meetingService;
        }

        public override RequestType RequestType => RequestType.AddEmployeeRequest;

        public override string RequestName => WorkflowResources.AddEmployeeRequestName;

        public override RequestTypeGroup RequestTypeGroup => RequestTypeGroup.Manager;

        public override SecurityRoleType? RequiredRoleType => SecurityRoleType.CanRequestAddingEmployee;

        public override AddEmployeeRequestDto GetDraft(IDictionary<string, object> parameters)
        {
            const decimal defaultHoursPerDay = 8M;

            var currentDate = _timeService.GetCurrentDate();

            return new AddEmployeeRequestDto
            {
                ContractFrom = currentDate,
                ContractSignDate = currentDate,
                MondayHours = defaultHoursPerDay,
                TuesdayHours = defaultHoursPerDay,
                WednesdayHours = defaultHoursPerDay,
                ThursdayHours = defaultHoursPerDay,
                FridayHours = defaultHoursPerDay,
                VacationHourToDayRatio = defaultHoursPerDay,
                TimeReportingMode = TimeReportingMode.Detailed
            };
        }

        public override bool ShouldStartAsDraft => true;

        public override IEnumerable<AlertDto> Validate(RequestDto request, AddEmployeeRequestDto addEmployeeRequest)
        {
            IList<AlertDto> alerts = new List<AlertDto>(base.Validate(request, addEmployeeRequest));

            if (!_locationService.CheckIfPlaceOfWorkIsAllowedForLocation(addEmployeeRequest.LocationId, addEmployeeRequest.PlaceOfWork))
            {
                alerts.Add(new AlertDto { Message = AddEmployeeRequestResources.WrongPlaceOfWorkError, Type = AlertType.Error });
            }

            var employmentType = GetEmploymentType(addEmployeeRequest.ContractTypeId);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var repository = unitOfWork.Repositories.Users;

                if (repository.Any(u => u.Email == addEmployeeRequest.Email))
                {
                    alerts.Add(new AlertDto { Message = AddEmployeeRequestResources.EmailExistsError, Type = AlertType.Error });
                }

                if (employmentType != EmploymentType.External)
                {
                    if (repository.Any(u => u.Login == addEmployeeRequest.Login) || _activeDirectorySearchService.CheckIfUsed(addEmployeeRequest.Login))
                    {
                        alerts.Add(new AlertDto { Message = AddEmployeeRequestResources.LoginExistsError, Type = AlertType.Error });
                    }
                }
            }

            return alerts;
        }

        public override string GetRequestDescription(RequestDto request, AddEmployeeRequestDto data)
        {
            return string.Format(AddEmployeeRequestResources.RequestDescriptionFormat, $"{data.FirstName} {data.LastName}");
        }

        public override IEnumerable<ApproverGroupDto> GetApproverGroups(RequestDto request, AddEmployeeRequestDto addEmployeeRequest)
        {
            return Enumerable.Empty<ApproverGroupDto>();
        }

        public override void Execute(RequestDto request, AddEmployeeRequestDto addEmployeeRequest)
        {
            var acronym = new UserAcronymDto
            {
                Acronym = _userAcronymService.GenerateAcronym(addEmployeeRequest.FirstName, addEmployeeRequest.LastName)
            };

            var activeDirectoryId = CreateActiveDirectoryUser(addEmployeeRequest, acronym.Acronym);
            var employmentType = GetEmploymentType(addEmployeeRequest.ContractTypeId);
            var userId = employmentType != EmploymentType.External
                ? CreateUser(addEmployeeRequest, activeDirectoryId)
                : (long?)null;

            var employeeId = CreateEmployee(addEmployeeRequest, userId, acronym.Acronym);

            CreateEmploymentPeriod(addEmployeeRequest, employeeId);
            AssignMinimunOvertimeBankBalance(addEmployeeRequest, employeeId);

            _userAcronymService.AddAcronym(acronym);
            _meetingService.ScheduleFirstMeeting(employeeId, addEmployeeRequest.ContractFrom);
        }

        private void AssignMinimunOvertimeBankBalance(AddEmployeeRequestDto addEmployeeRequest, long employeeId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var newMinBalance = unitOfWork.Repositories.ContractTypes
                    .Where(c => c.Id == addEmployeeRequest.ContractTypeId)
                    .Select(c => c.MinimunOvertimeBankBalance)
                    .Single();

                var employee = unitOfWork.Repositories.Employees.GetById(employeeId);
                employee.MinimumOvertimeBankBalance = newMinBalance;

                unitOfWork.Commit();
            }
        }

        private Guid? CreateActiveDirectoryUser(AddEmployeeRequestDto request, string acronym)
        {
            var activeDirectoryUserCreationDto = new ActiveDirectoryUserCreationDto
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Login = request.Login,
                Acronym = acronym,
                JobTitleId = request.JobTitleId,
                JobProfileId = request.JobProfileId,
                OrgUnitId = request.OrgUnitId,
                LocationId = request.LocationId,
                PlaceOfWork = request.PlaceOfWork,
                CompanyId = request.CompanyId,
                LineManagerId = request.LineManagerId,
                StartDate = request.ContractFrom,
                EndDate = request.ContractTo,
                EffectiveDate = request.ContractSignDate,
                ContractTypeId = request.ContractTypeId,
                CrmId = request.CrmId
            };

            return _activeDirectoryWriter.CreateActiveDirectoryUser(activeDirectoryUserCreationDto);
        }

        private long CreateEmployee(AddEmployeeRequestDto request, long? userId, string acronym)
        {
            return _employeeService.CreateEmployee(new EmployeeDto
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Acronym = acronym,
                JobTitleId = request.JobTitleId,
                LocationId = request.LocationId,
                PlaceOfWork = request.PlaceOfWork,
                CurrentEmployeeStatus = EmployeeStatus.NewHire,
                IsProjectContributor = _orgUnitService.GetDefaultProjectContributorForOrgUnit(request.OrgUnitId),
                JobProfileIds = new[] { request.JobProfileId },
                JobMatrixIds = GetJobMatrixLevelIds(request.JobTitleId),
                CompanyId = request.CompanyId,
                UserId = userId,
                LineManagerId = request.LineManagerId,
                OrgUnitId = request.OrgUnitId,
                EmployeeFeatures = ShowInUtilizationReport(request.OrgUnitId) ? EmployeeFeatures.ShowInUtilizationReport : 0
            });
        }

        private void CreateEmploymentPeriod(AddEmployeeRequestDto request, long employeeId)
        {
            _employmentPeriodService.CreateEmploymentPeriod(new EmploymentPeriodDto
            {
                StartDate = request.ContractFrom,
                EndDate = request.ContractTo,
                SignedOn = request.ContractSignDate,
                EmployeeId = employeeId,
                CompanyId = request.CompanyId,
                JobTitleId = request.JobTitleId,
                IsActive = true,
                NoticePeriod = NoticePeriod.OneMonth,
                MondayHours = request.MondayHours,
                TuesdayHours = request.TuesdayHours,
                WednesdayHours = request.WednesdayHours,
                ThursdayHours = request.ThursdayHours,
                FridayHours = request.FridayHours,
                SaturdayHours = request.SaturdayHours,
                SundayHours = request.SundayHours,
                ContractTypeId = request.ContractTypeId,
                ContractDuration = request.ContractDuration,
                VacationHourToDayRatio = request.VacationHourToDayRatio,
                TimeReportingMode = request.TimeReportingMode,
                AbsenceOnlyDefaultProjectId = request.AbsenceOnlyDefaultProjectId,
                CalendarId = request.CalendarId,
                LocationId = request.LocationId
            });
        }

        private long CreateUser(AddEmployeeRequestDto request, Guid? activeDirectoryId)
        {
            var userId = _userService.CreateUser(new UserDto
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Login = request.Login,
                Email = request.Email,
                IsActive = true,
                ActiveDirectoryId = activeDirectoryId
            });

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var profileIds = unitOfWork.Repositories.JobTitles
                    .Where(t => t.Id == request.JobTitleId)
                    .SelectMany(t => t.DefaultProfiles)
                    .Select(p => p.Id)
                    .ToList();

                _userPermissionService.AssignSecurityProfilesToUser(userId, profileIds);
            }

            return userId;
        }

        private EmploymentType? GetEmploymentType(long contractTypeId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.ContractTypes
                    .Where(c => c.Id == contractTypeId)
                    .Select(c => c.EmploymentType)
                    .Single();
            }
        }

        private long[] GetJobMatrixLevelIds(long jobTitleId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var jobMatrixLevelId = unitOfWork.Repositories.JobTitles
                    .Where(t => t.Id == jobTitleId)
                    .Select(t => t.JobMatrixLevelId)
                    .FirstOrDefault();

                return jobMatrixLevelId.HasValue ? new[] { jobMatrixLevelId.Value } : null;
            }
        }

        public override IQueryable<Request> ConfigureIncludes(IQueryable<Request> repository)
        {
            return repository.Include(r => r.AddEmployeeRequest);
        }

        private bool ShowInUtilizationReport(long orgUnitId)
        {
            var orgUnit = _orgUnitService.GetOrgUnitOrDefaultById(orgUnitId);

            return orgUnit != null
                ? orgUnit.OrgUnitFeatures.HasFlag(OrgUnitFeatures.ShowInUtilizationReport)
                : false;
        }
    }
}