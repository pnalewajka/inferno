﻿using System;

namespace Smt.Atomic.Business.PeopleManagement.Workflows.Dto
{
    public class EmploymentTerminationRequestDto
    {
        public long EmployeeId { get; set; }

        public DateTime TerminationDate { get; set; }

        public DateTime EndDate { get; set; }

        public long ReasonId { get; set; }
    }
}