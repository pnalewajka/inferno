﻿pivot.dataConverter = function (data) {
    return data.OnboardingRequests.map(function (a) {
        return {
            "Recruiter": a.Recruiter,
            "Requester": a.Requester,
            "New Hire": a.NewHire,
            "Recruiter Org. unit": a.RecruiterOrgUnit,
            "New Hire Org. unit": a.NewHireOrgUnit,
            "New Hire line manager": a.NewHireLineManager,
            "Onboarding date": Reporting.displayDate(a.OnboardingDate),
            "Start date": Reporting.displayDate(a.StartDate),
            "Is foreign employee": a.IsForeignEmployee ? "X" : "",
            "Welcome email info": a.IsWelcomeAnnounceEmailInfoDone ? "X" : "",
            "Welcome email photo": a.IsPhotoDocumentsDone ? "X" : "",
            "Payroll documents": a.IsPayrollDocumentsDone ? "X" : "",
            "Foreign employee documents": a.IsForeignEmployeeDocumentsDone ? "X" : "",
            "Onboarding status": Globals.enumResources.RequestStatus[a.RequestStatus] || "",
            "Assets request status": Globals.enumResources.RequestStatus[a.AssetsRequestStatus] || ""
        };
    });
};

pivot.configurations = [
    {
        name: "Default",
        code: "default",
        config: {
            "rows": [
                "Recruiter",
                "New Hire",
                "Start date",
                "New Hire Org. unit",
                "New Hire line manager",
                "Onboarding status",
                "Welcome email info",
                "Welcome email photo",
                "Foreign employee documents",
                "Payroll documents",
                "Assets request status"],
            "vals": [],
        },
    }
];
