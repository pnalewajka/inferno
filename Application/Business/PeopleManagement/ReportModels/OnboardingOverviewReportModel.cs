﻿using System;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.PeopleManagement.ReportModels
{
    public class OnboardingOverviewReportModel
    {
        public class OnboardingRequestModel
        {
            public string Recruiter { get; set; }
            public string Requester { get; set; }
            public string NewHire { get; set; }
            public string RecruiterOrgUnit { get; set; }
            public string NewHireOrgUnit { get; set; }
            public string NewHireLineManager { get; set; }
            public DateTime? OnboardingDate { get; set; }
            public DateTime? StartDate { get; set; }
            public bool IsForeignEmployee { get; set; }
            public bool IsWelcomeAnnounceEmailInfoDone { get; set; }
            public bool IsPhotoDocumentsDone { get; set; }
            public bool IsPayrollDocumentsDone { get; set; }
            public bool IsForeignEmployeeDocumentsDone { get; set; }
            public RequestStatus RequestStatus { get; set; }
            public RequestStatus? AssetsRequestStatus { get; set; }
        }

        public OnboardingRequestModel[] OnboardingRequests { get; set; }
    }
}
