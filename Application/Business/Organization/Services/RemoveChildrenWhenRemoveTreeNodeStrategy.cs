﻿using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using System.Linq;

namespace Smt.Atomic.Business.Organization.Services
{
    public class RemoveChildrenWhenRemoveTreeNodeStrategy : IRemoveTreeNodeStategy
    {
        void IRemoveTreeNodeStategy.Execute<TTreeNodeEntity>(IRepository<TTreeNodeEntity> repository, long nodeId)
        {
            var parentNode = repository.Single(n => n.Id == nodeId);
            var childrenNodes = repository.Where(n => n.Path.StartsWith(parentNode.Path) && n.Id != nodeId);
            repository.DeleteRange(childrenNodes, isDeletePermanent: true);
        }
    }
}
