﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Organization.Services
{
    public class OrgUnitImporterCardIndexDataService
        : TreeStructureImporterCardIndexDataService<OrgUnitImportDto, OrgUnit, IOrganizationScope>,
          IOrgUnitImporterCardIndexDataService
    {
        public OrgUnitImporterCardIndexDataService(
            ICardIndexServiceDependencies<IOrganizationScope> dependencies,
            ITreeStructureService<IOrganizationScope> treeStructureService,
            IRemoveTreeNodeStategy removeTreeNodeStategy)
            : base(dependencies, treeStructureService, removeTreeNodeStategy)
        {
        }
    }
}