﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Organization.Services
{
    public class PricingEngineerCardIndexDataService : CardIndexDataService<PricingEngineerDto, PricingEngineer, IOrganizationScope>,
        IPricingEngineerCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public PricingEngineerCardIndexDataService(ICardIndexServiceDependencies<IOrganizationScope> dependencies)
            : base(dependencies)
        {
        }

        protected override NamedFilters<PricingEngineer> NamedFilters
        {
            get
            {
                return new NamedFilters<PricingEngineer>(new[]
                {
                    new NamedFilter<PricingEngineer, long[]>(OrganizationFilterCodes.KeyTechnologyFilter,
                        (e, keyTechnologyIds) =>
                            (!keyTechnologyIds.Any() || e.SkillTags.Any(s => keyTechnologyIds.Contains(s.Id)))
                        ),

                    new NamedFilter<PricingEngineer, long[]>(OrganizationFilterCodes.LocationFilter,
                        (e, locationIds) => (e.Employee.LocationId.HasValue && locationIds.Contains(e.Employee.LocationId.Value))
                        )
                });
            }
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<PricingEngineer, PricingEngineerDto, IOrganizationScope> eventArgs)
        {
            eventArgs.InputEntity.SkillTags = EntityMergingService.CreateEntitiesFromIds<SkillTag, IOrganizationScope>(eventArgs.UnitOfWork, eventArgs.InputEntity.SkillTags.GetIds());
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<PricingEngineer, PricingEngineerDto, IOrganizationScope> eventArgs)
        {
            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.SkillTags, eventArgs.InputEntity.SkillTags);
        }

        protected override IOrderedQueryable<PricingEngineer> GetDefaultOrderBy(IQueryable<PricingEngineer> records, QueryCriteria criteria)
        {
            return records.OrderBy(p => p.Employee.Location.Name);
        }
    }
}