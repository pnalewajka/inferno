﻿using System.Collections.Generic;
using Castle.Core.Internal;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.Organization.Services
{
    public class TreeStructureCardIndexDataService<TTreeStructureDto, TTreeNodeEntity, TDbScope> :
        BaseTreeStructureCardIndexDataService<TTreeStructureDto, TTreeNodeEntity, TDbScope>,
        ITreeStructureCardIndexDataService<TTreeStructureDto>
        where TTreeNodeEntity : class, ITreeNodeEntity
        where TTreeStructureDto : class, ITreeNodeDto
        where TDbScope : IDbScope
    {
        public TreeStructureCardIndexDataService(
            ICardIndexServiceDependencies<TDbScope> dependencies,
            ITreeStructureService<TDbScope> treeStructureService,
            IRemoveTreeNodeStategy removeTreeNodeStategy)
            : base(dependencies, treeStructureService, removeTreeNodeStategy)
        {
        }

        public override CardIndexRecords<TTreeStructureDto> GetRecords(QueryCriteria criteria)
        {
            var records = base.GetRecords(criteria);

            if (IsSearchResult(criteria))
            {
                records.Rows.ForEach(r => r.Depth *= -1);
            }

            return records;
        }

        public override EditRecordResult EditRecord(TTreeStructureDto record, object context = null)
        {
            using (var transactionScope = TransactionHelper.CreateDefaultTransactionScope())
            {
                var originalRecord = GetRecordById(record.Id);
                var result = base.EditRecord(record, context);

                if (originalRecord.ParentId != record.ParentId || originalRecord.Name != record.Name || originalRecord.CustomOrder != record.CustomOrder)
                {
                    TreeStructureService.RefreshTreeStructure<TTreeNodeEntity>();
                }

                if (result.IsSuccessful)
                {
                    transactionScope.Complete();
                }

                return result;
            }
        }

        public override AddRecordResult AddRecord(TTreeStructureDto record, object context = null)
        {
            using (var transactionScope = TransactionHelper.CreateDefaultTransactionScope())
            {
                var result = base.AddRecord(record, context);
                TreeStructureService.RefreshTreeStructure<TTreeNodeEntity>();

                if (result.IsSuccessful)
                {
                    transactionScope.Complete();
                }

                return result;
            }
        }

        public override DeleteRecordsResult DeleteRecords(IEnumerable<long> idsToDelete, object context = null,
            bool isDeletePermanent = false)
        {
            using (var transactionScope = TransactionHelper.CreateDefaultTransactionScope())
            {
                var result = base.DeleteRecords(idsToDelete, context, isDeletePermanent);
                TreeStructureService.RefreshTreeStructure<TTreeNodeEntity>();

                if (result.IsSuccessful)
                {
                    transactionScope.Complete();
                }

                return result;
            }
        }

        protected bool IsSearchResult(QueryCriteria criteria)
        {
            return !string.IsNullOrEmpty(criteria.SearchPhrase);
        }
    }
}