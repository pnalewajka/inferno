﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Organization.Resources;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.Organization.Services
{
    public class TreeStructureImporterCardIndexDataService<TTreeNodeImportDto, TTreeNodeEntity, TDbScope>
        : BaseTreeStructureCardIndexDataService<TTreeNodeImportDto, TTreeNodeEntity, TDbScope>,
          ITreeStructureImporterCardIndexDataService<TTreeNodeImportDto>
        where TTreeNodeEntity : class, ITreeNodeEntity
        where TTreeNodeImportDto : class, ITreeNodeImportDto
        where TDbScope : IDbScope
    {
        public TreeStructureImporterCardIndexDataService(
            ICardIndexServiceDependencies<TDbScope> dependencies,
            ITreeStructureService<TDbScope> treeStructureService,
            IRemoveTreeNodeStategy removeTreeNodeStategy)
            : base(dependencies, treeStructureService, removeTreeNodeStategy)
        {
        }

        public override void BeforeBulkImportFinished(BeforeBulkImportFinishedEventArgs eventArgs)
        {
            base.BeforeBulkImportFinished(eventArgs);
            TreeStructureService.RefreshTreeStructure<TTreeNodeEntity>();
        }

        protected override void OnRecordAdding(
            RecordAddingEventArgs<TTreeNodeEntity, TTreeNodeImportDto, TDbScope> eventArgs)
        {
            FillParentId(eventArgs.InputEntity, eventArgs.InputDto, eventArgs);
            base.OnRecordAdding(eventArgs);
        }


        protected override void OnRecordEditing(
            RecordEditingEventArgs<TTreeNodeEntity, TTreeNodeImportDto, TDbScope> eventArgs)
        {
            FillParentId(eventArgs.InputEntity, eventArgs.InputDto, eventArgs);
            base.OnRecordEditing(eventArgs);
        }


        protected TTreeNodeImportDto GetRecordByCode(string code)
        {
            return GetQueryableRecords(new QueryCriteria()).FirstOrDefault(r => r.Code == code);
        }

        private void FillParentId(TTreeNodeEntity treeNodeEntity, TTreeNodeImportDto treeNodeImportItemDto,
                                  CardIndexEventArgs eventArgs)
        {
            if (string.IsNullOrEmpty(treeNodeImportItemDto.ParentCode))
            {
                return;
            }

            var parentNode = GetRecordByCode(treeNodeImportItemDto.ParentCode);

            if (parentNode != null)
            {
                treeNodeEntity.ParentId = parentNode.Id;
            }
            else
            {
                eventArgs.AddError(
                    $"{TreeStructureCardIndexDataServiceResources.ParentCodeDoNotExistMessage} {treeNodeImportItemDto.ParentCode}", false);
            }
        }
    }
}