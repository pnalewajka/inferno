﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Routing;
using Castle.Core;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Organization.Services
{
    [Interceptor(typeof(CacheInterceptor))]
    public class OrgChartService : IOrgChartService
    {
        private readonly IOrgUnitService _orgUnitService;
        private readonly IClassMapping<OrgUnitDto, OrgUnitChartDto> _orgUnitMapping;
        private readonly IUnitOfWorkService<IAllocationDbScope> _allocationUnitOfWorkService;
        private readonly IUnitOfWorkService<ISkillManagementDbScope> _skillManagementUnitOfWorkService;
        private readonly string _rootUnitCode;

        public OrgChartService(
            IOrgUnitService orgUnitService,
            IClassMapping<OrgUnitDto, OrgUnitChartDto> orgUnitMapping,
            IUnitOfWorkService<IAllocationDbScope> allocationUnitOfWorkService,
            IUnitOfWorkService<ISkillManagementDbScope> skillManagementUnitOfWorkService,
            ISystemParameterService parameterService)
        {
            _orgUnitService = orgUnitService;
            _orgUnitMapping = orgUnitMapping;
            _allocationUnitOfWorkService = allocationUnitOfWorkService;
            _skillManagementUnitOfWorkService = skillManagementUnitOfWorkService;
            _rootUnitCode = parameterService.GetParameter<string>(ParameterKeys.OrganizationRootUnitCode);
        }

        [Cached(CacheArea = CacheAreas.OrgChart, ExpirationInSeconds = 300)]
        public OrgChartDto GetOrganizationTree()
        {
            var orgUnits = _orgUnitService.GetAllOrgUnits().ToList();
            var employeeIds = orgUnits
                .Where(o => o.EmployeeId.HasValue)
                .Select(o => o.EmployeeId.Value)
                .Distinct()
                .ToList();
            var employees = GetEmployeesById(employeeIds).ToDictionary(e => e.Id);

            var jobTitles = GetAllJobTitles().ToDictionary(jt => jt.Id);
            var jobProfiles = GetAllJobProfiles().ToDictionary(jp => jp.Id);

            var levelsByPath = orgUnits
                .OrderBy(o => o.Path)
                .Aggregate(new Dictionary<string, OrgChartLevelDto>(),
                    MapTreeHierarchy(employees, jobTitles, jobProfiles));

            var employeesWithLineManager = GetEmployees(e => e.LineManagerId.HasValue && !employeeIds.Contains(e.Id) && employeeIds.Contains(e.LineManagerId.Value));

            var employeesByLineManager = employeesWithLineManager
                .ToLookup(e => e.LineManagerId.Value)
                .ToDictionary(e => e.Key, e => e.ToList());

            var levelToAddEmployees = levelsByPath
                .Select(l => l.Value)
                .Where(l => l.Root.LineManagerId.HasValue && l.Children.Any())
                .ToDictionary(l => l, l => new List<Employee>());

            foreach (var pair in levelsByPath)
            {
                var level = pair.Value;

                if (level.Root.LineManagerId.HasValue)
                {
                    List<Employee> children;

                    if (employeesByLineManager.TryGetValue(level.Root.LineManagerId.Value, out children))
                    {
                        var childrenWithSameOrgUnit = children.Where(e => e.OrgUnitId == level.Root.Id).ToList();

                        children.RemoveAll(e => childrenWithSameOrgUnit.Contains(e));

                        if (level.Children.Any())
                        {
                            levelToAddEmployees[level].AddRange(childrenWithSameOrgUnit);
                        }
                    }
                }
            }

            foreach (var pair in levelsByPath)
            {
                var level = pair.Value;

                if (level.Root.LineManagerId.HasValue)
                {
                    if (level.Children.Any())
                    {
                        List<Employee> children;

                        if (employeesByLineManager.TryGetValue(level.Root.LineManagerId.Value, out children))
                        {
                            levelToAddEmployees[level].AddRange(children);
                        }
                    }
                }
            }

            foreach (var pair in levelToAddEmployees)
            {
                var level = pair.Key;
                var employeesToAdd = pair.Value;

                if (level.Children.Any())
                {
                    level.Children
                    .AddRange(employeesToAdd.Select(e => GenerateEmployeeItem(e, employeesToAdd.Count + level.Children.Count, jobProfiles, jobTitles)));
                }
            }

            var orgTree = new OrgChartDto
            {
                TopLevel = levelsByPath
                    .FirstOrDefault(p => p.Value.Root?.Code == _rootUnitCode)
                    .Value ?? new OrgChartLevelDto()
            };

            return orgTree;
        }

        private Func<Dictionary<string, OrgChartLevelDto>, OrgUnitDto, Dictionary<string, OrgChartLevelDto>> MapTreeHierarchy(IDictionary<long, Employee> employees, Dictionary<long, JobTitle> jobTitles, IDictionary<long, JobProfile> jobProfiles)
        {
            return (levels, dto) =>
            {
                var segments = dto.Path.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
                var fullPath = string.Join("/", segments);
                var parentPath = string.Join("/", segments.Reverse().Skip(1).Reverse());

                if (!levels.ContainsKey(parentPath))
                {
                    levels[parentPath] = new OrgChartLevelDto();
                }

                var level = new OrgChartLevelDto
                {
                    Root = MapOrgUnitWithEmployees(dto, employees, jobTitles, jobProfiles)
                };

                levels[parentPath].Children.Add(level);
                levels[fullPath] = level;

                return levels;
            };
        }

        private OrgUnitChartDto MapOrgUnitWithEmployees(OrgUnitDto dto, IDictionary<long, Employee> employees, Dictionary<long, JobTitle> jobTitles, IDictionary<long, JobProfile> jobProfiles)
        {
            OrgUnitChartDto mappedOrgUnit = _orgUnitMapping.CreateFromSource(dto);
            Employee manager;

            if (!dto.EmployeeId.HasValue || !employees.TryGetValue(dto.EmployeeId.Value, out manager))
            {
                return mappedOrgUnit;
            }

            mappedOrgUnit.ManagerTitle = GetManagerTitle(jobTitles, jobProfiles, manager);

            mappedOrgUnit.ManagerName = $"{manager.FirstName} {manager.LastName}";
            mappedOrgUnit.LineManagerId = manager.Id;

            mappedOrgUnit.ImageAddress = manager.UserId != null ? GetImageAddress(manager.UserId.Value) : null;

            return mappedOrgUnit;
        }

        private static string GetImageAddress(long userId)
        {
            return new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext)
                .RouteUrl("UserPictureDownload", new RouteValueDictionary
                {
                    { "userId", userId},
                    { "userPictureSize", UserPictureSize.Normal}
                });
        }

        private static string GetManagerTitle(IDictionary<long, JobTitle> jobTitles, IDictionary<long, JobProfile> jobProfiles, Employee manager)
        {
            if (manager.JobTitle != null)
            {
                var jobTitleName = new LocalizedString
                {
                    English = manager.JobTitle.NameEn,
                    Polish = manager.JobTitle.NamePl
                };

                return jobTitleName.ToString();
            }

            var titleList = manager.JobProfiles.Select(jp => jobProfiles[jp.Id]).Select(jp => jp.Name).ToList();

            if (manager.JobTitleId != null)
            {
                var jobTitle = jobTitles[(long)manager.JobTitleId];

                var jobTitleName = new LocalizedString
                {
                    English = jobTitle.NameEn,
                    Polish = jobTitle.NamePl
                };

                titleList.Add(jobTitleName.ToString());
            }

            return string.Join(", ", titleList);
        }

        public OrgChartEmployeeDto GetEmployeeChildren(long lineManagerId, long? orgUnitId, bool includeOrgUnitManagers = false)
        {
            var orgUnits = _orgUnitService.GetAllOrgUnits();
            var employeesToSkip = orgUnits.Where(o => o.EmployeeId.HasValue).Select(o => o.EmployeeId.Value).ToList();
            var employees = GetEmployeesByLineManagerId(lineManagerId)
                .Where(x => includeOrgUnitManagers || !employeesToSkip.Contains(x.Id))
                .Where(e => !(e.LineManagerId != lineManagerId && orgUnits.Any(o => (!orgUnitId.HasValue || e.OrgUnitId != orgUnitId.Value) && e.OrgUnitId == o.Id && o.EmployeeId.HasValue && o.EmployeeId.Value == lineManagerId)))
                .ToList();

            var jobProfiles = GetAllJobProfiles().ToDictionary(jp => jp.Id);
            var jobTitles = GetAllJobTitles().ToDictionary(jt => jt.Id);

            var result = new OrgChartEmployeeDto()
            {
                Children = employees.Select(x => GenerateEmployeeItem(x, employees.Count, jobProfiles, jobTitles)).ToList()
            };

            return result;
        }

        private OrgChartEmployeeItemDto GenerateEmployeeItem(Employee employee, int employeeCount, IDictionary<long, JobProfile> jobProfiles, IDictionary<long, JobTitle> jobTitles)
        {
            var children = GetEmployeesByLineManagerId(employee.Id);

            return new OrgChartEmployeeItemDto()
            {
                Id = $"e{employee.Id}",
                Relationship =
                    $"1{(employeeCount > 1 ? 1 : 0)}{(children.Any() ? 1 : 0)}",
                Root = new OrgChartEmployeeRootDto()
                {
                    LongId = employee.Id,
                    Id = $"e{employee.Id}",
                    ImageAddress = employee.UserId != null ? GetImageAddress(employee.UserId.Value) : null,
                    ManagerName = $"{employee.FirstName} {employee.LastName}",
                    ManagerTitle = GetManagerTitle(jobTitles, jobProfiles, employee),
                    LocationName = employee.Location.Name
                }
            };
        }

        private List<JobProfile> GetAllJobProfiles()
        {
            using (var unitOfWork = _skillManagementUnitOfWorkService.Create())
            {
                return unitOfWork.Repositories.JobProfiles.ToList();
            }
        }

        private List<JobTitle> GetAllJobTitles()
        {
            using (var unitOfWork = _skillManagementUnitOfWorkService.Create())
            {
                return unitOfWork.Repositories.JobTitles.ToList();
            }
        }

        private List<Employee> GetEmployeesById(IEnumerable<long> ids)
        {
            return GetEmployees(e => ids.Contains(e.Id));
        }

        private List<Employee> GetEmployeesByLineManagerId(long lineManagerId)
        {
            return GetEmployees(e => e.LineManagerId == lineManagerId);
        }

        private List<Employee> GetEmployees(Expression<Func<Employee, bool>> predicate)
        {
            using (var unitOfWork = _allocationUnitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Employees
                    .Where(e => e.CurrentEmployeeStatus != EmployeeStatus.Terminated)
                    .Where(predicate)
                    .Include(e => e.Location)
                    .Include(e => e.JobTitle)
                    .Include(e => e.JobProfiles)
                    .ToList();
            }
        }
    }
}
