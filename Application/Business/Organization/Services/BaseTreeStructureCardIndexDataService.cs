﻿using System;
using System.Linq;
using Castle.Core.Internal;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Organization.Resources;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.Organization.Services
{
    public class BaseTreeStructureCardIndexDataService<TTreeStructureDto, TTreeNodeEntity, TDbScope> :
        CardIndexDataService<TTreeStructureDto, TTreeNodeEntity, TDbScope>, ITreeCardIndexDataService
        where TTreeNodeEntity : class, ITreeNodeEntity
        where TTreeStructureDto : class, ITreeNodeDto
        where TDbScope : IDbScope
    {
        protected IRemoveTreeNodeStategy RemoveTreeNodeStrategy { get; set; }
        protected ITreeStructureService<TDbScope> TreeStructureService { get; set; }

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public BaseTreeStructureCardIndexDataService(ICardIndexServiceDependencies<TDbScope> dependencies,
            ITreeStructureService<TDbScope> treeStructureService,
            IRemoveTreeNodeStategy removeTreeNodeStrategy)
            : base(dependencies)
        {
            TreeStructureService = treeStructureService;
            RemoveTreeNodeStrategy = removeTreeNodeStrategy;
        }

        protected override IOrderedQueryable<TTreeNodeEntity> GetDefaultOrderBy(IQueryable<TTreeNodeEntity> records, QueryCriteria criteria)
        {
            return records.OrderBy(r => r.NameSortOrder);
        }

        protected override void OnRecordAdding(
            RecordAddingEventArgs<TTreeNodeEntity, TTreeStructureDto, TDbScope> eventArgs)
        {
            base.OnRecordAdding(eventArgs);
            eventArgs.InputEntity.Path = Guid.NewGuid().ToString();
            eventArgs.InputEntity.DescendantCount = 0;
            eventArgs.InputEntity.NameSortOrder = 0;
            ValidateTreeStructure(eventArgs.InputEntity, (IExtendedUnitOfWork<TDbScope>)eventArgs.UnitOfWork, eventArgs);
        }

        protected override void OnRecordEditing(
            RecordEditingEventArgs<TTreeNodeEntity, TTreeStructureDto, TDbScope> eventArgs)
        {
            base.OnRecordEditing(eventArgs);

            if (eventArgs.InputEntity.Path.IsNullOrEmpty())
            {
                eventArgs.InputEntity.Path = Guid.NewGuid().ToString();
            }

            ValidateTreeStructure(eventArgs.InputEntity, (IExtendedUnitOfWork<TDbScope>)eventArgs.UnitOfWork, eventArgs);
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<TTreeNodeEntity, TDbScope> eventArgs)
        {
            base.OnRecordDeleting(eventArgs);

            var repositoryFactory = ((IRepositoryFactory)eventArgs.UnitOfWork.Repositories);
            var repository = repositoryFactory.Create<TTreeNodeEntity>();

            RemoveTreeNodeStrategy.Execute(repository, eventArgs.DeletingRecord.Id);
        }

        protected void ValidateTreeStructure(TTreeNodeEntity orgUnit, IExtendedUnitOfWork<TDbScope> unitOfWork, CardIndexEventArgs eventArgs)
        {
            if (orgUnit.Id == 0 || orgUnit.ParentId == null)
            {
                return;
            }

            if (orgUnit.Id == orgUnit.ParentId)
            {
                eventArgs.AddError(TreeStructureCardIndexDataServiceResources.ParentIdRecursionMessage, false);
                return;
            }

            if (TreeStructureService.IsDescendantOf<OrgUnit>(orgUnit.Id, orgUnit.ParentId.Value, unitOfWork))
            {
                eventArgs.AddError(TreeStructureCardIndexDataServiceResources.ParentIdRecursionMessage, false);
            }
        }

        public override IQueryable<TTreeNodeEntity> ApplyContextFiltering(IQueryable<TTreeNodeEntity> records, object context)
        {
            if (IsCollapsibleTreeMode)
            {
                var parentIdContext = (ParentIdContext)context;

                if (parentIdContext != null)
                {
                    return records.Where(r => r.ParentId == parentIdContext.ParentId);
                }

                return records.Where(r => !r.ParentId.HasValue);
            }

            return base.ApplyContextFiltering(records, context);
        }

        /// <summary>
        /// Is a tree renderer in collapsible mode
        /// </summary>
        public bool IsCollapsibleTreeMode { get; set; }

        protected override QueryCriteria AdjustPageSize(QueryCriteria criteria)
        {
            criteria = base.AdjustPageSize(criteria);

            if (IsCollapsibleTreeMode)
            {
                criteria.PageSize = null;
            }

            return criteria;
        }
    }
}