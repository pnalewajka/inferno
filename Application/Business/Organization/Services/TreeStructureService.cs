﻿using System;
using System.Data;
using System.Data.SqlClient;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.Organization.Services
{
    public class TreeStructureService<TDbScope> : ITreeStructureService<TDbScope>
        where TDbScope : IDbScope
    {
        private IUnitOfWorkService<TDbScope> _unitOfWorkService;

        public TreeStructureService(IUnitOfWorkService<TDbScope> unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        public void RefreshTreeStructure<TEntity>()
            where TEntity : class, IEntity
        {
            const string sqlCommand =
                @"EXEC [Runtime].[atomic_RefreshTreeStructure] @tableName, @schemaName";

            using (var unitOfWork = _unitOfWorkService.CreateExtended())
            {
                unitOfWork.ExecuteSqlCommand
                    (
                        sqlCommand,
                        new SqlParameter("@tableName", EntityHelper.GetTableName<TEntity>()),
                        new SqlParameter("@schemaName", EntityHelper.GetTableSchema<TEntity>())
                    );
                unitOfWork.Commit();
            }
        }

        public bool IsDescendantOf<TEntity>(long treeNodeId, long possibleDescendantId)
            where TEntity : class, IEntity
        {
            using (var unitOfWork = _unitOfWorkService.CreateExtended())
            {
                return IsDescendantOf<TEntity>(treeNodeId, possibleDescendantId, unitOfWork);
            }
        }

        public bool IsDescendantOf<TEntity>(long treeNodeId, long possibleDescendantId, IExtendedUnitOfWork<TDbScope> unitOfWork)
            where TEntity : class, IEntity
        {
            const string sqlCommand =
                @"EXEC [Runtime].[atomic_IsDescendantOf] @tableName, @schemaName,@treeNodeId,@possibleDescenantTreeNodeId,@res output";
            var outParam = new SqlParameter
            {
                ParameterName = "@res",
                SqlDbType = SqlDbType.Bit,
                Direction = ParameterDirection.Output
            };

            unitOfWork.ExecuteSqlCommand
                (
                    sqlCommand,
                    new SqlParameter("@tableName", EntityHelper.GetTableName<TEntity>()),
                    new SqlParameter("@schemaName", EntityHelper.GetTableSchema<TEntity>()),
                    new SqlParameter("@treeNodeId", treeNodeId),
                    new SqlParameter("@possibleDescenantTreeNodeId", possibleDescendantId),
                    outParam
                );

            return Convert.ToBoolean(outParam.Value);
        }
    }
}