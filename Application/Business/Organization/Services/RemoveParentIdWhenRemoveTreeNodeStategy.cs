﻿using Castle.Core.Internal;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using System.Linq;

namespace Smt.Atomic.Business.Organization.Services
{
    public class RemoveParentIdWhenRemoveTreeNodeStategy : IRemoveTreeNodeStategy
    {
        public void Execute<TTreeNodeEntity>(IRepository<TTreeNodeEntity> repository, long nodeId)
            where TTreeNodeEntity : class, ITreeNodeEntity
        {
            var itemsToBeModified = repository.Where(i => i.ParentId == nodeId);
            itemsToBeModified.ForEach(i =>
            {
                i.ParentId = null;
                repository.Edit(i);
            });
        }
    }
}
