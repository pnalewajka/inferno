﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Organization.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Scopes;
using SearchAreaCodes = Smt.Atomic.Business.Organization.Consts.SearchAreaCodes;

namespace Smt.Atomic.Business.Organization.Services
{
    public class TechnicalInterviewerCardIndexDataService : CardIndexDataService<TechnicalInterviewerDto, TechnicalInterviewer, IOrganizationScope>, ITechnicalInterviewerCardIndexDataService
    {
        private readonly ICardIndexServiceDependencies<IOrganizationScope> _dependencies;
        private readonly IUserPermissionService _permissionService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public TechnicalInterviewerCardIndexDataService(ICardIndexServiceDependencies<IOrganizationScope> dependencies, IUserPermissionService permissionService)
            : base(dependencies)
        {
            _dependencies = dependencies;
            _permissionService = permissionService;
        }

        protected override NamedFilters<TechnicalInterviewer> NamedFilters
        {
            get
            {
                return new NamedFilters<TechnicalInterviewer>(new[]
                {
                    new NamedFilter<TechnicalInterviewer, long[]>(OrganizationFilterCodes.SkillTagFilter,
                        (e, skillTagIds) =>
                            (!skillTagIds.Any() || e.SkillTags.Any(s => skillTagIds.Contains(s.Id)))
                        ),

                    new NamedFilter<TechnicalInterviewer, long[]>(OrganizationFilterCodes.LocationFilter,
                        (e, locationIds) => (e.Employee.LocationId.HasValue && locationIds.Contains(e.Employee.LocationId.Value))
                        )
                });
            }
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<TechnicalInterviewer, TechnicalInterviewerDto> recordAddedEventArgs)
        {
            using (var unitOfWork = _dependencies.UnitOfWorkService.Create())
            {
                if (unitOfWork.Repositories.TechnicalInterviewers.Count(t => t.EmployeeId == recordAddedEventArgs.InputEntity.EmployeeId) == 1)
                {
                    var profileName = _dependencies.SystemParameters.GetParameter<string>(ParameterKeys.RecruitmentTechnicalInterviewerProfileName);

                    _permissionService.AssignSecurityProfilesToUser(
                        unitOfWork.Repositories.Employees.SelectById(recordAddedEventArgs.InputEntity.EmployeeId, e => e.UserId.Value),
                        new[]
                        {
                            unitOfWork.Repositories.SecurityProfiles.Single(p => p.Name == profileName).Id
                        });
                }
            }

            base.OnRecordAdded(recordAddedEventArgs);
        }

        protected override void OnRecordsDeleted(RecordDeletedEventArgs<TechnicalInterviewer> eventArgs)
        {
            using (var unitOfWork = _dependencies.UnitOfWorkService.Create())
            {
                var employeeIds = eventArgs.DeletedEntities.DistinctBy(e => e.EmployeeId).Select(e => e.EmployeeId).ToList();

                var employeeIdsWithInterviewers = unitOfWork.Repositories.TechnicalInterviewers
                    .Where(t => employeeIds.Contains(t.EmployeeId)).Select(i => i.EmployeeId)
                    .ToList();

                var employeeIdsWithoutInterviewer = employeeIds.Except(employeeIdsWithInterviewers);

                var profileName = _dependencies.SystemParameters.GetParameter<string>(ParameterKeys.RecruitmentTechnicalInterviewerProfileName);
                var technicalInterviewerProfileId = unitOfWork.Repositories.SecurityProfiles.Single(p => p.Name == profileName).Id;

                foreach (var employeeId in employeeIdsWithoutInterviewer)
                {
                    _permissionService.RemoveSecurityProfilesFromUser(
                        unitOfWork.Repositories.Employees.SelectById(employeeId, e => e.UserId.Value),
                        new[] {technicalInterviewerProfileId});
                }
            }

            base.OnRecordsDeleted(eventArgs);
        }

        public override AddRecordResult AddRecord(TechnicalInterviewerDto record, object context = null)
        {
            var alerts = CheckSkillsDuplication(record);

            return alerts.IsNullOrEmpty()
                ? base.AddRecord(record, context)
                : new AddRecordResult(false, 0, null, alerts);
        }
        
        public override EditRecordResult EditRecord(TechnicalInterviewerDto record, object context = null)
        {
            var alerts = CheckSkillsDuplication(record);

            return alerts.IsNullOrEmpty()
                ? base.EditRecord(record, context)
                : new EditRecordResult(false, 0, null, alerts);
        }

        private List<AlertDto> CheckSkillsDuplication(TechnicalInterviewerDto technicalInterviewer)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var duplicatedSkills = unitOfWork.Repositories.TechnicalInterviewers
                    .Where(t => t.EmployeeId == technicalInterviewer.EmployeeId && t.Id != technicalInterviewer.Id)
                    .SelectMany(x => x.SkillTags)
                    .Where(e => technicalInterviewer.SkillTagIds.Contains(e.Id)).Distinct().ToList();


                if (duplicatedSkills.IsNullOrEmpty())
                {
                    return null;
                }

                var skillNames = duplicatedSkills.Count > 1
                    ? string.Join(", ", duplicatedSkills.Select(x => x.Name))
                    : duplicatedSkills.First().Name;

                return new List<AlertDto>
                {
                    AlertDto.CreateError(string.Format(TechnicaInterviewerCardIndexDataServiceResource.DuplicatedSkillsError, skillNames))
                };
            }
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<TechnicalInterviewer, TechnicalInterviewerDto, IOrganizationScope> eventArgs)
        {
            eventArgs.InputEntity.SkillTags = EntityMergingService.CreateEntitiesFromIds<SkillTag, IOrganizationScope>(eventArgs.UnitOfWork, eventArgs.InputEntity.SkillTags.GetIds());
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<TechnicalInterviewer, TechnicalInterviewerDto, IOrganizationScope> eventArgs)
        {
            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.SkillTags, eventArgs.InputEntity.SkillTags);
            var employee = eventArgs.UnitOfWork.Repositories.Employees.GetByIdOrDefault(eventArgs.InputEntity.EmployeeId);

            if (employee == null)
            {
                return;
            }

            employee.SkypeName = eventArgs.InputDto.SkypeName;
            employee.AvailabilityDetails = eventArgs.InputDto.AvailabilityDetails;
        }

        protected override IOrderedQueryable<TechnicalInterviewer> GetDefaultOrderBy(IQueryable<TechnicalInterviewer> records, QueryCriteria criteria)
        {
            return records.OrderBy(p => p.Employee.Location.Name);
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<TechnicalInterviewer> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            switch (sortingCriterion.GetSortingColumnName())
            {
                case nameof(TechnicalInterviewerDto.SkillTagIds):
                    var direction = sortingCriterion.GetSortingDirection();
                    var getOrderByExpression = LocalizedStringHelper.GetOrderByExpressionForSpecificCulture<LocalizedString, TechnicalInterviewer>(r => r.SkillTags.FirstOrDefault().Name);

                    orderedQueryBuilder.ApplySortingKey<string>(getOrderByExpression, direction);
                    break;

                default:
                    base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
                    break;
            }
        }

        protected override IEnumerable<Expression<Func<TechnicalInterviewer, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            if (searchCriteria.Includes(SearchAreaCodes.EmployeeName))
            {
                yield return i => i.Employee.FirstName;
                yield return i => i.Employee.LastName;
            }

            if (searchCriteria.Includes(SearchAreaCodes.SkypeName))
            {
                yield return i => i.Employee.SkypeName;
            }

            if (searchCriteria.Includes(SearchAreaCodes.SkillTag))
            {
                yield return i => i.SkillTags.Select(t => t.NameEn);
                yield return i => i.SkillTags.Select(t => t.NamePl);
            }

            yield return i => i.Comments;
        }

        protected override IQueryable<TechnicalInterviewer> ApplyBaseFilter(IQueryable<TechnicalInterviewer> records)
        {
            return records.Where(t => t.Employee.CurrentEmployeeStatus != EmployeeStatus.Terminated && t.Employee.CurrentEmployeeStatus != EmployeeStatus.Inactive);
        }
    }
}
