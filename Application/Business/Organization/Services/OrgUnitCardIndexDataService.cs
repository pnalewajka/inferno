﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Business.Extensions;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Organization.Services
{
    public class OrgUnitCardIndexDataService
        : TreeStructureCardIndexDataService<OrgUnitDto, OrgUnit, IOrganizationScope>,
          IOrgUnitCardIndexDataService
    {
        private readonly ISystemParameterService _systemParameterService;
        private readonly IClassMapping<OrgUnit, OrgUnitDto> _classMappingOrgUnit;

        public OrgUnitCardIndexDataService(
            ISystemParameterService systemParameterService,
            ICardIndexServiceDependencies<IOrganizationScope> dependencies,
            IClassMapping<OrgUnit, OrgUnitDto> classMappingOrgUnit,
            ITreeStructureService<IOrganizationScope> treeStructureService,
            IRemoveTreeNodeStategy removeTreeNodeStrategry)
            : base(dependencies, treeStructureService, removeTreeNodeStrategry)
        {
            _classMappingOrgUnit = classMappingOrgUnit;
            _systemParameterService = systemParameterService;

            RelatedCacheAreas = new[] { CacheAreas.OrgUnits, CacheAreas.ManagerOrgUnitIds };
        }

        protected override IEnumerable<Expression<Func<OrgUnit, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            if (searchCriteria.Includes(OrganizationFilterCodes.Name))
            {
                yield return o => o.Name;
            }

            if (searchCriteria.Includes(OrganizationFilterCodes.Description))
            {
                yield return o => o.Description;
            }

            if (searchCriteria.Includes(OrganizationFilterCodes.Code))
            {
                yield return o => o.Code;
            }

            if (searchCriteria.Includes(OrganizationFilterCodes.FlatName))
            {
                yield return o => o.FlatName;
            }

            if (searchCriteria.Includes(OrganizationFilterCodes.Employee))
            {
                yield return o => o.Employee.FirstName;
                yield return o => o.Employee.LastName;
            }
        }

        protected override NamedFilters<OrgUnit> NamedFilters
        {
            get
            {
                var defaultOrgUnitCode = _systemParameterService.GetParameter<string>(ParameterKeys.OrganizationDefaultOrgUnitCode);

                return new NamedFilters<OrgUnit>(new[]
                {
                    new NamedFilter<OrgUnit>(OrganizationFilterCodes.HideDefault, a => a.Code != defaultOrgUnitCode),
                });
            }
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<OrgUnit, OrgUnitDto, IOrganizationScope> eventArgs)
        {
            base.OnRecordAdding(eventArgs);
            UpdateIsDepartmentOnOrgUnits(eventArgs.InputDto, eventArgs.UnitOfWork);
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<OrgUnit, OrgUnitDto, IOrganizationScope> eventArgs)
        {
            base.OnRecordEditing(eventArgs);
            UpdateIsDepartmentOnOrgUnits(eventArgs.InputDto, eventArgs.UnitOfWork);
        }

        private void UpdateIsDepartmentOnOrgUnits(OrgUnitDto orgUnitDto, IUnitOfWork<IOrganizationScope> unitOfWork)
        {
            if (orgUnitDto.OrgUnitFeatures.Has(OrgUnitFeatures.IsDepartment))
            {
                var parentId = orgUnitDto.ParentId;

                while (parentId.HasValue)
                {
                    var parentUnit = unitOfWork.Repositories.OrgUnits.Single(u => u.Id == parentId);

                    if (!parentUnit.OrgUnitFeatures.Has(OrgUnitFeatures.IsDepartment))
                    {
                        parentUnit.OrgUnitFeatures = parentUnit.OrgUnitFeatures.Add(OrgUnitFeatures.IsDepartment);
                        unitOfWork.Repositories.OrgUnits.Edit(parentUnit);
                    }
                    parentId = parentUnit.ParentId;
                }
            }
            else
            {
                var childUnits = unitOfWork.Repositories.OrgUnits.Where(
                    o => o.Path.StartsWith(orgUnitDto.Path));

                foreach (var childUnit in childUnits)
                {
                    if (childUnit.OrgUnitFeatures.Has(OrgUnitFeatures.IsDepartment) && childUnit.Id != orgUnitDto.Id)
                    {
                        childUnit.OrgUnitFeatures = childUnit.OrgUnitFeatures.Add(OrgUnitFeatures.IsDepartment);
                        unitOfWork.Repositories.OrgUnits.Edit(childUnit);
                    }
                }
            }

            unitOfWork.Commit();
        }

        public OrgUnitDto TopChartOrgUnit(OrgUnitFeatures features)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var defaultOrgUnit = unitOfWork.Repositories.OrgUnits
                    .OrderBy(o => o.NameSortOrder)
                    .FirstOrDefault(o => o.OrgUnitFeatures.HasFlag(features));

                return defaultOrgUnit == null ? null : _classMappingOrgUnit.CreateFromSource(defaultOrgUnit);
            }
        }

        public IReadOnlyCollection<OrgUnitDto> ChartOrgUnits(OrgUnitFeatures features)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var units = unitOfWork.Repositories.OrgUnits
                    .OrderBy(ou => ou.NameSortOrder)
                    .Where(ou => ou.OrgUnitFeatures.HasFlag(features));

                return units.Select(_classMappingOrgUnit.CreateFromSource).ToList();
            }
        }

        public OrgUnitRelationConstraintsDto ValidateRelationConstraints(IList<long> ids)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var nodesToRemove = unitOfWork.Repositories.OrgUnits.GetByIds(ids);
                var childNodesQuery = unitOfWork.Repositories.OrgUnits.AsQueryable();

                foreach (var node in nodesToRemove)
                {
                    childNodesQuery = childNodesQuery.Where(ou => ou.Path.StartsWith(node.Path) && ou.Id != node.Id);
                }

                var childNodeIds = childNodesQuery.Select(ou => ou.Id).Union(ids).ToList();

                var employees = unitOfWork.Repositories.Employees.Where(e => childNodeIds.Contains(e.OrgUnitId)).ToList();
                var employeesPerOrgUnit = employees.Select(e => e.OrgUnit).Distinct().ToDictionary(ou => ou.FlatName, v => employees.Where(e => e.OrgUnit == v).Select(e => e.FullName));

                var projects = unitOfWork.Repositories.Projects.Where(p => childNodeIds.Contains(p.ProjectSetup.OrgUnitId)).ToList();
                var projectsPerOrgUnit = projects.Select(p => p.ProjectSetup.OrgUnit).Distinct().ToDictionary(ou => ou.FlatName, ou => projects.Where(p => p.ProjectSetup.OrgUnit == ou).Select(ProjectBusinessLogic.ClientProjectName.GetFunction()));

                var surveyConducts = unitOfWork.Repositories.SurveyConducts.Where(s => s.OrgUnits.Any(ou => childNodeIds.Contains(ou.Id))).ToList();
                var constrainedOrgUnits = surveyConducts.SelectMany(s => s.OrgUnits).Distinct().Where(ou => childNodeIds.Contains(ou.Id));
                var surveyConductsPerOrgUnit = constrainedOrgUnits.ToDictionary(k => k.Code, v => surveyConducts.Where(s => s.OrgUnits.Contains(v)).Select(s => s.SurveyName));

                var result = new OrgUnitRelationConstraintsDto();
                result.Employees = employeesPerOrgUnit;
                result.Projects = projectsPerOrgUnit;
                result.SurveyConducts = surveyConductsPerOrgUnit;

                return result;
            }
        }
    }
}