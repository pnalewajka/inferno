﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core;
using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Business.Extensions;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Organization.Services
{
    [Interceptor(typeof(CacheInterceptor))]
    public class OrgUnitService : IOrgUnitService
    {
        private readonly IUnitOfWorkService<IOrganizationScope> _unitOfWorkOrganizationService;
        private readonly IClassMapping<OrgUnit, OrgUnitDto> _orgUnitToOrgUnitDtoClassMapping;
        private readonly ITimeService _timeService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IOrgUnitListCacheService _orgUnitListCacheService;
        private readonly IPrincipalProvider _principalProvider;

        public OrgUnitService(
            IUnitOfWorkService<IOrganizationScope> unitOfWorkOrganizationService,
            IClassMapping<OrgUnit, OrgUnitDto> orgUnitToOrgUnitDtoClassMapping,
            ITimeService timeService,
            ISystemParameterService systemParameterService,
            IOrgUnitListCacheService orgUnitListCacheService,
            IPrincipalProvider principalProvider)
        {
            _unitOfWorkOrganizationService = unitOfWorkOrganizationService;
            _timeService = timeService;
            _systemParameterService = systemParameterService;
            _orgUnitListCacheService = orgUnitListCacheService;
            _principalProvider = principalProvider;
            _orgUnitToOrgUnitDtoClassMapping = orgUnitToOrgUnitDtoClassMapping;
        }

        public string RootOrgUnitCode
        {
            get
            {
                return _systemParameterService.GetParameter<string>(ParameterKeys.OrganizationRootUnitCode);
            }
        }

        public IEnumerable<OrgUnitDto> GetAllOrgUnits()
        {
            return _orgUnitListCacheService.GetAllOrgUnits().Values;
        }

        public OrgUnitDto DefaultOrgUnit()
        {
            using (var unitOfWork = _unitOfWorkOrganizationService.Create())
            {
                var currentUserId = _principalProvider.IsSet ? _principalProvider.Current.Id : null;
                if (currentUserId.HasValue)
                {
                    var managerOrgUnit = unitOfWork.Repositories.OrgUnits.OrderBy(ou => ou.NameSortOrder).FirstOrDefault(ou => ou.Employee.UserId == currentUserId);

                    if (managerOrgUnit != null)
                    {
                        return _orgUnitToOrgUnitDtoClassMapping.CreateFromSource(managerOrgUnit);
                    }

                    var employeeOrgUnit =
                        unitOfWork.Repositories.Employees.FirstOrDefault(e => e.OrgUnit != null && e.UserId == currentUserId);

                    if (employeeOrgUnit != null)
                    {
                        return _orgUnitToOrgUnitDtoClassMapping.CreateFromSource(employeeOrgUnit.OrgUnit);
                    }
                }

                var defaultOrgUnitCode = _systemParameterService.GetParameter<string>(ParameterKeys.OrganizationDefaultOrgUnitCode);
                var orgUnitEntity = unitOfWork.Repositories.OrgUnits.Single(ou => ou.Code == defaultOrgUnitCode);

                return _orgUnitToOrgUnitDtoClassMapping.CreateFromSource(orgUnitEntity);
            }
        }

        [Cached(CacheArea = CacheAreas.ManagerOrgUnitIds)]
        public IList<long> GetManagerOrgUnitDescendantIds(long employeeId)
        {
            var allManagerOrgUnits = new List<long>();

            using (var unitOfWork = _unitOfWorkOrganizationService.Create())
            {
                var parentOrgUnitsIds = unitOfWork.Repositories.OrgUnits.Where(o => o.EmployeeId == employeeId).Select(o => o.Id).ToList();
                allManagerOrgUnits.AddRange(parentOrgUnitsIds);

                foreach (var parentId in parentOrgUnitsIds)
                {
                    allManagerOrgUnits.AddRange(GetDescendantOrgUnitsIds(parentId));
                }
            }

            return allManagerOrgUnits;
        }

        [Cached(CacheArea = CacheAreas.ManagerOrgUnitIds)]
        public IList<long> GetManagerIdsOfManagerOrgUnitChildren(long employeeId)
        {
            var managerIds = new List<long>();

            using (var unitOfWork = _unitOfWorkOrganizationService.Create())
            {
                var parentOrgUnitsIds = unitOfWork.Repositories.OrgUnits.Where(o => o.EmployeeId == employeeId).Select(o => o.Id).ToList();

                foreach (var parentId in parentOrgUnitsIds)
                {
                    managerIds.AddRange(
                        unitOfWork.Repositories.OrgUnits.Where(o => o.ParentId == parentId && o.EmployeeId != null).Select(o => o.EmployeeId.Value).Distinct().ToList());
                }
            }

            return managerIds.Distinct().ToList();
        }

        [Cached(CacheArea = CacheAreas.ManagerOrgUnitIds)]
        public IList<long> GetDirectlyManagedOrgUnitIds(long employeeId)
        {
            using (var unitOfWork = _unitOfWorkOrganizationService.Create())
            {
                var managedByEmployeeOrgUnitIds = unitOfWork.Repositories.OrgUnits.Where(o => o.EmployeeId == employeeId).Select(o => o.Id).ToList();

                return managedByEmployeeOrgUnitIds;
            }
        }

        [Cached(CacheArea = CacheAreas.ManagerOrgUnitIds)]
        public IList<long> GetOrgUnitIdsForAllocationByEmployeeId(long employeeId)
        {
            const char pathSeparator = '/';

            using (var unitOfWork = _unitOfWorkOrganizationService.Create())
            {
                var currentDate = _timeService.GetCurrentDate();
                var orgUnitPathsWithAllocation = unitOfWork.Repositories.Employees
                    .Where(e => e.IsProjectContributor && e.AllocationRequests.Any(
                        r => r.StartDate <= currentDate && (!r.EndDate.HasValue || currentDate <= r.EndDate.Value)))
                    .Select(e => e.OrgUnit.Path)
                    .Distinct()
                    .ToList();

                var resultingOrgUnitPaths = unitOfWork.Repositories.OrgUnits
                    .Where(o => o.EmployeeId == employeeId)
                    .Select(o => o.Path)
                    .AsEnumerable()
                    .Select(p => orgUnitPathsWithAllocation
                        .Where(a => p.StartsWith(a))
                        .OrderByDescending(a => a.Count(c => c == pathSeparator))
                        .FirstOrDefault())
                    .Where(p => p != null)
                    .ToList();

                return unitOfWork.Repositories.OrgUnits
                    .Where(o => resultingOrgUnitPaths.Any(p => o.Path.StartsWith(p)))
                    .Select(o => o.Id)
                    .ToList();
            }
        }

        public IList<long> GetDescendantOrgUnitsIds(long parentOrgUnitId)
        {
            List<long> descendantOrgUnits = new List<long>();

            using (var unitOfWork = _unitOfWorkOrganizationService.Create())
            {
                var orgUnit = unitOfWork.Repositories.OrgUnits.FirstOrDefault(o => o.Id == parentOrgUnitId);

                if (orgUnit != null)
                {
                    descendantOrgUnits = unitOfWork.Repositories.OrgUnits.Where(
                                    o => o.Path.StartsWith(orgUnit.Path) && o.Id != parentOrgUnitId).Select(o => o.Id).ToList();
                }
            }

            return descendantOrgUnits;
        }

        public OrgUnitDto GetOrgUnit(string flatName)
        {
            using (var unitOfWork = _unitOfWorkOrganizationService.Create())
            {
                OrgUnit orgUnit = unitOfWork.Repositories.OrgUnits.AsNoTracking().SingleOrDefault(o => o.FlatName == flatName);

                if (orgUnit == null)
                {
                    return null;
                }

                return _orgUnitToOrgUnitDtoClassMapping.CreateFromSource(orgUnit);
            }
        }

        public OrgUnitDto GetOrgUnitByCode(string code)
        {
            using (var unitOfWork = _unitOfWorkOrganizationService.Create())
            {
                OrgUnit orgUnit = unitOfWork.Repositories.OrgUnits.AsNoTracking().SingleOrDefault(o => o.Code == code);

                if (orgUnit == null)
                {
                    return null;
                }

                return _orgUnitToOrgUnitDtoClassMapping.CreateFromSource(orgUnit);
            }
        }

        public OrgUnitDto GetOrgUnitWithDefaultTimeTrackingMode(long orgUnitId)
        {
            var orgUnits = _orgUnitListCacheService.GetAllOrgUnits();
            var orgUnit = orgUnits[orgUnitId];

            while (!orgUnit.TimeReportingMode.HasValue)
            {
                if (!orgUnit.ParentId.HasValue)
                {
                    const string message = "Root OrgUnit has TimeReportingMode value set to inherit.";

                    throw new InvalidOperationException(message);
                }

                orgUnit = orgUnits[orgUnit.ParentId.Value];
            }

            return orgUnit;
        }

        public bool GetDefaultProjectContributorForOrgUnit(long orgUnitId)
        {
            var orgUnits = _orgUnitListCacheService.GetAllOrgUnits();
            var orgUnit = orgUnits[orgUnitId];

            while (orgUnit.DefaultProjectContributionMode == InheritableBool.Inherited)
            {
                if (!orgUnit.ParentId.HasValue)
                {
                    const string message = "Root OrgUnit has DefaultProjectContributionMode value set to inherit.";

                    throw new InvalidOperationException(message);
                }

                orgUnit = orgUnits[orgUnit.ParentId.Value];
            }

            return orgUnit.DefaultProjectContributionMode.ToBool();
        }

        public long? GetOwnedOrgUnit(long employeeId)
        {
            using (var unitOfWork = _unitOfWorkOrganizationService.Create())
            {
                return unitOfWork.Repositories.OrgUnits.AsNoTracking().FirstOrDefault(o => o.EmployeeId == employeeId)?.Id;
            }
        }

        public long? GetOrgUnitManagerId(long id)
        {
            using (var unitOfWork = _unitOfWorkOrganizationService.Create())
            {
                return unitOfWork.Repositories.OrgUnits.Where(x => x.Id == id).Select(x => x.EmployeeId).SingleOrDefault();
            }
        }

        public IEnumerable<OrgUnitDto> GetOrgUnitParents(long orgUnitId)
        {
            var allOrgUnits = _orgUnitListCacheService.GetAllOrgUnits();

            var orgUnit = allOrgUnits[orgUnitId];

            while (orgUnit.ParentId != null)
            {
                yield return orgUnit;
                orgUnit = allOrgUnits[orgUnit.ParentId.Value];
            }
        }

        public OrgUnitTreeNodeDto GetOrgUnitTree(long? rootOrgUnitId = null)
        {
            var orgUnits = _orgUnitListCacheService.GetAllOrgUnits()
                .ToDictionary(o => o.Key, o => new OrgUnitTreeNodeDto
                {
                    OrgUnit = o.Value,
                    Children = new List<OrgUnitTreeNodeDto>()
                });

            OrgUnitTreeNodeDto root;

            if (rootOrgUnitId.HasValue)
            {
                orgUnits.TryGetValue(rootOrgUnitId.Value, out root);
            }
            else
            {
                var rootOrgUnitCode = RootOrgUnitCode;

                root = orgUnits.FirstOrDefault(o => o.Value.OrgUnit.Code == rootOrgUnitCode).Value;
            }

            if(root == null)
            {
                return null;
            }

            foreach (var pair in orgUnits)
            {
                var parentId = pair.Value.OrgUnit.ParentId;

                if (!parentId.HasValue)
                {
                    continue;
                }

                var node = pair.Value;

                OrgUnitTreeNodeDto parent;

                if (orgUnits.TryGetValue(parentId.Value, out parent))
                {
                    parent.Children.Add(node);
                }
            }

            return root;
        }

        public OrgUnitDto GetOrgUnitOrDefaultById(long id)
        {
            using (var unitOfWork = _unitOfWorkOrganizationService.Create())
            {
                var orgUnit = unitOfWork.Repositories.OrgUnits.GetByIdOrDefault(id);

                return orgUnit != null ? _orgUnitToOrgUnitDtoClassMapping.CreateFromSource(orgUnit) : null;
            }
        }
         
        public IEnumerable<OrgUnitDto> GetOrgUnitsByParent(long parentOrgUnitId)
        {
            var allOrgUnits = _orgUnitListCacheService.GetAllOrgUnits();

            foreach(var orgUnitKeyValuePair in allOrgUnits)
            {
                if (orgUnitKeyValuePair.Value.ParentId == parentOrgUnitId)
                {
                    yield return orgUnitKeyValuePair.Value;
                }
            }
        }

        public IDictionary<long, string> GetOrgUnitNames(IReadOnlyCollection<long> orgUnitIds)
        {
            using (var unitOfWork = _unitOfWorkOrganizationService.Create())
            {
                return unitOfWork.Repositories.OrgUnits.GetByIds(orgUnitIds)
                    .Select(a => new { a.Id, a.FlatName })
                    .ToDictionary(o => o.Id, o => o.FlatName);
            }
        }
    }
}
