﻿using System.Collections.Generic;
using System.Linq;
using Castle.Core;
using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Organization.Services
{
    [Interceptor(typeof(CacheInterceptor))]
    public class OrgUnitListCacheService : IOrgUnitListCacheService
    {
        private readonly IUnitOfWorkService<IOrganizationScope> _unitOfWorkService;
        private readonly IClassMapping<OrgUnit, OrgUnitDto> _orgUnitToOrgUnitDtoClassMapping;

        public OrgUnitListCacheService(
            IUnitOfWorkService<IOrganizationScope> unitOfWorkService,
            IClassMapping<OrgUnit, OrgUnitDto> orgUnitToOrgUnitDtoClassMapping)
        {
            _unitOfWorkService = unitOfWorkService;
            _orgUnitToOrgUnitDtoClassMapping = orgUnitToOrgUnitDtoClassMapping;
        }

        [Cached(CacheArea = CacheAreas.OrgUnits, ExpirationInSeconds = 300)]
        public IDictionary<long, OrgUnitDto> GetAllOrgUnits()
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.OrgUnits
                    .AsEnumerable()
                    .Select(_orgUnitToOrgUnitDtoClassMapping.CreateFromSource)
                    .ToDictionary(u => u.Id, u => u);
            }
        }
    }
}