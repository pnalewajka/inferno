﻿namespace Smt.Atomic.Business.Organization.Consts
{
    public class SearchAreaCodes
    {
        public const string EmployeeName = "name";
        public const string SkypeName = "skype";
        public const string SkillTag = "skill-tag";
    }
}
