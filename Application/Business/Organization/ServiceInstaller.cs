﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Organization.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Organization
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            switch (containerType)
            {
                case ContainerType.WebApp:
                case ContainerType.WebApi:
                    container.Register(Component.For<IOrgUnitCardIndexDataService>().ImplementedBy<OrgUnitCardIndexDataService>().LifestyleTransient());
                    container.Register(Component.For<IOrgUnitImporterCardIndexDataService>().ImplementedBy<OrgUnitImporterCardIndexDataService>().LifestyleTransient());
                    container.Register(Component.For<ITreeStructureService<IOrganizationScope>>().ImplementedBy<TreeStructureService<IOrganizationScope>>().LifestyleTransient());
                    container.Register(Component.For<IOrgUnitService>().ImplementedBy<OrgUnitService>().LifestyleTransient());
                    container.Register(Component.For<IOrgUnitListCacheService>().ImplementedBy<OrgUnitListCacheService>().LifestyleTransient());
                    container.Register(Component.For<IPricingEngineerCardIndexDataService>().ImplementedBy<PricingEngineerCardIndexDataService>().LifestyleTransient());
                    container.Register(Component.For<ITechnicalInterviewerCardIndexDataService>().ImplementedBy<TechnicalInterviewerCardIndexDataService>().LifestyleTransient());
                    container.Register(Component.For<IRemoveTreeNodeStategy>().ImplementedBy<RemoveParentIdWhenRemoveTreeNodeStategy>().LifestyleTransient());
                    container.Register(Component.For<RemoveChildrenWhenRemoveTreeNodeStrategy>().LifestyleTransient());
                    container.Register(Component.For<IOrgChartService>().ImplementedBy<OrgChartService>().LifestyleTransient());
                    break;
                case ContainerType.WebServices:
                    break;
                case ContainerType.JobScheduler:
                    container.Register(Component.For<IOrgUnitService>().ImplementedBy<OrgUnitService>().LifestyleTransient());
                    container.Register(Component.For<IOrgUnitListCacheService>().ImplementedBy<OrgUnitListCacheService>().LifestyleTransient());
                    break;
                case ContainerType.UnitTests:
                    break;
            }
        }
    }
}

