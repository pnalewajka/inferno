﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Organization.ReportModels;
using Smt.Atomic.Business.Organization.Resources;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Organization.ReportDataSources
{
    [Identifier("DataSource.OrgUnitsReportDataSource")]
    [RequireRole(SecurityRoleType.CanGenerateOrgUnitsReport)]
    [DefaultReportDefinition(
        "OrgUnitsReport",
        nameof(ReportsResources.OrgUnitsReportName),
        nameof(ReportsResources.OrgUnitsReportDescription),
        MenuAreas.Organization,
        MenuGroups.OrgUnit,
        ReportingEngine.MsExcel,
        "Smt.Atomic.Business.Organization.ReportTemplates.OrgUnitsReport.xlsx",
        typeof(ReportsResources))]
    public class OrgUnitsReportDataSource : BaseReportDataSource<OrgUnitsReportParametersDto>
    {
        private readonly IOrgUnitService _orgUnitService;
        private readonly IUnitOfWorkService<IAllocationDbScope> _allocationUnitOfWorkService;

        public OrgUnitsReportDataSource(
            IOrgUnitService orgUnitService,
            IUnitOfWorkService<IAllocationDbScope> allocationUnitOfWorkService)
        {
            _orgUnitService = orgUnitService;
            _allocationUnitOfWorkService = allocationUnitOfWorkService;
        }

        public override OrgUnitsReportParametersDto GetDefaultParameters(ReportGenerationContext<OrgUnitsReportParametersDto> reportGenerationContext)
        {
            return new OrgUnitsReportParametersDto();
        }

        public override object GetData(ReportGenerationContext<OrgUnitsReportParametersDto> context)
        {
            var initialOrgUnitNodes = GetOrgUnitNodes(context.Parameters.OrgUnitIds ?? Enumerable.Empty<long>());
            var orgUnitNodes = initialOrgUnitNodes
                .SelectMany(o => o.FlattenTree())
                .ToList();
            var orgUnitNodesById = orgUnitNodes.ToDictionary(o => o.OrgUnit.Id);
            var employeesById = GetOrgUnitManagers(orgUnitNodes);
            var employeesByOrgUnit = GetEmployeesByOrgUnit(orgUnitNodes);

            var models = GetModels(employeesById, initialOrgUnitNodes).ToList();
            var modelsById = models.ToDictionary(m => m.Id);

            foreach(var model in models.OrderByDescending(o => o.Depth))
            {
                var employees = employeesByOrgUnit[model.Id];

                model.ActiveEmployeeCount = employees.Count(e => e.CurrentEmployeeStatus == EmployeeStatus.Active)
                    + models.Where(m => orgUnitNodesById[model.Id].Children.Any(c => c.OrgUnit.Id == m.Id))
                        .Sum(m => m.ActiveEmployeeCount);

                model.InactiveEmployeeCount = employees.Count(e => e.CurrentEmployeeStatus == EmployeeStatus.Inactive)
                    + models.Where(m => orgUnitNodesById[model.Id].Children.Any(c => c.OrgUnit.Id == m.Id))
                        .Sum(m => m.InactiveEmployeeCount);

                model.LeavingEmployeeCount = employees.Count(e => e.CurrentEmployeeStatus == EmployeeStatus.Leave)
                    + models.Where(m => orgUnitNodesById[model.Id].Children.Any(c => c.OrgUnit.Id == m.Id))
                        .Sum(m => m.LeavingEmployeeCount);

                model.NewHireEmployeeCount = employees.Count(e => e.CurrentEmployeeStatus == EmployeeStatus.NewHire)
                    + models.Where(m => orgUnitNodesById[model.Id].Children.Any(c => c.OrgUnit.Id == m.Id))
                        .Sum(m => m.NewHireEmployeeCount);
            }

            return new OrgUnitsReportModel
            {
                OrgUnits = models
            };
        }

        private List<OrgUnitTreeNodeDto> GetOrgUnitNodes(IEnumerable<long> orgUnitIds)
        {
            var orgUnitRootNode = _orgUnitService.GetOrgUnitTree();
            var orgUnitNodes = new List<OrgUnitTreeNodeDto>();

            foreach (var orgUnitId in orgUnitIds.Distinct())
            {
                var orgUnitNode = orgUnitRootNode.FlattenTree()
                    .FirstOrDefault(o => o.OrgUnit.Id == orgUnitId);

                if (orgUnitNode != null)
                {
                    orgUnitNodes.Add(orgUnitNode);
                }
                else
                {
                    orgUnitNode = _orgUnitService.GetOrgUnitTree(orgUnitId);

                    if(orgUnitNode != null)
                    {
                        orgUnitNodes.Add(orgUnitNode);
                    }
                }
            }

            orgUnitNodes = orgUnitNodes.OrderBy(e => e.OrgUnit.Depth).ToList();

            for (int i = orgUnitNodes.Count - 1; i >= 0; i--)
            {
                if (orgUnitNodes.Any(o => o.IsDescendant(orgUnitNodes[i].OrgUnit.Id)))
                {
                    orgUnitNodes.RemoveAt(i);
                }
            }

            if (!orgUnitNodes.Any())
            {
                orgUnitNodes.Add(orgUnitRootNode);
            }

            return orgUnitNodes;
        }

        private Dictionary<long, Employee> GetOrgUnitManagers(IEnumerable<OrgUnitTreeNodeDto> orgUnitNodes)
        {
            var managerIds = orgUnitNodes
                .Select(e => e.OrgUnit.EmployeeId)
                .Where(e => e.HasValue)
                .Select(e => e.Value)
                .Distinct()
                .ToList();

            using (var unitOfWork = _allocationUnitOfWorkService.Create())
            {
               return unitOfWork.Repositories.Employees
                    .GetByIds(managerIds)
                    .ToDictionary(e => e.Id);
            }
        }

        private Dictionary<long, List<Employee>> GetEmployeesByOrgUnit(IEnumerable<OrgUnitTreeNodeDto> orgUnitNodes)
        {
            var orgUnitIds = orgUnitNodes
                .Select(e => e.OrgUnit.Id)
                .ToList();

            var managerIds = orgUnitNodes
                .Select(e => e.OrgUnit.EmployeeId)
                .Where(e => e.HasValue)
                .Select(e => e.Value)
                .Distinct()
                .ToList();

            var employeesByOrgUnitId = orgUnitNodes
                .ToDictionary(o => o.OrgUnit.Id, o => new List<Employee>());

            using (var unitOfWork = _allocationUnitOfWorkService.Create())
            {
                var allEmployees = unitOfWork.Repositories.Employees
                    .Include($"{nameof(Employee.LineManager)}")
                    .Where(e => orgUnitIds.Contains(e.OrgUnitId)
                        || managerIds.Contains(e.Id)
                        || e.LineManagerId.HasValue
                            && (orgUnitIds.Contains(e.LineManager.OrgUnitId)
                                || managerIds.Contains(e.LineManager.Id)))
                    .ToList();

                foreach(var employee in allEmployees)
                {
                    var orgUnitManagedByThisEmployee = orgUnitNodes
                        .FirstOrDefault(o => o.OrgUnit.EmployeeId == employee.Id);

                    if (orgUnitManagedByThisEmployee == null)
                    {
                        List<Employee> employees;
                        if(employeesByOrgUnitId.TryGetValue(employee.OrgUnitId, out employees))
                        {
                            employees.Add(employee);
                        }
                    }
                    else
                    {
                        List<Employee> employees;
                        if (employeesByOrgUnitId.TryGetValue(employee.OrgUnit.Id, out employees))
                        {
                            employees.Add(employee);
                        }
                        else if (employeesByOrgUnitId.TryGetValue(employee.LineManager.OrgUnit.Id, out employees))
                        {
                            employees.Add(employee);
                        }
                    }
                }
            }

            return employeesByOrgUnitId;
        }

        private IEnumerable<OrgUnitReportModel> GetModels(IDictionary<long, Employee> employeesById, IEnumerable<OrgUnitTreeNodeDto> orgUnitNodes, string identation = "")
        {
            foreach(var orgUnitNode in orgUnitNodes)
            {
                Employee manager = null;

                if (orgUnitNode.OrgUnit.EmployeeId.HasValue) {
                    employeesById.TryGetValue(orgUnitNode.OrgUnit.EmployeeId.Value, out manager);
                }

                yield return new OrgUnitReportModel
                {
                    Id = orgUnitNode.OrgUnit.Id,
                    Name = identation + orgUnitNode.OrgUnit.Name,
                    Depth = orgUnitNode.OrgUnit.Depth.Value,
                    LevelCount = orgUnitNode.GetMaxDepth(),
                    ManagerFullName = manager?.FullName
                };

                foreach (var model in GetModels(employeesById, orgUnitNode.Children, identation + new string(' ', 10)))
                {
                    yield return model;
                }
            }
        }
    }
}