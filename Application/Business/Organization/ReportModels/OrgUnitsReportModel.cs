﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Organization.ReportModels
{
    public class OrgUnitsReportModel
    {
        public ICollection<OrgUnitReportModel> OrgUnits { get; set; }
    }
}
