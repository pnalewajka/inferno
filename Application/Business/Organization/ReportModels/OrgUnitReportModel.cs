﻿namespace Smt.Atomic.Business.Organization.ReportModels
{
    public class OrgUnitReportModel
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string ManagerFullName { get; set; }

        public int Depth { get; set; }

        public int LevelCount { get; set; }

        public int ActiveEmployeeCount { get; set; }

        public int LeavingEmployeeCount { get; set; }

        public int InactiveEmployeeCount { get; set; }

        public int NewHireEmployeeCount { get; set; }
    }
}
