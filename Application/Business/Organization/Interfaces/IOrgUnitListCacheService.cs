﻿using System.Collections.Generic;
using Smt.Atomic.Business.Organization.Dto;

namespace Smt.Atomic.Business.Organization.Interfaces
{
    public interface IOrgUnitListCacheService
    {
        IDictionary<long, OrgUnitDto> GetAllOrgUnits();
    }
}