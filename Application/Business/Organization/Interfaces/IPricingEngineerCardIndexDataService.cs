﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Organization.Dto;

namespace Smt.Atomic.Business.Organization.Interfaces
{
    public interface IPricingEngineerCardIndexDataService : ICardIndexDataService<PricingEngineerDto>
    {
    }
}

