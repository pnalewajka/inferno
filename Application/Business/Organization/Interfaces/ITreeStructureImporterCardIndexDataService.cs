﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.Business.Common.Interfaces;

namespace Smt.Atomic.Business.Organization.Interfaces
{
    public interface ITreeStructureImporterCardIndexDataService<TDto> : ICardIndexDataService<TDto>
    {
    }
}