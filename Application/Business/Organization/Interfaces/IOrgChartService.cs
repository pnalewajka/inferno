﻿using Smt.Atomic.Business.Organization.Dto;

namespace Smt.Atomic.Business.Organization.Interfaces
{
    public interface IOrgChartService
    {
        /// <summary>
        /// Generates the OrgUnits tree.
        /// Only with OrgUnits and employees if the OrgUnit have other children OrgUnits
        /// </summary>
        /// <returns>The OrgChart tree</returns>
        OrgChartDto GetOrganizationTree();

        /// <summary>
        /// Generates the OrgChartEmployeeDto with the children of the manager
        /// </summary>
        OrgChartEmployeeDto GetEmployeeChildren(long lineManagerId, long? orgUnitId, bool includeOrgUnitManagers = false);
    }
}
