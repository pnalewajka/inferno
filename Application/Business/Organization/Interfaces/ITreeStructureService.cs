﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.Organization.Interfaces
{
    public interface ITreeStructureService<TDbScope>
        where TDbScope : IDbScope
    {
        void RefreshTreeStructure<TEntity>()
            where TEntity : class, IEntity;

        bool IsDescendantOf<TEntity>(long treeNodeId, long possibleDescenantTreeNodeId, IExtendedUnitOfWork<TDbScope> unitOfWork)
            where TEntity : class, IEntity;

        bool IsDescendantOf<TEntity>(long treeNodeId, long possibleDescenantTreeNodeId)
            where TEntity : class, IEntity;
    }
}