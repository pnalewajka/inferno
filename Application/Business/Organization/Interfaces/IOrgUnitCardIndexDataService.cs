﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Organization.Interfaces
{
    public interface IOrgUnitCardIndexDataService : ICardIndexDataService<OrgUnitDto>
    {
        OrgUnitDto TopChartOrgUnit(OrgUnitFeatures features);

        IReadOnlyCollection<OrgUnitDto> ChartOrgUnits(OrgUnitFeatures features);

        OrgUnitRelationConstraintsDto ValidateRelationConstraints(IList<long> ids);
    }
}