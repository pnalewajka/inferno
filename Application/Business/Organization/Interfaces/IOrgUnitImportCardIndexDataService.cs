﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Organization.Dto;

namespace Smt.Atomic.Business.Organization.Interfaces
{
    public interface IOrgUnitImporterCardIndexDataService : ICardIndexDataService<OrgUnitImportDto>
    {
    }
}