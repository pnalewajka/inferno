﻿namespace Smt.Atomic.Business.Organization.Interfaces
{
    public static class OrganizationFilterCodes
    {
        public const string LocationFilter = "location";
        public const string KeyTechnologyFilter = "key-technology";
        public const string SkillTagFilter = "skill-tag";
        public const string Code = "code";
        public const string Employee = "employee";
        public const string Name = "name";
        public const string Description= "descr";
        public const string FlatName = "flatname";
        public const string HideDefault = "hide-default";
    }
}