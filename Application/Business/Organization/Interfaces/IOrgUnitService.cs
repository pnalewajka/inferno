﻿using System.Collections.Generic;
using Smt.Atomic.Business.Organization.Dto;

namespace Smt.Atomic.Business.Organization.Interfaces
{
    public interface IOrgUnitService
    {
        IEnumerable<OrgUnitDto> GetAllOrgUnits();

        IList<long> GetManagerOrgUnitDescendantIds(long employeeId);

        IList<long> GetDirectlyManagedOrgUnitIds(long employeeId);

        IList<long> GetManagerIdsOfManagerOrgUnitChildren(long employeeId);

        /// <summary>
        /// The method tries to retrieve org units of given manager. If there are no active allocations
        /// for project contributors within given set of org units, parent org units will be taken
        /// into consideration until active allocations are found.
        /// </summary>
        /// <remarks>It is being used to select default org unit filter values for allocation page</remarks>
        /// <param name="employeeId">Employee identifier of manager</param>
        /// <returns>List of org unit identifiers</returns>
        IList<long> GetOrgUnitIdsForAllocationByEmployeeId(long employeeId);

        IList<long> GetDescendantOrgUnitsIds(long parentOrgUnitId);

        OrgUnitDto GetOrgUnit(string flatName);

        OrgUnitDto DefaultOrgUnit();

        OrgUnitDto GetOrgUnitByCode(string code);

        OrgUnitDto GetOrgUnitWithDefaultTimeTrackingMode(long orgUnitId);

        bool GetDefaultProjectContributorForOrgUnit(long orgUnitId);

        long? GetOwnedOrgUnit(long employeeId);

        long? GetOrgUnitManagerId(long id);

        IEnumerable<OrgUnitDto> GetOrgUnitParents(long orgUnitId);

        OrgUnitTreeNodeDto GetOrgUnitTree(long? rootOrgUnitId = null);

        OrgUnitDto GetOrgUnitOrDefaultById(long id);

        IEnumerable<OrgUnitDto> GetOrgUnitsByParent(long parentOrgUnitId);

        IDictionary<long, string> GetOrgUnitNames(IReadOnlyCollection<long> orgUnitIds);
    }
}
