﻿using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.Organization.Interfaces
{
    public interface IRemoveTreeNodeStategy
    {
        void Execute<TTreeNodeEntity>(IRepository<TTreeNodeEntity> repository, long nodeId)
            where TTreeNodeEntity : class, ITreeNodeEntity;
    }
}