using System;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Organization.Dto
{
    public class OrgUnitDto : ITreeNodeDto
    {
        public long Id { get; set; }

        public string Code { get; set; }

        public string Path { get; set; }

        public string Name { get; set; }

        public string FlatName { get; set; }

        public OrgUnitFeatures OrgUnitFeatures { get; set; }

        public long NameSortOrder { get; set; }

        public long DescendantCount { get; set; }

        public string Description { get; set; }

        public long? ParentId { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public TimeReportingMode? TimeReportingMode { get; set; }

        public long? AbsenceOnlyDefaultProjectId { get; set; }

        public InheritableBool DefaultProjectContributionMode { get; set; }

        public byte[] Timestamp { get; set; }

        public int? Depth { get; set; }

        public long? CalendarId { get; set; }

        public long? EmployeeId { get; set; }

        public long? RegionId { get; set; }

        public long? CustomOrder { get; set; }

        public OrgUnitManagerRole? OrgUnitManagerRole { get; set; }
    }
}