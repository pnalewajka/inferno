﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Organization.Dto
{
    public abstract class OrgChartLevelItemBaseDto
    {
        protected OrgChartLevelItemBaseDto()
        {
            Children = new List<OrgChartLevelItemBaseDto>();
        }
        
        public string Relationship { get; set; }
        
        public List<OrgChartLevelItemBaseDto> Children { get; set; }
    }
}