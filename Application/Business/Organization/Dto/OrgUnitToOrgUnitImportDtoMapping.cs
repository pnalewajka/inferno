﻿using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.Organization.Dto
{
    public class OrgUnitToOrgUnitImportDtoMapping : ClassMapping<OrgUnit, OrgUnitImportDto>
    {
        public OrgUnitToOrgUnitImportDtoMapping()
        {
            Mapping = e => new OrgUnitImportDto
            {
                Id = e.Id,
                Code = e.Code,
                Path = e.Path,
                Name = e.Name,
                OrgUnitFeatures = e.OrgUnitFeatures,
                NameSortOrder = e.NameSortOrder,
                DescendantCount = e.DescendantCount,
                Description = e.Description,
                ParentId = e.ParentId,
                ParentCode = e.Parent != null ? e.Parent.Code : null,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp,
                CalendarId = e.CalendarId,
                EmployeeId = e.EmployeeId,
            };
        }
    }
}