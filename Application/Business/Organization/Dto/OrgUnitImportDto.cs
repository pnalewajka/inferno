﻿using Smt.Atomic.Business.Common.Interfaces;

namespace Smt.Atomic.Business.Organization.Dto
{
    public class OrgUnitImportDto : OrgUnitDto, ITreeNodeImportDto
    {
        public string ParentCode { get; set; }
    }
}