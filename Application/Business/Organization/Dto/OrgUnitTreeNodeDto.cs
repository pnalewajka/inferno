﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Smt.Atomic.Business.Organization.Dto
{
    public class OrgUnitTreeNodeDto
    {
        public OrgUnitDto OrgUnit;

        public IList<OrgUnitTreeNodeDto> Children;

        public bool IsDescendant(long orgUnitId)
        {
            return Children.Any(n => n.OrgUnit.Id == orgUnitId)
                || Children.Any(n => n.IsDescendant(orgUnitId));
        }

        public int GetMaxDepth()
        {
            if(Children == null || !Children.Any())
            {
                return 0;
            }

            return Children.Max(c => c.GetMaxDepth()) + 1;
        }

        public IEnumerable<OrgUnitTreeNodeDto> FlattenTree()
        {
            yield return this;

            if (Children != null)
            {
                foreach (var child in Children)
                {
                    var childEnumerable = child.FlattenTree();

                    foreach (var element in childEnumerable)
                    {
                        yield return element;
                    }
                }
            }
        }
    }
}
