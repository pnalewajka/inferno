﻿namespace Smt.Atomic.Business.Organization.Dto
{
    public class OrgUnitChartDto
    {
        public long Id { get; set; }
        
        public string Name { get; set; }
        
        public long? LineManagerId { get; set; }
        
        public string ManagerName { get; set; }
        
        public string ManagerTitle { get; set; }
        
        public string ImageAddress { get; set; }
        
        public string Code { get; set; }
    }
}