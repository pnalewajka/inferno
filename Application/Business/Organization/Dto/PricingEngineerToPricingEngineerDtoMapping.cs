﻿using System.Linq;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.Organization.Dto
{
    public class PricingEngineerToPricingEngineerDtoMapping : ClassMapping<PricingEngineer, PricingEngineerDto>
    {
        public PricingEngineerToPricingEngineerDtoMapping()
        {
            Mapping =
                e =>
                    new PricingEngineerDto
                    {
                        Id = e.Id,
                        ImpersonatedById = e.ImpersonatedById,
                        ModifiedById = e.ModifiedById,
                        ModifiedOn = e.ModifiedOn,
                        Timestamp = e.Timestamp,
                        EmployeeId = e.EmployeeId,
                        SkillTagIds = e.SkillTags.Select(s => s.Id).ToArray(),
                        Location = e.Employee == null ? null : e.Employee.Location.Name,
                        FirstName = e.Employee == null ? null : e.Employee.FirstName,
                        LastName = e.Employee == null ? null : e.Employee.LastName,
                    };
        }
    }
}
