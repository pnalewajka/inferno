﻿using System;
namespace Smt.Atomic.Business.Organization.Dto
{
    public class TechnicalInterviewerDto
    {
        public long Id { get; set; }

        public long[] SkillTagIds { get; set; }

        public long EmployeeId { get; set; }

        public string Location { get; set; }

        public string CompanyName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Acronym { get; set; }

        public int CandidateMaxLevel { get; set; }

        public bool CanInterviewForeignCandidates { get; set; }

        public string Comments { get; set; }

        public string SkypeName { get; set; }

        public string AvailabilityDetails { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
