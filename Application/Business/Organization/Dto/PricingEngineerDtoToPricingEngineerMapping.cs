﻿using System.Collections.ObjectModel;
using System.Linq;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;

namespace Smt.Atomic.Business.Organization.Dto
{
    public class PricingEngineerDtoToPricingEngineerMapping : ClassMapping<PricingEngineerDto, PricingEngineer>
    {
        public PricingEngineerDtoToPricingEngineerMapping()
        {
            Mapping = d => new PricingEngineer
            {
                Id = d.Id,
                Timestamp = d.Timestamp,
                EmployeeId = d.EmployeeId,
                SkillTags = d.SkillTagIds != null ? d.SkillTagIds.Select(id => new SkillTag { Id = id }).ToList() : null
            };
        }
    }
}
