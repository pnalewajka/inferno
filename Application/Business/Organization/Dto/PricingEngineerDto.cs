﻿using System;

namespace Smt.Atomic.Business.Organization.Dto
{
    public class PricingEngineerDto
    {
        public long Id { get; set; }

        public long[] SkillTagIds { get; set; }

        public long EmployeeId { get; set; }

        public string Location { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }
        
        public byte[] Timestamp { get; set; }
    }
}
