﻿using System.Linq;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Organization.Dto
{
    public class TechnicalInterviewerToTechnicalInterviewerDtoMapping : ClassMapping<TechnicalInterviewer, TechnicalInterviewerDto>
    {
        public TechnicalInterviewerToTechnicalInterviewerDtoMapping()
        {
            Mapping = e => new TechnicalInterviewerDto
            {
                Id = e.Id,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp,
                EmployeeId = e.EmployeeId,
                SkillTagIds = e.SkillTags.Select(s => s.Id).ToArray(),
                Location = e.Employee == null ? null : e.Employee.Location.Name,
                CompanyName = e.Employee == null
                    ? null
                    : e.Employee.Company == null
                        ? null
                        : e.Employee.Company.Name,
                FirstName = e.Employee == null ? null : e.Employee.FirstName,
                LastName = e.Employee == null ? null : e.Employee.LastName,
                Acronym = e.Employee == null ? null : e.Employee.Acronym,
                CandidateMaxLevel = e.CandidateMaxLevel,
                CanInterviewForeignCandidates = e.CanInterviewForeignCandidates,
                Comments = e.Comments,
                AvailabilityDetails = e.Employee == null ? null : e.Employee.AvailabilityDetails,
                SkypeName = e.Employee == null ? null : e.Employee.SkypeName
            };
        }
    }
}
