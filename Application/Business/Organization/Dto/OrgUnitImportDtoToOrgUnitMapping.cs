﻿using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.Organization.Dto
{
    public class OrgUnitImportDtoToOrgUnitMapping : ClassMapping<OrgUnitImportDto, OrgUnit>
    {
        public OrgUnitImportDtoToOrgUnitMapping()
        {
            Mapping = d => new OrgUnit
            {
                Id = d.Id,
                Code = d.Code,
                Path = d.Path,
                Name = d.Name,
                OrgUnitFeatures = d.OrgUnitFeatures,
                NameSortOrder = d.NameSortOrder,
                DescendantCount = d.DescendantCount,
                Description = d.Description,
                ParentId = d.ParentId,
                CalendarId = d.CalendarId,
                EmployeeId = d.EmployeeId,
                Timestamp = d.Timestamp,
            };
        }
    }
}