﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Organization;

namespace Smt.Atomic.Business.Organization.Dto
{
    public class OrgUnitDtoToOrgUnitMapping : ClassMapping<OrgUnitDto, OrgUnit>
    {
        public OrgUnitDtoToOrgUnitMapping()
        {
            Mapping = d => new OrgUnit
            {
                Id = d.Id,
                Code = d.Code,
                Path = d.Path,
                Name = d.Name,
                FlatName = d.FlatName,
                OrgUnitFeatures = d.OrgUnitFeatures,
                NameSortOrder = d.NameSortOrder,
                DescendantCount = d.DescendantCount,
                Description = d.Description,
                ParentId = d.ParentId,
                DefaultProjectContributionMode = d.DefaultProjectContributionMode,
                TimeReportingMode = d.TimeReportingMode,
                AbsenceOnlyDefaultProjectId = d.AbsenceOnlyDefaultProjectId,
                Timestamp = d.Timestamp,
                Depth = d.Depth,
                CalendarId = d.CalendarId,
                EmployeeId = d.EmployeeId,
                RegionId = d.RegionId,
                CustomOrder = d.CustomOrder,
                OrgUnitManagerRole = d.OrgUnitManagerRole,
            };
        }
    }
}
