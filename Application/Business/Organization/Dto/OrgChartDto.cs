﻿namespace Smt.Atomic.Business.Organization.Dto
{
    public class OrgChartDto
    {
        public OrgChartLevelDto TopLevel { get; set; }
        
        public OrgChartClipDto Clip { get; set; }
    }
}