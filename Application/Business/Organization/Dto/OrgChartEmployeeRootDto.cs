﻿namespace Smt.Atomic.Business.Organization.Dto
{
    public class OrgChartEmployeeRootDto
    {
        public string Id { get; set; }

        public long LongId { get; set; }
        
        public string ManagerName { get; set; }
        
        public string ManagerTitle { get; set; }
        
        public string ImageAddress { get; set; }
        
        public string LocationName { get; set; }

    }
}