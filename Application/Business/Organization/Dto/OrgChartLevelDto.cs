﻿namespace Smt.Atomic.Business.Organization.Dto
{
    public class OrgChartLevelDto : OrgChartLevelItemBaseDto
    {
        public OrgChartLevelDto()
        {
            Root = new OrgUnitChartDto();    
        }
        
        public OrgUnitChartDto Root { get; set; }
    }
}