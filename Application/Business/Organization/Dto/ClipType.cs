﻿namespace Smt.Atomic.Business.Organization.Dto
{
    public enum ClipType
    {
        Unspecified = 0,
        DescendantsOnly = 1,
        AncestorsOnly = 2
    }
}