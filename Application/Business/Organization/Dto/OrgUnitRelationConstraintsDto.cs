﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Organization.Dto
{
    public class OrgUnitRelationConstraintsDto
    {
        public IDictionary<string, IEnumerable<string>> Employees { get; set; }

        public IDictionary<string, IEnumerable<string>> Projects { get; set; }

        public IDictionary<string, IEnumerable<string>> SurveyConducts { get; set; }

        public bool IsOrgUnitBound
        {
            get
            {
                return (Employees != null && Employees.Count > 0)
                  || (Projects != null && Projects.Count > 0)
                  || (SurveyConducts != null && SurveyConducts.Count > 0);
            }
        }
    }
}
