﻿namespace Smt.Atomic.Business.Organization.Dto
{
    public class OrgChartEmployeeItemDto : OrgChartLevelItemBaseDto
    {
        public string Id { get; set; }
        
        public OrgChartEmployeeRootDto Root { get; set; }
    }
}