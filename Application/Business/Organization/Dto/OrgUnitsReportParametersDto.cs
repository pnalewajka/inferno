﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Organization.Dto
{
    public class OrgUnitsReportParametersDto
    {
        public IList<long> OrgUnitIds { get; set; }
    }
}
