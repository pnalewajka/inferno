﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Organization.Dto
{
    public class OrgChartEmployeeDto
    {
        public List<OrgChartEmployeeItemDto> Children { get; set; }

        public OrgChartEmployeeDto()
        {
            Children = new List<OrgChartEmployeeItemDto>();
        }
    }
}