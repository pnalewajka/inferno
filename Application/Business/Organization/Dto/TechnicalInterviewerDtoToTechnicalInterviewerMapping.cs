﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;

namespace Smt.Atomic.Business.Organization.Dto
{
    public class TechnicalInterviewerDtoToTechnicalInterviewerMapping : ClassMapping<TechnicalInterviewerDto, TechnicalInterviewer>
    {
        public TechnicalInterviewerDtoToTechnicalInterviewerMapping()
        {
            Mapping = d => new TechnicalInterviewer
            {
                Id = d.Id,
                Timestamp = d.Timestamp,
                EmployeeId = d.EmployeeId,
                SkillTags = d.SkillTagIds != null ? d.SkillTagIds.Select(id => new SkillTag { Id = id }).ToList() : null,
                CandidateMaxLevel = d.CandidateMaxLevel,
                CanInterviewForeignCandidates = d.CanInterviewForeignCandidates,
                Comments = d.Comments
            };
        }
    }
}
