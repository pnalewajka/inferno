﻿namespace Smt.Atomic.Business.Organization.Dto
{
    public class OrgChartClipDto
    {
        public string FocusedNodeId { get; set; }
        
        public ClipType ClipType { get; set; }
    }
}