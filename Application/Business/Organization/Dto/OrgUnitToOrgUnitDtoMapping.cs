﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Organization;

namespace Smt.Atomic.Business.Organization.Dto
{
    public class OrgUnitToOrgUnitDtoMapping : ClassMapping<OrgUnit, OrgUnitDto>
    {
        public OrgUnitToOrgUnitDtoMapping()
        {
            Mapping = e => new OrgUnitDto
            {
                Id = e.Id,
                Code = e.Code,
                Path = e.Path,
                Name = e.Name,
                FlatName = e.FlatName,
                OrgUnitFeatures = e.OrgUnitFeatures,
                NameSortOrder = e.NameSortOrder,
                DescendantCount = e.DescendantCount,
                Description = e.Description,
                ParentId = e.ParentId,
                DefaultProjectContributionMode = e.DefaultProjectContributionMode,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                TimeReportingMode = e.TimeReportingMode,
                AbsenceOnlyDefaultProjectId = e.AbsenceOnlyDefaultProjectId,
                Timestamp = e.Timestamp,
                Depth = e.Depth,
                CalendarId = e.CalendarId,
                EmployeeId = e.EmployeeId,
                RegionId = e.RegionId,
                CustomOrder = e.CustomOrder,
                OrgUnitManagerRole = e.OrgUnitManagerRole
            };
        }
    }
}
