﻿namespace Smt.Atomic.Business.Finance.Jobs
{
    using System;

    using Castle.Core.Logging;

    using Smt.Atomic.Business.Common.Contexts;
    using Smt.Atomic.Business.Common.Interfaces;
    using Smt.Atomic.Business.Common.Models;
    using Smt.Atomic.Business.Finance.Interfaces;
    using Smt.Atomic.CrossCutting.Common.Attributes;
    using Smt.Atomic.CrossCutting.Common.Consts;
    using Smt.Atomic.CrossCutting.Common.Interfaces;

    [Identifier("Job.Finance.AggregatedProjectTransactionsSyncHourlyJob")]
    [JobDefinition(
        Code = "AggregatedProjectTransactionsSyncHourlyJob",
        Description =
            "Synchronize n-last month(s) of the Navision's transactions and prepare aggregated by project transaction with calculation",
        ScheduleSettings = "0 0 * * * *",
        MaxExecutioPeriodInSeconds = 180,
        PipelineName = PipelineCodes.NavisionSynchronization)]
    public sealed class AggregatedProjectTransactionsSyncHourlyJob : IJob
    {
        private readonly IAggregatedProjectTransactionSynchronizeService _aggregatedProjectTransactionSynchronizeService;

        private readonly ISystemParameterService _systemParameterService;

        private readonly ILogger _logger;

        public AggregatedProjectTransactionsSyncHourlyJob(
            IAggregatedProjectTransactionSynchronizeService synchronizeService,
            ISystemParameterService systemParameterService,
            ILogger logger)
        {
            _aggregatedProjectTransactionSynchronizeService = synchronizeService;
            _systemParameterService = systemParameterService;
            _logger = logger;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            try
            {
                var month = _systemParameterService.GetParameter<int>(ParameterKeys.NavisionTransactionMonthsToSyncHourly);

                _aggregatedProjectTransactionSynchronizeService.SynchronizeTransactions(month);

                return JobResult.Success();
            }
            catch (Exception ex)
            {
                _logger.Error($"{nameof(AggregatedProjectTransactionsSyncHourlyJob)} failed", ex);

                return JobResult.Fail();
            }
        }
    }
}