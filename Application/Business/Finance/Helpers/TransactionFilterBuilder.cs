﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Finance.Dto;
using Smt.Atomic.Business.Finance.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;

namespace Smt.Atomic.Business.Finance.Helpers
{
    public class TransactionFilterBuilder : ITransactionFilterBuilder
    {
        private readonly IOrgUnitService _orgUnitService;

        public TransactionFilterBuilder(IOrgUnitService orgUnitService)
        {
            _orgUnitService = orgUnitService;
        }

        public Expression<Func<TransactionDto, bool>> GetMyTransactionsFilter(long employeeId, long? userId)
        {
            var managerOrgUnitsIds = _orgUnitService.GetManagerOrgUnitDescendantIds(employeeId);
        
            return
                t =>
                    t.ProjectOrgUnitId.HasValue && managerOrgUnitsIds.Any(id => id == t.ProjectOrgUnitId) ||
                    (t.ProjectManagerUserId.HasValue && t.ProjectManagerUserId == userId) ||
                    (t.GeneralManagerUserId.HasValue && t.GeneralManagerUserId == userId) ||
                    (t.OperationalManagerUserId.HasValue && t.OperationalManagerUserId == userId) ||
                    (t.OperationalDirectorUserId.HasValue && t.OperationalDirectorUserId == userId) ||
                    (t.SalesPersonUserId.HasValue && t.SalesPersonUserId == userId);
        }

        public Expression<Func<AggregatedProjectTransactionsDto, bool>> GetMyAggregatedProjectTransactionsFilter(long employeeId, long? userId)
        {
            var managerOrgUnitsIds = _orgUnitService.GetManagerOrgUnitDescendantIds(employeeId);

            return
                a =>
                    managerOrgUnitsIds.Any(id => id == a.OrgUnitId) ||
                    (a.ProjectManagerUserId.HasValue && a.ProjectManagerUserId == userId) ||
                    (a.ProjectManagerOrgUnitId.HasValue && managerOrgUnitsIds.Contains(a.ProjectManagerOrgUnitId.Value)) ||
                    (a.SupervisorManagerUserId.HasValue && a.SupervisorManagerUserId == userId) ||
                    (a.SupervisorManagerOrgUnitId.HasValue &&
                     managerOrgUnitsIds.Contains(a.SupervisorManagerOrgUnitId.Value)) ||
                    (a.SalesAccountManagerUserId.HasValue && a.SalesAccountManagerUserId == userId) ||
                    (a.SalesAccountManagerOrgUnitId.HasValue &&
                     managerOrgUnitsIds.Contains(a.SalesAccountManagerOrgUnitId.Value));
        }
    }
}
