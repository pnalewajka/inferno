﻿namespace Smt.Atomic.Business.Finance.Interfaces
{
    public interface IAggregatedProjectTransactionSynchronizeService
    {
        /// <summary>
        /// Synchronize aggregated project transaction for last n months
        /// </summary>
        /// <param name="months">Months to synchronize</param>
        void SynchronizeTransactions(int months);
    }
}
