using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Finance.Dto;

namespace Smt.Atomic.Business.Finance.Interfaces
{
    public interface IAggregatedProjectTransactionsService
    {
        IList<AggregatedProjectTransactionsDto> GetAggregatedProjectTransactions(int year, int month);

        AggregatedProjectTransactionsDto GetAggregatedProjectTransactionsById(long id);
    }
}