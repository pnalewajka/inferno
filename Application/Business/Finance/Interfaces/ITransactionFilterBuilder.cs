﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.Business.Finance.Dto;

namespace Smt.Atomic.Business.Finance.Interfaces
{
    public interface ITransactionFilterBuilder
    {
        Expression<Func<TransactionDto, bool>> GetMyTransactionsFilter(long employeeId, long? userId);

        Expression<Func<AggregatedProjectTransactionsDto, bool>> GetMyAggregatedProjectTransactionsFilter(long employeeId, long? userId);
    }
}