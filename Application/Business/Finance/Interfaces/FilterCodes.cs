﻿namespace Smt.Atomic.Business.Finance.Interfaces
{
    public static class FilterCodes
    {
        public const string MyProjects = "my-projects";
        public const string AllProjects = "all-projects";
        public const string MyTransactions = "my-transactions";
        public const string AllTransactions = "all-transactions";
        public const string WithTransactions = "with-transactions";
        public const string WithoutTrasanctions = "without-transactions";
        public const string OrgUnits = "org-units";
        public const string Region = "region";
        public const string ClientName = "client";
        public const string ProjectName = "project";
    }
}
