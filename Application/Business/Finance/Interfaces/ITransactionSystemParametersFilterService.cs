using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Finance.Dto;

namespace Smt.Atomic.Business.Finance.Interfaces
{
    public interface ITransactionSystemParametersFilterService
    {
        bool IsRevenueTransaction(TransactionDto transaction);

        bool IsProjectCostTransaction(TransactionDto transaction);

        bool IsOperationCostTransaction(TransactionDto transaction);

        bool IsServiceCostTransaction(TransactionDto transaction);

        bool HasAllowedCostCenter(TransactionDto transaction);
    }
}