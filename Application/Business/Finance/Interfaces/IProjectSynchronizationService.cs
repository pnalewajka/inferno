using Smt.Atomic.Business.Finance.Dto;

namespace Smt.Atomic.Business.Finance.Interfaces
{
    public interface IProjectSynchronizationService
    {
        void CreateOrUpdateFinansialProject(FinancialProjectDto projectDto);

        long? SynchronizeAndGetUtilizationCategoryId(FinancialProjectDto projectDto);
    }
}