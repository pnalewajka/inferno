﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Finance.Interfaces
{
    public interface IDimensionValueMappingCacheService
    {
        IDictionary<string, long> GetApnToProjectIdMappings();

        IDictionary<string, long> GetApnToProjectOrgUnitIdMappings();

        IDictionary<string, long> GetLoginToUserIdMappings();

        IDictionary<long, string> GetEmployeeIdToAcronymMappings();
    }
}