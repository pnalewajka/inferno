﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Finance.Dto;

namespace Smt.Atomic.Business.Finance.Interfaces
{
    public interface IAggregatedProjectTransactionsCardIndexDataService : ICardIndexDataService<AggregatedProjectTransactionsDto>
    {
    }
}