using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Finance.Dto;

namespace Smt.Atomic.Business.Finance.Interfaces
{
    public interface ITransactionService
    {
        IList<TransactionDto> GetTransactions(DateTime from, DateTime to);

        TransactionDto GetTransactionById(long id);

        TransactionDto GetTransactionByIdOrDefault(long id);
    }
}