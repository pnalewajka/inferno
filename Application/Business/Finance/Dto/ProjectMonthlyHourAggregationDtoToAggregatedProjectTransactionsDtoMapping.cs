﻿namespace Smt.Atomic.Business.Finance.Dto
{
    using Smt.Atomic.CrossCutting.Common.Abstacts;
    using Smt.Atomic.Data.Entities.Modules.Finance;

    public class AggregatedTimeReportRowDtoToAggregatedProjectTransactionsMapping : ClassMapping<ProjectMonthlyHourAggregationDto, AggregatedProjectTransaction>
    {
        public AggregatedTimeReportRowDtoToAggregatedProjectTransactionsMapping()
        {
            Mapping = d => new AggregatedProjectTransaction()
            {
                Month = d.Month,
                Year = d.Year,
                ProjectId = d.ProjectId,
                Hours = d.Hours
            };
        }
    }
}
