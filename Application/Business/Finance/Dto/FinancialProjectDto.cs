using System;

namespace Smt.Atomic.Business.Finance.Dto
{
    public class FinancialProjectDto
    {
        private const int DescriptionSplitPointInCharacters = 50;

        public string Apn { get; set; }

        public string Description { get; set; }

        public long? ProjectManagerId { get; set; }

        public long? OperationalManagerId { get; set; }

        public long? SalesPersonId { get; set; }

        public long? GeneralManagerId { get; set; }

        public long? OperationalDirectorId { get; set; }

        public DateTime? StartDate { get; set; }

        public string Region { get; set; }

        public string DescriptionPart1 => Description.Substring(0, Math.Min(Description.Length, DescriptionSplitPointInCharacters));


        public string DescriptionPart2 => Description.Length > DescriptionSplitPointInCharacters
                    ? Description.Substring(DescriptionSplitPointInCharacters, Math.Min(Description.Length - DescriptionSplitPointInCharacters, DescriptionSplitPointInCharacters - 1))
                    : string.Empty;
    }
}