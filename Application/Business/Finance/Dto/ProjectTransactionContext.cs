﻿using Smt.Atomic.Business.Common.Interfaces;

namespace Smt.Atomic.Business.Finance.Dto
{
    public class ProjectTransactionContext : TransactionContext, IParentIdContext
    {
        public long ParentId { get; set; }
    }
}