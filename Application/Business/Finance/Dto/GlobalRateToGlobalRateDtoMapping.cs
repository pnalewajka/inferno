﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Finance;

namespace Smt.Atomic.Business.Finance.Dto
{
    public class GlobalRateToGlobalRateDtoMapping : ClassMapping<GlobalRate, GlobalRateDto>
    {
        public GlobalRateToGlobalRateDtoMapping()
        {
            Mapping = e => new GlobalRateDto
            {
                Id = e.Id,
                Name = e.Name,
                Value = e.Value,
                CurrencyId = e.CurrencyId,
                Description = e.Description,
                JobMatrixLevelId = e.JobMatrixLevelId,
                LocationId = e.LocationId,
                CompanyId = e.CompanyId
            };
        }
    }
}
