﻿namespace Smt.Atomic.Business.Finance.Dto
{
    public class GlobalRateDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public decimal Value { get; set; }

        public long CurrencyId { get; set; }

        public string Description { get; set; }

        public long? JobMatrixLevelId { get; set; }

        public long? LocationId { get; set; }

        public long? CompanyId { get; set; }
    }
}
