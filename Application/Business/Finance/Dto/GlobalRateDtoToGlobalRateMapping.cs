﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Finance;

namespace Smt.Atomic.Business.Finance.Dto
{
    public class GlobalRateDtoToGlobalRateMapping : ClassMapping<GlobalRateDto, GlobalRate>
    {
        public GlobalRateDtoToGlobalRateMapping()
        {
            Mapping = d => new GlobalRate
            {
                Id = d.Id,
                Name = d.Name,
                Value = d.Value,
                CurrencyId = d.CurrencyId,
                Description = d.Description,
                JobMatrixLevelId = d.JobMatrixLevelId,
                LocationId = d.LocationId,
                CompanyId = d.CompanyId
            };
        }
    }
}
