﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Business.Finance.Dto
{
    public class AggregatedProjectTransactionsDto : IEntity
    {
        public long Id { get; set; }
        public long ProjectId { get; set; }
        public long OrgUnitId { get; set; }
        public string  OrgUnitPath { get; set; }
        public long? ProjectManagerUserId { get; set; }
        public long? ProjectManagerOrgUnitId { get; set; }
        public long? SalesAccountManagerUserId { get; set; }
        public long? SalesAccountManagerOrgUnitId { get; set; }
        public long? SupervisorManagerUserId { get; set; }
        public long? SupervisorManagerOrgUnitId { get; set; }
        public string ClientShortName { get; set; }
        public string ProjectShortName { get; set; }
        public decimal? Revenue { get; set; }
        public decimal? EstimatedRevenue { get; set; }
        public decimal? ProjectCost { get; set; }
        public decimal? ProjectMargin { get; set; }
        public decimal? ProjectMarginPercent { get; set; }
        public decimal? OperationCost { get; set; }
        public decimal? OperationMargin { get; set; }
        public decimal? OperationMarginPercent { get; set; }
        public decimal? ServiceCost { get; set; }
        public decimal? ServiceMargin { get; set; }
        public decimal? ServiceMarginPercent { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Region { get; set; }
        public decimal Hours { get; set; }
    }
}