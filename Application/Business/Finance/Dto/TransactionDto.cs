﻿using System;
using Smt.Atomic.Data.Entities.Interfaces;

namespace Smt.Atomic.Business.Finance.Dto
{
    public class TransactionDto : IEntity
    {
        public long Id { get; set; }
        public string Description { get; set; }
        public decimal? AmountPln { get; set; }
        public decimal? AmountEur { get; set; }
        public DateTime? PostingDate { get; set; }        
        public DateTime? DocumentDate { get; set; }
        public string DocumentNo { get; set; }
        public string DocumentType { get; set; }
        public int EntryNo { get; set; }
        public string ExternalDocumentNo { get; set; }
        public string GeneralLedgerAccountNo { get; set; }
        public string SourceNo { get; set; }
        public string SourceName { get; set; }
        public string SourceCode { get; set; }
        public string SourceType { get; set; }
        public string SourceCompanyName { get; set; }
        public long? CreatedByUserId { get; set; }
        public string BudgetPositionCode { get; set; }
        public string BudgetPositionName { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string Ifrs { get; set; }
        public string LocationCode { get; set; }
        public string LocationName { get; set; }
        public bool NoTax { get; set; }
        public string OrgUnitCode { get; set; }
        public string OrgUnitName { get; set; }
        public long? ProjectId { get; set; }
        public long? ProjectOrgUnitId { get; set; }
        public string Account { get; set; }
        public string CostCenterCode { get; set; }
        public string CostCenterName { get; set; }
        public long? ProjectManagerUserId { get; set; }
        public string ProjectGroup { get; set; }
        public string ProjectType { get; set; }
        public string ProjectUtilization { get; set; }
        public string AccountType { get; set; }
        public long? OperationalManagerUserId { get; set; }
        public string Region { get; set; }
        public long? SalesPersonUserId { get; set; }
        public string Vertical { get; set; }
        public long? GeneralManagerUserId { get; set; }
        public long? OperationalDirectorUserId { get; set; }
    }
}