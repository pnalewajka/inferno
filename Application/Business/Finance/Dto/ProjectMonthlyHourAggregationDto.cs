﻿namespace Smt.Atomic.Business.Finance.Dto
{
    public class ProjectMonthlyHourAggregationDto
    {
        public long ProjectId { get; set; }
        public byte Month { get; set; }
        public int Year { get; set; }
        public decimal Hours { get; set; }
    }
}
