﻿namespace Smt.Atomic.Business.Finance.Dto
{
    public class TransactionContext
    {
        public TransactionContext()
        {
        }

        public TransactionContext(int year, int month)
        {
            Year = year;
            Month = month;
        }

        public int? Year { get; set; }

        public int? Month { get; set; }

        public bool IsSet()
        {
            return Year.HasValue && Month.HasValue;
        }
    }
}