﻿namespace Smt.Atomic.Business.Finance.Dto
{
    using Smt.Atomic.CrossCutting.Common.Abstacts;
    using Smt.Atomic.Data.Entities.BusinessLogic;
    using Smt.Atomic.Data.Entities.Modules.Finance;

    public class AggregatedProjectTransactionsToAggregatedProjectTransactionsDtoMapping : ClassMapping<AggregatedProjectTransaction, AggregatedProjectTransactionsDto>
    {
        public AggregatedProjectTransactionsToAggregatedProjectTransactionsDtoMapping()
        {
            Mapping = x => CreateDto(x);
        }

        private static AggregatedProjectTransactionsDto CreateDto(AggregatedProjectTransaction transaction)
        {
            return new AggregatedProjectTransactionsDto
            {
                ClientShortName = transaction.Project?.ProjectSetup.ClientShortName,
                EndDate = transaction.Project?.EndDate,
                EstimatedRevenue = transaction.EstimatedRevenue,
                Hours = transaction.Hours,
                Id = transaction.Id,
                OperationCost = transaction.OperationCost,
                OperationMargin = transaction.OperationMargin,
                OperationMarginPercent = transaction.OperationMarginPercent,
                OrgUnitId = transaction.Project?.ProjectSetup.OrgUnitId ?? 0,
                OrgUnitPath = transaction.Project?.ProjectSetup.OrgUnit?.Path,
                ProjectCost = transaction.ProjectCost,
                ProjectId = transaction.ProjectId,
                ProjectManagerOrgUnitId = transaction.Project?.ProjectSetup.ProjectManager?.OrgUnitId,
                ProjectManagerUserId = transaction.Project?.ProjectSetup.ProjectManager?.UserId,
                ProjectMargin = transaction.ProjectMargin,
                ProjectMarginPercent = transaction.ProjectMarginPercent,
                ProjectShortName = transaction.Project == null ? null : ProjectBusinessLogic.ProjectName.Call(transaction.Project),
                Region = transaction.Project?.ProjectSetup.Region,
                Revenue = transaction.Revenue,
                SalesAccountManagerOrgUnitId = transaction.Project?.ProjectSetup.SalesAccountManager?.OrgUnitId,
                SalesAccountManagerUserId = transaction.Project?.ProjectSetup.SalesAccountManager?.UserId,
                ServiceCost = transaction.ServiceCost,
                ServiceMargin = transaction.ServiceMargin,
                ServiceMarginPercent = transaction.ServiceMarginPercent,
                StartDate = transaction.Project?.StartDate,
                SupervisorManagerOrgUnitId = transaction.Project?.ProjectSetup.SupervisorManager?.OrgUnitId,
                SupervisorManagerUserId = transaction.Project?.ProjectSetup.SupervisorManager?.UserId
            };
        }
    }
}