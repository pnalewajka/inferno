﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.Business.Finance.Dto;
using Smt.Atomic.Business.Finance.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.Finance.EntitySecurityRestrictions
{
    public class TransactionSecurityRestriction : EntitySecurityRestriction<TransactionDto>
    {
        private readonly ITransactionFilterBuilder _transactionFilterBuilder;

        public TransactionSecurityRestriction(
            IPrincipalProvider principalProvider,
            ITransactionFilterBuilder transactionFilterBuilder)
          : base(principalProvider)
        {
            _transactionFilterBuilder = transactionFilterBuilder;

            RequiresAuthentication = true;
        }

        public override Expression<Func<TransactionDto, bool>> GetRestriction()
        {
            if (Roles.Contains(SecurityRoleType.CanViewAllTransactions))
            {
                return AllRecords();
            }

            if (Roles.Contains(SecurityRoleType.CanViewMyTransactions))
            {
                return _transactionFilterBuilder.GetMyTransactionsFilter(CurrentPrincipal.EmployeeId, CurrentPrincipal.Id);
            }

            return NoRecords();
        }
    }
}
