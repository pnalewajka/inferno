﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.Business.Finance.Dto;
using Smt.Atomic.Business.Finance.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.Finance.EntitySecurityRestrictions
{
    public class AggregatedProjectTransactionsSecurityRestriction : EntitySecurityRestriction<AggregatedProjectTransactionsDto>
    {
        private readonly ITransactionFilterBuilder _transactionFilterBuilder;

        public AggregatedProjectTransactionsSecurityRestriction(
            IPrincipalProvider principalProvider,
            ITransactionFilterBuilder transactionFilterBuilder)
          : base(principalProvider)
        {
            _transactionFilterBuilder = transactionFilterBuilder;

            RequiresAuthentication = true;
        }

        public override Expression<Func<AggregatedProjectTransactionsDto, bool>> GetRestriction()
        {
            if (Roles.Contains(SecurityRoleType.CanViewAllAggregatedProjectTransactions))
            {
                return AllRecords();
            }

            if (Roles.Contains(SecurityRoleType.CanViewMyAggregatedProjectTransactions))
            {
                return _transactionFilterBuilder.GetMyAggregatedProjectTransactionsFilter(CurrentPrincipal.EmployeeId, CurrentPrincipal.Id);
            }

            return NoRecords();
        }
    }
}
