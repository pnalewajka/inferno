﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Finance.Dto;
using Smt.Atomic.Business.Finance.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Repositories.Helpers;

namespace Smt.Atomic.Business.Finance.Services
{
    public class AggregatedProjectTransactionsCardIndexDataService : CustomDataCardIndexDataService<AggregatedProjectTransactionsDto>, IAggregatedProjectTransactionsCardIndexDataService
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly ITimeService _timeService;
        private readonly IAggregatedProjectTransactionsService _aggregatedProjectTransactionsService;
        private readonly ITransactionFilterBuilder _transactionFilterBuilder;

        public AggregatedProjectTransactionsCardIndexDataService(
            IPrincipalProvider principalProvider,
            ITimeService timeService,
            IAggregatedProjectTransactionsService aggregatedProjectTransactionsService,
            ITransactionFilterBuilder transactionFilterBuilder)
        {
            _principalProvider = principalProvider;
            _timeService = timeService;
            _aggregatedProjectTransactionsService = aggregatedProjectTransactionsService;
            _transactionFilterBuilder = transactionFilterBuilder;
        }

        protected override IList<AggregatedProjectTransactionsDto> GetAllRecords(object context = null)
        {
            var transactionContext = context as TransactionContext;

            if (transactionContext == null || !transactionContext.IsSet())
            {
                var currectDate = _timeService.GetCurrentDate();

                transactionContext = new TransactionContext(currectDate.Year, currectDate.Month);
            }

            return _aggregatedProjectTransactionsService
                .GetAggregatedProjectTransactions(transactionContext.Year.Value, transactionContext.Month.Value)
                .Filtered()
                .OrderByDescending(x => x.ProjectId)
                .ToList();
        }

        public override AggregatedProjectTransactionsDto GetRecordById(long id)
        {
            return new[] { _aggregatedProjectTransactionsService.GetAggregatedProjectTransactionsById(id) }.Filtered().Single();
        }

        public override AggregatedProjectTransactionsDto GetRecordByIdOrDefault(long id)
        {
            return new[] { _aggregatedProjectTransactionsService.GetAggregatedProjectTransactionsById(id) }.Filtered().SingleOrDefault();
        }

        protected override IEnumerable<Expression<Func<AggregatedProjectTransactionsDto, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            if (searchCriteria.Includes(FilterCodes.Region))
            {
                yield return t => t.Region;
            }

            if (searchCriteria.Includes(FilterCodes.ClientName))
            {
                yield return t => t.ClientShortName;
            }

            if (searchCriteria.Includes(FilterCodes.ProjectName))
            {
                yield return t => t.ProjectShortName;
            }
        }

        protected override NamedFilters<AggregatedProjectTransactionsDto> NamedFilters
        {
            get
            {
                return new NamedFilters<AggregatedProjectTransactionsDto>(new[]
                {
                    new NamedFilter<AggregatedProjectTransactionsDto>(
                        FilterCodes.MyProjects,
                        _transactionFilterBuilder.GetMyAggregatedProjectTransactionsFilter(_principalProvider.Current.EmployeeId, _principalProvider.Current.Id),
                        SecurityRoleType.CanViewMyAggregatedProjectTransactions),
                    new NamedFilter<AggregatedProjectTransactionsDto>(
                        FilterCodes.AllProjects,
                        a => true,
                        SecurityRoleType.CanViewAllAggregatedProjectTransactions),
                    new NamedFilter<AggregatedProjectTransactionsDto, long[]>(
                        FilterCodes.OrgUnits,
                        (a, orgUnitIds) => (orgUnitIds.Any(id => a.OrgUnitPath.Contains("/" + id + "/")))),
                    new NamedFilter<AggregatedProjectTransactionsDto>(
                        FilterCodes.WithTransactions,
                        a => a.Revenue.HasValue),
                    new NamedFilter<AggregatedProjectTransactionsDto>(
                        FilterCodes.WithoutTrasanctions,
                        a => !a.Revenue.HasValue)
                });
            }
        }
    }
}