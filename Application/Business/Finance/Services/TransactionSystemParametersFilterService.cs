﻿using System.Linq;
using Smt.Atomic.Business.Finance.Dto;
using Smt.Atomic.Business.Finance.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.Business.Finance.Services
{
    public class TransactionSystemParametersFilterService : ITransactionSystemParametersFilterService
    {
        private const string ProjectCostCentreCode = "1";
        private const string OperationCostCentreCode = "2B";
        private const string ServiceCostCentreCode = "3A";

        private readonly ISystemParameterService _systemParameterService;

        public TransactionSystemParametersFilterService(ISystemParameterService systemParameterService)
        {
            _systemParameterService = systemParameterService;
        }

        public bool IsRevenueTransaction(TransactionDto transaction)
        {
            return 
                RevenueIncludedAccounts.Any(x => transaction.GeneralLedgerAccountNo.Matches(x)) &&
                !RevenueExcludedAccounts.Any(x => transaction.GeneralLedgerAccountNo.Matches(x)) &&
                HasAllowedCostCenter(transaction);
        }

        public bool IsProjectCostTransaction(TransactionDto transaction)
        {
            return
                CostIncludedAccounts.Any(x => transaction.GeneralLedgerAccountNo.Matches(x)) &&
                !CostExcludedAccounts.Any(x => transaction.GeneralLedgerAccountNo.Matches(x)) &&
                transaction.CostCenterCode == ProjectCostCentreCode;
        }

        public bool IsOperationCostTransaction(TransactionDto transaction)
        {
            return transaction.CostCenterCode == OperationCostCentreCode;;
        }

        public bool IsServiceCostTransaction(TransactionDto transaction)
        {
            return
                !ServiceCostExcludedAccounts.Any(x => transaction.GeneralLedgerAccountNo.Matches(x)) &&
                transaction.CostCenterCode == ServiceCostCentreCode;
        }

        public bool HasAllowedCostCenter(TransactionDto transaction)
        {
            return AllowedCostCenters.Contains(transaction.CostCenterCode);
        }

        private string[] AllowedCostCenters => new[] { ProjectCostCentreCode, OperationCostCentreCode, ServiceCostCentreCode };

        private string[] _revenueIncludedAccounts;
        private string[] RevenueIncludedAccounts
        {
            get
            {
                if (_revenueIncludedAccounts == null)
                {
                    _revenueIncludedAccounts = 
                        _systemParameterService.GetParameter<string[]>(ParameterKeys.ProjectProfitabilityRevenueIncludedAccounts).Select(x => x.WildCardsToRegexSyntax()).ToArray();
                }

                return _revenueIncludedAccounts;
            }
        }

        private string[] _revenueExcludedAccounts;
        private string[] RevenueExcludedAccounts
        {
            get
            {
                if (_revenueExcludedAccounts == null)
                {
                    _revenueExcludedAccounts = 
                        _systemParameterService.GetParameter<string[]>(ParameterKeys.ProjectProfitabilityRevenueExcludedAccounts).Select(x => x.WildCardsToRegexSyntax()).ToArray();
                }

                return _revenueExcludedAccounts;
            }
        }

        private string[] _serviceCostExcludedAccounts;
        private string[] ServiceCostExcludedAccounts
        {
            get
            {
                if (_serviceCostExcludedAccounts == null)
                {
                    _serviceCostExcludedAccounts =
                        _systemParameterService.GetParameter<string[]>(ParameterKeys.ProjectProfitabilityServiceCostExcludedAccounts).Select(x => x.WildCardsToRegexSyntax()).ToArray();
                }

                return _serviceCostExcludedAccounts;
            }
        }

        private string[] _costIncludedAccounts;
        private string[] CostIncludedAccounts
        {
            get
            {
                if (_costIncludedAccounts == null)
                {
                    _costIncludedAccounts = 
                        _systemParameterService.GetParameter<string[]>(ParameterKeys.ProjectProfitabilityCostIncludedAccounts).Select(x => x.WildCardsToRegexSyntax()).ToArray();
                }

                return _costIncludedAccounts;
            }
        }

        private string[] _costExcludedAccounts;
        private string[] CostExcludedAccounts
        {
            get
            {
                if (_costExcludedAccounts == null)
                {
                    _costExcludedAccounts = 
                        _systemParameterService.GetParameter<string[]>(ParameterKeys.ProjectProfitabilityCostExcludedAccounts).Select(x => x.WildCardsToRegexSyntax()).ToArray();
                }

                return _costExcludedAccounts;
            }
        }
    }
}
