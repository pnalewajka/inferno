﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Finance.Dto;
using Smt.Atomic.Business.Finance.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Finance;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Finance.Services
{
    public class GlobalRateCardIndexDataService : CardIndexDataService<GlobalRateDto, GlobalRate, IFinanceDbScope>, IGlobalRateCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public GlobalRateCardIndexDataService(ICardIndexServiceDependencies<IFinanceDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override IEnumerable<Expression<Func<GlobalRate, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return g => g.Name;
            yield return g => g.Value;
            yield return g => g.Description;
        }
    }
}
