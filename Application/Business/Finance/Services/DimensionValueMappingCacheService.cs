﻿using System.Collections.Generic;
using System.Linq;
using Castle.Core;
using Smt.Atomic.Business.Finance.Interfaces;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Finance.Services
{
    [Interceptor(typeof(CacheInterceptor))]
    public class DimensionValueMappingCacheService : IDimensionValueMappingCacheService
    {
        private readonly IUnitOfWorkService<IAccountsDbScope> _accountsUnitOfWorkService;
        private readonly IUnitOfWorkService<IAllocationDbScope> _allocationUnitOfWorkService;

        public DimensionValueMappingCacheService(
            IUnitOfWorkService<IAccountsDbScope> accountsUnitOfWorkService,
            IUnitOfWorkService<IAllocationDbScope> allocationUnitOfWorkService)
        {
            _accountsUnitOfWorkService = accountsUnitOfWorkService;
            _allocationUnitOfWorkService = allocationUnitOfWorkService;
        }

        [Cached(CacheArea = CacheAreas.Projects, ExpirationInSeconds = 300)]
        public IDictionary<string, long> GetApnToProjectIdMappings()
        {
            var jiraApnToProjectIdMapping = new Dictionary<string, long>();

            using (var unitOfWork = _allocationUnitOfWorkService.Create())
            {
                foreach (var project in  unitOfWork
                    .Repositories
                    .Projects
                    .Where(p => p.Apn != null)
                    .Select(p => new {p.Id, p.Apn}))
                {
                    jiraApnToProjectIdMapping[project.Apn] = project.Id;
                }
            }

            return jiraApnToProjectIdMapping;
        }

        [Cached(CacheArea = CacheAreas.Projects, ExpirationInSeconds = 300)]
        public IDictionary<string, long> GetApnToProjectOrgUnitIdMappings()
        {
            var jiraApnToProjectOrgUnitIdMapping = new Dictionary<string, long>();

            using (var unitOfWork = _allocationUnitOfWorkService.Create())
            {
                foreach (var project in unitOfWork
                                        .Repositories
                                        .Projects
                                        .Where(p => p.Apn != null)
                                        .Select(p => new { p.ProjectSetup.OrgUnitId, p.Apn }))
                {
                    jiraApnToProjectOrgUnitIdMapping[project.Apn] = project.OrgUnitId;
                }
            }

            return jiraApnToProjectOrgUnitIdMapping;
        }

        [Cached(CacheArea = CacheAreas.Users, ExpirationInSeconds = 300)]
        public IDictionary<string, long> GetLoginToUserIdMappings()
        {
            using (var unitOfWork = _accountsUnitOfWorkService.Create())
            {
                return
                    unitOfWork
                        .Repositories
                        .Users
                        .Select(u => new {u.Id, u.Login})
                        .ToDictionary(e => e.Login.ToLower(), e => e.Id);
            }
        }

        [Cached(CacheArea = CacheAreas.Users, ExpirationInSeconds = 300)]
        public IDictionary<long, string> GetEmployeeIdToAcronymMappings()
        {
            using (var unitOfWork = _accountsUnitOfWorkService.Create())
            {
                return
                    unitOfWork
                        .Repositories
                        .Employees
                        .Select(u => new {u.Id, u.Acronym})
                        .ToDictionary(e => e.Id, e => e.Acronym);
            }
        }
    }
}