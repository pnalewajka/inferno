﻿using System;
using System.Linq;
using Castle.Core.Logging;
using Smt.Atomic.Business.Finance.Dto;
using Smt.Atomic.Business.Finance.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Navision.Interfaces;
using Smt.Atomic.Data.Navision.NavisionService;
using Smt.Atomic.Data.Navision.Services;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Finance.Services
{
    internal class ProjectSynchronizationService : IProjectSynchronizationService
    {
        private readonly INavisionUnitOfWorkService _navisionUnitOfWorkService;
        private readonly IDimensionValueMappingCacheService _dimensionValueMappingCacheService;
        private readonly IUnitOfWorkService<IDictionariesDbScope> _dictionariesUnitOfWorkService;
        private readonly ILogger _logger;

        public ProjectSynchronizationService(
            INavisionUnitOfWorkService navisionUnitOfWorkService,
            IDimensionValueMappingCacheService dimensionValueMappingCacheService,
            IUnitOfWorkService<IDictionariesDbScope> dictionariesUnitOfWorkService,
            ILogger logger)
        {
            _navisionUnitOfWorkService = navisionUnitOfWorkService;
            _dimensionValueMappingCacheService = dimensionValueMappingCacheService;
            _dictionariesUnitOfWorkService = dictionariesUnitOfWorkService;
            _logger = logger;
        }

        public long? SynchronizeAndGetUtilizationCategoryId(FinancialProjectDto projectDto)
        {
            if (ShouldSynchronizeWithNavision(projectDto))
            {
                return null;
            }

            var navisionUnitOfWork = _navisionUnitOfWorkService.Create();

            var projectNavision = navisionUnitOfWork.GetProjectByApn(projectDto.Apn);
           
            if (string.IsNullOrEmpty(projectNavision?.DimensionValue_x005B_13_x005D_))
            {
                return null;
            }

            var utilizationCategoryNavisionCode = projectNavision.DimensionValue_x005B_13_x005D_;

            using (var dictionariesUnitOfWork = _dictionariesUnitOfWorkService.Create())
            {
                if (!dictionariesUnitOfWork.Repositories.UtilizationCategories.Any(x => x.NavisionCode == utilizationCategoryNavisionCode))
                {
                    AddUtilizationCategory(navisionUnitOfWork, dictionariesUnitOfWork, utilizationCategoryNavisionCode);
                }

                return dictionariesUnitOfWork.Repositories.UtilizationCategories.Single(x => x.NavisionCode == utilizationCategoryNavisionCode).Id;
            }
        }

        private void AddUtilizationCategory(NavisionUnitOfWork navisionUnitOfWork, IUnitOfWork<IDictionariesDbScope> dictionariesUnitOfWork, string utilizationCategoryNavisionCode)
        {
            var utilizationCategoryDimensionValue = navisionUnitOfWork.GetUtilizationCategoryByCode(utilizationCategoryNavisionCode);

            var newUtilizationCategory = new UtilizationCategory
            {
                NavisionCode = utilizationCategoryDimensionValue.Code,
                NavisionName = utilizationCategoryDimensionValue.Name,
                DisplayName = CreateDisplayNameOfUtilizationCategory(utilizationCategoryDimensionValue.Name),
                IsActive = true
            };

            dictionariesUnitOfWork.Repositories.UtilizationCategories.Add(newUtilizationCategory);

            dictionariesUnitOfWork.Commit();
        }

        private string CreateDisplayNameOfUtilizationCategory(string navisionNameOfUtilizationCategory)
        {
            if (navisionNameOfUtilizationCategory.Length >= 2 && navisionNameOfUtilizationCategory[1] == '_')
            {
                return navisionNameOfUtilizationCategory.Substring(2);
            }
            
            return navisionNameOfUtilizationCategory;
        }

        public void CreateOrUpdateFinansialProject(FinancialProjectDto projectDto)
        {
            if (ShouldSynchronizeWithNavision(projectDto))
            {
                return;
            }

            var unitOfWork = _navisionUnitOfWorkService.Create();
            
            var projectNavision = unitOfWork.GetProjectByApn(projectDto.Apn) ?? new Project();

            bool newProject = string.IsNullOrEmpty(projectNavision.No);

            try
            {
                ModifyNavisionProject(projectNavision, projectDto);

                if (newProject)
                {
                    unitOfWork.AddProject(projectNavision);
                }
                else
                {
                    unitOfWork.UpdateProject(projectNavision);
                }
                
                unitOfWork.Commit();
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat(ex, "Failed to {0} project with APN number {1}", newProject ? "create" : "update",  projectNavision.No);
            }
        }

        private void ModifyNavisionProject(Project projectNavision, FinancialProjectDto projectDto)
        {
            if (projectNavision.No != projectDto.Apn)
            {
                projectNavision.No = projectDto.Apn;
            }

            var accountNumber = "A" + projectDto.Apn.Substring(0, 5);

            if (projectNavision.DimensionValue_x005B_8_x005D_ != accountNumber && !string.IsNullOrEmpty(accountNumber))
            {
                //prefix "A" or "BL" - we need to verify this in Accounts list
                //projectNavision.DimensionValue_x005B_8_x005D_ = accountNumber;
            }

            if (projectNavision.DimensionValue_x005B_7_x005D_ != projectDto.Apn)
            {
                //projectNavision.DimensionValue_x005B_7_x005D_ = projectDto.Apn;
            }

            if (projectNavision.Description != projectDto.DescriptionPart1)
            {
                projectNavision.Description = projectDto.DescriptionPart1;
            }

            if (projectNavision.Description_2 != projectDto.DescriptionPart2)
            {
                projectNavision.Description_2 = projectDto.DescriptionPart2;
            }

            if (projectNavision.Starting_Date != projectDto.StartDate)
            {
                projectNavision.Starting_Date = projectDto.StartDate;
            }

            string projectManagerAcronym;

            if (TryToGetAcronymFromEmployeeId(projectDto.ProjectManagerId, out projectManagerAcronym)  && projectManagerAcronym != projectNavision.DimensionValue_x005B_10_x005D_)
            {
                projectNavision.DimensionValue_x005B_10_x005D_ = projectManagerAcronym ?? string.Empty;
            }

            string operationalManagerAcronym;

            if (TryToGetAcronymFromEmployeeId(projectDto.OperationalManagerId, out operationalManagerAcronym) && operationalManagerAcronym != projectNavision.DimensionValue_x005B_15_x005D_)
            {
                projectNavision.DimensionValue_x005B_15_x005D_ = operationalManagerAcronym ?? string.Empty;
            }

            string salesPersonAcronym;

            if (TryToGetAcronymFromEmployeeId(projectDto.SalesPersonId, out salesPersonAcronym) && salesPersonAcronym != projectNavision.DimensionValue_x005B_17_x005D_)
            {
                projectNavision.DimensionValue_x005B_17_x005D_ = salesPersonAcronym ?? string.Empty;
            }

            string generalManagerAcronym;

            if (TryToGetAcronymFromEmployeeId(projectDto.GeneralManagerId, out generalManagerAcronym) && generalManagerAcronym != projectNavision.DimensionValue_x005B_19_x005D_)
            {
                projectNavision.DimensionValue_x005B_19_x005D_ = generalManagerAcronym ?? string.Empty;
            }

            string operationalDirectorAcronym;

            if (TryToGetAcronymFromEmployeeId(projectDto.OperationalDirectorId, out operationalDirectorAcronym) && operationalDirectorAcronym != projectNavision.DimensionValue_x005B_20_x005D_)
            {
                projectNavision.DimensionValue_x005B_20_x005D_ = operationalDirectorAcronym ?? string.Empty;
            }

            string region = GetNavisionRegionFromJiraRegion(projectDto.Region);

            if (projectNavision.DimensionValue_x005B_16_x005D_ != region && !string.IsNullOrEmpty(region))
            {
                projectNavision.DimensionValue_x005B_16_x005D_ = region;
            }
        }

        private string GetNavisionRegionFromJiraRegion(string jiraRegion)
        {
            switch (jiraRegion)
            {
                case "Poland":
                    return "PL / EUR";
                case "Germany":
                    return "GER";
                case "EMEA":
                    return "EMEA / EUR";
                case "UK":
                    return "UK / EUR";
                case "USA":
                    return "US";
                default:
                    return string.Empty;
            }
        }

        private bool TryToGetAcronymFromEmployeeId(long? userId, out string acronym)
        {
            var userIdToLoginMappings = _dimensionValueMappingCacheService.GetEmployeeIdToAcronymMappings();

            if (userId.HasValue && userIdToLoginMappings.TryGetValue(userId.Value, out acronym))
            {
                return true;
            }

            acronym = null;

            return false;
        }

        private static bool ShouldSynchronizeWithNavision(FinancialProjectDto projectDto)
        {
            if (string.IsNullOrEmpty(projectDto.Apn))
            {
                return true;
            }

            return false;
        }
    }
}