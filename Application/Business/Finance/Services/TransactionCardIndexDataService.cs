﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Finance.Dto;
using Smt.Atomic.Business.Finance.Interfaces;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Repositories.Helpers;

namespace Smt.Atomic.Business.Finance.Services
{
    public class TransactionCardIndexDataService : CustomDataCardIndexDataService<TransactionDto>, ITransactionCardIndexDataService
    {
        private readonly ITimeService _timeService;
        private readonly ITransactionService _transactionService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly ITransactionFilterBuilder _transactionFilterBuilder;

        public TransactionCardIndexDataService(
            ITimeService timeService,
            ITransactionService transactionService,
            IPrincipalProvider principalProvider,
            ITransactionFilterBuilder transactionFilterBuilder)
        {
            _timeService = timeService;
            _transactionService = transactionService;
            _principalProvider = principalProvider;
            _transactionFilterBuilder = transactionFilterBuilder;
        }

        protected override IList<TransactionDto> GetAllRecords(object context = null)
        {
            var transactionContext = context as TransactionContext;
            var currentDate = _timeService.GetCurrentDate();

            if (transactionContext == null || !transactionContext.IsSet())
            {
                transactionContext = new TransactionContext(currentDate.Year, currentDate.Month);
            }

            var from = DateHelper.BeginningOfMonth(transactionContext.Year.Value, (byte)transactionContext.Month.Value);
            var to = from.GetLastDayInMonth();

            return _transactionService.GetTransactions(from, to).Filtered().ToList();
        }

        public override TransactionDto GetRecordById(long id)
        {
            return new[] { _transactionService.GetTransactionById(id) }.Filtered().Single();
        }

        public override TransactionDto GetRecordByIdOrDefault(long id)
        {
            return new[] { _transactionService.GetTransactionByIdOrDefault(id) }.Filtered().SingleOrDefault();
        }

        protected override IEnumerable<Expression<Func<TransactionDto, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return t => t.DocumentNo;
            yield return t => t.DocumentType;
            yield return t => t.Description;
            yield return t => t.SourceName;
            yield return t => t.SourceNo;
            yield return t => t.OrgUnitCode;
            yield return t => t.CompanyCode;
            yield return t => t.LocationCode;
            yield return t => t.EntryNo;
        }

        protected override IList<TransactionDto> ApplyContextFiltering(IList<TransactionDto> records, object context)
        {
            var contextFilter = context as TransactionContext;

            if (contextFilter == null || !contextFilter.IsSet())
            {
                throw new BusinessException($"Couldn't resolve {nameof(TransactionContext)} context.");
            }

            return records.Where(i => i.PostingDate.Value.Year == contextFilter.Year.Value && i.PostingDate.Value.Month == contextFilter.Month.Value).ToList();
        }

        protected override NamedFilters<TransactionDto> NamedFilters
        {
            get
            {
                return new NamedFilters<TransactionDto>(new[]
                {
                    new NamedFilter<TransactionDto>(
                        FilterCodes.MyTransactions,
                        _transactionFilterBuilder.GetMyTransactionsFilter(_principalProvider.Current.EmployeeId, _principalProvider.Current.Id),
                        SecurityRoleType.CanViewMyTransactions),
                    new NamedFilter<TransactionDto>(
                        FilterCodes.AllTransactions,
                        a => true,
                        SecurityRoleType.CanViewAllTransactions)
                });
            }
        }
    }
}