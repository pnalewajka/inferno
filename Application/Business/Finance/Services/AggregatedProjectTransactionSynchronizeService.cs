﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Finance.Dto;
using Smt.Atomic.Business.Finance.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Finance;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Finance.Services
{
    public class AggregatedProjectTransactionSynchronizeService : IAggregatedProjectTransactionSynchronizeService
    {
        private readonly ITransactionService _transactionService;

        private readonly ITransactionSystemParametersFilterService _transactionSystemParametersFilterService;

        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _timeTrackingUnitOfWork;

        private readonly IUnitOfWorkService<IFinanceDbScope> _financeUnitOfWorkService;

        private readonly ISystemParameterService _systemParameterService;

        private readonly IClassMapping<ProjectMonthlyHourAggregationDto, AggregatedProjectTransaction>
            _aggregatedTimeReportRowDtoToAggregatedProjectTransactionsMapping;

        public AggregatedProjectTransactionSynchronizeService(
            ITransactionService transactionService,
            IUnitOfWorkService<ITimeTrackingDbScope> timeTrackingUnitOfWork,
            IUnitOfWorkService<IFinanceDbScope> financeUnitOfWorkService,
            ITransactionSystemParametersFilterService transactionSystemParametersFilterService,
            IClassMapping<ProjectMonthlyHourAggregationDto, AggregatedProjectTransaction> aggregatedTimeReportRowDtoToAggregatedProjectTransactionsMapping,
            ISystemParameterService systemParameterService)
        {
            _transactionService = transactionService;
            _transactionSystemParametersFilterService = transactionSystemParametersFilterService;
            _timeTrackingUnitOfWork = timeTrackingUnitOfWork;
            _financeUnitOfWorkService = financeUnitOfWorkService;
            _aggregatedTimeReportRowDtoToAggregatedProjectTransactionsMapping = aggregatedTimeReportRowDtoToAggregatedProjectTransactionsMapping;
            _systemParameterService = systemParameterService;
        }

        public void SynchronizeTransactions(int months)
        {
            var fromDate = DateTime.Now.AddMonths(-1 * months).GetFirstDayInMonth();
            var toDate = DateTime.Now.GetLastDayInMonth();

            SynchronizeTransactionByDate(fromDate, toDate);
            AddProjectsWithoutTransactions(fromDate, toDate);
        }

        private void SynchronizeTransactionByDate(DateTime fromDate, DateTime toDate)
        {
            var aggregatedTimeReportRowDtos = GetAggregatedTimeReportRowDtos(fromDate, toDate);
            var transactions = _transactionService.GetTransactions(fromDate, toDate);

            using (var unitOfWork = _financeUnitOfWorkService.Create())
            {
                foreach (var aggregatedTimeReportRowDto in aggregatedTimeReportRowDtos)
                {
                    var aggregatedProjectTransactions = unitOfWork
                        .Repositories
                        .AggregatedProjectTransactions
                        .SingleOrDefault(x => x.Month == aggregatedTimeReportRowDto.Month
                                           && x.Year == aggregatedTimeReportRowDto.Year
                                           && x.ProjectId == aggregatedTimeReportRowDto.ProjectId)
                        ?? _aggregatedTimeReportRowDtoToAggregatedProjectTransactionsMapping.CreateFromSource(aggregatedTimeReportRowDto);

                    var projectTransactions = transactions
                        .Where(x => x.ProjectId == aggregatedTimeReportRowDto.ProjectId && x.PostingDate.HasValue
                                 && x.PostingDate.Value.Month == aggregatedProjectTransactions.Month
                                 && x.PostingDate.Value.Year == aggregatedProjectTransactions.Year).ToList();

                    ComputeEstimatedRevenue(aggregatedProjectTransactions, transactions, aggregatedTimeReportRowDtos);
                    ComputeRevenue(projectTransactions, aggregatedProjectTransactions);
                    ComputeProjectCostAndMargin(projectTransactions, aggregatedProjectTransactions);
                    ComputeOperationCostAndMargin(projectTransactions, aggregatedProjectTransactions);
                    ComputeServiceCostAndMargin(projectTransactions, aggregatedProjectTransactions);

                    if (aggregatedProjectTransactions.Id == 0)
                    {
                        unitOfWork.Repositories.AggregatedProjectTransactions.Add(aggregatedProjectTransactions);
                    }
                    else
                    {
                        unitOfWork.Repositories.AggregatedProjectTransactions.Edit(aggregatedProjectTransactions);
                    }
                }

                unitOfWork.Commit();
            }
        }

        private void AddProjectsWithoutTransactions(DateTime fromDate, DateTime toDate)
        {
            using (var unitOfWork = _financeUnitOfWorkService.Create())
            {
                var activeProjects = unitOfWork.Repositories.Projects
                    .Where(p => p.StartDate <= toDate && (!p.EndDate.HasValue || p.EndDate.Value >= fromDate))
                    .Select(p => new { p.Id, p.StartDate, p.EndDate })
                    .ToList();

                foreach (var project in activeProjects)
                {
                    var start = project.StartDate.HasValue ? DateHelper.Max(fromDate, project.StartDate.Value) : fromDate;
                    var end = project.EndDate.HasValue ? DateHelper.Min(toDate, project.EndDate.Value) : toDate;

                    var endMonths = DateHelper.MonthsInDate(end) - 1;

                    for (var currentMonths = DateHelper.MonthsInDate(start) - 1; currentMonths <= endMonths; ++currentMonths)
                    {
                        var month = (byte)(currentMonths % 12 + 1);
                        var year = currentMonths / 12;

                        var aggregatedProjectTransaction = unitOfWork.Repositories.AggregatedProjectTransactions
                            .SingleOrDefault(t => t.ProjectId == project.Id
                                               && t.Month == month
                                               && t.Year == year)
                            ?? new AggregatedProjectTransaction { ProjectId = project.Id, Month = month, Year = year };

                        if (aggregatedProjectTransaction.Id == default(long))
                        {
                            unitOfWork.Repositories.AggregatedProjectTransactions.Add(aggregatedProjectTransaction);
                        }
                    }
                }

                unitOfWork.Commit();
            }
        }

        private void ComputeEstimatedRevenue(AggregatedProjectTransaction aggregatedProjectTransactions, IList<TransactionDto> transactions, IList<ProjectMonthlyHourAggregationDto> aggregatedTimeReportRowDtos)
        {
            var previousMonth = new DateTime(aggregatedProjectTransactions.Year, aggregatedProjectTransactions.Month, 1).AddMonths(-1);

            var previousMonthTransactions = transactions.Where(
                x => x.ProjectId == aggregatedProjectTransactions.ProjectId && x.PostingDate.HasValue
                     && x.PostingDate.Value.Month == previousMonth.Month
                     && x.PostingDate.Value.Year == previousMonth.Year).ToList();

            var previousMonthAggregatedTimeReportRowDto =
                aggregatedTimeReportRowDtos.FirstOrDefault(x => x.ProjectId == aggregatedProjectTransactions.ProjectId
                                                             && x.Month == previousMonth.Month
                                                             && x.Year == previousMonth.Year);

            if (!previousMonthTransactions.Any())
            {
                previousMonthAggregatedTimeReportRowDto = GetAggregatedTimeReportRowDtos(previousMonth, previousMonth)
                    .FirstOrDefault(x => x.ProjectId == aggregatedProjectTransactions.ProjectId
                                      && x.Month == previousMonth.Month
                                      && x.Year == previousMonth.Year);

                previousMonthTransactions = _transactionService
                    .GetTransactions(previousMonth.GetFirstDayInMonth(), previousMonth.GetLastDayInMonth())
                    .Where(x => x.ProjectId == aggregatedProjectTransactions.ProjectId && x.PostingDate.HasValue
                             && x.PostingDate.Value.Month == previousMonth.Month
                             && x.PostingDate.Value.Year == previousMonth.Year).ToList();
            }

            if (previousMonthAggregatedTimeReportRowDto == null)
            {
                aggregatedProjectTransactions.EstimatedRevenue = (decimal?)null;
            }
            else
            {
                var previousMonthAggregatedProjectTransactions =
                    _aggregatedTimeReportRowDtoToAggregatedProjectTransactionsMapping.CreateFromSource(previousMonthAggregatedTimeReportRowDto);

                ComputeRevenue(previousMonthTransactions, previousMonthAggregatedProjectTransactions);

                aggregatedProjectTransactions.EstimatedRevenue = previousMonthAggregatedProjectTransactions.Hours == 0 ? 0 :
                                                                     (previousMonthAggregatedProjectTransactions.Revenue / previousMonthAggregatedProjectTransactions.Hours)
                                                                     * aggregatedProjectTransactions.Hours;
            }
        }

        private IList<ProjectMonthlyHourAggregationDto> GetAggregatedTimeReportRowDtos(DateTime fromDate, DateTime toDate)
        {
            using (var unitOfWork = _timeTrackingUnitOfWork.Create())
            {
                var fromMonths = DateHelper.MonthsInDate(fromDate);
                var toMonths = DateHelper.MonthsInDate(toDate);

                return unitOfWork.Repositories.TimeReportRows.AsQueryable()
                    .Where(x => fromMonths <= (x.TimeReport.Year * 12 + x.TimeReport.Month)
                        && (x.TimeReport.Year * 12 + x.TimeReport.Month) <= toMonths)
                    .GroupBy(x => new { x.Project.Id, x.TimeReport.Month, x.TimeReport.Year }).Select(
                        x => new ProjectMonthlyHourAggregationDto()
                        {
                            ProjectId = x.Key.Id,
                            Month = x.Key.Month,
                            Year = x.Key.Year,
                            Hours = x.SelectMany(y => y.DailyEntries).Any() ? x.SelectMany(y => y.DailyEntries).Sum(y => y.Hours) : 0
                        })
                    .ToList();
            }
        }

        private void ComputeRevenue(IList<TransactionDto> transactions, AggregatedProjectTransaction aggregatedProjectTransactions)
        {
            if (transactions.Any(t => _transactionSystemParametersFilterService.IsRevenueTransaction(t)))
            {
                aggregatedProjectTransactions.Revenue =
                    transactions.Where(t => _transactionSystemParametersFilterService.IsRevenueTransaction(t)).Sum(t => t.AmountEur) * (-1);
            }
        }

        private void ComputeProjectCostAndMargin(IList<TransactionDto> transactions, AggregatedProjectTransaction aggregatedProjectTransactions)
        {
            aggregatedProjectTransactions.ProjectCost =
                transactions.Where(t => _transactionSystemParametersFilterService.IsProjectCostTransaction(t)).Sum(t => t.AmountEur);

            if (aggregatedProjectTransactions.Revenue.HasValue || aggregatedProjectTransactions.ProjectCost.HasValue)
            {
                aggregatedProjectTransactions.ProjectMargin = (aggregatedProjectTransactions.Revenue ?? 0) - (aggregatedProjectTransactions.ProjectCost ?? 0);
            }

            if (aggregatedProjectTransactions.Revenue.HasValue && aggregatedProjectTransactions.Revenue != 0)
            {
                aggregatedProjectTransactions.ProjectMarginPercent = (aggregatedProjectTransactions.ProjectMargin ?? 0) / aggregatedProjectTransactions.Revenue;
            }
        }

        private void ComputeOperationCostAndMargin(IList<TransactionDto> transactions, AggregatedProjectTransaction aggregatedProjectTransactions)
        {
            aggregatedProjectTransactions.OperationCost =
                transactions.Where(t => _transactionSystemParametersFilterService.IsOperationCostTransaction(t)).Sum(t => t.AmountEur);

            if (aggregatedProjectTransactions.ProjectMargin.HasValue || aggregatedProjectTransactions.OperationCost.HasValue)
            {
                aggregatedProjectTransactions.OperationMargin = (aggregatedProjectTransactions.ProjectMargin ?? 0) - (aggregatedProjectTransactions.OperationCost ?? 0);
            }

            if (aggregatedProjectTransactions.Revenue.HasValue && aggregatedProjectTransactions.Revenue != 0)
            {
                aggregatedProjectTransactions.OperationMarginPercent = (aggregatedProjectTransactions.OperationMargin ?? 0) / aggregatedProjectTransactions.Revenue;
            }
        }

        private void ComputeServiceCostAndMargin(IList<TransactionDto> transactions, AggregatedProjectTransaction aggregatedProjectTransactions)
        {
            aggregatedProjectTransactions.ServiceCost =
                transactions.Where(t => _transactionSystemParametersFilterService.IsServiceCostTransaction(t)).Sum(t => t.AmountEur);

            if (aggregatedProjectTransactions.OperationMargin.HasValue || aggregatedProjectTransactions.ServiceCost.HasValue)
            {
                aggregatedProjectTransactions.ServiceMargin = (aggregatedProjectTransactions.OperationMargin ?? 0) - (aggregatedProjectTransactions.ServiceCost ?? 0);
            }

            if (aggregatedProjectTransactions.Revenue.HasValue && aggregatedProjectTransactions.Revenue != 0)
            {
                aggregatedProjectTransactions.ServiceMarginPercent = (aggregatedProjectTransactions.ServiceMargin ?? 0) / aggregatedProjectTransactions.Revenue;
            }
        }
    }
}
