﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Smt.Atomic.Business.Finance.Dto;
using Smt.Atomic.Business.Finance.Interfaces;
using Smt.Atomic.Business.Finance.Resources;
using Smt.Atomic.Data.Navision.Entities;
using Smt.Atomic.Data.Navision.Interfaces;

namespace Smt.Atomic.Business.Finance.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly IFinanceDataService _financeDataService;
        private readonly IDimensionValueMappingCacheService _dimensionValueMappingCacheService;

        public TransactionService(
            IFinanceDataService financeDataService,
            IDimensionValueMappingCacheService dimensionValueMappingCacheService)
        {
            _financeDataService = financeDataService;
            _dimensionValueMappingCacheService = dimensionValueMappingCacheService;
        }

        public IList<TransactionDto> GetTransactions(DateTime from, DateTime to)
        {
            var loginOrAcronymToUserIdMapping = _dimensionValueMappingCacheService.GetLoginToUserIdMappings();
            var jiraApnToProjectIdMapping = _dimensionValueMappingCacheService.GetApnToProjectIdMappings();
            var jiraApnToProjectOrgUnitIdMapping = _dimensionValueMappingCacheService.GetApnToProjectOrgUnitIdMappings();
            var transactions = _financeDataService.GetTransactions(from, to);

            return transactions.Select(t => MapTransactionToTransactionDto(t, loginOrAcronymToUserIdMapping, jiraApnToProjectIdMapping, jiraApnToProjectOrgUnitIdMapping)).ToList();
        }

        public TransactionDto GetTransactionById(long id)
        {
            var transaction = GetTransactionByIdOrDefault(id);

            if (transaction == null)
            {
                throw new ArgumentException(string.Format(Exceptions.CouldntFindTransactionWithId, id));
            }

            return transaction;
        }

        public TransactionDto GetTransactionByIdOrDefault(long id)
        {
            var transaction = _financeDataService.GetTransactionByIdOrDefault(id);

            if (transaction == null)
            {
                return null;
            }

            var loginOrAcronymToUserIdMapping = _dimensionValueMappingCacheService.GetLoginToUserIdMappings();
            var jiraApnToProjectIdMapping = _dimensionValueMappingCacheService.GetApnToProjectIdMappings();
            var jiraApnToProjectOrgUnitIdMapping = _dimensionValueMappingCacheService.GetApnToProjectOrgUnitIdMappings();

            return MapTransactionToTransactionDto(transaction, loginOrAcronymToUserIdMapping, jiraApnToProjectIdMapping, jiraApnToProjectOrgUnitIdMapping);
        }

        private TransactionDto MapTransactionToTransactionDto(Transaction transaction, 
                                                              IDictionary<string, long> loginOrAcronymToUserIdMapping, 
                                                              IDictionary<string, long> jiraApnToProjectIdMapping,
                                                              IDictionary<string, long> jiraApnToProjectOrgUnitIdMapping)
        {
            return new TransactionDto
            {
                Id = transaction.Id,
                Account = transaction.Account?.Code,
                Description = transaction.Description,
                AmountPln = transaction.AmountPln,
                DocumentDate = transaction.DocumentDate,
                AccountType = transaction.AccountType?.Code,
                AmountEur = transaction.AmountEur,
                BudgetPositionCode = transaction.BudgetPosition?.Code,
                BudgetPositionName = transaction.BudgetPosition?.Name,
                CostCenterCode = transaction.CostCenter?.Code,
                CostCenterName = transaction.CostCenter?.Name,
                DocumentNo = transaction.DocumentNo,
                DocumentType = transaction.DocumentType,
                CompanyCode = transaction.Entity?.Code,
                CompanyName = transaction.Entity?.Name,
                EntryNo = transaction.EntryNo,
                ExternalDocumentNo = transaction.ExternalDocumentNo,
                GeneralLedgerAccountNo = transaction.GeneralLedgerAccountNo,
                Ifrs = transaction.Ifrs?.Code,
                LocationCode = transaction.Location?.Code,
                LocationName = transaction.Location?.Name,
                NoTax = transaction.NoTax != null,
                PostingDate = transaction.PostingDate,
                ProjectGroup = transaction.ProjectGroup?.Code,
                ProjectType = transaction.ProjectType?.Code,
                ProjectUtilization = transaction.ProjectUtilization?.Code,
                OrgUnitCode = SkipLeadingNumeration(transaction.OrgUnit?.Code),
                OrgUnitName = SkipLeadingNumeration(transaction.OrgUnit?.Name),
                ProjectId = GetEntityIdOrDefaultByCode(transaction.Project?.Code, jiraApnToProjectIdMapping),
                ProjectOrgUnitId = GetEntityIdOrDefaultByCode(transaction.Project?.Code, jiraApnToProjectOrgUnitIdMapping),
                SourceCode = transaction.SourceCode,
                SourceCompanyName = transaction.SourceCompanyName,
                Region = transaction.Region?.Code,
                SourceName = transaction.SourceName,
                SourceNo = transaction.SourceNo,
                SourceType = transaction.SourceType,
                Vertical = transaction.Vertical?.Code,
                OperationalDirectorUserId = GetEntityIdOrDefaultByCode(transaction.OperationalDirector?.Code, loginOrAcronymToUserIdMapping),
                GeneralManagerUserId = GetEntityIdOrDefaultByCode(transaction.GeneralManager?.Code, loginOrAcronymToUserIdMapping),
                OperationalManagerUserId = GetEntityIdOrDefaultByCode(transaction.OperationalManager?.Code, loginOrAcronymToUserIdMapping),
                ProjectManagerUserId = GetEntityIdOrDefaultByCode(transaction.ProjectManager?.Code, loginOrAcronymToUserIdMapping),
                SalesPersonUserId = GetEntityIdOrDefaultByCode(transaction.SalesPerson?.Code, loginOrAcronymToUserIdMapping),
                CreatedByUserId = GetEntityIdOrDefaultByCode(transaction.UserId, loginOrAcronymToUserIdMapping)
            };
        }

        private long? GetEntityIdOrDefaultByCode(string code, IDictionary<string, long> codeToEntityIdMapping)
        {
            if (string.IsNullOrEmpty(code))
            {
                return null;
            }

            long id;

            if (codeToEntityIdMapping.TryGetValue(code.ToLower(), out id))
            {
                return id;
            }

            return null;
        }

        private string SkipLeadingNumeration(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }

            return Regex.Replace(value, @"^\s*\d*\.?\d*\s*", "", RegexOptions.Compiled);
        }
    }
}