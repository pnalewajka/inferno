﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Castle.Core;
using Smt.Atomic.Business.Finance.Dto;
using Smt.Atomic.Business.Finance.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.Data.Entities.Modules.Finance;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Finance.Services
{
    [Interceptor(typeof(CacheInterceptor))]
    public class AggregatedProjectTransactionsService : IAggregatedProjectTransactionsService
    {
        private readonly IUnitOfWorkService<IFinanceDbScope> _financeUnitOfWorkService;

        private readonly IClassMapping<AggregatedProjectTransaction, AggregatedProjectTransactionsDto> _entityToDtoMapping;

        public AggregatedProjectTransactionsService(
            IUnitOfWorkService<IFinanceDbScope> financeUnitOfWorkService,
            IClassMapping<AggregatedProjectTransaction, AggregatedProjectTransactionsDto> entityToDtoMapping)
        {
            _financeUnitOfWorkService = financeUnitOfWorkService;
            _entityToDtoMapping = entityToDtoMapping;
        }

        public IList<AggregatedProjectTransactionsDto> GetAggregatedProjectTransactions(int year, int month)
        {
            if (year < 1 || year > 9999)
            {
                throw new ArgumentOutOfRangeException(nameof(year));
            }

            if (month < 1 || month > 12)
            {
                throw new ArgumentOutOfRangeException(nameof(month));
            }

            var aggregatedProjectTransactionsDtos = new List<AggregatedProjectTransactionsDto>();

            using (var financeUnitOfWork = _financeUnitOfWorkService.Create())
            {
                aggregatedProjectTransactionsDtos = financeUnitOfWork
                    .Repositories
                    .AggregatedProjectTransactions
                    .AsQueryable()
                    .Include("Project")
                    .Where(x => x.Month == month && x.Year == year)
                    .ToList()
                    .Select(x => _entityToDtoMapping.CreateFromSource(x))
                    .ToList();
            }

            return aggregatedProjectTransactionsDtos;
        }

        [Cached(ExpirationInSeconds = 3600)]
        public AggregatedProjectTransactionsDto GetAggregatedProjectTransactionsById(long id)
        {
            AggregatedProjectTransactionsDto aggregatedProjectTransactionsDto = null;

            using (var financeUnitOfWork = _financeUnitOfWorkService.Create())
            {
                var aggregatedProjectTransactions = financeUnitOfWork.Repositories
                    .AggregatedProjectTransactions
                    .AsQueryable()
                    .Include("Project")
                    .FirstOrDefault(x => x.Id == id);

                aggregatedProjectTransactionsDto = _entityToDtoMapping.CreateFromSource(aggregatedProjectTransactions);
            }

            return aggregatedProjectTransactionsDto;
        }
    }
}