﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Finance.Dto;
using Smt.Atomic.Business.Finance.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.Business.Finance.Services
{
    public class ProjectTransactionCardIndexDataService : TransactionCardIndexDataService, IProjectTransactionCardIndexDataService
    {
        private readonly ITransactionSystemParametersFilterService _transactionSystemParametersFilterService;

        public ProjectTransactionCardIndexDataService(
            ITimeService timeService,
            ITransactionService transactionService,
            IPrincipalProvider principalProvider,
            ITransactionFilterBuilder transactionFilterBuilder,
            ITransactionSystemParametersFilterService transactionSystemParametersFilterService)
          : base(timeService, transactionService, principalProvider, transactionFilterBuilder)
        {
            _transactionSystemParametersFilterService = transactionSystemParametersFilterService;
        }

        protected override IList<TransactionDto> GetAllRecords(object context = null)
        {
            return base.GetAllRecords(context).Where(t => _transactionSystemParametersFilterService.HasAllowedCostCenter(t)).ToList();
        }

        protected override IList<TransactionDto> ApplyContextFiltering(IList<TransactionDto> records, object context)
        {
            var contextFilter = context as ProjectTransactionContext;

            if (contextFilter == null || !contextFilter.IsSet())
            {
                throw new BusinessException($"Couldn't resolve {nameof(ProjectTransactionContext)} context.");
            }

            return 
                 records
                .Where(i => 
                    i.PostingDate.Value.Year == contextFilter.Year.Value && 
                    i.PostingDate.Value.Month == contextFilter.Month.Value &&
                    i.ProjectId == contextFilter.ParentId)
                .ToList();
        }
    }
}