﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.Finance.Helpers;
using Smt.Atomic.Business.Finance.Interfaces;
using Smt.Atomic.Business.Finance.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.Finance
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            container.Register(Component.For<ITransactionService>().ImplementedBy<TransactionService>().LifestyleTransient());
            container.Register(Component.For<ITransactionCardIndexDataService>().ImplementedBy<TransactionCardIndexDataService>().LifestyleTransient());
            container.Register(Component.For<IProjectTransactionCardIndexDataService>().ImplementedBy<ProjectTransactionCardIndexDataService>().LifestyleTransient());
            container.Register(Component.For<IDimensionValueMappingCacheService>().ImplementedBy<DimensionValueMappingCacheService>().LifestyleTransient());
            container.Register(Component.For<IProjectSynchronizationService>().ImplementedBy<ProjectSynchronizationService>().LifestyleTransient());
            container.Register(Component.For<IAggregatedProjectTransactionSynchronizeService>().ImplementedBy<AggregatedProjectTransactionSynchronizeService>().LifestyleTransient());
            container.Register(Component.For<IAggregatedProjectTransactionsCardIndexDataService>().ImplementedBy<AggregatedProjectTransactionsCardIndexDataService>().LifestyleTransient());
            container.Register(Component.For<IAggregatedProjectTransactionsService>().ImplementedBy<AggregatedProjectTransactionsService>().LifestyleTransient());
            container.Register(Component.For<ITransactionFilterBuilder>().ImplementedBy<TransactionFilterBuilder>().LifestyleTransient());
            container.Register(Component.For<ITransactionSystemParametersFilterService>().ImplementedBy<TransactionSystemParametersFilterService>().LifestyleTransient());
            container.Register(Component.For<IGlobalRateCardIndexDataService>().ImplementedBy<GlobalRateCardIndexDataService>().LifestyleTransient());
        }
    }
}