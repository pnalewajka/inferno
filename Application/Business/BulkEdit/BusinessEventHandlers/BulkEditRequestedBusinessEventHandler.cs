﻿using System;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.BulkEdit.Dto;
using Smt.Atomic.Business.Common.BusinessEvents;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Business.Configuration.Interfaces;

namespace Smt.Atomic.Business.BulkEdit.BusinessEventHandlers
{
    public class BulkEditRequestedBusinessEventHandler : BusinessEventHandler<BulkEditEvent>
    {
        private readonly IReliableEmailService _reliableEmailService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IUserCardIndexService _userCardIndexService;

        public BulkEditRequestedBusinessEventHandler(
            IReliableEmailService reliableEmailService,
            IMessageTemplateService messageTemplateService,
            IUserCardIndexService userCardIndexService)
        {
            _reliableEmailService = reliableEmailService;
            _messageTemplateService = messageTemplateService;
            _userCardIndexService = userCardIndexService;
        }

        public override void Handle(BulkEditEvent businessEvent)
        {
            var result = ProcessBulkEdit(businessEvent);

            if (!result.IsSuccessful)
            {
                SendNotification(
                    TemplateCodes.BulkEditErrorMessageSubject,
                    TemplateCodes.BulkEditErrorMessageBody,
                    businessEvent,
                    result);

                return;
            }

            SendNotification(
                TemplateCodes.BulkEditSuccessMessageSubject,
                TemplateCodes.BulkEditSuccessMessageBody,
                businessEvent,
                result);
        }

        private BoolResult ProcessBulkEdit(BulkEditEvent businessEvent)
        {
            var dtoType = TypeHelper.FindTypeInLoadedAssemblies(businessEvent.DtoType);
            var property = dtoType.GetProperty(businessEvent.PropertyName);

            if (property == null)
            {
                throw new InvalidOperationException($"Property '{businessEvent.PropertyName}' not found");
            }

            var cardIndexDataServiceType = TypeHelper.FindTypeInLoadedAssemblies(businessEvent.CardIndexDataServiceType);
            dynamic cardIndexDataService = ReflectionHelper.ResolveInterface(cardIndexDataServiceType);

            var actionResult = new BoolResult(true);

            using (var transaction = TransactionHelper.CreateDefaultTransactionScope())
            {
                foreach (var entityId in businessEvent.RecordIds)
                {
                    var entityDto = cardIndexDataService.GetRecordById(entityId);
                    property.SetValue(entityDto, businessEvent.PropertyValue);
                    EditRecordResult result;

                    try
                    {
                        result = cardIndexDataService.EditRecord(entityDto);
                    }
                    catch (BusinessException businessException)
                    {
                        actionResult = new BoolResult();
                        actionResult.AddAlert(AlertType.Error, businessException.Message, isDismissable: false);

                        return actionResult;
                    }

                    if (!result.IsSuccessful)
                    {
                        if (result.UniqueKeyViolations != null && result.UniqueKeyViolations.Any())
                        {
                            throw new BusinessException("BulkEdit cannot be run on properties with unique key constraint!");
                        }

                        return new BoolResult(isSuccessful: false, alerts: result.Alerts);
                    }

                    if (result.Alerts != null && result.Alerts.Count > 0)
                    {
                        actionResult.Alerts.AddRange(result.Alerts);
                    }
                }

                transaction.Complete();
            }

            return actionResult;
        }

        private void SendNotification(string subjectTemplateName, string bodyTemplateName, BulkEditEvent businessEvent, BoolResult result)
        {
            var messageDto = new BulkEditNotificationMessageDto { Messages = result.Alerts.Select(a => a.Message) };
            var subject = _messageTemplateService.ResolveTemplate(subjectTemplateName, messageDto);
            var body = _messageTemplateService.ResolveTemplate(bodyTemplateName, messageDto);

            var user = _userCardIndexService.GetRecordById(businessEvent.ModifyingUserId);
            var recipients = new List<EmailRecipientDto> { new EmailRecipientDto(user.Email, user.GetFullName()) };

            var emailDto = new EmailMessageDto
            {
                Body = body.Content,
                Subject = subject.Content,
                IsHtml = body.IsHtml,
                Recipients = recipients
            };

            _reliableEmailService.EnqueueMessage(emailDto);
        }
    }
}
