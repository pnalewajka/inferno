﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.EventSourcing.Interfaces;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.BulkEdit
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            switch (containerType)
            {
                case ContainerType.JobScheduler:
                case ContainerType.WebApi:
                case ContainerType.WebApp:
                case ContainerType.WebServices:
                case ContainerType.UnitTests:
                    container.Register(Classes.FromThisAssembly().BasedOn<IBusinessEventHandler>().WithService.FromInterface().LifestyleTransient());
                    break;
            }
        }
    }
}

