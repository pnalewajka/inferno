﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.BulkEdit.Dto
{
    public class BulkEditNotificationMessageDto
    {
        public IEnumerable<string> Messages { get; set; }
    }
}
