﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Smt.Atomic.Business.TimeTracking.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class OvertimeRequestResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal OvertimeRequestResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Smt.Atomic.Business.TimeTracking.Resources.OvertimeRequestResource", typeof(OvertimeRequestResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Date from must be greater than date to.
        /// </summary>
        internal static string DateFromGreaterThanDateTo_ErrorMessage {
            get {
                return ResourceManager.GetString("DateFromGreaterThanDateTo_ErrorMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Both dates must be set.
        /// </summary>
        internal static string DatesMustBeSet_ErrorMessage {
            get {
                return ResourceManager.GetString("DatesMustBeSet_ErrorMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Project must have approver..
        /// </summary>
        internal static string NoApproverForProject_ErrorMessage {
            get {
                return ResourceManager.GetString("NoApproverForProject_ErrorMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You cannot request for overtime because you are not employed.
        /// </summary>
        internal static string NotAnEmployee_ErrorMessage {
            get {
                return ResourceManager.GetString("NotAnEmployee_ErrorMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Overtime request for {2} hour(s) between {0:d} and {1:d}.
        /// </summary>
        internal static string RequestDescriptionFormat {
            get {
                return ResourceManager.GetString("RequestDescriptionFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Overtime hours have to be greater than zero.
        /// </summary>
        internal static string TotalHoursGreaterThanZero_ErrorMessage {
            get {
                return ResourceManager.GetString("TotalHoursGreaterThanZero_ErrorMessage", resourceCulture);
            }
        }
    }
}
