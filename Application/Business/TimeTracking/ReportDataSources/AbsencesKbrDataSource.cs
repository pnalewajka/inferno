﻿using System;
using System.Linq;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.ReportModels;
using Smt.Atomic.Business.TimeTracking.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.ReportDataSources
{
    [Identifier("DataSources.AbsencesKbrDataSource")]
    [RequireRole(SecurityRoleType.CanGenerateAbsencesKbrReport)]
    [DefaultReportDefinition(
        "AbsencesKbr",
        nameof(AbsenceRequestResources.AbsencesKbrDataSourceName),
        nameof(AbsenceRequestResources.AbsencesKbrDataSourceDescription),
        MenuAreas.TimeTracking,
        MenuGroups.TimeTrackingReportsHR,
        ReportingEngine.MsExcel,
        "Smt.Atomic.Business.TimeTracking.ReportTemplates.AbsencesKbrReport.xlsx",
        typeof(AbsenceRequestResources))]
    public class AbsencesKbrDataSource : AbsencesDataSourceBase<AbsencesKbrDataSourceParametersDto>
    {
        private const string IssueKey = "ABSENCE";

        public AbsencesKbrDataSource(ITimeService timeService, IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService)
        : base(timeService, unitOfWorkService)
        {
        }

        public override AbsencesKbrDataSourceParametersDto GetDefaultParameters(ReportGenerationContext<AbsencesKbrDataSourceParametersDto> reportGenerationContext)
        {
            var yesterday = TimeService.GetCurrentDate().AddDays(-1);

            return new AbsencesKbrDataSourceParametersDto
            {
                AbsencesFrom = DateHelper.BeginningOfMonth(yesterday),
                AbsencesTo = yesterday,
            };
        }

        public override object GetData(ReportGenerationContext<AbsencesKbrDataSourceParametersDto> reportGenerationContext)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var employeeAbsences = FilterDataScope(unitOfWork.Repositories, reportGenerationContext.Parameters);
                var absencesKbr = employeeAbsences.Select(CreateReportModel).OrderBy(a => a.From).ToList();

                return new AbsencesKbrReportModel
                {
                    AbsencesKbr = absencesKbr
                };
            }
        }

        protected override IQueryable<EmployeeAbsence> FilterByDatePeriod(AbsencesKbrDataSourceParametersDto parameters, IQueryable<EmployeeAbsence> rows)
        {
            var absencesFrom = parameters.AbsencesFrom.Date;
            var absencesTo = parameters.AbsencesTo.Date;

            if (absencesFrom <= absencesTo)
            {
                rows = rows.Where(DateRangeHelper.OverlapingDateRange<EmployeeAbsence>(r => r.From, r => r.To, absencesFrom, absencesTo));
            }

            if (parameters.ApprovedTo.HasValue)
            {
                var approvedTo = parameters.ApprovedTo.Value;
                rows = rows.Where(a => a.ApprovedOn <= approvedTo);
            }

            if (parameters.ApprovedFrom.HasValue)
            {
                var approvedFrom = parameters.ApprovedFrom.Value;
                rows = rows.Where(a => a.ApprovedOn >= approvedFrom);
            }

            return rows;
        }

        private AbsenceKbrReportModel CreateReportModel(EmployeeAbsence absence)
        {
            return new AbsenceKbrReportModel
            {
                IssueType = absence.AbsenceType.KbrCode,
                IssueKey = GetIssueKey(absence),
                IssueId = absence.Id,
                Summary = GetSummary(absence),
                Reporter = absence.Employee.Acronym,
                Acronym = absence.Employee.Acronym,
                From = FormatDateTime(absence.From, true),
                To = FormatDateTime(absence.To, true),
                ApprovedOn = FormatDateTime(absence.ApprovedOn),
                RequestingPerson = absence.Request.RequestingUser.FullName,
                AffectedUser = absence.Request.AbsenceRequest.AffectedUser.FullName
            };
        }

        private static string GetIssueKey(EmployeeAbsence absence)
        {
            return $"{IssueKey}-{absence.Id}";
        }

        private static string GetSummary(EmployeeAbsence absence)
        {
            return $"{absence.AbsenceType.KbrCode} {absence.From:yyyy-MM-dd} do {absence.To:yyyy-MM-dd}";
        }

        private static string FormatDateTime(DateTime date, bool zeroHour = false)
        {
            string format = zeroHour ? "dd/MMM/yy 00:00" : "dd/MMM/yy hh:mm";

            return date.ToInvariantString(format);
        }
    }
}