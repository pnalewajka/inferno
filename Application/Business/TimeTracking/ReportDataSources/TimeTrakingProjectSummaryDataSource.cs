﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Helpers;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.ReportModels;
using Smt.Atomic.Business.TimeTracking.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.ReportDataSources
{
    [Identifier("DataSources.TimeTrakingProjectSummaryDataSource")]
    [RequireRole(SecurityRoleType.CanGenerateTimeTrakingProjectSummaryReport)]
    [DefaultReportDefinition("TimeTrakingProjectSummary",
        nameof(TimeReportServiceResources.TimeTrakingProjectSummaryName),
        nameof(TimeReportServiceResources.TimeTrakingProjectSummaryDescription),
        MenuAreas.TimeTracking,
        MenuGroups.TimeTrackingReportsTT,
        ReportingEngine.MsExcel,
        "Smt.Atomic.Business.TimeTracking.ReportTemplates.TimeTrakingProjectSummary.xlsx",
        typeof(TimeReportServiceResources))]
    public class TimeTrakingProjectSummaryDataSource : BaseReportDataSource<TimeTrakingProjectSummaryParametersDto>
    {
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService;
        private readonly ITimeService _timeService;
        
        private const string DefaultTaskDescription = "Task";

        public TimeTrakingProjectSummaryDataSource(
            IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
            ITimeService timeService)
        {
            _unitOfWorkService = unitOfWorkService;
            _timeService = timeService;
        }

        public override object GetData(ReportGenerationContext<TimeTrakingProjectSummaryParametersDto> reportGenerationContext)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var tasks = CalculateDays(unitOfWork, reportGenerationContext.Parameters).ToList();

                ExcelWorksheetHelper.PrepareExtensibleColumn(
                    tasks,
                    t => t.Attributes,
                    "",
                    "{0}");

                foreach(var dataColumn in tasks.SelectMany(t => t.Attributes))
                {
                    dataColumn.ColumnHeaderFactory = (a, b) => "";
                }

                return new TimeTrackingProjectSummaryReportModel
                {
                    ProjectName = GetProjectName(unitOfWork, reportGenerationContext.Parameters.ProjectId),
                    Tasks = tasks
                };
            }
        }

        public override TimeTrakingProjectSummaryParametersDto GetDefaultParameters(ReportGenerationContext<TimeTrakingProjectSummaryParametersDto> reportGenerationContext)
        {
            var currentDate = _timeService.GetCurrentDate();

            return new TimeTrakingProjectSummaryParametersDto
            {
                StartDate = DateHelper.BeginningOfMonth(currentDate),
                EndDate = DateHelper.EndOfMonth(currentDate)
            };
        }

        private IEnumerable<DataColumn> DatesInRange(DateTime from, DateTime to, IReadOnlyCollection<TimeReportDailyEntry> dailyEntries, object defaultValue)
        {
            var days = (to - from).TotalDays;
            for (var i = 0; i < days; i++)
            {
                var date = from.AddDays(i);
                var currentDateValue = dailyEntries.FirstOrDefault(de => de.Day == date);

                if (currentDateValue == null)
                {
                    yield return new DataColumn { Value = defaultValue, ColumnName = from.AddDays(i).ToShortDateString() };
                }
                else
                {
                    yield return new DataColumn { Value = currentDateValue.Hours, ColumnName = from.AddDays(i).ToShortDateString() };
                }
            }
        }

        private IEnumerable<ProjectSummaryReportModel> CalculateGroupEntries(string taskName, IReadOnlyCollection<TimeReportDailyEntry> elements, ICollection<DataColumn> defaultColumns, DateTime from, DateTime to)
        {
            if (!elements.Any())
            {
                yield break;
            }

            yield return new ProjectSummaryReportModel
            {
                GroupName = taskName,
                TaskHoursSum = elements.Sum(degv => degv.Hours),
                Days = defaultColumns
            };

            var dailyEntriesGroups = elements.GroupBy(de => de.TimeReportRow.TimeReport.Employee);

            foreach (var dailyEntryGroup in dailyEntriesGroups)
            {
                yield return new ProjectSummaryReportModel
                {
                    EmployeeName = dailyEntryGroup.Key.FirstName + " " + dailyEntryGroup.Key.LastName,
                    TaskHoursSum = dailyEntryGroup.Sum(deg => deg.Hours),
                    Days = defaultColumns
                };

                foreach (var taskGroup in dailyEntryGroup.GroupBy(deg => new { deg.TimeReportRow.TaskName, deg.TimeReportRow.AttributeValues }))
                {
                    yield return new ProjectSummaryReportModel
                    {
                        TaskName = taskGroup.Key.TaskName,
                        Days = DatesInRange(from, to, taskGroup.ToList(), 0)
                            .ToList(),
                        TaskHoursSum = taskGroup.Sum(ts => ts.Hours),
                        Attributes = taskGroup.Key.AttributeValues.Select(v => new DataColumn
                        {
                            Value = v.Name,
                            ColumnName = string.Empty
                        }).ToList(),
                    };
                }
            }
        }

        private string GetProjectName(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, long projectId)
        {
            var projectName = unitOfWork.Repositories.Projects
                .Where(p => p.Id == projectId)
                .Select<Project, string>(ProjectBusinessLogic.ClientProjectName)
                .Single();

            return projectName;
        }

        private IEnumerable<ProjectSummaryReportModel> CalculateDays(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, TimeTrakingProjectSummaryParametersDto parameters)
        {
            var startDate = parameters.StartDate.StartOfDay();
            var endDate = parameters.EndDate.EndOfDay();

            var dailyEntries = unitOfWork
                .Repositories
                .DailyEntries
                .Include($"{nameof(TimeReportRow)}.{nameof(TimeReportRow.TimeReport)}")
                .Where(de =>
                    de.TimeReportRow.ProjectId == parameters.ProjectId &&
                    de.Day <= endDate &&
                    de.Day >= startDate
                ).ToList();

            var emptyDays = DatesInRange(startDate, endDate, new List<TimeReportDailyEntry>(), string.Empty).ToList();
            var groupEntries = CalculateGroupEntries(DefaultTaskDescription, dailyEntries, emptyDays, startDate, endDate);

            foreach (var projectSummaryReportModel in groupEntries)
            {
                yield return projectSummaryReportModel;
            }
        }
    }
}
