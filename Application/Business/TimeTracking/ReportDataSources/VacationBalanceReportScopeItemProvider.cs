using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.Business.TimeTracking.ReportDataSources
{
    public class VacationBalanceReportScopeItemProvider : IListItemProvider
    {
        private readonly IPrincipalProvider _principalProvider;

        public VacationBalanceReportScopeItemProvider(IPrincipalProvider principalProvider)
        {
            _principalProvider = principalProvider;
        }

        public IEnumerable<ListItem> GetItems()
        {
            var listItems = EnumHelper
                .GetEnumValues(typeof(ReportScopeEnum))
                .Select(enumValue => new ListItem
                {
                    Id = Convert.ToInt64((object) enumValue),
                    DisplayName = enumValue.GetDescriptionOrValue()
                });

            if (!_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateVacationBalanceReportForAllEmployees))
            {
                return listItems.Where(i => i.Id != Convert.ToInt64((object) ReportScopeEnum.AllEmployees));
            }

            return listItems;
        }
    }
}