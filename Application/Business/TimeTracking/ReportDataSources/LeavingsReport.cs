﻿using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.Business.TimeTracking.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.ReportDataSources
{
    [Identifier("DataSource.LeavingsReport")]
    [RequireRole(SecurityRoleType.CanGenerateLeavingsReport)]
    [DefaultReportDefinition(
    "LeavingsReport",
    nameof(TimeReportServiceResources.LeavingsReportName),
    nameof(TimeReportServiceResources.LeavingsReportDescription),
    MenuAreas.TimeTracking,
    MenuGroups.TimeTrackingReportsHR,
    ReportingEngine.PivotTable,
    new[]
    {
            "Smt.Atomic.Business.TimeTracking.ReportTemplates.LeavingsReport-config.js",
            "Smt.Atomic.Business.TimeTracking.ReportTemplates.LeavingsReport.manifest",
            "Smt.Atomic.Business.TimeTracking.ReportTemplates.LeavingsReport.sql"
    },
    typeof(TimeReportServiceResources))]
    public class LeavingsReport : SqlBasedDataSource
    {
        public LeavingsReport(IUnitOfWorkService<IDbScope> unitOfWork, IPrincipalProvider principalProvider, IUnitOfWorkService<IAccountsDbScope> unitOfWorkService)
            : base(unitOfWork, principalProvider, unitOfWorkService)
        {
        }
    }
}