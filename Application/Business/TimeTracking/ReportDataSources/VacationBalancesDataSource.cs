﻿using System.Linq;
using Castle.Core.Internal;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.Business.TimeTracking.Consts;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.ReportModels;
using Smt.Atomic.Business.TimeTracking.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.ReportDataSources
{
    [Identifier("DataSources.VacationBalancesDataSource")]
    [RequireRole(SecurityRoleType.CanGenerateVacationBalancesReport)]
    [DefaultReportDefinition("VacationBalances",
        nameof(VacationBalanceResources.VacationBalancesName),
        nameof(VacationBalanceResources.VacationBalancesDescription),
        MenuAreas.TimeTracking,
        MenuGroups.TimeTrackingReportsHR,
        ReportingEngine.MsExcel,
        "Smt.Atomic.Business.TimeTracking.ReportTemplates.VacationBalancesReport.xlsx",
        typeof(VacationBalanceResources))]
    public class VacationBalancesDataSource : BaseReportDataSource<VacationBalanceParametersDto>
    {
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService;
        private readonly ITimeService _timeService;
        private readonly IOrgUnitService _orgUnitService;
        private readonly IPrincipalProvider _principalProvider;

        public VacationBalancesDataSource(
            IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
            IPrincipalProvider principalProvider,
            IOrgUnitService orgUnitService,
            ITimeService timeService)
        {
            _unitOfWorkService = unitOfWorkService;
            _timeService = timeService;
            _orgUnitService = orgUnitService;
            _principalProvider = principalProvider;
        }

        public override object GetData(ReportGenerationContext<VacationBalanceParametersDto> reportGenerationContext)
        {
            var managerOrgUnitsIds = _orgUnitService.GetManagerOrgUnitDescendantIds(_principalProvider.Current.EmployeeId);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var parameters = reportGenerationContext.Parameters;

                var employees = unitOfWork.Repositories.Employees
                    .Where(e =>
                        e.EmploymentPeriods
                            .Where(p => p.StartDate <= parameters.EffectiveOn && (p.EndDate >= parameters.EffectiveOn || p.EndDate == null))
                            .Any());

                IQueryable<OvertimeRequest> overtimeRequests = unitOfWork.Repositories.OvertimeRequests;

                if (parameters.EmployeeScope == ReportScopeEnum.MyEmployees)
                {
                    employees = employees.Where(EmployeeBusinessLogic.IsEmployeeOf.Parametrize(_principalProvider.Current.EmployeeId, managerOrgUnitsIds));
                }

                if (parameters.EmployeeScope == ReportScopeEnum.AllEmployees && !_principalProvider.Current.IsInRole(SecurityRoleType.CanGenerateVacationBalanceReportForAllEmployees))
                {
                    throw new BusinessException(VacationBalanceResources.CantGenerateReportForAllEmployees);
                }

                if (!parameters.ContractTypeIds.IsNullOrEmpty())
                {
                    employees = employees.Where(e =>
                                    e.EmploymentPeriods
                                    .Where(p => p.StartDate < parameters.EffectiveOn)
                                    .Any(p => parameters.ContractTypeIds.Contains(p.ContractTypeId)));
                }

                var employeesData =
                    from employee in employees
                    let vacationBalance = employee.VacationBalances.Where(b => b.EffectiveOn <= parameters.EffectiveOn).OrderByDescending(b => b.EffectiveOn).Select(e => e.TotalHourBalance).FirstOrDefault()
                    let vacationTakenSum = employee.VacationBalances.Where(b => b.EffectiveOn <= parameters.EffectiveOn && b.Operation == VacationOperation.Vacation).Select(b => -b.HourChange).DefaultIfEmpty().Sum()
                    let overtimeBank = employee.OvertimeBanks.Where(b => b.EffectiveOn <= parameters.EffectiveOn).OrderByDescending(b => b.EffectiveOn).ThenByDescending(b => b.Id).FirstOrDefault()
                    let contractedHours = employee.EmploymentPeriods.Where(p => p.StartDate <= parameters.EffectiveOn).OrderByDescending(p => p.StartDate).Select(e => e.VacationHourToDayRatio).FirstOrDefault()
                    select new VacationBalanceReportModel
                    {
                        Acronym = employee.Acronym,
                        EmployeeId = employee.Id,
                        UserId = employee.UserId ?? default(long),
                        EmployeeFirstName = employee.FirstName,
                        EmployeeLastName = employee.LastName,
                        VacationBalance = vacationBalance,
                        VacationBalanceInDays = contractedHours != 0 ? vacationBalance / contractedHours : default(decimal),
                        VacationTakenSum = vacationTakenSum,
                        OvertimeBankBalance = overtimeBank != null ? overtimeBank.OvertimeBalance : default(decimal),
                        OvertimeBankBalanceEvectiveOn = overtimeBank.EffectiveOn != null ? overtimeBank.EffectiveOn.ToString() : string.Empty,
                        CompanyName = employee.Company != null ? employee.Company.Name : string.Empty,
                        LocationName = employee.Location != null ? employee.Location.Name : string.Empty,
                        OrgUnitName = employee.OrgUnit != null ? employee.OrgUnit.Name : string.Empty,
                        LineManagerName = employee.LineManager.FirstName + " " + employee.LineManager.LastName,
                        VacationRequestedHours = employee.User.AbsenceRequests
                                                     .Where(r => r.Request.Status == RequestStatus.Pending)
                                                     .Select(a => a.Hours)
                                                     .DefaultIfEmpty()
                                                     .Sum(),
                        VacationPlannedHours = employee.User.AbsenceRequests
                                                   .Where(r => r.Request.Status == RequestStatus.Completed && r.From >= parameters.EffectiveOn)
                                                   .Select(a => a.Hours)
                                                   .DefaultIfEmpty()
                                                   .Sum(),
                        VacationLeaveRequestedHours = employee.User.AbsenceRequests
                                                     .Where(r => r.Request.Status == RequestStatus.Pending
                                                      && r.AbsenceType.Code == AbsenceCodes.VacationLeaveCode)
                                                     .Select(a => a.Hours)
                                                     .DefaultIfEmpty()
                                                     .Sum(),
                        VacationLeavePlannedHours = employee.User.AbsenceRequests
                                                   .Where(r => r.Request.Status == RequestStatus.Completed && r.From >= parameters.EffectiveOn
                                                    && r.AbsenceType.Code == AbsenceCodes.VacationLeaveCode)
                                                   .Select(a => a.Hours)
                                                   .DefaultIfEmpty()
                                                   .Sum(),
                    };

                var result = employeesData.ToList();

                var userIds = result.Select(b => b.UserId).ToList();

                var overtimeData = overtimeRequests
                                       .Where(r => userIds.Contains(r.Request.RequestingUserId))
                                       .GroupBy(r => r.Request.RequestingUserId)
                                       .Select(g => new
                                       {
                                           UserId = g.Key,
                                           OvertimePlanned =
                                               g.Where(gt => gt.Request.Status == RequestStatus.Completed && gt.From >= parameters.EffectiveOn)
                                                   .Select(a => a.HourLimit)
                                                   .DefaultIfEmpty()
                                                   .Sum(),
                                           OvertimeRequested =
                                               g.Where(gt => gt.Request.Status == RequestStatus.Pending)
                                                   .Select(a => a.HourLimit)
                                                   .DefaultIfEmpty()
                                                   .Sum()
                                       }).ToList();

                foreach (var employee in result)
                {
                    employee.OvertimePlanned = overtimeData
                                                   .Where(d => d.UserId == employee.UserId)
                                                   .Select(d => d.OvertimePlanned).FirstOrDefault();
                    employee.OvertimeRequested = overtimeData
                                                     .Where(d => d.UserId == employee.UserId)
                                                     .Select(d => d.OvertimeRequested).FirstOrDefault();
                }

                return new VacationBalancesReportModel
                {
                    VacationBalances = result
                };
            }
        }

        public override VacationBalanceParametersDto GetDefaultParameters(ReportGenerationContext<VacationBalanceParametersDto> reportGenerationContext)
        {
            return new VacationBalanceParametersDto
            {
                EffectiveOn = _timeService.GetCurrentDate(),
                ContractTypeIds = new long[0]
            };
        }
    }
}
