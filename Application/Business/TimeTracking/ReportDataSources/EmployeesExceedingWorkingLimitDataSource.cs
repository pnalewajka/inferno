﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Castle.Core.Internal;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.ReportModels;
using Smt.Atomic.Business.TimeTracking.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Comparers;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.ReportDataSources
{
    [Identifier("DataSources.EmployeesExceedingWorkingLimitDataSource")]
    [RequireRole(SecurityRoleType.CanGenerateEmployeesExceedingWorkingLimitReport)]
    [DefaultReportDefinition(
        "EmployeesExceedingWorkingLimit",
        nameof(TimeReportServiceResources.EmployeesExceedingWorkingLimitDataSourceName),
        nameof(TimeReportServiceResources.EmployeesExceedingWorkingLimitDataSourceDescription),
        MenuAreas.TimeTracking,
        MenuGroups.TimeTrackingReportsHR,
        ReportingEngine.MsExcel,
        "Smt.Atomic.Business.TimeTracking.ReportTemplates.EmployeesExceedingWorkingLimit.xlsx",
        typeof(TimeReportServiceResources))]

    public class EmployeesExceedingWorkingLimitDataSource : BaseReportDataSource<EmployeesExceedingWorkingLimitParametersDto>
    {
        private readonly ITimeService _timeService;
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService;

        public EmployeesExceedingWorkingLimitDataSource(
            ITimeService timeService,
            IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService)
        {
            _timeService = timeService;
            _unitOfWorkService = unitOfWorkService;
        }

        public override object GetData(ReportGenerationContext<EmployeesExceedingWorkingLimitParametersDto> reportGenerationContext)
        {
            var daysInMonth = DateTime.DaysInMonth(reportGenerationContext.Parameters.Year, reportGenerationContext.Parameters.Month);
            var emptyMonthDays = Enumerable.Range(1, daysInMonth).Select(d => new DataColumn()
            {
                ColumnName = d.ToString(),
                Value = 0,
            });

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var rows = FilterDataScope(reportGenerationContext.Parameters, unitOfWork).ToList();

                return new EmployeesExceedingWorkingLimitReportModel
                {
                    Employees = rows.Select(t =>
                    {
                        return new EmployeeWithDailyExceededHoursModel
                        {
                            Employee = new EmployeeReportModel
                            {
                                Id = t.Employee.Id,
                                FirstName = t.Employee.FirstName,
                                LastName = t.Employee.LastName,
                                Acronym = t.Employee.Acronym,
                            },
                            ExceedingDays = t.Rows.Where(r => !r.AttributeValues.Any(v => v.Features.HasFlag(AttributeValueFeature.PassiveWorkingTime)))
                                .SelectMany(ro => ro.DailyEntries)
                                .GroupBy(d => d.Day)
                                .Select(d => new DataColumn()
                                {
                                    ColumnName = d.Key.Day.ToString(),
                                    Value = d.Sum(h => h.Hours),
                                })
                                .Union(emptyMonthDays, new ByKeyEqualityComparer<DataColumn, string>(d => d.ColumnName))
                                .OrderBy(d => int.Parse(d.ColumnName))
                                .ToArray(),
                        };
                    }).ToList(),
                };
            }
        }

        public override EmployeesExceedingWorkingLimitParametersDto GetDefaultParameters(ReportGenerationContext<EmployeesExceedingWorkingLimitParametersDto> reportGenerationContext)
        {
            var currentDate = _timeService.GetCurrentDate();

            return new EmployeesExceedingWorkingLimitParametersDto
            {
                Year = currentDate.Year,
                Month = currentDate.Month,
            };
        }

        private IQueryable<TimeReport> FilterDataScope(
            EmployeesExceedingWorkingLimitParametersDto parameters,
            IUnitOfWork<ITimeTrackingDbScope> unitOfWork)
        {
            var currentDate = _timeService.GetCurrentDate();
            var startOfMonth = new DateTime(parameters.Year, parameters.Month, 1);
            var endOfMonth = startOfMonth.GetLastDayInMonth().EndOfDay();

            var rows = unitOfWork.Repositories.TimeReports
                .Include(r => r.Employee);

            rows = rows.Where(r => r.Year == parameters.Year && r.Month == parameters.Month);

            // Rows exceeding daily working limit at some point
            Expression<Func<TimeReportRow, bool>> IsNotPassiveWorkingTime =
                r => !r.AttributeValues.Any(v => v.Features.HasFlag(AttributeValueFeature.PassiveWorkingTime));

            var GetDailyWorkingLimit
                = new BusinessLogic<IGrouping<DateTime, TimeReportDailyEntry>, TimeReport, decimal?>(
                    (d, t) => t.Employee.EmploymentPeriods
                        .FirstOrDefault(p => p.StartDate <= d.Key && (!p.EndDate.HasValue || d.Key <= p.EndDate))
                        .ContractType
                        .DailyWorkingLimit);

            rows = rows.Where(new BusinessLogic<TimeReport, bool>(
                t => t.Rows.AsQueryable()
                    .Where(IsNotPassiveWorkingTime)
                    .SelectMany(ro => ro.DailyEntries)
                    .GroupBy(d => d.Day)
                    .Where(d => GetDailyWorkingLimit.Call(d, t) != null)
                    .Any(d => d.Sum(v => v.Hours) > GetDailyWorkingLimit.Call(d, t))).AsExpression());

            if (!parameters.OrgUnitIds.IsNullOrEmpty())
            {
                rows = rows.Where(r => parameters.OrgUnitIds.Contains(r.Employee.OrgUnitId));
            }

            if (!parameters.LocationIds.IsNullOrEmpty())
            {
                rows = rows.Where(
                    r => r.Employee.LocationId.HasValue
                    && parameters.LocationIds.Contains(r.Employee.LocationId.Value));
            }

            if (!parameters.ContractTypeIds.IsNullOrEmpty())
            {
                rows = rows.Where(
                    r => r.Employee.EmploymentPeriods
                        .Where(p =>
                            ((r.Year * 12 + r.Month) >= (p.StartDate.Year * 12 + p.StartDate.Month)) &&
                            (!p.EndDate.HasValue ||
                             ((r.Year * 12 + r.Month) <= (p.EndDate.Value.Year * 12 + p.EndDate.Value.Month))))
                        .Any(p => parameters.ContractTypeIds.Contains(p.ContractTypeId)));
            }

            return rows;
        }
    }
}
