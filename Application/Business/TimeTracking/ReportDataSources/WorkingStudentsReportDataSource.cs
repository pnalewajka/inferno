﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Helpers;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.ReportModels;
using Smt.Atomic.Business.TimeTracking.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.ReportDataSources
{
    [Identifier("DataSources.WorkingStudentsReportDataSource")]
    [RequireRole(SecurityRoleType.CanGenerateWorkingStudentsReport)]
    [DefaultReportDefinition(
        "WorkingStudentsReport",
        nameof(TimeReportServiceResources.WorkingStudentsReportName),
        nameof(TimeReportServiceResources.WorkingStudentsReportDescription),
        MenuAreas.TimeTracking,
        MenuGroups.TimeTrackingReportsTT,
        ReportingEngine.MsExcel,
        "Smt.Atomic.Business.TimeTracking.ReportTemplates.WorkingStudentReport.xlsx",
        typeof(TimeReportServiceResources))]
    public class WorkingStudentsReportDataSource : BaseReportDataSource<WorkingStudentDataSourceParametersDto>
    {
        protected readonly ITimeService TimeService;
        protected readonly IUnitOfWorkService<ITimeTrackingDbScope> UnitOfWorkService;
        protected readonly IPrincipalProvider PrincipalProvider;
        protected readonly IOrgUnitService OrgUnitService;
        protected readonly IDanteCalendarService DanteCalendarService;
        protected readonly ICalendarDataService CalendarDataService;
        private readonly ISystemParameterService _parameterService;
        private readonly IEmployeeService _employeeService;

        public WorkingStudentsReportDataSource(
            ITimeService timeService,
            IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
            ISystemParameterService systemParameterService,
            IPrincipalProvider principalProvider,
            IOrgUnitService orgUnitService,
            ICalendarDataService calendarDataService,
            ISystemParameterService parameterService,
            IEmployeeService employeeService,
            IDanteCalendarService danteCalendarService)
        {
            TimeService = timeService;
            UnitOfWorkService = unitOfWorkService;
            PrincipalProvider = principalProvider;
            OrgUnitService = orgUnitService;
            DanteCalendarService = danteCalendarService;
            CalendarDataService = calendarDataService;
            _parameterService = parameterService;
            _employeeService = employeeService;
        }

        public override object GetData(ReportGenerationContext<WorkingStudentDataSourceParametersDto> reportGenerationContext)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var parameters = reportGenerationContext.Parameters;

                return GetReportModel(unitOfWork, parameters);
            }
        }

        public override WorkingStudentDataSourceParametersDto GetDefaultParameters(ReportGenerationContext<WorkingStudentDataSourceParametersDto> reportGenerationContext)
        {
            var dateTime = TimeService.GetCurrentDate();

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var workingStudentsGermanyContractType = _parameterService.GetParameter<string>(ParameterKeys.WorkingStudentsGermanyContractType);
                var contractTypeIds = unitOfWork.Repositories.ContractTypes.Where(c => c.NameEn == workingStudentsGermanyContractType).Select(s => s.Id).ToArray();

                return new WorkingStudentDataSourceParametersDto
                {
                    From = DateHelper.BeginningOfMonth(dateTime),
                    To = DateHelper.EndOfMonth(dateTime),
                    Status = TimeReportStatus.Accepted,
                    ContractTypeIds = contractTypeIds
                };
            }
        }

        private WorkingStudentsReportModel GetReportModel(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, WorkingStudentDataSourceParametersDto parameters)
        {
            var fromDate = parameters.From.StartOfDay();
            var toDate = parameters.To.EndOfDay();

            var employees = FilterEmployees(unitOfWork, parameters);
            var timeReportRows = FilterTimeReportRows(unitOfWork, parameters);

            var employeeRows = employees.GroupJoin(timeReportRows, e => e.Id, tr => tr.TimeReport.EmployeeId, (e, tr) => new
            {
                EmployeeId = e.Id,
                FirstName = e.FirstName,
                LastName = e.LastName,
                Acronym = e.Acronym,
                TotalHours = tr.SelectMany(x => x.DailyEntries)
                            .Where(de => de.Day >= fromDate && de.Day <= toDate).Select(de => de.Hours).DefaultIfEmpty().Sum(),
                DailyEntries = tr.SelectMany(v => v.DailyEntries)
            }).ToList();

            var weeks = DateHelper.WeeksInRange(fromDate, toDate);
            var calendarInfoList = _employeeService.GetCalendarInfoByEmployeeIds(employeeRows.Select(e => e.EmployeeId).ToList(), fromDate, toDate);

            var result = new WorkingStudentsReportModel
            {
                WorkingStudentsReportRows = employeeRows.Select(er => new WorkingStudentsRowReportModel
                {
                    Acronym = er.Acronym,
                    FirstName = er.FirstName,
                    LastName = er.LastName,
                    TotalHours = er.TotalHours,
                    HoursWeekly = CalculateHoursWeekly(er.DailyEntries, weeks),
                    SpecialPaidLeave = GetAbsenceDates(er.EmployeeId, fromDate, toDate, _parameterService.GetParameter<string>(ParameterKeys.SpecialPaidLeaveAbsenceType)),
                    UnpaidLeave = GetAbsenceDates(er.EmployeeId, fromDate, toDate, _parameterService.GetParameter<string>(ParameterKeys.UnpaidLeaveAbsenceType)),
                    BankHolidayHours = CalculateBankHolidayHours(er.EmployeeId, calendarInfoList, fromDate, toDate)
                }).ToList()
            };

            var reportedTasks = new List<WorkingStudentsTasksRowReportModel>();

            foreach (var employee in employeeRows)
            {
                reportedTasks.AddRange(employee.DailyEntries
                    .Where(e => e.Day >= fromDate && e.Day <= toDate)
                    .OrderBy(e => e.Day)
                    .Select(e => new WorkingStudentsTasksRowReportModel
                    {
                        Acronym = employee.Acronym,
                        FirstName = employee.FirstName,
                        LastName = employee.LastName,
                        Date = e.Day,
                        Hours = e.Hours,
                        Project = ProjectBusinessLogic.ProjectName.Call(e.TimeReportRow.Project),
                        Task = e.TimeReportRow.TaskName,
                        ReportStatus = e.TimeReportRow.TimeReport.Status
                    }));
            }

            result.WorkingStudentsTasksReportRows = reportedTasks;

            return result;
        }

        private IQueryable<Employee> FilterEmployees(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, WorkingStudentDataSourceParametersDto parameters)
        {
            var currentEmployeeId = PrincipalProvider.Current.EmployeeId;
            var fromDate = parameters.From.StartOfDay();
            var toDate = parameters.To.EndOfDay();
            var employees = unitOfWork.Repositories.Employees
               .Where(e => e.EmploymentPeriods
                   .Where(p => p.StartDate <= toDate && (!p.EndDate.HasValue || p.EndDate > fromDate)).Any());

            if (!parameters.EmployeeIds.IsNullOrEmpty())
            {
                employees = employees.Where(e => parameters.EmployeeIds.Contains(e.Id));
            }

            if (!parameters.OrgUnitIds.IsNullOrEmpty())
            {
                employees = employees.Where(e => parameters.OrgUnitIds.Contains(e.OrgUnitId));
            }

            if (!parameters.LocationIds.IsNullOrEmpty())
            {
                employees = employees.Where(e => parameters.LocationIds.Contains(e.LocationId.Value));
            }

            if (!parameters.ContractTypeIds.IsNullOrEmpty())
            {
                employees = employees.Where(e =>
                    e.EmploymentPeriods
                        .Where(p => p.StartDate <= toDate && (!p.EndDate.HasValue || p.EndDate > fromDate))
                        .Any(p => parameters.ContractTypeIds.Contains(p.ContractTypeId))
                );
            }

            if (parameters.IsMyEmployees)
            {
                var managerOrgUnitsIds = OrgUnitService.GetManagerOrgUnitDescendantIds(currentEmployeeId);
                employees = employees.Where(e => managerOrgUnitsIds.Contains(e.OrgUnitId));
            }

            return employees;
        }

        private IQueryable<TimeReportRow> FilterTimeReportRows(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, WorkingStudentDataSourceParametersDto parameters)
        {
            var fromNumberOfMonth = DateHelper.MonthsInDate(parameters.From.StartOfDay());
            var toNumberOfMonth = DateHelper.MonthsInDate(parameters.To.EndOfDay());

            var timeReportRows = unitOfWork.Repositories.TimeReportRows
                .Where(a => a.TimeReport.Year * 12 + a.TimeReport.Month >= fromNumberOfMonth && a.TimeReport.Year * 12 + a.TimeReport.Month <= toNumberOfMonth);
                                
            if (parameters.Status.HasValue)
            {
                timeReportRows = timeReportRows.Where(r => r.TimeReport.Status == parameters.Status.Value);
            }

            if (!parameters.ContractTypeIds.IsNullOrEmpty())
            {
                timeReportRows = timeReportRows.Where(r =>
                    r.TimeReport.Employee.EmploymentPeriods
                        .Where(p =>
                            ((r.TimeReport.Year * 12 + r.TimeReport.Month) >= (p.StartDate.Year * 12 + p.StartDate.Month)) &&
                            (!p.EndDate.HasValue ||
                             ((r.TimeReport.Year * 12 + r.TimeReport.Month) <= (p.EndDate.Value.Year * 12 + p.EndDate.Value.Month))))
                        .Any(p => parameters.ContractTypeIds.Contains(p.ContractTypeId)));
            }

            return timeReportRows;
        }

        private EmployeeCalendarInfoDto GetCalendar(IList<EmployeeCalendarInfoDto> calendarInfoList, long employeeId, DateTime effectiveOn)
        {
            var resultIfHaveActiveCalendar = calendarInfoList.FirstOrDefault(e =>
                e.EmployeeId == employeeId
                && ((e.StartDate <= effectiveOn && e.EndDate > effectiveOn) || (e.StartDate <= effectiveOn && e.EndDate == null) || e.StartDate == null));

            if (resultIfHaveActiveCalendar == null)
            {
                resultIfHaveActiveCalendar = calendarInfoList.Where(e => e.EmployeeId == employeeId && e.StartDate >= effectiveOn).OrderBy(e => e.StartDate).FirstOrDefault();
            }

            return resultIfHaveActiveCalendar;
        }

        private decimal CalculateBankHolidayHours(long employeeId, IList<EmployeeCalendarInfoDto> calendarInfoList, DateTime from, DateTime to)
        {
            var bankHolidayHours = 0M;
            var currentCalendarInfo = GetCalendar(calendarInfoList, employeeId, from);
            var calendarStartDate = currentCalendarInfo.StartDate ?? from;
            var calendarEndDate = currentCalendarInfo.EndDate ?? to;

            for (var currentDate = new DateTime(Math.Max(from.Ticks, calendarStartDate.Ticks)); currentDate <= to; currentDate = calendarEndDate.AddDays(1))
            {
                if (currentDate < to)
                {
                    currentCalendarInfo = GetCalendar(calendarInfoList, employeeId, currentDate);

                    if (currentCalendarInfo == null)
                    {
                        return bankHolidayHours;
                    }

                    calendarEndDate = currentCalendarInfo.EndDate ?? to;
                }

                bankHolidayHours += GetHolidayHours(employeeId, currentCalendarInfo.CalendarId, currentDate, new DateTime(Math.Min(to.Ticks, calendarEndDate.Ticks)));
            }

            return bankHolidayHours;
        }

        private decimal GetHolidayHours(long employeeId, long calendarId, DateTime from, DateTime to)
        {
            var holidayList = CalendarDataService.GetCalendarDataById(calendarId).KnownHolidays.GetHolidays(from, to).ToList();
            var sumHolidayHours = 0M;

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                foreach (var holidayDay in holidayList)
                {
                    var employmentPeriod = unitOfWork.Repositories.EmploymentPeriods.FirstOrDefault(p =>
                        p.EmployeeId == employeeId && p.StartDate <= holidayDay && (p.EndDate >= holidayDay || p.EndDate == null));

                    if (employmentPeriod != null)
                    {
                        sumHolidayHours += WeeklyHoursHelper.GetHoursByDayOrDefault(employmentPeriod, holidayDay.DayOfWeek);
                    }
                }
            }

            return sumHolidayHours;
        }

        private string GetAbsenceDates(long employeeId, DateTime from, DateTime to, string absenceType)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var absenceRequests = unitOfWork.Repositories.EmployeeAbsences.Where(a =>
                        a.EmployeeId == employeeId
                        && a.AbsenceType.NameEn == absenceType
                        && ((a.From >= from && a.To <= to)
                            || (a.From <= from && a.To >= from)
                            || (a.From <= to && a.To >= to))
                    );

                return AbsenceRequestToString(absenceRequests, from, to);
            }
        }

        private string AbsenceRequestToString(IQueryable<EmployeeAbsence> absenceRequests, DateTime from, DateTime to)
        {
            var absenceRequestDays = new List<DateTime>();

            foreach (var absenceRequest in absenceRequests)
            {
                for (DateTime selectedDate = absenceRequest.From; selectedDate <= absenceRequest.To; selectedDate = selectedDate.AddDays(1))
                {
                    if (selectedDate >= from && selectedDate <= to)
                    {
                        absenceRequestDays.Add(selectedDate);
                    }
                }
            }

            return string.Join(", ", absenceRequestDays.Select(d => d.ToShortDateString()));
        }

        private ICollection<DataColumn> CalculateHoursWeekly(IEnumerable<TimeReportDailyEntry> dailyEntries, List<int> weeks)
        {
            return weeks.Select(g => new DataColumn
            {
                ColumnName = "Week " + g,
                Value = dailyEntries.Where(de => de.Day.GetWeekOfYear() == g).Sum(d => d.Hours)
            }).ToList();
        }
    }
}
