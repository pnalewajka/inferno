﻿using System.Linq;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.ReportModels;
using Smt.Atomic.Business.TimeTracking.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Business.Reporting.Consts;

namespace Smt.Atomic.Business.TimeTracking.ReportDataSources
{
    [Identifier("DataSources.AbsencesDataSource")]
    [RequireRole(SecurityRoleType.CanGenerateAbsencesReport)]
    [DefaultReportDefinition(
        "Absences",
        nameof(AbsenceRequestResources.AbsencesReportName),
        nameof(AbsenceRequestResources.AbsencesReportDescription),
        MenuAreas.TimeTracking,
        MenuGroups.TimeTrackingReportsHR,
        ReportingEngine.MsExcel,
        "Smt.Atomic.Business.TimeTracking.ReportTemplates.AbsencesReport.xlsx",
        typeof(AbsenceRequestResources))]
    [DefaultReportDefinition(
        "AbsencesPivot",
        nameof(AbsenceRequestResources.AbsencesPivotReportName),
        nameof(AbsenceRequestResources.AbsencesReportDescription),
        MenuAreas.TimeTracking,
        MenuGroups.TimeTrackingReportsHR,
        ReportingEngine.PivotTable,
        "Smt.Atomic.Business.TimeTracking.ReportTemplates.AbsencesPivotTable-config.js",
        typeof(AbsenceRequestResources))]
    public class AbsencesDataSource : AbsencesDataSourceBase<AbsencesDataSourceParametersDto>
    {
        private readonly IEmploymentPeriodService _employmentPeriodService;

        public AbsencesDataSource(
            ITimeService timeService,
            IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
            IEmploymentPeriodService employmentPeriodService)
        : base(timeService, unitOfWorkService)
        {
            _employmentPeriodService = employmentPeriodService;
        }

        public override AbsencesDataSourceParametersDto GetDefaultParameters(ReportGenerationContext<AbsencesDataSourceParametersDto> reportGenerationContext)
        {
            var yesterday = TimeService.GetCurrentDate().AddDays(-1);

            return new AbsencesDataSourceParametersDto
            {
                ApprovedFrom = DateHelper.BeginningOfMonth(yesterday),
                ApprovedTo = yesterday,
            };
        }

        public override object GetData(ReportGenerationContext<AbsencesDataSourceParametersDto> reportGenerationContext)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var employeeAbsences = FilterDataScope(unitOfWork.Repositories, reportGenerationContext.Parameters);
                var absences = employeeAbsences.Select(CreateReportModel).OrderBy(a => a.From).ToList();

                return new AbsencesReportModel
                {
                    Absences = absences
                };
            }
        }

        protected override IQueryable<EmployeeAbsence> FilterByDatePeriod(AbsencesDataSourceParametersDto parameters, IQueryable<EmployeeAbsence> rows)
        {
            var endDate = parameters.ApprovedTo.Date.AddDays(1);

            rows = rows.Where(a => a.ApprovedOn >= parameters.ApprovedFrom && a.ApprovedOn < endDate);

            return rows;
        }

        private AbsenceReportModel CreateReportModel(EmployeeAbsence employeeAbsence)
        {
            var contractType = employeeAbsence.Employee.EmploymentPeriods.FirstOrDefault(p =>
                (employeeAbsence.To >= p.StartDate) &&
                (!p.EndDate.HasValue || employeeAbsence.From <= p.EndDate.Value))?.ContractType;

            var approvedByNames = employeeAbsence.Request.ApprovalGroups
                .SelectMany(g => g.Approvals)
                .Where(a => a.Status == ApprovalStatus.Approved)
                .Select(a => a.User)
                .DistinctBy(u => u.Id)
                .Select(u => u.FullName)
                .OrderBy(n => n);
            var approvedBy = string.Join(",", approvedByNames);

            var workingDays = _employmentPeriodService
                    .GetContractedHours(
                        employeeAbsence.EmployeeId,
                        employeeAbsence.From,
                        employeeAbsence.To).Count(x => x.Hours > 0);

            return new AbsenceReportModel
            {
                From = employeeAbsence.From,
                To = employeeAbsence.To,
                ApprovedOn = employeeAbsence.ApprovedOn,
                AbsenceTypeCode = employeeAbsence.AbsenceType.Code,
                IsVacation = employeeAbsence.AbsenceType.IsVacation,
                ContractType = contractType?.Name?.ToString() ?? string.Empty,
                RequestedOn = employeeAbsence.Request.CreatedOn,
                ApprovedBy = approvedBy,
                DaysOfAbsence = (employeeAbsence.To - employeeAbsence.From).TotalDays + 1,
                WorkingDays = workingDays,
                Employee = new EmployeeReportModel
                {
                    Id = employeeAbsence.EmployeeId,
                    ActiveDirectoryId = employeeAbsence.Employee.User.ActiveDirectoryId,
                    Login = employeeAbsence.Employee.User.Login,
                    FirstName = employeeAbsence.Employee.FirstName,
                    LastName = employeeAbsence.Employee.LastName,
                    Acronym = employeeAbsence.Employee.Acronym,
                    Email = employeeAbsence.Employee.Email,
                    CompanyName = employeeAbsence.Employee.Company.Name,
                    OrgUnitName = employeeAbsence.Employee.OrgUnit.Name,
                    OrgUnitCode = employeeAbsence.Employee.OrgUnit.Code,
                    LocationName = employeeAbsence.Employee.Location.Name,
                    Seniority = employeeAbsence.Employee.JobMatrixs.FirstOrDefault()?.Code ?? string.Empty,
                    JobTitle = employeeAbsence.Employee.JobTitle?.NameEn ?? string.Empty
                },
            };
        }
    }
}
