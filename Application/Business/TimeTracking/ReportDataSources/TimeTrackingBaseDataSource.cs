﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Helpers;
using Smt.Atomic.Business.TimeTracking.ReportModels;
using Smt.Atomic.Business.TimeTracking.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Data.Repositories.Helpers;
using System.Collections;
using System.Linq.Expressions;
using Smt.Atomic.Business.Reporting.Helpers;
using Smt.Atomic.CrossCutting.Business.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.Business.Reporting.Dto;

namespace Smt.Atomic.Business.TimeTracking.ReportDataSources
{
    public abstract class TimeTrackingBaseDataSource : BaseReportDataSource<TimeTrackingDataSourceParametersDto>
    {
        protected readonly ITimeService TimeService;
        protected readonly IUnitOfWorkService<ITimeTrackingDbScope> UnitOfWorkService;
        protected readonly IPrincipalProvider PrincipalProvider;
        protected readonly IOrgUnitService OrgUnitService;

        public TimeTrackingBaseDataSource(
            ITimeService timeService,
            IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
            IPrincipalProvider principalProvider,
            IOrgUnitService orgUnitService)
        {
            TimeService = timeService;
            UnitOfWorkService = unitOfWorkService;
            PrincipalProvider = principalProvider;
            OrgUnitService = orgUnitService;
        }

        public override TimeTrackingDataSourceParametersDto GetDefaultParameters(ReportGenerationContext<TimeTrackingDataSourceParametersDto> reportGenerationContext)
        {
            var dateTime = TimeService.GetCurrentDate();

            return new TimeTrackingDataSourceParametersDto
            {
                From = DateHelper.BeginningOfMonth(dateTime),
                To = DateHelper.EndOfMonth(dateTime),
                Status = TimeReportStatus.Accepted
            };
        }

        public override object GetData(ReportGenerationContext<TimeTrackingDataSourceParametersDto> reportGenerationContext)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var parameters = reportGenerationContext.Parameters;
                var rows = FilterDataScope(parameters, unitOfWork);

                return GetReportModel(rows, parameters.From.StartOfDay(), parameters.To.EndOfDay());
            }
        }

        protected abstract TimeTrackingTasksReportModel GetReportModel(IQueryable<TimeReportRow> rows, DateTime from, DateTime to);

        protected virtual IQueryable<TimeReportRow> FilterDataScope(TimeTrackingDataSourceParametersDto parameters, IUnitOfWork<ITimeTrackingDbScope> unitOfWork)
        {
            var currentEmployeeId = PrincipalProvider.Current.EmployeeId;

            var rows = unitOfWork.Repositories.TimeReportRows
                .Include(r => r.DailyEntries)
                .Include(r => r.Project.ProjectSetup.OrgUnit)
                .Include(r => r.TimeReport.Employee);

            if (parameters.Status.HasValue)
            {
                rows = rows.Where(r => r.TimeReport.Status == parameters.Status.Value);
            }

            if (parameters.EmploymentType.HasValue)
            {
                rows = rows.Where(r => r.TimeReport.Employee.CurrentEmploymentType == parameters.EmploymentType.Value);
            }

            if (parameters.ProjectType.HasValue)
            {
                rows = rows.Where(r => r.Project.TimeTrackingType == parameters.ProjectType.Value);
            }

            if (!parameters.EmployeeIds.IsNullOrEmpty())
            {
                rows = rows.Where(r => parameters.EmployeeIds.Contains(r.TimeReport.EmployeeId));
            }

            if (!parameters.ProjectIds.IsNullOrEmpty())
            {
                rows = rows.Where(r => parameters.ProjectIds.Contains(r.ProjectId));
            }

            if (!parameters.ProjectOrgUnitIds.IsNullOrEmpty())
            {
                rows = rows.Where(r => parameters.ProjectOrgUnitIds.Contains(r.Project.ProjectSetup.OrgUnitId));
            }

            if (!parameters.ProjectTagsIds.IsNullOrEmpty())
            {
                rows = rows.Where(r => r.Project.ProjectSetup.Tags.Any(t => parameters.ProjectTagsIds.Contains(t.Id)));
            }

            if (!parameters.EmployeeOrgUnitIds.IsNullOrEmpty())
            {
                rows = rows.Where(r => parameters.EmployeeOrgUnitIds.Contains(r.TimeReport.Employee.OrgUnitId));
            }

            if (!parameters.HourlyRateTypes.IsNullOrEmpty())
            {
                rows = rows.Where(r => parameters.HourlyRateTypes.Contains((long)r.OvertimeVariant));
            }

            if (!parameters.ProjectClientIds.IsNullOrEmpty())
            {
                var clientNames = unitOfWork.Repositories.Projects
                    .GetByIds(parameters.ProjectClientIds)
                    .Select(p => p.ProjectSetup.ClientShortName);

                rows = rows.Where(r => clientNames.Contains(r.Project.ProjectSetup.ClientShortName));
            }

            if (!parameters.LocationIds.IsNullOrEmpty())
            {
                rows = rows.Where(r =>
                    r.TimeReport.Employee.LocationId.HasValue &&
                    parameters.LocationIds.Contains(r.TimeReport.Employee.LocationId.Value));
            }

            if (!parameters.ContractTypeIds.IsNullOrEmpty())
            {
                rows = rows.Where(r =>
                    r.TimeReport.Employee.EmploymentPeriods
                        .Where(p =>
                            ((r.TimeReport.Year * 12 + r.TimeReport.Month) >= (p.StartDate.Year * 12 + p.StartDate.Month)) &&
                            (!p.EndDate.HasValue ||
                             ((r.TimeReport.Year * 12 + r.TimeReport.Month) <= (p.EndDate.Value.Year * 12 + p.EndDate.Value.Month))))
                        .Any(p => parameters.ContractTypeIds.Contains(p.ContractTypeId)));
            }

            if (parameters.MyEmployees)
            {
                var managerOrgUnitsIds = OrgUnitService.GetManagerOrgUnitDescendantIds(currentEmployeeId);
                var isEmployeeOfExpression = new BusinessLogic<TimeReportRow, bool>(
                    r => EmployeeBusinessLogic.IsEmployeeOf.Call(r.TimeReport.Employee, currentEmployeeId, managerOrgUnitsIds)).AsExpression();

                rows = rows.Where(isEmployeeOfExpression);
            }

            return rows;
        }

        protected void ProcessTasks(ICollection<TimeTrackingTaskReportModel> tasks)
        {
            SplitTaskNames(tasks);
        }

        protected void SplitTaskNames(ICollection<TimeTrackingTaskReportModel> tasks)
        {
            if (!tasks.Any())
            {
                return;
            }

            foreach (var task in tasks)
            {
                string originalTaskName = task.TaskName;
                task.TaskName = TimeReportRowHelper.CleanTaskName(originalTaskName);
            }
        }
    }
}