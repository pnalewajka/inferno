﻿using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.Business.TimeTracking.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.ReportDataSources
{
    [Identifier("DataSource.BonusRequestReport")]
    [RequireRole(SecurityRoleType.CanGenerateBonusRequestReport)]
    [DefaultReportDefinition(
    "BonusRequestReport",
    nameof(TimeReportServiceResources.BonusRequestReportName),
    nameof(TimeReportServiceResources.BonusRequestReportDescription),
    MenuAreas.TimeTracking,
    MenuGroups.TimeTrackingReportsHR,
    ReportingEngine.PivotTable,
    new[]
    {
            "Smt.Atomic.Business.TimeTracking.ReportTemplates.BonusRequestReport-config.js",
            "Smt.Atomic.Business.TimeTracking.ReportTemplates.BonusRequestReport.manifest",
            "Smt.Atomic.Business.TimeTracking.ReportTemplates.BonusRequestReport.sql"
    },
    typeof(TimeReportServiceResources))]

    public class BonusRequestReport : SqlBasedDataSource
    {
        public BonusRequestReport(IUnitOfWorkService<IDbScope> unitOfWork, IPrincipalProvider principalProvider, IUnitOfWorkService<IAccountsDbScope> unitOfWorkService)
           : base(unitOfWork, principalProvider, unitOfWorkService)
        {
        }
    }
}
