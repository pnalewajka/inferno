﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.ReportModels;
using Smt.Atomic.Business.TimeTracking.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.ReportDataSources
{
    [Identifier("DataSources.TimeTrackingSummaryHoursDataSource")]
    [RequireRole(SecurityRoleType.CanGenerateTimeTrackingSummaryHoursReport)]
    [DefaultReportDefinition(
        "TimeTrackingSummaryHours",
        nameof(TimeReportServiceResources.TimeTrackingSummaryHoursDataSourceName),
        nameof(TimeReportServiceResources.TimeTrackingSummaryHoursDataSourceDescription),
        MenuAreas.TimeTracking,
        MenuGroups.TimeTrackingReportsTT,
        ReportingEngine.MsExcel,
        "Smt.Atomic.Business.TimeTracking.ReportTemplates.HoursSummary.xlsx",
        typeof(TimeReportServiceResources))]
    public class TimeTrackingSummaryHoursDataSource : BaseReportDataSource<TimeTrackingSummaryHoursParametersDto>
    {
        private readonly ITimeService _timeService;
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService;
        private readonly ISystemParameterService _systemParameterService;

        public TimeTrackingSummaryHoursDataSource(
            ITimeService timeService,
            IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
            ISystemParameterService systemParameterService)
        {
            _timeService = timeService;
            _unitOfWorkService = unitOfWorkService;
            _systemParameterService = systemParameterService;
        }

        public override TimeTrackingSummaryHoursParametersDto GetDefaultParameters(ReportGenerationContext<TimeTrackingSummaryHoursParametersDto> reportGenerationContext)
        {
            var dateTime = _timeService.GetCurrentDate();

            return new TimeTrackingSummaryHoursParametersDto
            {
                FromYear = dateTime.Year,
                FromMonth = dateTime.Month,
                ToYear = dateTime.Year,
                ToMonth = dateTime.Month,
                Status = TimeReportStatus.Accepted
            };
        }

        public override object GetData(ReportGenerationContext<TimeTrackingSummaryHoursParametersDto> reportGenerationContext)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var rows = FilterDataScope(reportGenerationContext.Parameters, unitOfWork);
                var timeReports = GetTimeReports(rows, unitOfWork).ToList();

                return new SummaryHoursReportModel
                {
                    TimeReports = timeReports
                };
            }
        }

        private static IQueryable<TimeReportRow> FilterDataScope(TimeTrackingSummaryHoursParametersDto parameters, IUnitOfWork<ITimeTrackingDbScope> unitOfWork)
        {
            var rows = unitOfWork.Repositories.TimeReportRows
                .Include(r => r.TimeReport.Employee);

            rows = rows.Where(r =>
                r.TimeReport.Year * 12 + r.TimeReport.Month >= parameters.FromYear * 12 + parameters.FromMonth);

            rows = rows.Where(r =>
                r.TimeReport.Year * 12 + r.TimeReport.Month <= parameters.ToYear * 12 + parameters.ToMonth);

            if (parameters.Status.HasValue)
            {
                rows = rows.Where(r => r.TimeReport.Status == parameters.Status.Value);
            }

            if (parameters.EmploymentType.HasValue)
            {
                rows = rows.Where(r => r.TimeReport.Employee.CurrentEmploymentType == parameters.EmploymentType.Value);
            }

            if (!parameters.EmployeeIds.IsNullOrEmpty())
            {
                rows = rows.Where(r => parameters.EmployeeIds.Contains(r.TimeReport.EmployeeId));
            }

            if (!parameters.EmployeeOrgUnitIds.IsNullOrEmpty())
            {
                rows = rows.Where(r => parameters.EmployeeOrgUnitIds.Contains(r.TimeReport.Employee.OrgUnitId));
            }

            if (!parameters.LocationIds.IsNullOrEmpty())
            {
                rows = rows.Where(r =>
                    r.TimeReport.Employee.LocationId.HasValue &&
                    parameters.LocationIds.Contains(r.TimeReport.Employee.LocationId.Value));
            }

            if (!parameters.ContractTypeIds.IsNullOrEmpty())
            {
                rows = rows.Where(r =>
                    r.TimeReport.Employee.EmploymentPeriods
                        .Where(p =>
                            ((r.TimeReport.Year * 12 + r.TimeReport.Month) >= (p.StartDate.Year * 12 + p.StartDate.Month)) &&
                            (!p.EndDate.HasValue ||
                             ((r.TimeReport.Year * 12 + r.TimeReport.Month) <= (p.StartDate.Year * 12 + p.StartDate.Month))))
                        .Any(p => parameters.ContractTypeIds.Contains(p.ContractTypeId)));
            }

            return rows.OrderBy(r => r.TimeReport.Year).ThenBy(r => r.TimeReport.Month).ThenBy(r => r.TimeReport.Employee.Acronym);
        }

        private static IEnumerable<TRSummaryHoursReportModel> GetTimeReports(IQueryable<TimeReportRow> rows, IUnitOfWork<ITimeTrackingDbScope> unitOfWork)
        {
            var absenceCodesToNames = unitOfWork.Repositories.AbsenceTypes
                .Where(a => a.AbsenceProjectId != null)
                .ToDictionary(a => a.Code, a => a.NameEn);
                
            var absenceCodesForOvertime = unitOfWork.Repositories.AbsenceTypes.Where(a => a.IsTimeOffForOvertime).Select(a => a.Code).ToList();
            
            return rows
                .Select(r => new
                {
                    TimeReport = new
                    {
                        r.TimeReport.Id,
                        r.TimeReport.Year,
                        r.TimeReport.Month,
                        r.TimeReport.Status,
                        Employee = new EmployeeReportModel
                        {
                            Id = r.TimeReport.EmployeeId,
                            ActiveDirectoryId = r.TimeReport.Employee.User.ActiveDirectoryId,
                            Login = r.TimeReport.Employee.User.Login,
                            FirstName = r.TimeReport.Employee.FirstName,
                            LastName = r.TimeReport.Employee.LastName,
                            Acronym = r.TimeReport.Employee.Acronym,
                            CompanyName = r.TimeReport.Employee.Company.Name,
                            OrgUnitName = r.TimeReport.Employee.OrgUnit.Name,
                            OrgUnitCode = r.TimeReport.Employee.OrgUnit.Code,
                            Seniority = r.TimeReport.Employee.JobMatrixs.FirstOrDefault() != null
                                    ? r.TimeReport.Employee.JobMatrixs.FirstOrDefault().Code
                                    : string.Empty,
                            JobTitle = r.TimeReport.Employee.JobTitle != null 
                                        ? r.TimeReport.Employee.JobTitle.NameEn 
                                        : string.Empty

                        },
                    },
                    r.OvertimeVariant,
                    r.TaskName,
                    r.Project.TimeTrackingType,
                    Hours = r.DailyEntries.Sum(d => (decimal?)d.Hours) ?? 0M,
                })
                .AsEnumerable()
                .GroupBy(r => r.TimeReport)
                .Select(g => new
                {
                    TimeReport = g.Key,
                    TotalHours = g.Where(r => IsWorkedHoursProject(r.TimeTrackingType)).Sum(r => r.Hours),
                    OvertimeHours =
                        g.Where(
                            r => IsWorkedHoursProject(r.TimeTrackingType) && r.OvertimeVariant != HourlyRateType.Regular)
                            .Sum(r => r.Hours),
                    SumHoursByOverTime = new Func<HourlyRateType, decimal>(overtimeVariant =>
                        g.Where(r => IsWorkedHoursProject(r.TimeTrackingType) && r.OvertimeVariant == overtimeVariant)
                            .Sum(r => r.Hours)),
                    SumHoursByAbsence = new Func<string, decimal>(absenceCode =>
                        g.Where(r =>
                            r.TimeTrackingType == TimeTrackingProjectType.Absence &&
                            r.TaskName == absenceCodesToNames[absenceCode])
                            .Sum(r => r.Hours))
                })
                .Select(x => new TRSummaryHoursReportModel
                {
                    TimeReportId = x.TimeReport.Id,
                    StartDate = new DateTime(x.TimeReport.Year, x.TimeReport.Month, 1),
                    TimeReportStatus = x.TimeReport.Status,
                    Employee = x.TimeReport.Employee,
                    RegularHours = x.SumHoursByOverTime(HourlyRateType.Regular),
                    OverTimeNormalHours = x.SumHoursByOverTime(HourlyRateType.OverTimeNormal),
                    OverTimeLateNightHours = x.SumHoursByOverTime(HourlyRateType.OverTimeLateNight),
                    OverTimeWeekendHours = x.SumHoursByOverTime(HourlyRateType.OverTimeWeekend),
                    OverTimeHolidayHours = x.SumHoursByOverTime(HourlyRateType.OverTimeHoliday),
                    OverTimeToBePaidHours = x.OvertimeHours - absenceCodesForOvertime.Select(x.SumHoursByAbsence).DefaultIfEmpty(0).Sum(),
                    TotalHours = x.TotalHours,
                    HoursPerAbsenceType = absenceCodesToNames.Keys.OrderBy(c => c).Select(c => new DataColumn
                    {
                        ColumnName = c,
                        Value = x.SumHoursByAbsence(c),
                    }).ToList(),
                });
        }

        private static bool IsWorkedHoursProject(TimeTrackingProjectType timeTrackingType)
        {
            return timeTrackingType != TimeTrackingProjectType.Absence && timeTrackingType != TimeTrackingProjectType.Holiday;
        }
    }
}
