﻿using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.Business.TimeTracking.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.ReportDataSources
{
    [Identifier("DataSource.DeductibleCostReport")]
    [RequireRole(SecurityRoleType.CanGenerateDeductibleCostReport)]
    [DefaultReportDefinition(
    "DeductibleCostReport",
    nameof(TimeReportServiceResources.DeductibleCostReportName),
    nameof(TimeReportServiceResources.DeductibleCostReportDescription),
    MenuAreas.TimeTracking,
    MenuGroups.TimeTrackingReportsHR,
    ReportingEngine.PivotTable,
    new[]
    {
            "Smt.Atomic.Business.TimeTracking.ReportTemplates.DeductibleCostReport-config.js",
            "Smt.Atomic.Business.TimeTracking.ReportTemplates.DeductibleCostReport.manifest",
            "Smt.Atomic.Business.TimeTracking.ReportTemplates.DeductibleCostReport.sql"
    },
    typeof(TimeReportServiceResources))]

    public class DeductibleCostReport : SqlBasedDataSource
    {
        public DeductibleCostReport(IUnitOfWorkService<IDbScope> unitOfWork, IPrincipalProvider principalProvider, IUnitOfWorkService<IAccountsDbScope> unitOfWorkService)
           : base(unitOfWork, principalProvider, unitOfWorkService)
        {
        }
    }
}