﻿using System;
using System.Linq;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel;
using Smt.Atomic.Business.TimeTracking.ReportModels;
using Smt.Atomic.Business.TimeTracking.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.ReportDataSources
{
    [Identifier("DataSources.TimeTrackingTasksDataSource")]
    [RequireRole(SecurityRoleType.CanGenerateTimeTrackingTasksReport)]
    [DefaultReportDefinition(
        "TimeTrackingTasks",
        nameof(TimeReportServiceResources.TimeTrackingTasksDataSourceName),
        nameof(TimeReportServiceResources.TimeTrackingTasksDataSourceDescription),
        MenuAreas.TimeTracking,
        MenuGroups.TimeTrackingReportsTT,
        ReportingEngine.MsExcel,
        "Smt.Atomic.Business.TimeTracking.ReportTemplates.ReportedTasks.xlsx",
        typeof(TimeReportServiceResources))]
    public class TimeTrackingTasksDataSource : TimeTrackingBaseDataSource
    {
        private readonly ISystemParameterService _systemParameterService;

        public TimeTrackingTasksDataSource(
            ITimeService timeService,
            IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
            ISystemParameterService systemParameterService,
            IPrincipalProvider principalProvider,
            IOrgUnitService orgUnitService)
            : base(timeService, unitOfWorkService, principalProvider, orgUnitService)
        {
            _systemParameterService = systemParameterService;
        }

        protected override TimeTrackingTasksReportModel GetReportModel(IQueryable<TimeReportRow> rows, DateTime from, DateTime to)
        {
            string host = _systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);
            var canSeeAbsenceTaskName = PrincipalProvider.Current.IsInRole(SecurityRoleType.CanSeeAbsenceTaskName);
            var tasks = rows.Where(r => r.DailyEntries.Any(w => w.Day >= from && w.Day <= to))
                            .AsEnumerable()
                            .Select(GetCreateTaskReportModel(from, to, host, canSeeAbsenceTaskName))
                            .ToList();

            SplitTaskNames(tasks);

            var result = new TimeTrackingTasksReportModel
            {
                Tasks = tasks
            };

            return result;
        }

        private static Func<TimeReportRow, TimeTrackingTaskReportModel> GetCreateTaskReportModel(DateTime @from, DateTime to, string host, bool canSeeAbsenceTaskName)
        {
            return r => new TimeTrackingTaskReportModel
            {
                TaskName = canSeeAbsenceTaskName || r.Project.TimeTrackingType != TimeTrackingProjectType.Absence
                    ? r.TaskName
                    : TimeReportResources.AbsenceDefaultTaskName,
                Hours = r.DailyEntries.Where(d => d.Day >= from && d.Day <= to).Sum(e => (decimal?)e.Hours) ?? 0,
                ReportStatus = r.TimeReport.Status,
                OvertimeVariant = r.OvertimeVariant,
                IsRegular = r.OvertimeVariant == HourlyRateType.Regular,
                IsImported = r.ImportSource != null,
                IsBillable = r.IsBillable,
                ImportedTaskCode = r.ImportedTaskCode,
                ImportedTaskType = r.ImportedTaskType,
                Attributes = r.AttributeValues.Select(v => new DataColumn
                {
                    Value = v.Name,
                    ColumnName = v.Attribute.Name
                }).ToArray(),
                Project = new ProjectReportModel
                {
                    Name = ProjectBusinessLogic.ProjectName.Call(r.Project),
                    Apn = r.Project.Apn,
                    PetsCode = r.Project.PetsCode,
                    KupferwerkProjectId = r.Project.KupferwerkProjectId,
                    Client = r.Project.ProjectSetup.ClientShortName,
                    OrgUnitName = r.Project.ProjectSetup.OrgUnit.Name,
                    OrgUnitCode = r.Project.ProjectSetup.OrgUnit.Code,
                    Status = r.Project.ProjectSetup.ProjectStatus,
                },
                Employee = new EmployeeReportModel
                {
                    Id = r.TimeReport.EmployeeId,
                    Acronym = r.TimeReport.Employee.Acronym,
                    ActiveDirectoryId = r.TimeReport.Employee.User.ActiveDirectoryId,
                    Login = r.TimeReport.Employee.User.Login,
                    FirstName = r.TimeReport.Employee.FirstName,
                    LastName = r.TimeReport.Employee.LastName,
                    OrgUnitName = r.TimeReport.Employee.OrgUnit.Name,
                    OrgUnitCode = r.TimeReport.Employee.OrgUnit.Code,
                    CompanyName = r.TimeReport.Employee.Company.Name,
                    Seniority = r.TimeReport.Employee.JobMatrixs.FirstOrDefault() != null
                                    ? r.TimeReport.Employee.JobMatrixs.FirstOrDefault().Code
                                    : string.Empty,
                    JobTitle = r.TimeReport.Employee.JobTitle?.NameEn ?? string.Empty
                },
                ReportUrl = host + "timetracking/employeetimereport/timereport?id=" + r.TimeReport.EmployeeId + "&year=" + r.TimeReport.Year + "&month=" + r.TimeReport.Month
            };
        }
    }
}