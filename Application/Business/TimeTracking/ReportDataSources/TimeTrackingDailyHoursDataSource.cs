﻿using System;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel;
using Smt.Atomic.Business.TimeTracking.ReportModels;
using Smt.Atomic.Business.TimeTracking.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.ReportDataSources
{
    [Identifier("DataSources.TimeTrackingDailyHoursDataSource")]
    [RequireRole(SecurityRoleType.CanGenerateTimeTrackingDailyHoursReport)]
    [DefaultReportDefinition(
        "TimeTrackingDailyHours",
        nameof(TimeReportServiceResources.TimeTrackingDailyHoursName),
        nameof(TimeReportServiceResources.TimeTrackingDailyHoursDescription),
        MenuAreas.TimeTracking,
        MenuGroups.TimeTrackingReportsTT,
        ReportingEngine.MsExcel,
        "Smt.Atomic.Business.TimeTracking.ReportTemplates.ReportedDailyHours.xlsx",
        typeof(TimeReportServiceResources))]
    [DefaultReportDefinition(
        "TimeTrackingDailyHoursPivot",
        nameof(TimeReportServiceResources.TimeTrackingDailyHoursPivotName),
        nameof(TimeReportServiceResources.TimeTrackingDailyHoursPivotDescription),
        MenuAreas.TimeTracking,
        MenuGroups.TimeTrackingReportsTT,
        ReportingEngine.PivotTable,
        "Smt.Atomic.Business.TimeTracking.ReportTemplates.ReportedDailyHoursPivot-config.js",
        typeof(TimeReportServiceResources))]
    public class TimeTrackingDailyHoursDataSource : TimeTrackingBaseDataSource
    {
        private readonly ISystemParameterService _systemParameterService;

        public TimeTrackingDailyHoursDataSource(
            ITimeService timeService,
            IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
            ISystemParameterService systemParameterService,
            IPrincipalProvider principalProvider,
            IOrgUnitService orgUnitService)
            : base(timeService, unitOfWorkService, principalProvider, orgUnitService)
        {
            _systemParameterService = systemParameterService;
        }

        protected override TimeTrackingTasksReportModel GetReportModel(IQueryable<TimeReportRow> rows, DateTime from, DateTime to)
        {
            var host = _systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);
            var canSeeAbsenceTaskName = PrincipalProvider.Current.IsInRole(SecurityRoleType.CanSeeAbsenceTaskName);

            var reportRows = rows
                .Include(r => r.DailyEntries)
                .Include(r => r.TimeReport)
                .Include(r => r.AttributeValues)
                .Include(r => r.Project)
                .Include(r => r.TimeReport.Employee)
                .Include(r => r.TimeReport.Employee.JobMatrixs)
                .Include(r => r.TimeReport.Employee.User)
                .SelectMany(r => r.DailyEntries)
                .Where(d => d.Day >= from && d.Day <= to)
                .AsEnumerable();

            var tasks = reportRows
                .Select(GetCreateTaskReportModel(host, canSeeAbsenceTaskName))
                .OrderBy(r => r.Employee.Id)
                .ThenBy(r => r.Date)
                .ToList();

            ProcessTasks(tasks);

            var result = new TimeTrackingTasksReportModel
            {
                Tasks = tasks
            };

            return result;
        }

        private static Func<TimeReportDailyEntry, TimeTrackingTaskReportModel> GetCreateTaskReportModel(string host, bool canSeeAbsenceTaskName)
        {
            return r => new TimeTrackingTaskReportModel
            {
                TaskName = canSeeAbsenceTaskName || r.TimeReportRow.Project.TimeTrackingType != TimeTrackingProjectType.Absence
                    ? r.TimeReportRow.TaskName
                    : TimeReportResources.AbsenceDefaultTaskName,
                Date = r.Day,
                Hours = r.Hours,
                RequestStatus = r.TimeReportRow.Request?.Status,
                ReportStatus = r.TimeReportRow.TimeReport.Status,
                WeekNumber = r.Day.GetWeekOfYear(),
                OvertimeVariant = r.TimeReportRow.OvertimeVariant,
                IsRegular = r.TimeReportRow.OvertimeVariant == HourlyRateType.Regular,
                IsImported = r.TimeReportRow.ImportSource != null,
                IsBillable = r.TimeReportRow.IsBillable,
                ImportedTaskCode = r.TimeReportRow.ImportedTaskCode,
                ImportedTaskType = r.TimeReportRow.ImportedTaskType,
                Attributes = r.TimeReportRow.AttributeValues.Select(v => new DataColumn
                {
                    Value = v.Name,
                    ColumnName = v.Attribute.Name
                }).ToList(),
                Project = new ProjectReportModel
                {
                    Name = ProjectBusinessLogic.ProjectName.Call(r.TimeReportRow.Project),
                    Apn = r.TimeReportRow.Project.Apn,
                    PetsCode = r.TimeReportRow.Project.PetsCode,
                    KupferwerkProjectId = r.TimeReportRow.Project.KupferwerkProjectId,
                    Client = r.TimeReportRow.Project.ProjectSetup.ClientShortName,
                    OrgUnitName = r.TimeReportRow.Project.ProjectSetup.OrgUnit.Name,
                    OrgUnitCode = r.TimeReportRow.Project.ProjectSetup.OrgUnit.Code,
                    Status = r.TimeReportRow.Project.ProjectSetup.ProjectStatus,
                },
                Employee = new EmployeeReportModel
                {
                    Id = r.TimeReportRow.TimeReport.EmployeeId,
                    Acronym = r.TimeReportRow.TimeReport.Employee.Acronym,
                    ActiveDirectoryId = r.TimeReportRow.TimeReport.Employee.User.ActiveDirectoryId,
                    Login = r.TimeReportRow.TimeReport.Employee.User.Login,
                    FirstName = r.TimeReportRow.TimeReport.Employee.FirstName,
                    LastName = r.TimeReportRow.TimeReport.Employee.LastName,
                    OrgUnitName = r.TimeReportRow.TimeReport.Employee.OrgUnit.Name,
                    OrgUnitCode = r.TimeReportRow.TimeReport.Employee.OrgUnit.Code,
                    CompanyName = r.TimeReportRow.TimeReport.Employee.Company != null 
                        ? r.TimeReportRow.TimeReport.Employee.Company.Name 
                        : null,
                    Seniority = r.TimeReportRow.TimeReport.Employee.JobMatrixs.FirstOrDefault() != null 
                                    ? r.TimeReportRow.TimeReport.Employee.JobMatrixs.FirstOrDefault().Code 
                                    : string.Empty,
                    JobTitle = r.TimeReportRow.TimeReport.Employee.JobTitle?.NameEn ?? string.Empty
                },
                ReportUrl = host + "timetracking/employeetimereport/timereport?id=" + r.TimeReportRow.TimeReport.EmployeeId + "&year=" + r.Day.Year + "&month=" + r.Day.Month
            };
        }
    }
}