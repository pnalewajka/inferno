﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.ReportDataSources
{
    public abstract class AbsencesDataSourceBase<TParameters> : BaseReportDataSource<TParameters> where TParameters : AbsencesDataSourceParametersBaseDto
    {
        protected readonly ITimeService TimeService;
        protected readonly IUnitOfWorkService<ITimeTrackingDbScope> UnitOfWorkService;

        protected AbsencesDataSourceBase(ITimeService timeService, IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService)
        {
            TimeService = timeService;
            UnitOfWorkService = unitOfWorkService;
        }

        protected IEnumerable<EmployeeAbsence> FilterDataScope(ITimeTrackingDbScope dbScope, TParameters parameters)
        {
            var contractTypePath = string.Join(".",
                PropertyHelper.GetPropertyPathMemberInfo(
                    (EmployeeAbsence a) => a.Employee.EmploymentPeriods.Select(p => p.ContractType)).Select(m => m.Name));

            var rows = dbScope.EmployeeAbsences.Filtered()
                .Include(a => a.AbsenceType)
                .Include(a => a.Employee)
                .Include(a => a.Employee.Company)
                .Include(a => a.Employee.OrgUnit)
                .Include(a => a.Employee.Location)
                .Include(a => a.Employee.EmploymentPeriods)
                .Include(contractTypePath);

            rows = rows.Where(a => a.AbsenceType.AbsenceProjectId != null);

            rows = FilterByDatePeriod(parameters, rows);

            if (parameters.IsVacation.HasValue)
            {
                rows = rows.Where(a => a.AbsenceType.IsVacation == parameters.IsVacation.Value);
            }

            if (!parameters.AbsenceTypeIds.IsNullOrEmpty())
            {
                rows = rows.Where(a => parameters.AbsenceTypeIds.Contains(a.AbsenceTypeId));
            }

            if (parameters.EmploymentType.HasValue)
            {
                rows = rows.Where(a => a.Employee.EmploymentPeriods
                    .Where(p =>
                        (a.To >= p.StartDate) &&
                        (!p.EndDate.HasValue || a.From <= p.EndDate.Value))
                    .Any(p => p.ContractType.EmploymentType == parameters.EmploymentType));
            }

            if (!parameters.ContractTypeIds.IsNullOrEmpty())
            {
                rows = rows.Where(a => a.Employee.EmploymentPeriods
                    .Where(p =>
                        (a.To >= p.StartDate) &&
                        (!p.EndDate.HasValue || a.From <= p.EndDate.Value))
                    .Any(p => parameters.ContractTypeIds.Contains(p.ContractTypeId)));
            }

            if (!parameters.CompanyIds.IsNullOrEmpty())
            {
                rows = rows.Where(
                    a => a.Employee.CompanyId.HasValue && parameters.CompanyIds.Contains(a.Employee.CompanyId.Value));
            }

            if (!parameters.OrgUnitIds.IsNullOrEmpty())
            {
                var orgUnitPaths = dbScope.OrgUnits.GetByIds(parameters.OrgUnitIds).Select(o => o.Path).ToList();

                rows = rows.Where(a => orgUnitPaths.Any(p => a.Employee.OrgUnit.Path.StartsWith(p)));
            }

            if (!parameters.LocationIds.IsNullOrEmpty())
            {
                rows = rows.Where(a =>
                    a.Employee.LocationId.HasValue &&
                    parameters.LocationIds.Contains(a.Employee.LocationId.Value));
            }

            return rows;
        }

        protected abstract IQueryable<EmployeeAbsence> FilterByDatePeriod(TParameters parameters, IQueryable<EmployeeAbsence> rows);
    }
}
