﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.ReportModels;
using Smt.Atomic.Business.TimeTracking.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.ReportDataSources
{
    [Identifier("DataSources.AllocatedAndReportedHoursDataSource")]
    [RequireRole(SecurityRoleType.CanGenerateAllocatedAndReportedHoursReport)]
    [DefaultReportDefinition("AllocatedAndReportedHours",
        nameof(TimeReportServiceResources.TimeTrackingAllocatedAndReportedHours),
        nameof(TimeReportServiceResources.TimeTrackingAllocatedAndReportedHoursDescription),
        MenuAreas.TimeTracking,
        MenuGroups.TimeTrackingReportsTT,
        ReportingEngine.Chart,
        "Smt.Atomic.Business.TimeTracking.ReportTemplates.AllocatedAndReportedHoursChart-config.js",
        typeof(TimeReportServiceResources))]
    [DefaultReportDefinition("AllocatedAndReportedHoursPivot",
        nameof(TimeReportServiceResources.TimeTrackingAllocatedAndReportedHoursPivot),
        nameof(TimeReportServiceResources.TimeTrackingAllocatedAndReportedHoursDescriptionPivot),
        MenuAreas.TimeTracking,
        MenuGroups.TimeTrackingReportsTT,
        ReportingEngine.PivotTable,
        "Smt.Atomic.Business.TimeTracking.ReportTemplates.AllocatedAndReportedHoursPivot-config.js",
        typeof(TimeReportServiceResources))]
    public class AllocatedAndReportedHoursDataSource : BaseReportDataSource<AllocatedAndReportedHoursParametersDto>
    {
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService;
        private readonly ITimeService _timeService;
        private readonly IOrgUnitService _orgUnitService;
        private readonly IPrincipalProvider _principalProvider;

        public AllocatedAndReportedHoursDataSource(
            IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
            IPrincipalProvider principalProvider,
            IOrgUnitService orgUnitService,
            ITimeService timeService)
        {
            _unitOfWorkService = unitOfWorkService;
            _timeService = timeService;
            _orgUnitService = orgUnitService;
            _principalProvider = principalProvider;
        }

        public override object GetData(ReportGenerationContext<AllocatedAndReportedHoursParametersDto> reportGenerationContext)
        {
            const string unspecified = "unspecified";

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var parameters = reportGenerationContext.Parameters;
                var startDate = _timeService.GetCurrentDate().AddMonths(-9).StartOfDay();
                var endDate = _timeService.GetCurrentDate().AddMonths(3).EndOfDay();
                var employees = FilterDataScope(parameters, unitOfWork);

                var reportRows = employees
                    .SelectMany(e => e.TimeReports)
                    .SelectMany(r => r.Rows);

                if (!parameters.UtilizationCategoryIds.IsNullOrEmpty())
                {
                    reportRows = reportRows.Where(r => r.Project.UtilizationCategoryId.HasValue && parameters.UtilizationCategoryIds.Contains(r.Project.UtilizationCategoryId.Value));
                }

                var reportedHours = reportRows
                    .Include(r => r.DailyEntries)
                    .SelectMany(r => r.DailyEntries)
                    .Where(DateRangeHelper.IsDateInRange<TimeReportDailyEntry>(startDate, endDate, e => e.Day).AsExpression())
                    .Where(FilterBillability(parameters.BillabilityFiltering))
                    .GroupBy(d => new { d.TimeReportRow.TimeReport.Employee, d.Day })
                    .Select(d => new
                    {
                        Id = d.Key.Employee.Id,
                        JobProfile = d.Key.Employee.JobProfiles.Any() ? d.Key.Employee.JobProfiles.FirstOrDefault().Name : unspecified,
                        OrgUnitName = d.Key.Employee.OrgUnit.Name,
                        OrgUnitFlatName = d.Key.Employee.OrgUnit.FlatName,
                        Location = d.Key.Employee.Location.Name,
                        PlaceOfWork = d.Key.Employee.PlaceOfWork,
                        LineManager = d.Key.Employee.LineManager.Acronym,
                        Level = d.Key.Employee.JobMatrixs.Any() ? d.Key.Employee.JobMatrixs.FirstOrDefault().Code : unspecified,
                        Day = d.Key.Day,
                        ReportedHours = d.Sum(e => e.Hours),
                        AllocationHours = 0M
                    }).ToList();

                var allocatedHours = employees
                    .SelectMany(a => a.WeeklyAllocationStatuses)
                    .Where(DateRangeHelper.IsDateInRange<WeeklyAllocationStatus>(startDate, endDate, d => d.Week).AsExpression())
                    .GroupBy(d => new { d.Employee, d.Week })
                    .Select(d => new
                    {
                        Id = d.Key.Employee.Id,
                        JobProfile = d.Key.Employee.JobProfiles.Any() ? d.Key.Employee.JobProfiles.FirstOrDefault().Name : unspecified,
                        OrgUnitName = d.Key.Employee.OrgUnit.Name,
                        OrgUnitFlatName = d.Key.Employee.OrgUnit.FlatName,
                        Location = d.Key.Employee.Location.Name,
                        PlaceOfWork = d.Key.Employee.PlaceOfWork,
                        LineManager = d.Key.Employee.LineManager.Acronym,
                        Level = d.Key.Employee.JobMatrixs.Any() ? d.Key.Employee.JobMatrixs.FirstOrDefault().Code : unspecified,
                        Day = d.Key.Week,
                        ReportedHours = 0M,
                        AllocationHours = d.Sum(s => s.AllocatedHours)
                    }).ToList();

                return new AllocatedAndReportedHoursReportModels
                {
                    AllocatedAndReportedHours = allocatedHours
                    .Union(reportedHours)
                    .GroupBy(r => new { r.Id, r.JobProfile, r.Level, r.LineManager, r.Location, r.OrgUnitFlatName, r.OrgUnitName, r.PlaceOfWork, WeekNumber = r.Day.GetWeekOfYear(), Year = r.Day.Year })
                    .Select(g => new AllocatedAndReportedHoursReportModel
                    {
                        Id = g.Key.Id,
                        JobProfile = g.Key.JobProfile,
                        Level = g.Key.Level,
                        LineManager = g.Key.LineManager,
                        Location = g.Key.Location,
                        OrgUnitName = g.Key.OrgUnitName,
                        OrgUnitFlatName = g.Key.OrgUnitFlatName,
                        PlaceOfWork = g.Key.PlaceOfWork,
                        Year = g.Key.Year,
                        WeekNumber = g.Key.WeekNumber,
                        ReportedHours = g.Sum(b => b.ReportedHours),
                        AllocationHours = g.Sum(b => b.AllocationHours)
                    })
                    .OrderBy(r => r.Year)
                    .ThenBy(r => r.WeekNumber)
                    .ToList()
                };
            }
        }

        public override AllocatedAndReportedHoursParametersDto GetDefaultParameters(ReportGenerationContext<AllocatedAndReportedHoursParametersDto> reportGenerationContext)
        {
            const string IdleCategoryCode = "IDLE";

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var nonIdleCategoryIds = unitOfWork
                    .Repositories
                    .UtilizationCategories
                    .Where(c => c.NavisionCode != IdleCategoryCode)
                    .Select(c => c.Id);

                return new AllocatedAndReportedHoursParametersDto
                {
                    UtilizationCategoryIds = nonIdleCategoryIds.ToArray()
                };
            }
        }

        protected virtual IQueryable<Employee> FilterDataScope(AllocatedAndReportedHoursParametersDto parameters, IUnitOfWork<ITimeTrackingDbScope> unitOfWork)
        {
            var rows = unitOfWork.Repositories.Employees.Where(e => e.IsProjectContributor);

            if (!parameters.LocationIds.IsNullOrEmpty())
            {
                rows = rows.Where(e => e.LocationId.HasValue && parameters.LocationIds.Contains(e.LocationId.Value));
            }

            if (!parameters.EmployeeOrgUnitIds.IsNullOrEmpty())
            {
                rows = rows.Where(e => parameters.EmployeeOrgUnitIds.Any(i => e.OrgUnit.Path.Contains("/" + i + "/")));
            }

            if (!parameters.ProfileIds.IsNullOrEmpty())
            {
                rows = rows.Where(e => e.JobProfiles.Any(j => parameters.ProfileIds.Contains(j.Id)));
            }

            if (!parameters.LineManagerIds.IsNullOrEmpty())
            {
                rows = rows.Where(e => e.LineManagerId != null && parameters.LineManagerIds.Contains(e.LineManagerId.Value));
            }

            if (!parameters.JobMatrixIds.IsNullOrEmpty())
            {
                rows = rows.Where(e => e.JobMatrixs.Any(j => parameters.JobMatrixIds.Contains(j.Id)));
            }

            if (parameters.PlaceOfWork != null)
            {
                rows = rows.Where(e => parameters.PlaceOfWork == e.PlaceOfWork);
            }

            return rows;
        }

        private Expression<Func<TimeReportDailyEntry, bool>> FilterBillability(BillabilityFiltering billability)
        {
            if (billability == BillabilityFiltering.All)
            {
                return t => true;
            }

            if (billability == BillabilityFiltering.Billable)
            {
                return e => e.TimeReportRow.AttributeValues.All(v => !v.Features.HasFlag(AttributeValueFeature.NonBillable));
            }

            return e => e.TimeReportRow.AttributeValues.Any(v => v.Features.HasFlag(AttributeValueFeature.NonBillable));
        }
    }
}
