﻿using Smt.Atomic.Business.TimeTracking.Resources;
using Smt.Atomic.CrossCutting.Common.Exceptions;

namespace Smt.Atomic.Business.TimeTracking.Exceptions
{
    public class NoTimeTrackingConfiguredForUserException : BusinessException
    {
        public NoTimeTrackingConfiguredForUserException()
            : base(TimeTrackingExceptionResources.NoTimeTrackingConfiguredForUserException)
        {
        }
    }
}
