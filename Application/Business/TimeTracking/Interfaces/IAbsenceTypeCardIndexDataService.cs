﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.TimeTracking.Dto;

namespace Smt.Atomic.Business.TimeTracking.Interfaces
{
    public interface IAbsenceTypeCardIndexDataService : ICardIndexDataService<AbsenceTypeDto>
    {
        IEnumerable<long> AllowedAbsenceTypes(DateTime from, DateTime to, long? onBehalfOf);
    }
}

