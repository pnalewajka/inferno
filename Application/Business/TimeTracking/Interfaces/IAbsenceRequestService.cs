﻿using System;

namespace Smt.Atomic.Business.TimeTracking.Interfaces
{
    public interface IAbsenceRequestService
    {
        DateTime GetProperDateForNextMeeting(long userId, DateTime date);
    }
}