﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.TimeTracking.Dto;

namespace Smt.Atomic.Business.TimeTracking.Interfaces
{
    public interface IOvertimeApprovalDataService
    {
        IEnumerable<OvertimeApprovalDto> OvertimeApprovals(IReadOnlyCollection<long> projectIds, DateTime from, DateTime to, long employeeId);

        IEnumerable<OvertimeApprovalDto> OvertimeApproval(long projectId, DateTime from, DateTime to, long employeeId);
    }
}