﻿using Smt.Atomic.Business.TimeTracking.Dto;

namespace Smt.Atomic.Business.TimeTracking.Interfaces
{
    public interface ITimeReportImportSource
    {
        MyTimeReportDto GetTimeReport(long employeeId, int year, byte month);
    }
}
