﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Interfaces
{
    public interface IAbsenceService
    {
        string GetAbsenceTypeDescriptionByAbsenceTypeId(long absenceTypeId, long userId);

        LocalizedString GetAbsenceTypeNameByAbsenceTypeId(long absenceTypeId);

        MyTimeReportDto RecalculateAbsences(MyTimeReportDto myTimeReportDto);
        
        void UpdateTimeReportOnSubmitted(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, long requestId, long absenceTypeId, DateTime absenceFrom,
            DateTime absenceTo, long employeeId, decimal? dailyReportedHours);

        void UpdateTimeReportOnRejected(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, long requestId,
            DateTime absenceFrom, DateTime absenceTo, long employeeId);

        decimal GetAvailableVacationHours(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, long employeeId, DateTime? effectiveDate);

        decimal GetNoServiceHours(long employeeId, int year, byte month);

        long[] GetHRApprovalRequiredAbsenceTypeIds();

        bool IsHourlyReportingAllowed(long absenceTypeId, DateTime from, DateTime to);

        bool IsHourlyReportingAllowed(AbsenceType absenceType, DateTime from, DateTime to);

        void NotifyAboutRevokedAbsence(RequestDto requestDto, AbsenceRequestDto absenceRequestDto);

        IList<AbsenceRequestDto> GetAbsenceRequestsContainSpecificDate(long userId, DateTime date);
    }
}