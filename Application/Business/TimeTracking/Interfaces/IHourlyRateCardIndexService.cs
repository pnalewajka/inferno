﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.TimeTracking.Dto;

namespace Smt.Atomic.Business.TimeTracking.Interfaces
{
    public interface IHourlyRateCardIndexService : ICardIndexDataService<HourlyRateDto>
    {
    }
}
