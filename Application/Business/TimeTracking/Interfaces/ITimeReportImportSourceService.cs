﻿using System.Collections.Generic;
using Smt.Atomic.Business.TimeTracking.Dto;

namespace Smt.Atomic.Business.TimeTracking.Interfaces
{
    public interface ITimeReportImportSourceService
    {
        MyTimeReportDto GetTimeReport(long importSourceId, long employeeId, int year, byte month);

        IList<TimeTrackingImportSourceDto> GetImportSources();

        long GetImportSourceIdFromCode(string code);
    }
}
