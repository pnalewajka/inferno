﻿namespace Smt.Atomic.Business.TimeTracking.Interfaces
{
    public interface ICalendarAbsenceService
    {
        bool IsUserOwnerCalendarAbsence(long userId, long calendarAbsenceId);
    }
}
