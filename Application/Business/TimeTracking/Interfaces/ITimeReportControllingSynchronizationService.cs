﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.TimeTracking.Interfaces
{
    public interface ITimeReportControllingSynchronizationService
    {
        void ForceSynchronization(IEnumerable<long> timeReportIds);

        void MarkAllForSynchronization(int year, int month);

        void SynchronizeMarkedTimeReports();
    }
}
