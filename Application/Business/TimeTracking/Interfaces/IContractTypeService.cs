﻿using Smt.Atomic.Business.TimeTracking.Dto;

namespace Smt.Atomic.Business.TimeTracking.Interfaces
{
    public interface IContractTypeService
    {
        ContractTypeDto GetContractTypeOrDefaultById(long id);

        ContractTypeDto GetCurrentContractTypeOrDefaultByEmployeeId(long employeeId);

        ContractTypeDto GetLatestContractTypeOrDefaultByEmployeeId(long employeeId);
    }
}
