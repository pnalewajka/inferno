﻿using Smt.Atomic.Business.TimeTracking.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.TimeTracking.Interfaces
{
    public interface ITimeReportProjectAttributeService
    {
        IList<TimeReportProjectAttributeDto> GetAttributesByProjectId(long projectId);
    }
}
