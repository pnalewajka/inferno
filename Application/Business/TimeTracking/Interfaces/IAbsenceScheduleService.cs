﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.TimeTracking.Dto;

namespace Smt.Atomic.Business.TimeTracking.Interfaces
{
    public interface IAbsenceScheduleService
    { 
        ScheduleListDto CalculateSchedule(IReadOnlyCollection<string> userAcronyms, IReadOnlyCollection<long> projectIds, DateTime periodStartDate, DateTime periodEndDate);
    }
}
