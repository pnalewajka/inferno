﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.TimeTracking.Dto;
using System.Collections.Generic;

namespace Smt.Atomic.Business.TimeTracking.Interfaces
{
    public interface IMyVacationBalanceCardIndexDataService: ICardIndexDataService<VacationBalanceDto>
    {
        IEnumerable<FilterYearDto> GetFilterYears();
    }
}
