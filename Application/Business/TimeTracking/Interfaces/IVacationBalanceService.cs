﻿using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using System;

namespace Smt.Atomic.Business.TimeTracking.Interfaces
{
    public interface IVacationBalanceService
    {
        decimal GetHourBalanceForEmployee(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, long employeeId);
        decimal GetUsedVacationHoursForEmployee(long employeeId, DateTime startDate, DateTime endDate);
        decimal GetHourBalanceForEmployeeUpToEffectiveDate(long employeeId, DateTime effectiveOn);
        decimal GetHourBalanceForEmployeeUpToEffectiveDate(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, long employeeId, DateTime effectiveOn);
    }
}
