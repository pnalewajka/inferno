﻿using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using System;

namespace Smt.Atomic.Business.TimeTracking.Interfaces
{
    public interface IOvertimeBankService
    {
        decimal GetEmployeeOvertimeBalance(long employeeId, DateTime? effectiveDate = null);

        decimal GetEmployeeOvertimeBalance(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, long employeeId, DateTime? effectiveDate = null);
    }
}