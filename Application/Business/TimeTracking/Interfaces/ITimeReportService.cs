﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Interfaces
{
    public interface ITimeReportService
    {
        TimeReportDto GetTimeReport(long employeeId, int year, byte month);

        IReadOnlyCollection<TimeReportDto> GetEmployeeTimeReports(long employeeId, DateTime dateFrom, DateTime dateTo);

        IReadOnlyCollection<TimeReportDto> GetMyEmployeesTimeReports(long employeeId, int year, byte month);

        IReadOnlyCollection<TimeReportDto> GetMyProjectEmployeesTimeReports(long employeeId, int year, byte month);

        TimeReportingMode GetTimeReportingMode(long employeeId, int year, byte month);

        MyTimeReportDto GetMyTimeReport(long employeeId, int year, byte month, bool forceReadOnly = false);

        BusinessResult ValidateFullTimeReport(long employeeId, int year, byte month);

        BoolResult CheckOnSaveViolations(MyTimeReportDto currentTimeReportDto, MyTimeReportDto originalTimeReportDto);

        void UpdateFullTimeReport(MyTimeReportDto myTimeReportDto);

        BoolResult UpdateTimeReportFromImportSource(MyTimeReportDto myTimeReportDto);

        void UpdateTimeReportStatus(long timeReportId, TimeReportStatus newStatus, IEnumerable<long> unlinkRequestIds = null);

        void UpdateTimeReportStatus(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, long timeReportId, TimeReportStatus newStatus, IEnumerable<long> unlinkRequestIds = null);

        void UpdateContractedHours(DateTime day);

        void UpdateContractedHours(long employeeId, DateTime from, DateTime? to);

        IEnumerable<string> GetUserTimeReportSubmitWarnings(long employeeId, int year, byte month);

        BusinessResult OnSubmit(int year, byte month, long employeeId);

        BoolResult ReopenTimeReport(long employeeId, int year, byte month, string reason, long[] projectIdsToReopen);

        IDictionary<long, ProjectTimeReportApprovalRequestDto> GetReopenableProjectTimeReportApprovalRequests(long employeeId, int year, byte month);

        bool CanBeReopened(long employeeId, int year, byte month);

        bool IsPreviousMonthAvailable(long employeeId, int year, byte month);

        TimeTrackingProjectDto GetTimeTimeTrackingProjectDtoById(long projectId);

        IEnumerable<MyTimeReportDayDescriptionDto> CalculateDayDescriptions(int year, byte month, long employeeId);

        IList<TimeReportProjectAttributeDto> GetDefaultAttributesDtoByProjectId(long projectId);

        void AddEmployeeProjectsFromPreviousMonth(long employeeId, int year, byte month, bool importTasks = false);

        void UpdateTimeReportRowsStatus(long requestId, TimeReportStatus newStatus);

        void UpdateTimeReportRowsStatus(IEnumerable<long> rowIds, TimeReportStatus newStatus);

        IReadOnlyCollection<EmployeeOvertimeInProjectDto> GetOvertimeTimeReportRows(IReadOnlyCollection<long> projectIds, IReadOnlyCollection<long> employeeIds, int year, byte month);
    }
}
