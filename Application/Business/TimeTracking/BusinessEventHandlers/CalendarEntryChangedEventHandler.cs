﻿using Smt.Atomic.Business.Configuration.BusinessEvents;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Business.TimeTracking.Interfaces;

namespace Smt.Atomic.Business.TimeTracking.BusinessEventHandlers
{
    public class CalendarEntryChangedEventHandler : BusinessEventHandler<CalendarEntryChangedBusinessEvent>
    {
        private readonly ITimeReportService _timeReportService;

        public CalendarEntryChangedEventHandler(ITimeReportService timeReportService)
        {
            _timeReportService = timeReportService;
        }

        public override void Handle(CalendarEntryChangedBusinessEvent businessEvent)
        {
            _timeReportService.UpdateContractedHours(businessEvent.Day);
        }
    }
}
