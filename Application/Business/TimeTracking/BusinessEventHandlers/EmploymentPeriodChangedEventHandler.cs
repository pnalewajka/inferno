﻿using Smt.Atomic.Business.Allocation.BusinessEvents;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Business.TimeTracking.Interfaces;

namespace Smt.Atomic.Business.TimeTracking.BusinessEventHandlers
{
    public class EmploymentPeriodChangedEventHandler : BusinessEventHandler<EmploymentPeriodChangedBusinessEvent>
    {
        private readonly ITimeReportService _timeReportService;
        private readonly IEmployeeService _employeeService;

        public EmploymentPeriodChangedEventHandler(ITimeReportService timeReportService, IEmployeeService employeeService)
        {
            _timeReportService = timeReportService;
            _employeeService = employeeService;
        }

        public override void Handle(EmploymentPeriodChangedBusinessEvent businessEvent)
        {
            _employeeService.UpdateCurrentEmployeeStatus(businessEvent.EmployeeId);
            _employeeService.UpdateEmployeeBasedOnEmploymentPeriod(businessEvent.EmployeeId);
            _timeReportService.UpdateContractedHours(businessEvent.EmployeeId, businessEvent.From, businessEvent.To);
        }
    }
}
