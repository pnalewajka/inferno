﻿using System.Linq;
using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.TimeTracking.BusinessEvents;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.BusinessEventHandlers
{
    public class TimeReportReopenedBusinessEventHandler : BusinessEventHandler<TimeReportReopenedBusinessEvent>
    {
        private readonly IUnitOfWorkService<IWorkflowsDbScope> _unitOfWorkService;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly ISystemParameterService _systemParameterService;

        public TimeReportReopenedBusinessEventHandler(IUnitOfWorkService<IWorkflowsDbScope> unitOfWorkService, IReliableEmailService reliableEmailService, IMessageTemplateService messageTemplateService, ISystemParameterService systemParameterService)
        {
            _unitOfWorkService = unitOfWorkService;
            _reliableEmailService = reliableEmailService;
            _messageTemplateService = messageTemplateService;
            _systemParameterService = systemParameterService;
        }

        public override void Handle(TimeReportReopenedBusinessEvent businessEvent)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var reopenRequest = unitOfWork
                    .Repositories
                    .ReopenTimeTrackingReportRequests
                    .SelectById(businessEvent.RequestId, r => new
                    {
                        r.Id,
                        r.Year,
                        r.Month,
                        EmployeeName = UserBusinessLogic.FullName.Call(r.Request.RequestingUser),
                        CompanyActiveDirectoryCode = r.Request
                            .RequestingUser
                            .Employees
                            .Where(e => e.CompanyId.HasValue)
                            .Select(e => e.CompanyId.HasValue ? e.Company.ActiveDirectoryCode : "")
                            .FirstOrDefault(),
                        ApprovedBy = r.Request
                            .ApprovalGroups
                            .SelectMany(g =>
                                g.Approvals.Where(a => a.Status == ApprovalStatus.Approved)
                                .Select(a => new
                                {
                                    ApproverName = UserBusinessLogic.FullName.Call(a.User),
                                    ForcedByApproverName = a.ForcedByUser == null ? string.Empty : UserBusinessLogic.FullName.Call(a.ForcedByUser)
                                }))
                        .ToList()
                    });

                if (!string.IsNullOrEmpty(reopenRequest.CompanyActiveDirectoryCode))
                {
                    var timeReportReopenedCompanyNotifyPersons = _systemParameterService.GetParameter<string[]>(ParameterKeys.TimeReportReopenedCompanyNotifyPersons, new ReopenTimeReportNotifyEmailContext(reopenRequest.CompanyActiveDirectoryCode));

                    if (timeReportReopenedCompanyNotifyPersons.Any())
                    {
                        var emailModelDto = new TimeReportReopenedEmailDto
                        {
                            TimeReportCode = $"{reopenRequest.Month:D2}-{reopenRequest.Year}",
                            TimeReportUserDisplayName = reopenRequest.EmployeeName,
                            RequestApproverDisplayName = string.Join(", ", reopenRequest.ApprovedBy.Select(a => ApproverDisplayName(a.ApproverName, a.ForcedByApproverName)))
                        };

                        var subject = _messageTemplateService.ResolveTemplate(TemplateCodes.TimeTrackingReopenTimeReportSubject, emailModelDto);
                        var body = _messageTemplateService.ResolveTemplate(TemplateCodes.TimeTrackingReopenTimeReportBody, emailModelDto);

                        _reliableEmailService.EnqueueMessage(new EmailMessageDto
                        {
                            Body = body.Content,
                            Subject = subject.Content,
                            IsHtml = true,
                            Recipients = timeReportReopenedCompanyNotifyPersons.Select(a => new EmailRecipientDto { EmailAddress = a }).ToList()
                        });
                    }
                }
            }
        }

        private string ApproverDisplayName(string approverName, string forcingUserName = null)
        {
            if (string.IsNullOrEmpty(forcingUserName))
            {
                return approverName;
            }

            return string.Format(TimeReportResources.OnBehalfFormat, forcingUserName, approverName);
        }
    }
}
