﻿using System.Linq;
using Castle.Core.Logging;
using Smt.Atomic.Business.Compensation.BusinessEvents;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Business.TimeTracking.BusinessEvents;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.BusinessEventHandlers
{
    public class ProjectTimeReportApprovedEventHandler : BusinessEventHandler<ProjectTimeReportApprovedBusinessEvent>
    {
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService;
        private readonly ILogger _logger;
        private readonly IBusinessEventPublisher _businessEventPublisher;

        public ProjectTimeReportApprovedEventHandler(
            IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
            ILogger logger,
            IBusinessEventPublisher businessEventPublisher)
        {
            _unitOfWorkService = unitOfWorkService;
            _logger = logger;
            _businessEventPublisher = businessEventPublisher;
        }

        public override void Handle(ProjectTimeReportApprovedBusinessEvent businessEvent)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var timeReport = unitOfWork.Repositories.TimeReports.SingleOrDefault(
                    r => r.EmployeeId == businessEvent.EmployeeId 
                         && r.Year == businessEvent.Year 
                         && r.Month == businessEvent.Month);

                if (timeReport != null)
                {
                    if (timeReport.Status == TimeReportStatus.Submitted && timeReport.IsFullyAccepted())
                    {
                        timeReport.Status = TimeReportStatus.Accepted;
                        _logger.InfoFormat($"Accepting time report for employee id: {businessEvent.EmployeeId}, year: {businessEvent.Year} and month: {businessEvent.Month}");

                        unitOfWork.Commit();

                        _businessEventPublisher.PublishBusinessEvent(
                            new CompensationChangedEvent(timeReport.EmployeeId, timeReport.Year, timeReport.Month));
                    }
                }
                else
                {
                    _logger.ErrorFormat($"Time report for employee id: {businessEvent.EmployeeId}, year: {businessEvent.Year} and month: {businessEvent.Month} was not found.");
                }
            }
        }
    }
}