﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;

namespace Smt.Atomic.Business.TimeTracking.Helpers
{
    internal class CalendarAbsenceSortHelper
    {
        public static void OrderBySkillTags(OrderedQueryBuilder<CalendarAbsence> builder, bool ascending)
        {
            var direction = ascending ? SortingDirection.Ascending : SortingDirection.Descending;
            builder.ApplySortingKey(r => !r.EmployeesIds.Any(), direction);
            builder.ApplySortingKey(SkillsSortingExpression, direction);
        }

        private static Expression<Func<CalendarAbsence, string>> SkillsSortingExpression => r =>
            r.EmployeesIds.OrderBy(s => s.FirstName + " " + s.LastName)
                .Select(s => s.FirstName + " " + s.LastName)
                .FirstOrDefault();
    }
}