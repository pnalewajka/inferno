﻿namespace Smt.Atomic.Business.TimeTracking.Helpers
{
    public class ProjectDashboardFilterCodes
    {
        public const string ByProject = "by-project";
        public const string ByOrgUnit = "by-org-unit";
        public const string MyProjects = "my-projects";
        public const string AllProjects = "all-projects";
    }
}