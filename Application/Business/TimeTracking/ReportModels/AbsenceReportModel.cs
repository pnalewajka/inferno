﻿using System;

namespace Smt.Atomic.Business.TimeTracking.ReportModels
{
    public class AbsenceReportModel
    {
        public DateTime From { get; set; }

        public DateTime To { get; set; }

        public DateTime ApprovedOn { get; set; }

        public EmployeeReportModel Employee { get; set; }

        public string AbsenceTypeCode { get; set; }

        public bool IsVacation { get; set; }

        public string ContractType { get; set; }

        public DateTime RequestedOn { get; set; }

        public string ApprovedBy { get; set; }

        public double DaysOfAbsence { get; set; }

        public int WorkingDays { get; set; }
    }
}