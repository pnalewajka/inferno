using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Smt.Atomic.Business.TimeTracking.ReportModels
{
    public class TimeTrackingTasksReportModel
    {
        [JsonIgnore]
        public Lazy<IEnumerable<EmployeeReportModel>> Employees => new Lazy<IEnumerable<EmployeeReportModel>>(() =>
            Tasks.Select(t => t.Employee)
                .GroupBy(e => e.Id).Select(e => e.FirstOrDefault())
                .OrderBy(e => e.LastName).ThenBy(e => e.FirstName)
                .ToList());

        public IEnumerable<TimeTrackingTaskReportModel> Tasks { get; set; }
    }
}