﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.TimeTracking.ReportModels
{
    public class AllocatedAndReportedHoursReportModel
    {
        public long Id { get; set; }

        public int WeekNumber { get; set; }

        public int Year { get; set; }

        public decimal AllocationHours { get; set; }

        public decimal ReportedHours { get; set; }

        public string JobProfile { get; set; }

        public string OrgUnitName { get; set; }

        public string OrgUnitFlatName { get; set; }

        public string Location { get; set; }

        public string LineManager { get; set; }

        public string Level { get; set; }

        public PlaceOfWork? PlaceOfWork { get; set; }

        public DateTime Day { get; set; }
    }
}
