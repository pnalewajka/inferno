﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.TimeTracking.ReportModels
{
    public class AllocatedAndReportedHoursReportModels
    {
        public IEnumerable<AllocatedAndReportedHoursReportModel> AllocatedAndReportedHours { get; set; }
    }
}
