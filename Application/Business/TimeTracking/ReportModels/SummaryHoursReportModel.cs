﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.TimeTracking.ReportModels
{
    public class SummaryHoursReportModel
    {
        public IEnumerable<TRSummaryHoursReportModel> TimeReports { get; set; }
    }
}
