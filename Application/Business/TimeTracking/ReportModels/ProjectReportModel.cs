using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.TimeTracking.ReportModels
{
    public class ProjectReportModel
    {
        public string Apn { get; set; }

        public string PetsCode { get; set; }

        public string KupferwerkProjectId { get; set; }

        public string Client { get; set; }

        public string OrgUnitName { get; set; }

        public string OrgUnitCode { get; set; }

        public string Name { get; set; }

        public ProjectStatus Status { get; set; }
    }
}