﻿using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.TimeTracking.ReportModels
{
    public class VacationBalanceReportModel
    {
        public long EmployeeId { get; set; }

        public string EmployeeFirstName { get; set; }

        public string EmployeeLastName { get; set; }

        public long UserId { get; set; }

        public string Acronym { get; set; }

        public decimal VacationBalance { get; set; }

        public decimal VacationTakenSum { get; set; }

        public decimal OvertimeBankBalance { get; set; }

        public string CompanyName { get; set; }

        public string OrgUnitName { get; set; }

        public string LocationName { get; set; }

        public string LineManagerName { get; set; }

        public string OvertimeBankBalanceEvectiveOn { get; set; }

        public decimal VacationRequestedHours { get; set; }

        public decimal VacationPlannedHours { get; set; }

        public decimal OvertimeRequested { get; set; }

        public decimal OvertimePlanned { get; set; }

        public decimal VacationBalanceInDays { get; set; }

        public decimal VacationLeaveRequestedHours { get; set; }

        public decimal VacationLeavePlannedHours { get; set; }
    }
}
