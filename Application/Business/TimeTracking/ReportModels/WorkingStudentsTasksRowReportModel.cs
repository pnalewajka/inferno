﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.TimeTracking.ReportModels
{
    public class WorkingStudentsTasksRowReportModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Acronym { get; set; }

        public DateTime Date { get; set; }

        public string Project { get; set; }

        public string Task { get; set; }

        public decimal Hours { get; set; }

        public TimeReportStatus ReportStatus { get; set; }
    }
}
