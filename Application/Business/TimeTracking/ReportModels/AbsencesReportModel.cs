﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.TimeTracking.ReportModels
{
    public class AbsencesReportModel
    {
        public IEnumerable<AbsenceReportModel> Absences { get; set; }
    }
}
