﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.TimeTracking.ReportModels
{
    // ReSharper disable once InconsistentNaming
    public class TRSummaryHoursReportModel
    {
        public long TimeReportId { get; set; }

        public DateTime StartDate { get; set; }

        public TimeReportStatus TimeReportStatus { get; set; }

        public EmployeeReportModel Employee { get; set; }

        public decimal RegularHours { get; set; }

        public decimal OverTimeNormalHours { get; set; }

        public decimal OverTimeLateNightHours { get; set; }

        public decimal OverTimeWeekendHours { get; set; }

        public decimal OverTimeHolidayHours { get; set; }

        public decimal TotalHours { get; set; }

        public decimal OverTimeToBePaidHours { get; set; }

        public ICollection<DataColumn> HoursPerAbsenceType { get; set; }
    }
}
