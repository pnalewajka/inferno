﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.TimeTracking.ReportModels
{
    public class WorkingStudentsReportModel
    {
        public IEnumerable<WorkingStudentsRowReportModel> WorkingStudentsReportRows { get; set; }

        public IEnumerable<WorkingStudentsTasksRowReportModel> WorkingStudentsTasksReportRows { get; set; }
    }
}
