﻿using System;

namespace Smt.Atomic.Business.TimeTracking.ReportModels
{
    public class AbsenceKbrReportModel
    {
        public string IssueType { get; set; }

        public string IssueKey { get; set; }

        public long IssueId { get; set; }

        public string Summary { get; set; }

        public string Reporter { get; set; }

        public string Acronym { get; set; }

        public string From { get; set; }

        public string To { get; set; }

        public string ApprovedOn { get; set; }

        public string RequestingPerson { get; set; }

        public string AffectedUser { get; set; }
    }
}
