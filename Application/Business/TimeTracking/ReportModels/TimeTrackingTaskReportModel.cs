using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.TimeTracking.ReportModels
{
    public class TimeTrackingTaskReportModel
    {
        public string TaskName { get; set; }

        public bool IsImported { get; set; }

        public ProjectReportModel Project { get; set; }

        public EmployeeReportModel Employee { get; set; }

        public string ReportUrl { get; set; }

        public DateTime? Date { get; set; }

        public decimal Hours { get; set; }

        public TimeReportStatus ReportStatus { get; set; }

        public RequestStatus? RequestStatus { get; set; }

        public bool IsRegular { get; set; }

        public HourlyRateType OvertimeVariant { get; set; }

        public ICollection<DataColumn> Attributes { get; set; }

        public bool IsBillable { get; set; }

        public string ImportedTaskCode { get; set; }

        [MaxLength(128)]
        public string ImportedTaskType { get; set; }

        public int WeekNumber { get; set; }
    }
}