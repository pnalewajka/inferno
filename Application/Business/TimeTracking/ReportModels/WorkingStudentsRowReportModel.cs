﻿using System.Collections.Generic;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel;

namespace Smt.Atomic.Business.TimeTracking.ReportModels
{
    public class WorkingStudentsRowReportModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Acronym { get; set; }

        public decimal TotalHours { get; set; }

        public decimal BankHolidayHours { get; set; }

        public string SpecialPaidLeave { get; set; }

        public string UnpaidLeave { get; set; }

        public ICollection<DataColumn> HoursWeekly { get; set; }
    }
}
