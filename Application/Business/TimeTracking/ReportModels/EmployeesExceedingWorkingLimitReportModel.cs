﻿using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.TimeTracking.ReportModels
{
    public class EmployeeWithDailyExceededHoursModel
    {
        public EmployeeReportModel Employee { get; set; }

        public ICollection<DataColumn> ExceedingDays { get; set; }
    }

    public class EmployeesExceedingWorkingLimitReportModel
    {
        public IEnumerable<EmployeeWithDailyExceededHoursModel> Employees { get; set; }
    }
}
