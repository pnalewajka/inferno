﻿using Smt.Atomic.Data.Entities.Migrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.TimeTracking.ReportModels
{
    public class VacationBalancesReportModel
    {
        public IEnumerable<VacationBalanceReportModel> VacationBalances { get; set; }
    }
}
