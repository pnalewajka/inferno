﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.TimeTracking.ReportModels
{
    public class TimeTrackingProjectSummaryReportModel
    {
        public string ProjectName { get; set; }

        public IEnumerable<ProjectSummaryReportModel> Tasks { get; set; }
    }
}