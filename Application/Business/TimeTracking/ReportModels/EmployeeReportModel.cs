using System;

namespace Smt.Atomic.Business.TimeTracking.ReportModels
{
    public class EmployeeReportModel
    {
        public long Id { get; set; }

        public Guid? ActiveDirectoryId { get; set; }

        public string Login { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Acronym { get; set; }

        public string Email { get; set; }

        public string CompanyName { get; set; }

        public string OrgUnitName { get; set; }

        public string OrgUnitCode { get; set; }

        public string LocationName { get; set; }

        public string JobTitle { get; set; }

        public decimal Rate { get; set; }

        public string Seniority { get; set; }

        public override bool Equals(object obj)
        {
            return obj != null && obj.GetType() == typeof(EmployeeReportModel) && Id == ((EmployeeReportModel)obj).Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}