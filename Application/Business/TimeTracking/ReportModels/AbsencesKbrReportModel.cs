﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.TimeTracking.ReportModels
{
    public class AbsencesKbrReportModel
    {
        public IEnumerable<AbsenceKbrReportModel> AbsencesKbr { get; set; }
    }
}
