using System.Collections.Generic;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel;

namespace Smt.Atomic.Business.TimeTracking.ReportModels
{
    public class ProjectSummaryReportModel
    {
        public ProjectSummaryReportModel()
        {
            Days = new List<DataColumn>();
            Attributes = new List<DataColumn>();
        }

        public string ProjectName { get; set; }

        public string GroupName { get; set; }

        public string EmployeeName { get; set; }

        public string TaskName { get; set; }

        public ICollection<DataColumn> Days { get; set; }

        public ICollection<DataColumn> Attributes { get; set; }

        public decimal TaskHoursSum { get; set; }
    }
}