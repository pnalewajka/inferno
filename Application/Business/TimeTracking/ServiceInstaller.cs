﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Accounts.Services;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.EventSourcing.Interfaces;
using Smt.Atomic.Business.TimeTracking.BusinessEventHandlers;
using Smt.Atomic.Business.TimeTracking.ChoreProviders;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Business.TimeTracking.Services;
using Smt.Atomic.Business.TimeTracking.Workflows.Services;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.TimeTracking
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            if (containerType == ContainerType.WebApp || containerType == ContainerType.JobScheduler)
            {
                container.Register(Component.For<ITimeReportService>().ImplementedBy<TimeReportService>().LifestyleTransient());
                container.Register(Component.For<IVacationBalanceService>().ImplementedBy<VacationBalanceService>().LifestyleTransient());
                container.Register(Component.For<ITimeTrackingProjectCardIndexDataService>().ImplementedBy<TimeTrackingProjectCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IVacationBalanceCardIndexDataService>().ImplementedBy<VacationBalanceCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ICalendarAbsenceCardIndexDataService>().ImplementedBy<CalendarAbsenceCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ITimeTrackingImportSourceCardIndexDataService>().ImplementedBy<TimeTrackingImportSourceCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IMyVacationBalanceCardIndexDataService>().ImplementedBy<MyVacationBalanceCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IRequestService, IAbsenceRequestService>().ImplementedBy<AbsenceRequestService>().LifestyleTransient());
                container.Register(Component.For<IRequestService>().ImplementedBy<ProjectTimeReportApprovalRequestService>().LifestyleTransient());
                container.Register(Component.For<IRequestService>().ImplementedBy<ReopenTimeTrackingReportRequestService>().LifestyleTransient());
                container.Register(Component.For<IAbsenceTypeCardIndexDataService>().ImplementedBy<AbsenceTypeCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IContractTypeService>().ImplementedBy<ContractTypeService>().LifestyleTransient());
                container.Register(Component.For<IContractTypeCardIndexDataService>().ImplementedBy<ContractTypeCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ITimeReportImportSourceService>().ImplementedBy<TimeReportImportSourceService>().LifestyleTransient());
                container.Register(Component.For<IEmailAbsenceNotificationService>().ImplementedBy<EmailAbsenceNotificationService>().LifestyleTransient());
                container.Register(Component.For<IRequestService>().ImplementedBy<OvertimeRequestService>().LifestyleTransient());
                container.Register(Component.For<ITimeReportCardIndexDataService>().ImplementedBy<TimeReportCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IOvertimeApprovalDataService>().ImplementedBy<OvertimeApprovalDataService>().LifestyleTransient());
                container.Register(Component.For<IAbsenceService>().ImplementedBy<AbsenceService>().LifestyleTransient());
                container.Register(Component.For<ITimeTrackingDashboardCardIndexDataService>().ImplementedBy<TimeTrackingDashboardCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IHourlyRateCardIndexService>().ImplementedBy<HourlyRateCardIndexService>().LifestyleTransient());
                container.Register(Component.For<IJiraIssueToTimeReportRowMapperCardIndexDataService>().ImplementedBy<JiraIssueToTimeReportRowMapperCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IOvertimeBankService>().ImplementedBy<OvertimeBankService>().LifestyleTransient());
                container.Register(Component.For<IBusinessEventHandler>().ImplementedBy<EmploymentPeriodChangedEventHandler>().LifestyleTransient());
                container.Register(Component.For<IBusinessEventHandler>().ImplementedBy<ProjectTimeReportApprovedEventHandler>().LifestyleTransient());
                container.Register(Component.For<IBusinessEventHandler>().ImplementedBy<TimeReportReopenedBusinessEventHandler>().LifestyleTransient());
                container.Register(Component.For<IFilteredUserPickerCardIndexDataService>().ImplementedBy<FilteredUserPickerCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ICalendarAbsenceService>().ImplementedBy<CalendarAbsenceService>().LifestyleTransient());
                container.Register(Component.For<ITimeReportProjectAttributeCardIndexDataService>().ImplementedBy<TimeReportProjectAttributeCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ITimeReportProjectAttributeValueCardIndexDataService>().ImplementedBy<TimeReportProjectAttributeValueCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ITimeReportControllingSynchronizationService>().ImplementedBy<TimeReportControllingSynchronizationService>().LifestyleTransient());
                container.Register(Component.For<ITimeReportProjectAttributeService>().ImplementedBy<TimeReportProjectAttributeService>().LifestyleTransient());
                container.Register(Component.For<IOvertimeBankCardIndexDataService>().ImplementedBy<OvertimeBankCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IAbsenceScheduleService>().ImplementedBy<AbsenceScheduleService>().LifestyleTransient());
                container.Register(Component.For<IRequestService>().ImplementedBy<TaxDeductibleCostRequestService>().LifestyleTransient());
                container.Register(Component.For<IRequestService>().ImplementedBy<BonusRequestService>().LifestyleTransient());
                container.Register(Component.For<IRequestService>().ImplementedBy<ExpenseRequestService>().LifestyleTransient());
            }

            if (containerType == ContainerType.WebApp)
            {
                container.Register(Component.For<IChoreProvider>().ImplementedBy<OwnTimeReportReminderChoreProvider>().LifestylePerWebRequest());
                container.Register(Component.For<IChoreProvider>().ImplementedBy<MyEmployeesTimeReportReminderChoreProvider>().LifestylePerWebRequest());
            }

            if (containerType == ContainerType.JobScheduler)
            {
                container.Register(Component.For<IChoreProvider>().ImplementedBy<OwnTimeReportReminderChoreProvider>().LifestylePerThread());
                container.Register(Component.For<IChoreProvider>().ImplementedBy<MyEmployeesTimeReportReminderChoreProvider>().LifestylePerThread());
            }
        }
    }
}