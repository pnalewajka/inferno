﻿using System;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.TimeTracking.ChoreProviders
{
    public class OwnTimeReportReminderChoreProvider
        : IChoreProvider
    {
        private readonly ITimeService _timeService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IPrincipalProvider _principalProvider;

        public OwnTimeReportReminderChoreProvider(
            ITimeService timeService,
            ISystemParameterService systemParameterService,
            IPrincipalProvider principalProvider)
        {
            _timeService = timeService;
            _systemParameterService = systemParameterService;
            _principalProvider = principalProvider;
        }

        public IQueryable<ChoreDto> GetActiveChores(IReadOnlyRepositoryFactory repositoryFactory)
        {
            if (!_principalProvider.Current.IsInRole(SecurityRoleType.CanSaveMyTimeReport))
            {
                return null;
            }

            var dayFromLastInMonthToRemind = _systemParameterService.GetParameter<int>(
                ParameterKeys.TimeReportReminderChoreDayFromLastInMonthToRemind);

            var currentDate = _timeService.GetCurrentDate();
            var previousMonthDate = currentDate.AddMonths(-1);

            var currentMonthRemindingDate = currentDate.GetLastDayInMonth().AddDays(dayFromLastInMonthToRemind);
            var previousMonthRemindingDate = previousMonthDate.GetLastDayInMonth().AddDays(dayFromLastInMonthToRemind);

            var mustShowChoreForCurrentMonth = MustShowChoreForDate(currentDate.GetFirstDayInMonth(), currentDate, currentMonthRemindingDate);
            var mustShowChoreForPreviousMonth = MustShowChoreForDate(previousMonthDate.GetFirstDayInMonth(), currentDate, previousMonthRemindingDate);

            var timeReports = repositoryFactory.Create<Employee>().Filtered()
                .Where(e => e.Id == _principalProvider.Current.EmployeeId)
                .SelectMany(e => new[] {
                    new {
                        Visible = mustShowChoreForCurrentMonth.Call(e),
                        Chore = new ChoreDto
                        {
                            Type = ChoreType.OwnTimeReportReminder,
                            DueDate = currentMonthRemindingDate,
                            Parameters = currentDate.Year + "/" + currentDate.Month,
                        }
                    },
                    new {
                        Visible = mustShowChoreForPreviousMonth.Call(e),
                        Chore = new ChoreDto
                        {
                            Type = ChoreType.OwnTimeReportReminder,
                            DueDate = previousMonthRemindingDate,
                            Parameters = previousMonthDate.Year + "/" + previousMonthDate.Month,
                        }
                    }
                })
                .Where(e => e.Visible)
                .Select(e => e.Chore);

            return timeReports;
        }

        private BusinessLogic<Employee, bool> MustShowChoreForDate(DateTime trDate, DateTime currentDate, DateTime remindingDate)
        {
            if (currentDate < remindingDate)
            {
                return new BusinessLogic<Employee, bool>(e => false);
            }

            return new BusinessLogic<Employee, bool>(
                e => e.TimeReports.AsQueryable()
                    .Where(t => t.Year == trDate.Year && t.Month == trDate.Month)
                    .Where(t => t.Status == TimeReportStatus.NotStarted || t.Status == TimeReportStatus.Draft)
                    .Any());
        }
    }
}
