﻿using System;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.TimeTracking.ChoreProviders
{
    public class MyEmployeesTimeReportReminderChoreProvider
        : IChoreProvider
    {
        private readonly ITimeService _timeService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IOrgUnitService _orgUnitService;

        public MyEmployeesTimeReportReminderChoreProvider(
            ITimeService timeService,
            ISystemParameterService systemParameterService,
            IPrincipalProvider principalProvider,
            IOrgUnitService orgUnitService)
        {
            _timeService = timeService;
            _systemParameterService = systemParameterService;
            _principalProvider = principalProvider;
            _orgUnitService = orgUnitService;
        }

        public IQueryable<ChoreDto> GetActiveChores(IReadOnlyRepositoryFactory repositoryFactory)
        {
            if (!_principalProvider.Current.IsInRole(SecurityRoleType.CanViewOthersTimeReport))
            {
                return null;
            }

            var dayFromLastInMonthToRemind = _systemParameterService.GetParameter<int>(
                ParameterKeys.ManagerTimeReportReminderChoreDayFromLastInMonthToRemind);

            var currentEmployeeId = _principalProvider.Current.EmployeeId;
            var managerOrgUnitsIds = _orgUnitService.GetDirectlyManagedOrgUnitIds(currentEmployeeId);
            var managerIdsOfManagerOrgUnitChildren = _orgUnitService.GetManagerIdsOfManagerOrgUnitChildren(currentEmployeeId);
            var isMyEmployee = new BusinessLogic<Employee, bool>(e =>
                EmployeeBusinessLogic.IsEmployeeOf.Call(e, currentEmployeeId, managerOrgUnitsIds)
                || EmployeeBusinessLogic.IsOneOfManagers.Call(e, managerIdsOfManagerOrgUnitChildren));

            var currentDate = _timeService.GetCurrentDate();
            var previousMonthDate = currentDate.AddMonths(-1);

            var currentMonthRemindingDate = currentDate.GetLastDayInMonth().AddDays(dayFromLastInMonthToRemind);
            var previousMonthRemindingDate = previousMonthDate.GetLastDayInMonth().AddDays(dayFromLastInMonthToRemind);

            var mustShowChoreForCurrentMonth = MustShowChoreForDate(currentDate.GetFirstDayInMonth(), currentDate, currentMonthRemindingDate);
            var mustShowChoreForPreviousMonth = MustShowChoreForDate(previousMonthDate.GetFirstDayInMonth(), currentDate, previousMonthRemindingDate);

            var chores = repositoryFactory.Create<Employee>().Filtered()
                .Where(e => e.Id != currentEmployeeId)
                .Where(EmployeeBusinessLogic.IsWorking)
                .Where(e => e.CurrentEmployeeStatus != EmployeeStatus.Inactive)
                .Where(isMyEmployee)
                .GroupBy(e => 0)
                .SelectMany(g => new[] {
                    new {
                        Count = g.AsQueryable().Where(mustShowChoreForCurrentMonth).Count(),
                        Date = currentDate,
                        RemindingDate = currentMonthRemindingDate,
                    },
                    new {
                        Count = g.AsQueryable().Where(mustShowChoreForPreviousMonth).Count(),
                        Date = previousMonthDate,
                        RemindingDate = previousMonthRemindingDate,
                    }
                })
                .Where(e => e.Count > 0)
                .Select(e => new ChoreDto
                {
                    Type = ChoreType.MyEmployeesTimeReportReminder,
                    DueDate = e.RemindingDate,
                    Parameters = e.Count + "," + e.Date.Year + "," + e.Date.Month,
                });

            return chores;
        }

        private BusinessLogic<Employee, bool> MustShowChoreForDate(DateTime trDate, DateTime currentDate, DateTime remindingDate)
        {
            if (currentDate < remindingDate)
            {
                return new BusinessLogic<Employee, bool>(e => false);
            }

            return new BusinessLogic<Employee, bool>(
                e => e.TimeReports.AsQueryable()
                    .Where(t => t.Year == trDate.Year && t.Month == trDate.Month)
                    .Where(t => t.Status == TimeReportStatus.NotStarted || t.Status == TimeReportStatus.Draft)
                    .Any());
        }
    }
}
