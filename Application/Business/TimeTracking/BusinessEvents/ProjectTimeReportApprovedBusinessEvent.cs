﻿using System.Runtime.Serialization;
using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.TimeTracking.BusinessEvents
{
    [DataContract]
    public class ProjectTimeReportApprovedBusinessEvent : BusinessEvent
    {
        [DataMember]
        public long ProjectId { get; set; }

        [DataMember]
        public long Month { get; set; }

        [DataMember]
        public long Year { get; set; }

        [DataMember]
        public long EmployeeId { get; set; }
    }
}
