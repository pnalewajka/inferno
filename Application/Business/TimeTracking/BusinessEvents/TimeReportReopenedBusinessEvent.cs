﻿using System.Runtime.Serialization;
using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.TimeTracking.BusinessEvents
{
    [DataContract]
    public class TimeReportReopenedBusinessEvent : BusinessEvent
    {
        public TimeReportReopenedBusinessEvent(long requestId)
        {
            RequestId = requestId;
        }

        [DataMember]
        public long RequestId { get; set; }
    }
}
