﻿using System;

namespace Smt.Atomic.Business.TimeTracking.Workflows.Dto
{
    public class AbsenceRequestNotificationDto
    {
        public string ReceiverName { get; set; }

        public string EmployeeFullName { get; set; }

        public string Description { get; set; }
    }
}
