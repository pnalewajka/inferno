﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using System;
using System.Linq;

namespace Smt.Atomic.Business.TimeTracking.Workflows.Dto
{
    public class MyTimeReportRowDtoToTimeReportTaskApprovalDtoMapping : ClassMapping<MyTimeReportRowDto, TimeReportTaskApprovalDto>
    {
        public MyTimeReportRowDtoToTimeReportTaskApprovalDtoMapping(ISystemParameterService systemParameterService)
        {
            Mapping = r => new TimeReportTaskApprovalDto
            {
                TaskName = r.TaskName,
                From = GetFirstReportedDay(r),
                To = GetLastReportedDay(r),
                Hours = r.Days.Sum(d => d.Hours),
                OvertimeVariant = r.OvertimeVariant,
                Attributes = r.Attributes.SelectMany(
                    a => a.PossibleValues.Where(
                        v => r.SelectedAttributesValueIds.Contains(v.Id)).Select(v => v.Name))
                    .ToArray(),
                IsBillable = !r.Attributes.Any(a => a.PossibleValues.Where(
                    v => v.Features.HasFlag(AttributeValueFeature.NonBillable))
                    .Select(v => v.Id).Any(i => r.SelectedAttributesValueIds.Contains(i))),
            };
        }

        private DateTime GetFirstReportedDay(MyTimeReportRowDto rowDto)
        {
            var dailyEntry = rowDto.Days.Where(d => d.Hours != 0).OrderBy(d => d.Day).First();

            return dailyEntry.Day;
        }

        private DateTime GetLastReportedDay(MyTimeReportRowDto rowDto)
        {
            var dailyEntry = rowDto.Days.Where(d => d.Hours != 0).OrderByDescending(d => d.Day).First();

            return dailyEntry.Day;
        }
    }
}
