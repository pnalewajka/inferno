﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.TimeTracking.BusinessEvents;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Business.TimeTracking.Resources;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.Business.Workflows.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Workflows.Services
{
    public class ProjectTimeReportApprovalRequestService : RequestServiceBase<ProjectTimeReportApprovalRequest, ProjectTimeReportApprovalRequestDto>
    {
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService;
        private readonly IApprovingPersonService _approvingPersonService;
        private readonly IProjectService _projectService;
        private readonly IEmployeeService _employeeService;
        private readonly IBusinessEventPublisher _businessEventPublisher;
        private readonly ITimeReportService _timeReportService;

        public ProjectTimeReportApprovalRequestService(
            IWorkflowNotificationService workflowNotificationService,
            IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
            IApprovingPersonService approvingPersonService,
            IPrincipalProvider principalProvider,
            IOvertimeBankService overtimeBankService,
            IActionSetService actionSetService,
            ITimeReportService timeReportService,
            IClassMappingFactory classMappingFactory,
            IProjectService projectService,
            IEmployeeService employeeService,
            IBusinessEventPublisher businessEventPublisher,
            IRequestWorkflowService requestWorkflowService,
            ISystemParameterService systemParameterService)
            : base(actionSetService, principalProvider, classMappingFactory, workflowNotificationService, requestWorkflowService, systemParameterService)
        {
            _unitOfWorkService = unitOfWorkService;
            _approvingPersonService = approvingPersonService;
            _projectService = projectService;
            _employeeService = employeeService;
            _businessEventPublisher = businessEventPublisher;
            _timeReportService = timeReportService;
        }

        public override RequestType RequestType => RequestType.ProjectTimeReportApprovalRequest;

        public override string RequestName => WorkflowResources.ProjectTimeReportApprovalRequestName;

        public override RequestTypeGroup RequestTypeGroup => RequestTypeGroup.TimeTracking;
        

        public override SecurityRoleType? RequiredRoleType => null;

        public override bool CanBeStartedManually()
        {
            return false;
        }

        public override bool CanBeDeleted(RequestDto requestDto, long byUserId)
        {
            return false;
        }

        public override AlertDto GetRequestSubmittedAlert(RequestDto request, ICollection<string> approversNames)
        {
            var approvalRequest = (ProjectTimeReportApprovalRequestDto) request.RequestObject;
            var projectName = _projectService.GetProjectById(approvalRequest.ProjectId).ClientProjectName;

            return new AlertDto
            {
                Type = AlertType.Success,
                Message = string.Format(WorkflowResources.ProjectRequestSubmitted, projectName,
                    string.Join(", ", approversNames)),
                IsDismissable = true
            };
        }

        public override string GetRequestDescription(RequestDto request, ProjectTimeReportApprovalRequestDto data)
        {
            var projectName = _projectService.GetProjectOrDefaultById(data.ProjectId)?.ClientProjectName;
            var employeeFullName = _employeeService.GetEmployeeOrDefaultById(data.EmployeeId)?.FullName;
            var tasksWithLoggedHours = data.Tasks.Where(t => t.Hours > 0);
            var numberOfHours = (tasksWithLoggedHours != null && tasksWithLoggedHours.Any()) ? tasksWithLoggedHours.Sum(t => t.Hours) : 0;
            var monthOfRequest = data.From.ToString("MMM yyyy");

            return string.Format(ProjectTimeReportApprovalRequestResources.RequestDescriptionFormat,
                projectName, employeeFullName, numberOfHours, monthOfRequest);
        }

        public override IEnumerable<ApproverGroupDto> GetApproverGroups(RequestDto request, ProjectTimeReportApprovalRequestDto projectTimeReportApprovalRequest)
        {
            var projectApprovers =
                _approvingPersonService.GetProjectApprovers(projectTimeReportApprovalRequest.EmployeeId,
                    projectTimeReportApprovalRequest.ProjectId);

            if (projectApprovers.Any())
            {
                yield return new ApproverGroupDto(ApprovalGroupMode.Or, projectApprovers);
            }
        }

        public override IEnumerable<AlertDto> Validate(RequestDto request, ProjectTimeReportApprovalRequestDto projectTimeReportApprovalRequest)
        {
            var approverGroups = GetApproverGroups(request, projectTimeReportApprovalRequest).ToList();

            if (!approverGroups.Any() || !approverGroups.Any(g => g.Approvers.Any()))
            {
                yield return new AlertDto
                {
                    IsDismissable = false,
                    Type = AlertType.Error,
                    Message = string.Format(ProjectTimeReportApprovalRequestResources.CannotCreateBecauseProjectManagerWasNotDefined, request.Description)
                };
            }
        }

        public override void OnRejected(Request request, RequestDto requestDto)
        {
            base.OnRejected(request, requestDto);
            SetTimeReportStatus((ProjectTimeReportApprovalRequestDto)requestDto.RequestObject, TimeReportStatus.Rejected, requestDto.Id);
        }

        public override void OnSubmited(Request request, RequestDto requestDto)
        {
            base.OnSubmited(request, requestDto);
            SetTimeReportStatus((ProjectTimeReportApprovalRequestDto)requestDto.RequestObject, TimeReportStatus.Submitted, requestDto.Id);
        }

        public override void Execute(RequestDto request, ProjectTimeReportApprovalRequestDto projectTimeReportApprovalRequest)
        {
           SetTimeReportStatus(projectTimeReportApprovalRequest, TimeReportStatus.Accepted, request.Id);
        }

        private TimeReport GetTimeReport(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, long employeeId, int year,
            byte month)
        {
            return unitOfWork
                .Repositories
                .TimeReports
                .SingleOrDefault(
                    r =>
                        r.Year == year && r.Month == month &&
                        r.EmployeeId == employeeId);
        }

        private void AssignRequestToTimeReport(long employeeId, long projectId, int year, byte month, long requestId)
        {
            if (employeeId <= 0)
            {
                throw new BusinessException(TimeReportServiceResources.TimeReportNotAvailableNoEmployee);
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var timeReport = GetTimeReport(unitOfWork, employeeId, year, month);

                var timeReportRows =
                    unitOfWork.Repositories.TimeReportRows.Where(trr => trr.TimeReportId == timeReport.Id && trr.ProjectId == projectId);

                foreach (var timeReportRow in timeReportRows)
                {
                    timeReportRow.RequestId = requestId;
                }

                unitOfWork.Commit();
            }
        }

        private void SetTimeReportStatus(ProjectTimeReportApprovalRequestDto request, TimeReportStatus status, long requestId)
        {
            AssignRequestToTimeReport(request.EmployeeId, request.ProjectId, request.From.Year, (byte)request.From.Month, requestId);
            _timeReportService.UpdateTimeReportRowsStatus(requestId, status);
        }

        public override IQueryable<Request> ConfigureIncludes(IQueryable<Request> repository)
        {
            return repository.Include(r => r.ProjectTimeReportApprovalRequest);
        }

        public override void OnAllApproved(Request request, RequestDto requestDto)
        {
            base.OnAllApproved(request, requestDto);

            var requestDtoRequestObject = (ProjectTimeReportApprovalRequestDto)requestDto.RequestObject;

            var projectTimeReportApprovedBusinessEvent = new ProjectTimeReportApprovedBusinessEvent
            {
                ProjectId = requestDtoRequestObject.ProjectId,
                EmployeeId = requestDtoRequestObject.EmployeeId,
                Month = requestDtoRequestObject.From.Month,
                Year = requestDtoRequestObject.From.Year
            };

            _businessEventPublisher.PublishBusinessEvent(projectTimeReportApprovedBusinessEvent);
        }
    }
}
