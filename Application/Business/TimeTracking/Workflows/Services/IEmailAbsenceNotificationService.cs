﻿using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.TimeTracking.Workflows.Dto;

namespace Smt.Atomic.Business.TimeTracking.Workflows.Services
{
    public interface IEmailAbsenceNotificationService
    {
        void NotifyAboutAbsence(AbsenceRequestNotificationDto request, EmailRecipientDto receiver);
    }
}