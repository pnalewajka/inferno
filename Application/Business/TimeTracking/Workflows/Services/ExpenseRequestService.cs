﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Compensation.BusinessEvents;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.Business.Workflows.Services;
using Smt.Atomic.CrossCutting.Business.Helpers;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.TimeTracking.Workflows.Services
{
    public class ExpenseRequestService : RequestServiceBase<ExpenseRequest, ExpenseRequestDto>
    {
        private readonly IApprovingPersonService _approvingPersonService;
        private readonly IBusinessEventPublisher _businessEventPublisher;
        private readonly IProjectService _projectService;
        private readonly IEmployeeService _employeeService;

        public ExpenseRequestService(IActionSetService actionSetService,
            IPrincipalProvider principalProvider,
            IClassMappingFactory classMappingFactory,
            IWorkflowNotificationService workflowNotificationService,
            IApprovingPersonService approvingPersonService,
            IBusinessEventPublisher businessEventPublisher,
            IProjectService projectService,
            IEmployeeService employeeService,
            IRequestWorkflowService requestWorkflowService,
            ISystemParameterService systemParameterService)
            : base(actionSetService,
                  principalProvider,
                  classMappingFactory,
                  workflowNotificationService,
                  requestWorkflowService,
                  systemParameterService)
        {
            _approvingPersonService = approvingPersonService;
            _businessEventPublisher = businessEventPublisher;
            _projectService = projectService;
            _employeeService = employeeService;
        }

        public override RequestType RequestType => RequestType.ExpenseRequest;

        public override RequestTypeGroup RequestTypeGroup => RequestTypeGroup.Other;

        public override string RequestName => WorkflowResources.ExpenseRequestName;

        public override SecurityRoleType? RequiredRoleType => SecurityRoleType.CanRequestExpense;

        public override ExpenseRequestDto GetDraft(IDictionary<string, object> parameters) => new ExpenseRequestDto
        {
            EmployeeId = PrincipalProvider.Current.EmployeeId,
        };

        public override IQueryable<Request> ConfigureIncludes(IQueryable<Request> repository)
        {
            return repository.Include(r => r.ExpenseRequest);
        }

        public override void Execute(RequestDto request, ExpenseRequestDto expenseRequestDto)
        {
            _businessEventPublisher.PublishBusinessEvent(new PaymentApprovedEvent(expenseRequestDto));
            _businessEventPublisher.PublishBusinessEvent(new CompensationChangedEvent(
                expenseRequestDto.EmployeeId, expenseRequestDto.Date.Year, (byte)expenseRequestDto.Date.Month));
        }

        public override string ViewModelTypeName => IdentifierHelper.GetTypeByIdentifier("Models.ExpenseRequestViewModel").FullName;

        public override IEnumerable<ApproverGroupDto> GetApproverGroups(RequestDto request, ExpenseRequestDto expenseRequestDto)
        {
            if (expenseRequestDto.ProjectId != BusinessLogicHelper.NonExistingId)
            {
                var approvers = _approvingPersonService.GetProjectApprovers(PrincipalProvider.Current.EmployeeId, expenseRequestDto.ProjectId).ToList();

                if (approvers.Any())
                {
                    yield return new ApproverGroupDto(ApprovalGroupMode.Or, approvers);
                }
            }
        }

        public override string GetRequestDescription(RequestDto request, ExpenseRequestDto expenseRequest)
        {
            var projectName = _projectService.GetProjectOrDefaultById(expenseRequest.ProjectId)?.ClientProjectName;
            var affectedUserFullName = _employeeService.GetEmployeeOrDefaultById(expenseRequest.EmployeeId)?.FullName;

            return string.Format(WorkflowResources.ExpenseRequestDescriptionFormat,
                projectName, affectedUserFullName, expenseRequest.Amount, expenseRequest.CurrencyDisplayName);
        }
    }
}