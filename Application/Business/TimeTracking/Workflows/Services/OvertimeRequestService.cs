﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.TimeTracking.Resources;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.Business.Workflows.Services;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Workflows.Services
{
    internal class OvertimeRequestService : RequestServiceBase<OvertimeRequest, OvertimeRequestDto>
    {
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService;
        private readonly IApprovingPersonService _approvingPersonService;
        private readonly ITimeService _timeService;
        private readonly IProjectService _projectService;
        private readonly IEmployeeService _employeeService;

        public OvertimeRequestService(
            IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
            IApprovingPersonService approvingPersonService, 
            IPrincipalProvider principalProvider,
            IWorkflowNotificationService workflowNotificationService,
            ITimeService timeService,
            IProjectService projectService,
            IEmployeeService employeeService,
            IActionSetService actionSetService,
            IClassMappingFactory classMappingFactory,
            IRequestWorkflowService requestWorkflowService,
            ISystemParameterService systemParameterService)
            : base(actionSetService, principalProvider, classMappingFactory, workflowNotificationService, requestWorkflowService, systemParameterService)
        {
            _unitOfWorkService = unitOfWorkService;
            _approvingPersonService = approvingPersonService;
            _timeService = timeService;
            _projectService = projectService;
            _employeeService = employeeService;
        }

        public override RequestType RequestType => RequestType.OvertimeRequest;

        public override string RequestName => WorkflowResources.OvertimeRequestName;

        public override RequestTypeGroup RequestTypeGroup => RequestTypeGroup.TimeTracking;
        

        public override SecurityRoleType? RequiredRoleType => SecurityRoleType.CanRequestOvertime;

        public override string GetRequestDescription(RequestDto request, OvertimeRequestDto data)
        {
            return string.Format(OvertimeRequestResource.RequestDescriptionFormat,
                data.From, data.To, data.HourLimit);
        }

        public override IEnumerable<ApproverGroupDto> GetApproverGroups(RequestDto request, OvertimeRequestDto overtimeRequest)
        {
            if (overtimeRequest.ProjectId == 0)
            {
                yield break;
            }

            var employeeId = _employeeService.GetEmployeeOrDefaultByUserId(request.RequestingUserId).Id;

            var projectApprovers = _approvingPersonService.GetProjectApprovers(employeeId, overtimeRequest.ProjectId);

            if (projectApprovers.Any())
            {
                yield return new ApproverGroupDto(ApprovalGroupMode.Or, projectApprovers);
            }
        }

        public override void Execute(RequestDto request, OvertimeRequestDto overtimeRequest)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                AddOvertimeApproval(unitOfWork, request, overtimeRequest);
                unitOfWork.Commit();
            }
        }

        public override OvertimeRequestDto GetDraft(IDictionary<string, object> parameters)
        {
            ValidateCurrentPrincipalHasEmployee();

            var currentDate = _timeService.GetCurrentDate();

            return new OvertimeRequestDto
            {
                From = currentDate.GetFirstDayInMonth(),
                To = currentDate.GetLastDayInMonth(),
            };
        }

        public override AlertDto GetRequestSubmittedAlert(RequestDto request, ICollection<string> approvalNames)
        {
            var overtimeRequest = (OvertimeRequestDto)request.RequestObject;
            var projectName = _projectService.GetProjectById(overtimeRequest.ProjectId).ClientProjectName;

            return new AlertDto
            {
                Type = AlertType.Success,
                Message = string.Format(WorkflowResources.OvertimeRequestSubmitted, projectName,
                    string.Join(", ", approvalNames)),
                IsDismissable = true
            };
        }

        public override IEnumerable<AlertDto> Validate(RequestDto request, OvertimeRequestDto overtimeRequest)
        {
            ValidateCurrentPrincipalHasEmployee();

            var approverGroups = GetApproverGroups(request, overtimeRequest).ToList();

            if (!approverGroups.Any() || !approverGroups.Any(g => g.Approvers.Any()))
            {
                yield return
                    new AlertDto
                    {
                        IsDismissable = false,
                        Type = AlertType.Error,
                        Message = OvertimeRequestResource.NoApproverForProject_ErrorMessage
                    };
            }

            if (overtimeRequest.HourLimit <= 0)
            {
                yield return
                    new AlertDto
                    {
                        IsDismissable = false,
                        Type = AlertType.Error,
                        Message = OvertimeRequestResource.TotalHoursGreaterThanZero_ErrorMessage
                    };
            }

            if (overtimeRequest.From == DateTime.MinValue || overtimeRequest.To == DateTime.MinValue)
            {
                yield return
                    new AlertDto
                    {
                        IsDismissable = false,
                        Type = AlertType.Error,
                        Message = OvertimeRequestResource.DatesMustBeSet_ErrorMessage
                    };
            }

            if (overtimeRequest.From > overtimeRequest.To)
            {
                yield return
                    new AlertDto
                    {
                        IsDismissable = false,
                        Type = AlertType.Error,
                        Message = OvertimeRequestResource.DateFromGreaterThanDateTo_ErrorMessage
                    };
            }
        }

        private void AddOvertimeApproval(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, RequestDto request, OvertimeRequestDto overtimeRequest)
        {
            var employeeId = _employeeService.GetEmployeeOrDefaultByUserId(request.RequestingUserId).Id;

            var overtimeApproval = new OvertimeApproval
            {
                EmployeeId = employeeId,
                ProjectId = overtimeRequest.ProjectId,
                DateFrom = overtimeRequest.From,
                DateTo = overtimeRequest.To,
                HourLimit = overtimeRequest.HourLimit,
                RequestId = request.Id
            };

            unitOfWork.Repositories.OvertimeApprovals.Add(overtimeApproval);
        }

        public override IQueryable<Request> ConfigureIncludes(IQueryable<Request> repository)
        {
            return repository.Include(r => r.OvertimeRequest);
        }
    }
}