﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Compensation.BusinessEvents;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.Business.Workflows.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Business.Helpers;
using Smt.Atomic.CrossCutting.Business.Resources;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Compensation;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Workflows.Services
{
    public class BonusRequestService : RequestServiceBase<BonusRequest, BonusRequestDto>
    {
        private readonly IUnitOfWorkService<ICompensationDbScope> _unitOfWorkService;
        private readonly IApprovingPersonService _approvingPersonService;
        private readonly IEmployeeService _employeeService;
        private readonly ITimeService _timeService;
        private readonly IProjectService _projectService;
        private readonly IBusinessEventPublisher _businessEventPublisher;
        private readonly IUserService _userService;
        private readonly IClassMapping<EmployeeDto, Employee> _employeeDtoToEmployeeClassMapping;

        public BonusRequestService(
            IActionSetService actionSetService,
            IPrincipalProvider principalProvider,
            IClassMappingFactory classMappingFactory,
            IWorkflowNotificationService workflowNotificationService,
            IEmployeeService employeeService,
            IApprovingPersonService approvingPersonService,
            IUnitOfWorkService<ICompensationDbScope> unitOfWorkService,
            ITimeService timeService,
            IProjectService projectService,
            IBusinessEventPublisher businessEventPublisher,
            ISystemParameterService systemParameterService,
            IRequestWorkflowService requestWorkflowService,
            IUserService userService,
            IClassMapping<EmployeeDto, Employee> employeeDtoToEmployeeClassMapping)
            : base(actionSetService,
                  principalProvider,
                  classMappingFactory,
                  workflowNotificationService,
                  requestWorkflowService,
                  systemParameterService)
        {
            _unitOfWorkService = unitOfWorkService;
            _approvingPersonService = approvingPersonService;
            _employeeService = employeeService;
            _timeService = timeService;
            _projectService = projectService;
            _businessEventPublisher = businessEventPublisher;
            _userService = userService;
            _employeeDtoToEmployeeClassMapping = employeeDtoToEmployeeClassMapping;
        }

        public override RequestType RequestType => RequestType.BonusRequest;

        public override RequestTypeGroup RequestTypeGroup => RequestTypeGroup.Manager;

        public override string RequestName => WorkflowResources.BonusRequestName;

        public override SecurityRoleType? RequiredRoleType => SecurityRoleType.CanRequestBonus;

        public override string ViewModelTypeName => IdentifierHelper.GetTypeByIdentifier("Models.BonusRequestViewModel").FullName;

        public override IQueryable<Request> ConfigureIncludes(IQueryable<Request> repository)
        {
            return repository.Include(r => r.BonusRequest);
        }

        public override void Execute(RequestDto request, BonusRequestDto bonusRequestDto)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var bonus = new Bonus
                {
                    StartDate = bonusRequestDto.StartDate,
                    EndDate = bonusRequestDto.EndDate,
                    EmployeeId = bonusRequestDto.EmployeeId,
                    ProjectId = bonusRequestDto.ProjectId,
                    BonusAmount = bonusRequestDto.BonusAmount,
                    CurrencyId = bonusRequestDto.CurrencyId,
                    Justification = bonusRequestDto.Justification,
                    Type = bonusRequestDto.Type,
                    Reinvoice = bonusRequestDto.Reinvoice
                };

                var bonusRequest = unitOfWork.Repositories.BonusRequests.FirstOrDefault(b => b.Request.Id == request.Id);

                if (bonusRequest != null)
                {
                    bonusRequest.Bonus = bonus;
                }

                unitOfWork.Commit();
            }

            var endDate = bonusRequestDto.EndDate ?? _timeService.GetCurrentDate();

            foreach (var date in DateHelper.AllMonthsInRange(bonusRequestDto.StartDate, endDate))
            {
                _businessEventPublisher.PublishBusinessEvent(
                    new CompensationChangedEvent(bonusRequestDto.EmployeeId, date.Year, (byte)date.Month));
            }
        }

        public override IEnumerable<ApproverGroupDto> GetApproverGroups(RequestDto request, BonusRequestDto bonusRequestDto)
        {
            var employeeId = _employeeService.GetEmployeeOrDefaultByUserId(request.RequestingUserId).Id;

            if (bonusRequestDto.ProjectId != BusinessLogicHelper.NonExistingId)
            {
                var approvers = _approvingPersonService.GetProjectApprovers(employeeId, bonusRequestDto.ProjectId).ToList();
                var projectSupervisorApprover = _approvingPersonService.GetProjectSupervisorApprover(employeeId, bonusRequestDto.ProjectId);

                if (projectSupervisorApprover != default(long))
                {
                    approvers.Add(projectSupervisorApprover);
                }

                if (approvers.Any())
                {
                    yield return new ApproverGroupDto(ApprovalGroupMode.And, approvers);
                }
            }
        }

        public override BoolResult OnDeleting(Request request, RequestDto requestDto, IUnitOfWork<IWorkflowsDbScope> unitOfWork)
        {
            unitOfWork.Repositories.Bonuses.DeleteEach(b => b.Id == request.BonusRequest.BonusId);

            return base.OnDeleting(request, requestDto, unitOfWork);
        }

        public override bool CanBeDeleted(RequestDto requestDto, long byUserId)
        {
            var bonusRequestDto = (BonusRequestDto)requestDto.RequestObject;
            var userRoles = _userService.GetUserRolesById(byUserId);

            if (userRoles.Contains(SecurityRoleType.CanDeleteApprovedBonusRequest))
            {
                return CanDeleteCalculatedBonus(requestDto, byUserId);
            }

            return base.CanBeDeleted(requestDto, byUserId);
        }

        public override string GetRequestDescription(RequestDto request, BonusRequestDto bonusRequest)
        {
            return string.Format(WorkflowResources.RequestDescriptionFormat, bonusRequest.EmployeeFullName);
        }

        public override IEnumerable<AlertDto> Validate(RequestDto request, BonusRequestDto requestDto)
        {
            var project = _projectService.GetProjectById(requestDto.ProjectId);

            switch (requestDto.Type)
            {
                case BonusType.Bonus:
                    if (!project.AllowedBonusTypes.HasFlag(BonusTypeSet.Bonus))
                    {
                        yield return AlertDto.CreateError(string.Format(WorkflowResources.ProjectAllowedBonusTypesError, EnumResources.Bonus));
                    }
                    break;
                case BonusType.Oncall:
                    if (!project.AllowedBonusTypes.HasFlag(BonusTypeSet.Oncall))
                    {
                        yield return AlertDto.CreateError(string.Format(WorkflowResources.ProjectAllowedBonusTypesError, EnumResources.Oncall));
                    }
                    break;
                case BonusType.ShortTerm:
                    if (!project.AllowedBonusTypes.HasFlag(BonusTypeSet.ShortTerm))
                    {
                        yield return AlertDto.CreateError(string.Format(WorkflowResources.ProjectAllowedBonusTypesError, EnumResources.ShortTerm));
                    }
                    break;
                case BonusType.Reallocation:
                    if (!project.AllowedBonusTypes.HasFlag(BonusTypeSet.Reallocation))
                    {
                        yield return AlertDto.CreateError(string.Format(WorkflowResources.ProjectAllowedBonusTypesError, EnumResources.Reallocation));
                    }
                    break;
                case BonusType.Recommendation:
                    if (!project.AllowedBonusTypes.HasFlag(BonusTypeSet.Recommendation))
                    {
                        yield return AlertDto.CreateError(string.Format(WorkflowResources.ProjectAllowedBonusTypesError, EnumResources.Recommendation));
                    }
                    break;
                case BonusType.Benefits:
                    if (!project.AllowedBonusTypes.HasFlag(BonusTypeSet.Benefits))
                    {
                        yield return AlertDto.CreateError(string.Format(WorkflowResources.ProjectAllowedBonusTypesError, EnumResources.Benefits));
                    }
                    break;
                case BonusType.Regular:
                    if (!project.AllowedBonusTypes.HasFlag(BonusTypeSet.Regular))
                    {
                        yield return AlertDto.CreateError(string.Format(WorkflowResources.ProjectAllowedBonusTypesError, EnumResources.Regular));
                    }
                    break;
                case BonusType.Holiday:
                    if (!project.AllowedBonusTypes.HasFlag(BonusTypeSet.Holiday))
                    {
                        yield return AlertDto.CreateError(string.Format(WorkflowResources.ProjectAllowedBonusTypesError, EnumResources.Holiday));
                    }
                    break;
                default:
                    break;
            }
        }

        private bool CanDeleteCalculatedBonus(RequestDto requestDto, long byUserId)
        {
            var employeeDto = _employeeService.GetEmployeeOrDefaultByUserId(byUserId);
            var employee = _employeeDtoToEmployeeClassMapping.CreateFromSource(employeeDto);

            var canNotDeleteCalculatedBonus = requestDto.RequestType == RequestType.BonusRequest
                && requestDto.Status == RequestStatus.Completed
                && (employee?.InvoiceCalculations?.Any(i => i.Month == requestDto.ModifiedOn.Month
                    && i.Year == requestDto.ModifiedOn.Year) ?? false);

            return !canNotDeleteCalculatedBonus;
        }
    }
}