﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Security;
using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Business.TimeTracking.Resources;
using Smt.Atomic.Business.TimeTracking.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.Business.Workflows.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Exchange.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using AbsenceCalendarEntry = Smt.Atomic.Data.Entities.Modules.TimeTracking.AbsenceCalendarEntry;
using OvertimeBank = Smt.Atomic.Data.Entities.Modules.TimeTracking.OvertimeBank;
using VacationBalance = Smt.Atomic.Data.Entities.Modules.TimeTracking.VacationBalance;

namespace Smt.Atomic.Business.TimeTracking.Workflows.Services
{
    public class AbsenceRequestService : RequestServiceBase<AbsenceRequest, AbsenceRequestDto>, IAbsenceRequestService
    {
        private readonly ITimeService _timeService;
        private readonly IVacationBalanceService _vacationBalanceService;
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService;
        private readonly IEmailAbsenceNotificationService _absenceNotificationService;
        private readonly IEmploymentPeriodService _employmentPeriodService;
        private readonly IAbsenceService _absenceService;
        private readonly IOvertimeBankService _overtimeBankService;
        private readonly IAbsenceTypeCardIndexDataService _absenceTypeCardIndexDataService;
        private readonly IEmployeeService _employeeService;
        private readonly IApprovingPersonService _approvingPersonService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly ICalendarOfficeService _calendarOfficeService;
        private readonly IUserService _userService;
        private readonly IUserBusinessLogic _userBusinessLogic;
        private readonly ITimeReportService _timeReportService;

        public AbsenceRequestService(
            IEmailAbsenceNotificationService absenceNotificationService,
            IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
            IWorkflowNotificationService workflowNotificationService,
            IVacationBalanceService vacationBalanceService,
            IPrincipalProvider principalProvider,
            ITimeService timeService,
            IEmploymentPeriodService employmentPeriodService,
            IAbsenceTypeCardIndexDataService absenceTypeCardIndexDataService,
            IAbsenceService absenceService,
            IOvertimeBankService overtimeBankService,
            IEmployeeService employeeService,
            IApprovingPersonService approvingPersonService,
            ISystemParameterService systemParameterService,
            ICalendarOfficeService calendarOfficeService,
            IUserService userService,
            IUserBusinessLogic userBusinessLogic,
            IActionSetService actionSetService,
            IClassMappingFactory classMappingFactory,
            ITimeReportService timeReportService,
            IRequestWorkflowService requestWorkflowService)
            : base(actionSetService,
                  principalProvider,
                  classMappingFactory,
                  workflowNotificationService,
                  requestWorkflowService,
                  systemParameterService)
        {
            _timeService = timeService;
            _employmentPeriodService = employmentPeriodService;
            _absenceTypeCardIndexDataService = absenceTypeCardIndexDataService;
            _absenceService = absenceService;
            _overtimeBankService = overtimeBankService;
            _employeeService = employeeService;
            _approvingPersonService = approvingPersonService;
            _systemParameterService = systemParameterService;
            _calendarOfficeService = calendarOfficeService;
            _userService = userService;
            _unitOfWorkService = unitOfWorkService;
            _vacationBalanceService = vacationBalanceService;
            _absenceNotificationService = absenceNotificationService;
            _userBusinessLogic = userBusinessLogic;
            _timeReportService = timeReportService;
        }

        public override string RequestName => WorkflowResources.AbsenceRequestName;

        public override RequestType RequestType => RequestType.AbsenceRequest;

        public override RequestTypeGroup RequestTypeGroup => RequestTypeGroup.TimeTracking;

        public override SecurityRoleType? RequiredRoleType => SecurityRoleType.CanRequestAbsence;

        public override IEnumerable<AlertDto> Validate(RequestDto request, AbsenceRequestDto absenceRequest)
        {
            // Security validations

            if (!ValidateAffectedUser(absenceRequest))
            {
                throw new SecurityException("Affected user is invalid");
            }

            // Business validations

            var affectedEmployee = _employeeService.GetEmployeeOrDefaultByUserId(absenceRequest.AffectedUserId);
            var allowedNumberDay = _systemParameterService.GetParameter<int>(ParameterKeys.BackwardDaysAbsenceLimit);
            var currentDate = _timeService.GetCurrentDate();
            var permissibleDate = currentDate.AddDays(-allowedNumberDay);

            if (affectedEmployee == null)
            {
                yield return AlertDto.CreateError(AbsenceRequestResources.NotAnEmployee_ErrorMessage);
            }

            if (absenceRequest.To.Date < absenceRequest.From.Date)
            {
                yield return AlertDto.CreateError(AbsenceRequestResources.FromDateAfterToDate_ErrorMessage);
            }

            if (affectedEmployee != null && !_employmentPeriodService.IsEmployeeHiredInCompletePeriod(affectedEmployee.Id, absenceRequest.From, absenceRequest.To))
            {
                yield return AlertDto.CreateError(AbsenceRequestResources.EmployeeNotHiredInSpecifiedPeriod);
            }

            if (absenceRequest.From < permissibleDate)
            {
                yield return AlertDto.CreateError(AbsenceRequestResources.EffectiveOnInPast_ErrorMessage);
            }
            else if (absenceRequest.From < currentDate && string.IsNullOrEmpty(absenceRequest.Comment))
            {
                yield return AlertDto.CreateError(AbsenceRequestResources.CommentRequired_ErrorMessage);
            }

            if (_absenceTypeCardIndexDataService.AllowedAbsenceTypes(absenceRequest.From, absenceRequest.To, affectedEmployee.UserId).All(at => at != absenceRequest.AbsenceTypeId))
            {
                yield return AlertDto.CreateError(AbsenceRequestResources.AbsenceType_ErrorMessage);
            }

            if (ContainsLockedTimeReport(affectedEmployee.Id, absenceRequest.From, absenceRequest.To))
            {
                yield return AlertDto.CreateError(AbsenceRequestResources.ExistLockedTimeReports_ErrorMessage);
            }

            var absenceHours = GetAbsenceHours(affectedEmployee.Id, absenceRequest.From.StartOfDay(), absenceRequest.To.EndOfDay());

            if (absenceHours <= 0 || absenceRequest.Hours <= 0)
            {
                yield return AlertDto.CreateError(string.Format(AbsenceRequestResources.ErrorAbsenceNonValidHours, absenceHours));
                yield break;
            }

            if (absenceRequest.Hours <= 0)
            {
                yield return AlertDto.CreateError(string.Format(AbsenceRequestResources.ErrorAbsenceNonValidHours, absenceRequest.Hours));
                yield break;
            }

            if (absenceRequest.SubstituteUserId.HasValue)
            {
                using (var unitOfWork = _unitOfWorkService.Create())
                {
                    var user = unitOfWork.Repositories.Users.GetById(absenceRequest.SubstituteUserId.Value);

                    if (!user.IsActive || user.Employees.Any(e => e.CurrentEmployeeStatus == EmployeeStatus.Terminated))
                    {
                        yield return AlertDto.CreateError(WorkflowResources.InvalidSubstituteUser);
                    }
                }
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var absenceType = unitOfWork.Repositories.AbsenceTypes.GetById(absenceRequest.AbsenceTypeId);

                if (!absenceType.AllowHourlyReporting && absenceHours != absenceRequest.Hours)
                {
                    yield return AlertDto.CreateError(AbsenceRequestResources.ReportedHoursSet_ErrorMessage);
                    yield break;
                }

                if (absenceType.AllowHourlyReporting && absenceRequest.From.StartOfDay() != absenceRequest.To.StartOfDay() &&
                    absenceHours != absenceRequest.Hours)
                {
                    yield return AlertDto.CreateError(AbsenceRequestResources.ReportedHoursSetForFewDays_ErrorMessage);
                }

                if (_absenceService.IsHourlyReportingAllowed(absenceType, absenceRequest.From, absenceRequest.To) && absenceRequest.Hours > absenceHours)
                {
                    yield return AlertDto.CreateError(AbsenceRequestResources.ReportedHoursExceeded_ErrorMessage);
                }

                if (absenceType.IsVacation)
                {
                    var hourBalanceForEmployee = _absenceService.GetAvailableVacationHours(unitOfWork, affectedEmployee.Id, absenceRequest.From);

                    if (hourBalanceForEmployee - absenceRequest.Hours < 0)
                    {
                        yield return AlertDto.CreateError(AbsenceRequestResources.EnoughVacationDays_ErrorMessage);
                    }
                }

                if (absenceType.IsTimeOffForOvertime)
                {
                    var overtimeBalanceForEmployee = _overtimeBankService.GetEmployeeOvertimeBalance(unitOfWork, affectedEmployee.Id, null);

                    if (overtimeBalanceForEmployee - absenceRequest.Hours < affectedEmployee.MinimumOvertimeBankBalance)
                    {
                        yield return AlertDto.CreateError(AbsenceRequestResources.NotSufficientOvertimeAmount_ErrorMessage);
                    }
                }

                var isOverlaping = unitOfWork.Repositories.AbsenceRequests
                    .Where(r =>
                        r.AffectedUserId == absenceRequest.AffectedUserId
                        && r.Request.Id != absenceRequest.Id
                        && r.Request.Status != RequestStatus.Rejected
                        && r.Request.Status != RequestStatus.Error)
                    .Where(DateRangeHelper.OverlapingDateRange<AbsenceRequest>(r => r.From, r => r.To, absenceRequest.From, absenceRequest.To))
                    .Any();

                if (isOverlaping)
                {
                    yield return AlertDto.CreateError(AbsenceRequestResources.OverlapingAbsence_ErrorMessage);
                }
            }
        }

        private bool ValidateAffectedUser(AbsenceRequestDto absenceRequest)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkService.Create())
                {
                    var isMyHrEmployeeOrMyself =
                        _userBusinessLogic.GetIsMyHrEmployeeOrMyselfBusinessLogic(PrincipalProvider.Current.Id.Value);

                    return unitOfWork.Repositories.Users.Any(
                        new BusinessLogic<User, bool>(u =>
                            u.Id == absenceRequest.AffectedUserId
                            && isMyHrEmployeeOrMyself.Call(u)).AsExpression());
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public override string GetRequestDescription(RequestDto request, AbsenceRequestDto data)
        {
            var absenceName = _absenceService.GetAbsenceTypeNameByAbsenceTypeId(data.AbsenceTypeId);

            if (data.AffectedUserId != request.RequestingUserId)
            {
                var affectedUser = _userService.GetUserById(data.AffectedUserId);

                return string.Format(AbsenceRequestResources.BehalfRequestDescriptionFormat,
                    absenceName, affectedUser.GetFullName(), data.From, data.To, data.Hours);
            }

            return string.Format(AbsenceRequestResources.RequestDescriptionFormat,
                absenceName, data.From, data.To, data.Hours);
        }

        private bool ContainsLockedTimeReport(long employeeId, DateTime from, DateTime to)
        {
            if (from > to)
            {
                return false;
            }

            var fromDate = DateHelper.MonthsInDate(from);
            var toDate = DateHelper.MonthsInDate(to);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var containsClosedTimePeriod = unitOfWork.Repositories.TimeReports
                         .Any(t =>
                             t.EmployeeId == employeeId
                             && t.Year * 12 + t.Month >= fromDate
                             && t.Year * 12 + t.Month <= toDate
                             && (t.Status == TimeReportStatus.Submitted || t.Status == TimeReportStatus.Accepted));

                return containsClosedTimePeriod;
            }
        }

        private decimal GetAbsenceHours(long employeeId, DateTime from, DateTime to)
        {
            return _employmentPeriodService.GetContractedHours(employeeId, from, to).Sum(h => h.Hours);
        }

        public override AbsenceRequestDto GetDraft(IDictionary<string, object> parameters)
        {
            ValidateCurrentPrincipalHasEmployee();

            DateTime from = _timeService.GetCurrentDate();
            DateTime to = _timeService.GetCurrentDate().AddDays(1);

            var absenceRequest = new AbsenceRequestDto
            {
                AffectedUserId = PrincipalProvider.Current.Id.Value,
                From = from,
                To = to,
                Hours = GetAbsenceHours(PrincipalProvider.Current.EmployeeId, from.StartOfDay(), to.EndOfDay())
            };

            return absenceRequest;
        }

        public override bool CanBeDeleted(RequestDto requestDto, long byUserId)
        {
            var absenceRequest = (AbsenceRequestDto)requestDto.RequestObject;
            var currentDate = _timeService.GetCurrentDate();
            var revokingAbsenceThresholdInDays = _systemParameterService.GetParameter<int>(ParameterKeys.RevokingAbsenceThresholdInDays);

            if (_userService.GetUserRolesById(byUserId).Contains(SecurityRoleType.CanDeleteAbsenceRequests))
            {
                return true;
            }

            if (absenceRequest.AffectedUserId == byUserId && revokingAbsenceThresholdInDays < (absenceRequest.From - currentDate).Days)
            {
                return true;
            }

            return base.CanBeDeleted(requestDto, byUserId);
        }

        public override BoolResult OnDeleting(Request request, RequestDto requestDto, IUnitOfWork<IWorkflowsDbScope> unitOfWork)
        {
            var absenceRequest = (AbsenceRequestDto)requestDto.RequestObject;
            var affectedEmployee =
                unitOfWork.Repositories.Employees.FirstOrDefault(e => e.UserId == absenceRequest.AffectedUserId);

            if (unitOfWork.Repositories.TimeReportRows.Any(r =>
                r.RequestId == requestDto.Id
                && (
                    r.TimeReport.Status == TimeReportStatus.Submitted
                    || r.TimeReport.Status == TimeReportStatus.Accepted)))
            {
                return new BoolResult(
                    false,
                    new AlertDto[] { AlertDto.CreateError(AbsenceRequestResources.AffectsOpenTimeReport) });
            }
            else
            {
                using (var timeTrackingUnitOfWork = _unitOfWorkService.Create())
                {
                    _absenceService.UpdateTimeReportOnRejected(timeTrackingUnitOfWork, request.Id, absenceRequest.From, absenceRequest.To, affectedEmployee.Id);
                    timeTrackingUnitOfWork.Commit();
                }
            }

            unitOfWork.Repositories.EmployeeAbsences.DeleteEach(a => a.RequestId == requestDto.Id);
            unitOfWork.Repositories.Substitutions.DeleteEach(s => s.RequestId == requestDto.Id);

            DeleteCalendarEntries(unitOfWork, requestDto);

            var result = DeleteVacationBalance(unitOfWork, requestDto, absenceRequest, affectedEmployee);

            if (!result.IsSuccessful)
            {
                return result;
            }

            using (var ttUnitOfWork = _unitOfWorkService.Create())
            {
                result = DeleteOvertimeBanks(ttUnitOfWork, requestDto.Id);

                if (!result.IsSuccessful)
                {
                    return result;
                }
            }

            var periodsDuringAbsence = affectedEmployee.EmploymentPeriods
                .Where(p => EmploymentPeriodBusinessLogic.OverlapingRange.Call(p, absenceRequest.From, absenceRequest.To))
                .ToList();

            var shouldNotify = periodsDuringAbsence.Any(p => p.ContractType.ShouldNotifyOnAbsenceRemoval);

            if (shouldNotify)
            {
                _absenceService.NotifyAboutRevokedAbsence(requestDto, absenceRequest);
            }

            return base.OnDeleting(request, requestDto, unitOfWork);
        }

        private BoolResult DeleteOvertimeBanks(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, long requestId)
        {
            var result = new BoolResult(true);

            try
            {
                unitOfWork.Repositories.OvertimeBanks.DeleteEach(b => b.RequestId == requestId);
                unitOfWork.Commit();
            }
            catch (OvertimeBalanceLessThanMinimunDbException ex)
            {
                result.AddAlert(AlertType.Error, ex.Message);
                result.IsSuccessful = false;
            }

            return result;
        }

        private void DeleteCalendarEntries(IUnitOfWork<IWorkflowsDbScope> unitOfWork, RequestDto requestDto)
        {
            var calendarEntries =
                unitOfWork.Repositories.AbsenceCalendarEntries.Where(ce => ce.RequestId == requestDto.Id).ToList();

            foreach (var calendarEntry in calendarEntries)
            {
                try
                {
                    _calendarOfficeService.DeleteAppointment(calendarEntry.ExchangeId);
                    unitOfWork.Repositories.AbsenceCalendarEntries.Delete(calendarEntry);
                    unitOfWork.Commit();
                }
                catch (Exception)
                {
                    // ignored
                }
            }
        }

        public override void Execute(RequestDto request, AbsenceRequestDto absenceRequest)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var affectedEmployee = unitOfWork.Repositories.Employees.Single(e => e.UserId == absenceRequest.AffectedUserId);
                var absenceType = unitOfWork.Repositories.AbsenceTypes.GetById(absenceRequest.AbsenceTypeId);

                if (absenceType.IsVacation)
                {
                    AddVacationBalance(unitOfWork, request, absenceRequest, affectedEmployee);
                }

                if (absenceType.IsTimeOffForOvertime)
                {
                    UpdateOverTimeBank(unitOfWork, request, absenceRequest, affectedEmployee);
                }

                CreateEmployeeAbsence(unitOfWork, absenceRequest, request, affectedEmployee);
                CreateSubstitution(unitOfWork, absenceRequest, request);

                _timeReportService.UpdateTimeReportRowsStatus(request.Id, TimeReportStatus.Accepted);
                ApproveAbsenceOnlyTimeReports(unitOfWork, request.Id);

                unitOfWork.Commit();

                var calendars = GetAbsenceCalendars(affectedEmployee);

                foreach (var calendar in calendars)
                {
                    string appointmentId;

                    try
                    {
                        appointmentId = _calendarOfficeService.CreateAppointment(
                            calendar.OfficeGroupEmail,
                            CalendarAppointmentType.Absence,
                            absenceType.NameEn,
                            absenceRequest.From.StartOfDay(),
                            absenceRequest.To.EndOfDay(),
                            request.Description.English,
                            affectedEmployee.FullName,
                            new[] { affectedEmployee.Email });
                    }
                    catch (Exception)
                    {
                        appointmentId = null;
                    }

                    if (!appointmentId.IsNullOrEmpty() && !unitOfWork.Repositories.AbsenceCalendarEntries.Any(e => e.ExchangeId == appointmentId))
                    {
                        unitOfWork.Repositories.AbsenceCalendarEntries.Add(new AbsenceCalendarEntry
                        {
                            ExchangeId = appointmentId,
                            RequestId = request.Id
                        });

                        unitOfWork.Commit();
                    }
                }
            }
        }

        private void ApproveAbsenceOnlyTimeReports(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, long requestId)
        {
            //approve given report if all non-empty proejcts are of absence type and working hours are equal contracted hours
            var timeReports = unitOfWork.Repositories.TimeReports.Where(tr => tr.Rows.Any(r => r.RequestId == requestId)).ToList();

            foreach (var timeReport in timeReports)
            {
                if (timeReport.Rows.Where(r => r.DailyEntries.Select(e => e.Hours).DefaultIfEmpty().Sum() > 0).All(r => r.AbsenceTypeId.HasValue && r.Status == TimeReportStatus.Accepted) &&
                    timeReport.ContractedHours == timeReport.ReportedNormalHours)
                {
                    _timeReportService.UpdateTimeReportStatus(unitOfWork, timeReport.Id, TimeReportStatus.Accepted);
                }
            }
        }

        public List<CalendarAbsenceDto> GetAbsenceCalendars(Employee employee)
        {
            var calendarAbsenceToDtoMapping = ClassMappingFactory.CreateMapping<CalendarAbsence, CalendarAbsenceDto>();

            return employee.CalendarAbsences.Select(c => calendarAbsenceToDtoMapping.CreateFromSource(c)).ToList();
        }

        private void CreateSubstitution(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, AbsenceRequestDto absenceRequest, RequestDto request)
        {
            if (absenceRequest.SubstituteUserId.HasValue)
            {
                var substitution = unitOfWork.Repositories.Substitutions.CreateEntity();

                substitution.RequestId = request.Id;
                substitution.From = absenceRequest.From;
                substitution.To = absenceRequest.To;
                substitution.ReplacedUserId = absenceRequest.AffectedUserId;
                substitution.SubstituteUserId = absenceRequest.SubstituteUserId.Value;

                unitOfWork.Repositories.Substitutions.Add(substitution);
            }
        }

        private void CreateEmployeeAbsence(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, AbsenceRequestDto absenceRequest, RequestDto request, Employee employee)
        {
            var approvedOn = _timeService.GetCurrentTime();

            var employeeAbsence = unitOfWork.Repositories.EmployeeAbsences.CreateEntity();
            employeeAbsence.EmployeeId = employee.Id;
            employeeAbsence.AbsenceTypeId = absenceRequest.AbsenceTypeId;
            employeeAbsence.From = absenceRequest.From;
            employeeAbsence.To = absenceRequest.To;
            employeeAbsence.ApprovedOn = approvedOn;
            employeeAbsence.Hours = absenceRequest.Hours;
            employeeAbsence.RequestId = request.Id;

            unitOfWork.Repositories.EmployeeAbsences.Add(employeeAbsence);
        }

        public override IEnumerable<ApproverGroupDto> GetApproverGroups(RequestDto request, AbsenceRequestDto absenceRequest)
        {
            IList<long> approverIds;
            ApprovalGroupMode groupMode;

            if (IsApprovedByHr(absenceRequest.AbsenceTypeId))
            {
                groupMode = ApprovalGroupMode.Or;

                var affectedEmployeeId = _employeeService.GetEmployeeIdOrDefaultByUserId(absenceRequest.AffectedUserId);

                if (affectedEmployeeId == null)
                {
                    throw new BusinessException(AbsenceRequestResources.NotAnEmployee_ErrorMessage);
                }

                var context = _systemParameterService.GetDefaultContext(affectedEmployeeId.Value);
                var approverLogins = _systemParameterService.GetParameter<string[]>(ParameterKeys.HRAbsenceAcceptingPersonLogins, context);

                approverIds = _employeeService.GetEmployeeIdsByLogins(approverLogins);
            }
            else
            {
                groupMode = ApprovalGroupMode.And;
                approverIds = GetNonHrApproversEmployeeIds(absenceRequest, absenceRequest.AffectedUserId);
            }

            if (approverIds.IsNullOrEmpty())
            {
                yield break;
            }

            yield return new ApproverGroupDto(groupMode, approverIds);
        }

        public override IEnumerable<long> GetBusinessWatchersUserIds(RequestDto request, AbsenceRequestDto requestDto)
        {
            var watcherIds = new List<long>();
            if (request.RequestingUserId != requestDto.AffectedUserId)
            {
                watcherIds.Add(requestDto.AffectedUserId);
            }

            if (IsApprovedByHr(requestDto.AbsenceTypeId))
            {
                var approverEmployeeIds = GetNonHrApproversEmployeeIds(requestDto, requestDto.AffectedUserId);

                using (var unitOfWork = _unitOfWorkService.Create())
                {
                    var approverUserIds = unitOfWork.Repositories
                        .Employees
                        .GetByIds(approverEmployeeIds)
                        .Where(e => e.UserId.HasValue)
                        .Select(e => e.UserId.Value)
                        .ToList();

                    watcherIds.AddRange(approverUserIds);
                }
            }

            return watcherIds;
        }

        private bool IsApprovedByHr(long absenceType)
        {
            var hrApprovalRequiredAbsenceTypeIds = _absenceService.GetHRApprovalRequiredAbsenceTypeIds().Distinct();

            return hrApprovalRequiredAbsenceTypeIds.Contains(absenceType);
        }

        public override void OnSubmited(Request request, RequestDto requestDto)
        {
            base.OnSubmited(request, requestDto);
            var absenceRequest = (AbsenceRequestDto)requestDto.RequestObject;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var affectedEmployee = unitOfWork.Repositories.Employees.Single(e => e.UserId == absenceRequest.AffectedUserId);
                var absenceType = unitOfWork.Repositories.AbsenceTypes.GetById(absenceRequest.AbsenceTypeId);

                var hours = _absenceService.IsHourlyReportingAllowed(absenceType, absenceRequest.From, absenceRequest.To)
                    ? new decimal?(absenceRequest.Hours)
                    : null;

                _absenceService.UpdateTimeReportOnSubmitted(unitOfWork, requestDto.Id, absenceRequest.AbsenceTypeId, absenceRequest.From,
                    absenceRequest.To, affectedEmployee.Id, hours);

                unitOfWork.Commit();

                // Send notifications

                var peopleToNotifyIds = GetEmployeeIdsToNotify(requestDto);

                var receivers = unitOfWork.Repositories.Employees.GetByIds(peopleToNotifyIds.Distinct()).ToList();

                foreach (var receiver in receivers)
                {
                    var emailReceiver = new EmailRecipientDto
                    {
                        EmailAddress = receiver.Email,
                        FullName = receiver.FullName
                    };

                    var notificationDto = new AbsenceRequestNotificationDto
                    {
                        ReceiverName = receiver.FirstName,
                        EmployeeFullName = requestDto.RequesterFullName,
                        Description = requestDto.Description.English
                    };

                    _absenceNotificationService.NotifyAboutAbsence(notificationDto, emailReceiver);
                }
            }
        }

        public override void OnRejected(Request request, RequestDto requestDto)
        {
            base.OnRejected(request, requestDto);

            var absenceRequest = (AbsenceRequestDto)requestDto.RequestObject;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var affectedEmployee = unitOfWork.Repositories.Employees.Single(e => e.UserId == absenceRequest.AffectedUserId);

                _absenceService.UpdateTimeReportOnRejected(unitOfWork, request.Id, absenceRequest.From, absenceRequest.To, affectedEmployee.Id);
                unitOfWork.Commit();
            }
        }

        private List<long> GetEmployeeIdsToNotify(RequestDto request)
        {
            var absenceRequest = (AbsenceRequestDto)request.RequestObject;

            var peopleToNotifyIds = _employeeService.GetEmployeeIdsByUserIds(request.WatchersUserIds).ToList();
            //if it is HR we send to PM also
            bool isApprovedByHr = IsApprovedByHr(absenceRequest.AbsenceTypeId);

            if (isApprovedByHr)
            {
                var approversIds = GetNonHrApproversEmployeeIds(absenceRequest, absenceRequest.AffectedUserId);

                //if approvals has that email above (base.OnSubmitted) - I ommit them here 
                var approvalUsersIds = request.ApprovalGroups.SelectMany(x => x.Approvals).Select(x => x.UserId).Distinct().ToList();
                var approvalEmployeesIds = _employeeService.GetEmployeeIdsByUserIds(approvalUsersIds);
                approversIds = approversIds.Where(r => !approvalEmployeesIds.Contains(r)).ToList();

                peopleToNotifyIds = peopleToNotifyIds.Concat(approversIds).ToList();
            }

            return peopleToNotifyIds;
        }

        private List<long> GetNonHrApproversEmployeeIds(AbsenceRequestDto absenceRequest, long affectedUserId)
        {
            var affectedEmployeeId = _employeeService.GetEmployeeOrDefaultByUserId(affectedUserId).Id;
            var approversIds = _approvingPersonService.GetProjectApprovers(affectedEmployeeId, absenceRequest.From, absenceRequest.To).ToList();
            var mainApprover = _approvingPersonService.GetEmployeeMainManager(affectedEmployeeId);

            if (mainApprover.HasValue)
            {
                approversIds.Add(mainApprover.Value);
            }

            return approversIds;
        }

        private BoolResult DeleteVacationBalance(IUnitOfWork<IWorkflowsDbScope> unitOfWork, RequestDto request, AbsenceRequestDto absenceRequest, Employee employee)
        {
            var vacationBalance = unitOfWork.Repositories.VacationBalances.SingleOrDefault(v => v.RequestId == request.Id);

            // Try to find old version vacation balance :S
            if (vacationBalance == null)
            {
                vacationBalance = unitOfWork.Repositories.VacationBalances.SingleOrDefault(v =>
                    v.EmployeeId == employee.Id
                    && v.HourChange == absenceRequest.Hours * (-1)
                    && v.EffectiveOn == absenceRequest.From
                    && v.Operation == VacationOperation.Vacation);
            }

            var result = new BoolResult(true);

            if (vacationBalance != null)
            {
                try
                {
                    unitOfWork.Repositories.VacationBalances.Delete(vacationBalance);
                    unitOfWork.Commit();
                }
                catch (VacationBalanceLessThanZeroDbException ex)
                {
                    result.AddAlert(AlertType.Error, ex.Message);
                    result.IsSuccessful = false;
                }
            }

            return result;
        }

        private void AddVacationBalance(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, RequestDto request, AbsenceRequestDto absenceRequest, Employee employee)
        {
            var vacationBalanceDifference = absenceRequest.Hours * (-1);

            var vacationBalance = new VacationBalance
            {
                RequestId = request.Id,
                EffectiveOn = absenceRequest.From,
                EmployeeId = employee.Id,
                Operation = VacationOperation.Vacation,
                HourChange = vacationBalanceDifference,
                Comment = absenceRequest.Comment,
            };

            unitOfWork.Repositories.VacationBalance.Add(vacationBalance);
        }

        private void UpdateOverTimeBank(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, RequestDto request, AbsenceRequestDto absenceRequest, Employee affectedEmployee)
        {
            var overtimeBankEntry = new OvertimeBank
            {
                EmployeeId = affectedEmployee.Id,
                OvertimeChange = absenceRequest.Hours * -1,
                RequestId = request.Id,
                EffectiveOn = absenceRequest.From,
                Comment = $"Created based on the absence request: {absenceRequest.From.ToString("d", new CultureInfo("en-US"))}"
            };

            unitOfWork.Repositories.OvertimeBanks.Add(overtimeBankEntry);
        }

        public override IQueryable<Request> ConfigureIncludes(IQueryable<Request> repository)
        {
            return repository.Include(r => r.AbsenceRequest);
        }

        public override AlertDto GetRequestSubmittedAlert(RequestDto request, ICollection<string> approvalNames)
        {
            var absenceRequest = (AbsenceRequestDto)request.RequestObject;
            var hRApprovalRequiredAbsenceTypeIds = _absenceService.GetHRApprovalRequiredAbsenceTypeIds();
            var hRApprovalMessage = hRApprovalRequiredAbsenceTypeIds.Contains(absenceRequest.AbsenceTypeId)
                                    ? AbsenceRequestResources.HRApprovalMessage
                                    : string.Empty;

            return new AlertDto
            {
                Type = AlertType.Success,
                Message = string.Format(WorkflowResources.RequestSubmitted, string.Join(", ", approvalNames) + ". " + hRApprovalMessage),
                IsDismissable = true
            };
        }

        public DateTime GetProperDateForNextMeeting(long userId, DateTime date)
        {
            var absenceRequests = _absenceService.GetAbsenceRequestsContainSpecificDate(userId, date);

            if (!absenceRequests.IsNullOrEmpty())
            {
                var dayDelay = _systemParameterService.GetParameter<int>(ParameterKeys.NextMeetingScheduleAfterAbsenceDayDelay);

                return absenceRequests.Max(a => a.To).AddDays(dayDelay);
            }

            return date;
        }

        public override IEnumerable<EmailRecipientDto> GetExceptionNotificationRecipients(RequestDto request)
        {
            var recipientsUserIds = request.WatchersUserIds.ToList();

            recipientsUserIds.AddRange(request.AffectedUserIds);
            recipientsUserIds.Add(request.RequestingUserId);

            if (request.CurrentApprovalGroupId.HasValue)
            {
                var approvalGroup = request.ApprovalGroups
                    .Single(g => g.Id == request.CurrentApprovalGroupId);

                recipientsUserIds.AddRange(approvalGroup.Approvals.Select(a => a.UserId));
            }

            var recipients = _userService
                .GetUsersByIds(recipientsUserIds.Distinct().ToArray())
                .Select(e => new EmailRecipientDto
                {
                    FullName = e.GetFullName(),
                    EmailAddress = e.Email,
                    Type = RecipientType.Cc
                }).ToArray();

            return recipients;
        }
    }
}