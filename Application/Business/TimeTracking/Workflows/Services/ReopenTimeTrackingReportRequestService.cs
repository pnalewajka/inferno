﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.EventSourcing.Interfaces;
using Smt.Atomic.Business.TimeTracking.BusinessEvents;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Business.TimeTracking.Resources;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.Business.Workflows.Services;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.TimeTracking.Workflows.Services
{
    internal class ReopenTimeTrackingReportRequestService : RequestServiceBase<ReopenTimeTrackingReportRequest, ReopenTimeTrackingReportRequestDto>
    {
        private const string MonthPropertyName = "request-object-view-model.month";
        private const string YearPropertyName = "request-object-view-model.year";

        private readonly IUserService _userService;
        private readonly IEmployeeService _employeeService;
        private readonly ITimeReportService _timeReportService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly ITimeService _timeService;
        private readonly IBusinessEventQueueService _businessEventQueueService;

        public ReopenTimeTrackingReportRequestService(IPrincipalProvider principalProvider,
                                                      IWorkflowNotificationService workflowNotificationService,
                                                      IEmployeeService employeeService,
                                                      ITimeReportService timeReportService,
                                                      ISystemParameterService systemParameterService,
                                                      ITimeService timeService,
                                                      IActionSetService actionSetService,
                                                      IUserService userService,
                                                      IClassMappingFactory classMappingFactory,
                                                      IBusinessEventQueueService businessEventQueueService,
                                                      IRequestWorkflowService requestWorkflowService)
            : base(actionSetService, principalProvider, classMappingFactory, workflowNotificationService, requestWorkflowService, systemParameterService)
        {
            _employeeService = employeeService;
            _timeReportService = timeReportService;
            _systemParameterService = systemParameterService;
            _timeService = timeService;
            _userService = userService;
            _businessEventQueueService = businessEventQueueService;
        }

        public override string RequestName => WorkflowResources.ReopenTimeTrackingReportRequestName;

        public override RequestType RequestType => RequestType.ReopenTimeTrackingReportRequest;

        public override RequestTypeGroup RequestTypeGroup => RequestTypeGroup.TimeTracking;


        public override SecurityRoleType? RequiredRoleType => SecurityRoleType.CanReopenTimeTrackingReportRequest;

        public override void Execute(RequestDto request, ReopenTimeTrackingReportRequestDto requestDto)
        {
            var employeeId = _employeeService.GetEmployeeOrDefaultByUserId(request.RequestingUserId).Id;

            var result = _timeReportService.ReopenTimeReport(employeeId, requestDto.Year, (byte)requestDto.Month, requestDto.Reason, requestDto.ProjectIds);

            if (!result.IsSuccessful)
            {
                throw new BusinessException(result.Alerts.First().Message);
            }
        }

        public override bool CanBeStartedManually()
        {
            return false;
        }

        public override IEnumerable<ApproverGroupDto> GetApproverGroups(RequestDto request, ReopenTimeTrackingReportRequestDto requestDto)
        {
            var approverAcronyms = _systemParameterService.GetParameter<string[]>(ParameterKeys.TimeReportReopenApproverAcronyms);

            var approverIds = _employeeService.GetEmployeesOrDefaultByAcronyms(approverAcronyms).Select(e => e.Id).ToList();

            yield return new ApproverGroupDto(ApprovalGroupMode.Or, approverIds);
        }

        public override ReopenTimeTrackingReportRequestDto GetDraft(IDictionary<string, object> parameters)
        {
            int month, year;
            var employeeId = _employeeService.GetEmployeeOrDefaultByUserId(PrincipalProvider.Current.Id.Value).Id;

            if (parameters.ContainsKey(MonthPropertyName) && parameters.ContainsKey(YearPropertyName))
            {
                month = int.Parse(parameters[MonthPropertyName] as string);
                year = int.Parse(parameters[YearPropertyName] as string);
            }
            else
            {
                var currentDate = _timeService.GetCurrentDate();

                month = currentDate.Month;
                year = currentDate.Year;
            }

            return new ReopenTimeTrackingReportRequestDto
            {
                Month = month,
                Year = year,
                ProjectIds = _timeReportService.GetReopenableProjectTimeReportApprovalRequests(employeeId, year, (byte)month)
                                    .Select(p => p.Value.ProjectId)
                                    .Distinct()
                                    .ToArray(),
            };
        }

        public override IEnumerable<AlertDto> Validate(RequestDto request, ReopenTimeTrackingReportRequestDto requestDto)
        {
            var employeeId = _employeeService.GetEmployeeOrDefaultByUserId(request.RequestingUserId).Id;

            var timeReport = _timeReportService.GetReopenableProjectTimeReportApprovalRequests(employeeId, requestDto.Year, (byte)requestDto.Month);

            if (timeReport.Any())
            {
                if (!timeReport.Values.Any(x => requestDto.ProjectIds.Contains(x.ProjectId)))
                {
                    yield return new AlertDto { IsDismissable = false, Message = ReopenTimeTrackingRequestResources.ProjectNotCorrectError, Type = AlertType.Error };
                }
            }
        }

        public override string GetRequestDescription(RequestDto request, ReopenTimeTrackingReportRequestDto reopenRequest)
        {
            var requestingUser = _userService.GetUserById(request.RequestingUserId);

            return string.Format(WorkflowResources.ReopenTimeTrackingReportRequest,
                requestingUser.GetFullName(),
                reopenRequest.Year,
                reopenRequest.Month.ToString("00"));
        }

        public override IQueryable<Request> ConfigureIncludes(IQueryable<Request> repository)
        {
            return repository.Include(r => r.ReopenTimeTrackingReportRequest);
        }

        public override void OnAllApproved(Request request, RequestDto requestDto)
        {
            _businessEventQueueService.PublishBusinessEvent(new TimeReportReopenedBusinessEvent(request.Id));

            base.OnAllApproved(request, requestDto);
        }
    }
}
