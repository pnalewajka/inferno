﻿using System;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.TimeTracking.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Business.Configuration.Interfaces;

namespace Smt.Atomic.Business.TimeTracking.Workflows.Services
{
    public class EmailAbsenceNotificationService : IEmailAbsenceNotificationService
    {
        private readonly IReliableEmailService _reliableEmailService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IMessageTemplateService _messageTemplateService;

        public EmailAbsenceNotificationService(
            IReliableEmailService reliableEmailService,
            ISystemParameterService systemParameterService,
            IMessageTemplateService messageTemplateService)
        {
            _reliableEmailService = reliableEmailService;
            _systemParameterService = systemParameterService;
            _messageTemplateService = messageTemplateService;
        }

        public void NotifyAboutAbsence(AbsenceRequestNotificationDto request, EmailRecipientDto receiver)
        {
            var fromAddress = GetPortalAddress();

            var emailSubject = _messageTemplateService.ResolveTemplate(TemplateCodes.TimeTrackingAbsenceRequestNotificationMessageSubject, request);
            var emailBody = _messageTemplateService.ResolveTemplate(TemplateCodes.TimeTrackingAbsenceRequestNotificationMessageBody, request);
            var email = new EmailMessageDto
            {
                IsHtml = emailBody.IsHtml,
                Subject = emailSubject.Content,
                Body = emailBody.Content,
                From = fromAddress,
            };

            email.Recipients.Add(receiver);

            Send(email);
        }

        private void Send(EmailMessageDto message)
        {
            _reliableEmailService.EnqueueMessage(message);
        }

        private EmailRecipientDto GetPortalAddress()
        {
            return new EmailRecipientDto
            {
                EmailAddress = _systemParameterService.GetParameter<string>(ParameterKeys.SmtpEmailServiceDefaultFromEmailAddress),
                FullName = _systemParameterService.GetParameter<string>(ParameterKeys.SmtpEmailServiceDefaultFromFullName)
            };
        }
    }
}
