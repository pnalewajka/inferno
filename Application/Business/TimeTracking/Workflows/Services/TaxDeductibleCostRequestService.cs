﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.TimeTracking.Resources;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.Business.Workflows.Services;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Workflows.Services
{
    internal class TaxDeductibleCostRequestService : RequestServiceBase<TaxDeductibleCostRequest, TaxDeductibleCostRequestDto>
    {
        private readonly IUnitOfWorkService<IWorkflowsDbScope> _unitOfWorkService;
        private readonly IApprovingPersonService _approvingPersonService;
        private readonly IEmployeeService _employeeService;
        private readonly ITimeService _timeService;
        private readonly IProjectService _projectService;

        public TaxDeductibleCostRequestService(
            IActionSetService actionSetService,
            IPrincipalProvider principalProvider,
            IClassMappingFactory classMappingFactory,
            IWorkflowNotificationService workflowNotificationService,
            IEmployeeService employeeService,
            IApprovingPersonService approvingPersonService,
            IUnitOfWorkService<IWorkflowsDbScope> unitOfWorkService,
            ITimeService timeService,
            IProjectService projectService,
            IRequestWorkflowService requestWorkflowService,
            ISystemParameterService systemParameterService)
            : base(actionSetService,
                  principalProvider,
                  classMappingFactory,
                  workflowNotificationService,
                  requestWorkflowService,
                  systemParameterService)
        {
            _unitOfWorkService = unitOfWorkService;
            _approvingPersonService = approvingPersonService;
            _employeeService = employeeService;
            _timeService = timeService;
            _projectService = projectService;
        }
        public override RequestTypeGroup RequestTypeGroup => RequestTypeGroup.TimeTracking;

        public override SecurityRoleType? RequiredRoleType => SecurityRoleType.CanRequestTaxDeductibleCost;

        public override RequestType RequestType => RequestType.TaxDeductibleCostRequest;

        public override string RequestName => WorkflowResources.TaxDeductibleCostRequestName;

        public override IQueryable<Request> ConfigureIncludes(IQueryable<Request> repository)
        {
            return repository.Include(r => r.TaxDeductibleCostRequest);
        }

        public override void Execute(RequestDto request, TaxDeductibleCostRequestDto taxDeductibleCostRequest)
        {
        }

        public override string ViewModelTypeName => IdentifierHelper.GetTypeByIdentifier("Models.TaxDeductibleCostRequestViewModel").FullName;

        public override IEnumerable<ApproverGroupDto> GetApproverGroups(RequestDto request, TaxDeductibleCostRequestDto taxDeductibleCostRequest)
        {
            var affectedEmployeeId = _employeeService.GetEmployeeOrDefaultByUserId(request.RequestingUserId).Id;
            var employeeId = _employeeService.GetEmployeeOrDefaultByUserId(request.RequestingUserId).Id;

            var projectApprovers = _approvingPersonService.GetProjectApprovers(affectedEmployeeId, taxDeductibleCostRequest.ProjectId);

            if (projectApprovers.Any())
            {
                yield return new ApproverGroupDto(ApprovalGroupMode.Or, projectApprovers);
            }
        }

        public override string GetRequestDescription(RequestDto request, TaxDeductibleCostRequestDto data)
        {
            var projectName = _projectService.GetProjectOrDefaultById(data.ProjectId)?.ClientProjectName;
            var monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(data.Month);
            var affectedUserFullname = $"{PrincipalProvider.Current.FirstName} {PrincipalProvider.Current.LastName}";

            return string.Format(TaxDeductibleCostRequestResources.RequestDescriptionFormat,
            projectName, affectedUserFullname, data.Hours, monthName, data.Year);
        }

        public override TaxDeductibleCostRequestDto GetDraft(IDictionary<string, object> parameters)
        {
            ValidateCurrentPrincipalHasEmployee();

            var currentDate = _timeService.GetCurrentDate();
            var projectId = _projectService.GetProjectIdsByUserId(PrincipalProvider.Current.Id.Value, currentDate).FirstOrDefault();

            return new TaxDeductibleCostRequestDto
            {
                Month = currentDate.Month,
                Year = currentDate.Year,
                ProjectId = projectId,
                AffectedUserFullName = $"{PrincipalProvider.Current.FirstName} {PrincipalProvider.Current.LastName}",
            };
        }

        public override bool CanBeAdded()
        {
            return _employeeService.IsDeductibleCostEnabledForEmployee(PrincipalProvider.Current.EmployeeId);
        }

        public override bool CanBeDeleted(RequestDto requestDto, long byUserId)
        {
            return requestDto.RequestingUserId == byUserId;
        }
    }
}