﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class AllocatedAndReportedHoursParametersDto
    {
        public long[] ProfileIds { get; set; }

        public long[] EmployeeOrgUnitIds { get; set; }

        public long[] LocationIds { get; set; }

        public long[] LineManagerIds { get; set; }

        public long[] JobMatrixIds { get; set; }

        public long[] UtilizationCategoryIds { get; set; }

        public PlaceOfWork? PlaceOfWork { get; set; }

        public BillabilityFiltering BillabilityFiltering { get; set; }
    }
}
