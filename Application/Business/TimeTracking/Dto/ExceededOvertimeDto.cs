﻿namespace Smt.Atomic.Business.TimeTracking.Dto
{
    internal class ExceededOvertimeDto
    {
        public long ProjectId { get; set; }

        public decimal ExceededHours { get; set; }
    }
}