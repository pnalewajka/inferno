﻿using System.Linq;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class CalendarAbsenceToCalendarAbsenceDtoMapping : ClassMapping<CalendarAbsence, CalendarAbsenceDto>
    {
        public CalendarAbsenceToCalendarAbsenceDtoMapping()
        {
            Mapping = e => new CalendarAbsenceDto
            {
                Id = e.Id,
                Name = e.Name,
                Description = e.Description,
                OfficeGroupEmail = e.OfficeGroupEmail,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp,
                EmployeesIds = e.EmployeesIds.Select(s => s.Id).ToArray(),
            };
        }
    }
}
