﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class VacationBalanceDto
    {
        public long Id { get; set; }

        public long EmployeeId { get; set; }

        public string EmployeeFirstName { get; set; }

        public string EmployeeLastName { get; set; }

        public string EmployeeLogin { get; set; }

        public DateTime EffectiveOn { get; set; }

        public long? RequestId { get; set; }

        public VacationOperation Operation { get; set; }

        public string Comment { get; set; }

        public decimal HourChange { get; set; }

        public decimal TotalHourBalance { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
