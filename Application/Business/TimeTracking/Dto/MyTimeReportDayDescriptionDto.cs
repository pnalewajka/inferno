﻿using System;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class MyTimeReportDayDescriptionDto
    {
        public DateTime Date { get; set; }
        public decimal ContractedHours { get; set; }
        public decimal? DailyWorkingLimit { get; set; }
        public string HolidayName { get; set; }
        public bool IsHoliday { get; set; }
    }
}