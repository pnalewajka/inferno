﻿using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class ClockCardDailyEntryToClockCardDailyEntryDtoMapping : ClassMapping<ClockCardDailyEntry, ClockCardDailyEntryDto>
    {
        public ClockCardDailyEntryToClockCardDailyEntryDtoMapping()
        {
            Mapping = e => new ClockCardDailyEntryDto
            {
                Id = e.Id,
                TimeReportId = e.TimeReportId,
                Day = e.Day,
                InTime = e.InTime,
                OutTime = e.OutTime,
                BreakDuration = e.BreakDuration
            };
        }
    }
}
