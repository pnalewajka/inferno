﻿using System;
namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class TimeReportReminderDto
    {
        public DateTime ReportDate { get; set; }

        public string EmployeeFirstName { get; set; }

        public string ServerAddress { get; set; }
    }
}
