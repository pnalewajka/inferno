﻿using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Business.Enums;
using System;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class MyTimeReportRowDto
    {
        public MyTimeReportRowDto()
        {
            SelectedAttributesValueIds = new long[0];
        }

        public long Id { get; set; }
        public long ProjectId { get; set; }
        public long? RequestId { get; set; }
        public string ProjectName { get; set; }
        public string Apn { get; set; }
        public string PetsCode { get; set; }
        public string KupferwerkProjectId { get; set; }
        public DateTime? ReportingStartsOn { get; set; }
        public string TaskName { get; set; }
        public IEnumerable<MyTimeReportDailyEntryDto> Days { get; set; }
        public long? ImportSourceId { get; set; }
        public string ImportedTaskCode { get; set; }
        public string ImportedTaskType { get; set; }
        public HourlyRateType OvertimeVariant { get; set; }
        public OverTimeTypes AllowedProjectOvertime { get; set; }
        public TimeReportStatus Status { get; set; }
        public bool IsReadOnly { get; set; }
        public long? AbsenceTypeId { get; set; }
        public IList<TimeReportProjectAttributeDto> Attributes { get; set; }
        public long[] SelectedAttributesValueIds { get; set; }
    }
}