﻿using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class AbsenceTypeDtoToAbsenceTypeMapping : ClassMapping<AbsenceTypeDto, AbsenceType>
    {
        public AbsenceTypeDtoToAbsenceTypeMapping()
        {
            Mapping = d => new AbsenceType
            {
                Id = d.Id,
                Code = d.Code,
                NameEn = d.Name.English,
                NamePl = d.Name.Polish,
                DescriptionEn = d.Description.English,
                DescriptionPl = d.Description.Polish,
                EmployeeInstructionEn = d.EmployeeInstruction.English,
                EmployeeInstructionPl = d.EmployeeInstruction.Polish,
                KbrCode = d.KbrCode,
                IsVacation = d.IsVacation,
                IsTimeOffForOvertime = d.IsTimeOffForOvertime,
                RequiresHRApproval = d.RequiresHRApproval,
                Timestamp = d.Timestamp,
                Order = d.Order,
                AllowHourlyReporting = d.AllowHourlyReporting,
                AbsenceProjectId = d.AbsenceProjectId,
            };
        }
    }
}
