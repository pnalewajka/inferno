﻿using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class EmployeeAbsenceDtoToEmployeeAbsenceMapping : ClassMapping<EmployeeAbsenceDto, EmployeeAbsence>
    {
        public EmployeeAbsenceDtoToEmployeeAbsenceMapping()
        {
            Mapping = d => new EmployeeAbsence
            {
                Id = d.Id,
                EmployeeId = d.EmployeeId,
                AbsenceTypeId = d.AbsenceTypeId,
                From = d.From,
                To = d.To,
                Hours = d.Hours,
                RequestId = d.RequestId,
                Timestamp = d.Timestamp
            };
        }
    }
}
