﻿using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class TimeReportProjectAttributeValueDtoToTimeReportProjectAttributeValueMapping : ClassMapping<TimeReportProjectAttributeValueDto, TimeReportProjectAttributeValue>
    {
        public TimeReportProjectAttributeValueDtoToTimeReportProjectAttributeValueMapping()
        {
            Mapping = d => new TimeReportProjectAttributeValue
            {
                Id = d.Id,
                IsDeleted = d.IsDeleted,
                AttributeId = d.AttributeId,
                Name = d.Name,
                Description = d.Description,
                Features = d.Features,
                Timestamp = d.Timestamp
            };
        }
    }
}
