﻿namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class HourlyRateDto
    {
        public long Id { get; set; }
        
        public string Description { get; set; }
    }
}
