﻿using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Helpers;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class ContractTypeToContractTypeDtoMapping : ClassMapping<ContractType, ContractTypeDto>
    {
        public ContractTypeToContractTypeDtoMapping()
        {
            Mapping = e => new ContractTypeDto
            {
                Id = e.Id,
                Name = new LocalizedString
                {
                    English = e.NameEn,
                    Polish = e.NamePl
                },
                Description = new LocalizedString
                {
                    English = e.DescriptionEn,
                    Polish = e.DescriptionPl
                },
                EmploymentType = e.EmploymentType,
                IsActive = e.IsActive,
                DailyWorkingLimit = e.DailyWorkingLimit,
                ActiveDirectoryCode = e.ActiveDirectoryCode,
                TimeTrackingCompensationCalculator = e.TimeTrackingCompensationCalculator,
                BusinessTripCompensationCalculator = e.BusinessTripCompensationCalculator,
                AllowedOverTimeTypes = e.AllowedOverTimeTypes,
                HasPaidVacation = e.HasPaidVacation,
                MinimunOvertimeBankBalance = e.MinimunOvertimeBankBalance,
                UseAutomaticOvertimeBankCalculation = e.UseAutomaticOvertimeBankCalculation,
                AbsenceTypeIds = e.AbsenceTypes.GetIds(),
                ShouldNotifyOnAbsenceRemoval = e.ShouldNotifyOnAbsenceRemoval,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                SettlementSectionsFeatures = e.SettlementSectionsFeatures,
                NotifyInvoiceAmount = e.NotifyInvoiceAmount,
                NotifyMissingBusinessTripSettlement = e.NotifyMissingBusinessTripSettlement,
                UseDailySettlementMeals = e.UseDailySettlementMeals,
                HasToSpecifyDestinationType = e.HasToSpecifyDestinationType,
                IsTaxDeductibleCostEnabled = e.IsTaxDeductibleCostEnabled,
                Timestamp = e.Timestamp,
            };
        }
    }
}
