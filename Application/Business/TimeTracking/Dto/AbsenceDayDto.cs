﻿using System;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class AbsenceDayDto
    {
        public DateTime Date { get; set; }
        public decimal Hours { get; set; }
    }
}
