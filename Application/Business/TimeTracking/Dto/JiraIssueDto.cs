﻿using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Data.Jira.Entities;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class JiraIssueDto : IssueWithWorklogs
    {
        public ProjectDto Project { get; set; }

        public JiraIssueDto(IssueWithWorklogs issue)
        {
            Id = issue.Id;
            Key = issue.Key;
            Fields = issue.Fields;
        }
    }
}
