﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class TimeTrackingProjectDtoToProjectMapping : ClassMapping<TimeTrackingProjectDto, Project>
    {
        public TimeTrackingProjectDtoToProjectMapping()
        {
            Mapping = project => new Project
            {
                Id = project.Id
            };
        }
    }
}