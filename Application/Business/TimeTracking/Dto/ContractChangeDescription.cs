﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class ContractChangeDescription
    {
        public string ChangeDescription { get; set; }

        public IList<ContractType> ModifiedContractType { get; set; }
    }
}
