﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class MyTimeReportDtoToTimeReportMapping : ClassMapping<MyTimeReportDto, TimeReport>
    {
        public MyTimeReportDtoToTimeReportMapping(
            IClassMapping<MyTimeReportRowDto, TimeReportRow> myTimeReportRowDtoToTimeReportRow,
            IClassMapping<ClockCardDailyEntryDto, ClockCardDailyEntry> clockCardDailyEntryDtoToClockCardDailyEntry)
        {
            Mapping = dto => new TimeReport
            {
                Id = dto.TimeReportId,
                EmployeeId = dto.EmployeeId,
                Month = dto.Month,
                Year = dto.Year,
                Status = dto.Status,
                TimeZoneOffsetMinutes = dto.TimeZoneOffsetMinutes,
                TimeZoneDisplayName = dto.TimeZoneDisplayName,
                InvoiceStatus = dto.InvoiceStatus,
                Rows = dto.Rows != null
                    ? dto.Rows.Select(myTimeReportRowDtoToTimeReportRow.CreateFromSource).ToList()
                    : new List<TimeReportRow>(),
                ClockCardDailyEntries = dto.ClockCardDailyEntries != null
                    ? dto.ClockCardDailyEntries.Select(clockCardDailyEntryDtoToClockCardDailyEntry.CreateFromSource).ToList()
                    : new List<ClockCardDailyEntry>()
            };
        }
    }
}