﻿using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class MyTimeReportDto
    {
        public long TimeReportId { get; set; }
        public long EmployeeId { get; set; }
        public string EmployeeFullName { get; set; }
        public int Year { get; set; }
        public byte Month { get; set; }
        public bool IsReadOnly { get; set; }
        public bool IsTimeReportAvailablePreviousMonth { get; set; }
        public TimeReportStatus Status { get; set; }
        public IList<MyTimeReportDayDescriptionDto> Days { get; set; }
        public IList<MyTimeReportWeekDescriptionDto> Weeks { get; set; }
        public IList<MyTimeReportRowDto> Rows { get; set; }
        public IList<ClockCardDailyEntryDto> ClockCardDailyEntries { get; set; }
        public string TimeZoneDisplayName { get; set; }
        public int TimeZoneOffsetMinutes { get; set; }
        public OverTimeTypes AllowedOvertime { get; set; }
        public InvoiceStatus InvoiceStatus { get; set; }

        public MyTimeReportDto()
        {
            Days = new List<MyTimeReportDayDescriptionDto>();
            Weeks = new List<MyTimeReportWeekDescriptionDto>();
            Rows = new List<MyTimeReportRowDto>();
            ClockCardDailyEntries = new List<ClockCardDailyEntryDto>();
        }
    }
}
