﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class TimeTrackingSummaryHoursParametersDto
    {
        public int FromYear { get; set; }

        public int FromMonth { get; set; }

        public int ToYear { get; set; }

        public int ToMonth { get; set; }

        public TimeReportStatus? Status { get; set; }

        public EmploymentType? EmploymentType { get; set; }

        public long[] EmployeeIds { get; set; }

        public long[] EmployeeOrgUnitIds { get; set; }

        public long[] LocationIds { get; set; }

        public long[] ContractTypeIds { get; set; }
    }
}