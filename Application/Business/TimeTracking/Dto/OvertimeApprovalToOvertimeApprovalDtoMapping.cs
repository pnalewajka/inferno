﻿using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class OvertimeApprovalToOvertimeApprovalDtoMapping : ClassMapping<OvertimeApproval, OvertimeApprovalDto>
    {
        public OvertimeApprovalToOvertimeApprovalDtoMapping()
        {
            Mapping = e => new OvertimeApprovalDto
            {
                Id = e.Id,
                EmployeeId = e.EmployeeId,
                ProjectId = e.ProjectId,
                DateFrom = e.DateFrom,
                DateTo = e.DateTo,
                HourLimit = e.HourLimit,
                RequestId = e.RequestId,
                Timestamp = e.Timestamp
            };
        }
    }
}
