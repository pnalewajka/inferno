﻿using System;
using System.Collections.Generic;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class ScheduleListDto
    { 
        public ScheduleListDto()
        {
            Employees = new List<EmployeeScheduleListDto>();
        }

        public DateTime PeriodStartDate { get; set; }

        public DateTime PeriodEndDate { get; set; }

        public DateTime AbsenceMonth { get; set; }

        public IList<EmployeeScheduleListDto> Employees { get; set; }         
    }
}
