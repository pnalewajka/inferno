﻿using System;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class AbsenceTypeDto
    {
        public long Id { get; set; }

        public string Code { get; set; }

        public LocalizedString Name { get; set; }

        public LocalizedString Description { get; set; }

        public LocalizedString EmployeeInstruction { get; set; }

        public string KbrCode { get; set; }

        public bool IsVacation { get; set; }

        public bool IsTimeOffForOvertime { get; set; }

        public bool RequiresHRApproval { get; set; }

        public bool AllowHourlyReporting { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public int Order { get; set; }

        public long? AbsenceProjectId { get; set; }
    }
}
