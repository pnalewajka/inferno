﻿using System;
using System.Collections.Generic;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class TimeReportProjectAttributeDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public long? DefaultValueId { get; set; }

        public List<TimeReportProjectAttributeValueDto> PossibleValues { get; set; }
    }
}
