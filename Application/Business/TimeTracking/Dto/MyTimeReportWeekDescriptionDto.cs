﻿namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class MyTimeReportWeekDescriptionDto
    {
        public int WeekNumber { get; set; }

        public int WeekDays { get; set; }
    }
}