﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class TimeReportProjectAttributeToTimeReportProjectAttributeDtoMapping : ClassMapping<TimeReportProjectAttribute, TimeReportProjectAttributeDto>
    {
        public TimeReportProjectAttributeToTimeReportProjectAttributeDtoMapping(
            IClassMapping<TimeReportProjectAttributeValue, TimeReportProjectAttributeValueDto> timeReportProjectAttributeValueToTimeReportProjectAttributeValueDto)
        {
            Mapping = e => new TimeReportProjectAttributeDto
            {
                Id = e.Id,
                Name = e.Name,
                Description = e.Description,
                DefaultValueId = e.DefaultValueId,
                PossibleValues = e.Values.Select(timeReportProjectAttributeValueToTimeReportProjectAttributeValueDto.CreateFromSource).ToList(),
            };
        }
    }
}
