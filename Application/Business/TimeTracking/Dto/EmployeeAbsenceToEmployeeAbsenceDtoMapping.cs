﻿using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class EmployeeAbsenceToEmployeeAbsenceDtoMapping : ClassMapping<EmployeeAbsence, EmployeeAbsenceDto>
    {
        public EmployeeAbsenceToEmployeeAbsenceDtoMapping()
        {
            Mapping = e => new EmployeeAbsenceDto
            {
                Id = e.Id,
                EmployeeId = e.EmployeeId,
                AbsenceTypeId = e.AbsenceTypeId,
                From = e.From,
                To = e.To,
                Hours = e.Hours,
                RequestId = e.RequestId,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
