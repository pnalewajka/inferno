﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class AbsencesDataSourceParametersBaseDto
    {
        public bool? IsVacation { get; set; }

        public long[] AbsenceTypeIds { get; set; }

        public EmploymentType? EmploymentType { get; set; }

        public long[] ContractTypeIds { get; set; }

        public long[] CompanyIds { get; set; }

        public long[] OrgUnitIds { get; set; }

        public long[] LocationIds { get; set; }
    }
}
