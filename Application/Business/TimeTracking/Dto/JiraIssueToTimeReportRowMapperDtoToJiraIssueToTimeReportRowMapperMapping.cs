﻿using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class JiraIssueToTimeReportRowMapperDtoToJiraIssueToTimeReportRowMapperMapping : ClassMapping<JiraIssueToTimeReportRowMapperDto, JiraIssueToTimeReportRowMapper>
    {
        public JiraIssueToTimeReportRowMapperDtoToJiraIssueToTimeReportRowMapperMapping()
        {
            Mapping = d => new JiraIssueToTimeReportRowMapper
            {
                Id = d.Id,
                Name = d.Name,
                SourceCode = d.SourceCode,
                Timestamp = d.Timestamp
            };
        }
    }
}
