﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class TimeReportProjectDetailsDto
    {
        public long EmployeeId { get; set; }

        public string EmployeeLastName { get; set; }

        public string EmployeeFirstName { get; set; }

        public long OrgUnitId { get; set; }

        public long ProjectId { get; set; }

        public TimeReportStatus TimeReportStatus { get; set; }

        public decimal OvertimeTotals { get; set; }

        public decimal OvertimeNormalHours { get; set; }

        public decimal OvertimeLateNightHours { get; set; }

        public decimal OvertimeWeekendHours { get; set; }

        public decimal OvertimeHolidayHours { get; set; }

        public decimal TotalHours { get; set; }

        public long TimeReportId { get; set; }

        public long? RequestId { get; set; }

        public RequestStatus? RequestStatus { get; set; }

        public bool IsRequestAvailable { get; set; }
    }
}