﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class TimeTrackingImportSourceToTimeTrackingImportSourceDtoMapping : ClassMapping<TimeTrackingImportSource, TimeTrackingImportSourceDto>
    {
        public TimeTrackingImportSourceToTimeTrackingImportSourceDtoMapping()
        {
            Mapping =
                e =>
                    new TimeTrackingImportSourceDto
                    {
                        Id = e.Id,
                        Code = e.Code,
                        Name = new LocalizedString
                        {
                            English = e.NameEn,
                            Polish = e.NamePl
                        },
                        ImportStrategyIdentifier = e.ImportStrategyIdentifier,
                        Configuration = e.Configuration,
                        Timestamp = e.Timestamp
                    };
        }
    }
}
