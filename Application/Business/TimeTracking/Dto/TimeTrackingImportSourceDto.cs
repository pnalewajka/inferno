﻿using System;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class TimeTrackingImportSourceDto
    {
        public long Id { get; set; }

        public string Code { get; set; }

        public LocalizedString Name { get; set; }

        public string ImportStrategyIdentifier { get; set; }

        public string Configuration { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
