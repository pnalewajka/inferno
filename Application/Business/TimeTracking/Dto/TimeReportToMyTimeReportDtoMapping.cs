﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class TimeReportToMyTimeReportDtoMapping : ClassMapping<TimeReport, MyTimeReportDto>
    {
        public TimeReportToMyTimeReportDtoMapping(
            IClassMapping<TimeReportRow, MyTimeReportRowDto> timeReportRowToMyTimeReportRowDto,
            IClassMapping<ClockCardDailyEntry, ClockCardDailyEntryDto> clockCardDailyEntryToclockCardDailyEntryDto)
        {
            Mapping = e => new MyTimeReportDto
            {
                TimeReportId = e.Id,
                EmployeeId = e.EmployeeId,
                EmployeeFullName = e.Employee.FullName,
                Month = e.Month,
                Year = e.Year,
                Status = e.Status,
                IsReadOnly = e.IsReadOnly,
                TimeZoneOffsetMinutes = e.TimeZoneOffsetMinutes,
                TimeZoneDisplayName = e.TimeZoneDisplayName,
                InvoiceStatus = e.InvoiceStatus,
                Rows = e.Rows != null
                    ? e.Rows.Select(timeReportRowToMyTimeReportRowDto.CreateFromSource).ToList()
                    : new List<MyTimeReportRowDto>(),
                ClockCardDailyEntries = e.ClockCardDailyEntries != null
                    ? e.ClockCardDailyEntries.Select(clockCardDailyEntryToclockCardDailyEntryDto.CreateFromSource).ToList()
                    : new List<ClockCardDailyEntryDto>()
            };
        }
    }
}