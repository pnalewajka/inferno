﻿using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class TimeReportDtoToTimeReportMapping : ClassMapping<TimeReportDto, TimeReport>
    {
        public TimeReportDtoToTimeReportMapping()
        {
            Mapping = d => new TimeReport
            {
                Id = d.Id,
                EmployeeId = d.EmployeeId,
                Year = d.Year,
                Month = d.Month,
                Status = d.Status,
                ControllingStatus = d.ControllingStatus,
                ControllingErrorMessage = d.ControllingErrorMessage,
                InvoiceNotificationStatus = d.InvoiceNotificationStatus,
                InvoiceNotificationErrorMessage = d.InvoiceNotificationErrorMessage,
                InvoiceStatus = d.InvoiceStatus,
                InvoiceIssues = d.InvoiceIssues,
                InvoiceRejectionReason = d.InvoiceRejectionReason,
                Timestamp = d.Timestamp,
            };
        }
    }
}
