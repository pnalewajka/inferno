﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class AbsencesKbrDataSourceParametersDto : AbsencesDataSourceParametersBaseDto
    {
        public DateTime AbsencesFrom { get; set; }

        public DateTime AbsencesTo { get; set; }

        public DateTime? ApprovedFrom { get; set; }

        public DateTime? ApprovedTo { get; set; }
    }
}
