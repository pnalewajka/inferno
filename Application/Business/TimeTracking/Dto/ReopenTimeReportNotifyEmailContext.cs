﻿using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Business.Configuration.Attributes;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    [JsonObject]
    [Identifier(nameof(ReopenTimeReportNotifyEmailContext))]
    public class ReopenTimeReportNotifyEmailContext
    {
        [Alias("company")]
        public string Company { get; set; }

        public ReopenTimeReportNotifyEmailContext(string company)
        {
            Company = company;
        }
    }
}
