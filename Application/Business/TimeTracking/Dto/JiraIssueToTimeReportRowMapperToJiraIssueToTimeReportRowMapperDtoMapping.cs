﻿using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class JiraIssueToTimeReportRowMapperToJiraIssueToTimeReportRowMapperDtoMapping : ClassMapping<JiraIssueToTimeReportRowMapper, JiraIssueToTimeReportRowMapperDto>
    {
        public JiraIssueToTimeReportRowMapperToJiraIssueToTimeReportRowMapperDtoMapping()
        {
            Mapping = e => new JiraIssueToTimeReportRowMapperDto
            {
                Id = e.Id,
                Name = e.Name,
                SourceCode = e.SourceCode,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
