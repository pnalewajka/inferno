﻿using System;
namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class CalendarAbsenceDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string OfficeGroupEmail { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public long[] EmployeesIds { get; set; }
    }
}
