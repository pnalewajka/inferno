﻿using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class ContractTypeDtoToContractTypeMapping : ClassMapping<ContractTypeDto, ContractType>
    {
        public ContractTypeDtoToContractTypeMapping()
        {
            Mapping = d => new ContractType
            {
                Id = d.Id,
                NameEn = d.Name.English,
                NamePl = d.Name.Polish,
                DescriptionEn = d.Description.English,
                DescriptionPl = d.Description.Polish,
                EmploymentType = d.EmploymentType,
                IsActive = d.IsActive,
                ActiveDirectoryCode = d.ActiveDirectoryCode,
                TimeTrackingCompensationCalculator = d.TimeTrackingCompensationCalculator,
                BusinessTripCompensationCalculator = d.BusinessTripCompensationCalculator,
                AllowedOverTimeTypes = d.AllowedOverTimeTypes,
                HasPaidVacation = d.HasPaidVacation,
                MinimunOvertimeBankBalance = d.MinimunOvertimeBankBalance,
                DailyWorkingLimit = d.DailyWorkingLimit,
                UseAutomaticOvertimeBankCalculation = d.UseAutomaticOvertimeBankCalculation,
                ShouldNotifyOnAbsenceRemoval = d.ShouldNotifyOnAbsenceRemoval,
                SettlementSectionsFeatures = d.SettlementSectionsFeatures,
                NotifyInvoiceAmount = d.NotifyInvoiceAmount,
                NotifyMissingBusinessTripSettlement = d.NotifyMissingBusinessTripSettlement,
                UseDailySettlementMeals = d.UseDailySettlementMeals,
                HasToSpecifyDestinationType = d.HasToSpecifyDestinationType,
                IsTaxDeductibleCostEnabled = d.IsTaxDeductibleCostEnabled,
                Timestamp = d.Timestamp,
            };
        }
    }
}
