﻿namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class ContractTypeContext
    {
        public bool ShouldFilterInactive { get; set; }
    }
}