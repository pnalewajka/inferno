﻿using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class VacationBalanceDtoToVacationBalanceMapping : ClassMapping<VacationBalanceDto, VacationBalance>
    {
        public VacationBalanceDtoToVacationBalanceMapping()
        {
            Mapping = d => new VacationBalance
            {
                Id = d.Id,
                EmployeeId = d.EmployeeId,
                EffectiveOn = d.EffectiveOn,
                RequestId = d.RequestId,
                Operation = d.Operation,
                Comment = d.Comment,
                HourChange = d.HourChange,
                Timestamp = d.Timestamp
            };
        }
    }
}
