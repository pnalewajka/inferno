﻿using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class ClockCardDailyEntryDtoToClockCardDailyEntryMapping : ClassMapping<ClockCardDailyEntryDto, ClockCardDailyEntry>
    {
        public ClockCardDailyEntryDtoToClockCardDailyEntryMapping()
        {
            Mapping = d => new ClockCardDailyEntry
            {
                Id = d.Id,
                TimeReportId = d.TimeReportId,
                Day = d.Day,
                InTime = d.InTime,
                OutTime = d.OutTime,
                BreakDuration = d.BreakDuration
            };
        }
    }
}
