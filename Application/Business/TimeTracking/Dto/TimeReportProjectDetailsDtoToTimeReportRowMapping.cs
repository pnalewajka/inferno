using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class TimeReportProjectDetailsDtoToTimeReportRowMapping : ClassMapping<TimeReportProjectDetailsDto, TimeReportRow>
    {
        public TimeReportProjectDetailsDtoToTimeReportRowMapping()
        {
            Mapping = e => new TimeReportRow();
        }
    }
}