﻿namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class JiraTimeReportImportSourceConfiguration
    {
        public string ServerAddress { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public JiraTimeReportImportSourceConfiguration()
        {
            ServerAddress = null;
            Username = null;
            Password = null;
        }
    }
}
