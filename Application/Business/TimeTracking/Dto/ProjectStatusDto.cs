﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class ProjectStatusDto
    {
        public long ProjectId { get; set; }

        public string Name { get; set; }

        public TimeReportStatus Status { get; set; }
    }
}
