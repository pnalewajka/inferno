﻿using System;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class TimeTrackingProjectDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string PetsCode { get; set; }

        public string ClientName { get; set; }

        public string ProjectManagerName { get; set; }

        public string JiraCode { get; set; }

        public TimeReportProjectAttributeDto[] Attributes { get; set; }

        public string Apn { get; set; }

        public string KupferwerkProjectId { get; set; }

        public DateTime? ReportingStartsOn { get; set; }
    }
}