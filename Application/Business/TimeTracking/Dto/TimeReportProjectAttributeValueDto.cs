﻿using Smt.Atomic.CrossCutting.Business.Enums;
using System;
namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class TimeReportProjectAttributeValueDto
    {
        public long Id { get; set; }

        public bool IsDeleted { get; set; }

        public long AttributeId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public AttributeValueFeature Features { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
