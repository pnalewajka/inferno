﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class WorkingStudentDataSourceParametersDto
    {
        public DateTime From { get; set; }

        public DateTime To { get; set; }

        public TimeReportStatus? Status { get; set; }

        public long[] EmployeeIds { get; set; }

        public long[] OrgUnitIds { get; set; }

        public long[] LocationIds { get; set; }

        public long[] ContractTypeIds { get; set; }

        public bool IsMyEmployees { get; set; }
    }
}
