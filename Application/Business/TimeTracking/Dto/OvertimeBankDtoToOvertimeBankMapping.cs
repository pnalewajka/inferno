﻿using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class OvertimeBankDtoToOvertimeBankMapping : ClassMapping<OvertimeBankDto, OvertimeBank>
    {
        public OvertimeBankDtoToOvertimeBankMapping()
        {
            Mapping = d => new OvertimeBank
            {
                Id = d.Id,
                EmployeeId = d.EmployeeId,
                RequestId = d.RequestId,
                OvertimeChange = d.OvertimeChange,
                EffectiveOn = d.EffectiveOn,
                OvertimeBalance = d.OvertimeBalance,
                Comment = d.Comment,
                Timestamp = d.Timestamp                
            };
        }
    }
}
