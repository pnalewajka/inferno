﻿using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class AbsenceTypeToAbsenceTypeDtoMapping : ClassMapping<AbsenceType, AbsenceTypeDto>
    {
        public AbsenceTypeToAbsenceTypeDtoMapping()
        {
            Mapping = e => new AbsenceTypeDto
            {
                Id = e.Id,
                Code = e.Code,
                Name = new LocalizedString
                {
                    English = e.NameEn,
                    Polish = e.NamePl
                },
                Description = new LocalizedString
                {
                    English = e.DescriptionEn,
                    Polish = e.DescriptionPl,
                },
                EmployeeInstruction = new LocalizedString
                {
                    English = e.EmployeeInstructionEn,
                    Polish = e.EmployeeInstructionPl
                },
                KbrCode = e.KbrCode,
                IsVacation = e.IsVacation,
                IsTimeOffForOvertime = e.IsTimeOffForOvertime,
                RequiresHRApproval = e.RequiresHRApproval,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp,
                Order = e.Order,
                AllowHourlyReporting = e.AllowHourlyReporting,
                AbsenceProjectId = e.AbsenceProjectId,
            };
        }
    }
}
