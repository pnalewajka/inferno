﻿namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class TimeTrackingProjectPickerContext
    {
        public enum PickerMode
        {
            TimeReport = 0,

            TimeReportReopening = 1,

            Overtime = 2
        }

        public PickerMode? Mode { get; set; }

        public long? EmployeeId { get; set; }

        public int? Year { get; set; }

        public byte? Month { get; set; }
    }
}
