﻿using System;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class MyTimeReportDailyEntryDto
    {
        public DateTime Day { get; set; }
        public decimal Hours { get; set; }
        public bool IsReadOnly { get; set; }
    }
}