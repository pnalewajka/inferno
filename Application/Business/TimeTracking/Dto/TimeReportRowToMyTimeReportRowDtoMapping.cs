﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.CrossCutting.Common.Extensions;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.BusinessLogic;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class TimeReportRowToMyTimeReportRowDtoMapping : ClassMapping<TimeReportRow, MyTimeReportRowDto>
    {
        private readonly IClassMapping<TimeReportProjectAttribute, TimeReportProjectAttributeDto> _timeReportProjectAttributeToTimeReportProjectAttributeDto;

        public TimeReportRowToMyTimeReportRowDtoMapping(
            IClassMapping<TimeReportDailyEntry, MyTimeReportDailyEntryDto> timeReportDailyEntryToMyTimeReportDailyEntryDto,
            IClassMapping<TimeReportProjectAttribute, TimeReportProjectAttributeDto> timeReportProjectAttributeToTimeReportProjectAttributeDto)
        {
            _timeReportProjectAttributeToTimeReportProjectAttributeDto = timeReportProjectAttributeToTimeReportProjectAttributeDto;

            Mapping = e => new MyTimeReportRowDto
            {
                Id = e.Id,
                RequestId = e.RequestId,
                ProjectId = e.ProjectId,
                Apn = e.Project.Apn,
                PetsCode = e.Project.PetsCode,
                KupferwerkProjectId = e.Project.KupferwerkProjectId,
                ProjectName = ProjectBusinessLogic.ProjectName.Call(e.Project),
                ReportingStartsOn = e.Project.ReportingStartsOn,
                TaskName = e.TaskName,
                OvertimeVariant = e.OvertimeVariant,
                AllowedProjectOvertime = e.Project.AllowedOvertimeTypes,
                Status = e.Status,
                Days = e.DailyEntries.Select(timeReportDailyEntryToMyTimeReportDailyEntryDto.CreateFromSource).ToList(),
                ImportSourceId = e.ImportSourceId,
                ImportedTaskCode = e.ImportedTaskCode,
                ImportedTaskType = e.ImportedTaskType,
                AbsenceTypeId = e.AbsenceTypeId,
                Attributes = GetAttributes(e).ToList(),
                IsReadOnly = e.IsReadOnly,
                SelectedAttributesValueIds = e.AttributeValues.OrderBy(a => a.AttributeId).Select(v => v.Id).ToArray()
            };
        }

        private IEnumerable<TimeReportProjectAttributeDto> GetAttributes(TimeReportRow row)
        {
            var attributes = row.Project.Attributes.AsQueryable();

            // For readonly reports, fetch old attributes too
            if (row.Status == TimeReportStatus.Accepted || row.Status == TimeReportStatus.Submitted)
            {
                attributes = attributes.Concat(row.AttributeValues.Select(v => v.Attribute));
            }

            return attributes
                    .DistinctBy(a => a.Id, null)
                    .Select(_timeReportProjectAttributeToTimeReportProjectAttributeDto.CreateFromSource);
        }
    }
}
