﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class EmployeeAbsenceDto
    {
        public long Id { get; set; }

        public long EmployeeId { get; set; }

        public long AbsenceTypeId { get; set; }

        public DateTime From { get; set; }

        public DateTime To { get; set; }

        public decimal Hours { get; set; }

        public long RequestId { get; set; }

        public VacationOperation? VacationBalanceOperation { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
