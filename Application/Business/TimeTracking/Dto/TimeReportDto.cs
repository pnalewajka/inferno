﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class TimeReportDto
    {
        public long Id { get; set; }

        public long EmployeeId { get; set; }

        public string EmployeeFirstName { get; set; }

        public string EmployeeLastName { get; set; }

        public string EmployeeAcronym { get; set; }

        public string EmployeeEmail { get; set; }

        public ProjectStatusDto[] ProjectStatusDtos { get; set; }

        public int Year { get; set; }

        public byte Month { get; set; }

        public decimal ContractedHours { get; set; }

        public decimal ReportedNormalHours { get; set; }

        public decimal NoServiceHours { get; set; }

        public TimeReportStatus Status { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public decimal TotalHours { get; set; }

        public decimal RegularHours { get; set; }

        public decimal OvertimeHours { get; set; }

        public decimal BillableHours { get; set; }

        public decimal NonBillableHours { get; set; }

        public Dictionary<HourlyRateType, decimal> HoursPerOvertimeType { get; set; }

        public bool CanBeReopened { get; set; }

        public ControllingStatus ControllingStatus { get; set; }

        public string ControllingErrorMessage { get; set; }

        public InvoiceNotificationStatus InvoiceNotificationStatus { get; set; }

        public string InvoiceNotificationErrorMessage { get; set; }

        public InvoiceStatus InvoiceStatus { get; set; }

        public InvoiceIssues? InvoiceIssues { get; set; }

        public string InvoiceRejectionReason { get; set; }

        public string InvoiceCalculation { get; set; }

        public DocumentDto InvoiceDocument { get; set; }

        public bool IsReadOnly { get; set; }

        public string ContractTypeName { get; set; }

        public decimal TaxDeductHours { get; set; }

        public string OrgUnitName { get; set; }

        public string LineManagerName { get; set; }

        public long? LineManagerId { get; set; }
    }
}
