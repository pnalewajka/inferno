﻿namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class EmployeeOvertimeInProjectDto
    {
        public long EmployeeId { get; set; }

        public string EmployeeDisplayName { get; set; }

        public long ProjectId { get; set; }

        public string ProjectName { get; set; }

        public decimal OvertimeHours { get; set; }

        public int Year { get; set; }

        public byte Month { get; set; }
    }
}
