﻿using System;
using System.Collections.Generic;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class EmployeeScheduleListDto
    {
        public EmployeeScheduleListDto()
        {
            AbsenceDays = new List<AbsenceDayDto>();
            HomeOfficeDays = new List<DateTime>();
            HolidayDays = new List<DateTime>();
            NotContractedDays = new List<DateTime>();
        }

        public long Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public ICollection<AbsenceDayDto> AbsenceDays { get; set; }

        public ICollection<DateTime> HomeOfficeDays { get; set; }

        public ICollection<DateTime> HolidayDays { get; set; }

        public ICollection<DateTime> NotContractedDays { get; set; }
    }
}
