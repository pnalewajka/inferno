﻿using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class VacationBalanceToVacationBalanceDtoMapping : ClassMapping<VacationBalance, VacationBalanceDto>
    {
        public VacationBalanceToVacationBalanceDtoMapping()
        {
            Mapping = e => new VacationBalanceDto
            {
                Id = e.Id,
                EmployeeId = e.EmployeeId,
                EmployeeFirstName = e.Employee.FirstName,
                EmployeeLastName = e.Employee.LastName,
                EmployeeLogin = e.Employee.User.Login,
                EffectiveOn = e.EffectiveOn,
                RequestId = e.RequestId,
                Operation = e.Operation,
                Comment = e.Comment,
                HourChange = e.HourChange,
                TotalHourBalance = e.TotalHourBalance,
                Timestamp = e.Timestamp
            };
        }
    }
}
