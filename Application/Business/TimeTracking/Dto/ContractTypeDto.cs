﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class ContractTypeDto
    {
        public long Id { get; set; }

        public LocalizedString Name { get; set; }

        public LocalizedString Description { get; set; }

        public EmploymentType? EmploymentType { get; set; }

        public bool IsActive { get; set; }

        public string ActiveDirectoryCode { get; set; }

        public TimeTrackingCompensationCalculators? TimeTrackingCompensationCalculator { get; set; }

        public BusinessTripCompensationCalculators? BusinessTripCompensationCalculator { get; set; }

        public OverTimeTypes AllowedOverTimeTypes { get; set; }

        public bool HasPaidVacation { get; set; }

        public decimal? DailyWorkingLimit { get; set; }

        public bool UseAutomaticOvertimeBankCalculation { get; set; }

        public long[] AbsenceTypeIds { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public bool ShouldNotifyOnAbsenceRemoval { get; set; }

        public decimal MinimunOvertimeBankBalance { get; set; }

        public SettlementSectionsFeatureType SettlementSectionsFeatures { get; set; }

        public bool NotifyInvoiceAmount { get; set; }

        public bool NotifyMissingBusinessTripSettlement { get; set; }

        public bool UseDailySettlementMeals { get; set; }

        public bool HasToSpecifyDestinationType { get; set; }

        public bool IsTaxDeductibleCostEnabled { get; set; }
    }
}
