﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class TimeReportDailyEntryToMyTimeReportDailyEntryDto : ClassMapping<TimeReportDailyEntry, MyTimeReportDailyEntryDto>
    {
        public TimeReportDailyEntryToMyTimeReportDailyEntryDto()
        {
            Mapping = e => new MyTimeReportDailyEntryDto
            {
                Hours = e.Hours,
                Day = e.Day,
                IsReadOnly = e.TimeReportRow.IsReadOnly
                    || (e.TimeReportRow.Project.ReportingStartsOn.HasValue && e.TimeReportRow.Project.ReportingStartsOn.Value > e.Day),
            };
        }
    }
}
