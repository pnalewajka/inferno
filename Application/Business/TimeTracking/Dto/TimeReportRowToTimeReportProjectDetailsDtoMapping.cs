using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Helpers;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class TimeReportRowToTimeReportProjectDetailsDtoMapping : ClassMapping<TimeReportRow, TimeReportProjectDetailsDto>
    {
        public TimeReportRowToTimeReportProjectDetailsDtoMapping()
        {
            Mapping = e => new TimeReportProjectDetailsDto
            {
                EmployeeId = e.TimeReport.EmployeeId,
                EmployeeFirstName = e.TimeReport.Employee.FirstName,
                EmployeeLastName = e.TimeReport.Employee.LastName,
                ProjectId = e.ProjectId,
                OrgUnitId = e.TimeReport.Employee.OrgUnitId,
                TimeReportStatus = e.Status,
                TimeReportId = e.TimeReportId,
                RequestId = e.RequestId,
                RequestStatus = e.Request != null ? e.Request.Status : (RequestStatus?)null,
                IsRequestAvailable = e.Request != null ? IsRequestAvailable(e.Request) : false
            };
        }

        /// <remarks>Don't inline this to prevent caching security restrictions for different users</remarks>
        private static bool IsRequestAvailable(Request request)
        {
            var restriction = EntitySecurityRestrictionsHelper.GetSecurityRestriction<Request>();

            return restriction.GetRestriction().Compile().Invoke(request);
        }
    }
}
