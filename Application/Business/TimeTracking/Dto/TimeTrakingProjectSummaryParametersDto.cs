﻿using System;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class TimeTrakingProjectSummaryParametersDto
    {
        public long ProjectId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}