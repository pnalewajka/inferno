﻿using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class TimeReportProjectAttributeValueToTimeReportProjectAttributeValueDtoMapping : ClassMapping<TimeReportProjectAttributeValue, TimeReportProjectAttributeValueDto>
    {
        public TimeReportProjectAttributeValueToTimeReportProjectAttributeValueDtoMapping()
        {
            Mapping = e => new TimeReportProjectAttributeValueDto
            {
                Id = e.Id,
                IsDeleted = e.IsDeleted,
                AttributeId = e.AttributeId,
                Name = e.Name,
                Description = e.Description,
                Features = e.Features,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
