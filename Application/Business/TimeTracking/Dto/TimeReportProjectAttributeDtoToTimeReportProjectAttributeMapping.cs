﻿using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class TimeReportProjectAttributeDtoToTimeReportProjectAttributeMapping : ClassMapping<TimeReportProjectAttributeDto, TimeReportProjectAttribute>
    {
        public TimeReportProjectAttributeDtoToTimeReportProjectAttributeMapping()
        {
            Mapping = d => new TimeReportProjectAttribute
            {
                Id = d.Id,
                Name = d.Name,
                Description = d.Description,
                DefaultValueId = d.DefaultValueId,
            };
        }
    }
}
