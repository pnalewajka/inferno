﻿namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class TimeTrackingDashboardContext
    {
        public int? Year { get; set; }

        public int? Month { get; set; }

        public bool IsSet => Year.HasValue && Month.HasValue;
    }
}