﻿using System;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class ProjectToTimeTrackingProjectDtoMapping : ClassMapping<Project, TimeTrackingProjectDto>
    {
        public ProjectToTimeTrackingProjectDtoMapping(
            IClassMapping<TimeReportProjectAttribute, TimeReportProjectAttributeDto> timeReportProjectAttributeToTimeReportProjectAttributeDto)
        {
            Mapping = project => new TimeTrackingProjectDto
            {
                Id = project.Id,
                Name = ProjectBusinessLogic.ProjectName.Call(project),
                PetsCode = project.PetsCode,
                ClientName = project.ProjectSetup.ClientShortName,
                ProjectManagerName = project.ProjectSetup.ProjectManager != null ? project.ProjectSetup.ProjectManager.FullName : String.Empty,
                JiraCode = project.JiraKey,
                Attributes = project.Attributes.Select(timeReportProjectAttributeToTimeReportProjectAttributeDto.CreateFromSource).ToArray(),
                Apn = project.Apn,
                KupferwerkProjectId = project.KupferwerkProjectId,
                ReportingStartsOn = project.ReportingStartsOn,
            };
        }
    }
}