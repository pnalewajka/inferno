﻿using System;
namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class OvertimeApprovalDto
    {
        public long Id { get; set; }

        public long EmployeeId { get; set; }

        public long ProjectId { get; set; }

        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }

        public decimal HourLimit { get; set; }

        public long RequestId { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
