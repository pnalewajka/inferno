﻿namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class EmployeesExceedingWorkingLimitParametersDto
    {
        public int Year { get; set; }

        public int Month { get; set; }

        public long[] OrgUnitIds { get; set; }

        public long[] LocationIds { get; set; }

        public long[] ContractTypeIds { get; set; }
    }
}
