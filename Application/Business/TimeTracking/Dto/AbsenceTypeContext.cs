﻿using System;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class AbsenceTypeContext
    {
        public bool CurrentEmployeeOnly { get; set; }

        public DateTime From { get; set; }

        public DateTime To { get; set; }

        public long? OnBehalfOf { get; set; }
    }
}