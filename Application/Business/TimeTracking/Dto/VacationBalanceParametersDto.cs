﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class VacationBalanceParametersDto
    {
        public DateTime EffectiveOn { get; set; }

        public ReportScopeEnum EmployeeScope { get; set; }

        public long[] ContractTypeIds { get; set; }
    }
}
