﻿using System;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class JiraIssueToTimeReportRowMapperDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string SourceCode { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
