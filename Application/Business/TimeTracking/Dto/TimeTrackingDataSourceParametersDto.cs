﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class TimeTrackingDataSourceParametersDto
    {
        public DateTime From { get; set; }

        public DateTime To { get; set; }

        public TimeReportStatus? Status { get; set; }

        public EmploymentType? EmploymentType { get; set; }

        public TimeTrackingProjectType? ProjectType { get; set; }

        public long[] ProjectIds { get; set; }

        public long[] EmployeeIds { get; set; }

        public long[] ProjectOrgUnitIds { get; set; }

        public long[] ProjectTagsIds { get; set; }

        public long[] EmployeeOrgUnitIds { get; set; }
        
        public long[] HourlyRateTypes { get; set; }

        public long[] LocationIds { get; set; }

        public long[] ContractTypeIds { get; set; }

        public long[] ProjectClientIds { get; set; }

        public bool MyEmployees { get; set; }
    }
}