﻿using System;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class EmployeeAbsenceHoursInfoDto
    {
        public long EmployeeId { get; set; }

        public DateTime? EffectiveOn { get; set; }

        public decimal VacationHours { get; set; }

        public decimal OvertimeBankHours { get; set; }
    }
}
