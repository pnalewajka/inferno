﻿using System;
namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class OvertimeBankDto
    {
        public long Id { get; set; }

        public long EmployeeId { get; set; }

        public string EmployeeFirstName { get; set; }

        public string EmployeeLastName { get; set; }

        public long? RequestId { get; set; }

        public decimal OvertimeChange { get; set; }

        public DateTime EffectiveOn { get; set; }

        public decimal OvertimeBalance { get; set; }

        public string Comment { get; set; }

        public bool IsAutomatic { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
