﻿using System.Linq;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.SkillManagement;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class CalendarAbsenceDtoToCalendarAbsenceMapping : ClassMapping<CalendarAbsenceDto, CalendarAbsence>
    {
        public CalendarAbsenceDtoToCalendarAbsenceMapping()
        {
            Mapping = d => new CalendarAbsence
            {
                Id = d.Id,
                Name = d.Name,
                Description = d.Description,
                OfficeGroupEmail = d.OfficeGroupEmail,
                Timestamp = d.Timestamp,
                EmployeesIds = d.EmployeesIds != null ? d.EmployeesIds.Select(id => new Employee() { Id = id }).ToList() : null,
            };
        }
    }
}
