﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class AbsencesDataSourceParametersDto : AbsencesDataSourceParametersBaseDto
    {
        public DateTime ApprovedFrom { get; set; }

        public DateTime ApprovedTo { get; set; }
    }
}
