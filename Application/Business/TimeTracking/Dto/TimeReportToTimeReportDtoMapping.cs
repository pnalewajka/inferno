﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.TimeTracking.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;

using TTProjectBusinessLogic = Smt.Atomic.Business.TimeTracking.BusinessLogics.ProjectBusinessLogic;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class TimeReportToTimeReportDtoMapping : ClassMapping<TimeReport, TimeReportDto>
    {
        public TimeReportToTimeReportDtoMapping()
        {
            Mapping = e => new TimeReportDto
            {
                Id = e.Id,
                EmployeeId = e.EmployeeId,
                EmployeeFirstName = e.Employee.FirstName,
                EmployeeLastName = e.Employee.LastName,
                EmployeeAcronym = e.Employee.Acronym,
                EmployeeEmail = e.Employee.Email,
                ProjectStatusDtos = CreateProjectStatusDtos(e.Rows).ToArray(),
                Year = e.Year,
                Month = e.Month,
                ContractedHours = e.ContractedHours,
                ReportedNormalHours = e.ReportedNormalHours,
                NoServiceHours = e.NoServiceHours,
                Status = e.Status,
                ControllingStatus = e.ControllingStatus,
                ControllingErrorMessage = e.ControllingErrorMessage,
                InvoiceNotificationStatus = e.InvoiceNotificationStatus,
                InvoiceNotificationErrorMessage = e.InvoiceNotificationErrorMessage,
                InvoiceStatus = e.InvoiceStatus,
                InvoiceIssues = e.InvoiceIssues,
                InvoiceRejectionReason = e.InvoiceRejectionReason,
                InvoiceCalculation = e.Employee.InvoiceCalculations
                    .Where(r => r.Month == e.Month && r.Year == e.Year && r.IsValid)
                    .Select(r => r.CalculationResult)
                    .SingleOrDefault(),
                InvoiceDocument = e.InvoiceDocument == null ? null : new DocumentDto
                {
                    ContentType = e.InvoiceDocument.ContentType,
                    DocumentName = e.InvoiceDocument.Name,
                    DocumentId = e.InvoiceDocument.Id
                },
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp,
                TotalHours = e.Rows.SelectMany(de => de.DailyEntries).Sum(de => de.Hours),
                RegularHours = e.Rows.Where(r => r.OvertimeVariant == HourlyRateType.Regular).SelectMany(de => de.DailyEntries).Sum(de => de.Hours),
                OvertimeHours = e.Rows.Where(r => r.OvertimeVariant != HourlyRateType.Regular).SelectMany(de => de.DailyEntries).Sum(de => de.Hours),
                BillableHours = e.Rows.Where(r => r.AttributeValues.All(v => !v.Features.HasFlag(AttributeValueFeature.NonBillable))).SelectMany(de => de.DailyEntries).Sum(de => de.Hours),
                NonBillableHours = e.Rows.Where(r => r.AttributeValues.Any(v => v.Features.HasFlag(AttributeValueFeature.NonBillable))).SelectMany(de => de.DailyEntries).Sum(de => de.Hours),
                HoursPerOvertimeType = e.Rows.Where(r => r.OvertimeVariant != HourlyRateType.Regular).GroupBy(r => r.OvertimeVariant).ToDictionary(v => v.Key, v => v.SelectMany(de => de.DailyEntries).Sum(de => de.Hours)),
                TaxDeductHours = e.Employee.User.TaxDeductibleCostRequests != null
                    ? e.Employee.User.TaxDeductibleCostRequests.Where(r => r.Year == e.Year && r.Month == e.Month).Sum(r => r.Hours)
                    : default(decimal),
                CanBeReopened = TimeReportService.ReopenableStatuses.Contains(e.Status),
                IsReadOnly = e.IsReadOnly,
                ContractTypeName = e.Employee.GetCurrentEmploymentPeriod() != null ? e.Employee.GetCurrentEmploymentPeriod().ContractType.NameEn : default(string),
                OrgUnitName = e.Employee.OrgUnit.Name,
                LineManagerName = e.Employee.LineManager != null ? e.Employee.LineManager.FullName : string.Empty,
                LineManagerId = e.Employee.LineManagerId
            };
        }

        private IEnumerable<ProjectStatusDto> CreateProjectStatusDtos(ICollection<TimeReportRow> rows)
        {
            return rows
                .GroupBy(r => r.ProjectId)
                .Select(g => new ProjectStatusDto
                {
                    ProjectId = g.Key,
                    Name = ProjectBusinessLogic.ClientProjectName.Call(g.First().Project),
                    Status = TTProjectBusinessLogic.GetProjectStatus.Call(g.AsQueryable()),
                });
        }
    }
}