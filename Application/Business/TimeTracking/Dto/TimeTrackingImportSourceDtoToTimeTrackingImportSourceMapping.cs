﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class TimeTrackingImportSourceDtoToTimeTrackingImportSourceMapping : ClassMapping<TimeTrackingImportSourceDto, TimeTrackingImportSource>
    {
        public TimeTrackingImportSourceDtoToTimeTrackingImportSourceMapping()
        {
            Mapping =
                d =>
                    new TimeTrackingImportSource
                    {
                        Id = d.Id,
                        Code = d.Code,
                        NameEn = d.Name.English,
                        NamePl = d.Name.Polish,
                        ImportStrategyIdentifier = d.ImportStrategyIdentifier,
                        Configuration = d.Configuration,
                        Timestamp = d.Timestamp
                    };
        }
    }
}
