﻿using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class OvertimeApprovalDtoToOvertimeApprovalMapping : ClassMapping<OvertimeApprovalDto, OvertimeApproval>
    {
        public OvertimeApprovalDtoToOvertimeApprovalMapping()
        {
            Mapping = d => new OvertimeApproval
            {
                Id = d.Id,
                EmployeeId = d.EmployeeId,
                ProjectId = d.ProjectId,
                DateFrom = d.DateFrom,
                DateTo = d.DateTo,
                HourLimit = d.HourLimit,
                RequestId = d.RequestId,
                Timestamp = d.Timestamp
            };
        }
    }
}
