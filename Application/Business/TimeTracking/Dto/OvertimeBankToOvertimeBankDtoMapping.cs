﻿using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class OvertimeBankToOvertimeBankDtoMapping : ClassMapping<OvertimeBank, OvertimeBankDto>
    {
        public OvertimeBankToOvertimeBankDtoMapping()
        {
            Mapping = e => new OvertimeBankDto
            {
                Id = e.Id,
                EmployeeId = e.EmployeeId,
                EmployeeFirstName = e.Employee == null ? "": e.Employee.FirstName,
                EmployeeLastName = e.Employee == null ? "" : e.Employee.LastName,
                RequestId = e.RequestId,
                IsAutomatic = e.IsAutomatic,
                OvertimeChange = e.OvertimeChange,
                EffectiveOn = e.EffectiveOn,
                OvertimeBalance = e.OvertimeBalance,
                Comment = e.Comment,
                Timestamp = e.Timestamp                
            };
        }
    }
}
