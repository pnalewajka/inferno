﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;

namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class MyTimeReportRowDtoToTimeReportRowMapping : ClassMapping<MyTimeReportRowDto, TimeReportRow>
    {
        public MyTimeReportRowDtoToTimeReportRowMapping()
        {
            Mapping = r => new TimeReportRow
            {
                Id = r.Id,
                RequestId = r.RequestId,
                ProjectId = r.ProjectId,
                TaskName = r.TaskName,
                OvertimeVariant = r.OvertimeVariant,
                Status = r.Status,
                DailyEntries = r.Days.Where(d => d.Hours > 0).Select(d => new TimeReportDailyEntry
                {
                    Hours = d.Hours,
                    Day = d.Day
                }).ToList(),
                ImportSourceId = r.ImportSourceId,
                ImportedTaskCode = r.ImportedTaskCode,
                ImportedTaskType = r.ImportedTaskType,
                AbsenceTypeId = r.AbsenceTypeId,
                AttributeValues = r.SelectedAttributesValueIds != null
                    ? r.SelectedAttributesValueIds.Select(a => new TimeReportProjectAttributeValue
                    {
                        Id = a
                    }).ToList()
                    : new List<TimeReportProjectAttributeValue>(),
            };
        }
    }
}
