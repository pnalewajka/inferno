﻿namespace Smt.Atomic.Business.TimeTracking.Dto
{
    public class TimeReportReopenedEmailDto
    {
        public string TimeReportCode { get; set; }

        public string TimeReportUserDisplayName { get; set; }

        public string RequestApproverDisplayName { get; set; }
    }
}
