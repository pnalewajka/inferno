﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Allocation.Filters;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Compensation.BusinessLogics;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.TimeTracking.Consts;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    public class TimeReportCardIndexDataService : CardIndexDataService<TimeReportDto, TimeReport, ITimeTrackingDbScope>, ITimeReportCardIndexDataService
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly IOrgUnitService _orgUnitService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public TimeReportCardIndexDataService(
            ICardIndexServiceDependencies<ITimeTrackingDbScope> dependencies,
            IPrincipalProvider principalProvider,
            IOrgUnitService orgUnitService)
            : base(dependencies)
        {
            _principalProvider = principalProvider;
            _orgUnitService = orgUnitService;
        }

        protected override IEnumerable<Expression<Func<TimeReport, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return r => r.Employee.FirstName;
            yield return r => r.Employee.LastName;
        }

        protected override IQueryable<TimeReport> ConfigureIncludes(IQueryable<TimeReport> sourceQueryable)
        {
            //temporarily, currently Atomic cannot correctly generate includes for this case due to sums
            return sourceQueryable
                .Include(e => e.Rows)
                .Include(e => e.Rows.Select(r => r.DailyEntries))
                .Include(e => e.Rows.Select(r => r.Project));
        }

        protected override NamedFilters<TimeReport> NamedFilters
        {
            get
            {
                var employeeId = _principalProvider.Current.EmployeeId;

                return new NamedFilters<TimeReport>(new List<NamedFilter<TimeReport>>
                {
                    MyEmployeesFilter.GetDescendantEmployeeFilter<TimeReport>(
                        FilterCodes.MyEmployees, t => t.Employee, _orgUnitService, _principalProvider,  SecurityRoleType.CanViewOthersTimeReport),
                    MyEmployeesFilter.GetChildEmployeeFilter<TimeReport>(
                        FilterCodes.MyDirectEmployees, t => t.Employee, _orgUnitService, _principalProvider, SecurityRoleType.CanViewOthersTimeReport),
                    CreateMyProjectsFilter(FilterCodes.MyProjects, employeeId),
                    new NamedFilter<TimeReport>(FilterCodes.MyTimeReports, r => r.EmployeeId == employeeId),
                    new NamedFilter<TimeReport>(FilterCodes.AllEmployeesTimeReports, r => true, SecurityRoleType.CanViewOthersTimeReport),
                    new NamedFilter<TimeReport>(FilterCodes.InProgressReports, r => r.ReportedNormalHours < r.ContractedHours || r.ContractedHours == 0),
                    new NamedFilter<TimeReport>(FilterCodes.CompletedReports, r => r.ReportedNormalHours >= r.ContractedHours && r.ContractedHours > 0),
                    new NamedFilter<TimeReport, long[]>(FilterCodes.OrgUnitsFilter, (r, orgUnitIds) => orgUnitIds.Any(id => r.Employee.OrgUnit.Path.Contains("/" + id + "/"))),
                    new NamedFilter<TimeReport, long[]>(FilterCodes.CompanyFilter, (r, companyIds) => r.Employee.CompanyId.HasValue && companyIds.Contains(r.Employee.CompanyId.Value)),
                    ContractTypeFilter.GetTimeReportFilter(FilterCodes.ContractTypeFilter),
                }
                .Union(CardIndexServiceHelper.BuildEnumBasedFilters<TimeReport, ControllingStatus>(e => e.ControllingStatus))
                .Union(CardIndexServiceHelper.BuildEnumBasedFilters<TimeReport, InvoiceNotificationStatus>(e => e.InvoiceNotificationStatus))
                .Union(CardIndexServiceHelper.BuildEnumBasedFilters<TimeReport, TimeReportStatus>(e => e.Status))
                .Union(CardIndexServiceHelper.BuildEnumBasedFilters<TimeReport, InvoiceStatus>(e => e.InvoiceStatus)));
            }
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<TimeReport> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var direction = sortingCriterion.GetSortingDirection();

            if (sortingCriterion.GetSortingColumnName() == "Progress")
            {
                orderedQueryBuilder.ApplySortingKey(r => r.ContractedHours != 0M ? r.ReportedNormalHours / r.ContractedHours : 0M, direction);
            }
            else
            {
                base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
            }
        }

        private NamedFilter<TimeReport> CreateMyProjectsFilter(string code, long currentEmployeeId)
        {
            var managerOrgUnitsIds = _orgUnitService.GetManagerOrgUnitDescendantIds(currentEmployeeId);

            return new NamedFilter<TimeReport>(code, r => r.Rows.Any(row =>
                row.Project.ProjectSetup.ProjectManagerId == currentEmployeeId
                || managerOrgUnitsIds.Contains(row.Project.ProjectSetup.ProjectManager.OrgUnitId)), SecurityRoleType.CanViewOthersTimeReport);
        }

        public override IQueryable<TimeReport> ApplyContextFiltering(IQueryable<TimeReport> records, object context)
        {
            const string invoiceView = "invoice";

            var timeReportContext = context as TimeReportContext;

            if (timeReportContext == null || timeReportContext.TimeReportMonth == 0 || timeReportContext.TimeReportYear == 0)
            {
                return base.ApplyContextFiltering(records, context);
            }

            if (timeReportContext.View == invoiceView)
            {
                records = records.Where(r => !TimeReportInvoiceBusinessLogic.IsNotApplicable.Call(r));
            }

            return records.Where(x => x.Month == timeReportContext.TimeReportMonth &&
                                      x.Year == timeReportContext.TimeReportYear);
        }
    }
}