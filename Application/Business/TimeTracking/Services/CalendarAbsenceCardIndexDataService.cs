﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.TimeTracking.Consts;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Helpers;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Exchange.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    public class CalendarAbsenceCardIndexDataService : CardIndexDataService<CalendarAbsenceDto, CalendarAbsence, ITimeTrackingDbScope>, ICalendarAbsenceCardIndexDataService
    {
        private readonly ICalendarOfficeService _calendarOfficeService;
        private readonly IPrincipalProvider _principalProvider;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public CalendarAbsenceCardIndexDataService(
            ICardIndexServiceDependencies<ITimeTrackingDbScope> dependencies,
            ICalendarOfficeService calendarOfficeService,
            IPrincipalProvider principalProvider)
            : base(dependencies)
        {
            _calendarOfficeService = calendarOfficeService;
            _principalProvider = principalProvider;
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<CalendarAbsence, CalendarAbsenceDto, ITimeTrackingDbScope> eventArgs)
        {
            eventArgs.InputEntity.EmployeesIds = EntityMergingService.CreateEntitiesFromIds<Employee, ITimeTrackingDbScope>(eventArgs.UnitOfWork, eventArgs.InputEntity.EmployeesIds.GetIds());

            var availabilityResult = _calendarOfficeService.IsCalendarAvailable(eventArgs.InputEntity.OfficeGroupEmail);

            eventArgs.Canceled |= !availabilityResult.IsSuccessful;
            eventArgs.Alerts.AddRange(availabilityResult.Alerts);
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<CalendarAbsence, CalendarAbsenceDto, ITimeTrackingDbScope> eventArgs)
        {
            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.EmployeesIds, eventArgs.InputEntity.EmployeesIds);
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<CalendarAbsence> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            if (sortingCriterion.GetSortingColumnName() == nameof(CalendarAbsence.EmployeesIds))
            {
                CalendarAbsenceSortHelper.OrderBySkillTags(orderedQueryBuilder, sortingCriterion.Ascending);
            }
            else
            {
                base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
            }
        }

        protected override NamedFilters<CalendarAbsence> NamedFilters
        {
            get
            {
                long? currentUserId = _principalProvider.Current.Id;

                return new NamedFilters<CalendarAbsence>(new []
                {
                    new NamedFilter<CalendarAbsence>(FilterCodes.MyAbsenceCalendars, a => a.CreatedById == currentUserId), 
                    new NamedFilter<CalendarAbsence>(FilterCodes.AllAbsenceCalendars, _ => true, SecurityRoleType.CanViewAllCalendarAbsence) 
                });
            }
        }
    }
}