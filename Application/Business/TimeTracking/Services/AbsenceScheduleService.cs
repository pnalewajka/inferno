﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using EmployeeAbsence = Smt.Atomic.Data.Entities.Modules.TimeTracking.EmployeeAbsence;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    internal class AbsenceScheduleService : IAbsenceScheduleService
    {
        private IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService;
        private IDateRangeService _dateRangeSercice;
        private IDanteCalendarService _danteCalendarService;
        private ICalendarService _calendarService;
        private ITimeService _timeService;
        private IEmploymentPeriodService _employmentPeriodService;

        public AbsenceScheduleService(
            IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
            IDateRangeService dateRangeSercice,
            IDanteCalendarService danteCalendarService,
            ITimeService timeService,
            ICalendarService calendarService,
            IEmploymentPeriodService employmentPeriodService
            )
        {
            _dateRangeSercice = dateRangeSercice;
            _unitOfWorkService = unitOfWorkService;
            _danteCalendarService = danteCalendarService;
            _calendarService = calendarService;
            _timeService = timeService;
            _employmentPeriodService = employmentPeriodService;
        }

        public ScheduleListDto CalculateSchedule(IReadOnlyCollection<string> employeeAcronymes, IReadOnlyCollection<long> projectIds, DateTime periodStartDate, DateTime periodEndDate)
        {
            var result = new ScheduleListDto();

            result.PeriodStartDate = DateHelper.BeginningOfMonth(periodStartDate);
            result.PeriodEndDate = DateHelper.EndOfMonth(periodEndDate);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var projectEmployeeIds = projectIds.Any()
                    ? unitOfWork.Repositories.AllocationRequests
                        .Where(a => projectIds.Contains(a.ProjectId))
                        .Where(DateRangeHelper.OverlapingDateRange<AllocationRequest>(a => a.StartDate, a => a.EndDate ?? result.PeriodEndDate, result.PeriodStartDate, result.PeriodEndDate))
                        .Select(a => a.EmployeeId)
                        .ToList()
                    : new List<long>();

                var employees = unitOfWork.Repositories.Employees
                    .Where(e => employeeAcronymes.Contains(e.Acronym) || projectEmployeeIds.Contains(e.Id)).ToList();

                var employeeIds = employees.Select(e => e.Id).ToList();
                var employeeUserIds = employees.Select(e => e.UserId).ToList();
                var orgUnitIds = employees.Select(e => e.OrgUnitId).Distinct();

                var homeOffices = unitOfWork.Repositories.HomeOfficeRequests
                    .Where(h => employeeUserIds.Contains(h.Request.RequestingUserId))
                    .Where(DateRangeHelper.OverlapingDateRange<AddHomeOfficeRequest>(a => a.From, a => a.To, result.PeriodStartDate, result.PeriodEndDate))
                    .GroupBy(a => a.Request.RequestingUserId)
                    .ToDictionary(a => a.Key);

                var absences = unitOfWork.Repositories.EmployeeAbsences
                    .Where(a => employeeIds.Contains(a.EmployeeId))
                    .Where(DateRangeHelper.OverlapingDateRange<EmployeeAbsence>(a => a.From, a => a.To, result.PeriodStartDate, result.PeriodEndDate))
                    .GroupBy(r => r.EmployeeId)
                    .ToDictionary(a => a.Key);

                var holidaysDictionary = new Dictionary<long, ICollection<DateTime>>();

                foreach (var orgUnitId in orgUnitIds)
                {
                    var calendarId = _danteCalendarService.GetCalendarIdByOrgUnitId(orgUnitId);
                    var holidays = _calendarService.GetHolidays(result.PeriodStartDate, result.PeriodEndDate, calendarId);

                    holidaysDictionary.Add(orgUnitId, holidays);
                }

                foreach (var employee in employees)
                {
                    var employeeRowDto = new EmployeeScheduleListDto();

                    employeeRowDto.Id = employee.Id;
                    employeeRowDto.FirstName = employee.FirstName;
                    employeeRowDto.LastName = employee.LastName;

                    employeeRowDto.HomeOfficeDays = GetHomeOffice(homeOffices, employee);

                    employeeRowDto.HolidayDays = GetHolidays(holidaysDictionary, employee, result);

                    if (absences.ContainsKey(employee.Id))
                    {
                        employeeRowDto.AbsenceDays = absences[employee.Id].SelectMany(d =>
                        {
                            var dates = new List<DateTime> { d.From, d.To };

                            dates.AddRange(_dateRangeSercice.GetDaysInRange(d.From, d.To));

                            var absenceDays = dates
                                    .Where(date => !date.IsWeekend() && !employeeRowDto.HolidayDays.Contains(date))
                                    .Distinct();

                            var absenceHourPerDay = d.Hours / absenceDays.Count();

                            return absenceDays.Select(date => new AbsenceDayDto { Date = date, Hours = absenceHourPerDay });
                        }).ToList();
                    }

                    AddNotContracted(employee.Id, result.PeriodStartDate, result.PeriodEndDate, employeeRowDto);

                    result.Employees.Add(employeeRowDto);
                }
            }

            return result;
        }

        private void AddNotContracted(long employeeId, DateTime startDate, DateTime endDate, EmployeeScheduleListDto employeeScheduleListDto)
        {
            var workingDays = _employmentPeriodService
                .GetContractedHours(employeeId, startDate, endDate)
                .Where(h => h.Hours > 0)
                .Select(h => h.Date)
                .ToHashSet();

            for (var date = startDate; date <= endDate; date = date.AddDays(1))
            {
                if (!workingDays.Contains(date))
                {
                    employeeScheduleListDto.NotContractedDays.Add(date);
                }
            }
        }

        private ICollection<DateTime> GetHolidays(Dictionary<long, ICollection<DateTime>> holidaysDictionary,
            Employee employee,
            ScheduleListDto result)
        {
            var currentEmployeeEmploymentPeriod = employee
                .EmploymentPeriods
                .Where(x => x.CalendarId.HasValue)
                .OrderByDescending(x => x.EndDate)
                .FirstOrDefault(x => ((!x.EndDate.HasValue) || x.EndDate >= DateTime.Now) && x.StartDate <= DateTime.Now);

            if (currentEmployeeEmploymentPeriod == null)
            {
                return holidaysDictionary.ContainsKey(employee.OrgUnitId)
                    ? holidaysDictionary[employee.OrgUnitId]
                    : new List<DateTime>();
            }

            return _calendarService.GetHolidays(
                result.PeriodStartDate,
                result.PeriodEndDate,
                currentEmployeeEmploymentPeriod.CalendarId.Value);
        }

        private List<DateTime> GetHomeOffice(Dictionary<long, IGrouping<long, AddHomeOfficeRequest>> homeOffices, Employee employee)
        {
            List<DateTime> employeeHomeOffice = new List<DateTime>();

            if (homeOffices.ContainsKey(employee.UserId.Value))
            {
                employeeHomeOffice = homeOffices[employee.UserId.Value].SelectMany(d =>
                 {
                     var dates = new List<DateTime> { d.From, d.To };

                     dates.AddRange(_dateRangeSercice.GetDaysInRange(d.From, d.To));

                     return dates.Distinct();
                 }).ToList();
            }

            return employeeHomeOffice;
        }

        private List<DateTime> GetAbsenceDays(Dictionary<long, IReadOnlyCollection<EmployeeAbsence>> absences, Employee employee)
        {
            var employeeAbsenceDays = new List<DateTime>();

            if (absences.ContainsKey(employee.Id))
            {
                employeeAbsenceDays = absences[employee.Id].SelectMany(d =>
                 {
                     var dates = new List<DateTime> { d.From, d.To };

                     dates.AddRange(_dateRangeSercice.GetDaysInRange(d.From, d.To));

                     return dates.Distinct();
                 }).ToList();
            }

            return employeeAbsenceDays;
        }
    }
}
