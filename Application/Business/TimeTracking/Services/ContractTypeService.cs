﻿using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    public class ContractTypeService : IContractTypeService
    {
        private readonly ITimeService _timeService;
        private readonly IClassMapping<ContractType, ContractTypeDto> _contractTypeToContractTypeDtoMapping;
        private readonly IUnitOfWorkService<IAllocationDbScope> _unitOfWorkService;

        public ContractTypeService(
            ITimeService timeService,
            IClassMapping<ContractType, ContractTypeDto> contractTypeToContractTypeDtoMapping,
            IUnitOfWorkService<IAllocationDbScope> unitOfWorkService)
        {
            _timeService = timeService;
            _contractTypeToContractTypeDtoMapping = contractTypeToContractTypeDtoMapping;
            _unitOfWorkService = unitOfWorkService;
        }

        [Cached(CacheArea = CacheAreas.ContractTypes, ExpirationInSeconds = 600)]
        public ContractTypeDto GetContractTypeOrDefaultById(long id)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var contractType = unitOfWork.Repositories.ContractTypes.GetByIdOrDefault(id);

                return contractType != null ? _contractTypeToContractTypeDtoMapping.CreateFromSource(contractType) : null;
            }
        }

        [Cached(CacheArea = CacheAreas.ContractTypes, ExpirationInSeconds = 600)]
        public ContractTypeDto GetCurrentContractTypeOrDefaultByEmployeeId(long employeeId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var currentDate = _timeService.GetCurrentDate();
                var contractType = unitOfWork.Repositories.EmploymentPeriods
                    .SingleOrDefault(EmploymentPeriodBusinessLogic.OfEmployeeInDate.Parametrize(employeeId, currentDate)).ContractType;

                return contractType != null ? _contractTypeToContractTypeDtoMapping.CreateFromSource(contractType) : null;
            }
        }

        [Cached(CacheArea = CacheAreas.ContractTypes, ExpirationInSeconds = 600)]
        public ContractTypeDto GetLatestContractTypeOrDefaultByEmployeeId(long employeeId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var contractType = unitOfWork.Repositories.EmploymentPeriods
                    .Where(p => p.EmployeeId == employeeId)
                    .OrderByDescending(p => p.StartDate)
                    .Select(p => p.ContractType)
                    .FirstOrDefault();

                return contractType != null ? _contractTypeToContractTypeDtoMapping.CreateFromSource(contractType) : null;
            }
        }
    }
}
