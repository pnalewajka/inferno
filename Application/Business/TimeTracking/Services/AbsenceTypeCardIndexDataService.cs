﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Accounts.BusinessLogics;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Business.TimeTracking.Resources;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    public class AbsenceTypeCardIndexDataService :
        CardIndexDataService<AbsenceTypeDto, AbsenceType, ITimeTrackingDbScope>, IAbsenceTypeCardIndexDataService
    {
        private readonly IEmploymentPeriodService _employmentPeriodService;
        private readonly ICardIndexServiceDependencies<ITimeTrackingDbScope> _dependencies;
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService;
        private readonly IUserBusinessLogic _userBusinessLogic;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public AbsenceTypeCardIndexDataService(ICardIndexServiceDependencies<ITimeTrackingDbScope> dependencies,
            IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
            IUserBusinessLogic userBusinessLogic,
            IEmploymentPeriodService employmentPeriodService
            )
            : base(dependencies)
        {
            _dependencies = dependencies;
            _unitOfWorkService = unitOfWorkService;
            _userBusinessLogic = userBusinessLogic;
            _employmentPeriodService = employmentPeriodService;

            RelatedCacheAreas = new[] { CacheAreas.Absences };
        }

        protected override IEnumerable<Expression<Func<AbsenceType, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return a => a.NameEn;
            yield return a => a.NamePl;
            yield return a => a.DescriptionEn;
            yield return a => a.DescriptionPl;
        }

        public IEnumerable<long> AllowedAbsenceTypes(DateTime from, DateTime to, long? onBehalfOf)
        {
            var currentUserId = _dependencies.PrincipalProvider.Current.Id.Value;
            var canEditOnBehalfExpression = _userBusinessLogic.CanChangeUserDataOnBehalf(onBehalfOf ?? currentUserId);

            if (from > to)
            {
                return Enumerable.Empty<long>();
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var canEditOnBehalfEmployee =
                    new BusinessLogic<Employee, bool>(e => canEditOnBehalfExpression.Call(e.User));
                var employee = unitOfWork.Repositories.Employees.FirstOrDefault(canEditOnBehalfEmployee.AsExpression());

                if (employee == null)
                {
                    throw new BusinessException($"User with Id={currentUserId} can't get absence types on behalf of {onBehalfOf}");
                }
                
                var employmentPeriods = _employmentPeriodService.GetEmploymentPeriodsInDateRange(employee.Id, from, to);

                var allowedContractTypeIds = employmentPeriods.Select(ep => ep.ContractTypeId).ToList();

                var contractAbsenceGroups = unitOfWork.Repositories.ContractTypes
                    .GetByIds(allowedContractTypeIds)
                    .ToDictionary(ct => ct.Id, ct => ct.AbsenceTypes.Select(at => at.Id).ToList());

                IList<long> sharedAbseceTypes = null;

                foreach (var contractAbsenceGroup in contractAbsenceGroups)
                {
                    sharedAbseceTypes = sharedAbseceTypes?.Intersect(contractAbsenceGroup.Value).ToList() ?? contractAbsenceGroup.Value;
                }

                return sharedAbseceTypes ?? new List<long>();
            }
        }

        public override IQueryable<AbsenceType> ApplyContextFiltering(IQueryable<AbsenceType> records, object context)
        {
            var absenceTypeContext = context as AbsenceTypeContext;

            if (absenceTypeContext == null || !absenceTypeContext.CurrentEmployeeOnly)
            {
                return records;
            }

            var sharedAbseceTypes = AllowedAbsenceTypes(absenceTypeContext.From, absenceTypeContext.To, absenceTypeContext.OnBehalfOf).ToList();

            return records.GetByIds(sharedAbseceTypes);
        }

        protected override IOrderedQueryable<AbsenceType> GetDefaultOrderBy(IQueryable<AbsenceType> records, QueryCriteria criteria)
        {
            return records.OrderBy(x => x.Order);
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<AbsenceType, ITimeTrackingDbScope> eventArgs)
        {
            if (eventArgs.DeletingRecord.UsedInContractTypes.Any())
            {
                eventArgs.Canceled = true;
                eventArgs.AddError(AbsenceTypeResources.ErrorUsedInContractTypes);

                return;
            }

            if (eventArgs.DeletingRecord.UsedInTimeReportRow.Any())
            {
                eventArgs.Canceled = true;
                eventArgs.AddError(AbsenceTypeResources.ErrorUsedInTimeReport);

                return;
            }

            if (eventArgs.DeletingRecord.UsedInAbsenceRequest.Any())
            {
                eventArgs.Canceled = true;
                eventArgs.AddError(AbsenceTypeResources.ErrorUsedInAbsenceRequest);

                return;
            }

            base.OnRecordDeleting(eventArgs);
        }
    }
}