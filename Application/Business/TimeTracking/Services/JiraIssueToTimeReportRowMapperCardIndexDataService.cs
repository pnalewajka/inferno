﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    public class JiraIssueToTimeReportRowMapperCardIndexDataService : CardIndexDataService<JiraIssueToTimeReportRowMapperDto, JiraIssueToTimeReportRowMapper, ITimeTrackingDbScope>, IJiraIssueToTimeReportRowMapperCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public JiraIssueToTimeReportRowMapperCardIndexDataService(ICardIndexServiceDependencies<ITimeTrackingDbScope> dependencies)
            : base(dependencies)
        {
        }
    }
}
