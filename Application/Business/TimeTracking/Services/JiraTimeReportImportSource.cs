﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Jira.Entities;
using Smt.Atomic.Data.Jira.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.CrossCutting.Business.Helpers;
using Smt.Atomic.Business.Compensation.Interfaces;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    [Identifier("ImportSource.Jira")]
    public class JiraTimeReportImportSource : ITimeReportImportSource
    {
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService; 
        private readonly IJiraService _jiraService;
        private readonly IEmployeeService _employeeService;
        private readonly IProjectService _projectService;
        private readonly ITimeReportInvoiceService _timeReportInvoiceService;
        private readonly string _configuration;

        public JiraTimeReportImportSource(
            IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
            ITimeReportInvoiceService timeReportInvoiceService,
            IEmployeeService employeeService,
            IProjectService projectService,
            IJiraService jiraService,
            string configuration)
        {
            _unitOfWorkService = unitOfWorkService;
            _timeReportInvoiceService = timeReportInvoiceService;
            _employeeService = employeeService;
            _projectService = projectService;
            _jiraService = jiraService;
            _configuration = configuration;
        }

        public MyTimeReportDto GetTimeReport(long employeeId, int year, byte month)
        {
            var jiraUsername = _employeeService.GetUserLogin(employeeId);

            var from = new DateTime(year, month, 1).Date;
            var to = DateHelper.EndOfMonth(from);

            IList<IssueWithWorklogs> issuesWithWorklogs;

            using (var jiraContext = CreateJiraContext())
            {
                issuesWithWorklogs = jiraContext.GetIssuesWithWorklogs(jiraUsername, from, to);
            }

            var timeReportDto = new MyTimeReportDto
            {
                EmployeeId = employeeId,
                Year = year,
                Month = month,
                Rows = CreateTimeReportRows(issuesWithWorklogs),
                InvoiceStatus = _timeReportInvoiceService.GetInitialInvoiceStatusForEmployee(employeeId, year, month)
            };

            return timeReportDto;
        }

        private IJiraContextWrapper CreateJiraContext()
        {
            if (string.IsNullOrEmpty(_configuration))
            {
                return _jiraService.CreateReadOnly();
            }

            var configuration = JsonHelper.Deserialize<JiraConfiguration>(_configuration);

            return _jiraService.Create(configuration.ServerAddress, configuration.Username, configuration.Password);
        }

        private IList<MyTimeReportRowDto> CreateTimeReportRows(IEnumerable<IssueWithWorklogs> issues)
        {
            var groupedIssuesByIssueToTaskMapping = issues.Select(i => new JiraIssueDto(i)
            {
                Project = _projectService.GetProjectByJiraKeyOrDefault(i.Fields.Project.Key)
            }).Where(i => i.Project != null).GroupBy(i => i.Project.JiraIssueToTimeReportRowMapperId).ToList();

            IList<MyTimeReportRowDto> rows;

            using (var jiraContext = CreateJiraContext())
            {
                rows = groupedIssuesByIssueToTaskMapping
                    .Select(g => GetIssueToTimeReportRowMapping(g.Key, jiraContext).CreateFromSource(g))
                    .Aggregate(Enumerable.Empty<MyTimeReportRowDto>(), (s, e) => s.Concat(e)).ToList();
            }

            // Remove not allowed characters
            rows.ForEach(r =>
            {
                r.TaskName = TimeReportRowHelper.RemoveNotAllowedCharacters(r.TaskName);
                r.ImportedTaskType = TimeReportRowHelper.RemoveNotAllowedCharacters(r.ImportedTaskType);
            });

            return rows;
        }

        private JiraIssueToTimeReportRowMappingBase GetIssueToTimeReportRowMapping(long issueToTaskMappingId, IJiraContextWrapper jiraContext)
        {
            string methodBody;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                methodBody = unitOfWork.Repositories.JiraIssueToTimeReportRowMappers.GetById(issueToTaskMappingId).SourceCode;
            }

            var classSourceCode = GetTimeReportRowMappingClass(methodBody);

            var mappingType =
                CompilationHelper.Compile(classSourceCode, CompilationHelper.GetLoadedReferences())
                    .GetType("JiraIssueToTimeReportRowMapping");

            var mapping =
                (JiraIssueToTimeReportRowMappingBase)
                    ReflectionHelper.CreateInstanceWithIocDependencies(mappingType, new { jiraContext });

            return mapping;
        }

        private string GetTimeReportRowMappingClass(string methodBody)
        {
            return $@"
                using System;
                using System.Collections.Generic;
                using System.Linq;
                using Smt.Atomic.Business.Allocation.Dto;
                using Smt.Atomic.Business.Allocation.Interfaces;
                using Smt.Atomic.Data.Jira.Interfaces;
                using Smt.Atomic.Data.Repositories.Interfaces;
                using Smt.Atomic.Data.Repositories.Scopes;
                using Smt.Atomic.Business.TimeTracking.Services;
                using Smt.Atomic.Business.TimeTracking.Dto;

                public class JiraIssueToTimeReportRowMapping : JiraIssueToTimeReportRowMappingBase
                {{
                    public JiraIssueToTimeReportRowMapping(
                        IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
                        IProjectService projectService,
                        IJiraContextWrapper jiraContext) : base(unitOfWorkService, projectService, jiraContext)
                    {{}}

                    public override IEnumerable<MyTimeReportRowDto> CreateFromSource(IEnumerable<JiraIssueDto> issues)
                    {{
                        {methodBody}
                    }}
                }}";
        }

        private class JiraConfiguration
        {
            public string ServerAddress { get; set; }

            public string Username { get; set; }

            public string Password { get; set; }
        }
    }
}
