﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    public class HourlyRateCardIndexService : EnumBasedCardIndexDataService<HourlyRateDto, HourlyRateType>, IHourlyRateCardIndexService
    {
        protected override IEnumerable<Expression<Func<HourlyRateDto, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return r => r.Description;
        }

        protected override HourlyRateDto ConvertToDto(HourlyRateType enumValue)
        {
            return new HourlyRateDto
            {
                Id = (long)enumValue,
                Description = enumValue.GetDescription()
            };
        }
    }
}
