﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Consts;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Business.TimeTracking.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    public class OvertimeBankCardIndexDataService : CardIndexDataService<OvertimeBankDto, OvertimeBank, ITimeTrackingDbScope>, IOvertimeBankCardIndexDataService
    {
        private readonly IClassMappingFactory _classMappingFactory;
        private readonly IOrgUnitService _orgUnitService;
        private readonly IPrincipalProvider _principalProvider;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public OvertimeBankCardIndexDataService(ICardIndexServiceDependencies<ITimeTrackingDbScope> dependencies, IClassMappingFactory classMappingFactory,
            IOrgUnitService orgUnitService,
            IPrincipalProvider principalProvider)
            : base(dependencies)
        {
            _classMappingFactory = classMappingFactory;
            _orgUnitService = orgUnitService;
            _principalProvider = principalProvider;
        }

        protected override IEnumerable<Expression<Func<OvertimeBank, object>>> GetSearchColumns(SearchCriteria searchcriteria)
        {
            yield return u => u.Employee.FirstName;
            yield return u => u.Employee.LastName;
            yield return u => u.Comment;
        }

        protected override NamedFilters<OvertimeBank> NamedFilters
        {
            get
            {
                var managerOrgUnitsIds = _orgUnitService.GetManagerOrgUnitDescendantIds(_principalProvider.Current.EmployeeId);

                return new NamedFilters<OvertimeBank>(new[]
                    {
                        new NamedFilter<OvertimeBank>(FilterCodes.MyEmployees, new BusinessLogic<OvertimeBank, bool>(
                            b => EmployeeBusinessLogic.IsEmployeeOf.Call(b.Employee, _principalProvider.Current.EmployeeId, managerOrgUnitsIds)).AsExpression()),
                        new NamedFilter<OvertimeBank>(FilterCodes.AllEmployees, a => (true), SecurityRoleType.CanFilterOvertimeBankByAllEmployees),
                    }
                    .Union(CardIndexServiceHelper.BuildEnumBasedFilters<OvertimeBank, EmploymentType>(e => e.Employee.CurrentEmploymentType))
                );
            }
        }


        protected override void OnRecordAdding(RecordAddingEventArgs<OvertimeBank, OvertimeBankDto, ITimeTrackingDbScope> eventArgs)
        {
            var isOvertimeChangeZero = eventArgs.InputDto.OvertimeChange == 0;

            if (isOvertimeChangeZero)
            {
                eventArgs.AddError(OvertimeBankResources.IsOvertimeChangeZero);
                eventArgs.Canceled = true;
                return;
            }

            base.OnRecordAdding(eventArgs);
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<OvertimeBank, OvertimeBankDto, ITimeTrackingDbScope> eventArgs)
        {
            var isOvertimeChangeZero = eventArgs.InputDto.OvertimeChange == 0;

            if (isOvertimeChangeZero)
            {
                eventArgs.AddError(OvertimeBankResources.IsOvertimeChangeZero);
                eventArgs.Canceled = true;
                return;
            }

            base.OnRecordEditing(eventArgs);
        }

        public override DeleteRecordsResult DeleteRecords(IEnumerable<long> idsToDelete, object context = null, bool isDeletePermanent = false)
        {
            var toDelete = idsToDelete.ToList();

            if (toDelete.Count() > 1)
            {
                throw new BusinessException(OvertimeBankResources.OnlyOneRecord);
            }

            return base.DeleteRecords(toDelete, context, isDeletePermanent);
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<OvertimeBank, ITimeTrackingDbScope> eventArgs)
        {
            if (eventArgs.DeletingRecord.TimeReportId != null
                || eventArgs.DeletingRecord.TimeReportRowId != null
                || eventArgs.DeletingRecord.RequestId != null)
            {
                throw new BusinessException(OvertimeBankResources.CantDeleteConnectedOvertime);
            }
        }
    }
}

