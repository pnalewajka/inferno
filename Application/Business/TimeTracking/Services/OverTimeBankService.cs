﻿using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using System;
using System.Linq;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    internal class OvertimeBankService : IOvertimeBankService
    {
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService;

        public OvertimeBankService(IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        public decimal GetEmployeeOvertimeBalance(long employeeId, DateTime? effectiveDate = null)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return GetEmployeeOvertimeBalance(unitOfWork, employeeId, effectiveDate);
            }
        }

        public decimal GetEmployeeOvertimeBalance(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, long employeeId, DateTime? effectiveDate = null)
        {
            var employeeOvertimeBalances = unitOfWork.Repositories
                .OvertimeBanks
                .Where(x => x.EmployeeId == employeeId);

            if (effectiveDate.HasValue)
            {
                employeeOvertimeBalances = employeeOvertimeBalances.Where(b => b.EffectiveOn <= effectiveDate.Value);
            }

            var hours = employeeOvertimeBalances
                .OrderByDescending(x => x.EffectiveOn)
                .ThenByDescending(x => x.Id)
                .FirstOrDefault();

            return hours?.OvertimeBalance ?? 0;
        }
    }
}