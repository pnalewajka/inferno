﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    public class TimeReportProjectAttributeCardIndexDataService : CardIndexDataService<TimeReportProjectAttributeDto, TimeReportProjectAttribute, ITimeTrackingDbScope>, ITimeReportProjectAttributeCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public TimeReportProjectAttributeCardIndexDataService(ICardIndexServiceDependencies<ITimeTrackingDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override NamedFilters<TimeReportProjectAttribute> NamedFilters
        {
            get
            {
                return new NamedFilters<TimeReportProjectAttribute>(new NamedFilter<TimeReportProjectAttribute>[]
                {
                }
                .Union(CardIndexServiceHelper.BuildSoftDeletableFilters<TimeReportProjectAttribute>()));
            }
        }
    }
}
