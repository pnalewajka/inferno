﻿using System.Diagnostics;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    public class TimeReportProjectAttributeValueCardIndexDataService : CardIndexDataService<TimeReportProjectAttributeValueDto, TimeReportProjectAttributeValue, ITimeTrackingDbScope>, ITimeReportProjectAttributeValueCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public TimeReportProjectAttributeValueCardIndexDataService(ICardIndexServiceDependencies<ITimeTrackingDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override NamedFilters<TimeReportProjectAttributeValue> NamedFilters
        {
            get
            {
                return new NamedFilters<TimeReportProjectAttributeValue>(new NamedFilter<TimeReportProjectAttributeValue>[]
                {
                }
                .Union(CardIndexServiceHelper.BuildSoftDeletableFilters<TimeReportProjectAttributeValue>()));
            }
        }

        public override IQueryable<TimeReportProjectAttributeValue> ApplyContextFiltering(IQueryable<TimeReportProjectAttributeValue> records, object context)
        {
            if (context == null)
            {
                return records;
            }

            var parentIdContext = (ParentIdContext)context;

            return records.Where(r => r.AttributeId == parentIdContext.ParentId);
        }

        public override void SetContext(TimeReportProjectAttributeValue entity, object context)
        {
            if (context == null)
            {
                return;
            }

            var parentIdContext = (ParentIdContext)context;
            Debug.Assert(parentIdContext != null, "parentIdContext != null");
            entity.AttributeId = parentIdContext.ParentId;
        }
    }
}
