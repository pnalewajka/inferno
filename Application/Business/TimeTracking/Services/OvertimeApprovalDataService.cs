﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    internal class OvertimeApprovalDataService : IOvertimeApprovalDataService
    {
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _timeTrackingUnitOfWorkService;
        private readonly IClassMappingFactory _classMappingFactory;

        public OvertimeApprovalDataService(IUnitOfWorkService<ITimeTrackingDbScope> timeTrackingUnitOfWorkService,
            IClassMappingFactory classMappingFactory)
        {
            _timeTrackingUnitOfWorkService = timeTrackingUnitOfWorkService;
            _classMappingFactory = classMappingFactory;
        }

        public IEnumerable<OvertimeApprovalDto> OvertimeApprovals(IReadOnlyCollection<long> projectIds, DateTime from, DateTime to, long employeeId)
        {
            using (var unitOfWork = _timeTrackingUnitOfWorkService.Create())
            {
                var approvals = unitOfWork.Repositories.OvertimeApprovals
                    .Where(DateRangeHelper.OverlapingDateRange<OvertimeApproval>(e => e.DateFrom, e => e.DateTo, from, to)
                    .And(oa =>
                        projectIds.Contains(oa.ProjectId) &&
                        oa.EmployeeId == employeeId)
                        )
                    .OrderBy(oa => oa.DateFrom);

                var classMapping = _classMappingFactory.CreateMapping<OvertimeApproval, OvertimeApprovalDto>();

                return
                    approvals
                        .Select(classMapping.CreateFromSource)
                        .ToList();
            }
        }

        public IEnumerable<OvertimeApprovalDto> OvertimeApproval(long projectId, DateTime from, DateTime to, long employeeId)
        {
            return OvertimeApprovals(new[] { projectId }, from, to, employeeId);
        }
    }
}