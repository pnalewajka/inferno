﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using Castle.Core.Internal;
using Castle.Core.Logging;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Compensation.BusinessEvents;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Exceptions;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Business.TimeTracking.Resources;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Business.Extensions;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Presentation.Common.Interfaces;
using TTProjectBusinessLogic = Smt.Atomic.Business.TimeTracking.BusinessLogics.ProjectBusinessLogic;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    internal class TimeReportService : ITimeReportService
    {
        private const string CompanyJiraImportSourceCode = "IntiveJIRA";

        private readonly IClassMapping<MyTimeReportDto, TimeReport> _timeReportDtoToTimeReportMapping;
        private readonly IClassMapping<TimeReport, MyTimeReportDto> _timeReportToMyTimeReportDtoMapping;
        private readonly IClassMapping<TimeReport, TimeReportDto> _timeReportToTimeReportDtoMapping;
        private readonly IClassMapping<Project, TimeTrackingProjectDto> _projectToTimeTrackingProjectDto;
        private readonly IClassMapping<TimeReportProjectAttribute, TimeReportProjectAttributeDto> _timeReportProjectAttributeToTimeReportProjectAttributeDto;
        private readonly IClassMapping<Request, RequestDto> _requestToRequestDtoMapping;
        private readonly IClassMapping<TimeReportRow, MyTimeReportRowDto> _timeReportRowToMyTimeReportRowDto;
        private readonly IClassMapping<MyTimeReportRowDto, TimeReportRow> _myTimeReportRowDtoToTimeReportRow;
        private readonly ITimeService _timeService;
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService;
        private readonly ICalendarDataService _calendarDataService;
        private readonly IDanteCalendarService _danteCalendarService;
        private readonly IEmploymentPeriodService _employmentPeriodService;
        private readonly IEmployeeService _employeeService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly ILogger _logger;
        private readonly IAlertService _alertService;
        private readonly ITimeReportImportSourceService _importSourceService;
        private readonly IAbsenceService _absenceService;
        private readonly IOvertimeApprovalDataService _overtimeApprovalDataService;
        private readonly IRequestWorkflowService _requestWorkflowService;
        private readonly IRequestCardIndexDataService _requestCardIndexDataService;
        private readonly IOvertimeBankService _overtimeBankService;
        private readonly IProjectService _projectService;
        private readonly IBusinessEventPublisher _businessEventPublisher;
        private readonly ITimeReportInvoiceService _timeReportInvoiceService;

        public TimeReportService(
            IClassMappingFactory classMappingFactory,
            IClassMapping<Request, RequestDto> requestToRequestDtoMapping,
            ITimeService timeService,
            IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
            ICalendarDataService calendarDataService,
            IDanteCalendarService danteCalendarService,
            IPrincipalProvider principalProvider,
            IEmploymentPeriodService employmentPeriodService,
            IEmployeeService employeeService,
            ITimeReportImportSourceService importSourceService,
            IAbsenceService absenceService,
            IOvertimeApprovalDataService overtimeApprovalDataService,
            IRequestWorkflowService requestWorkflowService,
            IRequestCardIndexDataService requestCardIndexDataService,
            ILogger logger,
            IAlertService alertService,
            IOvertimeBankService overtimeBankService,
            IProjectService projectService,
            IBusinessEventPublisher businessEventPublisher,
            ITimeReportInvoiceService timeReportInvoiceService)
        {
            _timeReportDtoToTimeReportMapping = classMappingFactory.CreateMapping<MyTimeReportDto, TimeReport>();
            _timeReportToMyTimeReportDtoMapping = classMappingFactory.CreateMapping<TimeReport, MyTimeReportDto>();
            _timeReportToTimeReportDtoMapping = classMappingFactory.CreateMapping<TimeReport, TimeReportDto>();
            _timeReportRowToMyTimeReportRowDto = classMappingFactory.CreateMapping<TimeReportRow, MyTimeReportRowDto>();
            _projectToTimeTrackingProjectDto = classMappingFactory.CreateMapping<Project, TimeTrackingProjectDto>();
            _timeReportProjectAttributeToTimeReportProjectAttributeDto = classMappingFactory.CreateMapping<TimeReportProjectAttribute, TimeReportProjectAttributeDto>();
            _myTimeReportRowDtoToTimeReportRow = classMappingFactory.CreateMapping<MyTimeReportRowDto, TimeReportRow>();
            _requestToRequestDtoMapping = requestToRequestDtoMapping;
            _timeService = timeService;
            _unitOfWorkService = unitOfWorkService;
            _calendarDataService = calendarDataService;
            _danteCalendarService = danteCalendarService;
            _employmentPeriodService = employmentPeriodService;
            _employeeService = employeeService;
            _principalProvider = principalProvider;
            _importSourceService = importSourceService;
            _absenceService = absenceService;
            _overtimeApprovalDataService = overtimeApprovalDataService;
            _requestWorkflowService = requestWorkflowService;
            _requestCardIndexDataService = requestCardIndexDataService;
            _logger = logger;
            _alertService = alertService;
            _overtimeBankService = overtimeBankService;
            _projectService = projectService;
            _businessEventPublisher = businessEventPublisher;
            _timeReportInvoiceService = timeReportInvoiceService;
        }

        private MyTimeReportDto CreateDefaultTimeReport(long employeeId, int year, byte month)
        {
            var currentReportMode = GetTimeReportingMode(employeeId, year, month);

            switch (currentReportMode)
            {
                case TimeReportingMode.NoTracking:
                    throw new NoTimeTrackingConfiguredForUserException();

                case TimeReportingMode.AbsenceOnly:
                    return CreateDefaultAbsenceOnlyTimeReport(employeeId, year, month);

                case TimeReportingMode.Detailed:
                    return CreateDefaultNormalTimeReport(employeeId, year, month);

                default:
                    throw new Exception($"{nameof(TimeReportingMode)} not handled correctly");
            }
        }

        public TimeReportingMode GetTimeReportingMode(long employeeId, int year, byte month)
        {
            var monthStartDate = DateHelper.BeginningOfMonth(year, month);
            var monthEndDate = DateHelper.EndOfMonth(year, month);

            var monthEmploymentPeriods = _employmentPeriodService.GetEmploymentPeriodsInDateRange(employeeId,
                monthStartDate, monthEndDate);

            if (!monthEmploymentPeriods.Any())
            {
                throw new BusinessException(TimeReportServiceResources.TimeReportNotAvailableNoEmploymentPeriod);
            }

            var reportingMode = ExtractTimeReportingMode(monthEmploymentPeriods.Select(ep => ep.TimeReportingMode).ToList());

            return reportingMode;
        }

        public IReadOnlyCollection<EmployeeOvertimeInProjectDto> GetOvertimeTimeReportRows(IReadOnlyCollection<long> projectIds, IReadOnlyCollection<long> employeeIds, int year, byte month)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var query = unitOfWork.Repositories.TimeReportRows
                    .Where(r => r.TimeReport.Year == year && r.TimeReport.Month == month)
                    .Where(r => r.OvertimeVariant != HourlyRateType.Regular);

                if (!projectIds.IsNullOrEmpty())
                {
                    query = query.Where(tr => projectIds.Contains(tr.ProjectId));
                }

                if (!employeeIds.IsNullOrEmpty())
                {
                    query = query.Where(tr => employeeIds.Contains(tr.TimeReport.EmployeeId));
                }

                return query.Select(r => new EmployeeOvertimeInProjectDto
                {
                    EmployeeId = r.TimeReport.EmployeeId,
                    EmployeeDisplayName = r.TimeReport.Employee.FirstName + " " + r.TimeReport.Employee.LastName,
                    Month = r.TimeReport.Month,
                    Year = r.TimeReport.Year,
                    ProjectId = r.ProjectId,
                    ProjectName = r.Project.ProjectSetup.ProjectShortName,
                    OvertimeHours = r.DailyEntries.Sum(d => d.Hours),
                }).ToList();
            }
        }

        private MyTimeReportDto CreateDefaultAbsenceOnlyTimeReport(long employeeId, int year, byte month)
        {
            var contractedDays = _employmentPeriodService.GetContractedHours(employeeId, year, month, ep => ep.TimeReportingMode == TimeReportingMode.AbsenceOnly);

            var timeReport = new MyTimeReportDto
            {
                EmployeeId = employeeId,
                Year = year,
                Month = month,
                Rows = new List<MyTimeReportRowDto>(),
                InvoiceStatus = _timeReportInvoiceService.GetInitialInvoiceStatusForEmployee(employeeId, year, month)
            };

            timeReport = _absenceService.RecalculateAbsences(timeReport);
            timeReport.Rows = CombineDefaultProjectHoursAndAbsencesHours(timeReport.Rows, contractedDays, TimeReportServiceResources.DefaultProjectTaskName);

            return timeReport;
        }

        private IList<MyTimeReportRowDto> CombineDefaultProjectHoursAndAbsencesHours(
            IEnumerable<MyTimeReportRowDto> absencesHours,
            IEnumerable<DailyContractedHoursDto> contractedDays,
            string taskName)
        {
            var combinedTimeReportRows = absencesHours.ToList();

            var absencesHoursPerContractedDay =
                  combinedTimeReportRows.SelectMany(x => x.Days)
                 .GroupBy(x => x.Day)
                 .ToDictionary(x => x.Key, x => x.Sum(y => y.Hours));

            foreach (var contractedDay in contractedDays)
            {
                var defaultProjectId = contractedDay.EmploymentPeriod.AbsenceOnlyDefaultProjectId;

                if (!defaultProjectId.HasValue)
                {
                    continue;
                }

                var dailyEntry = new MyTimeReportDailyEntryDto
                {
                    Day = contractedDay.Date,
                    Hours = SubstractAbsencesHoursFromDefaultProjectHours(absencesHoursPerContractedDay, contractedDay.Date, contractedDay.Hours)
                };

                var timeReportRow = combinedTimeReportRows.SingleOrDefault(trr => trr.ProjectId == defaultProjectId);

                if (timeReportRow != null)
                {
                    var dayList = timeReportRow.Days.ToList();
                    dayList.Add(dailyEntry);
                    timeReportRow.Days = dayList;
                }
                else
                {
                    combinedTimeReportRows.Add(new MyTimeReportRowDto
                    {
                        ProjectId = defaultProjectId.Value,
                        ProjectName = contractedDay.EmploymentPeriod.AbsenceOnlyDefaultProjectShortName,
                        TaskName = taskName,
                        Days = new List<MyTimeReportDailyEntryDto> { dailyEntry }
                    });
                }
            }

            return combinedTimeReportRows;
        }

        private decimal SubstractAbsencesHoursFromDefaultProjectHours(Dictionary<DateTime, decimal> absencesHoursPerContractedDay, DateTime date, decimal contractedHours)
        {
            return Math.Max(0, contractedHours - (absencesHoursPerContractedDay.ContainsKey(date) ? absencesHoursPerContractedDay[date] : 0));
        }

        private MyTimeReportDto CreateDefaultNormalTimeReport(long employeeId, int year, byte month)
        {
            var allocatedProjects = GetAllocatedProjectsInDateRange(employeeId, year, month);

            MyTimeReportDto result;

            if (!allocatedProjects.Any())
            {
                result = GetEmptyTimeReport(employeeId, year, month);
            }
            else
            {
                var importTimeReport = GetImportSourceTimeReport(allocatedProjects, employeeId, year, month);

                if (importTimeReport != null)
                {
                    result = importTimeReport;
                }
                else
                {
                    result = new MyTimeReportDto
                    {
                        EmployeeId = employeeId,
                        Year = year,
                        Month = month,
                        Rows = CreateTimeReportRowsDtosFromProjects(allocatedProjects.Select(p => p.Id).ToList(), TimeReportServiceResources.DefaultTaskName),
                        InvoiceStatus = _timeReportInvoiceService.GetInitialInvoiceStatusForEmployee(employeeId, year, month)
                    };
                }
            }

            result = _absenceService.RecalculateAbsences(result);

            return result;
        }

        private MyTimeReportDto GetImportSourceTimeReport(IList<Project> allocatedProjects, long employeeId, int year, byte month)
        {
            var companyJiraImportSourceId =
                    _importSourceService.GetImportSourceIdFromCode(CompanyJiraImportSourceCode);

            var jiraData = new MyTimeReportDto
            {
                Rows = new List<MyTimeReportRowDto>()
            };

            var importProjectsWithoutErrors = true;

            try
            {
                jiraData = _importSourceService.GetTimeReport(companyJiraImportSourceId, employeeId, year, month);
            }
            catch (Exception exception)
            {
                importProjectsWithoutErrors = false;
                _logger.Error($"Import Error: {companyJiraImportSourceId}", exception);
            }

            var importSourceIds = allocatedProjects?.Where(
                p =>
                    p.TimeTrackingImportSourceId != null &&
                    p.TimeTrackingImportSource.Code != CompanyJiraImportSourceCode)
                .Select(p => p.TimeTrackingImportSourceId.Value)
                .Distinct();

            if (importSourceIds != null)
            {
                foreach (var importSourceId in importSourceIds)
                {
                    try
                    {
                        var projectJiraData = _importSourceService.GetTimeReport(importSourceId, employeeId, year, month);

                        if (jiraData == null)
                        {
                            jiraData = projectJiraData;
                        }
                        else
                        {
                            jiraData.Rows = jiraData.Rows.Concat(projectJiraData.Rows).ToList();
                        }
                    }
                    catch (Exception exception)
                    {
                        importProjectsWithoutErrors = false;
                        _logger.Error($"Import Error: {importSourceId}", exception);
                    }
                }
            }

            if (!importProjectsWithoutErrors)
            {
                _alertService.AddWarning(TimeReportResources.JiraImportErrorMessage);
            }

            if (jiraData?.Rows != null && jiraData.Rows.Any())
            {
                return jiraData;
            }

            return null;
        }

        private MyTimeReportDto GetPreviousMonthOrEmptyTimeReport(long employeeId, int year, byte month, string taskName)
        {
            var previousMonthDate = DateHelper.BeginningOfMonth(DateHelper.BeginningOfMonth(year, month).AddDays(-1));

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var oldTimeReport = GetDbTimeReport(unitOfWork, employeeId, previousMonthDate.Year,
                    (byte)previousMonthDate.Month);

                return oldTimeReport != null
                    ? CreateTimeReportBasedOnOldTimeReport(oldTimeReport, year, month, taskName)
                    : GetEmptyTimeReport(employeeId, year, month);
            }
        }

        private MyTimeReportDto GetEmptyTimeReport(long employeeId, int year, byte month)
        {
            return new MyTimeReportDto
            {
                EmployeeId = employeeId,
                Year = year,
                Month = month,
                Rows = new List<MyTimeReportRowDto>(),
                InvoiceStatus = _timeReportInvoiceService.GetInitialInvoiceStatusForEmployee(employeeId, year, month)
            };
        }

        private MyTimeReportDto CreateTimeReportBasedOnOldTimeReport(TimeReport timeReport, int year, byte month, string taskName)
        {
            var timeReportProjects = timeReport.Rows.Select(r => r.Project)
                .Where(p => p.TimeTrackingType == TimeTrackingProjectType.Normal
                    || p.TimeTrackingType == TimeTrackingProjectType.Guild
                    || p.TimeTrackingType == TimeTrackingProjectType.Training)
                .Select(p => p.Id)
                .Distinct()
                .ToList();

            var newTimeReport = new MyTimeReportDto
            {
                EmployeeId = timeReport.EmployeeId,
                Year = year,
                Month = month,
                Rows = CreateTimeReportRowsDtosFromProjects(timeReportProjects, taskName),
                InvoiceStatus = _timeReportInvoiceService.GetInitialInvoiceStatusForEmployee(timeReport.EmployeeId, year, month)
            };

            return newTimeReport;
        }

        private IList<MyTimeReportRowDto> CreateTimeReportRowsDtosFromProjects(IReadOnlyCollection<long> projectIds, string taskName)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var projects = unitOfWork
                    .Repositories
                    .Projects
                    .GetByIds(projectIds)
                    .ToList();

                var timeReportRows = projects.Select(p => CreateTimeReportRowFromProject(p, taskName));

                return timeReportRows.Select(x => new MyTimeReportRowDto
                {
                    ProjectId = x.ProjectId,
                    ProjectName = ProjectBusinessLogic.ProjectName.Call(x.Project),
                    TaskName = x.TaskName,
                    Days = Enumerable.Empty<MyTimeReportDailyEntryDto>()
                }).ToList();
            }
        }

        private static TimeReportRow CreateTimeReportRowFromProject(Project p, string taskName)
        {
            return new TimeReportRow
            {
                ProjectId = p.Id,
                Project = p,
                TaskName = taskName,
                DailyEntries = new List<TimeReportDailyEntry>(),
                AttributeValues = new List<TimeReportProjectAttributeValue>(),
                OvertimeBanks = new List<OvertimeBank>(),
            };
        }

        private IList<TimeReportRow> CloneTimeReportRows(IEnumerable<TimeReportRow> rows)
        {
            var dtos = rows.Select(_timeReportRowToMyTimeReportRowDto.CreateFromSource).ToList();

            dtos.ForEach(r =>
            {
                r.Id = -1;
                r.ImportSourceId = null;
                r.RequestId = null;
                r.Status = TimeReportStatus.Draft;

                r.IsReadOnly = false;
                r.Days.ForEach(d => d.IsReadOnly = false);
            });

            var result = dtos.Select(_myTimeReportRowDtoToTimeReportRow.CreateFromSource).ToList();

            // Restore original entities for attribute values
            var values = rows.SelectMany(r => r.AttributeValues).DistinctBy(v => v.Id).ToList();
            AssignAttributeValues(values, result);

            return result;
        }

        private IList<Project> GetAllocatedProjectsInDateRange(long employeeId, int year,
            byte month)
        {
            var monthStartDate = DateHelper.BeginningOfMonth(year, month);
            var monthEndDate = DateHelper.EndOfMonth(year, month);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var allocationRequestRepository = unitOfWork.Repositories.AllocationRequests
                    .Include(r => r.Project)
                    .Include(r => r.Project.TimeTrackingImportSource);

                var allocationRequests =
                    _employmentPeriodService.OverlapingAllocationRequests(allocationRequestRepository, employeeId, monthStartDate, monthEndDate);

                return
                    allocationRequests.Where(ar => ar.AllocationCertainty == AllocationCertainty.Certain)
                        .Select(ar => ar.Project)
                        .Include(p => p.TimeTrackingImportSource)
                        .Where(TTProjectBusinessLogic.IsAllowedProject(year, month).AsExpression())
                        .ToList();
            }
        }

        private TimeReportingMode ExtractTimeReportingMode(IList<TimeReportingMode> possibleModes)
        {
            if (possibleModes.Any(m => m == TimeReportingMode.Detailed))
            {
                return TimeReportingMode.Detailed;
            }

            if (possibleModes.Any(m => m == TimeReportingMode.AbsenceOnly))
            {
                return TimeReportingMode.AbsenceOnly;
            }

            if (possibleModes.Any(m => m == TimeReportingMode.NoTracking))
            {
                return TimeReportingMode.NoTracking;
            }

            throw new Exception("TimeReportingMode not handled correctly");
        }

        public TimeReportDto GetTimeReport(long employeeId, int year, byte month)
        {
            if (employeeId <= 0)
            {
                throw new BusinessException(TimeReportServiceResources.TimeReportNotAvailableNoEmployee);
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var timeReport = GetDbTimeReport(unitOfWork, employeeId, year, month);
                TimeReportDto timeReportDto = null;

                if (timeReport != null)
                {
                    timeReport.Rows = timeReport.Rows
                        .OrderBy(r => r.DisplayOrder)
                        .ToList();

                    timeReportDto = _timeReportToTimeReportDtoMapping.CreateFromSource(timeReport);
                }

                return timeReportDto;
            }
        }

        public IReadOnlyCollection<TimeReportDto> GetEmployeeTimeReports(long employeeId, DateTime dateFrom, DateTime dateTo)
        {
            if (employeeId <= 0)
            {
                throw new BusinessException(TimeReportServiceResources.TimeReportNotAvailableNoEmployee);
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                IReadOnlyCollection<TimeReportDto> response = null;
                var timeReports = unitOfWork
                    .Repositories
                    .TimeReports
                    .Where(
                        r =>
                            r.Year >= dateFrom.Year &&
                            r.Year <= dateTo.Year &&
                            r.Month >= (byte)dateFrom.Month &&
                            r.Month <= (byte)dateTo.Month &&
                            r.EmployeeId == employeeId)
                    .OrderBy(r => r.Year)
                    .ThenBy(r => r.Month);

                if (timeReports != null)
                {
                    timeReports.ForEach(r => r.Rows.OrderBy(rr => rr.DisplayOrder));
                    response = _timeReportToTimeReportDtoMapping.CreateFromSource(timeReports).ToList();
                }

                return response;
            }
        }

        public IReadOnlyCollection<TimeReportDto> GetMyEmployeesTimeReports(long employeeId, int year, byte month)
        {
            if (employeeId <= 0)
            {
                throw new BusinessException(TimeReportServiceResources.TimeReportNotAvailableNoEmployee);
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                IReadOnlyCollection<TimeReportDto> response = null;

                var timeReports = unitOfWork
                    .Repositories
                    .TimeReports
                    .Where(r =>
                        r.Year == year &&
                        r.Month == month &&
                        r.Employee.LineManagerId == employeeId)
                    .ToList();

                if (timeReports != null)
                {
                    timeReports.ForEach(r => r.Rows.OrderBy(rr => rr.DisplayOrder));
                    response = _timeReportToTimeReportDtoMapping.CreateFromSource(timeReports).ToList();
                }

                return response;
            }
        }

        public IReadOnlyCollection<TimeReportDto> GetMyProjectEmployeesTimeReports(long employeeId, int year, byte month)
        {
            if (employeeId <= 0)
            {
                throw new BusinessException(TimeReportServiceResources.TimeReportNotAvailableNoEmployee);
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                IReadOnlyCollection<TimeReportDto> response = null;
                var currentDate = _timeService.GetCurrentDate();
                var timeReports = unitOfWork
                    .Repositories
                    .AllocationRequests
                    .Where(a =>
                        a.Project.ProjectSetup.ProjectManagerId == employeeId &&
                        a.EmployeeId != employeeId &&
                        (!a.EndDate.HasValue || a.EndDate >= currentDate)
                        )
                    .SelectMany(a =>
                        a.Employee.TimeReports)
                    .Where(r =>
                        r.Year == year &&
                        r.Month == month);

                if (timeReports != null)
                {
                    timeReports.ForEach(r => r.Rows.OrderBy(rr => rr.DisplayOrder));
                    response = _timeReportToTimeReportDtoMapping.CreateFromSource(timeReports).ToList();
                }

                return response;
            }
        }

        public MyTimeReportDto GetMyTimeReport(long employeeId, int year, byte month, bool forceReadOnly)
        {
            if (employeeId <= 0)
            {
                throw new BusinessException(TimeReportServiceResources.TimeReportNotAvailableNoEmployee);
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var timeReport = GetDbTimeReport(unitOfWork, employeeId, year, month);
                timeReport = DiscardNotStartedTimeReport(unitOfWork, timeReport);

                MyTimeReportDto timeReportDto;

                if (timeReport != null)
                {
                    timeReport.Rows = timeReport.Rows
                        .OrderBy(r => r.DisplayOrder)
                        .ToList();

                    timeReportDto = _timeReportToMyTimeReportDtoMapping.CreateFromSource(timeReport);
                }
                else
                {
                    if (CanOpenTimeReportForDate(year, month))
                    {
                        timeReportDto = CreateDefaultTimeReport(employeeId, year, month);
                        timeReportDto = PersistAndReloadTimeReport(timeReportDto, unitOfWork);
                    }
                    else
                    {
                        throw new BusinessException(string.Format(TimeReportServiceResources.TimeReportNotAvailableBadDate, year, month));
                    }
                }

                timeReportDto.Days = CalculateDayDescriptions(year, month, employeeId).ToList();
                timeReportDto.Weeks = DateHelper.AllWeeksInMonth(year, month).Select(w => new MyTimeReportWeekDescriptionDto
                {
                    WeekNumber = w.WeekNumber,
                    WeekDays = w.DaysInWeek

                }).ToList();

                timeReportDto.IsTimeReportAvailablePreviousMonth = IsPreviousMonthAvailable(unitOfWork, employeeId, year, month);
                CalculateReadOnlyRestrictions(timeReportDto, forceReadOnly);

                timeReportDto.AllowedOvertime = _employmentPeriodService.AllowedOvertimeInGivenMonth(employeeId, year, month);

                return timeReportDto;
            }
        }

        private TimeReport DiscardNotStartedTimeReport(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, TimeReport timeReport)
        {
            if (timeReport != null && timeReport.Status == TimeReportStatus.NotStarted)
            {
                unitOfWork.Repositories.TimeReports.Delete(timeReport);
                unitOfWork.Commit();

                return null;
            }

            return timeReport;
        }

        private bool CanOpenTimeReportForDate(int year, byte month)
        {
            var currentDate = _timeService.GetCurrentDate();
            var date = new DateTime(year, month, 1);

            if (date == currentDate.GetFirstDayInMonth()
                || currentDate.AddMonths(-1).GetFirstDayInMonth() == date
                || currentDate.AddMonths(1).GetFirstDayInMonth() == date)
            {
                return true;
            }

            return false;
        }

        private MyTimeReportDto PersistAndReloadTimeReport(MyTimeReportDto timeReportDto, IUnitOfWork<ITimeTrackingDbScope> unitOfWork)
        {
            var timeReport = _timeReportDtoToTimeReportMapping.CreateFromSource(timeReportDto);

            timeReport.ContractedHours = GetContractedHours(timeReport);
            timeReport.ReportedNormalHours = timeReport.GetReportedNormalHours();
            timeReport.NoServiceHours = _absenceService.GetNoServiceHours(timeReport.EmployeeId, timeReport.Year, timeReport.Month);

            unitOfWork.Repositories.TimeReports.Add(timeReport);
            unitOfWork.Commit();
            timeReport = unitOfWork.Repositories.TimeReports.Where(t => t.Id == timeReport.Id).Include(t => t.Employee)
                .Include(e => e.Rows).Include(e => e.Rows.Select(r => r.DailyEntries))
                .Include(t => t.Rows.Select(r => r.Project)).Single();

            return _timeReportToMyTimeReportDtoMapping.CreateFromSource(timeReport);
        }

        public BoolResult UpdateTimeReportFromImportSource(MyTimeReportDto myTimeReportDto)
        {
            BoolResult result = null;
            var importSourceId = myTimeReportDto.Rows.FirstOrDefault()?.ImportSourceId;

            if (importSourceId == null)
            {
                return new BoolResult(true, new[]
                {
                    new AlertDto
                    {
                        Type = AlertType.Information,
                        Message = TimeReportResources.MyTimeReportImportJiraNoDataAlert,
                    },
                });
            }

            var timeReport = _timeReportDtoToTimeReportMapping.CreateFromSource(myTimeReportDto);
            MyTimeReportDto newTimeReportDto = null;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var previousReport = GetDbTimeReport(unitOfWork, timeReport.EmployeeId, timeReport.Year, timeReport.Month);
                MyTimeReportDto previousReportDto;

                if (previousReport != null)
                {
                    if (previousReport.IsReadOnly)
                    {
                        return new BoolResult(false, new[]
                        {
                            new AlertDto
                            {
                                Type = AlertType.Information,
                                Message = TimeReportResources.ModifyingReadOnlyReport,
                            },
                        });
                    }

                    SynchronizeTimeReportStatuses(previousReport, timeReport);

                    previousReportDto = _timeReportToMyTimeReportDtoMapping.CreateFromSource(previousReport);
                    newTimeReportDto = _timeReportToMyTimeReportDtoMapping.CreateFromSource(previousReport);

                    var importSourceRowsToDelete = previousReportDto.Rows.Where(r => r.ImportSourceId == importSourceId);

                    // Map id to previous rows based on ImportedTaskCode
                    importSourceRowsToDelete.ForEach(r =>
                    {
                        var matchingRow = myTimeReportDto.Rows.Where(x => x.ImportedTaskCode == r.ImportedTaskCode);

                        if (matchingRow.Count() == 1)
                        {
                            matchingRow.First().Id = r.Id;
                        }
                    });

                    // Delete old rows
                    newTimeReportDto.Rows = newTimeReportDto.Rows.Where(
                        r => !importSourceRowsToDelete.Select(ir => ir.Id).Contains(r.Id)).ToList();

                    // Add all new rows
                    myTimeReportDto.Rows.ForEach(r => newTimeReportDto.Rows.Add(r));
                }
                else
                {
                    previousReportDto = GetMyTimeReport(timeReport.EmployeeId, timeReport.Year, timeReport.Month, false);

                    timeReport.ContractedHours = GetContractedHours(timeReport);
                    timeReport.ReportedNormalHours = timeReport.GetReportedNormalHours();
                    timeReport.NoServiceHours = _absenceService.GetNoServiceHours(timeReport.EmployeeId, timeReport.Year, timeReport.Month);

                    newTimeReportDto = _timeReportToMyTimeReportDtoMapping.CreateFromSource(timeReport);
                }

                // Remove not disallowed projects
                var notAllowedProjectRemovedAlerts = RemoveNotAllowedProjects(newTimeReportDto);

                result = CheckOnSaveViolations(newTimeReportDto, previousReportDto);
                result.Alerts.AddRange(notAllowedProjectRemovedAlerts);
            }

            if (result.IsSuccessful && newTimeReportDto != null)
            {
                UpdateFullTimeReport(newTimeReportDto);

                result.AddAlert(AlertType.Success, TimeReportResources.MyTimeReportImportJiraSuccessAlert);
            }

            return result;
        }

        public void UpdateTimeReportStatus(long timeReportId, TimeReportStatus newStatus, IEnumerable<long> unlinkRequestIds)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                UpdateTimeReportStatus(unitOfWork, timeReportId, newStatus, unlinkRequestIds);

                unitOfWork.Commit();
            }
        }

        private void RemoveAutomaticOvertimes(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, TimeReport timeReport)
        {
            unitOfWork.Repositories.OvertimeBanks.DeleteRange(timeReport.OvertimeBanks);
        }

        public void UpdateTimeReportStatus(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, long timeReportId, TimeReportStatus newStatus, IEnumerable<long> unlinkRequestIds)
        {
            var timeReport = unitOfWork.Repositories.TimeReports.GetById(timeReportId);
            timeReport.Status = newStatus;

            if (newStatus == TimeReportStatus.Submitted || newStatus == TimeReportStatus.Accepted)
            {
                var emptyRows = timeReport.Rows.Where(r => !r.DailyEntries.Any()).ToList();
                emptyRows.ForEach(r => timeReport.Rows.Remove(r));
                unitOfWork.Repositories.TimeReportRows.DeleteRange(emptyRows);

                if (newStatus == TimeReportStatus.Accepted)
                {
                    _businessEventPublisher.PublishBusinessEvent(
                        new CompensationChangedEvent(timeReport.EmployeeId, timeReport.Year, timeReport.Month));
                }
            }

            if (newStatus == TimeReportStatus.Draft)
            {
                RemoveAutomaticOvertimes(unitOfWork, timeReport);

                var editableRows =
                    timeReport.Rows
                        .Where(r => !ReadOnlyTimeTrackingTypes.Contains(r.Project.TimeTrackingType))
                        .ToList();

                if (unlinkRequestIds != null)
                {
                    editableRows = editableRows.Where(
                        r => !r.RequestId.HasValue
                        || unlinkRequestIds.Contains(r.RequestId.Value)).ToList();
                }

                editableRows.ForEach(r => r.Status = TimeReportStatus.Draft);
                editableRows.ForEach(r => r.RequestId = null);
            }
        }

        private static TimeTrackingProjectType[] ReadOnlyTimeTrackingTypes => new[]
        {
            TimeTrackingProjectType.Absence,
            TimeTrackingProjectType.Holiday,
        };

        public void UpdateFullTimeReport(MyTimeReportDto myTimeReportDto)
        {
            var recalculatedTimeReport = _absenceService.RecalculateAbsences(myTimeReportDto);
            var timeReport = _timeReportDtoToTimeReportMapping.CreateFromSource(recalculatedTimeReport);
            timeReport.EmployeeId = myTimeReportDto.EmployeeId;

            using (var transaction = TransactionHelper.CreateDefaultTransactionScope())
            {
                using (var unitOfWork = _unitOfWorkService.Create())
                {
                    var previousReport = GetDbTimeReport(unitOfWork, timeReport.EmployeeId, timeReport.Year, timeReport.Month);
                    previousReport.IsTemporary = true; // disable unique check

                    SynchronizeTimeReportStatuses(previousReport, timeReport);
                    timeReport.ContractedHours = GetContractedHours(timeReport);
                    timeReport.ReportedNormalHours = timeReport.GetReportedNormalHours();
                    timeReport.NoServiceHours = _absenceService.GetNoServiceHours(timeReport.EmployeeId, timeReport.Year, timeReport.Month);

                    AttachAttributeValues(timeReport, unitOfWork);

                    CreateOvertimeBanksOnSaving(previousReport.Employee, timeReport);

                    // Maintain row order
                    var order = 0;
                    timeReport.Rows.ForEach(r =>
                    {
                        r.Id = 0; // To be added
                        r.DisplayOrder = order++;
                    });

                    unitOfWork.Repositories.TimeReports.Add(timeReport);
                    unitOfWork.Commit();

                    // Delete old report (+ OvertimeBanks)
                    if (previousReport != null)
                    {
                        unitOfWork.Repositories.OvertimeBanks.DeleteRange(previousReport.Rows.SelectMany(r => r.OvertimeBanks));
                        unitOfWork.Repositories.TimeReportRows.DeleteRange(previousReport.Rows);
                        unitOfWork.Repositories.OvertimeBanks.DeleteRange(previousReport.OvertimeBanks);
                        unitOfWork.Repositories.TimeReports.Delete(previousReport);
                    }

                    unitOfWork.Commit();
                }

                transaction.Complete();
            }
        }

        private static void AssignAttributeValues(
            ICollection<TimeReportProjectAttributeValue> attributeValues,
            ICollection<TimeReportRow> rows)
        {
            foreach (var row in rows)
            {
                foreach (var value in row.AttributeValues.ToList())
                {
                    var commonValue = attributeValues.Single(v => v.Id == value.Id);

                    row.AttributeValues.Remove(value);
                    row.AttributeValues.Add(commonValue);
                }
            }
        }

        private static void AttachAttributeValues(TimeReport timeReport, IUnitOfWork<ITimeTrackingDbScope> unitOfWork)
        {
            var values = timeReport.Rows.SelectMany(r => r.AttributeValues).DistinctBy(v => v.Id).ToList();

            foreach (var value in values)
            {
                unitOfWork.Repositories.TimeReportProjectAttributeValues.Attach(value);
            }

            AssignAttributeValues(values, timeReport.Rows);
        }

        private void CreateOvertimeBanksOnSaving(Employee employee, TimeReport timeReport)
        {
            var monthFrom = DateHelper.BeginningOfMonth(timeReport.Year, timeReport.Month);
            var monthTo = DateHelper.EndOfMonth(timeReport.Year, timeReport.Month);
            var employmentPeriod = employee.EmploymentPeriods.AsQueryable()
                .SingleOrDefault(EmploymentPeriodBusinessLogic.OfEmployeeInDate.Parametrize(employee.Id, monthFrom));

            if (timeReport.Rows.Any())
            {
                var useAutomaticOvertimeBankCalculation = employmentPeriod?.ContractType?.UseAutomaticOvertimeBankCalculation ?? false;

                if (!useAutomaticOvertimeBankCalculation)
                {
                    timeReport.Rows.Where(
                        r => r.OvertimeVariant == HourlyRateType.OverTimeBank && r.DailyEntries.Any(e => e.Hours > 0)
                    ).ForEach(r =>
                    {
                        if (r.OvertimeBanks == null)
                        {
                            r.OvertimeBanks = new List<OvertimeBank>();
                        }

                        r.OvertimeBanks.Add(new OvertimeBank
                        {
                            EmployeeId = timeReport.EmployeeId,
                            OvertimeChange = r.DailyEntries.Sum(d => d.Hours),
                            EffectiveOn = r.DailyEntries.First(e => e.Hours > 0).Day,
                            Comment = $"Created based on the overtime bank hours reported in {timeReport.Month}/{timeReport.Year}"
                        });
                    });
                }
            }
        }

        public void UpdateContractedHours(DateTime day)
        {
            IEnumerable<long> employeeIds;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                employeeIds = unitOfWork.Repositories.TimeReports
                    .Where(r => r.Year == day.Year && r.Month == day.Month)
                    .Select(r => r.EmployeeId)
                    .ToList();
            }

            foreach (long employeeId in employeeIds)
            {
                UpdateContractedHours(employeeId, day.Year, (byte)day.Month);
            }
        }

        public void UpdateContractedHours(long employeeId, DateTime from, DateTime? to)
        {
            if (!to.HasValue)
            {
                using (var unitOfWork = _unitOfWorkService.Create())
                {
                    var latestTimeReportDate = unitOfWork.Repositories.TimeReports
                        .Where(r => r.EmployeeId == employeeId)
                        .Select(r => new { r.Year, r.Month })
                        .OrderByDescending(r => r.Year).ThenByDescending(r => r.Month)
                        .FirstOrDefault();

                    if (latestTimeReportDate == null)
                    {
                        return;
                    }

                    to = new DateTime(latestTimeReportDate.Year, latestTimeReportDate.Month, 1).GetLastDayInMonth();
                }
            }

            for (var date = from; date <= to; date = date.AddMonths(1))
            {
                UpdateContractedHours(employeeId, date.Year, (byte)date.Month);
            }
        }

        private void UpdateContractedHours(long employeeId, int year, byte month)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var timeReport = GetDbTimeReport(unitOfWork, employeeId, year, month);

                if (timeReport == null)
                {
                    return;
                }

                timeReport.ContractedHours = GetContractedHours(timeReport);

                unitOfWork.Commit();
            }
        }

        private void SynchronizeTimeReportStatuses(TimeReport oldReport, TimeReport newReport)
        {
            if (oldReport == null)
            {
                return;
            }

            newReport.Status = oldReport.Status;
            newReport.InvoiceStatus = oldReport.InvoiceStatus;
            newReport.InvoiceIssues = oldReport.InvoiceIssues;
            newReport.InvoiceRejectionReason = oldReport.InvoiceRejectionReason;
            newReport.InvoiceNotificationStatus = oldReport.InvoiceNotificationStatus;
            newReport.InvoiceNotificationErrorMessage = oldReport.InvoiceNotificationErrorMessage;

            foreach (var newRow in newReport.Rows)
            {
                var oldRow = oldReport.Rows.FirstOrDefault(r => r.Id == newRow.Id);

                if (oldRow != null)
                {
                    newRow.Status = oldRow.Status;
                }
            }
        }

        private TimeReport GetDbTimeReport(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, long employeeId, int year,
            byte month)
        {
            return unitOfWork
                .Repositories
                .TimeReports
                .SingleOrDefault(
                    r =>
                        r.Year == year && r.Month == month &&
                        r.EmployeeId == employeeId);
        }

        public BoolResult CheckOnSaveViolations(MyTimeReportDto currentTimeReportDto, MyTimeReportDto originalTimeReportDto)
        {
            var readonlyViolationAlerts = GetReadOnlyViolations(currentTimeReportDto, originalTimeReportDto);
            var absenceProjectViolationsAlerts = GetAbsenceProjectsViolations(currentTimeReportDto, originalTimeReportDto);
            var notAllowedProjectsAlerts = ValidateIfProjectsAllowed(currentTimeReportDto);
            var overtimeVariantAlerts = ValidateIfAllowedProjectOvertime(currentTimeReportDto);
            var employeeOvertimeAllowedAlerts = ValidateIfAllowedEmployeeOvertime(currentTimeReportDto);

            var alerts =
                new List<AlertDto>()
                .Concat(readonlyViolationAlerts)
                .Concat(notAllowedProjectsAlerts)
                .Concat(overtimeVariantAlerts)
                .Concat(employeeOvertimeAllowedAlerts)
                .Concat(absenceProjectViolationsAlerts)
                .ToList();

            return new BoolResult(!alerts.Any(), alerts);
        }

        private IEnumerable<AlertDto> GetAbsenceProjectsViolations(MyTimeReportDto currentTimeReportDto, MyTimeReportDto originalTimeReportDto)
        {
            var canAddAbsenceManually = _principalProvider.Current.IsInRole(SecurityRoleType.CanAddAbsenceManually);
            var newAbsenceProjects = new List<Project>();

            if (!canAddAbsenceManually)
            {
                var originalReportProjects = originalTimeReportDto.Rows.Select(x => x.ProjectId);
                var curentReportProjects = currentTimeReportDto.Rows.Select(x => x.ProjectId);
                var newProjects = curentReportProjects.Except(originalReportProjects).ToList();

                if (newProjects.Any())
                {
                    using (var unitOfWork = _unitOfWorkService.Create())
                    {
                        newAbsenceProjects = unitOfWork
                           .Repositories.Projects
                           .GetByIds(newProjects)
                           .Where(x => x.TimeTrackingType == TimeTrackingProjectType.Absence)
                           .ToList();
                    }
                }
            }

            foreach (var project in newAbsenceProjects)
            {
                yield return new AlertDto
                {
                    Message = string.Format(TimeReportResources.AbsenceProjectTypeNotAllowed, ProjectBusinessLogic.ClientProjectName.Call(project)),
                    IsDismissable = false,
                    Type = AlertType.Error
                };
            }
        }

        private IEnumerable<AlertDto> GetReadOnlyViolations(MyTimeReportDto current, MyTimeReportDto original)
        {
            if (original.IsReadOnly)
            {
                throw new UnauthorizedException(TimeReportResources.ModifyingReadOnlyReport);
            }

            var alerts = new List<AlertDto>();
            var projects = _projectService.GetProjectsByIds(
                current.Rows.Select(r => r.ProjectId)
                .Concat(original.Rows.Select(r => r.ProjectId))
                .Distinct());
            var projectsWithViolations = new List<string>();

            // Avoid row duplicates
            if (current.Rows.Where(r => r.Id != 0).Select(r => r.Id).GroupBy(n => n).Any(c => c.Count() > 1))
            {
                alerts.Add(AlertDto.CreateError(TimeReportResources.DataIntegrityError));
            }

            // Validate old rows
            foreach (var originalRow in original.Rows)
            {
                var currentRow = current.Rows.FirstOrDefault(r => r.Id == originalRow.Id);
                var project = projects.Single(p => p.Id == originalRow.ProjectId);

                // If row exists in database, set status from backend. Don't trust the frontend
                if (currentRow != null)
                {
                    currentRow.IsReadOnly = originalRow.IsReadOnly;
                    currentRow.Status = originalRow.Status;
                    currentRow.RequestId = originalRow.RequestId;
                }

                // Delete row or modify task name, attributes
                if ((originalRow.IsReadOnly || originalRow.Days.Any(d => d.IsReadOnly))
                    && (currentRow == null
                        || originalRow.TaskName != currentRow.TaskName
                        || !originalRow.SelectedAttributesValueIds.SequenceEqual(currentRow.SelectedAttributesValueIds)
                        || originalRow.OvertimeVariant != currentRow.OvertimeVariant
                        || originalRow.ImportedTaskCode != currentRow.ImportedTaskCode
                        || originalRow.ImportedTaskType != currentRow.ImportedTaskType
                        || originalRow.ImportSourceId != currentRow.ImportSourceId
                        || originalRow.ProjectId != currentRow.ProjectId
                        || originalRow.RequestId != currentRow.RequestId))
                {
                    projectsWithViolations.Add(project.ClientProjectName);
                    break;
                }

                // Modify any old hours
                foreach (var originalDay in originalRow.Days)
                {
                    var currentDay = currentRow?.Days.FirstOrDefault(d => d.Day.Date == originalDay.Day.Date);

                    if ((originalRow.IsReadOnly || originalDay.IsReadOnly) &&
                        (currentDay == null || originalDay.Hours != currentDay.Hours))
                    {
                        projectsWithViolations.Add(project.ClientProjectName);
                        break;
                    }
                }

                // New days on existing row
                if (currentRow != null)
                {
                    foreach (var currentDay in currentRow.Days.Where(d => !originalRow.Days.Select(x => x.Day).Contains(d.Day)))
                    {
                        if (project.ReportingStartsOn.HasValue
                            && project.ReportingStartsOn.Value > currentDay.Day && currentDay.Hours != 0)
                        {
                            projectsWithViolations.Add(project.ClientProjectName);
                            break;
                        }
                    }
                }
            }

            // Validate new rows
            foreach (var newRow in current.Rows.Where(r => !original.Rows.Any(or => or.Id == r.Id)))
            {
                var project = projects.Single(p => p.Id == newRow.ProjectId);

                // Verify that project isn't ReadOnly
                if (original.Rows.Any(r => r.ProjectId == newRow.ProjectId && r.IsReadOnly))
                {
                    projectsWithViolations.Add(project.ClientProjectName);
                    break;
                }

                // When adding new row to project, explicitly check Project.ReportingStartsOn
                foreach (var newDay in newRow.Days)
                {
                    if (project.ReportingStartsOn.HasValue
                        && project.ReportingStartsOn.Value > newDay.Day && newDay.Hours != 0)
                    {
                        projectsWithViolations.Add(project.ClientProjectName);
                        break;
                    }
                }
            }

            return alerts.Concat(
                    projectsWithViolations.Distinct().Select(
                        p => AlertDto.CreateError(string.Format(TimeReportResources.ModifyingReadOnlyProject, p))));
        }

        private IEnumerable<AlertDto> ValidateClockCard(MyTimeReportDto currentTimeReportDto)
        {
            if (!_employmentPeriodService.IsClockCardRequiredInGivenMonth(
                currentTimeReportDto.EmployeeId, currentTimeReportDto.Year, currentTimeReportDto.Month))
            {
                yield break;
            }

            var incompatibleTimeDays = new List<DateTime>();
            var missingClockCardTimeDays = new List<DateTime>();

            foreach (var dailyEntry in currentTimeReportDto.ClockCardDailyEntries)
            {
                var reportedHours = currentTimeReportDto.Rows
                    .Where(r => !r.AbsenceTypeId.HasValue)
                    .Sum(r => r.Days.Where(d => d.Day.Date == dailyEntry.Day.Date).Select(d => d.Hours).SingleOrDefault());

                if (dailyEntry.InTime.HasValue && dailyEntry.OutTime.HasValue && dailyEntry.BreakDuration.HasValue)
                {
                    var reportedHoursTimeSpan = TimeSpan.FromHours(Convert.ToDouble(reportedHours));
                    var reportedTimeTimeSpan = dailyEntry.OutTime.Value.Subtract(dailyEntry.InTime.Value).Subtract(dailyEntry.BreakDuration.Value);

                    if (!TimeSpanHelper.ApproximatelyEquals(reportedTimeTimeSpan, reportedHoursTimeSpan))
                    {
                        incompatibleTimeDays.Add(dailyEntry.Day.Date);
                    }
                }
                else if (reportedHours != 0M && (!dailyEntry.InTime.HasValue || !dailyEntry.OutTime.HasValue || !dailyEntry.BreakDuration.HasValue))
                {
                    missingClockCardTimeDays.Add(dailyEntry.Day.Date);
                }
            }

            if (incompatibleTimeDays.Any())
            {
                var dates = string.Join(", ", incompatibleTimeDays.Select(d => d.ToString("d")));

                yield return AlertDto.CreateError(string.Format(TimeReportResources.ClockCardIncompatibleWithTimeReport, dates));
            }

            if (missingClockCardTimeDays.Any())
            {
                var dates = string.Join(", ", missingClockCardTimeDays.Select(d => d.ToString("d")));

                yield return AlertDto.CreateError(string.Format(TimeReportResources.ClockCardTimeMissing, dates));
            }
        }

        private IEnumerable<AlertDto> ValidateTaskAttributes(MyTimeReportDto currentTimeReportDto)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                foreach (var row in currentTimeReportDto.Rows.Where(r => !r.IsReadOnly))
                {
                    var project = unitOfWork.Repositories.Projects.GetById(row.ProjectId);
                    var missingAttributes = project.Attributes.Where(a => a.Values.Select(v => v.Id).Intersect(row.SelectedAttributesValueIds).Count() != 1);

                    if (missingAttributes.Any())
                    {
                        var projectName = ProjectBusinessLogic.ClientProjectName.Call(project);
                        var attributeNames = string.Join(", ", missingAttributes.Select(a => a.Name));

                        yield return AlertDto.CreateError(
                            string.Format(TimeReportResources.RequiredAttributesMissing, projectName, attributeNames));
                    }
                }
            }
        }

        private IEnumerable<AlertDto> ValidateIfAllowedEmployeeOvertime(MyTimeReportDto currentTimeReportDto)
        {
            var monthStartDate = DateHelper.BeginningOfMonth(currentTimeReportDto.Year, currentTimeReportDto.Month);
            var monthEndDate = DateHelper.EndOfMonth(currentTimeReportDto.Year, currentTimeReportDto.Month);

            var overtimeRows = currentTimeReportDto.Rows.Where(
                r =>
                    r.OvertimeVariant != HourlyRateType.Regular &&
                    r.Days.Sum(d => d.Hours) > 0)
                .ToList();

            if (!overtimeRows.Any())
            {
                yield break;
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var dateRangeExpression = DateRangeHelper.OverlapingDateRange<EmploymentPeriod>(e => e.StartDate,
                    e => e.EndDate ?? DateTime.MaxValue, monthStartDate, monthEndDate);

                var employmentPeriods =
                    unitOfWork.Repositories.EmploymentPeriods.Where(
                        dateRangeExpression
                        .And(e => e.EmployeeId == currentTimeReportDto.EmployeeId && e.ContractType != null))
                        .Select(e => new
                        {
                            e.StartDate,
                            e.EndDate,
                            e.ContractType.AllowedOverTimeTypes
                        }).ToList();

                foreach (var employmentPeriod in employmentPeriods)
                {
                    var notAllowedForPeriod =
                        overtimeRows
                            .Where(or => !or.OvertimeVariant.IsAllowedBy(employmentPeriod.AllowedOverTimeTypes))
                            .Where(or => or.Days.Any(d => d.Hours > 0 && employmentPeriod.StartDate <= d.Day && d.Day <= (employmentPeriod.EndDate ?? monthEndDate)))
                            .Select(or => or.OvertimeVariant)
                            .ToList();

                    if (notAllowedForPeriod.Any())
                    {
                        yield return
                                new AlertDto
                                {
                                    Type = AlertType.Error,
                                    Message = string.Format(TimeReportServiceResources.OvertimeNotAllowedForEmployeeMessageFormat, string.Join(", ", notAllowedForPeriod.Select(v => v.GetDescription())))
                                };
                    }
                }
            }
        }

        private IEnumerable<AlertDto> ValidateIfAllowedProjectOvertime(MyTimeReportDto currentTimeReportDto)
        {
            var overtimeOptions = currentTimeReportDto.Rows
                .Where(p => p.OvertimeVariant != HourlyRateType.Regular)
                .ToList();

            if (!overtimeOptions.Any())
            {
                yield break;
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var projectAllowedOvertimeTypes =
                    unitOfWork.Repositories.Projects.GetByIds(overtimeOptions.Select(p => p.ProjectId))
                        .ToDictionary(p => p.Id, p => p.AllowedOvertimeTypes);

                foreach (var myTimeReportRowDto in overtimeOptions)
                {
                    var overTimeTypes = projectAllowedOvertimeTypes[myTimeReportRowDto.ProjectId];
                    if (!myTimeReportRowDto.OvertimeVariant.IsAllowedBy(overTimeTypes))
                    {
                        yield return
                            new AlertDto
                            {
                                Message = string.Format(TimeReportServiceResources.OvertimeNotAllowedMessageFormat, myTimeReportRowDto.ProjectName, myTimeReportRowDto.OvertimeVariant.GetDescription()),
                                Type = AlertType.Error
                            };
                    }
                }
            }
        }

        private ICollection<long> GetInvalidProjectIds(MyTimeReportDto timeReport)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var projectIds = timeReport.Rows.Select(r => r.ProjectId).Distinct().ToList();

                var validProjects = unitOfWork
                    .Repositories
                    .Projects
                    .GetByIds(projectIds)
                    .Where(TTProjectBusinessLogic.IsAllowedProject(timeReport.Year, timeReport.Month))
                    .Select(p => p.Id)
                    .ToList();

                return projectIds.Except(validProjects).ToList();
            }
        }

        private IEnumerable<AlertDto> ValidateIfProjectsAllowed(MyTimeReportDto timeReportDto)
        {
            var notAllowedProjectIds = GetInvalidProjectIds(timeReportDto);

            var messages = timeReportDto.Rows
                .GroupBy(r => new { r.ProjectId, r.ProjectName })
                .Where(r => notAllowedProjectIds.Contains(r.Key.ProjectId))
                .Select(
                    r =>
                        string.Format(TimeReportServiceResources.ProjectNotAllowedForTimeTrackingMessageFormat,
                            r.Key.ProjectName))
                .ToList();

            foreach (var message in messages)
            {
                yield return new AlertDto { Message = message, Type = AlertType.Error };
            }
        }

        private IEnumerable<AlertDto> RemoveNotAllowedProjects(MyTimeReportDto timeReportDto)
        {
            var notAllowedProjectIds = GetInvalidProjectIds(timeReportDto);

            var messages = timeReportDto.Rows
                .GroupBy(r => new { r.ProjectId, r.ProjectName })
                .Where(r => notAllowedProjectIds.Contains(r.Key.ProjectId))
                .Select(
                    r =>
                        string.Format(TimeReportServiceResources.ProjectNotAllowedForTimeTrackingMessageFormat,
                            r.Key.ProjectName))
                .ToList();

            // delete rows
            timeReportDto.Rows = timeReportDto.Rows
                .Where(r => !notAllowedProjectIds.Contains(r.ProjectId))
                .ToList();

            return messages.Select(m => new AlertDto { Message = m, Type = AlertType.Warning });
        }

        private void CalculateReadOnlyRestrictions(MyTimeReportDto timeReportDto, bool forceReadOnly)
        {
            var currentPrincipal = _principalProvider.Current;

            timeReportDto.IsReadOnly = forceReadOnly
                                        || timeReportDto.IsReadOnly
                                        || (currentPrincipal.EmployeeId != timeReportDto.EmployeeId
                                            && !currentPrincipal.IsInRole(SecurityRoleType.CanEditReportOnBehalf));
        }

        public IEnumerable<MyTimeReportDayDescriptionDto> CalculateDayDescriptions(int year, byte month, long employeeId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employee = unitOfWork.Repositories.Employees.GetById(employeeId);
                long calendarId, orgUnitCalendarId;
                CalendarData calendarData, orgUnitCalendarData;

                calendarId = orgUnitCalendarId = _danteCalendarService.GetCalendarIdByOrgUnitId(employee.OrgUnitId);
                calendarData = orgUnitCalendarData = _calendarDataService.GetCalendarDataById(calendarId);
                var contractedDays = _employmentPeriodService.GetContractedHours(employeeId, year, month);

                foreach (var date in DateHelper.AllDatesInMonth(year, month).Select(d => DateTime.SpecifyKind(d, DateTimeKind.Utc)))
                {
                    var contractedDay = contractedDays.SingleOrDefault(cd => cd.Date.Date == date.Date);

                    if (contractedDay?.EmploymentPeriod != null
                        && contractedDay.EmploymentPeriod.CalendarId != null
                        && calendarId != contractedDay.EmploymentPeriod.CalendarId)
                    {
                        calendarId = contractedDay.EmploymentPeriod.CalendarId.Value;
                        calendarData = _calendarDataService.GetCalendarDataById(calendarId);
                    }
                    else if (contractedDay?.EmploymentPeriod?.CalendarId == null
                        && calendarId != orgUnitCalendarId)
                    {
                        calendarId = orgUnitCalendarId;
                        calendarData = orgUnitCalendarData;
                    }

                    var isHoliday = calendarData.KnownHolidays.IsHoliday(date);
                    var contractType = contractedDay?.EmploymentPeriod?.ContractTypeId != null
                        ? unitOfWork.Repositories.ContractTypes.GetById(contractedDay.EmploymentPeriod.ContractTypeId)
                        : null;

                    yield return new MyTimeReportDayDescriptionDto
                    {
                        Date = date,
                        ContractedHours = contractedDay?.Hours ?? 0,
                        DailyWorkingLimit = contractType?.DailyWorkingLimit,
                        IsHoliday = isHoliday,
                        HolidayName = isHoliday ? calendarData.KnownHolidays.GetHolidayDescriptionByDate(date) : string.Empty
                    };
                }
            }
        }

        private bool IsPreviousMonthAvailable(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, long employeeId, int year, byte month)
        {
            var date = new DateTime(year, month, 1);
            var previousMonthDate = date.AddMonths(-1);

            var isTheTimeReportAvailablePreviousMonth =
                GetDbTimeReport(unitOfWork, employeeId, previousMonthDate.Year, (byte)previousMonthDate.Month) != null;

            return isTheTimeReportAvailablePreviousMonth;
        }

        public bool IsPreviousMonthAvailable(long employeeId, int year, byte month)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return IsPreviousMonthAvailable(unitOfWork, employeeId, year, month);
            }
        }

        public IEnumerable<string> GetUserTimeReportSubmitWarnings(long employeeId, int year, byte month)
        {
            var exceededOvertimes = GetExceededOvertimes(year, month, employeeId).ToList();

            if (exceededOvertimes.Any())
            {
                yield return string.Format(TimeReportServiceResources.TimeReportNoOvertimeRequestsFormat, exceededOvertimes.Count(), exceededOvertimes.Sum(h => h.ExceededHours));
            }

            var absenceDaysWithReportedHours = GetAbsenceDaysWithReportedHours(employeeId, year, month);

            if (absenceDaysWithReportedHours.Any())
            {
                yield return string.Format(TimeReportServiceResources.TimeTrackingReportingOnAbsenceDayFormat,
                    string.Join(", ", absenceDaysWithReportedHours.Distinct().Select(d => d.Day)));
            }
        }

        private DateTime[] GetAbsenceDaysWithReportedHours(long employeeId, int year, byte month)
        {
            var isInMonth = DateRangeHelper.OverlapingDateRange<EmployeeAbsence>(
                a => a.From,
                a => a.To,
                DateHelper.BeginningOfMonth(year, month),
                DateHelper.EndOfMonth(year, month));

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var absences = unitOfWork.Repositories.EmployeeAbsences
                    .Where(a => a.EmployeeId == employeeId)
                    .Where(isInMonth)
                    .Select(a => new { a.From, a.To })
                    .ToList();

                if (!absences.Any())
                {
                    return Array.Empty<DateTime>();
                }

                var isAbsenceDay = absences
                    .Select(a => new Func<DateTime, bool>(d => a.From <= d && d <= a.To))
                    .Aggregate((f1, f2) => d => f1(d) || f2(d));

                var reportedDays = unitOfWork.Repositories.DailyEntries
                    .Where(d => d.TimeReportRow.TimeReport.EmployeeId == employeeId)
                    .Where(d => d.TimeReportRow.TimeReport.Year == year && d.TimeReportRow.TimeReport.Month == month)
                    .Where(d => d.TimeReportRow.Project.TimeTrackingType != TimeTrackingProjectType.Absence)
                    .Where(d => d.TimeReportRow.Project.TimeTrackingType != TimeTrackingProjectType.Holiday)
                    .Where(d => d.Hours > 0)
                    .Select(d => d.Day);

                var absenceDays = reportedDays.Where(isAbsenceDay).ToArray();

                return absenceDays;
            }
        }

        public BusinessResult ValidateFullTimeReport(long employeeId, int year, byte month)
        {
            var allAlerts = new List<AlertDto>();
            var myTimeReport = GetMyTimeReport(employeeId, year, month, false);

            var onSaveViolations = CheckOnSaveViolations(myTimeReport, myTimeReport);
            allAlerts.AddRange(onSaveViolations.Alerts);

            var clockCardErrors = ValidateClockCard(myTimeReport);
            allAlerts.AddRange(clockCardErrors);

            var attributeErrors = ValidateTaskAttributes(myTimeReport);
            allAlerts.AddRange(attributeErrors);

            return new BusinessResult(allAlerts);
        }

        public BusinessResult OnSubmit(int year, byte month, long employeeId)
        {
            var overtimeAlerts = CreateOvertimeRequests(year, month, employeeId).SelectMany(br => br.Alerts).ToList();
            var automaticOvertimeAlerts = CreateAutomaticOvertimeBalance(year, month, employeeId).Alerts.ToList();
            var taxDeductableStatusAlerts = GetTaxDeductibleCostStatus(year, month, employeeId).Alerts.ToList();

            return new BusinessResult(overtimeAlerts.Concat(automaticOvertimeAlerts).Concat(taxDeductableStatusAlerts));
        }

        private BusinessResult GetTaxDeductibleCostStatus(int year, byte month, long employeeId)
        {
            var result = new BusinessResult();

            var beginningOfMonth = DateHelper.BeginningOfMonth(year, month);
            var endOfMonth = DateHelper.EndOfMonth(year, month);
            var employmentPeriod = _employmentPeriodService
                .GetEmploymentPeriodsInDateRange(employeeId, beginningOfMonth, endOfMonth)
                .OrderBy(p => p.StartDate)
                .FirstOrDefault();

            using (var unitOfwork = _unitOfWorkService.Create())
            {
                var taxDeductibleRequestCount = unitOfwork.Repositories.Requests.Count(r => r.RequestType == RequestType.TaxDeductibleCostRequest
                    && r.Status == RequestStatus.Completed && r.CreatedOn >= beginningOfMonth && r.CreatedOn <= endOfMonth);

                if (employmentPeriod.IsTaxDeductibleCostEnabled && taxDeductibleRequestCount == 0)
                {
                    result.AddAlert(AlertType.Warning, TimeReportServiceResources.TaxDeductibleCostWarning);
                }
            }

            return result;
        }

        private BusinessResult CreateAutomaticOvertimeBalance(int year, byte month, long employeeId)
        {
            var result = new BusinessResult();

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var timeReport = GetDbTimeReport(unitOfWork, employeeId, year, month);

                var monthFrom = DateHelper.BeginningOfMonth(year, month);
                var monthTo = DateHelper.EndOfMonth(year, month);
                var employmentPeriod = unitOfWork.Repositories.EmploymentPeriods
                    .SingleOrDefault(EmploymentPeriodBusinessLogic.OfEmployeeInDate.Parametrize(timeReport.Employee.Id, monthFrom));

                if (timeReport.Rows.Any())
                {
                    var useAutomaticOvertimeBankCalculation = employmentPeriod?.ContractType?.UseAutomaticOvertimeBankCalculation ?? false;

                    if (useAutomaticOvertimeBankCalculation)
                    {
                        var contractedHours = _employmentPeriodService.GetContractedHours(timeReport.EmployeeId, monthFrom, monthTo).Sum(d => d.Hours);
                        var reportedHours = timeReport.Rows.SelectMany(r => r.DailyEntries).Sum(d => d.Hours);
                        var monthOvertimeHours = reportedHours - contractedHours;

                        if (monthOvertimeHours != 0)
                        {
                            if (timeReport.OvertimeBanks == null)
                            {
                                timeReport.OvertimeBanks = new List<OvertimeBank>();
                            }

                            timeReport.OvertimeBanks.Add(new OvertimeBank
                            {
                                EmployeeId = timeReport.EmployeeId,
                                OvertimeChange = monthOvertimeHours,
                                EffectiveOn = monthFrom,
                                Comment = $"Created based on the time report: {month}/{year}"
                            });
                        }
                    }
                }

                try
                {
                    unitOfWork.Commit();
                }
                catch (BusinessException e)
                {
                    result.AddAlert(AlertType.Error, e.Message);
                }
            }

            return result;
        }

        public BoolResult ReopenTimeReport(long employeeId, int year, byte month, string reason, long[] projectIdsToReopen)
        {
            var result = new BoolResult(true);
            var timeReport = GetMyTimeReport(employeeId, year, month, false);

            if (!CanBeReopened(timeReport.EmployeeId, timeReport.Year, timeReport.Month))
            {
                result.IsSuccessful = false;
                result.AddAlert(AlertType.Error, TimeReportServiceResources.CantReopenTimeReport);

                return result;
            }

            var requestsToBeRejected =
                GetAutoReopenableProjectTimeReportApprovalRequests(timeReport.EmployeeId, timeReport.Year, timeReport.Month)
                    .Select(e => e.Key);

            requestsToBeRejected =
                requestsToBeRejected
                    .Union(
                        GetReopenableProjectTimeReportApprovalRequests(timeReport.EmployeeId, timeReport.Year, timeReport.Month)
                            .Where(r => projectIdsToReopen.Contains(r.Value.ProjectId))
                            .Select(e => e.Key));

            using (var transaction = TransactionHelper.CreateDefaultTransactionScope())
            {
                try
                {
                    UpdateTimeReportStatus(timeReport.TimeReportId, TimeReportStatus.Draft, requestsToBeRejected);

                    foreach (var request in requestsToBeRejected)
                    {
                        var deleteResult = _requestWorkflowService.Delete(request);

                        if (!deleteResult.IsSuccessful)
                        {
                            return deleteResult;
                        }
                    }

                    transaction.Complete();

                    // TODO: Notification of reopen
                }
                catch (BusinessException ex)
                {
                    result.IsSuccessful = false;
                    result.Alerts.Add(AlertDto.CreateError(ex.Message));
                }
            }

            return result;
        }

        private IEnumerable<BusinessResult> CreateOvertimeRequests(int year, byte month, long employeeId)
        {
            var exceededOvertimes = GetExceededOvertimes(year, month, employeeId).ToList();

            if (!exceededOvertimes.Any())
            {
                yield break;
            }

            var monthStartDate = DateHelper.BeginningOfMonth(year, month);
            var monthEndDate = DateHelper.EndOfMonth(year, month);

            foreach (var exceededOvertimeDto in exceededOvertimes)
            {
                var requestDto = new RequestDto
                {
                    RequestType = RequestType.OvertimeRequest,
                    RequestObject = new OvertimeRequestDto
                    {
                        ProjectId = exceededOvertimeDto.ProjectId,
                        From = monthStartDate,
                        To = monthEndDate,
                        HourLimit = exceededOvertimeDto.ExceededHours,
                        Comment = "Created automatically on Time report submit"
                    }
                };

                FillRequester(requestDto, employeeId);

                _requestWorkflowService.AddApprovers(requestDto);

                yield return _requestCardIndexDataService.AddRecord(requestDto);
            }
        }

        private void FillRequester(RequestDto requestDto, long employeeId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employee = _employeeService.GetEmployeesById(new[] { employeeId }).Single();

                requestDto.RequestingUserId = employee.UserId.Value;
                requestDto.RequesterEmail = employee.Email;
                requestDto.RequesterFullName = employee.FullName;
            }
        }

        private IEnumerable<ExceededOvertimeDto> GetExceededOvertimes(int year, byte month, long employeeId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var overtimeHours =
                    unitOfWork.Repositories.TimeReportRows
                        .Include(a => a.DailyEntries)
                        .Where(trr =>
                            trr.TimeReport.EmployeeId == employeeId
                            && trr.TimeReport.Year == year
                            && trr.TimeReport.Month == month
                            && trr.OvertimeVariant != HourlyRateType.Regular
                        )
                        .GroupBy(trr => trr.ProjectId)
                        .Select(r => new ExceededOvertimeDto
                        {
                            ProjectId = r.Key,
                            ExceededHours = r.Sum(ro => ro.DailyEntries.Sum(d => d.Hours)),
                        }).ToList();

                if (!overtimeHours.Any())
                {
                    return Enumerable.Empty<ExceededOvertimeDto>();
                }

                var monthStartDate = DateHelper.BeginningOfMonth(year, month);
                var monthEndDate = DateHelper.EndOfMonth(year, month);

                var requestedOvertimes = unitOfWork.Repositories.OvertimeRequests.Where(r =>
                        r.From >= monthStartDate
                        && r.To <= monthEndDate
                        && r.Request.RequestingUser.Employees.Any(e => e.Id == employeeId)
                        && r.Request.Status != RequestStatus.Error
                        && r.Request.Status != RequestStatus.Rejected)
                    .GroupBy(r => r.ProjectId)
                    .Select(r => new ExceededOvertimeDto
                    {
                        ProjectId = r.Key,
                        ExceededHours = r.Sum(x => x.HourLimit)
                    }).ToList();

                // Minus already requested hours
                foreach (var overtimeHourEntry in overtimeHours)
                {
                    var requestedOvertime = requestedOvertimes.SingleOrDefault(o => o.ProjectId == overtimeHourEntry.ProjectId);

                    if (requestedOvertime != null)
                    {
                        overtimeHourEntry.ExceededHours -= requestedOvertime.ExceededHours;
                    }
                }

                var exceedingOvertimes = overtimeHours.Where(oh => oh.ExceededHours > 0).ToList();

                return exceedingOvertimes;
            }
        }

        private IList<long> GetPendingAbsenceRequestsIdsForMonth(long employeeId, int year, byte month, IUnitOfWork<ITimeTrackingDbScope> unitOfWork)
        {
            var monthStartDate = DateHelper.BeginningOfMonth(year, month);
            var monthEndDate = DateHelper.EndOfMonth(year, month);

            return unitOfWork.Repositories.AbsenceRequests
                .Where(r =>
                    r.AffectedUser.Employees.Any(e => e.Id == employeeId) &&
                    r.Request.Status == RequestStatus.Pending)
                .Where(DateRangeHelper.OverlapingDateRange<AbsenceRequest>(r => r.From, r => r.To, monthStartDate, monthEndDate))
                .Select(r => r.Id)
                .ToList();
        }

        private decimal GetContractedHours(TimeReport timeReport)
        {
            return _employmentPeriodService.GetContractedHours(timeReport.EmployeeId, timeReport.Year, timeReport.Month).Sum(d => d.Hours);
        }

        public IDictionary<long, ProjectTimeReportApprovalRequestDto> GetReopenableProjectTimeReportApprovalRequests(long employeeId, int year, byte month)
        {
            var currentMonth = new DateTime(year, month, 1);
            var nextMonth = currentMonth.AddMonths(1);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var reopenableRequests = unitOfWork.Repositories.ProjectTimeReportApprovalRequests.Where(r =>
                        r.Request.Status != RequestStatus.Draft
                        && r.Request.Status != RequestStatus.Rejected
                        && r.EmployeeId == employeeId
                        && r.From >= currentMonth
                        && r.To < nextMonth)
                    .Where(r => r.EmployeeId == employeeId)
                    .AsEnumerable();

                var dtos = reopenableRequests.ToDictionary(r => r.Id, r => (ProjectTimeReportApprovalRequestDto)_requestToRequestDtoMapping.CreateFromSource(r.Request).RequestObject);

                return dtos;
            }
        }

        private IDictionary<long, ProjectTimeReportApprovalRequestDto> GetAutoReopenableProjectTimeReportApprovalRequests(long employeeId, int year, byte month)
        {
            var currentMonth = new DateTime(year, month, 1);
            var nextMonth = currentMonth.AddMonths(1);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var rejectedRequests = unitOfWork.Repositories.ProjectTimeReportApprovalRequests.Where(r =>
                        r.Request.Status == RequestStatus.Rejected
                        && r.EmployeeId == employeeId
                        && r.From >= currentMonth
                        && r.To < nextMonth)
                    .Where(r => r.EmployeeId == employeeId)
                    .AsEnumerable();

                var dtos = rejectedRequests.ToDictionary(r => r.Id, r => (ProjectTimeReportApprovalRequestDto)_requestToRequestDtoMapping.CreateFromSource(r.Request).RequestObject);

                return dtos;
            }
        }

        public static readonly IReadOnlyCollection<TimeReportStatus> ReopenableStatuses = new[]
        {
            TimeReportStatus.Submitted,
            TimeReportStatus.Accepted,
            TimeReportStatus.Rejected
        };

        public bool CanBeReopened(long employeeId, int year, byte month)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var timeReport = unitOfWork.Repositories.TimeReports.SingleOrDefault(
                    r => r.EmployeeId == employeeId && r.Year == year && r.Month == month);

                if (timeReport == null)
                {
                    return false;
                }

                if (ReopenableStatuses.Contains(timeReport.Status))
                {
                    return true;
                }

                return GetAutoReopenableProjectTimeReportApprovalRequests(employeeId, year, month).Any()
                    || GetReopenableProjectTimeReportApprovalRequests(employeeId, year, month).Any();
            }
        }

        public TimeTrackingProjectDto GetTimeTimeTrackingProjectDtoById(long projectId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var projectEntity = unitOfWork.Repositories.Projects.GetById(projectId);

                return _projectToTimeTrackingProjectDto.CreateFromSource(projectEntity);
            }
        }

        public IList<TimeReportProjectAttributeDto> GetDefaultAttributesDtoByProjectId(long projectId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var attributesByProjectId = unitOfWork
                    .Repositories
                    .TimeReportProjectAttributes
                    .Where(a => a.UsedInProjects.Select(p => p.Id)
                    .Contains(projectId));

                return
                    attributesByProjectId.Select(
                        _timeReportProjectAttributeToTimeReportProjectAttributeDto.CreateFromSource).ToList();
            }
        }

        public void AddEmployeeProjectsFromPreviousMonth(long employeeId, int year, byte month, bool importTasks = false)
        {
            var previousMonthDate = DateHelper.BeginningOfMonth(year, month).AddMonths(-1);
            var previousMonthYear = previousMonthDate.Year;
            var previousMonth = previousMonthDate.Month;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                if (unitOfWork.Repositories.Employees.All(x => x.Id != employeeId))
                {
                    throw new DataException($"Employee with given id ({employeeId}) doesn't exist.");
                }

                var currentMonthTimeReport =
                    unitOfWork.Repositories.TimeReports
                        .FirstOrDefault(x =>
                            x.EmployeeId == employeeId &&
                            x.Year == year &&
                            x.Month == month);

                if (currentMonthTimeReport == null)
                {
                    throw new BusinessException(TimeReportResources.NoTimeReportExceptionMessage);
                }

                if (currentMonthTimeReport.IsReadOnly)
                {
                    return;
                }

                var previousMonthTimeReport = GetTimeReport(employeeId, unitOfWork, previousMonthYear, previousMonth);

                if (previousMonthTimeReport == null)
                {
                    return;
                }

                if (importTasks)
                {
                    var tasksToImport = previousMonthTimeReport.Rows
                        .Where(t => t.Project.TimeTrackingType == TimeTrackingProjectType.Normal
                            || t.Project.TimeTrackingType == TimeTrackingProjectType.Guild
                            || t.Project.TimeTrackingType == TimeTrackingProjectType.Training)
                        .Where(t => t.RequestId == null || t.Request.RequestType == RequestType.ProjectTimeReportApprovalRequest);

                    currentMonthTimeReport.Rows = currentMonthTimeReport.Rows
                        .Concat(CloneTimeReportRows(tasksToImport))
                        .ToList();

                    // Remove old hours
                    foreach (var row in currentMonthTimeReport.Rows)
                    {
                        row.DailyEntries = new List<TimeReportDailyEntry>();
                    }
                }
                else
                {
                    var projectsToImport = previousMonthTimeReport.Rows.Select(r => r.Project)
                        .Where(p => p.TimeTrackingType == TimeTrackingProjectType.Normal
                            || p.TimeTrackingType == TimeTrackingProjectType.Guild
                            || p.TimeTrackingType == TimeTrackingProjectType.Training)
                        .DistinctBy(p => p.Id);

                    foreach (var project in projectsToImport)
                    {
                        if (currentMonthTimeReport.Rows.Any(x => x.ProjectId == project.Id))
                        {
                            continue;
                        }

                        var rowToImport = CreateTimeReportRowFromProject(project, TimeReportServiceResources.DefaultTaskName);

                        currentMonthTimeReport.Rows.Add(rowToImport);
                    }
                }

                unitOfWork.Commit();
            }
        }

        private static TimeReport GetTimeReport(long employeeId, IUnitOfWork<ITimeTrackingDbScope> unitOfWork, int year, int month)
        {
            return unitOfWork.Repositories.TimeReports.FirstOrDefault(x =>
                x.EmployeeId == employeeId &&
                x.Year == year &&
                x.Month == month);
        }

        public void UpdateTimeReportRowsStatus(long requestId, TimeReportStatus newStatus)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var timeReportRowIds = unitOfWork.Repositories.TimeReportRows.Where(r => r.RequestId == requestId).GetIds();

                UpdateTimeReportRowsStatus(unitOfWork, timeReportRowIds, newStatus);
            }
        }

        public void UpdateTimeReportRowsStatus(IEnumerable<long> rowIds, TimeReportStatus newStatus)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                UpdateTimeReportRowsStatus(unitOfWork, rowIds, newStatus);
            }
        }

        private void UpdateTimeReportRowsStatus(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, IEnumerable<long> rowIds, TimeReportStatus newStatus)
        {
            var timeReportRows = unitOfWork.Repositories.TimeReportRows.GetByIds(rowIds).ToList();

            foreach (var timeReportRow in timeReportRows)
            {
                timeReportRow.Status = newStatus;
            }

            var affectedTimeReports = timeReportRows.Select(r => r.TimeReport).DistinctBy(t => t.Id);

            if (affectedTimeReports.Any())
            {
                foreach (var timeReport in affectedTimeReports)
                {
                    if (newStatus == TimeReportStatus.Rejected)
                    {
                        timeReport.Status = TimeReportStatus.Rejected;
                        RemoveAutomaticOvertimes(unitOfWork, timeReport);
                    }
                    else if (newStatus == TimeReportStatus.Accepted && timeReport.Status == TimeReportStatus.Submitted)
                    {
                        var isFullyAccepted = timeReport.IsFullyAccepted();

                        if (isFullyAccepted)
                        {
                            timeReport.Status = TimeReportStatus.Accepted;

                            _businessEventPublisher.PublishBusinessEvent(
                                new CompensationChangedEvent(timeReport.EmployeeId, timeReport.Year, timeReport.Month));
                        }
                    }
                }
            }

            unitOfWork.Commit();
        }
    }
}