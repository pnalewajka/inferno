﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.TimeTracking.BusinessLogics;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    internal class TimeTrackingProjectCardIndexDataService : ReadOnlyCardIndexDataService<TimeTrackingProjectDto, Project, ITimeTrackingDbScope>, ITimeTrackingProjectCardIndexDataService
    {
        private readonly ITimeReportService _timeReportService;

        public TimeTrackingProjectCardIndexDataService(
            ITimeReportService timeReportService,
            ICardIndexServiceDependencies<ITimeTrackingDbScope> dependencies)
            : base(dependencies)
        {
            _timeReportService = timeReportService;
        }

        public override IQueryable<Project> ApplyContextFiltering(IQueryable<Project> records, object context)
        {
            var timeTrackingContext = context as TimeTrackingProjectPickerContext;

            if (timeTrackingContext?.Mode == TimeTrackingProjectPickerContext.PickerMode.TimeReport)
            {
                return records.Where(ProjectBusinessLogic.IsAllowedProject(
                    timeTrackingContext.Year.Value, timeTrackingContext.Month.Value,
                    PrincipalProvider.Current.IsInRole(SecurityRoleType.CanAddAbsenceManually)).AsExpression());
            }
            else if (timeTrackingContext?.Mode == TimeTrackingProjectPickerContext.PickerMode.TimeReportReopening)
            {
                var requests =
                    _timeReportService.GetReopenableProjectTimeReportApprovalRequests(
                        timeTrackingContext.EmployeeId.Value, timeTrackingContext.Year.Value, timeTrackingContext.Month.Value);
                var projectIds = requests.Select(r => r.Value.ProjectId);

                return records.Where(p => projectIds.Contains(p.Id));
            }
            else if (timeTrackingContext?.Mode == TimeTrackingProjectPickerContext.PickerMode.Overtime)
            {
                var startDate = DateHelper.BeginningOfMonth(TimeService.GetCurrentDate().AddMonths(-1));
                var endDate = DateHelper.EndOfMonth(TimeService.GetCurrentDate().AddMonths(2));

                return records.Where(ProjectBusinessLogic.IsProjectActiveWithinGivenDate.Parametrize(startDate, endDate));
            }

            throw new InvalidOperationException("Unknown context mode");
        }

        protected override IEnumerable<BusinessLogic<Project, int>> GetSearchRelevanceFactors(QueryCriteria criteria)
        {
            yield return DynamicQueryHelper.CreateRelevanceFactor<Project>(
                score: 2,
                predicate: p => (p.SubProjectShortName != null && p.SubProjectShortName.StartsWith(criteria.SearchPhrase)));

            yield return DynamicQueryHelper.CreateRelevanceFactor<Project>(
                score: 1,
                predicate: p => (p.ProjectSetup.ProjectShortName.StartsWith(criteria.SearchPhrase)));
        }

        protected override IOrderedQueryable<Project> GetDefaultOrderBy(IQueryable<Project> records, QueryCriteria criteria)
        {
            if (!criteria.SearchPhrase.IsNullOrEmpty())
            {
                return DynamicQueryHelper.OrderByRelevanceFactors(records, GetSearchRelevanceFactors(criteria));
            }

            return base.GetDefaultOrderBy(records, criteria);
        }

        protected override IEnumerable<Expression<Func<Project, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return p => p.SubProjectShortName;
            yield return p => p.ProjectSetup.ProjectShortName;
            yield return p => p.ProjectSetup.ClientShortName;
            yield return p => p.PetsCode;
            yield return p => p.ProjectSetup.ProjectManager.FirstName;
            yield return p => p.ProjectSetup.ProjectManager.LastName;
            yield return p => p.JiraKey;
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<Project> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var direction = sortingCriterion.GetSortingDirection();

            if (sortingCriterion.GetSortingColumnName() == nameof(TimeTrackingProjectDto.ProjectManagerName))
            {
                orderedQueryBuilder.ApplySortingKey(p => p.ProjectSetup.ProjectManager.LastName + " " + p.ProjectSetup.ProjectManager.FirstName, direction);
            }
            else
            {
                base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
            }
        }
    }
}
