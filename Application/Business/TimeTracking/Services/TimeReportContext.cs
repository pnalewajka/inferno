﻿using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    public class TimeReportContext
    {
        [UrlFieldName("year")]
        public int TimeReportYear { get; set; }

        [UrlFieldName("month")]
        public byte TimeReportMonth { get; set; }

        [UrlFieldName("view")]
        public string View { get; set; }
    }
}
