﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    public class AbsenceProjectListItemProvider : IListItemProvider
    {
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService;

        public AbsenceProjectListItemProvider(IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        public IEnumerable<ListItem> GetItems()
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Projects
                    .Where(p => p.TimeTrackingType == TimeTrackingProjectType.Absence)
                    .OrderBy(e => ProjectBusinessLogic.ProjectName.Call(e))
                    .Select(p => new ListItem
                    {
                        Id = p.Id,
                        DisplayName = ProjectBusinessLogic.ProjectName.Call(p),
                    })
                    .ToList();
            }
        }
    }
}
