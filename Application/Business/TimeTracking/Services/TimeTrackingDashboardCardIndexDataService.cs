﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Helpers;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    internal class TimeTrackingDashboardCardIndexDataService :
        CardIndexDataService<TimeReportProjectDetailsDto, TimeReportRow, ITimeTrackingDbScope>,
        ITimeTrackingDashboardCardIndexDataService
    {
        private readonly ICardIndexServiceDependencies<ITimeTrackingDbScope> _dependencies;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public TimeTrackingDashboardCardIndexDataService(
            ICardIndexServiceDependencies<ITimeTrackingDbScope> dependencies) : base(dependencies)
        {
            _dependencies = dependencies;
        }

        public override IQueryable<TimeReportRow> ApplyContextFiltering(IQueryable<TimeReportRow> records, object context)
        {
            var contextFilter = context as TimeTrackingDashboardContext;

            if (contextFilter == null || !contextFilter.IsSet)
            {
                throw new BusinessException($"Couldn't resolve {nameof(TimeTrackingDashboardContext)} context.");
            }

            return records
                .Where(i => i.TimeReport.Year == contextFilter.Year.Value && i.TimeReport.Month == contextFilter.Month.Value)
                .GroupBy(i => new { i.ProjectId, i.TimeReportId })
                .Select(i => i.FirstOrDefault());
        }

        public override CardIndexRecords<TimeReportProjectDetailsDto> GetRecords(QueryCriteria criteria)
        {
            var records = base.GetRecords(criteria);

            var timeReportIds = records.Rows
                .Select(r => r.TimeReportId)
                .Distinct()
                .ToHashSet();

            using (var unitOfWork = _dependencies.UnitOfWorkService.Create())
            {
                var dbRecordsDictionary = unitOfWork.Repositories.TimeReportRows
                    .Include(nameof(TimeReportRow.DailyEntries))
                    .Where(tr => timeReportIds.Contains(tr.TimeReportId))
                    .GroupBy(r => new { r.ProjectId, r.TimeReportId })
                    .ToDictionary(r => r.Key, rows => new
                    {
                        OvertimeNormalHours = rows.Where(r => r.OvertimeVariant == HourlyRateType.OverTimeNormal).Sum(a => SumHours(a)),
                        OvertimeLateNightHours = rows.Where(r => r.OvertimeVariant == HourlyRateType.OverTimeLateNight).Sum(a => SumHours(a)),
                        OvertimeWeekendHours = rows.Where(r => r.OvertimeVariant == HourlyRateType.OverTimeWeekend).Sum(a => SumHours(a)),
                        OvertimeHolidayHours = rows.Where(r => r.OvertimeVariant == HourlyRateType.OverTimeHoliday).Sum(a => SumHours(a)),
                        OvertimeTotals = rows.Where(r => r.OvertimeVariant != HourlyRateType.Regular).Sum(a => SumHours(a)),
                        TotalHours = rows.Sum(a => SumHours(a))
                    });

                foreach (var cardIndexRecord in records.Rows)
                {
                    var key = new { cardIndexRecord.ProjectId, cardIndexRecord.TimeReportId };

                    if (dbRecordsDictionary.ContainsKey(key))
                    {
                        var dictionaryRecord = dbRecordsDictionary[key];

                        cardIndexRecord.OvertimeNormalHours = dictionaryRecord.OvertimeNormalHours;
                        cardIndexRecord.OvertimeLateNightHours = dictionaryRecord.OvertimeLateNightHours;
                        cardIndexRecord.OvertimeWeekendHours = dictionaryRecord.OvertimeWeekendHours;
                        cardIndexRecord.OvertimeHolidayHours = dictionaryRecord.OvertimeHolidayHours;
                        cardIndexRecord.OvertimeTotals = dictionaryRecord.OvertimeTotals;
                        cardIndexRecord.TotalHours = dictionaryRecord.TotalHours;
                    }
                }
            }

            return records;
        }

        protected override NamedFilters<TimeReportRow> NamedFilters
        {
            get
            {
                var currentEmployeeId = _dependencies.PrincipalProvider.Current.EmployeeId;

                var filters = new[]
                {
                    new NamedFilter<TimeReportRow>(ProjectDashboardFilterCodes.MyProjects,
                        u => u.Project.ProjectSetup.ProjectManagerId == currentEmployeeId),
                     new NamedFilter<TimeReportRow>(ProjectDashboardFilterCodes.AllProjects,
                        u => true),
                     new NamedFilter<TimeReportRow, long[], long[]>(ProjectDashboardFilterCodes.ByProject,
                     (x, projectsIds, employeesIds) =>
                        (!employeesIds.Any() || employeesIds.Contains(x.TimeReport.EmployeeId))
                        && (!projectsIds.Any() || projectsIds.Contains(x.ProjectId))
                     ),
                     new NamedFilter<TimeReportRow, long[]>(ProjectDashboardFilterCodes.ByOrgUnit, (row, orgUnitIds) => orgUnitIds.Any(id => row.TimeReport.Employee.OrgUnit.Path.Contains("/" + id + "/")) ),
                };

                return new NamedFilters<TimeReportRow>(filters);
            }
        }


        protected override IEnumerable<Expression<Func<TimeReportRow, object>>> GetSearchColumns(
            SearchCriteria searchCriteria)
        {
            yield return r => r.Project.SubProjectShortName;
            yield return r => r.Project.ProjectSetup.ProjectShortName;
            yield return r => r.TimeReport.Employee.LastName;
            yield return r => r.TimeReport.Employee.FirstName;
        }

        private decimal SumHours(TimeReportRow timeReportRow)
        {
            return timeReportRow.DailyEntries.Sum(de => de.Hours);
        }
    }
}