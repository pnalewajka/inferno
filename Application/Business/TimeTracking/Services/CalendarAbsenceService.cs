﻿using System.Linq;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    public class CalendarAbsenceService: ICalendarAbsenceService
    {
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _timeTrackingUnitOfWorkService;

        public CalendarAbsenceService(IUnitOfWorkService<ITimeTrackingDbScope> timeTrackingUnitOfWorkService)
        {
            _timeTrackingUnitOfWorkService = timeTrackingUnitOfWorkService;
        }

        public bool IsUserOwnerCalendarAbsence(long userId, long calendarAbsenceId)
        {
            using (var scope = _timeTrackingUnitOfWorkService.Create())
            {
                return scope.Repositories.CalendarAbsences.Any(x => x.Id == calendarAbsenceId && x.CreatedById == userId);
            }
        }
    }
}
