﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Business.TimeTracking.Resources;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    public class ContractTypeCardIndexDataService : CardIndexDataService<ContractTypeDto, ContractType, ITimeTrackingDbScope>, IContractTypeCardIndexDataService
    {
        private readonly IReliableEmailService _reliableEmailService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly ISystemParameterService _parameterService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public ContractTypeCardIndexDataService(ICardIndexServiceDependencies<ITimeTrackingDbScope> dependencies,
            IReliableEmailService reliableEmailService,
            IMessageTemplateService messageTemplateService,
            ISystemParameterService parameterService)
            : base(dependencies)
        {
            _reliableEmailService = reliableEmailService;
            _messageTemplateService = messageTemplateService;
            _parameterService = parameterService;

            RelatedCacheAreas = new[] { CacheAreas.ContractTypes };
        }

        protected override IEnumerable<Expression<Func<ContractType, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return a => a.NameEn;
            yield return a => a.NamePl;
            yield return a => a.DescriptionEn;
            yield return a => a.DescriptionPl;
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<ContractType, ContractTypeDto, ITimeTrackingDbScope> eventArgs)
        {
            eventArgs.InputEntity.AbsenceTypes =
                eventArgs.UnitOfWork.Repositories.AbsenceTypes.GetByIds(eventArgs.InputDto.AbsenceTypeIds).ToList();
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<ContractType, ContractTypeDto, ITimeTrackingDbScope> eventArgs)
        {
            eventArgs.InputEntity.AbsenceTypes =
                eventArgs.UnitOfWork.Repositories.AbsenceTypes.GetByIds(eventArgs.InputDto.AbsenceTypeIds).ToList();
            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.AbsenceTypes,
                eventArgs.InputEntity.AbsenceTypes);
        }

        public override IQueryable<ContractType> ApplyContextFiltering(IQueryable<ContractType> records, object context)
        {
            var contractsContext = context as ContractTypeContext;

            if (contractsContext != null && contractsContext.ShouldFilterInactive)
            {
                records = records.Where(t => t.IsActive);
            }

            return base.ApplyContextFiltering(records, context);
        }

        private void NotifyAboutChanges(ContractChangeDescription contractChangeDescription)
        {
            var recipient = _parameterService.GetParameter<string>(ParameterKeys.ContractTypeNotificationEmail);
            var emailSubject = _messageTemplateService.ResolveTemplate(TemplateCodes.TimeTrackingContractTypeNotificationMessageSubject, contractChangeDescription);
            var emailBody = _messageTemplateService.ResolveTemplate(TemplateCodes.TimeTrackingContractTypeNotificationMessageBody, contractChangeDescription);
            var emailMessageDto = new EmailMessageDto
            {
                IsHtml = emailBody.IsHtml,
                Subject = emailSubject.Content,
                Body = emailBody.Content,
            };
             emailMessageDto.Recipients.Add(new EmailRecipientDto(recipient, null));

            _reliableEmailService.EnqueueMessage(emailMessageDto);
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<ContractType, ContractTypeDto> recordAddedEventArgs)
        {
            base.OnRecordAdded(recordAddedEventArgs);

            NotifyAboutChanges(new ContractChangeDescription { ModifiedContractType = new List<ContractType> { recordAddedEventArgs.InputEntity }, ChangeDescription = TimeReportResources.Added });
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<ContractType, ContractTypeDto> recordEditedEventArgs)
        {
            base.OnRecordEdited(recordEditedEventArgs);

            NotifyAboutChanges(new ContractChangeDescription { ModifiedContractType = new List<ContractType> { recordEditedEventArgs.InputEntity }, ChangeDescription = TimeReportResources.Edited });
        }

        protected override void OnRecordsDeleted(RecordDeletedEventArgs<ContractType> eventArgs)
        {
            base.OnRecordsDeleted(eventArgs);

            NotifyAboutChanges(new ContractChangeDescription { ModifiedContractType = eventArgs.DeletedEntities, ChangeDescription = TimeReportResources.Deleted });
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<ContractType, ITimeTrackingDbScope> eventArgs)
        {
            if (eventArgs.DeletingRecord.UsedInEmploymentPeriods.Any())
            {
                eventArgs.Canceled = true;
                eventArgs.AddError(ContractTypeResources.ErrorUsedInEmploymentPeriods);

                return;
            }

            if (eventArgs.DeletingRecord.UsedInEmploymentDataChangeRequests.Any())
            {
                eventArgs.Canceled = true;
                eventArgs.AddError(ContractTypeResources.ErrorUsedInEmploymentDataChangeRequests);

                return;
            }

            base.OnRecordDeleting(eventArgs);
        }
    }
}