﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    public class TimeTrackingImportSourceCardIndexDataService : CardIndexDataService<TimeTrackingImportSourceDto, TimeTrackingImportSource, ITimeTrackingDbScope>, ITimeTrackingImportSourceCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public TimeTrackingImportSourceCardIndexDataService(ICardIndexServiceDependencies<ITimeTrackingDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override IEnumerable<Expression<Func<TimeTrackingImportSource, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return e => e.NamePl;
            yield return e => e.NameEn;
            yield return e => e.Code;
            yield return e => e.ImportStrategyIdentifier;
        }
    }
}
