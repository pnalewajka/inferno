﻿using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    public class TimeReportImportSourceService : ITimeReportImportSourceService
    {
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService;
        private readonly IClassMapping<TimeTrackingImportSource, TimeTrackingImportSourceDto> _entityToDtoMapping;

        public TimeReportImportSourceService(IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService, IClassMapping<TimeTrackingImportSource, TimeTrackingImportSourceDto> entityToDtoMapping)
        {
            _unitOfWorkService = unitOfWorkService;
            _entityToDtoMapping = entityToDtoMapping;
        }

        public MyTimeReportDto GetTimeReport(long importSourceId, long employeeId, int year, byte month)
        {
            var importSourceDefinition = GetImportSourceDefinition(importSourceId);
            var instance = CreateImportSource(importSourceDefinition);
            var timeReport = instance.GetTimeReport(employeeId, year, month);
            timeReport.Rows.ForEach(r => r.ImportSourceId = importSourceId);
            timeReport.EmployeeId = employeeId;

            return timeReport;
        }

        private ITimeReportImportSource CreateImportSource(TimeTrackingImportSource importSourceDefinition)
        {
            var instance = ReflectionHelper.CreateInstanceByIdentifier<ITimeReportImportSource>(
                importSourceDefinition.ImportStrategyIdentifier,
                new { configuration = importSourceDefinition.Configuration }
            );

            return instance;
        }

        public IList<TimeTrackingImportSourceDto> GetImportSources()
        {
            List<TimeTrackingImportSource> importSources;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                importSources = unitOfWork.Repositories.ImportSources.ToList();
            }

            return importSources.Select(i => _entityToDtoMapping.CreateFromSource(i)).ToList();
        }

        private TimeTrackingImportSource GetImportSourceDefinition(string code)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.ImportSources.Single(i => i.Code == code);
            }
        }

        private TimeTrackingImportSource GetImportSourceDefinition(long id)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.ImportSources.GetById(id);
            }
        }

        public long GetImportSourceIdFromCode(string code)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.ImportSources.Where(i => i.Code == code).Select(i => i.Id).Single();
            }
        }
    }
}
