﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Data.Jira.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    public abstract class JiraIssueToTimeReportRowMappingBase
    {
        protected readonly IJiraContextWrapper JiraContext;
        protected readonly IUnitOfWorkService<ITimeTrackingDbScope> UnitOfWorkService;
        protected readonly IProjectService ProjectService;
        
        protected JiraIssueToTimeReportRowMappingBase(
            IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
            IProjectService projectService,
            IJiraContextWrapper jiraContext)
        {
            UnitOfWorkService = unitOfWorkService;
            ProjectService = projectService;
            JiraContext = jiraContext;
        }
        
        protected ProjectDto GetProjectByJiraKeyOrDefault(string jiraKey)
        {
            return ProjectService.GetProjectByJiraKeyOrDefault(jiraKey);
        }
        
        protected ProjectDto GetProjectByJiraIdOrDefault(string jiraId)
        {
            var jiraProject = JiraContext.GetProjectById(jiraId);

            if (jiraProject == null)
            {
                return null;
            }

            return GetProjectByJiraKeyOrDefault(jiraProject.Key);
        }

        public abstract IEnumerable<MyTimeReportRowDto> CreateFromSource(IEnumerable<JiraIssueDto> issues);
    }
}
