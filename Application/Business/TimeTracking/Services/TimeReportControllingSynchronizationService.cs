﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.ImportHours.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    class TimeReportControllingSynchronizationService : ITimeReportControllingSynchronizationService
    {
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService;
        private readonly IImportHoursDataService _importHoursDataService;

        public TimeReportControllingSynchronizationService(
            IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
            IImportHoursDataService importHoursDataService)
        {
            _unitOfWorkService = unitOfWorkService;
            _importHoursDataService = importHoursDataService;
        }

        public void ForceSynchronization(IEnumerable<long> timeReportIds)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var timeReportsToUpdate = unitOfWork.Repositories.TimeReports
                                         .Where(t => 
                                            timeReportIds.Contains(t.Id) && 
                                            t.ControllingStatus != ControllingStatus.InProgress)
                                         .ToList();

                foreach (var timeReport in timeReportsToUpdate)
                {
                    timeReport.ControllingErrorMessage = null;
                    timeReport.ControllingStatus = ControllingStatus.InProgress;
                }

                unitOfWork.Commit();
            }
        }

        public void MarkAllForSynchronization(int year, int month)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var timeReportsToUpdate = unitOfWork.Repositories.TimeReports
                                         .Where(t => 
                                            t.Year == year && t.Month == month && 
                                            t.Status == TimeReportStatus.Accepted &&
                                            t.ControllingStatus != ControllingStatus.InProgress)
                                         .ToList();

                foreach (var timeReport in timeReportsToUpdate)
                {
                    timeReport.ControllingErrorMessage = null;
                    timeReport.ControllingStatus = ControllingStatus.InProgress;
                }

                unitOfWork.Commit();
            }
        }

        public void SynchronizeMarkedTimeReports()
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var timeReportsToSynchronize =
                    unitOfWork.Repositories.TimeReports
                        .Where(tr => tr.ControllingStatus == ControllingStatus.InProgress)
                        .Include(tr => tr.Employee)
                        .Include(tr => tr.Employee.Company)
                        .Include(tr => tr.Rows)
                        .Include(tr => tr.Rows.Select(r => r.DailyEntries))
                        .Include(tr => tr.Rows.Select(r => r.Project))
                        .ToList();

                foreach (var timeReport in timeReportsToSynchronize)
                {
                    var deleteRegistrationResult = _importHoursDataService.DeleteRegistration(timeReport);

                    if (deleteRegistrationResult.IsSuccessful)
                    {
                        var addRegistrationResult = _importHoursDataService.AddRegistration(timeReport);

                        if (addRegistrationResult.IsSuccessful)
                        {
                            timeReport.ControllingStatus = ControllingStatus.Sent;
                            timeReport.ControllingErrorMessage = null;
                        }
                        else
                        {
                            timeReport.ControllingStatus = ControllingStatus.Error;
                            timeReport.ControllingErrorMessage = addRegistrationResult.GetMessages();
                        }
                    }
                    else
                    {
                        timeReport.ControllingStatus = ControllingStatus.Error;
                        timeReport.ControllingErrorMessage = deleteRegistrationResult.GetMessages();
                    }
                }

                unitOfWork.Commit();
            }
        }
    }
}
