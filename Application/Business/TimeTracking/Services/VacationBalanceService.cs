﻿using System;
using System.Linq;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    public class VacationBalanceService : IVacationBalanceService
    {
        private readonly ITimeService _timeService;
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService;

        public VacationBalanceService(ITimeService timeService,
            IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService)
        {
            _timeService = timeService;
            _unitOfWorkService = unitOfWorkService;
        }

        public decimal GetHourBalanceForEmployee(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, long employeeId)
        {
            return GetHourBalanceForEmployeeUpToEffectiveDate(unitOfWork, employeeId, _timeService.GetCurrentTime());
        }

        public decimal GetHourBalanceForEmployeeUpToEffectiveDate(long employeeId, DateTime effectiveOn)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return GetHourBalanceForEmployeeUpToEffectiveDate(unitOfWork, employeeId, effectiveOn);
            }
        }

        public decimal GetHourBalanceForEmployeeUpToEffectiveDate(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, long employeeId, DateTime effectiveOn)
        {
            return GetHourBalance(unitOfWork, employeeId, effectiveOn);
        }

        public decimal GetUsedVacationHoursForEmployee(long employeeId, DateTime startDate, DateTime endDate)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return GetUsedVacationHours(unitOfWork, employeeId, startDate, endDate);
            }
        }

        private decimal GetHourBalance(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, long employeeId, DateTime effectiveOn)
        {
            var actualVacationBalance = unitOfWork.Repositories
                                      .VacationBalance
                                      .Where(x => x.EmployeeId == employeeId &&
                                                  x.EffectiveOn <= effectiveOn)
                                      .OrderByDescending(x => x.EffectiveOn)
                                      .ThenByDescending(x => x.Id)
                                      .FirstOrDefault();

            return actualVacationBalance?.TotalHourBalance ?? 0;
        }

        private decimal GetUsedVacationHours(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, long employeeId, DateTime startDate, DateTime endDate)
        {
            var isDateInRange = DateRangeHelper.IsDateInRange<VacationBalance>(startDate,
                    endDate,
                    p => p.EffectiveOn,
                    DateComparer.LeftClosed | DateComparer.RightClosed);

            var actualVacationBalance = unitOfWork.Repositories
                                      .VacationBalance
                                      .Where(x => x.EmployeeId == employeeId &&
                                            x.Operation == VacationOperation.Vacation &&
                                            isDateInRange.Call(x))
                                      .OrderByDescending(x => x.EffectiveOn)
                                      .ThenByDescending(x => x.Id);

            var result = actualVacationBalance?.Select(x => x.HourChange).DefaultIfEmpty(0).Sum();

            return result != null ? Math.Abs(result.Value) : 0;
        }
    }
}
