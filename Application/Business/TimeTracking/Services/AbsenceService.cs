﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Castle.Core;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Business.TimeTracking.Resources;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    [Interceptor(typeof(CacheInterceptor))]
    internal class AbsenceService : IAbsenceService
    {
        private readonly IEmploymentPeriodService _employmentPeriodService;
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService;
        private readonly IUnitOfWorkService<IAllocationDbScope> _allocationUnitOfWorkService;
        private readonly IClassMapping<Request, RequestDto> _requestToRequestDtoMapping;
        private readonly IClassMapping<AbsenceRequest, AbsenceRequestDto> _absenceRequestToAbsenceRequestDtoMapping;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IOvertimeBankService _overtimeBankService;
        private readonly IEmployeeService _employeeService;

        public AbsenceService(IEmploymentPeriodService employmentPeriodService,
            IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
            IUnitOfWorkService<IAllocationDbScope> allocationUnitOfWorkService,
            IClassMapping<Request, RequestDto> requestToRequestDtoMapping,
            IClassMapping<AbsenceRequest, AbsenceRequestDto> absenceRequestToAbsenceRequestDtoMapping,
            IReliableEmailService reliableEmailService,
            ISystemParameterService systemParameterService,
            IMessageTemplateService messageTemplateService,
            IOvertimeBankService overtimeBankService,
            IEmployeeService employeeService)
        {
            _employeeService = employeeService;
            _employmentPeriodService = employmentPeriodService;
            _unitOfWorkService = unitOfWorkService;
            _allocationUnitOfWorkService = allocationUnitOfWorkService;
            _requestToRequestDtoMapping = requestToRequestDtoMapping;
            _absenceRequestToAbsenceRequestDtoMapping = absenceRequestToAbsenceRequestDtoMapping;
            _reliableEmailService = reliableEmailService;
            _systemParameterService = systemParameterService;
            _messageTemplateService = messageTemplateService;
            _overtimeBankService = overtimeBankService;
        }

        private EmployeeAbsenceHoursInfoDto GetEmployeeAbsenceHoursInfo(long employeeId, DateTime? effectiveOn = null)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var overtimeBankHours = _overtimeBankService.GetEmployeeOvertimeBalance(unitOfWork, employeeId, effectiveOn);
                var vacationHours = GetAvailableVacationHours(unitOfWork, employeeId, effectiveOn);

                return new EmployeeAbsenceHoursInfoDto
                {
                    EmployeeId = employeeId,
                    EffectiveOn = effectiveOn,
                    OvertimeBankHours = overtimeBankHours,
                    VacationHours = vacationHours
                };
            }
        }

        public string GetAbsenceTypeDescriptionByAbsenceTypeId(long absenceTypeId, long userId)
        {
            var employeeId = _employeeService.GetEmployeeIdByUserId(userId).Value;
            var employeeAbsenceInfo = GetEmployeeAbsenceHoursInfo(employeeId);

            var availableHoursInfo = string.Format(AbsenceRequestResources.CurrentBalanceInfo,
                employeeAbsenceInfo.VacationHours, employeeAbsenceInfo.OvertimeBankHours);

            var employeeInstruction = GetLocalizedStringByAbsenceTypeId(absenceTypeId,
                t => new LocalizedString { English = t.EmployeeInstructionEn, Polish = t.EmployeeInstructionPl }).ToString();

            return !string.IsNullOrWhiteSpace(employeeInstruction)
                ? availableHoursInfo + Environment.NewLine + employeeInstruction
                : availableHoursInfo;
        }

        public LocalizedString GetAbsenceTypeNameByAbsenceTypeId(long absenceTypeId)
        {
            if (absenceTypeId == 0)
            {
                return new LocalizedString();
            }

            return GetLocalizedStringByAbsenceTypeId(absenceTypeId, t => new LocalizedString
            {
                English = t.NameEn,
                Polish = t.NamePl
            });
        }

        private LocalizedString GetLocalizedStringByAbsenceTypeId(long absenceTypeId, Expression<Func<AbsenceType, LocalizedString>> selector)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.AbsenceTypes
                    .Where(t => t.Id == absenceTypeId)
                    .Select(selector)
                    .Single();
            }
        }

        public MyTimeReportDto RecalculateAbsences(MyTimeReportDto myTimeReportDto)
        {
            var absenceTimereportRows = AbsenceReportRows(myTimeReportDto.EmployeeId, myTimeReportDto.Year,
                myTimeReportDto.Month);

            var absenceProjectIds = absenceTimereportRows.Select(r => r.ProjectId).Distinct();

            myTimeReportDto.Rows = absenceTimereportRows.Concat(
            myTimeReportDto.Rows.Where(r => !absenceProjectIds.Contains(r.ProjectId)))
                    .ToList();

            return myTimeReportDto;
        }

        public void UpdateTimeReportOnSubmitted(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, long requestId, long absenceTypeId,
            DateTime absenceFrom, DateTime absenceTo, long employeeId, decimal? dailyReportedHours)
        {
            var fromDate = DateHelper.MonthsInDate(absenceFrom);
            var toDate = DateHelper.MonthsInDate(absenceTo);

            var timeReports = unitOfWork.Repositories.TimeReports
                .Where(tr =>
                    tr.EmployeeId == employeeId
                    && tr.Year * 12 + tr.Month >= fromDate
                    && tr.Year * 12 + tr.Month <= toDate).ToList();

            if (timeReports.Any())
            {
                var employmentPeriodDictionary = _employmentPeriodService
                    .GetContractedHours(employeeId, absenceFrom, absenceTo)
                    .ToDictionary(ar => ar.Date, ar => ar.Hours);

                var absenceType =
                    unitOfWork.Repositories.AbsenceTypes.Where(at => at.Id == absenceTypeId)
                        .Select(at => new { at.NameEn, at.AbsenceProjectId })
                        .FirstOrDefault();

                var absenceTypeName = absenceType?.NameEn;
                var absenceProjectId = absenceType?.AbsenceProjectId;
                var createTasksForAbsences = absenceProjectId != null;
                var employmentPeriods = _employmentPeriodService.GetEmploymentPeriodsInDateRange(employeeId, absenceFrom, absenceTo);

                foreach (var timeReport in timeReports)
                {
                    var timeReportStartDate = DateHelper.BeginningOfMonth(timeReport.Year, timeReport.Month);
                    var timeReportEndDate = DateHelper.EndOfMonth(timeReportStartDate);

                    var contractedHoursForTimereport = employmentPeriodDictionary.Where(k =>
                        k.Key >= timeReportStartDate
                        && k.Key <= timeReportEndDate
                        && k.Key >= absenceFrom
                        && k.Key <= absenceTo
                        && k.Value > 0
                        ).ToArray();

                    if (createTasksForAbsences)
                    {
                        timeReport.Rows.Add(new TimeReportRow
                        {
                            ProjectId = absenceProjectId.Value,
                            RequestId = requestId,
                            Status = TimeReportStatus.Submitted,
                            TaskName = absenceTypeName,
                            AbsenceTypeId = absenceTypeId,
                            DailyEntries = contractedHoursForTimereport.Select(cht => new TimeReportDailyEntry
                            {
                                Day = cht.Key,
                                Hours = dailyReportedHours ?? cht.Value
                            }).ToList()
                        });

                        if (timeReport.Status == TimeReportStatus.NotStarted)
                        {
                            timeReport.Status = TimeReportStatus.Draft;
                        }
                    }

                    // Removing hours from default AbsenceOnly project during absence
                    foreach (var contractedDay in contractedHoursForTimereport)
                    {
                        var currentEmploymentPeriod = employmentPeriods.FirstOrDefault(p => (p.StartDate <= contractedDay.Key) && (p.EndDate == null || p.EndDate >= contractedDay.Key));

                        if (currentEmploymentPeriod != null && currentEmploymentPeriod.TimeReportingMode == TimeReportingMode.AbsenceOnly)
                        {
                            if (currentEmploymentPeriod.AbsenceOnlyDefaultProjectId.HasValue)
                            {
                                var absenceOnlyDefaultProjectRow = timeReport.Rows.FirstOrDefault(r => r.ProjectId == currentEmploymentPeriod.AbsenceOnlyDefaultProjectId.Value);

                                if (absenceOnlyDefaultProjectRow != null)
                                {
                                    var currentAbsenceOnlyDefaultProjectDailyEntry = absenceOnlyDefaultProjectRow.DailyEntries.FirstOrDefault(de => de.Day == contractedDay.Key);
                                    if (currentAbsenceOnlyDefaultProjectDailyEntry != null)
                                    {
                                        decimal hours = dailyReportedHours ?? contractedDay.Value;
                                        decimal hoursAfterDeduction = currentAbsenceOnlyDefaultProjectDailyEntry.Hours - hours;

                                        if (hoursAfterDeduction > 0)
                                        {
                                            currentAbsenceOnlyDefaultProjectDailyEntry.Hours = hoursAfterDeduction;
                                        }
                                        else
                                        {
                                            unitOfWork.Repositories.DailyEntries.Delete(
                                                currentAbsenceOnlyDefaultProjectDailyEntry);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    timeReport.ContractedHours = _employmentPeriodService.GetContractedHours(timeReport.EmployeeId, timeReport.Year, timeReport.Month).Sum(d => d.Hours);
                    timeReport.ReportedNormalHours = timeReport.GetReportedNormalHours();
                    timeReport.NoServiceHours = GetNoServiceHours(timeReport.EmployeeId, timeReport.Year, timeReport.Month);
                }
            }
        }

        public void UpdateTimeReportOnRejected(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, long requestId,
            DateTime absenceFrom, DateTime absenceTo, long employeeId)
        {
            var fromDate = DateHelper.MonthsInDate(absenceFrom);
            var toDate = DateHelper.MonthsInDate(absenceTo);

            var timeReports = unitOfWork.Repositories.TimeReports
                .Where(tr =>
                    tr.EmployeeId == employeeId
                    && tr.Year * 12 + tr.Month >= fromDate
                    && tr.Year * 12 + tr.Month <= toDate).ToList();

            var employmentPeriods = _employmentPeriodService.GetEmploymentPeriodsInDateRange(employeeId, absenceFrom, absenceTo);

            var employmentPeriodDictionary = _employmentPeriodService
                    .GetContractedHours(employeeId, absenceFrom, absenceTo)
                    .ToDictionary(ar => ar.Date, ar => ar.Hours);

            if (timeReports.Any())
            {
                foreach (var timeReport in timeReports)
                {
                    timeReport.Status = TimeReportStatus.Rejected;

                    var absenceProjectRow = timeReport.Rows.SingleOrDefault(r => r.RequestId == requestId);

                    if (absenceProjectRow != null)
                    {
                        var timeReportStartDate = DateHelper.BeginningOfMonth(timeReport.Year, timeReport.Month);
                        var timeReportEndDate = DateHelper.EndOfMonth(timeReportStartDate);

                        var contractedHoursForTimereport = employmentPeriodDictionary.Where(k =>
                                    k.Key >= timeReportStartDate
                                    && k.Key <= timeReportEndDate
                                    && k.Key >= absenceFrom
                                    && k.Key <= absenceTo
                                    && k.Value > 0
                                    ).ToArray();

                        foreach (var contractedDay in contractedHoursForTimereport)
                        {
                            var currentEmploymentPeriod = employmentPeriods.FirstOrDefault(
                                p => (p.StartDate <= contractedDay.Key) && (p.EndDate == null || p.EndDate >= contractedDay.Key));

                            if (currentEmploymentPeriod != null && currentEmploymentPeriod.TimeReportingMode == TimeReportingMode.AbsenceOnly)
                            {
                                var absenceOnlyDefaultProjectRow = timeReport.Rows.FirstOrDefault(r => r.ProjectId == currentEmploymentPeriod.AbsenceOnlyDefaultProjectId.Value);

                                if (absenceOnlyDefaultProjectRow != null)
                                {
                                    var currentAbsenceOnlyDefaultProjectDailyEntry = absenceOnlyDefaultProjectRow.DailyEntries.FirstOrDefault(de => de.Day == contractedDay.Key);
                                    var currentAbsenceProjectDailyEntry = absenceProjectRow.DailyEntries.FirstOrDefault(de => de.Day == contractedDay.Key);

                                    if (currentAbsenceProjectDailyEntry != null)
                                    {
                                        if (currentAbsenceOnlyDefaultProjectDailyEntry != null)
                                        {
                                            currentAbsenceOnlyDefaultProjectDailyEntry.Hours += currentAbsenceProjectDailyEntry.Hours;
                                        }
                                        else
                                        {
                                            absenceOnlyDefaultProjectRow.DailyEntries.Add(new TimeReportDailyEntry
                                            {
                                                Day = contractedDay.Key,
                                                Hours = currentAbsenceProjectDailyEntry.Hours,
                                            });
                                        }
                                    }
                                }
                            }
                        }

                        unitOfWork.Repositories.DailyEntries.DeleteRange(absenceProjectRow.DailyEntries);
                        unitOfWork.Repositories.TimeReportRows.Delete(absenceProjectRow);
                    }
                }
            }
        }

        public decimal GetAvailableVacationHours(IUnitOfWork<ITimeTrackingDbScope> unitOfWork, long employeeId, DateTime? effectiveDate)
        {
            var employeeVacationBalances = unitOfWork.Repositories.VacationBalance.Where(v => v.EmployeeId == employeeId);

            if (effectiveDate.HasValue)
            {
                employeeVacationBalances = employeeVacationBalances.Where(v => v.EffectiveOn <= effectiveDate.Value);
            }

            var vacationBalance = employeeVacationBalances
                .OrderByDescending(x => x.EffectiveOn)
                .ThenByDescending(x => x.Id)
                .FirstOrDefault();

            return vacationBalance?.TotalHourBalance ?? 0;
        }

        public decimal GetNoServiceHours(long employeeId, int year, byte month)
        {
            var result = 0m;

            var begginingOfMonth = DateHelper.BeginningOfMonth(year, month);
            var endOfMonth = DateHelper.EndOfMonth(year, month);

            var contractedHours = _employmentPeriodService
                .GetContractedHours(employeeId, begginingOfMonth, endOfMonth).ToDictionary(h => h.Date, h => h.Hours);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var absenceDays = unitOfWork.Repositories.EmployeeAbsences
                    .Where(a => a.EmployeeId == employeeId && a.AbsenceType.AbsenceProjectId == null)
                    .Where(DateRangeHelper.OverlapingDateRange<EmployeeAbsence>(a => a.From, a => a.To, begginingOfMonth, endOfMonth))
                    .Select(a => new { a.From, a.To })
                    .AsEnumerable()
                    .SelectMany(a => DateHelper.GetDaysInRange(a.From, a.To))
                    .ToList();

                foreach (var day in absenceDays)
                {
                    result += contractedHours.ContainsKey(day) ? contractedHours[day] : 0m;
                }
            }

            return result;
        }

        private ICollection<MyTimeReportRowDto> AbsenceReportRows(long employeeId, int year, byte month)
        {
            var result = new List<MyTimeReportRowDto>();

            var dates = DateHelper.AllDatesInMonth(year, month).ToList();
            var monthStartDate = DateHelper.BeginningOfMonth(year, month);
            var monthEndDate = DateHelper.EndOfMonth(year, month);

            var contractedHoursDictionary = _employmentPeriodService.GetContractedHours(employeeId, monthStartDate,
                monthEndDate)
                .ToDictionary(ch => ch.Date, ch => ch.Hours);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var userId = unitOfWork.Repositories.Employees.GetById(employeeId).UserId;

                var absences = unitOfWork
                    .Repositories
                    .EmployeeAbsences
                    .Include($"{nameof(EmployeeAbsence.AbsenceType)}.{nameof(AbsenceType.AbsenceProject)}")
                    .Where(ea => ea.EmployeeId == employeeId
                                 && ea.AbsenceType.AbsenceProjectId != null
                                 && !((ea.From < monthStartDate
                                       && ea.To < monthStartDate)
                                      || (ea.From > monthEndDate)))
                    .ToList();

                foreach (var employeeAbsence in absences)
                {
                    var absenceProject = employeeAbsence.AbsenceType.AbsenceProject;

                    result.Add(new MyTimeReportRowDto
                    {
                        RequestId = employeeAbsence.RequestId,
                        ProjectId = absenceProject.Id,
                        ProjectName = ProjectBusinessLogic.ClientProjectName.Call(absenceProject),
                        Status = TimeReportStatus.Accepted,
                        TaskName = employeeAbsence.AbsenceType.NameEn,
                        AbsenceTypeId = employeeAbsence.AbsenceType.Id,
                        Days = dates.Select(d => new MyTimeReportDailyEntryDto
                        {
                            Day = d,
                            Hours =
                                employeeAbsence.From <= d && d <= employeeAbsence.To ?
                                IsHourlyReportingAllowed(employeeAbsence.AbsenceType, employeeAbsence.From, employeeAbsence.To)
                                    ? employeeAbsence.Hours
                                    : contractedHoursDictionary.ContainsKey(d)
                                        ? contractedHoursDictionary[d]
                                        : 0
                                    : 0,
                            IsReadOnly = true
                        }).Where(d => d.Hours != 0).ToList()
                    });
                }

                // Add not certain absences (pending)

                var pendingAbsences = unitOfWork.Repositories.Requests.Where(r =>
                        r.RequestType == RequestType.AbsenceRequest
                        && r.Status == RequestStatus.Pending)
                    .ToList()
                    .Select(_requestToRequestDtoMapping.CreateFromSource);

                foreach (var absenceRequest in pendingAbsences)
                {
                    var content = (AbsenceRequestDto)absenceRequest.RequestObject;
                    var absenceType = unitOfWork.Repositories.AbsenceTypes.GetById(content.AbsenceTypeId);
                    var absenceProject = absenceType.AbsenceProject;

                    if (content.AffectedUserId == userId
                        && absenceProject != null
                        && !((content.From < monthStartDate
                                && content.To < monthStartDate)
                                || (content.From > monthEndDate)))
                    {
                        result.Add(new MyTimeReportRowDto
                        {
                            RequestId = absenceRequest.Id,
                            ProjectId = absenceProject.Id,
                            ProjectName = ProjectBusinessLogic.ClientProjectName.Call(absenceProject),
                            Status = TimeReportStatus.Submitted,
                            TaskName = absenceType.NameEn,
                            AbsenceTypeId = absenceType.Id,
                            Days = dates.Select(d => new MyTimeReportDailyEntryDto
                            {
                                Day = d,
                                Hours =
                                    content.From <= d && d <= content.To ?
                                    IsHourlyReportingAllowed(absenceType, content.From, content.To)
                                        ? content.Hours
                                        : contractedHoursDictionary.ContainsKey(d)
                                            ? contractedHoursDictionary[d]
                                            : 0
                                    : 0,
                                IsReadOnly = true
                            }).Where(d => d.Hours != 0).ToList()
                        });
                    }
                }
            }

            return result;
        }

        public bool IsHourlyReportingAllowed(AbsenceType absenceType, DateTime from, DateTime to)
        {
            return absenceType.AllowHourlyReporting && from.StartOfDay() == to.StartOfDay();
        }

        [Cached(CacheArea = CacheAreas.Absences, ExpirationInSeconds = 120)]
        public long[] GetHRApprovalRequiredAbsenceTypeIds()
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.AbsenceTypes.Where(at => at.RequiresHRApproval).Select(at => at.Id).ToArray();
            }
        }

        public bool IsHourlyReportingAllowed(long absenceTypeId, DateTime from, DateTime to)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var absenceType = unitOfWork.Repositories.AbsenceTypes.GetByIdOrDefault(absenceTypeId);

                if (absenceType == null)
                {
                    return false;
                }

                return IsHourlyReportingAllowed(absenceType, from, to);
            }
        }

        public void NotifyAboutRevokedAbsence(RequestDto request, AbsenceRequestDto absenceRequest)
        {
            var approverRecipients = CreateApproverRecipients(request);
            var recipientsList = CreaterAbsenceRemovalNotificationList();

            var email = new EmailMessageDto();
            RevokedAbsenceRequestNotificationDto model;

            using (var unitOfWork = _allocationUnitOfWorkService.Create())
            {
                model = new RevokedAbsenceRequestNotificationDto
                {
                    RequesterFullName = request.RequesterFullName,
                    From = absenceRequest.From,
                    To = absenceRequest.To,
                    Hours = absenceRequest.Hours,
                    AbsenceName = GetAbsenceTypeNameByAbsenceTypeId(absenceRequest.AbsenceTypeId).English,
                    CompanyName = GetCompanyName(unitOfWork, absenceRequest.AffectedUserId),
                    ContractType = GetContractType(unitOfWork, absenceRequest.AffectedUserId, absenceRequest.From),
                    AffectedUserFullName = _employeeService.GetEmployeeOrDefaultByUserId(absenceRequest.AffectedUserId).FullName
                };
            }

            var emailSubject =
                _messageTemplateService.ResolveTemplate(
                    TemplateCodes.WorkflowsRevokedAbsenceRequestNotificationMessageSubject, model);
            var emailBody =
                _messageTemplateService.ResolveTemplate(
                    TemplateCodes.WorkflowsRevokedAbsenceRequestNotificationMessageBody, model);

            email.Subject = emailSubject.Content;
            email.IsHtml = emailBody.IsHtml;
            email.Body = emailBody.Content;

            email.Recipients.AddRange(approverRecipients);
            email.Recipients.AddRange(recipientsList);
            _reliableEmailService.EnqueueMessage(email);
        }

        private string GetCompanyName(IUnitOfWork<IAllocationDbScope> unitOfWork, long userId)
        {
            return unitOfWork.Repositories.Employees
                .Where(e => e.UserId == userId)
                .Select(e => e.Company.Name).Single();
        }

        private string GetContractType(IUnitOfWork<IAllocationDbScope> unitOfWork, long userId, DateTime date)
        {
            return unitOfWork.Repositories.Employees
                .Where(e => e.UserId == userId)
                .SelectMany(e => e.EmploymentPeriods)
                .SingleOrDefault(EmploymentPeriodBusinessLogic.ForDate.Parametrize(date))
                .ContractType.NameEn;
        }

        private EmailRecipientDto[] CreaterAbsenceRemovalNotificationList()
        {
            var emails = _systemParameterService.GetParameter<string[]>(ParameterKeys.AbsenceRemovalNotificationList);

            return emails.Select(e => new EmailRecipientDto
            {
                EmailAddress = e,
            }).ToArray();
        }

        private static EmailRecipientDto[] CreateApproverRecipients(RequestDto request)
        {
            var approvers =
                request.ApprovalGroups.SelectMany(g => g.Approvals).GroupBy(a => a.UserId).Select(a => a.First());

            return approvers.Select(approval => new EmailRecipientDto
            {
                EmailAddress = approval.ApproverEmail,
                FullName = approval.ApproverFullName
            }).ToArray();
        }

        public IList<AbsenceRequestDto> GetAbsenceRequestsContainSpecificDate(long userId, DateTime date)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.AbsenceRequests
                    .Where(a => a.AffectedUserId == userId && a.Request.Status == RequestStatus.Completed)
                    .Where(DateRangeHelper.IsDateInRange<AbsenceRequest>(d => d.From, d => d.To, date))
                    .Select(_absenceRequestToAbsenceRequestDtoMapping.CreateFromSource).ToList();
            }
        }
    }
}