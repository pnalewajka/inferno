﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Allocation.Filters;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.Business.TimeTracking.Consts;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Business.TimeTracking.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    public class VacationBalanceCardIndexDataService : CardIndexDataService<VacationBalanceDto, VacationBalance, ITimeTrackingDbScope>, IVacationBalanceCardIndexDataService
    {
        private readonly ITimeService _timeService;
        private readonly IOrgUnitService _orgUnitService;
        private readonly IVacationBalanceService _vacationBalanceService;
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService;
        private readonly IClassMapping<VacationBalance, VacationBalanceDto> _entityToDtoMapping;
        private readonly IPrincipalProvider _principalProvider;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        protected override NamedFilters<VacationBalance> NamedFilters
        {
            get
            {
                var currentPrincipal = _principalProvider.Current;

                if (!currentPrincipal.IsAuthenticated || !currentPrincipal.Id.HasValue)
                {
                    return null;
                }

                var currentEmployeeId = currentPrincipal.EmployeeId;

                return new NamedFilters<VacationBalance>(new[]
                {
                    new NamedFilter<VacationBalance>(FilterCodes.AllVacations, r => true,
                        SecurityRoleType.CanViewAllVacationBalances),

                    MyEmployeesFilter.GetDescendantEmployeeFilter<VacationBalance>(
                        FilterCodes.MyEmployeeVacations, v => v.Employee, _orgUnitService, _principalProvider,  SecurityRoleType.CanViewMyEmployeeVacationBalances),
                    MyEmployeesFilter.GetChildEmployeeFilter<VacationBalance>(
                        FilterCodes.MyDirectEmployeeVacations, v => v.Employee, _orgUnitService, _principalProvider, SecurityRoleType.CanViewMyEmployeeVacationBalances),

                    new NamedFilter<VacationBalance>(FilterCodes.MyVacations, r => r.EmployeeId == currentEmployeeId,
                        SecurityRoleType.CanViewMyVacationBalance)
                }
                    .Concat(GetYearFilters()));
            }
        }

        public VacationBalanceCardIndexDataService(
            ICardIndexServiceDependencies<ITimeTrackingDbScope> dependencies, 
            IVacationBalanceService vacationBalanceService,
            IOrgUnitService orgUnitService,
            IPrincipalProvider principalProvider)
            : base(dependencies)
        {
            _unitOfWorkService = dependencies.UnitOfWorkService;
            _timeService = dependencies.TimeService;
            _orgUnitService = orgUnitService;
            _vacationBalanceService = vacationBalanceService;
            _principalProvider = principalProvider;
            _entityToDtoMapping = dependencies.ClassMappingFactory
                                              .CreateMapping<VacationBalance, VacationBalanceDto>();
        }

        public IEnumerable<FilterYearDto> GetFilterYears()
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var currentYear = _timeService.GetCurrentDate().Year;
                var years = unitOfWork.Repositories
                                      .VacationBalance
                                      .Select(c => c.EffectiveOn.Year)
                                      .Distinct()
                                      .ToHashSet();

                return FiltersHelper.CreateFiltersYears(currentYear, years);
            }
        }

        public override void BeforeBulkImportFinished(BeforeBulkImportFinishedEventArgs eventArgs)
        {
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<VacationBalance, VacationBalanceDto, ITimeTrackingDbScope> eventArgs)
        {
            var entity = eventArgs.InputEntity;

            ValidateRecord(entity, eventArgs);
        }

        public override EditRecordResult EditRecord(VacationBalanceDto record, object context = null)
        {
            var oldRecord = GetRecordById(record.Id);
            var dateChanged = oldRecord.EffectiveOn != record.EffectiveOn;

            if (dateChanged)
            {
                var result = DeleteAndAddRecord(record);
                return new EditRecordResult(result, record.Id, null);
            }

            return base.EditRecord(record, context);
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<VacationBalance, VacationBalanceDto, ITimeTrackingDbScope> eventArgs)
        {
            var entity = eventArgs.InputEntity;

            ValidateRecord(entity, eventArgs);
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<VacationBalance, ITimeTrackingDbScope> eventArgs)
        {
            var entity = eventArgs.DeletingRecord;

            if (entity.RequestId.HasValue)
            {
                eventArgs.Canceled = true;
                eventArgs.Alerts.Add(AlertDto.CreateError(VacationBalanceResources.ErrorCreatedFromAbsenceRequest));
            }
        }

        protected override IEnumerable<Expression<Func<VacationBalance, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return x => x.Employee.FirstName;
            yield return x => x.Employee.LastName;
            yield return x => x.Employee.Acronym;
        }

        private void ValidateRecord(VacationBalance entity, CardIndexEventArgs eventArgs)
        {
            if (!Enum.IsDefined(typeof(VacationOperation), entity.Operation))
            {
                eventArgs.Alerts.Add(
                    new AlertDto()
                    {
                        Type = AlertType.Error,
                        Message = string.Format(VacationBalanceResources.OperationTypeIsOutOfScopeErrorMessage, entity.Operation)
                    });

                eventArgs.Canceled = true;
            }
        }

        private bool DeleteAndAddRecord(VacationBalanceDto record, object context = null)
        {
            using (var transaction = TransactionHelper.CreateDefaultTransactionScope())
            {
                var recordId = record.Id;
                var deletedResult = DeleteRecords(new List<long> { recordId }, context);

                if (!deletedResult.IsSuccessful)
                {
                    return false;
                }

                record.Id = 0;
                var addedResult = AddRecord(record, context);

                if (addedResult.IsSuccessful)
                {
                    transaction.Complete();

                    return true;
                }
            }

            return false;
        }

        private List<NamedFilter<VacationBalance>> GetYearFilters()
        {
            var result = new List<NamedFilter<VacationBalance>>();

            foreach (var filterYear in GetFilterYears())
            {
                result.Add(new NamedFilter<VacationBalance>(filterYear.GetFilterCode(),
                                                            c => c.EffectiveOn.Year == filterYear.Year));
            }

            return result;
        }
    }
}