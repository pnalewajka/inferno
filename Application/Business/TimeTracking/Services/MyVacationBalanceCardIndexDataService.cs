﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Scopes;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Business.Common.Helpers;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    public class MyVacationBalanceCardIndexDataService 
        : ReadOnlyCardIndexDataService<VacationBalanceDto, VacationBalance, ITimeTrackingDbScope>, 
        IMyVacationBalanceCardIndexDataService
    {
        private readonly ITimeService _timeService;

        public MyVacationBalanceCardIndexDataService(ICardIndexServiceDependencies<ITimeTrackingDbScope> dependencies) 
            : base(dependencies)
        {
            _timeService = dependencies.TimeService;
        }

        protected override NamedFilters<VacationBalance> NamedFilters
        {
            get
            {
                return new NamedFilters<VacationBalance>(GetFilters());
            }
        }

        public override IQueryable<VacationBalance> ApplyContextFiltering(IQueryable<VacationBalance> records, object context)
        {
            return records.Where(r => r.EmployeeId == PrincipalProvider.Current.EmployeeId);
        }

        public IEnumerable<FilterYearDto> GetFilterYears()
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var currentYear = _timeService.GetCurrentDate().Year;
                var years = unitOfWork.Repositories
                                      .VacationBalance
                                      .Select(c => c.EffectiveOn.Year)
                                      .Distinct()
                                      .ToHashSet();

                return FiltersHelper.CreateFiltersYears(currentYear, years);
            }
        }

        private List<NamedFilter<VacationBalance>> GetFilters()
        {
            var result = new List<NamedFilter<VacationBalance>>();

            foreach (var filterYear in GetFilterYears())
            {
                result.Add(new NamedFilter<VacationBalance>(filterYear.GetFilterCode(),
                                                            c => c.EffectiveOn.Year == filterYear.Year));
            }

            return result;
        }
    }
}
