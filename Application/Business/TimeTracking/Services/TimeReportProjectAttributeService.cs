﻿using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.TimeTracking.Services
{
    public class TimeReportProjectAttributeService : ITimeReportProjectAttributeService
    {
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService;
        private readonly IClassMapping<TimeReportProjectAttribute, TimeReportProjectAttributeDto> _attributeToDtoMapping;

        public TimeReportProjectAttributeService(
            IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
            IClassMapping<TimeReportProjectAttribute, TimeReportProjectAttributeDto> attributeToDtoMapping)
        {
            _unitOfWorkService = unitOfWorkService;
            _attributeToDtoMapping = attributeToDtoMapping;
        }

        public IList<TimeReportProjectAttributeDto> GetAttributesByProjectId(long projectId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var attributes = unitOfWork.Repositories.TimeReportProjectAttributes.Where(
                    a => a.UsedInProjects.Any(p => p.Id == projectId)).ToList();

                return attributes.Select(_attributeToDtoMapping.CreateFromSource).ToList();
            }
        }
    }
}
