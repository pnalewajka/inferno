﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Repositories.FilteringQueries;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Business.Organization.Interfaces;

namespace Smt.Atomic.Business.TimeTracking.EntitySecurityRestrictions
{
    public class VacationBalanceSecurityRestriction : EntitySecurityRestriction<VacationBalance>
    {
        private readonly IOrgUnitService _orgUnitService;

        public VacationBalanceSecurityRestriction(
            IOrgUnitService orgUnitService,
            IPrincipalProvider principalProvider)
            : base(principalProvider)
        {
            _orgUnitService = orgUnitService;

            RequiresAuthentication = true;
        }

        public override Expression<Func<VacationBalance, bool>> GetRestriction()
        {
            if (Roles.Contains(SecurityRoleType.CanViewAllVacationBalances))
            {
                return AllRecords();
            }

            var currentEmployeeId = CurrentPrincipal.EmployeeId;
            Expression<Func<VacationBalance, bool>> expression = null;

            if (Roles.Contains(SecurityRoleType.CanViewMyEmployeeVacationBalances))
            {
                var managerOrgUnitsIds = _orgUnitService.GetManagerOrgUnitDescendantIds(currentEmployeeId);

                expression = new BusinessLogic<VacationBalance, bool>(
                    v => EmployeeBusinessLogic.IsEmployeeOf.Call(v.Employee, currentEmployeeId, managerOrgUnitsIds)
                ).AsExpression();
            }

            if (Roles.Contains(SecurityRoleType.CanViewMyVacationBalance))
            {
                expression = expression == null
                    ? r => r.EmployeeId == currentEmployeeId
                    : expression.Or(r => r.EmployeeId == currentEmployeeId);
            }

            return expression ?? NoRecords();
        }
    }
}
