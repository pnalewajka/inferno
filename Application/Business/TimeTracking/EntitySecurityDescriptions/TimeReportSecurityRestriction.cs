﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.TimeTracking.EntitySecurityDescriptions
{
    public class TimeReportSecurityRestriction : EntitySecurityRestriction<TimeReport>
    {
        public TimeReportSecurityRestriction(IPrincipalProvider principalProvider) : base(principalProvider)
        {
            RequiresAuthentication = true;
        }

        public override Expression<Func<TimeReport, bool>> GetRestriction()
        {
            if (Roles.Contains(SecurityRoleType.CanViewOthersTimeReport))
            {
                return AllRecords();
            }

            if (Roles.Contains(SecurityRoleType.CanViewTimeReport))
            {
                return MyTimeReports();
            }

            return NoRecords();
        }

        private Expression<Func<TimeReport, bool>> MyTimeReports()
        {
            return r => r.EmployeeId == CurrentPrincipal.EmployeeId;
        }
    }
}
