﻿﻿pivot.dataConverter = function (data) {
    return data.Records.map(function (t) {
        return t;
    });
};

pivot.configurations = [
    {
        name: "Leavings Report",
        code: "L1",
        config: {
            rows: ["Acronym", "FirstName", "LastName", "Company", "OrgUnit", "LineManager", "JobTitle",
                "Seniority", "Profile", "Location", "PlaceOfWork", "ContractType", "EndDate", "TerminatedOn", "VoluntarilyTurnover"],
            cols: [],
            vals: [],
            aggregatorName: "First",
            rowOrder: "key_a_to_z",
            colOrder: "key_a_to_z",
        }
    },
];