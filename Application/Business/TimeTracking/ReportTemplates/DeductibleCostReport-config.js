﻿﻿pivot.dataConverter = function (data) {
    return data.Records.map(function (t) {
        return t;
    });
};

pivot.configurations = [
    {
        name: "Deductible Cost Report",
        code: "T1",
        config: {
            rows: ["Acronym", "FirstName", "LastName", "Month", "Year", "WorkName", "Hours", "HoursMonthlyPercentage", "Url", "AttachmentLinks"],
            cols: [],
            vals: ["Hours"],
            aggregatorName: "Sum",
            Totals: "Hours",
            rowOrder: "key_a_to_z",
            colOrder: "key_a_to_z",
        }
    },
];