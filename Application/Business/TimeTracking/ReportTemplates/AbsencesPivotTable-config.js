pivot.dataConverter = function (data) {
    return data.Absences.map(function (a) {
        return {
            "From": Reporting.displayDate(new Date(a.From)),
            "To": Reporting.displayDate(new Date(a.To)),
            "Approved On": Reporting.displayDate(new Date(a.ApprovedOn)),
            "Requested On": Reporting.displayDate(new Date(a.RequestedOn)),
            "Absence Type Code": a.AbsenceTypeCode,
            "Is Vacation": a.IsVacation,
            "Active DirectoryId": a.Employee.ActiveDirectoryId,
            "Name": a.Employee.FirstName + " " + a.Employee.LastName,
            "First Name": a.Employee.FirstName,
            "Last Name": a.Employee.LastName,
            "Acronym": a.Employee.Acronym,
            "Email": a.Employee.Email,
            "Company Name": a.Employee.CompanyName,
            "Org Unit Code": a.Employee.OrgUnitCode,
            "Location Name": a.Employee.LocationName,
            "Contract Type": a.ContractType,
            "Approved By": a.ApprovedBy
          };
    });
};

pivot.configurations = [
    {
        name: "People vs Number of Absences (not days) vs Absence Type Code",
        code: "people-absencetypes-count",
        config: {
            rows: ["Name"],
            cols: ["Absence Type Code"],
            vals: ["Absence Type Code"],
            aggregatorName: "Count",
        },
    },
];
