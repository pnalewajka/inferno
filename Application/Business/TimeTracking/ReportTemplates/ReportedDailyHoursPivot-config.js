﻿pivot.dataConverter = function (data) {
    return data.Tasks.map(function (t) {
        return {
            "IsBillable": t.IsBillable,
            "IsRegular": t.IsRegular,
            "EmployeeId": t.Employee.ActiveDirectoryId,
            "Acronym": t.Employee.Acronym,
            "Name": t.Employee.LastName + " " + t.Employee.FirstName,
            "ProjectName": t.Project.Name,
            "TaskName": t.TaskName,
            "Overtime": t.OvertimeVariant,
            "Hours": t.Hours,
            "Seniority": t.Employee.Seniority,
            "JobTitle": t.Employee.JobTitle,
            "Login": t.Employee.Login,
            "Company": t.Employee.CompanyName,
            "OrgUnit": t.Employee.OrgUnitName,
            "WeekNo": t.WeekNumber,
            "Date": Reporting.displayDate(t.Date),
            "ProjectApn": t.Project.Apn,
            "ProjectKwId": t.Project.KupferwerkProjectId,
            "Client": t.Project.Client,
            "ProjectOrgUnit": t.Project.OrgUnitName,
            "ImportedTaskType": t.ImportedTaskType,
            "ProjectStatus": t.Project.Status,
            "ReportStatus": t.ReportStatus,
            "RequestStatus": t.RequestStatus,
            "Travel": t.Attributes.filter(a => a.Key == "Travel").map(a => a.Value)[0] || null,
            "Skill_DE": t.Attributes.filter(a => a.Key == "Skill_DE").map(a => a.Value)[0] || null
        };
    });
};

pivot.configurations = [
    {
        name: "Hours for People/Seniority",
        code: "hours-people-seniority",
        config: {
            rows: ["Name"],
            cols: ["Seniority"],
            vals: ["Hours"],
            aggregatorName: "Sum",
        },
    },
    {
        name: "Hours for People/Job Title",
        code: "hours-people-job-title",
        config: {
            rows: ["JobTitle", "Name"],
            cols: [],
            vals: ["Hours"],
            aggregatorName: "Sum",
        },
    },
    {
        name: "Hours for People/Org. Unit",
        code: "hours-people-orgunit",
        config: {
            rows: ["Name"],
            cols: ["OrgUnit"],
            vals: ["Hours"],
            aggregatorName: "Sum",
        },
    },
    {
        name: "Hours for People/Days",
        code: "hours-people-days",
        config: {
            rows: ["Date"],
            cols: ["Name"],
            vals: ["Hours"],
            aggregatorName: "Sum",
        },
    },
    {
        name: "Hours for People/Tasks",
        code: "hours-people-tasks",
        config: {
            rows: ["Name"],
            cols: ["TaskName"],
            vals: ["Hours"],
            aggregatorName: "Sum",
        },
    }
];
