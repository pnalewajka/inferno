﻿pivot.dataConverter = function (data) {
    return data.AllocatedAndReportedHours.map(function (t) {
        return {
            "JobProfile": t.JobProfile,
            "OrgUnitName": t.OrgUnitName,
            "OrgUnitFlatName": t.OrgUnitFlatName,
            "Location": t.Location,
            "LineManager": t.LineManager,
            "Level": t.Level,
            "PlaceOfWork": t.PlaceOfWork,
            "WeekNumber": t.WeekNumber,
            "Year": t.Year,
            "AllocationHours": t.AllocationHours,
            "ReportedHours": t.ReportedHours
        };
    });
};

pivot.configurations = [
    {
        name: "ReportedHours/JobProfile",
        code: "jobprofile-reportedhours",
        config: {
            rows: ["JobProfile"],
            cols: ["Year", "WeekNumber"],
            vals: ["ReportedHours"],
            aggregatorName: "Sum",
            rendererName: "Heatmap",
        },
    },
    {
        name: "ReportedHours/OrgUni",
        code: "orgunit-reportedhours",
        config: {
            rows: ["OrgUnitName"],
            cols: ["Year", "WeekNumber"],
            vals: ["ReportedHours"],
            aggregatorName: "Sum",
            rendererName: "Heatmap",
        },
    },
    {
        name: "ReportedHours/Location",
        code: "location-reportedhours",
        config: {
            rows: ["Location"],
            cols: ["Year", "WeekNumber"],
            vals: ["ReportedHours"],
            aggregatorName: "Sum",
            rendererName: "Heatmap",
        },
    },
    {
        name: "ReportedHours/LineManager",
        code: "linemanager-reportedhours",
        config: {
            rows: ["LineManager"],
            cols: ["Year", "WeekNumber"],
            vals: ["ReportedHours"],
            aggregatorName: "Sum",
            rendererName: "Heatmap",
        },
    },
    {
        name: "AllocationHours/JobProfile",
        code: "jobprofile-allocationhours",
        config: {
            rows: ["JobProfile"],
            cols: ["Year", "WeekNumber"],
            vals: ["AllocationHours"],
            aggregatorName: "Sum",
            rendererName: "Heatmap",
        },
    },
    {
        name: "AllocationHours/OrgUni",
        code: "orgunit-allocationhours",
        config: {
            rows: ["OrgUnitName"],
            cols: ["Year", "WeekNumber"],
            vals: ["AllocationHours"],
            aggregatorName: "Sum",
            rendererName: "Heatmap",
        },
    },
    {
        name: "AllocationHours/Location",
        code: "location-allocationhours",
        config: {
            rows: ["Location"],
            cols: ["Year", "WeekNumber"],
            vals: ["AllocationHours"],
            aggregatorName: "Sum",
            rendererName: "Heatmap",
        },
    },
    {
        name: "AllocationHours/LineManager",
        code: "linemanager-allocationhours",
        config: {
            rows: ["LineManager"],
            cols: ["Year", "WeekNumber"],
            vals: ["AllocationHours"],
            aggregatorName: "Sum",
            rendererName: "Heatmap",
        },
    },
];
