﻿SELECT
	   Employee.Acronym As Acronym,
	   Users.FirstName AS FirstName,
	   Users.LastName AS LastName,
	   TaxDeductibleCost.[Month] AS [Month],
	   TaxDeductibleCost.[Year] AS [Year],
	   TaxDeductibleCost.WorkName AS WorkName,
	   TaxDeductibleCost.[Hours] AS [Hours],
	   CAST(ROUND((TaxDeductibleCost.[Hours] * 100 / ISNULL((SELECT NULLIF(T.ReportedNormalHours,0) 
			FROM TimeTracking.TimeReport AS T 
				WHERE T.EmployeeId = Employee.Id
				AND T.[Month] = TaxDeductibleCost.[Month] 
				AND T.[Year] = TaxDeductibleCost.[Year] ),NULL)),1) AS DECIMAL(10,1)) AS HoursMonthlyPercentage,
	   TaxDeductibleCost.UrlField AS [Url],
	   (SELECT Stuff(
		(SELECT N', ' + R.[Name] FROM Workflows.RequestDocument AS R
		WHERE r.RequestId = TaxDeductibleCost.Id
		FOR XML PATH(''),TYPE)
		.value('text()[1]','nvarchar(max)'),1,2,N'')) AS AttachmentLinks
  FROM Workflows.TaxDeductibleCostRequest AS TaxDeductibleCost
 INNER 
  JOIN Accounts.[User] AS Users
    ON Users.Id = TaxDeductibleCost.AffectedUserId
INNER 
  JOIN Allocation.Employee AS Employee
	ON Employee.UserId = TaxDeductibleCost.AffectedUserId
 WHERE  
    DATEFROMPARTS(TaxDeductibleCost.[Year],TaxDeductibleCost.[Month],1) >= @from AND DATEFROMPARTS(TaxDeductibleCost.[Year],TaxDeductibleCost.[Month],1) <= @to
    AND (@IsEmployeeIdsSet = 0 OR Employee.Id IN ( @employeeIds )) 