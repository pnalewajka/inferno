var ReportDataChart = function () {
    var data = function () {
        var allocatedAndReportedHours = [];

        reportData.AllocatedAndReportedHours.map(function (data) {
            var allocatedAndReportedHourItem = findOrCreateItem(data.Year, data.WeekNumber, allocatedAndReportedHours);
            addDataToItem(data.ReportedHours, data.AllocationHours, allocatedAndReportedHourItem);
        });

        return {
            Rows: allocatedAndReportedHours
        }
    }();

    function addDataToItem(reportedHours, allocationHours, allocatedAndReportedHourItem) {
        allocatedAndReportedHourItem.ReportedHours += reportedHours;
        allocatedAndReportedHourItem.AllocationHours += allocationHours;
    };

    function findOrCreateItem(year, weekNumber, itemList) {
        var item = itemList.filter(function (item) {
            return item.Year === year && item.WeekNumber === weekNumber;
        })[0];

        if (!item) {
            item = createItem(year, weekNumber);
            itemList.push(item);
        }

        return item;
    };

    function createItem(year, weekNumber) {
        return {
            Year: year,
            WeekNumber: weekNumber,
            ReportedHours: 0,
            AllocationHours: 0
        };
    };

    return {
        Data: data
    }
}();

var configurations = [
           {
               name: "Allocated and reported hours report (vertical)",
               config: {
                   type: 'bar',
                   data: {
                       labels: ReportDataChart.Data.Rows.map(function (r) {
                           return r.WeekNumber + " " + r.Year;
                       }),
                       datasets: [{
                           label: "Allocation hours",
                           backgroundColor: ChartReport.getColor(0),
                           borderColor: ChartReport.getColor(0),
                           data: ReportDataChart.Data.Rows.map(function (r) {
                               return r.AllocationHours;
                           }),
                           fill: false,
                       },
                       {
                           label: "Reported hours",
                           backgroundColor: ChartReport.getColor(1),
                           borderColor: ChartReport.getColor(1),
                           data: ReportDataChart.Data.Rows.map(function (r) {
                               return r.ReportedHours;
                           }),
                           fill: false,
                       }
                       ]
                   },
                   options: {
                       responsive: true,
                       title: {
                           display: true,
                           text: 'Allocated and reported hours report'
                       },
                       tooltips: {
                           mode: 'index',
                           intersect: false,
                       },
                       hover: {
                           mode: 'nearest',
                           intersect: true
                       },
                       scales: {
                           xAxes: [{
                               display: true,
                               scaleLabel: {
                                   display: true,
                                   labelString: 'Weeks'
                               }
                           }],
                           yAxes: [{
                               display: true,
                               scaleLabel: {
                                   display: true,
                                   labelString: 'Hours'
                               }
                           }]
                       }
                   }
               },
           },
           {
               name: "Allocated and reported hours report (horizontal)",
               config: {
                   type: 'horizontalBar',
                   data: {
                       labels: ReportDataChart.Data.Rows.map(function (r) {
                           return r.WeekNumber + " " + r.Year;
                       }),
                       datasets: [{
                           label: "Allocation hours",
                           backgroundColor: ChartReport.getColor(0),
                           borderColor: ChartReport.getColor(0),
                           data: ReportDataChart.Data.Rows.map(function (r) {
                               return r.AllocationHours;
                           }),
                           fill: false,
                       },
                       {
                           label: "Reported hours",
                           backgroundColor: ChartReport.getColor(1),
                           borderColor: ChartReport.getColor(1),
                           data: ReportDataChart.Data.Rows.map(function (r) {
                               return r.ReportedHours;
                           }),
                           fill: false,
                       }
                       ]
                   },
                   options: {
                       responsive: true,
                       title: {
                           display: true,
                           text: 'Allocated and reported hours report'
                       },
                       tooltips: {
                           mode: 'index',
                           intersect: false,
                       },
                       hover: {
                           mode: 'nearest',
                           intersect: true
                       },
                       scales: {
                           xAxes: [{
                               display: true,
                               scaleLabel: {
                                   display: true,
                                   labelString: 'Hours'
                               }
                           }],
                           yAxes: [{
                               display: true,
                               scaleLabel: {
                                   display: true,
                                   labelString: 'Weeks'
                               }
                           }]
                       }
                   }
               },
           }
];
