﻿SELECT
	   Employee.Acronym As Acronym,
	   Employee.FirstName AS FirstName,
	   Employee.LastName AS LastName,
	   Company.[Name] AS Company,
	   OrgUnit.[Name] AS EmployeeOrgUnit,
	   (SELECT ContractType.NameEn FROM TimeTracking.ContractType AS ContractType
	   INNER JOIN Allocation.EmploymentPeriod AS EmploymentPeriod
	   ON  EmploymentPeriod.ContractTypeId = ContractType.Id
	   WHERE 
	   EmploymentPeriod.StartDate <= Request.CreatedOn AND 
	   (EmploymentPeriod.EndDate >= Request.CreatedOn OR EmploymentPeriod.EndDate IS NULL) 
	   and EmploymentPeriod.EmployeeIdEmployee = Employee.Id )
	   AS ContractType,
	   MONTH(BonusRequest.EndDate) AS SettlementMonth,
	   YEAR(BonusRequest.EndDate) AS SettlementYear,
	   CAST(CAST(Request.CreatedOn AS DATE) AS nvarchar) AS SettlementDate,
	   Project.Apn AS ProjectAPN,
	   ProjectSetup.ProjectShortName + IIF(Project.SubProjectShortName IS NULL, '', ' / ' + Project.SubProjectShortName) AS ProjectName,
	   'Bonus Request' AS DataSource,
	   BonusType.Code AS PaymentType,
	   BonusRequest.Justification AS Descripition,
	   BonusRequest.BonusAmount AS NetValue,
	   Currency.IsoCode AS Unit,
	   Users.FirstName + Users.LastName AS Requester,
	   CASE WHEN BonusRequest.Reinvoice = 1 THEN 'Yes' ELSE 'No' END AS Reinvoice,
	   [Status].Code AS [Status],
	   (SELECT Stuff(
	    (SELECT N', ' + CONCAT(E.FirstName,' ', E.LastName) 
		FROM Allocation.Employee AS E	
		WHERE BonusRequest.ProjectId = Project.Id AND (E.Id = ProjectSetup.ProjectManagerId OR E.Id = ProjectSetup.SupervisorManagerId)
		FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N''))
	    AS Approvers
  FROM Workflows.BonusRequest AS BonusRequest
INNER 
  JOIN Workflows.Request AS Request
    ON Request.Id = BonusRequest.Id
INNER 
  JOIN Allocation.Employee AS Employee
	ON Employee.Id = BonusRequest.EmployeeId
INNER 
	JOIN Dictionaries.Company AS Company
	ON Company.Id = Employee.CompanyId
INNER
	JOIN Organization.OrgUnit AS OrgUnit
	ON OrgUnit.Id = Employee.OrgUnitId
INNER
	JOIN Allocation.Project AS Project
	ON Project.Id = BonusRequest.ProjectId
INNER
	JOIN Allocation.ProjectSetup AS ProjectSetup
	ON ProjectSetup.Id = Project.ProjectSetupId
INNER
	JOIN Dictionaries.Currency AS Currency
	ON Currency.Id = BonusRequest.CurrencyId
INNER
	JOIN Accounts.[User] AS Users
	ON Users.Id = Request.RequestingUserId
INNER
	JOIN Compensation.BonusType AS BonusType
	ON BonusType.[Value] = BonusRequest.[Type]
INNER
	JOIN Workflows.RequestStatus AS [Status]
	ON [Status].[Value] = Request.[Status]
 WHERE  
    Request.CreatedOn >= @from AND Request.CreatedOn <= @to
    AND (@IsEmployeeIdsSet = 0 OR BonusRequest.EmployeeId IN ( @employeeIds )) 
	AND (@IsProjectIdsSet = 0 OR BonusRequest.ProjectId IN ( @projectIds ))