﻿--declare parameters for testing easier--
--declare @from DATETIME 
--declare @to DATETIME
--declare @fromTerminated DATETIME
--declare @toTerminated DATETIME
--declare @IsCompanyIdsSet bit 
--declare @IsOrgUnitIdsSet bit
--declare @IsLocationIdsSet bit
--declare @IsToTerminatedSet bit

--set @from = null
--set @to = null
--set @fromTerminated = null
--set @toTerminated = null
--set @IsCompanyIdsSet = 'false'  
--set @IsLocationIdsSet = 'false'	
--set @IsToTerminatedSet = 'false'

SELECT  
	   Employee.Acronym AS Acronym,
	   Employee.FirstName AS FirstName,
	   Employee.LastName AS LastName,
	   Company.[Name] AS Company,
	   OrgUnit.[Name] AS OrgUnit,
	   OrgUnit.FlatName AS SubOrgunitFlat,
	   (SELECT CONCAT(EM.FirstName,' ', EM.LastName) 
		FROM Allocation.Employee AS EM
		WHERE EM.Id = Employee.LineManagerId) AS LineManager,
	   (SELECT JobTitle.NameEn FROM SkillManagement.JobTitle AS JobTitle
	    INNER JOIN Allocation.EmploymentPeriod AS E
	    ON JobTitle.id = E.JobTitleId
	    WHERE E.StartDate <= EmploymentPeriod.EndDate AND 
	    (E.EndDate >= EmploymentPeriod.EndDate OR E.EndDate IS NULL) 
	    AND E.EmployeeIdEmployee = Employee.Id ) AS JobTitle,
	    JobMatrixLevel.Code AS  Seniority,
	    JobProfile.[Name] AS  [Profile],
	   (SELECT L.[Name] FROM Dictionaries.[Location] AS L
	    WHERE L.Id = Employee.LocationId) AS [Location],
	   (SELECT P.DescriptionEn FROM Allocation.PlaceOfWork AS P
	    WHERE P.[Value] = Employee.PlaceOfWork) AS PlaceOfWork,
	   (SELECT ContractType.NameEn FROM TimeTracking.ContractType AS ContractType
	    WHERE ContractType.Id = EmploymentPeriod.ContractTypeId
	    AND EmploymentPeriod.EmployeeIdEmployee = Employee.Id
	    AND (@from IS NULL OR EmploymentPeriod.EndDate >= @from)
		AND (@to IS NULL OR EmploymentPeriod.EndDate <= @to)
	    AND (@fromTerminated IS NULL OR EmploymentPeriod.TerminatedOn >= @fromTerminated) 
		AND (@toTerminated IS NULL OR EmploymentPeriod.TerminatedOn <= @toTerminated))
	    AS ContractType,
		CONVERT(varchar(11), (SELECT temp.EndDate FROM (SELECT 
	    ep.StartDate, 
	    ep.EndDate, 
	    ep.TerminatedOn,
	    ep.EmployeeIdEmployee,
	    ep.Id,
	   (SELECT DATEDIFF(DAY, ep.EndDate, (SELECT TOP 1 nep.StartDate FROM Allocation.EmploymentPeriod nep 
	   WHERE nep.EmployeeIdEmployee = ep.EmployeeIdEmployee AND nep.StartDate >= ep.EndDate
	   ORDER BY nep.StartDate ASC) ) ) AS NextPeriodGap FROM Allocation.EmploymentPeriod ep) AS temp
	   WHERE (temp.NextPeriodGap > 1 
	   OR (temp.NextPeriodGap IS NULL AND temp.TerminatedOn IS NOT NULL)
	   OR (temp.NextPeriodGap IS NULL AND temp.EndDate IS NOT NULL AND temp.EndDate < GETDATE()))
	   AND temp.Id = EmploymentPeriod.Id), 111) AS EndDate, 
	   CONVERT(varchar(11), (SELECT temp.TerminatedOn FROM (SELECT 
	   ep.StartDate,
	   ep.EndDate, 
	   ep.TerminatedOn, 
	   ep.EmployeeIdEmployee,
	   ep.Id,
	  (SELECT DATEDIFF(DAY, ep.EndDate, (SELECT TOP 1 nep.StartDate FROM Allocation.EmploymentPeriod nep 
	   WHERE nep.EmployeeIdEmployee = ep.EmployeeIdEmployee AND nep.StartDate >= ep.EndDate
	   ORDER BY nep.StartDate ASC) ) ) AS NextPeriodGap FROM Allocation.EmploymentPeriod ep) AS temp
	   WHERE (temp.NextPeriodGap > 1 
	   OR (temp.NextPeriodGap IS NULL AND temp.TerminatedOn IS NOT NULL)
	   OR (temp.NextPeriodGap IS NULL AND temp.EndDate IS NOT NULL AND temp.EndDate < GETDATE()))
	   AND temp.Id = EmploymentPeriod.Id) ,111) AS TerminatedOn,
	   CASE WHEN EmploymentTerminationReason.IsOtherDepartureReasons = 1 THEN 'Yes' ELSE 'No' END AS VoluntarilyTurnover
    FROM Allocation.EmploymentPeriod AS  EmploymentPeriod
LEFT 
	JOIN  Allocation.Employee AS Employee
	ON Employee.Id = EmploymentPeriod.EmployeeIdEmployee
LEFT 
	JOIN Dictionaries.Company AS Company
	ON Company.Id = Employee.CompanyId
LEFT
	JOIN Organization.OrgUnit AS OrgUnit
	ON OrgUnit.Id = Employee.OrgUnitId
LEFT
	JOIN Allocation.EmployeeJobMatrixLevel AS EmployeeJobMatrixLevel
	ON EmployeeJobMatrixLevel.Employee_Id = Employee.Id
LEFT
	JOIN SkillManagement.JobMatrixLevel AS JobMatrixLevel
	ON JobMatrixLevel.Id = EmployeeJobMatrixLevel.JobMatrixLevel_Id
LEFT
	JOIN Allocation.EmployeeJobProfile AS EmployeeJobProfile
	ON EmployeeJobProfile.Employee_Id = Employee.Id
LEFT
	JOIN SkillManagement.JobProfile AS JobProfile
	ON JobProfile.Id = EmployeeJobProfile.JobProfile_Id
LEFT
	JOIN PeopleManagement.EmploymentTerminationReason AS EmploymentTerminationReason
	ON EmploymentTerminationReason.Id = EmploymentPeriod.TerminationReasonId
    WHERE  
    (@from IS NULL OR EmploymentPeriod.EndDate >= @from)
	AND (@to IS NULL OR EmploymentPeriod.EndDate <= @to)
	AND (@fromTerminated IS NULL OR EmploymentPeriod.TerminatedOn >= @fromTerminated)
	AND (@toTerminated IS NULL OR EmploymentPeriod.TerminatedOn <= @toTerminated)
    AND EmploymentPeriod.Id IN  (SELECT temp.Id FROM (SELECT 
	ep.StartDate, 
	ep.EndDate,
	ep.TerminatedOn,
	ep.EmployeeIdEmployee,
	ep.Id,
	(SELECT DATEDIFF(DAY, ep.EndDate, (SELECT TOP 1 nep.StartDate FROM Allocation.EmploymentPeriod nep 
	 WHERE nep.EmployeeIdEmployee = ep.EmployeeIdEmployee AND nep.StartDate >= ep.EndDate
	 ORDER BY nep.StartDate ASC) ) ) AS NextPeriodGap
	FROM 
	Allocation.EmploymentPeriod ep) AS temp
	WHERE (temp.NextPeriodGap > 1 
	OR (temp.NextPeriodGap IS NULL AND temp.TerminatedOn IS NOT NULL)
	OR (temp.NextPeriodGap IS NULL AND temp.EndDate IS NOT NULL AND temp.EndDate < GETDATE()))
	AND temp.Id = EmploymentPeriod.Id)
	AND (@IsCompanyIdsSet = 0 OR Employee.CompanyId IN ( @companyIds )) 
	AND (@IsOrgUnitIdsSet = 0 OR Employee.OrgUnitId IN ( @orgUnitIds ))
	AND (@IsLocationIdsSet = 0 OR Employee.LocationId IN ( @locationIds ))