﻿﻿pivot.dataConverter = function (data) {
    return data.Records.map(function (t) {
        return t;
    });
};

pivot.configurations = [
    {
        name: " Bonus Request Report",
        code: "B1",
        config: {
            rows: ["Acronym", "FirstName", "LastName", "Company", "EmployeeOrgUnit", "ContractType", "SettlementMonth", "SettlementYear", "SettlementDate",
                "ProjectAPN", "ProjectName", "DataSource", "PaymentType", "Descripition", "NetValue", "Unit", "Requester", "Approvers", "Reinvoice", "Status"],
            cols: [],
            vals: [],
            aggregatorName: "First",
            rowOrder: "key_a_to_z",
            colOrder: "key_a_to_z",
        }
    },
];