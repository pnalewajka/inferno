﻿using System;
using Castle.Core.Logging;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.Business.TimeTracking.Jobs
{
    [Identifier("Job.TimeTracking.SendTimeReportsToControlling")]
    [JobDefinition(
        Code = "SendTimeReportsToControllingJob",
        Description = "Send time reports to controlling",
        ScheduleSettings = "0,30 * * * * *",
        MaxExecutioPeriodInSeconds = 1800,
        PipelineName = PipelineCodes.TimeTracking)]
    public sealed class SendTimeReportsToControllingJob : IJob
    {        
        private readonly ILogger _logger;
        private readonly ITimeReportControllingSynchronizationService _timeReportControllingSynchronizationService;

        public SendTimeReportsToControllingJob(
            ILogger logger,
            ITimeReportControllingSynchronizationService timeReportControllingSynchronizationService)
        {
            _logger = logger;
            _timeReportControllingSynchronizationService = timeReportControllingSynchronizationService;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            try
            {
                _timeReportControllingSynchronizationService.SynchronizeMarkedTimeReports();
            }
            catch (Exception ex)
            {
                _logger.Error("Synchronizing time reports with Controlling failed", ex);
                return JobResult.Fail();
            }

            return JobResult.Success();
        }
    }
}
