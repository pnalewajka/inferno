﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using Castle.Core.Logging;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Enums;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Jobs
{
    [Identifier("Job.TimeTracking.TimeReportReminder")]
    [JobDefinition(
        Code = "TimeReportReminder",
        Description = "Send email if time report is not completed",
        ScheduleSettings = "0 0 4 * * *",
        MaxExecutioPeriodInSeconds = 3600,
        PipelineName = PipelineCodes.TimeTracking)]
    public class TimeReportReminderJob : IJob
    {
        private readonly ILogger _logger;
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService;
        private readonly ITimeService _timeService;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly ISystemParameterService _parameterService;

        public TimeReportReminderJob(
            ILogger logger,
            IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
            IReliableEmailService reliableEmailService,
            IMessageTemplateService messageTemplateService,
            ISystemParameterService parameterService,
            ITimeService timeService)
        {
            _logger = logger;
            _unitOfWorkService = unitOfWorkService;
            _timeService = timeService;
            _reliableEmailService = reliableEmailService;
            _messageTemplateService = messageTemplateService;
            _parameterService = parameterService;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            try
            {
                var dayFromLastInMonthToRemind = _parameterService.GetParameter<int>(ParameterKeys.DayFromLastInMonthToRemind);

                var currentDate = _timeService.GetCurrentDate();
                var previousMonthDate = currentDate.AddMonths(-1);

                var currentMonthRemindingDate = currentDate.GetLastDayInMonth().AddDays(dayFromLastInMonthToRemind);
                var previousMonthRemindingDate = previousMonthDate.GetLastDayInMonth().AddDays(dayFromLastInMonthToRemind);

                if (currentMonthRemindingDate <= currentDate)
                {
                    var reportDate = currentDate.GetLastDayInMonth();
                    var employees = GetOverdueEmployees(currentDate.Year, currentDate.Month);

                    RemindAboutTimeReport(reportDate, employees);
                }

                if (previousMonthRemindingDate <= currentDate)
                {
                    var reportDate = currentDate.GetLastDayInMonth().AddMonths(-1);
                    var employees = GetOverdueEmployees(previousMonthDate.Year, previousMonthDate.Month);

                    RemindAboutTimeReport(reportDate, employees);
                }

                return JobResult.Success();
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);

                return JobResult.Fail();
            }
        }

        private IList<EmployeeData> GetOverdueEmployees(int year, int month)
        {
            var beginingOfTheMonth = new DateTime(year, month, 1);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var timeReports = unitOfWork
                    .Repositories
                    .TimeReports
                    .Include(r => r.Employee)
                    .Include(r => r.Employee.EmploymentPeriods)
                    .Where(t => (t.Status == TimeReportStatus.NotStarted || t.Status == TimeReportStatus.Draft || t.Status == TimeReportStatus.Rejected) && t.Month == month && t.Year == year);

                var employees =
                    timeReports.Where(tr => tr.Employee.EmploymentPeriods.Any(
                        p => p.StartDate <= beginingOfTheMonth &&
                             (p.EndDate == null || p.EndDate.Value >= beginingOfTheMonth) &&
                             p.TimeReportingMode != TimeReportingMode.NoTracking && p.IsActive))
                             .Select(tr => new EmployeeData
                             {
                                 FirstName = tr.Employee.FirstName,
                                 EmailAddress = tr.Employee.Email
                             });

                return employees.ToList();
            }
        }

        private void RemindAboutTimeReport(DateTime reportDate, IList<EmployeeData> employeesToNotify)
        {
            var serverAddress = _parameterService.GetParameter<string>(ParameterKeys.ServerAddress);

            foreach (var employee in employeesToNotify)
            {
                if (!string.IsNullOrWhiteSpace(employee.EmailAddress))
                {
                    var email = new EmailMessageDto();
                    var recipientsCulture = new CultureInfo("en-US");

                    var model = new TimeReportReminderDto
                    {
                        ReportDate = reportDate,
                        EmployeeFirstName = employee.FirstName,
                        ServerAddress = serverAddress
                    };

                    using (CultureInfoHelper.SetCurrentCulture(recipientsCulture)) // Date, numbers, etc.. formatting
                    using (CultureInfoHelper.SetCurrentUICulture(recipientsCulture)) // Resource language selection
                    {
                        var emailSubject =
                            _messageTemplateService.ResolveTemplate(
                                TemplateCodes.TimeTrackingTimeReportReminderJobSubject,
                                model);
                        var emailBody =
                            _messageTemplateService.ResolveTemplate(TemplateCodes.TimeTrackingTimeReportReminderJobBody,
                                model);

                        email.Subject = emailSubject.Content;
                        email.IsHtml = emailBody.IsHtml;
                        email.Body = emailBody.Content;
                    }
                    email.Recipients.Add(new EmailRecipientDto(employee.EmailAddress, null));
                    _reliableEmailService.EnqueueMessage(email);
                }
            }
        }

        private class EmployeeData
        {
            public string FirstName { get; set; }
            public string EmailAddress { get; set; }
        }
    }
}

