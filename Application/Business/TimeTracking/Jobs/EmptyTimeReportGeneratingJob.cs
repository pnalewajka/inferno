﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.TimeTracking.Jobs
{
    [Identifier("Job.TimeTracking.GenerateEmptyTimeReport")]
    [JobDefinition(
        Code = "GenerateEmptyTimeReport",
        Description = "Generate empty time report",
        ScheduleSettings = "0 0 2 * * *",
        MaxExecutioPeriodInSeconds = 1800,
        PipelineName = PipelineCodes.TimeTracking)]
    public class EmptyTimeReportGeneratingJob : IJob
    {
        private readonly IUnitOfWorkService<ITimeTrackingDbScope> _unitOfWorkService;
        private readonly ITimeService _timeService;

        public EmptyTimeReportGeneratingJob(
            IUnitOfWorkService<ITimeTrackingDbScope> unitOfWorkService,
            ITimeService timeService)
        {
            _unitOfWorkService = unitOfWorkService;
            _timeService = timeService;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            var currentDate = _timeService.GetCurrentDate();
            
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employeesToGenerate = unitOfWork.Repositories.Employees.Where(e =>
                    e.EmploymentPeriods.Any(
                        ep =>
                            ep.IsActive
                            && ep.TimeReportingMode != TimeReportingMode.NoTracking
                            && ep.StartDate <= currentDate && (!ep.EndDate.HasValue || ep.EndDate.Value >= currentDate))
                    && !e.TimeReports.Any(tr => tr.Year == currentDate.Year && tr.Month == (byte)currentDate.Month)).Select(e => e.Id).ToList();

                foreach (var employeeId in employeesToGenerate.TakeWhileNotCancelled(executionContext.CancellationToken))
                {
                    var timeReport = new TimeReport
                    {
                        EmployeeId = employeeId,
                        Year = currentDate.Year,
                        Month = (byte) currentDate.Month,
                        Status = TimeReportStatus.NotStarted,
                        Rows = new List<TimeReportRow>()
                    };

                    unitOfWork.Repositories.TimeReports.Add(timeReport);
                }

                unitOfWork.Commit();
            }

            return JobResult.Success();
        }
    }
}
