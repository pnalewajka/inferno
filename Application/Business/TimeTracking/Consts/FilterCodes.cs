﻿namespace Smt.Atomic.Business.TimeTracking.Consts
{
    public static class FilterCodes
    {
        public const string AllVacations = "all-vacations";
        public const string MyEmployeeVacations = "my-employee-vacations";
        public const string MyDirectEmployeeVacations = "my-direct-employee-vacations";
        public const string MyVacations = "my-vacations";
        public const string MyEmployees = "my-employees";
        public const string MyDirectEmployees = "my-direct-employees";
        public const string MyTimeReports = "my-reports";
        public const string AllEmployeesTimeReports = "all-employees";
        public const string InProgressReports = "progress-in-progress";
        public const string CompletedReports = "progress-completed";
        public const string OrgUnitsFilter = "org-units-filter";
        public const string CompanyFilter = "company-filter";
        public const string MyProjects = "my-projects";
        public const string MyAbsenceCalendars = "my-calendars";
        public const string AllAbsenceCalendars = "all-calendars";
        public const string ContractTypeFilter = "contract-type";
    }
}