﻿using System;
using System.Linq;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;

namespace Smt.Atomic.Business.TimeTracking.BusinessLogics
{
    using AllocationBusinessLogic = Data.Entities.BusinessLogic.ProjectBusinessLogic;

    public static class ProjectBusinessLogic
    {
        public static readonly BusinessLogic<Project, bool> IsAbsenceProject = new BusinessLogic<Project, bool>(
            project => project.TimeTrackingType == TimeTrackingProjectType.Absence);

        public static BusinessLogic<Project, bool> IsProjectActiveWithinGivenMonth(int year, byte month)
        {
            var beginningOfMonth = DateHelper.BeginningOfMonth(year, month);
            var endOfMonth = DateHelper.EndOfMonth(year, month);

            return new BusinessLogic<Project, bool>(IsProjectActiveWithinGivenDate.Parametrize(beginningOfMonth, endOfMonth));
        }

        public static BusinessLogic<Project, DateTime, DateTime, bool> IsProjectActiveWithinGivenDate = new BusinessLogic<Project, DateTime, DateTime, bool>(
            (project, from, to) => project.StartDate <= to && (project.EndDate >= from || project.EndDate == null));

        public static BusinessLogic<Project, bool> IsAllowedProject(int year, byte month, bool canAddAbsenceManually = true)
        {
            var isProjectActiveBusinessLogic = IsProjectActiveWithinGivenMonth(year, month);
            var activeProjectsWithKeys = new BusinessLogic<Project, bool>(
                project => AllocationBusinessLogic.IsProjectWithKey.Call(project) && isProjectActiveBusinessLogic.Call(project));

            return canAddAbsenceManually
                ? new BusinessLogic<Project, bool>(project => activeProjectsWithKeys.Call(project) || IsAbsenceProject.Call(project))
                : new BusinessLogic<Project, bool>(project => activeProjectsWithKeys.Call(project) && !IsAbsenceProject.Call(project));
        }

        public static readonly BusinessLogic<IQueryable<TimeReportRow>, TimeReportStatus> GetProjectStatus
            = new BusinessLogic<IQueryable<TimeReportRow>, TimeReportStatus>(
                projectRows => projectRows.All(r => r.Status == TimeReportStatus.Accepted)
                            ? TimeReportStatus.Accepted
                            : projectRows.Any(r => r.Status == TimeReportStatus.Rejected)
                                ? TimeReportStatus.Rejected
                                : projectRows.Any(r => r.Status == TimeReportStatus.Submitted)
                                    ? TimeReportStatus.Submitted
                                    : TimeReportStatus.Draft);
    }
}
