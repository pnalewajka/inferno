﻿using System.Runtime.Serialization;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.Compensation.BusinessEvents
{
    [DataContract]
    public class PaymentApprovedEvent : BusinessEvent
    {
        [DataMember]
        public long? BusinessTripSettlementRequestId { get; set; }

        [DataMember]
        public long? ExpenseRequestId { get; set; }

        public PaymentApprovedEvent(ExpenseRequestDto requestDto)
        {
            ExpenseRequestId = requestDto.Id;
        }

        public PaymentApprovedEvent(BusinessTripSettlementRequestDto requestDto)
        {
            BusinessTripSettlementRequestId = requestDto.Id;
        }
    }
}
