﻿using System.Runtime.Serialization;
using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.Compensation.BusinessEvents
{
    [DataContract]
    public class CompensationChangedEvent : BusinessEvent
    {
        [DataMember]
        public long EmployeeId { get; set; }

        [DataMember]
        public int Year { get; set; }

        [DataMember]
        public byte Month { get; set; }

        public CompensationChangedEvent()
        {
        }

        public CompensationChangedEvent(long employeeId, int year, byte month)
        {
            EmployeeId = employeeId;
            Year = year;
            Month = month;
        }
    }
}
