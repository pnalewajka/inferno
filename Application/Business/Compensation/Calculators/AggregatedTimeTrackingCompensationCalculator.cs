﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.Business.Compensation.Resources;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Business.Compensation.Calculators
{
    public class AggregatedTimeTrackingCompensationCalculator : ICalculator
    {
        private ICalculator[] _calculators;

        public AggregatedTimeTrackingCompensationCalculator(ICalculator[] calculators)
        {
            _calculators = calculators;
        }

        public CalculationResult Calculate()
        {
            var calculatorResults = _calculators.Select(c => c.Calculate()).ToArray();
            var costItems = calculatorResults.SelectMany(r => r.CostItems);

            if (costItems.DistinctBy(i => i.CurrencyIsoCode).Count() > 1)
            {
                throw new BusinessException(CalculatorResources.CurrenciesDontMatch);
            }

            var result = new CalculationResult
            {
                CostItems = costItems
                    .Where(i => i.Group != CostItemGroupType.TimeTrackingTotal
                             && i.Group != CostItemGroupType.AdditionalCostsTotal)
                    .ToList(),
                DiagnosticItems = calculatorResults.SelectMany(r => r.DiagnosticItems).ToList()
            };

            if (costItems.Any())
            {
                var timeTrackingTotalCostItem = new CostItem
                {
                    CurrencyIsoCode = costItems.Select(i => i.CurrencyIsoCode).First(),
                    Group = CostItemGroupType.TimeTrackingTotal,
                    Value = costItems.Where(i => i.Group == CostItemGroupType.TimeTrackingTotal).Sum(i => i.Value)
                };

                var additionalCostsTotalCostItem = new CostItem
                {
                    CurrencyIsoCode = costItems.Select(i => i.CurrencyIsoCode).First(),
                    Group = CostItemGroupType.AdditionalCostsTotal,
                    Value = costItems.Where(i => i.Group == CostItemGroupType.AdditionalCostsTotal).Sum(i => i.Value)
                };

                result.AddCostItem(timeTrackingTotalCostItem);
                result.AddCostItem(additionalCostsTotalCostItem);
            }

            return result;
        }
    }
}
