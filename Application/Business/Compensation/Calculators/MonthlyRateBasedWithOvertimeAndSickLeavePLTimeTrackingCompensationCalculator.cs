﻿using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;

namespace Smt.Atomic.Business.Compensation.Calculators
{
    public class MonthlyRateBasedWithOvertimeAndSickLeavePLTimeTrackingCompensationCalculator : BasePLTimeTrackingCompensationCalculator
    {
        public MonthlyRateBasedWithOvertimeAndSickLeavePLTimeTrackingCompensationCalculator(
            ISystemParameterService systemParameterService,
            TimeReport timeReport,
            EmploymentPeriod employmentPeriod,
            TimeTrackingCompensationCalculatorOptions calculatorOptions,
            ICalculatorCurrencyExchangeRateService calculatorCurrencyExchangeRateService)
            : base(systemParameterService, timeReport, employmentPeriod, calculatorOptions, calculatorCurrencyExchangeRateService)
        {
        }

        public override CalculationResult Calculate()
        {
            var result = base.Calculate();

            result.AddCostItem(CalculateMonthlyCompensation());
            result.AddCostItem(CalculateOvertimeBonus());
            result.AddCostItem(CalculateSickLeaveCompensation());
            result.AddCostItem(CalculateMonthlyBonus(CurrencyIsoCodes.PolishZloty));
            result.AddCostItem(CalculateExpenses(CurrencyIsoCodes.PolishZloty));
            result.AddCostItem(CalculateAdditionalCostsTotal(result.CostItems));
            result.AddCostItem(CalculateTimeTrackingTotal(result.CostItems));

            return result;
        }
    }
}
