﻿using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.Interfaces;

namespace Smt.Atomic.Business.Compensation.Calculators
{
    public class EmptyTimeTrackingCompensationCalculator : ICalculator
    {
        public CalculationResult Calculate()
        {
            return new CalculationResult();
        }
    }
}
