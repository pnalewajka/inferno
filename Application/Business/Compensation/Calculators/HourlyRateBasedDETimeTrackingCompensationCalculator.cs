﻿using System;
using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;

namespace Smt.Atomic.Business.Compensation.Calculators
{
    public class HourlyRateBasedDETimeTrackingCompensationCalculator : BaseTimeTrackingCompensationCalculator
    {
        private readonly ICalculatorCurrencyExchangeRateService _calculatorCurrencyExchangeRateService;

        public HourlyRateBasedDETimeTrackingCompensationCalculator(TimeReport timeReport,
            EmploymentPeriod employmentPeriod,
            TimeTrackingCompensationCalculatorOptions calculatorOptions,
            ICalculatorCurrencyExchangeRateService calculatorCurrencyExchangeRateService)
            : base(timeReport, employmentPeriod, calculatorOptions)
        {
            _calculatorCurrencyExchangeRateService = calculatorCurrencyExchangeRateService;
        }

        protected override CurrencyExchangeRateDto GetCurrencyExchangeRate(string currencyIsoCode, DateTime date) =>
            _calculatorCurrencyExchangeRateService.GetExchangeRateToEur(currencyIsoCode, date);
    }
}
