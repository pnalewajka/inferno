﻿using System.Collections.Generic;
using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.DiagnosticItems;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Compensation.Calculators
{
    public abstract class BaseBusinessTripCompensationCalculator : ICalculator
    {
        protected readonly BusinessTripSettlementRequest SettlementRequest;
        protected readonly EmploymentPeriod[] EmploymentPeriods;

        protected BaseBusinessTripCompensationCalculator(BusinessTripSettlementRequest settlementRequest, EmploymentPeriod[] employmentPeriods)
        {
            SettlementRequest = settlementRequest;
            EmploymentPeriods = employmentPeriods;
        }

        public CalculationResult Calculate()
        {
            var result = new CalculationResult();

            // skip calculation for empty settlements
            if (!SettlementRequest.IsEmpty)
            {
                PerformCalculation(result);
            }

            result.AddCostItem(CalculateBusinessTripCompensationTotal(result.CostItems));
            result.AddCostItem(CalculateBusinessTripExpensesTotal(result.CostItems));

            return result;
        }

        public abstract CostItem CalculateBusinessTripCompensationTotal(IEnumerable<CostItem> costItems);

        public abstract CostItem CalculateBusinessTripExpensesTotal(IEnumerable<CostItem> costItems);

        public virtual void PerformCalculation(CalculationResult result)
        {
            if (EmploymentPeriods.Length > 1)
            {
                result.AddSummaryDiagnosticItem(new MultipleEmploymentPeriodsWarningDiagnosticItem
                {
                    Group = DiagnosticGroupType.Summary
                });
            }
        }
    }
}
