﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Compensation.BusinessLogics;
using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.DiagnosticItems;
using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.Business.Compensation.Resources;
using Smt.Atomic.CrossCutting.Common.Comparers;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Compensation;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Compensation.Calculators
{
    public abstract class BaseTimeTrackingCompensationCalculator : ICalculator
    {
        protected readonly TimeReport TimeReport;
        protected readonly EmploymentPeriod EmploymentPeriod;
        protected readonly TimeTrackingCompensationCalculatorOptions CalculatorOptions;

        protected BaseTimeTrackingCompensationCalculator(
            TimeReport timeReport,
            EmploymentPeriod employmentPeriod,
            TimeTrackingCompensationCalculatorOptions calculatorOptions)
        {
            TimeReport = timeReport;
            EmploymentPeriod = employmentPeriod;
            CalculatorOptions = calculatorOptions;
        }

        public virtual CalculationResult Calculate()
        {
            var result = new CalculationResult();

            result.AddSummaryDiagnosticItem(new ContractTypeIdDiagnosticItem
            {
                Group = DiagnosticGroupType.Summary,
                ContractTypeId = EmploymentPeriod.ContractTypeId
            });

            return result;
        }

        public virtual CostItem CalculateMonthlyBonus(string currencyIsoCode)
        {
            if (!CalculatorOptions.HasFlag(TimeTrackingCompensationCalculatorOptions.IsFirstWorkingPeriod))
            {
                return null;
            }

            var activeBonuses = GetActiveBonuses();

            if (!activeBonuses.Any())
            {
                return null;
            }

            var result = new CostItem
            {
                Group = CostItemGroupType.MonthlyBonus,
                CurrencyIsoCode = currencyIsoCode
            };

            foreach (var activeBonus in activeBonuses)
            {
                if (activeBonus.Currency.IsoCode != currencyIsoCode)
                {
                    throw new BusinessException(string.Format(CalculatorResources.BonusInWrongCurrency,
                        activeBonus.Currency.IsoCode, currencyIsoCode));
                }

                result.Value += activeBonus.BonusAmount;
                result.AddDiagnosticItem(new MonthlyBonusDiagnosticItem
                {
                    Group = DiagnosticGroupType.MonthlyBonus,
                    Bonus = CurrencyAmount.CreateValueAmount(activeBonus.BonusAmount, activeBonus.Currency.IsoCode),
                    StartDate = activeBonus.StartDate,
                    EndDate = activeBonus.EndDate,
                    Justification = activeBonus.Justification
                });
            }

            var bonusesByProject = activeBonuses.GroupBy(b => b.Project, new ByKeyEqualityComparer<Project, long>(p => p.Id));

            foreach (var projectBonuses in bonusesByProject)
            {
                var project = projectBonuses.Key;

                foreach (var bonus in projectBonuses.GroupBy(b => b.Type))
                {
                    result.AddDiagnosticItem(new AccountingReportDiagnosticItem
                    {
                        Group = DiagnosticGroupType.MonthlyBonus,
                        Amount = 1,
                        AmountUnit = Units.Unit,
                        UnitPrice = bonus.Sum(b => b.BonusAmount),
                        UnitPriceCurrency = currencyIsoCode,
                        PaymentType = bonus.Key.GetLocalizedDescription().English,
                        ProjectName = ProjectBusinessLogic.ClientProjectName.Call(project),
                        ProjectApn = project.Apn,
                    });
                }
            }

            return result;
        }

        private IEnumerable<Bonus> GetActiveBonuses()
        {
            var begginingOfMonth = DateHelper.BeginningOfMonth(TimeReport.Year, TimeReport.Month);
            var endOfMonth = DateHelper.EndOfMonth(TimeReport.Year, TimeReport.Month);
            var bonusIsActiveForPeriod = BonusBusinessLogic.BonusIsActiveForPeriod(begginingOfMonth, endOfMonth);

            return EmploymentPeriod.Employee.Bonuses.Where(bonusIsActiveForPeriod.GetFunction()).ToList();
        }

        public virtual CostItem CalculateExpenses(string currencyIsoCode)
        {
            if (!CalculatorOptions.HasFlag(TimeTrackingCompensationCalculatorOptions.IsFirstWorkingPeriod))
            {
                return null;
            }

            var activeExpenses = GetActiveExpenses();

            if (!activeExpenses.Any())
            {
                return null;
            }

            var result = new CostItem
            {
                Group = CostItemGroupType.Expense,
                CurrencyIsoCode = currencyIsoCode
            };

            foreach (var activeExpense in activeExpenses)
            {
                var exchangeRate = GetCurrencyExchangeRate(activeExpense.Currency.IsoCode, activeExpense.Date);
                var calculatedResult = activeExpense.Amount * exchangeRate.Rate;

                result.Value += calculatedResult;

                result.AddDiagnosticItem(new ExpenseDiagnosticItem
                {
                    Group = DiagnosticGroupType.Expense,
                    Date = activeExpense.Date,
                    Expense = CurrencyAmount.CreateValueAmount(activeExpense.Amount, activeExpense.Currency.IsoCode),
                    CalculatedExpense = CurrencyAmount.CreateValueAmount(calculatedResult, currencyIsoCode)
                });

                if (activeExpense.Currency.IsoCode != currencyIsoCode)
                {
                    result.AddDiagnosticItem(new CurrencyExchangeRateDiagnosticItem
                    {
                        Group = DiagnosticGroupType.Expense,
                        CurrencyIsoCode = activeExpense.Currency.IsoCode,
                        Date = exchangeRate.Date,
                        Rate = exchangeRate.Rate,
                        TableNumber = exchangeRate.TableNumber
                    });
                }

                result.AddDiagnosticItem(new AccountingReportDiagnosticItem
                {
                    Group = DiagnosticGroupType.Expense,
                    Amount = 1,
                    AmountUnit = Units.Unit,
                    UnitPrice = calculatedResult,
                    UnitPriceCurrency = result.CurrencyIsoCode,
                    PaymentType = "Expense",
                    ProjectName = ProjectBusinessLogic.ClientProjectName.Call(activeExpense.Project),
                    ProjectApn = activeExpense.Project.Apn,
                });
            }

            return result;
        }

        protected abstract CurrencyExchangeRateDto GetCurrencyExchangeRate(string currencyIsoCode, DateTime date);

        private IEnumerable<ExpenseRequest> GetActiveExpenses()
        {
            var begginingOfMonth = DateHelper.BeginningOfMonth(TimeReport.Year, TimeReport.Month);
            var endOfMonth = DateHelper.EndOfMonth(TimeReport.Year, TimeReport.Month);
            var expensesIsActiveForPeriod = DateRangeHelper.IsDateInRange<ExpenseRequest>(begginingOfMonth, endOfMonth, e => e.Date);

            return EmploymentPeriod.Employee.ExpenseRequests
                .Where(expensesIsActiveForPeriod.GetFunction())
                .Where(e => e.Request.Status == RequestStatus.Completed)
                .ToList();
        }
    }
}