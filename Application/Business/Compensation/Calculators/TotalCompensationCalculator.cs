﻿using System.Linq;
using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.Business.Compensation.Resources;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Business.Compensation.Calculators
{
    public class TotalCompensationCalculator : ICalculator
    {
        private ICalculator _timeTrackingCalculator;
        private ICalculator[] _businessTripCalculators;

        public TotalCompensationCalculator(
            ICalculator timeTrackingCalculator,
            ICalculator[] businessTripCalculators)
        {
            _timeTrackingCalculator = timeTrackingCalculator;
            _businessTripCalculators = businessTripCalculators;
        }

        public CalculationResult Calculate()
        {
            var timeTrackingResult = _timeTrackingCalculator.Calculate();
            var businessTripResults = _businessTripCalculators.Select(c => c.Calculate()).ToArray();

            var result = new CalculationResult
            {
                CostItems = timeTrackingResult.CostItems
                    .Concat(businessTripResults.SelectMany(r => r.CostItems))
                    .ToList(),
                DiagnosticItems = timeTrackingResult.DiagnosticItems
                    .Concat(businessTripResults.SelectMany(r => r.DiagnosticItems))
                    .ToList(),
            };

            if (!result.CostItems.Select(i => i.CurrencyIsoCode).IsSingleOrEmpty())
            {
                throw new BusinessException(CalculatorResources.CurrenciesDontMatch);
            }

            var totalGroups = new[]
            {
                CostItemGroupType.BusinessTripCompensationTotal,
                CostItemGroupType.TimeTrackingTotal,
                CostItemGroupType.AdditionalCostsTotal,
            };

            var totalCostItems = result.CostItems
                .Where(i => totalGroups.Contains(i.Group))
                .ToList();

            if (totalCostItems.Any())
            {
                result.AddCostItem(new CostItem
                {
                    CurrencyIsoCode = totalCostItems.Select(i => i.CurrencyIsoCode).First(),
                    Group = CostItemGroupType.CompensationTotal,
                    Value = totalCostItems.Sum(i => i.Value),
                });
            }

            return result;
        }
    }
}
