﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.DiagnosticItems;
using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.Business.Compensation.Helpers;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.Business.Compensation.Resources;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Compensation.Calculators
{
    public class EmploymentContractUKBusinessTripCompensationCalculator : BaseBusinessTripCompensationCalculator
    {
        private readonly ICalculatorCurrencyExchangeRateService _calculatorCurrencyExchangeRateService;

        public EmploymentContractUKBusinessTripCompensationCalculator(
            ICalculatorCurrencyExchangeRateService calculatorCurrencyExchangeRateService,
            BusinessTripSettlementRequest settlementRequest,
            EmploymentPeriod[] employmentPeriods)
            : base(settlementRequest, employmentPeriods)
        {
            _calculatorCurrencyExchangeRateService = calculatorCurrencyExchangeRateService;
        }

        public override void PerformCalculation(CalculationResult result)
        {
            base.PerformCalculation(result);

            result.AddCostItem(CalculateOwnCosts());
            result.AddCostItem(CalculateCompanyCosts());
            result.AddCostItem(CalculateArrangements());
        }

        protected CostItem CalculateOwnCosts()
        {
            if (!SettlementRequest.OwnCosts.Any())
            {
                return null;
            }

            var result = new CostItem
            {
                Group = CostItemGroupType.OwnCosts,
                CurrencyIsoCode = CurrencyIsoCodes.PoundSterling
            };

            foreach (var ownCost in SettlementRequest.OwnCosts)
            {
                var accountingDate = ownCost.HasInvoiceIssued ? ownCost.TransactionDate : SettlementRequest.SettlementDate.Value;
                var exchangeRate = _calculatorCurrencyExchangeRateService.GetExchangeRateToGbp(ownCost.Currency.IsoCode, accountingDate);
                var calculatedCost = ownCost.Amount * exchangeRate.Rate;

                result.Value += calculatedCost;

                result.AddDiagnosticItem(new CostCurrencyRateMultiplicationDiagnosticItem
                {
                    Group = DiagnosticGroupType.OwnCosts,
                    Name = ownCost.Description,
                    CostValue = CurrencyAmount.CreateValueAmount(ownCost.Amount, ownCost.Currency.IsoCode),
                    Rate = exchangeRate.Rate,
                    CalculatedValue = CurrencyAmount.CreateValueAmount(calculatedCost, CurrencyIsoCodes.PoundSterling)
                });

                if (ownCost.Currency.IsoCode != CurrencyIsoCodes.PoundSterling)
                {
                    result.AddDiagnosticItem(new CurrencyExchangeRateDiagnosticItem
                    {
                        Group = DiagnosticGroupType.OwnCosts,
                        CurrencyIsoCode = ownCost.Currency.IsoCode,
                        Date = exchangeRate.Date,
                        Rate = exchangeRate.Rate,
                        TableNumber = exchangeRate.TableNumber
                    });
                }
            }

            return result;
        }

        protected CostItem CalculateCompanyCosts()
        {
            if (!SettlementRequest.CompanyCosts.Any())
            {
                return null;
            }

            var result = new CostItem
            {
                Group = CostItemGroupType.CompanyCosts,
                CurrencyIsoCode = CurrencyIsoCodes.PolishZloty
            };

            foreach (var companyCost in SettlementRequest.CompanyCosts)
            {
                var accountingDate = companyCost.HasInvoiceIssued ? companyCost.TransactionDate : SettlementRequest.SettlementDate.Value;
                var exchangeRate = _calculatorCurrencyExchangeRateService.GetExchangeRateToGbp(companyCost.Currency.IsoCode, accountingDate);
                var calculatedCost = companyCost.Amount * exchangeRate.Rate;

                result.Value += calculatedCost;

                result.AddDiagnosticItem(new CostCurrencyRateMultiplicationDiagnosticItem
                {
                    Group = DiagnosticGroupType.CompanyCosts,
                    Name = companyCost.Description,
                    CostValue = CurrencyAmount.CreateValueAmount(companyCost.Amount, companyCost.Currency.IsoCode),
                    Rate = exchangeRate.Rate,
                    CalculatedValue = CurrencyAmount.CreateValueAmount(calculatedCost, CurrencyIsoCodes.PolishZloty)
                });

                if (companyCost.Currency.IsoCode != CurrencyIsoCodes.PolishZloty)
                {
                    result.AddDiagnosticItem(new CurrencyExchangeRateDiagnosticItem
                    {
                        Group = DiagnosticGroupType.CompanyCosts,
                        CurrencyIsoCode = companyCost.Currency.IsoCode,
                        Date = exchangeRate.Date,
                        Rate = exchangeRate.Rate,
                        TableNumber = exchangeRate.TableNumber
                    });
                }
            }

            return result;
        }

        protected CostItem CalculateAdvancePayment()
        {
            var advancePaymentRequests = SettlementRequest.BusinessTripParticipant.AdvancedPaymentRequests
                .Where(r => r.Request.Status == RequestStatus.Completed)
                .ToList();

            if (!advancePaymentRequests.Any())
            {
                return null;
            }

            var result = new CostItem
            {
                Group = CostItemGroupType.AdvancePayments,
                CurrencyIsoCode = CurrencyIsoCodes.PolishZloty
            };

            foreach (var advancePayment in advancePaymentRequests)
            {
                var exchangeRate = _calculatorCurrencyExchangeRateService.GetExchangeRateToGbp(
                    advancePayment.Currency.IsoCode, SettlementRequest.SettlementDate.Value);
                var calculatedValue = advancePayment.Amount * exchangeRate.Rate;

                result.Value += calculatedValue;

                result.AddDiagnosticItem(new CurrencyRateMultiplicationDiagnosticItem
                {
                    Group = DiagnosticGroupType.AdvancePayment,
                    ForeignValue = CurrencyAmount.CreateValueAmount(advancePayment.Amount, advancePayment.Currency.IsoCode),
                    Rate = exchangeRate.Rate,
                    CalculatedValue = CurrencyAmount.CreateValueAmount(calculatedValue, CurrencyIsoCodes.PolishZloty)
                });
            }

            return result;
        }

        protected CostItem CalculateAdditionalAdvancePayment()
        {
            if (!SettlementRequest.AdditionalAdvancePayments.Any())
            {
                return null;
            }

            var result = new CostItem
            {
                Group = CostItemGroupType.AdditionalAdvancePayment,
                CurrencyIsoCode = CurrencyIsoCodes.PolishZloty
            };

            foreach (var advancePayment in SettlementRequest.AdditionalAdvancePayments)
            {
                var exchangeRate = _calculatorCurrencyExchangeRateService.GetExchangeRateToGbp(
                    advancePayment.Currency.IsoCode, advancePayment.WithdrawnOn);
                var calculatedValue = advancePayment.Amount * exchangeRate.Rate;

                result.Value += calculatedValue;

                result.AddDiagnosticItem(new CurrencyRateMultiplicationDiagnosticItem
                {
                    Group = DiagnosticGroupType.AdditionalAdvancePayment,
                    ForeignValue = CurrencyAmount.CreateValueAmount(advancePayment.Amount, advancePayment.Currency.IsoCode),
                    Rate = exchangeRate.Rate,
                    CalculatedValue = CurrencyAmount.CreateValueAmount(calculatedValue, CurrencyIsoCodes.PolishZloty)
                });
            }

            return result;
        }

        protected CostItem CalculateArrangements()
        {
            var arrangements = SettlementRequest.BusinessTripParticipant.BusinessTrip.Arrangements
                .Where(a => !a.Participants.Any() || a.Participants.Any(p => p.Id == SettlementRequest.BusinessTripParticipantId))
                .ToList();

            if (!arrangements.Any())
            {
                return null;
            }

            var result = new CostItem
            {
                Group = CostItemGroupType.Arrangements,
                CurrencyIsoCode = CurrencyIsoCodes.PolishZloty
            };

            foreach (var arrangement in arrangements.Where(a => a.Value.HasValue))
            {
                var accountingDate = arrangement.ArrangementType.HasInvoice() ? arrangement.IssuedOn : SettlementRequest.SettlementDate.Value;
                var exchangeRate = _calculatorCurrencyExchangeRateService.GetExchangeRateToPln(arrangement.Currency.IsoCode, accountingDate);

                var value = arrangement.Value.Value / Math.Max(arrangement.Participants.Count, 1);
                var calculatedValue = value * exchangeRate.Rate;

                result.Value += calculatedValue;

                var diagnosticItem = arrangement.ArrangementType.GetDiagnosticItemByArrangementType();

                diagnosticItem.Group = DiagnosticGroupType.Arrangements;
                diagnosticItem.Name = arrangement.Name;
                diagnosticItem.ArrangementValue = CurrencyAmount.CreateValueAmount(value, arrangement.Currency.IsoCode);
                diagnosticItem.CalculatedValue = CurrencyAmount.CreateValueAmount(calculatedValue, CurrencyIsoCodes.PolishZloty);

                result.AddDiagnosticItem(diagnosticItem);

                if (arrangement.Currency.IsoCode != CurrencyIsoCodes.PolishZloty)
                {
                    result.AddDiagnosticItem(new CurrencyExchangeRateDiagnosticItem
                    {
                        Group = DiagnosticGroupType.Arrangements,
                        CurrencyIsoCode = arrangement.Currency.IsoCode,
                        Date = exchangeRate.Date,
                        Rate = exchangeRate.Rate,
                        TableNumber = exchangeRate.TableNumber
                    });
                }
            }

            return result;
        }

        public override CostItem CalculateBusinessTripCompensationTotal(IEnumerable<CostItem> costItems)
        {
            var positiveGroups = new CostItemGroupType[]
            {
                CostItemGroupType.OwnCosts,
            };

            var negativeGroups = new CostItemGroupType[]
            {
                CostItemGroupType.AdditionalAdvancePayment,
                CostItemGroupType.AdvancePayments,
            };

            var relevantCostItems = costItems.Where(
                i => positiveGroups.Contains(i.Group) || negativeGroups.Contains(i.Group));

            if (relevantCostItems.Any(g => g.CurrencyIsoCode != CurrencyIsoCodes.PoundSterling))
            {
                throw new BusinessException(CalculatorResources.CurrenciesDontMatch);
            }

            var totalCostItem = new CostItem
            {
                Group = CostItemGroupType.BusinessTripCompensationTotal,
                CurrencyIsoCode = CurrencyIsoCodes.PoundSterling,
                Value = relevantCostItems
                    .Sum(i => positiveGroups.Contains(i.Group) ? i.Value
                        : negativeGroups.Contains(i.Group) ? -i.Value
                        : 0m)
            };

            return totalCostItem;
        }

        public override CostItem CalculateBusinessTripExpensesTotal(IEnumerable<CostItem> costItems)
        {
            var groups = new CostItemGroupType[]
            {
                CostItemGroupType.AdditionalAdvancePayment,
                CostItemGroupType.AdvancePayments,
                CostItemGroupType.Arrangements,
                CostItemGroupType.CompanyCosts,
                CostItemGroupType.BusinessTripCompensationTotal
            };

            var relevantCostItems = costItems.Where(i => groups.Contains(i.Group));

            if (relevantCostItems.Any(
                g => g.CurrencyIsoCode != CurrencyIsoCodes.PoundSterling
                    && g.CurrencyIsoCode != CurrencyIsoCodes.PolishZloty))
            {
                throw new BusinessException(CalculatorResources.CurrenciesDontMatch);
            }

            var poundExchangeRate = _calculatorCurrencyExchangeRateService.GetExchangeRateToGbp(
                CurrencyIsoCodes.PolishZloty, SettlementRequest.SettlementDate.Value);

            var totalCostItem = new CostItem
            {
                Group = CostItemGroupType.BusinessTripExpensesTotal,
                CurrencyIsoCode = CurrencyIsoCodes.PolishZloty,
                Value = relevantCostItems
                    .Select(i => i.CurrencyIsoCode == CurrencyIsoCodes.PoundSterling
                        ? i.Value / poundExchangeRate.Rate
                        : i.Value)
                    .Sum()
            };

            return totalCostItem;
        }
    }
}
