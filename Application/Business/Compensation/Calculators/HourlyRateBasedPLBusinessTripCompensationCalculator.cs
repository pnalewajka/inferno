﻿using System.Linq;
using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.DiagnosticItems;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.Business.Compensation.Resources;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Compensation.Calculators
{
    public class HourlyRateBasedPLBusinessTripCompensationCalculator : BasePLBusinessTripCompensationCalculator
    {
        public HourlyRateBasedPLBusinessTripCompensationCalculator(
            ISystemParameterService systemParameterService,
            ICalculatorCurrencyExchangeRateService calculatorCurrencyExchangeRateService,
            BusinessTripSettlementRequest settlementRequest,
            EmploymentPeriod[] employmentPeriods)
            : base(systemParameterService, calculatorCurrencyExchangeRateService, settlementRequest, employmentPeriods)
        {
        }

        public override void PerformCalculation(CalculationResult result)
        {
            base.PerformCalculation(result);

            result.AddCostItem(CalculateHoursWorkedForClientBonus());
            result.AddCostItem(CalculateLumpSum());
            result.AddCostItem(CalculateOwnCosts(c => c.TransactionDate));
            result.AddCostItem(CalculateCompanyCosts(c => c.TransactionDate));
            result.AddCostItem(CalculatePrivateVehicleMileage());
            result.AddCostItem(CalculateAdvancePayment());
            result.AddCostItem(CalculateAdditionalAdvancePayment());
            result.AddCostItem(CalculateArrangements());
        }

        private CostItem CalculateHoursWorkedForClientBonus()
        {
            var employmentPeriod = EmploymentPeriods.First();
            var employeeCountryId = SettlementRequest.BusinessTripParticipant.DepartureCity.CountryId;
            var destinationCountryId = SettlementRequest.BusinessTripParticipant.BusinessTrip.DestinationCity.CountryId;

            var hourlyRateBonus = employeeCountryId == destinationCountryId
                ? employmentPeriod.HourlyRateDomesticOutOfOfficeBonus
                : employmentPeriod.HourlyRateForeignOutOfOfficeBonus;

            if (!hourlyRateBonus.HasValue)
            {
                throw new BusinessException(CalculatorResources.EmploymentPeriodMissingDataError);
            }

            var result = new CostItem
            {
                Group = CostItemGroupType.HoursWorkedForClientBonus,
                CurrencyIsoCode = CurrencyIsoCodes.PolishZloty,
                Value = hourlyRateBonus.Value * SettlementRequest.HoursWorkedForClient
            };

            result.AddDiagnosticItem(new HoursMultiplicationDiagnosticItem
            {
                Group = DiagnosticGroupType.HoursWorkedForClientBonus,
                Hours = SettlementRequest.HoursWorkedForClient,
                HourlyRate = CurrencyAmount.CreateValueAmount(hourlyRateBonus.Value, CurrencyIsoCodes.PolishZloty),
                Compensation = CurrencyAmount.CreateValueAmount(result.Value, CurrencyIsoCodes.PolishZloty)
            });

            return result;
        }
    }
}
