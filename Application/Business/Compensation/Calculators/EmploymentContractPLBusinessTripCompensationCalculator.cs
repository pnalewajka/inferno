﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Compensation.BusinessLogics;
using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.DiagnosticItems;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.Business.Compensation.Resources;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Compensation.Calculators
{
    public class EmploymentContractPLBusinessTripCompensationCalculator : BasePLBusinessTripCompensationCalculator
    {
        public EmploymentContractPLBusinessTripCompensationCalculator(
            ISystemParameterService systemParameterService,
            ICalculatorCurrencyExchangeRateService calculatorCurrencyExchangeRateService,
            BusinessTripSettlementRequest settlementRequest,
            EmploymentPeriod[] employmentPeriods)
            : base(systemParameterService, calculatorCurrencyExchangeRateService, settlementRequest, employmentPeriods)
        {
        }

        public override void PerformCalculation(CalculationResult result)
        {
            base.PerformCalculation(result);

            result.AddCostItem(CalculateDailyAllowances());
            result.AddCostItem(CalculateLumpSum());
            result.AddCostItem(CalculateOwnCosts(c => c.HasInvoiceIssued ? c.TransactionDate : SettlementRequest.SettlementDate.Value));
            result.AddCostItem(CalculateCompanyCosts(c => c.HasInvoiceIssued ? c.TransactionDate : SettlementRequest.SettlementDate.Value));
            result.AddCostItem(CalculatePrivateVehicleMileage());
            result.AddCostItem(CalculateAdvancePayment());
            result.AddCostItem(CalculateAdditionalAdvancePayment());
            result.AddCostItem(CalculateArrangements());
        }

        private CostItem CalculateDailyAllowances()
        {
            var result = new CostItem
            {
                Group = CostItemGroupType.DailyAllowances,
                CurrencyIsoCode = CurrencyIsoCodes.PolishZloty
            };

            var domesticTime = CalculateDomesticTime();
            var foreignTimes = CalculateForeignTimes();

            var domesticDailyAllowanceUnits = CalculateDomesticDailyAllowanceUnits(domesticTime);
            var foreignDailyAllowancesUnits = foreignTimes.ToDictionary(p => p.Key, p => CalculateForeignDailyAllowanceUnits(p.Value));

            var domesticMeal = SettlementRequest.Meals.Single(m => m.Country.IsoCode == CountryIsoCodes.Poland);

            domesticDailyAllowanceUnits -= domesticMeal.BreakfastCount * 0.25m + domesticMeal.LunchCount * 0.5m + domesticMeal.DinnerCount * 0.25m;

            var domesticDailyAllowance = SettlementRequest.BusinessTripParticipant.DepartureCity.Country.DailyAllowances.SingleOrDefault(
                a => DailyAllowanceBusinessLogic.ForGivenCountryAndDate.Call(a, CountryIsoCodes.Poland, SettlementRequest.SettlementDate.Value));

            if (domesticDailyAllowance == null || domesticDailyAllowance.Currency.IsoCode != CurrencyIsoCodes.PolishZloty)
            {
                throw new BusinessException(CalculatorResources.DailyAllowanceMissingDataError);
            }

            result.Value = domesticDailyAllowance.Allowance * domesticDailyAllowanceUnits;

            result.AddDiagnosticItem(new DailyAllowanceRateDiagnosticItem
            {
                Group = DiagnosticGroupType.DailyAllowances,
                Country = domesticDailyAllowance.TravelCountry.Name,
                Allowance = CurrencyAmount.CreateValueAmount(domesticDailyAllowance.Allowance, domesticDailyAllowance.Currency.IsoCode),
                Rate = CalculatorCurrencyExchangeRateService.SameCurrencyExchangeRate,
                CalculatedValue = CurrencyAmount.CreateValueAmount(domesticDailyAllowance.Allowance, CurrencyIsoCodes.PolishZloty)
            });

            result.AddDiagnosticItem(new DailyAllowanceValueDiagnosticItem
            {
                Group = DiagnosticGroupType.DailyAllowances,
                Country = domesticDailyAllowance.TravelCountry.Name,
                Hours = (int)Math.Floor(domesticTime.TotalHours),
                AllowanceUnits = domesticDailyAllowanceUnits,
                CalculatedValue = CurrencyAmount.CreateValueAmount(result.Value, CurrencyIsoCodes.PolishZloty)
            });

            foreach (var countryId in foreignDailyAllowancesUnits.Keys.ToList())
            {
                var meal = SettlementRequest.Meals.Single(m => m.CountryId == countryId);

                foreignDailyAllowancesUnits[countryId] -= meal.BreakfastCount * 0.15m + meal.LunchCount * 0.3m + meal.DinnerCount * 0.3m;

                var foreignDailyAllowance = SettlementRequest.AbroadTimeEntries
                    .Where(e => e.DepartureCountryId == meal.CountryId)
                    .DistinctBy(e => e.DepartureCountryId)
                    .SelectMany(e => e.DepartureCountry.DailyAllowances)
                    .SingleOrDefault(a =>
                        DailyAllowanceBusinessLogic.ForGivenCountryAndDate.Call(a, CountryIsoCodes.Poland, SettlementRequest.SettlementDate.Value));

                if (foreignDailyAllowance == null)
                {
                    throw new BusinessException(CalculatorResources.DailyAllowanceMissingDataError);
                }

                var exchangeRate = CalculatorCurrencyExchangeRateService.GetExchangeRateToPln(
                    foreignDailyAllowance.Currency.IsoCode, SettlementRequest.SettlementDate.Value);
                var foreignDailyAllowanceInZlotys = foreignDailyAllowance.Allowance * exchangeRate.Rate;
                var resultingValue = foreignDailyAllowancesUnits[countryId] * foreignDailyAllowanceInZlotys;

                result.Value += resultingValue;

                if (foreignDailyAllowance.Currency.IsoCode != CurrencyIsoCodes.PolishZloty)
                {
                    result.AddDiagnosticItem(new DailyAllowanceRateDiagnosticItem
                    {
                        Group = DiagnosticGroupType.DailyAllowances,
                        Country = foreignDailyAllowance.TravelCountry.Name,
                        Allowance = CurrencyAmount.CreateValueAmount(foreignDailyAllowance.Allowance, foreignDailyAllowance.Currency.IsoCode),
                        Rate = exchangeRate.Rate,
                        CalculatedValue = CurrencyAmount.CreateValueAmount(foreignDailyAllowanceInZlotys, CurrencyIsoCodes.PolishZloty)
                    });
                }

                result.AddDiagnosticItem(new DailyAllowanceValueDiagnosticItem
                {
                    Group = DiagnosticGroupType.DailyAllowances,
                    Country = foreignDailyAllowance.TravelCountry.Name,
                    Hours = (int)Math.Floor(foreignTimes[countryId].TotalHours),
                    AllowanceUnits = foreignDailyAllowancesUnits[countryId],
                    CalculatedValue = CurrencyAmount.CreateValueAmount(resultingValue, CurrencyIsoCodes.PolishZloty)
                });
            }

            return result;
        }

        private TimeSpan CalculateDomesticTime()
        {
            var flightDepartureDateTime = SettlementRequest.AbroadTimeEntries.Any()
                ? SettlementRequest.AbroadTimeEntries.Min(e => e.DepartureDateTime)
                : (DateTime?)null;

            if (!flightDepartureDateTime.HasValue)
            {
                return SettlementRequest.ArrivalDateTime.Value - SettlementRequest.DepartureDateTime.Value;
            }

            var flightArrivalDateTime = SettlementRequest.AbroadTimeEntries.Max(e => e.DepartureDateTime);

            return (flightDepartureDateTime.Value - SettlementRequest.DepartureDateTime.Value)
                + (SettlementRequest.ArrivalDateTime.Value - flightArrivalDateTime);
        }

        private IDictionary<long, TimeSpan> CalculateForeignTimes()
        {
            var result = new Dictionary<long, TimeSpan>();
            var abroadTimeEntries = SettlementRequest.AbroadTimeEntries.OrderBy(e => e.DepartureDateTime).ToList();

            if (abroadTimeEntries.Any())
            {
                foreach (var countryId in abroadTimeEntries.Take(abroadTimeEntries.Count - 1).Select(e => e.DepartureCountryId).Distinct())
                {
                    result.Add(countryId, TimeSpan.Zero);
                }

                for (var i = 0; i < abroadTimeEntries.Count - 1; ++i)
                {
                    var entry = abroadTimeEntries[i];

                    result[entry.DepartureCountryId] += abroadTimeEntries[i + 1].DepartureDateTime - entry.DepartureDateTime;
                }
            }

            return result;
        }

        private static decimal CalculateDomesticDailyAllowanceUnits(TimeSpan domesticTime)
        {
            if (domesticTime.Days == 0)
            {
                return domesticTime.TotalHours < 8.0 ? 0m
                    : domesticTime.TotalHours >= 8.0 && domesticTime.TotalHours <= 12.0 ? 0.5m
                    : 1m;
            }

            var timePart = TimeSpanHelper.RemoveDatePart(domesticTime);
            var fraction = timePart != TimeSpan.Zero
                ? (timePart.TotalHours <= 8.0 ? 0.5m : 1m)
                : 0m;

            return domesticTime.Days + fraction;
        }

        private static decimal CalculateForeignDailyAllowanceUnits(TimeSpan foreignTime)
        {
            var timePart = TimeSpanHelper.RemoveDatePart(foreignTime);
            var fraction = timePart.TotalHours <= 8.0 ? 1m / 3
                : timePart.TotalHours > 8.0 && timePart.TotalHours <= 12.0 ? 0.5m
                : 1m;

            return foreignTime.Days + fraction;
        }
    }
}
