﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Compensation.BusinessLogics;
using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.DiagnosticItems;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.Business.Compensation.Helpers;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Compensation.Calculators
{
    public class EmploymentContractDEBusinessTripCompensationCalculator : BaseBusinessTripCompensationCalculator
    {
        private readonly ISystemParameterService _systemParameterService;
        private readonly ICalculatorCurrencyExchangeRateService _calculatorCurrencyExchangeRateService;

        public EmploymentContractDEBusinessTripCompensationCalculator(
            ISystemParameterService systemParameterService,
            ICalculatorCurrencyExchangeRateService calculatorCurrencyExchangeRateService,
            BusinessTripSettlementRequest settlementRequest,
            EmploymentPeriod[] employmentPeriods)
            : base(settlementRequest, employmentPeriods)
        {
            _systemParameterService = systemParameterService;
            _calculatorCurrencyExchangeRateService = calculatorCurrencyExchangeRateService;
        }

        public override void PerformCalculation(CalculationResult result)
        {
            base.PerformCalculation(result);

            result.AddCostItem(CalculateDailyAllowances());
            result.AddCostItem(CalculateLumpSum());
            result.AddCostItem(CalculateOwnCosts());
            result.AddCostItem(CalculateCompanyCosts());
            result.AddCostItem(CalculatePrivateVehicleMileage());
            result.AddCostItem(CalculateArrangements());
        }

        private CostItem CalculateDailyAllowances()
        {
            if (!SettlementRequest.AbroadTimeEntries.Any())
            {
                return null;
            }

            var result = new CostItem
            {
                Group = CostItemGroupType.DailyAllowances,
                CurrencyIsoCode = CurrencyIsoCodes.Euro
            };

            var days = DateHelper.GetDaysInRange(SettlementRequest.DepartureDateTime.Value, SettlementRequest.ArrivalDateTime.Value);
            var mealEntries = days.ToDictionary(d => d.Date, d => 0m);
            var allowanceEntries = days.ToDictionary(d => d.Date, d => 0m);
            var absenceTimeEntries = days.ToDictionary(d => d.Date, d => TimeSpan.Zero);
            var abroadTimeEntries = SettlementRequest.AbroadTimeEntries.OrderBy(e => e.DepartureDateTime).ToList();

            for (var i = 0; i < abroadTimeEntries.Count; ++i)
            {
                var entry = abroadTimeEntries[i];

                if (entry.DestinationType != BusinessTripDestinationType.CompanyOffice)
                {
                    var dailyAllowance = GetDailyAllowanceFromAbroadTimeEntry(entry, SettlementRequest.SettlementDate.Value);

                    var startDate = i == 0
                        ? SettlementRequest.DepartureDateTime.Value
                        : entry.DepartureDateTime;
                    var endDate = i == abroadTimeEntries.Count - 1
                        ? SettlementRequest.ArrivalDateTime.Value
                        : abroadTimeEntries[i + 1].DepartureDateTime;

                    foreach (var day in DateHelper.GetDaysInRange(startDate, endDate))
                    {
                        var meal = SettlementRequest.Meals.Single(m => m.Day == day);
                        var interval = DateRangeHelper.GetOverlapingInterval(startDate, endDate, day.Date, day.Date.AddDays(1.0));

                        absenceTimeEntries[day] += interval ?? TimeSpan.Zero;
                        allowanceEntries[day] = CalculateDailyAllowance(absenceTimeEntries[day], dailyAllowance);
                        mealEntries[day] = dailyAllowance.Allowance * (meal.BreakfastCount * 0.2m + meal.LunchCount * 0.4m + meal.DinnerCount * 0.4m);
                    }
                }
            }

            foreach (var day in days)
            {
                var calculatedValue = Math.Max(allowanceEntries[day] - mealEntries[day], 0m);

                result.Value += calculatedValue;

                result.AddDiagnosticItem(new IntiveGmbhDailyAllowanceValueDiagnosticItem
                {
                    Group = DiagnosticGroupType.DailyAllowances,
                    Date = day,
                    Allowance = CurrencyAmount.CreateValueAmount(allowanceEntries[day], CurrencyIsoCodes.Euro),
                    MealsValue = CurrencyAmount.CreateValueAmount(mealEntries[day], CurrencyIsoCodes.Euro),
                    CalculatedValue = CurrencyAmount.CreateValueAmount(calculatedValue, CurrencyIsoCodes.Euro)
                });
            }

            return result;
        }

        protected CostItem CalculateLumpSum()
        {
            if (!SettlementRequest.BusinessTripParticipant.AccommodationPreferences.HasFlag(AccommodationType.LumpSum))
            {
                return null;
            }

            var result = new CostItem
            {
                Group = CostItemGroupType.LumpSum,
                CurrencyIsoCode = CurrencyIsoCodes.Euro
            };

            var accommodations = CalculateAccommodationsPerCountry();

            foreach (var countryId in accommodations.Keys.ToList())
            {
                var abroadTimeEntry = SettlementRequest.AbroadTimeEntries.FirstOrDefault(e => e.DepartureCountryId == countryId);
                var dailyAllowance = abroadTimeEntry != null
                    ? GetDailyAllowanceFromAbroadTimeEntry(abroadTimeEntry, SettlementRequest.SettlementDate.Value)
                    : SettlementRequest.BusinessTripParticipant.DepartureCity.Country.DailyAllowances.SingleOrDefault(a =>
                        DailyAllowanceBusinessLogic.ForGivenCountryAndDate.Call(a, CountryIsoCodes.Germany, SettlementRequest.SettlementDate.Value));

                var resultingValue = dailyAllowance.AccommodationLumpSum * accommodations[countryId];

                result.Value += resultingValue;

                result.AddDiagnosticItem(new LumpSumCalculationDiagnosticItem
                {
                    Group = DiagnosticGroupType.LumpSum,
                    Country = dailyAllowance.TravelCity?.Country?.Name ?? dailyAllowance.TravelCountry.Name,
                    Rate = CurrencyAmount.CreateValueAmount(dailyAllowance.AccommodationLumpSum, dailyAllowance.Currency.IsoCode),
                    Accommodations = accommodations[countryId],
                    LumpSum = CurrencyAmount.CreateValueAmount(resultingValue, CurrencyIsoCodes.Euro)
                });
            }

            var involvedProjects = SettlementRequest.BusinessTripParticipant.BusinessTrip.Projects.ToList();

            involvedProjects.ForEach(
                p => result.AddDiagnosticItem(new AccountingReportDiagnosticItem
                {
                    Group = DiagnosticGroupType.LumpSum,
                    Amount = 1,
                    AmountUnit = Units.Unit,
                    UnitPrice = result.Value / involvedProjects.Count,
                    UnitPriceCurrency = result.CurrencyIsoCode,
                    PaymentType = "Business Trip (LumpSum)",
                    ProjectName = ProjectBusinessLogic.ClientProjectName.Call(p),
                    ProjectApn = p.Apn,
                }));

            return result;
        }

        protected CostItem CalculateOwnCosts()
        {
            if (!SettlementRequest.OwnCosts.Any())
            {
                return null;
            }

            var result = new CostItem
            {
                Group = CostItemGroupType.OwnCosts,
                CurrencyIsoCode = CurrencyIsoCodes.Euro,
            };

            foreach (var ownCost in SettlementRequest.OwnCosts)
            {
                var exchangeRate = _calculatorCurrencyExchangeRateService.GetExchangeRateToEur(ownCost.Currency.IsoCode, ownCost.TransactionDate);
                var calculatedCost = ownCost.Amount * exchangeRate.Rate;

                result.Value += calculatedCost;

                result.AddDiagnosticItem(new IntiveGmbhCostCurrencyRateMultiplicationDiagnosticItem
                {
                    Group = DiagnosticGroupType.OwnCosts,
                    Date = ownCost.TransactionDate,
                    Name = ownCost.Description,
                    CostValue = CurrencyAmount.CreateValueAmount(ownCost.Amount, ownCost.Currency.IsoCode),
                    Rate = exchangeRate.Rate,
                    CalculatedValue = CurrencyAmount.CreateValueAmount(calculatedCost, CurrencyIsoCodes.Euro),
                });

                if (ownCost.Currency.IsoCode != CurrencyIsoCodes.Euro)
                {
                    result.AddDiagnosticItem(new CurrencyExchangeRateDiagnosticItem
                    {
                        Group = DiagnosticGroupType.OwnCosts,
                        CurrencyIsoCode = ownCost.Currency.IsoCode,
                        Date = exchangeRate.Date,
                        Rate = exchangeRate.Rate,
                        TableNumber = exchangeRate.TableNumber
                    });
                }
            }

            var involvedProjects = SettlementRequest.BusinessTripParticipant.BusinessTrip.Projects.ToList();

            involvedProjects.ForEach(
                p => result.AddDiagnosticItem(new AccountingReportDiagnosticItem
                {
                    Group = DiagnosticGroupType.OwnCosts,
                    Amount = 1,
                    AmountUnit = Units.Unit,
                    UnitPrice = result.Value / involvedProjects.Count,
                    UnitPriceCurrency = CurrencyIsoCodes.Euro,
                    PaymentType = "Business Trip (own costs)",
                    ProjectName = ProjectBusinessLogic.ClientProjectName.Call(p),
                    ProjectApn = p.Apn,
                }));

            return result;
        }

        protected CostItem CalculateCompanyCosts()
        {
            if (!SettlementRequest.CompanyCosts.Any())
            {
                return null;
            }

            var result = new CostItem
            {
                Group = CostItemGroupType.CompanyCosts,
                CurrencyIsoCode = CurrencyIsoCodes.Euro
            };

            foreach (var companyCost in SettlementRequest.CompanyCosts)
            {
                var exchangeRate = _calculatorCurrencyExchangeRateService.GetExchangeRateToEur(companyCost.Currency.IsoCode, companyCost.TransactionDate);
                var calculatedCost = companyCost.Amount * exchangeRate.Rate;

                result.Value += calculatedCost;

                result.AddDiagnosticItem(new IntiveGmbhCostCurrencyRateMultiplicationDiagnosticItem
                {
                    Group = DiagnosticGroupType.CompanyCosts,
                    Date = companyCost.TransactionDate,
                    Name = companyCost.Description,
                    CostValue = CurrencyAmount.CreateValueAmount(companyCost.Amount, companyCost.Currency.IsoCode),
                    Rate = exchangeRate.Rate,
                    CalculatedValue = CurrencyAmount.CreateValueAmount(calculatedCost, CurrencyIsoCodes.Euro)
                });

                if (companyCost.Currency.IsoCode != CurrencyIsoCodes.Euro)
                {
                    result.AddDiagnosticItem(new CurrencyExchangeRateDiagnosticItem
                    {
                        Group = DiagnosticGroupType.CompanyCosts,
                        CurrencyIsoCode = companyCost.Currency.IsoCode,
                        Date = exchangeRate.Date,
                        Rate = exchangeRate.Rate,
                        TableNumber = exchangeRate.TableNumber
                    });
                }
            }

            var involvedProjects = SettlementRequest.BusinessTripParticipant.BusinessTrip.Projects.ToList();

            involvedProjects.ForEach(
                p => result.AddDiagnosticItem(new AccountingReportDiagnosticItem
                {
                    Group = DiagnosticGroupType.CompanyCosts,
                    Amount = 1,
                    AmountUnit = Units.Unit,
                    UnitPrice = result.Value / involvedProjects.Count,
                    UnitPriceCurrency = result.CurrencyIsoCode,
                    PaymentType = "Business Trip (company costs)",
                    ProjectName = ProjectBusinessLogic.ClientProjectName.Call(p),
                    ProjectApn = p.Apn,
                }));

            return result;
        }

        protected CostItem CalculatePrivateVehicleMileage()
        {
            if (!SettlementRequest.SimplifiedPrivateVehicleMileage.HasValue)
            {
                return null;
            }

            var ratePerKilometer = _systemParameterService.GetParameter<decimal>(ParameterKeys.CarGermanyRatePerKilometer);
            var value = SettlementRequest.SimplifiedPrivateVehicleMileage.Value * ratePerKilometer;

            var result = new CostItem
            {
                Group = CostItemGroupType.PrivateVehicleMileage,
                CurrencyIsoCode = CurrencyIsoCodes.Euro,
                Value = value
            };

            result.AddDiagnosticItem(new KilometersMultiplicationDiagnosticItem
            {
                Group = DiagnosticGroupType.PrivateVehicleMileage,
                Kilometers = SettlementRequest.SimplifiedPrivateVehicleMileage.Value,
                Rate = CurrencyAmount.CreateRateAmount(ratePerKilometer, CurrencyIsoCodes.Euro),
                CalculatedValue = CurrencyAmount.CreateValueAmount(value, CurrencyIsoCodes.Euro),
            });

            var involvedProjects = SettlementRequest.BusinessTripParticipant.BusinessTrip.Projects.ToList();

            SettlementRequest.BusinessTripParticipant.BusinessTrip.Projects.ToList().ForEach(
                p => result.AddDiagnosticItem(new AccountingReportDiagnosticItem
                {
                    Group = DiagnosticGroupType.PrivateVehicleMileage,
                    Amount = 1,
                    AmountUnit = Units.Unit,
                    UnitPrice = result.Value / involvedProjects.Count,
                    UnitPriceCurrency = result.CurrencyIsoCode,
                    PaymentType = "Business Trip (vehicle mileage)",
                    ProjectName = ProjectBusinessLogic.ClientProjectName.Call(p),
                    ProjectApn = p.Apn,
                }));

            return result;
        }

        protected CostItem CalculateArrangements()
        {
            var arrangements = SettlementRequest.BusinessTripParticipant.BusinessTrip.Arrangements
                .Where(a => !a.Participants.Any() || a.Participants.Any(p => p.Id == SettlementRequest.BusinessTripParticipantId))
                .ToList();

            if (!arrangements.Any())
            {
                return null;
            }

            var result = new CostItem
            {
                Group = CostItemGroupType.Arrangements,
                CurrencyIsoCode = CurrencyIsoCodes.Euro
            };

            foreach (var arrangement in arrangements.Where(a => a.Value.HasValue))
            {
                var accountingDate = arrangement.IssuedOn;
                var exchangeRate = _calculatorCurrencyExchangeRateService.GetExchangeRateToEur(arrangement.Currency.IsoCode, accountingDate);

                var participantsNumber = arrangement.Participants.Any()
                    ? arrangement.Participants.Count
                    : SettlementRequest.BusinessTripParticipant.BusinessTrip.Participants.Count;
                var value = arrangement.Value.Value / participantsNumber;
                var calculatedValue = value * exchangeRate.Rate;

                result.Value += calculatedValue;

                var diagnosticItem = arrangement.ArrangementType.GetDiagnosticItemByArrangementType();

                diagnosticItem.Group = DiagnosticGroupType.Arrangements;
                diagnosticItem.Name = arrangement.Name;
                diagnosticItem.ArrangementValue = CurrencyAmount.CreateValueAmount(value, arrangement.Currency.IsoCode);
                diagnosticItem.CalculatedValue = CurrencyAmount.CreateValueAmount(calculatedValue, CurrencyIsoCodes.Euro);

                result.AddDiagnosticItem(diagnosticItem);

                if (arrangement.Currency.IsoCode != CurrencyIsoCodes.Euro)
                {
                    result.AddDiagnosticItem(new CurrencyExchangeRateDiagnosticItem
                    {
                        Group = DiagnosticGroupType.Arrangements,
                        CurrencyIsoCode = arrangement.Currency.IsoCode,
                        Date = exchangeRate.Date,
                        Rate = exchangeRate.Rate,
                        TableNumber = exchangeRate.TableNumber
                    });
                }
            }

            var involvedProjects = SettlementRequest.BusinessTripParticipant.BusinessTrip.Projects.ToList();

            involvedProjects.ForEach(
                p => result.AddDiagnosticItem(new AccountingReportDiagnosticItem
                {
                    Group = DiagnosticGroupType.Arrangements,
                    Amount = 1,
                    AmountUnit = Units.Unit,
                    UnitPrice = result.Value / involvedProjects.Count,
                    UnitPriceCurrency = result.CurrencyIsoCode,
                    PaymentType = "Business Trip (arrangements)",
                    ProjectName = ProjectBusinessLogic.ClientProjectName.Call(p),
                    ProjectApn = p.Apn,
                }));

            return result;
        }

        public override CostItem CalculateBusinessTripCompensationTotal(IEnumerable<CostItem> costItems)
        {
            var relevantGroups = new CostItemGroupType[]
            {
                CostItemGroupType.DailyAllowances,
                CostItemGroupType.LumpSum,
                CostItemGroupType.OwnCosts,
                CostItemGroupType.PrivateVehicleMileage
            };

            var totalCostItem = new CostItem
            {
                Group = CostItemGroupType.BusinessTripCompensationTotal,
                CurrencyIsoCode = CurrencyIsoCodes.Euro,
                Value = costItems.Where(i => relevantGroups.Contains(i.Group)).Sum(i => i.Value)
            };

            var involvedProjects = SettlementRequest.BusinessTripParticipant.BusinessTrip.Projects.ToList();

            involvedProjects.ForEach(
                p => totalCostItem.AddDiagnosticItem(new AccountingReportDiagnosticItem
                {
                    Group = DiagnosticGroupType.BusinessTripCompensationTotal,
                    Amount = 1,
                    AmountUnit = Units.Unit,
                    UnitPrice = totalCostItem.Value / involvedProjects.Count,
                    UnitPriceCurrency = totalCostItem.CurrencyIsoCode,
                    PaymentType = "Business Trip",
                    ProjectName = ProjectBusinessLogic.ClientProjectName.Call(p),
                    ProjectApn = p.Apn,
                }));

            return totalCostItem;
        }

        public override CostItem CalculateBusinessTripExpensesTotal(IEnumerable<CostItem> costItems)
        {
            var relevantGroups = new CostItemGroupType[]
            {
                CostItemGroupType.Arrangements,
                CostItemGroupType.CompanyCosts,
                CostItemGroupType.BusinessTripCompensationTotal
            };

            var totalCostItem = new CostItem
            {
                Group = CostItemGroupType.BusinessTripExpensesTotal,
                CurrencyIsoCode = CurrencyIsoCodes.Euro,
                Value = costItems.Where(i => relevantGroups.Contains(i.Group)).Sum(i => i.Value)
            };

            var involvedProjects = SettlementRequest.BusinessTripParticipant.BusinessTrip.Projects.ToList();

            involvedProjects.ForEach(
                p => totalCostItem.AddDiagnosticItem(new AccountingReportDiagnosticItem
                {
                    Group = DiagnosticGroupType.BusinessTripCompensationTotal,
                    Amount = 1,
                    AmountUnit = Units.Unit,
                    UnitPrice = totalCostItem.Value / involvedProjects.Count,
                    UnitPriceCurrency = totalCostItem.CurrencyIsoCode,
                    PaymentType = "Business Trip",
                    ProjectName = ProjectBusinessLogic.ClientProjectName.Call(p),
                    ProjectApn = p.Apn,
                }));

            return totalCostItem;
        }

        private IDictionary<long, long> CalculateAccommodationsPerCountry()
        {
            var domesticCountryId = SettlementRequest.BusinessTripParticipant.DepartureCity.CountryId;
            var abroadTimeEntries = SettlementRequest.AbroadTimeEntries.OrderBy(e => e.DepartureDateTime).ToList();

            var result = abroadTimeEntries
                .Select(e => e.DepartureCountryId)
                .Concat(new[] { domesticCountryId })
                .Distinct()
                .ToDictionary(id => id, _ => 0L);

            if (!abroadTimeEntries.Any())
            {
                result[domesticCountryId] += CalculateAccommodationsForRange(
                    SettlementRequest.DepartureDateTime.Value, SettlementRequest.ArrivalDateTime.Value);
            }
            else
            {
                result[domesticCountryId] += CalculateAccommodationsForRange(
                    SettlementRequest.DepartureDateTime.Value, abroadTimeEntries[0].DepartureDateTime);

                for (var i = 0; i < abroadTimeEntries.Count - 1; ++i)
                {
                    var entry = abroadTimeEntries[i];

                    if (entry.DestinationType != BusinessTripDestinationType.CompanyOffice)
                    {
                        result[entry.DepartureCountryId] += CalculateAccommodationsForRange(
                            entry.DepartureDateTime, abroadTimeEntries[i + 1].DepartureDateTime);
                    }
                }

                var lastEntry = abroadTimeEntries.Last();

                if (lastEntry.DestinationType != BusinessTripDestinationType.CompanyOffice)
                {
                    result[lastEntry.DepartureCountryId] += CalculateAccommodationsForRange(
                        lastEntry.DepartureDateTime, SettlementRequest.ArrivalDateTime.Value);
                }
            }

            return result;
        }

        private static decimal CalculateDailyAllowance(TimeSpan absenceTime, DailyAllowance dailyAllowance)
        {
            if (absenceTime < TimeSpan.FromHours(8.0))
            {
                return 0m;
            }

            return absenceTime == TimeSpan.FromDays(1.0)
                ? dailyAllowance.Allowance
                : dailyAllowance.DepartureAndArrivalDayAllowance.Value;
        }

        private static long CalculateAccommodationsForRange(DateTime from, DateTime to)
        {
            const double accommodationStartHour = 20.0;
            const double accommodationEndHour = 24.0 + 4.0;
            const double accommodationMinimumLengthInHours = 6.0;

            return DateHelper.GetDaysInRange(from, to)
                .Where(d =>
                {
                    var interval = DateRangeHelper.GetOverlapingInterval(
                        from, to, d.Date.AddHours(accommodationStartHour), d.Date.AddHours(accommodationEndHour));

                    return interval.HasValue && interval >= TimeSpan.FromHours(accommodationMinimumLengthInHours);
                })
                .Count();
        }

        private static DailyAllowance GetDailyAllowanceFromAbroadTimeEntry(BusinessTripSettlementRequestAbroadTimeEntry entry, DateTime date)
        {
            var result = entry.DepartureCity?.DailyAllowances?.SingleOrDefault(a =>
                DailyAllowanceBusinessLogic.ForGivenCountryAndDate.Call(a, CountryIsoCodes.Germany, date));

            if (result == null)
            {
                result = entry.DepartureCountry.DailyAllowances.SingleOrDefault(a =>
                    DailyAllowanceBusinessLogic.ForGivenCountryAndDate.Call(a, CountryIsoCodes.Germany, date));
            }

            return result;
        }
    }
}
