﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.DiagnosticItems;
using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.Business.Compensation.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Comparers;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;

namespace Smt.Atomic.Business.Compensation.Calculators
{
    public abstract class BasePLTimeTrackingCompensationCalculator : BaseTimeTrackingCompensationCalculator
    {
        protected readonly ISystemParameterService SystemParameterService;
        protected readonly ICalculatorCurrencyExchangeRateService CalculatorCurrencyExchangeRateService;

        protected BasePLTimeTrackingCompensationCalculator(
            ISystemParameterService systemParameterService,
            TimeReport timeReport,
            EmploymentPeriod employmentPeriod,
            TimeTrackingCompensationCalculatorOptions calculatorOptions,
            ICalculatorCurrencyExchangeRateService calculatorCurrencyExchangeRateService)
            : base(timeReport, employmentPeriod, calculatorOptions)
        {
            SystemParameterService = systemParameterService;
            CalculatorCurrencyExchangeRateService = calculatorCurrencyExchangeRateService;
        }

        protected CostItem CalculateHourlyCompensation()
        {
            var result = new CostItem
            {
                Group = CostItemGroupType.HourlyCompensation,
                CurrencyIsoCode = CurrencyIsoCodes.PolishZloty
            };

            if (!EmploymentPeriod.HourlyRate.HasValue)
            {
                throw new BusinessException(CalculatorResources.EmploymentPeriodMissingDataError);
            }

            var days = GetNormalDays();
            var hours = days.Sum(d => d.Hours);
            var hourlyRate = EmploymentPeriod.HourlyRate.Value;
            var compensation = hourlyRate * hours;

            result.Value += compensation;

            result.AddDiagnosticItem(new HoursMultiplicationDiagnosticItem
            {
                Group = DiagnosticGroupType.HourlyCompensation,
                Hours = hours,
                HourlyRate = CurrencyAmount.CreateValueAmount(hourlyRate, CurrencyIsoCodes.PolishZloty),
                Compensation = CurrencyAmount.CreateValueAmount(compensation, CurrencyIsoCodes.PolishZloty)
            });

            AddAccountingReportDiagnosticItems(result, days, hourlyRate,
                DiagnosticGroupType.HourlyCompensation, ReportsResources.AccountingReportRegular);

            return result;
        }

        protected CostItem CalculateMonthlyCompensation()
        {
            var result = new CostItem
            {
                Group = CostItemGroupType.MonthlyCompensation,
                CurrencyIsoCode = CurrencyIsoCodes.PolishZloty
            };

            if (!EmploymentPeriod.MonthlyRate.HasValue)
            {
                throw new BusinessException(CalculatorResources.EmploymentPeriodMissingDataError);
            }

            var absenceTypeCodes = SystemParameterService.GetParameter<string[]>(ParameterKeys.PolishCalculatorFullyPaidAbsenceCodes);
            var normalDays = GetNormalDays();
            var absenceDays = GetAbsenceDays(absenceTypeCodes);

            var hours = normalDays.Sum(d => d.Hours) + absenceDays.Sum(d => d.Hours);
            var hourlyRate = GetHourlyRate();
            var compensation = hours * hourlyRate;

            result.Value += compensation;

            result.AddDiagnosticItem(new HoursMultiplicationDiagnosticItem
            {
                Group = DiagnosticGroupType.MonthlyCompensation,
                Hours = hours,
                HourlyRate = CurrencyAmount.CreateValueAmount(hourlyRate, CurrencyIsoCodes.PolishZloty),
                Compensation = CurrencyAmount.CreateValueAmount(compensation, CurrencyIsoCodes.PolishZloty)
            });

            AddAccountingReportDiagnosticItems(result, normalDays, hourlyRate,
                DiagnosticGroupType.MonthlyCompensation, ReportsResources.AccountingReportRegular);

            AddAccountingReportDiagnosticItems(result, absenceDays, hourlyRate,
                DiagnosticGroupType.MonthlyCompensation, ReportsResources.AccountingReportAbsences);

            return result;
        }

        protected CostItem CalculateOvertimeBonus()
        {
            if (!EmploymentPeriod.HourlyRate.HasValue && !EmploymentPeriod.MonthlyRate.HasValue)
            {
                throw new BusinessException(CalculatorResources.EmploymentPeriodMissingDataError);
            }

            var overtimeVariants = TimeReport.Rows
                .Where(r => !r.AbsenceTypeId.HasValue
                    && r.OvertimeVariant != HourlyRateType.Regular && r.OvertimeVariant != HourlyRateType.OverTimeBank)
                .SelectMany(r => r.DailyEntries)
                .Where(DateRangeHelper.IsDateInRange<TimeReportDailyEntry>(
                    EmploymentPeriod.StartDate, EmploymentPeriod.EndDate, e => e.Day).GetFunction())
                .GroupBy(d => d.TimeReportRow.OvertimeVariant)
                .ToArray();

            if (!overtimeVariants.Any())
            {
                return null;
            }

            var hourlyRate = EmploymentPeriod.HourlyRate ?? GetHourlyRate();

            var result = new CostItem
            {
                Group = CostItemGroupType.OvertimeBonus,
                CurrencyIsoCode = CurrencyIsoCodes.PolishZloty
            };

            foreach (var variantDays in overtimeVariants)
            {
                var bonusRate = variantDays.Key == HourlyRateType.OverTimeNormal ? 1.5m : 2m;
                var hours = variantDays.Sum(d => d.Hours);
                var bonus = hours * hourlyRate * bonusRate;

                result.Value += bonus;

                result.AddDiagnosticItem(new OvertimeMultiplicationDiagnosticItem
                {
                    Group = DiagnosticGroupType.OvertimeBonus,
                    Hours = hours,
                    HourlyRate = CurrencyAmount.CreateValueAmount(hourlyRate, CurrencyIsoCodes.PolishZloty),
                    BonusRate = bonusRate,
                    Bonus = CurrencyAmount.CreateValueAmount(bonus, CurrencyIsoCodes.PolishZloty)
                });

                AddAccountingReportDiagnosticItems(result, variantDays, hourlyRate * bonusRate,
                    DiagnosticGroupType.OvertimeBonus, string.Format(ReportsResources.AccountingReportOvertimeBonus, variantDays.Key.GetDescription()));
            }

            return result;
        }

        protected CostItem CalculateSickLeaveCompensation()
        {
            if (!EmploymentPeriod.MonthlyRate.HasValue)
            {
                throw new BusinessException(CalculatorResources.EmploymentPeriodMissingDataError);
            }

            const decimal sickLeaveRate = 0.8m;

            var absenceTypeCodes = SystemParameterService.GetParameter<string[]>(ParameterKeys.PolishCalculatorPartiallyPaidAbsenceCodes);
            var absenceDays = GetAbsenceDays(absenceTypeCodes);

            if (!absenceDays.Any())
            {
                return null;
            }

            var hours = absenceDays.Sum(d => d.Hours);
            var hourlyRate = GetHourlyRate();
            var compensation = hours * hourlyRate * sickLeaveRate;

            var result = new CostItem
            {
                Group = CostItemGroupType.SickLeaveCompensation,
                CurrencyIsoCode = CurrencyIsoCodes.PolishZloty,
                Value = compensation
            };

            result.AddDiagnosticItem(new SickLeaveMultiplicationDiagnosticItem
            {
                Group = DiagnosticGroupType.SickLeaveCompensation,
                Hours = hours,
                HourlyRate = CurrencyAmount.CreateValueAmount(hourlyRate, CurrencyIsoCodes.PolishZloty),
                SickLeaveRate = sickLeaveRate,
                Compensation = CurrencyAmount.CreateValueAmount(compensation, CurrencyIsoCodes.PolishZloty)
            });

            AddAccountingReportDiagnosticItems(result, absenceDays, hourlyRate * sickLeaveRate,
                    DiagnosticGroupType.SickLeaveCompensation, ReportsResources.AccountingReportSickLeave);

            return result;
        }

        protected CostItem CalculateAdditionalCostsTotal(IEnumerable<CostItem> costItems)
        {
            var groupsToInclude = new[]
            {
                CostItemGroupType.MonthlyBonus,
                CostItemGroupType.Expense,
            };

            var totalCostItem = new CostItem
            {
                Group = CostItemGroupType.AdditionalCostsTotal,
                CurrencyIsoCode = CurrencyIsoCodes.PolishZloty,
                Value = costItems.Where(i => groupsToInclude.Contains(i.Group)).Sum(i => i.Value)
            };

            return totalCostItem;
        }

        protected CostItem CalculateTimeTrackingTotal(IEnumerable<CostItem> costItems)
        {
            var groupsToInclude = new[]
            {
                CostItemGroupType.HourlyCompensation,
                CostItemGroupType.MonthlyCompensation,
                CostItemGroupType.OvertimeBonus,
                CostItemGroupType.SickLeaveCompensation,
            };

            var totalCostItem = new CostItem
            {
                Group = CostItemGroupType.TimeTrackingTotal,
                CurrencyIsoCode = CurrencyIsoCodes.PolishZloty,
                Value = costItems.Where(i => groupsToInclude.Contains(i.Group)).Sum(i => i.Value)
            };

            return totalCostItem;
        }

        protected override CurrencyExchangeRateDto GetCurrencyExchangeRate(string currencyIsoCode, DateTime date)
            => CalculatorCurrencyExchangeRateService.GetExchangeRateToPln(currencyIsoCode, date);

        private ICollection<TimeReportDailyEntry> GetAbsenceDays(string[] absenceTypeCodes)
        {
            var days = TimeReport.Rows
               .Where(r => r.AbsenceTypeId.HasValue && absenceTypeCodes.Contains(r.AbsenceType.Code))
               .SelectMany(r => r.DailyEntries)
               .Where(DateRangeHelper.IsDateInRange<TimeReportDailyEntry>(
                   EmploymentPeriod.StartDate, EmploymentPeriod.EndDate, e => e.Day, DateComparer.LeftClosed | DateComparer.RightClosed).GetFunction())
               .ToArray();

            return days;
        }

        private ICollection<TimeReportDailyEntry> GetNormalDays()
        {
            var days = TimeReport.Rows
                .Where(r => !r.AbsenceTypeId.HasValue && r.OvertimeVariant == HourlyRateType.Regular)
                .SelectMany(r => r.DailyEntries)
                .Where(DateRangeHelper.IsDateInRange<TimeReportDailyEntry>(
                    EmploymentPeriod.StartDate, EmploymentPeriod.EndDate, e => e.Day, DateComparer.LeftClosed | DateComparer.RightClosed).GetFunction())
                .ToArray();

            return days;
        }

        private decimal GetHourlyRate()
        {
            var contractedWeeklyHours = EmploymentPeriod.MondayHours
                + EmploymentPeriod.TuesdayHours
                + EmploymentPeriod.WednesdayHours
                + EmploymentPeriod.ThursdayHours
                + EmploymentPeriod.FridayHours
                + EmploymentPeriod.SaturdayHours
                + EmploymentPeriod.SundayHours;

            var fullTimeWeeklyHours = (decimal)SystemParameterService.GetParameter<int>(ParameterKeys.WorkingHoursInWeek);
            var compensationRatio = contractedWeeklyHours / fullTimeWeeklyHours;

            return EmploymentPeriod.MonthlyRate.Value / TimeReport.ContractedHours * compensationRatio;
        }

        private static void AddAccountingReportDiagnosticItems(
            CostItem target,
            IEnumerable<TimeReportDailyEntry> dailyEntries,
            decimal hourPrice,
            DiagnosticGroupType groupType,
            string paymentType)
        {
            var dailyProjects = dailyEntries
                .GroupBy(d => d.TimeReportRow.Project, new ByKeyEqualityComparer<Project, long>(p => p.Id))
                .ToList();

            foreach (var dayProject in dailyProjects)
            {
                target.AddDiagnosticItem(new AccountingReportDiagnosticItem
                {
                    Group = groupType,
                    Amount = dayProject.Sum(d => d.Hours),
                    AmountUnit = Units.Hours,
                    UnitPrice = hourPrice,
                    UnitPriceCurrency = CurrencyIsoCodes.PolishZloty,
                    PaymentType = paymentType,
                    ProjectName = ProjectBusinessLogic.ClientProjectName.Call(dayProject.Key),
                    ProjectApn = dayProject.Key.Apn,
                });
            }
        }
    }
}
