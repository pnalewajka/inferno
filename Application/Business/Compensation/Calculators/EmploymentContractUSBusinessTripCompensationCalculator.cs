﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.DiagnosticItems;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.Business.Compensation.Helpers;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Compensation.Calculators
{
    public class EmploymentContractUSBusinessTripCompensationCalculator : BaseBusinessTripCompensationCalculator
    {
        private readonly ISystemParameterService _systemParameterService;
        private readonly ICalculatorCurrencyExchangeRateService _calculatorCurrencyExchangeRateService;

        public EmploymentContractUSBusinessTripCompensationCalculator(
            ISystemParameterService systemParameterService,
            ICalculatorCurrencyExchangeRateService calculatorCurrencyExchangeRateService,
            BusinessTripSettlementRequest settlementRequest,
            EmploymentPeriod[] employmentPeriods)
            : base(settlementRequest, employmentPeriods)
        {
            _systemParameterService = systemParameterService;
            _calculatorCurrencyExchangeRateService = calculatorCurrencyExchangeRateService;
        }

        public override void PerformCalculation(CalculationResult result)
        {
            base.PerformCalculation(result);

            result.AddCostItem(CalculateOwnCosts());
            result.AddCostItem(CalculateCompanyCosts());
            result.AddCostItem(CalculatePrivateVehicleMileage());
            result.AddCostItem(CalculateArrangements());
        }

        protected CostItem CalculateOwnCosts()
        {
            if (!SettlementRequest.OwnCosts.Any())
            {
                return null;
            }

            var result = new CostItem
            {
                Group = CostItemGroupType.OwnCosts,
                CurrencyIsoCode = CurrencyIsoCodes.UnitedStatesDollar,
            };

            foreach (var ownCost in SettlementRequest.OwnCosts)
            {
                var exchangeRate = _calculatorCurrencyExchangeRateService.GetExchangeRateToUsd(ownCost.Currency.IsoCode, ownCost.TransactionDate);
                var calculatedCost = ownCost.Amount * exchangeRate.Rate;

                result.Value += calculatedCost;

                result.AddDiagnosticItem(new CostCurrencyRateMultiplicationDiagnosticItem
                {
                    Group = DiagnosticGroupType.OwnCosts,
                    Name = ownCost.Description,
                    CostValue = CurrencyAmount.CreateValueAmount(ownCost.Amount, ownCost.Currency.IsoCode),
                    Rate = exchangeRate.Rate,
                    CalculatedValue = CurrencyAmount.CreateValueAmount(calculatedCost, CurrencyIsoCodes.UnitedStatesDollar)
                });

                if (ownCost.Currency.IsoCode != CurrencyIsoCodes.UnitedStatesDollar)
                {
                    result.AddDiagnosticItem(new CurrencyExchangeRateDiagnosticItem
                    {
                        Group = DiagnosticGroupType.OwnCosts,
                        CurrencyIsoCode = ownCost.Currency.IsoCode,
                        Date = exchangeRate.Date,
                        Rate = exchangeRate.Rate,
                        TableNumber = exchangeRate.TableNumber
                    });
                }
            }

            var involvedProjects = SettlementRequest.BusinessTripParticipant.BusinessTrip.Projects.ToList();

            involvedProjects.ForEach(
                p => result.AddDiagnosticItem(new AccountingReportDiagnosticItem
                {
                    Group = DiagnosticGroupType.OwnCosts,
                    Amount = 1,
                    AmountUnit = Units.Unit,
                    UnitPrice = result.Value / involvedProjects.Count,
                    UnitPriceCurrency = CurrencyIsoCodes.UnitedStatesDollar,
                    PaymentType = "Business Trip (own costs)",
                    ProjectName = ProjectBusinessLogic.ClientProjectName.Call(p),
                    ProjectApn = p.Apn,
                }));

            return result;
        }

        protected CostItem CalculateCompanyCosts()
        {
            if (!SettlementRequest.CompanyCosts.Any())
            {
                return null;
            }

            var result = new CostItem
            {
                Group = CostItemGroupType.CompanyCosts,
                CurrencyIsoCode = CurrencyIsoCodes.UnitedStatesDollar
            };

            foreach (var companyCost in SettlementRequest.CompanyCosts)
            {
                var exchangeRate = _calculatorCurrencyExchangeRateService.GetExchangeRateToUsd(companyCost.Currency.IsoCode, companyCost.TransactionDate);
                var calculatedCost = companyCost.Amount * exchangeRate.Rate;

                result.Value += calculatedCost;

                result.AddDiagnosticItem(new CostCurrencyRateMultiplicationDiagnosticItem
                {
                    Group = DiagnosticGroupType.CompanyCosts,
                    Name = companyCost.Description,
                    CostValue = CurrencyAmount.CreateValueAmount(companyCost.Amount, companyCost.Currency.IsoCode),
                    Rate = exchangeRate.Rate,
                    CalculatedValue = CurrencyAmount.CreateValueAmount(calculatedCost, CurrencyIsoCodes.UnitedStatesDollar)
                });

                if (companyCost.Currency.IsoCode != CurrencyIsoCodes.UnitedStatesDollar)
                {
                    result.AddDiagnosticItem(new CurrencyExchangeRateDiagnosticItem
                    {
                        Group = DiagnosticGroupType.CompanyCosts,
                        CurrencyIsoCode = companyCost.Currency.IsoCode,
                        Date = exchangeRate.Date,
                        Rate = exchangeRate.Rate,
                        TableNumber = exchangeRate.TableNumber
                    });
                }
            }

            var involvedProjects = SettlementRequest.BusinessTripParticipant.BusinessTrip.Projects.ToList();

            involvedProjects.ForEach(
                p => result.AddDiagnosticItem(new AccountingReportDiagnosticItem
                {
                    Group = DiagnosticGroupType.CompanyCosts,
                    Amount = 1,
                    AmountUnit = Units.Unit,
                    UnitPrice = result.Value / involvedProjects.Count,
                    UnitPriceCurrency = result.CurrencyIsoCode,
                    PaymentType = "Business Trip (company costs)",
                    ProjectName = ProjectBusinessLogic.ClientProjectName.Call(p),
                    ProjectApn = p.Apn,
                }));

            return result;
        }

        protected CostItem CalculatePrivateVehicleMileage()
        {
            if (!SettlementRequest.SimplifiedPrivateVehicleMileage.HasValue)
            {
                return null;
            }

            var ratePerKilometer = _systemParameterService.GetParameter<decimal>(ParameterKeys.CarUnitedStatesRatePerKilometer);
            var value = SettlementRequest.SimplifiedPrivateVehicleMileage.Value * ratePerKilometer;

            var result = new CostItem
            {
                Group = CostItemGroupType.PrivateVehicleMileage,
                CurrencyIsoCode = CurrencyIsoCodes.UnitedStatesDollar,
                Value = value
            };

            result.AddDiagnosticItem(new KilometersMultiplicationDiagnosticItem
            {
                Group = DiagnosticGroupType.PrivateVehicleMileage,
                Kilometers = SettlementRequest.SimplifiedPrivateVehicleMileage.Value,
                Rate = CurrencyAmount.CreateRateAmount(ratePerKilometer, CurrencyIsoCodes.UnitedStatesDollar),
                CalculatedValue = CurrencyAmount.CreateValueAmount(value, CurrencyIsoCodes.UnitedStatesDollar)
            });

            var involvedProjects = SettlementRequest.BusinessTripParticipant.BusinessTrip.Projects.ToList();

            SettlementRequest.BusinessTripParticipant.BusinessTrip.Projects.ToList().ForEach(
                p => result.AddDiagnosticItem(new AccountingReportDiagnosticItem
                {
                    Group = DiagnosticGroupType.PrivateVehicleMileage,
                    Amount = 1,
                    AmountUnit = Units.Unit,
                    UnitPrice = result.Value / involvedProjects.Count,
                    UnitPriceCurrency = result.CurrencyIsoCode,
                    PaymentType = "Business Trip (vehicle mileage)",
                    ProjectName = ProjectBusinessLogic.ClientProjectName.Call(p),
                    ProjectApn = p.Apn,
                }));

            return result;
        }

        protected CostItem CalculateArrangements()
        {
            var arrangements = SettlementRequest.BusinessTripParticipant.BusinessTrip.Arrangements
                .Where(a => !a.Participants.Any() || a.Participants.Any(p => p.Id == SettlementRequest.BusinessTripParticipantId))
                .ToList();

            if (!arrangements.Any())
            {
                return null;
            }

            var result = new CostItem
            {
                Group = CostItemGroupType.Arrangements,
                CurrencyIsoCode = CurrencyIsoCodes.UnitedStatesDollar
            };

            foreach (var arrangement in arrangements.Where(a => a.Value.HasValue))
            {
                var accountingDate = arrangement.IssuedOn;
                var exchangeRate = _calculatorCurrencyExchangeRateService.GetExchangeRateToUsd(arrangement.Currency.IsoCode, accountingDate);

                var participantsNumber = arrangement.Participants.Any()
                    ? arrangement.Participants.Count
                    : SettlementRequest.BusinessTripParticipant.BusinessTrip.Participants.Count;
                var value = arrangement.Value.Value / participantsNumber;
                var calculatedValue = value * exchangeRate.Rate;

                result.Value += calculatedValue;

                var diagnosticItem = arrangement.ArrangementType.GetDiagnosticItemByArrangementType();

                diagnosticItem.Group = DiagnosticGroupType.Arrangements;
                diagnosticItem.Name = arrangement.Name;
                diagnosticItem.ArrangementValue = CurrencyAmount.CreateValueAmount(value, arrangement.Currency.IsoCode);
                diagnosticItem.CalculatedValue = CurrencyAmount.CreateValueAmount(calculatedValue, CurrencyIsoCodes.UnitedStatesDollar);

                result.AddDiagnosticItem(diagnosticItem);

                if (arrangement.Currency.IsoCode != CurrencyIsoCodes.UnitedStatesDollar)
                {
                    result.AddDiagnosticItem(new CurrencyExchangeRateDiagnosticItem
                    {
                        Group = DiagnosticGroupType.Arrangements,
                        CurrencyIsoCode = arrangement.Currency.IsoCode,
                        Date = exchangeRate.Date,
                        Rate = exchangeRate.Rate,
                        TableNumber = exchangeRate.TableNumber
                    });
                }
            }

            var involvedProjects = SettlementRequest.BusinessTripParticipant.BusinessTrip.Projects.ToList();

            involvedProjects.ForEach(
                p => result.AddDiagnosticItem(new AccountingReportDiagnosticItem
                {
                    Group = DiagnosticGroupType.Arrangements,
                    Amount = 1,
                    AmountUnit = Units.Unit,
                    UnitPrice = result.Value / involvedProjects.Count,
                    UnitPriceCurrency = result.CurrencyIsoCode,
                    PaymentType = "Business Trip (arrangements)",
                    ProjectName = ProjectBusinessLogic.ClientProjectName.Call(p),
                    ProjectApn = p.Apn,
                }));

            return result;
        }

        public override CostItem CalculateBusinessTripCompensationTotal(IEnumerable<CostItem> costItems)
        {
            var relevantGroups = new CostItemGroupType[]
            {
                CostItemGroupType.OwnCosts,
                CostItemGroupType.PrivateVehicleMileage
            };

            var totalCostItem = new CostItem
            {
                Group = CostItemGroupType.BusinessTripCompensationTotal,
                CurrencyIsoCode = CurrencyIsoCodes.UnitedStatesDollar,
                Value = costItems.Where(i => relevantGroups.Contains(i.Group)).Sum(i => i.Value)
            };

            var involvedProjects = SettlementRequest.BusinessTripParticipant.BusinessTrip.Projects.ToList();

            involvedProjects.ForEach(
                p => totalCostItem.AddDiagnosticItem(new AccountingReportDiagnosticItem
                {
                    Group = DiagnosticGroupType.BusinessTripCompensationTotal,
                    Amount = 1,
                    AmountUnit = Units.Unit,
                    UnitPrice = totalCostItem.Value / involvedProjects.Count,
                    UnitPriceCurrency = totalCostItem.CurrencyIsoCode,
                    PaymentType = "Business Trip",
                    ProjectName = ProjectBusinessLogic.ClientProjectName.Call(p),
                    ProjectApn = p.Apn,
                }));

            return totalCostItem;
        }

        public override CostItem CalculateBusinessTripExpensesTotal(IEnumerable<CostItem> costItems)
        {
            var relevantGroups = new CostItemGroupType[]
            {
                CostItemGroupType.Arrangements,
                CostItemGroupType.CompanyCosts,
                CostItemGroupType.BusinessTripCompensationTotal
            };

            var totalCostItem = new CostItem
            {
                Group = CostItemGroupType.BusinessTripExpensesTotal,
                CurrencyIsoCode = CurrencyIsoCodes.UnitedStatesDollar,
                Value = costItems.Where(i => relevantGroups.Contains(i.Group)).Sum(i => i.Value)
            };

            var involvedProjects = SettlementRequest.BusinessTripParticipant.BusinessTrip.Projects.ToList();

            involvedProjects.ForEach(
                p => totalCostItem.AddDiagnosticItem(new AccountingReportDiagnosticItem
                {
                    Group = DiagnosticGroupType.BusinessTripCompensationTotal,
                    Amount = 1,
                    AmountUnit = Units.Unit,
                    UnitPrice = totalCostItem.Value / involvedProjects.Count,
                    UnitPriceCurrency = totalCostItem.CurrencyIsoCode,
                    PaymentType = "Business Trip",
                    ProjectName = ProjectBusinessLogic.ClientProjectName.Call(p),
                    ProjectApn = p.Apn,
                }));

            return totalCostItem;
        }
    }
}
