﻿using System;
using System.Linq;
using Castle.Core.Logging;
using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.DiagnosticItems;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.Business.Compensation.Interfaces;

namespace Smt.Atomic.Business.Compensation.Calculators
{
    internal class ErrorHandlerCalculator : ICalculator
    {
        private readonly ICalculator _calculator;
        private readonly ILogger _logger;
        
        public ErrorHandlerCalculator(ICalculator innerCalculator, ILogger logger)
        {
            _calculator = innerCalculator;
            _logger = logger;
        }
        
        public CalculationResult Calculate()
        {
            try
            {
                return _calculator.Calculate();
            }
            catch (Exception exception)
            {
                var errorCalculation = new CalculationResult();

                errorCalculation.AddSummaryDiagnosticItem(new CalculationFailedErrorDiagnosticItem
                {
                    Group = DiagnosticGroupType.Summary,
                    Details = exception is AggregateException aggregatedException
                        ? aggregatedException.InnerExceptions.Select(e => e.Message).ToArray()
                        : new string[] { exception.Message }
                });

                _logger.Error($"Calculator {_calculator.GetType()} failed", exception);

                return errorCalculation;
            }
        }
    }
}
