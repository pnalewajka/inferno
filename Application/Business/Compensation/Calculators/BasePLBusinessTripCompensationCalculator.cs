﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Compensation.BusinessLogics;
using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.DiagnosticItems;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.Business.Compensation.Helpers;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.Business.Compensation.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Compensation.Calculators
{
    public abstract class BasePLBusinessTripCompensationCalculator : BaseBusinessTripCompensationCalculator
    {
        protected readonly ISystemParameterService SystemParameterService;
        protected readonly ICalculatorCurrencyExchangeRateService CalculatorCurrencyExchangeRateService;

        protected BasePLBusinessTripCompensationCalculator(
            ISystemParameterService systemParameterService,
            ICalculatorCurrencyExchangeRateService calculatorCurrencyExchangeRateService,
            BusinessTripSettlementRequest settlementRequest,
            EmploymentPeriod[] employmentPeriods)
            : base(settlementRequest, employmentPeriods)
        {
            SystemParameterService = systemParameterService;
            CalculatorCurrencyExchangeRateService = calculatorCurrencyExchangeRateService;
        }

        protected CostItem CalculateLumpSum()
        {
            if (!SettlementRequest.BusinessTripParticipant.AccommodationPreferences.HasFlag(AccommodationType.LumpSum))
            {
                return null;
            }

            var result = new CostItem
            {
                Group = CostItemGroupType.LumpSum,
                CurrencyIsoCode = CurrencyIsoCodes.PolishZloty
            };

            var accommodations = CalculateAccommodationsPerCountry();

            foreach (var countryId in accommodations.Keys.ToList())
            {
                var dailyAllowances = SettlementRequest.AbroadTimeEntries.Any()
                    ? SettlementRequest.AbroadTimeEntries
                        .Where(e => e.DepartureCountryId == countryId)
                        .SelectMany(e => e.DepartureCountry.DailyAllowances)
                    : SettlementRequest.BusinessTripParticipant.DepartureCity.Country.DailyAllowances;
                var dailyAllowance = dailyAllowances.SingleOrDefault(
                    a => DailyAllowanceBusinessLogic.ForGivenCountryAndDate.Call(a, CountryIsoCodes.Poland, SettlementRequest.SettlementDate.Value));

                var exchangeRate = CalculatorCurrencyExchangeRateService.GetExchangeRateToPln(
                    dailyAllowance.Currency.IsoCode, SettlementRequest.SettlementDate.Value);
                var resultingValue = dailyAllowance.AccommodationLumpSum * exchangeRate.Rate * accommodations[countryId];

                result.Value += resultingValue;

                result.AddDiagnosticItem(new LumpSumCalculationDiagnosticItem
                {
                    Group = DiagnosticGroupType.LumpSum,
                    Country = dailyAllowance.TravelCountry.Name,
                    Rate = CurrencyAmount.CreateValueAmount(dailyAllowance.AccommodationLumpSum, dailyAllowance.Currency.IsoCode),
                    Accommodations = accommodations[countryId],
                    LumpSum = CurrencyAmount.CreateValueAmount(resultingValue, CurrencyIsoCodes.PolishZloty)
                });

                if (dailyAllowance.Currency.IsoCode != CurrencyIsoCodes.PolishZloty)
                {
                    result.AddDiagnosticItem(new CurrencyExchangeRateDiagnosticItem
                    {
                        Group = DiagnosticGroupType.LumpSum,
                        CurrencyIsoCode = dailyAllowance.Currency.IsoCode,
                        Date = exchangeRate.Date,
                        Rate = exchangeRate.Rate,
                        TableNumber = exchangeRate.TableNumber
                    });
                }
            }

            var involvedProjects = SettlementRequest.BusinessTripParticipant.BusinessTrip.Projects.ToList();
            AddAccountingReportDiagnosticItems(result, involvedProjects,
                DiagnosticGroupType.LumpSum, ReportsResources.AccountingReportLumpSum);

            return result;
        }

        protected CostItem CalculateOwnCosts(Func<BusinessTripSettlementRequestOwnCost, DateTime> accountingDateSelector)
        {
            if (!SettlementRequest.OwnCosts.Any())
            {
                return null;
            }

            var result = new CostItem
            {
                Group = CostItemGroupType.OwnCosts,
                CurrencyIsoCode = CurrencyIsoCodes.PolishZloty,
            };

            foreach (var ownCost in SettlementRequest.OwnCosts)
            {
                var accountingDate = accountingDateSelector(ownCost);
                var exchangeRate = CalculatorCurrencyExchangeRateService.GetExchangeRateToPln(ownCost.Currency.IsoCode, accountingDate);
                var calculatedCost = ownCost.Amount * exchangeRate.Rate;

                result.Value += calculatedCost;

                result.AddDiagnosticItem(new CostCurrencyRateMultiplicationDiagnosticItem
                {
                    Group = DiagnosticGroupType.OwnCosts,
                    Name = ownCost.Description,
                    CostValue = CurrencyAmount.CreateValueAmount(ownCost.Amount, ownCost.Currency.IsoCode),
                    Rate = exchangeRate.Rate,
                    CalculatedValue = CurrencyAmount.CreateValueAmount(calculatedCost, CurrencyIsoCodes.PolishZloty)
                });

                if (ownCost.Currency.IsoCode != CurrencyIsoCodes.PolishZloty)
                {
                    result.AddDiagnosticItem(new CurrencyExchangeRateDiagnosticItem
                    {
                        Group = DiagnosticGroupType.OwnCosts,
                        CurrencyIsoCode = ownCost.Currency.IsoCode,
                        Date = exchangeRate.Date,
                        Rate = exchangeRate.Rate,
                        TableNumber = exchangeRate.TableNumber
                    });
                }
            }

            var involvedProjects = SettlementRequest.BusinessTripParticipant.BusinessTrip.Projects.ToList();
            AddAccountingReportDiagnosticItems(result, involvedProjects,
                DiagnosticGroupType.OwnCosts, ReportsResources.AccountingReportOwnCosts);

            return result;
        }

        protected CostItem CalculateCompanyCosts(Func<BusinessTripSettlementRequestCompanyCost, DateTime> accountingDateSelector)
        {
            if (!SettlementRequest.CompanyCosts.Any())
            {
                return null;
            }

            var result = new CostItem
            {
                Group = CostItemGroupType.CompanyCosts,
                CurrencyIsoCode = CurrencyIsoCodes.PolishZloty
            };

            foreach (var companyCost in SettlementRequest.CompanyCosts)
            {
                var accountingDate = accountingDateSelector(companyCost);
                var exchangeRate = CalculatorCurrencyExchangeRateService.GetExchangeRateToPln(companyCost.Currency.IsoCode, accountingDate);
                var calculatedCost = companyCost.Amount * exchangeRate.Rate;

                result.Value += calculatedCost;

                result.AddDiagnosticItem(new CostCurrencyRateMultiplicationDiagnosticItem
                {
                    Group = DiagnosticGroupType.CompanyCosts,
                    Name = companyCost.Description,
                    CostValue = CurrencyAmount.CreateValueAmount(companyCost.Amount, companyCost.Currency.IsoCode),
                    Rate = exchangeRate.Rate,
                    CalculatedValue = CurrencyAmount.CreateValueAmount(calculatedCost, CurrencyIsoCodes.PolishZloty)
                });

                if (companyCost.Currency.IsoCode != CurrencyIsoCodes.PolishZloty)
                {
                    result.AddDiagnosticItem(new CurrencyExchangeRateDiagnosticItem
                    {
                        Group = DiagnosticGroupType.CompanyCosts,
                        CurrencyIsoCode = companyCost.Currency.IsoCode,
                        Date = exchangeRate.Date,
                        Rate = exchangeRate.Rate,
                        TableNumber = exchangeRate.TableNumber
                    });
                }
            }

            var involvedProjects = SettlementRequest.BusinessTripParticipant.BusinessTrip.Projects.ToList();
            AddAccountingReportDiagnosticItems(result, involvedProjects,
                DiagnosticGroupType.CompanyCosts, ReportsResources.AccountingReportCompanyCosts);

            return result;
        }

        protected CostItem CalculatePrivateVehicleMileage()
        {
            if (SettlementRequest.PrivateVehicleMileage == null)
            {
                return null;
            }

            decimal ratePerKilometer;

            switch (SettlementRequest.PrivateVehicleMileage.VehicleType)
            {
                case VehicleType.Car:
                    ratePerKilometer = SettlementRequest.PrivateVehicleMileage.IsCarEngineCapacityOver900cc
                        ? SystemParameterService.GetParameter<decimal>(ParameterKeys.CarAbove900ccRatePerKilometer)
                        : SystemParameterService.GetParameter<decimal>(ParameterKeys.CarBelow900ccRatePerKilometer);
                    break;

                case VehicleType.Moped:
                    ratePerKilometer = SystemParameterService.GetParameter<decimal>(ParameterKeys.MopedRatePerKilometer);
                    break;

                case VehicleType.Motorcycle:
                    ratePerKilometer = SystemParameterService.GetParameter<decimal>(ParameterKeys.MotorcycleRatePerKilometer);
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(SettlementRequest.PrivateVehicleMileage.VehicleType), "Unknown vehicle type");
            }

            var inboundValue = SettlementRequest.PrivateVehicleMileage.InboundKilometers * ratePerKilometer;
            var outboundValue = SettlementRequest.PrivateVehicleMileage.OutboundKilometers * ratePerKilometer;

            var result = new CostItem
            {
                Group = CostItemGroupType.PrivateVehicleMileage,
                CurrencyIsoCode = CurrencyIsoCodes.PolishZloty,
                Value = inboundValue + outboundValue
            };

            result.AddDiagnosticItem(new KilometersMultiplicationDiagnosticItem
            {
                Group = DiagnosticGroupType.PrivateVehicleMileage,
                Kilometers = SettlementRequest.PrivateVehicleMileage.InboundKilometers,
                Rate = CurrencyAmount.CreateRateAmount(ratePerKilometer, CurrencyIsoCodes.PolishZloty),
                CalculatedValue = CurrencyAmount.CreateValueAmount(inboundValue, CurrencyIsoCodes.PolishZloty)
            });

            result.AddDiagnosticItem(new KilometersMultiplicationDiagnosticItem
            {
                Group = DiagnosticGroupType.PrivateVehicleMileage,
                Kilometers = SettlementRequest.PrivateVehicleMileage.OutboundKilometers,
                Rate = CurrencyAmount.CreateRateAmount(ratePerKilometer, CurrencyIsoCodes.PolishZloty),
                CalculatedValue = CurrencyAmount.CreateValueAmount(outboundValue, CurrencyIsoCodes.PolishZloty)
            });

            var involvedProjects = SettlementRequest.BusinessTripParticipant.BusinessTrip.Projects.ToList();
            AddAccountingReportDiagnosticItems(result, involvedProjects,
                DiagnosticGroupType.PrivateVehicleMileage, ReportsResources.AccountingReportVehicleMileage);

            return result;
        }

        protected CostItem CalculateAdvancePayment()
        {
            var advancePaymentRequests = SettlementRequest.BusinessTripParticipant.AdvancedPaymentRequests
                .Where(r => r.Request.Status == RequestStatus.Completed)
                .ToList();

            if (!advancePaymentRequests.Any())
            {
                return null;
            }

            var result = new CostItem
            {
                Group = CostItemGroupType.AdvancePayments,
                CurrencyIsoCode = CurrencyIsoCodes.PolishZloty
            };

            foreach (var advancePayment in advancePaymentRequests)
            {
                var exchangeRate = CalculatorCurrencyExchangeRateService.GetExchangeRateToPln(
                    advancePayment.Currency.IsoCode, SettlementRequest.SettlementDate.Value);
                var calculatedValue = advancePayment.Amount * exchangeRate.Rate;

                result.Value += calculatedValue;

                result.AddDiagnosticItem(new CurrencyRateMultiplicationDiagnosticItem
                {
                    Group = DiagnosticGroupType.AdvancePayment,
                    ForeignValue = CurrencyAmount.CreateValueAmount(advancePayment.Amount, advancePayment.Currency.IsoCode),
                    Rate = exchangeRate.Rate,
                    CalculatedValue = CurrencyAmount.CreateValueAmount(calculatedValue, CurrencyIsoCodes.PolishZloty)
                });

                if (advancePayment.Currency.IsoCode != CurrencyIsoCodes.PolishZloty)
                {
                    result.AddDiagnosticItem(new CurrencyExchangeRateDiagnosticItem
                    {
                        Group = DiagnosticGroupType.AdvancePayment,
                        CurrencyIsoCode = advancePayment.Currency.IsoCode,
                        Date = exchangeRate.Date,
                        Rate = exchangeRate.Rate,
                        TableNumber = exchangeRate.TableNumber
                    });
                }
            }

            var involvedProjects = SettlementRequest.BusinessTripParticipant.BusinessTrip.Projects.ToList();
            AddAccountingReportDiagnosticItems(result, involvedProjects,
                DiagnosticGroupType.AdvancePayment, ReportsResources.AccountingReportAdvancePayment);

            return result;
        }

        protected CostItem CalculateAdditionalAdvancePayment()
        {
            if (!SettlementRequest.AdditionalAdvancePayments.Any())
            {
                return null;
            }

            var result = new CostItem
            {
                Group = CostItemGroupType.AdditionalAdvancePayment,
                CurrencyIsoCode = CurrencyIsoCodes.PolishZloty
            };

            foreach (var advancePayment in SettlementRequest.AdditionalAdvancePayments)
            {
                var exchangeRate = CalculatorCurrencyExchangeRateService.GetExchangeRateToPln(
                    advancePayment.Currency.IsoCode, advancePayment.WithdrawnOn);
                var calculatedValue = advancePayment.Amount * exchangeRate.Rate;

                result.Value += calculatedValue;

                result.AddDiagnosticItem(new CurrencyRateMultiplicationDiagnosticItem
                {
                    Group = DiagnosticGroupType.AdditionalAdvancePayment,
                    ForeignValue = CurrencyAmount.CreateValueAmount(advancePayment.Amount, advancePayment.Currency.IsoCode),
                    Rate = exchangeRate.Rate,
                    CalculatedValue = CurrencyAmount.CreateValueAmount(calculatedValue, CurrencyIsoCodes.PolishZloty)
                });

                if (advancePayment.Currency.IsoCode != CurrencyIsoCodes.PolishZloty)
                {
                    result.AddDiagnosticItem(new CurrencyExchangeRateDiagnosticItem
                    {
                        Group = DiagnosticGroupType.AdditionalAdvancePayment,
                        CurrencyIsoCode = advancePayment.Currency.IsoCode,
                        Date = exchangeRate.Date,
                        Rate = exchangeRate.Rate,
                        TableNumber = exchangeRate.TableNumber
                    });
                }
            }

            var involvedProjects = SettlementRequest.BusinessTripParticipant.BusinessTrip.Projects.ToList();
            AddAccountingReportDiagnosticItems(result, involvedProjects,
                DiagnosticGroupType.AdditionalAdvancePayment, ReportsResources.AccountingReportAdditionalAdvancePayment);

            return result;
        }

        protected CostItem CalculateArrangements()
        {
            var arrangements = SettlementRequest.BusinessTripParticipant.BusinessTrip.Arrangements
                .Where(a => !a.Participants.Any() || a.Participants.Any(p => p.Id == SettlementRequest.BusinessTripParticipantId))
                .ToList();

            if (!arrangements.Any())
            {
                return null;
            }

            var result = new CostItem
            {
                Group = CostItemGroupType.Arrangements,
                CurrencyIsoCode = CurrencyIsoCodes.PolishZloty
            };

            foreach (var arrangement in arrangements.Where(a => a.Value.HasValue))
            {
                var accountingDate = arrangement.ArrangementType.HasInvoice() ? arrangement.IssuedOn : SettlementRequest.SettlementDate.Value;
                var exchangeRate = CalculatorCurrencyExchangeRateService.GetExchangeRateToPln(arrangement.Currency.IsoCode, accountingDate);

                var participantsNumber = arrangement.Participants.Any()
                    ? arrangement.Participants.Count
                    : SettlementRequest.BusinessTripParticipant.BusinessTrip.Participants.Count;
                var value = arrangement.Value.Value / participantsNumber;
                var calculatedValue = value * exchangeRate.Rate;

                result.Value += calculatedValue;

                var diagnosticItem = arrangement.ArrangementType.GetDiagnosticItemByArrangementType();

                diagnosticItem.Group = DiagnosticGroupType.Arrangements;
                diagnosticItem.Name = arrangement.Name;
                diagnosticItem.ArrangementValue = CurrencyAmount.CreateValueAmount(value, arrangement.Currency.IsoCode);
                diagnosticItem.CalculatedValue = CurrencyAmount.CreateValueAmount(calculatedValue, CurrencyIsoCodes.PolishZloty);

                result.AddDiagnosticItem(diagnosticItem);

                if (arrangement.Currency.IsoCode != CurrencyIsoCodes.PolishZloty)
                {
                    result.AddDiagnosticItem(new CurrencyExchangeRateDiagnosticItem
                    {
                        Group = DiagnosticGroupType.Arrangements,
                        CurrencyIsoCode = arrangement.Currency.IsoCode,
                        Date = exchangeRate.Date,
                        Rate = exchangeRate.Rate,
                        TableNumber = exchangeRate.TableNumber
                    });
                }
            }

            var involvedProjects = SettlementRequest.BusinessTripParticipant.BusinessTrip.Projects.ToList();
            AddAccountingReportDiagnosticItems(result, involvedProjects, DiagnosticGroupType.Arrangements, ReportsResources.AccountingReportArrangements);

            return result;
        }

        public override CostItem CalculateBusinessTripCompensationTotal(IEnumerable<CostItem> costItems)
        {
            var positiveGroups = new CostItemGroupType[]
            {
                CostItemGroupType.DailyAllowances,
                CostItemGroupType.LumpSum,
                CostItemGroupType.HoursWorkedForClientBonus,
                CostItemGroupType.OwnCosts,
                CostItemGroupType.PrivateVehicleMileage
            };

            var negativeGroups = new CostItemGroupType[]
            {
                CostItemGroupType.AdditionalAdvancePayment,
                CostItemGroupType.AdvancePayments
            };

            var totalCostItem = new CostItem
            {
                Group = CostItemGroupType.BusinessTripCompensationTotal,
                CurrencyIsoCode = CurrencyIsoCodes.PolishZloty,
                Value = costItems
                    .Sum(i => positiveGroups.Contains(i.Group) ? i.Value
                        : negativeGroups.Contains(i.Group) ? -i.Value
                        : 0m)
            };

            var involvedProjects = SettlementRequest.BusinessTripParticipant.BusinessTrip.Projects.ToList();
            AddAccountingReportDiagnosticItems(totalCostItem, involvedProjects, DiagnosticGroupType.BusinessTripCompensationTotal);

            return totalCostItem;
        }

        public override CostItem CalculateBusinessTripExpensesTotal(IEnumerable<CostItem> costItems)
        {
            var groups = new CostItemGroupType[]
            {
                CostItemGroupType.AdditionalAdvancePayment,
                CostItemGroupType.AdvancePayments,
                CostItemGroupType.Arrangements,
                CostItemGroupType.CompanyCosts,
                CostItemGroupType.BusinessTripCompensationTotal
            };

            var totalCostItem = new CostItem
            {
                Group = CostItemGroupType.BusinessTripExpensesTotal,
                CurrencyIsoCode = CurrencyIsoCodes.PolishZloty,
                Value = costItems.Where(i => groups.Contains(i.Group)).Sum(i => i.Value)
            };

            var involvedProjects = SettlementRequest.BusinessTripParticipant.BusinessTrip.Projects.ToList();
            AddAccountingReportDiagnosticItems(totalCostItem, involvedProjects, DiagnosticGroupType.BusinessTripExpensesTotal);

            return totalCostItem;
        }

        private IDictionary<long, long> CalculateAccommodationsPerCountry()
        {
            var domesticCountryId = SettlementRequest.BusinessTripParticipant.DepartureCity.CountryId;
            var abroadTimeEntries = SettlementRequest.AbroadTimeEntries.OrderBy(e => e.DepartureDateTime).ToList();

            var result = abroadTimeEntries
                .Select(e => e.DepartureCountryId)
                .Concat(new[] { domesticCountryId })
                .Distinct()
                .ToDictionary(id => id, _ => 0L);

            if (!abroadTimeEntries.Any())
            {
                result[domesticCountryId] += CalculateAccommodationsForRange(
                    SettlementRequest.DepartureDateTime.Value, SettlementRequest.ArrivalDateTime.Value);
            }
            else
            {
                result[domesticCountryId] += CalculateAccommodationsForRange(
                    SettlementRequest.DepartureDateTime.Value, abroadTimeEntries[0].DepartureDateTime);

                for (var i = 0; i < abroadTimeEntries.Count - 1; ++i)
                {
                    var entry = abroadTimeEntries[i];

                    result[entry.DepartureCountryId] += CalculateAccommodationsForRange(
                        entry.DepartureDateTime, abroadTimeEntries[i + 1].DepartureDateTime);
                }

                result[domesticCountryId] += CalculateAccommodationsForRange(
                    abroadTimeEntries.Last().DepartureDateTime, SettlementRequest.ArrivalDateTime.Value);
            }

            return result;
        }

        private static long CalculateAccommodationsForRange(DateTime from, DateTime to)
        {
            const double accommodationStartHour = 21.0;
            const double accommodationEndHour = 24.0 + 7.0;
            const double accommodationMinimumLengthInHours = 6.0;

            return DateHelper.GetDaysInRange(from, to)
                .Where(d =>
                {
                    var interval = DateRangeHelper.GetOverlapingInterval(
                        from, to, d.Date.AddHours(accommodationStartHour), d.Date.AddHours(accommodationEndHour));

                    return interval.HasValue && interval >= TimeSpan.FromHours(accommodationMinimumLengthInHours);
                })
                .Count();
        }

        private static void AddAccountingReportDiagnosticItems(
            CostItem target,
            IList<Project> involvedProjects,
            DiagnosticGroupType groupType,
            string paymentArea = null)
        {
            foreach (var project in involvedProjects)
            {
                target.AddDiagnosticItem(new AccountingReportDiagnosticItem
                {
                    Group = groupType,
                    Amount = 1,
                    AmountUnit = Units.Unit,
                    UnitPrice = target.Value / involvedProjects.Count,
                    UnitPriceCurrency = target.CurrencyIsoCode,
                    PaymentType = paymentArea == null
                        ? ReportsResources.AccountingReportBusinessTrip
                        : string.Format(ReportsResources.AccountingReportBusinessTripParametrized, paymentArea),
                    ProjectName = ProjectBusinessLogic.ClientProjectName.Call(project),
                    ProjectApn = project.Apn,
                });
            }
        }
    }
}
