﻿namespace Smt.Atomic.Business.Compensation.Consts
{
    public static class FilterCodes
    {
        public const string BonusAllEmployees = "all-employees";
        public const string BonusMyEmployees = "my-employees";
        public const string BonusOrgUnit = "org-unit";
    }
}
