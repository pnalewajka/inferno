﻿namespace Smt.Atomic.Business.Compensation.Enums
{
    public enum RenderingHintType
    {
        PlainDump = 0,

        MultipleEmploymentPeriodsWarning = 1,

        HoursMultiplication = 2,

        CurrencyRateMultiplication = 3,

        CurrencyExchangeRate = 4,

        KilometersMultiplication = 5,

        DailyAllowanceRate = 6,

        DailyAllowanceValue = 7,

        OvertimeMultiplication = 8,

        LumpSumCalculation = 9,

        SickLeaveMultiplication = 10,

        CalculationFailedError = 11,

        CostCurrencyRateMultiplication = 12,

        TransportArrangementCost = 13,

        AccommodationArrangementCost = 14,

        OtherArrangementCost = 15,

        ContractTypeId = 16,

        MonthlyBonus = 17,

        AccountingReport = 18,

        IntiveGmbhDailyAllowanceValue = 19,

        IntiveGmbhCostCurrencyRateMultiplication = 20,

        Expense = 21,
    }
}
