﻿namespace Smt.Atomic.Business.Compensation.Enums
{
    public enum CostItemGroupType
    {
        OwnCosts = 1,

        HoursWorkedForClientBonus = 2,

        HourlyCompensation = 3,

        CompanyCosts = 4,

        AdvancePayments = 5,

        AdditionalAdvancePayment = 6,

        PrivateVehicleMileage = 7,

        DailyAllowances = 8,

        TimeTrackingTotal = 9,

        BusinessTripCompensationTotal = 10,

        BusinessTripExpensesTotal = 11,

        CompensationTotal = 12,

        OvertimeBonus = 13,

        MonthlyCompensation = 14,

        SickLeaveCompensation = 15,

        Arrangements = 16,

        LumpSum = 17,

        MonthlyBonus = 18,

        Expense = 19,

        AdditionalCostsTotal = 20,
    }
}
