﻿using System;

namespace Smt.Atomic.Business.Compensation.Enums
{
    [Flags]
    public enum TimeTrackingCompensationCalculatorOptions
    {
        None = 0,

        IsFirstWorkingPeriod = 1 << 0,
    }
}
