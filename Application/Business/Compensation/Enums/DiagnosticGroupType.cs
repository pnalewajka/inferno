﻿namespace Smt.Atomic.Business.Compensation.Enums
{
    public enum DiagnosticGroupType
    {
        Summary = 1,

        OwnCosts = 2,

        HoursWorkedForClientBonus = 3,

        HourlyCompensation = 4,

        CompanyCosts = 5,

        AdditionalAdvancePayment = 6,

        PrivateVehicleMileage = 7,

        DailyAllowances = 8,

        OvertimeBonus = 9,

        MonthlyCompensation = 10,

        SickLeaveCompensation = 11,

        Arrangements = 12,

        AdvancePayment = 13,

        LumpSum = 14,

        MonthlyBonus = 15,

        BusinessTripCompensationTotal = 16,

        BusinessTripExpensesTotal = 17,

        Expense = 18,
    }
}
