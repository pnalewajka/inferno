﻿namespace Smt.Atomic.Business.Compensation.Enums
{
    public enum DiagnosticLevel
    {
        Error = -1,

        Critical = 1,

        Info = 2,

        Debug = 3
    }
}
