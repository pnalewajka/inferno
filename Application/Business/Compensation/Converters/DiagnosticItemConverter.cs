﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Smt.Atomic.Business.Compensation.DiagnosticItems;
using Smt.Atomic.Business.Compensation.Enums;

namespace Smt.Atomic.Business.Compensation.Converters
{
    public class DiagnosticItemConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(List<DiagnosticItem>);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var token = JToken.Load(reader);
            var objectCreationHandling = serializer.ObjectCreationHandling;

            serializer.ObjectCreationHandling = ObjectCreationHandling.Replace;

            var result = token.Select(i => CreateDiagnosticItem(i, serializer)).ToList();

            serializer.ObjectCreationHandling = objectCreationHandling;

            return result;
        }

        private DiagnosticItem CreateDiagnosticItem(JToken token, JsonSerializer serializer)
        {
            try
            {
                const string diagnosticItemNamespace = "Smt.Atomic.Business.Compensation.DiagnosticItems";

                var renderingHintType = token[nameof(DiagnosticItem.Type)].ToObject<RenderingHintType>();

                return (DiagnosticItem)token.ToObject(Type.GetType($"{diagnosticItemNamespace}.{renderingHintType}DiagnosticItem"), serializer);
            }
            catch
            {
#if DEV
                if (AppDomain.CurrentDomain.GetAssemblies().Any(a => a.FullName.Contains("CalculatorUtils")))
                {
                    return token.ToObject<LegacyDiagnosticItem>(serializer);
                }
#endif
                throw;
            }
        }

        public override bool CanWrite
        {
            get { return true; }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value);
        }
    }
}
