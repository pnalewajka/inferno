﻿using System.Linq;
using Smt.Atomic.Business.Allocation.Helpers;
using Smt.Atomic.Business.Compensation.DiagnosticItems;
using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.Business.Compensation.ReportModels;
using Smt.Atomic.Business.Compensation.Resources;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Compensation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Compensation.ReportDataSources
{
    [Identifier("DataSources.AccountingReport")]
    [RequireRole(SecurityRoleType.CanGenerateAccountingReport)]
    [DefaultReportDefinition(
        CompensationReportsHelper.AccountingReportReportCode,
        nameof(ReportsResources.AccountingReportName),
        nameof(ReportsResources.AccountingReportDescription),
        MenuAreas.Compensation,
        MenuGroups.CompensationReports,
        ReportingEngine.MsExcel,
        "Smt.Atomic.Business.Compensation.ReportTemplates.AccountingReport.xlsx",
        typeof(ReportsResources))]
    public class AccountingReportDataSource
        : BaseReportDataSource<AccountingReportParametersDto>
    {
        private readonly IUnitOfWorkService<ICompensationDbScope> _unitOfWorkService;
        private readonly IClassMapping<InvoiceCalculation, InvoiceCalculationDto> _invoiceToDto;
        private readonly ITimeService _timeService;

        public AccountingReportDataSource(
            IUnitOfWorkService<ICompensationDbScope> unitOfWorkService,
            IClassMapping<InvoiceCalculation, InvoiceCalculationDto> invoiceToDto,
            ITimeService timeService)
        {
            _unitOfWorkService = unitOfWorkService;
            _invoiceToDto = invoiceToDto;
            _timeService = timeService;
        }

        public override object GetData(
            ReportGenerationContext<AccountingReportParametersDto> reportGenerationContext)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var invoices = ApplyFiltering(reportGenerationContext, unitOfWork.Repositories.InvoiceCalculations);
                var model = new AccountingReportModel();

                foreach (var invoice in invoices)
                {
                    var dto = _invoiceToDto.CreateFromSource(invoice);
                    var reportItems = dto.CalculationResult.GetDiagnosticItems<AccountingReportDiagnosticItem>();
                    var contractTypeIds = dto.CalculationResult.DiagnosticItems
                        .OfType<ContractTypeIdDiagnosticItem>()
                        .Select(i => i.ContractTypeId)
                        .ToList();

                    var contractTypeNames = string.Join(", ", unitOfWork.Repositories.ContractTypes
                        .Where(c => contractTypeIds.Contains(c.Id))
                        .Select(c => c.NameEn));

                    foreach (var reportItem in reportItems)
                    {
                        model.Details.Add(new AccountingReportModel.InvoiceDetails
                        {
                            Id = invoice.Id,
                            Year = invoice.Year,
                            Month = invoice.Month,
                            FullName = invoice.Employee.DisplayName,
                            Acronym = invoice.Employee.Acronym,
                            OrgUnit = invoice.Employee.OrgUnit.Code,
                            Company = invoice.Employee.Company.Name,
                            ContractType = contractTypeNames,
                            CalculatedOn = invoice.NotifiedOn,
                            ProjectName = reportItem.ProjectName,
                            ProjectApn = reportItem.ProjectApn,
                            DataSource = reportItem.Group.GetDescriptionOrValue(),
                            PaymentType = reportItem.PaymentType,
                            Amount = reportItem.Amount,
                            AmountUnit = reportItem.AmountUnit,
                            UnitPrice = reportItem.UnitPrice,
                            UnitPriceCurrency = reportItem.UnitPriceCurrency,
                        });
                    }
                }

                return model;
            }
        }

        private IQueryable<InvoiceCalculation> ApplyFiltering(
            ReportGenerationContext<AccountingReportParametersDto> reportGenerationContext,
            IQueryable<InvoiceCalculation> repository)
        {
            if (reportGenerationContext.Parameters.EmployeeIds.Any())
            {
                repository = repository.Where(
                    i => reportGenerationContext.Parameters.EmployeeIds.Contains(i.EmployeeId));
            }

            if (reportGenerationContext.Parameters.Year.HasValue)
            {
                repository = repository.Where(
                    i => i.Year == reportGenerationContext.Parameters.Year.Value);
            }

            if (reportGenerationContext.Parameters.Month.HasValue)
            {
                repository = repository.Where(
                    i => i.Month == reportGenerationContext.Parameters.Month.Value);
            }

            repository = repository.Where(i => i.IsValid);

            return repository;
        }

        public override AccountingReportParametersDto GetDefaultParameters(
            ReportGenerationContext<AccountingReportParametersDto> reportGenerationContext)
        {
            var currentDate = _timeService.GetCurrentDate();

            return new AccountingReportParametersDto
            {
                Year = currentDate.Year,
                Month = currentDate.Month,
            };
        }
    }
}
