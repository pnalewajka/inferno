﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.Helpers;
using Smt.Atomic.Business.Compensation.DiagnosticItems;
using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.Business.Compensation.ReportModels;
using Smt.Atomic.Business.Compensation.Resources;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Compensation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Compensation.ReportDataSources
{
    [Identifier("DataSources.InvoiceCalculations")]
    [RequireRole(SecurityRoleType.CanGenerateInvoiceCalculationsReport)]
    [DefaultReportDefinition(
        CompensationReportsHelper.InvoiceCalculationsReportCode,
        nameof(ReportsResources.InvoiceCalculationsPivotReportName),
        nameof(ReportsResources.InvoiceCalculationsPivotReportDescription),
        MenuAreas.Compensation,
        MenuGroups.CompensationReports,
        ReportingEngine.PivotTable,
        "Smt.Atomic.Business.Compensation.ReportTemplates.InvoiceCalculationsPivot-config.js",
        typeof(ReportsResources))]
    public class InvoiceCalculationsDataSource
        : BaseReportDataSource<InvoiceCalculationsDataSourceParametersDto>
    {
        private readonly IUnitOfWorkService<ICompensationDbScope> _unitOfWorkService;
        private readonly ITimeService _timeService;
        private readonly IClassMapping<InvoiceCalculation, InvoiceCalculationDto> _notificationToDto;

        public InvoiceCalculationsDataSource(
            IUnitOfWorkService<ICompensationDbScope> unitOfWorkService,
            ITimeService timeService,
            IClassMapping<InvoiceCalculation, InvoiceCalculationDto> notificationToDto)
        {
            _unitOfWorkService = unitOfWorkService;
            _timeService = timeService;
            _notificationToDto = notificationToDto;
        }

        public override object GetData(ReportGenerationContext<InvoiceCalculationsDataSourceParametersDto> context)
        {
            return new InvoiceCalculationsReportModel
            {
                Invoices = GetInvoices(context).ToList(),
            };
        }

        private IEnumerable<InvoiceCalculationsReportModel.InvoiceInfo> GetInvoices(
            ReportGenerationContext<InvoiceCalculationsDataSourceParametersDto> context)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var notifications = unitOfWork.Repositories.InvoiceCalculations
                    .Where(n => !context.Parameters.Year.HasValue || n.Year == context.Parameters.Year)
                    .Where(n => !context.Parameters.Month.HasValue || n.Month == context.Parameters.Month)
                    .ToList();

                var employmentPeriodForDate = DateRangeHelper.IsDateInRange<EmploymentPeriod>(p => p.StartDate, p => p.EndDate);

                foreach (var n in notifications.Where(n => n.IsValid))
                {
                    var dto = _notificationToDto.CreateFromSource(n);

                    var contractTypeIds = dto.CalculationResult.DiagnosticItems
                        .OfType<ContractTypeIdDiagnosticItem>()
                        .Select(i => i.ContractTypeId)
                        .ToList();

                    // fallback for legacy data
                    if (!contractTypeIds.Any())
                    {
                        contractTypeIds = n.Employee.EmploymentPeriods.AsQueryable()
                            .Where(employmentPeriodForDate.Parametrize(new DateTime(n.Year, n.Month, 1)))
                            .Select(p => p.ContractTypeId).ToList();
                    }

                    yield return new InvoiceCalculationsReportModel.InvoiceInfo
                    {
                        Id = n.Id,
                        Year = n.Year,
                        Month = n.Month,
                        FullName = n.Employee.DisplayName,
                        Acronym = n.Employee.Acronym,
                        OrgUnit = n.Employee.OrgUnit.Code,
                        ContractType = string.Join(", ", unitOfWork.Repositories.ContractTypes
                            .Where(c => contractTypeIds.Contains(c.Id))
                            .Select(c => c.NameEn)),
                        CostItems = dto.CalculationResult.CostItems,
                        InvolvedBusinessTripCount = dto.CalculationResult.CostItems
                            .Count(i => i.Group == CostItemGroupType.BusinessTripCompensationTotal),
                        NotifiedOn = dto.NotifiedOn,
                        Version = notifications.Count(i => i.EmployeeId == n.EmployeeId),
                    };
                }
            }
        }

        public override InvoiceCalculationsDataSourceParametersDto GetDefaultParameters(
            ReportGenerationContext<InvoiceCalculationsDataSourceParametersDto> context)
        {
            var currentDate = _timeService.GetCurrentDate();

            return new InvoiceCalculationsDataSourceParametersDto
            {
                Year = currentDate.Year,
                Month = currentDate.Month,
            };
        }
    }
}
