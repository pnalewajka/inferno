﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Helpers;
using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.DiagnosticItems;
using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.Business.Compensation.ReportModels;
using Smt.Atomic.Business.Compensation.Resources;
using Smt.Atomic.Business.Compensation.Services;
using Smt.Atomic.Business.Reporting.Attributes;
using Smt.Atomic.Business.Reporting.Consts;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Services;
using Smt.Atomic.Business.Workflows.BusinessLogics;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Compensation.ReportDataSources
{
    [Identifier("DataSource.IntiveIncPaymentNotificationDataSource")]
    [RequireRole(SecurityRoleType.CanGenerateIntiveIncPaymentNotificationReport)]
    [DefaultReportDefinition(
        CompensationReportsHelper.IntiveIncPaymentNotificationReportCode,
        nameof(ReportsResources.IntiveIncPaymentNotificationReportName),
        nameof(ReportsResources.InvoiceCalculationsPivotReportDescription),
        MenuAreas.Compensation,
        MenuGroups.CompensationReports,
        ReportingEngine.MsExcelNoTemplate,
        new string[0],
        "Report.xlsx",
        typeof(ReportsResources))]
    public class IntiveIncPaymentNotificationDataSource
        : BaseReportDataSource<IntiveIncPaymentNotificationDataSourceParametersDto>
    {
        private readonly ISystemParameterService _systemParameterService;
        private readonly OxrCurrencyExchangeRateService _currencyExchangeRateService;
        private readonly IUnitOfWorkService<ICompensationDbScope> _unitOfWorkService;

        public IntiveIncPaymentNotificationDataSource(
            ISystemParameterService systemParameterService,
            OxrCurrencyExchangeRateService currencyExchangeRateService,
            IUnitOfWorkService<ICompensationDbScope> unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
            _systemParameterService = systemParameterService;
            _currencyExchangeRateService = currencyExchangeRateService;
        }

        public override object GetData(ReportGenerationContext<IntiveIncPaymentNotificationDataSourceParametersDto> context)
        {
            const string tabName = "Data";

            var result = new List<IntiveIncPaymentNotificationReportModel>();

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var contractTypeNames = _systemParameterService.GetParameter<string[]>(
                    ParameterKeys.CompensationIntiveIncPaymentNotificationContractTypes);

                if (context.Parameters.BusinessTripSettlementRequestId.HasValue)
                {
                    var rows = GetRowsForSettlementRequest(unitOfWork, context.Parameters.BusinessTripSettlementRequestId.Value, contractTypeNames);

                    result.AddRange(rows);
                }

                if (context.Parameters.ExpenseRequestId.HasValue)
                {
                    var row = GetRowForExpenseRequest(unitOfWork, context.Parameters.ExpenseRequestId.Value, contractTypeNames);

                    result.Add(row);
                }
            }

            if (!result.Any())
            {
                throw new BusinessException(ReportsResources.IntiveIncPaymentNotificationNoDataError);
            }

            return new Dictionary<string, object>()
            {
                [tabName] = result.ToArray()
            };
        }

        private IntiveIncPaymentNotificationReportModel GetRowForExpenseRequest(
            IUnitOfWork<ICompensationDbScope> unitOfWork, long expenseRequestId, string[] contractTypeNames)
        {
            const string category = "Miscellaneous";

            var expenseRequest = unitOfWork.Repositories.ExpenseRequests
                .Where(r => r.Id == expenseRequestId && RequestBusinessLogic.IsApprovedOrCompleted.Call(r.Request))
                .Where(r => r.Employee.EmploymentPeriods.AsQueryable()
                    .Where(p => EmploymentPeriodBusinessLogic.ForDate.Call(p, r.Date))
                    .Any(p => contractTypeNames.Contains(p.ContractType.NameEn)))
                .Select(r => new { r.Employee.FirstName, r.Employee.LastName, r.Amount, r.Date, r.Currency.IsoCode, r.Justification })
                .SingleOrDefault();

            if (expenseRequest == null)
            {
                return null;
            }

            var amount = expenseRequest.Amount;

            if (expenseRequest.IsoCode != CurrencyIsoCodes.UnitedStatesDollar)
            {
                var exchangeRate = _currencyExchangeRateService.GetExchangeRateForDay(expenseRequest.IsoCode, expenseRequest.Date);

                amount *= exchangeRate.Rate;
            }

            return new IntiveIncPaymentNotificationReportModel
            {
                Name = expenseRequest.FirstName,
                Surname = expenseRequest.LastName,
                Amount = amount,
                Category = category,
                Description = expenseRequest.Justification
            };
        }

        private IEnumerable<IntiveIncPaymentNotificationReportModel> GetRowsForSettlementRequest(
            IUnitOfWork<ICompensationDbScope> unitOfWork, long settlementRequestId, string[] contractTypeNames)
        {
            const string category = "Travel Others";

            var settlementRequest = unitOfWork.Repositories.BusinessTripSettlementRequests
                .Where(r => r.Id == settlementRequestId && RequestBusinessLogic.IsApprovedOrCompleted.Call(r.Request))
                .Where(r => r.SettlementDate.HasValue && r.BusinessTripParticipant.Employee.EmploymentPeriods.AsQueryable()
                    .Where(p => EmploymentPeriodBusinessLogic.ForDate.Call(p, r.SettlementDate.Value))
                    .Any(p => contractTypeNames.Contains(p.ContractType.NameEn)))
                .Select(r => new
                {
                    r.BusinessTripParticipant.BusinessTripId,
                    r.BusinessTripParticipant.Employee.FirstName,
                    r.BusinessTripParticipant.Employee.LastName,
                    r.CalculationResult
                })
                .SingleOrDefault();

            if (settlementRequest == null)
            {
                yield break;
            }

            var calculation = JsonHelper.Deserialize<CalculationResult>(settlementRequest.CalculationResult);
            var ownCosts = calculation.CostItems
                .Where(i => i.Group == CostItemGroupType.OwnCosts)
                .SelectMany(i => i.DiagnosticItems
                    .Where(d => d.Group == DiagnosticGroupType.OwnCosts)
                    .OfType<CostCurrencyRateMultiplicationDiagnosticItem>())
                    .ToList();

            foreach (var ownCost in ownCosts)
            {
                yield return new IntiveIncPaymentNotificationReportModel
                {
                    Name = settlementRequest.FirstName,
                    Surname = settlementRequest.LastName,
                    Amount = ownCost.CalculatedValue.Amount,
                    Category = category,
                    BusinessTripIdentifier = GetBusinessTripIdentifier(settlementRequest.BusinessTripId),
                    Description = ownCost.Name
                };
            }

            var kilometersMultiplications = calculation.GetDiagnosticItems<KilometersMultiplicationDiagnosticItem>();

            foreach (var kilometersMultiplication in kilometersMultiplications)
            {
                yield return new IntiveIncPaymentNotificationReportModel
                {
                    Name = settlementRequest.FirstName,
                    Surname = settlementRequest.LastName,
                    Amount = kilometersMultiplication.CalculatedValue.Amount,
                    Category = category,
                    BusinessTripIdentifier = GetBusinessTripIdentifier(settlementRequest.BusinessTripId),
                    Description = string.Format(ReportsResources.PrivateVehicleMileageDescriptionFormat, kilometersMultiplication.Kilometers)
                };
            }
        }

        public override IntiveIncPaymentNotificationDataSourceParametersDto GetDefaultParameters(
            ReportGenerationContext<IntiveIncPaymentNotificationDataSourceParametersDto> context)
        {
            return new IntiveIncPaymentNotificationDataSourceParametersDto();
        }

        private static string GetBusinessTripIdentifier(long businessTripId)
        {
            return $"BT-{businessTripId}";
        }
    }
}
