﻿using System.Linq;
using Smt.Atomic.Business.Compensation.BusinessEvents;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Compensation.BusinessEventHandlers
{
    public class CompensationChangedEventHandler : BusinessEventHandler<CompensationChangedEvent>
    {
        private readonly IUnitOfWorkService<ICompensationDbScope> _unitOfWorkService;

        public CompensationChangedEventHandler(IUnitOfWorkService<ICompensationDbScope> unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        public override void Handle(CompensationChangedEvent businessEvent)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var timeReport = unitOfWork.Repositories.TimeReports
                    .Where(t => t.EmployeeId == businessEvent.EmployeeId
                        && t.Year == businessEvent.Year && t.Month == businessEvent.Month
                        && t.InvoiceNotificationStatus == InvoiceNotificationStatus.NotificationSent)
                    .SingleOrDefault();

                if (timeReport != null)
                {
                    timeReport.InvoiceNotificationStatus = InvoiceNotificationStatus.NotificationOutdated;

                    unitOfWork.Commit();
                }
            }
        }
    }
}
