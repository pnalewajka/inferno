﻿using System.IO;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Allocation.Helpers;
using Smt.Atomic.Business.Compensation.BusinessEvents;
using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Compensation.BusinessEventHandlers
{
    public class PaymentApprovedEventHandler : BusinessEventHandler<PaymentApprovedEvent>
    {
        private readonly IReportingService _reportingService;
        private readonly IRazorTemplateService _razorTemplateService;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IUnitOfWorkService<ICompensationDbScope> _unitOfWorkService;

        public PaymentApprovedEventHandler(
            IReportingService reportingService,
            IRazorTemplateService razorTemplateService,
            IReliableEmailService reliableEmailService,
            IMessageTemplateService messageTemplateService,
            ISystemParameterService systemParameterService,
            IUnitOfWorkService<ICompensationDbScope> unitOfWorkService)
        {
            _reportingService = reportingService;
            _razorTemplateService = razorTemplateService;
            _reliableEmailService = reliableEmailService;
            _messageTemplateService = messageTemplateService;
            _systemParameterService = systemParameterService;
            _unitOfWorkService = unitOfWorkService;
        }

        public override void Handle(PaymentApprovedEvent businessEvent)
        {
            if (!ShouldSendEmail(businessEvent.BusinessTripSettlementRequestId, businessEvent.ExpenseRequestId))
            {
                return;
            }

            var reportGenerationContext = GenerateReport(businessEvent.BusinessTripSettlementRequestId, businessEvent.ExpenseRequestId);

            if (string.IsNullOrWhiteSpace(reportGenerationContext.Definition.OutputFileNameTemplate))
            {
                throw new InvalidDataException("Report has no proper output filename template defined");
            }

            var content = StreamHelper.ReadBytes(reportGenerationContext.Content).ToArray();
            var filename = _razorTemplateService.TemplateRunAndCompile(
                reportGenerationContext.ReportData.Value.GetType().FullName,
                reportGenerationContext.Definition.OutputFileNameTemplate,
                reportGenerationContext.ReportData.Value);

            SendEmail(filename, content);
        }

        private bool ShouldSendEmail(long? businessTripSettlementRequestId, long? expenseRequestId)
        {
            var contractTypeNames = _systemParameterService.GetParameter<string[]>(
                ParameterKeys.CompensationIntiveIncPaymentNotificationContractTypes);

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var properContractTypeForExpense = unitOfWork.Repositories.ExpenseRequests
                   .Where(r => r.Id == expenseRequestId)
                   .Any(r => r.Employee.EmploymentPeriods.AsQueryable()
                       .Where(p => EmploymentPeriodBusinessLogic.ForDate.Call(p, r.Date))
                       .Any(p => contractTypeNames.Contains(p.ContractType.NameEn)));

                var properContractTypeForSettlement = unitOfWork.Repositories.BusinessTripSettlementRequests
                    .Where(r => r.Id == businessTripSettlementRequestId && r.SettlementDate.HasValue)
                    .Any(r => r.BusinessTripParticipant.Employee.EmploymentPeriods.AsQueryable()
                        .Where(p => EmploymentPeriodBusinessLogic.ForDate.Call(p, r.SettlementDate.Value))
                        .Any(p => contractTypeNames.Contains(p.ContractType.NameEn)));

                return properContractTypeForExpense || properContractTypeForSettlement;
            }
        }

        private IReportGenerationContext GenerateReport(long? businessTripSettlementRequestId, long? expenseRequestId)
        {
            var reportCode = CompensationReportsHelper.IntiveIncPaymentNotificationReportCode;
            var parameters = new IntiveIncPaymentNotificationDataSourceParametersDto
            {
                BusinessTripSettlementRequestId = businessTripSettlementRequestId,
                ExpenseRequestId = expenseRequestId
            };

            return _reportingService.PerformReport(reportCode, parameters);
        }

        private void SendEmail(string attachmentFilename, byte[] attachmentContent)
        {
            var subject = _messageTemplateService.ResolveTemplate(TemplateCodes.IntiveIncPaymentNotificationSubject, null);
            var body = _messageTemplateService.ResolveTemplate(TemplateCodes.IntiveIncPaymentNotificationBody, null);
            var address = _systemParameterService.GetParameter<string>(ParameterKeys.CompensationIntiveIncPaymentNotificationEmailAddress);

            var message = new EmailMessageDto
            {
                IsHtml = body.IsHtml,
                Body = body.Content,
                Subject = subject.Content,
                Attachments = new[] { new EmailAttachmentDto { Name = attachmentFilename, Content = attachmentContent } }
            };

            message.Recipients.Add(new EmailRecipientDto { EmailAddress = address });

            _reliableEmailService.EnqueueMessage(message);
        }
    }
}
