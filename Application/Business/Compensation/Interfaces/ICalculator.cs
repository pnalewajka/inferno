﻿using Smt.Atomic.Business.Compensation.CalculationResults;

namespace Smt.Atomic.Business.Compensation.Interfaces
{
    public interface ICalculator
    {
        CalculationResult Calculate();
    }
}
