﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Compensation.Interfaces
{
    public interface ITimeReportInvoiceService
    {
        InvoiceDetailsDto GetInvoiceDetails(int year, byte month, long employeeId);

        void UploadInvoice(int year, byte month, long employeeId, DocumentDto invoiceDto);

        BoolResult AcceptInvoicesForPayment(ICollection<long> timeReportIds);

        BoolResult RejectInvoices(ICollection<long> timeReportIds, string justification);

        InvoiceStatus GetInitialInvoiceStatusForEmployee(long employeeId, int year, byte month);
    }
}
