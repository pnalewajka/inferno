﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Compensation.Dto;

namespace Smt.Atomic.Business.Compensation.Interfaces
{
    public interface IBonusCardIndexDataService : ICardIndexDataService<BonusDto>
    {
    }
}

