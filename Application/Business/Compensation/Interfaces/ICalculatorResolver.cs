﻿namespace Smt.Atomic.Business.Compensation.Interfaces
{
    public interface ICalculatorResolver
    {
        ICalculator GetErrorCalculator(ICalculator innerCalculator);

        ICalculator GetCompensationCalculator(long employeeId, int year, byte month);

        ICalculator GetCalculatorBySettlementRequestId(long settlementRequestId);

        ICalculator GetCalculatorByTimeReportId(long timeReportId);
    }
}
