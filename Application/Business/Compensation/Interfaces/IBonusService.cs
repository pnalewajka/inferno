﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Compensation.Interfaces
{
    public interface IBonusService
    {
        int GetAffectedInvoicesCount(long employeeId, DateTime from, DateTime? to);

        void RecalculateAffectedInvoices(long employeeId, DateTime from, DateTime? to);
    }
}
