﻿using System;
using Smt.Atomic.Business.Compensation.Dto;

namespace Smt.Atomic.Business.Compensation.Interfaces
{
    public interface ICurrencyExchangeRateService
    {
        CurrencyExchangeRateDto GetExchangeRateForDay(string quotedCurrencyIsoCode, DateTime date);
    }
}
