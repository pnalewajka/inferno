﻿using System;
using Smt.Atomic.Business.Compensation.Dto;

namespace Smt.Atomic.Business.Compensation.Interfaces
{
    public interface ICalculatorCurrencyExchangeRateService
    {
        decimal SameCurrencyExchangeRate { get; }

        CurrencyExchangeRateDto GetExchangeRateToEur(string currencyIsoCode, DateTime date);

        CurrencyExchangeRateDto GetExchangeRateToGbp(string currencyIsoCode, DateTime date);

        CurrencyExchangeRateDto GetExchangeRateToPln(string currencyIsoCode, DateTime date);

        CurrencyExchangeRateDto GetExchangeRateToUsd(string currencyIsoCode, DateTime date);
    }
}
