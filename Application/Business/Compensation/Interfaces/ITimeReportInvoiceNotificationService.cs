﻿using System.Collections.Generic;
using System.Threading;

namespace Smt.Atomic.Business.Compensation.Interfaces
{
    public interface ITimeReportInvoiceNotificationService
    {
        void MarkForRecalculating(IEnumerable<long> timeReportIds);

        void MarkAllForRecalculating(int year, int month);

        void SendMarkedNotifications(CancellationToken cancellationToken);
    }
}
