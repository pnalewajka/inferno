﻿using System;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.Compensation.Interfaces
{
    public interface ICalculatorFactory : IDisposable
    {
        ICalculator CreateAggregatedTimeTrackingCompensationCalculator(ICalculator[] calculators);

        ICalculator CreateBusinessTripCompensationCalculator(BusinessTripCompensationCalculators calculatorId,
            BusinessTripSettlementRequest settlementRequest, EmploymentPeriod[] employmentPeriods);

        ICalculator CreateEmptyTimeTrackingCompensationCalculator();

        ICalculator CreateErrorHandlerCalculator(ICalculator innerCalculator);

        ICalculator CreateTimeTrackingCompensationCalculator(TimeTrackingCompensationCalculators calculatorId,
            TimeReport timeReport, EmploymentPeriod employmentPeriod, TimeTrackingCompensationCalculatorOptions calculatorOptions);

        ICalculator CreateTotalCompensationCalculator(ICalculator timeTrackingCalculator, ICalculator[] businessTripCalculators);
    }
}
