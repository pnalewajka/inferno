﻿using System;
using Newtonsoft.Json;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Compensation.DiagnosticItems
{
    [Serializable]
    public class SickLeaveMultiplicationDiagnosticItem : DiagnosticItem
    {
        public override RenderingHintType Type => RenderingHintType.SickLeaveMultiplication;

        public override DiagnosticLevel Level => DiagnosticLevel.Debug;

        [JsonIgnore]
        public decimal Hours { get; set; }

        [JsonIgnore]
        public CurrencyAmount HourlyRate { get; set; }

        public decimal SickLeaveRate { get; set; }

        [JsonIgnore]
        public CurrencyAmount Compensation { get; set; }

        public override string[] SerializeValues() => new string[]
        {
            SerializeDecimal(Hours, 1),
            HourlyRate.ToString(),
            $"{SerializeDecimal(SickLeaveRate * 100, 2)}%",
            Compensation.ToString(),
        };

        public override void DeserializeValues(string[] values)
        {
            Hours = ParseDecimal(values[0]);
            HourlyRate = CurrencyAmount.Parse(values[1]);
            SickLeaveRate = ParsePercentage(values[2]);
            Compensation = CurrencyAmount.Parse(values[3]);
        }
    }
}
