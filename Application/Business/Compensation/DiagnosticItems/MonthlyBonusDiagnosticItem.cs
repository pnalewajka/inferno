﻿using System;
using Newtonsoft.Json;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Compensation.DiagnosticItems
{
    [Serializable]
    public class MonthlyBonusDiagnosticItem : DiagnosticItem
    {
        private const string NoEndDateSymbol = "∞";

        public override RenderingHintType Type => RenderingHintType.MonthlyBonus;

        public override DiagnosticLevel Level => DiagnosticLevel.Debug;

        [JsonIgnore]
        public CurrencyAmount Bonus { get; set; }

        [JsonIgnore]
        public DateTime StartDate { get; set; }

        [JsonIgnore]
        public DateTime? EndDate { get; set; }

        [JsonIgnore]
        public string Justification { get; set; }

        public override string[] SerializeValues() => new string[]
        {
            Bonus.ToString(),
            $"{StartDate.ToInvariantString(DateFormat)} - {EndDate?.ToInvariantString(DateFormat) ?? NoEndDateSymbol}",
            Justification
        };

        public override void DeserializeValues(string[] values)
        {
            var splittedValues = values[1].Split(new[] { " - " }, StringSplitOptions.RemoveEmptyEntries);

            Bonus = CurrencyAmount.Parse(values[0]);
            StartDate = ParseDateTime(splittedValues[0]);
            EndDate = splittedValues[1] != NoEndDateSymbol ? ParseDateTime(splittedValues[1]) : (DateTime?)null;
            Justification = values[2];
        }
    }
}
