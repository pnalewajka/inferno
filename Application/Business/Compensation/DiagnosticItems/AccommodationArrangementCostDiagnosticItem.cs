﻿using System;
using Smt.Atomic.Business.Compensation.Enums;

namespace Smt.Atomic.Business.Compensation.DiagnosticItems
{
    [Serializable]
    public class AccommodationArrangementCostDiagnosticItem : ArrangementCostDiagnosticItem
    {
        public override RenderingHintType Type => RenderingHintType.AccommodationArrangementCost;
    }
}
