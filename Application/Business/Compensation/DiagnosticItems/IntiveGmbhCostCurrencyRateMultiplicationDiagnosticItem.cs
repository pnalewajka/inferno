﻿using System;
using Newtonsoft.Json;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Compensation.DiagnosticItems
{
    [Serializable]
    public class IntiveGmbhCostCurrencyRateMultiplicationDiagnosticItem : CostCurrencyRateMultiplicationDiagnosticItem
    {
        public override RenderingHintType Type => RenderingHintType.IntiveGmbhCostCurrencyRateMultiplication;

        public override DiagnosticLevel Level => DiagnosticLevel.Info;

        [JsonIgnore]
        public DateTime Date { get; set; }

        public override string[] SerializeValues() => new string[]
        {
            Date.ToInvariantString(DateFormat),
            Name,
            CostValue.ToString(),
            SerializeDecimal(Rate, 4),
            CalculatedValue.ToString(),
        };

        public override void DeserializeValues(string[] values)
        {
            Date = ParseDateTime(values[0]);
            Name = values[1];
            CostValue = CurrencyAmount.Parse(values[2]);
            Rate = ParseDecimal(values[3]);
            CalculatedValue = CurrencyAmount.Parse(values[4]);
        }
    }
}
