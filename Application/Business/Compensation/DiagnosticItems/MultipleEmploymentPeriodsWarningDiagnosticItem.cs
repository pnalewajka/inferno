﻿using System;
using Smt.Atomic.Business.Compensation.Enums;

namespace Smt.Atomic.Business.Compensation.DiagnosticItems
{
    [Serializable]
    public class MultipleEmploymentPeriodsWarningDiagnosticItem : DiagnosticItem
    {
        public override RenderingHintType Type => RenderingHintType.MultipleEmploymentPeriodsWarning;

        public override DiagnosticLevel Level => DiagnosticLevel.Critical;

        public override string[] SerializeValues() => new string[0];

        public override void DeserializeValues(string[] values)
        {
        }
    }
}
