﻿using System;
using Newtonsoft.Json;
using Smt.Atomic.Business.Compensation.Enums;

namespace Smt.Atomic.Business.Compensation.DiagnosticItems
{
    [Serializable]
    public class AccountingReportDiagnosticItem : DiagnosticItem
    {
        public override RenderingHintType Type => RenderingHintType.AccountingReport;

        public override DiagnosticLevel Level => DiagnosticLevel.Info;

        [JsonIgnore]
        public string ProjectName { get; set; }

        [JsonIgnore]
        public string ProjectApn { get; set; }

        [JsonIgnore]
        public string PaymentType { get; set; }

        [JsonIgnore]
        public decimal Amount { get; set; }

        [JsonIgnore]
        public string AmountUnit { get; set; }

        [JsonIgnore]
        public decimal UnitPrice { get; set; }

        [JsonIgnore]
        public string UnitPriceCurrency { get; set; }

        public override string[] SerializeValues() => new string[]
        {
            ProjectName,
            ProjectApn,
            PaymentType,
            SerializeDecimal(Amount, 2),
            AmountUnit,
            SerializeDecimal(UnitPrice, null), // we don't round this value for the sake of proper total calculation in report
            UnitPriceCurrency,
        };

        public override void DeserializeValues(string[] values)
        {
            ProjectName = values[0];
            ProjectApn = values[1];
            PaymentType = values[2];
            Amount = ParseDecimal(values[3]);
            AmountUnit = values[4];
            UnitPrice = ParseDecimal(values[5]);
            UnitPriceCurrency = values[6];
        }
    }
}
