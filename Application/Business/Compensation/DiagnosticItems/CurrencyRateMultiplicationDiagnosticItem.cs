﻿using System;
using Newtonsoft.Json;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Compensation.DiagnosticItems
{
    [Serializable]
    public class CurrencyRateMultiplicationDiagnosticItem : DiagnosticItem
    {
        public override RenderingHintType Type => RenderingHintType.CurrencyRateMultiplication;

        public override DiagnosticLevel Level => DiagnosticLevel.Info;

        [JsonIgnore]
        public CurrencyAmount ForeignValue { get; set; }

        [JsonIgnore]
        public decimal Rate { get; set; }

        [JsonIgnore]
        public CurrencyAmount CalculatedValue { get; set; }

        public override string[] SerializeValues() => new string[]
        {
            ForeignValue.ToString(),
            SerializeDecimal(Rate, 4),
            CalculatedValue.ToString(),
        };

        public override void DeserializeValues(string[] values)
        {
            ForeignValue = CurrencyAmount.Parse(values[0]);
            Rate = ParseDecimal(values[1]);
            CalculatedValue = CurrencyAmount.Parse(values[2]);
        }
    }
}
