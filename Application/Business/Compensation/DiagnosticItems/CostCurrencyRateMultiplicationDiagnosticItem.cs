﻿using System;
using Newtonsoft.Json;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Compensation.DiagnosticItems
{
    [Serializable]
    public class CostCurrencyRateMultiplicationDiagnosticItem : DiagnosticItem
    {
        public override RenderingHintType Type => RenderingHintType.CostCurrencyRateMultiplication;

        public override DiagnosticLevel Level => DiagnosticLevel.Info;

        [JsonIgnore]
        public string Name { get; set; }

        [JsonIgnore]
        public CurrencyAmount CostValue { get; set; }

        [JsonIgnore]
        public decimal Rate { get; set; }

        [JsonIgnore]
        public CurrencyAmount CalculatedValue { get; set; }

        public override string[] SerializeValues() => new string[]
        {
            Name,
            CostValue.ToString(),
            SerializeDecimal(Rate, 4),
            CalculatedValue.ToString(),
        };

        public override void DeserializeValues(string[] values)
        {
            Name = values[0];
            CostValue = CurrencyAmount.Parse(values[1]);
            Rate = ParseDecimal(values[2]);
            CalculatedValue = CurrencyAmount.Parse(values[3]);
        }
    }
}
