﻿using System;
using Newtonsoft.Json;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Compensation.DiagnosticItems
{
    [Serializable]
    public class ExpenseDiagnosticItem : DiagnosticItem
    {
        public override RenderingHintType Type => RenderingHintType.Expense;

        public override DiagnosticLevel Level => DiagnosticLevel.Debug;

        [JsonIgnore]
        public DateTime Date { get; set; }

        [JsonIgnore]
        public CurrencyAmount Expense { get; set; }

        [JsonIgnore]
        public CurrencyAmount CalculatedExpense { get; set; }

        public override string[] SerializeValues() => new string[]
        {
            Date.ToInvariantString(DateFormat),
            Expense.ToString(),
            CalculatedExpense.ToString(),
        };

        public override void DeserializeValues(string[] values)
        {
            Date = ParseDateTime(values[0]);
            Expense = CurrencyAmount.Parse(values[1]);
            CalculatedExpense = CurrencyAmount.Parse(values[2]);
        }
    }
}