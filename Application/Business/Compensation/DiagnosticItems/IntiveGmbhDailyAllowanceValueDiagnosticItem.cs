﻿using System;
using Newtonsoft.Json;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Compensation.DiagnosticItems
{
    [Serializable]
    public class IntiveGmbhDailyAllowanceValueDiagnosticItem : DiagnosticItem
    {
        public override RenderingHintType Type => RenderingHintType.IntiveGmbhDailyAllowanceValue;

        public override DiagnosticLevel Level => DiagnosticLevel.Info;

        [JsonIgnore]
        public DateTime Date { get; set; }

        [JsonIgnore]
        public CurrencyAmount Allowance { get; set; }

        [JsonIgnore]
        public CurrencyAmount MealsValue { get; set; }

        [JsonIgnore]
        public CurrencyAmount CalculatedValue { get; set; }

        public override string[] SerializeValues() => new string[]
        {
            Date.ToInvariantString(DateFormat),
            Allowance.ToString(),
            MealsValue.ToString(),
            CalculatedValue.ToString(),
        };

        public override void DeserializeValues(string[] values)
        {
            Date = ParseDateTime(values[0]);
            Allowance = CurrencyAmount.Parse(values[1]);
            MealsValue = CurrencyAmount.Parse(values[2]);
            CalculatedValue = CurrencyAmount.Parse(values[3]);
        }
    }
}
