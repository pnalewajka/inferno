﻿using System;
using System.Globalization;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Business.Compensation.DiagnosticItems
{
    [Serializable]
    public abstract class DiagnosticItem
    {
        protected const string DateFormat = "dd-MM-yyyy";

        public DiagnosticGroupType Group { get; set; }

        public abstract RenderingHintType Type { get; }

        public abstract DiagnosticLevel Level { get; }

        public abstract string[] SerializeValues();

        public abstract void DeserializeValues(string[] values);

        public string[] Values
        {
            get => SerializeValues();
            set => DeserializeValues(value);
        }

        protected static string SerializeDecimal(decimal value, int? fixedDecimalPlaces)
        {
            var mask = fixedDecimalPlaces == null
                ? "0.".PadRight(2 + 28, '#') // removes trailing zeros, decimal has max 28 floating point precision
                : "0.".PadRight(2 + fixedDecimalPlaces.Value, '0');

            return value.ToInvariantString(mask);
        }

        protected static DateTime ParseDateTime(string value)
        {
            return DateTime.ParseExact(value, DateFormat, CultureInfo.InvariantCulture);
        }

        protected static decimal ParseDecimal(string value)
        {
            return decimal.Parse(value, NumberStyles.AllowLeadingSign | NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture);
        }

        protected static decimal ParsePercentage(string value)
        {
            return ParseDecimal(value.TrimEnd(new[] { '%' })) / 100m;
        }
    }
}
