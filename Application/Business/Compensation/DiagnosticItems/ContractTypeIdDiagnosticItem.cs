﻿using System;
using Newtonsoft.Json;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Business.Compensation.DiagnosticItems
{
    [Serializable]
    public class ContractTypeIdDiagnosticItem : DiagnosticItem
    {
        public override RenderingHintType Type => RenderingHintType.ContractTypeId;

        public override DiagnosticLevel Level => DiagnosticLevel.Info;

        [JsonIgnore]
        public long ContractTypeId { get; set; }

        public override string[] SerializeValues() => new string[]
        {
            ContractTypeId.ToInvariantString(),
        };

        public override void DeserializeValues(string[] values)
        {
            ContractTypeId = long.Parse(values[0]);
        }
    }
}
