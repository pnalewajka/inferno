﻿using System;
using Newtonsoft.Json;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Compensation.DiagnosticItems
{
    [Serializable]
    public class DailyAllowanceValueDiagnosticItem : DiagnosticItem
    {
        public override RenderingHintType Type => RenderingHintType.DailyAllowanceValue;

        public override DiagnosticLevel Level => DiagnosticLevel.Info;

        [JsonIgnore]
        public string Country { get; set; }

        [JsonIgnore]
        public int Hours { get; set; }

        [JsonIgnore]
        public decimal AllowanceUnits { get; set; }

        [JsonIgnore]
        public CurrencyAmount CalculatedValue { get; set; }

        public override string[] SerializeValues() => new string[]
        {
            Country,
            SerializeDecimal(Hours, 0),
            SerializeDecimal(AllowanceUnits, 2),
            CalculatedValue.ToString(),
        };

        public override void DeserializeValues(string[] values)
        {
            Country = values[0];
            Hours = (int)ParseDecimal(values[1]);
            AllowanceUnits = ParseDecimal(values[2]);
            CalculatedValue = CurrencyAmount.Parse(values[3]);
        }
    }
}
