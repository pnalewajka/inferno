﻿using System;
using Newtonsoft.Json;
using Smt.Atomic.Business.Compensation.Enums;

namespace Smt.Atomic.Business.Compensation.DiagnosticItems
{
    [Serializable]
    public class LegacyDiagnosticItem : DiagnosticItem
    {
        [JsonIgnore]
        public override RenderingHintType Type => DeserializedType;

        [JsonIgnore]
        public override DiagnosticLevel Level => DeserializedLevel;

        [JsonProperty("Type")]
        public RenderingHintType DeserializedType { get; set; }

        [JsonProperty("Level")]
        public DiagnosticLevel DeserializedLevel { get; set; }

        [JsonIgnore]
        public string[] LegacyValues { get; set; } = new string[0];

        public override void DeserializeValues(string[] values)
        {
            LegacyValues = values;
        }

        public override string[] SerializeValues() => LegacyValues;
    }
}
