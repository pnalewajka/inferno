﻿using System;
using Newtonsoft.Json;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Compensation.DiagnosticItems
{
    [Serializable]
    public class LumpSumCalculationDiagnosticItem : DiagnosticItem
    {
        public override RenderingHintType Type => RenderingHintType.LumpSumCalculation;

        public override DiagnosticLevel Level => DiagnosticLevel.Info;

        [JsonIgnore]
        public string Country { get; set; }

        [JsonIgnore]
        public CurrencyAmount Rate { get; set; }

        [JsonIgnore]
        public long Accommodations { get; set; }

        [JsonIgnore]
        public CurrencyAmount LumpSum { get; set; }

        public override string[] SerializeValues() => new string[]
        {
            Country,
            Rate.ToString(),
            SerializeDecimal(Accommodations, 0),
            LumpSum.ToString(),
        };

        public override void DeserializeValues(string[] values)
        {
            Country = values[0];
            Rate = CurrencyAmount.Parse(values[1]);
            Accommodations = (long)ParseDecimal(values[2]);
            LumpSum = CurrencyAmount.Parse(values[3]);
        }
    }
}
