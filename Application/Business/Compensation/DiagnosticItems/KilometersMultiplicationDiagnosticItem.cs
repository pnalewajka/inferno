﻿using System;
using Newtonsoft.Json;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Compensation.DiagnosticItems
{
    [Serializable]
    public class KilometersMultiplicationDiagnosticItem : DiagnosticItem
    {
        public override RenderingHintType Type => RenderingHintType.KilometersMultiplication;

        public override DiagnosticLevel Level => DiagnosticLevel.Info;

        [JsonIgnore]
        public decimal Kilometers { get; set; }

        [JsonIgnore]
        public CurrencyAmount Rate { get; set; }

        [JsonIgnore]
        public CurrencyAmount CalculatedValue { get; set; }

        public override string[] SerializeValues() => new string[]
        {
            SerializeDecimal(Kilometers, 2),
            Rate.ToString(),
            CalculatedValue.ToString(),
        };

        public override void DeserializeValues(string[] values)
        {
            Kilometers = ParseDecimal(values[0]);
            Rate = CurrencyAmount.Parse(values[1], 4);
            CalculatedValue = CurrencyAmount.Parse(values[2]);
        }
    }
}
