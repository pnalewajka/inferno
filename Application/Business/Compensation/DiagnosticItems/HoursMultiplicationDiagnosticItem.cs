﻿using System;
using Newtonsoft.Json;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Compensation.DiagnosticItems
{
    [Serializable]
    public class HoursMultiplicationDiagnosticItem : DiagnosticItem
    {
        public override RenderingHintType Type => RenderingHintType.HoursMultiplication;

        public override DiagnosticLevel Level => DiagnosticLevel.Debug;

        [JsonIgnore]
        public decimal Hours { get; set; }

        [JsonIgnore]
        public CurrencyAmount HourlyRate { get; set; }

        [JsonIgnore]
        public CurrencyAmount Compensation { get; set; }

        public override string[] SerializeValues() => new string[]
        {
            SerializeDecimal(Hours, 1),
            HourlyRate.ToString(),
            Compensation.ToString(),
        };

        public override void DeserializeValues(string[] values)
        {
            Hours = ParseDecimal(values[0]);
            HourlyRate = CurrencyAmount.Parse(values[1]);
            Compensation = CurrencyAmount.Parse(values[2]);
        }
    }
}
