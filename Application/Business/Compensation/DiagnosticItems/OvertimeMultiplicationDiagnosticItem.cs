﻿using System;
using Newtonsoft.Json;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Compensation.DiagnosticItems
{
    [Serializable]
    public class OvertimeMultiplicationDiagnosticItem : DiagnosticItem
    {
        public override RenderingHintType Type => RenderingHintType.OvertimeMultiplication;

        public override DiagnosticLevel Level => DiagnosticLevel.Debug;

        [JsonIgnore]
        public decimal Hours { get; set; }

        [JsonIgnore]
        public CurrencyAmount HourlyRate { get; set; }

        public decimal BonusRate { get; set; }

        [JsonIgnore]
        public CurrencyAmount Bonus { get; set; }

        public override string[] SerializeValues() => new string[]
        {
            SerializeDecimal(Hours, 1),
            HourlyRate.ToString(),
            $"{SerializeDecimal(BonusRate * 100, 2)}%",
            Bonus.ToString(),
        };

        public override void DeserializeValues(string[] values)
        {
            Hours = ParseDecimal(values[0]);
            HourlyRate = CurrencyAmount.Parse(values[1]);
            BonusRate = ParsePercentage(values[2]);
            Bonus = CurrencyAmount.Parse(values[3]);
        }
    }
}
