﻿using System;
using Newtonsoft.Json;
using Smt.Atomic.Business.Compensation.Enums;

namespace Smt.Atomic.Business.Compensation.DiagnosticItems
{
    [Serializable]
    public class CalculationFailedErrorDiagnosticItem : DiagnosticItem
    {
        public override RenderingHintType Type => RenderingHintType.CalculationFailedError;

        public override DiagnosticLevel Level => DiagnosticLevel.Error;

        [JsonIgnore]
        public string[] Details { get; set; } = new string[0];

        public override string[] SerializeValues() => Details;

        public override void DeserializeValues(string[] values)
        {
            Details = values;
        }
    }
}
