﻿using System;
using Newtonsoft.Json;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Business.Compensation.DiagnosticItems
{
    [Serializable]
    public class CurrencyExchangeRateDiagnosticItem : DiagnosticItem
    {
        public override RenderingHintType Type => RenderingHintType.CurrencyExchangeRate;

        public override DiagnosticLevel Level => DiagnosticLevel.Info;

        [JsonIgnore]
        public string CurrencyIsoCode { get; set; }

        [JsonIgnore]
        public DateTime Date { get; set; }

        [JsonIgnore]
        public decimal Rate { get; set; }

        [JsonIgnore]
        public string TableNumber { get; set; }

        public override string[] SerializeValues() => new string[]
        {
            CurrencyIsoCode,
            Date.ToInvariantString(DateFormat),
            SerializeDecimal(Rate, 4),
            TableNumber,
        };

        public override void DeserializeValues(string[] values)
        {
            CurrencyIsoCode = values[0];
            Date = ParseDateTime(values[1]);
            Rate = ParseDecimal(values[2]);
            TableNumber = values[3];
        }
    }
}
