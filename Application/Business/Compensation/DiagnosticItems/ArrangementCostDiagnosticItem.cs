﻿using System;
using Newtonsoft.Json;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Compensation.DiagnosticItems
{
    [Serializable]
    public abstract class ArrangementCostDiagnosticItem : DiagnosticItem
    {
        public override DiagnosticLevel Level => DiagnosticLevel.Info;

        [JsonIgnore]
        public string Name { get; set; }

        [JsonIgnore]
        public CurrencyAmount ArrangementValue { get; set; }

        [JsonIgnore]
        public CurrencyAmount CalculatedValue { get; set; }

        public override string[] SerializeValues() => new string[]
        {
            Name,
            ArrangementValue.ToString(),
            CalculatedValue.ToString(),
        };

        public override void DeserializeValues(string[] values)
        {
            Name = values[0];
            ArrangementValue = CurrencyAmount.Parse(values[1]);
            CalculatedValue = CurrencyAmount.Parse(values[2]);
        }
    }
}
