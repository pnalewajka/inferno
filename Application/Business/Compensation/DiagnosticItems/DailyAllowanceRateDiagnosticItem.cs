﻿using System;
using Newtonsoft.Json;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Compensation.DiagnosticItems
{
    [Serializable]
    public class DailyAllowanceRateDiagnosticItem : DiagnosticItem
    {
        public override RenderingHintType Type => RenderingHintType.DailyAllowanceRate;

        public override DiagnosticLevel Level => DiagnosticLevel.Info;

        [JsonIgnore]
        public string Country { get; set; }

        [JsonIgnore]
        public CurrencyAmount Allowance { get; set; }

        [JsonIgnore]
        public decimal Rate { get; set; }

        [JsonIgnore]
        public CurrencyAmount CalculatedValue { get; set; }

        public override string[] SerializeValues() => new string[]
        {
            Country,
            Allowance.ToString(),
            SerializeDecimal(Rate, 4),
            CalculatedValue.ToString(),
        };

        public override void DeserializeValues(string[] values)
        {
            Country = values[0];
            Allowance = CurrencyAmount.Parse(values[1]);
            Rate = ParseDecimal(values[2]);
            CalculatedValue = CurrencyAmount.Parse(values[3]);
        }
    }
}
