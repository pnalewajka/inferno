﻿namespace Smt.Atomic.Business.Allocation.Helpers
{
    public static class CompensationReportsHelper
    {
        public const string AccountingReportReportCode = "AccountingReport";
        public const string IntiveIncPaymentNotificationReportCode = "IntiveIncPaymentNotification";
        public const string InvoiceCalculationsReportCode = "InvoiceCalculationsPivot";
    }
}
