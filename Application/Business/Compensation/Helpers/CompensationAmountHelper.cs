﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Compensation.Helpers
{
    public static class CompensationAmountHelper
    {
        public static decimal GetAmountForExpenses(CalculationResult calculationResult)
            => GetExpensesCostItems(calculationResult).Sum(g => g.Value);

        public static decimal GetAmountForServices(CalculationResult calculationResult)
            => GetServicesCostItems(calculationResult).Sum(g => g.Value);

        public static decimal GetTotalAmount(CalculationResult calculationResult)
            => GetTotalCostItems(calculationResult).Sum(g => g.Value);

        public static CurrencyAmount GetCurrencyAmountForExpenses(CalculationResult calculationResult)
        {
            var costItems = GetExpensesCostItems(calculationResult);
            var amount = costItems.Sum(g => g.Value);

            return amount == default(decimal) ? null : CurrencyAmount.CreateValueAmount(
                amount, costItems.Select(g => g.CurrencyIsoCode).FirstOrDefault());
        }

        public static CurrencyAmount GetCurrencyAmountForServices(CalculationResult calculationResult)
        {
            var costItems = GetServicesCostItems(calculationResult);
            var amount = costItems.Sum(g => g.Value);

            return amount == default(decimal) ? null : CurrencyAmount.CreateValueAmount(
                amount, costItems.Select(g => g.CurrencyIsoCode).FirstOrDefault());
        }

        public static CurrencyAmount GetTotalCurrencyAmount(CalculationResult calculationResult)
        {
            var costItems = GetTotalCostItems(calculationResult);
            var amount = costItems.Sum(g => g.Value);

            return amount == default(decimal) ? null : CurrencyAmount.CreateValueAmount(
                amount, costItems.Select(g => g.CurrencyIsoCode).FirstOrDefault());
        }

        private static IList<CostItem> GetExpensesCostItems(CalculationResult calculationResult)
            => calculationResult.CostItems.Where(i => i.Group == CostItemGroupType.BusinessTripCompensationTotal
                                                   || i.Group == CostItemGroupType.AdditionalCostsTotal)
                                          .ToList();

        private static IList<CostItem> GetServicesCostItems(CalculationResult calculationResult)
            => calculationResult.CostItems.Where(i => i.Group == CostItemGroupType.TimeTrackingTotal).ToList();

        private static IList<CostItem> GetTotalCostItems(CalculationResult calculationResult)
            => calculationResult.CostItems.Where(i => i.Group == CostItemGroupType.CompensationTotal).ToList();
    }
}
