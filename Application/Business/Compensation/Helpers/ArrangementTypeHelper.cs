﻿using Smt.Atomic.Business.Compensation.DiagnosticItems;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Compensation.Helpers
{
    public static class ArrangementHelper
    {
        public static ArrangementCostDiagnosticItem GetDiagnosticItemByArrangementType(this ArrangementType arrangementType)
        {
            switch (arrangementType)
            {
                case ArrangementType.Flight:
                case ArrangementType.Taxi:
                case ArrangementType.Train:
                case ArrangementType.InvoiceFlight:
                case ArrangementType.InvoiceTaxi:
                case ArrangementType.InvoiceTrain:
                    return new TransportArrangementCostDiagnosticItem();

                case ArrangementType.Hotel:
                case ArrangementType.InvoiceHotel:
                    return new AccommodationArrangementCostDiagnosticItem();

                default:
                    return new OtherArrangementCostDiagnosticItem();
            }
        }

        public static bool HasInvoice(this ArrangementType arrangementType)
        {
            return arrangementType == ArrangementType.InvoiceFlight
                || arrangementType == ArrangementType.InvoiceHotel
                || arrangementType == ArrangementType.InvoiceOther
                || arrangementType == ArrangementType.InvoiceTaxi
                || arrangementType == ArrangementType.InvoiceTrain;
        }
    }
}
