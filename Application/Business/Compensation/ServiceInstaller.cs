﻿using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.Compensation.Calculators;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.Business.Compensation.Services;
using Smt.Atomic.Business.EventSourcing.Interfaces;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.Compensation
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            switch (containerType)
            {
                case ContainerType.WebApp:
                    container.Register(Component.For<IBonusCardIndexDataService>().ImplementedBy<BonusCardIndexDataService>().LifestyleTransient());
                    break;
            }

            container.Register(Classes.FromThisAssembly().BasedOn<IBusinessEventHandler>().WithService.FromInterface().LifestyleTransient());
            container.Register(Component.For<ICalculatorFactory>().AsFactory(c => c.SelectedWith<CalculatorFactoryComponentSelector>()).LifestyleTransient());
            container.Register(Component.For<ITypedFactoryComponentSelector>().ImplementedBy<CalculatorFactoryComponentSelector>().LifestyleTransient());
            container.Register(Component.For<ICalculatorResolver>().ImplementedBy<CalculatorResolver>().LifestyleTransient());
            container.Register(Component.For<ICalculator>().ImplementedBy<ErrorHandlerCalculator>().Named(nameof(ErrorHandlerCalculator)).LifestyleTransient());
            container.Register(Component.For<ICalculator>().ImplementedBy<AggregatedTimeTrackingCompensationCalculator>().Named(nameof(AggregatedTimeTrackingCompensationCalculator)).LifestyleTransient());
            container.Register(Component.For<ICalculator>().ImplementedBy<EmploymentContractPLBusinessTripCompensationCalculator>().Named(nameof(EmploymentContractPLBusinessTripCompensationCalculator)).LifestyleTransient());
            container.Register(Component.For<ICalculator>().ImplementedBy<EmploymentContractPLTimeTrackingCompensationCalculator>().Named(nameof(EmploymentContractPLTimeTrackingCompensationCalculator)).LifestyleTransient());
            container.Register(Component.For<ICalculator>().ImplementedBy<EmploymentContractUKBusinessTripCompensationCalculator>().Named(nameof(EmploymentContractUKBusinessTripCompensationCalculator)).LifestyleTransient());
            container.Register(Component.For<ICalculator>().ImplementedBy<EmploymentContractUSBusinessTripCompensationCalculator>().Named(nameof(EmploymentContractUSBusinessTripCompensationCalculator)).LifestyleTransient());
            container.Register(Component.For<ICalculator>().ImplementedBy<EmploymentContractWithCdePLTimeTrackingCompensationCalculator>().Named(nameof(EmploymentContractWithCdePLTimeTrackingCompensationCalculator)).LifestyleTransient());
            container.Register(Component.For<ICalculator>().ImplementedBy<HourlyRateBasedDETimeTrackingCompensationCalculator>().Named(nameof(HourlyRateBasedDETimeTrackingCompensationCalculator)).LifestyleTransient());
            container.Register(Component.For<ICalculator>().ImplementedBy<EmploymentContractDEBusinessTripCompensationCalculator>().Named(nameof(EmploymentContractDEBusinessTripCompensationCalculator)).LifestyleTransient());
            container.Register(Component.For<ICalculator>().ImplementedBy<HourlyRateBasedPLBusinessTripCompensationCalculator>().Named(nameof(HourlyRateBasedPLBusinessTripCompensationCalculator)).LifestyleTransient());
            container.Register(Component.For<ICalculator>().ImplementedBy<HourlyRateBasedPLTimeTrackingCompensationCalculator>().Named(nameof(HourlyRateBasedPLTimeTrackingCompensationCalculator)).LifestyleTransient());
            container.Register(Component.For<ICalculator>().ImplementedBy<HourlyRateBasedWithOvertimePLTimeTrackingCompensationCalculator>().Named(nameof(HourlyRateBasedWithOvertimePLTimeTrackingCompensationCalculator)).LifestyleTransient());
            container.Register(Component.For<ICalculator>().ImplementedBy<MonthlyRateBasedPLTimeTrackingCompensationCalculator>().Named(nameof(MonthlyRateBasedPLTimeTrackingCompensationCalculator)).LifestyleTransient());
            container.Register(Component.For<ICalculator>().ImplementedBy<MonthlyRateBasedWithOvertimePLTimeTrackingCompensationCalculator>().Named(nameof(MonthlyRateBasedWithOvertimePLTimeTrackingCompensationCalculator)).LifestyleTransient());
            container.Register(Component.For<ICalculator>().ImplementedBy<MonthlyRateBasedWithOvertimeAndSickLeavePLTimeTrackingCompensationCalculator>().Named(nameof(MonthlyRateBasedWithOvertimeAndSickLeavePLTimeTrackingCompensationCalculator)).LifestyleTransient());
            container.Register(Component.For<ICalculator>().ImplementedBy<MonthlyRateBasedWithSickLeavePLTimeTrackingCompensationCalculator>().Named(nameof(MonthlyRateBasedWithSickLeavePLTimeTrackingCompensationCalculator)).LifestyleTransient());
            container.Register(Component.For<ICalculator>().ImplementedBy<EmptyTimeTrackingCompensationCalculator>().Named(nameof(EmptyTimeTrackingCompensationCalculator)).LifestyleTransient());
            container.Register(Component.For<ICalculator>().ImplementedBy<TotalCompensationCalculator>().Named(nameof(TotalCompensationCalculator)).LifestyleTransient());
            container.Register(Component.For<ICalculator>().ImplementedBy<MonthlyRateEmptyTimeTrackingCompensationCalculator>().Named(nameof(MonthlyRateEmptyTimeTrackingCompensationCalculator)).LifestyleTransient());
            container.Register(Component.For<ICurrencyExchangeRateService, EcbCurrencyExchangeRateService>().ImplementedBy<EcbCurrencyExchangeRateService>().LifestyleTransient());
            container.Register(Component.For<ICurrencyExchangeRateService, NbpCurrencyExchangeRateService>().ImplementedBy<NbpCurrencyExchangeRateService>().LifestyleTransient());
            container.Register(Component.For<ICurrencyExchangeRateService, OxrCurrencyExchangeRateService>().ImplementedBy<OxrCurrencyExchangeRateService>().LifestyleTransient());
            container.Register(Component.For<ICalculatorCurrencyExchangeRateService>().ImplementedBy<CalculatorCurrencyExchangeRateService>().LifestyleTransient());
            container.Register(Component.For<ITimeReportInvoiceNotificationService>().ImplementedBy<TimeReportInvoiceNotificationService>().LifestyleTransient());
            container.Register(Component.For<ITimeReportInvoiceService>().ImplementedBy<TimeReportInvoiceService>().LifestyleTransient());
            container.Register(Component.For<IBonusService>().ImplementedBy<BonusService>());
        }
    }
}
