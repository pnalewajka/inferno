﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Compensation;
using Smt.Atomic.Data.Repositories.FilteringQueries;


namespace Smt.Atomic.Business.Compensation.EntitySecurityDescriptions
{
    public class BonusSecurityRestriction : EntitySecurityRestriction<Bonus>
    {
        private readonly IOrgUnitService _orgUnitService;

        public BonusSecurityRestriction(IPrincipalProvider principalProvider, IOrgUnitService orgUnitService)
            : base(principalProvider)
        {
            _orgUnitService = orgUnitService;
            RequiresAuthentication = true;
        }

        public override Expression<Func<Bonus, bool>> GetRestriction()
        {
            if (Roles.Contains(SecurityRoleType.CanViewAllEmployeesBonuses))
            {
                return AllRecords();
            }

            if (Roles.Contains(SecurityRoleType.CanViewMyEmployeesBonuses))
            {
                var currentEmployeeId = CurrentPrincipal.EmployeeId;
                var managerOrgUnitsIds = _orgUnitService.GetManagerOrgUnitDescendantIds(currentEmployeeId);

                return new BusinessLogic<Bonus, bool>(
                    b => EmployeeBusinessLogic.IsEmployeeOf.Call(b.Employee, currentEmployeeId, managerOrgUnitsIds));
            }

            return NoRecords();
        }
    }
}
