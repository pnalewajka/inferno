﻿using System;
using System.Linq;
using Smt.Atomic.Business.Compensation.BusinessLogics;
using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Modules.Compensation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Compensation.Services
{
    public abstract class BaseCurrencyExchangeRateService : ICurrencyExchangeRateService
    {
        protected readonly ICurrencyService CurrencyService;
        protected readonly IUnitOfWorkService<ICompensationDbScope> UnitOfWorkService;

        public BaseCurrencyExchangeRateService(
            ICurrencyService currencyService,
            IUnitOfWorkService<ICompensationDbScope> unitOfWorkService)
        {
            CurrencyService = currencyService;
            UnitOfWorkService = unitOfWorkService;
        }

        protected abstract string BaseCurrencyIsoCode { get; }

        protected abstract CurrencyExchangeRateSourceType SourceType { get; }

        protected virtual DateTime GetExchangeRateDate(DateTime date) => date.Date;

        public CurrencyExchangeRateDto GetExchangeRateForDay(string quotedCurrencyIsoCode, DateTime date)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var exchangeRateDate = GetExchangeRateDate(date);
                var exchangeRate = GetExchangeRateFromDatabase(unitOfWork, quotedCurrencyIsoCode, exchangeRateDate);

                if (exchangeRate != null)
                {
                    return exchangeRate;
                }

                return GetExchangeRateFromExternalSource(unitOfWork, quotedCurrencyIsoCode, exchangeRateDate);
            }
        }

        protected CurrencyExchangeRateDto GetExchangeRateFromDatabase(IUnitOfWork<ICompensationDbScope> unitOfWork, string quotedCurrencyIsoCode, DateTime date)
        {
            var exchangeRate = unitOfWork.Repositories.CurrencyExchangeRates
                .Where(CurrencyExchangeRateBusinessLogic.IsCachedByIsoCodes.Parametrize(
                    BaseCurrencyIsoCode, quotedCurrencyIsoCode, date.Date, SourceType))
                .Select(r => new CurrencyExchangeRateDto
                {
                    Date = r.Date,
                    Rate = r.Rate,
                    TableNumber = r.TableNumber
                })
                .SingleOrDefault();

            return exchangeRate;
        }

        protected abstract CurrencyExchangeRateDto GetExchangeRateFromExternalSource(IUnitOfWork<ICompensationDbScope> unitOfWork, string quotedCurrencyIsoCode, DateTime date);

        protected void SaveExchangeRatesToDatabase(IUnitOfWork<ICompensationDbScope> unitOfWork, CurrencyExchangeRate[] exchangeRates)
        {
            foreach (var exchangeRate in exchangeRates)
            {
                var isCached = unitOfWork.Repositories.CurrencyExchangeRates
                    .Where(CurrencyExchangeRateBusinessLogic.IsCached.Parametrize(
                        exchangeRate.BaseCurrencyId, exchangeRate.QuotedCurrencyId, exchangeRate.Date.Date, SourceType))
                    .Any();

                if (!isCached)
                {
                    unitOfWork.Repositories.CurrencyExchangeRates.Add(exchangeRate);
                }
            }

            unitOfWork.Commit();
        }
    }
}
