﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.Business.Compensation.BusinessEvents;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Compensation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Compensation.Services
{
    public class BonusService
        : IBonusService
    {
        private readonly IUnitOfWorkService<ICompensationDbScope> _unitOfWorkService;
        private readonly IBusinessEventPublisher _businessEventPublisher;

        public BonusService(
            IUnitOfWorkService<ICompensationDbScope> unitOfWorkService,
            IBusinessEventPublisher businessEventPublisher)
        {
            _unitOfWorkService = unitOfWorkService;
            _businessEventPublisher = businessEventPublisher;
        }

        private IQueryable<InvoiceCalculation> GetAffectedInvoices(
            IUnitOfWork<ICompensationDbScope> unitOfWork,
            long employeeId, DateTime from, DateTime? to)
        {
            return unitOfWork.Repositories.InvoiceCalculations
                .Where(i => i.IsValid)
                .Where(i => i.EmployeeId == employeeId)
                .Where(i => i.Year * 100 + i.Month >= from.Year * 100 + from.Month)
                .Where(i => !to.HasValue || (i.Year * 100 + i.Month <= to.Value.Year * 100 + to.Value.Month));
        }

        public int GetAffectedInvoicesCount(long employeeId, DateTime from, DateTime? to)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return GetAffectedInvoices(unitOfWork, employeeId, from, to).Count();
            }
        }

        public void RecalculateAffectedInvoices(long employeeId, DateTime from, DateTime? to)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var invoices = GetAffectedInvoices(unitOfWork, employeeId, from, to)
                    .Select(i => new { i.Year, i.Month });

                foreach (var invoiceDate in invoices)
                {
                    _businessEventPublisher.PublishBusinessEvent(
                        new CompensationChangedEvent(employeeId, invoiceDate.Year, invoiceDate.Month));
                }
            }
        }
    }
}
