﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.DiagnosticItems;
using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.Business.Compensation.EmailModels;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.Business.Compensation.Helpers;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.Business.Compensation.Resources;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Workflows.BusinessLogics;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Compensation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Compensation.Services
{
    public class TimeReportInvoiceNotificationService : ITimeReportInvoiceNotificationService
    {
        private readonly ITimeService _timeService;
        private readonly ICalendarService _calendarService;
        private readonly IEmployeeService _employeeService;
        private readonly IReliableEmailService _emailService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly ICalculatorResolver _calculatorResolver;
        private readonly IUnitOfWorkService<ICompensationDbScope> _unitOfWorkService;
        private readonly IClassMapping<InvoiceCalculationDto, InvoiceCalculation> _invoiceCalculationDtoToEntityMapping;

        public TimeReportInvoiceNotificationService(
            ITimeService timeService,
            ICalendarService calendarService,
            IEmployeeService employeeService,
            IReliableEmailService emailService,
            IMessageTemplateService messageTemplateService,
            ISystemParameterService systemParameterService,
            ICalculatorResolver calculatorResolver,
            IUnitOfWorkService<ICompensationDbScope> unitOfWorkService,
            IClassMapping<InvoiceCalculationDto, InvoiceCalculation> invoiceCalculationDtoToEntityMapping)
        {
            _timeService = timeService;
            _calendarService = calendarService;
            _employeeService = employeeService;
            _emailService = emailService;
            _messageTemplateService = messageTemplateService;
            _systemParameterService = systemParameterService;
            _calculatorResolver = calculatorResolver;
            _unitOfWorkService = unitOfWorkService;
            _invoiceCalculationDtoToEntityMapping = invoiceCalculationDtoToEntityMapping;
        }

        public void MarkForRecalculating(IEnumerable<long> timeReportIds)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var timeReportsToUpdate = unitOfWork.Repositories.TimeReports
                    .Where(t => timeReportIds.Contains(t.Id) && t.InvoiceNotificationStatus != InvoiceNotificationStatus.NotificationInProgress)
                    .ToList();

                foreach (var timeReport in timeReportsToUpdate)
                {
                    timeReport.InvoiceNotificationStatus = InvoiceNotificationStatus.NotificationInProgress;
                    timeReport.InvoiceNotificationErrorMessage = null;
                }

                unitOfWork.Commit();
            }
        }

        public void MarkAllForRecalculating(int year, int month)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var timeReportsToUpdate = unitOfWork.Repositories.TimeReports
                    .Where(t => t.Year == year && t.Month == month
                        && t.InvoiceNotificationStatus != InvoiceNotificationStatus.NotificationInProgress
                        && t.InvoiceNotificationStatus != InvoiceNotificationStatus.NotificationSent
                        && t.InvoiceNotificationStatus != InvoiceNotificationStatus.NotificationSkipped)
                    .ToList();

                foreach (var timeReport in timeReportsToUpdate)
                {
                    timeReport.InvoiceNotificationStatus = InvoiceNotificationStatus.NotificationInProgress;
                    timeReport.InvoiceNotificationErrorMessage = null;
                }

                unitOfWork.Commit();
            }
        }

        public void SendMarkedNotifications(CancellationToken cancellationToken)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var markedTimeReports = unitOfWork.Repositories.TimeReports
                    .Where(t => t.InvoiceNotificationStatus == InvoiceNotificationStatus.NotificationInProgress)
                    .ToList();

                foreach (var markedTimeReport in markedTimeReports.TakeWhileNotCancelled(cancellationToken))
                {
                    try
                    {
                        if (ShouldRecalculateCompensation(unitOfWork, markedTimeReport, markedTimeReport.Year, markedTimeReport.Month))
                        {
                            var compensationCalculator = _calculatorResolver.GetCompensationCalculator(
                                markedTimeReport.EmployeeId, markedTimeReport.Year, markedTimeReport.Month);

                            var notificationSent = false;
                            var calculationResult = compensationCalculator.Calculate();

                            if (ShouldSendNotification(unitOfWork, markedTimeReport, calculationResult))
                            {
                                notificationSent = SendNotification(unitOfWork, markedTimeReport, calculationResult);
                            }

                            SaveCalculation(unitOfWork, markedTimeReport, calculationResult, notificationSent);

                            markedTimeReport.InvoiceNotificationStatus = notificationSent
                                ? InvoiceNotificationStatus.NotificationSent
                                : InvoiceNotificationStatus.NotificationSkipped;
                        }
                    }
                    catch (NotImplementedException)
                    {
                        markedTimeReport.InvoiceNotificationStatus = InvoiceNotificationStatus.NotificationSkipped;
                    }
                    catch (BusinessException exception)
                    {
                        markedTimeReport.InvoiceNotificationErrorMessage = exception.Message;
                        markedTimeReport.InvoiceNotificationStatus = InvoiceNotificationStatus.NotificationBlocked;
                    }

                    unitOfWork.Commit();
                }
            }
        }

        private bool ShouldRecalculateCompensation(IUnitOfWork<ICompensationDbScope> unitOfWork, TimeReport timeReport, int year, byte month)
        {
            if (timeReport.Status != TimeReportStatus.Accepted)
            {
                timeReport.InvoiceNotificationStatus = InvoiceNotificationStatus.NotificationBlocked;
                timeReport.InvoiceNotificationErrorMessage = InvoiceNotificationResources.TimeReportNotAccepted;

                return false;
            }

            var dateComparer = new BusinessLogic<DateTime, bool>(d => d.Year == year && d.Month == month);
            var hasAllBusinessTripsSettled = unitOfWork.Repositories.BusinessTripParticipants
                .Where(p => p.EmployeeId == timeReport.Employee.Id
                    && dateComparer.Call(p.SettlementRequests.Select(s => s.ArrivalDateTime).FirstOrDefault() ?? p.BusinessTrip.EndedOn))
                .All(p => p.SettlementRequests.Any()
                    && p.SettlementRequests.Select(s => s.Request).AsQueryable().All(RequestBusinessLogic.IsApprovedOrCompleted));

            if (!hasAllBusinessTripsSettled)
            {
                timeReport.InvoiceNotificationStatus = InvoiceNotificationStatus.NotificationBlocked;
                timeReport.InvoiceNotificationErrorMessage = InvoiceNotificationResources.SettlementNotAccepted;

                return false;
            }

            return true;
        }

        private bool ShouldSendNotification(IUnitOfWork<ICompensationDbScope> unitOfWork, TimeReport timeReport, CalculationResult calculationResult)
        {
            const string allReceiversSymbol = "*";
            const string noneReceiversSymbol = "none";

            if (!calculationResult.CostItems.Exists(i => i.Group == CostItemGroupType.CompensationTotal))
            {
                return false;
            }

            var compensationNotificationReceivers =
                _systemParameterService.GetParameter<string>(ParameterKeys.CompensationChangedNotificationReceivers);

            if (string.IsNullOrWhiteSpace(compensationNotificationReceivers) || compensationNotificationReceivers == noneReceiversSymbol)
            {
                return false;
            }

            if (compensationNotificationReceivers == allReceiversSymbol)
            {
                var begginingOfMonth = DateHelper.BeginningOfMonth(timeReport.Year, timeReport.Month);
                var endOfMonth = DateHelper.EndOfMonth(timeReport.Year, timeReport.Month);
                var isInMonth = DateRangeHelper.OverlapingDateRange<EmploymentPeriod>(
                    e => e.StartDate, e => e.EndDate, begginingOfMonth, endOfMonth);

                return unitOfWork.Repositories.EmploymentPeriods
                    .Where(p => p.EmployeeId == timeReport.EmployeeId)
                    .Where(isInMonth)
                    .Any(p => p.ContractType.NotifyInvoiceAmount);
            }

            return compensationNotificationReceivers.Split(',').Select(a => a.Trim()).Contains(timeReport.Employee.Acronym);
        }

        private bool SendNotification(IUnitOfWork<ICompensationDbScope> unitOfWork, TimeReport timeReport, CalculationResult calculationResult)
        {
            var begginingOfMonth = DateHelper.BeginningOfMonth(timeReport.Year, timeReport.Month);
            var endOfMonth = DateHelper.EndOfMonth(timeReport.Year, timeReport.Month);
            var isInMonth = DateRangeHelper.OverlapingDateRange<EmploymentPeriod>(
                e => e.StartDate, e => e.EndDate, begginingOfMonth, endOfMonth);

            var companies = unitOfWork.Repositories.EmploymentPeriods
                .Where(p => p.EmployeeId == timeReport.EmployeeId && p.Company != null)
                .Where(isInMonth)
                .Select(p => p.Company)
                .DistinctBy(c => c.Id)
                .ToArray();

            if (!companies.Any())
            {
                throw new BusinessException(InvoiceNotificationResources.NoCompany);
            }

            if (companies.Count() != 1)
            {
                throw new BusinessException(InvoiceNotificationResources.MultipleCompanies);
            }

            var calendarId = _employeeService
                .GetCalendarInfoByEmployeeIds(new[] { timeReport.EmployeeId }, begginingOfMonth, endOfMonth)
                .OrderByDescending(i => i.StartDate)
                .Select(i => (long?)i.CalendarId)
                .FirstOrDefault();

            SendEmail(timeReport, companies.Single(), calendarId, calculationResult);

            return true;
        }

        private void SendEmail(TimeReport timeReport, Company company, long? calendarId, CalculationResult calculationResult)
        {
            var endOfMonth = DateHelper.EndOfMonth(timeReport.Year, timeReport.Month).Date;
            var diagnosticItems = calculationResult.CostItems.SelectMany(i => i.DiagnosticItems);

            var model = new CompensationChangedNotificationEmailModel
            {
                EmployeeId = timeReport.EmployeeId,
                FullName = timeReport.Employee.DisplayName,
                HoursAccepted = diagnosticItems
                    .Where(i => i.Group == DiagnosticGroupType.HourlyCompensation || i.Group == DiagnosticGroupType.MonthlyCompensation)
                    .OfType<HoursMultiplicationDiagnosticItem>().Sum(i => i.Hours)
                        + diagnosticItems.Where(i => i.Group == DiagnosticGroupType.OvertimeBonus)
                            .OfType<OvertimeMultiplicationDiagnosticItem>().Sum(i => i.Hours)
                                + diagnosticItems.Where(i => i.Group == DiagnosticGroupType.SickLeaveCompensation)
                                    .OfType<SickLeaveMultiplicationDiagnosticItem>().Sum(i => i.Hours),
                CompensationTotal = CompensationAmountHelper.GetTotalAmount(calculationResult),
                ExpensesTotal = CompensationAmountHelper.GetAmountForExpenses(calculationResult),
                ServicesTotal = CompensationAmountHelper.GetAmountForServices(calculationResult),
                CurrencyIsoCode = calculationResult.CostItems
                    .Where(i => i.Group == CostItemGroupType.CompensationTotal)
                    .Select(i => i.CurrencyIsoCode)
                    .FirstOrDefault(),
                Year = timeReport.Year,
                Month = timeReport.Month,
                SuggestedInvoiceIssue = calendarId.HasValue
                    ? _calendarService.AddWorkdays(endOfMonth.AddDays(1), -1, calendarId.Value)
                    : endOfMonth,
                InvoiceInformation = company.InvoiceInformation,
                CostItems = calculationResult.CostItems,
            };

            var subject = _messageTemplateService.ResolveTemplate(TemplateCodes.CompensationChangedNotificationSubject, model);
            var body = _messageTemplateService.ResolveTemplate(TemplateCodes.CompensationChangedNotificationBody, model);

            var message = new EmailMessageDto
            {
                IsHtml = body.IsHtml,
                Recipients = new List<EmailRecipientDto> { new EmailRecipientDto(timeReport.Employee.Email, timeReport.Employee.DisplayName) },
                Subject = subject.Content,
                Body = body.Content
            };

            var ccRecipients = _systemParameterService.GetParameter<string[]>(ParameterKeys.CompensationChangedNotificationCcReceivers);

            foreach (var ccRecipient in ccRecipients)
            {
                message.Recipients.Add(new EmailRecipientDto { EmailAddress = ccRecipient, Type = RecipientType.Cc });
            }

            _emailService.EnqueueMessage(message);
        }

        private void SaveCalculation(IUnitOfWork<ICompensationDbScope> unitOfWork, TimeReport timeReport, CalculationResult calculationResult, bool notificationSent)
        {
            var oldCalculations = unitOfWork.Repositories.InvoiceCalculations
                .Where(e => e.EmployeeId == timeReport.EmployeeId && e.Year == timeReport.Year && e.Month == timeReport.Month)
                .ToList();

            foreach (var calculation in oldCalculations)
            {
                calculation.IsValid = false;
            }

            var invoiceDto = new InvoiceCalculationDto
            {
                EmployeeId = timeReport.EmployeeId,
                Year = timeReport.Year,
                Month = timeReport.Month,
                CalculationResult = calculationResult,
                NotifiedOn = _timeService.GetCurrentTime(),
                IsValid = true,
                WasEmployeeNotified = notificationSent
            };

            var invoiceCalculation = _invoiceCalculationDtoToEntityMapping.CreateFromSource(invoiceDto);

            unitOfWork.Repositories.InvoiceCalculations.Add(invoiceCalculation);
        }
    }
}
