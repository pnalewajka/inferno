﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Compensation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Compensation.Services
{
    public class NbpCurrencyExchangeRateService : BaseCurrencyExchangeRateService
    {
        private readonly ISystemParameterService _systemParameterService;

        public NbpCurrencyExchangeRateService(
            ICurrencyService currencyService,
            ISystemParameterService systemParameterService,
            IUnitOfWorkService<ICompensationDbScope> unitOfWorkService)
            : base(currencyService, unitOfWorkService)
        {
            _systemParameterService = systemParameterService;
        }

        protected override string BaseCurrencyIsoCode => CurrencyIsoCodes.PolishZloty;

        protected override CurrencyExchangeRateSourceType SourceType => CurrencyExchangeRateSourceType.NationalBankOfPoland;

        protected override DateTime GetExchangeRateDate(DateTime date)
        {
            const double dayOffset = -1;

            return date.AddDays(dayOffset).Date;
        }

        protected override CurrencyExchangeRateDto GetExchangeRateFromExternalSource(IUnitOfWork<ICompensationDbScope> unitOfWork, string quotedCurrencyIsoCode, DateTime date)
        {
            const double dayOffsetForTo = -1;
            const double dayOffsetForFromTableA = -8;
            const double dayOffsetForFromTableB = -15;

            var strategies = new[]
            {
                new { Table = NbpTableType.A, From = date.AddDays(dayOffsetForFromTableA) },
                new { Table = NbpTableType.B, From = date.AddDays(dayOffsetForFromTableB) }
            };

            var to = date.AddDays(dayOffsetForTo);

            foreach (var strategy in strategies)
            {
                var exchangeRateTables = GetNbpExchangeRateTablesForRange(strategy.Table, strategy.From, to);

                SaveNbpExchangeRatesToDatabase(unitOfWork, exchangeRateTables);

                var exchangeRate = GetExchangeRateFromNbpTables(exchangeRateTables, quotedCurrencyIsoCode);

                if (exchangeRate != null)
                {
                    SaveNbpExchangeRateToDatabase(unitOfWork, exchangeRate, quotedCurrencyIsoCode, date);

                    return exchangeRate;
                }
            }

            throw new InvalidOperationException("NBP exchange rates are unavailable");
        }

        private NbpCurrencyExchangeRateTableDto[] GetNbpExchangeRateTablesForRange(NbpTableType table, DateTime from, DateTime to)
        {
            const string dateFormat = "yyyy-MM-dd";

            var queryUrl = _systemParameterService.GetParameter<string>(ParameterKeys.NbpApiCurrencyMeanInDateRangeUrl);
            var url = string.Format(queryUrl, table, from.ToString(dateFormat), to.ToString(dateFormat));

            for (var retriesLeft = _systemParameterService.GetParameter<int>(ParameterKeys.NbpApiRetryCount); retriesLeft > 0; --retriesLeft)
            {
                try
                {
                    var task = GetNbpResponseAsync(url);
                    var result = task.GetAwaiter().GetResult();

                    return JsonConvert.DeserializeObject<NbpCurrencyExchangeRateTableDto[]>(result);
                }
                catch { }
            }

            return null;
        }

        private async Task<string> GetNbpResponseAsync(string query)
        {
            var baseAddress = _systemParameterService.GetParameter<string>(ParameterKeys.NbpApiBaseUrl);

            using (var httpClient = new HttpClient())
            using (var httpResponse = await httpClient.GetAsync(baseAddress + query).ConfigureAwait(false))
            {
                if (!httpResponse.IsSuccessStatusCode)
                {
                    throw new InvalidOperationException("NBP exchange rates are unavailable");
                }

                return await httpResponse.Content.ReadAsStringAsync().ConfigureAwait(false);
            }
        }

        private void SaveNbpExchangeRateToDatabase(IUnitOfWork<ICompensationDbScope> unitOfWork, CurrencyExchangeRateDto exchangeRateDto, string quotedCurrencyIsoCode, DateTime date)
        {
            var currencyIds = CurrencyService.GetCurrencyIdByIsoCodes();

            var exchangeRate = new CurrencyExchangeRate
            {
                Date = date,
                BaseCurrencyId = currencyIds[BaseCurrencyIsoCode],
                QuotedCurrencyId = currencyIds[quotedCurrencyIsoCode],
                Rate = exchangeRateDto.Rate,
                TableNumber = exchangeRateDto.TableNumber,
                SourceType = SourceType
            };

            SaveExchangeRatesToDatabase(unitOfWork, new[] { exchangeRate });
        }

        private void SaveNbpExchangeRatesToDatabase(IUnitOfWork<ICompensationDbScope> unitOfWork, NbpCurrencyExchangeRateTableDto[] exchangeRateTables)
        {
            var currencyIds = CurrencyService.GetCurrencyIdByIsoCodes();

            var exchangeRates = exchangeRateTables
                .SelectMany(t => t.Rates.Select(r => new { t.Date, r.IsoCode, r.Rate, t.TableNumber }))
                .Where(e => currencyIds.ContainsKey(e.IsoCode))
                .Select(e => new CurrencyExchangeRate
                {
                    Date = e.Date,
                    BaseCurrencyId = currencyIds[BaseCurrencyIsoCode],
                    QuotedCurrencyId = currencyIds[e.IsoCode],
                    Rate = e.Rate,
                    TableNumber = e.TableNumber,
                    SourceType = SourceType
                })
                .ToArray();

            SaveExchangeRatesToDatabase(unitOfWork, exchangeRates);
        }

        private static CurrencyExchangeRateDto GetExchangeRateFromNbpTables(NbpCurrencyExchangeRateTableDto[] exchangeRateTables, string currencyIsoCode)
        {
            return exchangeRateTables
                .OrderByDescending(t => t.Date)
                .Where(t => t.Rates.Any(r => r.IsoCode == currencyIsoCode))
                .Select(t => new CurrencyExchangeRateDto
                {
                    TableNumber = t.TableNumber,
                    Date = t.Date,
                    Rate = t.Rates.Where(r => r.IsoCode == currencyIsoCode).Select(r => r.Rate).Single()
                })
                .FirstOrDefault();
        }
    }
}
