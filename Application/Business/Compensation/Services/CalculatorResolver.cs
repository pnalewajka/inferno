﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Compensation.Enums;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.Business.Compensation.Resources;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Compensation.Services
{
    public class CalculatorResolver : ICalculatorResolver
    {
        private readonly ICalculatorFactory _calculatorFactory;
        private readonly IReadOnlyUnitOfWorkService<ICompensationDbScope> _unitOfWorkService;

        public CalculatorResolver(
            ICalculatorFactory calculatorFactory,
            IReadOnlyUnitOfWorkService<ICompensationDbScope> unitOfWorkService)
        {
            _calculatorFactory = calculatorFactory;
            _unitOfWorkService = unitOfWorkService;
        }

        public ICalculator GetErrorCalculator(ICalculator innerCalculator)
        {
            return _calculatorFactory.CreateErrorHandlerCalculator(innerCalculator);
        }

        public ICalculator GetCompensationCalculator(long employeeId, int year, byte month)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var timeTrackingCalculator = unitOfWork.Repositories.TimeReports
                    .Where(r => r.EmployeeId == employeeId && r.Year == year && r.Month == month)
                    .Select(r => r.Id)
                    .AsEnumerable()
                    .Select(i => GetCalculatorByTimeReportId(unitOfWork, i))
                    .Single();

                var businessTripCalculators = unitOfWork.Repositories.BusinessTripSettlementRequests
                    .Where(s => s.BusinessTripParticipant.EmployeeId == employeeId
                        && (s.ArrivalDateTime ?? s.BusinessTripParticipant.BusinessTrip.EndedOn).Year == year
                        && (s.ArrivalDateTime ?? s.BusinessTripParticipant.BusinessTrip.EndedOn).Month == month)
                    .Select(r => r.Id)
                    .AsEnumerable()
                    .Select(i => GetCalculatorBySettlementRequestId(unitOfWork, i))
                    .ToArray();

                return _calculatorFactory.CreateTotalCompensationCalculator(timeTrackingCalculator, businessTripCalculators);
            }
        }

        public ICalculator GetCalculatorBySettlementRequestId(long settlementRequestId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return GetCalculatorBySettlementRequestId(unitOfWork, settlementRequestId);
            }
        }

        public ICalculator GetCalculatorByTimeReportId(long timeReportId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return GetCalculatorByTimeReportId(unitOfWork, timeReportId);
            }
        }

        private ICalculator GetCalculatorBySettlementRequestId(IReadOnlyUnitOfWork<ICompensationDbScope> unitOfWork, long settlementRequestId)
        {
            IQueryable<BusinessTripSettlementRequest> settlementRequestQuery = unitOfWork.Repositories.BusinessTripSettlementRequests;

            settlementRequestQuery = settlementRequestQuery
                .Include(r => r.AdditionalAdvancePayments)
                .Include(r => r.AdditionalAdvancePayments.Select(p => p.Currency));

            settlementRequestQuery = settlementRequestQuery
                .Include(r => r.Meals)
                .Include(r => r.Meals.Select(m => m.Country));

            settlementRequestQuery = settlementRequestQuery
                .Include(r => r.OwnCosts)
                .Include(r => r.OwnCosts.Select(c => c.Currency))
                .Include(r => r.CompanyCosts)
                .Include(r => r.CompanyCosts.Select(c => c.Currency));

            settlementRequestQuery = settlementRequestQuery
                .Include(r => r.PrivateVehicleMileage);

            var settlementRequest = settlementRequestQuery.GetById(settlementRequestId);

            settlementRequest.Load(
                r => r.AbroadTimeEntries,
                q => q
                    .Include(e => e.DepartureCity)
                    .Include(r => r.DepartureCity.DailyAllowances)
                    .Include(r => r.DepartureCity.DailyAllowances.Select(a => a.Currency))
                    .Include(r => r.DepartureCity.DailyAllowances.Select(a => a.EmploymentCountry))
                    .Include(r => r.DepartureCity.DailyAllowances.Select(a => a.TravelCountry))
                    .Include(r => r.DepartureCountry)
                    .Include(r => r.DepartureCountry.DailyAllowances)
                    .Include(r => r.DepartureCountry.DailyAllowances.Select(a => a.Currency))
                    .Include(r => r.DepartureCountry.DailyAllowances.Select(a => a.EmploymentCountry))
                    .Include(r => r.DepartureCountry.DailyAllowances.Select(a => a.TravelCountry)));

            settlementRequest.Load(
                r => r.BusinessTripParticipant,
                q => q
                    .Include(r => r.AdvancedPaymentRequests)
                    .Include(r => r.AdvancedPaymentRequests.Select(p => p.Request))
                    .Include(r => r.BusinessTrip)
                    .Include(r => r.BusinessTrip.Projects)
                    .Include(r => r.BusinessTrip.Projects.Select(p => p.ProjectSetup))
                    .Include(r => r.BusinessTrip.Arrangements)
                    .Include(r => r.BusinessTrip.Arrangements.Select(a => a.Currency))
                    .Include(r => r.BusinessTrip.Arrangements.Select(a => a.Participants))
                    .Include(r => r.BusinessTrip.DestinationCity)
                    .Include(r => r.BusinessTrip.Participants)
                    .Include(r => r.DepartureCity)
                    .Include(r => r.DepartureCity.Country)
                    .Include(r => r.DepartureCity.Country.DailyAllowances)
                    .Include(r => r.DepartureCity.Country.DailyAllowances.Select(a => a.Currency))
                    .Include(r => r.DepartureCity.Country.DailyAllowances.Select(a => a.EmploymentCountry))
                    .Include(r => r.DepartureCity.Country.DailyAllowances.Select(a => a.TravelCountry)));

            var businessTrip = settlementRequest.BusinessTripParticipant.BusinessTrip;

            var employmentPeriods = unitOfWork.Repositories.EmploymentPeriods
                .Include(p => p.ContractType)
                .Include(p => p.Employee)
                .Where(p => p.EmployeeId == settlementRequest.BusinessTripParticipant.EmployeeId)
                .Where(EmploymentPeriodBusinessLogic.OverlapingRange.Parametrize(businessTrip.StartedOn, businessTrip.EndedOn))
                .OrderBy(p => p.StartDate)
                .ToArray();

            var contractType = employmentPeriods.First().ContractType;

            if (!contractType.BusinessTripCompensationCalculator.HasValue)
            {
                throw new BusinessException(CalculatorResources.ContractTypeMissingDataError);
            }

            return _calculatorFactory.CreateBusinessTripCompensationCalculator(
                contractType.BusinessTripCompensationCalculator.Value, settlementRequest, employmentPeriods);
        }

        private ICalculator GetCalculatorByTimeReportId(IReadOnlyUnitOfWork<ICompensationDbScope> unitOfWork, long timeReportId)
        {
            var timeReport = unitOfWork.Repositories.TimeReports
                .Include(tr => tr.Rows)
                .Include(tr => tr.Rows.Select(r => r.AbsenceType))
                .Include(tr => tr.Rows.Select(r => r.DailyEntries))
                .Include(tr => tr.Rows.Select(r => r.DailyEntries.Select(d => d.TimeReportRow.Project)))
                .Include(tr => tr.Rows.Select(r => r.DailyEntries.Select(d => d.TimeReportRow.Project.ProjectSetup)))
                .Include(tr => tr.Rows.Select(r => r.Project))
                .Include(tr => tr.Rows.Select(r => r.Project.ProjectSetup))
                .GetById(timeReportId);

            var beginningOfMonth = DateHelper.BeginningOfMonth(timeReport.Year, timeReport.Month);
            var endOfMonth = DateHelper.EndOfMonth(timeReport.Year, timeReport.Month);

                var employmentPeriods = unitOfWork.Repositories.EmploymentPeriods
                    .Include(p => p.ContractType)
                    .Include(p => p.Employee)
                    .Include(p => p.Employee.Bonuses)
                    .Include(p => p.Employee.Bonuses.Select(b => b.Currency))
                    .Include(p => p.Employee.Bonuses.Select(b => b.Project))
                    .Include(p => p.Employee.Bonuses.Select(b => b.Project.ProjectSetup))
                    .Include(p => p.Employee.ExpenseRequests)
                    .Include(p => p.Employee.ExpenseRequests.Select(r => r.Currency))
                    .Include(p => p.Employee.ExpenseRequests.Select(r => r.Project))
                    .Include(p => p.Employee.ExpenseRequests.Select(r => r.Project.ProjectSetup))
                    .Include(p => p.Employee.ExpenseRequests.Select(r => r.Request))
                    .Where(p => p.EmployeeId == timeReport.EmployeeId)
                    .Where(EmploymentPeriodBusinessLogic.OverlapingRange.Parametrize(beginningOfMonth, endOfMonth))
                    .OrderBy(p => p.StartDate)
                    .ToList();

            var calculators = new List<ICalculator>();

            for (int i = 0; i < employmentPeriods.Count; i++)
            {
                var contractType = employmentPeriods[i].ContractType;

                if (!contractType.TimeTrackingCompensationCalculator.HasValue)
                {
                    continue;
                }

                var calculator = _calculatorFactory.CreateTimeTrackingCompensationCalculator(
                    contractType.TimeTrackingCompensationCalculator.Value, timeReport, employmentPeriods[i],
                    i == 0
                        ? TimeTrackingCompensationCalculatorOptions.IsFirstWorkingPeriod
                        : TimeTrackingCompensationCalculatorOptions.None);

                calculators.Add(calculator);
            }

            if (!calculators.Any())
            {
                return _calculatorFactory.CreateEmptyTimeTrackingCompensationCalculator();
            }

            return calculators.Count == 1
                ? calculators.Single()
                : _calculatorFactory.CreateAggregatedTimeTrackingCompensationCalculator(calculators.ToArray());
        }
    }
}
