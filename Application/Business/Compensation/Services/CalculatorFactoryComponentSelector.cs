﻿using System.Reflection;
using Castle.Facilities.TypedFactory;
using Smt.Atomic.Business.Compensation.Calculators;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Compensation.Services
{
    public class CalculatorFactoryComponentSelector : DefaultTypedFactoryComponentSelector
    {
        protected override string GetComponentName(MethodInfo method, object[] arguments)
        {
            switch (method.Name)
            {
                case nameof(ICalculatorFactory.CreateAggregatedTimeTrackingCompensationCalculator):
                    return nameof(AggregatedTimeTrackingCompensationCalculator);

                case nameof(ICalculatorFactory.CreateBusinessTripCompensationCalculator):
                    return $"{((BusinessTripCompensationCalculators)arguments[0]).ToString()}BusinessTripCompensationCalculator";

                case nameof(ICalculatorFactory.CreateEmptyTimeTrackingCompensationCalculator):
                    return nameof(EmptyTimeTrackingCompensationCalculator);

                case nameof(ICalculatorFactory.CreateErrorHandlerCalculator):
                    return nameof(ErrorHandlerCalculator);

                case nameof(ICalculatorFactory.CreateTimeTrackingCompensationCalculator):
                    return $"{((TimeTrackingCompensationCalculators)arguments[0]).ToString()}TimeTrackingCompensationCalculator";

                case nameof(ICalculatorFactory.CreateTotalCompensationCalculator):
                    return nameof(TotalCompensationCalculator);

                default:
                    return base.GetComponentName(method, arguments);
            }
        }
    }
}
