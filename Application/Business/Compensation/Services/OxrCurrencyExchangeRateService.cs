﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Compensation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Compensation.Services
{
    public class OxrCurrencyExchangeRateService : BaseCurrencyExchangeRateService
    {
        private readonly ISettingsProvider _settingsProvider;
        private readonly ISystemParameterService _systemParameterService;

        public OxrCurrencyExchangeRateService(
            ICurrencyService currencyService,
            ISettingsProvider settingsProvider,
            ISystemParameterService systemParameterService,
            IUnitOfWorkService<ICompensationDbScope> unitOfWorkService)
            : base(currencyService, unitOfWorkService)
        {
            _settingsProvider = settingsProvider;
            _systemParameterService = systemParameterService;
        }

        protected override string BaseCurrencyIsoCode => CurrencyIsoCodes.UnitedStatesDollar;

        protected override CurrencyExchangeRateSourceType SourceType => CurrencyExchangeRateSourceType.OpenExchangeRates;

        protected override CurrencyExchangeRateDto GetExchangeRateFromExternalSource(IUnitOfWork<ICompensationDbScope> unitOfWork, string quotedCurrencyIsoCode, DateTime date)
        {
            var exchangeRates = GetOxrExchangeRatesForDate(date);

            SaveOxrExchangeRatesToDatabase(unitOfWork, exchangeRates, date);

            var rate = exchangeRates.Rates.Where(r => r.Key == quotedCurrencyIsoCode).Select(r => (decimal?)r.Value).SingleOrDefault();

            return rate == null ? null : new CurrencyExchangeRateDto
            {
                Date = date,
                Rate = 1m / rate.Value,
                TableNumber = exchangeRates.Timestamp
            };
        }

        private OxrCurrencyExchangeRateDto GetOxrExchangeRatesForDate(DateTime date)
        {
            const string dateFormat = "yyyy-MM-dd";

            var appId = _settingsProvider.AuthorizationProviderSettings.OpenExchangeRatesSettings.AppId;
            var queryUrl = _systemParameterService.GetParameter<string>(ParameterKeys.OxrApiCurrencyMeanForDateUrl);
            var currencyIsoCodes = string.Join(",", CurrencyService.GetCurrencyIdByIsoCodes().Keys.Except(new[] { CurrencyIsoCodes.UnitedStatesDollar }));
            var url = string.Format(queryUrl, date.ToString(dateFormat), appId, CurrencyIsoCodes.UnitedStatesDollar, currencyIsoCodes);

            for (var retriesLeft = _systemParameterService.GetParameter<int>(ParameterKeys.OxrApiRetryCount); retriesLeft > 0; --retriesLeft)
            {
                try
                {
                    var task = GetOxrResponseAsync(url);
                    var result = task.GetAwaiter().GetResult();

                    return JsonConvert.DeserializeObject<OxrCurrencyExchangeRateDto>(result);
                }
                catch { }
            }

            throw new InvalidOperationException("OXR exchange rates are unavailable");
        }

        private async Task<string> GetOxrResponseAsync(string query)
        {
            var baseAddress = _systemParameterService.GetParameter<string>(ParameterKeys.OxrApiBaseUrl);

            using (var httpClient = new HttpClient())
            using (var httpResponse = await httpClient.GetAsync(baseAddress + query).ConfigureAwait(false))
            {
                if (!httpResponse.IsSuccessStatusCode)
                {
                    throw new InvalidOperationException("OXR exchange rates are unavailable");
                }

                return await httpResponse.Content.ReadAsStringAsync().ConfigureAwait(false);
            }
        }

        private void SaveOxrExchangeRatesToDatabase(IUnitOfWork<ICompensationDbScope> unitOfWork, OxrCurrencyExchangeRateDto exchangeRateDto, DateTime date)
        {
            var currencyIds = CurrencyService.GetCurrencyIdByIsoCodes();

            var exchangeRates = exchangeRateDto.Rates.Select(p => new CurrencyExchangeRate
            {
                Date = date,
                BaseCurrencyId = currencyIds[BaseCurrencyIsoCode],
                QuotedCurrencyId = currencyIds[p.Key],
                Rate = 1m / p.Value,
                TableNumber = exchangeRateDto.Timestamp,
                SourceType = SourceType
            }).ToArray();

            SaveExchangeRatesToDatabase(unitOfWork, exchangeRates);
        }
    }
}
