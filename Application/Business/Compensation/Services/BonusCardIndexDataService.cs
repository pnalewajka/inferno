﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Compensation.Consts;
using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Compensation;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Compensation.Services
{
    public class BonusCardIndexDataService : CardIndexDataService<BonusDto, Bonus, ICompensationDbScope>, IBonusCardIndexDataService
    {
        private readonly ICardIndexServiceDependencies<ICompensationDbScope> _dependencies;
        private readonly IOrgUnitService _orgUnitService;
        private readonly IBonusService _bonusService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public BonusCardIndexDataService(
            ICardIndexServiceDependencies<ICompensationDbScope> dependencies,
            IOrgUnitService orgUnitService,
            IBonusService bonusService)
            : base(dependencies)
        {
            _dependencies = dependencies;
            _orgUnitService = orgUnitService;
            _bonusService = bonusService;
        }

        protected override IEnumerable<Expression<Func<Bonus, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return t => t.Employee.FirstName;
            yield return t => t.Employee.LastName;
            yield return t => t.Project.SubProjectShortName;
            yield return t => t.Justification;
        }

        protected override NamedFilters<Bonus> NamedFilters
        {
            get
            {
                var managerOrgUnitsIds = _orgUnitService.GetManagerOrgUnitDescendantIds(_dependencies.PrincipalProvider.Current.EmployeeId);
                var currentEmployeeId = _dependencies.PrincipalProvider.Current.EmployeeId;

                return new NamedFilters<Bonus>(new[]
                    {
                        new NamedFilter<Bonus>(
                            FilterCodes.BonusMyEmployees,
                            new BusinessLogic<Bonus, bool>(
                                b => EmployeeBusinessLogic.IsEmployeeOf.Call(b.Employee, currentEmployeeId, managerOrgUnitsIds)),
                            SecurityRoleType.CanViewMyEmployeesBonuses),

                        new NamedFilter<Bonus>(FilterCodes.BonusAllEmployees, e => true, SecurityRoleType.CanViewAllEmployeesBonuses),
                        new NamedFilter<Bonus, long[]>(FilterCodes.BonusOrgUnit, (bonus, orgUnitIds) => orgUnitIds.Any(id => bonus.Employee.OrgUnit.Path.Contains("/" + id + "/")))
                    }
                    .Union(CardIndexServiceHelper.BuildSoftDeletableFilters<Bonus>()));
            }
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<Bonus, BonusDto> recordAddedEventArgs)
        {
            base.OnRecordAdded(recordAddedEventArgs);

            if (recordAddedEventArgs.InputDto.ShouldRecalculateInvoicesAfterChange)
            {
                _bonusService.RecalculateAffectedInvoices(
                recordAddedEventArgs.InputDto.EmployeeId,
                recordAddedEventArgs.InputDto.StartDate,
                recordAddedEventArgs.InputDto.EndDate);
            }
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<Bonus, BonusDto> recordEditedEventArgs)
        {
            base.OnRecordEdited(recordEditedEventArgs);

            if (recordEditedEventArgs.InputDto.ShouldRecalculateInvoicesAfterChange)
            {
                _bonusService.RecalculateAffectedInvoices(
                    recordEditedEventArgs.InputDto.EmployeeId,
                    recordEditedEventArgs.InputDto.StartDate,
                    recordEditedEventArgs.InputDto.EndDate);
            }
        }
    }
}
