﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Compensation.BusinessLogics;
using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.DocumentMappings;
using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.Business.Compensation.Helpers;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.Business.Compensation.Resources;
using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Compensation;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Compensation.Services
{
    public class TimeReportInvoiceService : ITimeReportInvoiceService
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly ITextExtractionService _textExtractionService;
        private readonly IUploadedDocumentHandlingService _uploadedDocumentHandlingService;
        private readonly IUnitOfWorkService<ICompensationDbScope> _unitOfWorkService;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly ISystemParameterService _systemParameterService;

        public TimeReportInvoiceService(
            IPrincipalProvider principalProvider,
            ITextExtractionService textExtractionService,
            IUploadedDocumentHandlingService uploadedDocumentHandlingService,
            IUnitOfWorkService<ICompensationDbScope> unitOfWorkService,
            IReliableEmailService reliableEmailService,
            IMessageTemplateService messageTemplateService,
            ISystemParameterService systemParameterService)
        {
            _principalProvider = principalProvider;
            _textExtractionService = textExtractionService;
            _uploadedDocumentHandlingService = uploadedDocumentHandlingService;
            _unitOfWorkService = unitOfWorkService;
            _reliableEmailService = reliableEmailService;
            _messageTemplateService = messageTemplateService;
            _systemParameterService = systemParameterService;
        }

        public InvoiceDetailsDto GetInvoiceDetails(int year, byte month, long employeeId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var timeReport = unitOfWork.Repositories.TimeReports
                    .Where(r => r.EmployeeId == employeeId && r.Year == year && r.Month == month)
                    .Select(r => new
                    {
                        r.Id,
                        r.InvoiceNotificationStatus,
                        r.InvoiceNotificationErrorMessage,
                        r.InvoiceStatus,
                        r.InvoiceIssues,
                        r.InvoiceRejectionReason,
                        r.InvoiceDocument
                    })
                    .SingleOrDefault();

                if (timeReport == null)
                {
                    throw new BusinessException(string.Format(InvoiceResources.NoTimeReportError, new DateTime(year, month, 1)));
                }

                var from = DateHelper.BeginningOfMonth(year, month);
                var to = DateHelper.EndOfMonth(year, month);

                var employmentPeriod = unitOfWork.Repositories.EmploymentPeriods
                    .Where(p => EmploymentPeriodBusinessLogic.OverlappingPeriodsForEmployee.Call(p, employeeId, from, to))
                    .Where(p => p.ContractType.NotifyInvoiceAmount)
                    .OrderByDescending(p => p.StartDate)
                    .Select(p => new
                    {
                        p.ContractorCompanyName,
                        p.ContractorCompanyStreetAddress,
                        p.ContractorCompanyZipCode,
                        p.ContractorCompanyCity,
                        p.ContractorCompanyTaxIdentificationNumber
                    })
                    .FirstOrDefault();

                if (employmentPeriod == null)
                {
                    throw new BusinessException(string.Format(InvoiceResources.NoContractorError, new DateTime(year, month, 1)));
                }

                var invoiceDetailsDto = new InvoiceDetailsDto
                {
                    Year = year,
                    Month = month,
                    EmployeeId = employeeId,
                    ContractorCompanyName = employmentPeriod.ContractorCompanyName,
                    ContractorCompanyStreetAddress = employmentPeriod.ContractorCompanyStreetAddress,
                    ContractorCompanyZipCode = employmentPeriod.ContractorCompanyZipCode,
                    ContractorCompanyCity = employmentPeriod.ContractorCompanyCity,
                    ContractorCompanyTaxIdentificationNumber = employmentPeriod.ContractorCompanyTaxIdentificationNumber,
                    InvoiceNotificationStatus = timeReport.InvoiceNotificationStatus,
                    InvoiceNotificationErrorMessage = timeReport.InvoiceNotificationErrorMessage,
                    InvoiceStatus = timeReport.InvoiceStatus,
                    InvoiceIssues = timeReport.InvoiceIssues,
                    InvoiceRejectionReason = timeReport.InvoiceRejectionReason,
                    Invoice = timeReport.InvoiceDocument == null ? null : new DocumentDto
                    {
                        ContentType = timeReport.InvoiceDocument.ContentType,
                        DocumentName = timeReport.InvoiceDocument.Name,
                        DocumentId = timeReport.InvoiceDocument.Id
                    }
                };

                var calculationResult = unitOfWork.Repositories.InvoiceCalculations
                    .Where(c => c.EmployeeId == employeeId && c.Year == year && c.Month == month)
                    .Where(c => c.IsValid)
                    .Select(c => c.CalculationResult)
                    .AsEnumerable()
                    .Select(JsonHelper.Deserialize<CalculationResult>)
                    .SingleOrDefault();

                if (calculationResult != null)
                {
                    invoiceDetailsDto.ServicesTotal = CompensationAmountHelper.GetCurrencyAmountForServices(calculationResult);
                    invoiceDetailsDto.ExpensesTotal = CompensationAmountHelper.GetCurrencyAmountForExpenses(calculationResult);
                    invoiceDetailsDto.CompensationTotal = CompensationAmountHelper.GetTotalCurrencyAmount(calculationResult);
                }

                return invoiceDetailsDto;
            }
        }

        public void UploadInvoice(int year, byte month, long employeeId, DocumentDto invoiceDto)
        {
            AttachInvoice(year, month, employeeId, invoiceDto);
            ValidateInvoice(year, month, employeeId);
        }

        public BoolResult AcceptInvoicesForPayment(ICollection<long> timeReportIds)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var timeReportsToUpdate = unitOfWork.Repositories.TimeReports
                    .GetByIds(timeReportIds)
                    .Where(t => TimeReportInvoiceBusinessLogic.IsAwaitingForDecision.Call(t) || t.InvoiceStatus == InvoiceStatus.InvoiceRejected)
                    .ToList();

                foreach (var timeReport in timeReportsToUpdate)
                {
                    timeReport.InvoiceStatus = InvoiceStatus.InvoiceAccepted;
                    timeReport.InvoiceRejectionReason = null;

                    SendInvoiceStatusEmail(timeReport, InvoiceStatus.InvoiceAccepted);
                }

                unitOfWork.Commit();

                if (timeReportIds.Count != timeReportsToUpdate.Count)
                {
                    return new BoolResult(false, new[]
                    {
                        new AlertDto { Message = InvoiceResources.NotAllUpdatedWarning, Type = AlertType.Error, IsDismissable = true }
                    });
                }

                return BoolResult.Success;
            }
        }

        public BoolResult RejectInvoices(ICollection<long> timeReportIds, string justification)
        {
            if (string.IsNullOrEmpty(justification))
            {
                throw new ArgumentNullException(nameof(justification));
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var timeReportsToUpdate = unitOfWork.Repositories.TimeReports
                    .GetByIds(timeReportIds)
                    .Where(t => TimeReportInvoiceBusinessLogic.IsAwaitingForDecision.Call(t) || t.InvoiceStatus == InvoiceStatus.InvoiceAccepted)
                    .ToList();

                foreach (var timeReport in timeReportsToUpdate)
                {
                    timeReport.InvoiceStatus = InvoiceStatus.InvoiceRejected;
                    timeReport.InvoiceRejectionReason = justification;

                    SendInvoiceStatusEmail(timeReport, InvoiceStatus.InvoiceRejected);
                }

                unitOfWork.Commit();

                if (timeReportIds.Count != timeReportsToUpdate.Count)
                {
                    return new BoolResult(false, new[]
                    {
                        new AlertDto { Message = InvoiceResources.NotAllUpdatedWarning, Type = AlertType.Error, IsDismissable = true }
                    });
                }

                return BoolResult.Success;
            }
        }

        public InvoiceStatus GetInitialInvoiceStatusForEmployee(long employeeId, int year, byte month)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var beginningOfMonth = DateHelper.BeginningOfMonth(year, month);
                var endOfMonth = DateHelper.EndOfMonth(beginningOfMonth);

                var isApplicable = unitOfWork.Repositories.EmploymentPeriods
                    .Where(p => EmploymentPeriodBusinessLogic.OverlappingPeriodsForEmployee.Call(p, employeeId, beginningOfMonth, endOfMonth))
                    .Any(p => p.ContractType.NotifyInvoiceAmount);

                return isApplicable
                    ? InvoiceStatus.InvoiceNotSent
                    : InvoiceStatus.InvoiceNotApplicable;
            }
        }

        private void AttachInvoice(int year, byte month, long employeeId, DocumentDto invoiceDto)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var principal = _principalProvider.Current;
                var timeReport = unitOfWork.Repositories.TimeReports
                    .Include(r => r.InvoiceDocument)
                    .Single(r => r.EmployeeId == employeeId && r.Year == year && r.Month == month);

                if (timeReport.EmployeeId != principal.EmployeeId && !principal.IsInRole(SecurityRoleType.CanUploadInvoiceOnBehalf))
                {
                    throw new MissingRequiredRoleException(new[] { SecurityRoleType.CanUploadInvoiceOnBehalf.ToString() });
                }

                if (!TimeReportInvoiceBusinessLogic.CanUploadInvoice.Call(timeReport))
                {
                    throw new BusinessException(InvoiceResources.InvalidStatusForUploadError);
                }

                var invoice = _uploadedDocumentHandlingService.UpdateRelatedDocument(unitOfWork, timeReport.InvoiceDocument, invoiceDto, p => new InvoiceDocument
                {
                    Name = p.DocumentName,
                    ContentType = p.ContentType
                });

                timeReport.InvoiceDocument = invoice;
                timeReport.InvoiceRejectionReason = null;
                timeReport.InvoiceStatus = InvoiceStatus.InvoiceSent;
                timeReport.InvoiceIssues = null;

                unitOfWork.Commit();
            }

            _uploadedDocumentHandlingService.PromoteTemporaryDocuments<InvoiceDocument, InvoiceDocumentContent, InvoiceDocumentMapping, ICompensationDbScope>(new[] { invoiceDto });
        }

        private void ValidateInvoice(int year, byte month, long employeeId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var from = DateHelper.BeginningOfMonth(year, month);
                var to = DateHelper.EndOfMonth(year, month);

                var contractorCompanyData = unitOfWork.Repositories.EmploymentPeriods
                    .Where(p => EmploymentPeriodBusinessLogic.OverlappingPeriodsForEmployee.Call(p, employeeId, from, to))
                    .Where(p => p.ContractType.NotifyInvoiceAmount)
                    .OrderByDescending(p => p.StartDate)
                    .Select(p => new
                    {
                        p.ContractorCompanyName,
                        p.ContractorCompanyStreetAddress,
                        p.ContractorCompanyZipCode,
                        p.ContractorCompanyCity,
                        p.ContractorCompanyTaxIdentificationNumber
                    })
                    .First();

                var timeReport = unitOfWork.Repositories.TimeReports
                    .Include(r => r.InvoiceDocument)
                    .Include(r => r.InvoiceDocument.DocumentContent)
                    .Single(r => r.EmployeeId == employeeId && r.Year == year && r.Month == month);

                var invoiceContent = _textExtractionService.ExtractPlainText(
                    timeReport.InvoiceDocument.DocumentContent.Content,
                    Path.GetExtension(timeReport.InvoiceDocument.Name));

                var calculationResult = unitOfWork.Repositories.InvoiceCalculations
                    .Where(c => c.EmployeeId == employeeId && c.Year == year && c.Month == month)
                    .Where(c => c.IsValid)
                    .Select(c => c.CalculationResult)
                    .AsEnumerable()
                    .Select(JsonHelper.Deserialize<CalculationResult>)
                    .SingleOrDefault();

                timeReport.InvoiceIssues =
                    ValidateCompanyName(invoiceContent, contractorCompanyData.ContractorCompanyName)
                    | ValidateCompanyStreetAddress(invoiceContent, contractorCompanyData.ContractorCompanyStreetAddress)
                    | ValidateCompanyZipCode(invoiceContent, contractorCompanyData.ContractorCompanyZipCode)
                    | ValidateCompanyCity(invoiceContent, contractorCompanyData.ContractorCompanyCity)
                    | ValidateCompanyTaxIdentificationNumber(invoiceContent, contractorCompanyData.ContractorCompanyTaxIdentificationNumber)
                    | ValidateTotalAmount(invoiceContent, CompensationAmountHelper.GetCurrencyAmountForServices(calculationResult).Amount);

                if (timeReport.InvoiceIssues != InvoiceIssues.None)
                {
                    timeReport.InvoiceStatus = InvoiceStatus.InvoiceSentWithIssues;
                }

                unitOfWork.Commit();
            }
        }

        private static InvoiceIssues ValidateCompanyName(string content, string companyName)
            => content.Contains(companyName.Trim())
                ? InvoiceIssues.None
                : InvoiceIssues.ContractorCompanyNameNotFound;

        private static InvoiceIssues ValidateCompanyStreetAddress(string content, string companyStreetAddress)
            => content.Contains(Regex.Replace(companyStreetAddress.Trim(), @"^(ul\.|os\.|pl\.|al\.)", "").TrimStart())
               ? InvoiceIssues.None
               : InvoiceIssues.ContractorCompanyStreetAddressNotFound;

        private static InvoiceIssues ValidateCompanyZipCode(string content, string companyZipCode)
            => content.RemoveCharacters('-').Contains(companyZipCode.RemoveCharacters('-').Trim())
                ? InvoiceIssues.None
                : InvoiceIssues.ContractorCompanyZipCodeNotFound;

        private static InvoiceIssues ValidateCompanyCity(string content, string companyCity)
            => content.Contains(companyCity.Trim())
                ? InvoiceIssues.None
                : InvoiceIssues.ContractorCompanyCityNotFound;

        private static InvoiceIssues ValidateCompanyTaxIdentificationNumber(string content, string companyTaxIdentificationNumber)
            => content.RemoveCharacters('-').Contains(companyTaxIdentificationNumber.RemoveCharacters('-').Trim())
                ? InvoiceIssues.None
                : InvoiceIssues.ContractorCompanyTaxIdentificationNumberNotFound;

        private static InvoiceIssues ValidateTotalAmount(string content, decimal totalAmount)
            => content.RemoveCharacters(' ', '.', ',').Contains(totalAmount.ToInvariantString("0.00").RemoveCharacters('.').Trim())
                ? InvoiceIssues.None
                : InvoiceIssues.TotalAmountNotFound;

        private void SendInvoiceStatusEmail(TimeReport timeReport, InvoiceStatus invoiceStatus)
        {
            var recipients = new List<EmailRecipientDto>();

            var recipient = new EmailRecipientDto
            {
                EmailAddress = timeReport.Employee.Email,
                FirstName = timeReport.Employee.FirstName,
                LastName = timeReport.Employee.LastName
            };

            recipients.Add(recipient);

            TemplateProcessingResult emailSubject = null;
            TemplateProcessingResult emailBody = null;
            var fullName = recipient.FullName;
            var timeReportYear = timeReport.Year;
            var timeReportMonth = timeReport.Month;
            var timeReportDate = new DateTime(timeReportYear, timeReportMonth, 1).ToString("MMM yyyy");
            var baseUri = new Uri(_systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress));
            var invoiceLink = new Uri(baseUri, $"Compensation/Invoice/Invoice?year={timeReportYear}&month={timeReportMonth}");

            var invoiceStatusEmailDto = new InvoiceStatusEmailDto
            {
                EmployeeFullName = fullName,
                InvoiceLink = invoiceLink
            };

            switch (invoiceStatus)
            {
                case InvoiceStatus.InvoiceRejected:
                    {
                        emailSubject = _messageTemplateService.ResolveTemplate(TemplateCodes.TimeReportInvoiceRejectedSubject, timeReportDate);
                        emailBody = _messageTemplateService.ResolveTemplate(TemplateCodes.TimeReportInvoiceRejectedBody, invoiceStatusEmailDto);

                        break;
                    }

                case InvoiceStatus.InvoiceAccepted:
                    {
                        emailSubject = _messageTemplateService.ResolveTemplate(TemplateCodes.TimeReportInvoiceAcceptedSubject, timeReportDate);
                        emailBody = _messageTemplateService.ResolveTemplate(TemplateCodes.TimeReportInvoiceAcceptedBody, invoiceStatusEmailDto);

                        break;
                    }

                default:
                    throw new ArgumentOutOfRangeException(nameof(invoiceStatus));
            }

            var emailMessageDto = new EmailMessageDto
            {
                IsHtml = emailBody.IsHtml,
                Subject = emailSubject.Content,
                Body = emailBody.Content,
                Recipients = recipients
            };

            _reliableEmailService.EnqueueMessage(emailMessageDto);
        }
    }
}