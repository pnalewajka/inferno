﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Compensation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Compensation.Services
{
    public class EcbCurrencyExchangeRateService : BaseCurrencyExchangeRateService
    {
        private readonly ISystemParameterService _systemParameterService;

        public EcbCurrencyExchangeRateService(
            ICurrencyService currencyService,
            ISystemParameterService systemParameterService,
            IUnitOfWorkService<ICompensationDbScope> unitOfWorkService)
            : base(currencyService, unitOfWorkService)
        {
            _systemParameterService = systemParameterService;
        }

        protected override string BaseCurrencyIsoCode => CurrencyIsoCodes.Euro;

        protected override CurrencyExchangeRateSourceType SourceType => CurrencyExchangeRateSourceType.EuropeanCentralBank;

        protected override CurrencyExchangeRateDto GetExchangeRateFromExternalSource(IUnitOfWork<ICompensationDbScope> unitOfWork, string quotedCurrencyIsoCode, DateTime date)
        {
            const double dayOffset = -7;

            var exchangeRates = GetEcbExchangeRatesForRange(quotedCurrencyIsoCode, date.AddDays(dayOffset), date);

            SaveEcbExchangeRatesToDatabase(unitOfWork, exchangeRates, quotedCurrencyIsoCode);

            var exchangeRate = exchangeRates.OrderByDescending(r => r.Date).First();

            if (exchangeRate != null)
            {
                SaveEcbExchangeRateToDatabase(unitOfWork, exchangeRate, quotedCurrencyIsoCode, date);

                return exchangeRate;
            }

            return null;
        }

        public CurrencyExchangeRateDto[] GetEcbExchangeRatesForRange(string isoCode, DateTime from, DateTime to)
        {
            const string dateFormat = "yyyy-MM-dd";

            var queryUrl = _systemParameterService.GetParameter<string>(ParameterKeys.EcbApiCurrencyMeanInDateRangeUrl);
            var url = string.Format(queryUrl, isoCode, from.ToString(dateFormat), to.ToString(dateFormat));

            for (var retriesLeft = _systemParameterService.GetParameter<int>(ParameterKeys.EcbApiRetryCount); retriesLeft > 0; --retriesLeft)
            {
                try
                {
                    var task = GetEcbResponseAsync(url);
                    var result = task.GetAwaiter().GetResult();

                    return GetExchangeRatesFromEcbResponse(result, isoCode);
                }
                catch { }
            }

            throw new InvalidOperationException("ECB exchange rates are unavailable");
        }

        private async Task<string> GetEcbResponseAsync(string query)
        {
            var baseAddress = _systemParameterService.GetParameter<string>(ParameterKeys.EcbApiBaseUrl);

            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "application/vnd.sdmx.data+json;version=1.0.0-wd");

                using (var httpResponse = await httpClient.GetAsync(baseAddress + query).ConfigureAwait(false))
                {
                    if (!httpResponse.IsSuccessStatusCode)
                    {
                        throw new InvalidOperationException("ECB exchange rates are unavailable");
                    }

                    return await httpResponse.Content.ReadAsStringAsync().ConfigureAwait(false);
                }
            }
        }

        private void SaveEcbExchangeRateToDatabase(IUnitOfWork<ICompensationDbScope> unitOfWork, CurrencyExchangeRateDto exchangeRateDto, string quotedCurrencyIsoCode, DateTime date)
        {
            var currencyIds = CurrencyService.GetCurrencyIdByIsoCodes();

            var exchangeRate = new CurrencyExchangeRate
            {
                Date = date,
                BaseCurrencyId = currencyIds[BaseCurrencyIsoCode],
                QuotedCurrencyId = currencyIds[quotedCurrencyIsoCode],
                Rate = exchangeRateDto.Rate,
                TableNumber = exchangeRateDto.TableNumber,
                SourceType = SourceType
            };

            SaveExchangeRatesToDatabase(unitOfWork, new[] { exchangeRate });
        }

        private void SaveEcbExchangeRatesToDatabase(IUnitOfWork<ICompensationDbScope> unitOfWork, CurrencyExchangeRateDto[] exchangeRateDtos, string quotedCurrencyIsoCode)
        {
            var currencyIds = CurrencyService.GetCurrencyIdByIsoCodes();

            var exchangeRates = exchangeRateDtos
                .Select(d => new CurrencyExchangeRate
                {
                    Date = d.Date,
                    BaseCurrencyId = currencyIds[BaseCurrencyIsoCode],
                    QuotedCurrencyId = currencyIds[quotedCurrencyIsoCode],
                    Rate = d.Rate,
                    TableNumber = d.TableNumber,
                    SourceType = SourceType
                })
                .ToArray();

            SaveExchangeRatesToDatabase(unitOfWork, exchangeRates);
        }

        private static CurrencyExchangeRateDto[] GetExchangeRatesFromEcbResponse(string response, string isoCode)
        {
            const string idPropertyName = "id";
            const string exchangeRatesNodeName = "dataSets";
            const string exchangeRatesArrayContainerNode = "observations";
            const string datesNodeName = "structure";
            const string datesId = "TIME_PERIOD";

            var container = (JContainer)JToken.Parse(response);
            var exchangeRates = container
                 .Children<JProperty>().Single(p => p.Name == exchangeRatesNodeName)
                 .Descendants().OfType<JProperty>().Single(p => p.Name == exchangeRatesArrayContainerNode)
                 .Descendants().OfType<JArray>().Values<decimal>()
                 .ToList();
            var dates = container
                 .Children<JProperty>().Single(p => p.Name == datesNodeName)
                 .Descendants().OfType<JProperty>().Single(p => p.Name == idPropertyName && p.Value.Value<string>() == datesId)
                 .Parent.Descendants().OfType<JArray>().Single()
                 .Descendants().OfType<JProperty>().Where(p => p.Name == idPropertyName).Values<DateTime>()
                 .ToList();

            return exchangeRates.Zip(dates, (r, d) => new CurrencyExchangeRateDto
            {
                Date = d,
                Rate = 1m / r,
                TableNumber = $"EXR/D.{isoCode}.EUR.SP00.A"
            }).ToArray();
        }
    }
}
