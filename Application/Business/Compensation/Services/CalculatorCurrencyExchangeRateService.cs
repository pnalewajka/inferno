﻿using System;
using Smt.Atomic.Business.Compensation.Dto;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.Business.Compensation.Resources;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.Business.Compensation.Services
{
    public class CalculatorCurrencyExchangeRateService : ICalculatorCurrencyExchangeRateService
    {
        public decimal SameCurrencyExchangeRate { get; } = 1m;

        private readonly ITimeService _timeService;
        private readonly ICurrencyExchangeRateService _ecbCurrencyExchangeRateService;
        private readonly ICurrencyExchangeRateService _nbpCurrencyExchangeRateService;
        private readonly ICurrencyExchangeRateService _oxrCurrencyExchangeRateService;

        public CalculatorCurrencyExchangeRateService(
            ITimeService timeService,
            EcbCurrencyExchangeRateService ecbCurrencyExchangeRateService,
            NbpCurrencyExchangeRateService nbpCurrencyExchangeRateService,
            OxrCurrencyExchangeRateService oxrCurrencyExchangeRateService)
        {
            _timeService = timeService;
            _ecbCurrencyExchangeRateService = ecbCurrencyExchangeRateService;
            _nbpCurrencyExchangeRateService = nbpCurrencyExchangeRateService;
            _oxrCurrencyExchangeRateService = oxrCurrencyExchangeRateService;
        }

        public CurrencyExchangeRateDto GetExchangeRateToEur(string currencyIsoCode, DateTime date)
        {
            if (date.Date >= _timeService.GetCurrentDate())
            {
                var message = string.Format(CalculatorResources.EcbCurrencyExchangeRateNotExistsYet, currencyIsoCode, date);

                throw new BusinessException(message);
            }

            if (currencyIsoCode == CurrencyIsoCodes.Euro)
            {
                return new CurrencyExchangeRateDto
                {
                    Date = date,
                    Rate = SameCurrencyExchangeRate
                };
            }

            try
            {
                return _ecbCurrencyExchangeRateService.GetExchangeRateForDay(currencyIsoCode, date);
            }
            catch (Exception exception)
            {
                var message = string.Format(CalculatorResources.EcbCurrencyExchangeRateRetrievalFailed, currencyIsoCode, date);

                throw new BusinessException(message, exception);
            }
        }

        public CurrencyExchangeRateDto GetExchangeRateToGbp(string currencyIsoCode, DateTime date)
        {
            if (currencyIsoCode == CurrencyIsoCodes.PoundSterling)
            {
                return new CurrencyExchangeRateDto
                {
                    Date = date,
                    Rate = SameCurrencyExchangeRate
                };
            }

            if (currencyIsoCode == CurrencyIsoCodes.PolishZloty)
            {
                var exchangeRate = GetExchangeRateToPln(CurrencyIsoCodes.PoundSterling, date);

                return new CurrencyExchangeRateDto
                {
                    Date = date,
                    TableNumber = exchangeRate.TableNumber,
                    Rate = 1m / exchangeRate.Rate
                };
            }

            var exchangeRateToPln = GetExchangeRateToPln(currencyIsoCode, date);
            var exchangeRateToGbp = GetExchangeRateToPln(CurrencyIsoCodes.PoundSterling, date);

            return new CurrencyExchangeRateDto
            {
                Date = date,
                TableNumber = exchangeRateToPln.TableNumber,
                Rate = exchangeRateToPln.Rate / exchangeRateToGbp.Rate
            };
        }

        public CurrencyExchangeRateDto GetExchangeRateToPln(string currencyIsoCode, DateTime date)
        {
            if (date.Date > _timeService.GetCurrentDate())
            {
                var message = string.Format(CalculatorResources.NbpCurrencyExchangeRateNotExistsYet, currencyIsoCode, date);

                throw new BusinessException(message);
            }

            if (currencyIsoCode == CurrencyIsoCodes.PolishZloty)
            {
                return new CurrencyExchangeRateDto
                {
                    Date = date,
                    Rate = SameCurrencyExchangeRate
                };
            }

            try
            {
                return _nbpCurrencyExchangeRateService.GetExchangeRateForDay(currencyIsoCode, date);
            }
            catch (Exception exception)
            {
                var message = string.Format(CalculatorResources.NbpCurrencyExchangeRateRetrievalFailed, currencyIsoCode, date);

                throw new BusinessException(message, exception);
            }
        }

        public CurrencyExchangeRateDto GetExchangeRateToUsd(string currencyIsoCode, DateTime date)
        {
            if (date.Date >= _timeService.GetCurrentDate())
            {
                var message = string.Format(CalculatorResources.OxrCurrencyExchangeRateNotExistsYet, currencyIsoCode, date);

                throw new BusinessException(message);
            }

            if (currencyIsoCode == CurrencyIsoCodes.UnitedStatesDollar)
            {
                return new CurrencyExchangeRateDto
                {
                    Date = date,
                    Rate = SameCurrencyExchangeRate
                };
            }

            try
            {
                return _oxrCurrencyExchangeRateService.GetExchangeRateForDay(currencyIsoCode, date);
            }
            catch (Exception exception)
            {
                var message = string.Format(CalculatorResources.OxrCurrencyExchangeRateRetrievalFailed, currencyIsoCode, date);

                throw new BusinessException(message, exception);
            }
        }
    }
}
