﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;

namespace Smt.Atomic.Business.Compensation.BusinessLogics
{
    public static class TimeReportInvoiceBusinessLogic
    {
        public static readonly BusinessLogic<InvoiceNotificationStatus, bool> IsUploadPermittedForNotificationStatus =
            new BusinessLogic<InvoiceNotificationStatus, bool>(status => status == InvoiceNotificationStatus.NotificationSent
                                                                      || status == InvoiceNotificationStatus.NotificationOutdated);

        public static readonly BusinessLogic<InvoiceStatus, bool> IsUploadPermittedForStatus =
            new BusinessLogic<InvoiceStatus, bool>(status => status == InvoiceStatus.InvoiceNotSent
                                                          || status == InvoiceStatus.InvoiceSent
                                                          || status == InvoiceStatus.InvoiceSentWithIssues
                                                          || status == InvoiceStatus.InvoiceRejected);

        public static readonly BusinessLogic<TimeReport, bool> CanUploadInvoice =
            new BusinessLogic<TimeReport, bool>(timeReport => IsUploadPermittedForStatus.Call(timeReport.InvoiceStatus));

        public static readonly BusinessLogic<TimeReport, bool> IsAwaitingForDecision =
            new BusinessLogic<TimeReport, bool>(timeReport => timeReport.InvoiceStatus == InvoiceStatus.InvoiceSent
                                                           || timeReport.InvoiceStatus == InvoiceStatus.InvoiceSentWithIssues);

        public static readonly BusinessLogic<TimeReport, bool> IsNotApplicable =
            new BusinessLogic<TimeReport, bool>(timeReport => timeReport.InvoiceStatus == InvoiceStatus.InvoiceNotApplicable);
    }
}
