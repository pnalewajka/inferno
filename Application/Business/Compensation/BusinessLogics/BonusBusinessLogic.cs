﻿using System;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.Compensation;

namespace Smt.Atomic.Business.Compensation.BusinessLogics
{
    public static class BonusBusinessLogic
    {
        public static readonly BusinessLogic<Bonus, bool> BonusIsActive
            = new BusinessLogic<Bonus, bool>(bonus => !bonus.IsDeleted);

        public static BusinessLogic<Bonus, bool> BonusForPeriod(DateTime from, DateTime to)
        {
            var overlapingExpression = DateRangeHelper.OverlapingDateRange<Bonus>(b => b.StartDate, b => b.EndDate, from, to);

            return new BusinessLogic<Bonus, bool>(overlapingExpression);
        }

        public static BusinessLogic<Bonus, bool> BonusIsActiveForPeriod(DateTime from, DateTime to)
        {
            var periodFilterLogic = BonusForPeriod(from, to);

            return new BusinessLogic<Bonus, bool>(bonus => BonusIsActive.Call(bonus) && periodFilterLogic.Call(bonus));
        }
    }
}
