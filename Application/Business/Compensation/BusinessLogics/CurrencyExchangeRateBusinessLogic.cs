﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Compensation;

namespace Smt.Atomic.Business.Compensation.BusinessLogics
{
    public static class CurrencyExchangeRateBusinessLogic
    {
        public static readonly BusinessLogic<CurrencyExchangeRate, string, string, DateTime, CurrencyExchangeRateSourceType, bool> IsCachedByIsoCodes
            = new BusinessLogic<CurrencyExchangeRate, string, string, DateTime, CurrencyExchangeRateSourceType, bool>(
                (currencyExchangeRate, baseCurrencyIsoCode, quotedCurrencyIsoCode, date, sourceType) =>
                    currencyExchangeRate.BaseCurrency.IsoCode == baseCurrencyIsoCode
                        && currencyExchangeRate.QuotedCurrency.IsoCode == quotedCurrencyIsoCode
                        && currencyExchangeRate.Date == date
                        && currencyExchangeRate.SourceType == sourceType);

        public static readonly BusinessLogic<CurrencyExchangeRate, long, long, DateTime, CurrencyExchangeRateSourceType, bool> IsCached
            = new BusinessLogic<CurrencyExchangeRate, long, long, DateTime, CurrencyExchangeRateSourceType, bool>(
                (currencyExchangeRate, baseCurrencyId, quotedCurrencyId, date, sourceType) =>
                    currencyExchangeRate.BaseCurrencyId == baseCurrencyId
                        && currencyExchangeRate.QuotedCurrencyId == quotedCurrencyId
                        && currencyExchangeRate.Date == date
                        && currencyExchangeRate.SourceType == sourceType);
    }
}
