﻿using System;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;

namespace Smt.Atomic.Business.Compensation.BusinessLogics
{
    public static class DailyAllowanceBusinessLogic
    {
        public static readonly BusinessLogic<DailyAllowance, string, DateTime, bool> ForGivenCountryAndDate
            = new BusinessLogic<DailyAllowance, string, DateTime, bool>((dailyAllowance, countryIsoCode, date) =>
                dailyAllowance.EmploymentCountry.IsoCode == countryIsoCode && DateRangeHelper.IsDateInRange<DailyAllowance>(
                    a => a.ValidFrom.Value, a => a.ValidTo, date, DateComparer.LeftClosed | DateComparer.RightClosed).Call(dailyAllowance));
    }
}
