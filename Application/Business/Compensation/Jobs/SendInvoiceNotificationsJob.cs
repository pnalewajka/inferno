﻿using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.Business.Compensation.Jobs
{
    [Identifier("Job.Compensation.SendInvoiceNotifications")]
    [JobDefinition(
        Code = "SendInvoiceNotifications",
        Description = "Send pending invoice notifications",
        ScheduleSettings = "0,30 * * * * *",
        MaxExecutioPeriodInSeconds = 1800,
        PipelineName = PipelineCodes.TimeTracking)]
    public sealed class SendInvoiceNotificationsJob : IJob
    {
        private readonly ITimeReportInvoiceNotificationService _timeReportInvoiceNotificationService;

        public SendInvoiceNotificationsJob(ITimeReportInvoiceNotificationService timeReportInvoiceNotificationService)
        {
            _timeReportInvoiceNotificationService = timeReportInvoiceNotificationService;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            _timeReportInvoiceNotificationService.SendMarkedNotifications(executionContext.CancellationToken);

            return JobResult.Success();
        }
    }
}