﻿using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.Business.Compensation.ReportModels
{
    public class IntiveIncPaymentNotificationReportModel
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public decimal Amount { get; set; }

        public string CurrencyIsoCode => CurrencyIsoCodes.UnitedStatesDollar;

        public string Category { get; set; }

        public string BusinessTripIdentifier { get; set; }

        public string Description { get; set; }
    }
}
