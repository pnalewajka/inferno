﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.Business.Compensation.CalculationResults;

namespace Smt.Atomic.Business.Compensation.ReportModels
{
    public class AccountingReportModel
    {
        public class InvoiceDetails
        {
            public long Id { get; set; }

            public int Year { get; set; }

            public byte Month { get; set; }

            public string FullName { get; set; }

            public string Acronym { get; set; }

            public string OrgUnit { get; set; }

            public string Company { get; set; }

            public string ContractType { get; set; }

            public DateTime CalculatedOn { get; set; }

            public string ProjectName { get; set; }

            public string ProjectApn { get; set; }

            public string DataSource { get; set; }

            public string PaymentType { get; set; }

            public decimal Amount { get; set; }

            public string AmountUnit { get; set; }

            public decimal UnitPrice { get; set; }

            public string UnitPriceCurrency { get; set; }

            public decimal TotalPrice => Amount * UnitPrice;
        }

        public List<InvoiceDetails> Details { get; set; } = new List<InvoiceDetails>();
    }
}
