﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.Business.Compensation.CalculationResults;

namespace Smt.Atomic.Business.Compensation.ReportModels
{
    public class InvoiceCalculationsReportModel
    {
        public class InvoiceInfo
        {
            public long Id { get; set; }

            public int Year { get; set; }

            public byte Month { get; set; }

            public string FullName { get; set; }

            public string Acronym { get; set; }

            public string OrgUnit { get; set; }

            public string ContractType { get; set; }

            public List<CostItem> CostItems { get; set; }

            public DateTime NotifiedOn { get; set; }

            public int InvolvedBusinessTripCount { get; set; }

            public int Version { get; set; }
        }

        public List<InvoiceInfo> Invoices { get; set; } = new List<InvoiceInfo>();
    }
}
