﻿pivot.dataConverter = function (data) {
    return data.Invoices.map(function (a) {
        obj = {
            "Year": a.Year,
            "Month": a.Month,
            "Name": a.FullName,
            "Acronym": a.Acronym,
            "Org Unit": a.OrgUnit,
            "Contract Type": a.ContractType,
            "Compensation Total": undefined, // force column to exist
            "Notified On": Reporting.displayDate(a.NotifiedOn),
            "Involved Business Trip Count": a.InvolvedBusinessTripCount,
            "Invoice Version": a.Version,
            "Was Employee Notified": a.WasEmployeeNotified
        };

        // cost-items
        for (i = 0; i < a.CostItems.length; ++i) {
            costItem = a.CostItems[i];
            currentValue = parseFloat(costItem.Value);

            if (currentValue > 0) {
                name = Globals.enumResources.CostItemGroupType[costItem.Group];

                previousValue = obj[name] !== undefined
                    ? parseFloat(obj[name].split(" ")[0]) // remove currency
                    : 0;

                currentValue += previousValue;
                value = currentValue + " " + costItem.CurrencyIsoCode;

                obj[name] = value;
            }
        }

        return obj;
    });
};

pivot.configurations = [
    {
        name: "By Org Units",
        code: "by-org-units",
        config: {
            rows: ["Name", "Acronym", "Contract Type", "Notified On", "Compensation Total"],
            cols: ["Org Unit"],
            vals: ["Compensation Total"],
            aggregatorName: "Sum"
        }
    }
];
