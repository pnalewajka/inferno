﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Compensation.CalculationResults;

namespace Smt.Atomic.Business.Compensation.EmailModels
{
    public class CompensationChangedNotificationEmailModel
    {
        public long EmployeeId { get; set; }

        public string FullName { get; set; }

        public decimal HoursAccepted { get; set; }

        public decimal ServicesTotal { get; set; }

        public decimal ExpensesTotal { get; set; }

        public decimal CompensationTotal { get; set; }

        public string CurrencyIsoCode { get; set; }

        public int Year { get; set; }

        public byte Month { get; set; }

        public DateTime SuggestedInvoiceIssue { get; set; }

        public string InvoiceInformation { get; set; }

        public List<CostItem> CostItems { get; set; }
    }
}
