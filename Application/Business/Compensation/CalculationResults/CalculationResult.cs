﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Smt.Atomic.Business.Compensation.Converters;
using Smt.Atomic.Business.Compensation.DiagnosticItems;

namespace Smt.Atomic.Business.Compensation.CalculationResults
{
    [Serializable]
    public class CalculationResult
    {
        public List<CostItem> CostItems { get; set; }

        [JsonConverter(typeof(DiagnosticItemConverter))]
        public List<DiagnosticItem> DiagnosticItems { get; set; }

        public CalculationResult()
        {
            CostItems = new List<CostItem>();
            DiagnosticItems = new List<DiagnosticItem>();
        }

        public IEnumerable<TDiagnosticItem> GetDiagnosticItems<TDiagnosticItem>()
            where TDiagnosticItem : DiagnosticItem
        {
            return DiagnosticItems.Concat(CostItems.SelectMany(i => i.DiagnosticItems)).OfType<TDiagnosticItem>();
        }

        public void AddCostItem(CostItem costItem)
        {
            if (costItem != null)
            {
                CostItems.Add(costItem);
            }
        }

        public void AddSummaryDiagnosticItem(DiagnosticItem diagnosticItem)
        {
            DiagnosticItems.Add(diagnosticItem);
        }
    }
}
