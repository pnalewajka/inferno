﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Smt.Atomic.Business.Compensation.Converters;
using Smt.Atomic.Business.Compensation.DiagnosticItems;
using Smt.Atomic.Business.Compensation.Enums;

namespace Smt.Atomic.Business.Compensation.CalculationResults
{
    [Serializable]
    public class CostItem
    {
        public CostItemGroupType Group { get; set; }

        public decimal Value { get; set; }

        public string CurrencyIsoCode { get; set; }

        [JsonConverter(typeof(DiagnosticItemConverter))]
        public List<DiagnosticItem> DiagnosticItems { get; set; }

        public CostItem()
        {
            DiagnosticItems = new List<DiagnosticItem>();
        }

        public void AddDiagnosticItem(DiagnosticItem diagnosticItem)
        {
            DiagnosticItems.Add(diagnosticItem);
        }
    }
}
