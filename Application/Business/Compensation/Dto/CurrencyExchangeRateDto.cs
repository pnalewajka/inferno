﻿using System;

namespace Smt.Atomic.Business.Compensation.Dto
{
    public class CurrencyExchangeRateDto
    {
        public string TableNumber { get; set; }

        public DateTime Date { get; set; }

        public decimal Rate { get; set; }
    }
}
