﻿using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.Compensation;

namespace Smt.Atomic.Business.Compensation.Dto
{
    public class InvoiceCalculationToInvoiceCalculationDtoMapping : ClassMapping<InvoiceCalculation, InvoiceCalculationDto>
    {
        public InvoiceCalculationToInvoiceCalculationDtoMapping()
        {
            Mapping = e => new InvoiceCalculationDto
            {
                Id = e.Id,
                EmployeeId = e.EmployeeId,
                Year = e.Year,
                Month = e.Month,
                CalculationResult = JsonHelper.Deserialize<CalculationResult>(e.CalculationResult),
                WasEmployeeNotified = e.WasEmployeeNotified,
                NotifiedOn = e.NotifiedOn,
                IsValid = e.IsValid,
            };
        }
    }
}
