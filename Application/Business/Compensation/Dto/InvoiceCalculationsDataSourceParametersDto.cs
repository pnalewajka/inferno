﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Compensation.Dto
{
    public class InvoiceCalculationsDataSourceParametersDto
    {
        public int? Year { get; set; }

        public int? Month { get; set; }
    }
}
