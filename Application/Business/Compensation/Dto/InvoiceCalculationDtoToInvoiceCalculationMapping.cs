﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.Compensation;

namespace Smt.Atomic.Business.Compensation.Dto
{
    public class InvoiceCalculationDtoToInvoiceCalculationMapping : ClassMapping<InvoiceCalculationDto, InvoiceCalculation>
    {
        public InvoiceCalculationDtoToInvoiceCalculationMapping()
        {
            Mapping = d => CreateEntity(d);
        }

        private InvoiceCalculation CreateEntity(InvoiceCalculationDto d)
        {
            return new InvoiceCalculation
            {
                Id = d.Id,
                EmployeeId = d.EmployeeId,
                Year = d.Year,
                Month = d.Month,
                CalculationResult = JsonHelper.Serialize(d.CalculationResult),
                WasEmployeeNotified = d.WasEmployeeNotified,
                NotifiedOn = d.NotifiedOn,
                IsValid = d.IsValid,
            };
        }
    }
}
