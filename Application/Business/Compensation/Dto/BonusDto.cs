﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Compensation.Dto
{
    public class BonusDto
    {
        public long Id { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public long EmployeeId { get; set; }

        public string EmployeeFullName { get; set; }

        public bool ShouldRecalculateInvoicesAfterChange { get; set; }

        public long ProjectId { get; set; }

        public decimal BonusAmount { get; set; }

        public long CurrencyId { get; set; }

        public string Justification { get; set; }

        public BonusType Type { get; set; }

        public bool IsDeleted { get; set; }

        public bool Reinvoice { get; set; }
    }
}
