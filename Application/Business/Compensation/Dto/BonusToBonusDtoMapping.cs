﻿using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Compensation;

namespace Smt.Atomic.Business.Compensation.Dto
{
    public class BonusToBonusDtoMapping : ClassMapping<Bonus, BonusDto>
    {
        public BonusToBonusDtoMapping()
        {
            Mapping = e => new BonusDto
            {
                Id = e.Id,
                StartDate = e.StartDate,
                EndDate = e.EndDate,
                EmployeeId = e.EmployeeId,
                EmployeeFullName = e.Employee == null ? null : EmployeeBusinessLogic.FullName.Call(e.Employee),
                ProjectId = e.ProjectId,
                BonusAmount = e.BonusAmount,
                CurrencyId = e.CurrencyId,
                Justification = e.Justification,
                Type = e.Type,
                IsDeleted = e.IsDeleted,
                Reinvoice = e.Reinvoice
            };
        }
    }
}
