﻿namespace Smt.Atomic.Business.Compensation.Dto
{
    public class IntiveIncPaymentNotificationDataSourceParametersDto
    {
        public long? BusinessTripSettlementRequestId { get; set; }

        public long? ExpenseRequestId { get; set; }
    }
}
