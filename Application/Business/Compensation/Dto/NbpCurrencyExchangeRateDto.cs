﻿using Newtonsoft.Json;

namespace Smt.Atomic.Business.Compensation.Dto
{
    public class NbpCurrencyExchangeRateDto
    {
        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("code")]
        public string IsoCode { get; set; }

        [JsonProperty("mid")]
        public decimal Rate { get; set; }
    }
}
