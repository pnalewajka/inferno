﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Compensation;

namespace Smt.Atomic.Business.Compensation.Dto
{
    public class BonusDtoToBonusMapping : ClassMapping<BonusDto, Bonus>
    {
        public BonusDtoToBonusMapping()
        {
            Mapping = d => new Bonus
            {
                Id = d.Id,
                StartDate = d.StartDate,
                EndDate = d.EndDate,
                EmployeeId = d.EmployeeId,
                ProjectId = d.ProjectId,
                BonusAmount = d.BonusAmount,
                CurrencyId = d.CurrencyId,
                Justification = d.Justification,
                Type = d.Type,
                IsDeleted = d.IsDeleted,
                Reinvoice = d.Reinvoice
            };
        }
    }
}
