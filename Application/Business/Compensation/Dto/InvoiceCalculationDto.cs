﻿using System;
using Smt.Atomic.Business.Compensation.CalculationResults;

namespace Smt.Atomic.Business.Compensation.Dto
{
    public class InvoiceCalculationDto
    {
        public long Id { get; set; }

        public long EmployeeId { get; set; }

        public int Year { get; set; }

        public byte Month { get; set; }

        public CalculationResult CalculationResult { get; set; }

        public DateTime NotifiedOn { get; set; }

        public bool WasEmployeeNotified { get; set; }

        public bool IsValid { get; set; }
    }
}
