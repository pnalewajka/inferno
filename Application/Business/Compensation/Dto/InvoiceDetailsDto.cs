﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Compensation.Dto
{
    public class InvoiceDetailsDto
    {
        public int Year { get; set; }

        public byte Month { get; set; }

        public long EmployeeId { get; set; }

        public string ContractorCompanyName { get; set; }

        public string ContractorCompanyStreetAddress { get; set; }

        public string ContractorCompanyZipCode { get; set; }

        public string ContractorCompanyCity { get; set; }

        public string ContractorCompanyTaxIdentificationNumber { get; set; }

        public InvoiceNotificationStatus InvoiceNotificationStatus { get; set; }

        public string InvoiceNotificationErrorMessage { get; set; }

        public CurrencyAmount ServicesTotal { get; set; }

        public CurrencyAmount ExpensesTotal { get; set; }

        public CurrencyAmount CompensationTotal { get; set; }

        public InvoiceStatus InvoiceStatus { get; set; }

        public InvoiceIssues? InvoiceIssues { get; set; }

        public string InvoiceRejectionReason { get; set; }

        public DocumentDto Invoice { get; set; }
    }
}
