﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Smt.Atomic.Business.Compensation.Dto
{
    public class OxrCurrencyExchangeRateDto
    {
        [JsonProperty("timestamp")]
        public string Timestamp { get; set; }

        [JsonProperty("base")]
        public string Base { get; set; }

        [JsonProperty("rates")]
        public IDictionary<string, decimal> Rates { get; set; }
    }
}
