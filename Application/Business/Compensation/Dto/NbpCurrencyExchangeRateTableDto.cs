﻿using System;
using Newtonsoft.Json;

namespace Smt.Atomic.Business.Compensation.Dto
{
    public class NbpCurrencyExchangeRateTableDto
    {
        [JsonProperty("table")]
        public string Table { get; set; }

        [JsonProperty("no")]
        public string TableNumber { get; set; }

        [JsonProperty("effectiveDate")]
        public DateTime Date { get; set; }

        [JsonProperty("rates")]
        public NbpCurrencyExchangeRateDto[] Rates { get; set; }
    }
}
