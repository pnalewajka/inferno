﻿using System;

namespace Smt.Atomic.Business.Compensation.Dto
{
    public class InvoiceStatusEmailDto
    {
        public string EmployeeFullName { get; set; }

        public Uri InvoiceLink { get; set; }
    }
}
