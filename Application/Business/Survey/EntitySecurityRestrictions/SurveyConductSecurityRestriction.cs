﻿using Smt.Atomic.Data.Repositories.FilteringQueries;
using Smt.Atomic.Data.Entities.Modules.Survey;
using System;
using System.Linq.Expressions;
using Smt.Atomic.Business.Survey.BusinessLogics;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Survey.EntitySecurityRestrictions
{
    public class SurveyConductSecurityRestriction : EntitySecurityRestriction<SurveyConduct>
    {
        public SurveyConductSecurityRestriction(IPrincipalProvider principalProvider) : base(principalProvider)
        {
            RequiresAuthentication = true;
        }

        public override Expression<Func<SurveyConduct, bool>> GetRestriction()
        {
            if (Roles.Contains(SecurityRoleType.CanViewAllSurveyConducts))
            {
                return AllRecords();
            }

            long? currentUserId = CurrentPrincipal.Id;

            if (Roles.Contains(SecurityRoleType.CanViewSurveyConducts))
            {
                return SurveyConductBusinessLogic.IsCreator.Parametrize(currentUserId.Value);
            }

            return NoRecords();
        }
    }
}
