﻿using Smt.Atomic.Data.Entities.Modules.Survey;

namespace Smt.Atomic.Business.Survey.Services.AnswerProviders
{
    public interface IAnswerProviderFactory
    {
        IAnswerProvider GetAnswerProvider(SurveyConductQuestion surveyQuestion);
    }
}