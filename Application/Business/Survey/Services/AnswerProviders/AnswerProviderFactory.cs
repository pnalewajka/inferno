﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Modules.Survey;

namespace Smt.Atomic.Business.Survey.Services.AnswerProviders
{
    public class AnswerProviderFactory : IAnswerProviderFactory
    {
        public IAnswerProvider GetAnswerProvider(SurveyConductQuestion surveyQuestion)
        {
            switch (surveyQuestion.Type)
            {
                case SurveyQuestionType.Combo:
                case SurveyQuestionType.Picker:
                case SurveyQuestionType.Checkbox:
                case SurveyQuestionType.Radio:

                    return new IdentifierAnswerProvider(new Dto.QuestionItemsDto(surveyQuestion.Items));

                case SurveyQuestionType.OpenQuestion:
                default:

                    return new TextAnswerProvider();
            }
        }
    }
}
