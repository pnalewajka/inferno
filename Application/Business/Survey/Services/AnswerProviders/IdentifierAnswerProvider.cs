﻿using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.Data.Entities.Modules.Survey;
using System;
using System.Text;

namespace Smt.Atomic.Business.Survey.Services.AnswerProviders
{
    class IdentifierAnswerProvider : IAnswerProvider
    {
        private readonly QuestionItemsDto _questionItems;

        private bool IsEmpty(string answer)
        {
            return string.IsNullOrWhiteSpace(answer);
        }

        private bool IsIdentifier(string answer, out int id)
        {
            return int.TryParse(answer, out id);
        }

        private void AddAnswer(StringBuilder answerBuilder, int selectedAnswerId)
        {
            var value = _questionItems[selectedAnswerId];

            if (!IsEmpty(value))
            {
                answerBuilder.AppendLine(value);
            }
        }

        public IdentifierAnswerProvider(QuestionItemsDto questionItems)
        {
            _questionItems = questionItems;
        }

        public string BuildAnswer(SurveyItemDto answer)
        {
            if (answer.SelectedAnswer == null)
            {
                return null;
            }

            var answerBuilder = new StringBuilder();

            foreach (var answerText in answer.SelectedAnswer)
            {
                if (IsEmpty(answerText))
                {
                    continue;
                }

                int selectedAnswerId;
                var identifiers = answerText.Split(',');

                foreach (var identifier in identifiers)
                {
                    if (IsIdentifier(identifier, out selectedAnswerId))
                    {
                        AddAnswer(answerBuilder, selectedAnswerId);
                    }
                }

            }

            var result = answerBuilder.ToString();
            if (string.IsNullOrWhiteSpace(result))
            {
                return null;
            }

            return result;
        }
    }
}
