﻿using Smt.Atomic.Business.Survey.Dto;
using System.Text;

namespace Smt.Atomic.Business.Survey.Services.AnswerProviders
{
    class TextAnswerProvider : IAnswerProvider
    {
        public string BuildAnswer(SurveyItemDto answer)
        {
            if (answer.SelectedAnswer == null)
            {
                return null;
            }

            var answerBuilder = new StringBuilder();

            foreach (var answerText in answer.SelectedAnswer)
            {
                if (string.IsNullOrWhiteSpace(answerText))
                {
                    continue;
                }

                answerBuilder.AppendLine(answerText);
            }

            var result = answerBuilder.ToString();
            if (string.IsNullOrWhiteSpace(result))
            {
                return null;
            }

            return result;
        }
    }
}
