﻿using Smt.Atomic.Business.Survey.Dto;

namespace Smt.Atomic.Business.Survey.Services.AnswerProviders
{
    public interface IAnswerProvider
    {
        string BuildAnswer(SurveyItemDto answer);
    }
}