﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.Business.Survey.Exceptions;
using Smt.Atomic.Business.Survey.Extensions;
using Smt.Atomic.Business.Survey.Interfaces;
using Smt.Atomic.Business.Survey.Resources;
using Smt.Atomic.Business.Survey.Services.AnswerProviders;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Survey;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Survey.Services
{
    public class SurveyCardIndexDataService : CardIndexDataService<SurveyDto, Data.Entities.Modules.Survey.Survey, ISurveyDbScope>, ISurveyCardIndexDataService
    {
        private readonly IAnswerProviderFactory _answerProviderFactory;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IMessageTemplateService _messageTemplateService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public SurveyCardIndexDataService(
            ICardIndexServiceDependencies<ISurveyDbScope> dependencies, 
            IAnswerProviderFactory answerProviderFactory,
            IReliableEmailService reliableEmailService,
            ISystemParameterService systemParameterService,
            IMessageTemplateService messageTemplateService)
            : base(dependencies)
        {
            _answerProviderFactory = answerProviderFactory;
            _reliableEmailService = reliableEmailService;
            _systemParameterService = systemParameterService;
            _messageTemplateService = messageTemplateService;
        }

        private static SurveyDto CreateSurvey(Data.Entities.Modules.Survey.Survey survey, IQueryable<SurveyConductQuestion> questions)
        {
            var surveyDto = new SurveyDto();
            surveyDto.Id = survey.Id;
            surveyDto.Name = survey.SurveyConduct.SurveyName;
            surveyDto.Description = survey.SurveyConduct.SurveyDescription;
            surveyDto.WelcomeInstruction = survey.SurveyConduct.SurveyWelcomeInstruction;
            surveyDto.ThankYouMessage = survey.SurveyConduct.SurveyThankYouMessage;
            surveyDto.IsCompleted = survey.IsCompleted;
            surveyDto.RespondentId = survey.RespondentId;
            surveyDto.IsSurveyClosed = survey.IsClosed();
            surveyDto.ShouldAutoClose = survey.SurveyConduct.ShouldAutoClose;
            surveyDto.RespondentFirstName = survey.Respondent.FirstName;
            surveyDto.RespondentLastName = survey.Respondent.LastName;
            surveyDto.IsConfirmationSent = survey.IsConfirmationSent;

            var answers = new List<QuestionDto>();

            foreach (var question in questions)
            {
                var answer = survey.Answers.Single(a => a.QuestionId == question.Id).Answer;

                answers.Add(new QuestionDto
                {
                    Id = question.Id,
                    IsRequired = question.IsRequired,
                    FieldName = question.FieldName,
                    FieldSize = question.FieldSize,
                    QuestionText = question.Text,
                    Type = question.Type,
                    Order = question.Order,
                    QuestionItems = new QuestionItemsDto(question.Items),
                    QuestionDescription = question.Description,
                    VisibilityCondition = question.VisibilityCondition,
                    Answer = answer
                });
            }
            surveyDto.Questions = answers;

            return surveyDto;
        }

        protected override IEnumerable<Expression<Func<Data.Entities.Modules.Survey.Survey, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return s => s.Respondent.FirstName;
            yield return s => s.Respondent.LastName;
        }

        public override IQueryable<Data.Entities.Modules.Survey.Survey> ApplyContextFiltering(IQueryable<Data.Entities.Modules.Survey.Survey> records, object context)
        {
            var surveyItemsContext = context as ParentIdContext;

            if (surveyItemsContext != null)
            {
                return records.Where(r => r.SurveyConductId == surveyItemsContext.ParentId);
            }

            return records;
        }

        public IEnumerable<SurveyDto> GetSurveys(long[] ids)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var surveys = unitOfWork.Repositories.Surveys
                    .Include(s => s.Answers)
                    .Include(s => s.Respondent)
                    .Include(s => s.SurveyConduct)
                    .GetByIds(ids);

                var questionIds = surveys.SelectMany(s => s.Answers).Select(a => a.QuestionId).Distinct();
                var questions = unitOfWork.Repositories.SurveyConductQuestions.GetByIds(questionIds);

                foreach (var survey in surveys)
                {
                    yield return CreateSurvey(survey, questions);
                }
            }
        }

        public IEnumerable<SurveyDto> GetSurveysForConduct(long surveyConductId)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var surveys = unitOfWork.Repositories.Surveys.Filtered()
                    .Include(s => s.Answers)
                    .Include(s => s.SurveyConduct)
                    .Where(s => s.SurveyConductId == surveyConductId);

                var questionIds = surveys.SelectMany(s => s.Answers).Select(a => a.QuestionId).Distinct();
                var questions = unitOfWork.Repositories.SurveyConductQuestions.GetByIds(questionIds);

                foreach (var survey in surveys)
                {
                    yield return CreateSurvey(survey, questions);
                }
            }
        }

        public SurveyDto GetSurvey(long surveyId)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var survey = unitOfWork.Repositories.Surveys
                    .Include(s => s.Answers)
                    .Include(s => s.SurveyConduct)
                    .GetById(surveyId);
                var questionIds = survey.Answers.Select(a => a.QuestionId);
                var questions = unitOfWork.Repositories.SurveyConductQuestions.GetByIds(questionIds);

                SurveyDto surveyDto = CreateSurvey(survey, questions);

                return surveyDto;
            }
        }

        public long GetSurveyForUser(long surveyConductId)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var userId = PrincipalProvider.Current.Id;

                var survey = unitOfWork.Repositories.Surveys
                    .SingleOrDefault(s => s.SurveyConductId == surveyConductId && s.RespondentId == userId);

                if (survey == null)
                {
                    throw new SurveyNotFoundException(SurveyResources.SurveyNotFound_ErrorMessage);
                }

                return survey.Id;
            }
        }

        public SurveyDto ParseAnswers(SurveyAnswersDto surveyAnswers)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var survey = unitOfWork.Repositories.Surveys
                    .Include(s => s.Answers)
                    .Include(s => s.SurveyConduct)
                    .GetById(surveyAnswers.SurveyId);
                var questionIds = survey.Answers.Select(a => a.QuestionId);
                var questions = unitOfWork.Repositories.SurveyConductQuestions.GetByIds(questionIds);

                SurveyDto surveyDto = CreateSurvey(survey, questions);

                foreach (var surveyItem in surveyAnswers.Items)
                {
                    var surveyQuestion = questions.SingleOrDefault(q => q.Id == surveyItem.QuestionId);
                    var question = surveyDto.Questions.SingleOrDefault(a => a.Id == surveyItem.QuestionId);

                    if (surveyQuestion == null)
                    {
                        continue;
                    }

                    var answerProvider = _answerProviderFactory.GetAnswerProvider(surveyQuestion);
                    var answer = answerProvider.BuildAnswer(surveyItem);

                    question.Answer = answer;
                }

                return surveyDto;
            }
        }

        public void FillSurvey(SurveyDto surveyDto, bool shouldFinishSurvey = false)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var survey = unitOfWork.Repositories.Surveys
                    .Include(s => s.Answers)
                    .Include(s => s.SurveyConduct)
                    .GetById(surveyDto.Id);

                if (survey.RespondentId != PrincipalProvider.Current.Id)
                {
                    throw new SurveyNotFoundException(SurveyResources.SurveyNotFound_ErrorMessage);
                }

                if (survey.IsClosed())
                {
                    throw new SurveyClosedException();
                }

                foreach (var question in surveyDto.Questions)
                {
                    var surveyAnswer = survey.Answers.SingleOrDefault(a => a.QuestionId == question.Id);

                    if (surveyAnswer == null)
                    {
                        continue;
                    }

                    if (question.IsRequired && string.IsNullOrWhiteSpace(question.VisibilityCondition) && string.IsNullOrWhiteSpace(question.Answer))
                    {
                        throw new SurveyValidationException(string.Format(SurveyResources.AnswerForQuestionIsRequired_ErrorMessageFormat, question.Id));
                    }
                    surveyAnswer.Answer = question.Answer;
                }

                if (shouldFinishSurvey)
                {
                    survey.IsCompleted = true;
                }

                survey.LastEditedAt = TimeService.GetCurrentTime();

                unitOfWork.Commit();
            }
        }

        public void SendServeyConfirmationAfterSubmit(long surveyId, string surveyAnswersUrl, string continueSurveyUrl = null)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var survey = unitOfWork.Repositories.Surveys.Include(s => s.Respondent).Include(s => s.SurveyConduct).GetById(surveyId);

                var emailRecipientDto = new EmailRecipientDto
                {
                    FullName = $"{survey.Respondent.FirstName} {survey.Respondent.LastName}",
                    EmailAddress = survey.Respondent.Email
                };

                var confirmationDto = new ServeyConfirmationAfterSubmitDto
                {
                    SurveyName = survey.SurveyConduct.SurveyName,
                    SurveyAnswersUrl = surveyAnswersUrl,
                    ContinueSurveyUrl = continueSurveyUrl,
                    PollsterFullName = survey.SurveyConduct.PollsterFullName,
                    PollsterEmailAddress = survey.SurveyConduct.OpenedBy.Email
                };

                var emailSubject = _messageTemplateService.ResolveTemplate(TemplateCodes.SurveyConfirmationAfterSubmitSubject, confirmationDto);
                var emailBody = _messageTemplateService.ResolveTemplate(TemplateCodes.SurveyConfirmationAfterSubmitBody, confirmationDto);
                var fromAddress = new EmailRecipientDto
                {
                    EmailAddress = _systemParameterService.GetParameter<string>(ParameterKeys.SmtpEmailServiceDefaultFromEmailAddress),
                    FullName = _systemParameterService.GetParameter<string>(ParameterKeys.SmtpEmailServiceDefaultFromFullName)
                };

                var emailMessageDto = new EmailMessageDto
                {
                    IsHtml = emailBody.IsHtml,
                    Subject = emailSubject.Content,
                    Body = emailBody.Content,
                    From = fromAddress,
                    Recipients = new List<EmailRecipientDto> { emailRecipientDto },
                    ReplyTo = new EmailRecipientDto { FullName = confirmationDto.PollsterFullName, EmailAddress = confirmationDto.PollsterEmailAddress }
                };

                _reliableEmailService.EnqueueMessage(emailMessageDto);

                survey.IsConfirmationSent = true;

                unitOfWork.Commit();
            }
        }
    }
}