﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Accounts.DocumentMappings;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.Business.Survey.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Survey;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Survey.Services
{
    public class WhistleBlowingCardIndexDataService : CardIndexDataService<WhistleBlowingDto, WhistleBlowing, ISurveyDbScope>, IWhistleBlowingCardIndexDataService
    {
        private readonly IUploadedDocumentHandlingService _uploadedDocumentHandlingService;
        private readonly ITimeService _timeService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public WhistleBlowingCardIndexDataService(
            IUploadedDocumentHandlingService uploadedDocumentHandlingService,
            ICardIndexServiceDependencies<ISurveyDbScope> dependencies,
            ITimeService timeService)
            : base(dependencies)
        {
            _uploadedDocumentHandlingService = uploadedDocumentHandlingService;
            _timeService = timeService;
        }

        protected override IEnumerable<Expression<Func<WhistleBlowing, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return r => r.Title;
            yield return r => r.Message;
            yield return r => r.ModifiedBy.FirstName;
            yield return r => r.ModifiedBy.LastName;
        }

        protected override IOrderedQueryable<WhistleBlowing> GetDefaultOrderBy(IQueryable<WhistleBlowing> records, QueryCriteria criteria)
        {
            return records.OrderBy(r => r.CreatedOn).ThenByDescending(r => r.Id);
        }

        protected override NamedFilters<WhistleBlowing> NamedFilters
        {
            get
            {
                return new NamedFilters<WhistleBlowing>(CardIndexServiceHelper.BuildEnumBasedFilters<WhistleBlowing, WhistleBlowingMessageStatus>(
                        r => r.MessageStatus));
            }
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<WhistleBlowing, WhistleBlowingDto, ISurveyDbScope> eventArgs)
        {
            _uploadedDocumentHandlingService.MergeDocumentCollections(eventArgs.UnitOfWork, eventArgs.InputEntity.Documents, eventArgs.InputDto.Documents,
                p => new WhistleBlowingDocument
                {
                    Name = p.DocumentName,
                    ContentType = p.ContentType
                });
        }

        public override WhistleBlowingDto GetDefaultNewRecord()
        {
            var newRecord = base.GetDefaultNewRecord();
            newRecord.CreatedOn = _timeService.GetCurrentTime();

            return newRecord;
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<WhistleBlowing, WhistleBlowingDto> recordAddedEventArgs)
        {
            _uploadedDocumentHandlingService.PromoteTemporaryDocuments<WhistleBlowingDocument, WhistleBlowingDocumentContent, WhistleBlowingDocumentMapping, ISurveyDbScope>(
                recordAddedEventArgs.InputDto.Documents);
        }

        public BoolResult ToggleSetIsRead(bool value, ICollection<long> ids)
        {
            if (ids == null)
            {
                return new BoolResult(true);
            }

            try
            {
                using (var unitOfWork = UnitOfWorkService.Create())
                {
                    var whistleBlowings = unitOfWork.Repositories.WhistleBlowings.Where(w => ids.Contains(w.Id));

                    foreach (var whistleBlowing in whistleBlowings)
                    {
                        whistleBlowing.MessageStatus = WhistleBlowingMessageStatus.Read;
                    }

                    unitOfWork.Commit();
                }
            }
            catch (DbUpdateException exc)
            {
                return new BoolResult(false, new[]
                {
                    new AlertDto
                    {
                        Message = exc.Message,
                        Type = AlertType.Error
                    }
                });
            }

            return new BoolResult(true);
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<WhistleBlowing> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var direction = sortingCriterion.GetSortingDirection();

            if (sortingCriterion.GetSortingColumnName() == nameof(WhistleBlowingDto.ModifiedByUserFullName))
            {                
                orderedQueryBuilder.ApplySortingKey(r => r.ModifiedBy.LastName + " " + r.ModifiedBy.FirstName, direction);
            }
            else
            {
                base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
            }
        }
    }
}
