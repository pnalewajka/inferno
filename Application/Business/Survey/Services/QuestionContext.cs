﻿namespace Smt.Atomic.Business.Survey.Services
{
    public class QuestionContext
    {
        public long QuestionId { get; set; }

        public bool IsQuestionPreview { get; set; }
    }
}
