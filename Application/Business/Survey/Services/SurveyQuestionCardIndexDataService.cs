﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.Business.Survey.Interfaces;
using Smt.Atomic.Business.Survey.Resources;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Survey;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Survey.Services
{
    public class SurveyQuestionCardIndexDataService : CardIndexDataService<SurveyQuestionDto, SurveyQuestion, ISurveyDbScope>, ISurveyQuestionCardIndexDataService
    {
        private readonly IUnitOfWorkService<ISurveyDbScope> _surveyQuestionUnitOfWork;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public SurveyQuestionCardIndexDataService(IUnitOfWorkService<ISurveyDbScope> surveyQuestionUnitOfWork,
            ICardIndexServiceDependencies<ISurveyDbScope> dependencies)
            : base(dependencies)
        {
            _surveyQuestionUnitOfWork = surveyQuestionUnitOfWork;
        }

        private long GetNextQuestionOrder(IUnitOfWork<ISurveyDbScope> unitOfWork, long surveyDefinitionId)
        {
            var surveyDefinition = unitOfWork.Repositories.SurveyDefinitions.GetById(surveyDefinitionId);
            if (surveyDefinition.SurveyQuestions.Count > 0)
            {
                return surveyDefinition.SurveyQuestions.Max(s => s.Order) + 1;
            }
            else
            {
                return 1;
            }
        }

        private void RenumerateAfterDelete(IUnitOfWork<ISurveyDbScope> unitOfWork, SurveyQuestion deletedQuestion)
        {
            var surveyQuestions = unitOfWork.Repositories.SurveyQuestions.Where(
                x =>
                    x.SurveyDefinitionId == deletedQuestion.SurveyDefinitionId &&
                    x.Order > deletedQuestion.Order
                    );

            foreach (var question in surveyQuestions)
            {
                question.Order--;
            }
        }

        private bool CanMoveQuestionUp(IUnitOfWork<ISurveyDbScope> unitOfWork, SurveyQuestion questionToMove)
        {
            return questionToMove.Order > 1;
        }

        private bool CanMoveQuestionDown(IUnitOfWork<ISurveyDbScope> unitOfWork, SurveyQuestion questionToMove)
        {
            var maxOrder = unitOfWork.Repositories.SurveyQuestions
                .Where(x => x.SurveyDefinitionId == questionToMove.SurveyDefinitionId)
                .Max(x => x.Order);

            return questionToMove.Order < maxOrder;
        }

        protected override IOrderedQueryable<SurveyQuestion> GetDefaultOrderBy(IQueryable<SurveyQuestion> records, QueryCriteria criteria)
        {
            return records.OrderBy(x => x.Order);
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<SurveyQuestion, SurveyQuestionDto, ISurveyDbScope> eventArgs)
        {
            var surveyQuestion = eventArgs.InputEntity;
            eventArgs.InputEntity.Order = GetNextQuestionOrder(eventArgs.UnitOfWork, surveyQuestion.SurveyDefinitionId);

            var surveyQuestionDto = eventArgs.InputDto;

            if (eventArgs.UnitOfWork.Repositories.SurveyQuestions
                .Any(q => q.SurveyDefinitionId == surveyQuestionDto.SurveyDefinitionId && q.FieldName == surveyQuestionDto.FieldName))
            {
                eventArgs.Alerts.Add(new AlertDto
                {
                    Message = SurveyQuestionResources.FieldNameNotUnique,
                    Type = AlertType.Error
                });    
            }
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<SurveyQuestion, ISurveyDbScope> eventArgs)
        {
            var surveyQuestion = eventArgs.DeletingRecord;
            RenumerateAfterDelete(eventArgs.UnitOfWork, eventArgs.DeletingRecord);
        }

        public override IQueryable<SurveyQuestion> ApplyContextFiltering(IQueryable<SurveyQuestion> records, object context)
        {
            var surveyDefinitionItemsContext = context as ParentIdContext;

            if (surveyDefinitionItemsContext != null)
            {
                return records.Where(r => r.SurveyDefinitionId == surveyDefinitionItemsContext.ParentId);
            }

            return records;
        }

        public void MoveQuestionUpOrder(long surveyQuestionId)
        {
            using (var unitOfWork = _surveyQuestionUnitOfWork.Create())
            {
                var surveyQuestionToMoveUp = unitOfWork.Repositories.SurveyQuestions.GetById(surveyQuestionId);

                if (!CanMoveQuestionUp(unitOfWork, surveyQuestionToMoveUp))
                    return;

                var surveyQuestionToMoveDown = unitOfWork.Repositories.SurveyQuestions.Single(
                    x =>
                        x.SurveyDefinitionId == surveyQuestionToMoveUp.SurveyDefinitionId &&
                        x.Order == surveyQuestionToMoveUp.Order - 1
                        );

                surveyQuestionToMoveUp.Order--;
                surveyQuestionToMoveDown.Order++;

                unitOfWork.Commit();
            }
        }

        public void MoveQuestionDownOrder(long surveyQuestionId)
        {
            using (var unitOfWork = _surveyQuestionUnitOfWork.Create())
            {
                var surveyQuestionToMoveDown = unitOfWork.Repositories.SurveyQuestions.GetById(surveyQuestionId);

                if (!CanMoveQuestionDown(unitOfWork, surveyQuestionToMoveDown))
                    return;

                var surveyQuestionToMoveUp = unitOfWork.Repositories.SurveyQuestions.Single(
                    x =>
                        x.SurveyDefinitionId == surveyQuestionToMoveDown.SurveyDefinitionId &&
                        x.Order == surveyQuestionToMoveDown.Order + 1
                        );

                surveyQuestionToMoveUp.Order--;
                surveyQuestionToMoveDown.Order++;

                unitOfWork.Commit();
            }
        }
    }
}
