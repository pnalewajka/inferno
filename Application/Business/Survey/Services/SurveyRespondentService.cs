﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Survey.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Srv = Smt.Atomic.Data.Entities.Modules.Survey;

namespace Smt.Atomic.Business.Survey.Services
{
    public class SurveyRespondentService : ISurveyRespondentService
    {
        private readonly IUnitOfWorkService<ISurveyDbScope> _unitOfWorkSurveyService;
        private readonly IReadOnlyUnitOfWorkService<IAllocationDbScope> _unitOfWorkAllocationService;

        public SurveyRespondentService(IUnitOfWorkService<ISurveyDbScope> unitOfWorkSurveyService, IReadOnlyUnitOfWorkService<IAllocationDbScope> unitOfWorkAllocationService)
        {
            _unitOfWorkSurveyService = unitOfWorkSurveyService;
            _unitOfWorkAllocationService = unitOfWorkAllocationService;
        }

        public bool AddOrgUnitSurveysForEmployeeId(long employeeId)
        {
            using (var unitOfWorkAllocation = _unitOfWorkAllocationService.Create())
            {
                var employee = unitOfWorkAllocation.Repositories.Employees.FirstOrDefault(e => e.Id == employeeId);

                if (employee != null)
                {
                    return AddOrgUnitSurveysForEmployee(employee);
                }

                return false;
            }
        }

        public bool AddOrgUnitSurveysForEmployee(Employee employee)
        {
            bool surveysAdded = false;
            if (!employee.UserId.HasValue)
            {
                return false;
            }

            using (var unitOfWork = _unitOfWorkSurveyService.Create())
            {
                var mainOrgUnit = unitOfWork.Repositories.OrgUnits.FirstOrDefault(o => o.Id == employee.OrgUnitId);

                if (string.IsNullOrEmpty(mainOrgUnit?.Path))
                {
                    return false;
                }

                foreach (var surveyConduct in unitOfWork.Repositories.SurveyConducts)
                {
                    bool haveCommonOrgUnits = 
                         surveyConduct.OrgUnits.Select(o => o.Id)
                        .Intersect(unitOfWork.Repositories.OrgUnits.Where(o => mainOrgUnit.Path.StartsWith(o.Path)).Select(o => o.Id))
                        .Any();

                    if (haveCommonOrgUnits)
                    {
                        bool surveyExists = unitOfWork.Repositories.Surveys.Any(s => s.RespondentId == employee.UserId && s.SurveyConductId == surveyConduct.Id);

                        if (!surveyExists)
                        {
                            var survey = CreateSurvey(employee, surveyConduct);
                            surveyConduct.Surveys.Add(survey);
                            surveysAdded = true;
                        }
                    }
                }

                unitOfWork.Commit();
            }
            return surveysAdded;
        }

        public bool AddRespondentsToSurveyConduct(long surveyConductId, IList<long> respondentIds)
        {
            bool respondentsAdded = false; 

            using (var unitOfWork = _unitOfWorkSurveyService.Create())
            {
                var surveyConduct = 
                    unitOfWork.Repositories.SurveyConducts.FirstOrDefault(s => s.Id == surveyConductId);  
                          
                var newRespondents = respondentIds.Where(
                    respondentId => !unitOfWork.Repositories.Surveys.Any(s => s.RespondentId == respondentId && s.SurveyConductId == surveyConduct.Id )).ToList();

                var newEmployeesRespondents = 
                    unitOfWork.Repositories.Employees.Where(e => e.UserId.HasValue && newRespondents.Contains(e.UserId.Value)).ToList();

                if (surveyConduct != null && newEmployeesRespondents.Any())
                {
                    foreach (var employeeRespondent in newEmployeesRespondents)
                    {
                        var survey = CreateSurvey(employeeRespondent, surveyConduct);
                        surveyConduct.Surveys.Add(survey);
                        surveyConduct.Employees.Add(employeeRespondent);
                        respondentsAdded = true; 
                    }

                    unitOfWork.Commit(); 
                }
            }
            return respondentsAdded;
        }

        private Srv.Survey CreateSurvey(Employee employee, Srv.SurveyConduct surveyConduct)
        {
            return new Srv.Survey
            {
                RespondentId = employee.UserId.Value,
                SurveyConduct = surveyConduct,
                Answers = surveyConduct.Questions.Select(
                    question =>
                        new Srv.SurveyAnswer
                        {
                            Questions = question,
                            Answer = question.DefaultAnswer
                        })
               .ToArray()
            };
        }
    }
}
