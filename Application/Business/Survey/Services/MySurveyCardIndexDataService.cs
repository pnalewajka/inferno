﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.Business.Survey.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Business.Survey.Helpers;

namespace Smt.Atomic.Business.Survey.Services
{
    public class MySurveyCardIndexDataService : ReadOnlyCardIndexDataService<MySurveyDto, Data.Entities.Modules.Survey.Survey, ISurveyDbScope>, IMySurveyCardIndexDataService
    {
        public MySurveyCardIndexDataService(ICardIndexServiceDependencies<ISurveyDbScope> dependencies)
            : base(dependencies)
        {
        }

        public override IQueryable<Data.Entities.Modules.Survey.Survey> ApplyContextFiltering(IQueryable<Data.Entities.Modules.Survey.Survey> records, object context)
        {
            var userId = PrincipalProvider.Current.Id;

            return records.Where(r => r.RespondentId == userId);
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<Data.Entities.Modules.Survey.Survey> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var direction = sortingCriterion.GetSortingDirection();

            if (sortingCriterion.GetSortingColumnName() == "IsClosed")
            {
                MySurveySortHelper.OrderByIsClosed(orderedQueryBuilder, direction);
            }
            else
            {
                base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
            }
        }
    }
}
