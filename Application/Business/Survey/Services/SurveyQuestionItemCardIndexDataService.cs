﻿using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.Business.Survey.Interfaces;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.Survey.Services
{
    public class SurveyQuestionItemCardIndexDataService : CustomDataCardIndexDataService<QuestionItemDto>, ISurveyQuestionItemCardIndexDataService
    {
        private IUnitOfWorkService<ISurveyDbScope> _unitOfWorkService;

        public SurveyQuestionItemCardIndexDataService(IUnitOfWorkService<ISurveyDbScope> unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        public override QuestionItemDto GetRecordById(long id)
        {
            throw new NotImplementedException();
        }

        public override QuestionItemDto GetRecordByIdOrDefault(long id)
        {
            throw new NotImplementedException();
        }

        protected override IList<QuestionItemDto> GetAllRecords(object context = null)
        {
            var questionContext = context as QuestionContext;

            if (questionContext == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var questionItems = questionContext.IsQuestionPreview ?
                      unitOfWork.Repositories.SurveyQuestions.GetById(questionContext.QuestionId).Items
                    : unitOfWork.Repositories.SurveyConductQuestions.GetById(questionContext.QuestionId).Items;
                var questionItemsDto = new QuestionItemsDto(questionItems);
                var items = new List<QuestionItemDto>();

                for (int i = 0; i < questionItemsDto.Count; i++)
                {
                    items.Add(new QuestionItemDto { Id = i, ItemText = questionItemsDto[i] });
                }

                return items;
            }
        }

        protected override IEnumerable<Expression<Func<QuestionItemDto, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            return new List<Expression<Func<QuestionItemDto, object>>>
            {
                c => c.ItemText
            };
        }
    }
}
