﻿using ClosedXML.Excel;
using CsvHelper;
using CsvHelper.Excel;
using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.Business.Survey.Interfaces;
using Smt.Atomic.Business.Survey.Resources;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web;
using System.Linq;
using System.Threading;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Business.Survey.Models;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.Business.Survey.Services
{
    public class SurveyExportService : ISurveyExportService
    {
        private const string ExcelExtension = ".xlsx";
        private const string blStreamCode = "BlStream";

        private readonly IUnitOfWorkService<IAllocationDbScope> _allocationUnitOfWorkService;

        public SurveyExportService(IUnitOfWorkService<IAllocationDbScope> allocationUnitOfWorkService)
        {
            _allocationUnitOfWorkService = allocationUnitOfWorkService;
        }

        private static void ApplyFormat(string tempFileName)
        {
            var xlWorkbook = new XLWorkbook(tempFileName);
            var xlWorksheet = xlWorkbook.Worksheet(1);

            if (xlWorksheet.IsEmpty())
                return;

            AdjustColumnsWidth(xlWorksheet);
            ApplyBoldOnHeaderRow(xlWorksheet);
            ApplyBoldOnNameColumns(xlWorksheet);
            ApplyAutoFilterOnHeaderRow(xlWorksheet);

            using (CultureInfoHelper.SetCurrentCulture(CultureInfo.CreateSpecificCulture(CultureCodes.EnglishAmerican)))
            {
                xlWorkbook.Save();
            }
        }

        private static void ApplyBoldOnHeaderRow(IXLWorksheet xlWorksheet)
        {
            xlWorksheet.FirstRow().CellsUsed().ForEach(cell => cell.Style.Font.Bold = true);
        }

        private static void ApplyAutoFilterOnHeaderRow(IXLWorksheet xlWorksheet)
        {
            xlWorksheet.Columns().ForEach(column => column.SetAutoFilter());
        }

        private static void ApplyBoldOnNameColumns(IXLWorksheet xlWorksheet)
        {
            xlWorksheet.Columns(1,2).CellsUsed().ForEach(cell => cell.Style.Font.Bold = true);
        }

        private static void AdjustColumnsWidth(IXLWorksheet xlWorksheet)
        {
            xlWorksheet.ColumnsUsed().ForEach(column =>
            {
                column.AdjustToContents();

                // Don't remove this. Throws exception if Width > 250
                if (column.Width > 250)
                {
                    column.Width = 250;
                }
            });
        }

        private static void WriteRespondentAnswer(CsvWriter csvWriter, QuestionDto question)
        {
            if (question.Type == SurveyQuestionType.Checkbox || question.Type == SurveyQuestionType.Picker)
            {
                WriteRespondentMultiAnswer(csvWriter, question);
            }
            else
            {
                csvWriter.WriteField(RenderCellText(question.Answer));
            }
        }

        private static void WriteRespondentMultiAnswer(CsvWriter csvWriter, QuestionDto question)
        {
            var answers = question.Answer?.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries) ?? new string[] { };

            foreach (var item in question.QuestionItems.Items)
            {
                csvWriter.WriteField(answers.Contains(item));
            }
        }

        private static void WriteRespondentData(CsvWriter csvWriter, SurveyDto survey, RespondentDataModel respondent)
        {
            csvWriter.WriteField(RenderCellText(survey.RespondentFirstName));
            csvWriter.WriteField(RenderCellText(survey.RespondentLastName));
            csvWriter.WriteField(RenderCellText(respondent.Login));
            csvWriter.WriteField(RenderCellText(respondent.Company));
            csvWriter.WriteField(RenderCellText(respondent.BusinessUnit));
            csvWriter.WriteField(RenderCellText(respondent.Location));
        }

        private static void WriteHeaderRow(CsvWriter csvWriter, SurveyDto row)
        {
            csvWriter.WriteField(SurveyResources.RespondentFirstName);
            csvWriter.WriteField(SurveyResources.RespondentLastName);
            csvWriter.WriteField(SurveyResources.Login);
            csvWriter.WriteField(SurveyResources.Company);
            csvWriter.WriteField(SurveyResources.BusinessUnit);
            csvWriter.WriteField(SurveyResources.Location);

            foreach (var question in row.Questions)
            {
                if (question.Type == SurveyQuestionType.Checkbox || question.Type == SurveyQuestionType.Picker)
                {
                    foreach (var item in question.QuestionItems.Items)
                    {
                        csvWriter.WriteField(RenderCellText($"{question.QuestionText} ({item})"));
                    }
                }
                else
                {
                    csvWriter.WriteField(RenderCellText(question.QuestionText));
                }
            }
            csvWriter.NextRecord();
        }

        private static string RenderCellText(string text)
        {
            text = text?.Replace(Environment.NewLine, ",");
            text = text?.Trim(',');
            return HttpUtility.HtmlDecode(text); // decode for excel and csv
        }

        private void Export(IEnumerable<SurveyDto> surveys, CsvWriter csvWriter)
        {
            IEnumerable<RespondentDataModel> respondentsData;

            using (var unitOfWork = _allocationUnitOfWorkService.Create())
            {
                var userIds = surveys.Select(survey => survey.RespondentId).ToList();

                respondentsData = 
                     unitOfWork.Repositories.Employees
                    .Where(employee => employee.User.IsActive && userIds.Contains(employee.UserId.Value))
                    .Select(employee =>
                        new RespondentDataModel
                        {
                            UserId = employee.UserId.Value,
                            Login = employee.User.Login,
                            Company = employee.Company.Name,
                            BusinessUnit = employee.OrgUnit.Name,
                            Location = employee.Location.Name
                        })
                    .ToList(); 
            }

            WriteHeaderRow(csvWriter, surveys.First());

            foreach (var survey in surveys)
            {
                var respondentData = respondentsData.FirstOrDefault(r => r.UserId == survey.RespondentId);

                if (respondentData == null) // user is not active => don't export data
                    continue;

                WriteRespondentData(csvWriter, survey, respondentData);

                foreach (var answer in survey.Questions)
                {
                    WriteRespondentAnswer(csvWriter, answer);
                }

                csvWriter.NextRecord();
            }
        }

        public string ExportToExcel(IEnumerable<SurveyDto> surveys)
        {
            var tempFileName = Path.ChangeExtension(Path.GetTempFileName(), ExcelExtension);

            using (var excelSerializer = new ExcelSerializer(tempFileName))
            {
                using (var csvWriter = new CsvWriter(excelSerializer))
                {
                    csvWriter.Configuration.CultureInfo = CultureInfo.GetCultureInfo(CultureCodes.EnglishAmerican);
                    Export(surveys, csvWriter);
                }
            }

            ApplyFormat(tempFileName);

            return tempFileName;
        }
    }
}