﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Survey.BusinessLogics;
using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.Business.Survey.Exceptions;
using Smt.Atomic.Business.Survey.Helpers;
using Smt.Atomic.Business.Survey.Interfaces;
using Smt.Atomic.Business.Survey.Resources;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Survey;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Survey.Services
{
    public class SurveyConductCardIndexDataService
        : CardIndexDataService<SurveyConductDto, SurveyConduct, ISurveyDbScope>, ISurveyConductCardIndexDataService
    {
        private readonly IReliableEmailService _reliableEmailService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IPrincipalProvider _principalProvider;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public SurveyConductCardIndexDataService(
            ICardIndexServiceDependencies<ISurveyDbScope> dependencies,
            IReliableEmailService reliableEmailService,
            ISystemParameterService systemParameterService,
            IMessageTemplateService messageTemplateService,
            IPrincipalProvider principalProvider)
            : base(dependencies)
        {
            _reliableEmailService = reliableEmailService;
            _systemParameterService = systemParameterService;
            _messageTemplateService = messageTemplateService;
            _principalProvider = principalProvider;
        }

        private void CreateSurveyFor(User user, SurveyConduct surveyConduct)
        {
            var survey = new Data.Entities.Modules.Survey.Survey()
            {
                Respondent = user,
                SurveyConduct = surveyConduct,
                Answers = surveyConduct.Questions.Select(
                    question =>
                        new SurveyAnswer()
                        {
                            Questions = question,
                            Answer = question.DefaultAnswer
                        })
                .ToArray()
            };

            surveyConduct.Surveys.Add(survey);
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<SurveyConduct, SurveyConductDto, ISurveyDbScope> eventArgs)
        {
            var unitOfWork = eventArgs.UnitOfWork;
            var surveyConduct = eventArgs.InputEntity;
            var surveyConductDto = eventArgs.InputDto;
            var respondentsOrgUnitIds = eventArgs.InputDto.OrgUnitIds;
            var respondentsIds = eventArgs.InputDto.EmployeeIds;
            var respondentsAdGroups = eventArgs.InputDto.ActiveDirectoryGroupIds.ToList();

            var surveyDefintion = unitOfWork.Repositories.SurveyDefinitions
                .Include(d => d.SurveyQuestions)
                .GetById(surveyConductDto.SurveyDefinitionId.Value);

            var orgUnits = unitOfWork.Repositories.OrgUnits.GetByIds(respondentsOrgUnitIds).ToList();
            var orgUnitsPaths = orgUnits.Select(ou => ou.Path);
            var acriveDirectoryGroups = unitOfWork.Repositories.ActiveDirectoryGroups.GetByIds(eventArgs.InputDto.ActiveDirectoryGroupIds).ToList();
            var employees = unitOfWork.Repositories.Employees.Include(employee => employee.OrgUnit)
                .Where(
                employee =>
                    employee.UserId.HasValue && 
                    employee.User.IsActive &&
                    (
                        orgUnitsPaths.Any(orgUnitPath => employee.OrgUnit.Path.StartsWith(orgUnitPath)) ||
                        respondentsIds.Contains(employee.Id) ||
                        employee.User.ActiveDirectoryGroups.Any(x => respondentsAdGroups.Contains(x.Id))
                    )
                ).Distinct().ToList();

            surveyConduct.SurveyName = surveyDefintion.Name;
            surveyConduct.SurveyDescription = surveyDefintion.Description;
            surveyConduct.ShouldAutoClose = surveyDefintion.ShouldAutoClose;
            surveyConduct.SurveyWelcomeInstruction = surveyDefintion.WelcomeInstruction;
            surveyConduct.SurveyThankYouMessage = surveyDefintion.ThankYouMessage;
            surveyConduct.OrgUnits = orgUnits.ToArray();
            surveyConduct.ActiveDirectoryGroups = acriveDirectoryGroups;
            surveyConduct.Employees = employees.Where(e => respondentsIds.Contains(e.Id)).ToArray();
            surveyConduct.OpenedOn = TimeService.GetCurrentTime();
            surveyConduct.OpenedById = PrincipalProvider.Current.Id;
            surveyConduct.Surveys = new HashSet<Data.Entities.Modules.Survey.Survey>();
            surveyConduct.Questions = new HashSet<SurveyConductQuestion>(
                surveyDefintion.SurveyQuestions.Select(q => new SurveyConductQuestion
                {
                    FieldName = q.FieldName,
                    FieldSize = q.FieldSize,
                    IsRequired = q.IsRequired,
                    Items = q.Items,
                    DefaultAnswer = q.DefaultAnswer,
                    Order = q.Order,
                    Text = q.Text,
                    Description = q.Description,
                    Type = q.Type,
                    VisibilityCondition = q.VisibilityCondition
                }).ToList());

            foreach (var employee in employees)
            {
                CreateSurveyFor(employee.User, surveyConduct);
            }
        }

        public void CloseSurveyConduct(long id)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var surveyConduct = unitOfWork.Repositories.SurveyConducts.Filtered().GetById(id);

                if (surveyConduct.IsClosed)
                {
                    return;
                }

                surveyConduct.IsClosed = true;
                unitOfWork.Commit();
            }
        }

        public void UrgeRespondents(long surveyConductId, string surveyUrl)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var surveyConduct = unitOfWork.Repositories.SurveyConducts.Filtered().Include(s => s.OpenedBy).GetById(surveyConductId);

                if (surveyConduct != null && surveyConduct.IsClosed)
                    throw new SurveyConductClosedException(SurveyConductResources.CannotUrgeRespondentsOfClosedSurveyConduct_ErrorMessage);

                var surveys = unitOfWork.Repositories.Surveys
                    .Include(s => s.Respondent)
                    .Where(s => !s.IsCompleted && s.SurveyConductId == surveyConductId)
                    .ToList();

                var respondentsToUrge = surveys.Select(survey => new EmailRecipientDto
                {
                    FullName = $"{survey.Respondent.FirstName} {survey.Respondent.LastName}",
                    EmailAddress = survey.Respondent.Email
                }).ToList();

                var reminderDto = new RespondentReminderDto
                {
                    SurveyName = surveyConduct.SurveyName,
                    SurveyUrl = surveyUrl,
                    PollsterFullName = surveyConduct.PollsterFullName,
                    PollsterEmail = surveyConduct.OpenedBy.Email
                };


                var emailSubject = _messageTemplateService.ResolveTemplate(TemplateCodes.SurveyRespondentReminderSubject, reminderDto);
                var emailBody = _messageTemplateService.ResolveTemplate(TemplateCodes.SurveyRespondentReminderBody, reminderDto);
                var fromAddress = new EmailRecipientDto
                {
                    EmailAddress = _systemParameterService.GetParameter<string>(ParameterKeys.SmtpEmailServiceDefaultFromEmailAddress),
                    FullName = _systemParameterService.GetParameter<string>(ParameterKeys.SmtpEmailServiceDefaultFromFullName)
                };

                var emails = respondentsToUrge.Select(respondent => new EmailMessageDto
                {
                    IsHtml = emailBody.IsHtml,
                    Subject = emailSubject.Content,
                    Body = emailBody.Content,
                    From = fromAddress,
                    Recipients = new List<EmailRecipientDto> { respondent },
                    ReplyTo = new EmailRecipientDto { FullName = reminderDto.PollsterFullName, EmailAddress = reminderDto.PollsterEmail }
                });

                foreach (var email in emails)
                {
                    _reliableEmailService.EnqueueMessage(email);
                }
            }
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<SurveyConduct> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var direction = sortingCriterion.GetSortingDirection();

            if (sortingCriterion.GetSortingColumnName() == nameof(SurveyConductDto.Pollster))
            {
                SurveyConductSortHelper.OrderByPollsterFullName(orderedQueryBuilder, direction);
            }
            else
            {
                base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
            }    
        }

        protected override NamedFilters<SurveyConduct> NamedFilters
        {
            get
            {
                long? currentUserId = _principalProvider.Current.Id;

                return new NamedFilters<SurveyConduct>(new[]
                {
                    new NamedFilter<SurveyConduct>(FilterCodes.MySurveys, SurveyConductBusinessLogic.IsCreator.Parametrize(currentUserId.Value)),
                    new NamedFilter<SurveyConduct>(FilterCodes.AllSurveys, _ => true, SecurityRoleType.CanViewAllSurveyConducts)
                });
            }
        }
    }
}