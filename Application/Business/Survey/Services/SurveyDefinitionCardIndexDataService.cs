﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.Business.Survey.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Survey;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Survey.Services
{
    public class SurveyDefinitionCardIndexDataService : CardIndexDataService<SurveyDefinitionDto, SurveyDefinition, ISurveyDbScope>, ISurveyDefinitionCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public SurveyDefinitionCardIndexDataService(ICardIndexServiceDependencies<ISurveyDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<SurveyDefinition, ISurveyDbScope> eventArgs)
        {
            var unitOfWork = eventArgs.UnitOfWork;
            var surveyDefinition = eventArgs.DeletingRecord;
            var surveyConductToUpdate = unitOfWork.Repositories.SurveyConducts.Where(s => s.SurveyDefinitionId == surveyDefinition.Id);

            foreach (var surveyConduct in surveyConductToUpdate)
            {
                surveyConduct.SurveyDefinitionId = null;
            }

            base.OnRecordDeleting(eventArgs);
        }

        public SurveyDto CreateSurveyPreview(long surveyDefinitionId)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var surveyDefinition = unitOfWork.Repositories.SurveyDefinitions.Include(s => s.SurveyQuestions).GetById(surveyDefinitionId);
                var surveyDto = new SurveyDto();
                surveyDto.Id = 0;
                surveyDto.Name = surveyDefinition.Name;
                surveyDto.Description = surveyDefinition.Description;
                surveyDto.IsCompleted = false;
                surveyDto.RespondentId = 0;
                surveyDto.IsSurveyClosed = false;
                surveyDto.WelcomeInstruction = surveyDefinition.WelcomeInstruction;
                surveyDto.ThankYouMessage = surveyDefinition.ThankYouMessage;

                var answers = new List<QuestionDto>();

                foreach (var question in surveyDefinition.SurveyQuestions)
                {
                    answers.Add(new QuestionDto
                    {
                        Id = question.Id,
                        IsRequired = question.IsRequired,
                        FieldName = question.FieldName,
                        FieldSize = question.FieldSize,
                        QuestionText = question.Text,
                        QuestionDescription = question.Description,
                        Type = question.Type,
                        Order = question.Order,
                        QuestionItems = new QuestionItemsDto(question.Items),
                        Answer = question.DefaultAnswer,
                        VisibilityCondition = question.VisibilityCondition,
                    });
                }
                surveyDto.Questions = answers;
                return surveyDto;
            }
        }
    }
}
