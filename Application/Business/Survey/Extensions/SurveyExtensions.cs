﻿using System;

namespace Smt.Atomic.Business.Survey.Extensions
{
    public static class SurveyExtensions
    {
        public static bool IsClosed(this Data.Entities.Modules.Survey.Survey survey)
        {
            if (survey.SurveyConduct == null)
            {
                throw new ArgumentNullException(nameof(survey.SurveyConduct));
            }

            return survey.SurveyConduct.IsClosed || survey.SurveyConduct.ShouldAutoClose && survey.IsCompleted;
        }
    }
}
