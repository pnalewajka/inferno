﻿using Smt.Atomic.Business.Common.Abstracts;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Data.Entities.Modules.Survey;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Accounts.DocumentMappings
{
    [Identifier("Documents.WhistleBlowingDocumentMapping")]
    public class WhistleBlowingDocumentMapping : DocumentMapping<WhistleBlowingDocument, WhistleBlowingDocumentContent, ISurveyDbScope>
    {
        public WhistleBlowingDocumentMapping(IUnitOfWorkService<ISurveyDbScope> unitOfWorkService) : base(unitOfWorkService)
        {
            ContentColumnExpression = o => o.Content;
            NameColumnExpression = o => o.Name;
            ContentTypeColumnExpression = o => o.ContentType;
            ContentLengthColumnExpression = o => o.ContentLength;
        }
    }
}
