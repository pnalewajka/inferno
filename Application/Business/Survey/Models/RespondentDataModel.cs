﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Survey.Models
{
    class RespondentDataModel
    {
        public long UserId { get; set; }

        public string Login { get; set; }

        public string Company { get; set; }

        public string BusinessUnit { get; set; }

        public string Location { get; set; }
    }
}
