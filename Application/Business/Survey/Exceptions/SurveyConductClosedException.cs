﻿using Smt.Atomic.CrossCutting.Common.Exceptions;

namespace Smt.Atomic.Business.Survey.Exceptions
{
    public class SurveyConductClosedException : BusinessException
    {
        public SurveyConductClosedException(string message) : base(message)
        {
        }
    }
}
