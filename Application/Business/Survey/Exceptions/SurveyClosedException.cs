﻿using Smt.Atomic.Business.Survey.Resources;
using Smt.Atomic.CrossCutting.Common.Exceptions;

namespace Smt.Atomic.Business.Survey.Exceptions
{
    public class SurveyClosedException : BusinessException
    {
        public SurveyClosedException() : base(SurveyResources.SurveyClosed_ErrorMessage)
        {
        }
    }
}
