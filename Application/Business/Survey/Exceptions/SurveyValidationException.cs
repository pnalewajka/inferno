﻿using Smt.Atomic.CrossCutting.Common.Exceptions;

namespace Smt.Atomic.Business.Survey.Exceptions
{
    public class SurveyValidationException : BusinessException
    {
        public SurveyValidationException(string message) : base(message)
        {
        }
    }
}
