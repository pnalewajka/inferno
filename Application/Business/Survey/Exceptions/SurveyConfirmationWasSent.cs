﻿using Smt.Atomic.CrossCutting.Common.Exceptions;

namespace Smt.Atomic.Business.Survey.Exceptions
{
    public class SurveyConfirmationWasSent : BusinessException
    {
        public SurveyConfirmationWasSent(string message) : base(message)
        {
        }
    }
}
