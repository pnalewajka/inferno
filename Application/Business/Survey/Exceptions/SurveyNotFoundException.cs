﻿using Smt.Atomic.CrossCutting.Common.Exceptions;

namespace Smt.Atomic.Business.Survey.Exceptions
{
    public class SurveyNotFoundException : BusinessException
    {
        public SurveyNotFoundException(string message) : base(message)
        {
        }
    }
}
