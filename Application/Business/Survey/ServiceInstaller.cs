﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Survey.ChoreProviders;
using Smt.Atomic.Business.Survey.Interfaces;
using Smt.Atomic.Business.Survey.Services;
using Smt.Atomic.Business.Survey.Services.AnswerProviders;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.Survey
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            if (containerType == ContainerType.WebApp)
            {
                container.Register(Component.For<IAnswerProviderFactory>().ImplementedBy<AnswerProviderFactory>().LifestyleTransient());
                container.Register(Component.For<ISurveyDefinitionCardIndexDataService>().ImplementedBy<SurveyDefinitionCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ISurveyQuestionCardIndexDataService>().ImplementedBy<SurveyQuestionCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ISurveyConductCardIndexDataService>().ImplementedBy<SurveyConductCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ISurveyCardIndexDataService>().ImplementedBy<SurveyCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IMySurveyCardIndexDataService>().ImplementedBy<MySurveyCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ISurveyQuestionItemCardIndexDataService>().ImplementedBy<SurveyQuestionItemCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ISurveyExportService>().ImplementedBy<SurveyExportService>().LifestyleTransient());
                container.Register(Component.For<ISurveyRespondentService>().ImplementedBy<SurveyRespondentService>().LifestyleTransient());
                container.Register(Component.For<IWhistleBlowingCardIndexDataService>().ImplementedBy<WhistleBlowingCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IChoreProvider>().ImplementedBy<PendingSurveysChoreProvider>().LifestylePerWebRequest());
            }

            if (containerType == ContainerType.JobScheduler)
            {
                container.Register(Component.For<ISurveyRespondentService>().ImplementedBy<SurveyRespondentService>().LifestyleTransient());
            }
        }
    }
}

