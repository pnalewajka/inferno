﻿using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Survey;

namespace Smt.Atomic.Business.Survey.BusinessLogics
{
    public static class SurveyConductBusinessLogic
    {
        public static readonly BusinessLogic<SurveyConduct, long, bool> IsCreator =
            new BusinessLogic<SurveyConduct, long, bool>((surveyConduct, userId) => surveyConduct.OpenedById == userId);
    }
}
