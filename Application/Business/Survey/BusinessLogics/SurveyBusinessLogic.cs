﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using SurveyEntity = Smt.Atomic.Data.Entities.Modules.Survey.Survey;

namespace Smt.Atomic.Business.Survey.BusinessLogics
{
    public static class SurveyBusinessLogic
    {
        public static readonly BusinessLogic<SurveyEntity, bool> IsClosed =
            new BusinessLogic<SurveyEntity, bool>(s => s.SurveyConduct.IsClosed || s.SurveyConduct.ShouldAutoClose && s.IsCompleted);
    }
}
