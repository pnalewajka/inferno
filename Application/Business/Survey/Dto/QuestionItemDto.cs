﻿namespace Smt.Atomic.Business.Survey.Dto
{
    public class QuestionItemDto
    {
        public long Id { get; set; }

        public string ItemText { get; set; }
    }
}
