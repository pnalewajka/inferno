﻿using System;
namespace Smt.Atomic.Business.Survey.Dto
{
    public class SurveyDefinitionDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool ShouldAutoClose { get; set; }
        
        public string WelcomeInstruction { get; set; }

        public string ThankYouMessage { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
