﻿using System;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Survey.Dto
{
    public class SurveyQuestionDto
    {
        public long Id { get; set; }

        public string Text { get; set; }

        public SurveyQuestionType Type { get; set; }

        public string Items { get; set; }

        public string DefaultAnswer { get; set; }

        public bool IsRequired { get; set; }

        public string Description { get; set; }

        public string VisibilityCondition { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public long SurveyDefinitionId { get; set; }

        public string FieldName { get; set; }

        public SurveyFieldSize FieldSize { get; set; }

        public long Order { get; set; }
    }
}
