﻿using System;
using System.Collections.Generic;

namespace Smt.Atomic.Business.Survey.Dto
{
    public class SurveyDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string WelcomeInstruction { get; set; }

        public string ThankYouMessage { get; set; }

        public long SurveyConductId { get; set; }

        public long RespondentId { get; set; }

        public string RespondentFirstName { get; internal set; }

        public string RespondentLastName { get; internal set; }

        public DateTime? LastEditedAt { get; set; }

        public bool IsSurveyClosed { get; set; }

        public bool ShouldAutoClose { get; set; }

        public bool IsCompleted { get; set; }

        public bool IsConfirmationSent { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public ICollection<QuestionDto> Questions { get; set; }
    }
}
