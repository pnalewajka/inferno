﻿using Smt.Atomic.Data.Entities.Modules.Survey;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Survey.Dto
{
    public class SurveyConductQuestionDtoToSurveyConductQuestionMapping : ClassMapping<SurveyConductQuestionDto, SurveyConductQuestion>
    {
        public SurveyConductQuestionDtoToSurveyConductQuestionMapping()
        {
            Mapping = dto =>
            new SurveyConductQuestion
            {
                Id = dto.Id,
                SurveyConductId = dto.SurveyConductId,
                Text = dto.Text,
                Type = dto.Type,
                Order = dto.Order,
                Items = dto.Items,
                DefaultAnswer = dto.DefaultAnswer,
                IsRequired = dto.IsRequired,
                FieldSize = dto.FieldSize,
                Description = dto.Description,
                VisibilityCondition = dto.VisibilityCondition,
                FieldName = dto.FieldName,
                Timestamp = dto.Timestamp
            };
        }
    }
}
