﻿namespace Smt.Atomic.Business.Survey.Dto
{
    public class ServeyConfirmationAfterSubmitDto
    {
        public string SurveyName { get; set; }

        public string SurveyAnswersUrl { get; set; }
            
        public string ContinueSurveyUrl { get; set; }

        public string PollsterFullName { get; set; }

        public string PollsterEmailAddress { get; internal set; }
    }
}
