﻿using Smt.Atomic.Data.Entities.Modules.Survey;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Survey.Dto
{
    public class SurveyConductQuestionToSurveyConductQuestionDtoMapping : ClassMapping<SurveyConductQuestion, SurveyConductQuestionDto>
    {
        public SurveyConductQuestionToSurveyConductQuestionDtoMapping()
        {
            Mapping = entity =>
            new SurveyConductQuestionDto
            {
                Id = entity.Id,
                SurveyConductId = entity.SurveyConductId,
                Text = entity.Text,
                Type = entity.Type,
                Order = entity.Order,
                Items = entity.Items,
                DefaultAnswer = entity.DefaultAnswer,
                IsRequired = entity.IsRequired,
                FieldSize = entity.FieldSize,
                Description = entity.Description,
                VisibilityCondition = entity.VisibilityCondition,
                FieldName = entity.FieldName,
                ImpersonatedById = entity.ImpersonatedById,
                ModifiedById = entity.ModifiedById,
                ModifiedOn = entity.ModifiedOn,
                Timestamp = entity.Timestamp
            };
        }
    }
}
