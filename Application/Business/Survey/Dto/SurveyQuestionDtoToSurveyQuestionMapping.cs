﻿using Smt.Atomic.Data.Entities.Modules.Survey;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Survey.Dto
{
    public class SurveyQuestionDtoToSurveyQuestionMapping : ClassMapping<SurveyQuestionDto, SurveyQuestion>
    {
        public SurveyQuestionDtoToSurveyQuestionMapping()
        {
            Mapping = dto =>
            new SurveyQuestion
            {
                Id = dto.Id,
                Text = dto.Text,
                Type = dto.Type,
                Items = dto.Items,
                DefaultAnswer = dto.DefaultAnswer,
                IsRequired = dto.IsRequired,
                Description = dto.Description,
                VisibilityCondition = dto.VisibilityCondition,
                Timestamp = dto.Timestamp,
                SurveyDefinitionId = dto.SurveyDefinitionId,
                FieldName = dto.FieldName,
                FieldSize = dto.FieldSize,
                Order = dto.Order,
            };
        }
    }
}
