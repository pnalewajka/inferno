﻿using System;
using System.Collections.Generic;

namespace Smt.Atomic.Business.Survey.Dto
{
    public class QuestionItemsDto
    {
        private bool _isInitialized = false;
        private string _itemsRaw;
        private List<string> _innerItems;
        private List<string> ItemsList
        {
            get
            {
                if (!_isInitialized)
                {
                    Initialize();
                }

                return _innerItems;
            }
        }

        private void Initialize()
        {
            if (string.IsNullOrWhiteSpace(_itemsRaw))
            {
                _innerItems = new List<string>();
            }
            else
            {
                var innerItems = _itemsRaw.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                _innerItems = new List<string>(innerItems);
            }
            _isInitialized = true;
        }

        public QuestionItemsDto()
        { }

        public QuestionItemsDto(string items)
        {
            _itemsRaw = items;
        }

        public string this[int i]
        {
            get
            {
                if (i < ItemsList.Count)
                {
                    return ItemsList[i];
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string[] Items { get { return ItemsList.ToArray(); } }

        public void AddItem(string item)
        {
            ItemsList.Add(item);
        }

        public int Count
        {
            get { return ItemsList.Count; }
        }

        public override string ToString()
        {
            return string.Join(Environment.NewLine, ItemsList);
        }
    }
}
