﻿using Smt.Atomic.Data.Entities.Modules.Survey;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using System.Linq;

namespace Smt.Atomic.Business.Survey.Dto
{
    public class SurveyConductToSurveyConductDtoMapping : ClassMapping<SurveyConduct, SurveyConductDto>
    {
        public SurveyConductToSurveyConductDtoMapping()
        {
            Mapping = entity => new SurveyConductDto
            {
                Id = entity.Id,
                SurveyName = entity.SurveyName,
                SurveyDescription = entity.SurveyDescription,
                ShouldAutoClose = entity.ShouldAutoClose,
                SurveyWelcomeInstruction = entity.SurveyWelcomeInstruction,
                SurveyThankYouMessage = entity.SurveyThankYouMessage,
                SurveyDefinitionId = entity.SurveyDefinitionId,
                OpenedOn = entity.OpenedOn,
                IsClosed = entity.IsClosed,
                Pollster = entity.PollsterFullName,
                OpenedById = entity.OpenedById,
                ImpersonatedById = entity.ImpersonatedById,
                ModifiedById = entity.ModifiedById,
                ModifiedOn = entity.ModifiedOn,
                Timestamp = entity.Timestamp,
                OrgUnitIds = entity.OrgUnits.Select(o => o.Id).ToArray(),
                EmployeeIds = entity.Employees.Select(e => e.Id).ToArray(),
                ActiveDirectoryGroupIds = entity.ActiveDirectoryGroups.Select(a => a.Id).ToArray(),
                OrganizationUnitNames = entity.OrgUnits.Select(o => o.Name).ToArray(),
                EmployeeNames = entity.Employees.Select(e => e.FullName).ToArray(),
                ActiveDirectoryGroupNames = entity.ActiveDirectoryGroups.Select(a => a.Name).ToArray()
            };
        }
    }
}
