﻿using Smt.Atomic.Business.Survey.BusinessLogics;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.Survey.Dto
{
    public class MySurveyToMySurveyDtoMapping : ClassMapping<Data.Entities.Modules.Survey.Survey, MySurveyDto>
    {
        public MySurveyToMySurveyDtoMapping()
        {
            Mapping = entity =>
            new MySurveyDto
            {
                Id = entity.Id,
                SurveyConductId = entity.SurveyConductId,
                Name = entity.SurveyConduct.SurveyName,
                Description = entity.SurveyConduct.SurveyDescription,
                OpenedOn = entity.SurveyConduct.OpenedOn.Value,
                Timestamp = entity.Timestamp,
                IsCompleted = entity.IsCompleted,
                IsClosed = SurveyBusinessLogic.IsClosed.Call(entity), 
            };
        }
    }
}
