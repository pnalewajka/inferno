﻿using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Survey.Dto
{
    public class QuestionDto
    {
        public long Id { get; set; }

        public string Answer { get; set; }

        public string QuestionText { get; set; }

        public string QuestionDescription { get; set; }

        public string VisibilityCondition { get; set; }

        public string FieldName { get; set; }

        public SurveyFieldSize FieldSize { get; set; }

        public bool IsRequired { get; set; }

        public QuestionItemsDto QuestionItems { get; set; }

        public long Order { get; set; }

        public SurveyQuestionType Type { get; set; }
    }
}
