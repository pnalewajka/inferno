﻿using Smt.Atomic.Data.Entities.Modules.Survey;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Survey.Dto
{
    public class SurveyDefinitionToSurveyDefinitionDtoMapping : ClassMapping<SurveyDefinition, SurveyDefinitionDto>
    {
        public SurveyDefinitionToSurveyDefinitionDtoMapping()
        {
            Mapping = e => new SurveyDefinitionDto
            {
                Id = e.Id,
                Name = e.Name,
                Description = e.Description,
                ShouldAutoClose = e.ShouldAutoClose,
                WelcomeInstruction = e.WelcomeInstruction,
                ThankYouMessage = e.ThankYouMessage,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
