﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Survey.Dto
{
    public class SurveyDtoToSurveyMapping : ClassMapping<SurveyDto, Data.Entities.Modules.Survey.Survey>
    {
        public SurveyDtoToSurveyMapping()
        {
            Mapping = dto => new Data.Entities.Modules.Survey.Survey
            {
                Id = dto.Id,
                SurveyConductId = dto.SurveyConductId,
                RespondentId = dto.RespondentId,
                Timestamp = dto.Timestamp
            };
        }
    }
}
