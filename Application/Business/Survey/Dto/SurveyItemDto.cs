﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Survey.Dto
{
    public class SurveyItemDto
    {
        public long QuestionId { get; set; }

        public bool IsRequired { get; set; }

        public string FieldName { get; set; }

        public IEnumerable<string> SelectedAnswer { get; set; }

        public string Answer { get; set; }
    }
}
