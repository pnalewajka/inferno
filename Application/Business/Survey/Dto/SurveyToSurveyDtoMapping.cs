﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Survey.Dto
{
    public class SurveyToSurveyDtoMapping : ClassMapping<Data.Entities.Modules.Survey.Survey, SurveyDto>
    {
        public SurveyToSurveyDtoMapping()
        {
            Mapping = entity => new SurveyDto
            {
                Id = entity.Id,
                Name = entity.SurveyConduct.SurveyName,
                Description = entity.SurveyConduct.SurveyDescription,
                ShouldAutoClose = entity.SurveyConduct.ShouldAutoClose,
                WelcomeInstruction = entity.SurveyConduct.SurveyWelcomeInstruction,
                ThankYouMessage = entity.SurveyConduct.SurveyThankYouMessage,
                SurveyConductId = entity.SurveyConductId,
                RespondentId = entity.RespondentId,
                RespondentFirstName = entity.Respondent.FirstName,
                RespondentLastName = entity.Respondent.LastName,
                LastEditedAt = entity.LastEditedAt,
                IsCompleted = entity.IsCompleted,
                IsConfirmationSent = entity.IsConfirmationSent,
                Timestamp = entity.Timestamp
            };
        }
    }
}
