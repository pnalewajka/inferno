﻿using Smt.Atomic.Data.Entities.Modules.Survey;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Survey.Dto
{
    public class SurveyQuestionToSurveyQuestionDtoMapping : ClassMapping<SurveyQuestion, SurveyQuestionDto>
    {
        public SurveyQuestionToSurveyQuestionDtoMapping()
        {
            Mapping = entity =>
            new SurveyQuestionDto
            {
                Id = entity.Id,
                Text = entity.Text,
                Type = entity.Type,
                Items = entity.Items,
                DefaultAnswer = entity.DefaultAnswer,
                IsRequired = entity.IsRequired,
                Description = entity.Description,
                VisibilityCondition = entity.VisibilityCondition,
                ImpersonatedById = entity.ImpersonatedById,
                ModifiedById = entity.ModifiedById,
                ModifiedOn = entity.ModifiedOn,
                Timestamp = entity.Timestamp,
                SurveyDefinitionId = entity.SurveyDefinitionId,
                FieldName = entity.FieldName,
                FieldSize = entity.FieldSize,
                Order = entity.Order,
            };
        }
    }
}
