﻿using System.Collections.ObjectModel;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Survey;

namespace Smt.Atomic.Business.Survey.Dto
{
    public class WhistleBlowingDtoToWhistleBlowingMapping : ClassMapping<WhistleBlowingDto, WhistleBlowing>
    {
        public WhistleBlowingDtoToWhistleBlowingMapping()
        {
            Mapping = d => new WhistleBlowing
            {
                Id = d.Id,
                Title = d.Title,
                Message = d.Message,
                ShowUserName = d.ShowUserName,
                Documents = new Collection<WhistleBlowingDocument>(),
                MessageStatus = d.MessageStatus,
                CreatedOn = d.CreatedOn,
            };
        }
    }
}
