﻿using Smt.Atomic.Data.Entities.Modules.Survey;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Survey.Dto
{
    public class SurveyDefinitionDtoToSurveyDefinitionMapping : ClassMapping<SurveyDefinitionDto, SurveyDefinition>
    {
        public SurveyDefinitionDtoToSurveyDefinitionMapping()
        {
            Mapping = d => new SurveyDefinition
            {
                Id = d.Id,
                Name = d.Name,
                Description = d.Description,
                ShouldAutoClose = d.ShouldAutoClose,
                WelcomeInstruction = d.WelcomeInstruction,
                ThankYouMessage = d.ThankYouMessage,
                Timestamp = d.Timestamp
            };
        }
    }
}
