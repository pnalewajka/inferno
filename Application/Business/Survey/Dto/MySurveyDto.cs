﻿using System;
namespace Smt.Atomic.Business.Survey.Dto
{
    public class MySurveyDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime OpenedOn { get; set; }

        public bool IsCompleted { get; set; }

        public bool IsClosed { get; set; }

        public byte[] Timestamp { get; set; }

        public long SurveyConductId { get; set; }
    }
}
