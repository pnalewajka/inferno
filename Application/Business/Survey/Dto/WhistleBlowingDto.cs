﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Survey.Dto
{
    public class WhistleBlowingDto
    {
        public long Id { get; set; }

        public string Title { get; set; }

        public string Message { get; set; }

        public bool ShowUserName { get; set; }

        public string ModifiedByUserFullName { get; set; }

        public DateTime CreatedOn { get; set; }

        public IEnumerable<DocumentDto> Documents { get; set; }

        public WhistleBlowingMessageStatus MessageStatus { get; set; }
    }
}
