﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Survey;

namespace Smt.Atomic.Business.Survey.Dto
{
    public class WhistleBlowingToWhistleBlowingDtoMapping : ClassMapping<WhistleBlowing, WhistleBlowingDto>
    {
        public WhistleBlowingToWhistleBlowingDtoMapping()
        {
            Mapping = e => new WhistleBlowingDto
            {
                Id = e.Id,
                Title = e.Title,
                Message = e.Message,
                ShowUserName = e.ShowUserName,
                ModifiedByUserFullName = e.ShowUserName ? e.ModifiedBy.FullName : null,
                CreatedOn = e.CreatedOn,
                Documents = e.Documents.Select(p => new DocumentDto
                {
                    ContentType = p.ContentType,
                    DocumentName = p.Name,
                    DocumentId = p.Id
                }),
                MessageStatus = e.MessageStatus,
            };
        }
    }
}
