﻿namespace Smt.Atomic.Business.Survey.Dto
{
    public class RespondentReminderDto
    {
        public string SurveyName { get; set; }

        public string SurveyUrl { get; set; }

        public string PollsterFullName { get; set; }

        public string PollsterEmail { get; internal set; }
    }
}
