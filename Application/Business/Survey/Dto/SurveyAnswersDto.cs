﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.Survey.Dto
{
    public class SurveyAnswersDto
    {
        public long SurveyId { get; set; }

        public IList<SurveyItemDto> Items { get; set; }
    }
}
