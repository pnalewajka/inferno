﻿using System;
namespace Smt.Atomic.Business.Survey.Dto
{
    public class SurveyConductDto
    {
        public long Id { get; set; }

        public long? SurveyDefinitionId { get; set; }

        public string SurveyName { get; set; }

        public string SurveyDescription { get; set; }

        public bool ShouldAutoClose { get;set;}
        
        public string SurveyWelcomeInstruction { get; set; }

        public string SurveyThankYouMessage { get; set; }

        public DateTime? OpenedOn { get; set; }

        public string Pollster { get; set; }

        public long? OpenedById { get; set; }

        public bool IsClosed { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public long[] OrgUnitIds { get; set; }

        public long[] EmployeeIds { get; set; }

        public long[] ActiveDirectoryGroupIds { get; set; }

        public string[] EmployeeNames { get; set; }

        public string[] ActiveDirectoryGroupNames { get; set; }

        public string[] OrganizationUnitNames { get; set; }
    }
}
