﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Survey.Dto
{
    public class MySurveyDtoToMySurveyMapping : ClassMapping<MySurveyDto, Data.Entities.Modules.Survey.Survey>
    {
        public MySurveyDtoToMySurveyMapping()
        {
            Mapping = dto => null;
        }
    }
}
