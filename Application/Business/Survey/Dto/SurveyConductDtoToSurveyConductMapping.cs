﻿using Smt.Atomic.Data.Entities.Modules.Survey;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.Survey.Dto
{
    public class SurveyConductDtoToSurveyConductMapping : ClassMapping<SurveyConductDto, SurveyConduct>
    {
        public SurveyConductDtoToSurveyConductMapping()
        {
            Mapping = dto => new SurveyConduct
            {
                Id = dto.Id,
                SurveyDefinitionId = dto.SurveyDefinitionId,
                SurveyName = dto.SurveyName,
                SurveyDescription = dto.SurveyDescription,
                ShouldAutoClose = dto.ShouldAutoClose,
                SurveyWelcomeInstruction = dto.SurveyWelcomeInstruction,
                SurveyThankYouMessage = dto.SurveyThankYouMessage,
                OpenedOn = dto.OpenedOn,
                OpenedById = dto.OpenedById,
                IsClosed = dto.IsClosed,
                Timestamp = dto.Timestamp,
            };
        }
    }
}
