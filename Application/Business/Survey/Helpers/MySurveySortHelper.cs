﻿using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Survey.Helpers
{
    internal class MySurveySortHelper
    {
        public static void OrderByIsClosed(OrderedQueryBuilder<Data.Entities.Modules.Survey.Survey> builder, SortingDirection direction)
        {
            builder.ApplySortingKey(r => r.SurveyConduct.IsClosed || r.SurveyConduct.ShouldAutoClose && r.IsCompleted, direction);
        }
    }
}
