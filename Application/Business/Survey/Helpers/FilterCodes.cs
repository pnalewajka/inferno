﻿namespace Smt.Atomic.Business.Survey.Helpers
{
    public static class FilterCodes
    {
        public const string MySurveys = "my-surveys";
        public const string AllSurveys = "all-surveys";
    }
}
