﻿using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Survey;

namespace Smt.Atomic.Business.Survey.Helpers
{
    internal class SurveyConductSortHelper
    {
        public static void OrderByPollsterFullName(OrderedQueryBuilder<SurveyConduct> builder, SortingDirection direction)
        {
            builder.ApplySortingKey(r => r.OpenedBy.LastName, direction);
            builder.ApplySortingKey(r => r.OpenedBy.FirstName, direction);
        }
    }
}
