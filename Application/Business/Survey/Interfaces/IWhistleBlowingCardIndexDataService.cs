﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Survey.Dto;

namespace Smt.Atomic.Business.Survey.Interfaces
{
    public interface IWhistleBlowingCardIndexDataService : ICardIndexDataService<WhistleBlowingDto>
    {
        BoolResult ToggleSetIsRead(bool value, ICollection<long> ids);
    }
}

