﻿using Smt.Atomic.Data.Entities.Modules.Allocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Survey.Interfaces
{
    public interface ISurveyRespondentService
    {
        bool AddOrgUnitSurveysForEmployee(Employee employee);
        bool AddOrgUnitSurveysForEmployeeId(long employeeId);
        bool AddRespondentsToSurveyConduct(long surveyConductId, IList<long> respondentIds);
    }
}
