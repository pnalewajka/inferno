﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Survey.Dto;

namespace Smt.Atomic.Business.Survey.Interfaces
{
    public interface ISurveyConductCardIndexDataService : ICardIndexDataService<SurveyConductDto>
    {
        void CloseSurveyConduct(long id);
        void UrgeRespondents(long surveyConductId, string surveyUrl);
    }
}