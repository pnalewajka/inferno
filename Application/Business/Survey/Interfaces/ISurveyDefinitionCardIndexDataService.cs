﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Survey.Dto;

namespace Smt.Atomic.Business.Survey.Interfaces
{
    public interface ISurveyDefinitionCardIndexDataService : ICardIndexDataService<SurveyDefinitionDto>
    {
        SurveyDto CreateSurveyPreview(long surveyDefinitionId);
    }
}

