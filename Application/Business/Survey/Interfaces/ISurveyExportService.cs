﻿using Smt.Atomic.Business.Survey.Dto;
using System.Collections.Generic;

namespace Smt.Atomic.Business.Survey.Interfaces
{
    public interface ISurveyExportService
    {
        string ExportToExcel(IEnumerable<SurveyDto> surveys);
    }
}
