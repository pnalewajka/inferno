﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Survey.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Survey.Interfaces
{
    public interface ISurveyCardIndexDataService : ICardIndexDataService<SurveyDto>
    {
        SurveyDto GetSurvey(long id);

        IEnumerable<SurveyDto> GetSurveys(long[] ids);

        IEnumerable<SurveyDto> GetSurveysForConduct(long surveyConductId);

        long GetSurveyForUser(long surveyConductId);

        SurveyDto ParseAnswers(SurveyAnswersDto surveyAnswers);

        void FillSurvey(SurveyDto surveyDto, bool shouldFinishSurvey = false);

        void SendServeyConfirmationAfterSubmit(long surveyId, string surveyAnswersUrl, string continueSurveyUrl = null);
    }
}