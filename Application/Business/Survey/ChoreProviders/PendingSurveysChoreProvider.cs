﻿using System;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Survey.BusinessLogics;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using SurveyEntity = Smt.Atomic.Data.Entities.Modules.Survey.Survey;

namespace Smt.Atomic.Business.Survey.ChoreProviders
{
    public class PendingSurveysChoreProvider
        : IChoreProvider
    {
        private readonly IPrincipalProvider _principalProvider;

        public PendingSurveysChoreProvider(
            IPrincipalProvider principalProvider)
        {
            _principalProvider = principalProvider;
        }

        public IQueryable<ChoreDto> GetActiveChores(IReadOnlyRepositoryFactory repositoryFactory)
        {
            if (!_principalProvider.Current.IsInRole(SecurityRoleType.CanViewSurveys))
            {
                return null;
            }

            var surveys = repositoryFactory.Create<SurveyEntity>().Filtered();

            var pendingSurveys = surveys
                .Where(s => s.Respondent.Id == _principalProvider.Current.Id.Value)
                .Where(s => !SurveyBusinessLogic.IsClosed.Call(s))
                .Where(s => !s.IsCompleted)
                .GroupBy(s => 0)
                .Select(g => new ChoreDto
                {
                    Type = ChoreType.PendingSurveys,
                    DueDate = null,
                    Parameters = g.Count().ToString(),
                });

            return pendingSurveys;
        }
    }
}
