﻿using System;
using System.Runtime.Serialization;
using Smt.Atomic.Data.Entities.Dto;

namespace Smt.Atomic.Business.ActiveDirectory.BusinessEvents
{
    [DataContract]
    public class RemoveUserFromActiveDirectoryGroupsEvent : BusinessEvent
    {
        [DataMember]
        public Guid UserId { get; set; }

        [DataMember]
        public string[] ActiveDirectoryGroupNames { get; set; }
    }
}
