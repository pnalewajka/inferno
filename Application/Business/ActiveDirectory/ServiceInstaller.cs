﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.ActiveDirectory.Interfaces;
using Smt.Atomic.Business.ActiveDirectory.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.ActiveDirectory
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            switch (containerType)
            {
                case ContainerType.JobScheduler:
                case ContainerType.WebApi:
                case ContainerType.WebApp:
                case ContainerType.WebServices:
                case ContainerType.UnitTests:
                    container.Register(Component.For<IActiveDirectoryWriter>().ImplementedBy<ActiveDirectoryWriter>().LifestyleTransient());
                    container.Register(Component.For<IActiveDirectorySearchService>().ImplementedBy<ActiveDirectorySearchService>().LifestyleTransient());
                    break;
            }
        }
    }
}

