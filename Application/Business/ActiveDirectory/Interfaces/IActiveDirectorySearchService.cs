﻿namespace Smt.Atomic.Business.ActiveDirectory.Interfaces
{
    public interface IActiveDirectorySearchService
    {
        bool CheckIfUsed(string acronym);
    }
}