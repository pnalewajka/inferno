﻿using System;
using Smt.Atomic.Business.ActiveDirectory.Dto;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.ActiveDirectory.Interfaces
{
    public interface IActiveDirectoryWriter
    {
        Guid? CreateActiveDirectoryUser(ActiveDirectoryUserCreationDto activeDirectoryUserCreationDto);

        BoolResult ModifyActiveDirectoryUser(ActiveDirectoryUserChangeDescriptionDto activeDirectoryUserChangeDescriptionDto);

        BoolResult ModifyActiveDirectoryGroupUsers(ActiveDirectoryGroupChangeDto activeDirectoryGroupChange);
    }
}