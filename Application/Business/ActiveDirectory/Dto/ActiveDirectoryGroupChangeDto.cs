﻿using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.ActiveDirectory.Dto
{
    public class ActiveDirectoryGroupChangeDto
    {
        public long GroupId { get; set; }

        public Optional<long?> OwnerId { get; set; }

        public Optional<long[]> ManagersIds { get; set; }

        public Optional<long[]> MembersToAddIds { get; set; }

        public Optional<long[]> MembersToRemoveIds { get; set; }

        public Optional<string> Info { get; set; }

        public Optional<string> Description { get; set; }
    }
}
