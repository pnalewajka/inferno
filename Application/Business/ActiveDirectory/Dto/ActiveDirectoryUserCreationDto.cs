﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.ActiveDirectory.Dto
{
    public class ActiveDirectoryUserCreationDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Login { get; set; }

        public long JobTitleId { get; set; }

        public long JobProfileId { get; set; }

        public long OrgUnitId { get; set; }

        public long LocationId { get; set; }

        public PlaceOfWork PlaceOfWork { get; set; }

        public long CompanyId { get; set; }

        public long LineManagerId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public DateTime? EffectiveDate { get; set; }

        public long ContractTypeId { get; set; }

        public string CrmId { get; set; }

        public string Acronym { get; set; }
    }
}