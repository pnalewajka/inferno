﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.ActiveDirectory.Dto
{
    public class ActiveDirectoryUserChangeDescriptionDto
    {
        public long EmployeeId { get; set; }

        public Optional<long> JobTitleId { get; set; }

        public Optional<long> LocationId { get; set; }

        public Optional<PlaceOfWork> PlaceOfWork { get; set; }

        public Optional<string> FirstName { get; set; }

        public Optional<string> LastName { get; set; }

        public Optional<string> Email { get; set; }

        public Optional<long?> CompanyId { get; set; }

        public Optional<long[]> JobMatrixLevelIds { get; set; }

        public Optional<long[]> JobProfileIds { get; set; }

        public Optional<long?> LineManagerId { get; set; }

        public Optional<long> OrgUnitId { get; set; }

        public Optional<Guid> CrmId { get; set; }

        public Optional<DateTime> StartDate { get; set; }

        public Optional<DateTime?> EndDate { get; set; }

        public Optional<long> ContractTypeId { get; set; }

        public Optional<EmploymentType?> ProfileType { get; set; }
    }
}