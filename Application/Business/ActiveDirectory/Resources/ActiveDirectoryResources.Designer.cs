﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Smt.Atomic.Business.ActiveDirectory.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class ActiveDirectoryResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ActiveDirectoryResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Smt.Atomic.Business.ActiveDirectory.Resources.ActiveDirectoryResources", typeof(ActiveDirectoryResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Active Directory was updated.
        /// </summary>
        internal static string ActiveDirectoryUpdatedSuccessfuly {
            get {
                return ResourceManager.GetString("ActiveDirectoryUpdatedSuccessfuly", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Could not find group in Active Directory.
        /// </summary>
        internal static string CouldNotFindGroupInActiveDirectory {
            get {
                return ResourceManager.GetString("CouldNotFindGroupInActiveDirectory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Could not find user in Active Directory.
        /// </summary>
        internal static string CouldNotFindUserInActiveDirectory {
            get {
                return ResourceManager.GetString("CouldNotFindUserInActiveDirectory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to AD write was skipped - object not on writable list.
        /// </summary>
        internal static string NoWritableObjectMessage {
            get {
                return ResourceManager.GetString("NoWritableObjectMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to AD write was skipped - no value for allowed writable object parameter.
        /// </summary>
        internal static string WritableObjectParameterEmpty {
            get {
                return ResourceManager.GetString("WritableObjectParameterEmpty", resourceCulture);
            }
        }
    }
}
