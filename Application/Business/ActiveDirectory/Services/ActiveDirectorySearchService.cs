﻿using System.Linq;
using Smt.Atomic.Business.ActiveDirectory.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Data.ActiveDirectory;

namespace Smt.Atomic.Business.ActiveDirectory.Services
{
    public class ActiveDirectorySearchService : IActiveDirectorySearchService
    {
        private readonly ISettingsProvider _settingsProvider;

        public ActiveDirectorySearchService(ISettingsProvider settingsProvider)
        {
            _settingsProvider = settingsProvider;
        }

        public bool CheckIfUsed(string acronym)
        {
            using (var coreDirectoryContext = new CoreDirectoryContext(_settingsProvider))
            {
                return coreDirectoryContext.Users.Count(u => u.Acronym == acronym || u.UserName == acronym) > 0;
            }
        }
    }
}