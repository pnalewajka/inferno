﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Security.Cryptography;
using Castle.Core.Logging;
using Smt.Atomic.Business.ActiveDirectory.Dto;
using Smt.Atomic.Business.ActiveDirectory.Interfaces;
using Smt.Atomic.Business.ActiveDirectory.Resources;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Business.Extensions;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Data.ActiveDirectory;
using Smt.Atomic.Data.ActiveDirectory.Entities;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.ActiveDirectory.Services
{
    internal class ActiveDirectoryWriter : IActiveDirectoryWriter, IDisposable
    {
        private const string TimeFormat = "yyyy-MM-dd";
        private const string AllWritesAllowedSymbol = "*";
        private const string ActiveDirectoryWriterChangeLoggerInfoFormat = "ADWriterChange: {0} has changed {1} value from {2} to {3} for user {4}.";

        private readonly RandomNumberGenerator _randomNumberGenerator = new RNGCryptoServiceProvider();

        private readonly ISettingsProvider _settingsProvider;
        private readonly IPrincipalProvider _principalProvider;
        private readonly ICompanyService _companyService;
        private readonly ILocationService _locationService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IUnitOfWorkService<IAllocationDbScope> _unitOfWorkService;
        private readonly ILogger _logger;

        public ActiveDirectoryWriter(
            ISettingsProvider settingsProvider,
            ICompanyService companyService,
            ILocationService locationService,
            ISystemParameterService systemParameterService,
            IUnitOfWorkService<IAllocationDbScope> unitOfWorkService,
            IPrincipalProvider principalProvider,
            ILogger logger)
        {
            _settingsProvider = settingsProvider;
            _systemParameterService = systemParameterService;
            _companyService = companyService;
            _locationService = locationService;
            _unitOfWorkService = unitOfWorkService;
            _principalProvider = principalProvider;
            _logger = logger;
        }

        public void Dispose()
        {
            _randomNumberGenerator.Dispose();
        }

        public Guid? CreateActiveDirectoryUser(ActiveDirectoryUserCreationDto activeDirectoryUserCreationDto)
        {
            if (GetAllowedWritableObjectsAsString() != AllWritesAllowedSymbol)
            {
                return null;
            }

            using (var coreDirectoryContext = CreateWritableCoreDirectoryContext())
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var contractType = unitOfWork.Repositories.ContractTypes
                    .Where(c => c.Id == activeDirectoryUserCreationDto.ContractTypeId)
                    .Select(c => new { c.ActiveDirectoryCode, c.EmploymentType })
                    .Single();
                var jobTitle = unitOfWork.Repositories.JobTitles
                    .Where(t => t.Id == activeDirectoryUserCreationDto.JobTitleId)
                    .Select(t => new { t.NameEn, t.JobMatrixLevelId })
                    .Single();
                var company = GetCompanyByIdOrDefault(activeDirectoryUserCreationDto.CompanyId);

                var user = new User
                {
                    FirstName = activeDirectoryUserCreationDto.FirstName,
                    LastName = activeDirectoryUserCreationDto.LastName,
                    DisplayName = GetDisplayName(coreDirectoryContext,
                        activeDirectoryUserCreationDto.FirstName, activeDirectoryUserCreationDto.LastName),
                    Email = activeDirectoryUserCreationDto.Email,
                    UserName = activeDirectoryUserCreationDto.Login,
                    PrincipalName = $"{activeDirectoryUserCreationDto.Login}@corp.smtsoftware.com",
                    Title = jobTitle.NameEn,
                    JobMatrixLevel = jobTitle.JobMatrixLevelId.HasValue
                        ? GetJobMatrixLevel(unitOfWork, new[] { jobTitle.JobMatrixLevelId.Value })
                        : null,
                    MainCompetence = GetMainCompetences(unitOfWork, new[] { activeDirectoryUserCreationDto.JobProfileId }),
                    Department = GetDepartment(unitOfWork, activeDirectoryUserCreationDto.OrgUnitId),
                    Location = GetLocationByIdOrDefault(activeDirectoryUserCreationDto.LocationId)?.Name,
                    PlaceOfWork = activeDirectoryUserCreationDto.PlaceOfWork.GetActiveDirectoryCode(),
                    Company = company.Name,
                    CompanyOfOrigin = GetCompanyCode(company),
                    LineManager = EncodeActiveDirectoryUserReference(unitOfWork, activeDirectoryUserCreationDto.LineManagerId),
                    StartDate = activeDirectoryUserCreationDto.StartDate.ToInvariantString(TimeFormat),
                    EndDate = activeDirectoryUserCreationDto.EndDate?.ToInvariantString(TimeFormat),
                    EffectiveDate = activeDirectoryUserCreationDto.EffectiveDate,
                    ContractType = contractType.ActiveDirectoryCode,
                    ProfileType = contractType.EmploymentType?.ToString(),
                    CrmId = activeDirectoryUserCreationDto.CrmId,
                    Acronym = activeDirectoryUserCreationDto.Acronym,
                    MSSFU30Name = activeDirectoryUserCreationDto.Login,
                    MSSFU30NisDomain = "corp",
                    Uid = activeDirectoryUserCreationDto.Login,
                    UnixHomeDirectory = $"/home/users/{activeDirectoryUserCreationDto.Login}",
                    UidNumber = GetUidNumber(coreDirectoryContext)
                };

                string parentPath;

                FillCompanySpecificFields(activeDirectoryUserCreationDto, user, company);
                FillEmploymentTypeSpecificFields(user, contractType.EmploymentType, out parentPath);

                user.SetParent(coreDirectoryContext.OrgUnits.Single(u => u.Path == parentPath));

                coreDirectoryContext.Users.AddEntry(user.DisplayName, user);

                coreDirectoryContext.SubmitChanges();
            }

            return GetActivatedUserGuid(activeDirectoryUserCreationDto.Login, GeneratePassword());
        }

        public BoolResult ModifyActiveDirectoryUser(
            ActiveDirectoryUserChangeDescriptionDto activeDirectoryUserChangeDescriptionDto)
        {
            if (string.IsNullOrWhiteSpace(GetAllowedWritableObjectsAsString()))
            {
                //if no value specified we won't write to AD
                var result = new BoolResult(true);
                result.AddAlert(AlertType.Warning, ActiveDirectoryResources.WritableObjectParameterEmpty);

                return result;
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employee = unitOfWork.Repositories.Employees.GetById(activeDirectoryUserChangeDescriptionDto.EmployeeId);

                var allowedWritableObjects = GetAllowedWritableObjects();

                if (allowedWritableObjects != null && (
                    !employee.User.ActiveDirectoryId.HasValue ||
                    !allowedWritableObjects.Contains(employee.User.ActiveDirectoryId.Value))
                    )
                {
                    var result = new BoolResult(true);
                    result.AddAlert(AlertType.Warning, ActiveDirectoryResources.NoWritableObjectMessage);

                    return result;
                }

                using (var coreDirectoryContext = CreateWritableCoreDirectoryContext())
                {
                    var syncPaths = _systemParameterService.GetParameter<string[]>(ParameterKeys.ActiveDirectoryUserSyncPaths);

                    var employeeActiveDirectoryRecord = coreDirectoryContext.Users
                        .Where(u => u.Email == employee.Email)
                        .AsEnumerable()
                        .SingleOrDefault(u => syncPaths.Any(p => u.Path.EndsWith(p)));

                    if (employeeActiveDirectoryRecord == null)
                    {
                        _logger.Info($"Couldn't find user in ActiveDirectory: {employee.User.Login}, Sync. failed");

                        return new BoolResult(false, new List<AlertDto> { new AlertDto { Message = ActiveDirectoryResources.CouldNotFindUserInActiveDirectory, Type = AlertType.Warning } });
                    }

                    bool recordChanged = false;

                    if (activeDirectoryUserChangeDescriptionDto.LocationId?.IsSet ?? false)
                    {
                        var location = unitOfWork.Repositories.Locations
                            .Where(t => t.Id == activeDirectoryUserChangeDescriptionDto.LocationId.Value)
                            .Select(t => new { t.Name, IsoCode = t.CityId.HasValue ? t.City.Country.IsoCode : null })
                            .Single();

                        string office = string.IsNullOrEmpty(location.IsoCode) ? $"{location.Name}" : $"{location.Name} ({location.IsoCode})";

                        LogActiveDirectoryChange(nameof(employeeActiveDirectoryRecord.Location), employeeActiveDirectoryRecord.Location,
                            location.Name, employeeActiveDirectoryRecord.UserName);

                        LogActiveDirectoryChange(nameof(employeeActiveDirectoryRecord.Office), employeeActiveDirectoryRecord.Office,
                            office, employeeActiveDirectoryRecord.UserName);

                        ModifyActiveDirectoryAttribute(employeeActiveDirectoryRecord, u => u.Location, location.Name);
                        ModifyActiveDirectoryAttribute(employeeActiveDirectoryRecord, u => u.Office, office);

                        recordChanged = true;
                    }

                    if (activeDirectoryUserChangeDescriptionDto.PlaceOfWork?.IsSet ?? false)
                    {
                        var placeOfWorkCode = activeDirectoryUserChangeDescriptionDto.PlaceOfWork.Value.GetActiveDirectoryCode();

                        LogActiveDirectoryChange(nameof(employeeActiveDirectoryRecord.PlaceOfWork), employeeActiveDirectoryRecord.PlaceOfWork,
                            placeOfWorkCode, employeeActiveDirectoryRecord.UserName);

                        ModifyActiveDirectoryAttribute(employeeActiveDirectoryRecord, u => u.PlaceOfWork, placeOfWorkCode);
                        recordChanged = true;
                    }

                    if (activeDirectoryUserChangeDescriptionDto.JobTitleId?.IsSet ?? false)
                    {
                        var jobTitle = unitOfWork.Repositories.JobTitles
                            .Where(t => t.Id == activeDirectoryUserChangeDescriptionDto.JobTitleId.Value).Select(t => t.NameEn).Single();

                        LogActiveDirectoryChange(nameof(employeeActiveDirectoryRecord.Title), employeeActiveDirectoryRecord.Title,
                            jobTitle, employeeActiveDirectoryRecord.UserName);

                        ModifyActiveDirectoryAttribute(employeeActiveDirectoryRecord, u => u.Title, jobTitle);
                        recordChanged = true;
                    }

                    if (activeDirectoryUserChangeDescriptionDto.FirstName?.IsSet ?? false)
                    {
                        LogActiveDirectoryChange(nameof(employeeActiveDirectoryRecord.FirstName), employeeActiveDirectoryRecord.FirstName,
                           activeDirectoryUserChangeDescriptionDto.FirstName.Value, employeeActiveDirectoryRecord.UserName);

                        ModifyActiveDirectoryAttribute(employeeActiveDirectoryRecord, u => u.FirstName, activeDirectoryUserChangeDescriptionDto.FirstName.Value);
                        recordChanged = true;
                    }

                    if (activeDirectoryUserChangeDescriptionDto.LastName?.IsSet ?? false)
                    {
                        LogActiveDirectoryChange(nameof(employeeActiveDirectoryRecord.LastName), employeeActiveDirectoryRecord.LastName,
                           activeDirectoryUserChangeDescriptionDto.LastName.Value, employeeActiveDirectoryRecord.UserName);

                        ModifyActiveDirectoryAttribute(employeeActiveDirectoryRecord, u => u.LastName, activeDirectoryUserChangeDescriptionDto.LastName.Value);
                        recordChanged = true;
                    }

                    if (activeDirectoryUserChangeDescriptionDto.Email?.IsSet ?? false)
                    {
                        LogActiveDirectoryChange(nameof(employeeActiveDirectoryRecord.Email), employeeActiveDirectoryRecord.Email,
                            activeDirectoryUserChangeDescriptionDto.Email.Value, employeeActiveDirectoryRecord.UserName);

                        ModifyActiveDirectoryAttribute(employeeActiveDirectoryRecord, u => u.Email, activeDirectoryUserChangeDescriptionDto.Email.Value);
                        recordChanged = true;
                    }

                    if (activeDirectoryUserChangeDescriptionDto.CompanyId?.IsSet ?? false)
                    {
                        var company = GetCompanyByIdOrDefault(activeDirectoryUserChangeDescriptionDto.CompanyId.Value);
                        var companyCode = GetCompanyCode(company);

                        LogActiveDirectoryChange(nameof(employeeActiveDirectoryRecord.Company), employeeActiveDirectoryRecord.Company,
                            company.Name, employeeActiveDirectoryRecord.UserName);
                        LogActiveDirectoryChange(nameof(employeeActiveDirectoryRecord.CompanyOfOrigin), employeeActiveDirectoryRecord.CompanyOfOrigin,
                            companyCode, employeeActiveDirectoryRecord.UserName);

                        ModifyActiveDirectoryAttribute(employeeActiveDirectoryRecord, u => u.Company, company.Name);
                        ModifyActiveDirectoryAttribute(employeeActiveDirectoryRecord, u => u.CompanyOfOrigin, companyCode);
                        recordChanged = true;
                    }

                    if (activeDirectoryUserChangeDescriptionDto.JobMatrixLevelIds?.IsSet ?? false)
                    {
                        var jobMatrixLevel = GetJobMatrixLevel(unitOfWork, activeDirectoryUserChangeDescriptionDto.JobMatrixLevelIds.Value);

                        LogActiveDirectoryChange(nameof(employeeActiveDirectoryRecord.JobMatrixLevel), employeeActiveDirectoryRecord.JobMatrixLevel,
                           jobMatrixLevel, employeeActiveDirectoryRecord.UserName);

                        ModifyActiveDirectoryAttribute(employeeActiveDirectoryRecord, u => u.JobMatrixLevel, jobMatrixLevel);
                        recordChanged = true;
                    }

                    if (activeDirectoryUserChangeDescriptionDto.JobProfileIds?.IsSet ?? false)
                    {
                        var mainCompetences = GetMainCompetences(unitOfWork, activeDirectoryUserChangeDescriptionDto.JobProfileIds.Value);

                        LogActiveDirectoryChange(nameof(employeeActiveDirectoryRecord.MainCompetence), employeeActiveDirectoryRecord.MainCompetence,
                           mainCompetences, employeeActiveDirectoryRecord.UserName);

                        ModifyActiveDirectoryAttribute(employeeActiveDirectoryRecord, u => u.MainCompetence, mainCompetences);
                        recordChanged = true;
                    }

                    if (activeDirectoryUserChangeDescriptionDto.LineManagerId?.IsSet ?? false)
                    {
                        var lineManager = activeDirectoryUserChangeDescriptionDto.LineManagerId.Value.HasValue
                            ? EncodeActiveDirectoryUserReference(unitOfWork, activeDirectoryUserChangeDescriptionDto.LineManagerId.Value.Value)
                            : null;

                        LogActiveDirectoryChange(nameof(employeeActiveDirectoryRecord.LineManager), employeeActiveDirectoryRecord.LineManager,
                           lineManager, employeeActiveDirectoryRecord.UserName);

                        ModifyActiveDirectoryAttribute(employeeActiveDirectoryRecord, u => u.LineManager, lineManager);
                        recordChanged = true;
                    }

                    if (activeDirectoryUserChangeDescriptionDto.OrgUnitId?.IsSet ?? false)
                    {
                        var department = unitOfWork.Repositories.OrgUnits.GetById(activeDirectoryUserChangeDescriptionDto.OrgUnitId.Value)?.Code;

                        LogActiveDirectoryChange(nameof(employeeActiveDirectoryRecord.Department), employeeActiveDirectoryRecord.Department,
                           department, employeeActiveDirectoryRecord.UserName);

                        ModifyActiveDirectoryAttribute(employeeActiveDirectoryRecord, u => u.Department, department);
                        recordChanged = true;
                    }

                    if (activeDirectoryUserChangeDescriptionDto.CrmId?.IsSet ?? false)
                    {
                        var crm = activeDirectoryUserChangeDescriptionDto.CrmId.Value.ToString();

                        LogActiveDirectoryChange(nameof(employeeActiveDirectoryRecord.CrmId), employeeActiveDirectoryRecord.CrmId,
                           crm, employeeActiveDirectoryRecord.UserName);

                        ModifyActiveDirectoryAttribute(employeeActiveDirectoryRecord, u => u.CrmId, crm);
                        recordChanged = true;
                    }

                    if (activeDirectoryUserChangeDescriptionDto.StartDate?.IsSet ?? false)
                    {
                        var newValue = activeDirectoryUserChangeDescriptionDto.StartDate.Value.ToInvariantString(TimeFormat);

                        LogActiveDirectoryChange(nameof(employeeActiveDirectoryRecord.StartDate), employeeActiveDirectoryRecord.StartDate,
                          newValue, employeeActiveDirectoryRecord.UserName);

                        ModifyActiveDirectoryAttribute(employeeActiveDirectoryRecord, u => u.StartDate, newValue);
                        recordChanged = true;
                    }

                    if (activeDirectoryUserChangeDescriptionDto.EndDate?.IsSet ?? false)
                    {
                        var newValue = activeDirectoryUserChangeDescriptionDto.EndDate.Value?.ToInvariantString(TimeFormat);

                        LogActiveDirectoryChange(nameof(employeeActiveDirectoryRecord.EndDate), employeeActiveDirectoryRecord.EndDate,
                          newValue, employeeActiveDirectoryRecord.UserName);

                        ModifyActiveDirectoryAttribute(employeeActiveDirectoryRecord, u => u.EndDate, newValue);
                        recordChanged = true;
                    }

                    if (activeDirectoryUserChangeDescriptionDto.ContractTypeId?.IsSet ?? false)
                    {
                        var contractType = unitOfWork.Repositories.ContractTypes
                            .Where(c => c.Id == activeDirectoryUserChangeDescriptionDto.ContractTypeId.Value)
                            .Select(c => c.ActiveDirectoryCode)
                            .SingleOrDefault();

                        LogActiveDirectoryChange(nameof(employeeActiveDirectoryRecord.ContractType), employeeActiveDirectoryRecord.ContractType,
                           contractType, employeeActiveDirectoryRecord.UserName);

                        ModifyActiveDirectoryAttribute(employeeActiveDirectoryRecord, u => u.ContractType, contractType);
                        recordChanged = true;
                    }

                    if (activeDirectoryUserChangeDescriptionDto.ProfileType?.IsSet ?? false)
                    {
                        var profileType = activeDirectoryUserChangeDescriptionDto.ProfileType.Value?.ToString();

                        LogActiveDirectoryChange(nameof(employeeActiveDirectoryRecord.ProfileType), employeeActiveDirectoryRecord.ProfileType,
                           profileType, employeeActiveDirectoryRecord.UserName);

                        ModifyActiveDirectoryAttribute(employeeActiveDirectoryRecord, u => u.ProfileType, profileType);
                        recordChanged = true;
                    }

                    if (recordChanged)
                    {
                        coreDirectoryContext.SubmitChanges();
                    }

                    var result = new BoolResult(true);
                    result.AddAlert(AlertType.Information, ActiveDirectoryResources.ActiveDirectoryUpdatedSuccessfuly);

                    return result;
                }
            }
        }

        private void ModifyActiveDirectoryAttribute<T>(User user, Expression<Func<User, T>> propertyExpression, T value)
        {
            var memberSelectorExpression = (MemberExpression)propertyExpression.Body;
            var property = (PropertyInfo)memberSelectorExpression.Member;
            var attribute = AttributeHelper.GetPropertyAttribute<System.DirectoryServices.Linq.Attributes.DirectoryPropertyAttribute>(property);

            user.SetAttribute(attribute.Name, value);
        }

        private void ModifyActiveDirectoryAttribute<T>(Group group, Expression<Func<Group, T>> propertyExpression, T value)
        {
            var memberSelectorExpression = (MemberExpression)propertyExpression.Body;
            var property = (PropertyInfo)memberSelectorExpression.Member;
            var attribute = AttributeHelper.GetPropertyAttribute<System.DirectoryServices.Linq.Attributes.DirectoryPropertyAttribute>(property);

            group.SetAttribute(attribute.Name, value);
        }

        public BoolResult ModifyActiveDirectoryGroupUsers(ActiveDirectoryGroupChangeDto activeDirectoryGroupChange)
        {
            if (string.IsNullOrWhiteSpace(GetAllowedWritableObjectsAsString()))
            {
                return new BoolResult(true);
            }

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var dbGroup = unitOfWork.Repositories.ActiveDirectoryGroups.GetByIdOrDefault(activeDirectoryGroupChange.GroupId);

                var allowedWritableObjects = GetAllowedWritableObjects();

                if (allowedWritableObjects != null && !allowedWritableObjects.Contains(dbGroup.ActiveDirectoryId))
                {
                    return new BoolResult(true);
                }

                using (var coreDirectoryContext = CreateWritableCoreDirectoryContext(useCustomResultMapper: true, useCustomChangeTracker: true))
                {
                    var adGroup = coreDirectoryContext.Groups.SingleOrDefault(g => g.Id == dbGroup.ActiveDirectoryId);

                    if (adGroup == null)
                    {
                        _logger.Info($"Couldn't find group in ActiveDirectory: {activeDirectoryGroupChange.GroupId}, Sync. failed");

                        return new BoolResult(false, new List<AlertDto> { new AlertDto { Message = ActiveDirectoryResources.CouldNotFindGroupInActiveDirectory, Type = AlertType.Warning } });
                    }

                    var recordChanged = false;

                    if (activeDirectoryGroupChange.OwnerId?.IsSet ?? false)
                    {
                        var dbOwnerId = unitOfWork.Repositories.Users
                            .Where(u => u.ActiveDirectoryId.HasValue && u.Id == activeDirectoryGroupChange.OwnerId.Value)
                            .Select(u => u.ActiveDirectoryId.Value)
                            .SingleOrDefault();
                        var adOwner = coreDirectoryContext.Users.SingleOrDefault(u => u.Id == dbOwnerId);

                        ModifyActiveDirectoryAttribute(adGroup, g => g.OwnerDn, adOwner?.InternalDn);
                        ModifyActiveDirectoryAttribute(adGroup, g => g.OwnerId, null);

                        recordChanged = true;
                    }

                    if (activeDirectoryGroupChange.ManagersIds?.IsSet ?? false)
                    {
                        var managersIds = unitOfWork.Repositories.Users
                            .Where(u => activeDirectoryGroupChange.ManagersIds.Value.Contains(u.Id) && u.ActiveDirectoryId.HasValue)
                            .Select(u => u.ActiveDirectoryId.Value)
                            .ToList();

                        adGroup.ClearManagersIds();
                        managersIds.ForEach(id => adGroup.AddManagerId(id.ToString()));

                        recordChanged = true;
                    }

                    if (activeDirectoryGroupChange.MembersToAddIds?.IsSet ?? false)
                    {
                        var userIds = unitOfWork.Repositories.Users
                            .Where(u => activeDirectoryGroupChange.MembersToAddIds.Value.Contains(u.Id) && u.ActiveDirectoryId.HasValue)
                            .Select(u => u.ActiveDirectoryId.Value)
                            .ToHashSet();

                        foreach (var userId in userIds.ToList())
                        {
                            if (coreDirectoryContext.Users.Count(u => u.Id == userId) == 0)
                            {
                                userIds.Remove(userId);
                            }
                        }

                        coreDirectoryContext.AddMembersToGroup(adGroup.Id, userIds);

                        recordChanged = true;
                    }

                    if (activeDirectoryGroupChange.MembersToRemoveIds?.IsSet ?? false)
                    {
                        var userIds = unitOfWork.Repositories.Users
                            .Where(u => activeDirectoryGroupChange.MembersToRemoveIds.Value.Contains(u.Id) && u.ActiveDirectoryId.HasValue)
                            .Select(u => u.ActiveDirectoryId.Value)
                            .ToHashSet();

                        foreach (var userId in userIds.ToList())
                        {
                            if (coreDirectoryContext.Users.Count(u => u.Id == userId) == 0)
                            {
                                userIds.Remove(userId);
                            }
                        }

                        coreDirectoryContext.RemoveMembersFromGroup(adGroup.Id, userIds);

                        recordChanged = true;
                    }

                    if (activeDirectoryGroupChange.Description?.IsSet ?? false)
                    {
                        ModifyActiveDirectoryAttribute(adGroup, g => g.Description, activeDirectoryGroupChange.Description.Value);

                        recordChanged = true;
                    }

                    if (activeDirectoryGroupChange.Info?.IsSet ?? false)
                    {
                        ModifyActiveDirectoryAttribute(adGroup, g => g.Info, activeDirectoryGroupChange.Info.Value);

                        recordChanged = true;
                    }

                    if (recordChanged)
                    {
                        coreDirectoryContext.SubmitChanges();
                    }

                    return new BoolResult(true);
                }
            }
        }

        private void LogActiveDirectoryChange(string field, string oldValue, string newValue, string userName)
        {
            var currentUser = _principalProvider.Current.Login;

            _logger.Info(string.Format(ActiveDirectoryWriterChangeLoggerInfoFormat, currentUser, field, oldValue,
                newValue, userName));
        }

        private CoreDirectoryContext CreateWritableCoreDirectoryContext(bool useCustomResultMapper = false, bool useCustomChangeTracker = false)
        {
            var allowedWritableObjects = GetAllowedWritableObjects();

            return new CoreDirectoryContext(_settingsProvider, true, allowedWritableObjects, _logger, useCustomResultMapper, useCustomChangeTracker);
        }

        private static string GetDisplayName(CoreDirectoryContext context, string firstName, string lastName)
        {
            var displayName = $"{firstName} {lastName}";
            var users = context.Users.Where(u => u.FirstName == firstName && u.LastName == lastName).ToList();

            for (int i = 2; users.Any(u => u.DisplayName == displayName); ++i)
            {
                displayName = $"{firstName} {lastName} ({i})";
            }

            return displayName;
        }

        private ISet<Guid> GetAllowedWritableObjects()
        {
            var allowedWritableObjectsAsString = GetAllowedWritableObjectsAsString();

            ISet<Guid> allowedWritableObjects = allowedWritableObjectsAsString == AllWritesAllowedSymbol
                ? null
                : allowedWritableObjectsAsString.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(Guid.Parse)
                    .ToHashSet();

            return allowedWritableObjects;
        }

        private string GetAllowedWritableObjectsAsString()
        {
            var result = _systemParameterService.GetParameter<string>(ParameterKeys.ActiveDirectoryWritableObjects);

            return result;
        }

        private CompanyDto GetCompanyByIdOrDefault(long? companyId)
        {
            return _companyService.GetAllCompanies().FirstOrDefault(c => c.Id == companyId);
        }

        private static string GetCompanyCode(CompanyDto company)
        {
            const string noCompanyCode = "NotApplicable";

            return company != null
                ? company.ActiveDirectoryCode
                : noCompanyCode;
        }

        private static string GetDepartment(IUnitOfWork<IAllocationDbScope> unitOfWork, long orgUnitId)
        {
            return unitOfWork.Repositories.OrgUnits.Where(o => o.Id == orgUnitId).Select(o => o.Code).Single();
        }

        private LocationDto GetLocationByIdOrDefault(long locationId)
        {
            return _locationService.GetAllLocations().FirstOrDefault(l => l.Id == locationId);
        }

        private static string GetMainCompetences(IUnitOfWork<IAllocationDbScope> unitOfWork, long[] jobProfileIds)
        {
            return string.Join(",", unitOfWork.Repositories.JobProfiles.GetByIds(jobProfileIds).Select(p => p.Name).ToList());
        }

        private static string GetJobMatrixLevel(IUnitOfWork<IAllocationDbScope> unitOfWork, long[] jobMatrixLevelId)
        {
            var jobMatrixLevelCodes = unitOfWork.Repositories.JobMatrixLevels.GetByIds(jobMatrixLevelId).Select(c => c.Code).ToList();

            return jobMatrixLevelCodes.Any() ? string.Join(",", jobMatrixLevelCodes) : null;
        }

        private int? GetUidNumber(CoreDirectoryContext context)
        {
            var path = _systemParameterService.GetParameter<string>(ParameterKeys.ActiveDirectoryDomainInfoPath);

            return context.DomainInfos.Where(i => i.Path == path).Select(i => i.MaxUidNumber).SingleOrDefault();
        }

        private static void FillCompanySpecificFields(ActiveDirectoryUserCreationDto activeDirectoryUserCreationDto,
            User user, CompanyDto company)
        {
            const string blStreamCode = "BlStream";

            if (company.ActiveDirectoryCode != blStreamCode)
            {
                return;
            }

            user.Gid = "100";
            user.HomeDirectory = $"/home/users/{activeDirectoryUserCreationDto.Login}";
            user.LoginShell = "/usr/bin/scponly";
        }

        private void FillEmploymentTypeSpecificFields(User user, EmploymentType? employmentType, out string parentPath)
        {
            if (employmentType == EmploymentType.External)
            {
                parentPath = _systemParameterService.GetParameter<string>(ParameterKeys.ActiveDirectoryExternalPath);

                return;
            }

            user.RadiusTunnelType = "VLAN";
            user.RadiusTunnelMediumType = "TMT802";
            user.RadiusTunnelPrivateGroupId = "2000";

            parentPath = _systemParameterService.GetParameter<string>(ParameterKeys.ActiveDirectoryEmployeePath);
        }

        private string EncodeActiveDirectoryUserReference(IUnitOfWork<IAllocationDbScope> unitOfWork, long id)
        {
            var userEncode = unitOfWork.Repositories.Employees.GetById(id);
            return $"{userEncode.FullName} | {userEncode.User.ActiveDirectoryId}";
        }

        private Guid? GetActivatedUserGuid(string userName, string password)
        {
            var settings = _settingsProvider.AuthorizationProviderSettings.ActiveDirectoryWriterSettings;

            using (var principalContext = new PrincipalContext(ContextType.Domain, null, settings.User, settings.Password))
            {
                using (var userPrincipal = UserPrincipal.FindByIdentity(principalContext, userName))
                {
                    if (userPrincipal == null)
                    {
                        return null;
                    }

                    userPrincipal.SetPassword(password);

                    userPrincipal.Enabled = true;
                    userPrincipal.PasswordNotRequired = false;

                    userPrincipal.Save();

                    return userPrincipal.Guid;
                }
            }
        }

        private string GeneratePassword()
        {
            // Length of this string should be divisor of 256 due to security reasons
            const string charactersSet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_-";

            string password;
            var data = new byte[16];

            do
            {
                _randomNumberGenerator.GetBytes(data);

                password = new string(data.Select(c => charactersSet[c % charactersSet.Length]).ToArray());
            }
            while (!password.Any(c => char.IsDigit(c)));

            return password;
        }
    }
}
