﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Dictionaries.Dto;

namespace Smt.Atomic.Business.Dictionaries.Interfaces
{
    public interface ILocationCardIndexDataService : ICardIndexDataService<LocationDto>
    {
        LocationDto GetLocationForEmployeeOrDefault(long employeeId);
    }
}

