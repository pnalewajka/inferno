﻿using System.Collections.Generic;
using Smt.Atomic.Business.Dictionaries.Dto;

namespace Smt.Atomic.Business.Dictionaries.Interfaces
{
    public interface ICityService
    {
        CityDto GetCityOrDefaultById(long id);

        CityDto GetCityOrDefaultByLocationId(long id);

        IList<CityDto> GetAllCities();

        IList<CityDto> GetCitiesByIds(long[] ids);
    }
}
