﻿using System.Collections.Generic;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Dictionaries.Interfaces
{
    public interface ILocationService
    {
        LocationDto GetLocationOrDefaultById(long id);

        IList<LocationDto> GetAllLocations();

        bool CheckIfPlaceOfWorkIsAllowedForLocation(long locationId, PlaceOfWork placeOfWork);

        IList<LocationDto> GetLocationsOrDefaultByIds(long[] ids);
    }
}
