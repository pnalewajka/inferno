﻿using System.Collections.Generic;
using Smt.Atomic.Business.Dictionaries.Dto;

namespace Smt.Atomic.Business.Dictionaries.Interfaces
{
    public interface ICurrencyService
    {
        CurrencyDto GetCurrencyById(long currencyId);

        IDictionary<string, long> GetCurrencyIdByIsoCodes();

        ICollection<CompanyAllowedAdvancePaymentCurrencyDto> GetAllowedAdvancedPaymentCurrenciesByCompanyId(long companyId);

        ICollection<CompanyAllowedAdvancePaymentCurrencyDto> GetAllowedAdvancedPaymentCurrenciesByBtParticipantId(long participantId);
    }
}
