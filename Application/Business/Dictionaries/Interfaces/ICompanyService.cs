﻿using System.Collections.Generic;
using Smt.Atomic.Business.Dictionaries.Dto;

namespace Smt.Atomic.Business.Dictionaries.Interfaces
{
    public interface ICompanyService
    {
        CompanyDto GetCompanyOrDefaultById(long id);

        IList<CompanyDto> GetAllCompanies();

        CompanyDto GetCompanyOrDefaultByEmployeeId(long employeeId);

        CompanyDto GetCompanyOrDefaultByBtParticipantId(long participantId);

        long? GetCompanyIdOrDefaultByActiveDirectorycode(string activeDirectoryCode);
    }
}