﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.Dictionaries.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.Dictionaries
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            if (containerType == ContainerType.WebApp)
            {
                container.Register(Component.For<ICityService>().ImplementedBy<CityService>().LifestyleTransient());
                container.Register(Component.For<ICityCardIndexDataService>().ImplementedBy<CityCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ICountryCardIndexDataService>().ImplementedBy<CountryCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ILocationCardIndexDataService>().ImplementedBy<LocationCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IIndustryCardIndexDataService>().ImplementedBy<IndustryCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ILanguageCardIndexDataService>().ImplementedBy<LanguageCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IRegionCardIndexDataService>().ImplementedBy<RegionCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IServiceLineCardIndexDataService>().ImplementedBy<ServiceLineCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ISoftwareCategoryCardIndexDataService>().ImplementedBy<SoftwareCategoryCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ICustomerSizeCardIndexDataService>().ImplementedBy<CustomerSizeCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ICompanyCardIndexDataService>().ImplementedBy<CompanyCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ICompanyService>().ImplementedBy<CompanyService>().LifestyleTransient());
                container.Register(Component.For<IUtilizationCategoryCardIndexDataService>().ImplementedBy<UtilizationCategoryCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ILanguageWithLevelCardIndexDataService>().ImplementedBy<LanguageWithLevelCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ICurrencyCardIndexDataService>().ImplementedBy<CurrencyCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<ICurrencyService>().ImplementedBy<CurrencyService>().LifestylePerWebRequest());
            }

            if (containerType == ContainerType.JobScheduler)
            {
                container.Register(Component.For<ICityService>().ImplementedBy<CityService>().LifestyleTransient());
                container.Register(Component.For<ICompanyService>().ImplementedBy<CompanyService>().LifestyleTransient());
                container.Register(Component.For<ICurrencyService>().ImplementedBy<CurrencyService>().LifestylePerThread());
            }
        }
    }
}