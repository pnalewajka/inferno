﻿using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Business.Dictionaries.BusinessLogics
{
    public static class CityBusinessLogic
    {
        public static readonly BusinessLogic<City, string> DisplayName =
            new BusinessLogic<City, string>(city => city.Name + ", " + city.Country.Name);
    }
}
