﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.Dictionaries.BusinessLogic
{
    public static class LanguageWithLevelBusinessLogic
    {
        private static long ModuloLanguageLevelValue = (long)LanguageReferenceLevel.C2 + 1;

        public static NamedFilter<Employee> EmployeeFilter =>
            new NamedFilter<Employee, long[]>(
                FilterCodes.LanguagesFilter,
                (e, languageIds) => e.Resumes.Any(
                    r => r.ResumeLanguages.Any(
                            l => languageIds.Contains(l.LanguageId * ModuloLanguageLevelValue + (int)l.Level))));

        public static long GetId(long languageId, LanguageReferenceLevel level)
        {
            return ModuloLanguageLevelValue * languageId + (int)level;
        }

        public static long GetLanguageId(long languageWithLevelId)
        {
            return (languageWithLevelId - (languageWithLevelId % ModuloLanguageLevelValue)) / ModuloLanguageLevelValue;
        }

        public static LanguageReferenceLevel GetLevel(long languageWithLevelId)
        {
            return (LanguageReferenceLevel)(languageWithLevelId % ModuloLanguageLevelValue);
        }
    }
}
