﻿using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Business.Dictionaries.BusinessLogics
{
    public static class LocationBusinessLogic
    {
        public static readonly BusinessLogic<Location, Country> Country =
            new BusinessLogic<Location, Country>(location =>
                location.City != null ? location.City.Country : location.Country);

        public static readonly BusinessLogic<Location, long?> CountryId =
            new BusinessLogic<Location, long?>(location =>
                location.City != null ? location.City.CountryId : location.CountryId);
    }
}
