﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Dictionaries.Services
{
    public class LanguageCardIndexDataService : CardIndexDataService<LanguageDto, Language, IDictionariesDbScope>, ILanguageCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public LanguageCardIndexDataService(ICardIndexServiceDependencies<IDictionariesDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override IEnumerable<Expression<Func<Language, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return a => a.NameEn;
            yield return a => a.NamePl;
        }
    }
}


