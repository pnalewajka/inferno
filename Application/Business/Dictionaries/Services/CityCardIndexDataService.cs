﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.Dictionaries.Resources;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Dictionaries.Services
{
    public class CityCardIndexDataService : CardIndexDataService<CityDto, City, IDictionariesDbScope>, ICityCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public CityCardIndexDataService(ICardIndexServiceDependencies<IDictionariesDbScope> dependencies)
            : base(dependencies)
        {
        }

        public override IQueryable<City> ApplyContextFiltering(IQueryable<City> records, object context)
        {
            if (context is ParentIdContext parentIdContext && parentIdContext.ParentId != default(long))
            {
                records = records.Where(c => c.CountryId == parentIdContext.ParentId);
            }

            return base.ApplyContextFiltering(records, context);
        }

        protected override IOrderedQueryable<City> GetDefaultOrderBy(IQueryable<City> records, QueryCriteria criteria)
        {
            return records.OrderBy(c => c.Name);
        }

        protected override IEnumerable<Expression<Func<City, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return c => c.Name;
            yield return c => c.Country.Name;
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<City, IDictionariesDbScope> eventArgs)
        {
            if (eventArgs.DeletingRecord.UsedInBusinessTripParticipantAsDeparture.Any() || eventArgs.DeletingRecord.UsedInBusinessTripAsDestination.Any())
            {
                eventArgs.Canceled = true;
                eventArgs.AddError(CityResources.ErrorUsedInBusinessTrips);

                return;
            }

            if (eventArgs.DeletingRecord.UsedInBusinessTripRequestAsDeparture.Any() || eventArgs.DeletingRecord.UsedInBusinessTripRequestAsDestination.Any())
            {
                eventArgs.Canceled = true;
                eventArgs.AddError(CityResources.ErrorUsedInBusinessTripRequests);

                return;
            }

            if (eventArgs.DeletingRecord.Locations.Any())
            {
                eventArgs.Canceled = true;
                eventArgs.AddError(CityResources.ErrorUsedInLocations);

                return;
            }

            base.OnRecordDeleting(eventArgs);
        }
    }
}
