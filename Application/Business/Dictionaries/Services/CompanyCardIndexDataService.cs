﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.Dictionaries.Resources;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Dictionaries.Services
{
    public class CompanyCardIndexDataService : CardIndexDataService<CompanyDto, Company, IDictionariesDbScope>, ICompanyCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public CompanyCardIndexDataService(ICardIndexServiceDependencies<IDictionariesDbScope> dependencies)
            : base(dependencies)
        {
            RelatedCacheAreas = new[] { CacheAreas.Companies };
        }

        protected override IEnumerable<Expression<Func<Company, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return c => c.Name;
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<Company, IDictionariesDbScope> eventArgs)
        {
            if (eventArgs.DeletingRecord.UsedInEmployees.Any())
            {
                eventArgs.Canceled = true;
                eventArgs.AddError(CompanyResources.ErrorUsedInEmployees);

                return;
            }

            if (eventArgs.DeletingRecord.UsedInAddEmployeeRequests.Any())
            {
                eventArgs.Canceled = true;
                eventArgs.AddError(CompanyResources.ErrorUsedInAddEmployeeRequests);

                return;
            }

            if (eventArgs.DeletingRecord.UsedInEmploymentPeriods.Any())
            {
                eventArgs.Canceled = true;
                eventArgs.AddError(CompanyResources.ErrorUsedInEmploymentPeriods);

                return;
            }

            if (eventArgs.DeletingRecord.UsedInEmploymentDataChangeRequests.Any())
            {
                eventArgs.Canceled = true;
                eventArgs.AddError(CompanyResources.ErrorUsedInEmploymentDataChangeRequests);

                return;
            }

            base.OnRecordDeleting(eventArgs);
        }

        protected override IOrderedQueryable<Company> GetDefaultOrderBy(IQueryable<Company> records, QueryCriteria criteria)
        {
            return records.OrderBy(c => c.CustomOrder ?? long.MaxValue);
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<Company, CompanyDto, IDictionariesDbScope> eventArgs)
        {
            eventArgs.InputEntity.TravelExpensesSpecialists =
                EntityMergingService.CreateEntitiesFromIds<Employee, IDictionariesDbScope>(
                    eventArgs.UnitOfWork, eventArgs.InputEntity.TravelExpensesSpecialists.GetIds());
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<Company, CompanyDto, IDictionariesDbScope> eventArgs)
        {
            EntityMergingService.MergeCollections(
                eventArgs.UnitOfWork,
                eventArgs.DbEntity.TravelExpensesSpecialists,
                eventArgs.InputEntity.TravelExpensesSpecialists);

            EntityMergingService.MergeCollections(
                eventArgs.UnitOfWork,
                eventArgs.DbEntity.AllowedAdvancedPaymentCurrencies,
                eventArgs.InputEntity.AllowedAdvancedPaymentCurrencies,
                e => new { e.CurrencyId, e.CompanyId, e.MinimumValue }, true);
        }
    }
}