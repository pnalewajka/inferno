﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Dictionaries.BusinessLogic;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Dictionaries.Services
{
    public class LanguageWithLevelCardIndexDataService : CustomDataCardIndexDataService<LanguageWithLevelDto>, ILanguageWithLevelCardIndexDataService
    {
        private readonly IUnitOfWorkService<IDictionariesDbScope> _unitOfWorkDictionaries;

        public LanguageWithLevelCardIndexDataService(IUnitOfWorkService<IDictionariesDbScope> unitOfWorkDictionaries)
        {
            _unitOfWorkDictionaries = unitOfWorkDictionaries;
        }

        protected override IEnumerable<Expression<Func<LanguageWithLevelDto, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return dto => dto.NameAndLevelPl;
            yield return dto => dto.NameAndLevelEng;
        }

        protected override IList<LanguageWithLevelDto> GetAllRecords(object context = null)
        {
            using (var unitOfWork = _unitOfWorkDictionaries.Create())
            {
                var languages = unitOfWork.Repositories.Languages.AsNoTracking().ToList();
                var languageReferenceLevels = EnumHelper.GetEnumValues<LanguageReferenceLevel>();
                return languages
                    .SelectMany(language => languageReferenceLevels.Where(l => l != LanguageReferenceLevel.None)
                        .Select(level =>
                            new LanguageWithLevelDto()
                            {
                                Id = LanguageWithLevelBusinessLogic.GetId(language.Id, level),
                                NameAndLevel = new LocalizedString
                                {
                                    English = $"{language.NameEn} {level.GetDescriptionOrValue(CultureInfo.GetCultureInfo(CultureCodes.English))}",
                                    Polish = $"{language.NamePl} {level.GetDescriptionOrValue(CultureInfo.GetCultureInfo(CultureCodes.Polish))}"
                                },
                                NameAndLevelEng = $"{language.NameEn} {level.GetDescriptionOrValue(CultureInfo.GetCultureInfo(CultureCodes.English))}",
                                NameAndLevelPl = $"{language.NamePl} {level.GetDescriptionOrValue(CultureInfo.GetCultureInfo(CultureCodes.Polish))}",
                                Priority = language.Priority
                            })).OrderByDescending(r => r.Priority ?? int.MaxValue).ThenBy(r => r.NameAndLevelEng).ToList();
            }
        }

        public override LanguageWithLevelDto GetRecordById(long id)
        {
            return GetAllRecords().Single(r => r.Id == id);
        }

        public override LanguageWithLevelDto GetRecordByIdOrDefault(long id)
        {
            return GetAllRecords().SingleOrDefault(r => r.Id == id);
        }
    }
}
