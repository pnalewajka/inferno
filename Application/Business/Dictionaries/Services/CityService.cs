﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Dictionaries.Services
{
    public class CityService : ICityService
    {
        private readonly IUnitOfWorkService<IDictionariesDbScope> _unitOfWorkService;
        private readonly IClassMapping<City, CityDto> _cityToCityDtoClassMapping;

        public CityService(
            IUnitOfWorkService<IDictionariesDbScope> unitOfWorkService,
            IClassMapping<City, CityDto> cityToCityDtoClassMapping)
        {
            _unitOfWorkService = unitOfWorkService;
            _cityToCityDtoClassMapping = cityToCityDtoClassMapping;
        }

        [Cached(CacheArea = CacheAreas.Companies, ExpirationInSeconds = 600)]
        public CityDto GetCityOrDefaultById(long id)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var city = unitOfWork.Repositories.Cities.GetByIdOrDefault(id);

                return city != null ? _cityToCityDtoClassMapping.CreateFromSource(city) : null;
            }
        }

        [Cached(CacheArea = CacheAreas.Companies, ExpirationInSeconds = 600)]
        public CityDto GetCityOrDefaultByLocationId(long id)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var city = unitOfWork.Repositories.Locations.SelectById(id, l => l.City);

                return city != null ? _cityToCityDtoClassMapping.CreateFromSource(city) : null;
            }
        }

        [Cached(CacheArea = CacheAreas.Companies, ExpirationInSeconds = 600)]
        public IList<CityDto> GetAllCities()
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Cities
                    .AsNoTracking().AsEnumerable().Select(_cityToCityDtoClassMapping.CreateFromSource).ToList();
            }
        }

        public IList<CityDto> GetCitiesByIds(long[] ids)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Cities.GetByIds(ids)
                                                     .AsEnumerable()
                                                     .Select(_cityToCityDtoClassMapping.CreateFromSource)
                                                     .ToList();
            }
        }
    }
}
