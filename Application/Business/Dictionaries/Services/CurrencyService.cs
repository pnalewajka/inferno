﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Dictionaries.Services
{
    public class CurrencyService
        : ICurrencyService
    {
        private readonly IUnitOfWorkService<IBusinessTripsDbScope> _unitOfWorkService;
        private readonly IClassMapping<Currency, CurrencyDto> _currencyToDtoMapping;
        private readonly IClassMapping<CompanyAllowedAdvancePaymentCurrency, CompanyAllowedAdvancePaymentCurrencyDto> _allowedCurrencyToDtoMapping;

        public CurrencyService(
            IUnitOfWorkService<IBusinessTripsDbScope> unitOfWorkService,
            IClassMapping<Currency, CurrencyDto> currencyToDtoMapping,
            IClassMapping<CompanyAllowedAdvancePaymentCurrency, CompanyAllowedAdvancePaymentCurrencyDto> allowedCurrencyToDtoMapping)
        {
            _unitOfWorkService = unitOfWorkService;
            _currencyToDtoMapping = currencyToDtoMapping;
            _allowedCurrencyToDtoMapping = allowedCurrencyToDtoMapping;
        }

        [Cached(CacheArea = CacheAreas.Currencies, ExpirationInSeconds = 600)]
        public CurrencyDto GetCurrencyById(long currencyId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Currencies
                    .Where(c => c.Id == currencyId)
                    .Select(_currencyToDtoMapping.CreateFromSource)
                    .Single();
            }
        }

        [Cached(CacheArea = CacheAreas.Currencies, ExpirationInSeconds = 600)]
        public IDictionary<string, long> GetCurrencyIdByIsoCodes()
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Currencies.ToDictionary(c => c.IsoCode, c => c.Id);
            }
        }

        public ICollection<CompanyAllowedAdvancePaymentCurrencyDto> GetAllowedAdvancedPaymentCurrenciesByCompanyId(long companyId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Companies
                    .SelectById(companyId, c => c.AllowedAdvancedPaymentCurrencies)
                    .Select(_allowedCurrencyToDtoMapping.CreateFromSource)
                    .ToList();
            }
        }

        public ICollection<CompanyAllowedAdvancePaymentCurrencyDto> GetAllowedAdvancedPaymentCurrenciesByBtParticipantId(long participantId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.BusinessTripParticipants
                    .SelectById(participantId, c => c.Employee.Company.AllowedAdvancedPaymentCurrencies)
                    .Select(_allowedCurrencyToDtoMapping.CreateFromSource)
                    .ToList();
            }
        }
    }
}
