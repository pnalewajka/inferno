﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Dictionaries.Services
{
    public class ServiceLineCardIndexDataService : CardIndexDataService<ServiceLineDto, ServiceLine, IDictionariesDbScope>, IServiceLineCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public ServiceLineCardIndexDataService(ICardIndexServiceDependencies<IDictionariesDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override IEnumerable<Expression<Func<ServiceLine, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return a => a.Name;
        }

        protected override IOrderedQueryable<ServiceLine> GetDefaultOrderBy(IQueryable<ServiceLine> records, QueryCriteria criteria)
        {
            return records.OrderBy(r => r.Name);
        }
    }
}
