﻿
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Dictionaries.Services
{
    public class LocationService : ILocationService
    {
        private readonly ISystemParameterService _systemParameterService;
        private readonly IUnitOfWorkService<IDictionariesDbScope> _unitOfWorkService;
        private readonly IClassMapping<Location, LocationDto> _locationToDtoClassMapping;

        public LocationService(
            ISystemParameterService systemParameterService,
            IUnitOfWorkService<IDictionariesDbScope> unitOfWorkService,
            IClassMapping<Location, LocationDto> locationToDtoClassMapping)
        {
            _unitOfWorkService = unitOfWorkService;
            _systemParameterService = systemParameterService;
            _locationToDtoClassMapping = locationToDtoClassMapping;
        }

        [Cached(CacheArea = CacheAreas.Locations, ExpirationInSeconds = 600)]
        public LocationDto GetLocationOrDefaultById(long id)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var location = unitOfWork.Repositories.Locations.GetByIdOrDefault(id);

                return location != null ? _locationToDtoClassMapping.CreateFromSource(location) : null;
            }
        }

        [Cached(CacheArea = CacheAreas.Locations, ExpirationInSeconds = 600)]
        public IList<LocationDto> GetAllLocations()
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Locations.AsNoTracking()
                                                        .AsEnumerable()
                                                        .Select(_locationToDtoClassMapping.CreateFromSource)
                                                        .ToList();                
            }
        }

        public bool CheckIfPlaceOfWorkIsAllowedForLocation(long locationId, PlaceOfWork placeOfWork)
        {
            var location = GetLocationOrDefaultById(locationId);

            if (location == null)
            {
                return false;
            }

            var companyOfficeOnlyLocations = _systemParameterService.GetParameter<string[]>(ParameterKeys.OrganizationCompanyOfficeOnlyLocations);
            var clientOfficeOnlyLocations = _systemParameterService.GetParameter<string[]>(ParameterKeys.OrganizationClientOfficeOnlyLocations);

            return (placeOfWork == PlaceOfWork.CompanyOffice || !companyOfficeOnlyLocations.Contains(location.Name))
                && (placeOfWork == PlaceOfWork.ClientOffice || !clientOfficeOnlyLocations.Contains(location.Name));
        }

        public IList<LocationDto> GetLocationsOrDefaultByIds(long[] ids)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Locations.GetByIds(ids)
                                                        .AsEnumerable()
                                                        .Select(_locationToDtoClassMapping.CreateFromSource)
                                                        .ToList();
            }
        }
    }
}
