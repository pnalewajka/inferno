using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Dictionaries.Services
{
    public class CurrencyCardIndexDataService : CardIndexDataService<CurrencyDto, Currency, IBusinessTripsDbScope>, ICurrencyCardIndexDataService
    {
        private readonly ICurrencyService _currencyService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public CurrencyCardIndexDataService(
            ICardIndexServiceDependencies<IBusinessTripsDbScope> dependencies,
            ICurrencyService currencyService)
            : base(dependencies)
        {
            _currencyService = currencyService;

            RelatedCacheAreas = new[] { CacheAreas.Currencies };
        }

        protected override IEnumerable<Expression<Func<Currency, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return a => a.Name;
            yield return a => a.IsoCode;
            yield return a => a.Symbol;
        }

        public override IQueryable<Currency> ApplyContextFiltering(IQueryable<Currency> records, object context)
        {
            var id = (context as ParticipantContext)?.ParticipantId;

            if (id != null)
            {
                var allowedCurrencies = _currencyService
                    .GetAllowedAdvancedPaymentCurrenciesByBtParticipantId(id.Value)
                    .Select(c => c.Currency.Id);

                records = records.Where(c => allowedCurrencies.Contains(c.Id));
            }

            return records;
        }
    }
}