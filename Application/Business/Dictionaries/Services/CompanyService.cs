﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Dictionaries.Services
{
    public class CompanyService : ICompanyService
    {
        private readonly IUnitOfWorkService<IDictionariesDbScope> _unitOfWorkService;
        private readonly IClassMapping<Company, CompanyDto> _companyToCompanyDtoClassMapping;

        public CompanyService(
            IUnitOfWorkService<IDictionariesDbScope> unitOfWorkService,
            IClassMapping<Company, CompanyDto> companyToCompanyDtoClassMapping)
        {
            _unitOfWorkService = unitOfWorkService;
            _companyToCompanyDtoClassMapping = companyToCompanyDtoClassMapping;
        }

        [Cached(CacheArea = CacheAreas.Companies, ExpirationInSeconds = 600)]
        public CompanyDto GetCompanyOrDefaultById(long id)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var company = unitOfWork.Repositories.Companies.GetByIdOrDefault(id);

                return company != null ? _companyToCompanyDtoClassMapping.CreateFromSource(company) : null;
            }
        }

        [Cached(CacheArea = CacheAreas.Companies, ExpirationInSeconds = 600)]
        public IList<CompanyDto> GetAllCompanies()
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Companies
                    .AsNoTracking().AsEnumerable().Select(_companyToCompanyDtoClassMapping.CreateFromSource).ToList();
            }
        }

        [Cached(CacheArea = CacheAreas.Companies, ExpirationInSeconds = 600)]
        public CompanyDto GetCompanyOrDefaultByEmployeeId(long employeeId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var company = unitOfWork.Repositories.Companies
                    .AsNoTracking()
                    .Where(c => c.UsedInEmployees.Select(e => e.Id).Contains(employeeId)).SingleOrDefault();

                return company != null ? _companyToCompanyDtoClassMapping.CreateFromSource(company) : null;
            }
        }

        [Cached(CacheArea = CacheAreas.Companies, ExpirationInSeconds = 600)]
        public CompanyDto GetCompanyOrDefaultByBtParticipantId(long participantId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var company = unitOfWork.Repositories.BusinessTripParticipants
                    .AsNoTracking()
                    .SelectById(participantId, p => p.Employee.Company);

                return company != null ? _companyToCompanyDtoClassMapping.CreateFromSource(company) : null;
            }
        }

        [Cached(CacheArea = CacheAreas.Companies, ExpirationInSeconds = 600)]
        public long? GetCompanyIdOrDefaultByActiveDirectorycode(string activeDirectoryCode)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var companyId = unitOfWork.Repositories.Companies
                    .AsNoTracking()
                    .Where(c => c.ActiveDirectoryCode == activeDirectoryCode)
                    .Select(c => c.Id)
                    .SingleOrDefault();

                return companyId;
            }
        }
    }
}