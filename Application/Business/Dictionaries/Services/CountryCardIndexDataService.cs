﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.Dictionaries.Resources;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Dictionaries.Services
{
    public class CountryCardIndexDataService : CardIndexDataService<CountryDto, Country, IDictionariesDbScope>, ICountryCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public CountryCardIndexDataService(ICardIndexServiceDependencies<IDictionariesDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override IOrderedQueryable<Country> GetDefaultOrderBy(IQueryable<Country> records, QueryCriteria criteria)
        {
            return records.OrderBy(c => c.Name);
        }

        protected override IEnumerable<Expression<Func<Country, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return c => c.Name;
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<Country, IDictionariesDbScope> eventArgs)
        {
            if (eventArgs.DeletingRecord.Cities.Any())
            {
                eventArgs.Canceled = true;
                eventArgs.AddError(CountryResources.ErrorUsedInCities);

                return;
            }

            base.OnRecordDeleting(eventArgs);
        }
    }
}
