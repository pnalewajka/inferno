﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.Dictionaries.Resources;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Dictionaries.Services
{
    public class LocationCardIndexDataService : CardIndexDataService<LocationDto, Location, IDictionariesDbScope>, ILocationCardIndexDataService
    {
        private readonly IClassMapping<Location, LocationDto> _entityToDtoMapping;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public LocationCardIndexDataService(ICardIndexServiceDependencies<IDictionariesDbScope> dependencies)
            : base(dependencies)
        {
            _entityToDtoMapping = dependencies.ClassMappingFactory.CreateMapping<Location, LocationDto>();

            RelatedCacheAreas = new[] { CacheAreas.Locations };
        }

        public LocationDto GetLocationForEmployeeOrDefault(long employeeId)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var employee = unitOfWork.Repositories.Employees.Include(e => e.Location).GetById(employeeId);

                if (employee.Location != null)
                {
                    return _entityToDtoMapping.CreateFromSource(employee.Location);
                }

                return null;
            }
        }

        protected override IEnumerable<Expression<Func<Location, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return a => a.Name;
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<Location, IDictionariesDbScope> eventArgs)
        {
            if (eventArgs.DeletingRecord.UsedInEmployees.Any())
            {
                eventArgs.Canceled = true;
                eventArgs.AddError(LocationResources.ErrorUsedInEmployees);

                return;
            }

            if (eventArgs.DeletingRecord.UsedInAddEmployeeRequests.Any())
            {
                eventArgs.Canceled = true;
                eventArgs.AddError(LocationResources.ErrorUsedInAddEmployeeRequests);

                return;
            }

            base.OnRecordDeleting(eventArgs);
        }
    }
}
