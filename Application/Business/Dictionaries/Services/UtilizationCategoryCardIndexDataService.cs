﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.Dictionaries.Interfaces;
using Smt.Atomic.Business.Dictionaries.Resources;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Dictionaries.Services
{
    public class UtilizationCategoryCardIndexDataService : CardIndexDataService<UtilizationCategoryDto, UtilizationCategory, IDictionariesDbScope>, IUtilizationCategoryCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public UtilizationCategoryCardIndexDataService(ICardIndexServiceDependencies<IDictionariesDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override IEnumerable<Expression<Func<UtilizationCategory, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return a => a.NavisionCode;
            yield return a => a.NavisionName;
            yield return a => a.DisplayName;
        }

        public bool MustFilterInactiveUtilizationCategories { get; set; }

        public override CardIndexRecords<UtilizationCategoryDto> GetRecords(QueryCriteria criteria)
        {
            var result = base.GetRecords(criteria);

            if (MustFilterInactiveUtilizationCategories)
            {
                var inactives = result.Rows.Where(x => !x.IsActive).ToList();
                inactives.ForEach(x => result.Rows.Remove(x));
            }

            return result;
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<UtilizationCategory, IDictionariesDbScope> eventArgs)
        {
            if (eventArgs.DeletingRecord.UsedInProjects.Any())
            {
                eventArgs.Canceled = true;
                eventArgs.AddError(UtilizationCategoryResources.ErrorUsedInProjects);

                return;
            }

            base.OnRecordDeleting(eventArgs);
        }
    }
}
