﻿using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class CustomerSizeToCustomerSizeDtoMapping : ClassMapping<CustomerSize, CustomerSizeDto>
    {
        public CustomerSizeToCustomerSizeDtoMapping()
        {
            Mapping = e => new CustomerSizeDto
            {
                Id = e.Id,
                Name = e.Name,
                Description = e.Description,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
