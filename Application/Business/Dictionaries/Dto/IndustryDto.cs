﻿using Smt.Atomic.CrossCutting.Common.Models;
using System;
namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class IndustryDto
    {
        public long Id { get; set; }

        public LocalizedString Name { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
