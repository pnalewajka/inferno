﻿using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class CityDtoToCityMapping : ClassMapping<CityDto, City>
    {
        public CityDtoToCityMapping()
        {
            Mapping = d => new City
            {
                Id = d.Id,
                Name = d.Name,
                CountryId = d.CountryId,
                IsCompanyApartmentAvailable = d.IsCompanyApartmentAvailable,
                IsVoucherServiceAvailable = d.IsVoucherServiceAvailable,
                Timestamp = d.Timestamp
            };
        }
    }
}
