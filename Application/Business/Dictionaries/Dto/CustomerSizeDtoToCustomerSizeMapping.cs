﻿using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class CustomerSizeDtoToCustomerSizeMapping : ClassMapping<CustomerSizeDto, CustomerSize>
    {
        public CustomerSizeDtoToCustomerSizeMapping()
        {
            Mapping = d => new CustomerSize
            {
                Id = d.Id,
                Name = d.Name,
                Description = d.Description,
                Timestamp = d.Timestamp
            };
        }
    }
}
