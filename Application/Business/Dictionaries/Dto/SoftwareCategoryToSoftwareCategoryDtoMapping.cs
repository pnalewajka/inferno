﻿using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class SoftwareCategoryToSoftwareCategoryDtoMapping : ClassMapping<SoftwareCategory, SoftwareCategoryDto>
    {
        public SoftwareCategoryToSoftwareCategoryDtoMapping()
        {
            Mapping = e => new SoftwareCategoryDto
            {
                Id = e.Id,
                Name = e.Name,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
