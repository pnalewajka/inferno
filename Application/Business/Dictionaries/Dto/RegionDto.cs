﻿namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class RegionDto
    {
        public long Id { get; set; }

        public string Name { get; set; }
        
        public byte[] Timestamp { get; set; }
    }
}
