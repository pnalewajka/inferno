﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class LanguageToLanguageDtoMapping : ClassMapping<Language, LanguageDto>
    {
        public LanguageToLanguageDtoMapping()
        {
            Mapping = e => new LanguageDto
            {
                Id = e.Id,
                Name = new LocalizedString
                {
                    English = e.NameEn,
                    Polish = e.NamePl
                }
            };
        }
    }
}
