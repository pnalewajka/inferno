﻿namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class CityDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public long CountryId { get; set; }

        public string CountryName { get; set; }

        public bool IsCompanyApartmentAvailable { get; set; }

        public bool IsVoucherServiceAvailable { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
