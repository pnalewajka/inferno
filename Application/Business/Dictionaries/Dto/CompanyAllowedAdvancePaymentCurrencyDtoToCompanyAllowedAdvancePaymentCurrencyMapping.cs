﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class CompanyAllowedAdvancePaymentCurrencyDtoToCompanyAllowedAdvancePaymentCurrencyMapping : ClassMapping<CompanyAllowedAdvancePaymentCurrencyDto, CompanyAllowedAdvancePaymentCurrency>
    {
        public CompanyAllowedAdvancePaymentCurrencyDtoToCompanyAllowedAdvancePaymentCurrencyMapping()
        {
            Mapping = d => new CompanyAllowedAdvancePaymentCurrency
            {
                CompanyId = d.CompanyId,
                CurrencyId = d.Currency.Id,
                MinimumValue = d.MinimumValue,
            };
        }
    }
}
