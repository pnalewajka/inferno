﻿using System;

namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class CompanyAllowedAdvancePaymentCurrencyDto
    {
        public long CompanyId { get; set; }

        public CurrencyDto Currency { get; set; }

        public decimal MinimumValue { get; set; }
    }
}
