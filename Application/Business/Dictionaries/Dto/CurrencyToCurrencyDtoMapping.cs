﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class CurrencyToCurrencyDtoMapping : ClassMapping<Currency, CurrencyDto>
    {
        public CurrencyToCurrencyDtoMapping()
        {
            Mapping = e => new CurrencyDto
            {
                Id = e.Id,
                Name = e.Name,
                IsoCode = e.IsoCode,
                Symbol = e.Symbol,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
