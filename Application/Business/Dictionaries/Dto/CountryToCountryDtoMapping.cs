﻿using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class CountryToCountryDtoMapping : ClassMapping<Country, CountryDto>
    {
        public CountryToCountryDtoMapping()
        {
            Mapping = e => new CountryDto
            {
                Id = e.Id,
                Name = e.Name,
                IsoCode = e.IsoCode,
                Timestamp = e.Timestamp
            };
        }
    }
}
