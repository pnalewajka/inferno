﻿using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class SoftwareCategoryDtoToSoftwareCategoryMapping : ClassMapping<SoftwareCategoryDto, SoftwareCategory>
    {
        public SoftwareCategoryDtoToSoftwareCategoryMapping()
        {
            Mapping = d => new SoftwareCategory
            {
                Id = d.Id,
                Name = d.Name,
                Timestamp = d.Timestamp
            };
        }
    }
}
