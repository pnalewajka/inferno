﻿using System;
namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class SoftwareCategoryDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
