﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class RegionDtoToRegionMapping : ClassMapping<RegionDto, Region>
    {
        public RegionDtoToRegionMapping()
        {
            Mapping = d => new Region
            {
                Id = d.Id,
                Name = d.Name,
                Timestamp = d.Timestamp
            };
        }
    }
}
