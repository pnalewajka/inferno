﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class LanguageDtoToLanguageMapping : ClassMapping<LanguageDto, Language>
    {
        public LanguageDtoToLanguageMapping()
        {
            Mapping = d => new Language
            {
                Id = d.Id,
                NameEn = d.Name.English,
                NamePl = d.Name.Polish,
            };
        }
    }
}
