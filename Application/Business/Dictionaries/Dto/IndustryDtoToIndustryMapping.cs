﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class IndustryDtoToIndustryMapping : ClassMapping<IndustryDto, Industry>
    {
        public IndustryDtoToIndustryMapping()
        {
            Mapping = d => new Industry
            {
                Id = d.Id,
                NameEn = d.Name.English,
                NamePl = d.Name.Polish,
                Timestamp = d.Timestamp
            };
        }
    }
}
