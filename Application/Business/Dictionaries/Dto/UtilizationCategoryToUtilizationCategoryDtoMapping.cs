﻿using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class UtilizationCategoryToUtilizationCategoryDtoMapping : ClassMapping<UtilizationCategory, UtilizationCategoryDto>
    {
        public UtilizationCategoryToUtilizationCategoryDtoMapping()
        {
            Mapping = e => new UtilizationCategoryDto
            {
                Id = e.Id,
                NavisionCode = e.NavisionCode,
                NavisionName = e.NavisionName,
                DisplayName = e.DisplayName,
                Order = e.Order,
                IsActive = e.IsActive,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
