﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class CompanyAllowedAdvancePaymentCurrencyToCompanyAllowedAdvancePaymentCurrencyDtoMapping
        : ClassMapping<CompanyAllowedAdvancePaymentCurrency, CompanyAllowedAdvancePaymentCurrencyDto>
    {
        public CompanyAllowedAdvancePaymentCurrencyToCompanyAllowedAdvancePaymentCurrencyDtoMapping(
            IClassMapping<Currency, CurrencyDto> currencyToDto)
        {
            Mapping = e => new CompanyAllowedAdvancePaymentCurrencyDto
            {
                CompanyId = e.CompanyId,
                Currency = currencyToDto.CreateFromSource(e.Currency),
                MinimumValue = e.MinimumValue,
            };
        }
    }
}
