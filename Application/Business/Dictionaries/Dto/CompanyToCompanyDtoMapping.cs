﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class CompanyToCompanyDtoMapping : ClassMapping<Company, CompanyDto>
    {
        public CompanyToCompanyDtoMapping(
            IClassMapping<CompanyAllowedAdvancePaymentCurrency, CompanyAllowedAdvancePaymentCurrencyDto> currencyToDto)
        {
            Mapping = e => new CompanyDto
            {
                Id = e.Id,
                Name = e.Name,
                Description = e.Description,
                EmailDomain = e.EmailDomain,
                ActiveDirectoryCode = e.ActiveDirectoryCode,
                NavisionCode = e.NavisionCode,
                TravelExpensesSpecialistsEmployeeIds = e.TravelExpensesSpecialists.GetIds(),
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp,
                CustomOrder = e.CustomOrder,
                IsDataAdministrator = e.IsDataAdministrator,
                AllowedAdvancedPaymentCurrencies = e.AllowedAdvancedPaymentCurrencies
                    .Select(currencyToDto.CreateFromSource)
                    .ToList(),
                DefaultAdvancedPaymentCurrencyId = e.DefaultAdvancedPaymentCurrencyId,
                DefaultAdvancedPaymentDailyAmount = e.DefaultAdvancedPaymentDailyAmount,
                AdvancePaymentAcceptingEmployeeId = e.AdvancePaymentAcceptingEmployeeId,
                InvoiceInformation = e.InvoiceInformation,
                AllowedAccomodations = e.AllowedAccomodations,
                FrontDeskAssigneeEmployeeId = e.FrontDeskAssigneeEmployeeId,
                AllowedMeansOfTransport = e.AllowedMeansOfTransport,
            };
        }
    }
}
