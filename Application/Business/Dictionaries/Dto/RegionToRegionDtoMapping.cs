﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class RegionToRegionDtoMapping : ClassMapping<Region, RegionDto>
    {
        public RegionToRegionDtoMapping()
        {
            Mapping = e => new RegionDto
            {
                Id = e.Id,
                Name = e.Name,
                Timestamp = e.Timestamp
            };
        }
    }
}
