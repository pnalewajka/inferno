﻿using System.Collections.ObjectModel;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.BusinessTrips;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class CompanyDtoToCompanyMapping : ClassMapping<CompanyDto, Company>
    {
        public CompanyDtoToCompanyMapping(
            IClassMapping<CompanyAllowedAdvancePaymentCurrencyDto, CompanyAllowedAdvancePaymentCurrency> currencyDtoToEntity)
        {
            Mapping = d => new Company
            {
                Id = d.Id,
                Name = d.Name,
                Description = d.Description,
                EmailDomain = d.EmailDomain,
                ActiveDirectoryCode = d.ActiveDirectoryCode,
                NavisionCode = d.NavisionCode,
                TravelExpensesSpecialists = d.TravelExpensesSpecialistsEmployeeIds == null ? null
                    : d.TravelExpensesSpecialistsEmployeeIds.Select(m => new Employee { Id = m }).ToList(),
                Timestamp = d.Timestamp,
                CustomOrder = d.CustomOrder,
                IsDataAdministrator = d.IsDataAdministrator,
                AllowedAdvancedPaymentCurrencies = d.AllowedAdvancedPaymentCurrencies == null ? null
                    : d.AllowedAdvancedPaymentCurrencies.Select(currencyDtoToEntity.CreateFromSource).ToList(),
                DefaultAdvancedPaymentCurrencyId = d.DefaultAdvancedPaymentCurrencyId,
                DefaultAdvancedPaymentDailyAmount = d.DefaultAdvancedPaymentDailyAmount,
                AdvancePaymentAcceptingEmployeeId = d.AdvancePaymentAcceptingEmployeeId,
                InvoiceInformation = d.InvoiceInformation,
                AllowedAccomodations = d.AllowedAccomodations,
                FrontDeskAssigneeEmployeeId = d.FrontDeskAssigneeEmployeeId,
                AllowedMeansOfTransport = d.AllowedMeansOfTransport,
            };
        }
    }
}
