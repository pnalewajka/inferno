﻿using System;
namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class UtilizationCategoryDto
    {
        public long Id { get; set; }

        public string NavisionCode { get; set; }

        public string NavisionName { get; set; }

        public string DisplayName { get; set; }

        public int? Order { get; set; }

        public bool IsActive { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
