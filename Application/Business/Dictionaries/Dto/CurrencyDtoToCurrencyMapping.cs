﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class CurrencyDtoToCurrencyMapping : ClassMapping<CurrencyDto, Currency>
    {
        public CurrencyDtoToCurrencyMapping()
        {
            Mapping = d => new Currency
            {
                Id = d.Id,
                Name = d.Name,
                IsoCode = d.IsoCode,
                Symbol = d.Symbol,
                Timestamp = d.Timestamp
            };
        }
    }
}
