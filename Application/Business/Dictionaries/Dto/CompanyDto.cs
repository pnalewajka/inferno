﻿using System;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class CompanyDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string EmailDomain { get; set; }

        public string ActiveDirectoryCode { get; set; }

        public string NavisionCode { get; set; }

        public long[] TravelExpensesSpecialistsEmployeeIds { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public long? CustomOrder { get; set; }

        public bool IsDataAdministrator { get; set; }

        public ICollection<CompanyAllowedAdvancePaymentCurrencyDto> AllowedAdvancedPaymentCurrencies { get; set; }

        public long DefaultAdvancedPaymentCurrencyId { get; set; }

        public decimal DefaultAdvancedPaymentDailyAmount { get; set; }

        public long? AdvancePaymentAcceptingEmployeeId { get; set; }

        public string InvoiceInformation { get; set; }

        public AccommodationType AllowedAccomodations { get; set; }

        public long? FrontDeskAssigneeEmployeeId { get; set; }

        public MeansOfTransport AllowedMeansOfTransport { get; set; }
    }
}
