﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class LocationDtoToLocationMapping : ClassMapping<LocationDto, Location>
    {
        public LocationDtoToLocationMapping()
        {
            Mapping = d => new Location
            {
                Id = d.Id,
                Name = d.Name,
                Address = d.Address,
                CityId = d.CityId,
                CountryId = d.CountryId,
                Timestamp = d.Timestamp
            };
        }
    }
}
