﻿using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class CityToCityDtoMapping : ClassMapping<City, CityDto>
    {
        public CityToCityDtoMapping()
        {
            Mapping = e => new CityDto
            {
                Id = e.Id,
                Name = e.Name,
                CountryId = e.CountryId,
                CountryName = e.Country != null ? e.Country.Name : "",
                IsCompanyApartmentAvailable = e.IsCompanyApartmentAvailable,
                IsVoucherServiceAvailable = e.IsVoucherServiceAvailable,
                Timestamp = e.Timestamp
            };
        }
    }
}
