﻿namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class CountryDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string IsoCode { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
