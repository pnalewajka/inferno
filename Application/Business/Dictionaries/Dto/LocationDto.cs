﻿namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class LocationDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public long? CityId { get; set; }

        public long? CountryId { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
