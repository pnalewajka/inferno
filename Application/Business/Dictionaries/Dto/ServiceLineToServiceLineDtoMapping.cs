﻿using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class ServiceLineToServiceLineDtoMapping : ClassMapping<ServiceLine, ServiceLineDto>
    {
        public ServiceLineToServiceLineDtoMapping()
        {
            Mapping = e => new ServiceLineDto
            {
                Id = e.Id,
                Name = e.Name,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
