﻿using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class CountryDtoToCountryMapping : ClassMapping<CountryDto, Country>
    {
        public CountryDtoToCountryMapping()
        {
            Mapping = d => new Country
            {
                Id = d.Id,
                Name = d.Name,
                IsoCode = d.IsoCode,
                Timestamp = d.Timestamp
            };
        }
    }
}
