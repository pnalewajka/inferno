namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class ParticipantContext
    {
        public long? ParticipantId { get; set; }
    }
}
