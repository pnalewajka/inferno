﻿using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class LanguageDto
    {
        public long Id { get; set; }

        public LocalizedString Name { get; set; }
    }
}
