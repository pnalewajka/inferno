﻿using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class UtilizationCategoryDtoToUtilizationCategoryMapping : ClassMapping<UtilizationCategoryDto, UtilizationCategory>
    {
        public UtilizationCategoryDtoToUtilizationCategoryMapping()
        {
            Mapping = d => new UtilizationCategory
            {
                Id = d.Id,
                NavisionCode = d.NavisionCode,
                NavisionName = d.NavisionName,
                DisplayName = d.DisplayName,
                Order = d.Order,
                IsActive = d.IsActive,
                Timestamp = d.Timestamp
            };
        }
    }
}
