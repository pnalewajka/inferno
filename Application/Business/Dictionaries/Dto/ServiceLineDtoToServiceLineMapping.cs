﻿using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class ServiceLineDtoToServiceLineMapping : ClassMapping<ServiceLineDto, ServiceLine>
    {
        public ServiceLineDtoToServiceLineMapping()
        {
            Mapping = d => new ServiceLine
            {
                Id = d.Id,
                Name = d.Name,
                Timestamp = d.Timestamp
            };
        }
    }
}
