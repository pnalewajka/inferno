﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;

namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class LocationToLocationDtoMapping : ClassMapping<Location, LocationDto>
    {
        public LocationToLocationDtoMapping()
        {
            Mapping = e => new LocationDto
            {
                Id = e.Id,
                Name = e.Name,
                Address = e.Address,
                CityId = e.CityId,
                CountryId = e.CountryId,
                Timestamp = e.Timestamp
            };
        }
    }
}
