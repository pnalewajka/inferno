﻿using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class LanguageWithLevelDto
    {
        public long Id { get; set; }

        public LocalizedString NameAndLevel { get; set; }

        public string NameAndLevelEng { get; set; }

        public string NameAndLevelPl { get; set; }

        public int? Priority { get; set; }
    }
}
