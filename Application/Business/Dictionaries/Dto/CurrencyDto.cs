﻿using System;
namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class CurrencyDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string IsoCode { get; set; }

        public string Symbol { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
