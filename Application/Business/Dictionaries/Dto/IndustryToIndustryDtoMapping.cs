﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
namespace Smt.Atomic.Business.Dictionaries.Dto
{
    public class IndustryToIndustryDtoMapping : ClassMapping<Industry, IndustryDto>
    {
        public IndustryToIndustryDtoMapping()
        {
            Mapping = e => new IndustryDto
            {
                Id = e.Id,
                Name = new LocalizedString
                {
                    English = e.NameEn,
                    Polish = e.NamePl
                },
                Timestamp = e.Timestamp
            };
        }
    }
}
