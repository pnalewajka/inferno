﻿namespace Smt.Atomic.Business.WebSite.Consts
{
    internal static class DashboardGroupUrlParameters
    {
        public const string EmployeeUrlParameter = "emp";
        public const string LineManagerUrlParameter = "slm";
        public const string ProjectManagerUrlParameter = "pm";
    }
}
