﻿namespace Smt.Atomic.Business.WebSite.Consts
{
    internal static class WidgetColors
    {
        public const string MattRed = "#d9534f";
        public const string Red = "#ff4444";
        public const string DarkRed = "#cc0000";
        public const string MattOrange = "#f0ad4e";
        public const string Orange = "#ffbb33";
        public const string DarkOrange = "#ff8800";
        public const string MattGreen = "#5cb85c";
        public const string Green = "#00c851";
        public const string DarkGreen = "#007e33";
        public const string LightBlue = "#5bc0de";
        public const string MattBlue = "#428bca";
        public const string Blue = "#33b5e5";
        public const string DarkBlue = "#0099cc";

        public static string[] AsArray()
        {
            return new[]
            {
                Blue,
                MattRed,
                Green,
                MattOrange,
                DarkBlue,
                Red,
                MattGreen,
                Orange,
                MattBlue,
                DarkRed,
                DarkGreen,
                DarkOrange,
                LightBlue,
            };
        }
    }
}