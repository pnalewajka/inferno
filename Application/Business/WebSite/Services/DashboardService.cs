﻿using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.WebSite.Dto;
using Smt.Atomic.Business.WebSite.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Smt.Atomic.Business.WebSite.Services
{
    internal class DashboardService : IDashboardService
    {
        private readonly IEmployeeService _employeeService;
        private readonly IProjectService _projectService;
        private readonly LineManagerDashboardDataService _lineManagerDashboardDataService;
        private readonly ProjectManagerDashboardDataService _projectManagerDashboardDataService;
        private readonly EmployeeDashboardDataService _employeeDashboardDataService;

        public DashboardService(LineManagerDashboardDataService lineManagerDashboardDataService,
            ProjectManagerDashboardDataService projectManagerDashboardDataService,
            EmployeeDashboardDataService employeeDashboardDataService,
            IEmployeeService employeeService,
            IProjectService projectService)
        {
            _lineManagerDashboardDataService = lineManagerDashboardDataService;
            _projectManagerDashboardDataService = projectManagerDashboardDataService;
            _employeeDashboardDataService = employeeDashboardDataService;
            _employeeService = employeeService;
            _projectService = projectService;
        }

        public DashboardConfigurationDto GetDashboardsForEmployee(long employeeId)
        {
            return new DashboardConfigurationDto
            {
                DefaultDashboardGroupIndex = 0,
                DashboardGroups = GetDashboardGroups(employeeId).ToList()
            };
        }

        private IEnumerable<DashboardGroupDto> GetDashboardGroups(long employeeId)
        {
            var response = new List<DashboardGroupDto>();
            response.Add(_employeeDashboardDataService.GetDashboardGroup(employeeId));

            if (_projectService.HasProjects(employeeId))
            {
                response.Add(_projectManagerDashboardDataService.GetDashboardGroup(employeeId));
            }

            if (_employeeService.HasEmployees(employeeId))
            {
                response.Add(_lineManagerDashboardDataService.GetDashboardGroup(employeeId));
            }

            return response;
        }
    }
}