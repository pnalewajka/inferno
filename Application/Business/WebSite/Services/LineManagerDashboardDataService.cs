﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Business.WebSite.Consts;
using Smt.Atomic.Business.WebSite.Dto;
using Smt.Atomic.Business.WebSite.Enums;
using Smt.Atomic.Business.WebSite.Interfaces;
using Smt.Atomic.Business.WebSite.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Consts;


namespace Smt.Atomic.Business.WebSite.Services
{
    internal class LineManagerDashboardDataService : DashboardDataBaseService
    {
        private readonly IAllocationRequestService _allocationService;
        private readonly IEmployeeService _employeeService;

        public LineManagerDashboardDataService(ITimeService timeService,
            ITimeReportService timeReportService,
            ISystemParameterService systemParameterService,
            IAllocationRequestService allocationService,
            IEmployeeService employeeService)
            : base(timeService, timeReportService, systemParameterService)
        {
            _allocationService = allocationService;
            _employeeService = employeeService;
        }

        internal DashboardGroupDto GetDashboardGroup(long employeeId)
        {
            var response = new DashboardGroupDto
            {
                DisplayName = DashboardWidgetResources.LineManagerDashboardGroupName,
                UrlParameter = DashboardGroupUrlParameters.LineManagerUrlParameter
            };

            var widgets = new List<IWidgetDto>();
            widgets.AddRange(GetProjectsWidgets(employeeId));
            widgets.AddRange(GetTimeReportWidgets(employeeId));

            response.Widgets = widgets;

            return response;
        }

        private IEnumerable<IWidgetDto> GetTimeReportWidgets(long employeeId)
        {
            var currentDate = TimeService.GetCurrentDate();
            var myEmployeeReports =
                TimeReportService.GetMyEmployeesTimeReports(employeeId, currentDate.Year, (byte)(currentDate.Month));
            var response = new List<IWidgetDto>();
            var url = GetTimeReportUrl(currentDate.Year, currentDate.Month, TimeReportFilters.MyEmployees);

            if (myEmployeeReports != null)
            {
                var myEmployeesTimeReportsWidget = new PieWidgetDto
                {
                    Icon = Glyphicons.Time,
                    Title = DashboardWidgetResources.MyEmployeeTimeReportsWidgetTitle,
                    PieceLabel = PieceLabel.Value,
                    Datasets = new List<IChartWidgetValueDto>
                    {
                        new PieWidgetValueDto
                        {
                            Label = DashboardWidgetResources.MyEmployeeTimeReportsWidgetTitle,
                            Data = new List<decimal>
                            {
                                myEmployeeReports.Count(r => r.Status == TimeReportStatus.Submitted),
                                myEmployeeReports.Count(r => r.Status == TimeReportStatus.Accepted),
                                myEmployeeReports.Count(r => r.Status == TimeReportStatus.Rejected),
                                myEmployeeReports.Count(r => r.Status == TimeReportStatus.Draft),
                                myEmployeeReports.Count(r => r.Status == TimeReportStatus.NotStarted)
                            },
                            BackgroundColor = new List<string>
                            {
                                WidgetColors.MattOrange,
                                WidgetColors.MattBlue,
                                WidgetColors.MattGreen,
                                WidgetColors.MattRed,
                                WidgetColors.LightBlue
                            },
                            HoverBackgroundColor = new List<string>
                            {
                                WidgetColors.MattOrange,
                                WidgetColors.MattBlue,
                                WidgetColors.MattGreen,
                                WidgetColors.MattRed,
                                WidgetColors.LightBlue
                            }
                        }
                    },
                    Labels = new List<string>
                    {
                        DashboardWidgetResources.SubmittedReportLabel,
                        DashboardWidgetResources.AcceptedReportLabel,
                        DashboardWidgetResources.RejectedReportLabel,
                        DashboardWidgetResources.DraftReportLabel,
                        DashboardWidgetResources.NotStartedReportLabel
                    },
                    FooterHtmlMessage = $"<a href='{ url }' class='btn btn-default'>{DashboardWidgetResources.DetailsButton}</a>"
                };

                response.Add(myEmployeesTimeReportsWidget);
            }

            var myEmployeeIds = _employeeService.GetEmployeeIdsByLineManagerEmployeeId(employeeId).ToList();
            var overtimeTimeReportDate = currentDate;
            var overtimeTimeReportRows = TimeReportService.GetOvertimeTimeReportRows(null, myEmployeeIds, overtimeTimeReportDate.Year, (byte)overtimeTimeReportDate.Month);

            if (overtimeTimeReportRows.IsNullOrEmpty())
            {
                overtimeTimeReportDate = overtimeTimeReportDate.GetFirstDayInMonth().AddMonths(-1);
                overtimeTimeReportRows = TimeReportService.GetOvertimeTimeReportRows(null, myEmployeeIds, overtimeTimeReportDate.Year, (byte)overtimeTimeReportDate.Month);
            }

            var widgetOvertimeRows = GetRowsForTopOvertimeEmployees(overtimeTimeReportRows)
                .OrderBy(r => r.EmployeeId);

            var overtimeWidget = new BarWidgetDto
            {
                Icon = Glyphicons.Time,
                Title = $"{DashboardWidgetResources.MyEmployeesOvertimeLabel} {overtimeTimeReportDate.ToYearMonthDateFormat()}",
                IsStacked = true,
                Labels = widgetOvertimeRows.Select(r => r.EmployeeDisplayName).Distinct().ToList()
            };

            var datasets = GetDatasetsForOvertimeWidget(widgetOvertimeRows);

            overtimeWidget.Datasets = datasets;

            if (!datasets.IsNullOrEmpty())
            {
                response.Add(overtimeWidget);
            }

            return response;
        }

        private IEnumerable<IWidgetDto> GetProjectsWidgets(long employeeId)
        {
            const string employeeHubIdentifier = "EmployeeValuePickers.Employee";
            const string projectHubIdentifier = "EmployeeValuePickers.Project";

            var currentDate = TimeService.GetCurrentDate();
            var employeeIds = _employeeService.GetEmployeeIdsByLineManagerEmployeeId(employeeId);
            var managerProjects = employeeIds.SelectMany(e => _allocationService.GetAllocationRequests(e, currentDate, currentDate))
                .OrderByDescending(p => p.StartDate);
            var response = new List<IWidgetDto>();

            if (managerProjects.IsNullOrEmpty())
            {
                return response;
            }

            var myProjectsWidget = new TableWidgetDto
            {
                Title = DashboardWidgetResources.MyProjectsLabel,
                Icon = Glyphicons.Heart,
                Columns = new[]
                {
                    DashboardWidgetResources.EmployeeLabel,
                    DashboardWidgetResources.ProjectLabel,
                    DashboardWidgetResources.StartDateLabel,
                    DashboardWidgetResources.EndDateLabel,
                },
                Rows = managerProjects.Select(p => new TableWidgetRowDto
                {
                    Actions = new List<TableWidgetRowActionDto>
                    {
                        new TableWidgetRowActionDto
                        {
                            DataHubIdentifier = employeeHubIdentifier,
                            DataHubRecordId = p.EmployeeId,
                            Value = p.EmployeeFirstName + " " + p.EmployeeLastName,
                        },
                        new TableWidgetRowActionDto
                        {
                            DataHubIdentifier = projectHubIdentifier,
                            DataHubRecordId = p.ProjectId,
                            Value = p.ProjectName,
                        },
                        new TableWidgetRowActionDto
                        {
                            Value = p.StartDate.ToDefaultDateFormat(),
                        },
                        new TableWidgetRowActionDto
                        {
                            Value = p.EndDate?.ToDefaultDateFormat(),
                        },
                    },
                }).ToList(),
                WidgetFooterButton = new WidgetButtonDto
                {
                    Label = DashboardWidgetResources.GoToMyProjectsLabel,
                    UrlType = CrossCutting.Common.Enums.ActionUrlType.ListAllocations,
                    Parameters = new Dictionary<string, string>
                    {
                        ["filter"] = "my-employees",
                    },
                },
            };

            response.Add(myProjectsWidget);

            return response;
        }
    }
}
