﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Business.WebSite.Consts;
using Smt.Atomic.Business.WebSite.Dto;
using Smt.Atomic.Business.WebSite.Enums;
using Smt.Atomic.Business.WebSite.Interfaces;
using Smt.Atomic.Business.WebSite.Resources;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Consts;


namespace Smt.Atomic.Business.WebSite.Services
{
    internal class EmployeeDashboardDataService : DashboardDataBaseService
    {
        private readonly IAllocationRequestService _requestAllocationService;
        private readonly IVacationBalanceService _vacationBalanceService;
        private readonly IProjectService _projectService;
        private readonly IEmployeeService _employeeService;
        private readonly IOvertimeBankService _overtimeBankService;

        public EmployeeDashboardDataService(IVacationBalanceService vacationBalanceService,
            ITimeService timeService,
            ITimeReportService timeReportService,
            ISystemParameterService systemParameterService,
            IProjectService projectService,
            IEmployeeService employeeService,
            IOvertimeBankService overtimeBankService,
            IAllocationRequestService requestAllocationService)
            : base(timeService, timeReportService, systemParameterService)
        {
            _requestAllocationService = requestAllocationService;
            _vacationBalanceService = vacationBalanceService;
            _projectService = projectService;
            _employeeService = employeeService;
            _overtimeBankService = overtimeBankService;
        }

        internal DashboardGroupDto GetDashboardGroup(long employeeId)
        {
            var response = new DashboardGroupDto
            {
                DisplayName = DashboardWidgetResources.EmployeeDashboardGroupName,
                UrlParameter = DashboardGroupUrlParameters.EmployeeUrlParameter
            };

            var widgets = new List<IWidgetDto>();
            widgets.AddRange(GetVacationAndOvertimeBalanceWidgets(employeeId));
            widgets.AddRange(GetTimeReportWidgets(employeeId));
            widgets.AddRange(GetAllocationWidgets(employeeId));
            response.Widgets = widgets;

            return response;
        }

        private IEnumerable<IWidgetDto> GetTimeReportWidgets(long employeeId)
        {
            var currentDate = TimeService.GetCurrentDate();
            var mountsCount = 5;
            var dateFrom = currentDate.AddMonths(-mountsCount);
            var employeeRecentReports = TimeReportService.GetEmployeeTimeReports(employeeId, dateFrom, currentDate);
            var employeeCurrentReport = employeeRecentReports?.SingleOrDefault(r => r.Year == currentDate.Year && r.Month == currentDate.Month);
            var response = new List<IWidgetDto>();

            if (employeeCurrentReport != null)
            {
                var contractedHoursSummaryMessage = string.Format(DashboardWidgetResources.ContractedHoursSummary, employeeCurrentReport.ContractedHours);

                var timeTrackingProgressWidget = new PieWidgetDto
                {
                    Icon = Glyphicons.Time,
                    Title = DashboardWidgetResources.TimeTrackingProgressWidgetTitle,
                    PieceLabel = PieceLabel.Value,
                    Datasets = new List<IChartWidgetValueDto>
                    {
                        new PieWidgetValueDto
                        {
                            Label = DashboardWidgetResources.TimeTrackingProgressWidgetTitle,
                            Data = new List<decimal>
                            {
                                employeeCurrentReport.ReportedNormalHours,
                                employeeCurrentReport.ContractedHours - employeeCurrentReport.ReportedNormalHours
                            },
                            BackgroundColor = new List<string> { WidgetColors.MattOrange, WidgetColors.LightBlue },
                            HoverBackgroundColor = new List<string> { WidgetColors.MattOrange, WidgetColors.LightBlue }
                        }
                    },
                    Labels = new List<string>
                    {
                        DashboardWidgetResources.ReportedHoursLabel,
                        DashboardWidgetResources.HoursLeftLabel
                    },
                    FooterHtmlMessage = $"<p>{contractedHoursSummaryMessage}</p>"
                };

                var timeTrackingDetailsWidget = new BarWidgetDto
                {
                    Icon = Glyphicons.Time,
                    Title = DashboardWidgetResources.TimeTrackingDetailsWidgetTitle,
                    IsStacked = true,
                    Datasets = new List<IChartWidgetValueDto>
                    {
                        new BarWidgetValueDto
                        {
                            Label = DashboardWidgetResources.ContractedHoursLabel,
                            Data = employeeRecentReports.Select(r => r.ContractedHours).ToList(),
                            BackgroundColor = Enumerable.Repeat(WidgetColors.MattBlue, mountsCount).ToList(),
                            HoverBackgroundColor = Enumerable.Repeat(WidgetColors.MattBlue, mountsCount).ToList(),
                            StackName = "Group1"
                        },
                        new BarWidgetValueDto
                        {
                            Label = DashboardWidgetResources.ReportedHoursLabel,
                            Data = employeeRecentReports.Select(r => r.ReportedNormalHours).ToList(),
                            BackgroundColor = Enumerable.Repeat(WidgetColors.MattOrange, mountsCount).ToList(),
                            HoverBackgroundColor = Enumerable.Repeat(WidgetColors.MattOrange, mountsCount).ToList(),
                            StackName = "Group2"
                        },
                        new BarWidgetValueDto
                        {
                            Label = DashboardWidgetResources.OvertimeLabel,
                            Data = employeeRecentReports.Select(r => r.OvertimeHours).ToList(),
                            BackgroundColor = Enumerable.Repeat(WidgetColors.MattGreen, mountsCount).ToList(),
                            HoverBackgroundColor = Enumerable.Repeat(WidgetColors.MattGreen, mountsCount).ToList(),
                            StackName = "Group2"
                        }
                    },
                    Labels = employeeRecentReports.Select(r => $"{r.Month}/{r.Year}").ToList()
                };

                response.Add(timeTrackingProgressWidget);
                response.Add(timeTrackingDetailsWidget);
            }

            return response;
        }

        private IEnumerable<IWidgetDto> GetAllocationWidgets(long employeeId)
        {
            var currentDate = TimeService.GetCurrentDate();
            var firstDayOfCurrentMonth = DateExtensions.GetFirstDayInMonth(currentDate);
            var lastDayOfCurrentMonth = DateExtensions.GetLastDayInMonth(currentDate);
            var allocationRequests =
                _requestAllocationService.GetAllocationRequests(employeeId, firstDayOfCurrentMonth, lastDayOfCurrentMonth);
            var response = new List<IWidgetDto>();

            if (allocationRequests != null)
            {
                var allocationRequestsGouped = allocationRequests
                    .GroupBy(a => a.ProjectShortName)
                    .Select(a => new
                    {
                        ProjectShortName = a.Key,
                        Time = a.Sum(b => b.MondayHours + b.TuesdayHours + b.WednesdayHours + b.ThursdayHours + b.FridayHours + b.SaturdayHours + b.SundayHours)
                    });

                var timeTrackingProgressWidget = new PieWidgetDto
                {
                    Icon = Glyphicons.Time,
                    Title = DashboardWidgetResources.AllocationWidgetTitle,
                    PieceLabel = PieceLabel.Percentage,
                    Datasets = new List<IChartWidgetValueDto>
                    {
                        new PieWidgetValueDto
                        {
                            Label = DashboardWidgetResources.AllocationWidgetTitle,
                            Data = allocationRequestsGouped.Select(a => a.Time).ToList(),
                            BackgroundColor = new List<string> { WidgetColors.MattBlue, WidgetColors.MattRed },
                            HoverBackgroundColor = new List<string> { WidgetColors.MattBlue, WidgetColors.MattRed }
                        }
                    },
                    Labels = allocationRequestsGouped.Select(a => a.ProjectShortName).ToList()
                };

                response.Add(timeTrackingProgressWidget);
            }

            return response;
        }

        private IEnumerable<IWidgetDto> GetVacationAndOvertimeBalanceWidgets(long employeeId)
        {
            var response = new List<IWidgetDto>();
            var simpleWidget = new SimpleWidgetDto
            {
                Icon = Glyphicons.Briefcase,
                Title = DashboardWidgetResources.VacationAndOvertimeBalanceWidgetTitle
            };

            var values = new List<SimpleWidgetValueDto>();
            values.AddRange(GetVacationBalanceWidgetValues(employeeId));
            values.AddRange(GetOvertimeBalanceWidgetValues(employeeId));

            simpleWidget.Values = values;

            response.Add(simpleWidget);

            return response;
        }

        private IEnumerable<SimpleWidgetValueDto> GetVacationBalanceWidgetValues(long employeeId)
        {
            var currentDate = TimeService.GetCurrentDate();
            var firstDayOfCurrentYear = DateExtensions.GetFirstDayOfYear(currentDate);
            var lastDayOfLastYear = DateExtensions.GetLastDayOfYear(currentDate.AddYears(-1));
            var currentVacationBalance = _vacationBalanceService.GetHourBalanceForEmployeeUpToEffectiveDate(employeeId, currentDate);
            var lastVacationBalance = _vacationBalanceService.GetHourBalanceForEmployeeUpToEffectiveDate(employeeId, lastDayOfLastYear);
            var usedVacation = _vacationBalanceService.GetUsedVacationHoursForEmployee(employeeId, firstDayOfCurrentYear, currentDate);

            return new List<SimpleWidgetValueDto>
            {
                new SimpleWidgetValueDto
                {
                    Caption = DashboardWidgetResources.VacationFromPreviousYears,
                    ValueCaption = $"{lastVacationBalance}"
                },
                new SimpleWidgetValueDto
                {
                    Caption = DashboardWidgetResources.TotalVacation,
                    ValueCaption = $"{currentVacationBalance}"
                },
                new SimpleWidgetValueDto
                {
                    Caption = DashboardWidgetResources.UsedThisYear,
                    ValueCaption = $"{usedVacation}"
                }
            };
        }

        private IEnumerable<SimpleWidgetValueDto> GetOvertimeBalanceWidgetValues(long employeeId)
        {
            var currentDate = TimeService.GetCurrentDate();
            var overtimeBank = _overtimeBankService.GetEmployeeOvertimeBalance(employeeId, effectiveDate: currentDate);

            return new List<SimpleWidgetValueDto>
            {
                new SimpleWidgetValueDto
                {
                    Caption = DashboardWidgetResources.OvertimeBank,
                    ValueCaption = $"{overtimeBank}"
                }
            };
        }
    }
}
