﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Business.WebSite.Consts;
using Smt.Atomic.Business.WebSite.Dto;
using Smt.Atomic.Business.WebSite.Enums;
using Smt.Atomic.Business.WebSite.Interfaces;
using Smt.Atomic.Business.WebSite.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Consts;

namespace Smt.Atomic.Business.WebSite.Services
{
    internal class ProjectManagerDashboardDataService : DashboardDataBaseService
    {
        private readonly IProjectService _projectService;

        public ProjectManagerDashboardDataService(ITimeService timeService,
            ITimeReportService timeReportService,
            ISystemParameterService systemParameterService,
            IProjectService projectService)
            : base(timeService, timeReportService, systemParameterService)
        {
            _projectService = projectService;
        }

        internal DashboardGroupDto GetDashboardGroup(long employeeId)
        {
            var response = new DashboardGroupDto
            {
                DisplayName = DashboardWidgetResources.ProjectManagerDashboardGroupName,
                UrlParameter = DashboardGroupUrlParameters.ProjectManagerUrlParameter
            };

            var widgets = new List<IWidgetDto>();
            widgets.AddRange(GetProjectsWidgets(employeeId));
            widgets.AddRange(GetTimeReportWidgets(employeeId));

            response.Widgets = widgets;

            return response;
        }

        private IEnumerable<IWidgetDto> GetTimeReportWidgets(long employeeId)
        {
            var currentDate = TimeService.GetCurrentDate();
            var myEmployeeReports =
                TimeReportService.GetMyProjectEmployeesTimeReports(employeeId, currentDate.Year, (byte)(currentDate.Month));
            var response = new List<IWidgetDto>();
            var url = GetTimeReportUrl(currentDate.Year, currentDate.Month, TimeReportFilters.MyProjects);

            if (myEmployeeReports != null)
            {
                var myEmployeesTimeReportsWidget = new PieWidgetDto
                {
                    Icon = Glyphicons.Time,
                    Title = DashboardWidgetResources.MyProjectEmployeeTimeReportsWidgetTitle,
                    PieceLabel = PieceLabel.Value,
                    Datasets = new List<IChartWidgetValueDto>
                    {
                        new PieWidgetValueDto
                        {
                            Label = DashboardWidgetResources.MyProjectEmployeeTimeReportsWidgetTitle,
                            Data = new List<decimal>
                            {
                                myEmployeeReports.Count(r => r.Status == TimeReportStatus.Submitted),
                                myEmployeeReports.Count(r => r.Status == TimeReportStatus.Accepted),
                                myEmployeeReports.Count(r => r.Status == TimeReportStatus.Rejected),
                                myEmployeeReports.Count(r => r.Status == TimeReportStatus.Draft),
                                myEmployeeReports.Count(r => r.Status == TimeReportStatus.NotStarted)
                            },
                            BackgroundColor = new List<string>
                            {
                                WidgetColors.MattOrange,
                                WidgetColors.MattBlue,
                                WidgetColors.MattGreen,
                                WidgetColors.MattRed,
                                WidgetColors.LightBlue
                            },
                            HoverBackgroundColor = new List<string>
                            {
                                WidgetColors.MattOrange,
                                WidgetColors.MattBlue,
                                WidgetColors.MattGreen,
                                WidgetColors.MattRed,
                                WidgetColors.LightBlue
                            }
                        }
                    },
                    Labels = new List<string>
                    {
                        DashboardWidgetResources.SubmittedReportLabel,
                        DashboardWidgetResources.AcceptedReportLabel,
                        DashboardWidgetResources.RejectedReportLabel,
                        DashboardWidgetResources.DraftReportLabel,
                        DashboardWidgetResources.NotStartedReportLabel
                    },
                    FooterHtmlMessage = $"<a href='{ url }' class='btn btn-default'>{DashboardWidgetResources.DetailsButton}</a>"
                };

                response.Add(myEmployeesTimeReportsWidget);
            }

            var myProjectIds = _projectService.GetActiveProjectsForManagerEmployeeId(employeeId).Select(p => p.Id).ToList();
            var overtimeTimeReportDate = currentDate;
            var overtimeTimeReportRows = TimeReportService.GetOvertimeTimeReportRows(myProjectIds, null, overtimeTimeReportDate.Year, (byte)overtimeTimeReportDate.Month);

            if (overtimeTimeReportRows.IsNullOrEmpty())
            {
                overtimeTimeReportDate = overtimeTimeReportDate.GetFirstDayInMonth().AddMonths(-1);
                overtimeTimeReportRows = TimeReportService.GetOvertimeTimeReportRows(myProjectIds, null, overtimeTimeReportDate.Year, (byte)overtimeTimeReportDate.Month);
            }

            var widgetOvertimeRows = GetRowsForTopOvertimeEmployees(overtimeTimeReportRows)
                .OrderBy(r => r.EmployeeId);

            var overtimeWidget = new BarWidgetDto
            {
                Icon = Glyphicons.Time,
                Title = $"{DashboardWidgetResources.MyEmployeesOvertimeLabel} {overtimeTimeReportDate.ToYearMonthDateFormat()}",
                IsStacked = true,
                Labels = widgetOvertimeRows.Select(r => r.EmployeeDisplayName).Distinct().ToList()
            };

            var datasets = GetDatasetsForOvertimeWidget(widgetOvertimeRows);

            overtimeWidget.Datasets = datasets;

            if (!datasets.IsNullOrEmpty())
            {
                response.Add(overtimeWidget);
            }

            return response;
        }

        private IEnumerable<IWidgetDto> GetProjectsWidgets(long employeeId)
        {
            const string projectHubIdentifier = "EmployeeValuePickers.Project";

            var managerProjects = _projectService.GetActiveProjectsForManagerEmployeeId(employeeId).OrderByDescending(p => p.StartDate);
            var response = new List<IWidgetDto>();

            if (managerProjects.IsNullOrEmpty())
            {
                return response;
            }

            var myProjectsWidget = new TableWidgetDto
            {
                Title = DashboardWidgetResources.MyProjectsLabel,
                Icon = Glyphicons.Heart,
                Columns = new[]
                {
                    DashboardWidgetResources.ProjectLabel,
                    DashboardWidgetResources.StartDateLabel,
                    DashboardWidgetResources.EndDateLabel,
                },
                Rows = managerProjects.Select(p => new TableWidgetRowDto
                {
                    Actions = new List<TableWidgetRowActionDto>
                    {
                        new TableWidgetRowActionDto
                        {
                            DataHubIdentifier = projectHubIdentifier,
                            DataHubRecordId = p.Id,
                            Value = p.ProjectName,
                        },
                        new TableWidgetRowActionDto
                        {
                            Value = p.StartDate?.ToDefaultDateFormat(),
                        },
                        new TableWidgetRowActionDto
                        {
                            Value = p.EndDate?.ToDefaultDateFormat(),
                        },
                    },
                }).ToList(),
                WidgetFooterButton = new WidgetButtonDto
                {
                    UrlType = CrossCutting.Common.Enums.ActionUrlType.ListProjects,
                    Label = DashboardWidgetResources.GoToMyProjectsLabel,
                    Parameters = new Dictionary<string, string>
                    {
                        ["filter"] = "my-projects",
                    },
                },
            };

            response.Add(myProjectsWidget);

            return response;
        }
    }
}
