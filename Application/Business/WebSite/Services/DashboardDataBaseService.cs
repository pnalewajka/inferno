﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.Business.WebSite.Consts;
using Smt.Atomic.Business.WebSite.Dto;
using Smt.Atomic.Business.WebSite.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.Business.WebSite.Services
{
    internal abstract class DashboardDataBaseService
    {
        private readonly ISystemParameterService _systemParameterService;

        protected readonly ITimeService TimeService;
        protected readonly ITimeReportService TimeReportService;

        protected DashboardDataBaseService(ITimeService timeService,
            ITimeReportService timeReportService,
            ISystemParameterService systemParameterService)
        {
            TimeService = timeService;
            TimeReportService = timeReportService;
            _systemParameterService = systemParameterService;
        }

        protected string GetTimeReportUrl(int year, int month, string filter)
        {
            var query = $"year={year}&month={month}&view=default&filter={filter}";

            return GetUrl("TimeTracking", "TimeReport", "List", query);
        }

        protected string GetUrl(string area, string controller, string action, string query)
        {
            var serverAddress = _systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);

            return $"{serverAddress}{area}/{controller}/{action}?{query}";
        }

        protected string GetUrl(string area, string controller, string action, long id)
        {
            var serverAddress = _systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);

            return $"{serverAddress}{area}/{controller}/{action}/{id}";
        }

        protected IReadOnlyCollection<IChartWidgetValueDto> GetDatasetsForOvertimeWidget(IEnumerable<EmployeeOvertimeInProjectDto> timeReportRows)
        {
            var colors = WidgetColors.AsArray();
            var datasets = new List<IChartWidgetValueDto>();
            var projectIds = timeReportRows.Select(r => r.ProjectId).Distinct();
            var employeeIds = timeReportRows.Select(r => r.EmployeeId).Distinct();
            var employeeCount = employeeIds.Count();
            const string stackName = "Group1";
            int i = 0;

            foreach (var projectId in projectIds)
            {
                var datasetColors = Enumerable.Repeat(colors[i % colors.Length], employeeCount).ToList();
                ++i;

                var dataset = new BarWidgetValueDto
                {
                    Label = timeReportRows.First(r => r.ProjectId == projectId).ProjectName,
                    BackgroundColor = datasetColors,
                    HoverBackgroundColor = datasetColors,
                    StackName = stackName,
                };

                var datasetData = new List<decimal>();

                foreach (var employeeId in employeeIds)
                {
                    var reportRows = timeReportRows.Where(r => r.EmployeeId == employeeId && r.ProjectId == projectId);
                    var value = !reportRows.IsNullOrEmpty()
                        ? reportRows.Sum(r => r.OvertimeHours)
                        : 0;

                    datasetData.Add(value);
                }

                dataset.Data = datasetData;
                datasets.Add(dataset);
            }

            return datasets;
        }

        protected IEnumerable<EmployeeOvertimeInProjectDto> GetRowsForTopOvertimeEmployees(IEnumerable<EmployeeOvertimeInProjectDto> source)
        {
            const int topEmployeeCount = 5;

            return source
                .GroupBy(r => r.EmployeeId)
                .OrderByDescending(r => r.Sum(d => d.OvertimeHours))
                .Take(topEmployeeCount)
                .SelectMany(g => g);
        }
    }
}