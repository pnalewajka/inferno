﻿using Smt.Atomic.Business.WebSite.Enums;
using Smt.Atomic.Business.WebSite.Interfaces;
using System.Collections.Generic;

namespace Smt.Atomic.Business.WebSite.Dto
{
    public class ChartWidgetDto : WidgetBaseDto, IChartWidgetDto
    {
        public ChartType ChartType { get; set; }

        public IReadOnlyCollection<string> Labels { get; set; }

        public virtual IReadOnlyCollection<IChartWidgetValueDto> Datasets { get; set; }

        public ChartWidgetDto()
        {
            Type = WidgetType.Chart;
        }
    }
}
