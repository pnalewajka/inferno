﻿using Smt.Atomic.Business.WebSite.Enums;

namespace Smt.Atomic.Business.WebSite.Dto
{
    public class PieWidgetDto : ChartWidgetDto
    {
        public PieceLabel PieceLabel { get; set; }

        public PieWidgetDto()
        {
            ChartType = ChartType.Pie;
        }
    }
}
