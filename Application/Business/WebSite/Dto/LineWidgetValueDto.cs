﻿namespace Smt.Atomic.Business.WebSite.Dto
{
    public class LineWidgetValueDto : ChartWidgetValueDto
    {
        public bool IsFilled { get; set; }
    }
}
