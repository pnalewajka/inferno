﻿using Smt.Atomic.Business.WebSite.Interfaces;
using System.Collections.Generic;
namespace Smt.Atomic.Business.WebSite.Dto
{
    public class DashboardGroupDto
    {
        public string DisplayName { get; set; }

        public string UrlParameter { get; set; }

        public IReadOnlyCollection<IWidgetDto> Widgets { get; set; }
    }
}
