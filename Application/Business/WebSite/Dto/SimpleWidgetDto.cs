﻿using Smt.Atomic.Business.WebSite.Enums;
using Smt.Atomic.Business.WebSite.Interfaces;
using System.Collections.Generic;
namespace Smt.Atomic.Business.WebSite.Dto
{
    public class SimpleWidgetDto : WidgetBaseDto, ISimpleWidgetDto
    {
        public IReadOnlyCollection<SimpleWidgetValueDto> Values { get; set; }

        public SimpleWidgetDto()
        {
            Type = WidgetType.Simple;
        }
    }
}
