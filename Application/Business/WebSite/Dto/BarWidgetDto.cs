﻿using Smt.Atomic.Business.WebSite.Enums;

namespace Smt.Atomic.Business.WebSite.Dto
{
    public class BarWidgetDto : ChartWidgetDto
    {
        public bool IsStacked { get; set; }

        public BarWidgetDto()
        {
            ChartType = ChartType.Bar;
        }
    }
}
