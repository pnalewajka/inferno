﻿using Smt.Atomic.Business.WebSite.Enums;

namespace Smt.Atomic.Business.WebSite.Dto
{
    public class LineWidgetDto : ChartWidgetDto
    {
        public LineWidgetDto()
        {
            ChartType = ChartType.Line;
        }
    }
}
