﻿using System.Collections.Generic;
namespace Smt.Atomic.Business.WebSite.Dto
{
    public class ListWidgetRowDto
    {
        public string Caption { get; set; }

        public IReadOnlyCollection<ListWidgetRowActionDto> Actions { get; set; }
    }
}
