﻿using Smt.Atomic.Business.WebSite.Enums;
using Smt.Atomic.Business.WebSite.Interfaces;

namespace Smt.Atomic.Business.WebSite.Dto
{
    public abstract class WidgetBaseDto : IWidgetDto
    {
        public WidgetType Type { get; set; }

        public string Icon { get; set; }

        public string Title { get; set; }

        public string TooltipText { get; set; }

        public WidgetButtonDto WidgetFooterButton { get; set; }

        public string FooterHtmlMessage { get; set; }

        public string HeaderBackgroud { get; set; }

        public bool IsHidden { get; set; }

        public string Url { get; set; }
    }
}
