﻿namespace Smt.Atomic.Business.WebSite.Dto
{
    public class SimpleWidgetValueDto
    {
        public string Caption { get; set; }

        public string ValueCaption { get; set; }

        public string Url { get; set; }
    }
}
