﻿using Smt.Atomic.Business.WebSite.Enums;

namespace Smt.Atomic.Business.WebSite.Dto
{
    public class ListWidgetRowActionDto
    {
        public string Caption { get; set; }

        public string Url { get; set; }

        public WidgetButtonClass? ButtonClass { get; set; }

        public string Method { get; set; }
    }
}
