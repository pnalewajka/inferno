﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.WebSite.Dto
{
    public class DashboardConfigurationDto
    {
        public long DefaultDashboardGroupIndex { get; set; }

        public IReadOnlyCollection<DashboardGroupDto> DashboardGroups { get; set; }
    }
}
