﻿using System.Collections.Generic;
using Smt.Atomic.Business.WebSite.Enums;
using Smt.Atomic.Business.WebSite.Interfaces;

namespace Smt.Atomic.Business.WebSite.Dto
{
    public class TableWidgetDto : WidgetBaseDto, ITableWidgetDto
    {
        public IReadOnlyCollection<TableWidgetRowDto> Rows { get; set; }

        public IReadOnlyCollection<string> Columns { get; set; }

        public string NoRowsMessage { get; set; }

        public TableWidgetDto()
        {
            Type = WidgetType.Table;
        }
    }
}
