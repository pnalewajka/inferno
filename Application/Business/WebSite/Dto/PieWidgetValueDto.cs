﻿namespace Smt.Atomic.Business.WebSite.Dto
{
    public class PieWidgetValueDto : ChartWidgetValueDto
    {
        public string StackName { get; set; }
    }
}
