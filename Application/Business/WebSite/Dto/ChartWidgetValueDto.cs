﻿using Smt.Atomic.Business.WebSite.Interfaces;
using System.Collections.Generic;

namespace Smt.Atomic.Business.WebSite.Dto
{
    public class ChartWidgetValueDto : IChartWidgetValueDto
    {
        public string Label { get; set; }

        public IReadOnlyCollection<string> BackgroundColor { get; set; }

        public IReadOnlyCollection<string> HoverBackgroundColor { get; set; }

        public IReadOnlyCollection<decimal> Data { get; set; }
    }
}
