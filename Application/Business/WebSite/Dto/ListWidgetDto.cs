﻿using Smt.Atomic.Business.WebSite.Enums;
using Smt.Atomic.Business.WebSite.Interfaces;
using System.Collections.Generic;

namespace Smt.Atomic.Business.WebSite.Dto
{
    public class ListWidgetDto : WidgetBaseDto, IListWidgetDto
    {
        public IReadOnlyCollection<ListWidgetRowDto> Rows { get; set; }

        public string NoRowsMessage { get; set; }

        public ListWidgetDto()
        {
            Type = WidgetType.List;
        }
    }
}
