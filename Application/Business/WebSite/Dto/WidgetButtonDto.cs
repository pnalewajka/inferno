﻿using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.WebSite.Dto
{
    public class WidgetButtonDto
    {
        public ActionUrlType UrlType { get; set; }

        public long? EntityId { get; set; }

        public IDictionary<string, string> Parameters { get; set; }

        public string Label { get; set; }
    }
}
