﻿namespace Smt.Atomic.Business.WebSite.Dto
{
    public class BarWidgetValueDto : ChartWidgetValueDto
    {
        public string StackName { get; set; }
    }
}
