﻿using System.Collections.Generic;
namespace Smt.Atomic.Business.WebSite.Dto
{
    public class TableWidgetRowDto
    {
        public IReadOnlyCollection<TableWidgetRowActionDto> Actions { get; set; }
    }
}
