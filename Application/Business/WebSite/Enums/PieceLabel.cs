﻿namespace Smt.Atomic.Business.WebSite.Enums
{
    public enum PieceLabel
    {
        Label = 1,
        Value = 2,
        Percentage = 3,
        Image = 4
    }
}
