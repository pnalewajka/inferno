﻿namespace Smt.Atomic.Business.WebSite.Enums
{
    public enum ChartType
    {
        Doughnut = 0,
        Pie = 1,
        Line = 2,
        Bar = 3
    }
}