﻿namespace Smt.Atomic.Business.WebSite.Enums
{
    public enum WidgetButtonClass
    {
        Default = 0,
        Primary = 1,
        Success = 2,
        Info = 3,
        Warning = 4,
        Danger = 5,
        Link = 6
    }
}