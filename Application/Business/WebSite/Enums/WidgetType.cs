﻿namespace Smt.Atomic.Business.WebSite.Enums
{
    public enum WidgetType
    {
        Simple = 0,
        Chart = 1,
        List = 2,
        Table = 3,
    }
}