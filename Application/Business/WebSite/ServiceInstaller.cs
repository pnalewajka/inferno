﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.WebSite.Interfaces;
using Smt.Atomic.Business.WebSite.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.WebSite
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            if (containerType == ContainerType.WebApp)
            {
                container.Register(Component.For<LineManagerDashboardDataService>().LifestylePerWebRequest());
                container.Register(Component.For<EmployeeDashboardDataService>().LifestylePerWebRequest());
                container.Register(Component.For<ProjectManagerDashboardDataService>().LifestylePerWebRequest());
                container.Register(Component.For<IDashboardService>().ImplementedBy<DashboardService>().LifestyleTransient());
            }
        }
    }
}
