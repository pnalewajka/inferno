﻿using Smt.Atomic.Business.WebSite.Dto;

namespace Smt.Atomic.Business.WebSite.Interfaces
{
    public interface IDashboardService
    {
        DashboardConfigurationDto GetDashboardsForEmployee(long employeeId);
    }
}
