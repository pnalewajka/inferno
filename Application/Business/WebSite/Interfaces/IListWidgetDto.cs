﻿using Smt.Atomic.Business.WebSite.Dto;
using System.Collections.Generic;

namespace Smt.Atomic.Business.WebSite.Interfaces
{
    public interface IListWidgetDto : IWidgetDto
    {
        IReadOnlyCollection<ListWidgetRowDto> Rows { get; set; }

        string NoRowsMessage { get; set; }
    }
}
