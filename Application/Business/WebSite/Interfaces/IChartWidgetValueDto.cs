﻿using System.Collections.Generic;

namespace Smt.Atomic.Business.WebSite.Interfaces
{
    public interface IChartWidgetValueDto
    {
        string Label { get; set; }

        IReadOnlyCollection<string> BackgroundColor { get; set; }

        IReadOnlyCollection<string> HoverBackgroundColor { get; set; }

        IReadOnlyCollection<decimal> Data { get; set; }
    }
}
