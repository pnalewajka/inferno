﻿using Smt.Atomic.Business.WebSite.Enums;

namespace Smt.Atomic.Business.WebSite.Interfaces
{
    public interface IWidgetDto
    {
        WidgetType Type { get; set; }

        string Icon { get; set; }

        string Title { get; set; }

        string TooltipText { get; set; }

        string FooterHtmlMessage { get; set; }

        string HeaderBackgroud { get; set; }

        bool IsHidden { get; set; }

        string Url { get; set; }
    }
}
