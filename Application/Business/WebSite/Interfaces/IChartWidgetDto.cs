﻿using Smt.Atomic.Business.WebSite.Enums;
using System.Collections.Generic;

namespace Smt.Atomic.Business.WebSite.Interfaces
{
    public interface IChartWidgetDto : IWidgetDto
    {
        ChartType ChartType { get; set; }

        IReadOnlyCollection<string> Labels { get; set; }

        IReadOnlyCollection<IChartWidgetValueDto> Datasets { get; set; }
    }
}
