﻿using System.Collections.Generic;
using Smt.Atomic.Business.WebSite.Dto;

namespace Smt.Atomic.Business.WebSite.Interfaces
{
    public interface ITableWidgetDto : IWidgetDto
    {
        IReadOnlyCollection<TableWidgetRowDto> Rows { get; }

        string NoRowsMessage { get; }
    }
}
