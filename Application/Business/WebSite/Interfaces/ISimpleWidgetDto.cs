﻿using Smt.Atomic.Business.WebSite.Dto;
using System.Collections.Generic;

namespace Smt.Atomic.Business.WebSite.Interfaces
{
    public interface ISimpleWidgetDto : IWidgetDto
    {
        IReadOnlyCollection<SimpleWidgetValueDto> Values { get; set; }
    }
}
