﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.MeetMe.ChoreProviders;
using Smt.Atomic.Business.MeetMe.Interfaces;
using Smt.Atomic.Business.MeetMe.Services;
using Smt.Atomic.Business.MeetMe.Workflows.Services;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.MeetMe
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            if (containerType == ContainerType.WebApp)
            {
                container.Register(Component.For<IEmployeeMeetingCardIndexDataService>().ImplementedBy<EmployeeMeetingCardIndexDataService>().LifestylePerWebRequest());
                container.Register(Component.For<IMeetingDetailsCardIndexService>().ImplementedBy<MeetingDetailsCardIndexService>().LifestylePerWebRequest());
                container.Register(Component.For<IMeetingNoteCardIndexService>().ImplementedBy<MeetingNoteCardIndexService>().LifestylePerWebRequest());
                container.Register(Component.For<IFeedbackCardIndexService>().ImplementedBy<FeedbackCardIndexService>().LifestylePerWebRequest());
                container.Register(Component.For<IMeetingService>().ImplementedBy<MeetingService>().LifestylePerWebRequest());
                container.Register(Component.For<IEmployeeReviewProcessOverview>().ImplementedBy<EmployeeReviewProcessOverviewService>().LifestylePerWebRequest());
                container.Register(Component.For<IRequestService>().ImplementedBy<FeedbackRequestService>().LifestylePerWebRequest());
                container.Register(Component.For<IFeedbackEmailNotificationService>().ImplementedBy<FeedbackEmailNotificationService>().LifestylePerWebRequest());
                container.Register(Component.For<IMeetingNoteService>().ImplementedBy<MeetingNoteService>().LifestylePerWebRequest());
                container.Register(Component.For<IMeetingInvitationCardIndexService>().ImplementedBy<MeetingInvitationCardIndexService>().LifestylePerWebRequest());
                container.Register(Component.For<IChoreProvider>().ImplementedBy<OverdueMeetingsChoreProvider>().LifestylePerWebRequest());
            }
            else if (containerType == ContainerType.JobScheduler)
            {
                container.Register(Component.For<IFeedbackEmailNotificationService>().ImplementedBy<FeedbackEmailNotificationService>().LifestyleTransient());
                container.Register(Component.For<IMeetingService>().ImplementedBy<MeetingService>().LifestyleTransient());
                container.Register(Component.For<IMeetingNoteService>().ImplementedBy<MeetingNoteService>().LifestyleTransient());
                container.Register(Component.For<IMeetingNotificationService>().ImplementedBy<MeetingNotificationService>().LifestyleTransient());
                container.Register(Component.For<IRequestService>().ImplementedBy<FeedbackRequestService>().LifestyleTransient());
            }
        }
    }
}


