﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.MeetMe;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.MeetMe.ChoreProviders
{
    public class OverdueMeetingsChoreProvider
        : IChoreProvider
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly ITimeService _timeService;

        public OverdueMeetingsChoreProvider(
            IPrincipalProvider principalProvider,
            ITimeService timeService,
            IOrgUnitService orgUnitService)
        {
            _principalProvider = principalProvider;
            _timeService = timeService;
        }

        public IQueryable<ChoreDto> GetActiveChores(IReadOnlyRepositoryFactory repositoryFactory)
        {
            if (!_principalProvider.Current.IsInRole(SecurityRoleType.CanViewMyEmployeeMeetings))
            {
                return null;
            }

            var currentDate = _timeService.GetCurrentDate();
            var currentEmployeeId = _principalProvider.Current.EmployeeId;

            return repositoryFactory.Create<Meeting>()
                .Where(m => m.Employee.LineManagerId == currentEmployeeId)
                .Where(m => m.MeetingDueDate < currentDate && m.Status != MeetingStatus.Done)
                .GroupBy(m => 0)
                .Select(g => new ChoreDto
                {
                    Type = ChoreType.OverdueMeetings,
                    DueDate = null,
                    Parameters = g.Count().ToString(),
                });
        }
    }
}
