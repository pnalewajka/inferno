﻿using System;
using System.Linq;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.MeetMe;
using System.Linq.Expressions;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace Smt.Atomic.Business.MeetMe.BusinessLogics
{
    public class MeetingBusinessLogics
    {
        public readonly static BusinessLogic<IQueryable<Meeting>, DateTime?> GetLastMeetingDate =
            new BusinessLogic<IQueryable<Meeting>, DateTime?>(
                (meetings) => meetings.Max(m => m.ScheduledMeetingDate));

        public readonly static BusinessLogic<IQueryable<Meeting>, Expression<Func<Meeting, DateTime?>>, DateTime?> GetNextMeetingDate =
            new BusinessLogic<IQueryable<Meeting>, Expression<Func<Meeting, DateTime?>>, DateTime?>(
                (meetings, dateSelector) => meetings.Select(dateSelector).Min());

        public readonly static BusinessLogic<IQueryable<Meeting>, DateTime?> GetNextMeetingScheduledDate =
            new BusinessLogic<IQueryable<Meeting>, DateTime?>(
                (meetings) => GetNextMeetingDate.Call(meetings, m => m.ScheduledMeetingDate));

        public readonly static BusinessLogic<IQueryable<Meeting>, DateTime?> GetNextMeetingDueDate =
            new BusinessLogic<IQueryable<Meeting>, DateTime?>(
                (meetings) => GetNextMeetingDate.Call(meetings, m => m.MeetingDueDate));

        public readonly static BusinessLogic<IQueryable<Meeting>, DateTime, MeetMeProcessStatus> GetMeetMeProcessStatus =
            new BusinessLogic<IQueryable<Meeting>, DateTime, MeetMeProcessStatus>(
                (meetings, currentDate) =>
                new[] { GetNextMeetingDueDate.Call(meetings), currentDate }.Min() != currentDate
                    ? MeetMeProcessStatus.Overdue
                    : MeetMeProcessStatus.Scheduled);

        public readonly static BusinessLogic<Meeting, DateTime> GetMeetingDate =
            new BusinessLogic<Meeting, DateTime>(
                (meeting) => meeting.ScheduledMeetingDate ?? meeting.MeetingDueDate.GetMinDateIfDefault());

        public readonly static BusinessLogic<Meeting, DateTime, int, int, long, bool> CanAddOrEditNoteForLastOwnMeeting =
            new BusinessLogic<Meeting, DateTime, int, int, long, bool>(
                (meeting, currentDate, canAddOrEditDaysMin, canAddOrEditDaysMax, currentEmployeeId) =>
                meeting.Status != MeetingStatus.Done
                && meeting.EmployeeId == currentEmployeeId
                && DateHelper.IsBetweenDates(
                    currentDate.Date,
                    GetMeetingDate.Call(meeting).AddDays(canAddOrEditDaysMin).Date,
                    GetMeetingDate.Call(meeting).AddDays(canAddOrEditDaysMax).Date));

        public readonly static BusinessLogic<Meeting, DateTime, int, bool> CanEditDoneMeetingNote =
            new BusinessLogic<Meeting, DateTime, int, bool>(
                (meeting, currentDate, canEditDays) =>
                meeting.Status == MeetingStatus.Done
                && meeting.ScheduledMeetingDate.GetMinDateIfDefault().Date <= currentDate.Date
                && meeting.ScheduledMeetingDate.GetMinDateIfDefault().AddDays(canEditDays).Date >= currentDate.Date);
    }
}
