﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class FeedbackDto
    {
        public long Id { get; set; }

        public string FeedbackContent { get; set; }

        public string AuthorName { get; set; }

        public long AuthorId { get; set; }

        public string ReceiverName { get; set; }

        public long RecieverId { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public string Context { get; set; }

        public FeedbackVisbility Visibility { get; set; }
    }
}
