﻿namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class EmployeeTimelineProjectAllocationDto : EmployeeTimelineEventDto
    {
        public string ProjectName { get; set; }
    }
}