﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.MeetMe;

namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class MeetingToEmployeeTimelineMeetingDtoMapping : ClassMapping<Meeting, EmployeeTimelineMeetingDto>
    {
        public MeetingToEmployeeTimelineMeetingDtoMapping()
        {
            Mapping = e => new EmployeeTimelineMeetingDto
            {
                EmployeeFullName = e.Employee != null ? e.Employee.FullName : string.Empty,
                EmployeeId = e.Employee != null ? e.EmployeeId : default(long),
                HappenedOn = e.ScheduledMeetingDate ?? e.MeetingDueDate.Value,
                IsScheduled = e.ScheduledMeetingDate.HasValue,
                Attachments = e.Attachments
                    .Select(x => new DocumentDto
                    {
                        DocumentId = x.Id,
                        DocumentName = x.Name,
                        ContentType = x.ContentType
                    }).ToList(),
                Notes = e.Notes
                    .Select(x => new EmployeeTimelineMeetingNoteDto
                    {
                        HappenedOn = x.CreatedOn,
                        ContentBody = x.NoteBody,
                        EmployeeFullName = x.Employee.FullName,
                        EmployeeId = x.Employee.Id,
                        Attachments = x.Attachments
                            .Select(at => new DocumentDto
                            {
                                DocumentId = at.Id,
                                DocumentName = at.Name,
                                ContentType = at.ContentType
                            }).ToList()
                    }).ToList()
            };
        }
    }
}
