﻿using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class FeedbackDtoToFeedbackRequestDtoMapping : ClassMapping<FeedbackDto, FeedbackRequestDto>
    {
        public FeedbackDtoToFeedbackRequestDtoMapping()
        {
            Mapping = d => new FeedbackRequestDto
            {
                Id = d.Id,
                EvaluatorEmployeeId = d.AuthorId,
                EvaluatedEmployeeId = d.RecieverId,
                FeedbackContent = d.FeedbackContent,
                Visibility = d.Visibility,
            };
        }
    }
}
