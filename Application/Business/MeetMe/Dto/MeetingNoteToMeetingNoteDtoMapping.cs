﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.MeetMe.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.MeetMe;

namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class MeetingNoteToMeetingNoteDtoMapping : ClassMapping<MeetingNote, MeetingNoteDto>
    {
        private readonly IMeetingNoteService _meetingNoteService;

        public MeetingNoteToMeetingNoteDtoMapping(IMeetingNoteService meetingNoteService)
        {
            _meetingNoteService = meetingNoteService;

            Mapping = e => new MeetingNoteDto
            {
                Id = e.Id,
                MeetingId = e.MeetingId,
                AuthorId = e.AuthorId,
                AuthorName = e.Employee != null ? e.Employee.FullName : string.Empty,
                NoteBody = e.NoteBody,
                CreatedOn = e.CreatedOn,
                Attachments = e.Attachments.Select(p => new DocumentDto
                {
                    ContentType = p.ContentType,
                    DocumentName = p.Name,
                    DocumentId = p.Id
                }).ToList(),
                Status = e.Status,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp,
                CanEdit = e.Meeting != null ? _meetingNoteService.CanEditNote(e.Id) : false,
            };
        }
    }
}
