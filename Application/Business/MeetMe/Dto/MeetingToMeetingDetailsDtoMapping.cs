﻿using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.MeetMe.BusinessLogics;
using Smt.Atomic.Business.MeetMe.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Business.Helpers;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.MeetMe;

namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class MeetingToMeetingDetailsDtoMapping : ClassMapping<Meeting, MeetingDetailsDto>
    {
        private readonly IClassMapping<MeetingNote, MeetingNoteDto> _meetingNoteToMettingNoteDtoMapping;
        private readonly IPrincipalProvider _principalProvider;
        private readonly ITimeService _timeService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IMeetingNoteService _meetingNoteService;

        public MeetingToMeetingDetailsDtoMapping(IClassMapping<MeetingNote, MeetingNoteDto> meetingNoteToMettingNoteDtoMapping,
            IPrincipalProvider principleProvider,
            ITimeService timeService,
            ISystemParameterService systemParameterService,
            IMeetingNoteService meetingNoteService)
        {
            _meetingNoteToMettingNoteDtoMapping = meetingNoteToMettingNoteDtoMapping;
            _principalProvider = principleProvider;
            _timeService = timeService;
            _systemParameterService = systemParameterService;
            _meetingNoteService = meetingNoteService;

            Mapping = e => new MeetingDetailsDto
            {
                Id = e.Id,
                MeetingStatus = e.Status,
                MeetingDueDate = e.MeetingDueDate,
                MeetingDate = e.ScheduledMeetingDate,
                Attachments = e.Attachments
                    .Select(x => new DocumentDto
                    {
                        DocumentId = x.Id,
                        DocumentName = x.Name,
                        ContentType = x.ContentType
                    }).ToList(),
                Notes = e.Notes
                    .Select(n => _meetingNoteToMettingNoteDtoMapping.CreateFromSource(n))
                    .ToList(),
                EmployeeEmail = e.Employee != null ? e.Employee.Email : null,
                LineManagerEmail = e.Employee != null && e.Employee.LineManager != null ? e.Employee.LineManager.Email : null,
                CanAddNote = e.Id != BusinessLogicHelper.NonExistingId && _meetingNoteService.CanAddNote(e.Id),
                CanBeEdited = e.Id != BusinessLogicHelper.NonExistingId && CanBeEdited(e)
            };
        }

        private bool CanBeEdited(Meeting meeting)
        {
            var currentTime = _timeService.GetCurrentTime();
            var currentEmployeeId = _principalProvider.Current.EmployeeId;
            var canEditDays = _systemParameterService.GetParameter<int>(ParameterKeys.EditDoneMeetingDays);
            var canSeeEmployeeMeetings = _principalProvider.Current.IsInRole(SecurityRoleType.CanViewAllEmployeeMeetings)
                || (meeting.Employee != null ? EmployeeBusinessLogic.HasGivenLineManager.Call(meeting.Employee, currentEmployeeId) : false);

            var canEdit = canSeeEmployeeMeetings && meeting.Status != MeetingStatus.Done;
            var canEditAfterMeetingDone = canSeeEmployeeMeetings
                        && MeetingBusinessLogics.CanEditDoneMeetingNote.Call(meeting, currentTime, canEditDays);

            return canEdit || canEditAfterMeetingDone;
        }
    }
}