﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class EmployeeReviewProcessOverviewDto
    {
        public string EmployeeFullName { get; set; }

        public IList<EmployeeTimelineMeetingDto> Meetings { get; set; }

        public IList<EmployeeTimelineFeedbackDto> Feedbacks { get; set; }

        public IList<EmployeeTimelineEmploymentPeriodDto> EmploymentPeriods { get; set; }

        public IList<EmployeeTimelineProjectAllocationDto> AllocationRequests { get; set; }

        public IList<EmployeeTimelineLineManagerChangedDto> LineManagerChanges { get; set; }
    }
}
