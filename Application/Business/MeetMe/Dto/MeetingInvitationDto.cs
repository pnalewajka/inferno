﻿using System;

namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class MeetingInvitationDto
    {
        public long Id { get; set; }

        public DateTime? MeetingDueDate { get; set; }

        public DateTime? MeetingDate { get; set; }

        public string LineManagerEmail { get; set; }

        public string EmployeeFirstName { get; set; }

        public string EmployeeEmail { get; set; }

        public string AdditionalInvitationContent { get; set; }
    }
}