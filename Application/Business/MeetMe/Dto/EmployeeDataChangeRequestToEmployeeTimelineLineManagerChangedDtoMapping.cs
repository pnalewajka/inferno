﻿using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Workflows;

namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class EmployeeDataChangeRequestToEmployeeTimelineLineManagerChangedDtoMapping : ClassMapping<EmployeeDataChangeRequest, EmployeeTimelineLineManagerChangedDto>
    {
        public EmployeeDataChangeRequestToEmployeeTimelineLineManagerChangedDtoMapping()
        {
            Mapping = e => new EmployeeTimelineLineManagerChangedDto
            {
                HappenedOn = e.EffectiveOn.Value,
                EmployeeId = e.Employees != null ? e.Employees.Select(em => em.Id).FirstOrDefault() : default(long),
                EmployeeFullName = e.Employees != null ? e.Employees.Select(em => em.FullName).FirstOrDefault() : string.Empty,
                CurrentLineManagerId = e.LineManagerId.Value,
                CurrentLineManagerFullName = e.LineManager != null ? EmployeeBusinessLogic.FullName.Call(e.LineManager) : string.Empty
            };
        }
    }
}
