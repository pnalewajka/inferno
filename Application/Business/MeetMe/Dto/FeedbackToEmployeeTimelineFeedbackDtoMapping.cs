﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.MeetMe;

namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class FeedbackToEmployeeTimelineFeedbackDtoMapping : ClassMapping<Feedback, EmployeeTimelineFeedbackDto>
    {
        public FeedbackToEmployeeTimelineFeedbackDtoMapping()
        {
            Mapping = e => new EmployeeTimelineFeedbackDto
            {
                AuthorFullName = e.Author != null ? e.Author.FullName : string.Empty,
                AuthorId = e.AuthorId,
                ReceiverFullName = e.Receiver != null ? e.Receiver.FullName : string.Empty,
                ReceiverId = e.ReceiverId,
                HappenedOn = e.ModifiedOn,
                FeedbackContent = e.FeedbackContent,
                Visibility = e.Visibility
            };
        }
    }
}