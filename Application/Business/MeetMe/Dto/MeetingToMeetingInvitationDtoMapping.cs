﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.MeetMe;

namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class MeetingToMeetingInvitationDtoMapping : ClassMapping<Meeting, MeetingInvitationDto>
    {
        public MeetingToMeetingInvitationDtoMapping()
        {
            Mapping = e => new MeetingInvitationDto
            {
                Id = e.Id,
                MeetingDueDate = e.MeetingDueDate,
                MeetingDate = e.ScheduledMeetingDate,
                EmployeeFirstName = e.Employee.FirstName,
                EmployeeEmail = e.Employee != null ? e.Employee.Email : null,
                LineManagerEmail = e.Employee != null && e.Employee.LineManager != null ? e.Employee.LineManager.Email : null
            };
        }
    }
}