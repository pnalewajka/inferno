﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.BusinessLogic;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class AllocationRequestToEmployeeTimelineProjectAllocationDtoMapping : ClassMapping<AllocationRequest, EmployeeTimelineProjectAllocationDto>
    {
        public AllocationRequestToEmployeeTimelineProjectAllocationDtoMapping()
        {
            Mapping = e => new EmployeeTimelineProjectAllocationDto
            {
                EmployeeFullName = e.Employee != null ? e.Employee.FullName : string.Empty,
                EmployeeId = e.Employee != null ? e.Employee.Id : default(long),
                HappenedOn = e.StartDate,
                ProjectName = ProjectBusinessLogic.ClientProjectName.Call(e.Project)
            };
        }
    }
}