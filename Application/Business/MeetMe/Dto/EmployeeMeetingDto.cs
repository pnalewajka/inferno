﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;
namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class EmployeeMeetingDto
    {
        public long Id { get; set; }

        public string EmployeeFullname { get; set; }

        public DateTime? EmployeeWorkStartDate { get; set; }

        public long? LineManagerId { get; set; }

        public string LineManagerName { get; set; }

        public string LineManagerEmail { get; set; }

        public string LocationName { get; set; }

        public int MeetingsDoneCount { get; set; }

        public MeetMeProcessStatus? MeetMeProcessStatus { get; set; }

        public DateTime? LastMeetingDate { get; set; }

        public DateTime? NextMeetingScheduledDate { get; set; }

        public DateTime? NextMeetingDueDate { get; set; }

        public DateTime? LastFeedbackDate { get; set; }
    }
}
