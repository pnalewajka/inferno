﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class EmployeeTimelineMeetingDto : EmployeeTimelineEventDto
    {
        public IList<DocumentDto> Attachments { get; set; }
        public IList<EmployeeTimelineMeetingNoteDto> Notes { get; set; }
    }
}