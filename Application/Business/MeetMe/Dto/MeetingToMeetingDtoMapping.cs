﻿using System.Linq;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Dictionaries;
using Smt.Atomic.Data.Entities.Modules.MeetMe;

namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class MeetingToMeetingDtoDMapping : ClassMapping<Meeting, MeetingDto>
    {
        public MeetingToMeetingDtoDMapping(
            IClassMapping<Employee, EmployeeDto> employeeMapping,
            IClassMapping<Location, LocationDto> locationMapping,
            IClassMapping<MeetingNote, MeetingNoteDto> noteDtoMapping)
        {
            Mapping = e => new MeetingDto
            {
                Documents = e.Attachments != null && e.Attachments.Any() ? e.Attachments.Select(CreateDocumentDto) : null,
                DueDate = e.MeetingDueDate,
                Employee = employeeMapping.CreateFromSource(e.Employee),
                Id = e.Id,
                LineManager = e.Employee.LineManager != null ? employeeMapping.CreateFromSource(e.Employee.LineManager) : null,
                Location = e.Employee.Location != null ? locationMapping.CreateFromSource(e.Employee.Location) : null,
                MeetingDate = e.ScheduledMeetingDate,
                Notes = e.Notes != null && e.Notes.Any() ? e.Notes.Select(noteDtoMapping.CreateFromSource) : null,
                Status = e.Status
            };
        }

        private DocumentDto CreateDocumentDto(MeetingAttachment attachment)
        {
            return new DocumentDto { DocumentName = attachment.Name, ContentType = attachment.ContentType, };
        }
    }
}

