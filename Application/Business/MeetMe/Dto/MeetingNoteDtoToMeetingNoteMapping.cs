﻿using Smt.Atomic.Data.Entities.Modules.MeetMe;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class MeetingNoteDtoToMeetingNoteMapping : ClassMapping<MeetingNoteDto, MeetingNote>
    {
        public MeetingNoteDtoToMeetingNoteMapping()
        {
            Mapping = d => new MeetingNote
            {
                Id = d.Id,
                MeetingId = d.MeetingId,
                AuthorId = d.AuthorId,
                NoteBody = d.NoteBody,
                CreatedOn = d.CreatedOn,
                Status = d.Status,
                Timestamp = d.Timestamp,
            };
        }
    }
}
