﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class MeetingDto
    {
        public long Id { get; set; }

        public EmployeeDto Employee { get; set; }

        public EmployeeDto LineManager { get; set; }

        public LocationDto Location { get; set; }

        public MeetingStatus Status { get; set; }

        public DateTime? DueDate { get; set; }

        public DateTime? MeetingDate { get; set; }

        public IEnumerable<MeetingNoteDto> Notes { get; set; }

        public IEnumerable<FeedbackDto> Feedbacks { get; set; }

        public IEnumerable<DocumentDto> Documents { get; set; }
    }
}