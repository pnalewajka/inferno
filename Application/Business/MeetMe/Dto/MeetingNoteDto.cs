﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class MeetingNoteDto
    {
        public long Id { get; set; }

        public long MeetingId { get; set; }      

        public long AuthorId { get; set; }

        public string AuthorName { get; set; }

        public string NoteBody { get; set; }

        public DateTime CreatedOn { get; set; }

        public NoteStatus Status { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public List<DocumentDto> Attachments { get; set; }

        public bool CanEdit { get; set; }
    }
}
