﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class EmploymentPeriodToEmployeeTimelineEmploymentPeriodDtoMapping : ClassMapping<EmploymentPeriod, EmployeeTimelineEmploymentPeriodDto>
    {
        public EmploymentPeriodToEmployeeTimelineEmploymentPeriodDtoMapping()
        {
            Mapping = e => new EmployeeTimelineEmploymentPeriodDto
            {
                EmployeeFullName = e.Employee != null ? e.Employee.FullName : string.Empty,
                EmployeeId = e.Employee != null ? e.EmployeeId : default(long),
                HappenedOn = e.StartDate,
                EndDate = e.EndDate,
                JobTitleName = e.JobTitle != null ? e.JobTitle.NameEn : string.Empty,
                ContractTypeName = e.ContractType != null ? e.ContractType.Name : null
            };
        }
    }
}