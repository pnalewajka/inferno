﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;
namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class MeetingDetailsDto
    {
        public long Id { get; set; }

        public DateTime? MeetingDueDate { get; set; }

        public DateTime? MeetingDate { get; set; }

        public MeetingStatus MeetingStatus { get; set; }

        public List<DocumentDto> Attachments { get; set; }

        public List<MeetingNoteDto> Notes { get; set; }

        public bool CanAddNote { get; set; }

        public bool CanBeEdited { get; set; }

        public string LineManagerEmail { get; set; }

        public string EmployeeEmail { get; set; }
    }
}