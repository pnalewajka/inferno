﻿using System;
using System.Collections.Generic;

namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class OverdueMeetingReminderDto
    {
        public string RecipientDisplayName { get; set; }

        public string RecipientEmail { get; set; }

        public IList<OverdueMeetingEmployeeDto> OverdueMeetingEmployee { get; set; }
    }

    public class OverdueMeetingEmployeeDto
    {
        public string EmployeeFullName { get; set; }

        public string EmployeeMeetingUrl { get; set; }

        public DateTime EmployeeNextMeetingDate { get; set; }
    }
}
