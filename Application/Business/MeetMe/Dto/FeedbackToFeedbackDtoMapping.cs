﻿using Smt.Atomic.Data.Entities.Modules.MeetMe;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class FeedbackToFeedbackDtoMapping : ClassMapping<Feedback, FeedbackDto>
    {
        public FeedbackToFeedbackDtoMapping()
        {
            Mapping = e => new FeedbackDto
            {
                Id = e.Id,
                AuthorName = e.Author != null ? e.Author.FullName : string.Empty,
                RecieverId = e.ReceiverId,
                ReceiverName = e.Receiver != null ? e.Receiver.FullName : string.Empty,
                FeedbackContent = e.FeedbackContent,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp,
                Visibility = e.Visibility
            };
        }
    }
}
