﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.MeetMe.Enums;
using Smt.Atomic.CrossCutting.Common.Models;

namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class EmployeeTimelineEmploymentPeriodDto : EmployeeTimelineEventDto
    {
        public string JobTitleName { get; set; }
        public DateTime? EndDate { get; set; }
        public LocalizedString ContractTypeName { get; set; }

        public string Header { get; set; }
        public string Body { get; set; }

        public List<EmploymentPeriodDtoDifference> GetEmploymentPeriodDtoDifference (EmployeeTimelineEmploymentPeriodDto other)
        {
            var differences = new List<EmploymentPeriodDtoDifference>();

            if (JobTitleName != other.JobTitleName)
            {
                differences.Add(EmploymentPeriodDtoDifference.JobTitleName);
            }

            if (ContractTypeName.English != other.ContractTypeName.English)
            {
                differences.Add(EmploymentPeriodDtoDifference.ContractType);
            }

            return differences;
        }
    }
}
