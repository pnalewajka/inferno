﻿using Smt.Atomic.Data.Entities.Modules.MeetMe;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class MeetingDetailsDtoToMeetingMapping : ClassMapping<MeetingDetailsDto, Meeting>
    {
        public MeetingDetailsDtoToMeetingMapping()
        {
            Mapping = d => new Meeting
            {
                Id = d.Id,
                MeetingDueDate = d.MeetingDueDate,
                Status = d.MeetingStatus,
                ScheduledMeetingDate = d.MeetingDate,
            };
        }
    }
}