﻿using Smt.Atomic.Data.Entities.Modules.MeetMe;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class FeedbackDtoToFeedbackMapping : ClassMapping<FeedbackDto, Feedback>
    {
        public FeedbackDtoToFeedbackMapping()
        {
            Mapping = d => new Feedback
            {
                Id = d.Id,
                ReceiverId = d.RecieverId,
                FeedbackContent = d.FeedbackContent,
                Timestamp = d.Timestamp,
                AuthorId = d.AuthorId,
                ModifiedOn = d.ModifiedOn,
                Visibility = d.Visibility
            };
        }
    }
}
