﻿using System;
using System.Linq;
using Atlassian.Jira;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.MeetMe.Helpers;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class IssueToFeedbackDtoMapping : ClassMapping<Issue, FeedbackDto>
    {
        private readonly IEmployeeService _employeeService;

        public IssueToFeedbackDtoMapping(IEmployeeService employeeService)
        {
            _employeeService = employeeService;

            Mapping = d => new FeedbackDto()
            {
                FeedbackContent = GetCustomFieldValueOrDefault(d.CustomFields, MeetMeJiraHelper.IssueCustomFieldKeyForFeedbackContent),
                Context = GetCustomFieldValueOrDefault(d.CustomFields, MeetMeJiraHelper.IssueCustomFieldKeyForContext),
                ModifiedOn = d.Updated.GetValueOrDefault(),
                AuthorId = GetAuthorId(d)
            };
        }

        private long GetAuthorId(Issue jiraIssue)
        {
            var employee = _employeeService.GetEmployeeOrDefaultByLogin(jiraIssue.Reporter);
            return employee?.Id ?? 0;
        }

        private string GetCustomFieldValueOrDefault(CustomFieldValueCollection customFields, string key)
        {
            return customFields.FirstOrDefault(x => String.Compare(x.Name, key, StringComparison.InvariantCultureIgnoreCase) == 0)?.Values
                       ?.FirstOrDefault() ?? string.Empty;
        }
    }
}
