﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class EmployeeMeetingDtoToEmployeeMapping : ClassMapping<EmployeeMeetingDto, Employee>
    {
        public EmployeeMeetingDtoToEmployeeMapping()
        {
            Mapping = d => new Employee
            {
                Id = d.Id
            };
        }
    }
}
