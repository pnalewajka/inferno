﻿using System;

namespace Smt.Atomic.Business.MeetMe.Dto
{
    public abstract class EmployeeTimelineEventDto
    {
        public long EmployeeId { get; set; }

        public DateTime HappenedOn { get; set; }

        public string EmployeeFullName { get; set; }

        public bool IsScheduled { get; set; }
    }
}
