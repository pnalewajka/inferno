﻿using System.Linq;

using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.MeetMe;

namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class MeetingDtoToMeetingMapping : ClassMapping<MeetingDto, Meeting>
    {
        public MeetingDtoToMeetingMapping()
        {
            Mapping = d => new Meeting()
            {
                Id = d.Id,
                EmployeeId = d.Employee.Id,
                MeetingDueDate = d.DueDate,
                ScheduledMeetingDate = d.MeetingDate,
                Status = d.Status
            };
        }
    }
}
