﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;

namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class EmployeeTimelineMeetingNoteDto : EmployeeTimelineEventDto
    {
        public string ContentBody { get; set; }
        public List<DocumentDto> Attachments { get; set; }
    }
}
