﻿namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class EmployeeTimelineLineManagerChangedDto : EmployeeTimelineEventDto
    {
        public long CurrentLineManagerId { get; set; }

        public string CurrentLineManagerFullName { get; set; }
    }
}