﻿using System;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class EmployeeTimelineFeedbackDto
    {
        public long AuthorId { get; set; }

        public string AuthorFullName { get; set; }

        public long ReceiverId { get; set; }

        public string ReceiverFullName { get; set; }

        public DateTime HappenedOn { get; set; }

        public string FeedbackContent { get; set; }

        public FeedbackVisbility Visibility { get; set; }
    }
}