﻿using Smt.Atomic.Data.Entities.Modules.MeetMe;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class MeetingInvitationDtoToMeetingMapping : ClassMapping<MeetingInvitationDto, Meeting>
    {
        public MeetingInvitationDtoToMeetingMapping()
        {
            Mapping = d => new Meeting
            {
                Id = d.Id,
                MeetingDueDate = d.MeetingDueDate,
                ScheduledMeetingDate = d.MeetingDate,
            };
        }
    }
}