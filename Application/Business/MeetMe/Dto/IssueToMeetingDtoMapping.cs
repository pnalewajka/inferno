﻿using System;
using System.Data.SqlTypes;
using System.Linq;

using Atlassian.Jira;

using Castle.Core.Logging;

using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.MeetMe.Helpers;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.Business.MeetMe.Dto
{

    public class IssueToMeetingDtoMapping : ClassMapping<Issue, MeetingDto>
    {
        private readonly IEmployeeService _employeeService;
        private readonly ITimeService _timeService;
        private readonly ILogger _logger;

        public IssueToMeetingDtoMapping(IEmployeeService employeeService, ILogger logger, ITimeService timeService)
        {
            _employeeService = employeeService;
            _logger = logger;
            _timeService = timeService;

            Mapping = d => CreateDto(d);
        }

        private MeetingDto CreateDto(Issue jiraIssue)
        {
            var login = GetCustomFieldValueOrDefault(jiraIssue.CustomFields, MeetMeJiraHelper.IssueCustomFieldKeyForEmployee);
            var employee = string.IsNullOrWhiteSpace(login) ? null : _employeeService.GetEmployeeOrDefaultByLogin(login);

            var meetingDateString = GetCustomFieldValueOrDefault(jiraIssue.CustomFields, MeetMeJiraHelper.MeetingDateFieldName);
            var meetingDate = string.IsNullOrWhiteSpace(meetingDateString) ? (DateTime)SqlDateTime.MinValue : DateTime.Parse(meetingDateString);

            if (employee == null || employee.Id == default(long))
            {
                _logger.Warn($"There is no employee for login: {login}");
            }

            return new MeetingDto
            {
                Employee = employee,
                Location = employee != null ? new LocationDto { Id = employee.LocationId.GetValueOrDefault() } : null,
                Status = meetingDate > _timeService.GetCurrentDate() ? MeetingStatus.Scheduled : MeetingStatus.Done,
                LineManager = _employeeService.GetEmployeeOrDefaultByLogin(GetCustomFieldValueOrDefault(jiraIssue.CustomFields, MeetMeJiraHelper.IssueCustomFieldKeyForLineManager)),
                MeetingDate = meetingDate
            };
        }

        private string GetCustomFieldValueOrDefault(CustomFieldValueCollection customFields, string key)
        {
            return customFields.FirstOrDefault(x => String.Compare(x.Name, key, StringComparison.InvariantCultureIgnoreCase) == 0)?.Values?.FirstOrDefault() ?? string.Empty;
        }
    }
}
