﻿using System;
using System.Linq;
using Smt.Atomic.Business.MeetMe.BusinessLogics;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace Smt.Atomic.Business.MeetMe.Dto
{
    public class EmployeeToEmployeeMeetingDtoMapping : ClassMapping<Employee, EmployeeMeetingDto>
    {
        private readonly ITimeService _timeService;

        public EmployeeToEmployeeMeetingDtoMapping(ITimeService timeService)
        {
            _timeService = timeService;

            Mapping = e => new EmployeeMeetingDto
            {
                Id = e.Id,
                EmployeeFullname = e.FullName,
                EmployeeWorkStartDate = e.EmploymentPeriods != null && e.EmploymentPeriods.Any() ? e.GetFirstEmploymentPeriod().StartDate : default(DateTime?),
                LineManagerId = e.LineManagerId,
                LineManagerName = e.LineManager != null ? e.LineManager.DisplayName : null,
                LineManagerEmail = e.LineManager != null ? e.LineManager.Email : null,
                MeetMeProcessStatus = e.Meetings != null && e.Meetings.Any()
                    ? MeetingBusinessLogics.GetMeetMeProcessStatus.Call(e.Meetings.AsQueryable().Where(x => x.Status != MeetingStatus.Done), _timeService.GetCurrentDate())
                    : MeetMeProcessStatus.NoMeetingCalculated,
                NextMeetingScheduledDate = e.Meetings != null
                    ? MeetingBusinessLogics.GetNextMeetingScheduledDate.Call(e.Meetings.AsQueryable().Where(x => x.Status == MeetingStatus.Scheduled))
                    : default(DateTime?),
                NextMeetingDueDate = e.Meetings != null
                    ? MeetingBusinessLogics.GetNextMeetingDueDate.Call(e.Meetings.AsQueryable().Where(x => x.Status == MeetingStatus.Pending))
                    : default(DateTime?),
                LastMeetingDate = e.Meetings != null
                    ? MeetingBusinessLogics.GetLastMeetingDate.Call(e.Meetings.AsQueryable().Where(x => x.Status == MeetingStatus.Done))
                    : default(DateTime?),
                LocationName = e.Location != null ? e.Location.Name : null,
                MeetingsDoneCount = e.Meetings != null
                    ? e.Meetings.Count(m => m.Status == MeetingStatus.Done)
                    : default(int),
                LastFeedbackDate = e.Feedbacks != null && e.Feedbacks.Any()
                    ? e.Feedbacks.Max(f => f.ModifiedOn)
                    : default(DateTime?)
            };
        }
    }
}
