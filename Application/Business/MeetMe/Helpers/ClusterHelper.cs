﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Smt.Atomic.Business.MeetMe.Helpers
{
    public static class ClusterHelper
    {
        public static IEnumerable<IGrouping<TMetric, TObject>> CreateClusteredCollection<TMetric, TObject>(
            IEnumerable<TObject> collection,
            Func<TObject, object> orderExpression,
            Func<TObject, TMetric, bool>groupRule,
            Func<TObject, TMetric> getMetricExpression)
        {
            var list = new List<Group<TMetric, TObject>>();

            if (!collection.Any())
                return list;

            var orderedCollection = collection.OrderBy(orderExpression);

            foreach (var current in orderedCollection)
            {
                var cluster = list.FirstOrDefault(x => groupRule.Invoke(current, x.Key));

                if (cluster!=null && cluster.Any())
                {
                    cluster.Add(current);
                }
                else
                {
                    list.Add(new Group<TMetric, TObject>(getMetricExpression.Invoke(current)) { current });
                }
            }

            return list;
        }

        public static IEnumerable<IGrouping<TMetric, TObject>> CreateClusteredCollection<TMetric, TObject>(
            IEnumerable<TObject> collectionToClustering,
            IEnumerable<TMetric> metrics,
            Func<TObject, object> orderExpression,
            Func<TMetric, TObject, bool> groupRule)
        {
            var list = new List<Group<TMetric, TObject>>();

            if (!(collectionToClustering.Any() && metrics.Any()))
            {
                return list;
            }

            var orderedCollection = collectionToClustering.OrderBy(orderExpression);

            foreach (var metric in metrics)
            {
                var metricsItems = orderedCollection.Where(x => groupRule.Invoke(metric,x));
                var group = new Group<TMetric, TObject>(metric);
                group.AddRange(metricsItems);
                list.Add(group);
            }

            return list;
        }

        private class Group<TKey, TSource> : List<TSource>, IGrouping<TKey, TSource>
        {
            public Group(TKey key)
            {
                Key = key;
            }

            public TKey Key { get; }
        }
    }
}
