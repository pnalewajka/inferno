﻿namespace Smt.Atomic.Business.MeetMe.Helpers
{
    public class MeetMeJiraHelper
    {
        public const string StatusFieldName = "status";
        public const string MeetMeJiraProject = "MGMT";
        public const string MeetMeJiraIssueType = "meet.me";
        public const string MeetMeFeedbackJiraIssueType = "Meet.me: Feedback";
        public const string MeetingDateFieldName = "Meeting Date";
        public const string NextMeetingDateFieldName = "Next meeting date";
        public const string IssueCustomFieldKeyForEmployee = "Employee";
        public const string IssueCustomFieldKeyForLineManager = "Line Manager";
        public const string IssueCustomFieldKeyForContext = "Context of cooperation";
        public const string IssueCustomFieldKeyForFeedbackContent = "Content of feedback";
    }
}
