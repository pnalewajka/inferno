﻿namespace Smt.Atomic.Business.MeetMe.Helpers
{
    public static class FilterCodes
    {
        public const string MyEmployees = "my-employees";
        public const string MyDirectEmployees = "my-direct-employees";
        public const string AllEmployees = "all-employees";
        public const string OrgUnitsFilter = "org-units-filter";
        public const string Terminated = "terminated";
        public const string Working = "working";
    }
}