﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.Business.MeetMe.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.Business.MeetMe.Services
{
    public class FeedbackEmailNotificationService : IFeedbackEmailNotificationService
    {
        private readonly IReliableEmailService _reliableEmailService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IMessageTemplateService _messageTemplateService;

        public FeedbackEmailNotificationService(
            IReliableEmailService reliableEmailService,
            ISystemParameterService systemParameterService,
            IMessageTemplateService messageTemplateService)
        {
            _reliableEmailService = reliableEmailService;
            _systemParameterService = systemParameterService;
            _messageTemplateService = messageTemplateService;
        }

        public void NotifyAboutFeedback(FeedbackRequestDto feedbackRequestDto, RequestDto requestDto)
        {
            var fromAddress = GetPortalAddress();

            var emailSubject = _messageTemplateService.ResolveTemplate(TemplateCodes.FeedbackRequestNotificationMessageSubject, feedbackRequestDto);
            var emailBody = _messageTemplateService.ResolveTemplate(TemplateCodes.FeedbackRequestNotificationMessageBody, feedbackRequestDto);
            var email = new EmailMessageDto
            {
                IsHtml = emailBody.IsHtml,
                Subject = emailSubject.Content,
                Body = emailBody.Content,
                From = fromAddress,
            };

            email.Recipients.AddRange(CreateApproverRecipients(requestDto));

            Send(email);
        }

        private void Send(EmailMessageDto message)
        {
            _reliableEmailService.EnqueueMessage(message);
        }

        private EmailRecipientDto GetPortalAddress()
        {
            return new EmailRecipientDto
            {
                EmailAddress = _systemParameterService.GetParameter<string>(ParameterKeys.SmtpEmailServiceDefaultFromEmailAddress),
                FullName = _systemParameterService.GetParameter<string>(ParameterKeys.SmtpEmailServiceDefaultFromFullName)
            };
        }

        private EmailRecipientDto[] CreateApproverRecipients(RequestDto request)
        {
            if (!request.CurrentApprovalGroupId.HasValue)
            {
                return new EmailRecipientDto[0];
            }

            var approvers = request.ApprovalGroups
                .Single(g => g.Id == request.CurrentApprovalGroupId).Approvals
                .GroupBy(a => a.UserId).Select(a => a.First());

            return approvers.Select(approval => new EmailRecipientDto
            {
                EmailAddress = approval.ApproverEmail,
                FullName = approval.ApproverFullName
            }).ToArray();
        }
    }
}
