﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.Business.MeetMe.Interfaces;
using Smt.Atomic.Business.MeetMe.Resources;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.MeetMe;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.MeetMe.Services
{
    public class FeedbackCardIndexService : CardIndexDataService<FeedbackDto, Feedback, IMeetMeDbScope>, IFeedbackCardIndexService
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly IRequestCardIndexDataService _requestCardIndexDataService;
        private readonly IClassMapping<FeedbackDto, FeedbackRequestDto> _feedbackDtoToFeedbackRequestDtoMapper;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public FeedbackCardIndexService(
            ICardIndexServiceDependencies<IMeetMeDbScope> dependencies,
            IPrincipalProvider principalProvider,
            IRequestCardIndexDataService requestCardIndexDataService,
            IClassMapping<FeedbackDto, FeedbackRequestDto> feedbackDtoToFeedbackRequestDtoMapper
            )
            : base(dependencies)
        {
            _principalProvider = principalProvider;
            _requestCardIndexDataService = requestCardIndexDataService;
            _feedbackDtoToFeedbackRequestDtoMapper = feedbackDtoToFeedbackRequestDtoMapper;
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<Feedback> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var direction = sortingCriterion.GetSortingDirection();

            switch (sortingCriterion.GetSortingColumnName())
            {
                case nameof(MeetingNoteDto.AuthorName):
                    orderedQueryBuilder.ApplySortingKey(m => m.Author.FirstName, direction);
                    orderedQueryBuilder.ApplySortingKey(m => m.Author.LastName, direction);
                    break;
                default:
                    base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
                    break;
            }
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<Feedback, FeedbackDto, IMeetMeDbScope> eventArgs)
        {
            eventArgs.InputEntity.AuthorId = _principalProvider.Current.EmployeeId;
            base.OnRecordAdding(eventArgs);
        }

        protected override AddRecordResult InternalAddRecord(FeedbackDto record, object context)
        {
            record.AuthorId = _principalProvider.Current.EmployeeId;
            var requestDto = _feedbackDtoToFeedbackRequestDtoMapper.CreateFromSource(record);
            requestDto.OriginScenario = FeedbackOriginScenario.OwnInitiative;
            requestDto.Comment = MeetingResources.CommentContent;
            var result = _requestCardIndexDataService.CreateRequestsExternally(RequestType.FeedbackRequest, new List<FeedbackRequestDto> { requestDto });

            return new AddRecordResult(result.IsSuccessful, result.AddedRecordIds.FirstOrDefault(), result.UniqueKeyViolations, result.Alerts);
        }

        public override void SetContext(Feedback entity, object context)
        {
            var parentIdContext = context as ParentIdContext;

            if (parentIdContext != null && parentIdContext.ParentId != default(long))
            {
                entity.ReceiverId = parentIdContext.ParentId;
            }
        }

        public override IQueryable<Feedback> ApplyContextFiltering(IQueryable<Feedback> records, object context)
        {
            var parentIdContext = context as ParentIdContext;

            return parentIdContext != null ?
                records.Where(r => r.ReceiverId == parentIdContext.ParentId)
                : records;
        }
    }
}
