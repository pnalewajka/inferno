﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.MeetMe.DocumentMappings;
using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.Business.MeetMe.Interfaces;
using Smt.Atomic.Business.MeetMe.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.Data.Entities.Modules.MeetMe;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.MeetMe.Services
{
    public class MeetingDetailsCardIndexService : CardIndexDataService<MeetingDetailsDto, Meeting, IMeetMeDbScope>, IMeetingDetailsCardIndexService
    {
        private readonly IUploadedDocumentHandlingService _uploadedDocumentHandlingService;
        private readonly IMeetingService _meetingService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public MeetingDetailsCardIndexService(
            ICardIndexServiceDependencies<IMeetMeDbScope> dependencies,
            IUploadedDocumentHandlingService uploadedDocumentHandlingService1,
            IMeetingService meetingService)
            : base(dependencies)
        {
            _uploadedDocumentHandlingService = uploadedDocumentHandlingService1;
            _meetingService = meetingService;
        }

        public override IQueryable<Meeting> ApplyContextFiltering(IQueryable<Meeting> records, object context)
        {
            var parentIdContext = context as ParentIdContext;

            return parentIdContext != null
                ? records.Where(r => r.EmployeeId == parentIdContext.ParentId)
                : base.ApplyContextFiltering(records, context);
        }

        public override void SetContext(Meeting entity, object context)
        {
            var parentIdContext = context as ParentIdContext;

            if (parentIdContext != null)
            {
                using (var unitOfWork = UnitOfWorkService.Create())
                {
                    var parentEmployee = unitOfWork.Repositories.Employees.Single(x => x.Id == parentIdContext.ParentId);
                    entity.EmployeeId = parentEmployee.Id;
                }
            }
        }
        
        protected override void OnRecordAdding(RecordAddingEventArgs<Meeting, MeetingDetailsDto, IMeetMeDbScope> eventArgs)
        {
            _uploadedDocumentHandlingService.MergeDocumentCollections(eventArgs.UnitOfWork,
                eventArgs.InputEntity.Attachments,
                eventArgs.InputDto.Attachments,
                p => new MeetingAttachment
                {
                    Name = p.DocumentName,
                    ContentType = p.ContentType
                });
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<Meeting, MeetingDetailsDto, IMeetMeDbScope> eventArgs)
        {
            if (!CanEditMeeting(eventArgs.InputDto.MeetingStatus))
                throw new BusinessException(AlertResources.FinishedMeetingException);

            _uploadedDocumentHandlingService.MergeDocumentCollections(eventArgs.UnitOfWork,
                eventArgs.DbEntity.Attachments,
                eventArgs.InputDto.Attachments,
                p => new MeetingAttachment
                {
                    Name = p.DocumentName,
                    ContentType = p.ContentType
                });
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<Meeting, MeetingDetailsDto> recordAddedEventArgs)
        {
            _uploadedDocumentHandlingService.PromoteTemporaryDocuments<MeetingAttachment, MeetingAttachmentContent, MeetingAttachmentMapping, IMeetMeDbScope>(recordAddedEventArgs.InputDto.Attachments);
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<Meeting, MeetingDetailsDto> recordAddedEventArgs)
        {
            _uploadedDocumentHandlingService.PromoteTemporaryDocuments<MeetingAttachment, MeetingAttachmentContent, MeetingAttachmentMapping, IMeetMeDbScope>(recordAddedEventArgs.InputDto.Attachments);
        }

        protected override void OnRecordsChanged(RecordChangedEventArgs<Meeting> eventArgs)
        {
            var meetingIds = eventArgs.ChangedEntities.Select(x => x.Id);
            _meetingService.UpdateMeetingStatus(meetingIds);
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<Meeting, IMeetMeDbScope> eventArgs)
        {
			base.OnRecordDeleting(eventArgs);
			
            if (eventArgs.DeletingRecord.Notes.Any())
            {
                eventArgs.Alerts.Add(new AlertDto
                {
                    Message = string.Format(AlertResources.MeetingWithExistingNotesDeletionAlert, eventArgs.DeletingRecord.Notes.Count),
                    Type = AlertType.Error
                });
            }
        }

        public bool CanEditMeeting(long meetingId)
        {
            var meetingStatus = _meetingService.GetMeetingById(meetingId).Status;

            return meetingStatus != MeetingStatus.Done;
        }

        public bool CanEditMeeting(MeetingStatus meetingStatus)
        {
            return meetingStatus != MeetingStatus.Done;
        }

        public BusinessResult FinishMeeting(long meetingId)
        {
            return _meetingService.FinishMeeting(meetingId);
        }
    }
}