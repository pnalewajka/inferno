﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Allocation.Filters;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.Business.MeetMe.Helpers;
using Smt.Atomic.Business.MeetMe.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.MeetMe.Services
{
    public class EmployeeMeetingCardIndexDataService : CardIndexDataService<EmployeeMeetingDto, Employee, IMeetMeDbScope>, IEmployeeMeetingCardIndexDataService
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly IOrgUnitService _orgUnitService;
        private readonly ITimeService _timeService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public EmployeeMeetingCardIndexDataService(
            ICardIndexServiceDependencies<IMeetMeDbScope> dependencies,
            IPrincipalProvider principalProvider,
            IOrgUnitService orgUnitService,
            ITimeService timeService)
            : base(dependencies)
        {
            _principalProvider = principalProvider;
            _orgUnitService = orgUnitService;
            _timeService = timeService;
        }

        protected override IQueryable<Employee> ConfigureIncludes(IQueryable<Employee> sourceQueryable)
        {
            return base.ConfigureIncludes(sourceQueryable.Include(e => e.Meetings));
        }

        protected override IQueryable<Employee> ApplyBaseFilter(IQueryable<Employee> records)
        {
            Expression<Func<Employee, bool>> hasAnyMeeting =
                e => e.Meetings.Filtered().Any();

            return records.Where(hasAnyMeeting);
        }

        protected override NamedFilters<Employee> NamedFilters =>
            new NamedFilters<Employee>(new[]
            {
                MyEmployeesFilter.GetDescendantEmployeeFilter(_orgUnitService, _principalProvider, SecurityRoleType.CanViewMyEmployeeMeetings),
                MyEmployeesFilter.GetChildEmployeeFilter(_orgUnitService, _principalProvider, SecurityRoleType.CanViewMyEmployeeMeetings),
                new NamedFilter<Employee>(FilterCodes.AllEmployees, e => true, SecurityRoleType.CanViewAllEmployeeMeetings),
                OrgUnitsFilter.EmployeeFilter,
                LineManagerFilter.EmployeeFilter,
                new NamedFilter<Employee>(FilterCodes.Working, e => e.CurrentEmployeeStatus != EmployeeStatus.Terminated),
                new NamedFilter<Employee>(FilterCodes.Terminated, e => e.CurrentEmployeeStatus == EmployeeStatus.Terminated, SecurityRoleType.CanViewEmployeeStatus)
            });

        protected override void ApplySortingCriterion(OrderedQueryBuilder<Employee> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var direction = sortingCriterion.GetSortingDirection();

            switch (sortingCriterion.GetSortingColumnName())
            {
                case nameof(EmployeeMeetingDto.LineManagerName):
                    orderedQueryBuilder.ApplySortingKey(m => m.LineManager.FirstName, direction);
                    orderedQueryBuilder.ApplySortingKey(m => m.LineManager.LastName, direction);
                    break;

                case nameof(EmployeeMeetingDto.MeetingsDoneCount):
                    orderedQueryBuilder.ApplySortingKey(m => m.Meetings.DefaultIfEmpty().Count(x => x.Status == MeetingStatus.Done), direction);
                    break;

                case nameof(EmployeeMeetingDto.MeetMeProcessStatus):
                    var currentDate = _timeService.GetCurrentDate();
                    orderedQueryBuilder.ApplySortingKey(
                        m =>
                            m.Meetings.Any()
                                ? m.Meetings.Max(o => o.MeetingDueDate) < currentDate
                                    ? MeetMeProcessStatus.Overdue
                                    : MeetMeProcessStatus.Scheduled
                                : MeetMeProcessStatus.NoMeetingCalculated, direction);
                    break;

                case nameof(EmployeeMeetingDto.EmployeeWorkStartDate):
                    orderedQueryBuilder.ApplySortingKey(m => m.EmploymentPeriods
                        .DefaultIfEmpty()
                        .Min(o => o.StartDate), direction);
                    break;

                case nameof(EmployeeMeetingDto.LastMeetingDate):
                    orderedQueryBuilder.ApplySortingKey(m => m.Meetings
                        .Where(n => n.Status == MeetingStatus.Done)
                        .DefaultIfEmpty()
                        .Max(o => o.ScheduledMeetingDate), direction);
                    break;

                case nameof(EmployeeMeetingDto.LastFeedbackDate):
                    orderedQueryBuilder.ApplySortingKey(m => m.Feedbacks
                        .DefaultIfEmpty()
                        .Max(o => o.ModifiedOn), direction);
                    break;

                case nameof(EmployeeMeetingDto.NextMeetingScheduledDate):
                    orderedQueryBuilder.ApplySortingKey(m => m.Meetings
                        .Where(n => n.Status == MeetingStatus.Scheduled)
                        .DefaultIfEmpty()
                        .Min(o => o.ScheduledMeetingDate), direction);
                    break;

                case nameof(EmployeeMeetingDto.NextMeetingDueDate):
                    orderedQueryBuilder.ApplySortingKey(m => m.Meetings
                        .Where(n => n.Status == MeetingStatus.Pending)
                        .DefaultIfEmpty()
                        .Min(o => o.MeetingDueDate), direction);
                    break;

                default:
                    base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
                    break;
            }
        }

        protected override IEnumerable<Expression<Func<Employee, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return m => m.Acronym;
            yield return m => m.FirstName;
            yield return m => m.LastName;
        }
    }
}
