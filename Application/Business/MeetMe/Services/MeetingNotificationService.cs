﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Castle.Core.Logging;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.Business.MeetMe.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.MeetMe.Services
{
    public class MeetingNotificationService : IMeetingNotificationService
    {
        private readonly IUnitOfWorkService<IMeetMeDbScope> _meetingUnitOfWorkService;
        private readonly ITimeService _timeService;
        private readonly IClassMapping<Employee, EmployeeMeetingDto> _employeeToEmployeeMeetingDtoMapping;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly ILogger _logger;

        private const string EmployeeMeetingsUrl = "{0}MeetMe/MeetingDetails/List?parent-id={1}";
        private readonly string _serverAddress;

        public MeetingNotificationService(IUnitOfWorkService<IMeetMeDbScope> meetingUnitOfWorkService,
            ITimeService timeService,
            IClassMapping<Employee, EmployeeMeetingDto> employeeToEmployeeMeetingDtoMapping,
            IMessageTemplateService messageTemplateService,
            IReliableEmailService reliableEmailService,
            ISystemParameterService systemParameterService,
            ILogger logger)
        {
            _meetingUnitOfWorkService = meetingUnitOfWorkService;
            _timeService = timeService;
            _employeeToEmployeeMeetingDtoMapping = employeeToEmployeeMeetingDtoMapping;
            _messageTemplateService = messageTemplateService;
            _reliableEmailService = reliableEmailService;
            _serverAddress = systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress);
            _logger = logger;
        }

        public void SendNotificationForOvedueMeetings(CancellationToken cancellationToken)
        {
            using (var meetingUnitOfWork = _meetingUnitOfWorkService.Create())
            {
                var overDuedEmployeeMeetings = meetingUnitOfWork.Repositories.Employees
                    .Where(e => e.CurrentEmployeeStatus != EmployeeStatus.Terminated)
                    .AsEnumerable()
                    .Select(e => _employeeToEmployeeMeetingDtoMapping.CreateFromSource(e))
                    .Where(e => e.MeetMeProcessStatus == MeetMeProcessStatus.Overdue)
                    .GroupBy(e => new { e.LineManagerName, e.LineManagerEmail })
                    .Select(e => new OverdueMeetingReminderDto
                    {
                        RecipientDisplayName = e.Key.LineManagerName,
                        RecipientEmail = e.Key.LineManagerEmail,
                        OverdueMeetingEmployee = e.Select(d => new OverdueMeetingEmployeeDto
                        {
                            EmployeeFullName = d.EmployeeFullname,
                            EmployeeMeetingUrl = string.Format(EmployeeMeetingsUrl, _serverAddress, d.Id),
                            EmployeeNextMeetingDate = d.NextMeetingScheduledDate ?? d.NextMeetingDueDate.GetValueOrDefault()
                        }).ToList()
                    })
                    .ToList();

                foreach (var overDuedEmployeeMeeting in overDuedEmployeeMeetings.TakeWhileNotCancelled(cancellationToken))
                {
                    if (!string.IsNullOrEmpty(overDuedEmployeeMeeting.RecipientEmail))
                    {
                        SendEmail(overDuedEmployeeMeeting);
                    }
                }
            }
        }

        private void SendEmail(OverdueMeetingReminderDto overdueMeetingReminderDto)
        {
            try
            {
                var emailRecipientDto = new EmailRecipientDto
                {
                    EmailAddress = overdueMeetingReminderDto.RecipientDisplayName,
                    FullName = overdueMeetingReminderDto.RecipientEmail,
                };

                var emailSubject = _messageTemplateService.ResolveTemplate(TemplateCodes.OverdueMeetingReminderSubject, overdueMeetingReminderDto);
                var emailBody = _messageTemplateService.ResolveTemplate(TemplateCodes.OverdueMeetingReminderBody, overdueMeetingReminderDto);

                var emailMessageDto = new EmailMessageDto
                {
                    IsHtml = emailBody.IsHtml,
                    Subject = emailSubject.Content,
                    Body = emailBody.Content,
                    Recipients = new List<EmailRecipientDto> { emailRecipientDto }
                };

                _reliableEmailService.EnqueueMessage(emailMessageDto);
            }
            catch (System.Exception ex)
            {
                _logger.Info($"{nameof(MeetingNotificationService)} failed on method {nameof(SendEmail)} - LineManager : {overdueMeetingReminderDto.RecipientDisplayName} - {overdueMeetingReminderDto.RecipientEmail}", ex);
            }
        }
    }
}
