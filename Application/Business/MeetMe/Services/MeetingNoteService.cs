﻿using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.MeetMe.BusinessLogics;
using Smt.Atomic.Business.MeetMe.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.MeetMe.Services
{
    public class MeetingNoteService : IMeetingNoteService
    {
        private readonly IUnitOfWorkService<IMeetMeDbScope> _meetingsUnitOfWorkService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly ITimeService _timeService;
        private readonly ISystemParameterService _systemParameterService;

        public MeetingNoteService(IUnitOfWorkService<IMeetMeDbScope> meetingsUnitOfWorkService,
            IPrincipalProvider principleProvider,
            ITimeService timeService,
            ISystemParameterService systemParameterService)
        {
            _meetingsUnitOfWorkService = meetingsUnitOfWorkService;
            _principalProvider = principleProvider;
            _timeService = timeService;
            _systemParameterService = systemParameterService;
        }

        public bool CanEditNote(long noteId)
        {
            using (var meetingsUnitOfWork = _meetingsUnitOfWorkService.Create())
            {
                var note = meetingsUnitOfWork.Repositories.MeetingNotes.GetById(noteId);

                var currentTime = _timeService.GetCurrentTime();
                var canAddOrEditDaysMin = _systemParameterService.GetParameter<int>(ParameterKeys.AddOrEditNoteForLastOwnMeetingDaysMin);
                var canAddOrEditDaysMax = _systemParameterService.GetParameter<int>(ParameterKeys.AddOrEditNoteForLastOwnMeetingDaysMax);
                var currentEmployeeId = _principalProvider.Current.EmployeeId;
                var canEditDays = _systemParameterService.GetParameter<int>(ParameterKeys.EditDoneMeetingDays);
                var canSeeEmployeeMeetings = _principalProvider.Current.IsInRole(SecurityRoleType.CanViewAllEmployeeMeetings)
                    || EmployeeBusinessLogic.HasGivenLineManager.Call(note.Meeting.Employee, currentEmployeeId);

                var canEmployeeEditOwnMeetingNote = note.Status != NoteStatus.Done
                            && note.AuthorId == currentEmployeeId
                            && _principalProvider.Current.IsInRole(SecurityRoleType.CanAddOrEditNoteForLastOwnMeeting)
                            && MeetingBusinessLogics.CanAddOrEditNoteForLastOwnMeeting.Call(note.Meeting, currentTime, canAddOrEditDaysMin, canAddOrEditDaysMax, currentEmployeeId);
                var canEdit = canSeeEmployeeMeetings && note.Status != NoteStatus.Done;
                var canEditAfterMeetingDone = canSeeEmployeeMeetings
                            && MeetingBusinessLogics.CanEditDoneMeetingNote.Call(note.Meeting, currentTime, canEditDays);

                return canEmployeeEditOwnMeetingNote || canEdit || canEditAfterMeetingDone;
            }
        }

        public bool CanAddNote(long meetingId)
        {
            using (var meetingUnitOfWork = _meetingsUnitOfWorkService.Create())
            {
                var meeting = meetingUnitOfWork.Repositories.Meetings.GetById(meetingId);
                var currentTime = _timeService.GetCurrentTime();
                var canAddOrEditDaysMin = _systemParameterService.GetParameter<int>(ParameterKeys.AddOrEditNoteForLastOwnMeetingDaysMin);
                var canAddOrEditDaysMax = _systemParameterService.GetParameter<int>(ParameterKeys.AddOrEditNoteForLastOwnMeetingDaysMax);
                var currentEmployeeId = _principalProvider.Current.EmployeeId;
                var canEditDays = _systemParameterService.GetParameter<int>(ParameterKeys.EditDoneMeetingDays);
                var canSeeEmployeeMeetings = _principalProvider.Current.IsInRole(SecurityRoleType.CanViewAllEmployeeMeetings)
                    || (meeting.Employee != null ? EmployeeBusinessLogic.HasGivenLineManager.Call(meeting.Employee, currentEmployeeId) : false);

                var canEmployeeAddOwnMeetingNote = meeting.Status != MeetingStatus.Done
                            && _principalProvider.Current.IsInRole(SecurityRoleType.CanAddOrEditNoteForLastOwnMeeting)
                            && MeetingBusinessLogics.CanAddOrEditNoteForLastOwnMeeting.Call(meeting, currentTime, canAddOrEditDaysMin, canAddOrEditDaysMax, currentEmployeeId);
                var canAdd = canSeeEmployeeMeetings && meeting.Status != MeetingStatus.Done;
                var canAddAfterMeetingDone = canSeeEmployeeMeetings
                            && MeetingBusinessLogics.CanEditDoneMeetingNote.Call(meeting, currentTime, canEditDays);

                return canEmployeeAddOwnMeetingNote || canAdd || canAddAfterMeetingDone;
            }
        }
    }
}