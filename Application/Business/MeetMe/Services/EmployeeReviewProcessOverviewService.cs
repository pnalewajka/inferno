﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.Business.MeetMe.Enums;
using Smt.Atomic.Business.MeetMe.Interfaces;
using Smt.Atomic.Business.MeetMe.Resources;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.MeetMe;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.MeetMe.Services
{
    public class EmployeeReviewProcessOverviewService : IEmployeeReviewProcessOverview
    {
        private readonly IUnitOfWorkService<IMeetMeDbScope> _meetingsUnitOfWorkService;
        private readonly IClassMappingFactory _classMappingFactory;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IOrgUnitService _orgUnitService;
        private int _numberOfUpgrades;

        public EmployeeReviewProcessOverviewService(
            IUnitOfWorkService<IMeetMeDbScope> meetingsUnitOfWorkService,
            IClassMappingFactory classMappingFactory,
            IPrincipalProvider principalProvider,
            IOrgUnitService orgUnitService)
        {
            _meetingsUnitOfWorkService = meetingsUnitOfWorkService;
            _classMappingFactory = classMappingFactory;
            _principalProvider = principalProvider;
            _orgUnitService = orgUnitService;
        }

        public EmployeeReviewProcessOverviewDto GetEmployeeReviewProcessOverview(long employeeId)
        {
            var result = new EmployeeReviewProcessOverviewDto();

            using (var meetingsUnitOfWork = _meetingsUnitOfWorkService.Create())
            {
                result.Meetings = GetMeetings(meetingsUnitOfWork, employeeId);
                result.Feedbacks = GetFeedbacks(meetingsUnitOfWork, employeeId);
                result.EmploymentPeriods = GetEmploymentPeriods(meetingsUnitOfWork, employeeId);
                result.AllocationRequests = GetAllocationRequests(meetingsUnitOfWork, employeeId);
                result.EmployeeFullName = GetEmployeeName(meetingsUnitOfWork, employeeId);
                result.LineManagerChanges = GetLineManagerChanges(meetingsUnitOfWork, employeeId);
            }

            return result;
        }

        public BoolResult CheckEmployeeOverviewRestrictions(long requestedEmployeeId)
        {
            using (var meetingsUnitOfWork = _meetingsUnitOfWorkService.Create())
            {
                var result = new BoolResult(false);
                var employeeId = _principalProvider.Current.EmployeeId;
                var managerOrgUnitsIds = _orgUnitService.GetManagerOrgUnitDescendantIds(employeeId);
                var requestedEmployee = meetingsUnitOfWork.Repositories.Employees.SingleOrDefault(e => e.Id == requestedEmployeeId);

                if (requestedEmployee == null)
                {
                    result.Alerts.Add(new AlertDto { Type = AlertType.Error, Message = AlertResources.GeneralOverviewRestrictionError });
                }

                if (!result.ContainsErrors)
                {
                    var isMyEmployee = EmployeeBusinessLogic.IsEmployeeOf.Call(requestedEmployee, employeeId, managerOrgUnitsIds);

                    if (_principalProvider.Current.IsInRole(SecurityRoleType.CanViewAllEmployeeMeetings)
                        || (_principalProvider.Current.IsInRole(SecurityRoleType.CanViewMyEmployeeMeetings) && isMyEmployee)
                        || requestedEmployeeId == employeeId)
                    {
                        result.IsSuccessful = true;
                    }
                    else
                    {
                        result.Alerts.Add(new AlertDto { Type = AlertType.Error, Message = AlertResources.GeneralOverviewRestrictionError });
                    }
                }

                return result;
            }
        }

        private string GetEmployeeName(IUnitOfWork<IMeetMeDbScope> meetingsUnitOfWork, long employeeId)
        {
            var employee = meetingsUnitOfWork.Repositories.Employees
                            .Where(e => e.Id == employeeId)
                            .Select(e => new { e.FirstName, e.LastName })
                            .SingleOrDefault();

            return employee != null ? $"{employee.FirstName} {employee.LastName}" : null;
        }

        private IList<EmployeeTimelineMeetingDto> GetMeetings(IUnitOfWork<IMeetMeDbScope> meetingsUnitOfWork, long employeeId)
        {
            var mapping = _classMappingFactory.CreateMapping<Meeting, EmployeeTimelineMeetingDto>();

            return meetingsUnitOfWork.Repositories
               .Meetings.Filtered()
               .Include(m => m.Employee)
               .Include(m => m.Notes)
               .Include(m => m.Notes.Select(n => n.Employee))
               .Include(m => m.Attachments)
               .Where(m => m.EmployeeId == employeeId)
               .AsEnumerable()
               .Select(m => mapping.CreateFromSource(m))
               .ToList();
        }

        private IList<EmployeeTimelineFeedbackDto> GetFeedbacks(IUnitOfWork<IMeetMeDbScope> meetingsUnitOfWork, long employeeId)
        {
            var mapping = _classMappingFactory.CreateMapping<Feedback, EmployeeTimelineFeedbackDto>();

            return meetingsUnitOfWork.Repositories
                .Feedbacks.Filtered()
                .Include(m => m.Author)
                .Where(m => m.Receiver.Id == employeeId)
                .AsEnumerable()
                .Select(m => mapping.CreateFromSource(m))
                .ToList();
        }

        private IList<EmployeeTimelineEmploymentPeriodDto> GetEmploymentPeriods(IUnitOfWork<IMeetMeDbScope> meetingsUnitOfWork, long employeeId)
        {
            var mapping = _classMappingFactory.CreateMapping<EmploymentPeriod, EmployeeTimelineEmploymentPeriodDto>();

            var employmentPeriodDtos = meetingsUnitOfWork.Repositories
                .EmploymentPeriods
                .Include(m => m.Employee)
                .Include(m => m.JobTitle)
                .Where(m => m.Employee.Id == employeeId)
                .OrderBy(m => m.StartDate)
                .AsEnumerable()
                .Select(m => mapping.CreateFromSource(m))
                .ToList();

            GenerateEmploymentMessage(employmentPeriodDtos);

            return employmentPeriodDtos;
        }

        private IList<EmployeeTimelineLineManagerChangedDto> GetLineManagerChanges(IUnitOfWork<IMeetMeDbScope> meetingsUnitOfWork, long employeeId)
        {
            var mapping = _classMappingFactory.CreateMapping<EmployeeDataChangeRequest, EmployeeTimelineLineManagerChangedDto>();

            return meetingsUnitOfWork.Repositories
                .EmployeeDataChangeRequests // not filtered as employees can't always see their own data change requests
                .Where(m => m.Employees.Any(e => e.Id == employeeId) && m.IsSetLineManager)
                .AsEnumerable()
                .Select(m => mapping.CreateFromSource(m))
                .ToList();
        }

        private void GenerateEmploymentMessage(IList<EmployeeTimelineEmploymentPeriodDto> employmentPeriodDtos)
        {
            const int thresholdBetweenContracts = 14;

            var firstEmploymentPeriod = employmentPeriodDtos.FirstOrDefault();

            if (firstEmploymentPeriod != null)
            {
                firstEmploymentPeriod.Header = AlertResources.WelcomeMessage;
                firstEmploymentPeriod.Body = AlertResources.WelcomeMessageBody;

                var employmentPeriodCount = employmentPeriodDtos.Count();

                for (int i = 1; i < employmentPeriodCount; i++)
                {
                    var changes = employmentPeriodDtos[i].GetEmploymentPeriodDtoDifference(employmentPeriodDtos[i - 1]);

                    if (changes.Any())
                    {
                        var employmentPeriodBody = new List<string>();

                        if (changes.Contains(EmploymentPeriodDtoDifference.JobTitleName))
                        {
                            employmentPeriodDtos[i].Header = getUpgradeMessage();
                            employmentPeriodBody.Add(String.Format(AlertResources.NewJobTitle, employmentPeriodDtos[i].JobTitleName));
                        }

                        if (changes.Contains(EmploymentPeriodDtoDifference.ContractType))
                        {
                            employmentPeriodDtos[i].Header = getUpgradeMessage();
                            employmentPeriodBody.Add(String.Format(AlertResources.NewContractType, employmentPeriodDtos[i].ContractTypeName));
                        }

                        employmentPeriodDtos[i].Body = string.Join("<br />", employmentPeriodBody);
                    }

                    if (employmentPeriodDtos[i].HappenedOn.AddDays(-thresholdBetweenContracts) > employmentPeriodDtos[i - 1].EndDate)
                    {
                        var goodbyeEmployeeTimelineItem = new EmployeeTimelineEmploymentPeriodDto
                        {
                            Header = AlertResources.GoodbyeMessage,
                            Body = AlertResources.GoodbyeMessageBody,
                            EmployeeId = employmentPeriodDtos[i - 1].EmployeeId,
                            EmployeeFullName = employmentPeriodDtos[i - 1].EmployeeFullName,
                            EndDate = (DateTime)employmentPeriodDtos[i - 1].EndDate,
                            JobTitleName = employmentPeriodDtos[i - 1].JobTitleName,
                            HappenedOn = (DateTime)employmentPeriodDtos[i - 1].EndDate
                        };
                        employmentPeriodDtos.Add(goodbyeEmployeeTimelineItem);

                        employmentPeriodDtos[i].Header = AlertResources.WelcomeBackMessage;
                        employmentPeriodDtos[i].Body = AlertResources.WelcomeBackMessageBody;
                    }

                    if (employmentPeriodDtos[i].EndDate != null && employmentPeriodCount == i + 1)
                    {
                        var goodbyeEmployeeTimelineItem = new EmployeeTimelineEmploymentPeriodDto
                        {
                            Header = AlertResources.GoodbyeMessage,
                            Body = AlertResources.GoodbyeMessageBody,
                            EmployeeId = employmentPeriodDtos[i].EmployeeId,
                            EmployeeFullName = employmentPeriodDtos[i].EmployeeFullName,
                            EndDate = (DateTime)employmentPeriodDtos[i].EndDate,
                            JobTitleName = employmentPeriodDtos[i].JobTitleName,
                            HappenedOn = (DateTime)employmentPeriodDtos[i].EndDate
                        };
                        employmentPeriodDtos.Add(goodbyeEmployeeTimelineItem);
                    }
                }
            }
        }

        private string getUpgradeMessage()
        {
            string[] MessageArray =
            {
                AlertResources.UpgradeMessage1,
                AlertResources.UpgradeMessage2,
                AlertResources.UpgradeMessage3
            };

            int index = _numberOfUpgrades % MessageArray.Count();
            _numberOfUpgrades++;

            return MessageArray[index];
        }

        private IList<EmployeeTimelineProjectAllocationDto> GetAllocationRequests(IUnitOfWork<IMeetMeDbScope> meetingsUnitOfWork, long employeeId)
        {
            var projectAllocations = meetingsUnitOfWork.Repositories
                .AllocationRequests
                .Include(m => m.Employee)
                .Include(m => m.Project)
                .Where(m => m.Employee.Id == employeeId)
                .Where(m => m.AllocationCertainty == AllocationCertainty.Certain)
                .AsEnumerable()
                .Select(m => _classMappingFactory.CreateMapping<AllocationRequest, EmployeeTimelineProjectAllocationDto>().CreateFromSource(m))
                .OrderBy(m => m.HappenedOn)
                .ToList();

            projectAllocations = FilterProjectAllocation(projectAllocations).ToList();

            return projectAllocations;
        }

        private IEnumerable<EmployeeTimelineProjectAllocationDto> FilterProjectAllocation(List<EmployeeTimelineProjectAllocationDto> projectAllocations)
        {
            var projectName = "";

            foreach (var p in projectAllocations)
            {
                if (p.ProjectName != projectName)
                {
                    projectName = p.ProjectName;
                    yield return p;
                }
            }
        }
    }
}