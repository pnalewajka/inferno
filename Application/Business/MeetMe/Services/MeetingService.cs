﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Dictionaries.Dto;
using Smt.Atomic.Business.MeetMe.DocumentMappings;
using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.Business.MeetMe.Interfaces;
using Smt.Atomic.Business.MeetMe.Resources;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Entities.Modules.MeetMe;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.MeetMe.Services
{
    public class MeetingService : IMeetingService
    {
        private readonly IUnitOfWorkService<IMeetMeDbScope> _meetingsUnitOfWorkService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IEmployeeService _employeeService;
        private readonly ITimeService _timeService;
        private readonly IUploadedDocumentHandlingService _uploadedDocumentHandlingService;
        private readonly IAbsenceRequestService _absenceRequestService;

        private readonly IClassMapping<MeetingDto, Meeting> _meetingDtoToMeetingMapping;
        private readonly IClassMapping<MeetingNoteDto, MeetingNote> _meetingNoteDtoTMeetingNoteMapping;
        private readonly IClassMapping<FeedbackDto, Feedback> _meetingFeedbackDtoToMeetingFeedbackMapping;
        private readonly IClassMapping<Meeting, MeetingDto> _meetingToMeetingDtoMapping;


        public MeetingService(IUnitOfWorkService<IMeetMeDbScope> meetingsUnitOfWorkService,
            ISystemParameterService systemParameterService,
            IEmployeeService employeeService,
            ITimeService timeService,
            IUploadedDocumentHandlingService uploadedDocumentHandlingService,
            IAbsenceRequestService absenceRequestService,
            IClassMapping<MeetingDto, Meeting> meetingDtoToMeetingMapping,
            IClassMapping<Meeting, MeetingDto> meetingToMeetingDtoMapping,
            IClassMapping<MeetingNoteDto, MeetingNote> meetingNoteDtoTMeetingNoteMapping,
            IClassMapping<FeedbackDto, Feedback> meetingFeedbackDtoToMeetingFeedbackMapping)
        {
            _absenceRequestService = absenceRequestService;
            _meetingsUnitOfWorkService = meetingsUnitOfWorkService;
            _systemParameterService = systemParameterService;
            _employeeService = employeeService;
            _timeService = timeService;
            _uploadedDocumentHandlingService = uploadedDocumentHandlingService;
            _meetingDtoToMeetingMapping = meetingDtoToMeetingMapping;
            _meetingNoteDtoTMeetingNoteMapping = meetingNoteDtoTMeetingNoteMapping;
            _meetingFeedbackDtoToMeetingFeedbackMapping = meetingFeedbackDtoToMeetingFeedbackMapping;
            _meetingToMeetingDtoMapping = meetingToMeetingDtoMapping;
        }

        public void UpdateMeetingStatus(IEnumerable<long> meetingIds)
        {
            using (var meetingsUnitOfWork = _meetingsUnitOfWorkService.Create())
            {
                var meetings = meetingsUnitOfWork.Repositories
                    .Meetings
                    .AsQueryable()
                    .Include(m => m.Notes)
                    .Where(m => meetingIds.Any(id => m.Id == id))
                    .ToList();

                foreach (var meeting in meetings)
                {
                    UpdateMeetingStatus(meeting);
                    meetingsUnitOfWork.Repositories.Meetings.Edit(meeting);
                }

                meetingsUnitOfWork.Commit();
            }
        }

        public BusinessResult FinishMeeting(long meetingId)
        {
            var result = new BusinessResult();

            using (var meetingsUnitOfWork = _meetingsUnitOfWorkService.Create())
            {
                var meeting = meetingsUnitOfWork.Repositories
                    .Meetings
                    .Filtered()
                    .Include(m => m.Notes)
                    .Where(m => m.Id == meetingId).Single();

                if (!meeting.Notes.Any() || meeting.Notes.Any(n => string.IsNullOrEmpty(n.NoteBody) && !n.Attachments.Any()))
                {
                    result.AddAlert(AlertType.Error, AlertResources.NoteWithContentEmptyAlert);

                    return result;
                }

                if (!meeting.ScheduledMeetingDate.HasValue)
                {
                    result.AddAlert(AlertType.Error, AlertResources.MeetingDateEmptyAlert);

                    return result;
                }

                meeting.Status = MeetingStatus.Done;
                meeting.Notes.ToList().ForEach(n => n.Status = NoteStatus.Done);

                var meetingDto = CreateScheduleMeetingDto(meeting.EmployeeId, GetInterval(meeting.EmployeeId), meeting.ScheduledMeetingDate ?? meeting.MeetingDueDate);

                var newMeeting = _meetingDtoToMeetingMapping.CreateFromSource(meetingDto);

                meetingsUnitOfWork.Repositories
                    .Meetings.Add(newMeeting);

                meetingsUnitOfWork.Commit();
            }

            return result;
        }

        private void UpdateMeetingStatus(Meeting meeting)
        {
            if (meeting.Status == MeetingStatus.Done)
            {
                return;
            }

            if (meeting.ScheduledMeetingDate == null)
            {
                meeting.Status = MeetingStatus.Pending;
            }

            else
            {
                meeting.Status = MeetingStatus.Scheduled;
            }
        }

        public void ScheduleFirstMeeting(long employeeId, DateTime? startDate)
        {
            var interval = _systemParameterService.GetParameter<int>(ParameterKeys.FirstMeetMeWeeksInterval);
            ScheduleMeeting(employeeId, interval, startDate);
        }

        public long GetEmployeeIdByMeetingId(long meetingId)
        {
            using (var meetingsUnitOfWork = _meetingsUnitOfWorkService.Create())
            {
                var meeting = meetingsUnitOfWork.Repositories.Meetings.GetById(meetingId);

                return meeting.EmployeeId;
            }
        }

        public long CreateMeeting(MeetingDto meetingDto)
        {
            using (var meetingsUnitOfWork = _meetingsUnitOfWorkService.Create())
            {
                var entity = _meetingDtoToMeetingMapping.CreateFromSource(meetingDto);
                meetingsUnitOfWork.Repositories.Meetings.Add(entity);
                meetingsUnitOfWork.Commit();
                return entity.Id;
            }
        }

        public MeetingDto GetMeetingById(long meetingId)
        {
            using (var meetingUnitOfWork = _meetingsUnitOfWorkService.Create())
            {
                return _meetingToMeetingDtoMapping.CreateFromSource(meetingUnitOfWork.Repositories.Meetings.GetById(meetingId));
            }
        }

        public NoteStatus GetNoteState(long noteId)
        {
            using (var meetingUnitOfWork = _meetingsUnitOfWorkService.Create())
            {
                return meetingUnitOfWork.Repositories.MeetingNotes.Where(n => n.Id == noteId).Select(n => n.Status).Single();
            }
        }

        private void ScheduleMeeting(long employeeId)
        {
            ScheduleMeeting(employeeId, GetInterval(employeeId));
        }

        private MeetingDto CreateScheduleMeetingDto(long employeeId, int weeksInterval, DateTime? lastMeetingDate = null)
        {
            var employee = _employeeService.GetEmployeeOrDefaultById(employeeId);

            if (employee == null)
            {
                throw new InvalidOperationException($"There is no employee for id: {employeeId}");
            }

            var dueDate = lastMeetingDate.HasValue
                              ? DateHelper.AddWeeks(lastMeetingDate.Value, weeksInterval)
                              : DateHelper.AddWeeks(_timeService.GetCurrentDate(), weeksInterval);

            dueDate = _absenceRequestService.GetProperDateForNextMeeting(employee.UserId.Value, dueDate);

            return new MeetingDto
            {
                Employee = employee,
                DueDate = dueDate,
                Location = new LocationDto() { Id = employee.LocationId.GetValueOrDefault() },
                LineManager = new EmployeeDto { Id = employee.LineManagerId.GetValueOrDefault() },
                Status = MeetingStatus.Pending,
            };
        }

        private void ScheduleMeeting(long employeeId, int weeksInterval, DateTime? lastMeetingDate = null)
        {
            var dto = CreateScheduleMeetingDto(employeeId, weeksInterval, lastMeetingDate);

            CreateMeeting(dto);
        }


        private int GetInterval(long employeeId)
        {
            using (var meetingsUnitOfWork = _meetingsUnitOfWorkService.Create())
            {
                var currentMeetingsCount = meetingsUnitOfWork
                    .Repositories
                    .Meetings
                    .Count(x => x.EmployeeId == employeeId);

                var parameterKey = currentMeetingsCount >= 2
                           ? ParameterKeys.NextMeetMeWeeksInterval
                           : ParameterKeys.SecondMeetMeWeeksInterval;

                return _systemParameterService.GetParameter<int>(parameterKey);
            }
        }
    }
}
