﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.Business.MeetMe.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.Data.Entities.Modules.MeetMe;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.MeetMe.Services
{
    public class MeetingInvitationCardIndexService : CardIndexDataService<MeetingInvitationDto, Meeting, IMeetMeDbScope>, IMeetingInvitationCardIndexService
    {
        private readonly IMeetingService _meetingService;
        private readonly ICalendarInvitationService _invitationService;
        private readonly IMessageTemplateService _messageTemplateService;

        public MeetingInvitationCardIndexService(
            ICardIndexServiceDependencies<IMeetMeDbScope> dependencies,
            IUploadedDocumentHandlingService uploadedDocumentHandlingService1,
            IMeetingService meetingService,
            ICalendarInvitationService invitationService,
            IMessageTemplateService messageTemplateService)
            : base(dependencies)
        {
            _meetingService = meetingService;
            _invitationService = invitationService;
            _messageTemplateService = messageTemplateService;
        }

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public void SendInvitation(MeetingInvitationDto meetingInvitationDto)
        {
            var dto = GetRecordById(meetingInvitationDto.Id);

            meetingInvitationDto.EmployeeFirstName = dto.EmployeeFirstName;

            var subject = _messageTemplateService.ResolveTemplate(TemplateCodes.MeetMeSendMeetingInvitationSubject, null);
            var body = _messageTemplateService.ResolveTemplate(TemplateCodes.MeetMeSendMeetingInvitationBody, meetingInvitationDto);

            var invitationMessageDto = new InvitationMessageDto
            {
                ParticipantEmails = new List<EmailRecipientDto> {
                    new EmailRecipientDto{ EmailAddress = string.IsNullOrEmpty(dto.EmployeeEmail) ? string.Empty : dto.EmployeeEmail,
                        Type = Common.Enums.RecipientType.To},
                    new EmailRecipientDto{ EmailAddress = string.IsNullOrEmpty(dto.LineManagerEmail) ? string.Empty : dto.LineManagerEmail,
                        Type = Common.Enums.RecipientType.Cc},
                },
                Subject = subject.Content,
                Body = body.Content,
                InvitationDateTime = meetingInvitationDto.MeetingDate.GetValueOrDefault()
            };

            _invitationService.SendInvitation(invitationMessageDto);
        }
    }
}