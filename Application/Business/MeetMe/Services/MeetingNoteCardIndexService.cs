﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.MeetMe.DocumentMappings;
using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.Business.MeetMe.Interfaces;
using Smt.Atomic.Business.MeetMe.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.DynamicQuerable;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.MeetMe;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.MeetMe.Services
{
    public class MeetingNoteCardIndexService : CardIndexDataService<MeetingNoteDto, MeetingNote, IMeetMeDbScope>, IMeetingNoteCardIndexService
    {
        private readonly IUploadedDocumentHandlingService _uploadedDocumentHandlingService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IMeetingService _meetingService;
        private readonly ITimeService _timeService;
        private readonly ISystemParameterService _systemParameterService;
        private readonly IMeetingNoteService _meetingNoteService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public MeetingNoteCardIndexService(
            ICardIndexServiceDependencies<IMeetMeDbScope> dependencies,
            IUploadedDocumentHandlingService uploadedDocumentHandlingService,
            IPrincipalProvider principleProvider,
            IMeetingService meetingService,
            ITimeService timeService,
            ISystemParameterService systemParameterService,
            IMeetingNoteService meetingNoteService)
            : base(dependencies)
        {
            _uploadedDocumentHandlingService = uploadedDocumentHandlingService;
            _meetingService = meetingService;
            _principalProvider = principleProvider;
            _timeService = timeService;
            _systemParameterService = systemParameterService;
            _meetingNoteService = meetingNoteService;
        }

        protected override void ApplySortingCriterion(OrderedQueryBuilder<MeetingNote> orderedQueryBuilder, SortingCriterion sortingCriterion)
        {
            var direction = sortingCriterion.GetSortingDirection();

            switch (sortingCriterion.GetSortingColumnName())
            {
                case nameof(MeetingNoteDto.AuthorName):
                    orderedQueryBuilder.ApplySortingKey(m => m.Employee.FirstName, direction);
                    orderedQueryBuilder.ApplySortingKey(m => m.Employee.LastName, direction);
                    break;
                default:
                    base.ApplySortingCriterion(orderedQueryBuilder, sortingCriterion);
                    break;
            }
        }

        public override void SetContext(MeetingNote entity, object context)
        {
            var parentIdContext = (ParentIdContext)context;
            entity.MeetingId = parentIdContext.ParentId;
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<MeetingNote, MeetingNoteDto, IMeetMeDbScope> eventArgs)
        {
            if (!_meetingNoteService.CanAddNote(eventArgs.InputEntity.MeetingId))
            {
                throw new BusinessException(AlertResources.CanNotAddEditNote);
            }

            if (string.IsNullOrEmpty(eventArgs.InputDto.NoteBody) && (eventArgs.InputDto.Attachments == null || !eventArgs.InputDto.Attachments.Any()))
            {
                throw new BusinessException(AlertResources.NoteWithContentEmptyAlert);
            }

            base.OnRecordAdding(eventArgs);

            eventArgs.InputEntity.CreatedOn = TimeService.GetCurrentDate();
            eventArgs.InputEntity.AuthorId = _principalProvider.Current.EmployeeId;
            _uploadedDocumentHandlingService.MergeDocumentCollections(eventArgs.UnitOfWork,
                eventArgs.InputEntity.Attachments,
                eventArgs.InputDto.Attachments,
                p => new MeetingNoteAttachment
                {
                    Name = p.DocumentName,
                    ContentType = p.ContentType
                });
            eventArgs.InputEntity.Status = NoteStatus.Draft;
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<MeetingNote, MeetingNoteDto, IMeetMeDbScope> eventArgs)
        {
            if (!_meetingNoteService.CanEditNote(eventArgs.DbEntity.Id))
            {
                throw new BusinessException(AlertResources.CanNotAddEditNote);
            }

            base.OnRecordEditing(eventArgs);

            eventArgs.InputEntity.ModifiedOn = TimeService.GetCurrentDate();
            eventArgs.InputEntity.AuthorId = eventArgs.DbEntity.AuthorId;
            eventArgs.InputEntity.ModifiedById = _principalProvider.Current.EmployeeId;
            _uploadedDocumentHandlingService.MergeDocumentCollections(eventArgs.UnitOfWork,
                eventArgs.DbEntity.Attachments,
                eventArgs.InputDto.Attachments,
                p => new MeetingNoteAttachment
                {
                    Name = p.DocumentName,
                    ContentType = p.ContentType
                });

            eventArgs.InputEntity.Status = NoteStatus.Draft;
        }

        protected override void OnRecordAdded(RecordAddedEventArgs<MeetingNote, MeetingNoteDto> recordAddedEventArgs)
        {
            _uploadedDocumentHandlingService.PromoteTemporaryDocuments<MeetingNoteAttachment, MeetingNoteAttachmentContent, MeetingNoteAttachmentMapping, IMeetMeDbScope>(recordAddedEventArgs.InputDto.Attachments);
        }

        protected override void OnRecordEdited(RecordEditedEventArgs<MeetingNote, MeetingNoteDto> recordAddedEventArgs)
        {
            _uploadedDocumentHandlingService.PromoteTemporaryDocuments<MeetingNoteAttachment, MeetingNoteAttachmentContent, MeetingNoteAttachmentMapping, IMeetMeDbScope>(recordAddedEventArgs.InputDto.Attachments);
        }

        protected override void OnRecordsChanged(RecordChangedEventArgs<MeetingNote> eventArgs)
        {
            var meetingIds = eventArgs.ChangedEntities.Select(x => x.MeetingId);
            _meetingService.UpdateMeetingStatus(meetingIds);
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<MeetingNote, IMeetMeDbScope> eventArgs)
        {
            base.OnRecordDeleting(eventArgs);
            eventArgs.UnitOfWork.Repositories.MeetingNoteAttachments.DeleteRange(eventArgs.DeletingRecord.Attachments);
        }

        public override IQueryable<MeetingNote> ApplyContextFiltering(IQueryable<MeetingNote> records, object context)
        {
            var parentIdContext = context as ParentIdContext;

            return parentIdContext != null
                ? records.Where(r => r.MeetingId == parentIdContext.ParentId)
                : records;
        }
    }
}