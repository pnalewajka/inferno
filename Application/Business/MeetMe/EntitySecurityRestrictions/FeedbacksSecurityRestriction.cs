﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.MeetMe;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.MeetMe.EntitySecurityRestrictions
{
    public class FeedbacksSecurityRestriction : EntitySecurityRestriction<Feedback>
    {
        private readonly IOrgUnitService _orgUnitService;
        public FeedbacksSecurityRestriction(IPrincipalProvider principalProvider,
            IOrgUnitService orgUnitService)
            : base(principalProvider)
        {
            RequiresAuthentication = true;
            _orgUnitService = orgUnitService;
        }

        public override Expression<Func<Feedback, bool>> GetRestriction()
        {
            if (Roles.Contains(SecurityRoleType.CanViewAllEmployeeMeetings))
            {
                return AllRecords();
            }

            var employeeId = CurrentPrincipal.EmployeeId;

            if (Roles.Contains(SecurityRoleType.CanViewMyEmployeeMeetings))
            {
                var managerOrgUnitsIds = _orgUnitService.GetManagerOrgUnitDescendantIds(employeeId);
                var isMyEmployee = new BusinessLogic<Feedback, bool>(
                    f => f.ReceiverId == employeeId || EmployeeBusinessLogic.IsEmployeeOf.Call(f.Receiver, employeeId, managerOrgUnitsIds));

                return isMyEmployee;
            }

            return m => m.ReceiverId == employeeId;
        }
    }
}
