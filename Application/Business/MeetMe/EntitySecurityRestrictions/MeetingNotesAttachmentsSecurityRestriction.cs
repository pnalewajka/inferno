﻿using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.MeetMe;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.MeetMe.EntitySecurityRestrictions
{
    public class MeetingNotesAttachmentsSecurityRestriction : RelatedEntitySecurityRestriction<MeetingNoteAttachment, Meeting>
    {
        public MeetingNotesAttachmentsSecurityRestriction(IPrincipalProvider principalProvider)
            : base(principalProvider)
        {
        }

        protected override BusinessLogic<MeetingNoteAttachment, Meeting> RelatedEntitySelector =>
            new BusinessLogic<MeetingNoteAttachment, Meeting>(meetingNoteAttachment => meetingNoteAttachment.MeetingNote.Meeting);
    }
}
