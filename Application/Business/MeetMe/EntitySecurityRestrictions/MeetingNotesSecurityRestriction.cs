﻿using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.MeetMe;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.MeetMe.EntitySecurityRestrictions
{
    public class MeetingNotesSecurityRestriction : RelatedEntitySecurityRestriction<MeetingNote, Meeting>
    {
        public MeetingNotesSecurityRestriction(IPrincipalProvider principalProvider)
            : base(principalProvider)
        {
        }

        protected override BusinessLogic<MeetingNote, Meeting> RelatedEntitySelector =>
            new BusinessLogic<MeetingNote, Meeting>(meetingNote => meetingNote.Meeting);
    }
}
