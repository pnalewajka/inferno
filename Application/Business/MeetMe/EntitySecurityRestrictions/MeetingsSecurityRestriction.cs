﻿using System;
using System.Linq.Expressions;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.MeetMe;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.MeetMe.EntitySecurityRestrictions
{
    public class MeetingsSecurityRestriction : EntitySecurityRestriction<Meeting>
    {
        private readonly IOrgUnitService _orgUnitService;
        public MeetingsSecurityRestriction(IPrincipalProvider principalProvider,
            IOrgUnitService orgUnitService)
            : base(principalProvider)
        {
            RequiresAuthentication = true;
            _orgUnitService = orgUnitService;
        }

        public override Expression<Func<Meeting, bool>> GetRestriction()
        {
            if (Roles.Contains(SecurityRoleType.CanViewAllEmployeeMeetings))
            {
                return AllRecords();
            }

            var employeeId = CurrentPrincipal.EmployeeId;

            if (Roles.Contains(SecurityRoleType.CanViewMyEmployeeMeetings))
            {
                var managerOrgUnitsIds = _orgUnitService.GetManagerOrgUnitDescendantIds(employeeId);
                var isMyEmployee = new BusinessLogic<Meeting, bool>(
                    m => m.EmployeeId == employeeId || EmployeeBusinessLogic.IsEmployeeOf.Call(m.Employee, employeeId, managerOrgUnitsIds));

                return isMyEmployee;
            }

            return m => m.EmployeeId == employeeId;
        }
    }
}
