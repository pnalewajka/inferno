﻿using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.MeetMe;
using Smt.Atomic.Data.Repositories.FilteringQueries;

namespace Smt.Atomic.Business.MeetMe.EntitySecurityRestrictions
{
    public class MeetingAttachmentsSecurityRestriction : RelatedEntitySecurityRestriction<MeetingAttachment, Meeting>
    {
        public MeetingAttachmentsSecurityRestriction(IPrincipalProvider principalProvider)
            : base(principalProvider)
        {
        }

        protected override BusinessLogic<MeetingAttachment, Meeting> RelatedEntitySelector =>
            new BusinessLogic<MeetingAttachment, Meeting>(meetingAttachment => meetingAttachment.Meeting);
    }
}
