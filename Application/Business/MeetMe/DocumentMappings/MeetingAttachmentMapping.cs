﻿using Smt.Atomic.Business.Common.Abstracts;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.Data.Entities.Modules.MeetMe;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.MeetMe.DocumentMappings
{
    [Identifier("Documents.MeetingAttachmentMapping")]
    public class MeetingAttachmentMapping : DocumentMapping<MeetingAttachment, MeetingAttachmentContent, IMeetMeDbScope>
    {
        public MeetingAttachmentMapping(IUnitOfWorkService<IMeetMeDbScope> unitOfWorkService)
        : base(unitOfWorkService)
        {
            ContentColumnExpression = o => o.Content;
            NameColumnExpression = o => o.Name;
            ContentTypeColumnExpression = o => o.ContentType;
            ContentLengthColumnExpression = o => o.ContentLength;
        }
    }
}
