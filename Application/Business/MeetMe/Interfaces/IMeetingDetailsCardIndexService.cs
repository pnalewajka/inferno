﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.MeetMe.Interfaces
{
    public interface IMeetingDetailsCardIndexService : ICardIndexDataService<MeetingDetailsDto>
    {
        bool CanEditMeeting(long meetingId);
        bool CanEditMeeting(MeetingStatus meetingStatus);
        BusinessResult FinishMeeting(long meetingId);
    }
}