﻿using Smt.Atomic.Business.Workflows.Dto;

namespace Smt.Atomic.Business.MeetMe.Interfaces
{
    public interface IFeedbackEmailNotificationService
    {
        void NotifyAboutFeedback(FeedbackRequestDto feedbackRequestDto, RequestDto requestDto);
    }
}
