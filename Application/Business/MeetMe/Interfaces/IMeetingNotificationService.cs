﻿using System;
using System.Threading;

namespace Smt.Atomic.Business.MeetMe.Interfaces
{
    public interface IMeetingNotificationService
    {
        void SendNotificationForOvedueMeetings(CancellationToken cancellationToken);
    }
}
