﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.MeetMe.Dto;

namespace Smt.Atomic.Business.MeetMe.Interfaces
{
    public interface IFeedbackCardIndexService : ICardIndexDataService<FeedbackDto>
    {

    }
}