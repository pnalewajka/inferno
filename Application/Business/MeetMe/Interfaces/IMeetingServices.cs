﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.MeetMe.Interfaces
{
    public interface IMeetingService
    {
        void UpdateMeetingStatus(IEnumerable<long> meetingIds);

        BusinessResult FinishMeeting(long meetingId);

        void ScheduleFirstMeeting(long employeeId, DateTime? startDate);

        long CreateMeeting(MeetingDto meetingDto);
        
        long GetEmployeeIdByMeetingId(long meetingId);

        NoteStatus GetNoteState(long noteId);

        MeetingDto GetMeetingById(long meetingId);
    }
}