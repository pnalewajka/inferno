﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.MeetMe.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.MeetMe.Interfaces
{
    public interface IMeetingInvitationCardIndexService : ICardIndexDataService<MeetingInvitationDto>
    {
        void SendInvitation(MeetingInvitationDto meetingInvitationDto);
    }
}