﻿using Smt.Atomic.Data.Entities.Modules.MeetMe;

namespace Smt.Atomic.Business.MeetMe.Interfaces
{
    public interface IMeetingNoteService
    {
        bool CanEditNote(long meetingNoteId);

        bool CanAddNote(long meetingId);
    }
}
