﻿using System.Collections.Generic;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.MeetMe.Dto;

namespace Smt.Atomic.Business.MeetMe.Interfaces
{
    public interface IEmployeeReviewProcessOverview
    {
        EmployeeReviewProcessOverviewDto GetEmployeeReviewProcessOverview(long employeeId);

        BoolResult CheckEmployeeOverviewRestrictions(long requestedEmployeeId);
    }
}