﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.MeetMe.Interfaces;
using Smt.Atomic.Business.MeetMe.Resources;
using Smt.Atomic.Business.Workflows.Dto;
using Smt.Atomic.Business.Workflows.Exceptions;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.Workflows.Resources;
using Smt.Atomic.Business.Workflows.Services;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.MeetMe;
using Smt.Atomic.Data.Entities.Modules.Workflows;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Presentation.Common.Interfaces;

namespace Smt.Atomic.Business.MeetMe.Workflows.Services
{
    internal class FeedbackRequestService : RequestServiceBase<FeedbackRequest, FeedbackRequestDto>
    {
        private readonly IUserService _userService;
        private readonly IEmployeeService _employeeService;
        private readonly IUnitOfWorkService<IMeetMeDbScope> _unitOfWorkService;
        private readonly IApprovingPersonService _approvingPersonService;
        private readonly IRequestCardIndexDataService _requestCardIndexDataService;
        private readonly IAlertService _alertService;
        private readonly IFeedbackEmailNotificationService _feedbackEmailNotificationService;
        private readonly ISystemParameterService _systemParameterService;

        public FeedbackRequestService(
            IUserService userService,
            IEmployeeService employeeService,
            IWorkflowNotificationService workflowNotificationService,
            IPrincipalProvider principalProvider,
            IUnitOfWorkService<IMeetMeDbScope> unitOfWorkService,
            IActionSetService actionSetService,
            IClassMappingFactory classMappingFactory,
            IApprovingPersonService approvingPersonService,
            IRequestCardIndexDataService requestCardIndexDataService,
            IAlertService alertService,
            IFeedbackEmailNotificationService feedbackEmailNotificationService,
            ISystemParameterService systemParameterService,
            IRequestWorkflowService requestWorkflowService)
            : base(actionSetService, principalProvider, classMappingFactory, workflowNotificationService, requestWorkflowService, systemParameterService)
        {
            _userService = userService;
            _employeeService = employeeService;
            _unitOfWorkService = unitOfWorkService;
            _approvingPersonService = approvingPersonService;
            _requestCardIndexDataService = requestCardIndexDataService;
            _alertService = alertService;
            _feedbackEmailNotificationService = feedbackEmailNotificationService;
            _systemParameterService = systemParameterService;
        }

        public override FeedbackRequestDto GetDraft(IDictionary<string, object> parameters)
        {
            return new FeedbackRequestDto
            {
                OriginScenario = FeedbackOriginScenario.Requested
            };
        }

        public override RequestType RequestType => RequestType.FeedbackRequest;

        public override string RequestName => WorkflowResources.FeedbackRequest;

        public override RequestTypeGroup RequestTypeGroup => RequestTypeGroup.Other;

        public override SecurityRoleType? RequiredRoleType => SecurityRoleType.CanRequestFeedback;

        public override bool CanBeDeleted(RequestDto requestDto, long byUserId)
        {
            return _userService.GetUserRolesById(byUserId).Contains(SecurityRoleType.CanDeleteFeedbackRequest);
        }

        public override bool CanBeEdited(RequestDto requestDto, long byUserId)
        {
            if (requestDto.Status == RequestStatus.Draft || requestDto.Status == RequestStatus.Pending)
            {
                var employeeId = _employeeService.GetEmployeeIdByUserId(byUserId);

                if (!employeeId.HasValue)
                {
                    return false;
                }

                var feedbackRequestDto = (FeedbackRequestDto)requestDto.RequestObject;
                var editorsEmployeeIds = new List<long> { feedbackRequestDto.EvaluatorEmployeeId };

                var evaluatedEmployeeManager = _approvingPersonService.GetEmployeeMainManager(feedbackRequestDto.EvaluatedEmployeeId);

                if (evaluatedEmployeeManager.HasValue &&
                    (evaluatedEmployeeManager.Value == feedbackRequestDto.EvaluatorEmployeeId ||
                    feedbackRequestDto.EvaluatorEmployeeIds.Contains(evaluatedEmployeeManager.Value)))
                {
                    evaluatedEmployeeManager = _approvingPersonService.GetEmployeeMainManager(evaluatedEmployeeManager.Value);
                }

                if (evaluatedEmployeeManager.HasValue)
                {
                    editorsEmployeeIds.Add(evaluatedEmployeeManager.GetValueOrDefault());
                }

                return editorsEmployeeIds.Contains(employeeId.Value);
            }

            return false;
        }

        public override IQueryable<Request> ConfigureIncludes(IQueryable<Request> repository)
        {
            return repository.Include(r => r.FeedbackRequest);
        }

        public override void Execute(RequestDto request, FeedbackRequestDto requestDto)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var EvaluatedEmployee = unitOfWork.Repositories.Employees.FirstOrDefault(x => x.Id == requestDto.EvaluatedEmployeeId);
                var EvaluatorEmployee = unitOfWork.Repositories.Employees.FirstOrDefault(x => x.Id == requestDto.EvaluatorEmployeeId);

                if (EvaluatedEmployee != null && EvaluatorEmployee != null)
                {
                    unitOfWork.Repositories.Feedbacks.Add(new Feedback()
                    {
                        AuthorId = requestDto.EvaluatorEmployeeId,
                        FeedbackContent = requestDto.FeedbackContent,
                        Author = EvaluatorEmployee,
                        Receiver = EvaluatedEmployee,
                        Visibility = requestDto.Visibility
                    });

                    unitOfWork.Commit();
                }
            }
        }

        public override string GetRequestDescription(RequestDto request, FeedbackRequestDto data)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var employee = unitOfWork.Repositories.Employees.FirstOrDefault(x => x.Id == data.EvaluatedEmployeeId);

                if (employee != null)
                {
                    return string.Format(AlertResources.RequestFeedbackDescription, employee.FirstName, employee.LastName);
                }
            }

            return RequestName;
        }

        public override IEnumerable<ApproverGroupDto> GetApproverGroups(RequestDto request, FeedbackRequestDto requestDto)
        {
            if (requestDto.EvaluatorEmployeeId != default(long) && requestDto.OriginScenario == FeedbackOriginScenario.Requested)
            {
                yield return new ApproverGroupDto(ApprovalGroupMode.Or, new[] { requestDto.EvaluatorEmployeeId });
            }

            var evaluatedEmployeeManager = _approvingPersonService.GetEmployeeMainManager(requestDto.EvaluatedEmployeeId);

            if (evaluatedEmployeeManager.HasValue &&
                (evaluatedEmployeeManager.Value == requestDto.EvaluatorEmployeeId ||
                requestDto.EvaluatorEmployeeIds.Contains(evaluatedEmployeeManager.Value))
               )
            {
                evaluatedEmployeeManager = _approvingPersonService.GetEmployeeMainManager(evaluatedEmployeeManager.Value);
            }

            if (evaluatedEmployeeManager.HasValue)
            {
                yield return new ApproverGroupDto(ApprovalGroupMode.Or, new[] { evaluatedEmployeeManager.Value });
            }

            yield break;
        }

        public override BoolResult OnApproving(Request request, RequestDto requestDto, IList<ApprovalDto> affectedApprovals)
        {
            var feedbackRequestDto = (FeedbackRequestDto)requestDto.RequestObject;

            if (string.IsNullOrEmpty(feedbackRequestDto.FeedbackContent) || feedbackRequestDto.Visibility == default(FeedbackVisbility))
            {
                throw new RequestMissingDataForStatusChangeBusinessException(AlertResources.FillRequiredFieldAlert);
            }

            return base.OnApproving(request, requestDto, affectedApprovals);
        }

        public override void OnEditing(RecordEditingEventArgs<Request, RequestDto, IWorkflowsDbScope> eventArgs)
        {
            base.OnEditing(eventArgs);

            eventArgs.InputDto.Status = eventArgs.DbEntity.Status;
            eventArgs.InputEntity.Status = eventArgs.DbEntity.Status;
        }

        public override IEnumerable<AlertDto> Validate(RequestDto request, FeedbackRequestDto requestDto)
        {
            if (request.Status == RequestStatus.Pending || request.Status == RequestStatus.Approved)
            {
                if (string.IsNullOrEmpty(requestDto.FeedbackContent))
                {
                    yield return new AlertDto { Message = AlertResources.FeedbackEmptyAlert, Type = AlertType.Error };
                }
            }

            if (requestDto.OriginScenario == FeedbackOriginScenario.Requested)
            {
                if (string.IsNullOrEmpty(requestDto.Comment))
                {
                    yield return new AlertDto { Message = AlertResources.FeedbackCommentEmptyAlert, Type = AlertType.Error };
                }

                if (requestDto.EvaluatedEmployeeId == default(long) && requestDto.EvaluatorEmployeeIds.IsNullOrEmpty())
                {
                    yield return new AlertDto { Message = AlertResources.FeedbackEmptyAlert, Type = AlertType.Error };
                }
            }
        }


        public override void OnSubmited(Request request, RequestDto requestDto)
        {
            var feedbackRequestDto = (FeedbackRequestDto)requestDto.RequestObject;

            if (feedbackRequestDto.OriginScenario == FeedbackOriginScenario.Requested &&
                feedbackRequestDto.EvaluatorEmployeeIds != null &&
                feedbackRequestDto.EvaluatorEmployeeIds.Any())
            {
                var additionalRequests = new List<FeedbackRequestDto>();

                additionalRequests.AddRange(feedbackRequestDto.EvaluatorEmployeeIds.Where(id => feedbackRequestDto.EvaluatorEmployeeId != id).Select(t => new FeedbackRequestDto
                {
                    Comment = feedbackRequestDto.Comment,
                    EvaluatedEmployeeId = feedbackRequestDto.EvaluatedEmployeeId,
                    EvaluatorEmployeeId = t,
                    OriginScenario = FeedbackOriginScenario.Requested
                }));

                var requestResults = _requestCardIndexDataService.CreateRequestsExternally(RequestType.FeedbackRequest, additionalRequests);
                requestResults.Alerts.ToList().ForEach(a =>
                {
                    _alertService.AddSuccess(a.Message, a.IsDismissable);
                });
            }

            feedbackRequestDto.RequestURL = $"{_systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress)}Workflows/Request/Edit/{requestDto.Id}";
            _feedbackEmailNotificationService.NotifyAboutFeedback(feedbackRequestDto, requestDto);
        }
    }
}
