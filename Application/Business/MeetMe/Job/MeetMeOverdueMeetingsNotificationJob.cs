﻿using System;
using Castle.Core.Logging;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.Business.MeetMe.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace Smt.Atomic.Business.MeetMe.Job
{
    [Identifier("Job.MeetMe.MeetMeOverdueMeetingsNotificationJob")]
    [JobDefinition(
        Code = "MeetMeOverdueMeetingsNotificationJob",
        Description = "Job to send notification to line managers when meeting goes overdue",
        MaxExecutioPeriodInSeconds = 500000,
        ScheduleSettings = "0 0 3 * * MON",
        PipelineName = PipelineCodes.Notifications)]
    public class MeetMeOverdueMeetingsNotificationJob : IJob
    {
        private readonly IMeetingNotificationService _meetingNotificationService;
        private readonly ILogger _logger;

        public MeetMeOverdueMeetingsNotificationJob(IMeetingNotificationService meetingNotificationService,
            ILogger logger)
        {
            _meetingNotificationService = meetingNotificationService;
            _logger = logger;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            try
            {
                _meetingNotificationService.SendNotificationForOvedueMeetings(executionContext.CancellationToken);

                return JobResult.Success();
            }
            catch (Exception ex)
            {
                _logger.Error($"{nameof(MeetMeOverdueMeetingsNotificationJob)} failed", ex);

                return JobResult.Fail();
            }
        }
    }
}
