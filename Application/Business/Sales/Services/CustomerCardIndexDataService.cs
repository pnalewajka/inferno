﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Sales.Dto;
using Smt.Atomic.Business.Sales.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Sales;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Sales.Services
{
    public class CustomerCardIndexDataService : CardIndexDataService<CustomerDto, Customer, ISalesDbScope>, ICustomerCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public CustomerCardIndexDataService(ICardIndexServiceDependencies<ISalesDbScope> dependencies)
            : base(dependencies)
        {
        }

        protected override IEnumerable<Expression<Func<Customer, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return c => c.LegalName;
            yield return c => c.ShortName;
        }

        protected override IOrderedQueryable<Customer> GetDefaultOrderBy(IQueryable<Customer> records, QueryCriteria criteria)
        {
            return records.OrderByDescending(c => c.Priority).ThenBy(c => c.LegalName);
        }
    }
}
