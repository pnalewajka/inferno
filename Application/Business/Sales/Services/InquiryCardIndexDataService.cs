﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Sales.Consts;
using Smt.Atomic.Business.Sales.Dto;
using Smt.Atomic.Business.Sales.Interfaces;
using Smt.Atomic.Business.Sales.Resources;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.Sales;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Sales.Services
{
    public class InquiryCardIndexDataService : CardIndexDataService<InquiryDto, Inquiry, ISalesDbScope>, IInquiryCardIndexDataService
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly IUnitOfWorkService<ISalesDbScope> _unitOfWorkService;

        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public InquiryCardIndexDataService(ICardIndexServiceDependencies<ISalesDbScope> dependencies)
            : base(dependencies)
        {
            _principalProvider = dependencies.PrincipalProvider;
            _unitOfWorkService = dependencies.UnitOfWorkService;
        }

        public override InquiryDto GetDefaultNewRecord()
        {
            var employeeId = _principalProvider.Current.EmployeeId;
            var orgUnitId = GetEmployeeOrgUnitId(employeeId);

            return new InquiryDto
            {
                SalesPersonId = employeeId,
                BusinessLineOrgUnitId = orgUnitId,
            };
        }

        protected override void OnRecordAdding(RecordAddingEventArgs<Inquiry, InquiryDto, ISalesDbScope> eventArgs)
        {
            eventArgs.InputEntity.Watchers =
                EntityMergingService.CreateEntitiesFromIds<Employee, ISalesDbScope>(eventArgs.UnitOfWork,
                    eventArgs.InputDto.WatcherIds.ToArray());

            base.OnRecordAdding(eventArgs);
        }

        protected override void OnRecordEditing(RecordEditingEventArgs<Inquiry, InquiryDto, ISalesDbScope> eventArgs)
        {
            var watcherIds = eventArgs.DbEntity.Watchers.GetIds();
            eventArgs.InputDto.WatcherIds = eventArgs.InputDto.WatcherIds.Union(watcherIds).ToArray();

            eventArgs.InputEntity.Watchers =
                EntityMergingService.CreateEntitiesFromIds<Employee, ISalesDbScope>(eventArgs.UnitOfWork,
                    eventArgs.InputDto.WatcherIds.ToArray());

            EntityMergingService.MergeCollections(eventArgs.UnitOfWork, eventArgs.DbEntity.Watchers, eventArgs.InputEntity.Watchers);

            base.OnRecordEditing(eventArgs);
        }

        protected override void OnRecordDeleting(RecordDeletingEventArgs<Inquiry, ISalesDbScope> eventArgs)
        {
            eventArgs.Canceled = eventArgs.DeletingRecord.ProcessStatus != InquiryStatus.Lead;

            if (eventArgs.Canceled)
            {
                eventArgs.AddError(InquiryResources.CantDeleteInquiryMessage);
            }

            base.OnRecordDeleting(eventArgs);
        }

        protected override IQueryable<Inquiry> ConfigureIncludes(IQueryable<Inquiry> sourceQueryable)
        {
            return base.ConfigureIncludes(sourceQueryable)
                .Include(a => a.SalesPerson)
                .Include(a => a.OfferType)
                .Include(a => a.BusinessLineOrgUnit)
                .Include(a => a.ExpectedRevenueCurrency)
                .Include(a => a.Customer)
                .Include(a => a.Watchers);
        }

        private long GetEmployeeOrgUnitId(long employeeId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                return unitOfWork.Repositories.Employees.SelectById(employeeId, e => e.OrgUnitId);
            }
        }

        protected override IEnumerable<Expression<Func<Inquiry, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return a => a.DisplayName;

            if (searchCriteria.Codes.Contains(SalesSearchAreaCodes.SalesPerson))
            {
                yield return a => a.SalesPerson.FirstName;
                yield return a => a.SalesPerson.LastName;
                yield return a => a.SalesPerson.Acronym;
            }

            if (searchCriteria.Codes.Contains(SalesSearchAreaCodes.Customer))
            {
                yield return a => a.Customer.LegalName;
                yield return a => a.Customer.ShortName;
            }

            if (searchCriteria.Codes.Contains(SalesSearchAreaCodes.Customer))
            {
                yield return a => a.Description;
            }
        }

        protected override NamedFilters<Inquiry> NamedFilters
        {
            get
            {
                return new NamedFilters<Inquiry>(
                    new[] {
                        new NamedFilter<Inquiry, long[], long[], long[]>(
                            SalesFilterCodes.Sales,
                            (i, orgUnitIds, salesPersonIds, offerTypeIds) =>
                            (!orgUnitIds.Any() || orgUnitIds.Any(id => i.BusinessLineOrgUnit.Path.Contains("/" + id + "/"))) &&
                            (!salesPersonIds.Any() || salesPersonIds.Contains(i.SalesPersonId)) &&
                            (!offerTypeIds.Any() || offerTypeIds.Contains(i.OfferTypeId)))
                        }.Union(CardIndexServiceHelper.BuildEnumBasedFilters<Inquiry, ImportanceType>(e => e.ProjectImportance))
                    );
            }
        }
    }
}
