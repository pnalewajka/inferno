﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Sales.Dto;
using Smt.Atomic.Business.Sales.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Sales;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Sales.Services
{
    public class OfferTypeCardIndexDataService : CardIndexDataService<OfferTypeDto, OfferType, ISalesDbScope>, IOfferTypeCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public OfferTypeCardIndexDataService(ICardIndexServiceDependencies<ISalesDbScope> dependencies)
            : base(dependencies)
        {
        }
    }
}
