﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Sales.Interfaces;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Sales.Services
{
    internal class InquiryService : IInquiryService
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly IUnitOfWorkService<ISalesDbScope> _unitOfWorkService;
        private readonly IClassMappingFactory _classMappingFactory;

        public InquiryService(IPrincipalProvider principalProvider, IUnitOfWorkService<ISalesDbScope> unitOfWorkService, IClassMappingFactory classMappingFactory)
        {
            _principalProvider = principalProvider;
            _unitOfWorkService = unitOfWorkService;
            _classMappingFactory = classMappingFactory;
        }

        public BoolResult ChangeStatus(long inquiryId, InquiryStatus newStatus)
        {
            using(var unitOfWork = _unitOfWorkService.Create())
            {
                var inquiry = unitOfWork.Repositories.Inquiries.GetById(inquiryId);

                inquiry.ProcessStatus = newStatus;

                unitOfWork.Commit();

                return BoolResult.Success;
            }
        }
    }
}
