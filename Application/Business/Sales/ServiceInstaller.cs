﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.Sales.Interfaces;
using Smt.Atomic.Business.Sales.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.Sales
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            if (containerType == ContainerType.WebApp)
            {
                container.Register(Component.For<ICustomerCardIndexDataService>().ImplementedBy<CustomerCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IInquiryCardIndexDataService>().ImplementedBy<InquiryCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IOfferTypeCardIndexDataService>().ImplementedBy<OfferTypeCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IInquiryService>().ImplementedBy<InquiryService>().LifestyleTransient());
            }
        }
    }
}
