﻿namespace Smt.Atomic.Business.Sales.Consts
{
    public class SalesSearchAreaCodes
    {
        public const string SalesPerson = "sales-person";
        public const string Customer = "customer";
        public const string Description = "description";
    }
}
