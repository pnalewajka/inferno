﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Sales;

namespace Smt.Atomic.Business.Sales.Dto
{
    public class InquiryDtoToInquiryMapping : ClassMapping<InquiryDto, Inquiry>
    {
        public InquiryDtoToInquiryMapping()
        {
            Mapping = d => new Inquiry
            {
                Id = d.Id,
                CustomerId = d.CustomerId,
                DisplayName = d.DisplayName,
                ProcessStatus = d.ProcessStatus,
                BusinessLineOrgUnitId = d.BusinessLineOrgUnitId,
                Description = d.Description,
                SalesPersonId = d.SalesPersonId,
                ProjectPropability = d.ProjectPropability,
                ProjectImportance = d.ProjectImportance,
                ExpectedRevenue = d.ExpectedRevenue,
                ExpectedRevenueCurrencyId = d.ExpectedRevenueCurrencyId,
                ExpectedMargin = d.ExpectedMargin,
                MaxPreSalesSpending = d.MaxPreSalesSpending,
                OfferDueDate = d.OfferDueDate,
                OfferTypeId = d.OfferTypeId,
                Timestamp = d.Timestamp
            };
        }
    }
}
