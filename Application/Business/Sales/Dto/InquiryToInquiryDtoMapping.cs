﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Allocation.BusinessLogics;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Entities.Modules.Sales;

namespace Smt.Atomic.Business.Sales.Dto
{
    public class InquiryToInquiryDtoMapping : ClassMapping<Inquiry, InquiryDto>
    {
        public InquiryToInquiryDtoMapping()
        {
            Mapping = e => new InquiryDto
            {
                Id = e.Id,
                CustomerId = e.CustomerId,
                CustomerDisplayName = e.Customer == null ? string.Empty : e.Customer.ShortName,
                DisplayName = e.DisplayName,
                ProcessStatus = e.ProcessStatus,
                BusinessLineOrgUnitId = e.BusinessLineOrgUnitId,
                BusinessLineOrgUnitName = e.BusinessLineOrgUnit == null ? string.Empty : e.BusinessLineOrgUnit.FlatName,
                Description = e.Description,
                SalesPersonId = e.SalesPersonId,
                SalesPersonName = e.SalesPerson == null ? string.Empty : e.SalesPerson.DisplayName,
                ProjectPropability = e.ProjectPropability,
                ProjectImportance = e.ProjectImportance,
                ExpectedRevenue = e.ExpectedRevenue,
                ExpectedRevenueCurrencyId = e.ExpectedRevenueCurrencyId,
                ExpectedRevenueCurrencyName = e.ExpectedRevenueCurrency == null ? string.Empty : e.ExpectedRevenueCurrency.DisplayName,
                ExpectedMargin = e.ExpectedMargin,
                MaxPreSalesSpending = e.MaxPreSalesSpending,
                OfferDueDate = e.OfferDueDate,
                OfferTypeId = e.OfferTypeId,
                OfferTypeName = e.OfferType == null ? string.Empty : e.OfferType.OfferName,
                WatcherIds = e.Watchers == null ? new long[0] : e.Watchers.GetIds(),
                WatchersNames = e.Watchers == null ? new Dictionary<long, string>() : e.Watchers.ToDictionary(w => w.Id, w => EmployeeBusinessLogic.FullName.Call(w)),
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
