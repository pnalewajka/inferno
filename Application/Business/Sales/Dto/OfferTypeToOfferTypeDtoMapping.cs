﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Sales;

namespace Smt.Atomic.Business.Sales.Dto
{
    public class OfferTypeToOfferTypeDtoMapping : ClassMapping<OfferType, OfferTypeDto>
    {
        public OfferTypeToOfferTypeDtoMapping()
        {
            Mapping = e => new OfferTypeDto
            {
                Id = e.Id,
                OfferName = e.OfferName,
                HasInquiries = e.Inquiries.Any(),
                Timestamp = e.Timestamp
            };
        }
    }
}
