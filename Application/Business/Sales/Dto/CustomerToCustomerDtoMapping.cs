﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Sales;

namespace Smt.Atomic.Business.Sales.Dto
{
    public class CustomerToCustomerDtoMapping : ClassMapping<Customer, CustomerDto>
    {
        public CustomerToCustomerDtoMapping()
        {
            Mapping = e => new CustomerDto
            {
                Id = e.Id,
                LegalName = e.LegalName,
                ShortName = e.ShortName,
                Timestamp = e.Timestamp
            };
        }
    }
}
