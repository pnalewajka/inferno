﻿using Smt.Atomic.CrossCutting.Business.Enums;
using System;
using System.Collections.Generic;

namespace Smt.Atomic.Business.Sales.Dto
{
    public class InquiryDto
    {
        public InquiryDto()
        {
            WatcherIds = new List<long>();
        }

        public long Id { get; set; }

        public long CustomerId { get; set; }

        public string DisplayName { get; set; }

        public string CustomerDisplayName { get; set; }

        public InquiryStatus ProcessStatus { get; set; }


        public long? BusinessLineOrgUnitId { get; set; }

        public string BusinessLineOrgUnitName { get; set; }

        public string Description { get; set; }

        public long SalesPersonId { get; set; }

        public string SalesPersonName { get; set; }

        public int? ProjectPropability { get; set; }

        public ImportanceType ProjectImportance { get; set; }

        public decimal? ExpectedRevenue { get; set; }

        public long? ExpectedRevenueCurrencyId { get; set; }

        public string ExpectedRevenueCurrencyName { get; set; }

        public decimal? ExpectedMargin { get; set; }

        public decimal? MaxPreSalesSpending { get; set; }

        public DateTime? OfferDueDate { get; set; }

        public long OfferTypeId { get; set; }

        public string OfferTypeName { get; set; }

        public IReadOnlyCollection<long> WatcherIds { get; set; }

        public IDictionary<long, string> WatchersNames { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
