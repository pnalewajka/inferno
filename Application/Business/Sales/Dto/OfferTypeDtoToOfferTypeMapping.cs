﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Sales;

namespace Smt.Atomic.Business.Sales.Dto
{
    public class OfferTypeDtoToOfferTypeMapping : ClassMapping<OfferTypeDto, OfferType>
    {
        public OfferTypeDtoToOfferTypeMapping()
        {
            Mapping = d => new OfferType
            {
                Id = d.Id,
                OfferName = d.OfferName,
                Timestamp = d.Timestamp
            };
        }
    }
}
