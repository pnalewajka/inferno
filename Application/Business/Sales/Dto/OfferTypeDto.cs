﻿using System;

namespace Smt.Atomic.Business.Sales.Dto
{
    public class OfferTypeDto
    {
        public long Id { get; set; }

        public string OfferName { get; set; }

        public bool HasInquiries { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
