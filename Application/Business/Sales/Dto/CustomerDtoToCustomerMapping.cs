﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Sales;

namespace Smt.Atomic.Business.Sales.Dto
{
    public class CustomerDtoToCustomerMapping : ClassMapping<CustomerDto, Customer>
    {
        public CustomerDtoToCustomerMapping()
        {
            Mapping = d => new Customer
            {
                Id = d.Id,
                LegalName = d.LegalName,
                ShortName = d.ShortName,
                Timestamp = d.Timestamp
            };
        }
    }
}
