﻿using System;

namespace Smt.Atomic.Business.Sales.Dto
{
    public class CustomerDto
    {
        public long Id { get; set; }

        public string LegalName { get; set; }

        public string ShortName { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
