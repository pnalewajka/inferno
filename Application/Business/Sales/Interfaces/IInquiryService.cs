﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Sales.Dto;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.Business.Sales.Interfaces
{
    public interface IInquiryService
    {
        BoolResult ChangeStatus(long inquiryId, InquiryStatus newStatus);
    }
}
