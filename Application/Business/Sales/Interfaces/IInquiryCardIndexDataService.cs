﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Sales.Dto;

namespace Smt.Atomic.Business.Sales.Interfaces
{
    public interface IInquiryCardIndexDataService : ICardIndexDataService<InquiryDto>
    {
    }
}

