using System.Collections.Generic;

namespace Smt.Atomic.Business.Consents.Dto
{
    public class ConsentGroupDto
    {
        public string EnumIdentifier { get; set; }

        public string Description { get; set; }

        public IEnumerable<ConsentDto> Consents { get; set; }  
    }
}