﻿using Smt.Atomic.Data.Entities.Modules.Consents;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Consents.Dto
{
    public class ConsentDecisionToConsentDecisionDtoMapping : ClassMapping<ConsentDecision, ConsentDecisionDto>
    {
        public ConsentDecisionToConsentDecisionDtoMapping()
        {
            Mapping = e => new ConsentDecisionDto
            {
                Id = e.Id,
                UserId = e.UserId,
                EnumIdentifier = e.EnumIdentifier,
                EnumValue = e.EnumValue,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
