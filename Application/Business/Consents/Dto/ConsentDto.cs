﻿namespace Smt.Atomic.Business.Consents.Dto
{
    public class ConsentDto
    {
        public string ConsentValue { get; set; }

        public string Description { get; set; }

        public bool IsRevoked { get; set; }
    }
}