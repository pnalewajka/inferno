﻿using Smt.Atomic.Data.Entities.Modules.Consents;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.Business.Consents.Dto
{
    public class ConsentDecisionDtoToConsentDecisionMapping : ClassMapping<ConsentDecisionDto, ConsentDecision>
    {
        public ConsentDecisionDtoToConsentDecisionMapping()
        {
            Mapping = d => new ConsentDecision
            {
                Id = d.Id,
                UserId = d.UserId,
                EnumIdentifier = d.EnumIdentifier,
                EnumValue = d.EnumValue,
                Timestamp = d.Timestamp
            };
        }
    }
}
