﻿using System;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.Business.Consents.Attributes
{
    public class ConsentRestrictionAttribute : DescriptionLocalizedAttribute
    {
        public ConsentRestrictionAttribute(string description, Type resourceType)
            : base(description, resourceType)
        {
        }
    }
}