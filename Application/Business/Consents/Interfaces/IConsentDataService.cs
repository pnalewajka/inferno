﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Consents.Dto;

namespace Smt.Atomic.Business.Consents.Interfaces
{
    public interface IConsentDataService
    {
        void ToggleConsent(ConsentDecisionDto consentDecisionDto, long userId);

        IEnumerable<ConsentGroupDto> GetUserConsentGroups(long userId);

        IEnumerable<long> ExcludeUsersWithoutConsents<T>(T consentValue, IReadOnlyCollection<long> userIds)
            where T : struct, IConvertible;
    }
}