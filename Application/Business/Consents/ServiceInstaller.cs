﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.Consents.Interfaces;
using Smt.Atomic.Business.Consents.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.Consents
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            container.Register(Component.For<IConsentDataService>().ImplementedBy<ConsentDataService>().LifestylePerThread());
        }
    }
}

