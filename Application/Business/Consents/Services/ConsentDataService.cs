﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Castle.Core.Internal;
using Smt.Atomic.Business.Consents.Attributes;
using Smt.Atomic.Business.Consents.Dto;
using Smt.Atomic.Business.Consents.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Data.Entities.Modules.Consents;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Consents.Services
{
    internal class ConsentDataService : IConsentDataService
    {
        private readonly IUnitOfWorkService<IConsentsDbScope> _unitOfWorkService;

        public ConsentDataService(
            IUnitOfWorkService<IConsentsDbScope> unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        public void ToggleConsent(ConsentDecisionDto consentDecision, long userId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var user = unitOfWork.Repositories.Users.GetById(userId);

                var userConsent =
                    unitOfWork.Repositories.ConsentDecisions.SingleOrDefault(
                        rc =>
                            rc.UserId == user.Id
                            && rc.EnumIdentifier == consentDecision.EnumIdentifier
                            && rc.EnumValue == consentDecision.EnumValue);

                if (userConsent == null)
                {
                    unitOfWork.Repositories.ConsentDecisions.Add(new ConsentDecision
                    {
                        User = user,
                        EnumIdentifier = consentDecision.EnumIdentifier,
                        EnumValue = consentDecision.EnumValue,
                        ConsentDecisionType = ConsentDecisionType.Revoked
                    });
                }
                else
                {
                    unitOfWork.Repositories.ConsentDecisions.Delete(userConsent);
                }

                unitOfWork.Commit();
            }
        }

        public IEnumerable<ConsentGroupDto> GetUserConsentGroups(long userId)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var user = unitOfWork.Repositories.Users.GetById(userId);

                var consentDecisions = unitOfWork
                    .Repositories
                    .ConsentDecisions
                    .Where(u => u.UserId == user.Id)
                    .GroupBy(rc => rc.EnumIdentifier)
                    .ToDictionary(rc => rc.Key, rc => rc.ToList());

                var types = Assembly
                    .GetExecutingAssembly()
                    .GetTypes()
                    .Where(t => t.IsEnum
                        && t.HasAttribute<ConsentRestrictionAttribute>()
                        && t.HasAttribute<IdentifierAttribute>())
                    .ToList();

                var consents = new List<ConsentGroupDto>();

                foreach (var type in types)
                {
                    var description = type.GetAttribute<ConsentRestrictionAttribute>().Description;
                    var enumIdentifier = type.GetAttribute<IdentifierAttribute>().Value;

                    var enumConsentDecisions = consentDecisions.ContainsKey(enumIdentifier)
                        ? consentDecisions[enumIdentifier]
                        : new List<ConsentDecision>();

                    consents.Add(new ConsentGroupDto
                    {
                        EnumIdentifier = enumIdentifier,
                        Description = description,
                        Consents = GetConsents(type, enumConsentDecisions)
                    });
                }

                return consents;
            }
        }

        public IEnumerable<long> ExcludeUsersWithoutConsents<T>(T consentValue, IReadOnlyCollection<long> userIds) where T : struct, IConvertible
        {
            var consentType = typeof(T);

            if (!consentType.IsEnum || !consentType.HasAttribute<ConsentRestrictionAttribute>() || !consentType.HasAttribute<IdentifierAttribute>())
            {
                throw new BusinessException($"{consentType.FullName} must be Enum with '{nameof(ConsentRestrictionAttribute)}' and '{nameof(IdentifierAttribute)}'");
            }

            var value = consentValue.ToString(CultureInfo.CurrentCulture);
            var enumIdentifier = consentType.GetAttribute<IdentifierAttribute>().Value;

            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var excludedUserIds = unitOfWork.Repositories.ConsentDecisions.Where(rc =>
                        rc.EnumIdentifier == enumIdentifier
                        && rc.ConsentDecisionType == ConsentDecisionType.Revoked
                        && rc.EnumValue == value
                        && userIds.Contains(rc.UserId)
                    )
                    .Select(rc => rc.UserId)
                    .ToList();

                return userIds.Except(excludedUserIds).ToList();
            }
        }

        private IEnumerable<ConsentDto> GetConsents(Type enumeType, IReadOnlyCollection<ConsentDecision> consentDecisions)
        {
            var values = Enum.GetValues(enumeType).Cast<Enum>();

            foreach (var enumValue in values)
            {
                var value = enumValue.ToString();

                yield return new ConsentDto
                {
                    ConsentValue = value,
                    Description = enumValue.GetDescriptionOrValue(),
                    IsRevoked = consentDecisions.Any(rc => rc.EnumValue == value && rc.ConsentDecisionType == ConsentDecisionType.Revoked)
                };
            }
        }
    }
}