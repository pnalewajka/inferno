﻿using Smt.Atomic.Business.Consents.Attributes;
using Smt.Atomic.Business.Consents.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.Business.Consents.Enums
{
    [Identifier("AllocationRequest.EmailConsent")]
    [ConsentRestriction("GroupName", typeof(AllocationRequestEmailConsentEnumResources))]
    public enum AllocationRequestEmailConsentEnum
    {
        [DescriptionLocalized("RequestAddedEmailName", typeof(AllocationRequestEmailConsentEnumResources))]
        RequestAddedEmail,

        [DescriptionLocalized("RequestEditedEmailName", typeof(AllocationRequestEmailConsentEnumResources))]
        RequestEditedEmail,

        [DescriptionLocalized("RequestDeletedEmailName", typeof(AllocationRequestEmailConsentEnumResources))]
        RequestDeletedEmail,

        [DescriptionLocalized("UnderUtilizedEmployeesEmailRaportEmailName", typeof(AllocationRequestEmailConsentEnumResources))]
        UnderUtilizedEmployeesEmailRaportEmail,

        [DescriptionLocalized("NonAllocatedNewHiresEmailRaportEmailName", typeof(AllocationRequestEmailConsentEnumResources))]
        NonAllocatedNewHiresEmailRaportEmail,

        [DescriptionLocalized("FreeOrPlannedRaportEmailName", typeof(AllocationRequestEmailConsentEnumResources))]
        FreeOrPlannedRaportEmail
    }
}
