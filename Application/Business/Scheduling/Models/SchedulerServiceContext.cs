﻿namespace Smt.Atomic.Business.Scheduling.Models
{
    public class SchedulerServiceContext
    {
        public string Host { get; private set; }
        public string SchedulerName { get; private set; }
        public string Pipeline { get; private set; }

        public SchedulerServiceContext(string host, string schedulerName, string pipeline)
        {
            Host = host;
            SchedulerName = schedulerName;
            Pipeline = pipeline;
        }
    }
}
