using System;
using System.Reflection;
using System.Threading;
using Castle.Core.Logging;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.Business.Scheduling.Exceptions;
using Smt.Atomic.Business.Scheduling.Interfaces;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;

namespace Smt.Atomic.Business.Scheduling.Models
{
    public class JobTask
    {
        private const string JobStartedOnFormat = "{0}. Job started.";
        private const string JobFinishedOnFormat = "{0}. Job finished.";
        private const string JobExecutionFailedFormat = "{0}. Job execution failed";
        private const string JobExecutionFailedLastAttemptFormat = "{0}. Job execution failed. This was the last attempt.";
        private const string JobExecutionExceptionFormat = "{0}. Job execution exception.";
        private const string JobExecutionExceptionLastAttemptFormat = "{0}. Job execution exception. This was the last attempt.";
        private const string JobExecutionAbortedFormat = "{0}. Job execution aborted at {1}, execution time exceeded ({2}).";

        private readonly ILogger _logger;
        private readonly IJobExecutionContextService _jobExecutionContextService;
        private IJob _job;
        private readonly ITimeService _timeService;
        private JobDefinitionDto _jobDefinitionDto;
        private JobExecutionContext _jobExecutionContext;

        public JobTask(ILogger logger, IJobExecutionContextService jobExecutionContextService, ITimeService timeService)
        {
            _logger = logger;
            _jobExecutionContextService = jobExecutionContextService;
            _timeService = timeService;
        }

        public void Init(JobDefinitionDto jobDefinitionDto, JobExecutionContext jobExecutionContext)
        {
            _jobDefinitionDto = jobDefinitionDto;
            _job = ReflectionHelper.CreateInstanceByIdentifier<IJob>(jobDefinitionDto.ClassIdentifier);
            _jobExecutionContext = jobExecutionContext;
        }

        public void Execute()
        {
            try
            {
                _jobExecutionContextService.SetContext(Thread.CurrentThread, _jobExecutionContext);

                _logger.InfoFormat(JobStartedOnFormat, _jobExecutionContext.GetMessagePreambleWithThreadInfo());

                var result = _job.DoJob(_jobExecutionContext);

                if (!result.IsJobFinishedGracefully)
                {
                    throw new JobFailedException();
                }

                _logger.InfoFormat(JobFinishedOnFormat, _jobExecutionContext.GetMessagePreambleWithThreadInfo());
            }
            catch (ThreadAbortException)
            {
                _logger.WarnFormat(JobExecutionAbortedFormat, _jobExecutionContext.GetMessagePreambleWithThreadInfo(), _timeService.GetCurrentTime(), TimeSpan.FromSeconds(_jobDefinitionDto.MaxExecutionPeriodInSeconds));
            }
            catch (JobFailedException)
            {
                _logger.ErrorFormat(_jobExecutionContext.IsLastAttempt
                        ? JobExecutionFailedLastAttemptFormat
                        : JobExecutionFailedFormat, _jobExecutionContext.GetMessagePreambleWithThreadInfo());

                throw;
            }
            catch (Exception exception)
            {
                _logger.ErrorFormat(exception,
                        _jobExecutionContext.IsLastAttempt
                            ? JobExecutionExceptionLastAttemptFormat
                            : JobExecutionExceptionFormat, _jobExecutionContext.GetMessagePreambleWithThreadInfo());

                var reflectionTypeLoadException = exception as ReflectionTypeLoadException;

                if (reflectionTypeLoadException?.LoaderExceptions != null)
                {
                    foreach (var loaderException in reflectionTypeLoadException.LoaderExceptions)
                    {
                        _logger.Fatal("Failed to load type", loaderException);
                    }
                }

                throw;
            }
            finally
            {
                _jobExecutionContextService.DeleteContext(Thread.CurrentThread);
            }
        }
    }
}