﻿using System;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Scheduling.Dto;

namespace Smt.Atomic.Business.Scheduling.Models
{
    public class JobLastAttemptFailureDto
    {
        public DateTime CurrentTime { get; set; }
        public string DetailsUrl { get; set; }
        public JobDefinitionDto JobDefinition { get; set; }
        public JobExecutionContext JobExecutionContext { get; set; }
    }
}
