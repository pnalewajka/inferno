﻿namespace Smt.Atomic.Business.Scheduling.Models
{
    internal enum JobExecutionPriority
    {
        Normal = 0,
        Overdue = 1,
        Retry = 2,
        Critical = 3
    }
}