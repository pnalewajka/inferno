﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Runtime.Dto;
using Smt.Atomic.Business.Scheduling.Dto;

namespace Smt.Atomic.Business.Scheduling.Models
{
    internal class FailureInfoManager : Dictionary<long, FailureInfo>
    {
        public FailureInfo GetFailureInfo(JobDefinitionDto jobDefinitionDto)
        {
            return this[jobDefinitionDto.Id];
        }

        public void ClearFailureInfo(JobDefinitionDto jobDefinitionDto)
        {
            this[jobDefinitionDto.Id].Clear();
        }

        public void MaintainFailureInfo(IEnumerable<JobDefinitionDto> jobDefinitionDtos)
        {
            foreach (var jobDefinitionDto in jobDefinitionDtos)
            {
                if (!ContainsKey(jobDefinitionDto.Id))
                {
                    Add(jobDefinitionDto.Id, new FailureInfo());
                }
            }
        }

        public void MaintainFailedExecutionCount(long jobDefinitionId, int? timesFailedInRow)
        {
            this[jobDefinitionId].RunAttemptInBulk = timesFailedInRow ?? 0;
        }

        public void SetGeneralFailure(JobDefinitionDto jobDefinitionDto)
        {
            this[jobDefinitionDto.Id].IsGeneralFailure = true;
        }

        public void SetFailure(JobDefinitionDto jobDefinitionDto, DateTime currentTime)
        {
            var failureInfo = this[jobDefinitionDto.Id];

            failureInfo.LastFailure = currentTime;
            failureInfo.RunAttemptInBulk++;
        }

        public void AdjustFailureCount(JobDefinitionDto jobDefinitionDto, JobRunInfoDto jobRunInfoDto)
        {
            var failureInfo = this[jobDefinitionDto.Id];

            failureInfo.BulkRunAttempt = jobRunInfoDto.TimesFailedInRow.HasValue
                ? jobRunInfoDto.TimesFailedInRow.Value
                : 0;
        }

        public int? GetFailureCount(JobDefinitionDto jobDefinitionDto)
        {
            var failureInfo = this[jobDefinitionDto.Id];

            if (failureInfo.RunAttemptInBulk == 0)
            {
                return null;
            }

            return failureInfo.RunAttemptInBulk;
        }
    }
}