﻿using System;

namespace Smt.Atomic.Business.Scheduling.Models
{
    internal class FailureInfo
    {
        public bool IsGeneralFailure { get; set; }
        public int RunAttemptInBulk { get; set; }
        public int BulkRunAttempt { get; set; }
        public DateTime? LastFailure { get; set; }

        public void Clear()
        {
            RunAttemptInBulk = 0;
            BulkRunAttempt = 0;
            LastFailure = null;
            IsGeneralFailure = false;
        }
    }
}
