﻿using System;
using Smt.Atomic.Business.Scheduling.Dto;

namespace Smt.Atomic.Business.Scheduling.Models
{
    internal class JobScheduledRun
    {
        public JobDefinitionDto JobDefinitionDto { get; set; }
        public DateTime NextRunTime { get; set; }
        public int? TimesFailedInRow { get; set; }
        public DateTime? LastFinishTime { get; set; }
    }
}