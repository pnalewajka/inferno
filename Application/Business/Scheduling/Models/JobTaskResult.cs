using System;

namespace Smt.Atomic.Business.Scheduling.Models
{
    public class JobTaskResult
    {
        public bool HasFinishedProperly { get; set; }
        public bool WasAborted { get; set; }
        public Exception Exception { get; private set; }

        public JobTaskResult(bool hasFinishedProperly, bool wasAborted, Exception exception)
        {
            HasFinishedProperly = hasFinishedProperly;
            WasAborted = wasAborted;
            Exception = exception;
        }
    }
}