﻿using System;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Exceptions;

namespace Smt.Atomic.Business.Scheduling.Interfaces
{
    public interface IJobManagementService
    {
        bool AcquireExecutionRights(long jobDefinitionId);
        void ReleaseExecutionRights(long jobDefinitionId);

        /// <summary>
        /// Set all required conditions for the job to start and returns the name of the pipeline for the job
        /// </summary>
        /// <param name="jobId"></param>
        /// <returns>job's pipeline name</returns>
        string ForceImmediateExecution(long jobId);

        /// <summary>
        /// Set all required conditions for the job to start and returns the name of the pipeline for the job
        /// </summary>
        /// <param name="jobCode"></param>
        /// <exception cref="BusinessException">Thrown when cannot find job by jobCode</exception>
        /// <returns>job's pipeline name</returns>
        string ForceImmediateExecution(string jobCode);
    }
}
