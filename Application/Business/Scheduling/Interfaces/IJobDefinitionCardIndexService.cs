﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Scheduling.Dto;

namespace Smt.Atomic.Business.Scheduling.Interfaces
{
    public interface IJobDefinitionCardIndexService : ICardIndexDataService<JobDefinitionDto>
    {
        BoolResult Resurrect(long jobDefinitionId);

        BoolResult ForceRun(long jobDefinitionId);
    }
}
