﻿namespace Smt.Atomic.Business.Scheduling.Interfaces
{
    /// <summary>
    /// Interface of module responsible of running jobs at given schedule
    /// </summary>
    public interface ISchedulerService
    {
        /// <summary>
        /// start main loop
        /// </summary>
        void StartScheduler(string pipelineName);

        /// <summary>
        /// signal end of main loop (will return synchronously after loop is stopped)
        /// </summary>
        void StopScheduler();
    }
}
