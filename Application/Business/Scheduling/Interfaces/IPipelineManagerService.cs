﻿namespace Smt.Atomic.Business.Scheduling.Interfaces
{
    public interface IPipelineManagerService
    {
        void CreateAndStartPipeline(string pipelineName);
        void Stop();
        void Abort();
        int PipelineCount { get; }
    }
}
