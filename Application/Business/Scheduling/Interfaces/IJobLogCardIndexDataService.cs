﻿using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Scheduling.Dto;

namespace Smt.Atomic.Business.Scheduling.Interfaces
{
    public interface IJobLogCardIndexDataService : ICardIndexDataService<JobLogDto>
    {
    }
}

