﻿using System;

namespace Smt.Atomic.Business.Scheduling.Interfaces
{
    public interface IEntitySynchronizationConfigurationService
    {
        /// <summary>
        /// Returns last synchronization time
        /// </summary>
        /// <param name="entityName">Name of synchronized entity</param>
        /// <returns></returns>
        DateTime? GetLastSynchronizationTime(string entityName);

        /// <summary>
        /// Stores last synchronization time
        /// </summary>
        /// <param name="entityName">Name of synchronized entity</param>
        /// <param name="synchronizationTime">Time of synchronization to store</param>
        void SetLastSynchronizationTime(string entityName, DateTime? synchronizationTime);
    }
}