﻿using System.Collections.Generic;
using Smt.Atomic.Business.Scheduling.Dto;

namespace Smt.Atomic.Business.Scheduling.Interfaces
{
    public interface IJobDefinitionService
    {
        /// <summary>
        /// Get list of available jobs for specific pipeline
        /// </summary>
        /// <param name="pipeline"></param>
        /// <returns></returns>
        List<JobDefinitionDto> GetJobDefinitions(string pipeline);
    }
}
