﻿using System.Threading;
using Smt.Atomic.Business.Common.Contexts;

namespace Smt.Atomic.Business.Scheduling.Interfaces
{
    public interface IJobExecutionContextService
    {
        JobExecutionContext GetContext(Thread thread);

        void SetContext(Thread thread, JobExecutionContext jonExecutionContext);

        void DeleteContext(Thread thread);
    }
}