﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Scheduling.ChoreProviders;
using Smt.Atomic.Business.Scheduling.Interfaces;
using Smt.Atomic.Business.Scheduling.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Business.Scheduling
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            if (containerType == ContainerType.JobScheduler)
            {
                container.Register(Component.For<IPipelineManagerService>().ImplementedBy<PipelineManagerService>().LifestylePerThread());
                container.Register(Component.For<ISchedulerService>().ImplementedBy<SchedulerService>().LifestylePerThread());
                container.Register(Component.For<IJobDefinitionService>().ImplementedBy<JobDefinitionService>().LifestylePerThread());
                container.Register(Classes.FromThisAssembly().BasedOn<IJob>().LifestylePerThread());
                container.Register(Component.For<IJobManagementService>().ImplementedBy<JobManagementService>().LifestylePerThread());
                container.Register(Component.For<IJobExecutionContextService>().ImplementedBy<JobExecutionContextService>().LifestyleSingleton());
            }

            if (containerType == ContainerType.WebApp)
            {
                container.Register(Component.For<IJobDefinitionCardIndexService>().ImplementedBy<JobDefinitionCardIndexService>().LifestylePerWebRequest());
                container.Register(Component.For<IJobDefinitionParameterCardIndexService>().ImplementedBy<JobDefinitionParameterCardIndexService>().LifestylePerWebRequest());
                container.Register(Component.For<IPipelineCardIndexService>().ImplementedBy<PipelineCardIndexService>().LifestylePerWebRequest());
                container.Register(Component.For<IJobLogCardIndexDataService>().ImplementedBy<JobLogCardIndexDataService>().LifestyleTransient());
                container.Register(Component.For<IChoreProvider>().ImplementedBy<DefunctJobChoreProvider>().LifestylePerWebRequest());
            }

            if (containerType == ContainerType.JobScheduler)
            {
                container.Register(Component.For<IEntitySynchronizationConfigurationService>().ImplementedBy<EntitySynchronizationConfigurationService>().LifestyleTransient());
            }
        }
    }
}

