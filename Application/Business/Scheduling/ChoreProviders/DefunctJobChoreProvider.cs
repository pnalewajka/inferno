﻿using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Scheduling;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Business.Scheduling.ChoreProviders
{
    public class DefunctJobChoreProvider
        : IChoreProvider
    {
        private readonly IPrincipalProvider _principalProvider;

        public DefunctJobChoreProvider(
            IPrincipalProvider principalProvider)
        {
            _principalProvider = principalProvider;
        }

        public IQueryable<ChoreDto> GetActiveChores(
            IReadOnlyRepositoryFactory repositoryFactory)
        {
            if (!_principalProvider.Current.IsInRole(SecurityRoleType.CanManageJobDefinitions))
            {
                return null;
            }

            var jobDefinitions = repositoryFactory.Create<JobDefinition>().Filtered();

            var chores = jobDefinitions
                .Where(d => d.IsEnabled)
                .Where(d => d.JobRunInfos.Any())
                .Where(d => d.JobRunInfos
                    .OrderByDescending(i => i.Id).Take(1)
                    .Where(i => i.Status == JobStatus.Failed)
                    .Where(i => i.TimesFailedInRow >= (i.JobDefinition.MaxBulkRunAttempts * i.JobDefinition.MaxRunAttemptsInBulk))
                    .Any())
                .Select(j => new ChoreDto
                {
                    Type = ChoreType.DefunctJob,
                    DueDate = j.LastFailureOn,
                    Parameters = j.Id + "," + j.Code,
                });

            return chores;
        }
    }
}
