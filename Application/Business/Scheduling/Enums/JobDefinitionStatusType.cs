﻿using Smt.Atomic.Business.Scheduling.Resources;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace Smt.Atomic.Business.Scheduling.Enums
{
    public enum JobStatusType
    {
        [DescriptionLocalized("NoDataEnum", typeof(JobDefinitionResources))]
        NoData = 0,

        [DescriptionLocalized("HealthyEnum", typeof (JobDefinitionResources))]
        Healthy = 1,

        [DescriptionLocalized("RunningEnum", typeof (JobDefinitionResources))]
        Running = 2,

        [DescriptionLocalized("FailedEnum", typeof (JobDefinitionResources))]
        Failed = 3,

        [DescriptionLocalized("DefunctEnum", typeof (JobDefinitionResources))]
        Defunct = 4
    }
}