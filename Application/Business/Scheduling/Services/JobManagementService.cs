﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Scheduling.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Models;
using Smt.Atomic.Data.Entities.Modules.Scheduling;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Scheduling.Services
{
    /// <summary>
    /// This class should be instantiated only once (lifetime : singleton)
    /// </summary>
    internal class JobManagementService : IJobManagementService
    {
        private const int ExpirationPeriodInSeconds = 5 * 60;
        private readonly ISettingsProvider _settingsProvider;
        private readonly IUnitOfWorkService<ISchedulingDbScope> _unitOfWorkService;
        private JobSchedulerSettings _jobSchedulerSettings;
        private DateTime _lastSettingsSyncedOn;
        private readonly List<long> _runningJobIds = new List<long>();
        private readonly object _lockObject = new object();

        public JobManagementService(
            ISettingsProvider settingsProvider,
            IUnitOfWorkService<ISchedulingDbScope> unitOfWorkService)
        {
            _settingsProvider = settingsProvider;
            _unitOfWorkService = unitOfWorkService;
            SyncSettings();
        }

        private void SyncSettings()
        {
            if (DateTime.Now.Subtract(_lastSettingsSyncedOn).TotalSeconds >= ExpirationPeriodInSeconds)
            {
                _jobSchedulerSettings = _settingsProvider.JobSchedulerSettings;
                _lastSettingsSyncedOn = DateTime.Now;
            }
        }

        public bool AcquireExecutionRights(long jobDefinitionId)
        {
            SyncSettings();
            lock (_lockObject)
            {
                if (_jobSchedulerSettings.MaxPipelines >= _runningJobIds.Count)
                {
                    _runningJobIds.Add(jobDefinitionId);
                    return true;
                }

                return false;
            }
        }

        public void ReleaseExecutionRights(long jobDefinitionId)
        {
            lock (_lockObject)
            {
                _runningJobIds.Remove(jobDefinitionId);
            }
        }

        public string ForceImmediateExecution(long jobId)
        {
            return ForceImmediateExecution(x => x.Id == jobId);
        }

        public string ForceImmediateExecution(string jobCode)
        {
            return ForceImmediateExecution(x => x.Code.ToLower() == jobCode.ToLower());
        }

        private string ForceImmediateExecution(Expression<Func<JobDefinition, bool>> predicate)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var jobDefinitions = unitOfWork.Repositories.JobDefinitions;
                var jobRunInfos = unitOfWork.Repositories.JobRunInfos;

                foreach (var job in jobDefinitions)
                {
                    job.ShouldRunNow = false;
                }

                var jobDefinition = jobDefinitions.Single(predicate);

                long jobId = jobDefinition.Id;
                jobDefinition.ShouldRunNow = true;
                var pipelineName = jobDefinition.Pipeline.Name;

                jobRunInfos.DeleteEach(i => i.JobDefinitionId == jobId);

                unitOfWork.Commit();

                return pipelineName;
            }
        }
    }
}
