﻿using System;
using System.Security.Principal;
using System.Threading;
using Smt.Atomic.Business.Scheduling.Interfaces;

namespace Smt.Atomic.Business.Scheduling.Services
{
    public class PipelineThread
    {
        private readonly Func<ISchedulerService> _schedulerServiceFactory;
        private readonly string _name;
        private readonly Thread _thread;
        private readonly IPrincipal _principal;
        private ISchedulerService _schedulerService;

        public PipelineThread(Func<ISchedulerService> schedulerServiceFactory, string name, IPrincipal principal)
        {
            _schedulerServiceFactory = schedulerServiceFactory;
            _name = name;
            _thread = new Thread(WorkerMethod);
            _principal = principal;
        }

        public void Run()
        {
            _thread.Start();
        }

        public void Stop()
        {
            if (_schedulerService != null)
            {
                _schedulerService.StopScheduler();
            }
        }

        public void Abort()
        {
            _thread.Abort();
        }

        public void Join()
        {
            _thread.Join();
        }

        private void WorkerMethod()
        {
            try
            {
                Thread.CurrentPrincipal = _principal;
                _schedulerService = _schedulerServiceFactory();
                _schedulerService.StartScheduler(_name);
            }
            catch (ThreadAbortException)
            {
                // ignore abort exception
                Thread.ResetAbort();
            }
        }
    }
}
