﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.Business.Scheduling.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Scheduling;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Scheduling.Services
{
    public class JobDefinitionParameterCardIndexService : CardIndexDataService<JobDefinitionParameterDto, JobDefinitionParameter, ISchedulingDbScope>, IJobDefinitionParameterCardIndexService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public JobDefinitionParameterCardIndexService(ICardIndexServiceDependencies<ISchedulingDbScope> dependencies) : base(dependencies)
        {
        }

        protected override IEnumerable<Expression<Func<JobDefinitionParameter, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return j => j.ParameterKey;
        }

        public override IQueryable<JobDefinitionParameter> ApplyContextFiltering(IQueryable<JobDefinitionParameter> records, object context)
        {
            if (context != null)
            {
                var parentChildContext = (ParentIdContext)context;
                return records.Where(j => j.JobDefinitionId == parentChildContext.ParentId);
            }

            return base.ApplyContextFiltering(records, null);
        }

        public override void SetContext(JobDefinitionParameter entity, object context)
        {
            if (context != null)
            {
                var parentChildContext = context as ParentIdContext;
                if (parentChildContext != null)
                {
                    entity.JobDefinitionId = parentChildContext.ParentId;
                }

            }

            base.SetContext(entity, context);
        }
    }
}