﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.Business.Scheduling.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Scheduling;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Scheduling.Services
{
    public class PipelineCardIndexService : CardIndexDataService<PipelineDto, Pipeline, ISchedulingDbScope>, IPipelineCardIndexService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public PipelineCardIndexService(ICardIndexServiceDependencies<ISchedulingDbScope> dependencies) : base(dependencies)
        {
        }

        protected override IEnumerable<Expression<Func<Pipeline, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return j => j.Name;
        }
    }
}