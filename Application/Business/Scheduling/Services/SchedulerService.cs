﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Castle.Core.Logging;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Runtime.Dto;
using Smt.Atomic.Business.Runtime.Interfaces;
using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.Business.Scheduling.Exceptions;
using Smt.Atomic.Business.Scheduling.Interfaces;
using Smt.Atomic.Business.Scheduling.Models;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Business.Configuration.Interfaces;

namespace Smt.Atomic.Business.Scheduling.Services
{
    internal class SchedulerService : ISchedulerService
    {
        private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        private readonly FailureInfoManager _failureInfoManager = new FailureInfoManager();
        private readonly IJobDefinitionService _jobDefinitionService;
        private readonly IJobManagementService _jobExecutionRightsService;
        private readonly IJobRunInfoService _jobRunInfoService;
        private readonly object _lockObject = new object();
        private readonly ILogger _logger;
        private readonly DateTime _serviceStartTime;
        private readonly ITimeService _timeService;
        private readonly IDnsService _dnsService;
        private readonly ISettingsProvider _settingsProvider;
        private readonly IReliableEmailService _reliableEmailService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly ISystemParameterService _systemParameterService;
        private volatile bool _serviceStarted;
        private SchedulerServiceContext _schedulerContext;

        public SchedulerService(
            ILogger logger, 
            IJobDefinitionService jobDefinitionService,
            IJobManagementService jobExecutionRightsService, 
            IJobRunInfoService jobRunInfoService,
            ITimeService timeService, 
            IDnsService dnsService, 
            ISettingsProvider settingsProvider,
            IReliableEmailService reliableEmailService,
            IMessageTemplateService messageTemplateService,
            ISystemParameterService systemParameterService)
        {
            _logger = logger;
            _jobDefinitionService = jobDefinitionService;
            _jobExecutionRightsService = jobExecutionRightsService;
            _jobRunInfoService = jobRunInfoService;
            _timeService = timeService;
            _dnsService = dnsService;
            _settingsProvider = settingsProvider;
            _reliableEmailService = reliableEmailService;
            _messageTemplateService = messageTemplateService;
            _systemParameterService = systemParameterService;

            _serviceStartTime = _timeService.GetCurrentTime();
        }

        public void StartScheduler(string pipelineName)
        {
            _schedulerContext = new SchedulerServiceContext(_dnsService.GetHostName(), _settingsProvider.JobSchedulerSettings.SchedulerName, pipelineName);

            _logger.InfoFormat("Starting job scheduler for pipeline {0}", pipelineName);

            lock (_lockObject)
            {
                _serviceStarted = true;
            }

            PerformLoop();
        }

        public void StopScheduler()
        {
            lock (_lockObject)
            {
                _cancellationTokenSource.Cancel(false);
                _serviceStarted = false;
            }

            _logger.InfoFormat("Stopping job scheduler for pipeline {0}", _schedulerContext.Pipeline);
        }

        private bool IsServiceStarted()
        {
            lock (_lockObject)
            {
                return _serviceStarted;
            }
        }

        private void PerformLoop()
        {
            while (IsServiceStarted())
            {
                try
                {
                    ExecuteJob();
                    SleepAfterJobExecution();
                }
                catch (JobConfigurationException)
                {
                    throw;
                }
                catch (Exception e)
                {
                    _logger.ErrorFormat(e, "Exception on pipeline {0}", _schedulerContext.Pipeline);
                }
            }
        }

        private void SleepAfterJobExecution()
        {
            var delayTimespan = new TimeSpan(0, 0, 0, 5 + GetRandomExecutionDelay());

            var waitTask = new Task(() => Thread.Sleep((int) delayTimespan.TotalMilliseconds));

            waitTask.Start();

            try
            {
                waitTask.Wait(_cancellationTokenSource.Token);
            }
            catch (OperationCanceledException)
            {
                //ok, silence exception
            }
        }

        /// <summary>
        /// Random execution delay - to enable load balancing between different instances of JobScheduler
        /// </summary>
        /// <returns></returns>
        private int GetRandomExecutionDelay()
        {
            return new Random().Next(0, 5);
        }

        private void ExecuteJob()
        {
            var currentTime = _timeService.GetCurrentTime();

            //get job list on pipeline ordered by planned execution time
            var jobsRunsOnPipeline = GetJobsRunsFromExecutionList(currentTime).ToList();

            var nextJobRunOnPipeline = 
                jobsRunsOnPipeline.FirstOrDefault(r => r.JobDefinitionDto.ShouldRunNow) ??
                jobsRunsOnPipeline.FirstOrDefault(r => ShouldRun(r, currentTime));

            if (nextJobRunOnPipeline == null)
            {
                return;
            }
            
            //verify existence of IJob implementation for JobDefinition.ClassIdentifier
            VerifyJobDefinition(nextJobRunOnPipeline.JobDefinitionDto);

            JobRunInfoDto jobRunInfoDto = null;

            //acquire execution rights
            if (_jobExecutionRightsService.AcquireExecutionRights(nextJobRunOnPipeline.JobDefinitionDto.Id))
            {
                var jobDefinitionDto = nextJobRunOnPipeline.JobDefinitionDto;
                try
                {
                    var timeoutSpan =
                        TimeSpan.FromSeconds(jobDefinitionDto.MaxExecutionPeriodInSeconds);

                    //db lock, add new runinfo & check if there're any jobs already running
                    jobRunInfoDto =
                        _jobRunInfoService.AcquireJobRunInfoWithDbLock(jobDefinitionDto.Id,
                            currentTime, _schedulerContext.Host, _schedulerContext.SchedulerName);

                    if (jobRunInfoDto == null)
                    {
                        return;
                    }

                    _failureInfoManager.AdjustFailureCount(jobDefinitionDto, jobRunInfoDto);


                    _logger.InfoFormat("Pipeline {0}, next job {1} with cron expression {2} will be started with date {3}",
                        _schedulerContext.Pipeline, jobDefinitionDto.Code, jobDefinitionDto.CronExpression, currentTime);

                    //execute job within tasks for timeout enforcement
                    if (ExecuteWithTimeLimit(nextJobRunOnPipeline, timeoutSpan))
                    {
                        //if everything was ok, then clear failure info
                        SetJobSuccessInfo(jobDefinitionDto, jobRunInfoDto);
                    }
                    else
                    {
                        SetJobFailureInfo(nextJobRunOnPipeline, jobRunInfoDto, jobDefinitionDto);
                    }
                }
                catch (Exception)
                {
                    SetJobFailureInfo(nextJobRunOnPipeline, jobRunInfoDto, jobDefinitionDto);

                    //rethrow for general exception handling
                    throw;
                }
                finally
                {
                    //always release execution rights
                    _jobExecutionRightsService.ReleaseExecutionRights(nextJobRunOnPipeline.JobDefinitionDto.Id);
                }
            }
        }

        private void SetJobSuccessInfo(JobDefinitionDto jobDefinitionDto, JobRunInfoDto jobRunInfoDto)
        {
            _failureInfoManager.ClearFailureInfo(jobDefinitionDto);

            var now = _timeService.GetCurrentTime();

            _jobRunInfoService.ReleaseJobRunInfoWithDbLock(
                jobRunInfoDto.Id,
                jobRunInfoDto.JobDefinitionId,
                now,
                JobStatus.Finished,
                jobDefinitionDto.CronExpression.Schedule.GetNextOccurrence(now, DateTime.MaxValue),
                null
                );
        }

        private void SetJobFailureInfo(JobScheduledRun nextJobRunOnPipeline, JobRunInfoDto jobRunInfoDto,
            JobDefinitionDto jobDefinitionDto)
        {
            _failureInfoManager.SetFailure(nextJobRunOnPipeline.JobDefinitionDto, _timeService.GetCurrentTime());

            if (jobRunInfoDto != null)
            {
                try
                {
                    var now = _timeService.GetCurrentTime();

                    _jobRunInfoService.ReleaseJobRunInfoWithDbLock(
                        jobRunInfoDto.Id,
                        jobRunInfoDto.JobDefinitionId,
                        now,
                        JobStatus.Failed,
                        jobDefinitionDto.CronExpression.Schedule.GetNextOccurrence(now, DateTime.MaxValue),
                        _failureInfoManager.GetFailureCount(jobDefinitionDto)
                        );
                }
                catch (Exception ex)
                {
                    //silencing is intentional, we may not be able to update job state to failed if there are problems with connectivity
                    _logger.Error("Release of db job lock failed", ex);
                }
            }
        }

        private bool ExecuteWithTimeLimit(JobScheduledRun jobScheduledRun, TimeSpan timeoutSpan)
        {
            var jobDefinitionDto = jobScheduledRun.JobDefinitionDto;

            var isLastAttempt = (jobScheduledRun.TimesFailedInRow.HasValue &&
                                jobScheduledRun.TimesFailedInRow.Value + 1 >=
                                jobScheduledRun.JobDefinitionDto.MaxTotalRunAttempts) || jobScheduledRun.JobDefinitionDto.MaxTotalRunAttempts < 2;

            var jobExecutionContext = new JobExecutionContext(_cancellationTokenSource.Token,
                jobDefinitionDto.Parameters.ToDictionary(jdp => jdp.ParameterKey, jdp => jdp.ParameterValue),
                _schedulerContext.Host,
                _schedulerContext.SchedulerName,
                _schedulerContext.Pipeline,
                jobDefinitionDto.Id,
                isLastAttempt,
                timeoutSpan);

            //job execution and task timeout handling
            var timeoutTask = new Task<JobTaskResult>(() =>
            {
                Thread jobThread = null;

                var jobTaskToRun = new Task<JobTaskResult>(() =>
                {
                    try
                    {
                        jobThread = Thread.CurrentThread;


                        var job = ReflectionHelper.CreateInstanceWithIocDependencies<JobTask>();
                        job.Init(jobDefinitionDto, jobExecutionContext);
                        job.Execute();

                        return new JobTaskResult(true, false, null);
                    }
                    catch (Exception e)
                    {
                        if (jobExecutionContext.IsLastAttempt)
                        {
                            SendLastAttemptFailedEmail(new JobLastAttemptFailureDto
                            {
                                CurrentTime = _timeService.GetCurrentTime(),
                                DetailsUrl = _systemParameterService.GetParameter<string>(ParameterKeys.ServerAddress) + "Scheduling/JobLog/List?parent-id=" + jobDefinitionDto.Id,
                                JobDefinition = jobDefinitionDto,
                                JobExecutionContext = jobExecutionContext
                            });
                        }

                        return new JobTaskResult(false, false, e);
                    }
                }, TaskCreationOptions.LongRunning);

                jobTaskToRun.Start();

                var result = jobTaskToRun.Wait((int)timeoutSpan.TotalMilliseconds, _cancellationTokenSource.Token);

                if (!result && jobThread != null)
                {
                    if (!jobThread.Join(new TimeSpan(0, 0, 0, 1)))
                    {
                        jobThread.Abort();

                        return new JobTaskResult(false, true, null);
                    }
                }

                return jobTaskToRun.Result;
            });

            timeoutTask.Start();
            timeoutTask.Wait();

            if (timeoutTask.Result.WasAborted || timeoutTask.Result.Exception != null)
            {
                return false;
            }

            return true;
        }

        private void SendLastAttemptFailedEmail(JobLastAttemptFailureDto jobLastAttemptFailureDto)
        {
            var subject = _messageTemplateService.ResolveTemplate(TemplateCodes.JobSchedulerLastAttemptOfJobFailedSubject, jobLastAttemptFailureDto);
            var body = _messageTemplateService.ResolveTemplate(TemplateCodes.JobSchedulerLastAttemptOfJobFailedBody, jobLastAttemptFailureDto);
            var emailDto = new EmailMessageDto
            {
                IsHtml = body.IsHtml,
                Subject = subject.Content,
                Body = body.Content,
                From = new EmailRecipientDto
                {
                    EmailAddress = _systemParameterService.GetParameter<string>(ParameterKeys.SmtpEmailServiceDefaultFromEmailAddress),
                    FullName = _systemParameterService.GetParameter<string>(ParameterKeys.SmtpEmailServiceDefaultFromFullName)
                },
                Recipients =
                    _systemParameterService.GetParameter<string[]>(ParameterKeys.AdminEmailRecipients)
                        .Select(r => new EmailRecipientDto(r, null))
                        .ToList()
            };

            _reliableEmailService.EnqueueMessage(emailDto);
        }

        private bool ShouldRun(JobScheduledRun scheduledRun, DateTime currentTime)
        {
            //Allow starting items, that've failed too many times, but before service restart
            if (scheduledRun.TimesFailedInRow.HasValue &&
                scheduledRun.TimesFailedInRow.Value >=
                scheduledRun.JobDefinitionDto.MaxTotalRunAttempts &&
                scheduledRun.LastFinishTime.HasValue &&
                scheduledRun.LastFinishTime.Value < _serviceStartTime
                )
            {
                return true;
            }

            //Disallow starting items, that've failed too many times
            if (scheduledRun.TimesFailedInRow.HasValue &&
                scheduledRun.TimesFailedInRow.Value >=
                scheduledRun.JobDefinitionDto.MaxTotalRunAttempts)
            {
                _logger.InfoFormat("Pipeline {0}, next job {1} execution dropped. Too many execution failed in row {2}",
                    _schedulerContext.Pipeline, scheduledRun.JobDefinitionDto.Id, scheduledRun.TimesFailedInRow.Value);

                return false;
            }

            if (scheduledRun.JobDefinitionDto.ShouldRunNow || scheduledRun.NextRunTime <= currentTime)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Check if job defintition contains existing identifier
        /// </summary>
        /// <param name="jobDefinitionDto"></param>
        private void VerifyJobDefinition([NotNull] JobDefinitionDto jobDefinitionDto)
        {
            if (jobDefinitionDto.ClassIdentifier == null)
            {
                var exception = new JobConfigurationException(jobDefinitionDto.Id);
                _logger.Fatal("AcquireJobFromDto failure", exception);
                _failureInfoManager.SetGeneralFailure(jobDefinitionDto);

                throw exception;
            }


            var jobType = IdentifierHelper.GetTypeByIdentifier(jobDefinitionDto.ClassIdentifier, false);

            if (jobType == null || jobType.GetInterface(typeof (IJob).Name) == null)
            {
                var exception = new JobConfigurationException(jobDefinitionDto.Id, jobDefinitionDto.ClassIdentifier);
                _logger.Fatal("AcquireJobFromDto failure", exception);
                _failureInfoManager.SetGeneralFailure(jobDefinitionDto);

                throw exception;
            }
        }

        /// <summary>
        /// Add required information to lastJobRunInfo, calculate nextrunon if required
        /// </summary>
        /// <param name="lastRunInfos"></param>
        /// <param name="jobDefinitionDtoDictionary"></param>
        /// <param name="currentTime"></param>
        private void FillRunInfoData(IEnumerable<LastJobRunInfoDto> lastRunInfos,
            Dictionary<long, JobDefinitionDto> jobDefinitionDtoDictionary,
            DateTime currentTime)
        {
            foreach (var lastJobRunInfoDto in lastRunInfos)
            {
                var jobDefinitionDto = jobDefinitionDtoDictionary[lastJobRunInfoDto.Id];

                _failureInfoManager.MaintainFailedExecutionCount(jobDefinitionDto.Id,
                    lastJobRunInfoDto.TimesFailedInRow ?? 0);

                lastJobRunInfoDto.MaxExecutionPeriodInSeconds = jobDefinitionDto.MaxExecutionPeriodInSeconds;
                lastJobRunInfoDto.ShouldRunNow = jobDefinitionDto.ShouldRunNow;
                lastJobRunInfoDto.IsJobEnabled = jobDefinitionDto.IsEnabled;

                if (!lastJobRunInfoDto.NextRunOn.HasValue)
                {
                    if (!lastJobRunInfoDto.FinishedOn.HasValue)
                    {
                        lastJobRunInfoDto.FinishedOn = currentTime.AddMinutes(-1);
                    }

                    lastJobRunInfoDto.NextRunOn =
                        jobDefinitionDto.CronExpression.Schedule.GetNextOccurrence(lastJobRunInfoDto.FinishedOn.Value, DateTime.MaxValue);
                }
            }
        }

        private IEnumerable<JobScheduledRun> GetJobsRunsFromExecutionList(DateTime currentTime)
        {
            var jobDefinitionDtos = _jobDefinitionService.GetJobDefinitions(_schedulerContext.Pipeline);
            var jobDefinitionDtoDictionary = jobDefinitionDtos.ToDictionary(jdd => jdd.Id);

            //no jobs found at all, return immediately
            if (!jobDefinitionDtos.Any(jd => jd.IsEnabled))
            {
                return Enumerable.Empty<JobScheduledRun>();
            }

            _failureInfoManager.MaintainFailureInfo(jobDefinitionDtos);

            _jobRunInfoService.CleanupExpiredRunInfos(_schedulerContext.Pipeline);

            var lastRunInfos = _jobRunInfoService.GetLastJobRunInfos(_schedulerContext.Pipeline);

            if (lastRunInfos.Any(lri => lri.Status == JobStatus.Starting))
            {
                return Enumerable.Empty<JobScheduledRun>();
            }

            FillRunInfoData(lastRunInfos, jobDefinitionDtoDictionary, currentTime);

            return lastRunInfos
                .Where(r => r.IsJobEnabled || r.ShouldRunNow)
                .OrderByDescending(r => r.ShouldRunNow)
                .ThenBy(r => r.NextRunOn)
                .Where(r => r.NextRunOn.HasValue)
                .Select(r => new JobScheduledRun
                {
                    NextRunTime = r.NextRunOn.Value,
                    JobDefinitionDto = jobDefinitionDtoDictionary[r.Id],
                    TimesFailedInRow = r.TimesFailedInRow
                });
        }
    }
}