﻿using System;
using System.Linq;
using Smt.Atomic.Business.Scheduling.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Scheduling;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Scheduling.Services
{
    internal class EntitySynchronizationConfigurationService : IEntitySynchronizationConfigurationService
    {
        private readonly IUnitOfWorkService<ISchedulingDbScope> _unitOfWorkService;

        public EntitySynchronizationConfigurationService(IUnitOfWorkService<ISchedulingDbScope> unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        public DateTime? GetLastSynchronizationTime(string parameterName)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var value = unitOfWork.Repositories.JobInternalParameters.FirstOrDefault(p => p.Name == parameterName)?.Value;

                DateTime result;

                if (DateTime.TryParse(value, out result))
                {
                    return result;
                }

                return null;
            }
        }

        public void SetLastSynchronizationTime(string entityName, DateTime? synchronizationTime)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var existingParameter = unitOfWork.Repositories.JobInternalParameters.FirstOrDefault(p => p.Name == entityName);

                if (existingParameter == null)
                {
                    existingParameter = new JobInternalParameter
                    {
                        Name = entityName
                    };

                    unitOfWork.Repositories.JobInternalParameters.Add(existingParameter);
                }

                existingParameter.Value = synchronizationTime.ToString();

                unitOfWork.Commit();
            }
        }
    }
}