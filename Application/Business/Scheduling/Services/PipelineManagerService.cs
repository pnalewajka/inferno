﻿using System.Collections.Generic;
using System.IO;
using Castle.Core.Logging;
using Smt.Atomic.Business.Scheduling.Interfaces;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.Business.Scheduling.Services
{
    public class PipelineManagerService : IPipelineManagerService
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly ILogger _logger;
        private readonly Dictionary<string, PipelineThread> _pipelines = new Dictionary<string, PipelineThread>();

        public PipelineManagerService(IPrincipalProvider principalProvider, ILogger logger)
        {
            _principalProvider = principalProvider;
            _logger = logger;
        }

        public void CreateAndStartPipeline(string pipelineName)
        {
            _logger.InfoFormat("Pipeline '{0}' created", pipelineName);

            if (_pipelines.ContainsKey(pipelineName))
            {
                throw new InvalidDataException($"Duplicate pipeline found: '{pipelineName}'");
            }

            var pipeline = new PipelineThread(
                () => ReflectionHelper.CreateInstanceWithIocDependencies<SchedulerService>(),
                pipelineName,
                _principalProvider.Current);

            _pipelines.Add(pipelineName, pipeline);

            pipeline.Run();
        }

        public void Stop()
        {
            _logger.Info("Stopping pipelines");

            foreach (var pipeline in _pipelines)
            {
                pipeline.Value.Stop();
            }

            foreach (var pipeline in _pipelines)
            {
                pipeline.Value.Join();
            }

            _logger.Info("All pipelines stopped");
        }

        public void Abort()
        {
            _logger.Info("Aborting pipelines");

            foreach (var pipeline in _pipelines)
            {
                pipeline.Value.Abort();
                pipeline.Value.Join();
            }

            _logger.Info("All pipelines aborted");
        }


        public int PipelineCount => _pipelines.Count;
    }
}
