﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.Business.Scheduling.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Scheduling;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Scheduling.Services
{
    public class JobLogCardIndexDataService : CardIndexDataService<JobLogDto, JobLog, ISchedulingDbScope>, IJobLogCardIndexDataService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public JobLogCardIndexDataService(ICardIndexServiceDependencies<ISchedulingDbScope> dependencies)
            : base(dependencies)
        {
        }

        public override IQueryable<JobLog> ApplyContextFiltering(IQueryable<JobLog> records, object context)
        {
            var parentIdContext = (ParentIdContext)context;
            return records.Where(jl => jl.JobDefinitionId == parentIdContext.ParentId);
        }

        protected override IEnumerable<Expression<Func<JobLog, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return j => j.Message;
        }

        protected override NamedFilters<JobLog> NamedFilters
        {
            get
            {
                return new NamedFilters<JobLog>(
                    new[]
                    {
                        new NamedFilter<JobLog, DateTime, DateTime>(JobLogFilterCodes.DateRange,
                            (log, from, to) => log.ModifiedOn >= from && log.ModifiedOn <= to)
                    }
                    .Union(CardIndexServiceHelper.BuildEnumBasedFilters<JobLog, JobLogSeverity>(jl => jl.Severity)));
            }
        }
    }
}
