﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.Business.Scheduling.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Scheduling;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Scheduling.Services
{
    internal class JobDefinitionService : IJobDefinitionService
    {
        private readonly IReadOnlyUnitOfWorkService<ISchedulingDbScope> _unitOfWorkService;
        private readonly IClassMapping<JobDefinition, JobDefinitionDto> _jobEntityToDtoMapping;

        public JobDefinitionService(
            IReadOnlyUnitOfWorkService<ISchedulingDbScope> unitOfWorkService, 
            IClassMapping<JobDefinition, JobDefinitionDto> jobEntityToDtoMapping
            )
        {
            _unitOfWorkService = unitOfWorkService;
            _jobEntityToDtoMapping = jobEntityToDtoMapping;
        }

        public List<JobDefinitionDto> GetJobDefinitions(string pipeline)
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                var jobDefintionDtos = 
                        unitOfWork
                        .Repositories
                        .JobDefinitions
                        .Where(jd => jd.Pipeline.Name == pipeline)
                        .Select(_jobEntityToDtoMapping.CreateFromSource)
                        .ToList();

                return jobDefintionDtos;
            }
        }

    }
}