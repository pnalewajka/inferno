﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.Business.Scheduling.Enums;
using Smt.Atomic.Business.Scheduling.Interfaces;
using Smt.Atomic.Business.Scheduling.Resources;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Data.Entities.Modules.Runtime;
using Smt.Atomic.Data.Entities.Modules.Scheduling;
using Smt.Atomic.Data.Repositories.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Scheduling.Services
{
    public class JobDefinitionCardIndexService : CardIndexDataService<JobDefinitionDto, JobDefinition, ISchedulingDbScope>, IJobDefinitionCardIndexService
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public JobDefinitionCardIndexService(ICardIndexServiceDependencies<ISchedulingDbScope> dependencies) : base(dependencies)
        {
        }

        public override JobDefinitionDto GetRecordById(long id)
        {
            var record = base.GetRecordById(id);

            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var jobRunInfo = LastJobRunInfo(unitOfWork, record.Id);

                FillJobRunInfoData(record, jobRunInfo);
            }

            return record;
        }

        public override CardIndexRecords<JobDefinitionDto> GetRecords(QueryCriteria criteria)
        {
            var result = base.GetRecords(criteria);
            var jobDefinitionIds = result.Rows.Select(jd => jd.Id).Distinct().ToList();

            if (jobDefinitionIds.Any())
            {
                using (var unitOfWork = UnitOfWorkService.Create())
                {
                    var jobRunIds =
                        unitOfWork.Repositories.JobRunInfos
                            .Where(jri => jobDefinitionIds.Contains(jri.JobDefinitionId))
                            .GroupBy(jri => jri.JobDefinitionId)
                            .Select(jri => jri.Max(e => e.Id))
                            .ToList();

                    var jobRunsDictionary =
                        unitOfWork.Repositories.JobRunInfos
                            .Where(jri => jobRunIds.Contains(jri.Id))
                            .ToDictionary(jri => jri.JobDefinitionId);

                    foreach (var jobDefinitionDto in result.Rows)
                    {
                        var jobRunInfo = jobRunsDictionary.ContainsKey(jobDefinitionDto.Id) ? jobRunsDictionary[jobDefinitionDto.Id] : null;

                        FillJobRunInfoData(jobDefinitionDto, jobRunInfo);
                    }
                }
            }

            return result;
        }

        public BoolResult Resurrect(long jobDefinitionId)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                unitOfWork.Repositories.JobRunInfos
                    .DeleteEach(j => j.JobDefinitionId == jobDefinitionId);

                unitOfWork.Commit();
            }

            return new BoolResult(true, new[] { new AlertDto { IsDismissable = true, Message = JobDefinitionResources.JobResurrectedText, Type = AlertType.Success } });
        }

        public BoolResult ForceRun(long jobDefinitionId)
        {
            using (var unitOfWork = UnitOfWorkService.Create())
            {
                var jobDefinition = unitOfWork.Repositories.JobDefinitions.GetById(jobDefinitionId);

                jobDefinition.ShouldRunNow = true;

                unitOfWork.Commit();
            }

            return new BoolResult(true, new[] { new AlertDto { IsDismissable = true, Message = JobDefinitionResources.JobRunForcedText, Type = AlertType.Success } });
        }

        protected override IEnumerable<Expression<Func<JobDefinition, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return j => j.Code;
            yield return j => j.Description;
        }

        private JobRunInfo LastJobRunInfo(IUnitOfWork<ISchedulingDbScope> unitOfWork, long jobDefinitionId)
        {
            return unitOfWork.Repositories.JobRunInfos
                    .OrderByDescending(jri => jri.Id)
                    .FirstOrDefault(jri => jri.JobDefinitionId == jobDefinitionId);
        }

        private void FillJobRunInfoData(JobDefinitionDto jobDefinitionDto, JobRunInfo jobRunInfo)
        {
            if (jobRunInfo != null)
            {
                jobDefinitionDto.StartedOn = jobRunInfo.StartedOn;
                jobDefinitionDto.TimesFailedInRow = jobRunInfo.TimesFailedInRow ?? 0;

                switch (jobRunInfo.Status)
                {
                    case JobStatus.Starting:
                        jobDefinitionDto.Status = JobStatusType.Running;
                        break;

                    case JobStatus.Failed:
                        jobDefinitionDto.Status = jobRunInfo.TimesFailedInRow >= jobDefinitionDto.MaxTotalRunAttempts
                            ? JobStatusType.Defunct
                            : JobStatusType.Failed;
                        break;

                    case JobStatus.Finished:
                        jobDefinitionDto.Status = JobStatusType.Healthy;
                        break;

                    default:
                        jobDefinitionDto.Status = JobStatusType.Defunct;
                        break;
                }
            }
            else
            {
                jobDefinitionDto.Status = JobStatusType.NoData;
            }
        }
    }
}
