﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Scheduling.Interfaces;

namespace Smt.Atomic.Business.Scheduling.Services
{
    public class JobExecutionContextService : IJobExecutionContextService
    {
        private readonly IDictionary<Thread, JobExecutionContext> _jobExecutionContexts = new ConcurrentDictionary<Thread, JobExecutionContext>();

        public JobExecutionContext GetContext(Thread thread)
        {
            return _jobExecutionContexts.SingleOrDefault(c => c.Key == thread).Value;
        }

        public void SetContext(Thread thread, JobExecutionContext jobExecutionContext)
        {
            _jobExecutionContexts[thread] = jobExecutionContext;
        }

        public void DeleteContext(Thread thread)
        {
            _jobExecutionContexts.Remove(thread);
        }
    }
}