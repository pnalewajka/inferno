﻿using Smt.Atomic.Data.Entities.Modules.Scheduling;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Scheduling.Dto
{
    public class JobDefinitionDtoToJobDefinitionMapping : ClassMapping<JobDefinitionDto, JobDefinition>
    {
        public JobDefinitionDtoToJobDefinitionMapping()
        {
            Mapping = d => new JobDefinition
            {
                Id = d.Id,
                Code = d.Code,
                Description = d.Description,
                ClassIdentifier = d.ClassIdentifier,
                ScheduleSettings = d.ScheduleSettings,
                LastRunOn = d.LastRunOn,
                LastSuccessOn = d.LastSuccessOn,
                LastFailureOn = d.LastFailureOn,
                IsEnabled = d.IsEnabled,
                ShouldRunNow = d.ShouldRunNow,
                PipelineId = d.PipelineId,
                MaxExecutionPeriodInSeconds = d.MaxExecutionPeriodInSeconds,
                DelayBetweenBulkRunAttemptsInSeconds = d.DelayBetweenBulkRunAttemptsInSeconds,
                MaxBulkRunAttempts = d.MaxBulkRunAttempts,
                MaxRunAttemptsInBulk = d.MaxRunAttemptsInBulk,
                Timestamp =  d.Timestamp
            };
        }
    }
}
