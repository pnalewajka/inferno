﻿using System.Linq;
using Smt.Atomic.CrossCutting.Common.SemanticTypes;
using Smt.Atomic.Data.Entities.Modules.Scheduling;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Scheduling.Dto
{
    public class JobDefinitionToJobDefinitionDtoMapping : ClassMapping<JobDefinition, JobDefinitionDto>
    {
        private readonly IClassMapping<JobDefinitionParameter, JobDefinitionParameterDto> _toDtoMapping;

        public JobDefinitionToJobDefinitionDtoMapping(IClassMapping<JobDefinitionParameter, JobDefinitionParameterDto> toDtoMapping)
        {
            _toDtoMapping = toDtoMapping;

            Mapping = e => new JobDefinitionDto
            {
                Id = e.Id,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp,
                Code = e.Code,
                Description = e.Description,
                ClassIdentifier = e.ClassIdentifier,
                ScheduleSettings = e.ScheduleSettings,
                LastRunOn = e.LastRunOn,
                LastSuccessOn = e.LastSuccessOn,
                LastFailureOn = e.LastFailureOn,
                IsEnabled = e.IsEnabled,
                ShouldRunNow = e.ShouldRunNow,
                PipelineId = e.PipelineId,
                MaxExecutionPeriodInSeconds = e.MaxExecutionPeriodInSeconds,
                CronExpression = new CronExpression(e.ScheduleSettings),
                DelayBetweenBulkRunAttemptsInSeconds = e.DelayBetweenBulkRunAttemptsInSeconds,
                MaxBulkRunAttempts = e.MaxBulkRunAttempts,
                MaxRunAttemptsInBulk = e.MaxRunAttemptsInBulk,
                Parameters =
                    (e.JobDefinitionParameters ?? Enumerable.Empty<JobDefinitionParameter>()).Select(
                        p => _toDtoMapping.CreateFromSource(p)).ToList()
            };
        }
    }
}
