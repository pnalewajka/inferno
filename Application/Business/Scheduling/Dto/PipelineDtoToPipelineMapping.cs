﻿using Smt.Atomic.Data.Entities.Modules.Scheduling;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Scheduling.Dto
{
    public class PipelineDtoToPipelineMapping : ClassMapping<PipelineDto, Pipeline>
    {
        public PipelineDtoToPipelineMapping()
        {
            Mapping = d => new Pipeline
            {
                Id = d.Id,
                Name = d.Name,
                Timestamp = d.Timestamp
            };
        }
    }
}
