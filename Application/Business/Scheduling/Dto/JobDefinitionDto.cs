﻿using System;
using System.Collections.Generic;
using Smt.Atomic.Business.Runtime.Dto;
using Smt.Atomic.Business.Scheduling.Enums;
using Smt.Atomic.CrossCutting.Common.SemanticTypes;

namespace Smt.Atomic.Business.Scheduling.Dto
{
    public class JobDefinitionDto
    {
        public long Id { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public string ClassIdentifier { get; set; }

        public string ScheduleSettings { get; set; }

        public DateTime? LastRunOn { get; set; }

        public DateTime? LastSuccessOn { get; set; }

        public DateTime? LastFailureOn { get; set; }

        public bool IsEnabled { get; set; }

        public bool ShouldRunNow { get; set; }

        public long PipelineId { get; set; }

        public int MaxExecutionPeriodInSeconds { get; set; }

        public CronExpression CronExpression { get; set; }


        public ICollection<JobRunInfoDto> JobRunInfoDtos { get; set; }

        public DateTime NextCalculatedRun { get; set; }

        public DateTime LastRunOrCurrentDateTime { get; set; }

        public int MaxRunAttemptsInBulk { get; set; }

        public int DelayBetweenBulkRunAttemptsInSeconds { get; set; }

        public int MaxBulkRunAttempts { get; set; }

        public List<JobDefinitionParameterDto> Parameters { get; set; }

        public JobStatusType Status { get; set; }

        public int TimesFailedInRow { get; set; }

        public DateTime? StartedOn { get; set; }

        public int MaxTotalRunAttempts => MaxBulkRunAttempts * MaxRunAttemptsInBulk;
    }
}
