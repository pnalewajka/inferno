﻿using System;
namespace Smt.Atomic.Business.Scheduling.Dto
{
    public class PipelineDto
    {
        public long Id { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public string Name { get; set; }
    }
}
