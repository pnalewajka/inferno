﻿using Smt.Atomic.Data.Entities.Modules.Scheduling;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Scheduling.Dto
{
    public class JobLogDtoToJobLogMapping : ClassMapping<JobLogDto, JobLog>
    {
        public JobLogDtoToJobLogMapping()
        {
            Mapping = d => new JobLog
            {
                Id = d.Id,
                Message = d.Message,
                JobDefinitionId = d.JobDefinitionId,
                SchedulerName = d.SchedulerName,
                Host = d.Host,
                Pipeline = d.Pipeline,
                Severity = d.Severity,
                Timestamp = d.Timestamp
            };
        }
    }
}