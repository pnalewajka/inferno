﻿using Smt.Atomic.Data.Entities.Modules.Scheduling;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Scheduling.Dto
{
    public class PipelineToPipelineDtoMapping : ClassMapping<Pipeline, PipelineDto>
    {
        public PipelineToPipelineDtoMapping()
        {
            Mapping = e => new PipelineDto
            {
                Id = e.Id,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp,
                Name = e.Name
            };
        }
    }
}
