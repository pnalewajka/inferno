﻿using Smt.Atomic.Data.Entities.Modules.Scheduling;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Scheduling.Dto
{
    public class JobLogToJobLogDtoMapping : ClassMapping<JobLog, JobLogDto>
    {
        public JobLogToJobLogDtoMapping()
        {
            Mapping = e => new JobLogDto
            {
                Id = e.Id,
                Message = e.Message,
                JobDefinitionId = e.JobDefinitionId,
                SchedulerName = e.SchedulerName,
                Host = e.Host,
                Pipeline = e.Pipeline,
                Severity = e.Severity,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp
            };
        }
    }
}
