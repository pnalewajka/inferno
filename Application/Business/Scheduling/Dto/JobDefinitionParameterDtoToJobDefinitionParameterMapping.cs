﻿using Smt.Atomic.Data.Entities.Modules.Scheduling;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Scheduling.Dto
{
    public class JobDefinitionParameterDtoToJobDefinitionParameterMapping : ClassMapping<JobDefinitionParameterDto, JobDefinitionParameter>
    {
        public JobDefinitionParameterDtoToJobDefinitionParameterMapping()
        {
            Mapping = d => new JobDefinitionParameter
            {
                Id = d.Id,
                JobDefinitionId = d.JobDefinitionId,
                ParameterKey = d.ParameterKey,
                ParameterValue = d.ParameterValue,
                Timestamp = d.Timestamp
            };
        }
    }
}
