﻿using Smt.Atomic.Data.Entities.Modules.Scheduling;
using Smt.Atomic.CrossCutting.Common.Abstacts;
namespace Smt.Atomic.Business.Scheduling.Dto
{
    public class JobDefinitionParameterToJobDefinitionParameterDtoMapping : ClassMapping<JobDefinitionParameter, JobDefinitionParameterDto>
    {
        public JobDefinitionParameterToJobDefinitionParameterDtoMapping()
        {
            Mapping = e => new JobDefinitionParameterDto
            {
                Id = e.Id,
                ImpersonatedById = e.ImpersonatedById,
                ModifiedById = e.ModifiedById,
                ModifiedOn = e.ModifiedOn,
                Timestamp = e.Timestamp,
                JobDefinitionId = e.JobDefinitionId,
                ParameterKey = e.ParameterKey,
                ParameterValue = e.ParameterValue
            };
        }
    }
}
