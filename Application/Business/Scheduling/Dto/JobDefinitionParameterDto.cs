﻿using System;
namespace Smt.Atomic.Business.Scheduling.Dto
{
    public class JobDefinitionParameterDto
    {
        public long Id { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }

        public long JobDefinitionId { get; set; }

        public string ParameterKey { get; set; }

        public string ParameterValue { get; set; }
    }
}
