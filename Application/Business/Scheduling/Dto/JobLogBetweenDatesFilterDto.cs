﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smt.Atomic.Business.Scheduling.Dto
{
    public class JobLogBetweenDatesFilterDto
    {
        public  DateTime? From { get; set; }

        public  DateTime? To { get; set; }
    }
}
