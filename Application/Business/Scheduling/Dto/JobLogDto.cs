﻿using System;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.Business.Scheduling.Dto
{
    public class JobLogDto
    {
        public long Id { get; set; }

        public string Message { get; set; }

        public long JobDefinitionId { get; set; }

        public string SchedulerName { get; set; }

        public string Host { get; set; }

        public string Pipeline { get; set; }

        public JobLogSeverity Severity { get; set; }

        public long? ImpersonatedById { get; set; }

        public long? ModifiedById { get; set; }

        public DateTime ModifiedOn { get; set; }

        public byte[] Timestamp { get; set; }
    }
}
