﻿using System;

namespace Smt.Atomic.Business.Scheduling.Exceptions
{
    /// <summary>
    /// Misconfigured job exception
    /// </summary>
    public class JobConfigurationException : Exception
    {
        public JobConfigurationException(long jobDefinitionId)
            : base($"Class identifier for IJob is empty for JobDefinition:{jobDefinitionId}")
        {
            
        }

        public JobConfigurationException(long jobDefinitionId, string classIdentifier)
            : base($"IJob implementation for JobDefinition:{jobDefinitionId}, identifier {classIdentifier} not found")
        {
        }
    }
}
