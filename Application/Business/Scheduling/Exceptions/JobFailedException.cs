﻿using System;

namespace Smt.Atomic.Business.Scheduling.Exceptions
{
    /// <summary>
    /// Job failed Exception
    /// </summary>
    public class JobFailedException : Exception
    {
        public JobFailedException() : base("Job didn't finish gracefully")
        {
        }
    }
}