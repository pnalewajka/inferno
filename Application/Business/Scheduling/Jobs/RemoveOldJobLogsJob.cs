﻿using System.Data.SqlClient;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Scheduling.Jobs
{
    [Identifier("Job.Db.RemoveOldJobLogs")]
    [JobDefinition(
        Code = "RemoveOldJobLogs",
        Description = "Remove old job logs",
        ScheduleSettings = "0 0 0 * * *",
        PipelineName = PipelineCodes.System,
        MaxExecutioPeriodInSeconds = 6000)]
    public class RemoveOldJobLogsJob : IJob
    {
        private readonly IUnitOfWorkService<ISchedulingDbScope> _unitOfWorkService;
        private readonly ITimeService _timeService;

        public RemoveOldJobLogsJob(
            IUnitOfWorkService<ISchedulingDbScope> unitOfWorkService,
            ITimeService timeService)
        {
            _unitOfWorkService = unitOfWorkService;
            _timeService = timeService;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            using (var unitOfWork = _unitOfWorkService.CreateExtended())
            {
                RemoveJobLogs(executionContext, unitOfWork);
                RemoveJobRunInfos(executionContext, unitOfWork);
            }

            return JobResult.Success();
        }

        private void RemoveJobLogs(JobExecutionContext executionContext, IExtendedUnitOfWork<ISchedulingDbScope> unitOfWork)
        {
            var currentDate = _timeService.GetCurrentDate();
            int affectedRows;

            do
            {
                affectedRows =
                    unitOfWork.ExecuteSqlCommand(
                        @"DELETE TOP(500) FROM [Scheduling].[JobLog] WHERE [ModifiedOn] < DATEADD(DAY, -14, @currentDate)",
                        new SqlParameter("@currentDate", currentDate));

                unitOfWork.Commit();
            }
            while (!executionContext.CancellationToken.IsCancellationRequested && affectedRows > 0);
        }

        private void RemoveJobRunInfos(JobExecutionContext executionContext, IExtendedUnitOfWork<ISchedulingDbScope> unitOfWork)
        {
            int affectedRows;

            do
            {
                affectedRows =
                    unitOfWork.ExecuteSqlCommand(
                        @"DELETE TOP(500) FROM [Runtime].[JobRunInfo]
                            WHERE [Id] NOT IN (
                                SELECT [Tmp].[Id]
                                FROM (
                                    SELECT [Id], [JobDefinitionId], ROW_NUMBER() OVER (
                                        PARTITION BY [JobDefinitionId]
                                        ORDER BY [ModifiedOn] DESC
                                    ) AS [RowNum]
                                    FROM [Runtime].[JobRunInfo]
                                ) AS [Tmp]
                                WHERE [Tmp].[RowNum] <= @jobsRunInfosToLeave
                            )",
                        new SqlParameter("@jobsRunInfosToLeave", 3));

                unitOfWork.Commit();
            }
            while (!executionContext.CancellationToken.IsCancellationRequested && affectedRows > 0);
        }
    }
}
