﻿using Castle.Core.Logging;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using System;
using System.IO;
using System.Linq;

namespace Smt.Atomic.Business.Scheduling.Jobs
{
    [Identifier("Job.ClearTemporaryDirectories")]
    [JobDefinition(
           Code = "ClearTemporaryDirectories",
           Description = "Clear temporary files and directories from file system",
           ScheduleSettings = "0 22 * * * *",
           MaxExecutioPeriodInSeconds = 1800,
           PipelineName = PipelineCodes.System)]
    public class ClearTemporaryDirectories : IJob
    {
        private readonly ILogger _logger;

        public ClearTemporaryDirectories(ILogger logger)
        {
            _logger = logger;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            ClearTemporaryDirectory(Path.GetTempPath());
            ClearTemporaryDirectory(Environment.GetEnvironmentVariable("TEMP", EnvironmentVariableTarget.Machine));

            return new JobResult
            {
                IsJobFinishedGracefully = true
            };
        }

        private void ClearTemporaryDirectory(string path)
        {
            int notDeletedItemsCount = 0;

            ClearTemporaryDirectory(path, ref notDeletedItemsCount);

            if (notDeletedItemsCount > 10000)
            {
                _logger.Error($"{notDeletedItemsCount} files or directories can not be deleted in location: {path} ");
            }
            else if (notDeletedItemsCount > 0)
            {
                _logger.Info($"{notDeletedItemsCount} files or directories can not be deleted in location: {path} ");
            }
        }

        private void ClearTemporaryDirectory(string path, ref int notDeletedItemsCount)
        {
            FileInfo[] files = null;
            DirectoryInfo directory = null;

            try
            {
                directory = new DirectoryInfo(path);
                files = directory.GetFiles();
            }
            catch { }

            foreach (var file in files ?? Enumerable.Empty<FileInfo>())
            {
                try
                {
                    file.Delete();
                }
                catch
                {
                    notDeletedItemsCount++;
                }
            }

            DirectoryInfo[] directories = null;

            if (directory != null)
            {
                try
                {
                    directories = directory.GetDirectories();
                }
                catch { }
            }

            foreach (var subDirectory in directories ?? Enumerable.Empty<DirectoryInfo>())
            {
                var directoryDeleted = false;

                try
                {
                    subDirectory.Delete(true);
                    directoryDeleted = true;
                }
                catch
                {
                    notDeletedItemsCount++;
                }

                if (!directoryDeleted)
                {
                    ClearTemporaryDirectory(subDirectory.FullName, ref notDeletedItemsCount);
                }
            }
        }
    }
}
