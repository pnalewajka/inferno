﻿using System;
using System.Data.SqlClient;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Scheduling.Jobs
{
    [Identifier("Job.Documents.RemoveOldTemporaryDocuments")]
    [JobDefinition(
        Code = "RemoveOldTemporaryDocuments",
        Description = "Remove temporary documents that are older than defined in system parameter",
        ScheduleSettings = "0 15 * * * *",
        PipelineName = PipelineCodes.System)]
    internal class RemoveOldTemporaryDocumentsJob : IJob
    {
        private readonly IUnitOfWorkService<IRuntimeDbScope> _unitOfWorkService;
        private readonly ISystemParameterService _systemParameterService;

        private const string SqlCommand = "EXEC [Runtime].[atomic_CleanUpTemporaryDocuments] @maxAgeInSeconds";

        public RemoveOldTemporaryDocumentsJob(IUnitOfWorkService<IRuntimeDbScope> unitOfWorkService, ISystemParameterService systemParameterService)
        {
            _unitOfWorkService = unitOfWorkService;
            _systemParameterService = systemParameterService;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            var temporaryDocumentTimeToLiveInSeconds =
                _systemParameterService.GetParameter<TimeSpan>(ParameterKeys.TemporaryDocumentTimeToLiveInSeconds)
                    .TotalSeconds;

            using (var unitOfWork = _unitOfWorkService.CreateExtended())
            {
                unitOfWork.ExecuteSqlCommand(SqlCommand,
                    new SqlParameter("@maxAgeInSeconds", temporaryDocumentTimeToLiveInSeconds));

                unitOfWork.Commit();
            }

            return new JobResult
            {
                IsJobFinishedGracefully = true
            };
        }
    }
}
