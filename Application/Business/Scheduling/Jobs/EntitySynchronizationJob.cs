﻿using System;
using System.Collections.Generic;
using Castle.Core.Logging;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.Business.Scheduling.Interfaces;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace Smt.Atomic.Business.Scheduling.Jobs
{
    public abstract class EntitySynchronizationJob<TExternalEntity, TEntity> : IJob
        where TExternalEntity : class
        where TEntity : class
    {
        protected abstract DateTime? Upsert(TExternalEntity externalEntity);

        protected abstract IEnumerable<TExternalEntity> GetModifiedData(DateTime? dateFrom);

        private readonly IEntitySynchronizationConfigurationService _entitySynchronizationConfigurationService;

        private readonly ILogger _logger;

        private static string ParameterPrefix => $"EntitySynchronization.{typeof(TExternalEntity).Name}.{typeof(TEntity).Name}";

        private static string LastSynchronizationTimeParameterName => $"{ParameterPrefix}.LastSynchronizationTime";

        public EntitySynchronizationJob(IEntitySynchronizationConfigurationService entitySynchronizationConfigurationService, ILogger logger)
        {
            _entitySynchronizationConfigurationService = entitySynchronizationConfigurationService;
            _logger = logger;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            try
            {
                var lastSynchronizationTime = GetLastSynchronizationTime();
                var externalEntities = GetModifiedData(lastSynchronizationTime);
                PersistData(externalEntities, executionContext);

                return JobResult.Success();
            }
            catch(Exception exception)
            {
                _logger.Error($"Synchronizing entities failed: {exception}");
                return JobResult.Fail();
            }
        }

        private void PersistData(IEnumerable<TExternalEntity> externalEntities, JobExecutionContext executionContext)
        {
            foreach (var externalEntity in externalEntities
                .TakeWhileNotCancelled(executionContext.CancellationToken)
                .TakeIfEnoughTime(executionContext.MaxExecutionTime))
            {
                var modificationData = Upsert(externalEntity);
                UpdateLastSynchronizationTime(modificationData);
            }
        }

        private DateTime? GetLastSynchronizationTime()
        {
            return _entitySynchronizationConfigurationService.GetLastSynchronizationTime(LastSynchronizationTimeParameterName);
        }

        private void UpdateLastSynchronizationTime(DateTime? time)
        {
            _entitySynchronizationConfigurationService.SetLastSynchronizationTime(LastSynchronizationTimeParameterName, time);
        }
    }
}