﻿using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;

namespace Smt.Atomic.Business.Scheduling.Jobs
{
    [Identifier("Job.Db.MaintainDbIndexesAndStatistics")]
    [JobDefinition(
        Code = "MaintainDbIndexesAndStatisticsJob",
        Description = "Maintain fragmented indexes and expired statistics",
        ScheduleSettings = "0 30 2 * * 6",
        PipelineName = PipelineCodes.System,
        MaxExecutioPeriodInSeconds = 120)]
    internal class MaintainDbIndexesAndStatisticsJob : IJob
    {
        private readonly IUnitOfWorkService<IRuntimeDbScope> _unitOfWorkService;
        private const string SqlCommand =
            "EXEC [Scheduling].[atomic_MaintainIndexAndStatistics]";

        public MaintainDbIndexesAndStatisticsJob(IUnitOfWorkService<IRuntimeDbScope> unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }

        public JobResult DoJob(JobExecutionContext executionContext)
        {
            using (var unitOfWork = _unitOfWorkService.CreateExtended())
            {
                unitOfWork.ExecuteSqlCommand(SqlCommand);

                unitOfWork.Commit();
            }

            return new JobResult { IsJobFinishedGracefully = true };
        }
    }
}