﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.OData;
using System.Web.OData.Extensions;
using Castle.Core.Internal;
using Microsoft.OData;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.WebApi.Resources;

namespace Smt.Atomic.WebApi.Controllers
{
    public class CardIndexODataController<TViewModel, TDto> : ODataController
        where TViewModel : class, new()
        where TDto : class, new()
    {
        private readonly ICardIndexDataService<TDto> _cardIndexDataServices;
        private readonly IClassMapping<TDto, TViewModel> _dtoToViewModelMapping;
        private readonly IClassMapping<TViewModel, TDto> _viewModelToDtoMapping;
        private readonly IPrincipalProvider _principalProvider;
        private DisposableQueryable<TDto> _disposableQueryable;

        public CardIndexSettings Settings { get; set; }

        public CardIndexODataController(
            ICardIndexDataService<TDto> cardIndexDataServices,
            IClassMapping<TDto, TViewModel> dtoToViewModelMapping,
            IClassMapping<TViewModel, TDto> viewModelToDtoMapping,
            IPrincipalProvider principalProvider)
        {
            _cardIndexDataServices = cardIndexDataServices;
            _dtoToViewModelMapping = dtoToViewModelMapping;
            _viewModelToDtoMapping = viewModelToDtoMapping;
            _principalProvider = principalProvider;
            Settings = new CardIndexSettings();
        }

        [EnableQuery]
        public IQueryable<TViewModel> Get(string filters = null)
        {
            try
            {
                ThrowOnMissingRole(Settings.GetRequiredRole);
                QueryCriteria criteria = null;

                if (!filters.IsNullOrEmpty())
                {
                    criteria = new QueryCriteria
                    {
                        Filters = FiltersHelper.ParseFilterString(filters)
                    };
                }

                _disposableQueryable = _cardIndexDataServices.GetQueryableRecords(criteria);

                return _disposableQueryable.Select(_dtoToViewModelMapping.Mapping);
            }
            catch (UnauthorizedException ex)
            {
                throw CreateHttpResponseException(ex);
            }
        }

        [EnableQuery]
        public SingleResult<TViewModel> Get([FromODataUri] long key)
        {
            try
            {
                ThrowOnMissingRole(Settings.GetByIdRequiredRole);
                var dto = _cardIndexDataServices.GetRecordByIdOrDefault(key);

                if (dto == null)
                {
                    return SingleResult.Create(new List<TViewModel>().AsQueryable());
                }

                var viewModel = _dtoToViewModelMapping.CreateFromSource(dto);

                return SingleResult.Create(new[] {viewModel}.AsQueryable());
            }
            catch (UnauthorizedException ex)
            {
                throw CreateHttpResponseException(ex);
            }
        }

        public HttpResponseMessage Post(TViewModel viewModel)
        {
            try
            {
                ThrowOnMissingRole(Settings.AddRequiredRole);

                if (!ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }

                var dto = _viewModelToDtoMapping.CreateFromSource(viewModel);
                AddRecordResult result = _cardIndexDataServices.AddRecord(dto);

                if (!result.IsSuccessful)
                {
                    return Request.CreateErrorResponse(
                        HttpStatusCode.BadRequest,
                        GetRecordOperationResultMessage(result));
                }

                return Request.CreateResponse(HttpStatusCode.Created, viewModel);
            }
            catch (UnauthorizedException ex)
            {
                throw CreateHttpResponseException(ex);
            }
        }

        public HttpResponseMessage Patch([FromODataUri] long key, Delta<TViewModel> deltaViewModel)
        {
            try
            {
                ThrowOnMissingRole(Settings.EditRequiredRole);

                if (!ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }

                var dto = _cardIndexDataServices.GetRecordByIdOrDefault(key);

                if (dto == null)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }

                var viewModel = _dtoToViewModelMapping.CreateFromSource(dto);
                deltaViewModel.Patch(viewModel);
                dto = _viewModelToDtoMapping.CreateFromSource(viewModel);
                EditRecordResult result = _cardIndexDataServices.EditRecord(dto);

                if (!result.IsSuccessful)
                {
                    return Request.CreateErrorResponse(
                        HttpStatusCode.BadRequest,
                        GetRecordOperationResultMessage(result));
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (UnauthorizedException ex)
            {
                throw CreateHttpResponseException(ex);
            }
        }

        public HttpResponseMessage Put([FromODataUri] long key, TViewModel viewModel)
        {
            try
            {
                ThrowOnMissingRole(Settings.EditRequiredRole);

                if (!ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }

                var dto = _viewModelToDtoMapping.CreateFromSource(viewModel);

                if (_cardIndexDataServices.GetRecordId(dto) != key)
                {
                    return Request.CreateErrorResponse(
                        HttpStatusCode.BadRequest,
                        ControllerResources.DifferentEntityIdAndRequestId);
                }

                EditRecordResult result = _cardIndexDataServices.EditRecord(dto);

                if (!result.IsSuccessful)
                {
                    return Request.CreateErrorResponse(
                        HttpStatusCode.BadRequest,
                        GetRecordOperationResultMessage(result));
                }

                return Request.CreateResponse(HttpStatusCode.OK, viewModel);
            }
            catch (UnauthorizedException ex)
            {
                throw CreateHttpResponseException(ex);
            }
        }

        public HttpResponseMessage Delete([FromODataUri] long key)
        {
            try
            {
                ThrowOnMissingRole(Settings.DeleteRequiredRole);
                DeleteRecordsResult result = _cardIndexDataServices.DeleteRecords(new[] {key});

                if (result.DeletedCount == 0)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }

                if (!result.IsSuccessful)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetBusinessResultMessage(result));
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (UnauthorizedException ex)
            {
                throw CreateHttpResponseException(ex);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (_disposableQueryable != null)
            {
                _disposableQueryable.Dispose();
            }

            base.Dispose(disposing);
        }

        private string GetRecordOperationResultMessage(RecordOperationResult result)
        {
            var builder = new StringBuilder();
            AppendRecordOperationResult(builder, result);

            return builder.ToString();
        }

        private string GetBusinessResultMessage(BusinessResult result)
        {
            var builder = new StringBuilder();
            AppendBusinessResult(builder, result);

            return builder.ToString();
        }

        private void AppendRecordOperationResult(StringBuilder builder, RecordOperationResult result)
        {
            var uniqueKeys = result.UniqueKeyViolations == null
                ? null
                : result.UniqueKeyViolations.ToList();

            if (!uniqueKeys.IsNullOrEmpty())
            {
                var violationMsg = string.Join(", ", uniqueKeys.Select(p => $"({string.Join(", ", p.PropertyNames)})"));
                builder.AppendLine(string.Format(ControllerResources.UniqueKeysViolated, violationMsg));
            }

            AppendBusinessResult(builder, result);
        }

        private void AppendBusinessResult(StringBuilder builder, BusinessResult result)
        {
            var alerts = result.Alerts;

            if (!alerts.IsNullOrEmpty())
            {
                var alertsMsg = AlertsHelper.SerializeToString(alerts);
                builder.AppendLine(alertsMsg);
            }
        }

        private Exception CreateHttpResponseException(UnauthorizedException ex = null)
        {
            return ex == null
                ? new HttpResponseException(HttpStatusCode.Unauthorized)
                : new HttpResponseException(
                    Request.CreateErrorResponse(
                        HttpStatusCode.Unauthorized,
                        new ODataError {Message = ex.Message}));
        }

        private void ThrowOnMissingRole(SecurityRoleType? requiredRole)
        {
            if (requiredRole.HasValue && !_principalProvider.Current.IsInRole(requiredRole.Value))
            {
                throw new UnauthorizedException(
                    string.Format(
                        ControllerResources.MissingRoleAuthorizationDenied,
                        requiredRole.Value));
            }
        }
    }
}
