using Castle.Core.Logging;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Results;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Presentation.Common.Attributes;

namespace Smt.Atomic.WebApi.Controllers.CustomerPortal
{
    [ApiAuthorize(SecurityRoleType.CanViewRecommendations)]
    [RoutePrefix("api/customerPortal/recommendations/{recommendationId:long}")]
    public class DocumentsController : BaseApiController
    {
        private readonly IDocumentCardIndexDataService _documentService;
        private readonly IRecommendationCardIndexDataService _recommendationService;

        public DocumentsController(
            ILogger logger,
            IUserService userService,
            IPrincipalProvider principalProvider,
            IRecommendationCardIndexDataService recommendationService,
            IDocumentCardIndexDataService documentService) 
            : base(principalProvider, userService, logger)
        {
            _documentService = documentService;
            _recommendationService = recommendationService;
        }

        // GET: api/customerPortal/recommendations/1/documents
        [HttpGet]
        [Route("documents")]
        public IHttpActionResult GetDocuments(long recommendationId)
        {
            try
            {
                ValidateAuthorization(new InputParameters(recommendationId));
                var documents = _documentService.GetDocumentsByRecommendationId(recommendationId);

                return Ok(documents);
            }
            catch (UnauthorizedAccessException uex)
            {
                Logger.Error("Unauthorized access.", uex);

                return Unauthorized();
            }
            catch (Exception ex)
            {
                var message = $"Getting recommendation documents by recommendationId: {recommendationId} failed.";
                Logger.Error(message, ex);

                return BadRequest(message);
            }
        }

        // GET: api/Documents/5
        [HttpGet]
        [Route("documents/{documentId:long}")]
        public IHttpActionResult GetDocument(long recommendationId, long documentId)
        {
            try
            {
                ValidateAuthorization(new InputParameters(recommendationId, documentId));

                var document = _documentService.GetRecordById(documentId);
                if (document?.RecommendationId == recommendationId)
                {
                    return Ok(document);
                }

                return Unauthorized();
            }
            catch (UnauthorizedAccessException uex)
            {
                Logger.Error("Unauthorized access.", uex);

                return Unauthorized();
            }
            catch (Exception ex)
            {
                var message = $"Getting document by id: {documentId} failed.";
                Logger.Error(message, ex);

                return BadRequest(message);
            }
        }

        [HttpGet]
        [Route("documents/inline/{documentId:long}")]
        public IHttpActionResult GetInlineContent(long recommendationId, long documentId)
        {
            return LoadDocument(recommendationId, documentId, "inline", "application/pdf");
        }

        [HttpGet]
        [Route("documents/attachment/{documentId:long}")]
        public IHttpActionResult GetDocumentContent(long recommendationId, long documentId)
        {
            return LoadDocument(recommendationId, documentId, "attachment", "application/octet-stream");
        }

        private IHttpActionResult LoadDocument(long recommendationId, long documentId, string dispositionType, string mediaType)
        {
            try
            {
                ValidateAuthorization(new InputParameters(recommendationId, documentId));

                var documentContent = _documentService.GetContentById(documentId);
                if (documentContent?.Content == null)
                {
                    return NotFound();
                }

                var ok = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new ByteArrayContent(documentContent.Content)
                };

                ok.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue(dispositionType) { FileName = documentContent.Name };
                ok.Content.Headers.ContentType = new MediaTypeHeaderValue(mediaType);

                return new ResponseMessageResult(ok);
            }
            catch (UnauthorizedAccessException uex)
            {
                Logger.Error("Unauthorized access.", uex);

                return Unauthorized();
            }
            catch (Exception ex)
            {
                var message = $"Getting document content by id: {documentId} failed.";
                Logger.Error(message, ex);

                return BadRequest(message);
            }
        }

        protected override void ValidateAuthorization(object state)
        {
            base.ValidateAuthorization(state);

            var inputParameters = (InputParameters) state;
            if (!this.CanGetAccess(inputParameters))
            {
                var user = GetLoggedInUser();
                Logger.Error($"User '{user.Login}' is not authorized to access recommendation id '{inputParameters.RecommendationId}'.");

                throw new UnauthorizedAccessException();
            }
        }

        private bool CanGetAccess(InputParameters inputParameters)
        {
            return inputParameters.DocumentId.HasValue && _recommendationService.CanAccesDocument(inputParameters.RecommendationId, inputParameters.DocumentId.Value);
        }

        private class InputParameters
        {
            public InputParameters(long recommendationId, long? documentId = null)
            {
                RecommendationId = recommendationId;
                DocumentId = documentId;
            }

            public long RecommendationId { get; private set; }

            public long? DocumentId { get; private set; }
        }
    }
}
