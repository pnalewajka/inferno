﻿using Castle.Core.Logging;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using System;
using System.Data.Entity.Core;
using System.Linq;
using System.Web.Http;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.WebApi.Models.ResourceForm;
using Smt.Atomic.WebApi.Models;

namespace Smt.Atomic.WebApi.Controllers.CustomerPortal
{
    [RoutePrefix("api/vacancies")]
    [AtomicAuthorize(SecurityRoleType.CanViewRecommendations)]
    public class VacanciesController : BaseApiController
    {
        private readonly INeededResourceCardIndexDataService _neededResourceService;
        private readonly IResourceFormDataService _resourceFormDataService;
        private readonly IClassMappingFactory _classMappingFactory;

        public VacanciesController(
            INeededResourceCardIndexDataService neededResourceService,
            IResourceFormDataService resourceFormDataService,
            IPrincipalProvider principalProvider,
            IUserService userService,
            IClassMappingFactory classMappingFactory,
            ILogger logger)
            : base(principalProvider, userService, logger)
        {
            _neededResourceService = neededResourceService;
            _resourceFormDataService = resourceFormDataService;
            _classMappingFactory = classMappingFactory;
        }

        [HttpGet]
        public IHttpActionResult GetVacancies()
        {
            try
            {
                ValidateAuthorization();

                var resourceFormMapping = _classMappingFactory.CreateMapping<ResourceFormDto, ResourceFormListModel>();

                var user = GetLoggedInUser();
                var dashboardItems = _neededResourceService.GetVacanciesByUserId(user.Id);
                var requests = _resourceFormDataService
                    .GetCurrentUserResources()
                    .Select(resourceFormMapping.CreateFromSource)
                    .ToList();

                return Ok(new
                {
                    Vacancies = dashboardItems.Select(v => new
                    {
                        NeededResourceName = v.Name,
                        NeededResourceId = v.Id,
                        v.RecommendationsCount,
                        v.RejectedRecommendationsCount,
                        v.InvitedRecommendationsCount,
                        v.ApprovedRecommendationsCount,
                        v.IsOnHold,
                        v.ResourceFormId
                    }),
                    Requests = requests
                });
            }
            catch (UnauthorizedAccessException uex)
            {
                Logger.Error("Unauthorized access.", uex);
                return Unauthorized();
            }
            catch (ObjectNotFoundException oex)
            {
                Logger.Error("LoggedIn user not specified.", oex);
                return NotFound();
            }
            catch (Exception ex)
            {
                Logger.Error("Getting dashboard for user failed.", ex);
                return BadRequest();
            }
        }

        [HttpPost]
        [Route("{neededResourceId}/hold")]
        public IHttpActionResult HoldRecommendations(long neededResourceId, [FromBody]NeededResourceChangeStatusModel model)
        {
            if (ModelState.IsValid)
            {
                if (_neededResourceService.PutOnHold(neededResourceId, model.Reason))
                {
                    return Ok();
                }
                else
                {
                    return NotFound();
                }
            }

            return BadRequest();
        }

        [HttpPost]
        [Route("{neededResourceId}/resume")]
        public IHttpActionResult ResumeRecommendations(long neededResourceId, [FromBody]NeededResourceChangeStatusModel model)
        {
            if (ModelState.IsValid)
            {
                _neededResourceService.Resume(neededResourceId, model.Reason);

                return Ok();
            }

            return BadRequest();
        }
    }
}