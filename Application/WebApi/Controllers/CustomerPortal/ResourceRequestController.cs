﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using Castle.Core.Internal;
using Castle.Core.Logging;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.WebApi.Models.ResourceForm;
using Smt.Atomic.WebApi.Resources;

namespace Smt.Atomic.WebApi.Controllers.CustomerPortal
{
    [RoutePrefix("api/resource")]
    [AtomicAuthorize(SecurityRoleType.CanViewRecommendations)]
    public class ResourceRequestController : BaseApiController
    {
        private readonly IClassMappingFactory _classMappingFactory;
        private readonly IResourceFormDataService _resourceService;
        private readonly IBusinessEventPublisher _businessEventPublisher;
        private readonly IDocumentDownloadService _documentDownloadService;

        public ResourceRequestController(
            IBusinessEventPublisher businessEventPublisher,
            IPrincipalProvider principalProvider,
            IResourceFormDataService resourceService,
            IClassMappingFactory classMappingFactory,
            IDocumentDownloadService documentDownloadService,
            IUserService userService,
            ILogger logger)
            : base(principalProvider, userService, logger)
        {
            _businessEventPublisher = businessEventPublisher;
            _resourceService = resourceService;
            _classMappingFactory = classMappingFactory;
            _documentDownloadService = documentDownloadService;
        }

        [HttpGet]
        [Route("languages")]
        public IHttpActionResult GetLanguageOptions()
        {
            var languageOptionDtos = _resourceService.GetLanguageOptions();
            var languages = languageOptionDtos.Select(l => new LanguageOptionModel
            {
                Id = l.Id,
                LanguageName = l.LanguageName,
                LevelName = l.LevelName
            }).ToArray();

            return Ok(languages);
        }

        [HttpGet]
        [Route("{requestId}/details")]
        public IHttpActionResult GetRequestDetails(long requestId)
        {
            var resourceRequestDto = _resourceService.GetResourceFormById(requestId);

            var languageOptionDtos = _resourceService.GetLanguageOptions();

            if (resourceRequestDto.CreatedBy == UserId)
            {
                var mapping = _classMappingFactory.CreateMapping<ResourceFormDto, ResourceRequestDraftModel>();
                var resurceRequest = mapping.CreateFromSource(resourceRequestDto);

                var languages = languageOptionDtos.Select(l => new LanguageOptionModel
                {
                    Id = l.Id,
                    LanguageName = l.LanguageName,
                    LevelName = l.LevelName
                }).ToArray();

                return OkResult(new { ResourceRequest = resurceRequest, LanguageOptions = languages });
            }

            return BadRequest();
        }

        [HttpPost]
        [Route("submit")]
        public IHttpActionResult SubmitRequest([FromBody]ResourceRequestModel model)
        {
            var mapping = _classMappingFactory.CreateMapping<ResourceRequestModel, ResourceFormDto>();
            var resourceRequest = mapping.CreateFromSource(model);
            resourceRequest.State = CrossCutting.Business.Enums.ResourceFormState.Submitted;

            RecordOperationResult result;

            if (model.Id.HasValue)
            {
                resourceRequest.Id = model.Id.Value;
                result = _resourceService.EditRecord(resourceRequest);
            }
            else
            {
                var addResult = _resourceService.AddRecord(resourceRequest);
                if (addResult.IsSuccessful)
                {
                    model.Id = addResult.AddedRecordId;
                }
                result = addResult;
            }

            if (result.IsSuccessful)
            {
                return OkResult(new { }, ResourceRequestResources.RequestSubmittedMessage);
            }

            var alertsMessage = new StringBuilder();
            result.Alerts.ForEach(a => alertsMessage.AppendLine(a.Message));

            return InternalServerError(new Exception(alertsMessage.ToString()));
        }

        [HttpPost]
        [Route("addDraft")]
        public IHttpActionResult AddDraftRequest([FromBody]ResourceRequestDraftModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var mapping = _classMappingFactory.CreateMapping<ResourceRequestDraftModel, ResourceFormDto>();
            var resourceRequest = mapping.CreateFromSource(model);
            var result = _resourceService.AddRecord(resourceRequest);

            if (result.IsSuccessful)
            {
                return OkResult(new { result.AddedRecordId }, ResourceRequestResources.RequestAddedMessage);
            }

            var alertsMessage = new StringBuilder();
            result.Alerts.ForEach(a => alertsMessage.AppendLine(a.Message));

            return InternalServerError(new Exception(alertsMessage.ToString()));
        }

        [HttpPatch]
        [Route("{requestId}/edit")]
        public IHttpActionResult EditRequest(long requestId, [FromBody]ResourceRequestDraftModel model)
        {
            var resourceRequestDto = _resourceService.GetRecordByIdOrDefault(requestId);

            if (resourceRequestDto == null)
            {
                return NotFound();
            }

            if (resourceRequestDto.CreatedBy != UserId)
            {
                return BadRequest();
            }

            resourceRequestDto.Location = model.Location;
            resourceRequestDto.PlannedStart = model.PlannedStart;
            resourceRequestDto.PositionRequirements = model.PositionRequirements;
            resourceRequestDto.ProjectDescription = model.ProjectDescription;
            resourceRequestDto.ProjectDuration = model.ProjectDuration;
            resourceRequestDto.ResourceCount = model.ResourceCount;
            resourceRequestDto.ResourceName = model.ResourceName;
            resourceRequestDto.AdditionalComment = model.AdditionalComment;
            resourceRequestDto.LanguageComment = model.LanguageComment;
            resourceRequestDto.LanguageOptions = model.Languages.Select(l => new LanguageOptionDto { Id = l });
            resourceRequestDto.RecruitmentPhases = model.RecruitmentPhases;
            resourceRequestDto.RoleDescription = model.RoleDescription;
            resourceRequestDto.Files = model.Files
                .Select(f => new Business.Common.Dto.DocumentDto
                {
                    DocumentId = f.Id,
                    TemporaryDocumentId = f.TemporaryKey,
                    DocumentName = f.Name
                }).ToArray();
            resourceRequestDto.RequestType = model.RequestType;

            var result = _resourceService.EditRecord(resourceRequestDto);

            if (result.IsSuccessful)
            {
                NotificationMessages.Add(ResourceRequestResources.RequestEditedMessage);
                return GetRequestDetails(result.EditedRecordId);
            }

            var alertsMessage = new StringBuilder();
            result.Alerts.ForEach(a => alertsMessage.AppendLine(a.Message));

            return InternalServerError(new Exception(alertsMessage.ToString()));
        }

        [HttpPost]
        [Route("{requestId}/remove")]
        public IHttpActionResult RemoveRequest(long requestId)
        {
            var businessResult = _resourceService.DeleteRecords(new[] { requestId });

            if (businessResult.IsSuccessful)
            {
                return OkResult(new { requestId }, ResourceRequestResources.RequestDeletedMessage);
            }
            else
            {
                return BadRequest("Can't remove resource request");
            }
        }

        [HttpGet]
        [Route("{id}/download")]
        public HttpResponseMessage DownloadFile(long id)
        {
            var documentDownloadDto = _documentDownloadService.GetDocument("Documents.ResourceFormFile", id);

            var response = new HttpResponseMessage(HttpStatusCode.OK);

            response.Content = new ByteArrayContent(documentDownloadDto.Content);
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(documentDownloadDto.ContentType);
            response.Content.Headers.ContentDisposition.FileName = documentDownloadDto.DocumentName;

            return response;
        }
    }
}
