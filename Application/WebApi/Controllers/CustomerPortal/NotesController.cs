﻿using Castle.Core.Internal;
using Castle.Core.Logging;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Web.Http;
using Smt.Atomic.WebApi.Models;

namespace Smt.Atomic.WebApi.Controllers.CustomerPortal
{
    [ApiAuthorize(SecurityRoleType.CanViewRecommendations)]
    [RoutePrefix("api/customerPortal/notes/{recommendationId:long}")]
    public class NotesController : BaseApiController
    {
        private readonly INoteCardIndexDataService _noteService;

        public NotesController(
            ILogger logger,
            IUserService userService,
            IPrincipalProvider principalProvider,
            INoteCardIndexDataService noteService)
            : base(principalProvider, userService, logger)
        {
            _noteService = noteService;
        }

        // GET: api/customerPortal/recommendations/1/notes
        [HttpGet]
        [Route("get")]
        public IHttpActionResult GetNotes(long recommendationId)
        {
            try
            {
                ValidateAuthorization();

                var notes = _noteService.GetNotesByRecommendationId(recommendationId).Where(n => n.UserId.HasValue).ToList();
                var users = UserService.GetUsersByIds(notes.Select(n => n.UserId.Value).ToArray());

                foreach (var note in notes)
                {
                    note.UserFullName = users.FirstOrDefault(u => u.Id == note.UserId.Value).GetFullName();
                }

                return Ok(notes);
            }
            catch (UnauthorizedAccessException uex)
            {
                Logger.Error("Unauthorized access.", uex);

                return Unauthorized();
            }
            catch (Exception ex)
            {
                var message = $"Getting recommendation notes by recommendationId: {recommendationId} failed.";
                Logger.Error(message, ex);

                return BadRequest(message);
            }
        }

        [HttpPost]
        [Route("add")]
        public IHttpActionResult AddNote([FromUri] long recommendationId, [FromBody]NewNote model)
        {
            try
            {
                ValidateAuthorization();

                var noteDto = new NoteDto
                {
                    RecommendationId = recommendationId,
                    Content = model.Content,
                    SavedInCRM = false,
                    UserId = GetLoggedInUser().Id,
                    CreatedOn = DateTime.Now
                };

                var result = _noteService.AddRecord(noteDto);
                if (result.IsSuccessful)
                {
                    return Ok();
                }

                var alertsMessage = new StringBuilder();
                result.Alerts.ForEach(a => alertsMessage.AppendLine(a.Message));

                return InternalServerError(new Exception(alertsMessage.ToString()));
            }
            catch (UnauthorizedAccessException uex)
            {
                Logger.Error("Unauthorized access.", uex);

                return Unauthorized();
            }
            catch (Exception ex)
            {
                Logger.Error($"Adding note for recommendation id {recommendationId} failed.", ex);

                return BadRequest();
            }
        }
    }
}
