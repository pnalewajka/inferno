﻿using System.Web.Http;
using Castle.Core.Logging;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.WebApi.Models.Recomendations;
using Smt.Atomic.WebApi.Resources;
using System;
using System.Web.Http.Results;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;

namespace Smt.Atomic.WebApi.Controllers.CustomerPortal
{
    [ApiAuthorize(SecurityRoleType.CanViewRecommendations)]
    [RoutePrefix("api/recommendations")]
    public class RecommendationsController : BaseApiController
    {
        private readonly IRecommendationCardIndexDataService _recommendationService;
        private readonly IRecommendationStateCardIndexDataService _recommendationStateService;
        private readonly IChallengeCardIndexDataService _challengeCardIndexDataService;

        public RecommendationsController(
            ILogger logger,
            IUserService userService,
            IPrincipalProvider principalProvider,
            IRecommendationCardIndexDataService recommendationService,
            IChallengeCardIndexDataService challengeCardIndexDataService,
            IRecommendationStateCardIndexDataService recommendationStateService)
            : base(principalProvider, userService, logger)
        {
            _recommendationService = recommendationService;
            _recommendationStateService = recommendationStateService;
            _challengeCardIndexDataService = challengeCardIndexDataService;
        }

        [HttpGet]
        [Route("details/{recommendationId:long}")]
        public IHttpActionResult GetRecomendationDetails(long recommendationId)
        {
            var recommendation = _recommendationService.GetRecommendationById(recommendationId);
            var challenge = _challengeCardIndexDataService.GetByRecommendationId(recommendationId);

            var recommendationDetails = new RecommendationDetails
            {
                Candidate = recommendation,
                Challenge = challenge
            };

            return Ok(recommendationDetails);
        }

        [HttpGet]
        [Route("{recommendationId:long}/challenge/answer/{documentId:long}")]
        public IHttpActionResult GetDocument(long recommendationId, long documentId)
        {
                var documentContent = _challengeCardIndexDataService.GetAnswerContentById(documentId);
                if (documentContent?.Content == null)
                {
                    return NotFound();
                }

                var ok = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new ByteArrayContent(documentContent.Content)
                };

                ok.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = documentContent.Name };
                ok.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                return new ResponseMessageResult(ok);

        }

        [HttpPost]
        [Route("approve")]
        public IHttpActionResult Approve([FromBody] RecommendationApproved model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            _recommendationStateService.ApproveRecommendation(model);
            var recommendationDto = _recommendationService.GetRecordById(model.RecomendationId);

            return OkResult(recommendationDto, RecommendationResources.RecommendationApprovedMessage);
        }
    }
}