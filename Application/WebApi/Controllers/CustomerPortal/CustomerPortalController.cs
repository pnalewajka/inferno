using Castle.Core.Logging;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Crm.Interfaces;
using Smt.Atomic.WebApi.Models;
using Smt.Atomic.WebApi.Models.Recomendations;
using System;
using System.Data.Entity.Core;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Results;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.WebApi.Resources;

namespace Smt.Atomic.WebApi.Controllers.CustomerPortal
{
    [ApiAuthorize(SecurityRoleType.CanViewRecommendations)]
    [RoutePrefix("api/customerPortal")]
    public class CustomerPortalController : BaseApiController
    {
        private readonly ICrmService _crmService;
        private readonly IQuestionCardIndexDataService _questionService;
        private readonly IRecommendationCardIndexDataService _recommendationService;
        private readonly IRecommendationStateCardIndexDataService _recommendationStateService;
        private readonly IRecomendationScheduleAppointmentsService _recomendationScheduleAppointmentsService;
        private readonly IClientDataCardIndexDataService _clientDataCardIndexDataService;
        private readonly IChallengeCardIndexDataService _challengeCardIndexDataService;

        public CustomerPortalController(
            ILogger logger,
            ICrmService crmService,
            IUserService userService,
            IPrincipalProvider principalProvider,
            IQuestionCardIndexDataService questionService,
            IChallengeCardIndexDataService challengeCardIndexDataService,
            IClientDataCardIndexDataService clientDataCardIndexDataService,
            IRecommendationCardIndexDataService recommendationService,
            IRecommendationStateCardIndexDataService recommendationStateService,
            IRecomendationScheduleAppointmentsService recomendationScheduleAppointmentsService)
            : base(principalProvider, userService, logger)
        {
            _crmService = crmService;
            _questionService = questionService;
            _challengeCardIndexDataService = challengeCardIndexDataService;
            _recommendationService = recommendationService;
            _recommendationStateService = recommendationStateService;
            _clientDataCardIndexDataService = clientDataCardIndexDataService;
            _recomendationScheduleAppointmentsService = recomendationScheduleAppointmentsService;
        }

        [HttpGet]
        [Route("userInfo")]
        public IHttpActionResult UserInfo()
        {
            try
            {
                ValidateAuthorization();
                var user = GetLoggedInUser();
                var userInfo = _crmService.GetClientContactInfo(new Guid(user.CrmId));
                return Ok(userInfo);
            }
            catch (UnauthorizedAccessException uex)
            {
                Logger.Error("Unauthorized access.", uex);
                return Unauthorized();
            }
            catch (ObjectNotFoundException oex)
            {
                Logger.Error("LoggedIn user not specified.", oex);
                return NotFound();
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat(ex, "Getting info for user {0} failed.", User.Identity.Name);
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("recommendations")]
        public IHttpActionResult Recommendations(long? neededResourceId)
        {
            try
            {
                ValidateAuthorization();
                var user = GetLoggedInUser();
                var recommendations = _recommendationService.GetRecords(user.Id, neededResourceId);
                return Ok(recommendations);
            }
            catch (UnauthorizedAccessException uex)
            {
                Logger.Error("Unauthorized access.", uex);
                return Unauthorized();
            }
            catch (ObjectNotFoundException oex)
            {
                Logger.Error("LoggedIn user not specified.", oex);
                return NotFound();
            }
            catch (Exception ex)
            {
                Logger.Error("Getting recommendations for user failed.", ex);
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("user/contactPerson")]
        public IHttpActionResult GetUserContactPerson()
        {
            ValidateAuthorization();
            var user = GetLoggedInUser();
            var operationManager = _clientDataCardIndexDataService.GetAccountManagerForClient(user.Id);

            return Ok(new ContactInfoViewModel
            {
                Email = operationManager.Email,
                Name = operationManager.Name,
                Phone = operationManager.Phone
            });
        }

        [HttpPost]
        [Route("recommendations/{recommendationId:long}/reject")]
        public IHttpActionResult ChangeStatus([FromUri] long recommendationId, [FromBody] ChangeRecommendationStatusRequest request)
        {
            var result = _recommendationService.RejectRecommendation(recommendationId, request.Note);

            return OkResult(result, RecommendationResources.RecommendationRejectedMessage);
        }

        [HttpPost]
        [Route("recommendations/nextStep")]
        public IHttpActionResult ScheduleNextStep([FromBody] RecommendationSchedule model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var recommendation = _recommendationService.GetRecordById(model.RecomendationId);

            if (recommendation == null)
            {
                return NotFound();
            }

            _recommendationStateService.ScheduleRecomendationNextStep(model);

            return Ok(recommendation);
        }

        [HttpPost]
        [Route("recommendations/challenge")]
        public IHttpActionResult ChallengeNextStep([FromBody] RecommendationChallenge model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var recommendation = _recommendationService.GetRecordById(model.RecomendationId);

            if (recommendation == null)
            {
                return NotFound();
            }

            _recommendationStateService.ScheduleChallengeNextStep(model);
            
            return Ok(recommendation);
        }

        [HttpGet]
        [Route("recommendations/appointments")]
        public IHttpActionResult GetScheduleAppointments()
        {
            var appointments = _recomendationScheduleAppointmentsService.GetAppointments()
                .Select(a => new ScheduleAppointmentModel
                {
                    Id = a.Id,
                    RecomendationId = a.RecomendationId,
                    RecomendationName = a.RecomendationName,
                    NeededResourceName = a.NeededResourceName,
                    AppointmentDate = a.AppointmentDate,
                    AppointmentEndDate = a.AppointmentEndDate,
                    AppointmentStatus = a.AppointmentStatus,
                    RecommendationId = a.RecommendationId
                }).ToList();

            return Ok(new
            {
                Appointments = appointments
            });
        }

        [HttpGet]
        [Route("calendar/appointments")]
        public IHttpActionResult GetMyScheduleAppointments()
        {
            var appointments = _recomendationScheduleAppointmentsService.GetAppointments();

            return Ok(new
            {
                Appointments = appointments
            });
        }

        [HttpPost]
        [Route("recommendations/{recommendationId:long}/question")]
        public IHttpActionResult AskAQuestion([FromUri] long recommendationId, [FromBody] RecommendationQuestionRequest request)
        {
            IHttpActionResult result;

            try
            {
                if (TryValidateAuthorizedAccess(out result) &&
                    TryValidateOwnershipOfRecommendation(recommendationId, out result) &&
                    TryValidateRecommendationQuestionRequest(request, out result))
                {
                    result = StoreQuestion(recommendationId, request.Question);
                }
            }
            catch (ObjectNotFoundException oex)
            {
                Logger.Error("LoggedIn user not specified.", oex);
                result = NotFound();
            }
            catch (Exception ex)
            {
                Logger.Error($"Ask a question for recommendation id {recommendationId} failed.", ex);
                result = BadRequest();
            }

            return result;
        }

        private bool TryValidateOwnershipOfRecommendation(long recommendationId, out IHttpActionResult result)
        {
            result = Ok();

            var recommendation = _recommendationService.GetRecordById(recommendationId);

            if (UserId != recommendation.UserId)
            {
                Logger.Error("LoggedIn user doesn't have permission to access this recommendation.");
                result = Unauthorized();
            }

            return result is OkResult;
        }

        private bool TryValidateRecommendationQuestionRequest(RecommendationQuestionRequest request, out IHttpActionResult result)
        {
            result = Ok();

            if (request == null)
            {
                Logger.Error($"Request {nameof(request)} not found.", new ArgumentNullException(nameof(request)));
                result = NotFound();
            }
            else if (string.IsNullOrWhiteSpace(request.Question))
            {
                Logger.Error($"Requested parameter {nameof(request.Question)} not found.", new ArgumentException(nameof(request.Question)));
                result = NotFound();
            }

            return result is OkResult;
        }

        private IHttpActionResult StoreQuestion(long recommendationId, string question)
        {
            IHttpActionResult result = Ok();
            var questionDto = new QuestionDto { UserId = UserId.GetValueOrDefault(), RecommendationId = recommendationId, Query = question, QueryDate = DateTime.Now };
            var dbResult = _questionService.AddRecord(questionDto);

            if (!dbResult.IsSuccessful)
            {
                var alertsMessage = string.Join(Environment.NewLine, dbResult.Alerts.Select(a => a.Message));
                result = InternalServerError(new Exception(alertsMessage));
            }

            return result;
        }
    }
}
