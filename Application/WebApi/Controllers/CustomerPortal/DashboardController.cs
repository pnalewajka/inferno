﻿using System.Web.Http;
using Castle.Core.Logging;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;

namespace Smt.Atomic.WebApi.Controllers.CustomerPortal
{
    [RoutePrefix("api/dashboard")]
    [ApiAuthorize(SecurityRoleType.CanViewRecommendations)]
    public class DashboardController : BaseApiController
    {
        private readonly INeededResourceCardIndexDataService _neededResourceService;
        private readonly IRecomendationScheduleAppointmentsService _recomendationScheduleAppointmentsService;

        public DashboardController(
            INeededResourceCardIndexDataService neededResourceService,
            IRecomendationScheduleAppointmentsService recomendationScheduleAppointmentsService,
            IPrincipalProvider principalProvider,
            IUserService userService,
            ILogger logger)
            : base(principalProvider, userService, logger)
        {
            _neededResourceService = neededResourceService;
            _recomendationScheduleAppointmentsService = recomendationScheduleAppointmentsService;
        }

        [HttpGet]
        public IHttpActionResult GetDashboard()
        {
            ValidateAuthorization();

            var dashboardItems = _neededResourceService.GetActivityLogs();
            var appointments = _recomendationScheduleAppointmentsService.GetAppointments();
            var challenges = _recomendationScheduleAppointmentsService.GetChallenges();

            return Ok(new
            {
                DashboardItems = dashboardItems,
                Appointments = appointments,
                Challenges = challenges
            });
        }

        [HttpPost]
        [Route("discardActivity")]
        public void DiscardActivity(long? activityId)
        {
            if (activityId.HasValue)
            {
                _neededResourceService.DiscardActivity(activityId.Value);
            }
        }
    }
}