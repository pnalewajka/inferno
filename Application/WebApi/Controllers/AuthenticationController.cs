﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.WebApi.Models;
using Smt.Atomic.WebApi.Resources;
using Smt.Atomic.CrossCutting.Security.Principal;
using Castle.Core.Logging;

namespace Smt.Atomic.WebApi.Controllers
{
    [ApiAuthorize]
    public class AuthenticationController : BaseApiController
    {
        private readonly ISecurityService _securityService;

        public AuthenticationController(
            ILogger logger,
            IUserService userService,
            IPrincipalProvider principalProvider,
            ISecurityService securityService)
            : base(principalProvider, userService, logger)
        {
            _securityService = securityService;
        }

        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult IsAuthenticated()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = GetLoggedInUser();
                return OkResult(new UserInfoViewModel
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Email = user.Email
                });
            }
            else
            {
                return Ok();
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("api/authentication/login")]
        public IHttpActionResult Login(LoginRequest model)
        {
            if (ModelState.IsValid)
            {
                var validationResult = _securityService.ValidateUser(model.Login, model.Password);

                if (validationResult.IsUserAuthenticated)
                {
                    HandleSuccessfulAuthentication(validationResult, model.RememberMe);
                    var user = UserService.GetUserById(validationResult.UserId);

                    return OkResult(new UserInfoViewModel
                    {
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Email = user.Email
                    });
                }

                return Unauthorized();
            }

            return BadRequest();
        }

        [AllowAnonymous]
        [HttpPost]
        public HttpResponseMessage LogOut()
        {
            if (User.Identity.IsAuthenticated)
            {
                //FormsAuthentication.SignOut();
                // NOTE: FormsAuthentication.SignOut() cannot be used as it doesn't allow to specify cookie' path

                var authenticationCookie = new HttpCookie(FormsAuthentication.FormsCookieName, string.Empty)
                {
                    Expires = new System.DateTime(1970, 1, 1),
                    HttpOnly = true,
#if (PROD || STAGE)
                    Secure = true,
#endif
                    Path = "/"
                };

                HttpContext.Current.Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
                HttpContext.Current.Response.Cookies.Add(authenticationCookie);

                var successfullResult = new ApiAuthenticationResponse
                {
                    Status = HttpStatusCode.OK,
                    Message = Authentication.LoggedOutSuccessfullyMessage
                };

                return Request.CreateResponse(HttpStatusCode.OK, successfullResult);
            }

            var nonAuthorizedResult = new ApiAuthenticationResponse
            {
                Status = HttpStatusCode.Unauthorized,
                Message = Authentication.NotAuthenticatedMessage
            };

            return Request.CreateResponse(HttpStatusCode.Unauthorized, nonAuthorizedResult);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("api/authentication/changepassword")]
        public HttpResponseMessage ChangePassword(ChangePasswordRequest model)
        {
            if (ModelState.IsValid)
            {
                var validationResult = _securityService.ValidateUser(model.Token);

                if (validationResult.IsUserAuthenticated)
                {
                    var result = _securityService.SetPassword(validationResult.Login, model.Password);

                    var response = new ChangePasswordResponse
                    {
                        Status = HttpStatusCode.OK,
                        Result = result.IsSuccessful,
                        Login = result.IsSuccessful ? validationResult.Login : string.Empty
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, response);
                }

                var unauthorizedResult = new ApiAuthenticationResponse
                {
                    Status = HttpStatusCode.Unauthorized,
                    Message = Authentication.AuthenticationFailedMessage,
                    Errors = validationResult.Alerts.Select(a => a.Message).ToArray()
                };

                return Request.CreateResponse(HttpStatusCode.Unauthorized, unauthorizedResult);
            }

            var badRequestResult = new ApiAuthenticationResponse
            {
                Status = HttpStatusCode.BadRequest,
                Message = Authentication.WrongData,
                Errors = ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage).ToArray()
            };

            return Request.CreateResponse(HttpStatusCode.BadRequest, badRequestResult);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("api/authentication/istokenvalid")]
        public HttpResponseMessage IsTokenValid(IsTokenValidRequest model)
        {
            if (!ModelState.IsValid)
            {
                var badRequestResult = new ApiAuthenticationResponse
                {
                    Status = HttpStatusCode.BadRequest,
                    Message = Authentication.WrongData
                };

                return Request.CreateResponse(HttpStatusCode.BadRequest, badRequestResult);
            }

            var result = _securityService.IsTokenValid(model.Token);

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("api/authentication/resendActivationEmail")]
        public async Task<HttpResponseMessage> ResendActivationEmail(ResendActivationEmailRequest model)
        {
            if (!ModelState.IsValid)
            {
                var badRequestResult = new ApiAuthenticationResponse
                {
                    Status = HttpStatusCode.BadRequest,
                    Message = Authentication.WrongData
                };

                return Request.CreateResponse(HttpStatusCode.BadRequest, badRequestResult);
            }

            var result = await UserService.ResendActivationEmailAsync(model.Email);

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        private void HandleSuccessfulAuthentication(UserValidationResult validationResult, bool rememberMe)
        {
            var cookie = AuthenticationHelper.CreateAuthenticationCookie(validationResult.UserId, validationResult.Login, rememberMe);
            cookie.Path = "/";
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        private async Task<HttpResponseMessage> CreateFailedResponse(LoginRequest model, UserValidationResult validationResult)
        {
            if (!validationResult.IsPasswordExpired)
            {
                return CreateUnauthorizedResponse(validationResult);
            }

            var user = UserService.GetUserByLogin(model.Login);
            var canResendEmail = await UserService.CanActivationEmailBeResendAsync(user.Email);

            if (!canResendEmail)
            {
                return CreateUnauthorizedResponse(validationResult);
            }

            var result = await UserService.ResendActivationEmailAsync(user.Email);

            return result.IsSuccessful
                ? CreateForbiddenResponse(validationResult)
                : CreateUnauthorizedResponse(validationResult);
        }

        private HttpResponseMessage CreateUnauthorizedResponse(UserValidationResult validationResult)
        {
            var unauthorizedResult = new ApiAuthenticationResponse
            {
                Status = HttpStatusCode.Unauthorized,
                Message = Authentication.AuthenticationFailedMessage,
                Errors = validationResult.Alerts.Select(a => a.Message).ToArray()
            };

            return Request.CreateResponse(HttpStatusCode.Unauthorized, unauthorizedResult);
        }

        private HttpResponseMessage CreateForbiddenResponse(UserValidationResult validationResult)
        {
            var expiredPasswordResult = new ApiAuthenticationResponse
            {
                Status = HttpStatusCode.Forbidden,
                Message = Authentication.PasswordExpiredMessage,
                Errors = validationResult.Alerts.Select(a => a.Message).ToArray()
            };

            return Request.CreateResponse(HttpStatusCode.Forbidden, expiredPasswordResult);
        }
    }
}