﻿using Smt.Atomic.CrossCutting.Common.Enums;

namespace Smt.Atomic.WebApi.Controllers
{
    public class CardIndexSettings
    {
        public SecurityRoleType? GetRequiredRole { get; set; }

        public SecurityRoleType? GetByIdRequiredRole { get; set; }

        public SecurityRoleType? AddRequiredRole { get; set; }

        public SecurityRoleType? EditRequiredRole { get; set; }

        public SecurityRoleType? DeleteRequiredRole { get; set; }
    }
}