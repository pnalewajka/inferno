﻿using System.Linq;
using System.Web.Http;
using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.WebApp.Areas.Organization.Models;

namespace Smt.Atomic.WebApi.Controllers.Organization
{
    [ApiAuthorize(SecurityRoleType.CanViewOrgUnits)]
    [RoutePrefix("api/organization/orgUnit")]
    public class OrgUnitController : CardIndexApiController<OrgUnitViewModel, OrgUnitDto>
    {
        private readonly IOrgUnitCardIndexDataService _cardIndexDataService;
        private readonly IClassMapping<OrgUnitDto, OrgUnitViewModel> _dtoToViewModelMapping;

        public OrgUnitController(
            IOrgUnitCardIndexDataService cardIndexDataServices,
            IClassMapping<OrgUnitDto, OrgUnitViewModel> dtoToViewModelMapping,
            IClassMapping<OrgUnitViewModel, OrgUnitDto> viewModelToDtoMapping,
            IPrincipalProvider principalProvider)
            : base(cardIndexDataServices, dtoToViewModelMapping, viewModelToDtoMapping, principalProvider)
        {
            _cardIndexDataService = cardIndexDataServices;
            _dtoToViewModelMapping = dtoToViewModelMapping;
            Settings.AddRequiredRole = SecurityRoleType.CanAddOrgUnits;
            Settings.EditRequiredRole = SecurityRoleType.CanEditOrgUnits;
            Settings.DeleteRequiredRole = SecurityRoleType.CanDeleteOrgUnits;
        }

        [HttpGet]
        [Route("")]
        public IHttpActionResult Get()
        {
            var viewModel = _cardIndexDataService.GetQueryableRecords().Select(_dtoToViewModelMapping.CreateFromSource);

            return Ok(viewModel);
        }

    }
}