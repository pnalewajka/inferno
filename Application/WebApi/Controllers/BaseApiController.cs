﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Results;
using Castle.Core.Logging;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Interfaces;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.WebApi.Models;

namespace Smt.Atomic.WebApi.Controllers
{
    public abstract class BaseApiController : ApiController
    {
        protected readonly ILogger Logger;
        protected readonly IUserService UserService;
        protected readonly IPrincipalProvider PrincipalProvider;

        protected BaseApiController(IPrincipalProvider principalProvider, IUserService userService, ILogger logger)
        {
            Logger = logger;
            UserService = userService;
            PrincipalProvider = principalProvider;
        }

        protected IHttpActionResult OkResult<T>(T result, BusinessResult businessResult)
        {
            var messages =
                businessResult.Alerts.Where(m => m.Type == AlertType.Information || m.Type == AlertType.Success);

            var errors =
                businessResult.Alerts.Where(m => m.Type == AlertType.Error || m.Type == AlertType.Warning);

            return OkResult(result, string.Join(", ", messages.Select(m => m.Message)), string.Join(", ", errors.Select(m => m.Message)));
        }

        protected IHttpActionResult OkResult(BusinessResult businessResult)
        {
            return OkResult((object)null, businessResult);
        }

        protected List<string> NotificationMessages = new List<string>();

        protected IHttpActionResult OkResult<T>(T result, string message = null, string warning = null)
        {
            var alerts = NotificationMessages;
            var successMessage = message ?? string.Join(", ", NotificationMessages);
            var warningMessage = warning;

            var response = new ApiResponse<T>
            {
                Payload = result,
                Message = successMessage,
                Warning = warningMessage
            };
            return Ok(response);
        }

        protected long? UserId => PrincipalProvider.Current.Id;

        protected virtual void ValidateAuthorization(object state = null)
        {
            if (!User.Identity.IsAuthenticated || !PrincipalProvider.IsSet)
            {
                throw new UnauthorizedAccessException();
            }

            long? userId = UserId;
            if (!userId.HasValue)
            {
                throw new UnauthorizedException();
            }
        }

        protected virtual bool TryValidateAuthorizedAccess(out IHttpActionResult result)
        {
            result = Ok();

            if (!User.Identity.IsAuthenticated || !PrincipalProvider.IsSet)
            {
                Logger.Error("Unauthorized access.", new UnauthorizedAccessException());
                result = Unauthorized();
            }

            long? userId = UserId;
            if (!userId.HasValue)
            {
                Logger.Error("Unauthorized access.", new UnauthorizedException());
                result = Unauthorized();
            }

            return result is OkResult;
        }

        protected virtual UserDto GetLoggedInUser()
        {
            var userId = UserId.GetValueOrDefault();
            var user = UserService.GetUserById(userId);
            if (user == null)
            {
                throw new ObjectNotFoundException();
            }

            return user;
        }
    }
}