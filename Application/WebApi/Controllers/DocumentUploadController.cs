﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Newtonsoft.Json;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.Business.CustomerPortal.Interfaces;
using Smt.Atomic.WebApi.Models;

namespace Smt.Atomic.WebApi.Controllers
{


    [RoutePrefix("api/upload")]
    public class DocumentUploadController : ApiController
    {
        private readonly ITemporaryDocumentPromotionService _temporaryDocumentService;

        private class DocumentFileDto
        {
            public string FileName { get; set; }

            public string ContentType { get; set; }

            public byte[] Content { get; set; }
        }

        private readonly IChallengeFileService _challengeFileService;

        public DocumentUploadController(IChallengeFileService challengeFileService, ITemporaryDocumentPromotionService temporaryDocumentService)
        {
            _challengeFileService = challengeFileService;
            _temporaryDocumentService = temporaryDocumentService;
        }

        private async Task<IReadOnlyCollection<DocumentFileDto>> GetFiles()
        {
            var request = Request;

            if (!request.Content.IsMimeMultipartContent())
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            var provider = new MultipartMemoryStreamProvider();
            var fileDtos = new List<DocumentFileDto>();

            await request.Content.ReadAsMultipartAsync(provider).ContinueWith(o =>
            {
                foreach (var ctnt in o.Result.Contents)
                {
                    var byteArray = ctnt.ReadAsByteArrayAsync().Result;
                    fileDtos.Add(new DocumentFileDto
                    {
                        Content = byteArray,
                        FileName = ctnt.Headers.ContentDisposition?.FileName,
                        ContentType = ctnt.Headers.ContentType.MediaType
                    });
                }
            });

            return fileDtos;
        }

        public async Task<HttpResponseMessage> UploadDocument()
        {
            var fileDtos = await GetFiles();

            var identifiers = new List<Guid>();

            foreach (var challengeFileDto in fileDtos.Select(f => new ChallengeFileDto { Data = f.Content, Name = f.FileName, ContentType = f.ContentType }))
            {
                var identifier = _challengeFileService.AddTemporaryChallengeFile(challengeFileDto);
                identifiers.Add(identifier);
            }

            return new HttpResponseMessage
            {
                Content = new StringContent(string.Join(",", identifiers))
            };
        }

        [HttpPost]
        [Route("begin")]
        public object BeginUpload([FromBody]NewDocumentViewModel model)
        {
            var result = new TemporaryDocumentIdentifier();

            _temporaryDocumentService.CreateTemporaryDocument(model.FileName, model.ContentType,
                    model.ContentLength, result.Identifier);

            return result;
        }

        [HttpPost]
        [Route("chunk")]
        public async Task<HttpResponseMessage> UploadChunk(Guid temporaryDocumentId)
        {
            var fileDtos = await GetFiles();

            foreach (var fileDto in fileDtos)
            {
                using (var stream = new MemoryStream(fileDto.Content))
                {
                    _temporaryDocumentService.AddChunk(temporaryDocumentId, stream);
                }
            }

            return new HttpResponseMessage
            {
                Content = new StringContent(temporaryDocumentId.ToString())
            };
        }

        private class TemporaryDocumentIdentifier
        {
            public TemporaryDocumentIdentifier()
            {
                Identifier = Guid.NewGuid();
            }

            [JsonProperty("temporaryDocumentId")]
            public Guid Identifier { get; private set; }
        }

        private class DocumentDescriptor
        {
            [JsonProperty("documentName")]
            public string DocumentName { get; set; }

            [JsonProperty("temporaryDocumentId")]
            public Guid TemporaryDocumentId { get; set; }

            [JsonProperty("contentType")]
            public string ContentType { get; set; }

            [JsonProperty("uploadFailed")]
            public bool UploadFailed { get; set; }

            [JsonProperty("wasDocumentCompleted")]
            public bool WasDocumentCompleted { get; set; }

            public static readonly DocumentDescriptor UploadFailedResult = new DocumentDescriptor { UploadFailed = true };
        }
    }
}
