﻿using System.Web.Http;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.WebApi.Controllers
{
    public class CardIndexApiController<TViewModel, TDto> : ApiController
        where TViewModel : class, new()
        where TDto : class, new()
    {
        private readonly ICardIndexDataService<TDto> _cardIndexDataServices;
        private readonly IClassMapping<TDto, TViewModel> _dtoToViewModelMapping;
        private readonly IClassMapping<TViewModel, TDto> _viewModelToDtoMapping;
        private readonly IPrincipalProvider _principalProvider;

        public CardIndexSettings Settings { get; set; }

        public CardIndexApiController(
            ICardIndexDataService<TDto> cardIndexDataServices,
            IClassMapping<TDto, TViewModel> dtoToViewModelMapping,
            IClassMapping<TViewModel, TDto> viewModelToDtoMapping,
            IPrincipalProvider principalProvider)
        {
            _cardIndexDataServices = cardIndexDataServices;
            _dtoToViewModelMapping = dtoToViewModelMapping;
            _viewModelToDtoMapping = viewModelToDtoMapping;
            _principalProvider = principalProvider;
            Settings = new CardIndexSettings();
        }
    }
}