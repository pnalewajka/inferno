﻿using System;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Http.Cors;
using Castle.Windsor;
using Newtonsoft.Json;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.CrossCutting.Security.Services;
using Smt.Atomic.Presentation.Common;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.WebApi.Windsor;
using Swank;

namespace Smt.Atomic.WebApi
{
    public class WebApiApplication : HttpApplication
    {
        private static RequestAuthenticationHandler _requestAuthenticationHandler;
        private static WindsorContainer _container;

        protected void Application_Start()
        {
            var containerConfiguration = new WebApiContainerConfiguration();
            _container = containerConfiguration.Configure(ContainerType.WebApi);

            _requestAuthenticationHandler = new RequestAuthenticationHandler(_container);
            GlobalConfiguration.Configuration.DependencyResolver = new WindsorDependencyResolver(_container.Kernel);

            GlobalConfiguration.Configure(config =>
            {
                config.MapHttpAttributeRoutes();
                ODataConfig.Register(config);
                ConfigureCors(config);
                config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new { id = RouteParameter.Optional });
                ConfigureDateFormatter(config);
                ConfigureSwank(config);
            });
           
            GlobalConfiguration.Configuration.EnsureInitialized();
        }

        protected void Application_BeginRequest()
        {
            var currentContext = _container.Resolve<HttpCurrentContextDataService>();
            currentContext.SetNewRequestId();

            if (ActivityTrackingHelper.GetActivityIdFromActivityTrackingCookie(currentContext.Request) == null)
            {
                var cookie = ActivityTrackingHelper.CreateActivityTrackingCookie();
                currentContext.Request.Cookies.Add(cookie);
                currentContext.Response.Cookies.Add(cookie);
            }
        }

        private void ConfigureDateFormatter(HttpConfiguration config)
        {
            config.Formatters.JsonFormatter.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
        }

        private void ConfigureSwank(HttpConfiguration config)
        {
#if DEV || STAGE
            config.Swank(x => x.WithHeader("Dante API").WithAppAt("doc"));
#endif
        }

        private static void ConfigureCors(HttpConfiguration config)
        {
            var cors = new EnableCorsAttribute(
                WebConfigurationManager.AppSettings["Cors.Origins"],
                WebConfigurationManager.AppSettings["Cors.Headers"],
                WebConfigurationManager.AppSettings["Cors.Methods"]);

            bool supportsCredentials;
            bool.TryParse(WebConfigurationManager.AppSettings["Cors.SupportsCredentials"],
                out supportsCredentials);
            cors.SupportsCredentials = supportsCredentials;

            config.EnableCors(cors);
        }

        private void Application_PostAuthenticateRequest(object sender, EventArgs eventArgs)
        {
            _requestAuthenticationHandler.HandlePostAuthenticateRequest(this);
        }
    }
}