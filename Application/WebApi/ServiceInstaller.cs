﻿using System.Web.Http.Controllers;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.WebApp.Areas.Organization.Models;

namespace Smt.Atomic.WebApi
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            container.Register
            (
                Classes.FromThisAssembly().BasedOn<IHttpController>().LifestyleScoped(),
                Component.For<IClassMapping<OrgUnitViewModel, OrgUnitDto>>().ImplementedBy<OrgUnitViewModelToOrgUnitDtoMapping>().LifestyleSingleton(),
                Component.For<IClassMapping<OrgUnitDto, OrgUnitViewModel>>().ImplementedBy<OrgUnitDtoToOrgUnitViewModelMapping>().LifestyleSingleton()
            );
        }
    }
}