﻿using System.Web.Http;
using System.Web.OData.Builder;
using System.Web.OData.Extensions;
using Smt.Atomic.WebApi.Models.LittlePony;
using Smt.Atomic.WebApp.Areas.Organization.Models;

namespace Smt.Atomic.WebApi
{
    public class ODataConfig
    {
        public static void Register(HttpConfiguration config)
        {
            RegisterModel(config);
        }

        private static void RegisterModel(HttpConfiguration config)
        {
            //ODataConventionModelBuilder builder = new ODataConventionModelBuilder();

            //builder.EntitySet<UserViewModel>("User");
            //builder.EntitySet<OrgUnitViewModel>("OrgUnit");
            //// More view models here

            //config.MapODataServiceRoute("ODataRoute", "odata", builder.GetEdmModel());
        }
    }
}