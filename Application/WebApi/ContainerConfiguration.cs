﻿using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.WebApi
{
    public class WebApiContainerConfiguration : ContainerConfiguration
    {
        protected override IEnumerable<AtomicInstaller> GetInstallers(ContainerType containerType)
        {
            yield return new Business.Accounts.ServiceInstaller();
            yield return new Business.Configuration.ServiceInstaller();
            yield return new Business.Common.ServiceInstaller();
            yield return new Business.Notifications.ServiceInstaller();
            yield return new Business.Organization.ServiceInstaller();
            yield return new Data.Crm.ServiceInstaller();
            yield return new Data.SharePoint.ServiceInstaller();
            yield return new Data.Entities.ServiceInstaller();
            yield return new Data.Repositories.ServiceInstaller();
            yield return new CrossCutting.Common.ServiceInstaller();
            yield return new CrossCutting.Settings.ServiceInstaller();
            yield return new CrossCutting.Security.ServiceInstaller();
            yield return new Business.EventSourcing.ServiceInstaller();
            yield return new Business.CustomerPortal.ServiceInstaller();

            yield return new ServiceInstaller();
        }
    }
}