﻿namespace Smt.Atomic.WebApi.Models
{
    public class NewDocumentViewModel
    {
        public string FileName { get; set; }

        public string ContentType { get; set; }

        public long ContentLength { get; set; }
    }
}