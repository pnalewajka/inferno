﻿using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApi.Models
{
    public class ChangePasswordRequest
    {
        [Required]
        public string Token { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}