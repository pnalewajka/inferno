﻿namespace Smt.Atomic.WebApi.Models
{
    public class ApiResponse<T>
    {
        public T Payload { get; set; }
        public string Message { get; set; }
        public string Error { get; set; }
        public string Warning { get; set; }
    }
}