﻿using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApi.Models
{
    public class NeededResourceChangeStatusModel
    {
        [Required]
        [MaxLength(2000)]
        public string Reason { get; set; }
    }
}