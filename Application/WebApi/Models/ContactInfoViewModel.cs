﻿namespace Smt.Atomic.WebApi.Models
{
    public class ContactInfoViewModel
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}