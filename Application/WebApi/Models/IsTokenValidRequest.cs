﻿using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApi.Models
{
    public class IsTokenValidRequest
    {
        [Required]
        public string Token { get; set; }
    }
}