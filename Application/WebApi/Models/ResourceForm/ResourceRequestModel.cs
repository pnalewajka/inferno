﻿using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApi.Models.ResourceForm
{
    public class ResourceRequestModel : ResourceRequestDraftModel
    {
        [Required]
        public override string ResourceName { get; set; }

        [Required]
        public override int ResourceCount { get; set; }

        [Required]
        public override string PlannedStart { get; set; }

        [Required]
        public override string ProjectDescription { get; set; }

        [Required]
        public override string RoleDescription { get; set; }
    }
}