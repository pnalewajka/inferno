﻿using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.WebApi.Models.ResourceForm
{
    public class ResourceFormListModel
    {
        public long Id { get; set; }

        public string ResourceName { get; set; }

        public ResourceFormState State { get; set; }
    }
}