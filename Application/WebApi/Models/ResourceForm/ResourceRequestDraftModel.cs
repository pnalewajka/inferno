﻿using Smt.Atomic.CrossCutting.Business.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApi.Models.ResourceForm
{
    public class ResourceRequestDraftModel
    {
        public long? Id { get; set; }

        [MaxLength(200)]
        public virtual string ResourceName { get; set; }

        public virtual int ResourceCount { get; set; }

        [MaxLength(200)]
        public virtual string PlannedStart { get; set; }

        [MaxLength(200)]
        public string ProjectDuration { get; set; }

        [MaxLength(200)]
        public string Location { get; set; }

        public int State { get; set; }

        [MaxLength(2000)]
        public virtual string ProjectDescription { get; set; }

        [MaxLength(2000)]
        public virtual string RoleDescription { get; set; }

        [MaxLength(2000)]
        public string PositionRequirements { get; set; }

        [MaxLength(2000)]
        public string RecruitmentPhases { get; set; }

        [MaxLength(2000)]
        public string AdditionalComment { get; set; }

        [MaxLength(2000)]
        public string LanguageComment { get; set; }

        public List<long> Languages { get; set; }

        [MaxLength(2000)]
        public string Question { get; set; }

        public ResourceRequestFileViewModel[] Files { get; set; }

        public ResourceRequestType RequestType { get; set; }
    }

    public class ResourceRequestFileViewModel
    {
        public long? Id { get; set; }

        public Guid? TemporaryKey { get; set; }

        public string Name { get; set; }
    }
}