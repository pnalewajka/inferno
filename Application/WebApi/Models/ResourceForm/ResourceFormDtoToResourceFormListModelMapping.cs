﻿using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApi.Models.ResourceForm
{
    public class ResourceFormDtoToResourceFormListModelMapping : ClassMapping<ResourceFormDto, ResourceFormListModel>
    {
        public ResourceFormDtoToResourceFormListModelMapping()
        {
            Mapping = d => new ResourceFormListModel
            {
                Id = d.Id,
                ResourceName = d.ResourceName,
                State = d.State
            };
        }
    }
}