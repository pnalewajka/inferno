﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using System.Linq;

namespace Smt.Atomic.WebApi.Models.ResourceForm
{
    public class ResourceRequestDtoToResourceRequestDraftModelMapping : ClassMapping<Business.CustomerPortal.Dto.ResourceFormDto, ResourceRequestDraftModel>
    {
        public ResourceRequestDtoToResourceRequestDraftModelMapping()
        {
            Mapping = dto => new ResourceRequestDraftModel
            {
                Id = dto.Id,
                ResourceName = dto.ResourceName,
                PlannedStart = dto.PlannedStart,
                PositionRequirements = dto.PositionRequirements,
                ProjectDescription = dto.ProjectDescription,
                ProjectDuration = dto.ProjectDuration,
                ResourceCount = dto.ResourceCount,
                Location = dto.Location,
                State = (int)dto.State,
                AdditionalComment = dto.AdditionalComment,
                LanguageComment = dto.LanguageComment,
                RecruitmentPhases = dto.RecruitmentPhases,
                RoleDescription = dto.RoleDescription,
                Languages = dto.LanguageOptions.Select(l => l.Id).ToList(),
                Question = dto.Question,
                Files = dto.Files
                .Select(f =>
                new ResourceRequestFileViewModel
                {
                    Id = f.DocumentId,
                    Name = f.DocumentName
                })
                .ToArray(),
                RequestType = dto.RequestType
            };
        }
    }
}