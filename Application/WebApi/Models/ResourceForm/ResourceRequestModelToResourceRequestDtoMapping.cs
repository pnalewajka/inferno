﻿using System.Linq;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApi.Models.ResourceForm
{
    public class ResourceRequestModelToResourceRequestDtoMapping : ClassMapping<ResourceRequestModel, Business.CustomerPortal.Dto.ResourceFormDto>
    {
        public ResourceRequestModelToResourceRequestDtoMapping()
        {
            Mapping = d => new ResourceFormDto
            {
                ResourceName = d.ResourceName,
                PlannedStart = d.PlannedStart,
                PositionRequirements = d.PositionRequirements,
                ProjectDescription = d.ProjectDescription,
                ProjectDuration = d.ProjectDuration,
                ResourceCount = d.ResourceCount,
                Location = d.Location,
                AdditionalComment = d.AdditionalComment,
                LanguageComment = d.LanguageComment,
                RecruitmentPhases = d.RecruitmentPhases,
                RoleDescription = d.RoleDescription,
                LanguageOptions = d.Languages == null ? new LanguageOptionDto[0] : d.Languages.Select(l => new LanguageOptionDto { Id = l }),
                Files = d.Files.Select(f => new Business.Common.Dto.DocumentDto
                {
                    DocumentId = f.Id,
                    DocumentName = f.Name,
                    TemporaryDocumentId = f.TemporaryKey
                }).ToArray()
            };
        }
    }
}