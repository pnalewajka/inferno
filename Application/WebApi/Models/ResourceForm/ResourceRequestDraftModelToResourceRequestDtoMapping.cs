﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApi.Models.ResourceForm
{
    public class ResourceRequestDraftModelToResourceRequestDtoMapping : ClassMapping<ResourceRequestDraftModel, Business.CustomerPortal.Dto.ResourceFormDto>
    {
        public ResourceRequestDraftModelToResourceRequestDtoMapping()
        {
            Mapping = model => new Business.CustomerPortal.Dto.ResourceFormDto
            {
                ResourceName = model.ResourceName,
                PlannedStart = model.PlannedStart,
                PositionRequirements = model.PositionRequirements,
                ProjectDescription = model.ProjectDescription,
                ProjectDuration = model.ProjectDuration,
                ResourceCount = model.ResourceCount,
                Location = model.Location,
                AdditionalComment = model.AdditionalComment,
                LanguageComment = model.LanguageComment,
                RecruitmentPhases = model.RecruitmentPhases,
                RoleDescription = model.RoleDescription,
                LanguageOptions = (model.Languages ?? new List<long>()).Select(l => new LanguageOptionDto { Id = l }),
                Question = model.Question,
                Files = model.Files.Select(f => new Business.Common.Dto.DocumentDto
                {
                    DocumentId = f.Id,
                    TemporaryDocumentId = f.TemporaryKey,
                    DocumentName = f.Name
                }).ToArray(),
                RequestType = model.RequestType
            };
        }
    }
}