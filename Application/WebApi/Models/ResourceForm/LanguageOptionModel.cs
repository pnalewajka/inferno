﻿namespace Smt.Atomic.WebApi.Models.ResourceForm
{
    public class LanguageOptionModel
    {
        public long Id { get; set; }
        public string LevelName { get; set; }
        public string LanguageName { get; set; }
    }
}