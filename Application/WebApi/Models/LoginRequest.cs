﻿using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApi.Models
{
    public class LoginRequest
    {
        [Required]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
}