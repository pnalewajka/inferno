using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApi.Models
{
    public class ChangeRecommendationStatusRequest
    {
        [Required]
        public bool IsAccepted { get; set; }

        public string Note { get; set; }
    }
}