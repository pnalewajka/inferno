using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApi.Models.LittlePony
{
    public class UserDtoToUserViewModelMapping : ClassMapping<UserDto, UserViewModel>
    {
        public UserDtoToUserViewModelMapping()
        {
            Mapping = e => new UserViewModel
            {
                Id = e.Id,
                Email = e.Email,
                Login = e.Login,
                FirstName = e.FirstName,
                IsActive = e.IsActive,
                LastName = e.LastName,
            };
        }
    }
}