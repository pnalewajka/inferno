﻿using System.Collections.Generic;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace Smt.Atomic.WebApi.Models.LittlePony
{
    public class UserViewModelToUserDtoMapping : ClassMapping<UserViewModel, UserDto>
    {
        public UserViewModelToUserDtoMapping()
        {
            Mapping = e => new UserDto
            {
                Id = e.Id,
                Email = e.Email,
                Login = e.Login,
                FirstName = e.FirstName,
                IsActive = e.IsActive,
                LastName = e.LastName,
                Documents = new List<DocumentDto>(),
            };
        }
    }
}