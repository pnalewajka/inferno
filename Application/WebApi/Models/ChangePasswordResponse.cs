﻿using System.Net;

namespace Smt.Atomic.WebApi.Models
{
    public class ChangePasswordResponse
    {
        public HttpStatusCode Status { get; set; }

        public bool Result { get; set; }

        public string Login { get; set; }
    }
}