﻿using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApi.Models
{
    public class ResendActivationEmailRequest
    {
        [Required]
        public string Email { get; set; }
    }
}