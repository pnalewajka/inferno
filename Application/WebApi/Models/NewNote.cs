﻿using System.ComponentModel.DataAnnotations;

namespace Smt.Atomic.WebApi.Models
{
    public class NewNote
    {
        [Required]
        public string Content { get; set; }
    }
}