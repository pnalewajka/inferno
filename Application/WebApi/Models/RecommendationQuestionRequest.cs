namespace Smt.Atomic.WebApi.Models
{
    public class RecommendationQuestionRequest
    {
        public string Question { get; set; }
    }
}