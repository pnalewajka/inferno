﻿using System.Net;

namespace Smt.Atomic.WebApi.Models
{
    public class ApiAuthenticationResponse
    {
        public HttpStatusCode Status { get; set; }
        public string Message { get; set; }
        public string[] Errors { get; set; }
    }
}