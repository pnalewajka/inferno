﻿using Smt.Atomic.Business.CustomerPortal.Dto;

namespace Smt.Atomic.WebApi.Models.Recomendations
{
    public class RecommendationApproved: IRecommendationApproved
    {
        public long RecomendationId { get; set; }

        public string Comment { get; set; }
    }
}