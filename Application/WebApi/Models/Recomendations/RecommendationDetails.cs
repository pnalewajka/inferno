﻿using Smt.Atomic.Business.CustomerPortal.Dto;

namespace Smt.Atomic.WebApi.Models.Recomendations
{
    public class RecommendationDetails
    {
        public RecommendationDto Candidate { get; set; }
        public ChallengeDto Challenge { get; set; }
    }
}