﻿using System;
using Smt.Atomic.Business.CustomerPortal.Dto;
using Swank.Description;

namespace Smt.Atomic.WebApi.Models.Recomendations
{
    public class RecommendationChallenge : IRecomendationChallenge
    {
        public long RecomendationId { get; set; }

        [MaxLength(500)]
        public string Note { get; set; }

        public Guid[] Files { get; set; }

        [Required]
        public DateTime Deadline { get; set; }
    }
}