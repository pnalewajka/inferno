﻿using System;
using Smt.Atomic.Business.CustomerPortal.Dto;

namespace Smt.Atomic.WebApi.Models.Recomendations
{
    public class SuggestedDate: ISuggestedDate
    {
        public DateTime StartsOn { get; set; }

        public DateTime EndsOn { get; set; }
    }
}