﻿using Smt.Atomic.Business.CustomerPortal.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Swank.Description;

namespace Smt.Atomic.WebApi.Models.Recomendations
{
    public class RecommendationSchedule : IRecomendationSchedule
    {
        public long RecomendationId { get; set; }

        [MaxLength(500)]
        public string Note { get; set; }

        [Required]
        public InterviewType InterviewType { get; set; }

        [Required]
        public SuggestedDate[] SuggestedDates { get; set; }

        ISuggestedDate[] IRecomendationSchedule.SuggestedDates => SuggestedDates;
    }
}