﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Smt.Atomic.CrossCutting.Business.Enums;

namespace Smt.Atomic.WebApi.Models.Recomendations
{
    public class ScheduleAppointmentModel
    {
        public long Id { get; set; }

        public long RecomendationId { get; set; }

        public string RecomendationName { get; set; }

        public string NeededResourceName { get; set; }

        public DateTime AppointmentDate { get; set; }

        public DateTime AppointmentEndDate { get; set; }

        public AppointmentStatusType AppointmentStatus { get; set; }

        public long RecommendationId { get; set; }
    }
}