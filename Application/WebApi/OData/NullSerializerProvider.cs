﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.OData.Formatter.Serialization;
using Microsoft.OData.Edm;

namespace Smt.Atomic.WebApi.OData
{
    public class NullSerializerProvider : DefaultODataSerializerProvider
    {
        private readonly NullEntityTypeSerializer _nullEntityTypeSerializer;

        public NullSerializerProvider()
        {
            _nullEntityTypeSerializer = new NullEntityTypeSerializer(this);
        }

        public override ODataSerializer GetODataPayloadSerializer(IEdmModel model, Type type, HttpRequestMessage request)
        {
            var serializer = base.GetODataPayloadSerializer(model, type, request);

            if (serializer == null)
            {
                var functions = model.SchemaElements.Where(s => s.SchemaElementKind == EdmSchemaElementKind.Function
                                                                || s.SchemaElementKind == EdmSchemaElementKind.Action);
                var isFunctionCall = false;

                foreach (var function in functions)
                {
                    var functionFullName = $"{function.Namespace}.{function.Name}";

                    if (request.RequestUri.OriginalString.Contains(functionFullName))
                    {
                        isFunctionCall = true;
                        break;
                    }
                }

                if (!isFunctionCall)
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }

                return _nullEntityTypeSerializer;
            }

            return serializer;
        }
    }
}