﻿IF NOT EXISTS(SELECT TOP 1 [Id] FROM [SkillManagement].[JobMatrix])
BEGIN
	BEGIN TRANSACTION
		INSERT INTO [SkillManagement].[JobMatrix] ([Code],[Name],[Description],[ModifiedOn]) SELECT 'S','Software Development',NULL,GETDATE()
		INSERT INTO [SkillManagement].[JobMatrix] ([Code],[Name],[Description],[ModifiedOn]) SELECT 'QA','QA',NULL,GETDATE()
		INSERT INTO [SkillManagement].[JobMatrix] ([Code],[Name],[Description],[ModifiedOn]) SELECT 'IT','IT',NULL,GETDATE()
		INSERT INTO [SkillManagement].[JobMatrix] ([Code],[Name],[Description],[ModifiedOn]) SELECT 'SD','Service Desk',NULL,GETDATE()
		INSERT INTO [SkillManagement].[JobMatrix] ([Code],[Name],[Description],[ModifiedOn]) SELECT 'A','Analyst Consultant',NULL,GETDATE()
		INSERT INTO [SkillManagement].[JobMatrix] ([Code],[Name],[Description],[ModifiedOn]) SELECT 'PO','Product Owner',NULL,GETDATE()
		INSERT INTO [SkillManagement].[JobMatrix] ([Code],[Name],[Description],[ModifiedOn]) SELECT 'KAM','Key Account Manager',NULL,GETDATE()
		INSERT INTO [SkillManagement].[JobMatrix] ([Code],[Name],[Description],[ModifiedOn]) SELECT 'BDM','Business Development Manager',NULL,GETDATE()
	COMMIT
	PRINT('JobMatrices finished :D')
END
ELSE
BEGIN
	RAISERROR ('Table [SkillManagement].[JobMatrix] is not empty', 18, -1)
END