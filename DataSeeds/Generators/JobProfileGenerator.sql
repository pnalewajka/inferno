SELECT CONCAT(
	'INSERT INTO [SkillManagement].[JobProfile] ([Name],[Description],[CustomOrder],[ModifiedOn]) VALUES ('
	,[Runtime].[SanitizeNvarchar]([Name]), ','
	,IIF([Description] IS NULL, 'NULL', [Runtime].[SanitizeNvarchar]([Description])), ','
	,IIF([CustomOrder] IS NULL, 'NULL', [CustomOrder]), ','
	,'GETDATE())')
FROM [SkillManagement].[JobProfile]