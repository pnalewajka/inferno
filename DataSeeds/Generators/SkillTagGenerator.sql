SELECT CONCAT(
	'INSERT INTO [SkillManagement].[SkillTag] ([Name],[Description],[TagType],[ModifiedOn]) VALUES ('
	,[Runtime].[SanitizeNvarchar]([Name]), ','
	,IIF([Description] IS NULL, 'NULL', [Runtime].[SanitizeNvarchar]([Description])), ','
	,IIF([TagType] IS NULL, 'NULL', CAST([TagType] AS VARCHAR(MAX))), ','
	,'GETDATE())')
FROM [SkillManagement].[SkillTag]