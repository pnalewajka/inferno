SELECT CONCAT(
	'INSERT INTO [Configuration].[CalendarEntry]([EntryOn],[Description],[CalendarId],[ModifiedOn]) SELECT '
	,[Runtime].[SanitizeVarchar]([CE].[EntryOn]), ','
    ,IIF([CE].[Description] IS NULL, 'NULL', [Runtime].[SanitizeNvarchar]([CE].[Description])), ','
    ,'[Configuration].[GetCalendarIdFromCode](', [Runtime].[SanitizeVarchar]([C].[Code]), '),'
    ,'GETDATE()'
)
FROM [Configuration].[CalendarEntry] [CE]
LEFT JOIN [Configuration].[Calendar] [C] ON [C].[Id] = [CE].[CalendarId]