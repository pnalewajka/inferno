SELECT CONCAT(
	'INSERT INTO [SkillManagement].[Skill] ([Name],[Description],[ModifiedOn]) VALUES ('
	,[Runtime].[SanitizeNvarchar]([Name]), ','
	,IIF([Description] IS NULL, 'NULL', [Runtime].[SanitizeVarchar]([Description])), ','
	,'GETDATE())')
FROM [SkillManagement].[Skill]