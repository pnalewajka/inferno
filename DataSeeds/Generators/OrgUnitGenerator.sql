SELECT
	CONCAT(
		'INSERT INTO [Organization].[OrgUnit]([Code], [Path], [Name], [NameSortOrder], [FlatName], [Description], [ParentId], [CalendarId], [RegionId], [OrgUnitFeatures], [CustomOrder], [TimeReportingMode], [DefaultProjectContributionMode], [DescendantCount], [ModifiedOn]) SELECT '
		,[Runtime].[SanitizeVarchar]([O].[Code]), ','
		,IIF([O].[Path] IS NULL, 'NULL', [Runtime].[SanitizeVarchar]([O].[Path])), ','
		,IIF([O].[Name] IS NULL, 'NULL', [Runtime].[SanitizeNvarchar]([O].[Name])), ','
		,[O].[NameSortOrder], ','
		,IIF([O].[FlatName] IS NULL, 'NULL', [Runtime].[SanitizeNvarchar]([O].[FlatName])), ','
		,IIF([O].[Description] IS NULL, 'NULL', [Runtime].[SanitizeNvarchar]([O].[Description])), ','
		,'[Organization].[GetOrgUnitIdFromCode](', IIF([O].[ParentId] IS NULL, 'NULL', (SELECT TOP 1 [Runtime].[SanitizeVarchar]([Code]) FROM [Organization].[OrgUnit] WHERE [Id] = [O].[ParentId])), '),'
		,'[Configuration].[GetCalendarIdFromCode](', IIF([O].[CalendarId] IS NULL, 'NULL', [Runtime].[SanitizeVarchar]([C].[Code])), '),'
		,'[Dictionaries].[GetRegionIdFromName](', IIF([O].[RegionId] IS NULL, 'NULL', [Runtime].[SanitizeNvarchar]([R].[Name])), '),'
		,[O].[OrgUnitFeatures], ','
		,IIF([O].[CustomOrder] IS NULL, 'NULL', CAST([O].[CustomOrder] AS VARCHAR(MAX))), ','
		,IIF([O].[TimeReportingMode] IS NULL, 'NULL', CAST([O].[TimeReportingMode] AS VARCHAR(MAX))), ','
		,IIF([O].[DefaultProjectContributionMode] IS NULL, 'NULL', CAST([O].[DefaultProjectContributionMode] AS VARCHAR(MAX))), ','
		,'0,GETDATE()'
	)
FROM [Organization].[OrgUnit] [O]
LEFT JOIN [Configuration].[Calendar] [C] ON [C].[Id] = [O].[CalendarId]
LEFT JOIN [Dictionaries].[Region] [R] ON [R].[Id] = [O].[RegionId]
WHERE
	[O].[Code] != 'UNASSIGNED'
ORDER BY [O].[ParentId]