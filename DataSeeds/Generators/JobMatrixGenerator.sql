SELECT CONCAT(
	'INSERT INTO [SkillManagement].[JobMatrix] ([Code],[Name],[Description],[ModifiedOn]) SELECT '
	,[Runtime].[SanitizeVarchar]([Code]), ','
	,IIF([Name] IS NULL, 'NULL', [Runtime].[SanitizeNvarchar]([Name])), ','
	,IIF([Description] IS NULL, 'NULL', [Runtime].[SanitizeNvarchar]([Description])), ','
	,'GETDATE()')
FROM [SkillManagement].[JobMatrix]