SELECT CONCAT(
	'INSERT INTO [Configuration].[Calendar]([Code],[Description],[ModifiedOn]) VALUES ('
	,[Runtime].[SanitizeVarchar]([Code]), ','
	,IIF([Description] IS NULL, 'NULL', [Runtime].[SanitizeVarchar]([Description])), ','
	,'GETDATE())')
FROM
	[Configuration].[Calendar]