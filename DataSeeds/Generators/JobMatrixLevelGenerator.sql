SELECT CONCAT(
	'INSERT INTO [SkillManagement].[JobMatrixLevel]([Code],[Description],[JobMatrixId],[Level],[ModifiedOn]) SELECT '
	,[Runtime].[SanitizeVarchar]([JML].[Code]), ','
	,IIF([JML].[Description] IS NULL, 'NULL', [Runtime].[SanitizeNvarchar]([JML].[Description])), ','
	,'[SkillManagement].[GetJobMatrixIdFromCode](', [Runtime].[SanitizeVarchar]([JM].[Code]), '),'
	,[JML].[Level], ','
	,'GETDATE()'
)
FROM [SkillManagement].[JobMatrixLevel] [JML]
LEFT JOIN [SkillManagement].[JobMatrix] [JM] ON [JM].[Id] = [JML].[JobMatrixId]