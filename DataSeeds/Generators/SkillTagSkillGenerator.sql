SELECT
	CONCAT(
		'INSERT INTO [SkillManagement].[SkillTagSkill] ([SkillTag_Id],[Skill_Id]) SELECT '
		,'[SkillManagement].[GetSkillTagIdFromName](', [Runtime].[SanitizeNvarchar]([ST].[Name]), '),'
		,'[SkillManagement].[GetSkillIdFromName](', [Runtime].[SanitizeNvarchar]([S].[Name]), ')')
FROM [SkillManagement].[SkillTagSkill]
LEFT JOIN [SkillManagement].[SkillTag] [ST] ON [SkillTag_Id] = [ST].[Id]
LEFT JOIN [SkillManagement].[Skill] [S] ON [Skill_Id] = [S].[Id]