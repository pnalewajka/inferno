@echo off

sqlcmd -dInferno -E -Slocalhost -iCalendars.sql -r1 1> NUL && echo.
sqlcmd -dInferno -E -Slocalhost -iCalendarEntries.sql -r1 1> NUL && echo.
sqlcmd -dInferno -E -Slocalhost -iOrgUnits.sql -r1 1> NUL && echo.
sqlcmd -dInferno -E -Slocalhost -iJobProfiles.sql -r1 1> NUL && echo.
sqlcmd -dInferno -E -Slocalhost -iJobMatrices.sql -r1 1> NUL && echo.
sqlcmd -dInferno -E -Slocalhost -iJobMatrixLevels.sql -r1 1> NUL && echo.
sqlcmd -dInferno -E -Slocalhost -iSkillTags.sql -r1 1> NUL && echo.
sqlcmd -dInferno -E -Slocalhost -iSkills.sql -r1 1> NUL && echo.
sqlcmd -dInferno -E -Slocalhost -iSkillTagsSkills.sql -r1 1> NUL && echo.
sqlcmd -dInferno -E -Slocalhost -iLocations.sql -r1 1> NUL && echo.
sqlcmd -dInferno -E -Slocalhost -iRegions.sql -r1 1> NUL && echo.
sqlcmd -dInferno -E -Slocalhost -iIndustries.sql -r1 1> NUL && echo.

pause