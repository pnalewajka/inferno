﻿IF NOT EXISTS(SELECT TOP 1 [Id] FROM [Configuration].[Calendar])
BEGIN
	BEGIN TRANSACTION

		INSERT INTO [Configuration].[Calendar]([Code],[Description],[ModifiedOn]) VALUES ('default.pl','Domyślny kalendarz dla Polski',GETDATE())

	COMMIT
	
	PRINT('Calendars finished :D')
END
ELSE
BEGIN
	RAISERROR ('Table [Configuration].[Calendar] is not empty', 18, -1)
END