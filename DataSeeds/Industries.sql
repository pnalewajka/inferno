﻿IF NOT EXISTS(SELECT TOP 1 [Id] FROM [Dictionaries].[Industry])
BEGIN
	BEGIN TRANSACTION
		INSERT INTO [Dictionaries].[Industry]([Name],[ModifiedOn]) VALUES ('Automotive',GETDATE())
		INSERT INTO [Dictionaries].[Industry]([Name],[ModifiedOn]) VALUES ('Education',GETDATE())
		INSERT INTO [Dictionaries].[Industry]([Name],[ModifiedOn]) VALUES ('Finance+Insurance',GETDATE())
		INSERT INTO [Dictionaries].[Industry]([Name],[ModifiedOn]) VALUES ('Healthcare',GETDATE())
		INSERT INTO [Dictionaries].[Industry]([Name],[ModifiedOn]) VALUES ('High Tech',GETDATE())
		INSERT INTO [Dictionaries].[Industry]([Name],[ModifiedOn]) VALUES ('Industrial',GETDATE())
		INSERT INTO [Dictionaries].[Industry]([Name],[ModifiedOn]) VALUES ('Media+Entertainment',GETDATE())
		INSERT INTO [Dictionaries].[Industry]([Name],[ModifiedOn]) VALUES ('Professional Services',GETDATE())
		INSERT INTO [Dictionaries].[Industry]([Name],[ModifiedOn]) VALUES ('Public sector',GETDATE())
		INSERT INTO [Dictionaries].[Industry]([Name],[ModifiedOn]) VALUES ('Retail+E-commerce',GETDATE())
		INSERT INTO [Dictionaries].[Industry]([Name],[ModifiedOn]) VALUES ('Telco',GETDATE())
		INSERT INTO [Dictionaries].[Industry]([Name],[ModifiedOn]) VALUES ('Telematics+GIS',GETDATE())
		INSERT INTO [Dictionaries].[Industry]([Name],[ModifiedOn]) VALUES ('Transportation',GETDATE())
		INSERT INTO [Dictionaries].[Industry]([Name],[ModifiedOn]) VALUES ('Travel+Leisure',GETDATE())
	COMMIT
	PRINT('Industries finished :D')
END
ELSE
BEGIN
	RAISERROR ('Table [Dictionaries].[Industry] is not empty', 18, -1)
END