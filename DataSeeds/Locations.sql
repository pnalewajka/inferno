﻿IF NOT EXISTS(SELECT TOP 1 [Id] FROM [Dictionaries].[Location])
BEGIN
	BEGIN TRANSACTION
		INSERT INTO [Dictionaries].[Location]([Name],[ModifiedOn]) VALUES ('Berlin',GETDATE())
		INSERT INTO [Dictionaries].[Location]([Name],[ModifiedOn]) VALUES ('Białystok',GETDATE())
		INSERT INTO [Dictionaries].[Location]([Name],[ModifiedOn]) VALUES ('Gliwice',GETDATE())
		INSERT INTO [Dictionaries].[Location]([Name],[ModifiedOn]) VALUES ('Innsbruck',GETDATE())
		INSERT INTO [Dictionaries].[Location]([Name],[ModifiedOn]) VALUES ('Katowice',GETDATE())
		INSERT INTO [Dictionaries].[Location]([Name],[ModifiedOn]) VALUES ('Katowice Client',GETDATE())
		INSERT INTO [Dictionaries].[Location]([Name],[ModifiedOn]) VALUES ('Kraków',GETDATE())
		INSERT INTO [Dictionaries].[Location]([Name],[ModifiedOn]) VALUES ('Kraków Client',GETDATE())
		INSERT INTO [Dictionaries].[Location]([Name],[ModifiedOn]) VALUES ('London',GETDATE())
		INSERT INTO [Dictionaries].[Location]([Name],[ModifiedOn]) VALUES ('Lublin',GETDATE())
		INSERT INTO [Dictionaries].[Location]([Name],[ModifiedOn]) VALUES ('Łódź',GETDATE())
		INSERT INTO [Dictionaries].[Location]([Name],[ModifiedOn]) VALUES ('Munchen',GETDATE())
		INSERT INTO [Dictionaries].[Location]([Name],[ModifiedOn]) VALUES ('Poznań',GETDATE())
		INSERT INTO [Dictionaries].[Location]([Name],[ModifiedOn]) VALUES ('Regensburg',GETDATE())
		INSERT INTO [Dictionaries].[Location]([Name],[ModifiedOn]) VALUES ('Santa Clara',GETDATE())
		INSERT INTO [Dictionaries].[Location]([Name],[ModifiedOn]) VALUES ('Southampton',GETDATE())
		INSERT INTO [Dictionaries].[Location]([Name],[ModifiedOn]) VALUES ('Szczecin',GETDATE())
		INSERT INTO [Dictionaries].[Location]([Name],[ModifiedOn]) VALUES ('Warszawa',GETDATE())
		INSERT INTO [Dictionaries].[Location]([Name],[ModifiedOn]) VALUES ('Warszawa Client',GETDATE())
		INSERT INTO [Dictionaries].[Location]([Name],[ModifiedOn]) VALUES ('Wrocław Client',GETDATE())
		INSERT INTO [Dictionaries].[Location]([Name],[ModifiedOn]) VALUES ('Wrocław Kościuszki',GETDATE())
		INSERT INTO [Dictionaries].[Location]([Name],[ModifiedOn]) VALUES ('Wrocław Prosta',GETDATE())
	COMMIT
	PRINT('Locations finished :D')
END
ELSE
BEGIN
	RAISERROR ('Table [Dictionaries].[Location] is not empty', 18, -1)
END