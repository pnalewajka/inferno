﻿IF NOT EXISTS(SELECT TOP 1 [Id] FROM [Dictionaries].[Region])
BEGIN
	BEGIN TRANSACTION
		INSERT INTO [Dictionaries].[Region]([Name],[ModifiedOn]) VALUES ('EMEA',GETDATE())
		INSERT INTO [Dictionaries].[Region]([Name],[ModifiedOn]) VALUES ('Germany',GETDATE())
		INSERT INTO [Dictionaries].[Region]([Name],[ModifiedOn]) VALUES ('Other',GETDATE())
		INSERT INTO [Dictionaries].[Region]([Name],[ModifiedOn]) VALUES ('Poland',GETDATE())
		INSERT INTO [Dictionaries].[Region]([Name],[ModifiedOn]) VALUES ('UK',GETDATE())
		INSERT INTO [Dictionaries].[Region]([Name],[ModifiedOn]) VALUES ('US',GETDATE())
	COMMIT
	PRINT('Regions finished :D')
END
ELSE
BEGIN
	RAISERROR ('Table [Dictionaries].[Region] is not empty', 18, -1)
END