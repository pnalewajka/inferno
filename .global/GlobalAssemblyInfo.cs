using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyVersion("0.3.0.0")]
[assembly: AssemblyFileVersion("0.3.0.0")]
[assembly: AssemblyCompany("intive")]
[assembly: AssemblyProduct("Atomic")]
[assembly: AssemblyCopyright("Copyright � intive 2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]

[assembly: InternalsVisibleTo("UnitTests")]