DELETE FROM [Accounts].[SecurityRoleSecurityProfiles]
GO
DELETE FROM [Accounts].[UserSecurityRoles]
GO
DELETE FROM [Accounts].[UserSecurityProfiles]
GO
DELETE FROM [Accounts].[SecurityRole]
GO
DELETE FROM [Accounts].[SecurityProfile]
GO

SET IDENTITY_INSERT [Accounts].[SecurityProfile] ON

-- PASTE STEP #1 RESULTS HERE

SET IDENTITY_INSERT [Accounts].[SecurityProfile] OFF
