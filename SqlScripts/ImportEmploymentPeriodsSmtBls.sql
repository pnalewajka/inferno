--Script used to import EmploymentPeriod

--First you need to import Excel file to Inferno database. ('Arkusz1$')
--Steps will be similar to this description
--https://www.mssqltips.com/sqlservertutorial/203/simple-way-to-import-data-into-sql-server/
--Only difference is that you should change table name from [dbo].[Arkusz1$] to [Allocation].[EmploymentPeriodImportData]

--Next step is to add new fields in imported table. They will be used in import
ALTER TABLE [Allocation].[EmploymentPeriodImportData] ADD Email VARCHAR(255) NULL, EmployeeId BIGINT NULL, StartDate DATETIME;

/* Srcipt version 1 - disabled
--Update is preparing Email addresses which will be used to compare employees
UPDATE [Allocation].[EmploymentPeriodImportData]
SET Email = 
	CAST(
		CASE CHARINDEX(' ', REVERSE([name allocation]))
		WHEN 0 THEN [name allocation] 
		ELSE
			REVERSE(SUBSTRING(REVERSE([name allocation]),1, CHARINDEX(' ', REVERSE([name allocation])) -1)) + '.' + RTRIM(LEFT([name allocation], CHARINDEX(' ', [name allocation]) - 1))
		END
 AS VARCHAR(100)) COLLATE Cyrillic_General_CI_AI --this will change polish characters to normal

--Next, we are comparing emails to assign EmployeeId to this Employees which we found
UPDATE u
SET u.EmployeeId = s.Id
FROM [Allocation].[EmploymentPeriodImportData] u
    INNER JOIN [Allocation].[Employee] s ON
        s.Email LIKE '%' + u.Email + '%'

*/

/* Script version 2 - currently enabled */
-- assing EmployeeId basing on logins
UPDATE ep
SET ep.EmployeeId = e.Id
FROM [Allocation].[EmploymentPeriodImportData] ep
INNER JOIN [Accounts].[User] s ON s.[Login] = ep.[Login]
INNER JOIN [Allocation].[Employee] e ON e.[UserId] = s.[Id] 


/* Script version 1 - disabled 
--Next update step is prepare Start date in correct format.
UPDATE [Allocation].[EmploymentPeriodImportData]
SET StartDate = 
		 CASE CHARINDEX('.', [Start date])
		 WHEN 5 then CONVERT(DATETIME, [Start date], 102)
		 WHEN 3 then CONVERT(DATETIME, [Start date], 104)
		 END
*/	

/* Script version 2 - currently enabled */
UPDATE [Allocation].[EmploymentPeriodImportData]
SET StartDate = [Start date]


--Last step is to insert employmentPeriods using data which we prepare
  INSERT INTO [Allocation].[EmploymentPeriod] ([StartDate] ,[EndDate],[EmployeeIdEmployee],[EmploymentType],[MondayHours]
      ,[TuesdayHours],[WednesdayHours],[ThursdayHours],[FridayHours],[SaturdayHours],[SundayHours]      
            ,[ModifiedOn],[IsActive],[InactivityReason],[SignedOn],[NoticePeriod])
	  SELECT 
	  ea.[StartDate], ea.[End date],
	  e.Id, 
	  CASE 
		WHEN [contract type] like '%N/A%' THEN NULL 
		WHEN [contract type] like 'UoP%' or [contract type] like 'Umowa o dzie%' or [contract type] like 'Umowa zlecenie%' THEN 1 
		WHEN [contract type] like 'B2B%'  THEN 2
		WHEN [contract type] like 'external%' THEN 3
	  END,
       [FTE]*8,[FTE]*8,[FTE]*8,[FTE]*8,[FTE]*8,0,0, GetDate(), 1,0,
       [StartDate],
	  CASE 
		WHEN DATEDIFF(mm, [StartDate], GetDate()) < 6 THEN 200
		WHEN DATEDIFF(mm, [StartDate], GetDate()) < 36 and DATEDIFF(mm, [StartDate], GetDate()) >= 6 THEN 10000
		WHEN DATEDIFF(mm, [StartDate], GetDate()) >= 36 THEN 30000
	  END 
    FROM [Allocation].[EmploymentPeriodImportData] ea
		join [Allocation].[Employee] e on e.id = ea.EmployeeId
	WHERE ea.StartDate is not null
  
  --Below select return not imported records
  SELECT * FROM [Allocation].[EmploymentPeriodImportData] where EmployeeId is null or StartDate is null

--After import you need to manually delete table [Allocation].[EmploymentPeriodImportData]. It wasn't deleted to check not imported records.
