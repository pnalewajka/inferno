--Script used to import AllocationRequest

--First you need to import Excel file to Inferno database. ('Output$')
--Steps will be similar to this description
--https://www.mssqltips.com/sqlservertutorial/203/simple-way-to-import-data-into-sql-server/
--Only difference is that you should change table name from [dbo].[Output$] to [Allocation].[AllocationRequestImportData]

--Next step is to add new fields in imported table. They will be used in import
ALTER TABLE [Allocation].[AllocationRequestImportData] ADD Email VARCHAR(255) NULL, EmployeeId BIGINT NULL, StartDate DATETIME,
		ProjectId BIGINT NULL, EndDate DATETIME NULL;

--Update is preparing Email addresses which will be used to compare employees
UPDATE [Allocation].[AllocationRequestImportData]
SET Email = 
	CAST(
		CASE CHARINDEX(' ', REVERSE([Employee Name]))
		WHEN 0 THEN [Employee Name] 
		ELSE
			REVERSE(SUBSTRING(REVERSE([Employee Name]),1, CHARINDEX(' ', REVERSE([Employee Name])) -1)) + '.' + RTRIM(LEFT([Employee Name], CHARINDEX(' ', [Employee Name]) - 1))
		END
 AS VARCHAR(100)) COLLATE Cyrillic_General_CI_AI --this will change polish characters to normal

 --Next, we are comparing emails to assign EmployeeId to this Employees which we found
UPDATE u
SET u.EmployeeId = s.Id
FROM [Allocation].[AllocationRequestImportData] u
    INNER JOIN [Allocation].[Employee] s ON
        s.Email LIKE '%' + u.Email + '%'

--In Next step we are comparing First and Last name to assign EmployeeId when Employee wasn't found by Email
UPDATE u
SET u.EmployeeId = s.Id
FROM [Allocation].[AllocationRequestImportData] u
    INNER JOIN [Allocation].[Employee] s ON
        s.LastName + ' ' + s.FirstName LIKE u.[Employee Name]
WHERE EmployeeId IS NULL

--Next update step is prepare Start date in correct format.
UPDATE [Allocation].[AllocationRequestImportData]
SET StartDate = 
		 CASE CHARINDEX('.', [From])
		 WHEN 5 then CONVERT(DATETIME, [From], 102)
		 WHEN 3 then CONVERT(DATETIME, [From], 104)
		 END

--Next update step is prepare End date in correct format.
UPDATE [Allocation].[AllocationRequestImportData]
SET EndDate = 
		 CASE CHARINDEX('.', [To])
		 WHEN 5 then CONVERT(DATETIME, [To], 102)
		 WHEN 3 then CONVERT(DATETIME, [To], 104)
		 END

--Below updates are used to clean ProjectCodes before we create Fake projects
UPDATE [Allocation].[AllocationRequestImportData] SET [Project Code] = '[Consulting][Atomic]' WHERE [Project Code] = 'intive - Atomic'
UPDATE [Allocation].[AllocationRequestImportData] SET [Project Code] = 'Zumtobel - Setbuilder (Tridonic)' WHERE [Project Code] = 'Zumtobel - Tridonic'
UPDATE [Allocation].[AllocationRequestImportData] SET [Project Code] = 'BBRY - Good Work Android' WHERE [Project Code] = 'Good Work Android'

UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'LESE/SPORT', [Hours] = 0.3 WHERE [Input Code] = '30% LESE/SPORT';
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'NTV/INTR', [Hours] = 0.25 WHERE [Input Code] = '25% NTV/INTR';
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'SKOK/PORT/IADD', [Hours] = 0.5 WHERE [Input Code] = 'SKOK/PORT/IADD 50%';
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'POEL/ETINEW', [Hours] = 0.7 WHERE [Input Code] = '70% POEL/ETINEW';
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'PRVI/EPRF/MNTC', [Hours] = 0.25 WHERE [Input Code] = '25% PRVI/EPRF/MNTC';
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'NEWC/DIRC', [Hours] = 0.2 WHERE [Input Code] = '20% NEWC/DIRC';
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'NEWC/DIRC', [Hours] = 0.5 WHERE [Input Code] = '50% NEWC/DIRC';
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'VT' WHERE [Input Code] = 'VT   ';

UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'THNK/VUE/IMPL' WHERE [Input Code] = 'THNK/VUE/IMPL  '
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'The The Foundry - Colorway - Nuke' WHERE [Input Code] = 'The The Foundry - Colorway - Nuke  '
UPDATE [Allocation].[AllocationRequestImportData] SET [Hints] = 'free', [Input Code] = NULL WHERE [Input Code] = '(core)'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'Weather.com' WHERE [Input Code] = 'Weather.com - 1.09'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'VECT/MAIN/IMPL' WHERE [Input Code] = 'VECT/MAIN/IMPL 16.08'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'VECT/MAIN/IMPL' WHERE [Input Code] = 'VECT/MAIN/IMPL 29.08'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'VECT/MAIN/IMPL' WHERE [Input Code] = 'VECT/MAIN/IMPL 7.11'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'Provident' WHERE [Input Code] = 'Provident 3.10'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'Provident' WHERE [Input Code] = 'Provident 1.10'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'Pockit' WHERE [Input Code] = 'Pockit 1.08'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'Pockit' WHERE [Input Code] = 'Pockit 1.11.2016'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'Pockit' WHERE [Input Code] = 'Pockit 14.11.2016'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'PMI - WebHub/GPW' WHERE [Input Code] = 'PMI - WebHub/GPW 16.08'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'Onet/Skapiec' WHERE [Input Code] = 'Onet/Skapiec 08.08'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'Nice Agency' WHERE [Input Code] = 'Nice Agency (10.10)'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'KUPF/ESPRIT' WHERE [Input Code] = 'KUPF/ESPRIT 10.10.2016'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'GPW' WHERE [Input Code] = 'GPW 15.08.2016'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'Faundrising' WHERE [Input Code] = 'Faundrising (17.10)'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'Alior Bank' WHERE [Input Code] = 'Alior Bank (30.09)'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'Alior Bank' WHERE [Input Code] = 'Alior Bank (3.11)'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'Onet/VOD' WHERE [Input Code] = '3.10 Onet/VOD'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'Prudential Panda/TEVA' WHERE [Input Code] = '25.10 Prudential Panda/TEVA'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'Prudential Panda/Australia' WHERE [Input Code] = '17.10 Prudential Panda/Australia'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'Attorn' WHERE [Input Code] = '16.11 Attorn'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'Tandem Bank - Yodelee' WHERE [Input Code] = '8.08.2016 Tandem Bank - Yodelee'

UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'Viacom/A+E Networks/Arqiva' WHERE [Input Code] = 'Viacom/ A+E Networks/Arqiva'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'EcoVadis - CSR' WHERE [Input Code] = 'EcoVadis - CSR  5.09'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'EcoVadis - CSR' WHERE [Input Code] = 'EcoVadis - CSR 16.08'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'EcoVadis - CSR BI' WHERE [Input Code] = 'EcoVadis - CSR BI 16.08.2016   '
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'DigitalAsset (core)' WHERE [Input Code] = 'DigitalAsset 01.08 (core)'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'DigitalAsset (core)' WHERE [Input Code] = 'DigitalAsset 03.10 (core)'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'Attorn' WHERE [Input Code] = 'Attorn 16.11'
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'Attorn' WHERE [Input Code] = 'Attorn 17.10   '
UPDATE [Allocation].[AllocationRequestImportData] SET [Input Code] = 'Ecovadis' WHERE [Input Code] = 'ecoVadis '

--Next, we are comparing Project Code to assign ProjectId
UPDATE u
SET u.ProjectId = p.Id
FROM [Allocation].[AllocationRequestImportData] u
    INNER JOIN [Allocation].[Project] p ON
        u.[Project Code] = p.[Name]

--Next, we are comparing Input Code to assign ProjectId
UPDATE u
SET u.ProjectId = p.Id
FROM [Allocation].[AllocationRequestImportData] u
    INNER JOIN [Allocation].[Project] p ON
        u.[Input Code] = p.[Name]
WHERE u.ProjectId IS NULL 

--Below we are checking default calendar code
DECLARE @calendarId INT;
SELECT @calendarId = Id FROM [Configuration].[Calendar] WHERE Code = 'PL';

--In This step we are creating Fake projects for allocations which wasn't assign to any projects
INSERT INTO [Allocation].[Project]
           ([Name]
           ,[Description]
           ,[StartDate]
           ,[EndDate]
           ,[ProjectStatus]
           ,[ClientShortName]
           ,[ProjectShortName]
           ,[ProjectFeatures]
		   ,[ModifiedOn]
		   ,[CalendarId]
	 )
  	SELECT [Input Code], [Input Code], MIN(StartDate), NULL, 1, [Input Code], [Input Code], 1 , GETDATE(), @calendarId
	FROM [Allocation].[AllocationRequestImportData] ar
	WHERE  EmployeeId IS NOT NULL AND ProjectId IS NULL 
		AND ([Project Code] NOT IN ('will join intive', 'Left intive') OR [Project Code] IS NULL)
		AND ([Hints] NOT IN ('Left','holiday','maternity','is not contributor', 'free','part-time, holiday','training') OR [Hints] IS NULL)
		AND [Input Code] <> ' '
	GROUP BY [Input Code]

--Now we are assigning Fakce projects to Allocations which don't have any projects
UPDATE u
SET u.ProjectId = p.Id
FROM [Allocation].[AllocationRequestImportData] u
    INNER JOIN [Allocation].[Project] p ON
        u.[Input Code] = p.[Name]
WHERE u.ProjectId IS NULL

--Last step is to insert allocation requests using data which we prepare
INSERT INTO [Allocation].[AllocationRequest]
           ([EmployeeId]
           ,[ProjectId]
           ,[StartDate]
           ,[EndDate]
           ,[ModifiedOn]
           ,[MondayHours]
           ,[TuesdayHours]
           ,[WednesdayHours]
           ,[ThursdayHours]
           ,[FridayHours]
           ,[SaturdayHours]
           ,[SundayHours]
           ,[ContentType]
           ,[NextRebuildTime]
           ,[AllocationCertainty])
SELECT EmployeeId, ProjectId, StartDate, EndDate, GETDATE(),
	  [Hours]*8, [Hours]*8, [Hours]*8, [Hours]*8, [Hours]*8, 0, 0, 
	  1, NULL,
	  CASE 
		WHEN [Hints] = 'planned' THEN 0 
		ELSE 1
	  END
FROM [Allocation].[AllocationRequestImportData] ar
WHERE EmployeeId IS NOT NULL AND ProjectId IS NOT NULL 

--Below select return not imported records
SELECT *
FROM [Allocation].[AllocationRequestImportData]
WHERE EmployeeId IS NULL OR ProjectId IS NULL 

--Below select return fake projects
SELECT *
FROM [Allocation].[Project]
WHERE [ProjectFeatures] = 1

--After import you need to manually delete table [Allocation].[AllocationRequestImportData]. It wasn't deleted to check not imported records.
