SELECT 
'INSERT INTO [Accounts].[SecurityProfile] (Id, Name, ActiveDirectoryGroupName, ModifiedOn) VALUES (' + CONVERT(NVARCHAR(MAX), ID) + ', ''' + [Name] + ''', ''' + ISNULL([ActiveDirectoryGroupName], '') + ''', GETDATE());'
FROM [Accounts].[SecurityProfile]
UNION
SELECT 
'INSERT INTO [Accounts].[SecurityRole] (Id, Name) VALUES (' + CONVERT(NVARCHAR(MAX), ID) + ', ''' + [Name] + ''');' 
FROM [Accounts].[SecurityRole]
UNION
SELECT 
'INSERT INTO [Accounts].[SecurityRoleSecurityProfiles] (RoleId, ProfileId) VALUES (' + CONVERT(NVARCHAR(MAX), RoleID) + ', ' + CONVERT(NVARCHAR(MAX), ProfileId) + ');'
FROM [Accounts].[SecurityRoleSecurityProfiles]
UNION
SELECT 
'INSERT INTO [Accounts].[UserSecurityProfiles] (UserId, ProfileId) VALUES (' + CONVERT(NVARCHAR(MAX), UserId) + ', ' + CONVERT(NVARCHAR(MAX), ProfileId) + ');'
FROM [Accounts].[UserSecurityProfiles]
UNION
SELECT 
'INSERT INTO [Accounts].[UserSecurityRoles] (UserId, RoleId) VALUES (' + CONVERT(NVARCHAR(MAX), UserId) + ', ' + CONVERT(NVARCHAR(MAX), RoleId) + ');'
FROM [Accounts].[UserSecurityRoles]