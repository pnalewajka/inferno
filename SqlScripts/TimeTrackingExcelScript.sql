CREATE PROCEDURE [Allocation].[SetEmploymentPeriodParams]
	@UserLogin VARCHAR(50),
	@reportingMode VARCHAR(100),
	@defaultProject VARCHAR(100),
	@contractType VARCHAR(100)
AS
BEGIN

	DECLARE @EmploymentPeriodId INT;
	DECLARE @ReportingModeNumber INT;
	DECLARE @IsUserActive BIT

	SELECT 
		@IsUserActive = IsActive
	FROM
		Accounts.[User]
	WHERE
		Login = @UserLogin;

	IF @IsUserActive = 1
	BEGIN

		SELECT TOP 1 
			@EmploymentPeriodId = ep.Id 
		FROM Allocation.EmploymentPeriod ep
		LEFT JOIN Allocation.Employee e ON e.id = ep.EmployeeIdEmployee
		LEFT JOIN Accounts.[User] u ON u.id = e.UserId
		WHERE 
			u.Login = @UserLogin
		ORDER BY 
			ISNULL(EndDate, '9999-12-31') DESC


		SELECT @ReportingModeNumber = 
			CASE 
				WHEN @ReportingMode = 'Detailed mode' THEN 2 
				WHEN @ReportingMode = 'Absence only reporting' THEN 1 
				WHEN @ReportingMode = 'No reporting' THEN 0 
			END


		DECLARE @DefaultProjectId INT;

		IF (@ReportingModeNumber = 1 OR @defaultProject IS NOT NULL)
		BEGIN
			SELECT @DefaultProjectId = Id 
			FROM Allocation.Project 
			WHERE 
				ClientShortName + ' - ' + ProjectShortName LIKE @defaultProject + '%' 
				OR ProjectShortName LIKE @defaultProject + '%'
	
			IF @DefaultProjectId IS NULL
			BEGIN
				SELECT @DefaultProjectId = CASE 
					WHEN @defaultProject = 'SD EMEA General Costs (992508)' THEN (SELECT Id FROM Allocation.Project WHERE ProjectShortName =  'SD EMEA General Costs')
					WHEN @defaultProject = 'SD Operations Management (995011)' THEN (SELECT Id FROM Allocation.Project WHERE ProjectShortName = 'SD Operations Management')
					WHEN @defaultProject = 'SD UK General Costs (992208)' THEN (SELECT Id  FROM Allocation.Project WHERE ProjectShortName = 'SD UK General Costs')
					WHEN @defaultProject = 'SD US General Costs (992308)' THEN (SELECT Id FROM Allocation.Project WHERE ProjectShortName =  'SD US General Costs')
					WHEN @defaultProject = 'Accounting/Backoffice' OR @defaultProject = 'Accounting (General  Administration)' THEN (SELECT Id FROM Allocation.Project WHERE ProjectShortName =  'Accounting (General & Administration)')
					WHEN @defaultProject = 'MS Maintenance  Support general costs' THEN (SELECT Id FROM Allocation.Project WHERE ProjectShortName =  'Maintenance & Support general costs')
				END
			END
	
		END

		DECLARE @ContractTypeId INT;
		SELECT @ContractTypeId = Id FROM [TimeTracking].[ContractType] WHERE [NameEn] = @contractType OR [NamePl] = @contractType

		IF @ContractTypeId IS NULL
		BEGIN
			SELECT @ContractTypeId = CASE 
				WHEN @contractType = 'Permanent employment � Germany' THEN (SELECT Id FROM [TimeTracking].[ContractType] WHERE [NameEn] = 'Permanent employment - Germany')
				WHEN @contractType = 'Permanent employment � Poland' THEN (SELECT Id FROM [TimeTracking].[ContractType] WHERE [NameEn] = 'Permanent employment - Poland')
				WHEN @contractType = 'Umowa o dzie�o (A)' THEN (SELECT Id FROM [TimeTracking].[ContractType] WHERE [NameEn] = 'Specified task contract (A type) - Poland')
				WHEN @contractType = 'Umowa o dzie�o (B)' THEN (SELECT Id FROM [TimeTracking].[ContractType] WHERE [NameEn] = 'Specified task contract (B type) - Poland')
				WHEN @contractType = 'Umowa o dzie�o (E)' THEN (SELECT Id FROM [TimeTracking].[ContractType] WHERE [NameEn] = 'Specified task contract (E type) - Poland')
				WHEN @contractType = 'Umowa zlecenie z ZUS (A)' THEN (SELECT Id FROM [TimeTracking].[ContractType] WHERE [NameEn] = 'Service contract (A type, ZUS incl.) - Poland')
				WHEN @contractType = 'Umowa zlecenie z ZUS (B)' THEN (SELECT Id FROM [TimeTracking].[ContractType] WHERE [NameEn] = 'Service contract (B type, ZUS incl.) - Poland')
				WHEN @contractType = 'Umowa zlecenie z ZUS (E)' THEN (SELECT Id FROM [TimeTracking].[ContractType] WHERE [NameEn] = 'Service contract (E type, ZUS incl.) - Poland')
				WHEN @contractType = 'Umowa zlecenie bez ZUS (A)' THEN (SELECT Id FROM [TimeTracking].[ContractType] WHERE [NameEn] = 'Service contract (A type, ZUS excl.) - Poland')
				WHEN @contractType = 'Umowa zlecenie bez ZUS (B)' THEN (SELECT Id FROM [TimeTracking].[ContractType] WHERE [NameEn] = 'Service contract (B type, ZUS excl.) - Poland')
				WHEN @contractType = 'Umowa zlecenie bez ZUS (E)' THEN (SELECT Id FROM [TimeTracking].[ContractType] WHERE [NameEn] = 'Service contract (E type, ZUS excl.) - Poland')
				WHEN @contractType = 'Permanent employment with 50% KUP - Poland' THEN (SELECT Id FROM [TimeTracking].[ContractType] WHERE [NameEn] = 'Permanent employment with 50% CDE - Poland')
				WHEN @contractType = 'Working students � Germany' THEN (SELECT Id FROM [TimeTracking].[ContractType] WHERE [NameEn] = 'Working students - Germany')
				END
		END


		IF @EmploymentPeriodId IS NULL OR @ReportingModeNumber IS NULL OR @ContractTypeId IS NULL OR (@reportingModeNumber=(1) AND @DefaultProjectId IS NULL)
		BEGIN
			DECLARE @InvalidColumn VARCHAR(20);
			select @InvalidColumn = CASE
				WHEN @EmploymentPeriodId IS NULL THEN 'EmploymentPeriodId'
				WHEN @reportingModeNumber IS NULL THEN 'ReportingModeId'
				WHEN @ContractTypeId IS NULL THEN 'ContractTypeId'
				WHEN @reportingModeNumber=(1) AND @DefaultProjectId IS NULL THEN 'DefaultProjectId'
				END

			INSERT INTO [Imports].[ImportLog] ([EmployeeLogin],	[EmploymentPeriodId], [ReportingMode], [ReportingModeId],
				[DefaultProject], [DefaultProjectId], [ContractType], [ContractTypeId], [InvalidColumn], [CreatedOn])
				VALUES (@UserLogin, @EmploymentPeriodId, @reportingMode, @reportingModeNumber, 
				@defaultProject, @DefaultProjectId, @contractType, @ContractTypeId, @InvalidColumn, GETDATE())
		END
		ELSE
		BEGIN
			UPDATE Allocation.EmploymentPeriod 
			SET
				TimeReportingMode = @reportingModeNumber,
				ProjectIdAbsenceOnlyDefaultProject = @DefaultProjectId,
				[ContractTypeId] = @ContractTypeId
			WHERE Id = @EmploymentPeriodId
		END
	END

END;

/*
CREATE TABLE [Imports].[ImportLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[EmployeeLogin] [nvarchar](50) NULL,
	[EmploymentPeriodId] [bigint] NULL,
	[ReportingMode] [nvarchar](100) NULL,
	[ReportingModeId] [bigint] NULL,
	[DefaultProject] [nvarchar](100) NULL,
	[DefaultProjectId] [bigint] NULL,
	[ContractType] [nvarchar](100) NULL,
	[ContractTypeId] [bigint] NULL,
	[InvalidColumn] [nvarchar](20) NULL,
	[CreatedOn] datetime);


 Example for row 8
  =("EXECUTE [Allocation].[SetEmploymentPeriodParams]  '"&L8&"', '"&M8&"', '"&N8&"', '"&O8&"'")   
*/