﻿DECLARE @beforeBackup NVARCHAR(1024) = 'E:\Uploads\Inferno ' + CONVERT(NVARCHAR, GETDATE(), 105) +'.bak';

BACKUP DATABASE Inferno TO DISK= @beforeBackup

DECLARE @dbPath NVARCHAR(2048) = 'D:\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\';

DECLARE @mdfPath NVARCHAR(2048) = @dbPath + 'Inferno2.mdf'
DECLARE @logPath NVARCHAR(2048) = @dbPath + 'Inferno2_Log.ldf'

RESTORE DATABASE InfernoAnonimization FROM DISK = @beforeBackup
WITH
   MOVE 'Inferno' TO @mdfPath,
   MOVE 'Inferno_Log' TO @logPath;
GO

USE InfernoAnonimization

DECLARE @dbName NVARCHAR(100);
SELECT @dbName = DB_NAME(db_id());
 
IF (@dbName <>'InfernoAnonimization')
BEGIN
   RAISERROR('Wrong database, terminating', 20, -1) WITH LOG
END

-- reduce DB size

DECLARE @RecruitmentEntitiesToKeep TABLE ( CandidateId BIGINT NOT NULL, ProcessId BIGINT NOT NULL, JobOpeningId BIGINT NOT NULL )

INSERT INTO @RecruitmentEntitiesToKeep
   SELECT TOP 100 C.Id as CandidateId, P.Id as ProcessId, P.JobOpeningId as JobOpeningId FROM Recruitment.Candidate C
   JOIN Recruitment.RecruitmentProcess P ON P.CandidateId = C.Id
   ORDER BY NEWID()

DELETE FROM GDPR.DataOwner WHERE CandidateId NOT IN (SELECT CandidateId From @RecruitmentEntitiesToKeep)

DELETE FROM Recruitment.RecruitmentProcessNote WHERE RecruitmentProcessId NOT IN (SELECT ProcessId FROM @RecruitmentEntitiesToKeep) OR RecruitmentProcessStepId IS NOT NULL
DELETE FROM Recruitment.RecruitmentProcessStep WHERE RecruitmentProcessId NOT IN (SELECT ProcessId FROM @RecruitmentEntitiesToKeep)
DELETE FROM Recruitment.RecruitmentProcessScreening WHERE Id NOT IN (SELECT ProcessId FROM @RecruitmentEntitiesToKeep)
DELETE FROM Recruitment.RecruitmentProcessFinal WHERE Id NOT IN (SELECT ProcessId FROM @RecruitmentEntitiesToKeep)
DELETE FROM Recruitment.RecruitmentProcessOffer WHERE Id NOT IN (SELECT ProcessId FROM @RecruitmentEntitiesToKeep)
DELETE FROM Recruitment.RecruitmentProcess WHERE Id NOT IN (SELECT ProcessId FROM @RecruitmentEntitiesToKeep)

DELETE FROM Recruitment.InboundEmailDocumentContent
DELETE FROM Recruitment.InboundEmailDocument
DELETE FROM Recruitment.InboundEmailValue

UPDATE Recruitment.InboundEmail SET Subject = '(sample subject)', Body = '(sample Body)'

DELETE FROM Recruitment.ProjectDescription WHERE JobOpeningId NOT IN (SELECT JobOpeningId FROM @RecruitmentEntitiesToKeep)
DELETE FROM Recruitment.JobOpeningNote WHERE JobOpeningId NOT IN (SELECT JobOpeningId FROM @RecruitmentEntitiesToKeep)
DELETE FROM Recruitment.JobOpening WHERE Id NOT IN (SELECT JobOpeningId FROM @RecruitmentEntitiesToKeep)
DELETE FROM Recruitment.CandidateDocument
DELETE FROM Recruitment.CandidateFile
DELETE FROM Recruitment.CandidateNote WHERE CandidateId NOT IN (SELECT CandidateId From @RecruitmentEntitiesToKeep)
DELETE FROM Recruitment.CandidateLanguage WHERE CandidateId NOT IN (SELECT CandidateId From @RecruitmentEntitiesToKeep)
DELETE FROM Recruitment.CandidateContactRecord WHERE CandidateId NOT IN (SELECT CandidateId From @RecruitmentEntitiesToKeep)
DELETE FROM Recruitment.JobApplication WHERE CandidateId NOT IN (SELECT CandidateId From @RecruitmentEntitiesToKeep)
DELETE FROM Recruitment.Candidate WHERE Id NOT IN (SELECT CandidateId From @RecruitmentEntitiesToKeep)

-- end reduce DB size

-- clear all sensitive data

UPDATE Finance.AggregatedProjectTransaction
SET
   EstimatedRevenue = 1000 *RAND() - 500,
   [Hours] = 1000 *RAND() - 500,
   OperationCost = 1000 *RAND() - 500,
   OperationMargin = 1000 *RAND() - 500,
   OperationMarginPercent = 1000 *RAND() - 500,
   ProjectCost = 1000 *RAND() - 500,
   ProjectMargin = 1000 *RAND() - 500,
   ProjectMarginPercent = 1000 *RAND() - 500,
   Revenue = 1000 *RAND() - 500,
   ServiceCost = 1000 *RAND() - 500,
   ServiceMargin = 1000 *RAND() - 500,
   ServiceMarginPercent = 1000 *RAND() - 500

UPDATE [Configuration].SystemParameter SET [Value] = 'dante.alighieri@intive-projects.com' WHERE [Key] = 'System.Smtp.DefaultFrom.EmailAddress'
UPDATE [Configuration].SystemParameter SET [Value] = 'Dante' WHERE [Key] = 'System.Smtp.DefaultFrom.FullName'
UPDATE [Configuration].SystemParameter SET [Value] = 'serwer1645489.home.pl' WHERE [Key] = 'System.Smtp.EmailServer.HostName'
UPDATE [Configuration].SystemParameter SET [Value] = '587' WHERE [Key] = 'System.Smtp.EmailServer.Port'
UPDATE [Configuration].SystemParameter SET [Value] = 'dante.alighieri@intive-projects.com' WHERE [Key] = 'System.Smtp.EmailServer.UserName'
UPDATE [Configuration].SystemParameter SET [Value] = '2YS17J:OQoHa' WHERE [Key] = 'System.Smtp.EmailServer.Password'
UPDATE [Configuration].SystemParameter SET [Value] = 'true' WHERE [Key] = 'System.Smtp.EmailServer.UseSsl'
UPDATE [Configuration].SystemParameter SET [Value] = 'false' WHERE [Key] = 'System.Smtp.EmailServer.SpecifyRecipientName'
UPDATE [Configuration].SystemParameter SET [Value] = 'dante.alighieri@smtprojects.com' WHERE [Key] = 'System.Smtp.Mock.EmailAddress'
UPDATE [Configuration].SystemParameter SET [Value] = 'true' WHERE [Key] = 'System.Smtp.Mock.SendMockEmailFlag'

UPDATE Allocation.Employee
SET
   SkypeName = 'echo123',
   PersonalNumber = '70010100070',
   IdCardNumber = 'ABC' + RIGHT('00000000' + CAST(ID AS VARCHAR), 8),
   PhoneNumber = '+48 71 769 59 00',
   HomeAddress = N'ul. Kościuszki 29; 50-011 Wrocław',
   DateOfBirth = '1970-01-01',
   AvailabilityDetails = CAST(8 + ID % 3 AS VARCHAR) + ':00 - ' + CAST(16 + ID % 3 AS VARCHAR) + ':00'

UPDATE BusinessTrips.BusinessTripParticipant
SET
   IdCardNumber = 'ABC' + RIGHT('00000000' + CAST(ID AS VARCHAR), 8),
   PhoneNumber = '+48 71 769 59 00',
   HomeAddress = N'ul. Kościuszki 29; 50-011 Wrocław'

UPDATE Workflows.OnboardingRequest
SET
   Compensation = 100 * RAND(),
   ProbationPeriodCompensation = 100 * RAND(),
   CommentsForHiringManager = '',
   CommentsForPayroll = ''

UPDATE Workflows.EmploymentDataChangeRequest
SET
   Salary = 100 * RAND(),
   CommentsForPayroll = '',
   Justification = '',
   EmployeeId = (SELECT TOP 1 Id FROM Allocation.Employee WHERE LineManagerId = 11 ORDER BY NEWID())


DELETE FROM Workflows.EmployeePhotoDocumentContent
DELETE FROM Workflows.EmployeePhotoDocument
DELETE FROM Workflows.ForeignEmployeeDocumentContent
DELETE FROM Workflows.ForeignEmployeeDocument
DELETE FROM Workflows.PayrollDocumentContent
DELETE FROM Workflows.PayrollDocument
DELETE FROM Workflows.RequestDocumentContent
DELETE FROM Workflows.RequestDocument

-- RELEASE 19

DELETE FROM Notifications.EmailAttachment
DELETE FROM Notifications.Email

DELETE FROM Compensation.InvoiceCalculation

UPDATE Dictionaries.Company
SET DefaultAdvancedPaymentDailyAmount = 10 -- easy for debug

UPDATE Allocation.EmploymentPeriod
SET
   HourlyRate = IIF(HourlyRate IS NULL, NULL, 1),
   HourlyRateDomesticOutOfOfficeBonus = IIF(HourlyRateDomesticOutOfOfficeBonus IS NULL, NULL, 1),
   HourlyRateForeignOutOfOfficeBonus = IIF(HourlyRateForeignOutOfOfficeBonus IS NULL, NULL, 1),
   MonthlyRate = IIF(MonthlyRate IS NULL, NULL, 10),
   SalaryGross = IIF(SalaryGross IS NULL, NULL, 1000),
   AuthorRemunerationRatio = IIF(AuthorRemunerationRatio IS NULL, NULL, 80)

DELETE FROM MeetMe.MeetingNoteAttachment
DELETE FROM MeetMe.MeetingNote
DELETE FROM MeetMe.MeetingAttachment
DELETE FROM MeetMe.Meeting

UPDATE [Finance].[GlobalRate] SET [Value] = 1

-- Recruitment

UPDATE Recruitment.CandidateNoteMessageMentions SET
   EmployeeId = (SELECT TOP 1 Id FROM Allocation.Employee ORDER BY NEWID())

UPDATE Recruitment.CandidateNote SET
   Content = IIF(Content IS NOT NULL, '?????', NULL)

delete from Recruitment.CandidateDocument
delete from Recruitment.CandidateFile

UPDATE Recruitment.TechnicalReview SET
   SeniorityLevelAssessment = (SELECT TOP 1 [Key] FROM (VALUES(0),(1),(2),(3),(4)) T([Key]) ORDER BY NEWID()),   
   TechnicalKnowledgeAssessment = IIF(TechnicalKnowledgeAssessment IS NOT NULL, '(sample Technical Knowledge Assessment)', NULL),
   TechnicalAssignmentAssessment = IIF(TechnicalAssignmentAssessment IS NOT NULL,  '(sample Technical Assignment Assessment)', NULL),
   TeamProjectSuitabilityAssessment = IIF(TeamProjectSuitabilityAssessment IS NOT NULL, '(sample Team Project Suitability Assessment)', NULL),
   EnglishUsageAssessment = IIF(EnglishUsageAssessment IS NOT NULL, '(sample English Usage Assessment)', NULL),
   RiskAssessment = IIF(RiskAssessment IS NOT NULL, '(sample Risk Assessment)', NULL),
   StrengthAssessment = IIF(StrengthAssessment IS NOT NULL, '(sample Strength Assessment)', NULL),
   OtherRemarks = IIF(OtherRemarks IS NOT NULL, '(sample Other Remarks)', NULL)

UPDATE Recruitment.RecruitmentProcessStep SET
   AssignedEmployeeId = (SELECT TOP 1 Id FROM Allocation.Employee ORDER BY NEWID()),
   DecisionComment = IIF(DecisionComment IS NOT NULL, '(sample Decision Comment)', NULL),
   RecruitmentHint = IIF(RecruitmentHint IS NOT NULL, '(sample Recruitment Hint)', NULL)

UPDATE Recruitment.RecruitmentProcessNoteMessageMentions SET
   EmployeeId = (SELECT TOP 1 Id FROM Allocation.Employee ORDER BY NEWID())

UPDATE Recruitment.RecruitmentProcessNote SET
   Content = IIF(Content IS NOT NULL,  '(sample Content)', NULL)

-- Recruiters temporary table
DECLARE @RecruiterEmployees TABLE ( [Id] BIGINT NOT NULL )
INSERT INTO @RecruiterEmployees
SELECT E.Id FROM Allocation.Employee E
JOIN Accounts.[User] U ON u.Id = E.UserId
JOIN Accounts.UserSecurityProfiles SP ON SP.UserId = U.Id
JOIN Accounts.SecurityProfile P ON P.Id = SP.ProfileId
WHERE P.ActiveDirectoryGroupName = (SELECT [Value] FROM Configuration.SystemParameter WHERE [Key] = 'Organization.RecruitmentSpecialistAdGroup')

UPDATE Recruitment.RecruitmentProcess SET   
   ClosedComment = IIF(ClosedComment IS NOT NULL, '(sample Closed Comment)', NULL),
   IsMatureEnoughForHiringManager = (SELECT CAST(ROUND(RAND(),0) AS BIT))   
   
UPDATE Recruitment.RecruitmentProcess 
   SET RecruiterId = RandomizeRecruiters.RecruiterId
FROM (
      SELECT 
            Jo.Id AS RecruitmentProcessId, 
            ROU.Id AS RecruiterId
        FROM (
            SELECT Id, 
                  ABS(CHECKSUM(NewId())) % (SELECT Count(1) - 1 FROM @RecruiterEmployees) + 1 AS RpRandomRow 
              FROM Recruitment.RecruitmentProcess
             ) AS JO
         JOIN ( 
            SELECT 
                   ROW_NUMBER() OVER(ORDER BY NEWID()) AS RecruiterRandomRow , 
                   Id AS Id
              FROM @RecruiterEmployees
            ) AS ROU ON JO.RpRandomRow = ROU.RecruiterRandomRow
      ) AS RandomizeRecruiters
WHERE Recruitment.RecruitmentProcess.Id = RandomizeRecruiters.RecruitmentProcessId
   
UPDATE Recruitment.RecruitmentProcessFinal SET
   PositionId = IIF(PositionId IS NULL, NULL, (SELECT TOP 1 Id FROM SkillManagement.JobTitle ORDER BY NEWID())),
   TrialContractTypeId = IIF(TrialContractTypeId IS NULL, NULL, (SELECT TOP 1 Id FROM TimeTracking.ContractType ORDER BY NEWID())),
   LongTermContractTypeId = IIF(LongTermContractTypeId IS NULL, NULL, (SELECT TOP 1 Id FROM TimeTracking.ContractType ORDER BY NEWID())),
   TrialSalary = IIF(TrialSalary IS NULL, NULL, '(sample Trial Salary)'),
   LongTermSalary = IIF(LongTermSalary IS NULL, NULL, '(sample Long Term Salary)'),
   Comment = IIF(Comment IS NULL, NULL, '(sample Comment)')
   
UPDATE Recruitment.RecruitmentProcessOffer SET
   PositionId = IIF(PositionId IS NULL, NULL, (SELECT TOP 1 Id FROM SkillManagement.JobTitle ORDER BY NEWID())),
   ContractType = IIF(ContractType IS NULL, NULL, '(sample Contract Type)'),
   Salary = IIF(Salary IS NULL, NULL, '(sample Salary)'),
   Comment = IIF(Comment IS NULL, NULL, '(sample Comment)')

UPDATE Recruitment.RecruitmentProcessScreening SET
   GeneralOpinion = IIF(GeneralOpinion IS NULL, NULL, '(sample General Opinion)'),
   Motivation = IIF(Motivation IS NULL, NULL, '(sample Motivation)'),
   WorkplaceExpectations = IIF(WorkplaceExpectations IS NULL, NULL, '(sample Workplace Expectations)'),
   Skills = IIF(Skills IS NULL, NULL, '(sample Skills)'),
   LanguageEnglish = IIF(LanguageEnglish IS NULL, NULL, '(sample Language English)'),
   LanguageGerman = IIF(LanguageGerman IS NULL, NULL, '(sample Language German)'),
   LanguageOther = IIF(LanguageOther IS NULL, NULL, '(sample Language Other)'),
   ContractExpectations = IIF(ContractExpectations IS NULL, NULL, '(sample Contract Expectations)'),
   Availability = IIF(Availability IS NULL, NULL, '(sample Availability)'),
   OtherProcesses = IIF(OtherProcesses IS NULL, NULL, '(sample Other Processes)'),
   CounterOfferCriteria = IIF(CounterOfferCriteria IS NULL, NULL, '(sample Counter Offer Criteria)'),
   RelocationComment = IIF(RelocationComment IS NULL, NULL, '(sample Relocation Comment)'),
   BusinessTripsComment = IIF(BusinessTripsComment IS NULL, NULL, '(sample Business Trips Comment)'),
   EveningWorkComment = IIF(EveningWorkComment IS NULL, NULL, '(sample Evening Work Comment)'),
   WorkPermitComment = IIF(WorkPermitComment IS NULL, NULL, '(sample Work Permit Comment)')

UPDATE Recruitment.Candidate SET
   FirstName = (SELECT TOP 1 FirstName FROM Accounts.[User] ORDER BY NEWID()),
   LastName = (SELECT TOP 1 LastName FROM Accounts.[User] ORDER BY NEWID()),
   MobilePhone = '(sample Mobile Phone)',
   OtherPhone = '(sample Other Phone)',
   EmailAddress = IIF(EmailAddress IS NULL, NULL, LOWER(FirstName) + '.' + LOWER(LastName) + CONVERT(NVARCHAR, Id) + '@intive.com'),
   OtherEmailAddress = NULL,
   SkypeLogin = IIF(SkypeLogin IS NULL, NULL, FirstName),
   SocialLink = NULL,
   RelocateDetails = NULL,
   ResumeTextContent = NULL,
   OriginalSourceComment = IIF(OriginalSourceComment IS NULL, NULL, '(sample Original Source Comment)'),
   IsHighlyConfidential = (SELECT CAST(ROUND(RAND(),0) AS BIT))
   
UPDATE Recruitment.ProjectDescription SET
   AboutClient = IIF(AboutClient IS NULL, NULL, '(sample About Client)'),
   SpecificDuties = IIF(SpecificDuties IS NULL, NULL, '(sample Specific Duties)'),
   ToolsAndEnvironment = IIF(ToolsAndEnvironment IS NULL, NULL, '(sample Tools And Environment)'),
   AboutTeam = IIF(AboutTeam IS NULL, NULL,  '(sample About Team)'),
   ProjectTimeline = IIF(ProjectTimeline IS NULL, NULL, '(sample Project Timeline)'),
   Methodology = IIF(Methodology IS NULL, NULL, '(sample Methodology)'),
   RequiredSkills = IIF(RequiredSkills IS NULL, NULL, '(sample Required Skills)'),
   AdditionalSkills = IIF(AdditionalSkills IS NULL, NULL, '(sample Additional Skills)'),
   RecruitmentDetails = IIF(RecruitmentDetails IS NULL, NULL, '(sample Recruitment Details)'),
   TravelAndAccomodation = IIF(TravelAndAccomodation IS NULL, NULL, '(sample Travel And Accomodation)'),
   RemoteWorkArrangements = IIF(RemoteWorkArrangements IS NULL, NULL, '(sample Remote Work Arrangements)'),
   Comments = IIF(Comments IS NULL, NULL, '(sample Comments)')

UPDATE Recruitment.JobOpeningNoteMessageMentions SET
   EmployeeId = (SELECT TOP 1 Id FROM Allocation.Employee ORDER BY NEWID())

UPDATE Recruitment.JobOpeningNote SET
   Content = IIF(Content IS NULL, NULL, '(sample Content)')

UPDATE Recruitment.JobOpening SET
   PositionName = IIF(PositionName IS NULL, NULL,  '(sample Position Name)'),
   CustomerId = NULL,
   NoticePeriod = NULL,
   SalaryRate = NULL,
   OtherLanguages = NULL,
   ClosedComment =  '(sample Closed Comment)',
   RejectionReason =  '(sample Rejection Reason)',
   IsIntiveResumeRequired = (SELECT CAST(ROUND(RAND(),0) AS BIT)),
   IsHighlyConfidential = (SELECT CAST(ROUND(RAND(),0) AS BIT)),
   TechnicalReviewComment = IIF(TechnicalReviewComment IS NULL, NULL, '(sample Technical Review Comment)')
      
UPDATE Recruitment.JobOpening 
   SET OrgUnitId = RandomizeOrgUnit.OrgUnitId
FROM (
      SELECT 
            Jo.Id AS JobOpeningId, 
            ROU.Id AS OrgUnitId
        FROM (
            SELECT Id, 
                  ABS(CHECKSUM(NewId())) % (SELECT Count(1) - 1 FROM Organization.OrgUnit) + 1 AS JoOrgUnitRandomRow 
              FROM Recruitment.JobOpening
             ) AS JO
         JOIN ( 
            SELECT 
                   ROW_NUMBER() OVER(ORDER BY NEWID()) AS OrgUnitRandomRow , 
                   Id AS Id
              FROM Organization.OrgUnit
            ) AS ROU ON JO.JoOrgUnitRandomRow = ROU.OrgUnitRandomRow
      ) AS RandomizeOrgUnit
WHERE Recruitment.JobOpening.Id = RandomizeOrgUnit.JobOpeningId

UPDATE Recruitment.JobOpening 
   SET CompanyId = RandomizeCompany.CompanyId
FROM (
      SELECT 
            Jo.Id AS JobOpeningId, 
            ROU.Id AS CompanyId
        FROM (
            SELECT Id, 
                  ABS(CHECKSUM(NewId())) % (SELECT Count(1) - 1 FROM Dictionaries.Company) + 1 AS JoRandomRow 
              FROM Recruitment.JobOpening
             ) AS JO
         JOIN ( 
            SELECT 
                   ROW_NUMBER() OVER(ORDER BY NEWID()) AS CompanyRandomRow , 
                   Id AS Id
              FROM Dictionaries.Company 
            ) AS ROU ON JO.JoRandomRow = ROU.CompanyRandomRow
      ) AS RandomizeCompany
WHERE Recruitment.JobOpening.Id = RandomizeCompany.JobOpeningId

UPDATE Recruitment.JobOpening 
   SET DecisionMakerEmployeeId = RandomizeRecruiters.RecruiterId
FROM (
      SELECT 
            Jo.Id AS JobOpeningId, 
            ROU.Id AS RecruiterId
        FROM (
            SELECT Id, 
                  ABS(CHECKSUM(NewId())) % (SELECT Count(1) - 1 FROM @RecruiterEmployees) + 1 AS JoRandomRow 
              FROM Recruitment.JobOpening
             ) AS JO
         JOIN ( 
            SELECT 
                   ROW_NUMBER() OVER(ORDER BY NEWID()) AS RecruiterRandomRow , 
                   Id AS Id
              FROM @RecruiterEmployees
            ) AS ROU ON JO.JoRandomRow = ROU.RecruiterRandomRow
      ) AS RandomizeRecruiters
WHERE Recruitment.JobOpening.Id = RandomizeRecruiters.JobOpeningId

UPDATE Recruitment.JobOpening 
   SET AcceptingEmployeeId = RandomizeRecruiters.RecruiterId
FROM (
      SELECT 
            Jo.Id AS JobOpeningId, 
            ROU.Id AS RecruiterId
        FROM (
            SELECT Id, 
                  ABS(CHECKSUM(NewId())) % (SELECT Count(1) - 1 FROM @RecruiterEmployees) + 1 AS JoRandomRow 
              FROM Recruitment.JobOpening
             ) AS JO
         JOIN ( 
            SELECT 
                   ROW_NUMBER() OVER(ORDER BY NEWID()) AS RecruiterRandomRow , 
                   Id AS Id
              FROM @RecruiterEmployees
            ) AS ROU ON JO.JoRandomRow = ROU.RecruiterRandomRow
      ) AS RandomizeRecruiters
WHERE Recruitment.JobOpening.Id = RandomizeRecruiters.JobOpeningId
AND Recruitment.JobOpening.AcceptingEmployeeId IS NOT NULL
   
DECLARE @JobOpeningProfiles TABLE ( [JobOpeningId] INT NOT NULL, [PositionName] NVARCHAR(MAX) )
INSERT INTO @JobOpeningProfiles
SELECT JO.Id, Jp.Name
FROM Recruitment.JobOpening AS JO
JOIN Recruitment.JobOpeningJobProfile AS JOJP ON JO.Id = JOJP.JobOpeningId
JOIN SkillManagement.JobProfile AS JP ON JP.Id = JOJP.JobProfileId

DECLARE @JobOpeningCities TABLE ( [JobOpeningId] INT NOT NULL, CityName NVARCHAR(MAX) )
INSERT INTO @JobOpeningCities
SELECT JO.Id, Jp.Name
FROM Recruitment.JobOpening AS JO
JOIN Recruitment.JobOpeningCities AS JOJP ON JO.Id = JOJP.JobOpeningId
JOIN Dictionaries.City AS JP ON JP.Id = JOJP.LocationId

UPDATE Recruitment.JobOpening
SET PositionName = JOProfileName.PostionName
FROM (
      SELECT 
            ProfileNames.[JobOpeningId],
            STUFF(
                  (
                   SELECT ', ' + PositionName  
                     FROM @JobOpeningProfiles
                    WHERE [JobOpeningId] = ProfileNames.[JobOpeningId] 
                      FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)'),1,   2,''
                  ) 
                  + ' - ' + ISNULL(MAX(CityNames.NameValues), 'All locations') AS PostionName
      FROM @JobOpeningProfiles ProfileNames
            LEFT JOIN (
            SELECT 
                 [JobOpeningId],
                 STUFF(
                     (
                      SELECT ', ' + CityName 
                        FROM @JobOpeningCities
                       WHERE [JobOpeningId] = Results.[JobOpeningId]
                        FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)'),1,2,'') AS NameValues
             FROM @JobOpeningCities Results
             GROUP BY [JobOpeningId]
      ) AS CityNames ON ProfileNames.JobOpeningId = CityNames.JobOpeningId
      GROUP BY ProfileNames.[JobOpeningId]
) AS JOProfileName
WHERE Recruitment.JobOpening.Id = JOProfileName.JobOpeningId
   
-- end of clearing data

UPDATE Compensation.Bonus SET 
   Justification = 'Anonymized to 100 PLN', 
   BonusAmount = 100

UPDATE Workflows.BonusRequest SET
   Justification = 'Anonymized to 100 PLN',
   BonusAmount = 100

DBCC SHRINKDATABASE(InfernoAnonimization, 10)

DECLARE @afterBackup NVARCHAR(1024) = 'E:\Uploads\Inferno anonimized ' + CONVERT(NVARCHAR, GETDATE(), 105) +'.bak';

BACKUP DATABASE InfernoAnonimization
TO DISK = @afterBackup

USE [master]

DROP DATABASE InfernoAnonimization
