# WebApiDemo

##Run
* Open Atomic Solution in Visual Studio
* Run Application/WebApi by `right-click` -> `Debug` -> `Start new instance`
    * Ensure, the app is running on `localhost:5881`
    * Do not bother errors on first page.
    * OData is available on `http://localhost:5881/odata/`
    * Other resources are available on `http://localhost:5881/api/`
* Wait couple of seconds. Application is starting.
* Run Example/WebApiDemo by `right-click` -> `Debug` -> `Start new instance`

You should have the Demo Application ready to use in your browser.

## Using

You have basically 4 pages available in Demo:
* Welcome page
* Users view - custom grid
* Users view - 3rd party grid [GitHub - UI Grid OData](https://github.com/mirik123/ui-grid-odata)
* Login Page

Both user views are restricted, so only Administrator is able to see Users.

When you are not logged in, you are redirected to login page on each try to get to the restricted area.

Go to Login page, type credentials and go to Custom User View.
You can enter some filter to display only matching rows (more about filters [here](http://www.odata.org/documentation/odata-version-2-0/uri-conventions/#FilterSystemQueryOption))

Now open `UI Grid OData` and check out pagination sorting and filtering!

When you log out, you looses possibility to look at users.