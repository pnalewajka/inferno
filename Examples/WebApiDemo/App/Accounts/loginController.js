﻿'use strict';

usersModule.controller('loginController', ['$scope', '$timeout', '$window', '$http', '$location', 'accountsService',
    function ($scope, $timeout, $window, $http, $location, accountsService) {
        $scope.message = "Login";
        $scope.LoginMessage = "";
        $scope.errorDetails = [];
        $scope.loginFailed = false;
        $scope.loginSucceeded = false;

        this.credentials = {
            Login: "",
            Password: "",
            RememberMe: false
        }

        this.login = function (credentials) {
            var postData = "Login=" + credentials.Login + "&Password=" + credentials.Password + "&RememberMe=" + credentials.RememberMe;

            accountsService.login(postData)
                .then(
                    function success(response) {
                        $scope.loginFailed = false;
                        $scope.loginSucceeded = true;
                        $scope.LoginMessage = response.data.Message;
                        $timeout(function () {
                            $window.location = '#/';
                            $window.location.reload();
                        }, 2000);
                    },
                    function error(response) {
                        $scope.loginFailed = true;
                        $scope.loginSucceeded = false;

                        if (response.status == -1) {
                            $scope.LoginMessage = "Server not responding";
                        } else {
                            $scope.LoginMessage = response.data.Message;
                            for (var i = 0; i < response.data.Errors.length; i++) {
                                $scope.errorDetails[i] = response.data.Errors[i];
                            };
                        }
                    }
                );
        }
    }]);