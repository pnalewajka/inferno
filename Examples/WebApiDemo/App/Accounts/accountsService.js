﻿'use strict';

usersModule.factory('accountsService', function ($http) {

    var accountsService = {}

    accountsService.login = function (postData) {
        return $http({
            method: 'POST',
            url: 'http://localhost:5881/api/authentication/login',
            data: postData,
            withCredentials:true,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        });
    }

    accountsService.logout = function () {
        return $http({
            method: 'POST',
            url: 'http://localhost:5881/api/authentication/logout'
        });
    }

    accountsService.isAuthenticated = function () {
        return $http({
            method: 'GET',
            url: 'http://localhost:5881/api/authentication/IsAuthenticated',
            withCredentials:true
        });
    }

    return accountsService;
});