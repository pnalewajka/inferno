﻿'use strict';

usersModule.factory('usersService',  function ($http) {
    var usersService = {}

    usersService.getUsers = function (filter) {
        if (filter)
        {
            filter = "?$filter=" + filter;
        }

        return $http({
            method: 'GET',
            url: 'http://localhost:5881/odata/User'+filter
        });
    }

    return usersService;
});