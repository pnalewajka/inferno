﻿'use strict';

usersModule.controller('usersController', ['$scope', 'usersService', 'gridUtil',
    function ($scope, usersService, gridUtil) {

    $scope.myGrid = {enableFiltering: true,
        showGridFooter: true,
        showColumnFooter: true,
        enableColumnMenus: true,
        paginationPageSizes: [5, 50, 75],
        paginationPageSize: 5,

        odata: {
            metadatatype: 'xml',
            datatype: 'json',
            expandable: 'subgrid',
            entitySet: 'User',
            dataurl: "http://localhost:5881/odata/User",
            metadataurl: "http://localhost:5881/odata/$metadata#User",
            gencolumns: true
        }
    };

    $scope.myGrid.onRegisterApi = function (gridApi) {
        gridApi.expandable.on.rowExpandedStateChanged($scope, function (row) {
            gridUtil.logDebug('expanded: ' + row.entity.Description);
        });

        gridApi.odata.on.success($scope, function (grid) {
            gridUtil.logDebug('succeeded');
        });

        gridApi.odata.on.error($scope, function (data, message) {
            gridUtil.logError(message);
        });
    };

    $scope.message = "Now viewing users!";
    $scope.filter = '';
    $scope.users = {};
    this.getUsers = function () {
        var $btn = $('#loadBtn').button('loading');
        $scope.loadingMessage = "Loading... please wait.";
        $scope.users = usersService.getUsers($scope.filter).then(function (response) {
            $scope.users = response.data.value;
            $scope.loadingMessage = "Done! Got "+$scope.users.length+' rows';
            $btn.button('reset');
        },function (response) {
            $scope.loadingMessage = "Error when getting data...";
            console.log(response.error);
            $btn.button('reset');
        });
    }
}]);