﻿

var usersModule = angular.module('myApp',
    ['ngRoute', 'ngCookies', 'ui.grid', 'ui.grid.expandable', 'ui.grid.odata', 'ui.grid.pagination'])
    .config([
        '$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {
            this.authResolve = {
                    authorize: [
                        "accountsService", "$location", function (accountsService, $location) {
                            return accountsService.isAuthenticated()
                                .then(
                                    function (result) {
                                    },
                                    function () {
                                        $location.path("/login");
                                    });
                        }
                    ]
            };

            $routeProvider
                .when('/',
                {
                    templateUrl: 'App/Home/home.html',
                    controller: 'homeController'
                })
                .when('/users',
                {
                    templateUrl: 'App/Users/users.html',
                    controller: 'usersController',
                    resolve: this.authResolve
                })
                .when('/users2',
                {
                    templateUrl: 'App/Users/users2.html',
                    controller: 'usersController',
                    resolve: this.authResolve
                })
                .when('/login',
                {
                    templateUrl: 'App/Accounts/login.html',
                    controller: 'loginController'
                })
                .otherwise({
                    redirectTo: '/'
                });

            $httpProvider.defaults.useXDomain = true;
            $httpProvider.defaults.withCredentials = true;
            delete $httpProvider.defaults.headers.common['X-Requested-With'];
        }
    ])
    .controller('mainController',
    [
        '$scope', 'accountsService', function ($scope, accountsService) {
            $scope.message = "Main Content";
            $scope.User = "Guest";
            $scope.logged = false;

            $scope.logOut = function() {
                accountsService.logout().then(
                    function(response) {
                        $scope.loginReload();
                    }
                );
            }

            $scope.loginReload = function () {
                accountsService.isAuthenticated()
                .then(
                    function (result) {
                        $scope.User = result.data;
                        $scope.logged = true;
                    },
                    function () {
                        $scope.User = "Guest";
                        $scope.logged = false;
                    }
                );
            }

            $scope.loginReload();
        }
    ]);

