﻿using System.Web;
using NUnit.Framework;
using Smt.Atomic.Presentation.Renderers.Extensions;

namespace UnitTests.Application.Presentation.Extensions
{
    [TestFixture]
    public class HtmlStringExtensionsTest
    {
        [Test]
        [TestCase("", "")]
        [TestCase("$_>/\\aasd=+=+%a&af;awv", "$_>/\\aasd=+=+%a&af;awv")]
        [TestCase("asd<br>bcd", "asd\nbcd")]
        [TestCase("asd<br/>bcd", "asd\nbcd")]
        [TestCase("asd<br />bcd", "asd\nbcd")]
        [TestCase("asd&nbsp;bcd", "asd bcd")]
        [TestCase("asd &nbsp; bcd", "asd   bcd")]
        [TestCase("asd      bcd", "asd bcd")]
        [TestCase("<p a=\"asdasd\">asd &nbsp; b<br/>cd</p>", "asd   b\ncd")]
        public static void HtmlStringToPlainText(string input, string expectedOutput)
        {
            var result = new HtmlString(input).ToPlainText();

            Assert.AreEqual(expectedOutput, result);
        }
    }
}
