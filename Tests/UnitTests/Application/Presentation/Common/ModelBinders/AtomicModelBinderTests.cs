﻿using System.Collections.Specialized;
using System.Web.Mvc;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Moq;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.CrossCutting.Common.ViewModels;
using Smt.Atomic.Presentation.Common.ModelBinders;
using Smt.Atomic.Tests.Common;
using Smt.Atomic.Tests.Common.AutoMock;

namespace UnitTests.Application.Presentation.Common.ModelBinders
{
    [TestFixture]
    public class AtomicModelBinderTests
    {
        private ControllerContext _controllerContext;
        private ModelBindingContext _bindingContext;

        private AutoMockingContainer CreateContainer()
        {
            var config = new TestContainerConfiguration();
            config.Configure(ContainerType.UnitTests);

            return config.AutoMockingContainer;
        }

        private AtomicModelBinder CreateSut(WindsorContainer container)
        {
            var binder = new AtomicModelBinder();
            var formCollection = new NameValueCollection();
            var valueProvider = new NameValueCollectionValueProvider(formCollection, null);
            var metadata = ModelMetadataProviders.Current.GetMetadataForType(null, typeof(object));
            _bindingContext = new ModelBindingContext
            {
                ModelName = "",
                ValueProvider = valueProvider,
                ModelMetadata = metadata
            };
            _controllerContext = new ControllerContext();

            return binder;
        }

        [Test]
        public void BindModel_NoFactoryForViewModel_DefaultCalled()
        {
            // Arrange
            var container = CreateContainer();
            var binder = CreateSut(container);

            // Act
            var actual = binder.BindModel(_controllerContext, _bindingContext);

            // Assert
            Assert.That(actual, Is.Not.Null);
        }

        [Test]
        public void BindModel_FactoryForViewModelRegistered_FactoryCalled()
        {
            // Arrange
            var container = CreateContainer();
            var factory = new Mock<IViewModelFactory<object>>();
            container.Register(Component.For<IViewModelFactory<object>>().Instance(factory.Object));
            var binder = CreateSut(container);

            // Act
            binder.BindModel(_controllerContext, _bindingContext);

            // Assert
            factory.Verify(f => f.Create());
        }

        [Test]
        [TestCase(nameof(ViewModelWithAliases.OrdinaryName))]
        [TestCase("AliasedName1")]
        [TestCase("AliasedName2")]
        public void BindModel_PropertyWithAlias_AllNamesHandledCorrectly(string nameToUse)
        {
            // Arrange
            var expectedValue = "VALUE";
            var modelName = "model";
            var container = new WindsorContainer();
            var binder = new AtomicModelBinder();
            var formCollection = new NameValueCollection
            {
                [$"{modelName}.{nameToUse}"] = expectedValue
            };
            var valueProvider = new NameValueCollectionValueProvider(formCollection, null);
            var metadata = ModelMetadataProviders.Current.GetMetadataForType(null, typeof(ViewModelWithAliases));
            var bindingContext = new ModelBindingContext
            {
                ModelName = modelName,
                ValueProvider = valueProvider,
                ModelMetadata = metadata
            };
            var controllerContext = new ControllerContext();

            // Act
            var viewModel = (ViewModelWithAliases) binder.BindModel(controllerContext, bindingContext);

            // Assert
            Assert.That(viewModel.OrdinaryName, Is.EqualTo(expectedValue));
        }
    }
}