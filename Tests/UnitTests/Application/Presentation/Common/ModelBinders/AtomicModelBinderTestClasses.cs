﻿using Smt.Atomic.Presentation.Common.Attributes;

namespace UnitTests.Application.Presentation.Common.ModelBinders
{
    public class ViewModelWithAliases
    {
        [Alias("AliasedName1")]
        [Alias("AliasedName2")]
        public string OrdinaryName { get; set; }
    }
}