﻿using NUnit.Framework;
using Smt.Atomic.Presentation.Common.Helpers;

namespace UnitTests.Application.Presentation.Common.Helpers
{
    [TestFixture]
    public class HtmlMarkupHelperTests
    {
        [Test]
        public void ToInputValue_ShouldReturnTrue()
        {
            var result = HtmlMarkupHelper.ToInputValue(true);
            Assert.AreEqual("true", result);
        }

        [Test]
        public void ToInputValue_ShouldReturnFalse()
        {
            var result = HtmlMarkupHelper.ToInputValue(false);
            Assert.AreEqual("false", result);
        }

        [Test]
        public void AddCssClass_ShouldReturnNullForNullNull()
        {
            var result = HtmlMarkupHelper.AddCssClass(null, null);
            Assert.IsNull(result);
        }

        [Test]
        public void AddCssClass_ShouldReturnNewClassesForNullExistingClass()
        {
            const string classAClassB = "class-a class-b";

            var result = HtmlMarkupHelper.AddCssClass(null, classAClassB);
            Assert.AreEqual(classAClassB, result);
        }

        [Test]
        public void AddCssClass_ShouldReturnFirstParameterForNullNewClass()
        {
            const string cssClass = "class-a";

            var result = HtmlMarkupHelper.AddCssClass(cssClass, null);
            Assert.AreEqual(cssClass, result);
        }

        [Test]
        public void AddCssClass_ShouldReturnFirstParameterForEmptyNewClass()
        {
            const string cssClass = "class-a";

            var result = HtmlMarkupHelper.AddCssClass(cssClass, string.Empty);
            Assert.AreEqual(cssClass, result);
        }


        [Test]
        public void AddCssClass_ShouldReturnFirstParameterForNewClassOnlyWhitespace()
        {
            const string cssClass = "class-a";

            var result = HtmlMarkupHelper.AddCssClass(cssClass, "   ");
            Assert.AreEqual(cssClass, result);
        }

        [Test]
        public void AddCssClass_ShouldIgnoreExistingClassName()
        {
            const string cssClass = "class-a";

            var result = HtmlMarkupHelper.AddCssClass(cssClass, cssClass);
            Assert.AreEqual(cssClass, result);
        }

        [Test]
        public void AddCssClass_ShouldIgnoreExistingClassName2()
        {
            const string cssClass = "class-a class-b";

            var result = HtmlMarkupHelper.AddCssClass(cssClass, "class-b");
            Assert.AreEqual(cssClass, result);
        }

        [Test]
        public void AddCssClass_ShouldIgnoreExistingClassName3()
        {
            const string cssClass = "class-a class-b";

            var result = HtmlMarkupHelper.AddCssClass(cssClass, cssClass);
            Assert.AreEqual(cssClass, result);
        }

        [Test]
        public void AddCssClass_ShouldOnlyAddNewClassNames()
        {
            var result = HtmlMarkupHelper.AddCssClass("class-a", "class-a class-b");
            Assert.AreEqual("class-a class-b", result);
        }

        [Test]
        public void AddCssClass_ShouldOnlyAddNewClassNames2()
        {
            var result = HtmlMarkupHelper.AddCssClass("class-a class-b", "class-b class-c");
            Assert.AreEqual("class-a class-b class-c", result);
        }

        [Test]
        public void AddCssClass_ShouldIgnoreNewExtraWhitespace()
        {
            var result = HtmlMarkupHelper.AddCssClass("class-a class-b", "  class-b   class-c   ");
            Assert.AreEqual("class-a class-b class-c", result);
        }

        [Test]
        public void AddCssClass_ShouldIngoreExistingExtraWhitespace()
        {
            var result = HtmlMarkupHelper.AddCssClass("   class-a         class-b   ", "class-b class-c");
            Assert.AreEqual("class-a class-b class-c", result);
        }

        [Test]
        public void AddCssClass_ShouldIgnoreRepeatedNewClasses()
        {
            var result = HtmlMarkupHelper.AddCssClass("class-a class-b", "class-c class-c");
            Assert.AreEqual("class-a class-b class-c", result);
        }


        [Test]
        public void AddCssClass_ShouldIgnoreRepeatedExistingClasses()
        {
            var result = HtmlMarkupHelper.AddCssClass("class-a class-a", "class-b class-c");
            Assert.AreEqual("class-a class-b class-c", result);
        }

        [Test]
        public void AddCssClass_ShouldAddAllNewClasses()
        {
            var result = HtmlMarkupHelper.AddCssClass("class-a class-b", "class-c class-d");
            Assert.AreEqual("class-a class-b class-c class-d", result);
        }
        
        [Test]
        public void GetAccelerator_ShouldReturnNullForNoAccelerator()
        {
            var result = HtmlMarkupHelper.GetAccelerator("some text");
            Assert.AreEqual(null, result);
        }

        [Test]
        public void GetAccelerator_ShouldReturnAcceleratorWhenGiven()
        {
            var result = HtmlMarkupHelper.GetAccelerator("some &text");
            Assert.AreEqual("t", result);
        }

        [Test]
        public void GetAccelerator_ShouldReturnFirstAccelerator()
        {
            var result = HtmlMarkupHelper.GetAccelerator("some &text with &extra markers");
            Assert.AreEqual("t", result);
        }

        [Test]
        public void HighlightAccelerator_ShouldReturnSameTextWhenNoAccelerator()
        {
            const string buttonText = "Some text";

            var result = HtmlMarkupHelper.HighlightAccelerator(buttonText);
            Assert.AreEqual(buttonText, result);
        }

        [Test]
        public void HighlightAccelerator_ShouldContainHightlightedAccelerator()
        {
            const string buttonText = "Some &text";

            var result = HtmlMarkupHelper.HighlightAccelerator(buttonText);
            
            Assert.AreNotEqual(buttonText, result);
            Assert.IsTrue(result.Contains("<span class=\"accelerator\">t</span>"));
        }

        [Test]
        public void RemoveAccelerator_ShouldReturnSameTextWhenNoAccelerator()
        {
            const string buttonText = "Some text";

            var result = HtmlMarkupHelper.RemoveAccelerator(buttonText);
            Assert.AreEqual(buttonText, result);
        }

        [Test]
        public void RemoveAccelerator_ShouldRemoveAcceleratorMarkersWhenPresent()
        {
            const string buttonText = "Some &text with &accelerator markers";

            var result = HtmlMarkupHelper.RemoveAccelerator(buttonText);
            Assert.AreEqual("Some text with accelerator markers", result);
        }
    }
}