﻿using System;
using System.Web.Mvc;
using NUnit.Framework;
using Smt.Atomic.Presentation.Common.Helpers;
using UnitTests.Application.Presentation.Common.Helpers.AuxiliaryClasses;
using UnitTests.Application.Presentation.Common.Helpers.AuxiliaryClasses.Areas;
using UnitTests.Application.Presentation.Common.Helpers.AuxiliaryClasses.Areas.SomeArea;

namespace UnitTests.Application.Presentation.Common.Helpers
{
    [TestFixture]
    public class RoutingHelperTests
    {
        [Test, ExpectedException(typeof(ArgumentNullException))]
        public void GetValueDictionary_ShouldThrowExceptionForNullParameters()
        {
            RoutingHelper.GetValueDictionary(null);
        }

        [Test, ExpectedException(typeof(ArgumentException))]
        public void GetValueDictionary_ShouldThrowExceptionForInvalidParameters()
        {
            RoutingHelper.GetValueDictionary("a=b&");
        }

        [Test]
        public void GetValueDictionary_ShouldReturnEmptyDictionaryForEmptyString()
        {
            var result = RoutingHelper.GetValueDictionary(string.Empty);
            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public void GetValueDictionary_ShouldReturnTwoItemsForTwoParams()
        {
            var result = RoutingHelper.GetValueDictionary("a=1&b=2");

            Assert.AreEqual(2, result.Count);

            Assert.IsTrue(result.ContainsKey("a"));
            Assert.IsTrue(result.ContainsKey("b"));

            Assert.AreEqual("1", result["a"]);
            Assert.AreEqual("2", result["b"]);
        }

        [Test, ExpectedException(typeof(ArgumentNullException))]
        public void GetControllerTypeName_ShouldThrowExceptionForNullParameter()
        {
            RoutingHelper.GetControllerTypeName(null);
        }

        [Test]
        public void GetControllerTypeName_ShouldExpandShorthandName()
        {
            var name = RoutingHelper.GetControllerTypeName("Some");
            Assert.AreEqual("SomeController", name);
        }

        [Test]
        public void GetControllerTypeName_ShouldLeaveFullNameAsIs()
        {
            const string controllerName = "SomeController";

            var name = RoutingHelper.GetControllerTypeName(controllerName);
            Assert.AreEqual(controllerName, name);
        }

        [Test, ExpectedException(typeof(ArgumentNullException))]
        public void GetAreaName_ShouldThrowExceptionForNullType()
        {
            RoutingHelper.GetAreaName((Controller)null);
        }

        [Test, ExpectedException(typeof(ArgumentNullException))]
        public void GetAreaName_ShouldThrowExceptionForNull()
        {
            RoutingHelper.GetAreaName((Type)null);
        }

        [Test]
        public void GetAreaName_ShouldReturnNullForNoAreaController()
        {
            var areaName = RoutingHelper.GetAreaName(typeof(FancySchmancyController));
            Assert.AreEqual(null, areaName);
        }

        [Test]
        public void GetAreaName_ShouldReturnNullForNoAreaNameController()
        {
            var areaName = RoutingHelper.GetAreaName(typeof(LostInSpaceController));
            Assert.AreEqual(null, areaName);
        }

        [Test]
        public void GetAreaName_ShouldReturnAreaForControllerWithArea()
        {
            var areaName = RoutingHelper.GetAreaName(typeof(PlainVanilaController));
            Assert.AreEqual("SomeArea", areaName);
        }
    }
}