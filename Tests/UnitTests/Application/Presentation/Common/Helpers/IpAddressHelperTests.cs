﻿using System.Web;
using Moq;
using NUnit.Framework;
using Smt.Atomic.Presentation.Common.Helpers;

namespace UnitTests.Application.Presentation.Common.Helpers
{
    [TestFixture]
    public class IpAddressHelperTests
    {
        private const string MalformedIpAddress = "BROKEN_IP_ADDRESS";
        private const string DefaultIpAddress = "0.0.0.0";
        private const string SmtIpAddress = "87.105.187.246";
        private const string PrivateIpAddress = "192.168.0.0";
        private const string PrivateIpAddress2 = "10.0.0.1";

// ReSharper disable InconsistentNaming
        private const string IPv4Loopback = "127.0.0.1";
        private const string IPv6Loopback = "::1";
// ReSharper restore InconsistentNaming

        [Test]
        public void GetClientIpAddress_PublicIp()
        {
            var moqObj = new Mock<HttpRequestBase>();
            moqObj.Setup(a => a.UserHostAddress).Returns(SmtIpAddress);
            moqObj.Setup(a => a.ServerVariables.Get(It.IsAny<string>())).Returns(string.Empty);
            Assert.AreEqual(SmtIpAddress, IpAddressHelper.GetClientIpAddress(moqObj.Object));
        }

        [Test]
        public void GetClientIpAddress_Malformed()
        {
            var moqObj = new Mock<HttpRequestBase>();
            moqObj.Setup(a => a.UserHostAddress).Returns(MalformedIpAddress);
            moqObj.Setup(a => a.ServerVariables.Get(It.IsAny<string>())).Returns(string.Empty);
            Assert.AreEqual(DefaultIpAddress, IpAddressHelper.GetClientIpAddress(moqObj.Object));
        }

        [Test]
        public void GetClientIpAddress_Proxy()
        {
            var moqObj = new Mock<HttpRequestBase>();
            moqObj.Setup(a => a.UserHostAddress).Returns(PrivateIpAddress);
            moqObj.Setup(a => a.ServerVariables.Get(It.IsAny<string>())).Returns(SmtIpAddress);
            Assert.AreEqual(SmtIpAddress, IpAddressHelper.GetClientIpAddress(moqObj.Object));
        }

        [Test]
        public void GetClientIpAddress_IPv6Loopback()
        {
            var moqObj = new Mock<HttpRequestBase>();
            moqObj.Setup(a => a.UserHostAddress).Returns(IPv6Loopback);
            moqObj.Setup(a => a.ServerVariables.Get(It.IsAny<string>())).Returns(string.Empty);
            Assert.AreEqual(IPv4Loopback, IpAddressHelper.GetClientIpAddress(moqObj.Object));
        }

        [Test]
        public void GetClientIpAddress_MultipleProxies()
        {
            var moqObj = new Mock<HttpRequestBase>();
            moqObj.Setup(a => a.UserHostAddress).Returns(PrivateIpAddress);
            moqObj.Setup(a => a.ServerVariables.Get(It.IsAny<string>())).Returns(SmtIpAddress + "," + PrivateIpAddress2);
            Assert.AreEqual(SmtIpAddress, IpAddressHelper.GetClientIpAddress(moqObj.Object));

            moqObj.Setup(a => a.UserHostAddress).Returns(PrivateIpAddress);
            moqObj.Setup(a => a.ServerVariables.Get(It.IsAny<string>())).Returns(PrivateIpAddress2 + "," + SmtIpAddress);
            Assert.AreEqual(SmtIpAddress, IpAddressHelper.GetClientIpAddress(moqObj.Object), "Different ordering");
        }

        [Test]
        public void IsPrivate_TestNegative()
        {
            const string publicAddress = "156.17.8.1";
            Assert.IsFalse(IpAddressHelper.IsPrivate(publicAddress));
        }

    }
}
