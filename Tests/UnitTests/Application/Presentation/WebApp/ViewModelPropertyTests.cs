﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Castle.Core.Internal;
using Moq;
using NUnit.Framework;
using Smt.Atomic.WebApp;
using System.Reflection;
using Castle.Windsor;
using Smt.Atomic.Business.Workflows.Interfaces;
using Smt.Atomic.Business.TimeTracking.Interfaces;
using Smt.Atomic.CrossCutting.Common.Services;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Business.Configuration.Interfaces;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Data.Jira.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Component = Castle.MicroKernel.Registration.Component;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace UnitTests.Application.Presentation.WebApp
{
    [TestFixture]
    public class ViewModelPropertyTests
    {
        private const int OutOfScopeMaxLengthRestrictionValue = -1;
        private const string ViewModelSufix = "ViewModel";
        private readonly ClassMappingFactory _classMappingFactory;

        public ViewModelPropertyTests()
        {
            var containerConfiguration = new WebAppContainerConfiguration();
            var windsorContainer = containerConfiguration.Configure(ContainerType.UnitTests);
            _classMappingFactory = new ClassMappingFactory(windsorContainer.Kernel);

            var timeServiceMock = new Mock<ITimeService>();
            timeServiceMock.Setup(m => m.GetCurrentTime()).Returns(()=>DateTime.Now);
            windsorContainer.Register(Component.For<ITimeService>().Instance(timeServiceMock.Object));

            RegisterMockInstance<IUnitOfWorkService<IAllocationDbScope>>(windsorContainer);
            RegisterMockInstance<IUnitOfWorkService<IWorkflowsDbScope>>(windsorContainer);
            RegisterMockInstance<IUnitOfWorkService<IAccountsDbScope>>(windsorContainer);
            RegisterMockInstance<IMessageTemplateService>(windsorContainer);
            RegisterMockInstance<IReliableEmailService>(windsorContainer);
            RegisterMockInstance<IRazorTemplateService>(windsorContainer);
            RegisterMockInstance<IPrincipalProvider>(windsorContainer);
            RegisterMockInstance<ITimeReportService>(windsorContainer);
            RegisterMockInstance<IRequestService>(windsorContainer);
            RegisterMockInstance<IJiraService>(windsorContainer);
            RegisterMockInstance<ISystemParameterService>(windsorContainer);
        }

        [Test]
        public void MaxLengthShouldBeConsistentBetweenViewModelsAndEntities()
        {
            var webAppAssembly = typeof (MvcApplication).Assembly;
            var types = webAppAssembly.GetTypes();
            var viewModelTypes = types.Where(t => t.Name.EndsWith(ViewModelSufix));

            var errors = new StringBuilder();

            foreach (var viewModelType in viewModelTypes)
            {
                var moduleName = GetModuleName(viewModelType);

                var entityName = GetEntityName(viewModelType);
                var dtoTypeName = GetDtoTypeName(moduleName, entityName);
                var entityTypeName = GetEntityTypeName(moduleName, entityName);
                
                var dtoType = TypeHelper.GetType(webAppAssembly, dtoTypeName);
                var entityType = TypeHelper.GetType(webAppAssembly, entityTypeName);

                if (dtoType != null && entityType != null)
                {
                    var result = VerifyConsistency(viewModelType, dtoType, entityType);
                    errors.Append(result);
                }
            }

            if (!string.IsNullOrEmpty(errors.ToString()))
            {
                throw new Exception("Some property max length inconsistencies found: \r\n\r\n" + errors);
            }
        }

        private void RegisterMockInstance<T>(WindsorContainer windsorContainer) where T : class
        {
            var mock = new Mock<T>();
            windsorContainer.Register(Component.For<T>().Instance(mock.Object));
        }

        private Type GetImplementationFor(Type interfaceType)
        {
            return TypeHelper.GetImplementationsOf(interfaceType).SingleOrDefault();
        }

        private StringBuilder VerifyConsistency(Type viewModelType, Type dtoType, Type entityType)
        {
            var errors = new StringBuilder();

            var entityToDtoMappingType = typeof(IClassMapping<,>).MakeGenericType(entityType, dtoType);
            var dtoToViewModelMappingType = typeof(IClassMapping<,>).MakeGenericType(dtoType, viewModelType);

            if (GetImplementationFor(entityToDtoMappingType)?.GetConstructor(Type.EmptyTypes) == null
                || GetImplementationFor(dtoToViewModelMappingType)?.GetConstructor(Type.EmptyTypes) == null)
            {
                return errors;
            }

            var entityToDtoMapping = _classMappingFactory.CreateMapping(entityType, dtoType);
            var dtoToViewModelMapping = _classMappingFactory.CreateMapping(dtoType, viewModelType);

            var entityToDtoFieldMapping = entityToDtoMapping.GetFieldMappings(entityType, dtoType);
            var dtoToViewModelFieldMapping = dtoToViewModelMapping.GetFieldMappings(dtoType, viewModelType);

            foreach (var entityPropertyInfo in entityType.GetProperties())
            {
                var maxLengthAttribute = entityPropertyInfo.GetAttribute<MaxLengthAttribute>();

                if (maxLengthAttribute != null)
                {
                    List<string> mappings;

                    if (entityToDtoFieldMapping.TryGetValue(entityPropertyInfo.Name, out mappings) && mappings.Count == 1)
                    {
                        var dtoPropertyName = mappings.Single();

                        if (dtoToViewModelFieldMapping.TryGetValue(dtoPropertyName, out mappings) && mappings.Count == 1)
                        {
                            var viewModelPropertyName = mappings.Single();
                            var viewModelPropertyInfo = viewModelType.GetProperty(viewModelPropertyName);

                            if (viewModelPropertyInfo != null)
                            {
                                var maximumLength = GetMaximumLengthValueBasedOnAttributes(viewModelPropertyInfo);

                                if (maximumLength > maxLengthAttribute.Length)
                                {
                                    var error =
                                        $"{entityType.Name}.{entityPropertyInfo.Name} => {viewModelType.Name}.{viewModelPropertyName}";

                                    errors.AppendLine(error);
                                }
                            }
                        }
                    }
                }
            }

            return errors;
        }

        private int GetMaximumLengthValueBasedOnAttributes(PropertyInfo viewModelPropertyInfo)
        {
            var stringLengthAttribute = viewModelPropertyInfo.GetAttribute<StringLengthAttribute>();

            if (stringLengthAttribute != null)
            {
                return stringLengthAttribute.MaximumLength;
            }

            var localizedStringLengthAttribute = viewModelPropertyInfo.GetAttribute<LocalizedStringLengthAttribute>();

            return localizedStringLengthAttribute != null ? localizedStringLengthAttribute.MaxLength 
                                                          : OutOfScopeMaxLengthRestrictionValue;
        }

        private static string GetEntityTypeName(string moduleName, string entityName)
        {
            return $"Smt.Atomic.Data.Entities.Modules.{moduleName}.{entityName}";
        }

        private static string GetDtoTypeName(string moduleName, string entityName)
        {
            return $"Smt.Atomic.Business.{moduleName}.Dto.{entityName}Dto";
        }

        private static string GetEntityName(Type viewModelType)
        {
            return viewModelType.Name.Replace(ViewModelSufix, string.Empty);
        }

        private static string GetModuleName(Type viewModelType)
        {
            var viewModelTypeName = viewModelType.FullName;
            var moduleName = viewModelTypeName
                .Replace("Smt.Atomic.WebApp.Areas.", string.Empty)
                .Replace(".Models." + viewModelType.Name, string.Empty);

            return moduleName;
        }
    }
}
