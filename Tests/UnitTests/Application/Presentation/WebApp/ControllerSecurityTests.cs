﻿using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Presentation.Common.Attributes;
using Smt.Atomic.Presentation.Common.Helpers;
using Smt.Atomic.Presentation.Renderers.Controllers;
using System;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace UnitTests.Application.Presentation.WebApp
{
    [TestFixture]
    public class ControllerSecurityTests
    {
        [Test]
        public void ControllerHasAuthorizationAttribute()
        {
            var webAppTypes = FunctionalHelper.Try(
                () => typeof(Smt.Atomic.WebApp.ServiceInstaller).Assembly.GetTypes(),
                () => Enumerable.Empty<Type>());

            var controllers = webAppTypes
                .Where(t => !t.IsAbstract)
                .Where(t => typeof(AtomicController).IsAssignableFrom(t));

            var unauthorizedControllers = controllers
                .Where(t => !t.IsDefined(typeof(AtomicAuthorizeAttribute))
                    && !t.IsDefined(typeof(ApiAuthorizeAttribute))
                    && !t.IsDefined(typeof(AllowAnonymousAttribute)));


            var controllersWithUnauthorizedActions = unauthorizedControllers
                
                .Where(t => t.GetMethods(BindingFlags.Public | BindingFlags.Instance)
                    .Where(m => typeof(ActionResult).IsAssignableFrom(m.ReturnType))
                    .Any(m => !m.GetCustomAttributes(typeof(AtomicAuthorizeAttribute), true).Any()
                        && !m.GetCustomAttributes(typeof(ApiAuthorizeAttribute), true).Any()
                        && !m.GetCustomAttributes(typeof(AllowAnonymousAttribute), true).Any()))
                .ToArray();

            Assert.IsFalse(controllersWithUnauthorizedActions.Any(),
                $"Errors: {controllersWithUnauthorizedActions.Length} controllers or methods do not have [AtomicAuthorizeAttribute] => " +
                string.Join($", {Environment.NewLine}", controllersWithUnauthorizedActions.Select(c => c.FullName)));
        }
    }
}
