﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Tests.Common;

namespace UnitTests.Application.Presentation.Renderers
{
    [TestFixture]
    public partial class ViewModelPropertyHelperTests
    {
        public ViewModelPropertyHelperTests()
        {
            var containerConfiguration = new TestContainerConfiguration();
            containerConfiguration.Configure(ContainerType.UnitTests);
            containerConfiguration.Register<IPropertyAccessProvider>(new PropertyAccessProvider());
        }
        
        [Test]
        public void GetPropertyAccessMode_TestPositive()
        {
            var roles =
                new List<SecurityRoleType> {SecurityRoleType.CanAddUsers, SecurityRoleType.CanEditUsers}.Select(
                    r => r.ToString()).ToHashSet();

            var nameAccessMode = SecurityHelper.GetPropertyAccessMode(PropertyHelper.GetProperty<TestViewModel>(v => v.Name), roles);
            var surnameAccessMode = SecurityHelper.GetPropertyAccessMode(PropertyHelper.GetProperty<TestViewModel>(v => v.Surname), roles);
            var totalValueAccessMode = SecurityHelper.GetPropertyAccessMode(PropertyHelper.GetProperty<TestViewModel>(v => v.TotalValue), roles);

            Assert.AreEqual(PropertyAccessMode.ReadWrite, nameAccessMode);
            Assert.AreEqual(PropertyAccessMode.ReadOnly, surnameAccessMode);
            Assert.AreEqual(PropertyAccessMode.NoAccess, totalValueAccessMode);
        }
    }
}
