﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Tests.Common;

namespace UnitTests.Application.Presentation.Renderers
{
    [TestFixture]
    public partial class AnonymizeViewModelServiceTests
    {
        public AnonymizeViewModelServiceTests()
        {
            var containerConfiguration = new TestContainerConfiguration();
            containerConfiguration.Configure(ContainerType.UnitTests);
            containerConfiguration.Register<IPropertyAccessProvider>(new PropertyAccessProvider());
        }

        [Test]
        public void Anonymize_TestPositive()
        {
            var roles = new List<SecurityRoleType> {SecurityRoleType.CanAddUsers, SecurityRoleType.CanDeleteUsers};
            var principalProviderMock = new Mock<IPrincipalProvider>();
            var principalMock = new Mock<IAtomicPrincipal>();
            principalMock.Setup(p => p.Roles).Returns(roles.Select(r => r.ToString()).ToArray());
            principalProviderMock.Setup(p => p.Current).Returns(principalMock.Object);

            var anonymizeViewModelService = new AnonymizationService(principalProviderMock.Object);

            const string expectedName = "Name";
            const string expectedSurname = "Surname";
            const string expectedNotProtectedName = "NotProtectedName";

            var viewModel = new TestViewModel
            {
                Date = new DateTime(2015, 1, 1),
                Income = 100,
                FirstName = expectedName,
                LastName = expectedSurname,
                NotProtectedName = expectedNotProtectedName,
                Status = TestEnum1.Option2,
                TotalValue = 15,
                Items = new List<TestItemViewModel> {new TestItemViewModel {Name = "name"}}
            };

            anonymizeViewModelService.Anonymize(viewModel);

            Assert.AreNotEqual(TestEnum1.Option1, viewModel.Status);
            Assert.AreNotEqual(TestEnum1.Option2, viewModel.Status);
            Assert.AreEqual(expectedName, viewModel.FirstName);
            Assert.AreEqual(expectedNotProtectedName, viewModel.NotProtectedName);
            Assert.AreEqual(string.Empty, viewModel.LastName);
            Assert.AreEqual(0, viewModel.Income);
            Assert.AreEqual(DateTime.MinValue, viewModel.Date);
            Assert.AreEqual(0, viewModel.Items.Count);
        }
    }
}
