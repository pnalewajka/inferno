﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace UnitTests.Application.Presentation.Renderers
{
    public partial class ViewModelPropertyHelperTests
    {
        public class TestViewModel
        {
            [RoleRequired(SecurityRoleType.CanAddUsers, SecurityRoleType.CanEditUsers)]
            public string Name { get; set; }

            [RoleRequired(SecurityRoleType.CanAddUsers, SecurityRoleType.CanChangeOwnPassword)]
            public string Surname { get; set; }

            [RoleRequired(SecurityRoleType.CanChangeOwnPassword, SecurityRoleType.CanChangeOwnPassword)]
            public int? TotalValue { get; set; }
        }
    }
}
