﻿using System;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace UnitTests.Application.Presentation.Renderers
{
    public partial class AnonymizeViewModelServiceTests
    {
        public class TestViewModel
        {
            public string NotProtectedName { get; set; }

            [RoleRequired(SecurityRoleType.CanAddUsers, SecurityRoleType.CanEditUsers)]
            public string FirstName { get; set; }

            [RoleRequired(SecurityRoleType.CanChangeOwnPassword, SecurityRoleType.CanChangeOwnPassword)]
            public string LastName { get; set; }

            [RoleRequired(SecurityRoleType.CanChangeOwnPassword, SecurityRoleType.CanChangeOwnPassword)]
            public DateTime Date { get; set; }

            [RoleRequired(SecurityRoleType.CanChangeOwnPassword, SecurityRoleType.CanChangeOwnPassword)]
            public decimal Income { get; set; }

            [RoleRequired(SecurityRoleType.CanChangeOwnPassword, SecurityRoleType.CanChangeOwnPassword)]
            public List<TestItemViewModel> Items { get; set; }

            [RoleRequired(SecurityRoleType.CanChangeOwnPassword, SecurityRoleType.CanChangeOwnPassword)]
            public TestEnum1 Status { get; set; }

            //Nullable
            [RoleRequired(SecurityRoleType.CanChangeOwnPassword, SecurityRoleType.CanChangeOwnPassword)]
            public int? TotalValue { get; set; }
        }

        public class TestItemViewModel
        {
            public string Name { get; set; }
        }

        public enum TestEnum1
        {
            Option1,
            Option2
        }
    }
}
