﻿using System;

namespace UnitTests.Application.Presentation.Renderers.Attributes
{
    public partial class RequiredEnumAttributeTests
    {
        enum TestEnum : long
        {
            None = 1,
            First = 2,
            Last = 1000
        }

        [Flags]
        enum TestFlaggedEnum : long
        {
            Red = 1,
            Blue = 2,
            Green = 4
        }
    }
}
