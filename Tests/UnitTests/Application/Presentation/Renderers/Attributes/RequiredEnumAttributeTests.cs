﻿using System;
using NUnit.Framework;
using Smt.Atomic.Presentation.Renderers.Attributes;

namespace UnitTests.Application.Presentation.Renderers.Attributes
{
    [TestFixture]
    public partial class RequiredEnumAttributeTests
    {
        [Test]
        public void IsValid_TestPositive()
        {
            var attribute = new RequiredEnumAttribute(typeof (TestEnum));
            var testValue = (object) TestEnum.First;

            Assert.IsTrue(attribute.IsValid(testValue));
        }

        [Test]
        public void IsValid_TestPositiveWithEmptyValue()
        {
            var attribute = new RequiredEnumAttribute(typeof(TestEnum), TestEnum.None);
            var testValue = (object) TestEnum.Last;

            Assert.IsTrue(attribute.IsValid(testValue));
        }

        [Test]
        [ExpectedException(typeof(NotSupportedException))]
        public void Constructor_TestEnumTypeRestriction()
        {
            var attribute = new RequiredEnumAttribute(typeof(RequiredEnumAttributeTests));
        }

        [Test]
        public void IsValid_TestPositiveFlaggedEnum()
        {
            var attribute = new RequiredEnumAttribute(typeof(TestFlaggedEnum));
            var testValue = TestFlaggedEnum.Blue | TestFlaggedEnum.Green | TestFlaggedEnum.Red;

            Assert.IsTrue(attribute.IsValid(testValue));
        }

        [Test]
        public void IsValid_TestNegativeFlaggedEnum()
        {
            var attribute = new RequiredEnumAttribute(typeof(TestFlaggedEnum));
            TestFlaggedEnum testValue = 0;

            Assert.IsFalse(attribute.IsValid(testValue));
        }
    }
}
