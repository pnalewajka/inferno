﻿using System;
using System.Web.Mvc;
using Moq;
using NUnit.Framework;
using Smt.Atomic.WebApp.Extensions;

namespace UnitTests.Application.WebApp.Extensions
{
    [TestFixture]
    public class HtmlHelperExtensionsTests
    {
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConvertNewLinesToBrTag_HtmlHelperIsNull_ShouldThrowException()
        {
            var returnValue = HtmlHelperExtensions.ConvertNewLinesToBrTag(null, "some_input");
        }

        [Test]
        public void ConvertNewLinesToBrTag_ConvertMultilineInput()
        {
            // arrange
            var input = "line1\nline2\r\nline3";
            var expected = "line1<br>line2<br>line3";

            // act
            var output = HtmlHelperExtensions.ConvertNewLinesToBrTag(GetHtmlHelperMock(), input).ToHtmlString();

            // assert
            Assert.AreEqual(expected, output);
        }

        private HtmlHelper GetHtmlHelperMock()
        {
            var moqViewDataContainer = new Mock<IViewDataContainer>();
            var htmlHelper = new HtmlHelper(new ViewContext(), moqViewDataContainer.Object);
            return htmlHelper;
        }

        [Test]
        public void StripHtml_RemoveTagsAndSpecialEntitiesFromHtml()
        {
            var input = "<root>line1&nbsp;<Item>line2=&quot;quota&quot;</item>line3 &amp; &lt;&gt;</root>";
            var expected = "line1 line2=\"quota\"line3 & <>";

            var output = HtmlHelperExtensions.StripHtml(input);

            Assert.AreEqual(expected, output);
        }
    }
}
