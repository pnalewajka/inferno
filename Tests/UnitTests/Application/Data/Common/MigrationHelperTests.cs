﻿using System.Linq;
using NUnit.Framework;
using Smt.Atomic.Data.Common.Helpers;

namespace UnitTests.Application.Data.Common
{
    [TestFixture]
    public class MigrationHelperTests
    {
        [Test]
        public void SplitStringByGo_TestPositve()
        {
            const string sqlStatement = @"
                CREATE TABLE test.test (
                    id int
                )
                GO
                INSERT INTO test.test SELECT 1
                GO
            ";
            var splitItems = MigrationHelper.SplitStringByGo(sqlStatement);
            Assert.AreEqual(2, splitItems.Count());
        }

        [Test]
        public void ExecuteFromResources_TestPositive()
        {
            const string resourceWithSql = "UnitTests.Application.Data.Common.Resources.MigrationHelperTestsResource1.sql";
            const int expectedExecutionCount = 2;
            var counter = 0;
            MigrationHelper.ExecuteFromResources(GetType(), resourceWithSql, s=>counter++);
            Assert.AreEqual(expectedExecutionCount, counter);
        }
    }
}
