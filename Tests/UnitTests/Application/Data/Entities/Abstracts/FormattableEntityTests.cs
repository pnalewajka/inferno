﻿using NUnit.Framework;
using UnitTests.Application.Data.Entities.Abstracts.FormattableTestEntities;

namespace UnitTests.Application.Data.Entities.Abstracts
{
    [TestFixture]
    public class FormattableEntityTests
    {
        private static readonly NoPropertiesFormattableEntity NoProperties = new NoPropertiesFormattableEntity();

        private static readonly FormattableEntityWithId WithId = new FormattableEntityWithId {Id = 1};

        private static readonly FormattableEntityWithCode WithCode = new FormattableEntityWithCode
        {
            Id = 2,
            Code = "TestFormattable"
        };

        private static readonly FormattableEntityWithName WithName = new FormattableEntityWithName
        {
            Id = 3,
            Name = "Formattable named entity"
        };

        private static readonly FormattableEntityWithNameEn WithNameEn = new FormattableEntityWithNameEn
        {
            Id = 4,
            NameEn = "English entity"
        };

        private static readonly FormattableEntityWithFullName WithFullName = new FormattableEntityWithFullName
        {
            Id = 777,
            FullName = "John Smith"
        };

        [Test]
        public void GeneralFormat()
        {
            Assert.AreEqual("NoPropertiesFormattableEntity (hash=42)", NoProperties.ToString("G"));
            Assert.AreEqual("NoPropertiesFormattableEntity (hash=42)", NoProperties.ToString(null));
            Assert.AreEqual("NoPropertiesFormattableEntity (hash=42)", NoProperties.ToString());
            Assert.AreEqual("FormattableEntityWithId (id=1)", WithId.ToString("G"));
            Assert.AreEqual("FormattableEntityWithId (id=1)", WithId.ToString(null));
            Assert.AreEqual("FormattableEntityWithId (id=1)", WithId.ToString());
            Assert.AreEqual("FormattableEntityWithCode (code=TestFormattable)", WithCode.ToString("G"));
            Assert.AreEqual("FormattableEntityWithCode (code=TestFormattable)", WithCode.ToString(null));
            Assert.AreEqual("FormattableEntityWithCode (code=TestFormattable)", WithCode.ToString());
            Assert.AreEqual("Formattable named entity (id=3)", WithName.ToString("G"));
            Assert.AreEqual("Formattable named entity (id=3)", WithName.ToString(null));
            Assert.AreEqual("Formattable named entity (id=3)", WithName.ToString());
            Assert.AreEqual("English entity (id=4)", WithNameEn.ToString("G"));
            Assert.AreEqual("English entity (id=4)", WithNameEn.ToString(null));
            Assert.AreEqual("English entity (id=4)", WithNameEn.ToString());
            Assert.AreEqual("John Smith (id=777)", WithFullName.ToString("G"));
            Assert.AreEqual("John Smith (id=777)", WithFullName.ToString(null));
            Assert.AreEqual("John Smith (id=777)", WithFullName.ToString());
        }
        
        [Test]
        public void FullFormat()
        {
            Assert.AreEqual("NoPropertiesFormattableEntity (hash=42)", NoProperties.ToString("F"));
            Assert.AreEqual("FormattableEntityWithId (id=1)", WithId.ToString("F"));
            Assert.AreEqual("FormattableEntityWithCode (code=TestFormattable)", WithCode.ToString("F"));
            Assert.AreEqual("Formattable named entity (id=3)", WithName.ToString("F"));
            Assert.AreEqual("English entity (id=4)", WithNameEn.ToString("F"));
            Assert.AreEqual("John Smith (id=777)", WithFullName.ToString("F"));
        }
        
        [Test]
        public void NameFormat()
        {
            Assert.AreEqual("NoPropertiesFormattableEntity", NoProperties.ToString("N"));
            Assert.AreEqual("FormattableEntityWithId", WithId.ToString("N"));
            Assert.AreEqual("FormattableEntityWithCode", WithCode.ToString("N"));
            Assert.AreEqual("Formattable named entity", WithName.ToString("N"));
            Assert.AreEqual("English entity", WithNameEn.ToString("N"));
            Assert.AreEqual("John Smith", WithFullName.ToString("N"));
            Assert.AreEqual("Hello John Smith", $"Hello {WithFullName:N}");
        }

        [Test]
        public void Codeformat()
        {
            Assert.AreEqual("42", NoProperties.ToString("C"));
            Assert.AreEqual("1", WithId.ToString("C"));
            Assert.AreEqual("TestFormattable", WithCode.ToString("C"));
            Assert.AreEqual("3", WithName.ToString("C"));
            Assert.AreEqual("4", WithNameEn.ToString("C"));
            Assert.AreEqual("777", WithFullName.ToString("C"));
        }
    }
}
