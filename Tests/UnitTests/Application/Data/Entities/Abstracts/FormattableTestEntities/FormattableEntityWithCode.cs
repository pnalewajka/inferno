﻿namespace UnitTests.Application.Data.Entities.Abstracts.FormattableTestEntities
{
    internal class FormattableEntityWithCode : FormattableEntityWithId
    {
        public string Code { get; set; }
    }
}
