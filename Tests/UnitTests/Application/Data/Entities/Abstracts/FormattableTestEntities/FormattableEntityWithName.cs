﻿namespace UnitTests.Application.Data.Entities.Abstracts.FormattableTestEntities
{
    internal class FormattableEntityWithName : FormattableEntityWithId
    {
        public string Name { get; set; }
    }
}
