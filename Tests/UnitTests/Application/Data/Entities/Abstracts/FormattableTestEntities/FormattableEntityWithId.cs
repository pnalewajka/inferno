﻿namespace UnitTests.Application.Data.Entities.Abstracts.FormattableTestEntities
{
    internal class FormattableEntityWithId : NoPropertiesFormattableEntity
    {
        public long Id { get; set; }
    }
}
