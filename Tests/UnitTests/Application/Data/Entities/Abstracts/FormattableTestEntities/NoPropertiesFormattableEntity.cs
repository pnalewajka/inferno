﻿using Smt.Atomic.Data.Entities.Abstracts;

namespace UnitTests.Application.Data.Entities.Abstracts.FormattableTestEntities
{
    internal class NoPropertiesFormattableEntity : FormattableEntity
    {
        public override int GetHashCode()
        {
            return 42;
        }
    }
}
