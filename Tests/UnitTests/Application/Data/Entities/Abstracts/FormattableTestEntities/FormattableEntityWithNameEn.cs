﻿namespace UnitTests.Application.Data.Entities.Abstracts.FormattableTestEntities
{
    internal class FormattableEntityWithNameEn : FormattableEntityWithId
    {
        public string NameEn { get; set; }
    }
}
