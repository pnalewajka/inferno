﻿namespace UnitTests.Application.Data.Entities.Abstracts.FormattableTestEntities
{
    internal class FormattableEntityWithFullName : FormattableEntityWithId
    {
        public string FullName { get; set; }
    }
}
