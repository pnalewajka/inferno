﻿using System;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.Data.Entities.Modules.Configuration;
using Smt.Atomic.Tests.Common.Wrappers;

namespace UnitTests.Application.Data.Repositiories.Services
{
    internal class ConfigurationDatabaseMock : DatabaseMock
    {
        private readonly List<SystemParameter> _systemParameters = new List<SystemParameter>
        {
            new SystemParameter
            {
                Id = 1,
                Key = ParameterKeys.SystemTimeOffsetInSeconds,
                ValueType = "long",
                Value = "3600",
                ModifiedOn = DateTime.Now
            }
        };

        public ConfigurationDatabaseMock()
        {
            AddEntities(_systemParameters);
        }
    }
}
