﻿using Castle.MicroKernel.Registration;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Services;
using Smt.Atomic.Tests.Common;

namespace UnitTests.Application.Data.Repositiories.Services
{
    [TestFixture]
    public class RepositoryTests
    {
        private TestContainerConfiguration _testContainerConfiguration;

        [TestFixtureSetUp]
        public void Initialize()
        {
            _testContainerConfiguration = new TestContainerConfiguration();
            _testContainerConfiguration.Configure(ContainerType.UnitTests);

            var settingProvicerMoq = _testContainerConfiguration.AutoMockingContainer.GetMock<ISettingsProvider>();
            settingProvicerMoq.Setup(p => p.IsFakeTimeAllowed).Returns(true);
        }

        [Test]
        public void Delete_SoftDeletableEntity_EntityFixedBeforeDeletion()
        {
            // Arrange
            var softDeletableEntity = new SoftDeletableEntityMock();
            _testContainerConfiguration.AutoMockingContainer.Register(Component.For<IRepository<SoftDeletableEntityMock>>().ImplementedBy<Repository<SoftDeletableEntityMock>>());
            var repository = _testContainerConfiguration.AutoMockingContainer.Resolve<IRepository<SoftDeletableEntityMock>>();

            // Act
            repository.Delete(softDeletableEntity);

            // Assert
            Assert.That(softDeletableEntity.WasFixedBeforeSoftDeleting, Is.True);
            Assert.That(softDeletableEntity.IsDeleted, Is.True);
        }
    }
}