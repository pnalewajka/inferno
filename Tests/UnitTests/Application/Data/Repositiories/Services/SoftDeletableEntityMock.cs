﻿using Smt.Atomic.Data.Entities.Interfaces;

namespace UnitTests.Application.Data.Repositiories.Services
{
    public class SoftDeletableEntityMock : IEntity, ISoftDeletable
    {
        public long Id { get; set; }
        public bool IsDeleted { get; set; }

        public bool WasFixedBeforeSoftDeleting { get; set; }
        public void OnSoftDeleting()
        {
            WasFixedBeforeSoftDeleting = true;
        }
    }
}