﻿using System;
using Castle.DynamicProxy;
using Castle.MicroKernel.Registration;
using Moq;
using NUnit.Framework;
using Smt.Atomic.Business.Configuration.Services;
using Smt.Atomic.CrossCutting.Common.Caching;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Services;
using Smt.Atomic.Tests.Common;

namespace UnitTests.Application.Data.Repositiories.Services
{
    [TestFixture]
    public class DbTimeServiceTests
    {
        private TestContainerConfiguration _testContainerConfiguration;
        private ConfigurationDatabaseMock _databaseMock;

        [TestFixtureSetUp]
        public void Initialize()
        {
            _testContainerConfiguration = new TestContainerConfiguration();
            _testContainerConfiguration.Configure(ContainerType.UnitTests);
            _databaseMock = new ConfigurationDatabaseMock();
            _testContainerConfiguration.AutoMockingContainer.RegisterDatabase(_databaseMock);

            _testContainerConfiguration.AutoMockingContainer.Register(Component.For<ISystemSwitchService>().ImplementedBy<SystemSwitchService>());
            _testContainerConfiguration.AutoMockingContainer.Register(Component.For<IInterceptor>().ImplementedBy<CacheInterceptor>());
            _testContainerConfiguration.AutoMockingContainer.GetMock<IDbContextWrapperFactory>().Setup(m => m.Create()).Returns(new Mock<IDbContextWrapper>().Object);

            var settingProvicerMoq = _testContainerConfiguration.AutoMockingContainer.GetMock<ISettingsProvider>();
            settingProvicerMoq.Setup(p => p.IsFakeTimeAllowed).Returns(true);
        }

        [Test]
        public void GetOffset_TestParameterRetrievalAndOffsetCalculation()
        {
            var timeSpan = new TimeSpan(0, 1, 0, 0);

            _testContainerConfiguration.AutoMockingContainer.Register(Component.For<ITimeService>().ImplementedBy<DbTimeService>());
            _testContainerConfiguration.AutoMockingContainer.Register(Component.For(typeof(IReadOnlyUnitOfWorkService<>)).ImplementedBy(typeof(ReadOnlyUnitOfWorkService<>)).LifestyleTransient());
            _testContainerConfiguration.AutoMockingContainer.Register(Component.For(typeof(IReadOnlyRepository<>)).UsingFactoryMethod((k, c) => _databaseMock.GetReadOnlyRepositoryMockFactoryMethod(c.RequestedType.GenericTypeArguments[0])));
            _testContainerConfiguration.AutoMockingContainer.Register(Component.For(typeof(IRepository<>)).UsingFactoryMethod((k, c) => _databaseMock.GetRepositoryMockFactoryMethod(c.RequestedType.GenericTypeArguments[0])));
            _testContainerConfiguration.AutoMockingContainer.Register(Component.For<CacheInterceptor>().LifestyleTransient().Named("Cache"));

            //_testContainerConfiguration.AutoMockingContainer.Register(Component.For<ISystemSwitchService>().ImplementedBy())

            var timeService = _testContainerConfiguration.AutoMockingContainer.Resolve<ITimeService>();

            var offset = timeService.GetOffset();
            Assert.IsTrue(offset.HasValue);
            Assert.AreEqual(timeSpan, offset.Value);
        }
    }
}
