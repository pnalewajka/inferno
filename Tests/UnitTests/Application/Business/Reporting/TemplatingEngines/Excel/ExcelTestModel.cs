﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel;

namespace UnitTests.Application.Business.Reporting.TemplatingEngines.Excel
{
    class ExcelTestModel
    {
        public Subclass1 SingleValue { get; set; }

        public IEnumerable<Subclass1> Collection1 { get; set; }

        public IEnumerable<Subclass2> Collection2 { get; set; }

        public IEnumerable<int> Collection3 { get; set; }

        public IEnumerable<DataColumn> MultiColumns1 { get; set; }

        public IEnumerable<DataColumn> MultiColumns2 { get; set; }

        public IEnumerable<Subclass3> MultiColumnsCollection { get; set; }

        public IEnumerable<Subclass1> EmptyCollection => Enumerable.Empty<Subclass1>();

        public IEnumerable<DataColumn> EmptyColumns => Enumerable.Empty<DataColumn>();

        public class Subclass1
        {
            public int Value { get; set; }
        }
        
        public class Subclass2
        {
            private readonly int _increment;

            public Subclass2(int increment)
            {
                _increment = increment;
            }


            public Subclass1 Get(int value)
            {
                return new Subclass1 {Value = value + _increment};
            }
        }

        public class Subclass3
        {
            public IEnumerable<DataColumn> Columns { get; set; }
        }
    }
}
