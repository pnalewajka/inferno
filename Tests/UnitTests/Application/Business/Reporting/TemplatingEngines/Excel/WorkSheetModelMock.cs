﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel.Model;

namespace UnitTests.Application.Business.Reporting.TemplatingEngines.Excel
{
    class WorkSheetModelMock : IWorkSheetModel
    {
        public List<List<object>> Rows { get; set; }

        public IEnumerable<TemplateTag> GetTemplateTags()
        {
            return Rows
                .Select((r, i) => new
                {
                    RowId = i + 1,
                    Columns = r
                })
                .SelectMany(x => x.Columns.Select((c, i) => new
                {
                    ColumnId = i + 1,
                    x.RowId,
                    Value = c as string
                }))
                .Where(x => x.Value != null)
                .Select(x =>
                    ((TemplateTag) CollectionTemplateTag.TryParse(x.ColumnId, x.RowId, x.Value)) ??
                    ObjectTemplateTag.TryParse(x.ColumnId, x.RowId, x.Value))
                .Where(t => t != null);
        }

        public object GetValue(int columnId, int rowId)
        {
            return Rows[rowId - 1][columnId - 1];
        }

        public void SetValue(int columnId, int rowId, object value)
        {
            Rows[rowId -1 ][columnId - 1] = value;
        }

        public void SetHeaderValue(int columnId, int rowId, string value)
        {
            SetValue(columnId, rowId, value);
        }

        public void InsertColumns(int columnId, int count)
        {
            foreach (var row in Rows)
            {
                var n = count;
                while (n-- > 0)
                {
                    row.Insert(columnId - 1, null);
                }
            }
        }

        public void InsertRows(int rowId, int count)
        {
            var columnCount = Rows.First().Count;

            while (count-- > 0)
            {
                Rows.Insert(rowId, Enumerable.Repeat<object>(null, columnCount).ToList());
            }
        }

        public void RemoveColumn(int columnId)
        {
            foreach (var row in Rows)
            {
                row.RemoveAt(columnId - 1);
            }
        }

        public void RemoveRow(int rowId)
        {
            Rows.RemoveAt(rowId - 1);
        }
    }
}
