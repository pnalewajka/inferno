﻿using DocumentFormat.OpenXml.Spreadsheet;
using OfficeOpenXml;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Reporting.Dto;
using Smt.Atomic.Business.Reporting.Interfaces;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel;
using System.Collections.Generic;
using System.IO;

namespace UnitTests.Application.Business.Reporting.TemplatingEngines.Excel
{
    internal class ExcelReportingTester
    {
        private class FakeDataSource : IReportDataSource
        {
            private readonly object _model; 

            public FakeDataSource(
                object model)
            {
                _model = model;
            }

            public object GetData(
                IReportGenerationContext reportGenerationContext)
            {
                return _model;
            }
        }

        private readonly ExcelGenerationService _service;
        private ExcelPackage _template;

        public ExcelReportingTester()
        {
            _service = new ExcelGenerationService();
        }

        public ExcelWorksheet CreateTemplate()
        {
            _template = new ExcelPackage();

            return _template.Workbook.Worksheets.Add("TemplateTest");
        }

        public ExcelWorksheet GenerateReport(ExcelWorksheet template, object model)
        {
            using (var stream = new MemoryStream())
            {
                _template.SaveAs(stream);

                var dataSource = new FakeDataSource(model);
                var context = new ReportGenerationContext<object>(dataSource, null, null, new ReportDefinitionDto
                {
                    ReportTemplates = new[] { new DocumentDto { DocumentId = 0 } },
                });

                context.TemplateContents = new Dictionary<long, byte[]>{ { 0, stream.ToArray() } };

                var reportStream = _service.GenerateContent(context);
                var report = new ExcelPackage(reportStream);

                return report.Workbook.Worksheets["TemplateTest"];
            }
        }
    }
}
