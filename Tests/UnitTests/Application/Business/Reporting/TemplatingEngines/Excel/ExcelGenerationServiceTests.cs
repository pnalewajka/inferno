﻿using NUnit.Framework;
using OfficeOpenXml.Style;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel;
using System.Collections.Generic;
using Smt.Atomic.Business.Reporting.TemplatingEngines.Excel.TagProcessing;

namespace UnitTests.Application.Business.Reporting.TemplatingEngines.Excel
{
    [TestFixture]
    public class ExcelGenerationServiceTests
    {
        [Test]
        public void TestTemplateTagsProcessing_TestTemplateTagProcessing()
        {
            var model = CreateTestModel();
            var workSheet = CreateInitialWorksheet();
            
            new RowExtendingTagProcessor(workSheet, model).Process();

            CollectionAssert.AreEqual(ContentAfterRowExtending(), workSheet.Rows);

            new ColumnExtendingTagProcessor(workSheet, model).Process();

            CollectionAssert.AreEqual(ContentAfterColumnExtending(), workSheet.Rows);

            new ValueReplacingTagProcessor(workSheet, model).Process();

            CollectionAssert.AreEqual(ContentAfterValueReplacing(), workSheet.Rows);
        }

        [Test]
        public void GenerateReport_WhenTemplateContainsSingleValueMarker_ThenReplaceItWithValue()
        {
            var model = new { SingleValue = "ExpectedText" };

            var tester = new ExcelReportingTester();
            var template = tester.CreateTemplate();
            template.Cells["A1"].Value = "{{SingleValue}}";

            var report = tester.GenerateReport(template, model);

            Assert.AreEqual("ExpectedText", report.Cells["A1"].Text);
        }

        [Test]
        public void GenerateReport_WhenTemplateContainsCollectionMarker_ThenAddRowForEachElementWithValue()
        {
            var model = new
            {
                Collection = new List<object>
                {
                    new { Item = "ExpectedText1" },
                    new { Item = "ExpectedText2" },
                    new { Item = "ExpectedText3" }
                }
            };

            var tester = new ExcelReportingTester();
            var template = tester.CreateTemplate();
            template.Cells["A1"].Value = "{{foreach Item in Collection}}";

            var report = tester.GenerateReport(template, model);

            Assert.AreEqual("ExpectedText1", report.Cells["A1"].Text);
            Assert.AreEqual("ExpectedText2", report.Cells["A2"].Text);
            Assert.AreEqual("ExpectedText3", report.Cells["A3"].Text);
        }

        [Test]
        public void GenerateReport_WhenTemplateContainsCellFormat_ThenKeepFormatInEachRow()
        {
            var model = new
            {
                Collection = new List<object>
                {
                    new { Item = 12000000 },
                    new { Item = 23000000 },
                    new { Item = 34000000 }
                }
            };

            var tester = new ExcelReportingTester();
            var template = tester.CreateTemplate();
            template.Cells["A1"].Style.Numberformat.Format = "#,, \\M";
            template.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            template.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
            template.Cells["A1"].Value = "{{foreach Item in Collection}}";

            var report = tester.GenerateReport(template, model);

            Assert.AreEqual("12 M", report.Cells["A1"].Text);
            Assert.AreEqual("23 M", report.Cells["A2"].Text);
            Assert.AreEqual("FF808080", report.Cells["A2"].Style.Fill.BackgroundColor.Rgb);
            Assert.AreEqual("34 M", report.Cells["A3"].Text);
            Assert.AreEqual("FF808080", report.Cells["A3"].Style.Fill.BackgroundColor.Rgb);
        }

        [Test]
        public void GenerateReport_WhenTemplateContainsTable_ThenExtendTableForEachRow()
        {
            var model = new
            {
                Collection = new List<object>
                {
                    new { Item = 12000000 },
                    new { Item = 23000000 },
                    new { Item = 34000000 }
                }
            };

            var tester = new ExcelReportingTester();
            var template = tester.CreateTemplate();
            template.Tables.Add(new OfficeOpenXml.ExcelAddressBase("A1:A2"), "TestTable");
            template.Cells["A1"].Value = "HeaderColumn1";
            template.Cells["A2"].Style.Numberformat.Format = "#,, \\M";
            template.Cells["A2"].Value = "{{foreach Item in Collection}}";

            var report = tester.GenerateReport(template, model);
            var table = report.Tables["TestTable"];

            Assert.AreEqual("A1:A4", table.Address.Address);

            Assert.AreEqual("12 M", report.Cells["A2"].Text);
            Assert.AreEqual("23 M", report.Cells["A3"].Text);
            Assert.AreEqual("34 M", report.Cells["A4"].Text);
        }

        [Test]
        public void GenerateReport_WhenTemplateContainsColumnsCollectionMarker_ThenAddColumnForEachElementWithValue()
        {
            var model = new
            {
                ColumnCollectionValue = new List<DataColumn>
                {
                    new DataColumn { ColumnName = "ExpectedText1", Value = 1 },
                    new DataColumn { ColumnName = "ExpectedText2", Value = 2 },
                    new DataColumn { ColumnName = "ExpectedText3", Value = 3 }
                }
            };

            var tester = new ExcelReportingTester();
            var template = tester.CreateTemplate();
            template.Cells["A1"].Value = "{{ColumnCollectionValue}}";

            var report = tester.GenerateReport(template, model);

            Assert.AreEqual("1", report.Cells["A1"].Text);
            Assert.AreEqual("2", report.Cells["B1"].Text);
            Assert.AreEqual("3", report.Cells["C1"].Text);
        }

        [Test]
        public void GenerateReport_WhenColumnsCollectionMarkerNotInFirstRow_ThenAddColumnsHeaders()
        {
            var model = new
            {
                ColumnCollectionValue = new List<DataColumn>
                {
                    new DataColumn { ColumnName = "ExpectedText1", Value = 1 },
                    new DataColumn { ColumnName = "ExpectedText2", Value = 2 }
                }
            };

            var tester = new ExcelReportingTester();
            var template = tester.CreateTemplate();
            template.Cells["A2"].Value = "{{ColumnCollectionValue}}";

            var report = tester.GenerateReport(template, model);

            Assert.AreEqual("ExpectedText1", report.Cells["A1"].Text);
            Assert.AreEqual("1", report.Cells["A2"].Text);

            Assert.AreEqual("ExpectedText2", report.Cells["B1"].Text);
            Assert.AreEqual("2", report.Cells["B2"].Text);
        }

        [Test]
        public void GenerateReport_WhenTemplateContainsCellFormat_ThenKeepFormatInEachColumn()
        {
            var model = new
            {
                ColumnCollectionValue = new List<DataColumn>
                {
                    new DataColumn { ColumnName = "ExpectedText1", Value = 12000000 },
                    new DataColumn { ColumnName = "ExpectedText2", Value = 23000000 }
                }
            };

            var tester = new ExcelReportingTester();
            var template = tester.CreateTemplate();
            template.Cells["B2"].Style.Numberformat.Format = "#,, \\M";
            template.Cells["B2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            template.Cells["B2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
            template.Cells["B2"].Value = "{{ColumnCollectionValue}}";

            var report = tester.GenerateReport(template, model);

            Assert.AreEqual("ExpectedText1", report.Cells["B1"].Text);
            Assert.AreEqual("12 M", report.Cells["B2"].Text);

            Assert.AreEqual("ExpectedText2", report.Cells["C1"].Text);
            Assert.AreEqual("23 M", report.Cells["C2"].Text);
        }

        [Test]
        public void GenerateReport_WhenTemplateContainsTable_ThenExtendTableForEachColumn()
        {
            var model = new
            {
                ColumnCollectionValue = new List<DataColumn>
                {
                    new DataColumn { ColumnName = "ExpectedText1", Value = 12000000 },
                    new DataColumn { ColumnName = "ExpectedText2", Value = 23000000 },
                    new DataColumn { ColumnName = "ExpectedText3", Value = 23000000 }
                }
            };

            var tester = new ExcelReportingTester();
            var template = tester.CreateTemplate();
            template.Tables.Add(new OfficeOpenXml.ExcelAddressBase("A1:B2"), "TestTable");
            template.Cells["A1"].Value = "MyTests";
            template.Cells["B2"].Style.Numberformat.Format = "#,, \\M";
            template.Cells["B2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            template.Cells["B2"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
            template.Cells["B2"].Value = "{{ColumnCollectionValue}}";

            var report = tester.GenerateReport(template, model);

            var table = report.Tables["TestTable"];
            Assert.AreEqual("A1:D2", table.Address.Address);

            Assert.AreEqual("ExpectedText1", report.Cells["B1"].Text);
            Assert.AreEqual("12 M", report.Cells["B2"].Text);

            Assert.AreEqual("ExpectedText2", report.Cells["C1"].Text);
            Assert.AreEqual("23 M", report.Cells["C2"].Text);
        }

        [Test]
        public void GenerateReport_WhenTemplateContainsCollectionWithColumnsCollection_ThenAddRowsAndColumns()
        {
            var model = new
            {
                RowsCollection = new List<object>
                {
                    new
                    {
                        ColumnsCollection = new List<DataColumn>
                        {
                            new DataColumn { ColumnName = "Column1", Value = 1 },
                            new DataColumn { ColumnName = "Column2", Value = 2 }
                        }
                    },
                    new
                    {
                        ColumnsCollection = new List<DataColumn>
                        {
                            new DataColumn { ColumnName = "Column1", Value = 3 },
                            new DataColumn { ColumnName = "Column2", Value = 4 }
                        }
                    }
                }
            };

            var tester = new ExcelReportingTester();
            var template = tester.CreateTemplate();
            template.Cells["A2"].Value = "{{foreach ColumnsCollection in RowsCollection}}";

            var report = tester.GenerateReport(template, model);

            Assert.AreEqual("Column1", report.Cells["A1"].Text);
            Assert.AreEqual("1", report.Cells["A2"].Text);
            Assert.AreEqual("3", report.Cells["A3"].Text);

            Assert.AreEqual("Column2", report.Cells["B1"].Text);
            Assert.AreEqual("2", report.Cells["B2"].Text);
            Assert.AreEqual("4", report.Cells["B3"].Text);
        }

        private static WorkSheetModelMock CreateInitialWorksheet()
        {
            return new WorkSheetModelMock
            {
                Rows = new List<List<object>>
                {
                    new List<object> {"Column1", "Column2", "Column3", "Column4", "Column5", "Column6"},
                    new List<object>
                    {
                        "{{foreach Value in Collection1}}",
                        "{{foreach Get(3).Value in Collection2}}",
                        "{{MultiColumns1}}",
                        "{{foreach Columns in MultiColumnsCollection}}",
                        "{{EmptyColumns}}",
                        "{{SingleValue.Value}}",
                    },
                    new List<object> {null, "{{foreach ToString() in Collection3}}", null, null, null, null},
                    new List<object>
                    {
                        null,
                        null,
                        null,
                        "{{foreach Value in EmptyCollection}}",
                        null,
                        null,
                    },
                    new List<object> {null, 88, "Column7", null, null, null},
                    new List<object> {null, null, "{{MultiColumns2}}", null, null, null},
                }
            };
        }

        private static IEnumerable<List<object>> ContentAfterRowExtending()
        {
            return new List<List<object>>
            {
                new List<object> {"Column1", "Column2", "Column3", "Column4", "Column5", "Column6"},
                new List<object>
                {
                    "{{Collection1[0].Value}}",
                    "{{Collection2[0].Get(3).Value}}",
                    "{{MultiColumns1}}",
                    "{{MultiColumnsCollection[0].Columns}}",
                    "{{EmptyColumns}}",
                    "{{SingleValue.Value}}",
                },
                new List<object>
                {
                    "{{Collection1[1].Value}}",
                    "{{Collection2[1].Get(3).Value}}",
                    null,
                    "{{MultiColumnsCollection[1].Columns}}",
                    null,
                    null,
                },
                new List<object>
                {
                    null,
                    "{{Collection2[2].Get(3).Value}}",
                    null,
                    "{{MultiColumnsCollection[2].Columns}}",
                    null,
                    null,
                },
                new List<object> {null, "{{Collection3[0].ToString()}}", null, null, null, null},
                new List<object> {null, "{{Collection3[1].ToString()}}", null, null, null, null},
                new List<object> {null, null, null, null, null, null},
                new List<object> {null, 88, "Column7", null, null, null},
                new List<object> {null, null, "{{MultiColumns2}}", null, null, null},
            };
        }

        private static IEnumerable<List<object>> ContentAfterColumnExtending()
        {
            return new List<List<object>>
            {
                new List<object> {"Column1", "Column2", "Dynamic1/1", "Dynamic1/2", "Dynamic3/1", "Dynamic3/2", "Dynamic3/3", "Column6"},
                new List<object>
                {
                    "{{Collection1[0].Value}}",
                    "{{Collection2[0].Get(3).Value}}",
                    "{{MultiColumns1[0]}}",
                    "{{MultiColumns1[1]}}",
                    "{{MultiColumnsCollection[0].Columns[0]}}",
                    "{{MultiColumnsCollection[0].Columns[1]}}",
                    null,
                    "{{SingleValue.Value}}",
                },
                new List<object>
                {
                    "{{Collection1[1].Value}}",
                    "{{Collection2[1].Get(3).Value}}",
                    null,
                    null,
                    "{{MultiColumnsCollection[1].Columns[1]}}",
                    "{{MultiColumnsCollection[1].Columns[0]}}",
                    null,
                    null,
                },
                new List<object>
                {
                    null,
                    "{{Collection2[2].Get(3).Value}}",
                    null,
                    null,
                    null,
                    "{{MultiColumnsCollection[2].Columns[1]}}",
                    "{{MultiColumnsCollection[2].Columns[0]}}",
                    null,
                },
                new List<object> {null, "{{Collection3[0].ToString()}}", null, null, null, null, null, null},
                new List<object> {null, "{{Collection3[1].ToString()}}", null, null, null, null, null, null},
                new List<object> {null, null, null, null, null, null, null, null},
                new List<object> {null, 88, "Dynamic2/1", "Dynamic2/2", null, null, null, null},
                new List<object> {null, null, "{{MultiColumns2[0]}}", "{{MultiColumns2[1]}}", null, null, null, null},
            };
        }

        private static IEnumerable<List<object>> ContentAfterValueReplacing()
        {
            return new List<List<object>>
            {
                new List<object> {"Column1", "Column2", "Dynamic1/1", "Dynamic1/2", "Dynamic3/1", "Dynamic3/2", "Dynamic3/3", "Column6"},
                new List<object> {100, 203, "1/1", "1/2", "3/1a", "3/2a", null, 42},
                new List<object> {101, 303, null, null, "3/1b", "3/2b", null, null},
                new List<object> {null, 403, null, null, null, "3/2c", "3/3c", null},
                new List<object> {null, "70", null, null, null, null, null, null},
                new List<object> {null, "71", null, null, null, null, null, null},
                new List<object> {null, null, null, null, null, null, null, null},
                new List<object> {null, 88, "Dynamic2/1", "Dynamic2/2", null, null, null, null},
                new List<object> {null, null, "2/1", "2/2", null, null, null, null},
            };
        }

        private static ExcelTestModel CreateTestModel()
        {
            return new ExcelTestModel
            {
                SingleValue = new ExcelTestModel.Subclass1 { Value = 42 },
                Collection1 = new[]
                {
                    new ExcelTestModel.Subclass1 {Value = 100},
                    new ExcelTestModel.Subclass1 {Value = 101},
                },
                Collection2 = new[]
                {
                    new ExcelTestModel.Subclass2(200),
                    new ExcelTestModel.Subclass2(300),
                    new ExcelTestModel.Subclass2(400),
                },
                Collection3 = new[] { 70, 71 },
                MultiColumns1 = new[]
                {
                    new DataColumn {ColumnName = "Dynamic1/1", Value = "1/1"},
                    new DataColumn {ColumnName = "Dynamic1/2", Value = "1/2"}
                },
                MultiColumns2 = new[]
                {
                    new DataColumn {ColumnName = "Dynamic2/1", Value = "2/1"},
                    new DataColumn {ColumnName = "Dynamic2/2", Value = "2/2"}
                },
                MultiColumnsCollection = new[]
                {
                    new ExcelTestModel.Subclass3
                    {
                        Columns = new[]
                        {
                            new DataColumn {ColumnName = "Dynamic3/1", Value = "3/1a"},
                            new DataColumn {ColumnName = "Dynamic3/2", Value = "3/2a"}
                        }
                    },
                    new ExcelTestModel.Subclass3
                    {
                        Columns = new[]
                        {
                            new DataColumn {ColumnName = "Dynamic3/2", Value = "3/2b"},
                            new DataColumn {ColumnName = "Dynamic3/1", Value = "3/1b"}
                        }
                    },
                    new ExcelTestModel.Subclass3
                    {
                        Columns = new[]
                        {
                            new DataColumn {ColumnName = "Dynamic3/3", Value = "3/3c"},
                            new DataColumn {ColumnName = "Dynamic3/2", Value = "3/2c"}
                        }
                    },
                }
            };
        }
    }
}
