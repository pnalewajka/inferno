﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Moq;
using NUnit.Framework;
using Smt.Atomic.Business.Common.Enums;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Enums;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.Business.Notifications.Services;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Tests.Common;
using Smt.Atomic.Tests.Common.AutoMock;

namespace UnitTests.Application.Business.Notifications
{
    [TestFixture]
    public class SmtpEmailServiceTests
    {
        [SetUp]
        public void SetupFixture()
        {
            var containerConfig = new TestContainerConfiguration();
            containerConfig.Configure(ContainerType.UnitTests);

            _container = containerConfig.AutoMockingContainer;

            _container.Register(Component.For<IEmailService>().ImplementedBy<EmailService>());

            SetupSmtpService(_container);
            SetupSystemParameterService(_container);
        }

        [TearDown]
        public void TeardownFixture()
        {
            _container.Dispose();
        }

        private AutoMockingContainer _container;
        private Mock<ISmtpService> _smtpServiceMock;
        private Mock<ISystemParameterService> _systemParameterService;

        private void SetupSmtpService(IWindsorContainer container)
        {
            _smtpServiceMock = new Mock<ISmtpService>();
            container.Register(Component.For<ISmtpService>().Instance(_smtpServiceMock.Object));
        }

        private void SetupSystemParameterService(IWindsorContainer container)
        {
            _systemParameterService = new Mock<ISystemParameterService>();

            _systemParameterService.Setup(
                a => a.GetParameter<string>(ParameterKeys.SmtpEmailServiceDefaultFromEmailAddress))
                .Returns("SmtpEmailServiceDefaultFromEmailAddress@email.com");

            _systemParameterService.Setup(
                a => a.GetParameter<string>(ParameterKeys.SmtpEmailServiceDefaultFromFullName))
                .Returns("SmtpEmailServiceDefaultFromFullName");

            container.Register(Component.For<ISystemParameterService>().Instance(_systemParameterService.Object));
        }

        [Test]
        public void Send_MockEmailRecipient()
        {
            _systemParameterService.Setup(a => a.GetParameter<string>(ParameterKeys.MockEmailRecipient, It.IsAny<EmailMessageParameterContext>()))
                .Returns("test@email.com");

            var emailService = _container.Resolve<IEmailService>();

            emailService.SendMessage(new EmailMessageDto
            {
                Recipients = new List<EmailRecipientDto>
                {
                    new EmailRecipientDto {EmailAddress = "real@email.com", FullName = "real"},
                    new EmailRecipientDto {EmailAddress = "other@email.com", FullName = "other"}
                },
                Body = "email body"
            });

            _systemParameterService.Verify(a => a.GetParameter<string>(ParameterKeys.MockEmailRecipient, It.IsAny<EmailMessageParameterContext>()), Times.Once);
            _smtpServiceMock.Verify(a => a.Send(It.IsAny<MailMessage>()), Times.Once);

            _smtpServiceMock.Verify(a => a.Send(It.Is<MailMessage>(message => message.To.Count == 1)), Times.Once);
            _smtpServiceMock.Verify(
                a => a.Send(It.Is<MailMessage>(message => message.To.Any(m => m.Address == "test@email.com"))),
                Times.Once);
        }

        [Test]
        public void Send_RecipientEmails()
        {
            var emailService = _container.Resolve<IEmailService>();

            emailService.SendMessage(new EmailMessageDto
            {
                Recipients = new List<EmailRecipientDto>
                {
                    new EmailRecipientDto {EmailAddress = "real@email.com", FullName = "real", Type = RecipientType.To},
                    new EmailRecipientDto
                    {
                        EmailAddress = "other@email.com",
                        FullName = "other",
                        Type = RecipientType.To
                    },
                    new EmailRecipientDto
                    {
                        EmailAddress = "otherCC@email.com",
                        FullName = "other",
                        Type = RecipientType.Cc
                    },
                    new EmailRecipientDto
                    {
                        EmailAddress = "otherBcc@email.com",
                        FullName = "other",
                        Type = RecipientType.Bcc
                    },
                    new EmailRecipientDto
                    {
                        EmailAddress = "otherBcc@email.com",
                        FullName = "other",
                        Type = RecipientType.Bcc
                    },
                    new EmailRecipientDto
                    {
                        EmailAddress = "otherBcc@email.com",
                        FullName = "other",
                        Type = RecipientType.Bcc
                    }
                },
                Body = "email body"
            });

            _systemParameterService.Verify(a => a.GetParameter<string>(ParameterKeys.MockEmailRecipient, It.IsAny<EmailMessageParameterContext>()), Times.Once);
            _smtpServiceMock.Verify(a => a.Send(It.IsAny<MailMessage>()), Times.Once);

            _smtpServiceMock.Verify(a => a.Send(It.Is<MailMessage>(message => message.To.Count == 2)), Times.Once);
            _smtpServiceMock.Verify(a => a.Send(It.Is<MailMessage>(message => message.CC.Count == 1)), Times.Once);
            _smtpServiceMock.Verify(a => a.Send(It.Is<MailMessage>(message => message.Bcc.Count == 3)), Times.Once);
        }
    }
}