﻿using NUnit.Framework;
using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.Business.Survey.Services.AnswerProviders;
using System;

namespace UnitTests.Application.Business.Survey
{
    [TestFixture]
    public class IdentifierAnswerProviderTests
    {
        private static QuestionItemsDto CreateOneOptionQuestion()
        {
            var questionItemsDto = new QuestionItemsDto("Option1");

            return questionItemsDto;
        }

        private static QuestionItemsDto CreateThreeOptionsQuestion()
        {
            var questionItemsDto = new QuestionItemsDto();
            questionItemsDto.AddItem("Option1");
            questionItemsDto.AddItem("Option2");
            questionItemsDto.AddItem("Option3");

            return questionItemsDto;
        }

        [Test]
        public void BuildAnswer_ShouldReturnNullAnswerWhenAnswerIsNull()
        {
            var surveyQuestion = CreateOneOptionQuestion();
            var identifierAnswerProvider = new IdentifierAnswerProvider(surveyQuestion);
            var answerDto = new SurveyItemDto();
            answerDto.SelectedAnswer = null;
            var result = identifierAnswerProvider.BuildAnswer(answerDto);
            Assert.IsNull(result);
        }

        [Test]
        public void BuildAnswer_ShouldReturnNullAnswerWhenAnswerIsEmpty()
        {
            var surveyQuestion = CreateOneOptionQuestion();
            var identifierAnswerProvider = new IdentifierAnswerProvider(surveyQuestion);
            var answerDto = new SurveyItemDto();
            answerDto.SelectedAnswer = new[] { string.Empty };
            var result = identifierAnswerProvider.BuildAnswer(answerDto);
            Assert.IsNull(result);
        }

        [Test]
        public void BuildAnswer_ShouldReturnNullAnswerWhenAnswerIdIsNotInOptions()
        {
            var surveyQuestion = CreateThreeOptionsQuestion();

            var identifierAnswerProvider = new IdentifierAnswerProvider(surveyQuestion);

            var answerDto = new SurveyItemDto();
            answerDto.SelectedAnswer = new[] { "5" };
            var result = identifierAnswerProvider.BuildAnswer(answerDto);

            Assert.IsNull(result);
        }

        [Test]
        public void BuildAnswer_ShouldReturnOptionWhenIdIsInAnswer()
        {
            var surveyQuestion = CreateThreeOptionsQuestion();

            var identifierAnswerProvider = new IdentifierAnswerProvider(surveyQuestion);

            var answerDto = new SurveyItemDto();
            answerDto.SelectedAnswer = new[] { "1" };
            var result = identifierAnswerProvider.BuildAnswer(answerDto);

            var expectedResult = $"Option2{Environment.NewLine}";
            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void BuildAnswer_ShouldReturnNewLineSeparatedOptionsWhenIdsAreInAnswer()
        {
            var surveyQuestion = CreateThreeOptionsQuestion();

            var identifierAnswerProvider = new IdentifierAnswerProvider(surveyQuestion);

            var answerDto = new SurveyItemDto();
            answerDto.SelectedAnswer = new[] { "0,2" };
            var result = identifierAnswerProvider.BuildAnswer(answerDto);

            var expectedResult = $"Option1{Environment.NewLine}Option3{Environment.NewLine}";
            Assert.AreEqual(expectedResult, result);
        }
    }
}
