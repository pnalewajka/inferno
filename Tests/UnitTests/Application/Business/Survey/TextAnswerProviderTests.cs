﻿using NUnit.Framework;
using Smt.Atomic.Business.Survey.Dto;
using Smt.Atomic.Business.Survey.Services.AnswerProviders;
using System;

namespace UnitTests.Application.Business.Survey
{
    [TestFixture]
    public class TextAnswerProviderTests
    {
        [Test]
        public void BuildAnswer_ShouldReturnNullAnswerWhenAnswerIsNull()
        {
            var textAnswerProvider = new TextAnswerProvider();
            var answerDto = new SurveyItemDto();
            answerDto.SelectedAnswer = null;
            var result = textAnswerProvider.BuildAnswer(answerDto);
            Assert.IsNull(result);
        }

        [Test]
        public void BuildAnswer_ShouldReturnNullAnswerWhenAnswerIsEmpty()
        {
            var textAnswerProvider = new TextAnswerProvider();
            var answerDto = new SurveyItemDto();
            answerDto.SelectedAnswer = new[] { string.Empty };
            var result = textAnswerProvider.BuildAnswer(answerDto);
            Assert.IsNull(result);
        }

        [Test]
        public void BuildAnswer_ShouldReturnNewLineSeparatedAnswers()
        {
            var textAnswerProvider = new TextAnswerProvider();
            var answerDto = new SurveyItemDto();
            answerDto.SelectedAnswer = new[] { "Option1", "Option2", "Option3" };
            var result = textAnswerProvider.BuildAnswer(answerDto);

            string expectedResult = $"Option1{Environment.NewLine}Option2{Environment.NewLine}Option3{Environment.NewLine}";
            Assert.AreEqual(expectedResult, result);
        }
    }
}
