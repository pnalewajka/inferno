﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Castle.MicroKernel.Registration;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using Smt.Atomic.Business.TimeTracking.Services;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Data.Repositories.Services;
using Smt.Atomic.Tests.Common;
using Smt.Atomic.Tests.Common.AutoMock;

namespace UnitTests.Application.Business.TimeTracking.Services
{
    public class AddMyProjectsFromPreviousMonthTests
    {
        private TestContainerConfiguration _testContainerConfiguration;
        private AutoMockingContainer _container;
        private AddMyProjectsFromPreviousMonthDatabaseMock _databaseMock;
        private Mock<IPrincipalProvider> _principalProviderMock;

        private TimeReportService _service;

        [SetUp]
        public void Setup()
        {
            _testContainerConfiguration = new TestContainerConfiguration();
            _testContainerConfiguration.Configure(ContainerType.UnitTests);

            _container = _testContainerConfiguration.AutoMockingContainer;
            _databaseMock = new AddMyProjectsFromPreviousMonthDatabaseMock();

            _container.RegisterDatabase(_databaseMock);
            _container.Register(
                Component.For(typeof(IUnitOfWorkService<ITimeTrackingDbScope>))
                    .ImplementedBy(typeof(UnitOfWorkService<ITimeTrackingDbScope>))
                    .LifestyleTransient());

            _principalProviderMock = new Mock<IPrincipalProvider>();
            _container.RegisterMock(_principalProviderMock);

            _container.Register(Component.For<TimeReportService>());
            _service = _container.Resolve<TimeReportService>();
        }

        [Test]
        public void AddEmployeeProjectsFromPreviousMonth_ShouldVerifyEmployeeId()
        {
            // Arrange
            const int currentYear = 2017;
            const byte currentMonth = 8;
            const int notExistingEmployeeId = 0;
            SetupTimeReportInDbMock(1, notExistingEmployeeId, currentYear, currentMonth);

            // Act
            Action verifyEmployeeId = () => { _service.AddEmployeeProjectsFromPreviousMonth(notExistingEmployeeId, currentYear, currentMonth); };

            // Assert
            verifyEmployeeId.ShouldThrow<DataException>("existing user employee id should be used to modify the Time Report");
        }

        [Test]
        public void AddEmployeeProjectsFromPreviousMonth_ShouldVerifyIfGivenMonthAndYearAreValid()
        {
            // Arrange
            const int currentYear = 0;
            const byte currentMonth = 0;

            // Act
            Action verifyArgumentException = () => { _service.AddEmployeeProjectsFromPreviousMonth(TimeTrackingDatabaseMock.EmployeeId, currentYear, currentMonth); };

            // Assert
            verifyArgumentException.ShouldThrowExactly<ArgumentException>("given year and month can't be used to create valid date.");
        }

        [Test]
        public void AddEmployeeProjectsFromPreviousMonth_ShouldThrowBusinesExeptionIfThereIsNoTimeReportForGivenMonth()
        {
            // Arrange
            const int currentYear = 2017;
            const byte currentMonth = 8;
            const int currentUserEmployeeId = 2;
            SetupCurrentUserEmployeeId(currentUserEmployeeId);
            SetupTimeReportInDbMock(1, currentUserEmployeeId, currentYear, currentMonth-1);
            
            // Act
            Action verifyBusinessException = () => { _service.AddEmployeeProjectsFromPreviousMonth(currentUserEmployeeId, currentYear, currentMonth); };

            // Assert
            verifyBusinessException.ShouldThrowExactly<BusinessException>("Time Report for given month must exist.");
        }

        [Test]
        public void AddEmployeeProjectsFromPreviousMonth_ShouldAddRecordWithProjectsFromPreviousMonthReport()
        {
            // Arrange
            const int currentYear = 2017;
            const byte currentMonth = 8;
            const int currentUserEmployeeId = 2;
            SetupCurrentUserEmployeeId(currentUserEmployeeId);

            const long previousMonthProjectId1 = 5;
            const long previousMonthProjectId2 = 6;
            var previousMonthProjects = new List<Project>{ new Project { Id = previousMonthProjectId1 }, new Project { Id = previousMonthProjectId2 } };

            const int previousMonthTimeReportId = 1000;
            const int currentMonthTimeReportId = 1001;
            const string timeReportRow1TaskName = "Task name 1";
            SetupTimeReportInDbMock(
                new Dictionary<long, string> { {previousMonthProjectId1, timeReportRow1TaskName }, { previousMonthProjectId2, null } }, 
                previousMonthTimeReportId, 
                currentUserEmployeeId, 
                currentYear, 
                currentMonth - 1, 
                previousMonthProjects);

            var currentMonthTimeReport = SetupTimeReportInDbMock(currentMonthTimeReportId, currentUserEmployeeId, currentYear, currentMonth);

            // Act
            _service.AddEmployeeProjectsFromPreviousMonth(currentUserEmployeeId, currentYear, currentMonth);

            // Assert
            Assert.AreEqual(currentMonthTimeReport.Rows.Count, 2);
            Assert.AreEqual(currentMonthTimeReport.Rows.ElementAt(0).ProjectId, previousMonthProjectId1);
            Assert.AreEqual(currentMonthTimeReport.Rows.ElementAt(1).ProjectId, previousMonthProjectId2);
        }

        private Mock<IAtomicPrincipal> SetupCurrentUserEmployeeId(int currentUserEmployeeId)
        {
            var currentPrincipalMock = new Mock<IAtomicPrincipal>();
            currentPrincipalMock.Setup(x => x.EmployeeId).Returns(currentUserEmployeeId);
            _principalProviderMock.Setup(x => x.Current).Returns(currentPrincipalMock.Object);
            _databaseMock.AddEntity(new Employee { Id = currentUserEmployeeId });

            return currentPrincipalMock;
        }

        private TimeReport SetupTimeReportInDbMock(
            int monthTiemReportId,
            int currentUserEmployeeId,
            int year,
            byte month,
            IEnumerable<TimeReportRow> timeReportRows = null)
        {
            var timeReport = new TimeReport
            {
                Id = monthTiemReportId,
                EmployeeId = currentUserEmployeeId,
                Year = year,
                Month = month
            };
            _databaseMock.AddEntity(timeReport);

            if (timeReportRows == null)
            {
                return timeReport;
            }

            foreach (var timeReportRow in timeReportRows)
            {
                timeReportRow.TimeReport = timeReport;
                timeReport.Rows.Add(timeReportRow);
            }

            return timeReport;
        }

        private void SetupTimeReportInDbMock(
            IDictionary<long, string> timeReportRowTaskNames,
            int timeReportId, 
            int currentUserEmployeeId, 
            int currentYear,
            byte month,
            IList<Project> projects)
        {
            var timeReportRows = projects.Select(project => SetupTimeReportRowInDbMock(project.Id, timeReportRowTaskNames[project.Id])).ToList();
            var previousMonthTimeReport = SetupTimeReportInDbMock(timeReportId, currentUserEmployeeId, currentYear, month, timeReportRows);

            foreach (var timeReportRow in timeReportRows)
            {
                previousMonthTimeReport.Rows.Add(timeReportRow);
                timeReportRow.TimeReport = previousMonthTimeReport;
                timeReportRow.Project = projects.First(x => x.Id == timeReportRow.ProjectId);
            }
        }

        private TimeReportRow SetupTimeReportRowInDbMock(long previousMonthProjectId1, string timeReportRow1TaskName)
        {
            var timeReportRow = new TimeReportRow
            {
                ProjectId = previousMonthProjectId1,
                TaskName = timeReportRow1TaskName
            };
            _databaseMock.AddEntity(timeReportRow);

            return timeReportRow;
        }
    }
}