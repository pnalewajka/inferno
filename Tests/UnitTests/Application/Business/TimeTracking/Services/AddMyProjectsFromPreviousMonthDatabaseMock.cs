﻿using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Tests.Common.Wrappers;

namespace UnitTests.Application.Business.TimeTracking.Services
{
    public class AddMyProjectsFromPreviousMonthDatabaseMock : DatabaseMock
    {
        public AddMyProjectsFromPreviousMonthDatabaseMock()
        {
            GetRepositoryMock<Employee>();
            GetRepositoryMock<TimeReport>();
        }
    }
}