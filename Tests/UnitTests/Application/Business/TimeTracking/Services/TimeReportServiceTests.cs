﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.MicroKernel.Registration;
using Moq;
using NUnit.Framework;
using Smt.Atomic.Business.TimeTracking.Dto;
using Smt.Atomic.Business.TimeTracking.Services;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Data.Repositories.Services;
using Smt.Atomic.Tests.Common;
using Smt.Atomic.Tests.Common.AutoMock;
using Smt.Atomic.Business.Allocation.Services;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace UnitTests.Application.Business.TimeTracking.Services
{
    [TestFixture]
    public class TimeReportServiceTests
    {
        private TestContainerConfiguration _testContainerConfiguration;
        private AutoMockingContainer _container;
        private TimeTrackingDatabaseMock _databaseMock;

        private TimeReportService _service;

        [SetUp]
        public void Setup()
        {
            _testContainerConfiguration = new TestContainerConfiguration();
            _testContainerConfiguration.Configure(ContainerType.UnitTests);

            _container = _testContainerConfiguration.AutoMockingContainer;
            _databaseMock = new TimeTrackingDatabaseMock();

            _container
                .GetMock<IPrincipalProvider>()
                .Setup(m => m.Current)
                .Returns(new Mock<IAtomicPrincipal>().Object);

            _container.RegisterDatabase(_databaseMock);
            _container.Register(
                Component.For(typeof(IUnitOfWorkService<ITimeTrackingDbScope>))
                    .ImplementedBy(typeof(UnitOfWorkService<ITimeTrackingDbScope>))
                    .LifestyleTransient());
            _container.Register(
                Component.For(typeof(IUnitOfWorkService<IAllocationDbScope>))
                    .ImplementedBy(typeof(UnitOfWorkService<IAllocationDbScope>))
                    .LifestyleTransient());

            _container.Register(
                Component.For<IProjectService>()
                    .ImplementedBy<MockProjectService>()
                    .IsDefault());

            _container.Register(Component.For<TimeReportService>());
            _service = _container.Resolve<TimeReportService>();
        }

        [Test]
        [ExpectedException(typeof(UnauthorizedException))]
        public void CheckReadOnlyViolations_ReadOnlyReport()
        {
            var originalTimeReportDto = CreateMyTimeReportDto();
            originalTimeReportDto.IsReadOnly = true;
            var currentTimeReportDto = CreateMyTimeReportDto();

            var result = _service.CheckOnSaveViolations(currentTimeReportDto, originalTimeReportDto);
        }

        [Test]
        public void CheckReadOnlyViolations_EditableNotModifiedReport()
        {
            var originalTimeReportDto = CreateMyTimeReportDto();
            var currentTimeReportDto = CreateMyTimeReportDto();

            var result = _service.CheckOnSaveViolations(currentTimeReportDto, originalTimeReportDto);
            Assert.IsTrue(result.IsSuccessful);
        }

        [Test]
        public void CheckReadOnlyViolations_EditableModifiedReport()
        {
            var originalTimeReportDto = CreateMyTimeReportDto();
            var currentTimeReportDto = CreateMyTimeReportDto();
            var row = currentTimeReportDto.Rows.First();
            row.TaskName = "some other name";
            row.Days.First().Hours = 3;

            var result = _service.CheckOnSaveViolations(currentTimeReportDto, originalTimeReportDto);
            Assert.IsTrue(result.IsSuccessful);
        }

        [Test]
        public void CheckReadOnlyViolations_EditableReportNotModifiedReadOnlyRow()
        {
            var originalTimeReportDto = CreateMyTimeReportDto();
            originalTimeReportDto.Rows.First().Status = TimeReportStatus.Submitted;
            var currentTimeReportDto = CreateMyTimeReportDto();

            var result = _service.CheckOnSaveViolations(currentTimeReportDto, originalTimeReportDto);
            Assert.IsTrue(result.IsSuccessful);
        }

        [Test]
        public void CheckReadOnlyViolations_EditableReportModifiedReadOnlyRow()
        {
            var originalTimeReportDto = CreateMyTimeReportDto();
            originalTimeReportDto.Rows.First().IsReadOnly = true;
            var currentTimeReportDto = CreateMyTimeReportDto();
            originalTimeReportDto.Rows.First().TaskName = "some other name";

            var result = _service.CheckOnSaveViolations(currentTimeReportDto, originalTimeReportDto);
            Assert.IsFalse(result.IsSuccessful);
        }

        [Test]
        public void CheckReadOnlyViolations_EditableReportModifiedDayForReadOnlyRow()
        {
            var originalTimeReportDto = CreateMyTimeReportDto();
            originalTimeReportDto.Rows.First().IsReadOnly = true;
            var currentTimeReportDto = CreateMyTimeReportDto();
            originalTimeReportDto.Rows.First().Days.First().Hours = 3;

            var result = _service.CheckOnSaveViolations(currentTimeReportDto, originalTimeReportDto);
            Assert.IsFalse(result.IsSuccessful);
        }

        [Test]
        public void CheckReadOnlyViolations_EditableReportNotModifiedReadOnlyDay()
        {
            var originalTimeReportDto = CreateMyTimeReportDto();
            originalTimeReportDto.Rows.First().Days.First().IsReadOnly = true;
            var currentTimeReportDto = CreateMyTimeReportDto();

            var result = _service.CheckOnSaveViolations(currentTimeReportDto, originalTimeReportDto);
            Assert.IsTrue(result.IsSuccessful);
        }

        [Test]
        public void CheckReadOnlyViolations_EditableReportModifiedReadOnlyDay()
        {
            var originalTimeReportDto = CreateMyTimeReportDto();
            originalTimeReportDto.Rows.First().IsReadOnly = true;
            var currentTimeReportDto = CreateMyTimeReportDto();
            originalTimeReportDto.Rows.First().Days.First().Hours = 3;

            var result = _service.CheckOnSaveViolations(currentTimeReportDto, originalTimeReportDto);
            Assert.IsFalse(result.IsSuccessful);
        }

        [Test]
        public void CheckReadOnlyViolations_EditableReportDeletedReadOnlyRow()
        {
            var originalTimeReportDto = CreateMyTimeReportDto();
            originalTimeReportDto.Rows.First().IsReadOnly = true;
            var currentTimeReportDto = CreateMyTimeReportDto();
            var rows = currentTimeReportDto.Rows.ToList();
            rows.RemoveAt(0);
            currentTimeReportDto.Rows = rows;

            var result = _service.CheckOnSaveViolations(currentTimeReportDto, originalTimeReportDto);
            Assert.IsFalse(result.IsSuccessful);
        }

        private MyTimeReportDto CreateMyTimeReportDto()
        {
            return new MyTimeReportDto
            {
                Year = 2117,
                Month = 1,
                Rows = new List<MyTimeReportRowDto>
                {
                    new MyTimeReportRowDto
                    {
                        Id = 111,
                        ProjectId = 1,
                        TaskName = "task name",
                        Days = new List<MyTimeReportDailyEntryDto>
                        {
                            new MyTimeReportDailyEntryDto
                            {
                                Day = new DateTime(2117, 1, 15),
                                Hours = 2
                            },
                             new MyTimeReportDailyEntryDto
                            {
                                Day = new DateTime(2117, 1, 16),
                                Hours = 4
                            },
                              new MyTimeReportDailyEntryDto
                            {
                                Day = new DateTime(2117, 1, 17),
                                Hours = 8
                            }
                        }
                    }
                }
            };
        }
    }
}
