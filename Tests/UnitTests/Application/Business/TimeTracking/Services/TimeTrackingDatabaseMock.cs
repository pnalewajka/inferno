using System;
using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Business.Enums;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Entities.Modules.TimeTracking;
using Smt.Atomic.Tests.Common.Wrappers;

namespace UnitTests.Application.Business.TimeTracking.Services
{
    public  class TimeTrackingDatabaseMock : DatabaseMock
    {
        public static long EmployeeId => 111;

        private readonly IEnumerable<TimeReport> _timeReports = new List<TimeReport>
        {
             new TimeReport
            {
                EmployeeId = EmployeeId,
                Year = 2017,
                Month = 1,
                Status = TimeReportStatus.Accepted,
                Rows = new List<TimeReportRow>()
            },
            new TimeReport
            {
                EmployeeId = EmployeeId,
                Year = 2017,
                Month = 2,
                Status = TimeReportStatus.Submitted,
                Rows = new List<TimeReportRow>()
            },
            new TimeReport
            {
                EmployeeId = EmployeeId,
                Year = 2017,
                Month = 3,
                Status = TimeReportStatus.Draft,
                Rows = new List<TimeReportRow>()
            },
            new TimeReport
            {
                EmployeeId = EmployeeId,
                Year = 2017,
                Month = 4,
                Status = TimeReportStatus.Draft,
                Rows = new List<TimeReportRow>
                {
                    new TimeReportRow
                    {
                        Status = TimeReportStatus.Submitted
                    }
                }
            },
             new TimeReport
            {
                EmployeeId = EmployeeId,
                Year = 2017,
                Month = 5,
                Status = TimeReportStatus.Draft,
                Rows = new List<TimeReportRow>
                {
                    new TimeReportRow
                    {
                        Status = TimeReportStatus.Accepted
                    }
                }
            }
        };

        public TimeTrackingDatabaseMock()
        {
            AddEntities(_timeReports);

            AddEntities(new[]
            {
                new Project { Id = 1, StartDate = new DateTime(), JiraIssueKey = "1", Apn = "1", UtilizationCategoryId = 1 }
            });
        }
    }
}