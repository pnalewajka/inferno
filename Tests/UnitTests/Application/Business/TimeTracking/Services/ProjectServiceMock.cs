﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Interfaces;

namespace UnitTests.Application.Business.TimeTracking.Services
{
    internal class MockProjectService : IProjectService
    {
        public void AssignAndDetachTags(List<ProjectTagDto> tagsToAssign, List<ProjectTagDto> tagsToDetach, long projectDtoId)
        {
            throw new NotImplementedException();
        }

        public ProjectDto CreateOrEditProject(ProjectDto project)
        {
            throw new NotImplementedException();
        }

        public void CreateOrEditProjectTags(List<ProjectTagDto> tagsToCreate)
        {
            throw new NotImplementedException();
        }

        public IReadOnlyCollection<string> GetAllProjectAcronyms()
        {
            throw new NotImplementedException();
        }

        public long GetDefaultJiraIssueToTimeReportRowMapperId()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ProjectPresentationDto> GetProjecListByIdsForPresentationGeneration(long[] ids)
        {
            throw new NotImplementedException();
        }

        public ProjectDto GetProjectByClientAndNameOrDefault(string project, string client)
        {
            throw new NotImplementedException();
        }

        public ProjectDto GetProjectById(long id)
        {
            throw new NotImplementedException();
        }

        public ProjectDto GetProjectByJiraIssueKeyOrDefault(string jiraIssueKey)
        {
            throw new NotImplementedException();
        }

        public ProjectDto GetProjectByJiraKeyOrDefault(string jiraKey)
        {
            throw new NotImplementedException();
        }

        public ProjectDto GetProjectByApn(string apn)
        {
            throw new NotImplementedException();
        }

        public ICollection<long> GetProjectIdsByUserId(long userId, DateTime date)
        {
            throw new NotImplementedException();
        }

        public ProjectDto GetProjectOrDefaultById(long id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ProjectDto> GetProjectsByIds(IEnumerable<long> ids)
        {
            return ids.Select(i => new ProjectDto { Id = i, StartDate = new DateTime(), Apn = i.ToString(), JiraIssueKey = i.ToString(), UtilizationCategoryId = 1 });
        }

        public ProjectTagDto GetProjectTagByName(string name)
        {
            throw new NotImplementedException();
        }

        public IList<ProjectTagDto> GetProjectTagsByNames(string[] names)
        {
            throw new NotImplementedException();
        }

        public IList<ProjectTagDto> GetProjectTagsByProjectId(long projectId)
        {
            throw new NotImplementedException();
        }

        public bool HasProjects(long employeeId)
        {
            throw new NotImplementedException();
        }

        public bool IsEmployeeOwnProject(long employeeId, long projectId)
        {
            throw new NotImplementedException();
        }

        public void MergeProjects(IList<long> projectsIds, long targetProjectId)
        {
            throw new NotImplementedException();
        }

        public long GetSetupIdByProjectId(long projectId)
        {
            throw new NotImplementedException();
        }

        public IReadOnlyCollection<ProjectDto> GetActiveProjectsForManagerEmployeeId(long employeeId)
        {
            throw new NotImplementedException();
        }
    }
}
