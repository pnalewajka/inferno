﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.MicroKernel.Registration;
using Moq;
using NUnit.Framework;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Data.Repositories.Services;
using Smt.Atomic.Tests.Common;
using Smt.Atomic.Tests.Common.AutoMock;
using UnitTests.Application.Business.Accounts;
using UnitTests.Application.Business.Common;
using Smt.Atomic.Business.Organization.Services;

namespace UnitTests.Application.Business.Organizations
{
    [TestFixture]
    public class OrgUnitCardIndexDataServiceTests
    {
        private AutoMockingContainer _container;
        private OrgUnitsDatabaseMock _databaseMock;
        private TestContainerConfiguration _testContainerConfiguration;

        [SetUp]
        public void Setup()
        {
            _testContainerConfiguration = new TestContainerConfiguration();
            _testContainerConfiguration.Configure(ContainerType.UnitTests);

            _container = _testContainerConfiguration.AutoMockingContainer;
            _databaseMock = new OrgUnitsDatabaseMock();
            _container.RegisterDatabase(_databaseMock);

            _container.Register(
                Component.For<IRemoveTreeNodeStategy>().ImplementedBy<RemoveParentIdWhenRemoveTreeNodeStategy>());
            _container.Register(
                Component.For<IOrgUnitCardIndexDataService>().ImplementedBy<OrgUnitCardIndexDataService>());
            _container.Register(
                Component.For(typeof (ICardIndexServiceDependencies<IOrganizationScope>))
                    .ImplementedBy(typeof (CardIndexDataServiceDependencies<IOrganizationScope>))
                    .LifestyleTransient());
            _container.Register(
                Component.For(typeof (IUnitOfWorkService<>))
                    .ImplementedBy(typeof (UnitOfWorkService<>))
                    .LifestyleTransient());
            _container.Register(
                Component.For<IClassMappingFactory>().ImplementedBy<ClassMappingFactory>().LifestyleTransient());
            _container.Register(
                Component.For<IClassMapping<OrgUnit, OrgUnitDto>>()
                    .ImplementedBy<OrgUnitToOrgUnitDtoMapping>()
                    .LifestyleTransient());
            _container.Register(
                Component.For<IClassMapping<OrgUnitDto, OrgUnit>>()
                    .ImplementedBy<OrgUnitDtoToOrgUnitMapping>()
                    .LifestyleTransient());

            _container.GetMock<IDbContextWrapperFactory>()
                .Setup(m => m.Create())
                .Returns(new Mock<IDbContextWrapper>().Object);
            _container.Register(
                Component.For(typeof (IRepository<>))
                    .UsingFactoryMethod(
                        (k, c) => _databaseMock.GetRepositoryMockFactoryMethod(c.RequestedType.GenericTypeArguments[0])));
        }

        [Test]
        public void GetRecords_DefaultCriteriaShouldReturnAllRecordsWithNonNegativeDepths()
        {
            var orgUnitCardIndexDataService = _container.Resolve<IOrgUnitCardIndexDataService>();

            var userDtos = orgUnitCardIndexDataService.GetRecords(new QueryCriteria());

            Assert.IsTrue(userDtos.Rows.All(r => r.Depth == null || r.Depth >= 0));
        }

        [Test]
        public void GetRecords_WithSearchCriteriaShouldReturnAllRecordsWithNonPositiveDepths()
        {
            var orgUnitCardIndexDataService = _container.Resolve<IOrgUnitCardIndexDataService>();

            var userDtos = orgUnitCardIndexDataService.GetRecords(new QueryCriteria() {SearchPhrase = "a"});

            Assert.IsTrue(userDtos.Rows.All(r => r.Depth == null || r.Depth <= 0));
        }

        [Test]
        public void EditRecord_OrgUnitWithWrongRecursionShouldntBeModified()
        {
            _container.GetMock<ITreeStructureService<IOrganizationScope>>()
                .Setup(m => m.IsDescendantOf<OrgUnit>(3, 5, It.IsAny<IExtendedUnitOfWork<IOrganizationScope>>())).Returns(true);
            var orgUnitCardIndexDataService = _container.Resolve<IOrgUnitCardIndexDataService>();
            var orgUnit = orgUnitCardIndexDataService.GetRecordById(3);
            orgUnit.ParentId = 5;

            var result = orgUnitCardIndexDataService.EditRecord(orgUnit);

            Assert.IsFalse(result.IsSuccessful);
        }

        [Test]
        public void EditRecord_OrgUnitWithValidRecursionShouldBeModified()
        {
            var orgUnitCardIndexDataService = _container.Resolve<IOrgUnitCardIndexDataService>();
            _container.GetMock<ITreeStructureService<IOrganizationScope>>()
                .Setup(m => m.IsDescendantOf<OrgUnit>(4, 3, It.IsAny<IExtendedUnitOfWork<IOrganizationScope>>())).Returns(false);
            var orgUnit = orgUnitCardIndexDataService.GetRecordById(4);
            orgUnit.ParentId = 3;

            var result = orgUnitCardIndexDataService.EditRecord(orgUnit);

            Assert.IsTrue(result.IsSuccessful);
        }

        [Test]
        public void EditRecord_AfterRecordIsEditedTreeStructureShouldBeRefreshedIfNameIsChanged()
        {
            var refreshTreeStructureExecuted = 0;
            _container.GetMock<ITreeStructureService<IOrganizationScope>>()
                .Setup(m => m.RefreshTreeStructure<OrgUnit>())
                .Callback(() => refreshTreeStructureExecuted++);
            var orgUnitCardIndexDataService = _container.Resolve<IOrgUnitCardIndexDataService>();
            var orgUnit = orgUnitCardIndexDataService.GetRecordById(4);
            orgUnit.Name = "Changed";

            orgUnitCardIndexDataService.EditRecord(orgUnit);

            Assert.AreEqual(1, refreshTreeStructureExecuted);
        }

        [Test]
        public void EditRecord_AfterRecordIsEditedTreeStructureShouldBeRefreshedIfParentIsChanged()
        {
            var refreshTreeStructureExecuted = 0;
            _container.GetMock<ITreeStructureService<IOrganizationScope>>()
                .Setup(m => m.RefreshTreeStructure<OrgUnit>())
                .Callback(() => refreshTreeStructureExecuted++);
            var orgUnitCardIndexDataService = _container.Resolve<IOrgUnitCardIndexDataService>();
            var orgUnit = orgUnitCardIndexDataService.GetRecordById(4);
            orgUnit.ParentId = null;

            orgUnitCardIndexDataService.EditRecord(orgUnit);

            Assert.AreEqual(1, refreshTreeStructureExecuted);
        }

        [Test]
        public void EditRecord_AfterRecordIsEditedTreeStructureShouldntBeRefreshedIfNameAndParentIdIsNotChanged()
        {
            var refreshTreeStructureExecuted = 0;
            _container.GetMock<ITreeStructureService<IOrganizationScope>>()
                .Setup(m => m.RefreshTreeStructure<OrgUnit>())
                .Callback(() => refreshTreeStructureExecuted++);
            var orgUnitCardIndexDataService = _container.Resolve<IOrgUnitCardIndexDataService>();
            var orgUnit = orgUnitCardIndexDataService.GetRecordById(4);

            orgUnitCardIndexDataService.EditRecord(orgUnit);

            Assert.AreEqual(0, refreshTreeStructureExecuted);
        }

        [Test]
        public void AddRecord_AfterRecordIsAddedTreeStructureShouldBeRefreshed()
        {
            var refreshTreeStructureExecuted = 0;
            _container.GetMock<ITreeStructureService<IOrganizationScope>>()
                .Setup(m => m.RefreshTreeStructure<OrgUnit>())
                .Callback(() => refreshTreeStructureExecuted++);
            var orgUnitCardIndexDataService = _container.Resolve<IOrgUnitCardIndexDataService>();
            var orgUnit = new OrgUnitDto
            {
                Name = "NewOne",
                Code = "Code",
                Description = "Description",
                Path = "/1/1/",
                ParentId = null
            };
            orgUnitCardIndexDataService.AddRecord(orgUnit);

            Assert.AreEqual(1, refreshTreeStructureExecuted);
        }

        [Test]
        public void DeleteRecord_AfterRecordIsDeletedAllDescendantNodesShouldHaveNoParentId()
        {
            var refreshTreeStructureExecuted = 0;
            _container.GetMock<ITreeStructureService<IOrganizationScope>>()
                .Setup(m => m.RefreshTreeStructure<OrgUnit>())
                .Callback(() => refreshTreeStructureExecuted++);
            var orgUnitCardIndexDataService = _container.Resolve<IOrgUnitCardIndexDataService>();
            var polandId = _databaseMock.OrgUnits.Where(o => o.Code == "PLN").Select(o => o.Id).FirstOrDefault();

            orgUnitCardIndexDataService.DeleteRecords(new[] {polandId});

            Assert.AreEqual(0, _databaseMock.OrgUnits.Count(o => o.ParentId == polandId));
        }
    }
}