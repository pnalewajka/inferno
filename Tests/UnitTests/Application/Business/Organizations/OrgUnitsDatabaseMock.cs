﻿using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Tests.Common.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Allocation;

namespace UnitTests.Application.Business.Organizations
{
    public class OrgUnitsDatabaseMock:DatabaseMock
    {
        private readonly List<OrgUnit> _orgUnits = new List<OrgUnit>
        {
            new OrgUnit() {Id = 1,Path=@"/1/",ParentId = null,Code = "PLN",Description = "Poland",Name = "Polska", Employee = new Employee()},
            new OrgUnit() {Id = 2,Path=@"/1/2/",ParentId = 1,Code = "WLKP",Description = "Wielkopolskie",Name = "Wielkopolska", Employee = new Employee()},
            new OrgUnit() {Id = 3,Path=@"/1/3/",ParentId = 1,Code = "PO",Description = "Pomorskie",Name = "Pomorskie", Employee = new Employee()},
            new OrgUnit() {Id = 4,Path=@"/1/2/4/",ParentId = 2,Code = "PZN",Description = "Poznań",Name = "Poznań", Employee = new Employee()},
            new OrgUnit() {Id = 5,Path=@"/1/3/5/",ParentId = 3,Code = "GD",Description = "Gdynia",Name = "Gdynia", Employee = new Employee()},
        };

        public List<OrgUnit> OrgUnits => _orgUnits;

        public OrgUnitsDatabaseMock()
        {
            AddEntities(_orgUnits);
        }
    }
}
