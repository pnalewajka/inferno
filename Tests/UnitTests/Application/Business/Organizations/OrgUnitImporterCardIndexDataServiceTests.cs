﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.MicroKernel.Registration;
using Moq;
using NUnit.Framework;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Organization.Dto;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Organization;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Data.Repositories.Services;
using Smt.Atomic.Tests.Common;
using Smt.Atomic.Tests.Common.AutoMock;
using UnitTests.Application.Business.Accounts;
using UnitTests.Application.Business.Common;
using Smt.Atomic.Business.Organization.Services;
using Smt.Atomic.CrossCutting.Common.Enums;

namespace UnitTests.Application.Business.Organizations
{
    [TestFixture]
    public class OrgUnitImporterCardIndexDataServiceTests
    {
        private AutoMockingContainer _container;
        private OrgUnitsDatabaseMock _databaseMock;
        private TestContainerConfiguration _testContainerConfiguration;

        [SetUp]
        public void Setup()
        {
            _testContainerConfiguration = new TestContainerConfiguration();
            _testContainerConfiguration.Configure(ContainerType.UnitTests);

            _container = _testContainerConfiguration.AutoMockingContainer;
            _databaseMock = new OrgUnitsDatabaseMock();
            _container.RegisterDatabase(_databaseMock);

            _container.Register(Component.For<IOrgUnitImporterCardIndexDataService>().ImplementedBy<OrgUnitImporterCardIndexDataService>());
            _container.Register(Component.For(typeof (ICardIndexServiceDependencies<IOrganizationScope>)).ImplementedBy(typeof (CardIndexDataServiceDependencies<IOrganizationScope>)).LifestyleTransient());
            _container.Register(Component.For(typeof (IUnitOfWorkService<>)).ImplementedBy(typeof (UnitOfWorkService<>)).LifestyleTransient());
            _container.Register(Component.For<IClassMappingFactory>().ImplementedBy<ClassMappingFactory>().LifestyleTransient());
            _container.Register(Component.For<IClassMapping<OrgUnit, OrgUnitImportDto>>().ImplementedBy<OrgUnitToOrgUnitImportDtoMapping>().LifestyleTransient());
            _container.Register(Component.For<IClassMapping<OrgUnitImportDto, OrgUnit>>().ImplementedBy<OrgUnitImportDtoToOrgUnitMapping>().LifestyleTransient());

            _container.GetMock<IDbContextWrapperFactory>().Setup(m => m.Create()).Returns(new Mock<IDbContextWrapper>().Object);
            _container.Register(Component.For(typeof (IRepository<>)).UsingFactoryMethod((k, c) => _databaseMock.GetRepositoryMockFactoryMethod(c.RequestedType.GenericTypeArguments[0])));
        }


        [Test]
        public void ImportRecord_OrgUnitWithWrongRecursionShouldntBeImported()
        {
            var orgUnitCardIndexDataService = _container.Resolve<IOrgUnitImporterCardIndexDataService>();
            var orgUnit = orgUnitCardIndexDataService.GetRecordById(3);
            orgUnit.ParentCode = "GDA";

            var result = orgUnitCardIndexDataService.ImportRecord(orgUnit, ImportBehavior.InsertOrUpdate);

            Assert.IsFalse(result.IsSuccessful);
        }

        [Test]
        public void ImportRecord_OrgUnitWithValidRecursionShouldBeImported()
        {
            var orgUnitCardIndexDataService = _container.Resolve<IOrgUnitImporterCardIndexDataService>();
            var orgUnit = orgUnitCardIndexDataService.GetRecordById(3);
            orgUnit.ParentCode = "PLN";

            var result = orgUnitCardIndexDataService.ImportRecord(orgUnit, ImportBehavior.InsertOrUpdate);

            Assert.IsTrue(result.IsSuccessful);
        }

        [Test]
        public void ImportRecord_OrgUnitWithNotExistingParentCodeShouldntBeImported()
        {
            var orgUnitCardIndexDataService = _container.Resolve<IOrgUnitImporterCardIndexDataService>();
            var orgUnit = new OrgUnitImportDto()
            {
                Name = "NewOne",
                Code = "Code",
                Description = "Description",
                ParentCode = "XXX"
            };

            var result = orgUnitCardIndexDataService.ImportRecord(orgUnit, ImportBehavior.InsertOrUpdate);

            Assert.IsFalse(result.IsSuccessful);
        }
    }
}