﻿using System.Collections.Generic;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Tests.Common.Wrappers;

namespace UnitTests.Application.Business.Accounts
{
    public class UsersDatabaseMock : DatabaseMock
    {
        private readonly List<User> _users = new List<User>
        {
            new User {Id = 1, FirstName = "John", LastName = "ZZZZ", ImpersonatedById = 4, Login = "administrator"},
            new User {Id = 2, FirstName = "John", LastName = "Public", ImpersonatedById = 3, Login = "user1"},
            new User {Id = 3, FirstName = "John", LastName = "Doe", ImpersonatedById = 2, Login = "user2"},
            new User {Id = 4, FirstName = "John", LastName = "AAAA", ImpersonatedById = 1, Login = "user3", Email = "administrator@domena.pl.aa"},
        };

        public List<User> Users => _users;

        public UsersDatabaseMock()
        {
            AddEntities(_users);
        }
    }
}