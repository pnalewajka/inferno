﻿using System.Collections.Generic;
using System.Linq;
using Castle.MicroKernel.Registration;
using Moq;
using NUnit.Framework;
using Smt.Atomic.Business.Accounts.BusinessEvents;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Accounts.Services;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.CrossCutting.Common.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Dto;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Repositories.FilteringQueries;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Data.Repositories.Services;
using Smt.Atomic.Tests.Common;
using Smt.Atomic.Tests.Common.AutoMock;

namespace UnitTests.Application.Business.Accounts
{
    [TestFixture]
    public class UserCardIndexServiceTests
    {
        private AutoMockingContainer _container;
        private TestContainerConfiguration _testContainerConfiguration;
        private UsersDatabaseMock _databaseMock;

        [SetUp]
        public void Setup()
        {
            _testContainerConfiguration = new TestContainerConfiguration();
            _testContainerConfiguration.Configure(ContainerType.UnitTests);

            _container = _testContainerConfiguration.AutoMockingContainer;
            _databaseMock = new UsersDatabaseMock();
            _container.RegisterDatabase(_databaseMock);

            _container.Register(Component.For<UserCardIndexService>());
            _container.Register(Component.For(typeof(IUnitOfWorkService<>)).ImplementedBy(typeof(UnitOfWorkService<>)).LifestyleTransient());
            _container.Register(Component.For<IClassMappingFactory>().ImplementedBy<ClassMappingFactory>().LifestyleTransient());
            _container.Register(Component.For<IClassMapping<User, UserDto>>().ImplementedBy<UserToUserDtoMapping>().LifestyleTransient());
            _container.Register(Component.For<IClassMapping<UserDto, User>>().ImplementedBy<UserDtoToUserMapping>().LifestyleTransient());

            _container
                .Register(Component
                    .For<ICardIndexServiceDependencies<IAccountsDbScope>>()
                    .ImplementedBy<CardIndexDataServiceDependencies<IAccountsDbScope>>()
                    .LifestyleTransient());

            _container
                 .GetMock<IDbContextWrapperFactory>()
                 .Setup(m => m.Create())
                 .Returns(new Mock<IDbContextWrapper>().Object);

            _container
                .Register(Component
                    .For(typeof(IRepository<>))
                    .UsingFactoryMethod((k, c) => _databaseMock.GetRepositoryMockFactoryMethod(c.RequestedType.GenericTypeArguments[0])));

            _container
                .GetMock<IPrincipalProvider>()
                .Setup(m => m.Current)
                .Returns(new Mock<IAtomicPrincipal>().Object);

            _container
                .GetMock<IDataFilterService>()
                .Setup(m => m.GetUserDataFilters(It.IsAny<long>()))
                .Returns(new UserDataFilters());
        }

        [TestCase(null, 1, Result = 3)]
        [TestCase(0, 1, Result = 3)]
        [TestCase(1, 1, Result = 3)]
        [TestCase(null, 2, Result = 1)]
        [TestCase(0, 2, Result = 1)]
        [TestCase(1, 2, Result = 1)]
        [TestCase(null, 3, Result = 1)]
        [TestCase(0, 3, Result = 1)]
        [TestCase(1, 3, Result = 1)]
        [TestCase(null, 4, Result = 0)]
        [TestCase(0, 4, Result = 0)]
        [TestCase(1, 4, Result = 0)]
        public int? GetRecords_LastPageIndex(int? currentPageIndex, int pageSize)
        {
            var records = _container
                .Resolve<UserCardIndexService>()
                .GetRecords(new QueryCriteria { CurrentPageIndex = currentPageIndex, PageSize = pageSize });

            return records.LastPageIndex;
        }

        [TestCase(null, 1, Result = 1)]
        [TestCase(0, 1, Result = 1)]
        [TestCase(1, 1, Result = 1)]
        [TestCase(4, 1, Result = 0)]
        [TestCase(null, 2, Result = 2)]
        [TestCase(0, 2, Result = 2)]
        [TestCase(1, 2, Result = 2)]
        [TestCase(2, 2, Result = 0)]
        [TestCase(null, 3, Result = 3)]
        [TestCase(0, 3, Result = 3)]
        [TestCase(1, 3, Result = 1)]
        [TestCase(2, 3, Result = 0)]
        public int? GetRecords_RowsCount(int? currentPageIndex, int pageSize)
        {
            var records = _container
                .Resolve<UserCardIndexService>()
                .GetRecords(new QueryCriteria { CurrentPageIndex = currentPageIndex, PageSize = pageSize });

            return records.Rows.Count;
        }

        [Test]
        public void AddRecord_ShouldPublishEvent()
        {
            var service = _container.Resolve<UserCardIndexService>();
            var eventPublisherMock = _container.GetMock<IBusinessEventPublisher>();
            UserAddedBusinessEvent actualEvent = null;
            var userDto = new UserDto
            {
                FirstName = "Aaa",
                LastName = "Bbb",
                Email = "aaa@bbb.pl",
                Login = "aaabbb",
            };

            eventPublisherMock
                .Setup(m => m.PublishBusinessEvent(It.IsAny<BusinessEvent>()))
                .Callback<BusinessEvent>(e => { actualEvent = (UserAddedBusinessEvent)e; })
                .Verifiable();

            service.AddRecord(userDto);

            eventPublisherMock.Verify(m => m.PublishBusinessEvent(It.IsAny<BusinessEvent>()), Times.Once);
            AssertCorrectAddedUserEvent(userDto, actualEvent);
        }

        [Test]
        public void AddRecord_ShouldNotPublishEventOnUnsuccessfulSave()
        {
            var service = _container.Resolve<UserCardIndexService>();
            var eventPublisherMock = _container.GetMock<IBusinessEventPublisher>();

            SetupUniqueKeyViolation();

            eventPublisherMock
                .Setup(m => m.PublishBusinessEvent(It.IsAny<BusinessEvent>()))
                .Verifiable();

            service.AddRecord(new UserDto());

            eventPublisherMock.Verify(m => m.PublishBusinessEvent(It.IsAny<BusinessEvent>()), Times.Never);
        }

        [Test]
        public void EditRecord_ShouldPublishEvent()
        {
            var service = _container.Resolve<UserCardIndexService>();
            var eventPublisherMock = _container.GetMock<IBusinessEventPublisher>();
            UserEditedBusinessEvent actualEvent = null;
            var originalUser = CloneUserFromDb(1);
            var editedUser = CloningHelper.CloneObject(originalUser);
            editedUser.FirstName = "Edit";

            eventPublisherMock
                .Setup(m => m.PublishBusinessEvent(It.IsAny<BusinessEvent>()))
                .Callback<BusinessEvent>(e => { actualEvent = (UserEditedBusinessEvent)e; })
                .Verifiable();

            service.EditRecord(editedUser);

            eventPublisherMock.Verify(m => m.PublishBusinessEvent(It.IsAny<BusinessEvent>()), Times.Once);
            AssertCorrectEditedUserEvent(editedUser, originalUser, actualEvent);
        }

        [Test]
        public void EditRecord_ShouldNotPublishEventOnUnsuccessfulEdit()
        {
            var service = _container.Resolve<UserCardIndexService>();
            var eventPublisherMock = _container.GetMock<IBusinessEventPublisher>();
            var editedUser = CloneUserFromDb(1);

            SetupUniqueKeyViolation();

            eventPublisherMock
                .Setup(m => m.PublishBusinessEvent(It.IsAny<BusinessEvent>()))
                .Verifiable();

            service.EditRecord(editedUser);

            eventPublisherMock.Verify(m => m.PublishBusinessEvent(It.IsAny<BusinessEvent>()), Times.Never);
        }

        [Test]
        public void DeleteRecord_ShouldPublishEvents()
        {
            var service = _container.Resolve<UserCardIndexService>();
            var eventPublisherMock = _container.GetMock<IBusinessEventPublisher>();
            var actualEvents = new List<UserDeletedBusinessEvent>();
            var deletedUser1 = CloneUserFromDb(1);
            var deletedUser2 = CloneUserFromDb(2);

            eventPublisherMock
                .Setup(m => m.PublishBusinessEvent(It.IsAny<BusinessEvent>()))
                .Callback<BusinessEvent>(e => { actualEvents.Add((UserDeletedBusinessEvent) e); })
                .Verifiable();

            service.DeleteRecords(new[] { deletedUser1.Id, deletedUser2.Id });

            eventPublisherMock.Verify(m => m.PublishBusinessEvent(It.IsAny<BusinessEvent>()), Times.Exactly(2));
            AssertCorrectDeletedUserEvent(deletedUser1, actualEvents[0]);
            AssertCorrectDeletedUserEvent(deletedUser2, actualEvents[1]);
        }

        [Test]
        public void DeleteRecord_ShouldPublishEventOnIncompleteDelete()
        {
            var service = _container.Resolve<UserCardIndexService>();
            var eventPublisherMock = _container.GetMock<IBusinessEventPublisher>();
            UserDeletedBusinessEvent actualEvent = null;
            var deletedUser = CloneUserFromDb(1);

            eventPublisherMock
                .Setup(m => m.PublishBusinessEvent(It.IsAny<BusinessEvent>()))
                .Callback<BusinessEvent>(e => { actualEvent = (UserDeletedBusinessEvent)e; })
                .Verifiable();

            var notExistingIds = new[] {21L, 37L};
            service.DeleteRecords(notExistingIds.Concat(new[] { deletedUser.Id }));

            eventPublisherMock.Verify(m => m.PublishBusinessEvent(It.IsAny<BusinessEvent>()), Times.Once);
            AssertCorrectDeletedUserEvent(deletedUser, actualEvent);
        }

        private void  AssertCorrectAddedUserEvent(UserDto addedUser, UserAddedBusinessEvent actualEvent)
        {
            Assert.IsNotNull(actualEvent);
            Assert.AreEqual(_databaseMock.LastId, actualEvent.UserId);
            AssertEqualBusinessEventData(new UserBusinessEventData(addedUser), actualEvent.RecordData);
        }

        private void AssertCorrectEditedUserEvent(UserDto editedUser, UserDto originalUser, UserEditedBusinessEvent actualEvent)
        {
            Assert.IsNotNull(actualEvent);
            Assert.AreEqual(editedUser.Id, actualEvent.UserId);
            AssertEqualBusinessEventData(new UserBusinessEventData(editedUser), actualEvent.NewRecordData);
            AssertEqualBusinessEventData(new UserBusinessEventData(originalUser), actualEvent.OriginalRecordData);
        }

        private void AssertCorrectDeletedUserEvent(UserDto deletedUser, UserDeletedBusinessEvent actualEvent)
        {
            Assert.IsNotNull(actualEvent);
            Assert.AreEqual(deletedUser.Id, actualEvent.UserId);
            AssertEqualBusinessEventData(new UserBusinessEventData(deletedUser), actualEvent.RecordData);
        }

        private void AssertEqualBusinessEventData(UserBusinessEventData expected, UserBusinessEventData actual)
        {
            Assert.AreEqual(expected.FirstName, actual.FirstName);
            Assert.AreEqual(expected.LastName, actual.LastName);
            Assert.AreEqual(expected.Email, actual.Email);
            Assert.AreEqual(expected.Login, actual.Login);
        }

        private UserDto CloneUserFromDb(long id)
        {
            var user = _databaseMock.Users.First(u => u.Id == id);
            CloningHelper.CloneObject(user);
            return _container.Resolve<IClassMapping<User, UserDto>>().CreateFromSource(user);
        }

        private void SetupUniqueKeyViolation()
        {
            var repositoryMock = _container.GetMock<IRepository<User>>();

            repositoryMock
                .Setup(m => m.GetUniqueKeyViolations(It.IsAny<User>()))
                .Returns(new[] {new PropertyGroup {PropertyNames = new[] {"FirstName"}}});
        }
    }
}
