﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Helpers;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Repositories.Scopes;

namespace UnitTests.Application.Business.Common
{
    public class TestCardIndexService : CardIndexDataService<UserDto, User, IAccountsDbScope>
    {
        protected override bool ShouldUseDownwardMappingForFieldResolution => false;

        public TestCardIndexService(ICardIndexServiceDependencies<IAccountsDbScope> dependencies) : base(dependencies)
        {
        }

        protected override IEnumerable<Expression<Func<User, object>>> GetSearchColumns(SearchCriteria searchCriteria)
        {
            yield return u => u.FirstName;
            yield return u => u.LastName;
        }

        protected override NamedFilters<User> NamedFilters
        {
            get
            {
                var filters = CardIndexServiceHelper
                    .BuildSoftDeletableFilters<User>()
                    .Concat(new []
                    {
                        new NamedFilter<User>("LastNameStartsWithP", u => u.LastName.StartsWith("P")),
                        new NamedFilter<User>("FirstNameStartsWithP", u => u.FirstName.StartsWith("P")), 
                    });

                return new NamedFilters<User>(filters);
            }
        }
    }
}