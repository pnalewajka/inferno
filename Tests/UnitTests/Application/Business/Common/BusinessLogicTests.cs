﻿using System;
using System.Linq.Expressions;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.BusinessLogic;
using Smt.Atomic.CrossCutting.Common.Helpers;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace UnitTests.Application.Business.Common
{
    [TestFixture]
    public static class BusinessLogicTests
    {
        [Test]
        public static void TestUsages()
        {
            var isThree = new BusinessLogic<int, bool>(i => i == 3);
            var isFour = new BusinessLogic<int, bool>(i => i == 4);
            var isThreeOrFour = new BusinessLogic<int, bool>(i => isThree.Call(i) || isFour.Call(i));
            var isEqual = new BusinessLogic<int, int, bool>((i, e) => i == e);
            var getDigits = new BusinessLogic<int, int>(i => i.ToString().Length);

            // Call :)
            Assert.IsTrue(isThree.Call(3));
            Assert.IsFalse(isThree.Call(4));
            Assert.IsTrue(isFour.Call(4));
            Assert.IsTrue(isThreeOrFour.Call(3));
            Assert.IsTrue(isThreeOrFour.Call(4));
            Assert.IsFalse(isThreeOrFour.Call(5));
            Assert.IsTrue(isEqual.Call(getDigits.Call(66), 2));
            Assert.IsTrue(isFour.Call(getDigits.Call(6666)));

            // Nested Call replacements :D
            Assert.IsTrue(ExpressionHelper.ExpressionEquals(
                isThreeOrFour.BaseExpression,
                (Expression<Func<int, bool>>)(i => i == 3 || i == 4)));

            // Parameter replacement
            var sum = new BusinessLogic<int, int, int>((x, y) => x + y);
            var sumThree = sum.Inline(3);

            Assert.IsTrue(ExpressionHelper.ExpressionEquals(
                sumThree,
                (Expression<Func<int, int>>)(x => x + 3)));
        }

        [Test]
        public static void ComplexTypesReplacement()
        {
            var hasCountElementsEnumerable = new BusinessLogic<TestObject, long, bool>(
                (e, c) => e.ElementEnumerable.Count() == c);
            var hasCountElementsCollection = new BusinessLogic<TestObject, long, bool>(
                (e, c) => e.ElementCollection.Count() == c);
            var hasCountElementsList = new BusinessLogic<TestObject, long, bool>(
                (e, c) => e.ElementList.Count() == c);
            var hasCountElementsArray = new BusinessLogic<TestObject, long, bool>(
                (e, c) => e.ElementArray.Count() == c);

            var testObject = new TestObject(new List<long> { 3, 445, 24, 1 });

            Assert.IsFalse(hasCountElementsEnumerable.Call(testObject, 2));
            Assert.IsTrue(hasCountElementsEnumerable.Call(testObject, 4));
            Assert.IsFalse(hasCountElementsCollection.Call(testObject, 2));
            Assert.IsTrue(hasCountElementsCollection.Call(testObject, 4));
            Assert.IsFalse(hasCountElementsList.Call(testObject, 2));
            Assert.IsTrue(hasCountElementsList.Call(testObject, 4));
            Assert.IsFalse(hasCountElementsArray.Call(testObject, 2));
            Assert.IsTrue(hasCountElementsArray.Call(testObject, 4));
        }
    }

    class TestObject
    {
        public IEnumerable<long> ElementEnumerable { get; set; }
        public ICollection<long> ElementCollection { get; set; }
        public List<long> ElementList { get; set; }
        public long[] ElementArray { get; set; }

        public TestObject(IEnumerable<long> elements)
        {
            ElementEnumerable = elements;
            ElementCollection = new Collection<long>(elements.ToList());
            ElementList = new List<long>(elements.ToList());
            ElementArray = elements.ToArray();
        }
    }
}
