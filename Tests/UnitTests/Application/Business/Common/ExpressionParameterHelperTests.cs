﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Application.Business.Common
{
    [TestFixture]
    public static class ExpressionParameterHelperTests
    {
        [Test]
        public static void ExpressionEqualsTest()
        {
            Assert.IsTrue(ExpressionHelper.ExpressionEquals(
                (Expression<Func<bool>>)(() => true),
                (Expression<Func<bool>>)(() => true)
                ));

            Assert.IsFalse(ExpressionHelper.ExpressionEquals(
                (Expression<Func<bool>>)(() => false),
                (Expression<Func<bool>>)(() => true)
                ));

            Assert.IsFalse(ExpressionHelper.ExpressionEquals(
                (Expression<Func<int>>)(() => 1),
                (Expression<Func<long>>)(() => 1)
                ));

            Assert.IsTrue(ExpressionHelper.ExpressionEquals(
                (Expression<Func<int, bool>>)(e => e == 33),
                (Expression<Func<int, bool>>)(e => e == 33)
                ));

            Assert.IsFalse(ExpressionHelper.ExpressionEquals(
                (Expression<Func<int, bool>>)(e => e == 20),
                (Expression<Func<int, bool>>)(e => e == 33)
                ));

            Assert.IsFalse(ExpressionHelper.ExpressionEquals(
                (Expression<Func<int, bool>>)(e => e == 20 * 2 + 1),
                (Expression<Func<int, bool>>)(e => e == 20 * (2 + 1))
                ));
        }

        [Test]
        public static void ParameterReplacerTest()
        {
            var baseExpression = (Expression<Func<int, int, bool>>)((e, a) => e == a);

            var baseNoA = ExpressionParameterHelper.Replace(baseExpression, new Dictionary<ParameterExpression, Expression>
            {
                {baseExpression.Parameters[1], Expression.Constant(34)}
            });

            Assert.IsTrue(ExpressionHelper.ExpressionEquals(
                baseNoA,
                (Expression<Func<int, bool>>)(e => e == 34)
                ));
        }
    }
}
