﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.MicroKernel.Registration;
using Moq;
using NUnit.Framework;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Data.Repositories.Services;
using Smt.Atomic.Tests.Common;
using Smt.Atomic.Tests.Common.AutoMock;
using UnitTests.Application.Business.Accounts;
using Smt.Atomic.CrossCutting.Common.Consts;

namespace UnitTests.Application.Business.Common
{
    [TestFixture]
    public class CardIndexDataServiceTests
    {
        private AutoMockingContainer _container;
        private UsersDatabaseMock _databaseMock;
        private TestContainerConfiguration _testContainerConfiguration;
        private Mock<IAtomicPrincipal> _principalMock;

        [SetUp]
        public void Setup()
        {
            _principalMock = new Mock<IAtomicPrincipal>();

            _testContainerConfiguration = new TestContainerConfiguration();
            _testContainerConfiguration.Configure(ContainerType.UnitTests);

            _container = _testContainerConfiguration.AutoMockingContainer;
            _databaseMock = new UsersDatabaseMock();
            _container.RegisterDatabase(_databaseMock);

            _container.Register(Component
                .For<ICardIndexServiceDependencies<IAccountsDbScope>>()
                .ImplementedBy<CardIndexDataServiceDependencies<IAccountsDbScope>>()
                .Named(Guid.NewGuid().ToString())
                .LifestyleTransient()
                .DependsOn(
                    Dependency.OnValue(
                        PropertyHelper.GetPropertyName<ICardIndexServiceDependencies<IAccountsDbScope>>(p => p.PrincipalProvider),
                        GetPrincipalProviderMock().Object
                    ),
                    Dependency.OnValue<ISystemParameterService>(GetSytemParameterServiceStub())
                )
            );

            _container.Register(Component.For<ICardIndexDataService<UserDto>>().ImplementedBy<TestCardIndexService>());
            _container.Register(Component.For(typeof(IUnitOfWorkService<>)).ImplementedBy(typeof(UnitOfWorkService<>)).LifestyleTransient());
            _container.Register(Component.For<IClassMappingFactory>().ImplementedBy<ClassMappingFactory>().LifestyleTransient());
            _container.Register(Component.For<IClassMapping<User, UserDto>>().ImplementedBy<UserToUserDtoMapping>().LifestyleTransient());
            _container.Register(Component.For<IClassMapping<UserDto, User>>().ImplementedBy<UserDtoToUserMapping>().LifestyleTransient());
            _container.GetMock<IDbContextWrapperFactory>()
                .Setup(m => m.Create()).Returns(new Mock<IDbContextWrapper>().Object);
            _container.Register(Component.For(typeof(IRepository<>)).UsingFactoryMethod((k, c) => _databaseMock.GetRepositoryMockFactoryMethod(c.RequestedType.GenericTypeArguments[0])));
        }

        [Test]
        public void GetRecordById_SoftDeletedUserShouldNotBePresent()
        {
            SetupPrincipalRoles(new SecurityRoleType[0]);

            var userCardIndexService = _container.Resolve<ICardIndexDataService<UserDto>>();
            _databaseMock.Users.Single(u => u.Id == 1).IsDeleted = true;

            var result = userCardIndexService.GetRecordByIdOrDefault(1);
            Assert.IsNull(result);
        }

        [Test]
        public void GetRecordByIdOrDefault_SoftDeletedUserShouldNotBePresent()
        {
            SetupPrincipalRoles(new SecurityRoleType[0]);

            var userCardIndexService = _container.Resolve<ICardIndexDataService<UserDto>>();
            _databaseMock.Users.Single(u => u.Id == 1).IsDeleted = true;

            Assert.IsNull(userCardIndexService.GetRecordByIdOrDefault(1));
        }

        [Test]
        public void GetRecordById_SoftDeletedUserShouldBePresent()
        {
            SetupPrincipalRoles(new [] { SecurityRoleType.CanViewSoftDeletedEntities });

            var userCardIndexService = _container.Resolve<ICardIndexDataService<UserDto>>();
            _databaseMock.Users.Single(u => u.Id == 1).IsDeleted = true;

            Assert.NotNull(userCardIndexService.GetRecordById(1));
        }

        [Test]
        public void GetRecordByIdOrDefault_SoftDeletedUserShouldBePresent()
        {
            SetupPrincipalRoles(new[] { SecurityRoleType.CanViewSoftDeletedEntities });

            var userCardIndexService = _container.Resolve<ICardIndexDataService<UserDto>>();
            _databaseMock.Users.Single(u => u.Id == 1).IsDeleted = true;

            Assert.NotNull(userCardIndexService.GetRecordByIdOrDefault(1));
        }

        [Test]
        public void GetRecords_DefaultCriteriaShouldReturnAllRecords()
        {
            var userCardIndexService = _container.Resolve<ICardIndexDataService<UserDto>>();
            var userDtos = userCardIndexService.GetRecords(new QueryCriteria());

            Assert.AreEqual(_databaseMock.Users.Count, userDtos.Rows.Count);
        }

        [Test]
        public void GetRecords_SoftDeletedUserShouldNotBePresent()
        {
            var userCardIndexService = _container.Resolve<ICardIndexDataService<UserDto>>();
            _databaseMock.Users.First().IsDeleted = true;

            var userDtos = userCardIndexService.GetRecords(new QueryCriteria());

            Assert.AreEqual(_databaseMock.Users.Where(u => !u.IsDeleted).ToList().Count, userDtos.Rows.Where(u => !u.IsDeleted).ToList().Count);
        }

        [Test]
        public void GetRecords_SoftDeleteFilterExistingShouldGetExistingRecords()
        {
            var userCardIndexService = _container.Resolve<ICardIndexDataService<UserDto>>();
            var queryCriteria = GetQueryCriteriaBySoftDeletableFilter(SoftDeletableStateFilter.Existing);
            _databaseMock.Users.First().IsDeleted = true;

            var userDtos = userCardIndexService.GetRecords(queryCriteria);

            Assert.AreEqual(_databaseMock.Users.Where(u => !u.IsDeleted).ToList().Count, userDtos.Rows.ToList().Count);
        }

        [Test]
        public void GetRecords_SoftDeleteFilterAllShouldGetAllRecords()
        {
            SetupPrincipalRoles(new[] { SecurityRoleType.CanViewSoftDeletedEntities });

            var userCardIndexService = _container.Resolve<ICardIndexDataService<UserDto>>();
            var queryCriteria = GetQueryCriteriaBySoftDeletableFilter(SoftDeletableStateFilter.All);
            _databaseMock.Users.First().IsDeleted = true;

            var userDtos = userCardIndexService.GetRecords(queryCriteria);
            
            Assert.AreEqual(_databaseMock.Users.ToList().Count, userDtos.Rows.ToList().Count);
        }

        [Test]
        public void GetRecords_SoftDeleteFilterDeletedShouldGetDeletedRecords()
        {
            SetupPrincipalRoles(new[] { SecurityRoleType.CanViewSoftDeletedEntities });

            var userCardIndexService = _container.Resolve<ICardIndexDataService<UserDto>>();
            var queryCriteria = GetQueryCriteriaBySoftDeletableFilter(SoftDeletableStateFilter.Deleted);
            _databaseMock.Users.First().IsDeleted = true;

            var userDtos = userCardIndexService.GetRecords(queryCriteria);

            Assert.AreEqual(_databaseMock.Users.Where(u => u.IsDeleted).ToList().Count, userDtos.Rows.ToList().Count);
        }

        [Test]
        public void GetRecords_ShouldBeSortedAscendingAccordingToSortCriteria()
        {
            var userCardIndexService = _container.Resolve<ICardIndexDataService<UserDto>>();
            var queryCriteria = new QueryCriteria
            {
                OrderBy = new List<SortingCriterion>
                {
                    SortingCriterion.ForProperty<UserDto>(u => u.LastName)
                }
            };

            var userDtos = userCardIndexService.GetRecords(queryCriteria);

            var properOrder = _databaseMock.Users.Select(u => u.LastName).OrderBy(u => u);
            Assert.IsTrue(userDtos.Rows.Select(u => u.LastName).SequenceEqual(properOrder));
        }

        [Test]
        public void GetRecords_ShouldBeSortedDescendingAccordingToSortCriteria()
        {
            var userCardIndexService = _container.Resolve<ICardIndexDataService<UserDto>>();
            var queryCriteria = new QueryCriteria
            {
                OrderBy = new List<SortingCriterion>
                {
                    SortingCriterion.ForProperty<UserDto>(u => u.LastName, false)
                }
            };

            var userDtos = userCardIndexService.GetRecords(queryCriteria);

            var propertOrder = _databaseMock.Users.Select(u => u.LastName).OrderByDescending(u => u);
            Assert.IsTrue(userDtos.Rows.Select(u => u.LastName).SequenceEqual(propertOrder));
        }

        [Test]
        public void GetRecords_ShouldFilterBySearchText()
        {
            var userCardIndexService = _container.Resolve<ICardIndexDataService<UserDto>>();
            var queryCriteria = new QueryCriteria
            {
                SearchPhrase = "Doe"
            };

            var userDtos = userCardIndexService.GetRecords(queryCriteria);

            var searchText = queryCriteria.SearchPhrase.ToLower();
            Assert.IsTrue(userDtos.Rows.Any(u => u.LastName.ToLower().Contains(searchText) || u.FirstName.ToLower().Contains(searchText)));
        }

        [Test]
        public void OrderBy_ShouldWorkWithNullable()
        {
            var userCardIndexService = _container.Resolve<ICardIndexDataService<UserDto>>();
            var queryCriteria = new QueryCriteria
            {
                OrderBy = new List<SortingCriterion>
                {
                    new SortingCriterion(PropertyHelper.GetPropertyName<UserDto>(u => u.ImpersonatedById), true)
                }
            };

            var userDtos = userCardIndexService.GetRecords(queryCriteria);
            var properOrder = _databaseMock.Users.OrderBy(u => u.ImpersonatedById).Select(u => u.Id);
            var currentOrder = userDtos.Rows.Select(u => u.Id);

            Assert.IsTrue(currentOrder.SequenceEqual(properOrder));
        }

        [Test]
        public void NamedFilters_ShouldWork()
        {
            var filterName = NamingConventionHelper.ConvertPascalCaseToHyphenated("LastNameStartsWithP");
            var userCardIndexService = _container.Resolve<ICardIndexDataService<UserDto>>();
            var queryCriteria = new QueryCriteria
            {
                Filters = new List<FilterCodeGroup>
                {
                    new FilterCodeGroup
                    {
                        Codes = new List<string>
                        {
                            filterName
                        }
                    }
                }
            };

            var userDtos = userCardIndexService.GetRecords(queryCriteria);
            Assert.AreEqual(_databaseMock.Users.Count(u => u.LastName.StartsWith("P")), userDtos.Rows.Count); 
        }

        private ISystemParameterService GetSytemParameterServiceStub(int defaultPageSize = 25)
        {
            var systemParametersServiceStub = new Mock<ISystemParameterService>();
            systemParametersServiceStub.Setup(x => x.GetParameter<int>(ParameterKeys.DefaultPageSize))
                                       .Returns(defaultPageSize);

            return systemParametersServiceStub.Object;
        }

        private Mock<IPrincipalProvider> GetPrincipalProviderMock()
        {
            var principalProviderMock = new Mock<IPrincipalProvider>();

            SetupPrincipalRoles(new SecurityRoleType[0]);
            principalProviderMock.Setup(p => p.Current).Returns(_principalMock.Object);

            return principalProviderMock;
        }

        private void SetupPrincipalRoles(SecurityRoleType[] roles)
        {
            _principalMock
                .Setup(p => p.Roles)
                .Returns(roles.Select(r => r.ToString()).ToArray());

            _principalMock
                .Setup(p => p.IsInRole(It.IsAny<SecurityRoleType>()))
                .Returns<SecurityRoleType>(i => roles.Any(r => r == i));
        }


        private static QueryCriteria GetQueryCriteriaBySoftDeletableFilter(SoftDeletableStateFilter filter)
        {
            return new QueryCriteria
            {
                Filters = new List<FilterCodeGroup>
                {
                    new FilterCodeGroup
                    {
                        Codes = new List<string>
                        {
                            filter.ToString()
                        }
                    }
                }
            };
        }
    }
}