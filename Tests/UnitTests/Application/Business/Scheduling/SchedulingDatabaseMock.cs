﻿using System.Collections.Generic;
using Smt.Atomic.Data.Entities.Modules.Scheduling;
using Smt.Atomic.Tests.Common.Wrappers;

namespace UnitTests.Application.Business.Scheduling
{
    internal class SchedulingDatabaseMock : DatabaseMock
    {
        private readonly List<Pipeline> _pipelines = new List<Pipeline>
        {
            new Pipeline {Name = "SlowPipe", Id = 1},
            new Pipeline {Name = "FastPipe", Id = 2},
        };

        private readonly List<JobDefinition> _jobDefinitions = new List<JobDefinition>
        {
            new JobDefinition {ClassIdentifier = "Job.Job1", PipelineId = 1, IsEnabled = true, ScheduleSettings = "10 * * * *"},
            new JobDefinition {ClassIdentifier = "Job.Job2", PipelineId = 1, IsEnabled = true, ScheduleSettings = "12 * * * *"},
            new JobDefinition {ClassIdentifier = "Job.Job3", PipelineId = 2, IsEnabled = true, ScheduleSettings = "13 * * * *"},
            new JobDefinition {ClassIdentifier = "Job.Job4", PipelineId = 1, IsEnabled = true, ScheduleSettings = "15 * * * *"},
        };

        public List<JobDefinition> JobDefinitions => _jobDefinitions;

        public List<Pipeline> Pipelines => _pipelines;

        public SchedulingDatabaseMock()
        {
            RegisterNavigationPropertyHandler<JobDefinition, SchedulingDatabaseMock>((j, c) => j.Pipeline = c.GetById<Pipeline>(j.PipelineId));
            RegisterNavigationPropertyHandler<JobDefinition, SchedulingDatabaseMock>((j, c) => j.Pipeline.JobDefinitions.Add(j));
            AddEntities(_pipelines);
            AddEntities(_jobDefinitions);
        }
    }
}
