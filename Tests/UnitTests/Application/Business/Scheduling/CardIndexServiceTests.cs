﻿using System.Linq;
using Castle.MicroKernel.Registration;
using Moq;
using NUnit.Framework;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.Business.Scheduling.Dto;
using Smt.Atomic.Business.Scheduling.Interfaces;
using Smt.Atomic.Business.Scheduling.Services;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.Scheduling;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Data.Repositories.Services;
using Smt.Atomic.Tests.Common;

namespace UnitTests.Application.Business.Scheduling
{
    [TestFixture]
    public class CardIndexServiceTests
    {
        private SchedulingDatabaseMock _databaseMock;
        private TestContainerConfiguration _testContainerConfiguration;

        [SetUp]
        public void Setup()
        {
            _testContainerConfiguration = new TestContainerConfiguration();
            _testContainerConfiguration.Configure(ContainerType.UnitTests);
            _databaseMock = new SchedulingDatabaseMock();
            _testContainerConfiguration.AutoMockingContainer.RegisterDatabase(_databaseMock);
        }

        [Test]
        public void EditRecord_TestChangingValueOnNamePropertyViaCardIndexService()
        {
            _testContainerConfiguration.AutoMockingContainer.Register(Component.For<IPipelineCardIndexService>().ImplementedBy<PipelineCardIndexService>());
            _testContainerConfiguration.AutoMockingContainer.Register(Component.For(typeof(IUnitOfWorkService<>)).ImplementedBy(typeof(UnitOfWorkService<>)).LifestyleTransient());
            _testContainerConfiguration.AutoMockingContainer.Register(Component.For(typeof(IRepository<>)).UsingFactoryMethod((k, c) => _databaseMock.GetRepositoryMockFactoryMethod(c.RequestedType.GenericTypeArguments[0])));
            _testContainerConfiguration.AutoMockingContainer.Register(Component.For<ICardIndexServiceDependencies<ISchedulingDbScope>>().ImplementedBy<CardIndexDataServiceDependencies<ISchedulingDbScope>>().LifestyleTransient());
            _testContainerConfiguration.AutoMockingContainer.Register(Component.For<IClassMappingFactory>().ImplementedBy<ClassMappingFactory>().LifestyleTransient());
            _testContainerConfiguration.AutoMockingContainer.Register(Component.For<IClassMapping<Pipeline, PipelineDto>>().ImplementedBy<PipelineToPipelineDtoMapping>().LifestyleTransient());
            _testContainerConfiguration.AutoMockingContainer.Register(Component.For<IClassMapping<PipelineDto, Pipeline>>().ImplementedBy<PipelineDtoToPipelineMapping>().LifestyleTransient());

            var dbContextWrapperFactoryMock = _testContainerConfiguration.AutoMockingContainer.GetMock<IDbContextWrapperFactory>();
            var dbContextWrapper = new Mock<IDbContextWrapper>();
            dbContextWrapperFactoryMock.Setup(m => m.Create()).Returns(dbContextWrapper.Object);

            var unitOfWorkService = _testContainerConfiguration.AutoMockingContainer.Resolve<IUnitOfWorkService<ISchedulingDbScope>>();

            using (var unitOfWork = unitOfWorkService.Create())
            {
                var pipelineRepository = unitOfWork.Repositories.Pipelines;

                var pipelineCardIndexService =
                    _testContainerConfiguration.AutoMockingContainer.Resolve<IPipelineCardIndexService>();

                var entityInRepository = pipelineRepository.First();

                var pipelineDtoObject = pipelineCardIndexService.GetRecordById(entityInRepository.Id);
                pipelineDtoObject.Name = "new name";

                pipelineCardIndexService.EditRecord(pipelineDtoObject);

                //This is expected behavior, mocking repository should always return same object from mock respository internal list storing entities.
                //Editrecord on cardindex creates detached entity, therefore mocking repository copies public properties of detached object to one on internal list
                Assert.AreEqual(pipelineDtoObject.Name, entityInRepository.Name);
            }

        }

        [Test]
        public void DeleteRecord_TestRecordDeletionViaCardIndexService()
        {
            _testContainerConfiguration.AutoMockingContainer.Register(Component.For<IPipelineCardIndexService>().ImplementedBy<PipelineCardIndexService>());
            _testContainerConfiguration.AutoMockingContainer.Register(Component.For<ICardIndexServiceDependencies<ISchedulingDbScope>>().ImplementedBy<CardIndexDataServiceDependencies<ISchedulingDbScope>>().LifestyleTransient());
            _testContainerConfiguration.AutoMockingContainer.Register(Component.For(typeof(IUnitOfWorkService<>)).ImplementedBy(typeof(UnitOfWorkService<>)).LifestyleTransient());
            _testContainerConfiguration.AutoMockingContainer.Register(Component.For(typeof(IRepository<>)).UsingFactoryMethod((k, c) => _databaseMock.GetRepositoryMockFactoryMethod(c.RequestedType.GenericTypeArguments[0])));
            _testContainerConfiguration.AutoMockingContainer.Register(Component.For<IClassMappingFactory>().ImplementedBy<ClassMappingFactory>().LifestyleTransient());
            _testContainerConfiguration.AutoMockingContainer.Register(Component.For<IClassMapping<User, UserDto>>().ImplementedBy<UserToUserDtoMapping>().LifestyleTransient());

            var dbContextWrapperFactoryMock = _testContainerConfiguration.AutoMockingContainer.GetMock<IDbContextWrapperFactory>();
            var dbContextWrapper = new Mock<IDbContextWrapper>();
            dbContextWrapperFactoryMock.Setup(m => m.Create()).Returns(dbContextWrapper.Object);

            var unitOfWorkService = _testContainerConfiguration.AutoMockingContainer.Resolve<IUnitOfWorkService<ISchedulingDbScope>>();

            using (var unitOfWork = unitOfWorkService.Create())
            {
                var pipelineRepository = unitOfWork.Repositories.Pipelines;
                var pipelineCardIndexService = _testContainerConfiguration.AutoMockingContainer.Resolve<IPipelineCardIndexService>();

                var entityInRepository = pipelineRepository.First();
                pipelineCardIndexService.DeleteRecords(new[] { entityInRepository.Id });
                var pipelineItem = pipelineRepository.SingleOrDefault(p => p.Id == entityInRepository.Id);

                Assert.AreEqual(null, pipelineItem);
            }
        }
    }
}
