﻿using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace UnitTests.Application.Business.Scheduling.TestJobs
{
    [Identifier("Jobs.TestJob")]
    public class TestJob : IJob
    {
        public JobResult DoJob(JobExecutionContext executionContext)
        {
            return new JobResult {IsJobFinishedGracefully = true};
        }
    }
}
