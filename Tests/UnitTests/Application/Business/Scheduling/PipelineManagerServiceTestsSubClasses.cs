﻿using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace UnitTests.Application.Business.Scheduling
{
    public partial class PipelineManagerServiceTests
    {
        [Identifier("Job.Job1")]
        public class TestJob1 : IJob 
        {
            public JobResult DoJob(JobExecutionContext executionContext)
            {
                return new JobResult {IsJobFinishedGracefully = true};
            }
        }

        [Identifier("Job.Job2")]
        public class TestJob2 : IJob
        {
            public JobResult DoJob(JobExecutionContext executionContext)
            {
                return new JobResult { IsJobFinishedGracefully = true };
            }
        }

        [Identifier("Job.Job3")]
        public class TestJob3 : IJob
        {
            public JobResult DoJob(JobExecutionContext executionContext)
            {
                return new JobResult { IsJobFinishedGracefully = true };
            }
        }

        [Identifier("Job.Job4")]
        public class TestJob4 : IJob
        {
            public JobResult DoJob(JobExecutionContext executionContext)
            {
                return new JobResult { IsJobFinishedGracefully = true };
            }
        }

    }
}
