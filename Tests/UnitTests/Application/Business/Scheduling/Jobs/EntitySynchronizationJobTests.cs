﻿using System;
using System.Linq;
using System.Threading;
using Castle.Core.Logging;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture.NUnit2;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Scheduling.Interfaces;

namespace UnitTests.Application.Business.Scheduling.Jobs
{
    [TestFixture]
    public class EntitySynchronizationJobTests
    {
        private Mock<IEntitySynchronizationConfigurationService> _entitySynchronizationConfigurationServiceMock;
        private Mock<TestEntitySynchronizationJob> _entitySynchronizationJobMock;
        private JobExecutionContext _jobExecutionContext;
        private static readonly string LastSynchronizationTimeParameter = "EntitySynchronization.TestExternalEntity.TestEntity.LastSynchronizationTime";

        [SetUp]
        public void SetUp()
        {
            _entitySynchronizationConfigurationServiceMock = new Mock<IEntitySynchronizationConfigurationService>();
            _jobExecutionContext = new JobExecutionContext(new CancellationToken(false), null, null, null, null, 1, false, TimeSpan.MaxValue);
            _entitySynchronizationJobMock = new Mock<TestEntitySynchronizationJob>(
                    _entitySynchronizationConfigurationServiceMock.Object, new Mock<ILogger>().Object)
                {
                    CallBase = true
                };
            _entitySynchronizationJobMock.Setup(m => m.TestUpsert(It.IsAny<TestExternalEntity>())).Returns((TestExternalEntity e) => e.LastUpdated);
        }
        
        [Test]
        public void Synchronize_MultipleEntitiesMeetingConstraints_TimeModifiedForEachEntity()
        {
            var first = new TestExternalEntity
            {
                Field = "First",
                LastUpdated = new DateTime(2017, 4, 28)
            };
            var second = new TestExternalEntity
            {
                Field = "Second",
                LastUpdated = first.LastUpdated.AddDays(1)
            };

            // Arrange
            _entitySynchronizationJobMock.Setup(m => m.TestGetModifiedData(It.IsAny<DateTime>())).Returns(new[]
            {
                first, second
            }.AsQueryable());
            _entitySynchronizationConfigurationServiceMock
                .Setup(m => m.GetLastSynchronizationTime(LastSynchronizationTimeParameter))
                .Returns(first.LastUpdated.AddDays(-1));

            // Act
            _entitySynchronizationJobMock.Object.DoJob(_jobExecutionContext);

            // Assert
            _entitySynchronizationJobMock.Verify(m => m.TestUpsert(It.Is<TestExternalEntity>(e => e.Field == first.Field)), Times.Once);
            _entitySynchronizationJobMock.Verify(m => m.TestUpsert(It.Is<TestExternalEntity>(e => e.Field == second.Field)), Times.Once);
            _entitySynchronizationConfigurationServiceMock.Verify(m => m.SetLastSynchronizationTime(LastSynchronizationTimeParameter, first.LastUpdated));
            _entitySynchronizationConfigurationServiceMock.Verify(m => m.SetLastSynchronizationTime(LastSynchronizationTimeParameter, second.LastUpdated));
        }
    }
}