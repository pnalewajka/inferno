﻿using System;
using System.Collections.Generic;
using Castle.Core.Logging;
using Smt.Atomic.Business.Scheduling.Interfaces;
using Smt.Atomic.Business.Scheduling.Jobs;

namespace UnitTests.Application.Business.Scheduling.Jobs
{
    public class TestEntitySynchronizationJob : EntitySynchronizationJob<TestExternalEntity, TestEntity>
    {
        public TestEntitySynchronizationJob(IEntitySynchronizationConfigurationService entitySynchronizationConfigurationService, ILogger logger) : base(entitySynchronizationConfigurationService, logger)
        {
        }

        protected override DateTime? Upsert(TestExternalEntity externalEntity)
        {
            return TestUpsert(externalEntity);
        }

        public virtual DateTime? TestUpsert(TestExternalEntity externalEntity)
        {
            throw new NotImplementedException();
        }

        protected override IEnumerable<TestExternalEntity> GetModifiedData(DateTime? dateFrom)
        {
            return TestGetModifiedData(dateFrom);
        }

        public virtual IEnumerable<TestExternalEntity> TestGetModifiedData(DateTime? dateFrom)
        {
            throw new NotImplementedException();
        }
    }

    public class TestExternalEntity
    {
        public string Field;
        public DateTime LastUpdated;
    }

    public class TestEntity
    {
    }
}