﻿using System.IO;
using Castle.MicroKernel.Registration;
using NUnit.Framework;
using Smt.Atomic.Business.Scheduling.Interfaces;
using Smt.Atomic.Business.Scheduling.Services;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Models;
using Smt.Atomic.Tests.Common;

namespace UnitTests.Application.Business.Scheduling
{
    [TestFixture]
    public partial class PipelineManagerServiceTests
    {
        private const string SchedulerName = "TestScheduler";
        private const string HostName = "n333pv0001";

        private SchedulingDatabaseMock _databaseMock;
        private TestContainerConfiguration _testContainerConfiguration;

        [SetUp]
        public void Setup()
        {
            _testContainerConfiguration = new TestContainerConfiguration();
            _testContainerConfiguration.Configure(ContainerType.UnitTests);
            _databaseMock = new SchedulingDatabaseMock();
            _testContainerConfiguration.AutoMockingContainer.RegisterDatabase(_databaseMock);

            var settingsProviderServiceMock = _testContainerConfiguration.AutoMockingContainer.GetMock<ISettingsProvider>();
            settingsProviderServiceMock
                .Setup(s => s.JobSchedulerSettings)
                .Returns(new JobSchedulerSettings { SchedulerName = SchedulerName });

            var dnsServiceMock = _testContainerConfiguration.AutoMockingContainer.GetMock<IDnsService>();
            dnsServiceMock.Setup(d => d.GetHostName()).Returns(HostName);
        }

        //TODO: uncomment when scheduler is finished
        //[Test]
        //public void PipelineManagerService_GeneralTest()
        //{
        //    const string slowpipe = "SlowPipe";
        //    const string fastpipe = "FastPipe";

        //    var pipelines = new[]
        //    {
        //        slowpipe,
        //        fastpipe
        //    };

        //    _testContainerConfiguration.AutoMockingContainer.Register(Component.For<IPipelineManagerService>().ImplementedBy<PipelineManagerService>().LifestyleSingleton());
        //    _testContainerConfiguration.AutoMockingContainer.Register(Component.For<ISchedulerService>().ImplementedBy<SchedulerService>().LifestyleTransient());
        //    _testContainerConfiguration.AutoMockingContainer.Register(Component.For<IJobDefinitionService>().ImplementedBy<JobDefinitionService>().LifestyleTransient());
        //    _testContainerConfiguration.AutoMockingContainer.Register(Component.For<IUnitOfWorkService>().ImplementedBy<UnitOfWorkService>().LifestyleTransient());
        //    _testContainerConfiguration.AutoMockingContainer.Register(Component.For<IJobLogService>().ImplementedBy<JobLogService>().LifestyleTransient());

        //    _testContainerConfiguration.AutoMockingContainer.RegisterMock(_databaseMock.GetRepositoryMock<JobLog>());

        //    var pipelineManagerService = _testContainerConfiguration.AutoMockingContainer.Resolve<IPipelineManagerService>();

        //    foreach (var pipeline in pipelines)
        //    {
        //        pipelineManagerService.CreateAndStartPipeline(pipeline);
        //    }

        //    Assert.AreEqual(pipelines.Count(), pipelineManagerService.PipelineCount);

        //    var jobLogRepository = _testContainerConfiguration.AutoMockingContainer.Resolve<IRepository<JobLog>>();
        //    Thread.Sleep(2 * 1000);
        //    pipelineManagerService.Stop();

        //    var logItems = jobLogRepository.ToList();
        //    Assert.IsTrue(logItems.Count > 1, "At least 2 logs should appear in repository");
        //    Assert.IsTrue(logItems.All(l => l.Host == HostName),"All of the logs should have host equal to mocked one");
        //    Assert.IsTrue(logItems.Any(l => l.Pipeline == slowpipe),"At least one log should've been inserted by slow pipe");
        //    Assert.IsTrue(logItems.Any(l => l.Pipeline == fastpipe), "At least one log should've been inserted by slow pipe");
        //    Assert.IsTrue(logItems.All(l => l.SchedulerName == SchedulerName), "Scheduler field of logs should be equal to mocked one");
        //}

        [Test]
        [ExpectedException(typeof(InvalidDataException))]
        public void PipelineManagerService_SamePipelineTwice()
        {
            _testContainerConfiguration.AutoMockingContainer.Register(Component.For<IPipelineManagerService>().ImplementedBy<PipelineManagerService>().LifestyleSingleton());
            _testContainerConfiguration.AutoMockingContainer.Register(Component.For<ISchedulerService>().ImplementedBy<SchedulerService>());
            _testContainerConfiguration.AutoMockingContainer.Register(Component.For<IJobDefinitionService>().ImplementedBy<JobDefinitionService>());

            var pipelineManagerService = _testContainerConfiguration.AutoMockingContainer.Resolve<IPipelineManagerService>();

            var pipelines = new[]
            {
                "SlowPipe" , "SlowPipe"
            };

            foreach (var pipeline in pipelines)
            {
                pipelineManagerService.CreateAndStartPipeline(pipeline);
            }
        }
    }
}