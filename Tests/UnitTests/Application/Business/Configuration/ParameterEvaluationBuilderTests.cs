﻿using System;
using NUnit.Framework;
using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.Business.Configuration.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Tests.Common;

namespace UnitTests.Application.Business.Configuration
{
    [TestFixture]
    public partial class ParameterEvaluationBuilderTests
    {
        private const string SampleClassParameterValue = "[] => {'Value': 'Good'}\r\n[now.Month == 12] => {'Value': 'Awesome'}";
        private const string SampleClassParameterValue2 = "[] => {'Value': 'Good'}\r\n[now.Month == 12 || now.Month == 1] => {'Value': 'Pretty good'}";
        private const string SampleClassParameterValue3 = "[] => {'Value': 'Good'}\r\n[now.Month == 12 || now.Month == 1] => {'Value': 'Pretty good'}\r\n[isZenek] => {'Value': 'Just fine'}";

        private const string SampleStringArrayParameterValue = "new string[] { \"Mick Jagger\", \"Keith Richards\" }";

        [SetUp]
        protected void Setup()
        {
            var container = new TestContainerConfiguration();
            container.Configure(ContainerType.UnitTests);
        }

        [Test]
        public void GetValue_ReturnsExpectedEvaluationBaseContextSampleClass()
        {
            var builder = new ParameterEvaluationBuilder(SampleClassParameterValue, typeof(SampleClass), typeof(ParameterContext));
            var context = new ParameterContext(new DateTime(2014, 12, 1));

            var result = builder.GetValue(context) as SampleClass;

            Assert.IsNotNull(result);
            Assert.AreEqual("Awesome", result.Value);
        }

        [Test]
        public void GetValue_ReturnsDifferentEvaluationsForDifferentValues()
        {
            var builder1 = new ParameterEvaluationBuilder(SampleClassParameterValue, typeof(SampleClass), typeof(ParameterContext));
            var builder2 = new ParameterEvaluationBuilder(SampleClassParameterValue2, typeof(SampleClass), typeof(ParameterContext));

            var context = new ParameterContext(new DateTime(2014, 12, 1));

            var result1 = builder1.GetValue(context) as SampleClass;
            var result2 = builder2.GetValue(context) as SampleClass;

            Assert.IsNotNull(result1);
            Assert.AreEqual("Awesome", result1.Value);

            Assert.IsNotNull(result2);
            Assert.AreEqual("Pretty good", result2.Value);
        }

        [Test]
        public void GetValue_ReturnsExpectedEvaluationSampleContextSampleClass()
        {
            var builder = new ParameterEvaluationBuilder(SampleClassParameterValue3, typeof(SampleClass), typeof(SampleParameterContext));
            var context = new SampleParameterContext(new DateTime(2014, 12, 1))
            {
                IsZenek = true
            };

            var result = builder.GetValue(context) as SampleClass;

            Assert.IsNotNull(result);
            Assert.AreEqual("Just fine", result.Value);
        }

        [Test]
        public void GetValue_ReturnsExpectedEvaluationSampleContextSampleClassByIdentifier()
        {
            var builder = new ParameterEvaluationBuilder(SampleClassParameterValue3, "UnitTests.SampleValue", "UnitTests.SampleContext");
            var context = new SampleParameterContext(new DateTime(2014, 12, 1))
            {
                IsZenek = true
            };

            var result = builder.GetValue(context) as SampleClass;

            Assert.IsNotNull(result);
            Assert.AreEqual("Just fine", result.Value);
        }

        [Test]
        public void GetValue_ReturnsExpectedValue()
        {
            var builder = new ParameterEvaluationBuilder(SampleClassParameterValue3, "UnitTests.SampleValue", "UnitTests.SampleContext");

            var context = new SampleParameterContext(new DateTime(2014, 12, 1))
            {
                IsZenek = true
            };

            var result = builder.GetValue(context) as SampleClass;

            Assert.IsNotNull(result);
            Assert.AreEqual("Just fine", result.Value);
        }

        [Test]
        public void GetValue_ArrayType()
        {
            var builder = new ParameterEvaluationBuilder(SampleStringArrayParameterValue, "UnitTests.SampleValue", "UnitTests.SampleContext");
            var context = new SampleParameterContext(new DateTime(2014, 12, 1))
            {
                IsZenek = true
            };

            var result = builder.GetValue(context) as string[];

            Assert.IsNotNull(result);
            Assert.AreEqual(result.Length, 2);
            Assert.AreEqual("Keith Richards", result[1]);
        }

        [Test]
        public void GetValue_JsonString()
        {
            var builder = new ParameterEvaluationBuilder("[{\"a\": 123, \"b\": [1,2,3]}, {\"x\": 0, \"c\": \"works\"}]", null, "UnitTests.SampleContext");
            var context = new SampleParameterContext(new DateTime(2014, 12, 1))
            {
                IsZenek = true
            };

            var result = builder.GetValue(context) as Newtonsoft.Json.Linq.JArray;

            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count, 2);
        }
    }
}
