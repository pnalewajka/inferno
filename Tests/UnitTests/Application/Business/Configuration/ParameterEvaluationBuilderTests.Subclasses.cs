﻿using System;
using NUnit.Framework;
using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace UnitTests.Application.Business.Configuration
{
    [TestFixture]
    public partial class ParameterEvaluationBuilderTests
    {
        [Identifier("UnitTests.SampleValue")]
        public class SampleClass
        {
            public string Value { get; set; }
        }

        [Identifier("UnitTests.SampleContext")]
        public class SampleParameterContext : ParameterContext
        {
            public bool IsZenek { get; set; }

            public SampleParameterContext(DateTime currentTime) : base(currentTime)
            {
            }
        }
    }
}
