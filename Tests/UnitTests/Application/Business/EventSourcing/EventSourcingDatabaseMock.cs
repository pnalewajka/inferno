﻿using System;
using System.Collections.Generic;
using System.Text;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Dto;
using Smt.Atomic.Data.Entities.Modules.EventSourcing;

namespace UnitTests.Application.Business.EventSourcing
{
    public class EventSourcingDatabaseMock : EmptyEventSourcingDatabaseMock
    {
        private readonly List<PublishedEventQueueItem> _publishedEvents = new List<PublishedEventQueueItem>
        {
            new PublishedEventQueueItem
            {
                Id = 1,
                BusinessEventId = Guid.Parse("939c3d6c-e0e6-4a97-a57a-1655fff8411a"),
                PublishedOn = DateTime.Now, 
                Timestamp = new byte[]{255,255,251},
                LeasedOn = null,
                BusinessEvent = 
                    DataContractSerializationHelper.SerializeToXml(
                        GetTestBusinessEvent(new DateTime(2016, 1, 1), "939c3d6c-e0e6-4a97-a57a-1655fff8411a"),
                        typeof(BusinessEvent),
                        new [] { typeof(TestBusinessEvent) })
            },
        };

        public List<PublishedEventQueueItem> PublishedEvents => _publishedEvents;

        public EventSourcingDatabaseMock()
        {
            AddEntities(_publishedEvents);
        }

        private static TestBusinessEvent GetTestBusinessEvent(DateTime createdOn, string guid)
        {
            return new TestBusinessEvent
            {
                CreatedOn = createdOn,
                Id = Guid.Parse(guid),
            };
        }
    }
}