using System;
using System.IO;
using System.Linq;
using Castle.MicroKernel.Registration;
using Moq;
using NUnit.Framework;
using Smt.Atomic.Business.EventSourcing.Dto;
using Smt.Atomic.Business.EventSourcing.Interfaces;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.EventSourcing;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Services;
using Smt.Atomic.Tests.Common;
using Smt.Atomic.Tests.Common.AutoMock;
using Smt.Atomic.Tests.Common.Wrappers;

namespace UnitTests.Application.Business.EventSourcing
{
    [TestFixture]
    public class BusinessEventQueueServiceTests
    {
        private AutoMockingContainer _container;

        private void ConfigureContainer(DatabaseMock databaseMock)
        {
            var containerConfig = new TestContainerConfiguration();
            containerConfig.Configure(ContainerType.UnitTests);
            containerConfig.AutoMockingContainer.Register(Component.For(typeof (IUnitOfWorkService<>)).ImplementedBy(typeof (UnitOfWorkService<>)).LifestyleTransient());
            containerConfig.AutoMockingContainer.Register(Component.For(typeof (IRepository<>)).UsingFactoryMethod((k, c) => databaseMock.GetRepositoryMockFactoryMethod(c.RequestedType.GenericTypeArguments[0])));
            containerConfig.AutoMockingContainer.Register(Component.For<IBusinessEventQueueService, IBusinessEventPublisher>().ImplementedBy<MsSqlBusinessEventQueueService>().LifestyleTransient());
            _container = containerConfig.AutoMockingContainer;
        }

        [Test]
        public void GetNextBusinessEvent_ShouldReturnNullForEmptyPublishedQueue()
        {
            ConfigureContainer(new EmptyEventSourcingDatabaseMock());

            var businessEventQueueService = _container.Resolve<IBusinessEventQueueService>();

            var businessEventQueueItem = businessEventQueueService.GetNextBusinessEvent();

            Assert.IsNull(businessEventQueueItem);
        }

        [Test]
        public void GetNextBusinessEvent_ShouldReturnFirstPublishedEvent()
        {
            var database = new EventSourcingDatabaseMock();
            ConfigureContainer(database);

            var businessEventQueueService = _container.Resolve<IBusinessEventQueueService>();

            var businessEventQueueItem = businessEventQueueService.GetNextBusinessEvent();

            Assert.IsNotNull(businessEventQueueItem);
            Assert.IsNotNull(businessEventQueueItem.LeaseToken);
            Assert.IsNotNull(database.PublishedEvents[0].LeasedOn);
        }

        [Test]
        public void AddBusinessEvent_ShouldPublishBusinessEvent()
        {
            var database = new EmptyEventSourcingDatabaseMock();
            ConfigureContainer(database);

            var businessEventQueueService = _container.Resolve<IBusinessEventQueueService>();

            var testBusinessEvent = new TestBusinessEvent();
            businessEventQueueService.PublishBusinessEvent(testBusinessEvent);
            var businessEventQueueItem = businessEventQueueService.GetNextBusinessEvent();

            Assert.IsNotNull(testBusinessEvent.Id);
            Assert.IsNotNull(testBusinessEvent.CreatedOn);
            Assert.IsTrue(businessEventQueueItem.BusinessEvent.Id == testBusinessEvent.Id);
            Assert.AreEqual(0, database.GetRepositoryMock<ProcessedEventQueueItem>().Object.Count());
            Assert.AreEqual(1, database.GetRepositoryMock<PublishedEventQueueItem>().Object.Count());
        }

        [Test]
        public void DeleteBusinessEvent_ShouldCreateProcessedBusinessEvent()
        {
            var database = new EmptyEventSourcingDatabaseMock();
            ConfigureContainer(database);

            var businessEventQueueService = _container.Resolve<IBusinessEventQueueService>();

            var testBusinessEvent = new TestBusinessEvent();
            businessEventQueueService.PublishBusinessEvent(testBusinessEvent);
            var businessEventQueueItem = businessEventQueueService.GetNextBusinessEvent();
            businessEventQueueService.DeleteBusinessEvent(businessEventQueueItem);

            Assert.IsTrue(businessEventQueueItem.BusinessEvent.Id == testBusinessEvent.Id);
            Assert.IsNull(businessEventQueueService.GetNextBusinessEvent());
            Assert.AreEqual(1, database.GetRepositoryMock<ProcessedEventQueueItem>().Object.Count());
            Assert.AreEqual(0, database.GetRepositoryMock<PublishedEventQueueItem>().Object.Count());
        }

        [Test]
        public void CancelBusinessEvent_ShouldRestorePublishedBusinessEvent()
        {
            var database = new EmptyEventSourcingDatabaseMock();
            ConfigureContainer(database);

            var businessEventQueueService = _container.Resolve<IBusinessEventQueueService>();

            var testBusinessEvent = new TestBusinessEvent();
            businessEventQueueService.PublishBusinessEvent(testBusinessEvent);
            var businessEventQueueItem = businessEventQueueService.GetNextBusinessEvent();
            businessEventQueueService.CancelBusinessEventLease(businessEventQueueItem);

            Assert.IsTrue(businessEventQueueItem.BusinessEvent.Id == testBusinessEvent.Id);
            Assert.IsNotNull(businessEventQueueService.GetNextBusinessEvent());
            Assert.AreEqual(0, database.GetRepositoryMock<ProcessedEventQueueItem>().Object.Count());
            Assert.AreEqual(1, database.GetRepositoryMock<PublishedEventQueueItem>().Object.Count());
        }

        [Test]
        public void CancelBusinessEvent_ShouldThrowOnEmptyLeasingTime()
        {
            BusinessEventQueueItem publishedItem;
            var businessEventQueueService = PrepareItemWithNullLeasingTime(out publishedItem);

            Assert.Throws<InvalidDataException>(() => businessEventQueueService.CancelBusinessEventLease(publishedItem));
        }

        [Test]
        public void DeleteBusinessEvent_ShouldThrowOnEmptyLeasingTime()
        {
            BusinessEventQueueItem publishedItem;
            var businessEventQueueService = PrepareItemWithNullLeasingTime(out publishedItem);

            Assert.Throws<InvalidDataException>(() => businessEventQueueService.DeleteBusinessEvent(publishedItem));
        }

        [Test]
        public void CancelBusinessEvent_ShouldThrowOnCancelingAlreadyCanceledEvent()
        {
            BusinessEventQueueItem businessEvent;
            var businessEventQueueService = PrepareItem(out businessEvent);

            businessEventQueueService.CancelBusinessEventLease(businessEvent);
            Assert.Throws<InvalidOperationException>(() => businessEventQueueService.CancelBusinessEventLease(businessEvent));
        }

        [Test]
        public void DeleteBusinessEvent_ShouldThrowOnDeletingAlreadyDeletedEvent()
        {
            BusinessEventQueueItem businessEvent;
            var businessEventQueueService = PrepareItem(out businessEvent);

            businessEventQueueService.DeleteBusinessEvent(businessEvent);
            Assert.Throws<InvalidOperationException>(() => businessEventQueueService.DeleteBusinessEvent(businessEvent));
        }

        [Test]
        public void DeleteBusinessEvent_ShouldThrowOnDeletingAlreadyCancledEvent()
        {
            BusinessEventQueueItem businessEvent;
            var businessEventQueueService = PrepareItem(out businessEvent);

            businessEventQueueService.CancelBusinessEventLease(businessEvent);
            Assert.Throws<InvalidOperationException>(() => businessEventQueueService.DeleteBusinessEvent(businessEvent));
        }

        [TestCase(-1, Result = true)]
        [TestCase(0, Result = true)]
        [TestCase(1, Result = false)]
        public bool GetNextBusinessEvent_IsNullAfterLeasedTimePeriod(int ticksAfterLeasedPeriod)
        {
            const int leaseTimeInSecondsParam = 10;

            var time = new DateTime(2016, 1, 1);
            var database = new EventSourcingDatabaseMock();
            var businessEvent = database.PublishedEvents.First();
            businessEvent.LeasedOn = time;

            ConfigureContainer(database);
            SetupLeaseSeconds(leaseTimeInSecondsParam);
            SetupCurrentTime(time.AddSeconds(leaseTimeInSecondsParam).AddTicks(ticksAfterLeasedPeriod));

            var businessEventQueueService = _container.Resolve<IBusinessEventQueueService>();
            var businessEventQueueItem = businessEventQueueService.GetNextBusinessEvent();

            return businessEventQueueItem == null;
        }

        private IBusinessEventQueueService PrepareItem(out BusinessEventQueueItem businessEvent)
        {
            var database = new EventSourcingDatabaseMock();
            ConfigureContainer(database);

            var businessEventQueueService = _container.Resolve<IBusinessEventQueueService>();
            businessEvent = businessEventQueueService.GetNextBusinessEvent();
            return businessEventQueueService;
        }

        private IBusinessEventQueueService PrepareItemWithNullLeasingTime(out BusinessEventQueueItem publishedItem)
        {
            var database = new EmptyEventSourcingDatabaseMock();
            ConfigureContainer(database);

            var businessEventQueueService = _container.Resolve<IBusinessEventQueueService>();
            businessEventQueueService.PublishBusinessEvent(new TestBusinessEvent());
            publishedItem = businessEventQueueService.GetNextBusinessEvent();
            database.GetRepositoryMock<PublishedEventQueueItem>().Object.First().LeasedOn = null;
            return businessEventQueueService;
        }

        private void SetupLeaseSeconds(int seconds)
        {
            _container
                .GetMock<ISystemParameterService>()
                .Setup(m => m.GetParameter<int>(It.Is<string>(p => p == ParameterKeys.BusinessEventLeaseTimeInSeconds)))
                .Returns(seconds);
        }

        private void SetupCurrentTime(DateTime time)
        {
            _container
                .GetMock<IDbTimeService>()
                .Setup(m => m.GetCurrentTime())
                .Returns(time);
        }
    }
}