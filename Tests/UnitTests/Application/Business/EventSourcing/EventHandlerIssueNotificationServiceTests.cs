﻿using System;
using Microsoft.OData.UriParser;
using Moq;
using NUnit.Framework;
using Smt.Atomic.Business.Configuration.Dto;
using Smt.Atomic.Business.EventSourcing.Jobs;
using Smt.Atomic.Business.Notifications.Dto;
using Smt.Atomic.Business.Notifications.Interfaces;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Business.Configuration.Interfaces;

namespace UnitTests.Application.Business.EventSourcing
{
    [TestFixture]
    public class EventHandlerIssueNotificationServiceTests
    {
        [Test]
        [TestCase(false, TemplateCodes.EventSourcingEventHandlingFailedMoreAttemptsBody)]
        [TestCase(true, TemplateCodes.EventSourcingEventHandlingFailedNoMoreAttemptsBody)]
        public void NotifyAboutEventHandlingError_EmailEnqueued(bool lastNotification, string template)
        {
            // Arrange
            var emailService = new Mock<IReliableEmailService>();
            var templateService = new Mock<IMessageTemplateService>();
            templateService.Setup((m) => m.ResolveTemplate(It.IsAny<string>(), It.IsAny<object>(), null, false)).Returns(new TemplateProcessingResult
            {
                Content = string.Empty,
                IsHtml = true
            });

            var @event = new TestBusinessEvent
            {
                Id = Guid.NewGuid()
            };

            var notifier = new EventHandlerIssueNotificationService(emailService.Object, templateService.Object, new Mock<ISystemParameterService>().Object);

            // Act
            notifier.NotifyAboutEventHandlingError(@event, lastNotification);

            // Assert
            emailService.Verify(e => e.EnqueueMessage(It.IsAny<EmailMessageDto>()));
            templateService.Verify(t => t.ResolveTemplate(template, @event, null, false));
            templateService.Verify(t => t.ResolveTemplate(TemplateCodes.EventSourcingEventHandlingFailedSubject, @event, null, false));
        }
    }
}