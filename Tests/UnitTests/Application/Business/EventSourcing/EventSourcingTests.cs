﻿using System;
using System.Data;
using System.Threading;
using Castle.Core.Logging;
using Moq;
using NUnit.Framework;
using Smt.Atomic.Business.Common.Contexts;
using Smt.Atomic.Business.Common.Models;
using Smt.Atomic.Business.EventSourcing.Dto;
using Smt.Atomic.Business.EventSourcing.Interfaces;
using Smt.Atomic.Business.EventSourcing.Jobs;
using Smt.Atomic.Business.EventSourcing.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Tests.Common;

namespace UnitTests.Application.Business.EventSourcing
{
    [TestFixture]
    public class EventProcessingJobTests
    {
        public abstract class TestBusinessEventHandler : BusinessEventHandler<TestBusinessEvent> { }

        private static JobExecutionContext GetExecutionContext()
        {
            return new JobExecutionContext(new CancellationToken(false), null, null, null, null, 1, false, TimeSpan.Zero);
        }

        private TestContainerConfiguration _container;
        private Mock<IBusinessEventQueueService> _eventQueueMock;
        private TestBusinessEvent _testBusinessEvent;
        private Mock<IEventHandlerIssueNotificationService> _eventHandlerIssueNotificationServiceMock;

        [SetUp]
        public void SetUp()
        {
            _container = new TestContainerConfiguration();
            _container.Configure(ContainerType.UnitTests);

            _eventQueueMock = new Mock<IBusinessEventQueueService>();

            _testBusinessEvent = new TestBusinessEvent();

            _eventHandlerIssueNotificationServiceMock = new Mock<IEventHandlerIssueNotificationService>();
        }

        private EventProcessingJob GetEventProcessingJob()
        {
            return new EventProcessingJob(_eventQueueMock.Object, new Mock<ILogger>().Object, _eventHandlerIssueNotificationServiceMock.Object);
        }

        private JobResult ExecuteTest()
        {
            var eventProcessingJob = GetEventProcessingJob();
            var jobExecutionContext = GetExecutionContext();
            return eventProcessingJob.DoJob(jobExecutionContext);
        }

        private Mock<IBusinessEventHandler> RegisterBusinessEventHandler()
        {
            var businessEventHandler = new Mock<IBusinessEventHandler>();
            businessEventHandler.Setup(m => m.GetBusinessEventType()).Returns(typeof(TestBusinessEvent));
            _container.Register(typeof(IBusinessEventHandler), businessEventHandler.Object);

            return businessEventHandler;
        }

        [Test]
        public void DoJob_ShouldReturnSuccessWhenNoJobs()
        {
            // Arrange
            // Act
            var jobResult = ExecuteTest();

            // Assert
            Assert.IsTrue(jobResult.IsJobFinishedGracefully);
            _eventQueueMock.Verify(m => m.GetNextBusinessEvent());
            _eventQueueMock.Verify(m => m.DeleteBusinessEvent(It.IsAny<BusinessEventQueueItem>()), Times.Never);
        }

        [Test]
        public void DoJob_ShouldGetAndDeleteEventIfNoHandlers()
        {
            // Arrange
            _eventQueueMock.SetupSequence(m => m.GetNextBusinessEvent())
                .Returns(new BusinessEventQueueItem(_testBusinessEvent, null))
                .Returns(null);

            // Act
            var jobResult = ExecuteTest();

            // Assert
            Assert.IsTrue(jobResult.IsJobFinishedGracefully);
            _eventQueueMock.Verify(m => m.GetNextBusinessEvent());
            _eventQueueMock.Verify(m => m.DeleteBusinessEvent(It.IsAny<BusinessEventQueueItem>()), Times.Once);
        }

        [Test]
        public void DoJob_ShouldGetProcessAndDeleteEventIfHandlerExists()
        {
            // Arrange
            var businessEventHandler = RegisterBusinessEventHandler();
            _eventQueueMock.SetupSequence(m => m.GetNextBusinessEvent())
                .Returns(new BusinessEventQueueItem(_testBusinessEvent, null))
                .Returns(null);

            // Act
            var jobResult = ExecuteTest();

            // Assert
            Assert.IsTrue(jobResult.IsJobFinishedGracefully);
            businessEventHandler.Verify(m => m.Handle(_testBusinessEvent));
            _eventQueueMock.Verify(m => m.GetNextBusinessEvent());
            _eventQueueMock.Verify(m => m.DeleteBusinessEvent(It.IsAny<BusinessEventQueueItem>()), Times.Once);
        }

        [Test]
        public void DoJob_EventWithTransientException_ProcessingRetried()
        {
            // Arrange
            _eventQueueMock.SetupSequence(m => m.GetNextBusinessEvent())
                .Returns(new BusinessEventQueueItem(_testBusinessEvent, null))
                .Returns(null);

            var callTimes = 0;
            var businessEventHandler = RegisterBusinessEventHandler();
            businessEventHandler.Setup(h => h.Handle(_testBusinessEvent))
                .Callback(() =>
                {
                    if (++callTimes < 3)
                    {
                        throw new DBConcurrencyException();
                    }
                });

            // Act
            var jobResult = ExecuteTest();

            // Assert
            Assert.IsTrue(jobResult.IsJobFinishedGracefully);
            businessEventHandler.Verify(m => m.Handle(_testBusinessEvent), Times.Exactly(3));
            _eventQueueMock.Verify(m => m.GetNextBusinessEvent());
            _eventQueueMock.Verify(m => m.DeleteBusinessEvent(It.IsAny<BusinessEventQueueItem>()), Times.Once);
        }

        [Test]
        public void DoJob_EventWithPersistentTransientException_AdministratorNotified()
        {
            // Arrange
            _eventQueueMock.SetupSequence(m => m.GetNextBusinessEvent())
                .Returns(new BusinessEventQueueItem(_testBusinessEvent, null))
                .Returns(null);
            
            var businessEventHandler = RegisterBusinessEventHandler();
            businessEventHandler.Setup(h => h.Handle(_testBusinessEvent)).Throws(new DBConcurrencyException());

            // Act
            var jobResult = ExecuteTest();

            // Assert
            Assert.IsTrue(jobResult.IsJobFinishedGracefully);
            businessEventHandler.Verify(m => m.Handle(_testBusinessEvent), Times.Exactly(3));
            _eventQueueMock.Verify(m => m.GetNextBusinessEvent());
            _eventQueueMock.Verify(m => m.DeleteBusinessEvent(It.IsAny<BusinessEventQueueItem>()), Times.Once);
            _eventHandlerIssueNotificationServiceMock.Verify(m => m.NotifyAboutEventHandlingError(_testBusinessEvent, false), Times.Exactly(2));
            _eventHandlerIssueNotificationServiceMock.Verify(m => m.NotifyAboutEventHandlingError(_testBusinessEvent, true), Times.Once);
        }

        [Test]
        public void DoJob_EventWithNonTransientException_AdministratorNotified()
        {
            // Arrange
            _eventQueueMock.SetupSequence(m => m.GetNextBusinessEvent())
                .Returns(new BusinessEventQueueItem(_testBusinessEvent, null))
                .Returns(null);

            var businessEventHandler = RegisterBusinessEventHandler();
            businessEventHandler.Setup(h => h.Handle(_testBusinessEvent)).Throws(new Exception());

            // Act
            var jobResult = ExecuteTest();

            // Assert
            Assert.IsTrue(jobResult.IsJobFinishedGracefully);
            businessEventHandler.Verify(m => m.Handle(_testBusinessEvent), Times.Once);
            _eventQueueMock.Verify(m => m.GetNextBusinessEvent());
            _eventQueueMock.Verify(m => m.DeleteBusinessEvent(It.IsAny<BusinessEventQueueItem>()), Times.Once);
            _eventHandlerIssueNotificationServiceMock.Verify(m => m.NotifyAboutEventHandlingError(_testBusinessEvent, false), Times.Never);
            _eventHandlerIssueNotificationServiceMock.Verify(m => m.NotifyAboutEventHandlingError(_testBusinessEvent, true), Times.Once);
        }
    }
}
