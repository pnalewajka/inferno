﻿using System;
using System.Collections.Generic;
using Castle.MicroKernel.Registration;
using NUnit.Framework;
using Smt.Atomic.Business.Allocation.Dto;
using Smt.Atomic.Business.Allocation.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Data.Repositories.Services;
using Smt.Atomic.Tests.Common;
using Smt.Atomic.Tests.Common.AutoMock;

namespace UnitTests.Application.Business.Allocation.Services
{
    [TestFixture]
    public class EmploymentPeriodServiceTests
    {
        private TestContainerConfiguration _testContainerConfiguration;
        private AutoMockingContainer _container;
        private EmploymentPeriodDatabaseMock _databaseMock;
        private IList<EmploymentPeriod> _employmentPeriods;
        private EmploymentPeriod _finiteEmploymentPeriodFebruary;
        private long _employeeIdWithInfiniteEmployment;

        private EmploymentPeriodService _service;

        [SetUp]
        public void Setup()
        {
            CreateEmploymentPeriods();

            _testContainerConfiguration = new TestContainerConfiguration();
            _testContainerConfiguration.Configure(ContainerType.UnitTests);

            _container = _testContainerConfiguration.AutoMockingContainer;
            _databaseMock = new EmploymentPeriodDatabaseMock(_employmentPeriods);

            _container.RegisterDatabase(_databaseMock);
            _container.Register(
                Component.For(typeof (IUnitOfWorkService<IAllocationDbScope>))
                    .ImplementedBy(typeof (UnitOfWorkService<IAllocationDbScope>))
                    .LifestyleTransient());

            _service = new EmploymentPeriodService(null,
                _container.Resolve<IUnitOfWorkService<IAllocationDbScope>>(),
                null, null, null, null, null, null, null, null, null,
                new DateRangeService(), new EmploymentPeriodToEmploymentPeriodDtoMapping());
        }

        private void CreateEmploymentPeriods()
        {
            _finiteEmploymentPeriodFebruary = new EmploymentPeriod
            {
                EmployeeId = 111,
                StartDate = new DateTime(2017, 2, 1),
                EndDate = new DateTime(2017, 2, 28)
            };

            _employeeIdWithInfiniteEmployment = 222;

            _employmentPeriods = new List<EmploymentPeriod>
            {
                _finiteEmploymentPeriodFebruary,
                new EmploymentPeriod
                {
                    EmployeeId = _employeeIdWithInfiniteEmployment,
                    StartDate = new DateTime(2017, 2, 1),
                    EndDate = new DateTime(2017, 2, 28)
                },
                new EmploymentPeriod
                {
                    EmployeeId = _employeeIdWithInfiniteEmployment,
                    StartDate = new DateTime(2017, 4, 1),
                    EndDate = null
                }
            };
        }

        [Test]
        public void IsEmployeeHiredInCompletePeriod_StartDateAfterEndDate()
        {
            var isHired = _service.IsEmployeeHiredInCompletePeriod(123,
                new DateTime(2017, 5, 10), new DateTime(2017, 1, 20));

            Assert.IsFalse(isHired);
        }

        [Test]
        public void IsEmployeeHiredInCompletePeriod_EmployeeWithNoEmployment()
        {
            var isHired = _service.IsEmployeeHiredInCompletePeriod(123,
                new DateTime(2017, 1, 10), new DateTime(2017, 1, 20));

            Assert.IsFalse(isHired);
        }

        [Test]
        [TestCase(2017, 1, 1, 2017, 1, 10, false)]
        [TestCase(2017, 1, 1, 2017, 3, 10, false)]
        [TestCase(2017, 3, 1, 2017, 4, 10, false)]
        [TestCase(2017, 1, 1, 2017, 2, 10, false)]
        [TestCase(2017, 2, 10, 2017, 3, 10, false)]
        [TestCase(2017, 2, 10, 2017, 2, 20, true)]
        public void IsEmployeeHiredInCompletePeriod_EmployeeWithFiniteEmployment(
            int startYear, int startMonth, int startDay,
            int endYear, int endMonth, int endDay,
            bool expectedResult)
        {
            var isHired = _service.IsEmployeeHiredInCompletePeriod(
                _finiteEmploymentPeriodFebruary.EmployeeId,
                new DateTime(startYear, startMonth, startDay),
                new DateTime(endYear, endMonth, endDay));

            Assert.AreEqual(expectedResult, isHired);
        }

        [Test]
        [TestCase(2017, 1, 1, 2017, 1, 10, false)]
        [TestCase(2017, 2, 1, 2017, 2, 10, true)]
        [TestCase(2017, 3, 1, 2017, 3, 10, false)]
        [TestCase(2017, 4, 1, 2017, 4, 10, true)]
        [TestCase(2017, 1, 1, 2017, 4, 10, false)]
        [TestCase(2050, 1, 1, 2050, 1, 10, true)]
        public void IsEmployeeHiredInCompletePeriod_EmployeeWithInfiniteEmployment(
            int startYear, int startMonth, int startDay,
            int endYear, int endMonth, int endDay,
            bool expectedResult)
        {
            var isHired = _service.IsEmployeeHiredInCompletePeriod(
                _employeeIdWithInfiniteEmployment,
                new DateTime(startYear, startMonth, startDay),
                new DateTime(endYear, endMonth, endDay));

            Assert.AreEqual(expectedResult, isHired);
        }
    }
}
