﻿using System;
using System.Linq;
using NUnit.Framework;
using Smt.Atomic.Business.Allocation.Services;

namespace UnitTests.Application.Business.Allocation.Services
{
    [TestFixture]
    public class DateRangeServiceTests
    {
        private DateRangeService _service;

        [SetUp]
        public void Setup()
        {
            _service = new DateRangeService();
        }

        [Test]
        public void GetDaysInRange_WeekWithWeekends()
        {
            var days = _service.GetDaysInRange(new DateTime(2016, 10, 24), new DateTime(2016, 10, 30), true).ToList();

            Assert.AreEqual(7, days.Count);
            Assert.AreEqual(new DateTime(2016, 10, 24), days[0]);
            Assert.AreEqual(new DateTime(2016, 10, 25), days[1]);
            Assert.AreEqual(new DateTime(2016, 10, 26), days[2]);
            Assert.AreEqual(new DateTime(2016, 10, 27), days[3]);
            Assert.AreEqual(new DateTime(2016, 10, 28), days[4]);
            Assert.AreEqual(new DateTime(2016, 10, 29), days[5]);
            Assert.AreEqual(new DateTime(2016, 10, 30), days[6]);
        }

        [Test]
        public void GetDaysInRange_WeekWithoutWeekends()
        {
            var days = _service.GetDaysInRange(new DateTime(2016, 10, 24), new DateTime(2016, 10, 30), false).ToList();

            Assert.AreEqual(5, days.Count);
            Assert.AreEqual(new DateTime(2016, 10, 24), days[0]);
            Assert.AreEqual(new DateTime(2016, 10, 25), days[1]);
            Assert.AreEqual(new DateTime(2016, 10, 26), days[2]);
            Assert.AreEqual(new DateTime(2016, 10, 27), days[3]);
            Assert.AreEqual(new DateTime(2016, 10, 28), days[4]);
        }
    }
}
