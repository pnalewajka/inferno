﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.MicroKernel.Registration;
using Moq;
using NUnit.Framework;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Organization.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Tests.Common;
using Smt.Atomic.Tests.Common.AutoMock;
using Smt.Atomic.WebApp.Areas.Allocation.UrlToken;
using Smt.Atomic.Business.Allocation.Consts;

namespace UnitTests.Application.Business.Allocation.Services
{
    [TestFixture]
    public class FilterTokenResolverTests
    {
        [Test]
        public void GetTokens_PositiveTest()
        {
            var container = SetupMockForBasicTest();

            var allocationRequestResolvedToken = AssertResult<AllocationRequestFilterTokenResolver>(container);
            var employeeAllocationResolvedToken = AssertResult<EmployeeAllocationFilterTokenResolver>(container);
            var projectResolvedToken = AssertResult<ProjectFilterTokenResolver>(container);
            var employeeResolvedToken = AssertResult<EmployeeFilterTokenResolver>(container);
            var employeeBenchResolvedToken = AssertResult<BenchEmployeesFilterTokenResolver>(container);

            Assert.AreEqual(1, allocationRequestResolvedToken.Count);
            Assert.AreEqual("%ALLOCATION-REQUEST-SCOPE-FILTER%", allocationRequestResolvedToken.First());
            Assert.AreEqual("%EMPLOYEE-ALLOCATION-SCOPE-FILTER%", employeeAllocationResolvedToken.First());
            Assert.AreEqual("%PROJECT-SCOPE-FILTER%", projectResolvedToken.First());
            Assert.AreEqual("%EMPLOYEE-SCOPE-FILTER%", employeeResolvedToken.First());
            Assert.AreEqual("%EMPLOYEE-BENCH-SCOPE-FILTER%", employeeBenchResolvedToken.First());
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetTokenValue_ForNotSpecifiedToken()
        {
            var resolver = new AllocationRequestFilterTokenResolver(null, null, null, null);

            resolver.GetTokenValue(null);
        }

        [Test]
        [TestCase("%ALLOCATION-REQUEST-SCOPE-FILTER%", FilterCodes.AllEmployees, SecurityRoleType.CanScopeAllocationRequestsToAllEmployeesByDefault)]
        public void GetTokenValue_UserBelongsToSpecificRole_AllocationRequest(string token, string expectedValue, SecurityRoleType roleThatUserBelongsTo )
        {
            var container = SetupMockForBasicRoleTest(roleThatUserBelongsTo);

            var resolvedToken = AssertResult<AllocationRequestFilterTokenResolver>(token, container);

            Assert.AreEqual(expectedValue, resolvedToken);
        }

        [Test]
        [TestCase("%EMPLOYEE-ALLOCATION-SCOPE-FILTER%", FilterCodes.AllAllocations, SecurityRoleType.CanScopeEmployeeAllocationsToAllAllocationsByDefault)]
        public void GetTokenValue_UserBelongsToSpecificRole_EmployeeAllocation(string token, string expectedValue, SecurityRoleType roleThatUserBelongsTo)
        {
            var container = SetupMockForBasicRoleTest(roleThatUserBelongsTo);

            var resolvedToken = AssertResult<EmployeeAllocationFilterTokenResolver>(token, container);

            Assert.AreEqual(expectedValue, resolvedToken);
        }

        [Test]
        [TestCase("%EMPLOYEE-SCOPE-FILTER%", FilterCodes.AllEmployees, SecurityRoleType.CanScopeEmployeesToAllEmployeesByDefault)]
        public void GetTokenValue_UserBelongsToSpecificRole_Employee(string token, string expectedValue, SecurityRoleType roleThatUserBelongsTo)
        {
            var container = SetupMockForBasicRoleTest(roleThatUserBelongsTo);

            var resolvedToken = AssertResult<EmployeeFilterTokenResolver>(token, container);

            Assert.AreEqual(expectedValue, resolvedToken);
        }

        [Test]
        [TestCase("%PROJECT-SCOPE-FILTER%", FilterCodes.AllProjects + "," + FilterCodes.ActiveProjects, SecurityRoleType.CanScopeProjectsToAllProjectsByDefault)]
        public void GetTokenValue_UserBelongsToSpecificRole_Project(string token, string expectedValue, SecurityRoleType roleThatUserBelongsTo) 
        {
            var container = SetupMockForBasicRoleTest(roleThatUserBelongsTo);

            var resolvedToken = AssertResult<ProjectFilterTokenResolver>(token, container);

            Assert.AreEqual(expectedValue, resolvedToken);
        }

        [Test]
        [TestCase("%ALLOCATION-REQUEST-SCOPE-FILTER%", FilterCodes.MyEmployees)]
        public void GetTokenValue_UserContainsEmployee_AllocationRequest(string token, string expectedValue)
        {
            var container = SetupMockForEmployeeTest(true);

            var resolvedToken = AssertResult<AllocationRequestFilterTokenResolver>(token, container);

            Assert.AreEqual(expectedValue, resolvedToken);
        }

        [Test]
        [TestCase("%EMPLOYEE-ALLOCATION-SCOPE-FILTER%", FilterCodes.MyEmployees)]
        public void GetTokenValue_UserContainsEmployee_EmployeeAllocation(string token, string expectedValue)
        {
            var container = SetupMockForEmployeeTest(true);

            var resolvedToken = AssertResult<EmployeeAllocationFilterTokenResolver>(token, container);

            Assert.AreEqual(expectedValue, resolvedToken);
        }

        [Test]
        [TestCase("%PROJECT-SCOPE-FILTER%", FilterCodes.MyProjects + "," + FilterCodes.ActiveProjects)]
        public void GetTokenValue_UserContainsEmployee_Project(string token, string expectedValue)
        {
            var container = SetupMockForEmployeeTest(true);

            var resolvedToken = AssertResult<ProjectFilterTokenResolver>(token, container);

            Assert.AreEqual(expectedValue, resolvedToken);
        }

        [Test]
        [TestCase("%EMPLOYEE-SCOPE-FILTER%", FilterCodes.MyEmployees)]
        public void GetTokenValue_UserContainsEmployee_Employee(string token, string expectedValue)
        {
            var container = SetupMockForEmployeeTest(true);

            var resolvedToken = AssertResult<EmployeeFilterTokenResolver>(token, container);

            Assert.AreEqual(expectedValue, resolvedToken);
        }

        [Test]
        [TestCase("%ALLOCATION-REQUEST-SCOPE-FILTER%", FilterCodes.AllEmployees)]
        public void GetTokenValue_UserWithNoEmployee_AllocationRequest(string token, string expectedValue)
        {
            var container = SetupMockForEmployeeTest(false);

            var resolvedToken = AssertResult<AllocationRequestFilterTokenResolver>(token, container);

            Assert.AreEqual(expectedValue, resolvedToken);
        }

        [Test]
        [TestCase("%EMPLOYEE-ALLOCATION-SCOPE-FILTER%", FilterCodes.AllAllocations)]
        public void GetTokenValue_UserWithNoEmployee_EmployeeAllocation(string token, string expectedValue)
        {
            var container = SetupMockForEmployeeTest(false);

            var resolvedToken = AssertResult<EmployeeAllocationFilterTokenResolver>(token, container);

            Assert.AreEqual(expectedValue, resolvedToken);
        }

        [Test]
        [TestCase("%PROJECT-SCOPE-FILTER%", FilterCodes.MyProjects + "," + FilterCodes.ActiveProjects)]
        public void GetTokenValue_UserWithNoEmployee_Project(string token, string expectedValue)
        {
            var container = SetupMockForEmployeeTest(false);

            var resolvedToken = AssertResult<ProjectFilterTokenResolver>(token, container);

            Assert.AreEqual(expectedValue, resolvedToken);
        }

        [Test]
        [TestCase("%EMPLOYEE-SCOPE-FILTER%", FilterCodes.AllEmployees)]
        public void GetTokenValue_UserWithNoEmployee_Employee(string token, string expectedValue)
        {
            var container = SetupMockForEmployeeTest(false);

            var resolvedToken = AssertResult<EmployeeFilterTokenResolver>(token, container);

            Assert.AreEqual(expectedValue, resolvedToken);
        }

        [Test]
        [TestCase("%ALLOCATION-REQUEST-SCOPE-FILTER%", "my-employees", SecurityRoleType.CanScopeAllocationRequestsToOwnOrgUnitsByDefault)]
        public void GetTokenValue_UserBelongsToSpecificOrgUnitManagerRole_AllocationRequest(string token, string expectedValue, SecurityRoleType roleThatUserBelongsTo)
        {
            var container = SetupMockForAdvanceRoleTest(roleThatUserBelongsTo, new List<long> { 1, 2, 3 });

            var resolvedToken = AssertResult<AllocationRequestFilterTokenResolver>(token, container);

            Assert.AreEqual(expectedValue, resolvedToken);
        }

        [Test]
        [TestCase("%EMPLOYEE-ALLOCATION-SCOPE-FILTER%", "my-employees", SecurityRoleType.CanScopeEmployeeAllocationsToOwnOrgUnitsByDefault)]
        public void GetTokenValue_UserBelongsToSpecificOrgUnitManagerRole_EmployeeAllocation(string token, string expectedValue, SecurityRoleType roleThatUserBelongsTo)
        {
            var container = SetupMockForAdvanceRoleTest(roleThatUserBelongsTo, new List<long> { 1, 2, 3 });

            var resolvedToken = AssertResult<EmployeeAllocationFilterTokenResolver>(token, container);

            Assert.AreEqual(expectedValue, resolvedToken);
        }

        [Test]
        [TestCase("%EMPLOYEE-BENCH-SCOPE-FILTER%", "", SecurityRoleType.CanScopeBenchEmployeesToOwnOrgUnitsByDefault)]
        public void GetTokenValue_UserBelongsToSpecificOrgUnitManagerRole_EmployeeBench(string token, string expectedValue, SecurityRoleType roleThatUserBelongsTo)
        {
            var container = SetupMockForAdvanceRoleTest(roleThatUserBelongsTo, new List<long> { 1, 2, 3 });

            var resolvedToken = AssertResult<BenchEmployeesFilterTokenResolver>(token, container);

            Assert.AreEqual(expectedValue, resolvedToken);
        }

        [Test]
        [TestCase("%EMPLOYEE-SCOPE-FILTER%", "my-employees", SecurityRoleType.CanScopeEmployeesToOwnOrgUnitsByDefault)]
        public void GetTokenValue_UserBelongsToSpecificOrgUnitManagerRole_Employee(string token, string expectedValue, SecurityRoleType roleThatUserBelongsTo)
        {
            var container = SetupMockForAdvanceRoleTest(roleThatUserBelongsTo, new List<long> { 1, 2, 3 });

            var resolvedToken = AssertResult<EmployeeFilterTokenResolver>(token, container);

            Assert.AreEqual(expectedValue, resolvedToken);
        }

        [Test]
        [TestCase("%ALLOCATION-REQUEST-SCOPE-FILTER%", "my-employees", SecurityRoleType.CanScopeAllocationRequestsToOwnOrgUnitsByDefault)]
        public void GetTokenValue_UserBelongsToSpecificOrgUnitManagerRoleButHasNoOrgUnit_AllocationRequest(string token, string expectedValue, SecurityRoleType roleThatUserBelongsTo)
        {
            var container = SetupMockForAdvanceRoleTest(roleThatUserBelongsTo, new List<long>());

            var resolvedToken = AssertResult<AllocationRequestFilterTokenResolver>(token, container);

            Assert.AreEqual(expectedValue, resolvedToken);
        }

        [Test]
        [TestCase("%EMPLOYEE-ALLOCATION-SCOPE-FILTER%", "my-employees", SecurityRoleType.CanScopeEmployeeAllocationsToOwnOrgUnitsByDefault)]
        public void GetTokenValue_UserBelongsToSpecificOrgUnitManagerRoleButHasNoOrgUnit_EmployeeAllocation(string token, string expectedValue, SecurityRoleType roleThatUserBelongsTo)
        {
            var container = SetupMockForAdvanceRoleTest(roleThatUserBelongsTo, new List<long>());

            var resolvedToken = AssertResult<EmployeeAllocationFilterTokenResolver>(token, container);

            Assert.AreEqual(expectedValue, resolvedToken);
        }

        [Test]
        [TestCase("%EMPLOYEE-SCOPE-FILTER%", "my-employees", SecurityRoleType.CanScopeEmployeesToOwnOrgUnitsByDefault)]
        public void GetTokenValue_UserBelongsToSpecificOrgUnitManagerRoleButHasNoOrgUnit_Employee(string token, string expectedValue, SecurityRoleType roleThatUserBelongsTo)
        {
            var container = SetupMockForAdvanceRoleTest(roleThatUserBelongsTo, new List<long>());

            var resolvedToken = AssertResult<EmployeeFilterTokenResolver>(token, container);

            Assert.AreEqual(expectedValue, resolvedToken);
        }

        private string AssertResult<T>(string token, AutoMockingContainer container) where T : FilterTokenResolver
        {
            container.Register(Component.For<T>());
            var tokenResolver = container.Resolve<T>();

            return tokenResolver.GetTokenValue(token);
        }

        private List<string> AssertResult<T>(AutoMockingContainer container) where T : FilterTokenResolver
        {
            container.Register(Component.For<T>());
            var tokenResolver = container.Resolve<T>();

            return tokenResolver.GetTokens().ToList();
        }

        private AutoMockingContainer SetupMockForAdvanceRoleTest(SecurityRoleType roleThatUserBelongsTo, List<long> orgUnitsIds)
        {
            var containerConfig = new TestContainerConfiguration();
            containerConfig.Configure(ContainerType.UnitTests);

            AutoMockingContainer container = containerConfig.AutoMockingContainer;

            var principal = new Mock<IAtomicPrincipal>();
            principal.Setup(p => p.IsInRole(roleThatUserBelongsTo)).Returns(true);
            principal.SetupGet(p => p.EmployeeId).Returns(123);

            var principalProvider = new Mock<IPrincipalProvider>();
            principalProvider.SetupGet(p => p.Current).Returns(principal.Object);
            principalProvider.SetupGet(p => p.IsSet).Returns(true);

            var orgUnitService = new Mock<IOrgUnitService>();
            orgUnitService.Setup(o => o.GetManagerOrgUnitDescendantIds(principal.Object.EmployeeId)).Returns(orgUnitsIds);

            var employeeService = new Mock<IEmployeeService>();
            employeeService.Setup(e => e.HasEmployees(principal.Object.EmployeeId)).Returns(true);

            var projectService = new Mock<IProjectService>();
            projectService.Setup(p => p.HasProjects(principal.Object.EmployeeId)).Returns(true);

            container.Register(Component.For<IPrincipalProvider>().Instance(principalProvider.Object));
            container.Register(Component.For<IOrgUnitService>().Instance(orgUnitService.Object));
            container.Register(Component.For<IEmployeeService>().Instance(employeeService.Object));
            container.Register(Component.For<IProjectService>().Instance(projectService.Object));

            return container;
        }

        private AutoMockingContainer SetupMockForEmployeeTest(bool hasEmployees)
        {
            var containerConfig = new TestContainerConfiguration();
            containerConfig.Configure(ContainerType.UnitTests);

            AutoMockingContainer container = containerConfig.AutoMockingContainer;

            var principal = new Mock<IAtomicPrincipal>();
            principal.Setup(p => p.EmployeeId).Returns(1);

            var principalProvider = new Mock<IPrincipalProvider>();
            principalProvider.SetupGet(p => p.Current).Returns(principal.Object);
            principalProvider.SetupGet(p => p.IsSet).Returns(true);

            var employeeService = new Mock<IEmployeeService>();
            employeeService.Setup(e => e.HasEmployees(principal.Object.EmployeeId)).Returns(hasEmployees);

            var projectService = new Mock<IProjectService>();
            projectService.Setup(p => p.HasProjects(principal.Object.EmployeeId)).Returns(true);

            container.Register(Component.For<IPrincipalProvider>().Instance(principalProvider.Object));
            container.Register(Component.For<IEmployeeService>().Instance(employeeService.Object));
            container.Register(Component.For<IProjectService>().Instance(projectService.Object));

            return container;
        }

        private AutoMockingContainer SetupMockForBasicRoleTest(SecurityRoleType roleThatUserBelongsTo)
        {
            var containerConfig = new TestContainerConfiguration();
            containerConfig.Configure(ContainerType.UnitTests);

            AutoMockingContainer container = containerConfig.AutoMockingContainer;

            var principal = new Mock<IAtomicPrincipal>();
            principal.Setup(p => p.IsInRole(roleThatUserBelongsTo)).Returns(true);

            var principalProvider = new Mock<IPrincipalProvider>();
            principalProvider.SetupGet(p => p.Current).Returns(principal.Object);
            principalProvider.SetupGet(p => p.IsSet).Returns(true);

            container.Register(Component.For<IPrincipalProvider>().Instance(principalProvider.Object));

            return container;
        }

        private AutoMockingContainer SetupMockForBasicTest()
        {
            var containerConfig = new TestContainerConfiguration();
            containerConfig.Configure(ContainerType.UnitTests);

            AutoMockingContainer container = containerConfig.AutoMockingContainer;

            return container;
        }
    }
}
