﻿using System.Collections.Generic;
using Smt.Atomic.Data.Entities.Modules.Allocation;
using Smt.Atomic.Tests.Common.Wrappers;

namespace UnitTests.Application.Business.Allocation.Services
{
    public class EmploymentPeriodDatabaseMock : DatabaseMock
    {
        public IList<EmploymentPeriod> Periods { get; set; }

        public EmploymentPeriodDatabaseMock(IList<EmploymentPeriod> periods = null)
        {
            Periods = periods ?? new List<EmploymentPeriod>();
            AddEntities(Periods);
        }
    }
}
