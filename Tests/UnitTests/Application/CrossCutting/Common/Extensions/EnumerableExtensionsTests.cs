﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace UnitTests.Application.CrossCutting.Common.Extensions
{
    [TestFixture]
    public class EnumerableExtensionsTests
    {
        [Test, ExpectedException(typeof(ArgumentNullException))]
        public void WithIndex_ShouldThrowExceptionForNullCollection()
        {
            EnumerableExtensions.WithIndex<int>(null);
        }

        [Test]
        public void WithIndex_ShouldReturnEmptyCollectionForEmptyCollection()
        {
            var result = new int[0].WithIndex();

            Assert.AreEqual(0, result.Count());
        }

        [Test]
        public void WithIndex_ShouldReturnProperlyIndexedCollection()
        {
            var original = new[] {1, 2, 4};

            var result = original
                .WithIndex()
                .ToList();

            Assert.AreEqual(3, result.Count);
            
            for (var i = 0; i < result.Count; i++)
            {
                Assert.AreEqual(i, result[i].Index);
                Assert.AreEqual(original[i], result[i].Item);
            }
        }

        [Test]
        public void DistinctBy_WithoutKeyComparer()
        {
            var original = new[]
            {
                Tuple.Create(1, "a"),
                Tuple.Create(1, "a"),
                Tuple.Create(2, "a"),
                Tuple.Create(2, "b"),
                Tuple.Create(3, "x"),
            };

            var expected = new[]
            {
                Tuple.Create(1, "a"),
                Tuple.Create(2, "a"),
                Tuple.Create(3, "x"),
            };

            var result = original.DistinctBy(t => t.Item1);
            CollectionAssert.AreEqual(expected, result);
        }
        
        [Test]
        public void DistinctBy_WithKeyComparer()
        {
            var original = new[]
            {
                Tuple.Create(1, "a"),
                Tuple.Create(1, "a"),
                Tuple.Create(2, "aa"),
                Tuple.Create(2, "bb"),
                Tuple.Create(3, "xxxx"),
            };

            var expected = new[]
            {
                Tuple.Create(1, "a"),
                Tuple.Create(2, "aa"),
                Tuple.Create(3, "xxxx"),
            };

            var mock = new Mock<IEqualityComparer<string>>();
            
            mock.Setup(e => e.GetHashCode(It.IsAny<string>())).Returns((string x) => x.Length);
            mock.Setup(e => e.Equals(It.IsAny<string>(), It.IsAny<string>()))
                .Returns((string x, string y) => x.Length == y.Length);

            var result = original.DistinctBy(t => t.Item2, mock.Object);
            CollectionAssert.AreEqual(expected, result);
        }
    }
}