﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace UnitTests.Application.CrossCutting.Common.Extensions
{
    [TestFixture]
    public class DictionaryExtensionsTests
    {
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddRange_ShouldThrowArgumentNullExceptionForNullTarget()
        {
            IDictionary<string, string> target = null;
            var values = new Dictionary<string, string>();

            // ReSharper disable ExpressionIsAlwaysNull
            target.AddRange(values);
            // ReSharper restore ExpressionIsAlwaysNull
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddRange_ShouldThrowArgumentNullExceptionForNullValues()
        {
            var target = new Dictionary<string, string>();
            IDictionary<string, string> values = null;

            // ReSharper disable ExpressionIsAlwaysNull
            target.AddRange(values);
            // ReSharper restore ExpressionIsAlwaysNull
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void AddRange_ShouldThrowArgumentExceptionForUnwantedDuplicate()
        {
            var target = new Dictionary<string, string>
                {
                    { "Mick", "Jagger" },
                    { "Charlie", "Watts" }
                };

            var values = new Dictionary<string, string>
                {
                    { "Charlie", "Wonka" },
                    { "Ronnie", "Woods" }
                };

            target.AddRange(values, DictionaryExtensions.DuplicateKeyBehavior.ThrowException);
        }

        [Test]
        public void AddRange_ShouldSkipDuplicateWhenInstructedSo()
        {
            var target = new Dictionary<string, string>
                {
                    { "Mick", "Jagger" },
                    { "Charlie", "Watts" }
                };

            var values = new Dictionary<string, string>
                {
                    { "Charlie", "Wonka" },
                    { "Ronnie", "Woods" }
                };

            target.AddRange(values, DictionaryExtensions.DuplicateKeyBehavior.SkipExisting);

            Assert.AreEqual(target.Count, 3);

            Assert.IsTrue(target.ContainsKey("Charlie"));
            Assert.AreEqual("Watts", target["Charlie"]);
        }

        [Test]
        public void AddRange_ShouldOverrideDuplicateWhenInstructedSo()
        {
            var target = new Dictionary<string, string>
                {
                    { "Mick", "Jagger" },
                    { "Charlie", "Watts" }
                };

            var values = new Dictionary<string, string>
                {
                    { "Charlie", "Wonka" }
                };

            target.AddRange(values);

            Assert.AreEqual(target.Count, 2);

            Assert.IsTrue(target.ContainsKey("Charlie"));
            Assert.AreEqual("Wonka", target["Charlie"]);
        }

        [Test]
        public void AddRange_ShouldAddNewValuesRegardlessOfDuplicateKeyBehavior()
        {
            AddNewValuesToDictionary(DictionaryExtensions.DuplicateKeyBehavior.OverrideValues);
            AddNewValuesToDictionary(DictionaryExtensions.DuplicateKeyBehavior.SkipExisting);
            AddNewValuesToDictionary(DictionaryExtensions.DuplicateKeyBehavior.ThrowException);
        }

        private static void AddNewValuesToDictionary(DictionaryExtensions.DuplicateKeyBehavior duplicateKeyBehavior)
        {
            var target = new Dictionary<string, string>
                {
                    {"Mick", "Jagger"},
                    {"Charlie", "Watts"}
                };

            var values = new Dictionary<string, string>
                {
                    {"Keith", "Richards"},
                    {"Ronnie", "Woods"}
                };

            target.AddRange(values, duplicateKeyBehavior);

            Assert.AreEqual(target.Count, 4);

            Assert.IsTrue(target.ContainsKey("Mick"));
            Assert.IsTrue(target.ContainsKey("Charlie"));
            Assert.IsTrue(target.ContainsKey("Keith"));
            Assert.IsTrue(target.ContainsKey("Ronnie"));
        }


        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Remove_ShouldThrowArgumentNullExceptionForNullTarget()
        {
            IDictionary<string, string> target = null;
            var keys = Enumerable.Empty<string>();

            // ReSharper disable ExpressionIsAlwaysNull
            target.Remove(keys);
            // ReSharper restore ExpressionIsAlwaysNull
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Remove_ShouldThrowArgumentNullExceptionForNullKeys()
        {
            var target = new Dictionary<string, string>();
            IEnumerable<string> keys = null;

            // ReSharper disable ExpressionIsAlwaysNull
            target.Remove(keys);
            // ReSharper restore ExpressionIsAlwaysNull
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void Remove_ShouldThrowArgumentExceptionForMissingKeyWhenSaidSo()
        {
            var target = new Dictionary<string, string>
                {
                    { "Mick", "Jagger" },
                    { "Charlie", "Watts" },
                    { "Keith", "Richards" },
                    { "Ronnie", "Woods" }
                };

            var keys = new [] { "Brian" };

            target.Remove(keys, DictionaryExtensions.MissingKeyBehavior.ThrowException);
        }

        [Test]
        public void Remove_ShouldSkipMissingKeyWhenSaidSo()
        {
            var target = new Dictionary<string, string>
                {
                    { "Mick", "Jagger" },
                    { "Charlie", "Watts" },
                    { "Keith", "Richards" },
                    { "Ronnie", "Woods" }
                };

            var keys = new[] { "Brian" };

            target.Remove(keys);

            Assert.AreEqual(target.Count, 4);
        }

        [Test]
        public void Remove_ShouldRemoveExistingKeysRegardlessOfMissingKeyBehavior()
        {
            RemoveExistingKeysFromDictionary(DictionaryExtensions.MissingKeyBehavior.SkipMissing);
            RemoveExistingKeysFromDictionary(DictionaryExtensions.MissingKeyBehavior.ThrowException);
        }

        [Test]
        public void GetByKeyOrDefault_Method_Should_Return_Value_From_Dictionary()
        {
            string key = "key";
            string value = "value";

            var dictionary = new Dictionary<string, string>
            {
                [key] = value
            };

            Assert.AreEqual(value, dictionary.GetByKeyOrDefault(key));
        }

        [Test]
        public void GetByKeyOrDefault_Method_Should_Return_Default_Value()
        {
            string key = "key";

            var dictionary = new Dictionary<string, string>();

            Assert.AreEqual(default(string), dictionary.GetByKeyOrDefault(key));
        }

        private static void RemoveExistingKeysFromDictionary(DictionaryExtensions.MissingKeyBehavior missingKeyBehavior)
        {
            var target = new Dictionary<string, string>
                {
                    {"Mick", "Jagger"},
                    {"Charlie", "Watts"},
                    {"Keith", "Richards"},
                    {"Ronnie", "Woods"},
                    {"Brian", "Jones"},
                    {"Bill", "Wymann"}
                };

            var keys = new[] {"Brian", "Bill"};

            target.Remove(keys, missingKeyBehavior);

            Assert.AreEqual(target.Count, 4);
        }
    }
}