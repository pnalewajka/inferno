﻿using System;
using System.Linq;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace UnitTests.Application.CrossCutting.Common.Extensions
{
    [TestFixture]
    public class StringExtensionsTests
    {
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Matches_ShouldThrowArgumentNullExceptionForNullString()
        {
            string s = null;
            var regex = string.Empty;

            // ReSharper disable ExpressionIsAlwaysNull
            s.Matches(regex);
            // ReSharper restore ExpressionIsAlwaysNull
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Matches_ShouldThrowArgumentNullExceptionForNullRegex()
        {
            var s = string.Empty;
            string regex = null;

            // ReSharper disable ExpressionIsAlwaysNull
            s.Matches(regex);
            // ReSharper restore ExpressionIsAlwaysNull
        }

        [Test]
        public void Matches_ShouldReturnTrueForEmptyRegex()
        {
            const string s = "Mick Jagger";
            var regex = string.Empty;

            Assert.IsTrue(s.Matches(regex));
        }

        [Test]
        public void Matches_ShouldReturnTrueForMatchingRegex()
        {
            const string s = "Mick Jagger";
            const string regex = ".*ick.*";

            Assert.IsTrue(s.Matches(regex));
        }

        [Test]
        public void Matches_ShouldReturnFalseForNonMatchingRegex()
        {
            const string s = "Mick Jagger";
            const string regex = ".*ickey.*";

            Assert.IsFalse(s.Matches(regex));
        }

        [Test]
        [TestCase("5,7")]
        [TestCase("5, 7")]
        [TestCase("   5,   7   ")]
        [TestCase("\"5,7\"")]
        [TestCase("\"5, 7\"")]
        public void ParseCommaSeparatedList_TestPositive(string cellValue)
        {
            var list = cellValue.ParseCommaSeparatedList<long>().ToList();

            Assert.AreEqual(2, list.Count);
            Assert.AreEqual(5, list[0]);
            Assert.AreEqual(7, list[1]);
        }

        [Test]
        public void ParseCommaSeparatedList_TestPositiveEmptyCell()
        {
            var list = string.Empty.ParseCommaSeparatedList<long>().ToList();

            Assert.AreEqual(0, list.Count);
        }

        [Test]
        public void ParseCommaSeparatedList_TestPositiveOneInt()
        {
            var list = "1".ParseCommaSeparatedList<int>().ToList();

            Assert.AreEqual(1, list.Count);
            Assert.AreEqual(1, list[0]);
        }

        [Test]
        [ExpectedException(typeof(FormatException))]
        public void ParseCommaSeparatedList_TestNegativeInvalidData()
        {
            // ReSharper disable once UnusedVariable
            var result = "not a number".ParseCommaSeparatedList<int>().ToList();
        }

        [Test]
        public void NormalizeEndLinesToUnix_ReturnsNullForNull()
        {
            var result = StringExtensions.NormalizeEndLinesToUnix(null);
            Assert.IsNull(result);
        }

        [Test]
        public void NormalizeEndLinesToUnix_ReturnsEmptyForEmpty()
        {
            var result = string.Empty.NormalizeEndLinesToUnix();
            Assert.IsEmpty(result);
        }

        [Test]
        public void NormalizeEndLinesToUnix_ReturnsSameStringForNoNewlines()
        {
            const string passage = "When I was younger so much younger than today";

            var result = passage.NormalizeEndLinesToUnix();
            Assert.AreEqual(passage, result);
        }

        [Test]
        public void NormalizeEndLinesToUnix_ReturnsSameStringForUnixNewlines()
        {
            const string passage = "When I was younger so much younger than today\nI never needed anybody to help me any way";

            var result = passage.NormalizeEndLinesToUnix();
            Assert.AreEqual(passage, result);
        }

        [Test]
        public void NormalizeEndLinesToUnix_ReturnsNormalizedStringForMacNewlines()
        {
            const string passage = "When I was younger so much younger than today\rI never needed anybody to help me any way";
            const string normalized = "When I was younger so much younger than today\nI never needed anybody to help me any way";

            var result = passage.NormalizeEndLinesToUnix();
            Assert.AreEqual(normalized, result);
        }

        [Test]
        public void NormalizeEndLinesToUnix_ReturnsNormalizedStringForWindowsNewlines()
        {
            const string passage = "When I was younger so much younger than today\r\nI never needed anybody to help me any way";
            const string normalized = "When I was younger so much younger than today\nI never needed anybody to help me any way";

            var result = passage.NormalizeEndLinesToUnix();
            Assert.AreEqual(normalized, result);
        }
    }
}