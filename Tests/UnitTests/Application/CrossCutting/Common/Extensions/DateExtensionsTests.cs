﻿using System;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace UnitTests.Application.CrossCutting.Common.Extensions
{
    [TestFixture]
    public class DateExtensionsTests
    {
        [Test]
        public void GetLastDayOfWeekInMonth_TestPositive()
        {
            var date = DateTime.ParseExact("2015-01-15", "yyyy-MM-dd", null);
            var lastFridayInJanuary = DateTime.ParseExact("2015-01-30", "yyyy-MM-dd", null);
            var lastCalculatedFriday = date.GetLastDayOfWeekInMonth(DayOfWeek.Friday);
            Assert.AreEqual(lastFridayInJanuary, lastCalculatedFriday);
        }

        [Test]
        public void GetLastDayInMonth_TestPositive()
        {
            var date = DateTime.ParseExact("2015-01-15", "yyyy-MM-dd", null);
            var lastDateInJanuary = DateTime.ParseExact("2015-01-31", "yyyy-MM-dd", null);
            var lastCalculatedDay = date.GetLastDayInMonth();
            Assert.AreEqual(lastDateInJanuary, lastCalculatedDay);
        }

        [Test]
        public void GetNextDayOfWeek_TestPositive()
        {
            var date = DateTime.ParseExact("2015-01-15", "yyyy-MM-dd", null);
            var nextMonday = DateTime.ParseExact("2015-01-19", "yyyy-MM-dd", null);
            var calculatedDay = date.GetNextDayOfWeek(DayOfWeek.Monday);
            Assert.AreEqual(nextMonday, calculatedDay);
        }

        [Test]
        public void GetPreviousDayOfWeek_TestPositive()
        {
            var date = DateTime.ParseExact("2015-01-15", "yyyy-MM-dd", null);
            var nextMonday = DateTime.ParseExact("2015-01-12", "yyyy-MM-dd", null);
            var calculatedDay = date.GetPreviousDayOfWeek(DayOfWeek.Monday);
            Assert.AreEqual(nextMonday, calculatedDay);
        }

        [Test]
        [TestCase(14, false, Description = "Monday")]
        [TestCase(15, false, Description = "Tuesday")]
        [TestCase(16, false, Description = "Wednesday")]
        [TestCase(17, false, Description = "Thursday")]
        [TestCase(18, false, Description = "Friday")]
        [TestCase(19, true, Description = "Saturday")]
        [TestCase(20, false, Description = "Sunday")]
        public void IsLastWorkingDayOfWeek_CheckingTheWeekDays(int day, bool isLastWorkingDay)
        {
            var date = new DateTime(2016, 11, day);

            var result = date.IsLastWorkingDayOfWeek(DayOfWeek.Saturday);

            Assert.AreEqual(isLastWorkingDay, result);
        }

        [Test]
        [TestCase(14, false, Description = "Monday")]
        [TestCase(15, false, Description = "Tuesday")]
        [TestCase(16, false, Description = "Wednesday")]
        [TestCase(17, false, Description = "Thursday")]
        [TestCase(18, false, Description = "Friday")]
        [TestCase(19, true, Description = "Saturday")]
        [TestCase(20, true, Description = "Sunday")]
        public void IsWeekend_CheckingTheWeekDays(int day, bool isWeekend)
        {
            var date = new DateTime(2016, 11, day);

            var result = date.IsWeekend();

            Assert.AreEqual(isWeekend, result);
        }

        [Test]
        public void StartOfDay_TestPositive()
        {
            var date = new DateTime(2015, 10, 17, 12, 0, 0);
            var dateTimeStartOfDay = new DateTime(2015, 10, 17);

            var result = date.StartOfDay();

            Assert.AreEqual(dateTimeStartOfDay, result);
        }

        [Test]
        public void EndOfDay_TestPositive()
        {
            var date = new DateTime(2015, 10, 17, 12, 0, 0);
            var dateTimeEndOfDay = new DateTime(2015, 10, 18).AddTicks(-1);

            var result = date.EndOfDay();

            Assert.AreEqual(dateTimeEndOfDay, result);
        }
    }
}
