﻿using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Extensions;
using System;
using UnitTests.Application.CrossCutting.Common.Helpers.Resources;

namespace UnitTests.Application.CrossCutting.Common.Extensions
{
    [TestFixture]
    public class EnumExtensionsTests
    {
        private const string EnumDescription = "Option";
        private enum TestEnum
        {
            [System.ComponentModel.Description(EnumDescription)]
            OptionWithDescription,

            [DescriptionLocalized("Option2Label", typeof(SampleResources))]
            OptionWithLocalizedDescription,

            OptionWithNoDescription
        }

        [Flags]
        private enum TestFlagsEnum
        {
            [System.ComponentModel.Description(EnumDescription)]
            OptionWithDescription = 1 << 0,

            [DescriptionLocalized("Option2Label", typeof(SampleResources))]
            OptionWithLocalizedDescription = 1 << 1,

            OptionWithNoDescription = 1 << 2
        }

        [Test]
        public void GetDescription_TestPositiveSomeFlags()
        {
            Assert.AreEqual($"{EnumDescription}, {SampleResources.Option2Label}", 
                EnumExtensions.GetDescription(
                    TestFlagsEnum.OptionWithDescription | TestFlagsEnum.OptionWithLocalizedDescription));
        }

        [Test]
        public void GetDescription_TestPositiveFlagWithLocalizedDescription()
        {
            Assert.AreEqual(SampleResources.Option2Label, 
                TestFlagsEnum.OptionWithLocalizedDescription.GetDescription());
        }

        [Test]
        public void GetDescription_TestPositiveFlagWithDescription()
        {
            Assert.AreEqual(EnumDescription, TestFlagsEnum.OptionWithDescription.GetDescription());
        }

        [Test]
        public void GetDescription_TestPositiveFlagWithNoDescription()
        {
            Assert.AreEqual(string.Empty, TestFlagsEnum.OptionWithNoDescription.GetDescription());
        }

        [Test]
        public void GetDescriptionOrValue_TestPositiveSomeFlags()
        {
            Assert.AreEqual($"{EnumDescription}, {SampleResources.Option2Label}",
                EnumExtensions.GetDescriptionOrValue(
                    TestFlagsEnum.OptionWithDescription | TestFlagsEnum.OptionWithLocalizedDescription));
        }

        [Test]
        public void GetDescriptionOrValue_TestPositiveFlagWithLocalizedDescription()
        {
            Assert.AreEqual(SampleResources.Option2Label, 
                TestFlagsEnum.OptionWithLocalizedDescription.GetDescriptionOrValue());
        }

        [Test]
        public void GetDescriptionOrValue_TestPositiveFlagWithDescription()
        {
            Assert.AreEqual(EnumDescription, TestFlagsEnum.OptionWithDescription.GetDescriptionOrValue());
        }

        [Test]
        public void GetDescriptionOrValue_TestPositiveFlagWithNoDescription()
        {
            Assert.AreEqual(TestFlagsEnum.OptionWithNoDescription.ToString(), 
                TestFlagsEnum.OptionWithNoDescription.GetDescriptionOrValue());
        }

        [Test]
        public void GetDescription_TestPositive()
        {
            Assert.AreEqual(EnumDescription, TestEnum.OptionWithDescription.GetDescription());
        }

        [Test]
        public void GetDescription_TestPositiveLocalized()
        {
            Assert.AreEqual(SampleResources.Option2Label, TestEnum.OptionWithLocalizedDescription.GetDescription());
        }

        [Test]
        public void GetDescription_TestPositiveEmpty()
        {
            Assert.AreEqual(string.Empty, TestEnum.OptionWithNoDescription.GetDescription());
        }

        [Test]
        public void GetDescriptionOrValue_TestPositive()
        {
            Assert.AreEqual(EnumDescription, TestEnum.OptionWithDescription.GetDescriptionOrValue());
        }

        [Test]
        public void GetDescriptionOrValue_TestPositiveLocalized()
        {
            Assert.AreEqual(SampleResources.Option2Label, 
                TestEnum.OptionWithLocalizedDescription.GetDescriptionOrValue());
        }

        [Test]
        public void GetDescriptionOrValue_TestPositiveName()
        {
            Assert.AreEqual(TestEnum.OptionWithNoDescription.ToString(), 
                TestEnum.OptionWithNoDescription.GetDescriptionOrValue());
        }
    }
}
