﻿using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Models;
using System;
using System.Collections.Generic;

namespace UnitTests.Application.CrossCutting.Common
{
    [TestFixture]
    public class PropertyPathTests
    {
        [Test]
        public void Constructor_WhenPropertyPathIsNull_ThenThrowArgumentNullException()
        {
            IEnumerable<string> propertyPath = null;
            Assert.Throws<ArgumentNullException>(() => new PropertyPath(propertyPath));
        }

        [Test]
        public void ResolveValueOrDefault_WhenModelContainsPropertyFromPath_ThenCanResolveValue()
        {
            var model = new
            {
                ExistingProperty = "TestString"
            };

            var propertyPath = new PropertyPath("ExistingProperty");
            var propertyValue = propertyPath.ResolveValueOrDefault<string>(model);

            Assert.AreEqual("TestString", propertyValue);
        }

        [Test]
        public void ResolveValueOrDefault_WhenModelNotContainsPropertyFromPath_ThenThrowPropertyNotFoundException()
        {
            var model = new
            {
                ExistingProperty = string.Empty
            };

            var propertyPath = new PropertyPath("NotExistingProperty");
            Assert.Throws<PropertyNotFoundException>(() => propertyPath.ResolveValueOrDefault<string>(model));
        }

        [Test]
        public void ResolveValueOrDefault_WhenPathContainsPropertyChain_ThenCanResolveLastPropertyValue()
        {
            var model = new
            {
                Level1Property = new
                {
                    Level2Property = "TestString"
                }
            };

            var propertyPath = new PropertyPath("Level1Property.Level2Property");
            var propertyValue = propertyPath.ResolveValueOrDefault<string>(model);

            Assert.AreEqual("TestString", propertyValue);
        }

        [Test]
        public void ResolveValueOrDefault_WhenPathContainsPropertyChainAndReturnsNull_ThenReturnNull()
        {
            var model = new
            {
                Level1Property = new TestClass()
            };

            var propertyPath = new PropertyPath("Level1Property.PropertyNull.PropertyString");
            var propertyValue = propertyPath.ResolveValueOrDefault<string>(model);

            Assert.Null(propertyValue);
        }

        [Test]
        public void ResolveType_WhenPathContainsPropertyName_ThenCanResolvePropertyType()
        {
            var model = new
            {
                Level1Property = string.Empty
            };

            var propertyPath = new PropertyPath("Level1Property");
            var propertyType = propertyPath.ResolveType(model);

            Assert.AreEqual(typeof(string), propertyType);
        }

        [Test]
        public void ResolveType_WhenPathContainsPropertyChain_ThenCanResolveLastPropertyType()
        {
            var model = new
            {
                Level1Property = new
                {
                    Level2Property = "TestString"
                }
            };

            var propertyPath = new PropertyPath("Level1Property.Level2Property");
            var propertyType = propertyPath.ResolveType(model);

            Assert.AreEqual(typeof(string), propertyType);
        }

        [Test]
        public void ResolveType_WhenPathContainsPropertyChainAndReturnNull_ThenCanResolveLastPropertyType()
        {
            var model = new
            {
                Level1Property = new TestClass()
            };

            var propertyPath = new PropertyPath("Level1Property.PropertyNull.PropertyInt");
            var propertyType = propertyPath.ResolveType(model);

            Assert.AreEqual(typeof(int), propertyType);
        }

        [Test]
        public void ResolveType_WhenPathContainsNoParamsVoidMethod_ThenCanResolveMethodReturnType()
        {
            var propertyPath = new PropertyPath("MethodVoid()");
            var propertyType = propertyPath.ResolveType(new TestClass());

            Assert.AreEqual(typeof(void), propertyType);
        }

        [Test]
        public void ResolveType_WhenPathContainsNoParamsMethod_ThenCanResolveMethodReturnType()
        {
            var propertyPath = new PropertyPath("MethodString()");
            var propertyType = propertyPath.ResolveType(new TestClass());

            Assert.AreEqual(typeof(string), propertyType);
        }

        [Test]
        public void ResolveType_WhenPathContainsMethodNameAndProperty_ThenCanResolvePropertyType()
        {
            var propertyPath = new PropertyPath("MethodParent().PropertyInt");
            var propertyType = propertyPath.ResolveType(new TestClass());

            Assert.AreEqual(typeof(int), propertyType);
        }

        [Test]
        public void ResolveType_WhenModelContainsMethodNameAndPropertyReturnsNull_ThenCanResolvePropertyType()
        {
            var propertyPath = new PropertyPath("MethodNull().PropertyInt");
            var propertyType = propertyPath.ResolveType(new TestClass());

            Assert.AreEqual(typeof(int), propertyType);
        }

        [Test]
        public void ResolveType_WhenPropertyPathContainsIndexerProperty_ThenCanResolveCollectionArgumentType()
        {
            var propertyPath = new PropertyPath("IndexerTest[0]");
            var propertyType = propertyPath.ResolveType(new TestClass());

            Assert.AreEqual(typeof(TestClass), propertyType);
        }

        [Test]
        public void ResolveType_WhenPropertyPathContainsPropertyChaninWithIndexerProperty_ThenCanResolvePropertyType()
        {
            var propertyPath = new PropertyPath("IndexerTest[0].PropertyInt");
            var propertyType = propertyPath.ResolveType(new TestClass());

            Assert.AreEqual(typeof(int), propertyType);
        }

        [Test]
        public void ResolveType_WhenPropertyPathContainsIndexerPropertyWithWrongIndexValue_ThenCanResolveCollectionArgumentType()
        {
            var propertyPath = new PropertyPath("IndexerTest[100]");
            var propertyType = propertyPath.ResolveType(new TestClass());

            Assert.AreEqual(typeof(TestClass), propertyType);
        }

        [Test]
        public void ResolveType_WhenPropertyPathContainsPropertyChaninWithIndexerPropertyWithWrongIndexValue_ThenCanResolvePropertyType()
        {
            var propertyPath = new PropertyPath("IndexerTest[100].PropertyInt");
            var propertyType = propertyPath.ResolveType(new TestClass());

            Assert.AreEqual(typeof(int), propertyType);
        }

        private class TestClass
        {
            public int PropertyInt { get; set; }

            public string PropertyString { get; set; }

            public TestClass PropertyNull { get { return null; } }

            public List<TestClass> IndexerNull { get; }

            public List<TestClass> IndexerTest
            {
                get
                {
                    return new List<TestClass> {
                    new TestClass(),
                    new TestClass()
                };
                }
            }

            public void MethodVoid() { }

            public string MethodString() { return string.Empty; }

            public TestClass MethodParent() { return new TestClass(); }

            public TestClass MethodNull() { return null; }
        }
    }
}
