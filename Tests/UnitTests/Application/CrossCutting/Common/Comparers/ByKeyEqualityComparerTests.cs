﻿using System;
using System.Collections.Generic;
using Castle.Components.DictionaryAdapter.Xml;
using Moq;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Comparers;

namespace UnitTests.Application.CrossCutting.Common.Comparers
{
    [TestFixture]
    public class ByKeyEqualityComparerTests
    {
        private readonly Tuple<int, string> _value1 = Tuple.Create(1, "a");
        private readonly Tuple<int, string> _value2 = Tuple.Create(1, "b");
        private readonly Tuple<int, string> _value3 = Tuple.Create(2, "a");
        private readonly Tuple<int, string> _value4 = Tuple.Create(2, "b");
        private readonly Tuple<int, string> _value5 = Tuple.Create(3, "a");

        [Test]
        public void Equals_Item1()
        {
            var equalityComparer = new ByKeyEqualityComparer<Tuple<int, string>, int>(t => t.Item1);

            Assert.IsTrue(equalityComparer.Equals(_value1, _value1));
            Assert.IsTrue(equalityComparer.Equals(_value1, _value2));
            Assert.IsFalse(equalityComparer.Equals(_value1, _value3));
            Assert.IsFalse(equalityComparer.Equals(_value1, _value4));
            Assert.IsTrue(equalityComparer.Equals(_value2, _value1));
            Assert.IsTrue(equalityComparer.Equals(_value2, _value2));
            Assert.IsFalse(equalityComparer.Equals(_value2, _value3));
            Assert.IsFalse(equalityComparer.Equals(_value2, _value4));
            Assert.IsFalse(equalityComparer.Equals(_value3, _value1));
            Assert.IsFalse(equalityComparer.Equals(_value3, _value2));
            Assert.IsTrue(equalityComparer.Equals(_value3, _value3));
            Assert.IsTrue(equalityComparer.Equals(_value3, _value4));
            Assert.IsFalse(equalityComparer.Equals(_value4, _value1));
            Assert.IsFalse(equalityComparer.Equals(_value4, _value2));
            Assert.IsTrue(equalityComparer.Equals(_value4, _value3));
            Assert.IsTrue(equalityComparer.Equals(_value4, _value4));
        }
        
        [Test]
        public void Equals_Item2()
        {
            var equalityComparer = new ByKeyEqualityComparer<Tuple<int, string>, string>(t => t.Item2);

            Assert.IsTrue(equalityComparer.Equals(_value1, _value1));
            Assert.IsFalse(equalityComparer.Equals(_value1, _value2));
            Assert.IsTrue(equalityComparer.Equals(_value1, _value3));
            Assert.IsFalse(equalityComparer.Equals(_value1, _value4));
            Assert.IsFalse(equalityComparer.Equals(_value2, _value1));
            Assert.IsTrue(equalityComparer.Equals(_value2, _value2));
            Assert.IsFalse(equalityComparer.Equals(_value2, _value3));
            Assert.IsTrue(equalityComparer.Equals(_value2, _value4));
            Assert.IsTrue(equalityComparer.Equals(_value3, _value1));
            Assert.IsFalse(equalityComparer.Equals(_value3, _value2));
            Assert.IsTrue(equalityComparer.Equals(_value3, _value3));
            Assert.IsFalse(equalityComparer.Equals(_value3, _value4));
            Assert.IsFalse(equalityComparer.Equals(_value4, _value1));
            Assert.IsTrue(equalityComparer.Equals(_value4, _value2));
            Assert.IsFalse(equalityComparer.Equals(_value4, _value3));
            Assert.IsTrue(equalityComparer.Equals(_value4, _value4));
        }

        [Test]
        public void GetHashCode_Item1()
        {
            // if hashes become equal some day, we'll need rewrite this test
            Assert.AreNotEqual(1.GetHashCode(), 2.GetHashCode());

            var equalityComparer = new ByKeyEqualityComparer<Tuple<int, string>, int>(t => t.Item1);

            Assert.AreEqual(1.GetHashCode(), equalityComparer.GetHashCode(_value1));
            Assert.AreEqual(1.GetHashCode(), equalityComparer.GetHashCode(_value2));
            Assert.AreEqual(2.GetHashCode(), equalityComparer.GetHashCode(_value3));
            Assert.AreEqual(2.GetHashCode(), equalityComparer.GetHashCode(_value4));
        }

        [Test]
        public void GetHashCode_Item2()
        {
            // if hashes become equal some day, we'll need rewrite this test
            Assert.AreNotEqual("a".GetHashCode(), "b".GetHashCode());

            var equalityComparer = new ByKeyEqualityComparer<Tuple<int, string>, string>(t => t.Item2);

            Assert.AreEqual("a".GetHashCode(), equalityComparer.GetHashCode(_value1));
            Assert.AreEqual("b".GetHashCode(), equalityComparer.GetHashCode(_value2));
            Assert.AreEqual("a".GetHashCode(), equalityComparer.GetHashCode(_value3));
            Assert.AreEqual("b".GetHashCode(), equalityComparer.GetHashCode(_value4));
        }

        [Test]
        public void Equals_KeyComparer()
        {
            var mock = new Mock<IEqualityComparer<int>>();
            var equalityComparer = new ByKeyEqualityComparer<Tuple<int, string>, int>(t => t.Item1, mock.Object);

            mock.Setup(e => e.Equals(It.IsAny<int>(), It.IsAny<int>()))
                .Returns((int x, int y) => (x % 2) == (y %2));

            Assert.IsTrue(equalityComparer.Equals(_value1, _value1));
            Assert.IsFalse(equalityComparer.Equals(_value1, _value3));
            Assert.IsTrue(equalityComparer.Equals(_value1, _value5));
            Assert.IsFalse(equalityComparer.Equals(_value3, _value1));
            Assert.IsTrue(equalityComparer.Equals(_value3, _value3));
            Assert.IsFalse(equalityComparer.Equals(_value3, _value5));
            Assert.IsTrue(equalityComparer.Equals(_value5, _value1));
            Assert.IsFalse(equalityComparer.Equals(_value5, _value3));
            Assert.IsTrue(equalityComparer.Equals(_value5, _value5));
        }

        [Test]
        public void GetHashCode_KeyComparer()
        {
            var mock = new Mock<IEqualityComparer<int>>();
            var equalityComparer = new ByKeyEqualityComparer<Tuple<int, string>, int>(t => t.Item1, mock.Object);

            const int hashCodeOf1 = 42;
            const int hashCodeOf2 = 13;

            mock.Setup(e => e.GetHashCode(1)).Returns(hashCodeOf1);
            mock.Setup(e => e.GetHashCode(2)).Returns(hashCodeOf2);

            Assert.AreEqual(hashCodeOf1, equalityComparer.GetHashCode(_value1));
            Assert.AreEqual(hashCodeOf1, equalityComparer.GetHashCode(_value2));
            Assert.AreEqual(hashCodeOf2, equalityComparer.GetHashCode(_value3));
            Assert.AreEqual(hashCodeOf2, equalityComparer.GetHashCode(_value4));
        }
    }
}
