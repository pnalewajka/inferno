﻿using System;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Application.CrossCutting.Common.Exceptions
{
    [TestFixture]
    public class GenericParameterExceptionTests
    {
        [Test]
        public void GenericParameterExceptionValuesPassing_SpecializedConstructor()
        {
            var encounteredType = typeof(string);
            var expectedType = typeof(Enum);
            var exception = new GenericParameterException(encounteredType, expectedType);
            Assert.IsNotNull(exception.Message);
            Assert.AreEqual(encounteredType.FullName, exception.GenericParameterType);
            Assert.AreEqual(expectedType.FullName, exception.ExpectedParameterType);
        }

        [Test]
        public void GenericParameterExceptionValuesPassing_MessageConstructor()
        {
            const string message = "Incorrect generic parameter type";
            var exceptionWithMessageOnly = new GenericParameterException(message);
            Assert.AreEqual(message, exceptionWithMessageOnly.Message);
            Assert.IsNull(exceptionWithMessageOnly.GenericParameterType);
        }

        [Test]
        public void GenericParameterExceptionValuesPassing_DefaultConstructor()
        {
            var emptyException = new GenericParameterException();
            Assert.IsNotEmpty(emptyException.Message);
            Assert.IsNull(emptyException.GenericParameterType);
        }

        [Test]
        public void GenericParameterExceptionValuesPassing_InnerExceptionConstructor()
        {
            const string message = "Incorrect generic parameter type";

            var emptyException = new GenericParameterException();

            var exceptionWithInnerException = new GenericParameterException(message, emptyException);
            Assert.AreEqual(message, exceptionWithInnerException.Message);
            Assert.AreSame(emptyException, exceptionWithInnerException.InnerException);
        }



        [Test]
        public void GenericParameterExceptionTest_Serialization()
        {
            var typeEncountered = typeof (string);
            var expectedType = typeof (Enum);
            var exception = new GenericParameterException(typeEncountered, expectedType);

            var xml = DataContractSerializationHelper.SerializeToXml(exception);
            var deserializedException = DataContractSerializationHelper.DeserializeFromXml<GenericParameterException>(xml);

            Assert.AreEqual(exception.Message, deserializedException.Message);
            Assert.AreEqual(exception.GenericParameterType, deserializedException.GenericParameterType);
        }

    }
}