﻿using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Application.CrossCutting.Common.Exceptions
{
    [TestFixture]
    public class ResourceNotFoundExceptionTests
    {
        [Test]
        public void ResourceNotFoundExceptionValuesPassing_SpecializedConstructor()
        {
            const string resourceNotFound = "Resource not found";
            const string resource1Txt = "Resource1.txt";
            const string assemblyName = "NUnitTests.Application.CrossCutting.Common.Exceptions";
            var exception = new ResourceNotFoundException(resourceNotFound, resource1Txt,
                assemblyName);
            Assert.AreEqual(resourceNotFound, exception.Message);
            Assert.AreEqual(resource1Txt, exception.ResourceName);
            Assert.AreEqual(assemblyName, exception.AssemblyName);
        }

        [Test]
        public void ResourceNotFoundExceptionValuesPassing_MessageConstructor()
        {
            const string resourceNotFound = "Resource not found";

            var exceptionWithMessageOnly = new ResourceNotFoundException(resourceNotFound);
            Assert.AreEqual(resourceNotFound, exceptionWithMessageOnly.Message);
            Assert.IsNull(exceptionWithMessageOnly.ResourceName);
            Assert.IsNull(exceptionWithMessageOnly.AssemblyName);
        }

        [Test]
        public void ResourceNotFoundExceptionValuesPassing_DefaultConstructor()
        {
            var emptyException = new ResourceNotFoundException();
            Assert.IsNotEmpty(emptyException.Message);
            Assert.IsNull(emptyException.ResourceName);
            Assert.IsNull(emptyException.AssemblyName);
        }

        [Test]
        public void ResourceNotFoundExceptionValuesPassing_InnerExceptionConstructor()
        {
            const string resourceNotFound = "Resource not found";

            var emptyException = new ResourceNotFoundException();

            var exceptionWithInnerException = new ResourceNotFoundException(resourceNotFound, emptyException);
            Assert.AreEqual(resourceNotFound, exceptionWithInnerException.Message);
            Assert.AreSame(emptyException, exceptionWithInnerException.InnerException);
        }



        [Test]
        public void ResourceNotFoundExceptionTest_Serialization()
        {
            const string resourceNotFound = "Resource not found";
            const string resource1Txt = "Resource1.txt";
            const string assemblyName = "NUnitTests.Application.CrossCutting.Common.Exceptions";
            var exception = new ResourceNotFoundException(resourceNotFound, resource1Txt,
                assemblyName);

            var xml = DataContractSerializationHelper.SerializeToXml(exception);
            var deserializedException = DataContractSerializationHelper.DeserializeFromXml<ResourceNotFoundException>(xml);

            Assert.AreEqual(exception.Message, deserializedException.Message);
            Assert.AreEqual(exception.ResourceName, deserializedException.ResourceName);
            Assert.AreEqual(exception.AssemblyName, deserializedException.AssemblyName);
        }

    }
}
