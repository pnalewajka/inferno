﻿using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Application.CrossCutting.Common.Exceptions
{
    [TestFixture]
    public class SystemParameterNotFoundExceptionTests
    {
        [Test]
        public void SystemParameterNotFoundException_Serialization()
        {
            const string parameterKey = "TEST_KEY";
            const string parameterType = "int";

            var exception = new SystemParameterNotFoundException(parameterKey, parameterType);

            var xml = DataContractSerializationHelper.SerializeToXml(exception);
            var deserializedException = DataContractSerializationHelper.DeserializeFromXml<SystemParameterNotFoundException>(xml);


            Assert.AreEqual(parameterKey, exception.Key);
            Assert.AreEqual(parameterType, exception.ValueTypeId);
            Assert.AreEqual(exception.Message, deserializedException.Message);
            Assert.AreEqual(exception.ValueTypeId, deserializedException.ValueTypeId);
            Assert.AreEqual(exception.Key, deserializedException.Key);
        }

    }
}