﻿using System;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    public partial class PropertyHelperTests
    {
        private abstract class TestClass1
        {
            protected TestClass1(int testProperty1, int testProperty2, DateTime testPropertyDateTime)
            {
                TestPropertyDateTime = testPropertyDateTime;
                TestProperty2 = testProperty2;
                TestProperty1 = testProperty1;
            }

            public int TestProperty1 { get; private set; }
            public int TestProperty2 { get; private set; }
            public DateTime TestPropertyDateTime { get; private set; }
        }
        
// ReSharper disable once ClassWithVirtualMembersNeverInherited.Local
        private class TestClassVirtualProp
        {
            public virtual long Id { get; set; }
        }

        private class TestClassNotVirtualProp
        {
// ReSharper disable once UnusedMember.Local
            public long Id { get; set; }
        }

    }
}
