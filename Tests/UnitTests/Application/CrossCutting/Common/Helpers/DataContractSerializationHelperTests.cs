﻿using System;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public class DataContractSerializationHelperTests
    {
        [Serializable]
        public class Test
        {
            public int IntValue1 { get; set; }
            public string StringValue1 { get; set; }
        }

        public class TestClass1
        {
            public string TestValue1 { get; set; }
        }

        [Test]
        public void SerializeToXml_TestSerializationAndDeserialization()
        {
            var dataObject = new Test
            {
                IntValue1 = 123,
                StringValue1 = Guid.NewGuid().ToString(),
            };
            var xml = DataContractSerializationHelper.SerializeToXml(dataObject, typeof (Test));
            var deserializedObject = DataContractSerializationHelper.DeserializeFromXml(xml, typeof(Test)) as Test;
            Assert.IsNotNull(deserializedObject);
            Assert.AreEqual(dataObject.IntValue1, deserializedObject.IntValue1);
            Assert.AreEqual(dataObject.StringValue1, deserializedObject.StringValue1);
        }

    }
}