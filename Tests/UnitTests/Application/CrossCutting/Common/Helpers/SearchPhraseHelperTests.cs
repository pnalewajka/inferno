﻿using System.Linq;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public class SearchPhraseHelperTests
    {
        [Test]
        public void GetPhrases_TestSimpleWordsSplitting()
        {
            var searchElements = SearchPhraseHelper.GetPhrases("this is a test").ToList();
            
            Assert.AreEqual(4, searchElements.Count);
            Assert.AreEqual("this", searchElements[0]);
            Assert.AreEqual("is", searchElements[1]);
            Assert.AreEqual("a", searchElements[2]);
            Assert.AreEqual("test", searchElements[3]);
        }

        [Test]
        public void GetPhrases_TestSingleWordSplitting()
        {
            var searchElements = SearchPhraseHelper.GetPhrases("single").ToList();

            Assert.AreEqual(1, searchElements.Count);
            Assert.AreEqual("single", searchElements[0]);
        }

        [Test]
        public void GetPhrases_TestSimpleQuotation()
        {
            var searchElements = SearchPhraseHelper.GetPhrases("\"quoted\"").ToList();

            Assert.AreEqual(1, searchElements.Count);
            Assert.AreEqual("quoted", searchElements[0]);
        }

        [Test]
        public void GetPhrases_TestComplexQuotation()
        {
            var searchElements = SearchPhraseHelper.GetPhrases("\"quoted\" and \"quoted\"").ToList();

            Assert.AreEqual(3, searchElements.Count);
            Assert.AreEqual("quoted", searchElements[0]);
            Assert.AreEqual("and", searchElements[1]);
            Assert.AreEqual("quoted", searchElements[2]);
        }

        [Test]
        public void GetPhrases_TestQuotationWithWhiteSpace()
        {
            var searchElements = SearchPhraseHelper.GetPhrases("\"quoted with whitespace\" and others").ToList();

            Assert.AreEqual(3, searchElements.Count);
            Assert.AreEqual("quoted with whitespace", searchElements[0]);
            Assert.AreEqual("and", searchElements[1]);
            Assert.AreEqual("others", searchElements[2]);
        }

        [Test]
        public void GetPhrases_TestEmptyQuotationShouldBeIgnored()
        {
            var searchElements = SearchPhraseHelper.GetPhrases("\"\" and \"quoted\"").ToList();

            Assert.AreEqual(2, searchElements.Count);
            Assert.AreEqual("and", searchElements[0]);
            Assert.AreEqual("quoted", searchElements[1]);
        }

        [Test]
        public void GetPhrases_TextShouldBeLowercased()
        {
            var searchElements = SearchPhraseHelper.GetPhrases("\"TesT\" TesT").ToList();

            Assert.AreEqual(2, searchElements.Count);
            Assert.AreEqual("test", searchElements[0]);
            Assert.AreEqual("test", searchElements[1]);            
        }
    }
}