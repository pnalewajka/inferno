﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public partial class TypeHelperTests
    {
        [Test]
        public void FindTypeInLoadedAssemblies_PositiveTest()
        {
            var searchingForType = typeof(TypeHelperTests);
            var foundType = TypeHelper.FindTypeInLoadedAssemblies(searchingForType.FullName);
            Assert.AreEqual(searchingForType, foundType);
        }

        [Test]
        public void FindTypeInLoadedAssemblies_NegativeTest()
        {
            var searchingForType = "System." + Guid.NewGuid();
            var foundType = TypeHelper.FindTypeInLoadedAssemblies(searchingForType);
            Assert.IsNull(foundType);
        }

        [Test]
        public void GetSubClassesOf_PositiveTest()
        {
            var subClassess = TypeHelper.GetSubclassesOf(typeof(TestClass2));
            const int expectedSubclassesCount = 1;
            Assert.AreEqual(expectedSubclassesCount, subClassess.Count());
        }

        [Test]
        public void GetSubClassesOf_ShouldReturnExistingSubClassesFromSpecifiedAssembly()
        {
            var type = typeof(TestClass2);
            var subClassess = TypeHelper.GetSubclassesOf(type, type.Assembly);

            Assert.AreEqual(1, subClassess.Count());
        }

        [Test]
        public void GetAllSubClassesOf_ShouldReturnExistingSubClassesOnly()
        {
            var type = typeof(TestClass1);
            var subClassess = TypeHelper.GetAllSubClassesOf(type, false);

            Assert.AreEqual(1, subClassess.Count());
        }

        [Test]
        public void GetAllSubClassesOf_ShouldReturnExistingSubClassesIncludingType()
        {
            var type = typeof(TestClass1);
            var subClassess = TypeHelper.GetAllSubClassesOf(type, true);

            Assert.AreEqual(2, subClassess.Count());
        }

        [Test]
        public void GetAllSubClassesOf_ShouldReturnOnlyOneForStringIncludingType()
        {
            var type = typeof(String);
            var subClassess = TypeHelper.GetAllSubClassesOf(type, true);

            Assert.AreEqual(1, subClassess.Count());
        }

        [Test]
        public void GetAllSubClassesOf_ShouldReturnNoneForStringOnly()
        {
            var type = typeof(String);
            var subClassess = TypeHelper.GetAllSubClassesOf(type, false);

            Assert.AreEqual(0, subClassess.Count());
        }

        [Test]
        public void GetSubClassesOf_ShouldReturnNoSubClassesFromOtherSpecifiedAssembly()
        {
            var type = typeof(TestClass2);
            var otherType = typeof(DateTime);

            var subClassess = TypeHelper.GetSubclassesOf(type, otherType.Assembly);

            Assert.AreEqual(0, subClassess.Count());
        }

        [Test]
        public void IsSubclassOfRawGeneric_PositiveTest()
        {
            var result = TypeHelper.IsSubclassOfRawGeneric(typeof(TestClassSubTypeOfGeneric<>), typeof(List<>));
            Assert.IsTrue(result);
        }

        [Test]
        public void ExtractIfNullable_PositiveTest()
        {
            Assert.AreEqual(typeof(int), TypeHelper.ExtractIfNullable(typeof(int?)));
        }

        [Test]
        public void GetFullNameWithAssemblyName_PositiveTest()
        {
            var thisClass = GetType().FullName + ", " + GetType().Assembly.GetName().Name;
            Assert.AreEqual(thisClass, TypeHelper.GetFullNameWithAssemblyName(GetType()));
        }

        [Test]
        public void IsNullableEnum_ShouldReturnTrueForNullableEnums()
        {
            Assert.IsTrue(TypeHelper.IsNullableEnum(typeof(DayOfWeek?)));
        }

        [Test]
        public void IsNullableEnum_ShouldReturnFalseForOtherTypes()
        {
            Assert.IsFalse(TypeHelper.IsNullableEnum(typeof(DayOfWeek)));
            Assert.IsFalse(TypeHelper.IsNullableEnum(typeof(int?)));
            Assert.IsFalse(TypeHelper.IsNullableEnum(typeof(int)));
        }

        [Test]
        public void GetNonPublicInstanceMethods_PositiveTest()
        {
            var members = TypeHelper.GetNonPublicInstanceMethods(typeof(TestClass3)).ToList();
            //2 methods from TestClass3, 2 methods - finalize & memberwiseclone
            const int expectedNumberOfMethods = 4;
            Assert.AreEqual(expectedNumberOfMethods, members.Count());
            const string testMethod1Name = "TestMethod1";
            Assert.IsTrue(members.Any(m => m.Name == testMethod1Name));
        }

        [Test]
        public void GetCollectionElementType_PositiveTest()
        {
            var list = new List<object>();
            var type = TypeHelper.GetCollectionElementType(list.GetType());

            Assert.AreEqual(typeof(object), type);
        }

        [Test]
        [ExpectedException(ExpectedException = typeof(InvalidOperationException))]
        public void GetCollectionElementType_NegativeTest()
        {
            TypeHelper.GetCollectionElementType(typeof(List));
        }

        [Test]
        public void IsNumeric_PositiveTest()
        {
            Assert.IsTrue(TypeHelper.IsNumeric(typeof(double)));
            Assert.IsTrue(TypeHelper.IsNumeric(typeof(long?)));
        }

        [Test]
        public void IsNumeric_NegativeTest()
        {
            Assert.IsFalse(TypeHelper.IsNumeric(typeof(string)));
            Assert.IsFalse(TypeHelper.IsNumeric(typeof(long?), false));
        }

        [Test]
        public void IsIntegral_PositiveTest()
        {
            Assert.IsTrue(TypeHelper.IsIntegral(typeof(int)));
            Assert.IsTrue(TypeHelper.IsIntegral(typeof(long?)));
        }

        [Test]
        public void IsIntegral_NegativeTest()
        {
            Assert.IsFalse(TypeHelper.IsIntegral(typeof(string)));
            Assert.IsFalse(TypeHelper.IsIntegral(typeof(long?), false));
            Assert.IsFalse(TypeHelper.IsIntegral(typeof(double)));
            Assert.IsFalse(TypeHelper.IsIntegral(typeof(double?)));
        }

        [Test]
        public void IsEnumerationOfGivenTypes_PositiveTest()
        {
            Assert.IsTrue(TypeHelper.IsEnumerationOfGivenTypes(typeof(List<long>), typeof(long)));
            Assert.IsTrue(TypeHelper.IsEnumerationOfGivenTypes(typeof(IList<long>), typeof(long), typeof(int)));
            Assert.IsTrue(TypeHelper.IsEnumerationOfGivenTypes(typeof(long[]), typeof(long), typeof(int)));
        }

        [Test]
        public void IsEnumerationOfGivenTypes_NegativeTest()
        {
            Assert.IsFalse(TypeHelper.IsEnumerationOfGivenTypes(typeof(List<string>), typeof(long)));
            Assert.IsFalse(TypeHelper.IsEnumerationOfGivenTypes(typeof(string), typeof(string)));
            Assert.IsFalse(TypeHelper.IsEnumerationOfGivenTypes(typeof(List<string>)));
        }

        [Test]
        public void IsFlaggedEnum_For_Flagged_Enum()
        {
            Assert.IsTrue(TypeHelper.IsFlaggedEnum(typeof(FlaggedTestEnum)));
        }

        [Test]
        public void IsFlaggedEnum_For_NotFlagged_Enum()
        {
            Assert.IsFalse(TypeHelper.IsFlaggedEnum(typeof(TestNotFlaggedEnum)));
        }

        [Test]
        public void IsFlaggedEnum_For_object()
        {
            Assert.IsFalse(TypeHelper.IsFlaggedEnum(typeof(object)));
        }

        [Test]
        public void IsMethodOverridden_ShouldReturnTrueForMethosdOverriddenInTheGivenType()
        {
            var classWithOverriddenMethodsType = typeof(ClassWithOverriddenMethods);

            TypeHelper.IsMethodOverridden(nameof(ToString), classWithOverriddenMethodsType)
                .Should().BeTrue("method overrides ToString of the Object class");
            TypeHelper.IsMethodOverridden(nameof(ClassWithOverriddenMethods.VirtualMethodToOverrideInTheMiddleClass), classWithOverriddenMethodsType)
                .Should().BeTrue("method overrides virtual method of the middle class");
            TypeHelper.IsMethodOverridden(nameof(ClassWithOverriddenMethods.AbstractMethodOverriddenInTheMiddleClass), classWithOverriddenMethodsType)
                .Should().BeTrue("method overrides abstract method of the middle class");
            TypeHelper.IsMethodOverridden(nameof(ClassWithOverriddenMethods.PublicMethodOverriddenInTheDerivedClass), classWithOverriddenMethodsType)
                .Should().BeTrue("method overrides public method in the middle class");
            TypeHelper.IsMethodOverridden("ProtectedMethodOverriddenInTheDerivedClass", classWithOverriddenMethodsType)
                .Should().BeTrue("method overrides protected method");
            TypeHelper.IsMethodOverridden("ProtectedMethodOverriddenInTheMiddleClass", classWithOverriddenMethodsType)
                .Should().BeTrue("method overrides protected method in the middle class");

            TypeHelper.IsMethodOverridden(nameof(BaseClassWithMethods.AbstractMethodNotOverriddenInTheMiddleClass), typeof(BaseClassWithMethods))
                .Should().BeTrue("method overrides abstract method of the base class");
        }

        [Test]
        public void IsMethodOverridden_ShouldReturnTrueForMethosdOverriddenInBaseType()
        {
            var classWithNotOverriddenMethodsType = typeof(ClassWithNotOverriddenMethods);
            TypeHelper.IsMethodOverridden(nameof(ToString), classWithNotOverriddenMethodsType)
                .Should().BeTrue("ToString method of the Object class is overridden in the middle class");
            TypeHelper.IsMethodOverridden(nameof(ClassWithNotOverriddenMethods.VirtualMethodToOverrideInTheMiddleClass), classWithNotOverriddenMethodsType)
                .Should().BeTrue("method is overridden in the middle class");
            TypeHelper.IsMethodOverridden(nameof(ClassWithNotOverriddenMethods.AbstractMethodOverriddenInTheMiddleClass), classWithNotOverriddenMethodsType)
                .Should().BeTrue("abstract method is overridden in the middle class");
            TypeHelper.IsMethodOverridden(nameof(BaseClassWithMethods.AbstractMethodNotOverriddenInTheMiddleClass), classWithNotOverriddenMethodsType)
                .Should().BeTrue("abstract method is overridden in the base class");
        }

        [Test]
        public void IsMethodOverridden_ShouldReturnFalseForNotOverriddenMethods()
        {
            TypeHelper.IsMethodOverridden("GetAddRecordEvents", typeof(ClassWithNoBaseClassAndVirtualMethodMethods<>))
                .Should().BeFalse("GetAddRecordEvents method is declared only in this class");
            TypeHelper.IsMethodOverridden("GetAddRecordEvents", typeof(ClassWithBaseClassAndMethowWithIsNotOverridden<>))
                .Should().BeFalse("GetAddRecordEvents method of the base class is not overridden");
            TypeHelper.IsMethodOverridden(nameof(ToString), typeof(ClassWithNoBaseClassAndNoOverriddenMethods))
                .Should().BeFalse("ToString method of the Object class is not overridden in base class");
            TypeHelper.IsMethodOverridden(nameof(ToString), typeof(ClassWithBaseClassAndNoOverriddenMethods))
                .Should().BeFalse("ToString method of the Object class is not overridden in derived class");
            
            TypeHelper.IsMethodOverridden(nameof(ClassWithNotOverriddenMethods.MethodToOverrideWithNewInTheMiddleClass), typeof(ClassWithNotOverriddenMethods))
                .Should().BeFalse("method doesn't override method of the middle class (it defines new method with new key word)");
        }

        [Test]
        public void IsMethodOverridden_ShouldReturnFalseForNotExistingMethods()
        {
            TypeHelper.IsMethodOverridden("NotExistingMethod", typeof(ClassWithNotOverriddenMethods))
                .Should().BeFalse("method doesn't exist");
        }

        [Test]
        public void IsMethodOverridden_ShouldThrowExceptionIfAnyOfArgumentsIsNull()
        {
            Action isMethodOverridden = () => { TypeHelper.IsMethodOverridden(null, typeof(ClassWithOverriddenMethods)); };
            isMethodOverridden.ShouldThrow<ArgumentNullException>("method name is null");

            isMethodOverridden = () => { TypeHelper.IsMethodOverridden(nameof(ToString), null); };
            isMethodOverridden.ShouldThrow<ArgumentNullException>("type is null");
        }

        public class ClassWithOverriddenMethods : MiddleClassWithMethods
        {
            public override void VirtualMethodToOverrideInTheMiddleClass()
            {
            }

            public override void AbstractMethodOverriddenInTheMiddleClass()
            {
            }

            public new void MethodToOverrideWithNewInTheMiddleClass()
            { }

            public override string ToString()
            {
                return string.Empty;
            }

            public override void PublicMethodOverriddenInTheDerivedClass()
            {
            }

            protected override void ProtectedMethodOverriddenInTheDerivedClass()
            {
            }
        }

        public class ClassWithNotOverriddenMethods : MiddleClassWithMethods
        {
        }

        public class MiddleClassWithMethods : BaseClassWithMethods
        {
            public override string ToString()
            {
                return string.Empty;
            }

            public override void VirtualMethodToOverrideInTheMiddleClass()
            {
            }

            public virtual void PublicMethodOverriddenInTheDerivedClass()
            {
            }

            public override void AbstractMethodOverriddenInTheMiddleClass()
            {
            }

            protected virtual void ProtectedMethodOverriddenInTheDerivedClass()
            {
            }

            protected override void ProtectedMethodOverriddenInTheMiddleClass()
            {
            }
        }

        public class BaseClassWithMethods : AbstractBaseClassWithMethods
        {
            public new void MethodToOverrideWithNewInTheMiddleClass()
            {
            }

            public override void AbstractMethodOverriddenInTheMiddleClass()
            {
            }

            public override void AbstractMethodNotOverriddenInTheMiddleClass()
            {
            }

            protected virtual void ProtectedMethodOverriddenInTheMiddleClass()
            {
            }
        }

        public abstract class AbstractBaseClassWithMethods
        {
            public virtual void VirtualMethodToOverrideInTheMiddleClass()
            { }
            public void MethodToOverrideWithNewInTheMiddleClass()
            { }

            public abstract void AbstractMethodOverriddenInTheMiddleClass();

            public abstract void AbstractMethodNotOverriddenInTheMiddleClass();
        }

        public class ClassWithNoBaseClassAndNoOverriddenMethods
        {
        }

        public class ClassWithBaseClassAndNoOverriddenMethods : ClassWithNoBaseClassAndNoOverriddenMethods
        {
        }

        public class ClassWithNoBaseClassAndVirtualMethodMethods<TDto>
        {
            protected virtual IEnumerable<BusinessEvent> GetAddRecordEvents(AddRecordResult result, TDto record)
            {
                yield break;
            }

            public class BusinessEvent
            {
            }

            public class AddRecordResult
            {
            }
        }

        public class ClassWithBaseClassAndMethowWithIsNotOverridden<TDto> : ClassWithNoBaseClassAndVirtualMethodMethods<TDto>
        {
        }
    }
}
