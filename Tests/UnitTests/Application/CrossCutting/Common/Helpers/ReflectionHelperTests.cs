﻿using System;
using Castle.MicroKernel.Registration;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Tests.Common;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public partial class ReflectionHelperTests
    {
        [Test]
        public void FindNamespaceInLoadedAssemblies_PositiveTest()
        {
            var searchingForNamespace = typeof(ReflectionHelperTests).Namespace;
            Assert.IsTrue(ReflectionHelper.FindNamespaceInLoadedAssemblies(searchingForNamespace));
        }

        [Test]
        public void FindNamespaceInLoadedAssemblies_NegativeTest()
        {
            var searchingForNamespace = Guid.NewGuid().ToString();
            Assert.IsFalse(ReflectionHelper.FindNamespaceInLoadedAssemblies(searchingForNamespace));
        }

        [Test]
        public void CreateInstance_CanCreateInstanceWithParameterlessConstructorNonGeneric()
        {
            var instance = ReflectionHelper.CreateInstance(GetType());

            Assert.IsNotNull(instance);
            Assert.AreSame(GetType(), instance.GetType());
        }

        [Test]
        public void CreateInstance_CanCreateInstanceWithParametrizedConstructorNonGeneric()
        {
            var expectedType = typeof(SampleClassWithParameterizedConstructor);
            var instance = ReflectionHelper.CreateInstance(expectedType, 123);
            var instance2 = ReflectionHelper.CreateInstance(expectedType, "right", 123, 0.5);

            Assert.IsNotNull(instance);
            Assert.AreSame(expectedType, instance.GetType());

            Assert.IsNotNull(instance2);
            Assert.AreSame(expectedType, instance2.GetType());
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void CreateInstance_CannotCreateInstanceWithWrongConstructorParametersNonGeneric()
        {
            var expectedType = typeof(SampleClassWithParameterizedConstructor);
            var instance = ReflectionHelper.CreateInstance(expectedType, 123, "wrong");

            Assert.IsNotNull(instance);
            Assert.AreSame(expectedType, instance.GetType());
        }

        [Test]
        public void CreateInstance_CanCreateInstanceWithParametrizedConstructorNullableParameterNonGeneric()
        {
            var expectedType = typeof(SampleClassWithParameterizedConstructor);
            var instance = ReflectionHelper.CreateInstance(expectedType, "right", 123, null);
            var instance2 = ReflectionHelper.CreateInstance(expectedType, null, "null");

            Assert.IsNotNull(instance);
            Assert.AreSame(expectedType, instance.GetType());

            Assert.IsNotNull(instance2);
            Assert.AreSame(expectedType, instance2.GetType());
        }

        [Test]
        public void CreateInstance_CanCreateInstanceWithParametrizedConstructorDefaultParameterNonGeneric()
        {
            var expectedType = typeof(SampleClassWithParameterizedConstructor);
            var instance = ReflectionHelper.CreateInstance(expectedType, "right", 123);
            var instance2 = ReflectionHelper.CreateInstance(expectedType, "left");
            var instance3 = ReflectionHelper.CreateInstance(expectedType);

            Assert.IsNotNull(instance);
            Assert.AreSame(expectedType, instance.GetType());

            Assert.IsNotNull(instance2);
            Assert.AreSame(expectedType, instance2.GetType());

            var sample = (SampleClassWithParameterizedConstructor) instance;
            Assert.AreEqual(SampleClassWithParameterizedConstructor.DefaultRatio, sample.Ratio);

            var sample2 = (SampleClassWithParameterizedConstructor)instance2;
            Assert.AreEqual(SampleClassWithParameterizedConstructor.DefaultLabel, sample2.Label);

            var sample3 = (SampleClassWithParameterizedConstructor)instance3;
            Assert.AreEqual(SampleClassWithParameterizedConstructor.DefaultInput, sample3.Input);
            Assert.AreEqual(SampleClassWithParameterizedConstructor.DefaultRatio, sample3.Ratio);
            Assert.IsNull(sample3.Text);
        }

        [Test]
        public void CreateInstance_CanCreateInstanceWithParameterlessConstructorGeneric()
        {
            var instance = ReflectionHelper.CreateInstance<ReflectionHelperTests>();

            Assert.IsNotNull(instance);
            Assert.AreSame(GetType(), instance.GetType());
        }

        [Test]
        public void CreateInstance_CanCreateInstanceWithParametrizedConstructorGeneric()
        {
            var expectedType = typeof(SampleClassWithParameterizedConstructor);
            var instance = ReflectionHelper.CreateInstance<SampleClassWithParameterizedConstructor>(123);
            var instance2 = ReflectionHelper.CreateInstance<SampleClassWithParameterizedConstructor>("right", 123, 0.5);

            Assert.IsNotNull(instance);
            Assert.AreSame(expectedType, instance.GetType());

            Assert.IsNotNull(instance2);
            Assert.AreSame(expectedType, instance2.GetType());
        }

        [Test]
        public void CreateInstance_CanCreateInstanceWithParametrizedConstructorNullableParameterGeneric()
        {
            var expectedType = typeof(SampleClassWithParameterizedConstructor);
            var instance = ReflectionHelper.CreateInstance<SampleClassWithParameterizedConstructor>("right", 123, null);
            var instance2 = ReflectionHelper.CreateInstance<SampleClassWithParameterizedConstructor>(expectedType, null, "null");

            Assert.IsNotNull(instance);
            Assert.AreSame(expectedType, instance.GetType());

            Assert.IsNotNull(instance2);
            Assert.AreSame(expectedType, instance2.GetType());
        }

        [Test]
        public void CreateInstance_CanCreateInstanceWithParametrizedConstructorDefaultParameterGeneric()
        {
            var expectedType = typeof(SampleClassWithParameterizedConstructor);
            var instance = ReflectionHelper.CreateInstance<SampleClassWithParameterizedConstructor>("right", 123);
            var instance2 = ReflectionHelper.CreateInstance<SampleClassWithParameterizedConstructor>("left");
            var instance3 = ReflectionHelper.CreateInstance<SampleClassWithParameterizedConstructor>();

            Assert.IsNotNull(instance);
            Assert.AreSame(expectedType, instance.GetType());

            Assert.AreEqual(SampleClassWithParameterizedConstructor.DefaultRatio, instance.Ratio);
            Assert.AreEqual(SampleClassWithParameterizedConstructor.DefaultLabel, instance2.Label);

            Assert.AreEqual(SampleClassWithParameterizedConstructor.DefaultInput, instance3.Input);
            Assert.AreEqual(SampleClassWithParameterizedConstructor.DefaultRatio, instance3.Ratio);
            Assert.IsNull(instance3.Text);

        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void CreateInstance_CannotCreateInstanceWithWrongConstructorParametersGeneric()
        {
            var expectedType = typeof(SampleClassWithParameterizedConstructor);
            var instance = ReflectionHelper.CreateInstance<SampleClassWithParameterizedConstructor>(123, "wrong");

            Assert.IsNotNull(instance);
            Assert.AreSame(expectedType, instance.GetType());
        }

        [Test]
        public void CreateInstanceWithIocDependencies_TestPositive()
        {
            const string testingValue = "testname";

            var testConfiguration = new TestContainerConfiguration();
            testConfiguration.Configure(ContainerType.UnitTests);

            ContainerConfiguration.Container.Register(Component.For<ITestInterface1>().ImplementedBy<TestInterface1>().LifestyleTransient());
            ContainerConfiguration.Container.Register(Component.For<ITestInterface2>().ImplementedBy<TestInterface2>().LifestyleTransient());

            var testInterface1Impl = new TestInterface1(testingValue);

            var testClassInstance = ReflectionHelper.CreateInstanceWithIocDependencies<TestClassWithParametersInConstuctor>(new {testInterface1 = testInterface1Impl});

            Assert.IsNotNull(testClassInstance.TestInterface1);
            Assert.IsNotNull(testClassInstance.TestInterface2);
            Assert.AreEqual(testingValue, testClassInstance.TestInterface1.Name);
        }
    }
}
