using System;
using System.Collections.Generic;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    public partial class ReflectionHelperTests
    {
        private class TestClass1
        {
            
        }

        private class TestClass2 : TestClass1
        {
            
        }

        private class TestClassSubTypeOfGeneric<T> : List<T>
        {
            
        }

        private interface ITestInterface1
        {
            string Name { get; }
        }

        private interface ITestInterface2
        {
            
        }

        private class TestInterface1 : ITestInterface1
        {
            public TestInterface1()
            {
                Name = Guid.NewGuid().ToString();
            }

            public TestInterface1(string name)
            {
                Name = name;
            }

            public string Name { get; private set; }
        }

        private class TestInterface2 : ITestInterface2
        {
            
        }

        private class TestClassWithParametersInConstuctor
        {
            public ITestInterface1 TestInterface1 { get; set; }
            public ITestInterface2 TestInterface2 { get; set; }

            public TestClassWithParametersInConstuctor(ITestInterface1 testInterface1, ITestInterface2 testInterface2)
            {
                TestInterface1 = testInterface1;
                TestInterface2 = testInterface2;
            }
        }

        public class SampleClassWithParameterizedConstructor
        {
            public const int DefaultInput = 0;
            public const string DefaultLabel = "Label";
            public const double DefaultRatio = 0.123;

            public int Input { get; set; }
            public string Text { get; set; }
            public string Label { get; set; }
            public double? Ratio { get; set; }

            public SampleClassWithParameterizedConstructor(int input)
            {
                Input = input;
            }

            public SampleClassWithParameterizedConstructor(string text, int input, double? ratio)
            {
                Input = input;
                Text = text;
                Ratio = ratio;
            }

            public SampleClassWithParameterizedConstructor(string text, int input, double ratio = DefaultRatio)
            {
                Input = input;
                Text = text;
                Ratio = ratio;
            }

            public SampleClassWithParameterizedConstructor(string text, string label = DefaultLabel)
            {
                Text = text;
                Label = label;
            }

            public SampleClassWithParameterizedConstructor(int input = DefaultInput, double ratio = DefaultRatio)
            {
                Input = input;
                Ratio = ratio;
            }
        }
    }
}