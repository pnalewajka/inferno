﻿using System;
using System.Linq;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public partial class AttributeHelperTests
    {
        [Test, ExpectedException(typeof (InvalidOperationException))]
        public void GetClassAttribute_ExceptionAfterGettingOneAttributeValueForMultipleOccuringAttribute()
        {
            var instance = new TestClass2();
            AttributeHelper.GetClassAttribute<InternalMultipleTestAttribute>(instance);
        }

        [Test, ExpectedException(typeof(InvalidOperationException))]
        public void GetClassAttribute_ExceptionAfterGettingOneAttributeValueForMultipleOccuringAttributeFromType()
        {
            var type = typeof(TestClass2);
            AttributeHelper.GetClassAttribute<InternalMultipleTestAttribute>(type);
        }

        [Test]
        public void GetClassAttribute_TestAttributeExtractionWithInheritance()
        {
            var instance = new TestClass2();
            var attribute = AttributeHelper.GetClassAttribute<InternalTestAttribute>(instance);
            Assert.IsNotNull(attribute);
            Assert.AreSame(attribute.GetType(), typeof (InternalTestAttribute));
            Assert.AreEqual(AttributeTestValue, attribute.Value);
        }

        [Test]
        public void GetClassAttribute_ShouldReturnSpecificAttributeFromType()
        {
            var type = typeof(TestClass2);
            var attribute = AttributeHelper.GetClassAttribute<InternalTestAttribute>(type);
            Assert.IsNotNull(attribute);
            Assert.AreSame(attribute.GetType(), typeof(InternalTestAttribute));
            Assert.AreEqual(AttributeTestValue, attribute.Value);
        }

        [Test]
        public void GetClassAttributes_TestAttributesExtractionWithInheritance()
        {
            var instance = new TestClass2();
            var attributes = AttributeHelper.GetClassAttributes<InternalMultipleTestAttribute>(instance);
            const int expectedAttributesCount = 3;
            Assert.AreEqual(expectedAttributesCount, attributes.Count());
        }

        [Test]
        public void GetClassAttributes_ShouldReturnAllClassAttributesFromType()
        {
            var attributes = AttributeHelper.GetClassAttributes<InternalMultipleTestAttribute>(typeof(TestClass2));
            Assert.AreEqual(3, attributes.Count());
        }

        [Test]
        public void GetEnumFieldAttribute_TestAttributeExtraction()
        {
            var attribute = AttributeHelper.GetEnumFieldAttribute<InternalEnumAttribute>(TestEnum.Option1);
            Assert.IsNotNull(attribute);
            Assert.AreEqual(AttributeTestValue, attribute.Value);
        }

        [Test]
        public void GetPropertyAttribute_TestAttributeExtraction()
        {
            var propertyInfo = PropertyHelper.GetProperty<TestClass2>(r => r.Property1);
            var attribute = AttributeHelper.GetPropertyAttribute<InternalTestPropertyAttribute>(propertyInfo);
            Assert.IsNotNull(attribute);
            Assert.AreEqual(AttributeTestValue, attribute.Value);
        }
    }
}