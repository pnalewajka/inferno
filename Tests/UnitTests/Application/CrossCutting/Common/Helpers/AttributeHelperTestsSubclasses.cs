﻿using System;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    public partial class AttributeHelperTests
    {
        private const int AttributeTestValue = 1;

        [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
        private class InternalTestAttribute : Attribute
        {
            public InternalTestAttribute(int value)
            {
                Value = value;
            }

            public int Value { get; private set; }
        }

        [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
        private class InternalMultipleTestAttribute : Attribute
        {
            public InternalMultipleTestAttribute(int value)
            {
                Value = value;
            }

            private int Value { get; set; }
        }

        [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
        private class InternalTestPropertyAttribute : Attribute
        {
            public InternalTestPropertyAttribute(int value)
            {
                Value = value;
            }

            public int Value { get; private set; }
        }

        [InternalTest(AttributeTestValue)]
        [InternalMultipleTestAttribute(AttributeTestValue)]
        [InternalMultipleTestAttribute(AttributeTestValue)]
        private class TestClass1
        {
            [InternalTestPropertyAttribute(AttributeTestValue)]
            public virtual int Property1 { get; set; }
        }

        [InternalMultipleTestAttribute(AttributeTestValue)]
        private class TestClass2 : TestClass1
        {
        }


        [AttributeUsage(AttributeTargets.Field)]
        private class InternalEnumAttribute : Attribute
        {
            public InternalEnumAttribute(int value)
            {
                Value = value;
            }

            public int Value { get; private set; }
        }

        private enum TestEnum
        {
            [InternalEnum(AttributeTestValue)]
            Option1,
        }

    }
}
