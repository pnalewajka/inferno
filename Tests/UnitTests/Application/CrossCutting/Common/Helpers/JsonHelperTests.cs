using System;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public class JsonHelperTests
    {
        [Test]
        public void Serialize_ShouldSerializeStringToQuotedString()
        {
            object value = "Some string";

            var result = JsonHelper.Serialize(value);

            Assert.AreEqual("\"Some string\"", result);
        }

        [Test]
        public void Deserialize_ShouldDeserializeQuotedStringToString()
        {
            var result = JsonHelper.Deserialize("\"Some string\"");

            Assert.AreEqual("Some string", result);
        }

        [Test]
        public void Serialize_ShouldSerializeIntToNumericString()
        {
            object value = 1024;

            var result = JsonHelper.Serialize(value);

            Assert.AreEqual("1024", result);
        }

        [Test]
        public void Deserialize_ShouldDeserializeNumericStringToInt()
        {
            var result = JsonHelper.Deserialize("1024");

            Assert.AreEqual(1024, result);
        }

        [Test]
        public void Serialize_ShouldSerializeBoolToLogicalString()
        {
            object value = true;

            var result = JsonHelper.Serialize(value);

            Assert.AreEqual("true", result);
        }

        [Test]
        public void Deserialize_ShouldDeserializeLogicalStringToBool()
        {
            var result = JsonHelper.Deserialize("true");

            Assert.AreEqual(true, result);
        }

        [Test]
        public void Serialize_ShouldSerializeDateTimeToQuotedSortableDateString()
        {
            object value = new DateTime(2015, 01, 28, 7, 47, 35);

            var result = JsonHelper.Serialize(value);

            Assert.AreEqual("\"2015-01-28T07:47:35\"", result);
        }

        [Test]
        public void Deserialize_ShouldDeserializeQuotedSortableDateStringToDateTime()
        {
            object value = new DateTime(2015, 01, 28, 7, 47, 35);

            var result = JsonHelper.Deserialize("\"2015-01-28T07:47:35\"");

            Assert.AreEqual(value, result);
        }

        [Test]
        public void Serialize_ShouldSerializeNullToNullString()
        {
            object value = null;

            // ReSharper disable ExpressionIsAlwaysNull
            var result = JsonHelper.Serialize(value);
            // ReSharper restore ExpressionIsAlwaysNull

            Assert.AreEqual("null", result);
        }

        [Test]
        public void Deserialize_ShouldDeserializeNullStringToNull()
        {
            var result = JsonHelper.Deserialize("null");

            Assert.AreEqual(null, result);
        }

        [Test]
        public void Serialize_ShouldSerializeArrayToBracketedCommaSeparatedList()
        {
            object value = new[] { 0, 1, 1, 2, 3, 5, 8, 13, 21 };

            var result = JsonHelper.Serialize(value);

            Assert.AreEqual("[0,1,1,2,3,5,8,13,21]", result);
        }

        [Test]
        public void Deserialize_ShouldDeserializeBracketedCommaSeparatedListToArray()
        {
            object value = new[] { 0, 1, 1, 2, 3, 5, 8, 13, 21 };

            var result = JsonHelper.Deserialize("[0,1,1,2,3,5,8,13,21]", typeof(int[]));

            Assert.AreEqual(value, result);
        }

        private class RollingStone
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
        }

        [Test]
        public void Serialize_ShouldSerializeObjectToProperJson()
        {
            object value = new RollingStone { FirstName = "Mick", LastName = "Jagger" };

            var result = JsonHelper.Serialize(value);

            Assert.AreEqual("{\"FirstName\":\"Mick\",\"LastName\":\"Jagger\"}", result);
        }

        [Test]
        public void Deserialize_ShouldDeserializeProperJsonToObject()
        {
            var value = new RollingStone { FirstName = "Mick", LastName = "Jagger" };

            var result = (RollingStone)JsonHelper.Deserialize("{\"FirstName\":\"Mick\",\"LastName\":\"Jagger\"}", typeof(RollingStone));

            Assert.AreEqual(value.FirstName, result.FirstName);
            Assert.AreEqual(value.LastName, result.LastName);
        }

        [Test]
        public void Deserialize_ShouldDeserializeWithTemplateProperJsonToObject()
        {
            var value = new RollingStone { FirstName = "Mick", LastName = "Jagger" };

            var result = JsonHelper.Deserialize<RollingStone>("{\"FirstName\":\"Mick\",\"LastName\":\"Jagger\"}");
            
            Assert.AreEqual(value.FirstName, result.FirstName);
            Assert.AreEqual(value.LastName, result.LastName);
        }
    }
}