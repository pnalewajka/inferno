﻿using System.Collections.Generic;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    public partial class TypeHelperTests
    {
        private class TestClass1
        {

        }

        private class TestClass2 : TestClass1
        {

        }

        private class TestClassSubTypeOfGeneric<T> : List<T>
        {

        }

        private class TestClass3 
        {
            private void TestMethod1()
            {
                
            }

            private void TestMethod2()
            {

            }

        }
    }
}
