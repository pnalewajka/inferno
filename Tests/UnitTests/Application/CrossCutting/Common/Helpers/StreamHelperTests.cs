﻿using System;
using System.IO;
using System.Xml.Linq;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public class StreamHelperTests
    {
        public class TestClass1
        {
            public string Value { get; set; }
        }

        [Test]
        public void XDocumentToStream_TestPositive()
        {
            var testValue = Guid.NewGuid().ToString();
            var xElement = XmlSerializationHelper.SerializeToXElement(new TestClass1 {Value = testValue});
            var xDocument = new XDocument();
            xDocument.Add(xElement);
            using(var reader = new StreamReader(StreamHelper.XDocumentToStream(xDocument)))
            {
                var doc = reader.ReadToEnd();
                Assert.IsNotNullOrEmpty(doc);
                Assert.IsTrue(doc.Contains(testValue));
            }
        }
    }
}
