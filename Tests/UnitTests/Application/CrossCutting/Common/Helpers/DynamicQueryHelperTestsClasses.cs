﻿using System.Collections.Generic;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    public class DynamicQueryHelperTestsClasses
    {
        public class DynamicQueryEntityMock
        {
            public IEnumerable<DynamicQueryEntityValueMock> Inner { get; set; }
        }

        public class DynamicQueryEntityValueMock
        {
            public IEnumerable<string> Value { get; set; }
        }
    }
}