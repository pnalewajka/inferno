using System;
using System.Collections;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    public class ResourceHelperTestCasesFactory
    {
        internal const string ResourceFileContents = "test";

        public static IEnumerable GetResourceGetStreamByFullNameTestCases
        {
            get
            {
                var existingResourceName =
                    $"{typeof(ResourceHelperTestCasesFactory).Namespace}.Resources.Resource_01.txt";
                var nonExistingResourceName = Guid.NewGuid().ToString();
                yield return
                    new TestCaseData(existingResourceName, null).Returns(ResourceFileContents)
                        .SetDescription("Failed to read expected value from assembly resource");
                yield return
                    new TestCaseData(existingResourceName, typeof(ResourceHelperTestCasesFactory).Assembly).Returns(ResourceFileContents)
                        .SetDescription("Failed to read expected value from assembly resource");
                yield return
                    new TestCaseData(existingResourceName, typeof(ResourceHelper).Assembly).Returns(ResourceFileContents)
                        .Throws(typeof(ResourceNotFoundException))
                        .SetDescription("System should've thrown exception here");
                yield return
                    new TestCaseData(nonExistingResourceName, typeof(ResourceHelperTestCasesFactory).Assembly).Returns(ResourceFileContents)
                        .Throws(typeof(ResourceNotFoundException))
                        .SetDescription("System should've thrown exception here");
            }
        }
    }
}