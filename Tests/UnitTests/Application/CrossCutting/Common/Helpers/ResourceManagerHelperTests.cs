﻿using System;
using System.Globalization;
using System.Resources;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;
using UnitTests.Application.CrossCutting.Common.Helpers.Resources;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public class ResourceManagerExtensionsTests
    {
        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void GetAllStrings_ShouldThrowArgumentNullExceptionForNullResourceManager()
        {
            var cultureInfo = CultureInfo.CurrentCulture;

// ReSharper disable ExpressionIsAlwaysNull
            ResourceManagerHelper.GetAllStrings(null, cultureInfo);
// ReSharper restore ExpressionIsAlwaysNull
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetAllStrings_ShouldThrowArgumentNullExceptionForNullCulture()
        {
            CultureInfo cultureInfo = null;

            // ReSharper disable ExpressionIsAlwaysNull
            ResourceManagerHelper.GetAllStrings(typeof(SampleResources), cultureInfo);
            // ReSharper restore ExpressionIsAlwaysNull
        }

        [Test]
        public void GetAllStrings_ShouldReturnAllStringsWithNoRegex()
        {
            var cultureInfo = CultureInfo.CurrentCulture;

            var strings = ResourceManagerHelper.GetAllStrings(typeof(SampleResources), cultureInfo);

            Assert.AreEqual(4, strings.Count);
        }


        [Test]
        public void GetAllStrings_ShouldReturnAllStringsWithCatchAllRegex()
        {
            var cultureInfo = CultureInfo.CurrentCulture;

            var strings = ResourceManagerHelper.GetAllStrings(typeof(SampleResources), cultureInfo, ".*");

            Assert.AreEqual(4, strings.Count);
        }

        [Test]
        public void GetAllStrings_ShouldReturnRightNumberOfStringsWithGivenRegex()
        {
            var cultureInfo = CultureInfo.CurrentCulture;

            var strings = ResourceManagerHelper.GetAllStrings(typeof(SampleResources), cultureInfo, ".*Little.*");

            Assert.AreEqual(2, strings.Count);
        }

        [Test]
        public void GetAllStrings_ShouldReturnNoStringsWithNoMatchRegex()
        {
            var cultureInfo = CultureInfo.CurrentCulture;

            var strings = ResourceManagerHelper.GetAllStrings(typeof(SampleResources), cultureInfo, ".*SomethingWeird.*");

            Assert.AreEqual(0, strings.Count);
        }

        [Test]
        public void GetAllStrings_ShouldReturnCultureSpecificStringsForExistingLocalization()
        {
            var cultureInfo = new CultureInfo("pl-PL");

            var strings = ResourceManagerHelper.GetAllStrings(typeof(SampleResources), cultureInfo);

            Assert.AreEqual(4, strings.Count);

            const string littleLateKey = "LittleLate";
            const string littleLateInPolish = "nieco późno";

            Assert.IsTrue(strings.ContainsKey(littleLateKey));
            Assert.AreEqual(littleLateInPolish, strings[littleLateKey]);
        }

        [Test]
        public void GetAllStrings_ShouldReturnDefaultCultureStringsForMissingLocalization()
        {
            var cultureInfo = new CultureInfo("es-ES");

            var strings = ResourceManagerHelper.GetAllStrings(typeof(SampleResources), cultureInfo);

            Assert.AreEqual(4, strings.Count);

            const string littleLateKey = "LittleLate";
            const string littleLateInPolish = "little late";

            Assert.IsTrue(strings.ContainsKey(littleLateKey));
            Assert.AreEqual(littleLateInPolish, strings[littleLateKey]);
        }

        [Test]
        public void GetResourceManager_ShouldReturnProperValueForValidResourcesClassGeneric()
        {
            var resourceManager = ResourceManagerHelper.GetResourceManager<SampleResources>();
            Assert.IsNotNull(resourceManager);
        }

        [Test]
        public void GetResourceManager_ShouldReturnProperValueForValidResourcesClassByType()
        {
            var resourceManager = ResourceManagerHelper.GetResourceManager(typeof(SampleResources));
            Assert.IsNotNull(resourceManager);
        }
    }
}