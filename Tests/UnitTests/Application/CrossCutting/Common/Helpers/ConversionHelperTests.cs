﻿using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public class ConversionHelperTests
    {
        [Test]
        public void ConvertTo36Base_RoundTripTest()
        {
            const long value = 123456;
            var convertedString = ConversionHelper.ConvertTo36Base(value);
            var newValue = ConversionHelper.ConvertFrom36Base(convertedString);
            Assert.AreEqual(value, newValue);
        }

        [Test]
        public void ConvertFileSizeToHumanReadableString_TestPositive()
        {
            const int value = 123456;
            const string expectedValue = "120 KB";
            Assert.AreEqual(expectedValue, ConversionHelper.ConvertFileSizeToHumanReadableString(value));
        }
    }
}
