﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    public partial class TypeHelperTests
    {
        private enum TestNotFlaggedEnum
        {
            Value
        }

        [Flags]
        private enum FlaggedTestEnum
        {
            Value
        }
    }
}
