﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Services;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public class ClassMappingTests
    {
        [MappingsType(typeof(SourceToDestinationMapping))]
        public class Source
        {
            public string A { get; set; }
            public string B { get; set; }
        }

        public class Destination
        {
            public string A { get; set; }
            public string B { get; set; }
        }

        public class SourceToDestinationMapping : ClassMapping<Source, Destination>
        {
            public SourceToDestinationMapping()
            {
                Mapping = s => new Destination
                {
                    A = s.A,
                    B = s.B
                };
            }
        }

        [Test]
        public void MappingsTypeAttribute_MappingShouldBeFound()
        {
            var container = new WindsorContainer();
            container.Register(Component.For<IClassMapping<Source, Destination>>().ImplementedBy<SourceToDestinationMapping>());
            var classMappingFactory = new ClassMappingFactory(container.Kernel);
            var mapping = classMappingFactory.CreateMapping<Source, Destination>();

            Assert.NotNull(mapping);
            Assert.AreEqual(mapping.GetType(), typeof(SourceToDestinationMapping));

            var source = new Source {A = "a", B = "B"};
            var destination = mapping.CreateFromSource(source);

            Assert.AreEqual(destination.A, source.A);
            Assert.AreEqual(destination.B, source.B);
        }
    }
}