﻿using System;
using System.Collections.Generic;
using System.Globalization;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using System.Reflection;
using System.Linq;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public partial class DictionaryHelperTests
    {
        [Test]
        public void ToDictionary_TestPositive()
        {
            const long longValue = 1;
            const string stringValue = @"test";

            var arrayOfLongsValue = new[] { 1L, 2, 3 };

            var testObject = new TestPoco
            {
                LongValue = longValue,
                StringValue = stringValue,
                ArrayOfLongsValue = arrayOfLongsValue
            };

            var dictionaryOfProperties = DictionaryHelper.ToDictionary(testObject, p => p, v => v);

            Assert.AreEqual(longValue, dictionaryOfProperties[PropertyHelper.GetPropertyName<TestPoco>(t => t.LongValue)]);
            Assert.AreEqual(null, dictionaryOfProperties[PropertyHelper.GetPropertyName<TestPoco>(t => t.NullableLongValue)]);
            Assert.AreEqual(arrayOfLongsValue, dictionaryOfProperties[PropertyHelper.GetPropertyName<TestPoco>(t => t.ArrayOfLongsValue)]);
        }

        [Test]
        public void ToDictionary_ShouldBuildDictionaryBasedOnPassedFunctionsToCreateKeyAndValue()
        {
            var testObject = new TestStringsPoco
            {
                StringValue = "test1",
                AnotherStringValue = "test2"
            };

            Func<string, string> mockKeyFunction = x =>
            {
                return x;
            };

            Func<PropertyInfo, string, TestResultPoco> mockValueFunction = (p, x) =>
            {
                return new TestResultPoco(p, x);
            };

            var dictionaryOfProperties = DictionaryHelper.ToDictionary(testObject, mockKeyFunction, mockValueFunction);

            Assert.AreEqual(2, dictionaryOfProperties.Count);
            Assert.AreEqual("StringValue", dictionaryOfProperties.Keys.First());
            Assert.AreEqual("AnotherStringValue", dictionaryOfProperties.Keys.Last());
            Assert.AreEqual("StringValue", dictionaryOfProperties["StringValue"].PropertyInfo.Name);
            Assert.AreEqual("AnotherStringValue", dictionaryOfProperties["AnotherStringValue"].PropertyInfo.Name);
            Assert.AreEqual("test1", dictionaryOfProperties["StringValue"].Value);
            Assert.AreEqual("test2", dictionaryOfProperties["AnotherStringValue"].Value);
        }

        public void ToDictionary_ShouldReturnNullWhenPassedObjectIsNull()
        {
            TestPoco testObject = null;

            var dictionaryOfProperties = DictionaryHelper.ToDictionary(testObject, x => x, v => v);

            Assert.IsNull(dictionaryOfProperties);
        }

        [Test]
        public void EndToEndTest()
        {
            const long longValue = 1;
            const string stringValue = @"test";

            var dateValue = DateTime.Now;
            var arrayOfLongsValue = new[] { 1L, 2, 3 };

            var testObject = new TestPoco
            {
                LongValue = longValue,
                StringValue = stringValue,
                ArrayOfLongsValue = arrayOfLongsValue,
                DateTimeValue = dateValue
            };

            var dictionaryOfProperties = DictionaryHelper.ToDictionary(testObject, p => p, v => v);
            var objectToBePopulated = new TestPoco();

            DictionaryHelper.PopulateObjectProperties(objectToBePopulated, dictionaryOfProperties);

            Assert.AreEqual(testObject.ArrayOfLongsValue, objectToBePopulated.ArrayOfLongsValue);
            Assert.AreEqual(testObject.DateTimeValue, objectToBePopulated.DateTimeValue);
            Assert.AreEqual(testObject.LongValue, objectToBePopulated.LongValue);
            Assert.AreEqual(testObject.NullableLongValue, objectToBePopulated.NullableLongValue);
            Assert.AreEqual(testObject.StringValue, objectToBePopulated.StringValue);
        }

        [Test]
        public void PopulateObjectProperties_TestHyphenatedEnumConversion()
        {
            const TestEnum expectedEnumValue = TestEnum.AnotherTestValue;
            const TestEnum expectedEnumValueNullable = TestEnum.TestValue;

            var testObjectDictionary = new Dictionary<string, object>
            {
                {"EnumValue", NamingConventionHelper.ConvertPascalCaseToHyphenated(expectedEnumValue.ToString())},
                {"EnumValueNullable", NamingConventionHelper.ConvertPascalCaseToHyphenated(expectedEnumValueNullable.ToString())}
            };

            var objectToBePopulated = new TestPocoWithEnum();
            var settings = DictionaryHelper.DefaultConvertTypeSettings;
            settings.EnumNameConversion = EnumNameConversion.ToHyphenated;

            DictionaryHelper.PopulateObjectProperties(objectToBePopulated, testObjectDictionary, settings);

            Assert.AreEqual(expectedEnumValue, objectToBePopulated.EnumValue);
            Assert.IsNotNull(objectToBePopulated.EnumValueNullable);
            Assert.AreEqual(expectedEnumValueNullable, objectToBePopulated.EnumValueNullable);
        }

        [TestCase("pl-PL")]
        [TestCase("en-US")]
        [TestCase(null)]
        public void PopulateObjectProperties_TestCulture(string cultureName)
        {
            var culture = cultureName == null
                ? CultureInfo.InvariantCulture
                : new CultureInfo(cultureName, true);

            var expectedDouble = 3.14;
            var expectedDateTime = new DateTime(2016, 1, 1, 21, 37, 30);

            var objectDictionary = new Dictionary<string, object>
            {
                { "DoubleValue", expectedDouble.ToString(culture) },
                { "DateTimeValue", expectedDateTime.ToString(culture) },
            };

            var objectToPopulate = new TestCulturePoco();
            var settings = DictionaryHelper.DefaultConvertTypeSettings;
            settings.FormatProvider = culture;

            DictionaryHelper.PopulateObjectProperties(objectToPopulate, objectDictionary, settings);

            Assert.AreEqual(expectedDouble, objectToPopulate.DoubleValue);
            Assert.AreEqual(expectedDateTime, objectToPopulate.DateTimeValue);
        }
    }
}
