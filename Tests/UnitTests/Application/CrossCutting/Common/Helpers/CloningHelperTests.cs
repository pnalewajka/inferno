﻿using System;
using System.Linq;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;
using System.Collections.Generic;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public partial class CloningHelperTests
    {
        [Test]
        public void CloneObject_TestSimpleObject()
        {
            const int intValue = 1;

            var datetimeValue = DateTime.Now;

            var objectToCopy = new TestPoco
            {
                DateTimeValue =  datetimeValue,
                IntValue = intValue
            };

            var copiedObject = CloningHelper.CloneObject(objectToCopy);

            Assert.AreEqual(intValue, copiedObject.IntValue);
            Assert.AreEqual(null, copiedObject.LongValue);
            Assert.AreEqual(datetimeValue, copiedObject.DateTimeValue);
        }

        [Test]
        public void CloneList_ShallowClone()
        {
            var pocoObject = new TestPoco
            {
                DateTimeValue = DateTime.Now,
                IntValue = 1
            };

            var sourceList = new List<TestPoco> {pocoObject};

            var copiedList = CloningHelper.CloneList(sourceList);

            Assert.IsNotNull(copiedList);
            Assert.AreNotSame(sourceList, copiedList);
            Assert.AreEqual(sourceList.Count, copiedList.Count);
            Assert.AreSame(sourceList.First(), copiedList.First());
        }

        [Test]
        public void CloneList_DeepClone()
        {
            var sourceObject = new TestPoco
            {
                DateTimeValue = DateTime.Now,
                IntValue = 1
            };
            var sourceList = new List<TestPoco> { sourceObject };

            var copiedList = CloningHelper.CloneList(sourceList, CloningHelper.CloneObject);

            Assert.IsNotNull(copiedList);
            Assert.AreNotSame(sourceList, copiedList);
            Assert.AreEqual(sourceList.Count, copiedList.Count);
            Assert.AreNotSame(sourceList.First(), copiedList.First());
        }


    }
}
