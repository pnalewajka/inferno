﻿using System;
using System.Linq;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public class MentionItemHelperTests
    {
        [Test]
        [TestCase("@{ Id: 1, Label: 'some name', SourceName: 'some_name'  }", Result = 1)]
        [TestCase("@{Id:1,Label:'some name',SourceName:'some_name'}", Result = 1)]
        [TestCase("@{Id:1,Label:\"some name\",SourceName:\"some_name\"}", Result = 1)]
        [TestCase("some text before @{ Id: 1, Label: 'some name', SourceName: 'some_name'  }", Result = 1)]
        [TestCase("@{ Id: 1, Label: 'some name', SourceName: 'some_name'  } some text after ", Result = 1)]
        [TestCase("some text before @{ Id: 1, Label: 'some name', SourceName: 'some_name'  } some text after ", Result = 1)]
        [TestCase("some text before @{ Id: 1, Label: 'some name', SourceName: 'some_name'  } some text between @{ Id: 2, Label: 'some name', SourceName: 'some_name'  } some text after ", Result = 2)]
        [TestCase("some text before @{ Id: 1 } ", Result = 0)]
        [TestCase("some text before @{ not json ", Result = 0)]
        [TestCase("some text before @{ not json }", Result = 0)]
        public int ResolveTokenIds_PositiveCaseTest(string input_text)
        {
            var tokenIds = MentionItemHelper.GetTokenIds(input_text).Distinct();

            return tokenIds.Count();
        }

        [Test]
        [TestCase("some text without token")]
        [TestCase("some text before @{ not json")]
        [TestCase("some text before @{ not json }")]
        [TestCase("some text before @{ not json } af}ter")]
        [TestCase("some text before @{ after")]
        [TestCase("some text before }} after")]
        [TestCase("some text before } after")]
        public void ReplaceTokens_NegativeCaseTests(string input_text)
        {
            var newInput = string.Empty;

            Assert.DoesNotThrow(() =>
            {
                newInput = MentionItemHelper.ReplaceTokens(input_text, item =>
                {
                    throw new Exception("Replace string without valid token");
                });
            });

            Assert.AreEqual(input_text, newInput);
        }

        [Test]
        [TestCase("some text with token @{ Id: 1, Label: 'some name', SourceName: 'some_name' }", "some name", "some_name")]
        [TestCase("some text with token @{ Id: 1, Label: 'X', SourceName: 'X' }", "X", "X")]
        public void ReplaceTokens_SingleToken_PositiveCaseTests(string input_text, string tokenLabel, string tokenSourceName)
        {
            var newInput = string.Empty;
            var resolverWasTriggered = false;

            newInput = MentionItemHelper.ReplaceTokens(input_text, item =>
            {
                Assert.AreEqual(tokenLabel, item.Label);
                Assert.AreEqual(tokenSourceName, item.SourceName);
                resolverWasTriggered = true;

                return item.Label;
            });

            Assert.IsTrue(resolverWasTriggered, "Resolver was not triggered");
            Assert.AreNotEqual(input_text, newInput);
        }

        [Test]
        [TestCase("@{ Id: 1, Label: 'some name', SourceName: 'some_name' }", Result = "some name")]
        [TestCase("text @{ Id: 1, Label: 'some name', SourceName: 'some_name' }", Result = "text some name")]
        [TestCase("text @{ Id: 1, Label: 'some name', SourceName: 'some_name' } after ", Result = "text some name after")]
        [TestCase("text @{ Id: 1, Label: 'some name', SourceName: 'some_name' } after @{ Id: 1, Label: 'other text', SourceName: 'some_name' }", Result = "text some name after other text")]
        public string ReplaceTokens_SingleToken_PositiveCaseTests(string input_text)
        {
            return MentionItemHelper.ReplaceTokens(input_text, item =>
            {
                return item.Label;
            });
        }
    }
}
