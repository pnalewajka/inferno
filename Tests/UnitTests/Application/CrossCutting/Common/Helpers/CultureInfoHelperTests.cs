using System.Globalization;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public class CultureInfoHelperTests
    {
        //WARNING: test will fail on Windows 10, due to differences in PL regional settings
        //commenting out due to reliance on platform
        //[Test]
        //public void GetJavaScriptDateFormat_TestForPolishFormat()
        //{
        //    var javaScriptDateFormat = CultureInfoHelper.GetJavaScriptDateFormat(new CultureInfo("pl-PL"));
        //    Assert.AreEqual("yyyy-mm-dd", javaScriptDateFormat);
        //}

        [Test]
        public void GetJavaScriptDateFormat_TestForGreatBritainFormat()
        {
            var javaScriptDateFormat = CultureInfoHelper.GetJavaScriptDateFormat(new CultureInfo("en-GB", false));
            Assert.AreEqual("dd/mm/yyyy", javaScriptDateFormat);
        }

        [Test]
        public void GetJavaScriptDateFormat_TestUSFormat()
        {
            var javaScriptDateFormat = CultureInfoHelper.GetJavaScriptDateFormat(new CultureInfo("en-US", false));
            Assert.AreEqual("m/d/yyyy", javaScriptDateFormat);
        }
    }
}