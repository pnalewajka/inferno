﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq.Expressions;
using System.Reflection;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Models;
using UnitTests.Application.CrossCutting.Abstracts;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public class LocalizedStringHelperTests
    {
        private static readonly ExampleLocalizedString LocalizedString = new ExampleLocalizedString
        {
            English = "Centre",
            AmericanEnglish = "Center",
            SwissGerman = "Zentrum",
            Polish = "Centrum",
        };

        private class EntityTest
        {
            public string NamePl { get; set; }

            public string NameEn { get; set; }

            public string Name { get; set; }
        }
        
        [Test]
        public void GetLocalizedString_ArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>((() => LocalizedStringHelper.GetLocalizedString(null)));
        }

        [Test]
        public void GetLocalizedString_SpecifiedCulture_ExactMatch()
        {
            var culture = new CultureInfo("en");
            Assert.AreEqual(LocalizedString.English, LocalizedStringHelper.GetLocalizedString(LocalizedString, culture));

            culture = new CultureInfo("en-US");
            Assert.AreEqual(LocalizedString.AmericanEnglish, LocalizedStringHelper.GetLocalizedString(LocalizedString, culture));

            culture = new CultureInfo("de-CH");
            Assert.AreEqual(LocalizedString.SwissGerman, LocalizedStringHelper.GetLocalizedString(LocalizedString, culture));

            culture = new CultureInfo("");
            Assert.AreEqual(LocalizedString.Polish, LocalizedStringHelper.GetLocalizedString(LocalizedString, culture));
        }

        [Test]
        public void GetLocalizedString_SpecifiedCulture_ParentMatch()
        {
            var culture = new CultureInfo("en-GB");
            Assert.AreEqual(LocalizedString.English, LocalizedStringHelper.GetLocalizedString(LocalizedString, culture));
        }

        [Test]
        public void GetLocalizedString_SpecifiedCulture_DefaultMatch()
        {
            var culture = new CultureInfo("es");
            Assert.AreEqual(LocalizedString.Polish, LocalizedStringHelper.GetLocalizedString(LocalizedString, culture));

            culture = new CultureInfo("de");
            Assert.AreEqual(LocalizedString.Polish, LocalizedStringHelper.GetLocalizedString(LocalizedString, culture));
        }

        [Test]
        public void GetLocalizedString_CurrentUICulture_ExactMatch()
        {
            using (CultureInfoHelper.SetCurrentUICulture(new CultureInfo("en")))
            {
                Assert.AreEqual(LocalizedString.English, LocalizedStringHelper.GetLocalizedString(LocalizedString));
            }

            using (CultureInfoHelper.SetCurrentUICulture(new CultureInfo("en-US")))
            {
                Assert.AreEqual(LocalizedString.AmericanEnglish,
                    LocalizedStringHelper.GetLocalizedString(LocalizedString));
            }

            using (CultureInfoHelper.SetCurrentUICulture(new CultureInfo("de-CH")))
            {
                Assert.AreEqual(LocalizedString.SwissGerman, LocalizedStringHelper.GetLocalizedString(LocalizedString));
            }

            using (CultureInfoHelper.SetCurrentUICulture(new CultureInfo("")))
            {
                Assert.AreEqual(LocalizedString.Polish, LocalizedStringHelper.GetLocalizedString(LocalizedString));
            }
        }

        [Test]
        public void GetLocalizedString_CurrentUICulture_ParentMatch()
        {
            using (CultureInfoHelper.SetCurrentUICulture(new CultureInfo("en-GB")))
            {
                Assert.AreEqual(LocalizedString.English, LocalizedStringHelper.GetLocalizedString(LocalizedString));
            }
        }

        [Test]
        public void GetLocalizedString_CurrentUICulture_DefaultMatch()
        {
            using (CultureInfoHelper.SetCurrentUICulture(new CultureInfo("es")))
            {
                Assert.AreEqual(LocalizedString.Polish, LocalizedStringHelper.GetLocalizedString(LocalizedString));
            }

            using (CultureInfoHelper.SetCurrentUICulture(new CultureInfo("de")))
            {
                Assert.AreEqual(LocalizedString.Polish, LocalizedStringHelper.GetLocalizedString(LocalizedString));
            }
        }

        [Test]
        public void GetLocalizedString_NoDefault()
        {
            var culture = new CultureInfo("ru");
            var localizedString = new ExampleLocalizedStringWithoutDefault
            {
                English = "Centre",
                AmericanEnglish = "Center",
                SwissGerman = "Zentrum",
            };

            Assert.Throws<InvalidOperationException>(
                () => LocalizedStringHelper.GetLocalizedString(localizedString, culture));

            using (CultureInfoHelper.SetCurrentUICulture(new CultureInfo("bg")))
            {
                Assert.Throws<InvalidOperationException>(() => LocalizedStringHelper.GetLocalizedString(localizedString));
            }
        }

        [Test]
        public void GetLocalizedPropertyInfo_SpecifiedCulture_ExactMatch()
        {
            var culture = new CultureInfo("en");
            Assert.AreEqual(typeof(ExampleLocalizedString).GetProperty(nameof(ExampleLocalizedString.English)),
                LocalizedStringHelper.GetLocalizedPropertyInfo(typeof(ExampleLocalizedString), culture));

            culture = new CultureInfo("en-US");
            Assert.AreEqual(
                typeof(ExampleLocalizedString).GetProperty(nameof(ExampleLocalizedString.AmericanEnglish)),
                LocalizedStringHelper.GetLocalizedPropertyInfo(typeof(ExampleLocalizedString), culture));

            culture = new CultureInfo("de-CH");
            Assert.AreEqual(typeof(ExampleLocalizedString).GetProperty(nameof(ExampleLocalizedString.SwissGerman)),
                LocalizedStringHelper.GetLocalizedPropertyInfo(typeof(ExampleLocalizedString), culture));

            culture = new CultureInfo("");
            Assert.AreEqual(typeof(ExampleLocalizedString).GetProperty(nameof(ExampleLocalizedString.Polish)),
                LocalizedStringHelper.GetLocalizedPropertyInfo(typeof(ExampleLocalizedString), culture));
        }

        [Test]
        public void GetLocalizedPropertyInfo_SpecifiedCulture_ParentMatch()
        {
            var culture = new CultureInfo("en-GB");
            Assert.AreEqual(typeof(ExampleLocalizedString).GetProperty(nameof(ExampleLocalizedString.English)),
                LocalizedStringHelper.GetLocalizedPropertyInfo(typeof(ExampleLocalizedString), culture));
        }

        [Test]
        public void GetLocalizedPropertyInfo_SpecifiedCulture_DefaultMatch()
        {
            var culture = new CultureInfo("es");
            Assert.AreEqual(typeof(ExampleLocalizedString).GetProperty(nameof(ExampleLocalizedString.Polish)),
                LocalizedStringHelper.GetLocalizedPropertyInfo(typeof(ExampleLocalizedString), culture));

            culture = new CultureInfo("de");
            Assert.AreEqual(typeof(ExampleLocalizedString).GetProperty(nameof(ExampleLocalizedString.Polish)),
                LocalizedStringHelper.GetLocalizedPropertyInfo(typeof(ExampleLocalizedString), culture));
        }

        [Test]
        public void GetLocalizedPropertyInfo_CurrentUICulture_ExactMatch()
        {
            using (CultureInfoHelper.SetCurrentUICulture(new CultureInfo("en")))
            {
                Assert.AreEqual(typeof(ExampleLocalizedString).GetProperty(nameof(ExampleLocalizedString.English)),
                    LocalizedStringHelper.GetLocalizedPropertyInfo(typeof(ExampleLocalizedString)));
            }

            using (CultureInfoHelper.SetCurrentUICulture(new CultureInfo("en-US")))
            {
                Assert.AreEqual(typeof(ExampleLocalizedString).GetProperty(nameof(ExampleLocalizedString.AmericanEnglish)),
                    LocalizedStringHelper.GetLocalizedPropertyInfo(typeof(ExampleLocalizedString)));
            }

            using (CultureInfoHelper.SetCurrentUICulture(new CultureInfo("de-CH")))
            {
                Assert.AreEqual(typeof(ExampleLocalizedString).GetProperty(nameof(ExampleLocalizedString.SwissGerman)),
                    LocalizedStringHelper.GetLocalizedPropertyInfo(typeof(ExampleLocalizedString)));
            }

            using (CultureInfoHelper.SetCurrentUICulture(new CultureInfo("")))
            {
                Assert.AreEqual(typeof(ExampleLocalizedString).GetProperty(nameof(ExampleLocalizedString.Polish)),
                    LocalizedStringHelper.GetLocalizedPropertyInfo(typeof(ExampleLocalizedString)));
            }
        }

        [Test]
        public void GetLocalizedPropertyInfo_CurrentUICulture_ParentMatch()
        {
            using (CultureInfoHelper.SetCurrentUICulture(new CultureInfo("en-GB")))
            {
                Assert.AreEqual(typeof(ExampleLocalizedString).GetProperty(nameof(ExampleLocalizedString.English)),
                    LocalizedStringHelper.GetLocalizedPropertyInfo(typeof(ExampleLocalizedString)));
            }
        }

        [Test]
        public void GetLocalizedPropertyInfo_CurrentUICulture_DefaultMatch()
        {
            using (CultureInfoHelper.SetCurrentUICulture(new CultureInfo("es")))
            {
                Assert.AreEqual(typeof(ExampleLocalizedString).GetProperty(nameof(ExampleLocalizedString.Polish)),
                    LocalizedStringHelper.GetLocalizedPropertyInfo(typeof(ExampleLocalizedString)));
            }

            using (CultureInfoHelper.SetCurrentUICulture(new CultureInfo("de")))
            {
                Assert.AreEqual(typeof(ExampleLocalizedString).GetProperty(nameof(ExampleLocalizedString.Polish)),
                    LocalizedStringHelper.GetLocalizedPropertyInfo(typeof(ExampleLocalizedString)));
            }
        }

        [Test]
        public void GetLocalizedPropertyInfo_NoDefault()
        {
            var culture = new CultureInfo("ru");

            Assert.Throws<InvalidOperationException>(
                () =>
                    LocalizedStringHelper.GetLocalizedPropertyInfo(typeof(ExampleLocalizedStringWithoutDefault),
                        culture));

            using (CultureInfoHelper.SetCurrentUICulture(new CultureInfo("bg")))
            {
                Assert.Throws<InvalidOperationException>(
                    () => LocalizedStringHelper.GetLocalizedPropertyInfo(typeof(ExampleLocalizedStringWithoutDefault)));
            }
        }

        [Test]
        public void GetCultureToPropertyMapping()
        {
            var actual = LocalizedStringHelper.GetCultureToPropertyMapping(typeof(ExampleLocalizedString));

            var expected = new Dictionary<string, PropertyInfo>
            {
                {"en", typeof(ExampleLocalizedString).GetProperty(nameof(ExampleLocalizedString.English))},
                {"en-US", typeof(ExampleLocalizedString).GetProperty(nameof(ExampleLocalizedString.AmericanEnglish))},
                {"de-CH", typeof(ExampleLocalizedString).GetProperty(nameof(ExampleLocalizedString.SwissGerman))},
                {"", typeof(ExampleLocalizedString).GetProperty(nameof(ExampleLocalizedString.Polish))},
            };

            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void GetOrderByExpressionForSpecificCulture()
        {          
            string expectedPL = "x => x.NamePl";
            string expectedEn = "x => x.NameEn";

            Expression<Func<EntityTest, string>> exp = x => x.Name;

            string outputPL;
            using (CultureInfoHelper.SetCurrentUICulture(new CultureInfo("pl")))
            {
                var expChanged = LocalizedStringHelper.GetOrderByExpressionForSpecificCulture<LocalizedString, EntityTest>(exp);
                outputPL = expChanged.ToString();
            }

            Assert.AreEqual(expectedPL, outputPL);

            string outputEn;
            using (CultureInfoHelper.SetCurrentUICulture(new CultureInfo("en")))
            {
                var expChanged = LocalizedStringHelper.GetOrderByExpressionForSpecificCulture<LocalizedString, EntityTest>(exp);
                outputEn = expChanged.ToString();
            }

            Assert.AreEqual(expectedEn, outputEn);
        }
    }
}
