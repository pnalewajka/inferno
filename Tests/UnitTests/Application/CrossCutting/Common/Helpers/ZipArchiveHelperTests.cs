﻿namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    using NUnit.Framework;
    using Smt.Atomic.CrossCutting.Common.Interfaces;
    using System.Linq;
    using System.IO;
    using Smt.Atomic.CrossCutting.Common.Helpers;
    using System.IO.Compression;
    using System.Collections.Generic;
    using System.Text;
    using System;

    [TestFixture]
    public class ZipArchiveHelperTests
    {
        private const string FileNameTemplate = "fileName{0}.txt";
        private const string FileContentTemplate = "Test content {0}";
        private const int FilesCount = 5;

        private class FakeArchivableFile : IArchivable
        {
            public byte[] Content { get; set; }
            public string FileName { get; set; }
        }

        [Test]
        public void GetZipArchive_ShouldCompressAllProvidedFiles()
        {
            var expectedFiles = ArrangeTest();

            var resultArchiveEntries = Act(expectedFiles, GetZipArchive_Act);

            AssertTest(expectedFiles, resultArchiveEntries);
        }

        private void GetZipArchive_Act(List<FakeArchivableFile> expectedFiles, MemoryStream memoryStream)
        {
            var resultArchive = ZipArchiveHelper.GetZipArchive(memoryStream, expectedFiles);
        }

        [Test]
        public void GetZipArchiveBytes_ShouldCompressAllProvidedFiles()
        {
            var expectedFiles = ArrangeTest();

            var resultArchiveEntries = Act(expectedFiles, GetZipArchiveBytes_Act);

            AssertTest(expectedFiles, resultArchiveEntries);
        }

        private void GetZipArchiveBytes_Act(List<FakeArchivableFile> expectedFiles, MemoryStream memoryStream)
        {
            var resultArchiveBytes = ZipArchiveHelper.GetZipArchiveBytes(expectedFiles);
            memoryStream.Write(resultArchiveBytes, 0, resultArchiveBytes.Length);
        }

        private static List<FakeArchivableFile> ArrangeTest()
        {
            return Enumerable.Range(1, FilesCount).Select(number =>
            {
                string fileContent = string.Format(FileContentTemplate, number);

                return new FakeArchivableFile
                {
                    FileName = string.Format(FileNameTemplate, number),
                    Content = Encoding.UTF8.GetBytes(fileContent)
                };
            }).ToList();
        }

        private static void AssertTest(List<FakeArchivableFile> expectedFiles, List<ZipArchiveEntry> resultArchiveEntries)
        {
            Assert.AreEqual(expectedFiles.Count, resultArchiveEntries.Count);
            resultArchiveEntries.ForEach(entry =>
            {
                var sourcefile = expectedFiles.SingleOrDefault(f => entry.Name == f.FileName);
                Assert.IsNotNull(sourcefile);
                Assert.AreEqual(sourcefile.FileName, entry.Name);
                Assert.AreEqual(sourcefile.Content.Length, entry.Length);
            });
        }

        private static List<ZipArchiveEntry> Act(List<FakeArchivableFile> expectedFiles, Action<List<FakeArchivableFile>, MemoryStream> actAction)
        {
            List<ZipArchiveEntry> resultArchiveEntries = null;

            using (var memoryStream = new MemoryStream())
            {
                actAction(expectedFiles, memoryStream);
                resultArchiveEntries = ReadZipArchiveEntries(memoryStream).ToList();
            }

            return resultArchiveEntries;
        }

        private static IEnumerable<ZipArchiveEntry> ReadZipArchiveEntries(MemoryStream memoryStream)
        {
            using (var archive = new ZipArchive(memoryStream, ZipArchiveMode.Read))
            {
                return archive.Entries;
            }
        }
    }
}
