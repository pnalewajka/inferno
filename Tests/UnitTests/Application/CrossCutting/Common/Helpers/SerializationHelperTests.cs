﻿using System;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public class SerializationHelperTests
    {
        [Serializable]
        private class Test
        {
            public int IntValue1 { get; set; }
            public string StringValue1 { get; set; }
        }

        [Test]
        public void SerializationTest()
        {
            var dataObject = new Test
            {
                IntValue1 = 123,
                StringValue1 = Guid.NewGuid().ToString(),
            };
            var xml = SerializationHelper.SerializeToXml(dataObject, typeof (Test));
            var deserializedObject = SerializationHelper.DeserializeFromXml(xml, typeof (Test)) as Test;
            Assert.IsNotNull(deserializedObject);
            Assert.AreEqual(dataObject.IntValue1, deserializedObject.IntValue1);
            Assert.AreEqual(dataObject.StringValue1, deserializedObject.StringValue1);
        }
    }
}