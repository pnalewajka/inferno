﻿using System;
using System.Linq;
using System.Linq.Expressions;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public partial class PropertyHelperTests
    {
        [Test]
        public void GetProperty_TestPositiveExtraction()
        {
            var propertyInfo = PropertyHelper.GetProperty<TestClass1>(r => r.TestProperty1);
            Assert.IsNotNull(propertyInfo);
        }

        [Test]
        public void GetPropertyName_TestPositiveExtraction()
        {
            const string testPropertyName = "TestProperty1";
            var propertyName = PropertyHelper.GetPropertyName<TestClass1>(r => r.TestProperty1);
            Assert.AreEqual(testPropertyName, propertyName);
        }

        [Test]
        public void GetPropertyType_TestPositiveExtraction()
        {
            var realPropertyType = typeof (int);
            var propertyType = PropertyHelper.GetPropertyType<TestClass1>(r => r.TestProperty1);
            Assert.AreSame(realPropertyType, propertyType);
        }

        [Test]
        public void GetPropertyNames_TestPositiveExtraction()
        {
            var propertiesNames = PropertyHelper.GetPropertyNames<TestClass1>(r => r.TestProperty1, r=>r.TestProperty2).ToList();
            const int expectedLength = 2;
            Assert.AreEqual(expectedLength, propertiesNames.Count());
            Assert.IsTrue(propertiesNames.Contains(PropertyHelper.GetPropertyName<TestClass1>(r => r.TestProperty1)));
            Assert.IsTrue(propertiesNames.Contains(PropertyHelper.GetPropertyName<TestClass1>(r => r.TestProperty2)));
        }

        [Test]
        public void GetPropertyPath_TestPathExtractionOnDoubleGenericParameterVersion()
        {
            var path = PropertyHelper.GetPropertyPath<TestClass1, int>(t => t.TestPropertyDateTime.Day);
            const string expectedPath = "TestPropertyDateTime.Day";
            Assert.AreEqual(expectedPath, path);
        }

        [TestCase(typeof(TestClassVirtualProp), Result = true)]
        [TestCase(typeof(TestClassNotVirtualProp), Result = false)]
        public bool? IsVirtual_TestSeries(Type testClass)
        {
            var propertyInfo = testClass.GetProperties().First(p => p.Name == "Id");
            return PropertyHelper.IsVirtual(propertyInfo);
        }

        [Test]
        public void GetPropertyPathMemberInfo_TestLambdaWithoutCalls()
        {
            Expression<Func<User, object>> expression = u => u.ImpersonatedBy.FirstName;
            var propertyPathMemberInfo = PropertyHelper.GetPropertyPathMemberInfo(expression).Select(p => p.Name);
            var expected = new[] {nameof(User.ImpersonatedBy), nameof(User.FirstName)};

            CollectionAssert.AreEqual(expected, propertyPathMemberInfo);
        }

        [Test]
        public void GetPropertyPathMemberInfo_TestLambdaWithCalls1()
        {
            Expression<Func<User, object>> expression = u => u.Documents.Select(d => d.ContentType);
            var propertyPathMemberInfo = PropertyHelper.GetPropertyPathMemberInfo(expression).Select(p => p.Name);
            var expected = new[] { nameof(User.Documents), nameof(UserDocument.ContentType) };

            CollectionAssert.AreEqual(expected, propertyPathMemberInfo);
        }

        [Test]
        public void GetPropertyPathMemberInfo_TestLambdaWithCalls2()
        {
            Expression<Func<User, object>> expression = u => u.Profiles.SelectMany(p => p.Roles).Select(r => r.Name);
            var propertyPathMemberInfo = PropertyHelper.GetPropertyPathMemberInfo(expression).Select(p => p.Name);
            var expected = new[] { nameof(User.Profiles), nameof(SecurityProfile.Roles), nameof(SecurityRole.Name) };

            CollectionAssert.AreEqual(expected, propertyPathMemberInfo);
        }

        [Test]
        public void GetPropertyPathMemberInfo_TestLambdaWithCalls3()
        {
            Expression<Func<User, object>> expression = u => u.Profiles.SelectMany(p => p.Roles.Select(r => r.Name));
            var propertyPathMemberInfo = PropertyHelper.GetPropertyPathMemberInfo(expression).Select(p => p.Name);
            var expected = new[] { nameof(User.Profiles), nameof(SecurityProfile.Roles), nameof(SecurityRole.Name) };

            CollectionAssert.AreEqual(expected, propertyPathMemberInfo);
        }
    }
}
