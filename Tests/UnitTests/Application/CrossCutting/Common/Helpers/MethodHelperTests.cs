﻿using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public class MethodHelperTests
    {
        [Test]
        public void GetMethodExpressionByName_PositiveTest()
        {
            var expression = MethodHelper.GetLambdaFromMethod<TestClass>(
                MethodHelper.GetMethod<TestClass>("GetTestProperty"));
            var test = new TestClass(999);

            var resultValue = expression.Compile().Invoke(test);

            Assert.AreEqual(test.GetTestProperty(), resultValue);
        }

        private class TestClass
        {
            private readonly int _property;

            public TestClass(int property)
            {
                _property = property;
            }

            public int GetTestProperty()
            {
                return _property;
            }
        }
    }
}
