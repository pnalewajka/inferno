﻿using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public class ColorHelperTests
    {
        [TestCase("#000", Result = true)]
        [TestCase("#000000", Result = true)]
        [TestCase("Black", Result = true)]
        [TestCase("#6f1111", Result = true)]
        [TestCase("#39116f", Result = true)]
        [TestCase("#116f65", Result = true)]
        [TestCase("#fff", Result = false)]
        [TestCase("#FFF", Result = false)]
        [TestCase("#FFFFFF", Result = false)]
        [TestCase("#FFFFFF", Result = false)]
        [TestCase("#f35f5f", Result = false)]
        [TestCase("#acf35f", Result = false)]
        [TestCase("#bd5ff3", Result = false)]
        [TestCase("#f35f5f", Result = false)]
        [TestCase("White", Result = false)]
        public bool ColorHelper_IsDark(string color)
        {
            return ColorHelper.IsDark(color);
        }
    }
}
