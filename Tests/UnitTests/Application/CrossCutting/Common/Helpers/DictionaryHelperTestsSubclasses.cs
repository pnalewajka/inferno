﻿using System;
using System.Reflection;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    public partial class DictionaryHelperTests
    {
        private class TestPoco
        {
            public long LongValue { get; set; }
            public long? NullableLongValue { get; set; }
            public DateTime DateTimeValue { get; set; }
            public string StringValue { get; set; }
            public long[] ArrayOfLongsValue { get; set; }
        }

        private class TestPocoWithEnum
        {
            public TestEnum EnumValue { get; set; }
            public TestEnum? EnumValueNullable { get; set; }
        }

        private class TestCulturePoco
        {
            public double DoubleValue { get; set; }
            public DateTime DateTimeValue { get; set; }
        }

        private class TestStringsPoco
        {
            public string StringValue { get; set; }
            public string AnotherStringValue { get; set; }
        }

        private class TestResultPoco
        {
            public PropertyInfo PropertyInfo { get; private set; }
            public string Value { get; private set; }

            public TestResultPoco(PropertyInfo propertyInfo, string value)
            {
                PropertyInfo = propertyInfo;
                Value = value;
            }
        }

        private enum TestEnum
        {
            TestValue,
            AnotherTestValue
        }
    }
}