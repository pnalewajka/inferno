﻿using System;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public class DateHelperTests
    {
        [Test]
        public void FromUnixEpochTime_PositiveTest()
        {
            // todo: test fails for different system time zones
            const int epochTime = 1421857642;
            var date = DateHelper.FromUnixEpochTime(epochTime);
            var dateExpected = DateTime.ParseExact("2015-01-21 17:27:22","yyyy-MM-dd HH:mm:ss", null);
            Assert.AreEqual(dateExpected, date);
        }

        /// <summary>
        /// Warning! HelperMethods seems to process big values improperly:
        /// Example: P7M29DT22H22M30S should be 20994750, but is 20730150
        /// </summary>
        [Test]
        public void FromIso8601Duration()
        {
            const string duration = "PT310S";
            const long durationInSeconds = 310;
            var calculatedDuration = (long)(DateHelper.FromIso8601Duration(duration).TotalSeconds);
            Assert.AreEqual(durationInSeconds, calculatedDuration);
        }

    }
}
