﻿using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Attributes;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public partial class IdentifierHelperTests
    {
        [Identifier(SampleClassIdentifier)]
        public class SampleClass
        {
            public string SampleProperty { get; set; }
        }
    }
}