﻿using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public class NamingConventionHelperTests
    {
        [Test]
        public void ConvertPascalCaseToHyphenated_SimpleCaseTest()
        {
            var result = NamingConventionHelper.ConvertPascalCaseToHyphenated("someText");
            Assert.AreEqual("some-text", result);
        }

        [Test]
        public void ConvertPascalCaseToHyphenated_AdvancedCaseTest()
        {
            var result = NamingConventionHelper.ConvertPascalCaseToHyphenated("SomeText");
            Assert.AreEqual("some-text", result);
        }

        [Test]
        [TestCase("")]
        [TestCase("sometext")]
        [TestCase("SomeText")]
        [TestCase("some-text")]
        [TestCase("Some-Text")]
        [TestCase("SOMETEXT")]
        [TestCase("-S-OMETEXT-")]
        [TestCase("--SomeText--")]
        public void ConvertPascalCaseToHyphenated_CallingTwiceTest(string test)
        {
            var first = NamingConventionHelper.ConvertPascalCaseToHyphenated(test);
            var second = NamingConventionHelper.ConvertPascalCaseToHyphenated(first);

            Assert.AreEqual(first, second);
        }

        [Test]
        public void ConvertHyphenatedToCamelCase_SimpleCaseTest()
        {
            var result = NamingConventionHelper.ConvertHyphenatedToCamelCase("some-text");
            Assert.AreEqual("someText", result);
        }

        [Test]
        public void ConvertHyphenatedToCamelCase_AdvanceCaseTest()
        {
            var result = NamingConventionHelper.ConvertHyphenatedToCamelCase("some-text-to-be-checked");
            Assert.AreEqual("someTextToBeChecked", result);
        }

        [Test]
        public void ConvertPascalCaseToLabel_SimpleCase()
        {
            var result = NamingConventionHelper.ConvertPascalCaseToLabel("SomeText");
            Assert.AreEqual("Some Text", result);
        }

        [Test]
        public void ConvertToClassName_ShouldReturnSameStringForValidName()
        {
            var result = NamingConventionHelper.ConvertToClassName("SomeName");
            Assert.AreEqual("SomeName", result);
        }

        [Test]
        public void ConvertToClassName_ShouldUpperCaseFirstCharacter()
        {
            var result = NamingConventionHelper.ConvertToClassName("someName");
            Assert.AreEqual("SomeName", result);
        }

        [Test]
        public void ConvertToClassName_ShouldReturnValidNameForKeyLikeString()
        {
            var result = NamingConventionHelper.ConvertToClassName("This.is.some.Key");
            Assert.AreEqual("ThisIsSomeKey", result);
        }

        [Test]
        public void ConvertToClassName_ShouldReturnValidNameForAccentedString()
        {
            var result = NamingConventionHelper.ConvertToClassName("To być jakiś klucz.");
            Assert.AreEqual("ToBycJakisKlucz", result);
        }

        [Test]
        public void ConvertToClassName_ShouldReturnValidNameForNumber()
        {
            var result = NamingConventionHelper.ConvertToClassName("123");
            Assert.AreEqual("_123", result);
        }
    }
}
