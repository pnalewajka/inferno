﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public partial class IdentifierHelperTests
    {
        private const string SampleClassIdentifier = "UnitTests.IdentifierHelperTests.SampleClass";

        [Test, ExpectedException]
        public void GetTypeByIdentifier_ShouldThrowExceptionForUnknownType()
        {
            IdentifierHelper.GetTypeByIdentifier("Zenon");
        }

        [Test]
        public void GetTypeByIdentifier_ShouldReturnNullForUnknownTypeWhenNotThrowingException()
        {
            var type = IdentifierHelper.GetTypeByIdentifier("Zenon", false);
            Assert.IsNull(type);
        }

        [Test]
        public void GetTypeByIdentifier_ShouldReturnTypeForBasicType()
        {
            var intType = IdentifierHelper.GetTypeByIdentifier("int");
            var longType = IdentifierHelper.GetTypeByIdentifier("long");
            var decimalType = IdentifierHelper.GetTypeByIdentifier("decimal");
            var stringType = IdentifierHelper.GetTypeByIdentifier("string");
            var boolType = IdentifierHelper.GetTypeByIdentifier("bool");
            var dateTimeType = IdentifierHelper.GetTypeByIdentifier("DateTime");
            var timeSpanType = IdentifierHelper.GetTypeByIdentifier("TimeSpan");
            var intArrayType = IdentifierHelper.GetTypeByIdentifier("int[]");
            var longArrayType = IdentifierHelper.GetTypeByIdentifier("long[]");
            var decimalArrayType = IdentifierHelper.GetTypeByIdentifier("decimal[]");
            var stringArrayType = IdentifierHelper.GetTypeByIdentifier("string[]");
            var boolArrayType = IdentifierHelper.GetTypeByIdentifier("bool[]");
            var dateTimeArrayType = IdentifierHelper.GetTypeByIdentifier("DateTime[]");
            var timeSpanArrayType = IdentifierHelper.GetTypeByIdentifier("TimeSpan[]");
            
            Assert.NotNull(intType);
            Assert.NotNull(longType);
            Assert.NotNull(decimalType);
            Assert.NotNull(stringType);
            Assert.NotNull(boolType);
            Assert.NotNull(dateTimeType);
            Assert.NotNull(timeSpanType);
            Assert.NotNull(intArrayType);
            Assert.NotNull(longArrayType);
            Assert.NotNull(decimalArrayType);
            Assert.NotNull(stringArrayType);
            Assert.NotNull(boolArrayType);
            Assert.NotNull(dateTimeArrayType);
            Assert.NotNull(timeSpanArrayType);
        }

        [Test]
        public void GetTypeIdentifier_ShouldReturnValidIdentifierForBasicType()
        {
            var intId = IdentifierHelper.GetTypeIdentifier(typeof(int));
            var longId = IdentifierHelper.GetTypeIdentifier(typeof(long));
            var decimalId = IdentifierHelper.GetTypeIdentifier(typeof(decimal));
            var stringId = IdentifierHelper.GetTypeIdentifier(typeof(string));
            var boolId = IdentifierHelper.GetTypeIdentifier(typeof(bool));
            var dateTimeId = IdentifierHelper.GetTypeIdentifier(typeof(DateTime));
            var timeSpanId = IdentifierHelper.GetTypeIdentifier(typeof(TimeSpan));
            var intArrayId = IdentifierHelper.GetTypeIdentifier(typeof(int[]));
            var longArrayId = IdentifierHelper.GetTypeIdentifier(typeof(long[]));
            var decimalArrayId = IdentifierHelper.GetTypeIdentifier(typeof(decimal[]));
            var stringArrayId = IdentifierHelper.GetTypeIdentifier(typeof(string[]));
            var boolArrayId = IdentifierHelper.GetTypeIdentifier(typeof(bool[]));
            var dateTimeArrayId = IdentifierHelper.GetTypeIdentifier(typeof(DateTime[]));
            var timeSpanArrayId = IdentifierHelper.GetTypeIdentifier(typeof(TimeSpan[]));

            Assert.AreEqual("int", intId);
            Assert.AreEqual("long", longId);
            Assert.AreEqual("decimal", decimalId);
            Assert.AreEqual("string", stringId);
            Assert.AreEqual("bool", boolId);
            Assert.AreEqual("DateTime", dateTimeId);
            Assert.AreEqual("TimeSpan", timeSpanId);
            Assert.AreEqual("int[]", intArrayId);
            Assert.AreEqual("long[]", longArrayId);
            Assert.AreEqual("decimal[]", decimalArrayId);
            Assert.AreEqual("string[]", stringArrayId);
            Assert.AreEqual("bool[]", boolArrayId);
            Assert.AreEqual("DateTime[]", dateTimeArrayId);
            Assert.AreEqual("TimeSpan[]", timeSpanArrayId);
        }

        [Test, ExpectedException]
        public void GetTypeIdentifier_ShouldThrowExceptionForUnidentifiedType()
        {
            IdentifierHelper.GetTypeIdentifier(typeof(UIntPtr));
        }

        [Test]
        public void GetTypeIdentifier_ShouldReturnNullForUnidentifiedTypeWhenNotThrowingException()
        {
            var identifier = IdentifierHelper.GetTypeIdentifier(typeof(UIntPtr), false);
            Assert.IsNull(identifier);
        }

        [Test]
        public void GetTypeIdentifier_ShouldReturnValidIdentifier()
        {
            var identifier = IdentifierHelper.GetTypeIdentifier(typeof (SampleClass));
            Assert.AreEqual(SampleClassIdentifier, identifier);
        }

        [Test]
        public void GetTypeByIdentifier_ShouldReturnValidType()
        {
            var type = IdentifierHelper.GetTypeByIdentifier(SampleClassIdentifier);
            Assert.AreEqual(typeof(SampleClass), type);
        }

        [Test]
        public void AllIdentifiersShouldBeUnique()
        {
            var identifiableTypes =
                from a in AppDomain.CurrentDomain.GetAssemblies()
                from t in a.GetTypes()
                let attributes = t.GetCustomAttributes(typeof(IdentifierAttribute), true)
                where attributes != null && attributes.Length > 0
                select new { Type = t, IdentifierAttribute = (IdentifierAttribute)attributes.First() };

            var identifiers = new HashSet<string>();

            foreach (var identifiableType in identifiableTypes)
            {
                var identifier = identifiableType.IdentifierAttribute.Value;
                if (identifiers.Contains(identifier))
                {
                    var message = $"Not unique identifier: {identifier}";
                    throw new InvalidDataException(message);
                }

                identifiers.Add(identifier);
            }
        }
    }
}