﻿using System.Linq;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Helpers;
using UnitTests.Application.CrossCutting.Common.Helpers.Resources;
using System;
using System.Collections.Generic;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public class EnumHelperTests
    {
        private const string EnumDescription = "Option";
        private enum TestEnum1
        {
            [System.ComponentModel.Description(EnumDescription)]
            Option1,

            [DescriptionLocalized("Option2Label", typeof(SampleResources))]
            Option2,

            Option3
        }

        [Flags]
        private enum TestFlagsEnum
        {
            Flag1 = 1 << 0,

            Flag2 = 1 << 1,

            Flag4 = 1 << 2
        }

        [Test]
        public void GetFlags_TestPositive()
        {
            var enums = EnumHelper.GetFlags(TestFlagsEnum.Flag1).ToList();

            Assert.AreEqual(1, enums.Count);
            Assert.AreEqual(TestFlagsEnum.Flag1, enums.First());
        }

        [Test]
        public void GetFlags_TestPositiveWithMixedFlags()
        {
            var enums = EnumHelper.GetFlags(TestFlagsEnum.Flag1 | TestFlagsEnum.Flag4).ToList();

            Assert.AreEqual(2, enums.Count);
            Assert.Contains(TestFlagsEnum.Flag1, enums);
            Assert.Contains(TestFlagsEnum.Flag4, enums);
        }

        [Test]
        public void GetEnumValue_TestPositive()
        {
            var enumName = TestEnum1.Option1.ToString();

            Assert.AreEqual(TestEnum1.Option1, EnumHelper.GetEnumValue<TestEnum1>(enumName));
        }

        [Test]
        public void GetEnumValues_PositiveTest()
        {
            var enumValues = EnumHelper.GetEnumValues<TestEnum1>();

            Assert.AreEqual(3, enumValues.Count());
            Assert.IsTrue(enumValues.Contains(TestEnum1.Option2));
        }

        [Test]
        public void GetOutOfRangeValue_GenericTestPositive()
        {
            var valueOutOfRange = EnumHelper.GetOutOfRangeValue<TestEnum1>();

            Assert.AreNotEqual(TestEnum1.Option1, valueOutOfRange);
            Assert.AreNotEqual(TestEnum1.Option2, valueOutOfRange);
            Assert.AreNotEqual(TestEnum1.Option3, valueOutOfRange);
        }

        [Test]
        public void GetOutOfRangeValue_TestPositive()
        {
            var valueOutOfRange = (TestEnum1)EnumHelper.GetOutOfRangeValue(typeof(TestEnum1));

            Assert.AreNotEqual(TestEnum1.Option1, valueOutOfRange);
            Assert.AreNotEqual(TestEnum1.Option2, valueOutOfRange);
            Assert.AreNotEqual(TestEnum1.Option3, valueOutOfRange);
        }

        [Test]
        public void GetEnumNames_GenericTestPositive()
        {
            var names = EnumHelper.GetEnumNames<TestEnum1>().ToList();

            Assert.AreEqual(3, names.Count());
            Assert.AreEqual(TestEnum1.Option1.ToString(), names[0]);
            Assert.AreEqual(TestEnum1.Option2.ToString(), names[1]);
            Assert.AreEqual(TestEnum1.Option3.ToString(), names[2]);
        }

        [Test]
        public void GetEnumNames_NonGenericTestPositive()
        {
            var names = EnumHelper.GetEnumNames(typeof(TestEnum1)).ToList();

            Assert.AreEqual(3, names.Count());
            Assert.AreEqual(TestEnum1.Option1.ToString(), names[0]);
            Assert.AreEqual(TestEnum1.Option2.ToString(), names[1]);
            Assert.AreEqual(TestEnum1.Option3.ToString(), names[2]);
        }

        [Test]
        public void GetEnumValue_TestPositive_AggregateFlags()
        {
            var value = EnumHelper.GetEnumValue(typeof(TestFlagsEnum), new List<string>
            {
                TestFlagsEnum.Flag1.ToString(),
                TestFlagsEnum.Flag4.ToString()
            });

            Assert.AreEqual(TestFlagsEnum.Flag1 | TestFlagsEnum.Flag4, value);
        }

        [Test]
        public void TryParse_WithGeneric_ValidName()
        {
            var succeeded = EnumHelper.TryParse("Option1", out TestEnum1 value);

            Assert.IsTrue(succeeded);
            Assert.AreEqual(value, TestEnum1.Option1);
        }

        [Test]
        public void TryParse_WithGeneric_ValidNumber()
        {
            var succeeded = EnumHelper.TryParse("1", out TestEnum1 value);

            Assert.IsTrue(succeeded);
            Assert.AreEqual(value, TestEnum1.Option2);
        }

        [Test]
        public void TryParse_WithGeneric_InvalidName()
        {
            var succeeded = EnumHelper.TryParse("Option343", out TestEnum1 value);

            Assert.IsFalse(succeeded);
        }

        [Test]
        public void TryParse_WithGeneric_InvalidNumber()
        {
            var succeeded = EnumHelper.TryParse("-45", out TestEnum1 value);

            Assert.IsFalse(succeeded);
        }

        [Test]
        public void TryParse_WithGeneric_Flagged_ValidNumber()
        {
            var succeeded = EnumHelper.TryParse("3", out TestFlagsEnum value);

            Assert.IsTrue(succeeded);
            Assert.IsTrue(value.HasFlag(TestFlagsEnum.Flag1));
            Assert.IsTrue(value.HasFlag(TestFlagsEnum.Flag2));
            Assert.IsFalse(value.HasFlag(TestFlagsEnum.Flag4));
        }
    }
}
