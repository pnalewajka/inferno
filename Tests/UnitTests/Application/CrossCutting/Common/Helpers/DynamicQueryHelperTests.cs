﻿using System;
using System.Linq;
using System.Linq.Expressions;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Entities.Modules.EventSourcing;
using Smt.Atomic.Data.Entities.Modules.Runtime;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public class DynamicQueryHelperTests
    {
        [Test]
        public void BuildOrderByExpression_ShouldReturnPropertyValueForString()
        {
            var propertyName = PropertyHelper.GetPropertyName<User>(u => u.LastName);
            var expression = DynamicQueryHelper.BuildOrderByExpression<User>(propertyName);
            var user = new User { LastName = "John" };

            Assert.AreEqual(user.LastName, expression.Compile().Invoke(user));
        }

        [Test]
        public void BuildOrderByExpression_ShouldReturnPropertyValueForInt()
        {
            var propertyName = PropertyHelper.GetPropertyName<User>(u => u.Id);
            var expression = DynamicQueryHelper.BuildOrderByExpression<User>(propertyName);
            var user = new User { Id = 128 };

            Assert.AreEqual(user.Id, expression.Compile().Invoke(user));
        }

        [Test]
        public void Or_ShouldCombineConditionsProperly()
        {
            Expression<Func<User, bool>> alwaysTrue = u => true;
            Expression<Func<User, bool>> alwaysFalse = u => false;

            Assert.IsFalse(alwaysFalse.Or(alwaysFalse).Compile().Invoke(null));
            Assert.IsTrue(alwaysTrue.Or(alwaysFalse).Compile().Invoke(null));
            Assert.IsTrue(alwaysFalse.Or(alwaysTrue).Compile().Invoke(null));
            Assert.IsTrue(alwaysTrue.Or(alwaysTrue).Compile().Invoke(null));
        }

        [Test]
        public void OrAnd_ShouldCombineComplexConditionsProperly()
        {
            var sampleUser = new User
            {
                Id = 1,
                FirstName = "Adam",
                LastName = "Smith",
                Email = "adam.smith@test.com"
            };

            Expression<Func<User, bool>> firstNameTrueCondition = u => u.FirstName == "Adam";
            Expression<Func<User, bool>> firstNameFalseCondition = u => u.FirstName == "Zenon";

            Expression<Func<User, bool>> lastNameTrueCondition = u => u.LastName == "Smith";
            Expression<Func<User, bool>> lastNameFalseCondition = u => u.LastName == "Jones";

            Expression<Func<User, bool>> firstNameAlternative = firstNameFalseCondition.Or(firstNameTrueCondition);
            Expression<Func<User, bool>> lastNameAlternative = lastNameFalseCondition.Or(lastNameTrueCondition);

            Expression<Func<User, bool>> emailTrueCondition = u => u.Email.Contains("@");
            Expression<Func<User, bool>> emailFalseCondition = u => u.Email.Contains("#");

            Expression<Func<User, bool>> idTrueCondition = u => u.Id > 0;
            Expression<Func<User, bool>> idFalseCondition = u => u.Id < 0;

            Expression<Func<User, bool>> emailAlternative = emailFalseCondition.Or(emailTrueCondition);
            Expression<Func<User, bool>> idAlternative = idFalseCondition.Or(idTrueCondition);

            Expression<Func<User, bool>> mixedTrueCondition = idFalseCondition.And(emailFalseCondition).Or(emailTrueCondition);
            Expression<Func<User, bool>> mixedFalseCondition = emailTrueCondition.And(idFalseCondition.Or(emailFalseCondition));

            Expression<Func<User, bool>> nameConjunction = firstNameAlternative.And(lastNameAlternative);
            Expression<Func<User, bool>> grandConjunction = nameConjunction.And(emailAlternative).And(idAlternative);

            Assert.IsTrue(nameConjunction.Compile().Invoke(sampleUser));
            Assert.IsTrue(grandConjunction.Compile().Invoke(sampleUser));

            Assert.IsTrue(mixedTrueCondition.Compile().Invoke(sampleUser));
            Assert.IsFalse(mixedFalseCondition.Compile().Invoke(sampleUser));
        }

        [Test]
        public void Xor_ShouldCombineConditionsProperly()
        {
            Expression<Func<User, bool>> alwaysTrue = u => true;
            Expression<Func<User, bool>> alwaysFalse = u => false;

            Assert.IsFalse(alwaysFalse.Xor(alwaysFalse).Compile().Invoke(null));
            Assert.IsTrue(alwaysTrue.Xor(alwaysFalse).Compile().Invoke(null));
            Assert.IsTrue(alwaysFalse.Xor(alwaysTrue).Compile().Invoke(null));
            Assert.IsFalse(alwaysTrue.Xor(alwaysTrue).Compile().Invoke(null));
        }

        [Test]
        public void BuildSearchCondition_ShouldBuildProperExpressionForGuid()
        {
            var guid = new Guid("abe7d587-590f-4ac7-b324-6c0661e1ebed");
            Expression<Func<PublishedEventQueueItem, object>> conditionElement = x => x.BusinessEventId;

            var expression = DynamicQueryHelper.BuildSearchCondition(conditionElement, "abe7d587-590f");
            var containsCondition = expression.Compile();
            
            Assert.IsTrue(containsCondition(new PublishedEventQueueItem { BusinessEventId = guid }));          
        }

        [Test]
        public void BuildSearchCondition_ShouldBuildProperExpression()
        {
            var expression = DynamicQueryHelper.BuildSearchCondition<User>(u => u.LastName, "a");
            var containsCondition = expression.Compile();

            Assert.IsTrue(containsCondition(new User { LastName = "AAA" }));
            Assert.IsTrue(containsCondition(new User { LastName = "aaa" }));
            Assert.IsFalse(containsCondition(new User { LastName = "bbb" }));
            Assert.IsFalse(containsCondition(new User { LastName = "BBB" }));
        }

        [Test]
        public void BuildSearchCondition_ShouldHandleSelectMany()
        {
            var expression = DynamicQueryHelper.BuildSearchCondition<DynamicQueryHelperTestsClasses.DynamicQueryEntityMock>(u => u.Inner.SelectMany(w => w.Value), "1");
            var containsCondition = expression.Compile();

            Assert.IsTrue(containsCondition(new DynamicQueryHelperTestsClasses.DynamicQueryEntityMock
            {
                Inner = new[]
                {
                    new DynamicQueryHelperTestsClasses.DynamicQueryEntityValueMock
                    {
                        Value = new[]
                        {
                            "1",
                            "2",
                            "3"
                        }
                    }
                }
            }));
        }

        [Test]
        public void BuildSearchCondition_ShouldHandleNotExistingEnumValue()
        {
            var expression = DynamicQueryHelper.BuildSearchCondition<JobRunInfo>(j => j.Status, "notExisting");
            var containsCondition = expression.Compile();

            Assert.IsFalse(containsCondition(new JobRunInfo
            {
                Status = JobStatus.Aborted
            }));
        }

        [Test]
        public void BuildSearchCondition_ShouldWorkForNavigationProperites()
        {
            var expression = DynamicQueryHelper.BuildSearchCondition<User>(u => u.ImpersonatedBy.FirstName, "a");
            var containsCondition = expression.Compile();

            Assert.IsTrue(containsCondition(new User { ImpersonatedBy = new User { FirstName = "aa" } }));
        }

        [Test]
        public void GenerateEqualPredicate_ShouldWorkForStringValues()
        {
            var predicate = DynamicQueryHelper.GenerateEqualPredicate<User>("FirstName", "John");
            var user1 = new User { FirstName = "John" };
            var user2 = new User { FirstName = "Bob" };

            Assert.IsTrue(predicate.Compile().Invoke(user1));
            Assert.IsFalse(predicate.Compile().Invoke(user2));
        }

        [Test]
        public void GenerateEqualPredicate_ShouldWorkForLongValues()
        {
            var predicate = DynamicQueryHelper.GenerateEqualPredicate<User>("Id", 42L);
            var user1 = new User { Id = 42 };
            var user2 = new User { Id = 44 };

            Assert.IsTrue(predicate.Compile().Invoke(user1));
            Assert.IsFalse(predicate.Compile().Invoke(user2));
        }

        [Test]
        public void GenerateEqualPredicate_ShouldWorkForDateTimeValues()
        {
            var dateTimeNow = DateTime.Now;
            var dateTimeNotNow = dateTimeNow.AddDays(28);

            var predicate = DynamicQueryHelper.GenerateEqualPredicate<Credential>("CreatedOn", dateTimeNow);

            var credential1 = new Credential {CreatedOn = dateTimeNow};
            var credential2 = new Credential {CreatedOn = dateTimeNotNow};

            Assert.IsTrue(predicate.Compile().Invoke(credential1));
            Assert.IsFalse(predicate.Compile().Invoke(credential2));
        }
    }
}