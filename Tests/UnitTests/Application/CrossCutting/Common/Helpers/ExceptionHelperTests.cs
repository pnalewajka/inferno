﻿using System;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public class ExceptionHelperTests
    {
        private class InternalException : Exception
        {
            public InternalException(string message) : base(message)
            {
                
            }
        }

        [Test]
        public void GetFirstOfType_TestPositive()
        {
            var message = Guid.NewGuid().ToString();
            var exceptionInternal = new InternalException(message);
            var exceptionOuter = new ArgumentOutOfRangeException(string.Empty, exceptionInternal);
            var extractedException = ExceptionHelper.GetFirstOfType<InternalException>(exceptionOuter);
            Assert.IsNotNull(extractedException);
            Assert.AreEqual(message, extractedException.Message);
        }
    }
}
