﻿using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using UnitTests.Application.CrossCutting.Common.Helpers.Resources;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public class ResourceHelperTests
    {
        [Test, TestCaseSource(typeof(ResourceHelperTestCasesFactory), nameof(ResourceHelperTestCasesFactory.GetResourceGetStreamByFullNameTestCases))]
        public string GetResource_GetStreamByFullName(string resourceName, Assembly assembly)
        {
            using (var streamReader = new StreamReader(ResourceHelper.GetResource(resourceName, assembly)))
            {
                return streamReader.ReadToEnd();
            }
        }

        [Test]
        public void GetResourceAsString_TestShouldPass()
        {
            var existingResourceName = $"{typeof(ResourceHelperTestCasesFactory).Namespace}.Resources.Resource_01.txt";
            Assert.AreEqual(ResourceHelperTestCasesFactory.ResourceFileContents,
                ResourceHelper.GetResourceAsString(existingResourceName));
        }

        [Test, ExpectedException(typeof(ResourceNotFoundException))]
        public void GetResourceAsString_TestThrowsException()
        {
            var missingResourceName = Guid.NewGuid().ToString();
            ResourceHelper.GetResourceAsString(missingResourceName);
        }

        [Test]
        public void GetResourceAsStringGeneric_TestShouldPass()
        {
            const string existingResourceName = "Resources.Resource_01.txt";
            Assert.AreEqual(ResourceHelperTestCasesFactory.ResourceFileContents,
                ResourceHelper.GetResourceAsStringByShortName(existingResourceName, typeof(ResourceHelperTestCasesFactory)));
        }

        [Test]
        public void GetResourceAsStringGeneric_BomTestShouldPass()
        {
            const string existingResourceName = "Resources.Resource_02.txt";
            Assert.AreEqual(ResourceHelperTestCasesFactory.ResourceFileContents,
                ResourceHelper.GetResourceAsStringByShortName(existingResourceName, typeof(ResourceHelperTestCasesFactory)));
        }

        [Test, ExpectedException(typeof(ResourceNotFoundException))]
        public void GetResourceAsStringGeneric_TestThrowsException()
        {
            var missingResourceName = Guid.NewGuid().ToString();
            ResourceHelper.GetResourceAsStringByShortName(missingResourceName, typeof(ResourceHelperTestCasesFactory));
        }

        [Test]
        public void GetResourceNameEn_TestShouldPass()
        {
            var typeResourceManager = typeof(SampleResources);
            const string bigMistakeInEnglish = "big mistake";

            Assert.AreEqual(bigMistakeInEnglish, ResourceHelper.GetString(typeResourceManager, "BigMistake", CultureInfo.GetCultureInfo(CultureCodes.English)));
        }

        [Test]
        public void GetResourceNamePl_TestShouldPass()
        {
            var typeResourceManager = typeof(SampleResources);
            const string bigMistakeInPolish = "duży błąd";

            Assert.AreEqual(bigMistakeInPolish, ResourceHelper.GetString(typeResourceManager, "BigMistake", CultureInfo.GetCultureInfo(CultureCodes.Polish)));
        }
    }
}
