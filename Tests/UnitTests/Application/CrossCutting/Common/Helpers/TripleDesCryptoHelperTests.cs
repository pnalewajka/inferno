﻿using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public class TripleDesCryptoHelperTests
    {
        [Test]
        public void Encrypt_RoundtripTest()
        {
            const string password = "Password!@#";
            const string content = "Sample content !@#!%#%#%!#$";
            var encryptedContent = TripleDesCryptoHelper.Encrypt(content, password);
            var decodedString = TripleDesCryptoHelper.Decrypt(encryptedContent, password);
            Assert.AreEqual(content, decodedString);
        }
    }
}
