﻿using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public class HashHelperTests
    {
        [Test]
        public void HashHelper_TestPositive()
        {
            const string expectedHash = "ba3107be218cc3174ee182961e1865d6";
            const string content = "test contet for hash calculation";
            var calculatedHash = HashHelper.CalculateMd5Hash(content);
            Assert.AreEqual(expectedHash, calculatedHash);
        }

        [Test]
        public void CalculateSha1Hash_TestPositive()
        {
            const string expectedHash = "9c5c0f39d7b2e02cc27305c973c45055187e8f6f";
            const string content = "test contet for hash calculation";
            var calculatedHash = HashHelper.CalculateSha1Hash(content);
            Assert.AreEqual(expectedHash, calculatedHash);
        }
    }
}
