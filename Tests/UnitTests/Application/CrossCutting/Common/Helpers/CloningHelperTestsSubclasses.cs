﻿using System;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    public partial class CloningHelperTests
    {
        private class TestPoco
        {
            public int IntValue { get; set; }
            public long? LongValue { get; set; }
            public DateTime DateTimeValue { get; set; }
        }
    }
}
