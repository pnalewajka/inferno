﻿using System;
using System.Linq;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public class StringHelperTests
    {
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Join_ShouldThrowArgumentNullExceptionForNullSeparator()
        {
            // ReSharper disable ExpressionIsAlwaysNull
            StringHelper.Join(null, new string[0]);
            // ReSharper restore ExpressionIsAlwaysNull
        }

        [Test]
        public void Join_ShouldReturnEmptyStringForNoItems()
        {
            Assert.AreEqual(string.Empty, StringHelper.Join(","));
        }

        [Test]
        public void Join_ShouldReturnEmptyStringForNoNotNullItems()
        {
            Assert.AreEqual(string.Empty, StringHelper.Join(",", null, string.Empty));
        }

        [Test]
        public void Join_ShouldJoinNonEmptyItems()
        {
            Assert.AreEqual("Mick Jagger", StringHelper.Join(" ", "Mick", "Jagger"));
        }

        [Test]
        public void Join_ShouldJoinOnlyNonEmptyItems()
        {
            Assert.AreEqual("Mick Jagger", StringHelper.Join(" ", "Mick", null, "Jagger"));
        }

        [Test, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void GetRandomString_ShouldThrowExceptionForInvalidLength()
        {
            StringHelper.GetRandomString(-1);
        }

        [Test, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void GetRandomString2_ShouldThrowExceptionForInvalidMinLength()
        {
            StringHelper.GetRandomString(-1, 5);
        }

        [Test, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void GetRandomString2_ShouldThrowExceptionForMaxLengthSmallerThanMinLength()
        {
            StringHelper.GetRandomString(2, 1);
        }

        [Test, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void GetRandomString_ShouldThrowExceptionForEmptyAllowedCharacters()
        {
            StringHelper.GetRandomString(5, new char[0]);
        }

        [Test, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void GetRandomString2_ShouldThrowExceptionForEmptyAllowedCharacters()
        {
            StringHelper.GetRandomString(5, 8, new char[0]);
        }

        [Test]
        public void GetRandomString_ShouldReturnEmptyStringForZeroLength()
        {
            var result = StringHelper.GetRandomString(0);
            Assert.AreEqual(string.Empty, result);
        }

        [Test]
        public void GetRandomString2_ShouldReturnEmptyStringForZeroLength()
        {
            var result = StringHelper.GetRandomString(0, 0);
            Assert.AreEqual(string.Empty, result);
        }

        [Test]
        public void GetRandomString_ShouldReturnStringOfSpecifiedLength()
        {
            var result = StringHelper.GetRandomString(13);
            Assert.AreEqual(13, result.Length);
        }

        [Test]
        public void GetRandomString2_ShouldReturnStringOfLengthWithinSpecifiedBounds()
        {
            var result = StringHelper.GetRandomString(13, 21);

            Assert.IsTrue(13 <= result.Length);
            Assert.IsTrue(result.Length < 21);
        }

        [Test]
        public void GetRandomString2_ShouldReturnStringOfExactlySpecifiedLength()
        {
            var result = StringHelper.GetRandomString(13, 13);
            Assert.AreEqual(13, result.Length);
        }

        [Test]
        public void GetRandomString_ShouldReturnAllAsBsOrCs()
        {
            var allowedCharacters = new[] {'A', 'B', 'C'};
            var result = StringHelper.GetRandomString(34, allowedCharacters);

            foreach (var c in result)
            {
                Assert.IsTrue(allowedCharacters.Contains(c));
            }
        }

        [Test]
        public void GetRandomString2_ShouldReturnAllAsBsOrCs()
        {
            const string allowedCharacters = "ABC";
            var result = StringHelper.GetRandomString(34, 55, allowedCharacters);

            foreach (var c in result)
            {
                Assert.IsTrue(allowedCharacters.Contains(c));
            }
        }       

        [Test]
        public void ReplaceDiacritics_ShouldReplaceDiacritics()
        {
            var value = "ĘÓĄŚŁŻŹĆŃ ęóąśłżźćń Ðàäèî Êóëüòóà";
            var result = StringHelper.RemoveDiacritics(value);

            Assert.AreEqual("EOASLZZCN eoaslzzcn Daaei Eoeuooa", result);
        }

        [Test]
        public void ReplaceDiacritics_TestPositiveIfEmpty()
        {
            var value = string.Empty;
            var result = StringHelper.RemoveDiacritics(value);

            Assert.IsEmpty(result);
        }

        [Test]
        public void ReplaceDiacritics_ShouldThrowExceptionIfValueIsNull()
        {
            var exception = Assert.Throws<ArgumentNullException>(() =>
            {
                StringHelper.RemoveDiacritics(null);
            });

            Console.WriteLine(exception);
        }

        [Test]
        public void ReplaceDiacritics_ShouldNotReplaceSpecialCharacter()
        {
            var value = @"!@#$%^&*()[]{}\|;:,.<>~`'""";
            var result = StringHelper.RemoveDiacritics(value);

            Assert.AreEqual(value, result);
        }

        [Test]
        public void ReplaceDiacritics_ShouldKeepNonAccentedStringAsIs()
        {
            const string mickJagger = "Mick Jagger";
            Assert.AreEqual(mickJagger, StringHelper.RemoveDiacritics(mickJagger));
        }

        [Test]
        public void ReplaceDiacritics_ShouldReplaceFrenchAccents()
        {
            const string pangram = "Dès Noël où un zéphyr haï me vêt de glaçons würmiens je dîne d’exquis rôtis de bœuf au kir à l’aÿ d’âge mûr & cætera";
            Assert.AreEqual("Des Noel ou un zephyr hai me vet de glacons wurmiens je dine d'exquis rotis de boeuf au kir a l'ay d'age mur & caetera", StringHelper.RemoveDiacritics(pangram));
        }

        [Test]
        public void ReplaceDiacritics_ShouldReplaceGermanAccents()
        {
            const string pangram = "Verbüß öd’ Joch, kämpf Qual, zwing Styx!";
            Assert.AreEqual("Verbuss od' Joch, kampf Qual, zwing Styx!", StringHelper.RemoveDiacritics(pangram));
        }

        [Test]
        public void ReplaceDiacritics_ShouldReplacePolishAccents()
        {
            const string pangram = "Pchnąć w tę łódź jeża lub ośm skrzyń fig";
            Assert.AreEqual("Pchnac w te lodz jeza lub osm skrzyn fig", StringHelper.RemoveDiacritics(pangram));
        }

        [Test]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TruncateAtWords_ShouldThrowExceptionForNegativeLimit()
        {
            StringHelper.TruncateAtWords("XYZ", -1);
        }

        [Test]
        public void TruncateAtWords_ShouldReturnNullForNullInput()
        {
            Assert.IsNull(StringHelper.TruncateAtWords(null, 10));
        }

        [Test]
        public void TruncateAtWords_ShouldReturnInputIfWithinLengthLimit()
        {
            const string input = "Lorem ipsum dolor sit amet";

            Assert.AreEqual(input, StringHelper.TruncateAtWords(input, input.Length));
        }

        [Test]
        public void TruncateAtWords_ShouldUseWordSeparatorsToTruncate()
        {
            const string input = "Lorem ipsum dolor sit amet";

            Assert.AreEqual("Lorem ipsum…", StringHelper.TruncateAtWords(input, 14));
        }

        [Test]
        public void TruncateAtWords_ShouldHandleOneWordTexts()
        {
            const string input = "LoremIpsumDolorSitAmet";

            Assert.AreEqual("LoremIpsumDol…", StringHelper.TruncateAtWords(input, 14));
        }

        [Test]
        public void TruncateAtWords_ShouldHandleLimitEqualOne()
        {
            const string input = "Lorem ipsum dolor sit amet";

            Assert.AreEqual("…", StringHelper.TruncateAtWords(input, 1));
        }
    }
}  