﻿using System;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public class HtmlHelperTests
    {
        [Test]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TruncateAtWords_ShouldThrowExceptionForNegativeLimit()
        {
            HtmlHelper.TruncateAtWords("XYZ", -1);
        }

        [Test]
        public void TruncateAtWords_ShouldReturnNullForNullInput()
        {
            Assert.IsNull(HtmlHelper.TruncateAtWords(null, 10));
        }

        [Test]
        public void TruncateAtWords_ShouldReturnInputIfWithinLengthLimit()
        {
            const string input = "<span>Lorem ipsum dolor</span> sit amet";

            Assert.AreEqual(input, HtmlHelper.TruncateAtWords(input, input.Length - 13));
        }

        [Test]
        public void TruncateAtWords_ShouldUseWordSeparatorsToTruncate()
        {
            const string input = "<span>Lorem ipsum dolor</span> sit amet";

            Assert.AreEqual("<span>Lorem ipsum…</span>", HtmlHelper.TruncateAtWords(input, 14));
        }

        [Test]
        public void TruncateAtWords_ShouldHandleOneWordTexts()
        {
            const string input = "<span>LoremIpsumDolorSitAmet</span>";

            Assert.AreEqual("<span>LoremIpsumDol…</span>", HtmlHelper.TruncateAtWords(input, 14));
        }

        [Test]
        public void TruncateAtWords_ShouldHandleLimitEqualOne()
        {
            const string input = "<span>Lorem ipsum dolor</span> sit amet";

            Assert.AreEqual("<span>…</span>", HtmlHelper.TruncateAtWords(input, 1));
        }

        [Test]
        public void TruncateAtWords_ShouldHandleComplexMarkup()
        {
            const string input = "<span>Hello <u>there, <i>Mrs.</i> <b>Robinson</b>, nice day</u>,</span> <span>isn’t <strong>it</strong></span><sup>?</sup></span>";

            Assert.AreEqual("<span>Hello <u>there, <i>Mrs.</i> <b>Robinson</b>, nice…</u></span>", HtmlHelper.TruncateAtWords(input, 34));
        }

        [Test]
        public void TruncateAtWords_ShouldCloseMultipleTags()
        {
            const string input = "<span>Hello <u>there, <i>Mrs. </i><b>Robinson</b>, nice day</u>,</span> <span>isn’t <strong>it</strong></span><sup>?</sup></span>";

            Assert.AreEqual("<span>Hello <u>there, <i>Mrs.…</i></u></span>", HtmlHelper.TruncateAtWords(input, 18));
        }

        [Test]
        public void TruncateAtWords_ShouldHandleTagsWithAttributes()
        {
            const string input = "Do you want to know <a href=\"http://www.google.com/search?q=something+awesome\" target=\"_blank\">why is it 42</a>? Don't be shy... I mean, <b>c'mon</b>!";

            Assert.AreEqual("Do you want to know <a href=\"http://www.google.com/search?q=something+awesome\" target=\"_blank\">why…</a>", HtmlHelper.TruncateAtWords(input, 26));
        }

        [Test]
        public void TruncateAtWords_ShouldHandleTextOutsideTags()
        {
            const string input = "Do you want to know <a href=\"http://www.google.com/search?q=something+awesome\" target=\"_blank\">why is it 42</a>? Don't be shy... I mean, <b>c'mon</b>!";

            Assert.AreEqual("Do you want to know <a href=\"http://www.google.com/search?q=something+awesome\" target=\"_blank\">why is it 42</a>? Don't be…", HtmlHelper.TruncateAtWords(input, 46));
        }
    }
}