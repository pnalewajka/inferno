﻿using System;
using System.Collections;
using System.Linq;
using System.Net;
using System.Xml.Linq;
using System.Xml.XPath;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public class XmlSerializationHelperTests
    {
        [Serializable]
        public class Test
        {
            public int IntValue1 { get; set; }
            public string StringValue1 { get; set; }
        }

        public class TestClass1
        {
            public string TestValue1 { get; set; }
        }

        [Test]
        public void SerializeToXml_TestSerializationAndDeserialization()
        {
            var dataObject = new Test
            {
                IntValue1 = 123,
                StringValue1 = Guid.NewGuid().ToString(),
            };
            var xml = XmlSerializationHelper.SerializeToXml(dataObject, typeof (Test));
            var deserializedObject = XmlSerializationHelper.DeserializeFromXml(xml, typeof (Test)) as Test;
            Assert.IsNotNull(deserializedObject);
            Assert.AreEqual(dataObject.IntValue1, deserializedObject.IntValue1);
            Assert.AreEqual(dataObject.StringValue1, deserializedObject.StringValue1);
        }

        [Test]
        public void ToXElement_PositiveTest()
        {
            var guidValue = Guid.NewGuid().ToString();
            var objectToSerialize = new TestClass1 {TestValue1 = guidValue};
            var xElement = XmlSerializationHelper.SerializeToXElement(objectToSerialize);
            const string searchForNode = "//TestValue1/text()";
            var iEnumerableNodes = xElement.XPathEvaluate(searchForNode) as IEnumerable;
            Assert.IsNotNull(iEnumerableNodes);
            var foundTextNodes = iEnumerableNodes.OfType<XText>().ToList();
            const int expectedListSize = 1;
            Assert.AreEqual(expectedListSize, foundTextNodes.Count);
            Assert.AreEqual(guidValue, foundTextNodes.First().Value);
        }
    }
}