﻿using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Application.CrossCutting.Common.Helpers
{
    [TestFixture]
    public class CompilationHelperTests
    {
        [Test]
        public void CompileSourceCode_ShouldCompileEnum()
        {
            const string sourceCode = "public enum OneTwoThree { One = 100, Two = 200, Three = 300 }";

            var assembly = CompilationHelper.Compile(sourceCode, new string[0]);
            var type123 = assembly.GetType("OneTwoThree");

            Assert.IsTrue(type123.IsEnum);
        }

        [Test]
        public void CompileSourceCode_ShouldCompileStaticClass()
        {
            const string sourceCode = "using System.Linq; using System.Text; public static class SampleClass {public static string GetSomething() { var sb = new StringBuilder(); for (var i = 0; i < 10; i++) { if (i > 0) sb.Append(\";\"); sb.Append(string.Join(\",\", Enumerable.Range(0, i + 1))); } return sb.ToString(); } }";

            var assembly = CompilationHelper.Compile(sourceCode, new[] { "System.Core.dll" });
            var sampleClass = assembly.GetType("SampleClass");

            var getSomething = sampleClass.GetMethod("GetSomething");

            var result = (string) getSomething.Invoke(null, null);

            Assert.IsTrue(result.StartsWith("0;0,1;0,1,2;0,1,2,3;"));
            Assert.IsTrue(result.EndsWith(";0,1,2,3,4,5,6,7,8,9"));
        }

        [Test]
        public void CompileFiles_ShouldCompileStaticClass()
        {
            var fileNames = new[] {@"Application\CrossCutting\Common\Helpers\Resources\SampleClass.txt"};

            var assembly = CompilationHelper.Compile(fileNames, new[] { "System.Core.dll" });
            var sampleClass = assembly.GetType("SampleClass");

            var getSomething = sampleClass.GetMethod("GetSomething");

            var result = (string)getSomething.Invoke(null, null);

            Assert.IsTrue(result.StartsWith("0;0,1;0,1,2;0,1,2,3;"));
            Assert.IsTrue(result.EndsWith(";0,1,2,3,4,5,6,7,8,9"));
        }
    }
}