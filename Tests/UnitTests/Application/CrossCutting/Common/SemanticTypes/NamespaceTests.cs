﻿using System;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.SemanticTypes;

namespace UnitTests.Application.CrossCutting.Common.SemanticTypes
{
    [TestFixture]
    public class NamespaceTests
    {
        [Test]
        public void Namespace_ValidationTestPositive()
        {
            var ns = new Namespace(GetType().Namespace);
            Assert.IsNotNull(ns);
        }

        [Test, ExpectedException(typeof(ArgumentException))]
        public void Namespace_ValidationTestException()
        {
// ReSharper disable ObjectCreationAsStatement
            new Namespace(Guid.NewGuid().ToString());
// ReSharper restore ObjectCreationAsStatement
        }

    }
}
