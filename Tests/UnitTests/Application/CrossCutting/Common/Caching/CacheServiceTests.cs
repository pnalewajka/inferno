﻿using System;
using System.Threading;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Moq;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Caching;

namespace UnitTests.Application.CrossCutting.Common.Caching
{
    [TestFixture]
    public class CacheServiceTests
    {
        private const string CacheArea = "TestArea";
        
        private WindsorContainer _container;
        private Mock<ICacheInvalidationNotifications> _cacheInvalidationNotificationsMock;

        [SetUp]
        public void SetUp()
        {
            _container = new WindsorContainer();
            _container.Register(Component.For<CacheInterceptor>().LifestyleTransient().Named("Cache"));
            _container.Register(Component.For<ICacheBackroundProcessingService>().ImplementedBy<CacheBackroundProcessingService>().LifestyleTransient());
            _container.Register(Component.For<ICacheService, ICacheInvalidationService>().ImplementedBy<CacheService>().LifestyleSingleton());
            _container.Register(Component.For<ICacheTestService>().ImplementedBy<CacheTestService>().LifestyleTransient());
            _cacheInvalidationNotificationsMock = new Mock<ICacheInvalidationNotifications>();
            _cacheInvalidationNotificationsMock.Setup(m => m.GetLastInvalidationToken(CacheArea)).Returns(Guid.Empty);
            _cacheInvalidationNotificationsMock.Setup(m => m.NotifyAboutAreaInvalidation(CacheArea));
            _container.Register(Component.For<ICacheInvalidationNotifications>().Instance(_cacheInvalidationNotificationsMock.Object));
            _container.Resolve<ICacheInvalidationService>().Invalidate();
        }

        [Test]
        public void MethodResultShouldBeCached()
        {
            var testService = _container.Resolve<ICacheTestService>();
            Assert.AreEqual(0, testService.GetNext());
            Assert.AreEqual(0, testService.GetNext());
        }

        [Test]
        public void AfterSometimeCacheShouldExpire()
        {
            var testService = _container.Resolve<ICacheTestService>();

            Assert.AreEqual(0, testService.GetNext());
            Thread.Sleep(2000);
            Assert.AreEqual(1, testService.GetNext());
            Assert.AreEqual(1, testService.GetNext());
        }

        [Test]
        public void InvalidateShouldClearCache()
        {
            var testService = _container.Resolve<ICacheTestService>();
            var cacheInvalidationService = _container.Resolve<ICacheInvalidationService>();

            Assert.AreEqual(0, testService.GetNext());
            cacheInvalidationService.InvalidateArea(CacheArea);
            Assert.AreEqual(1, testService.GetNext());
            Assert.AreEqual(1, testService.GetNext());
        }

        [Test]
        public void ExternalInvalidationShouldClearCache()
        {
            var testService = _container.Resolve<ICacheTestService>();

            Assert.AreEqual(0, testService.GetNext());
            _cacheInvalidationNotificationsMock.Setup(m => m.GetLastInvalidationToken(CacheArea)).Returns(Guid.NewGuid);
            CacheService.MaxInvalidationCheckIntervalInSeconds = 0;
            Thread.Sleep(1500);
            Assert.AreEqual(1, testService.GetNext());
        }
    }
}
