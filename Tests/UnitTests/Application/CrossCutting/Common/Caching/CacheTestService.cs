using Castle.Core;
using Smt.Atomic.CrossCutting.Common.Caching;

namespace UnitTests.Application.CrossCutting.Common.Caching
{
    [Interceptor(typeof(CacheInterceptor))]
    internal class CacheTestService : ICacheTestService
    {
        private int _counter;

        [Cached(CacheArea = "TestArea", ExpirationInSeconds = 1)]
        public long GetNext()
        {
            return _counter++;
        }
    }
}