﻿namespace UnitTests.Application.CrossCutting.Common.Caching
{
    public interface ICacheTestService
    {
        long GetNext();
    }
}