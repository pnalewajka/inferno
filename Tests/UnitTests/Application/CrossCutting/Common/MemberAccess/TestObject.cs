﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests.Application.CrossCutting.Common.MemberAccess
{
    internal class TestObject
    {
        public object Property1 { get; set; }

        public int Property2 { get; set; }

        public object Result1 { private get; set; }

        public IList Elements { private get; set; }

        public object Method1()
        {
            return Result1;
        }

        public string Method2(int x, string prefix)
        {
            return prefix + x;
        }

        public object this[int index] => Elements[index];

        public override string ToString()
        {
            return "This is a test object";
        }
    }
}
