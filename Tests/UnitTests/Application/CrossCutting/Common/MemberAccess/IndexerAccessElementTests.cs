﻿using System;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.MemberAccess;
using System.Collections.Generic;

namespace UnitTests.Application.CrossCutting.Common.MemberAccess
{
    [TestFixture]
    public class IndexerAccessElementTests
    {
        [Test]
        public void IndexerAccessElement_TryParse()
        {
            var indexerAccessElement = IndexerAccessElement.TryParse("[1]");
            Assert.AreEqual(1, indexerAccessElement.Index);

            indexerAccessElement = IndexerAccessElement.TryParse("[2]");
            Assert.AreEqual(2, indexerAccessElement.Index);

            indexerAccessElement = IndexerAccessElement.TryParse("5");
            Assert.IsNull(indexerAccessElement);

            indexerAccessElement = IndexerAccessElement.TryParse("X");
            Assert.IsNull(indexerAccessElement);
        }

        [Test]
        public void PropertyAccessElement_ToString()
        {
            var indexerAccessElement = new IndexerAccessElement(1);
            Assert.AreEqual("[1]", indexerAccessElement.ToString());

            indexerAccessElement = new IndexerAccessElement(2);
            Assert.AreEqual("[2]", indexerAccessElement.ToString());
        }

        [Test]
        public void PropertyAccessElement_GetValueOf()
        {
            var testObject1 = new TestObject
            {
                Elements = new[] {7, 8}
            };

            var testObject2 = new TestObject
            {
                Elements = new List<string> {"one", "two"}
            };
         
            var indexerAccessElement = new IndexerAccessElement(0);
            Assert.AreEqual(7, indexerAccessElement.GetValueOf(testObject1));
            Assert.AreEqual("one", indexerAccessElement.GetValueOf(testObject2));

            indexerAccessElement = new IndexerAccessElement(1);
            Assert.AreEqual(8, indexerAccessElement.GetValueOf(testObject1));
            Assert.AreEqual("two", indexerAccessElement.GetValueOf(testObject2));

            indexerAccessElement = new IndexerAccessElement(2);
            Assert.Throws<IndexOutOfRangeException>(() => indexerAccessElement.GetValueOf(testObject1));
            Assert.Throws<ArgumentOutOfRangeException>(() => indexerAccessElement.GetValueOf(testObject2));

            var testObject3 = new object();
            Assert.Throws<InvalidOperationException>(() => indexerAccessElement.GetValueOf(testObject3));
        }
    }
}
