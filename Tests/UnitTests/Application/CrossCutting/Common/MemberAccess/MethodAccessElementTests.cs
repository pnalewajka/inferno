﻿using System;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.MemberAccess;

namespace UnitTests.Application.CrossCutting.Common.MemberAccess
{
    [TestFixture]
    public class MethodAccessElementTests
    {
        [Test]
        public void MethodAccessElement_TryParse()
        {
            var propertyAccessElement = MethodAccessElement.TryParse("Method1()");
            Assert.AreEqual("Method1", propertyAccessElement.MethodName);
            CollectionAssert.AreEqual(new string[0], propertyAccessElement.Arguments);

            propertyAccessElement = MethodAccessElement.TryParse(".Method2(12, somearg,x)");
            Assert.AreEqual("Method2", propertyAccessElement.MethodName);
            CollectionAssert.AreEqual(new string[] {"12", "somearg", "x"}, propertyAccessElement.Arguments);

            propertyAccessElement = MethodAccessElement.TryParse("Method3");
            Assert.IsNull(propertyAccessElement);

            propertyAccessElement = MethodAccessElement.TryParse("(1, 24)");
            Assert.IsNull(propertyAccessElement);
        }

        [Test]
        public void MethodAccessElement_ToString()
        {
            var propertyAccessElement = new MethodAccessElement("Method1", new string[0]);
            Assert.AreEqual("Method1()", propertyAccessElement.ToString());

            propertyAccessElement = new MethodAccessElement("Method2", new[] {"2", "somearg", "x"});
            Assert.AreEqual("Method2(2,somearg,x)", propertyAccessElement.ToString());
        }

        [Test]
        public void MethodAccessElement_GetValueOf()
        {
            var testObject = new TestObject
            {
                Result1 = 42
            };

            var propertyAccessElement = new MethodAccessElement("Method1", new string[0]);
            Assert.AreEqual(42, propertyAccessElement.GetValueOf(testObject));

            propertyAccessElement = new MethodAccessElement("Method2", new[] { "2", "arg-" });
            Assert.AreEqual("arg-2", propertyAccessElement.GetValueOf(testObject));

            propertyAccessElement = new MethodAccessElement("ToString", new string[0]);
            Assert.AreEqual("42", propertyAccessElement.GetValueOf(42));
            Assert.AreEqual("This is a test object", propertyAccessElement.GetValueOf(testObject));

            propertyAccessElement = new MethodAccessElement("Method2", new[] { "1", "2", "3" });
            Assert.Throws<InvalidOperationException>(() => propertyAccessElement.GetValueOf(testObject));

            propertyAccessElement = new MethodAccessElement("Method2", new[] { "x", "arg-" });
            Assert.Throws<InvalidOperationException>(() => propertyAccessElement.GetValueOf(testObject));

            propertyAccessElement = new MethodAccessElement("Method3", new string[0]);
            Assert.Throws<InvalidOperationException>(() => propertyAccessElement.GetValueOf(testObject));
        }
    }
}
