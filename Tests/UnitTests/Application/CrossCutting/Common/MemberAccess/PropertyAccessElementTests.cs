﻿using System;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.MemberAccess;

namespace UnitTests.Application.CrossCutting.Common.MemberAccess
{
    [TestFixture]
    public class PropertyAccessElementTests
    {
        [Test]
        public void PropertyAccessElement_TryParse()
        {
            var propertyAccessElement = PropertyAccessElement.TryParse("Prop1");
            Assert.AreEqual("Prop1", propertyAccessElement.PropertyName);

            propertyAccessElement = PropertyAccessElement.TryParse(".Prop2");
            Assert.AreEqual("Prop2", propertyAccessElement.PropertyName);

            propertyAccessElement = PropertyAccessElement.TryParse("1st");
            Assert.IsNull(propertyAccessElement);
        }

        [Test]
        public void PropertyAccessElement_ToString()
        {
            var propertyAccessElement = new PropertyAccessElement("Prop1");
            Assert.AreEqual("Prop1", propertyAccessElement.ToString());

            propertyAccessElement = new PropertyAccessElement("Prop2");
            Assert.AreEqual("Prop2", propertyAccessElement.ToString());
        }

        [Test]
        public void PropertyAccessElement_GetValueOf()
        {
            var testObject = new TestObject
            {
                Property1 = "1st value",
                Property2 = 42
            };

            var propertyAccessElement = new PropertyAccessElement("Property1");
            Assert.AreEqual("1st value", propertyAccessElement.GetValueOf(testObject));

            propertyAccessElement = new PropertyAccessElement("Property2");
            Assert.AreEqual(42, propertyAccessElement.GetValueOf(testObject));

            propertyAccessElement = new PropertyAccessElement("Property3");
            Assert.Throws<InvalidOperationException>(() => propertyAccessElement.GetValueOf(testObject));
        }
    }
}
