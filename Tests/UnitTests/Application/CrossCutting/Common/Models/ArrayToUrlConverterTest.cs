﻿using Newtonsoft.Json;
using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Models;
using System.IO;

namespace UnitTests.Application.CrossCutting.Common.Models
{
    [TestFixture]
    public class ArrayToUrlConverterTest
    {
        [Test]
        public void CanConvert_ShouldWorkForArrays()
        {
            var converter = new ArrayToUrlConverter();

            Assert.IsTrue(converter.CanConvert(typeof(long[])), "Should work for array of long");
            Assert.IsTrue(converter.CanConvert(typeof(int[])), "Should work for array of int");
            Assert.IsTrue(converter.CanConvert(typeof(double[])), "Should work for array of double");

            Assert.IsFalse(converter.CanConvert(typeof(string)), "Should not work for string");
            Assert.IsFalse(converter.CanConvert(typeof(long)), "Should not work for long");
            Assert.IsFalse(converter.CanConvert(typeof(int)), "Should not work for int");
            Assert.IsFalse(converter.CanConvert(typeof(double)), "Should not work for double");
        }

        [Test]
        public void WriteJson_ShouldWorkForSimpleArray()
        {
            var converter = new ArrayToUrlConverter();
            var stringWriter = new StringWriter();
            var jsonSerializer = new JsonSerializer();
            var jsonWriter = new JsonTextWriter(stringWriter);

            converter.WriteJson(jsonWriter, new long[] { 1, 2, 3, 4 }, jsonSerializer);

            Assert.AreEqual("\"1,2,3,4\"", stringWriter.ToString());
        }

        [Test]
        public void ReadJson_ShouldWorkForSimpleArray()
        {
            var converter = new ArrayToUrlConverter();
            var stringReader = new StringReader("\"1,2,3,4\"");
            var jsonSerializer = new JsonSerializer();
            var jsonReader = new JsonTextReader(stringReader);

            var result = converter.ReadJson(jsonReader, typeof(long[]), null, jsonSerializer);

            Assert.AreEqual(new long[] { 1, 2, 3, 4 }, result);
        }
    }
}
