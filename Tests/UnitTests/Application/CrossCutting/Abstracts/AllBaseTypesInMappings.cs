﻿using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using System;

namespace UnitTests.Application.CrossCutting.Abstracts
{
    public class BaseTypes
    {
        public string Text { get; set; }

        public long Long { get; set; }

        public DateTime DateTime { get; set; }

        public TimeSpan Timespan { get; set; }

        public byte[] ByteArray { get; set; }

        public bool IsActive { get; set; }

        public int? NullableNumber { get; set; }

        public UserPicture Picture { get; set; }
    }

    public class BaseTypesDto
    {
        public string Text { get; set; }

        public long Long { get; set; }

        public DateTime DateTime { get; set; }

        public TimeSpan Timespan { get; set; }

        public byte[] ByteArray { get; set; }

        public bool IsActive { get; set; }

        public int? NullableNumber { get; set; }

        public DocumentDto Picture { get; set; }
    }

    public class BaseTypesToBaseTypesDtoMapping : ClassMapping<BaseTypes, BaseTypesDto>
    {
        public BaseTypesToBaseTypesDtoMapping()
        {
            Mapping = e => new BaseTypesDto
            {
                Text = e.Text,
                Long = e.Long,
                DateTime = e.DateTime,
                Timespan = e.Timespan,
                ByteArray = e.ByteArray,
                IsActive = e.IsActive,
                NullableNumber = e.NullableNumber,
                Picture = new DocumentDto
                {
                    ContentType = e.Picture.ContentType,
                    DocumentName = e.Picture.Name,
                    DocumentId = e.Picture.Id
                }
            };
        }
    }
}
