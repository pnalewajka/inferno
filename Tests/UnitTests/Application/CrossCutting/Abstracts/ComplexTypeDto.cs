﻿using Smt.Atomic.CrossCutting.Common.Models;

namespace UnitTests.Application.CrossCutting.Abstracts
{
    public class ComplexTypeDto
    {
        public LocalizedString ComplexValue { get; set; }

        public string SimpleValue { get; set; }
    }
}
