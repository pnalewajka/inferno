﻿using System.Linq;
using NUnit.Framework;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.WebApp.Areas.Accounts.Models;
using Smt.Atomic.WebApp.Areas.Configuration.Models;
using Smt.Atomic.WebApp.Areas.Scheduling.Models;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace UnitTests.Application.CrossCutting.Abstracts
{
    [TestFixture]
    public class ClassMappingTests
    {
        [Test]
        public void UserToUserDtoMapping_CorrectIncludes()
        {
            // Arrange
            var mapping = new UserToUserDtoMapping();

            // Act
            // Assert
            CollectionAssert.AreEquivalent(new [] {"Documents"}, mapping.NavigationProperties);
        }

        [Test]
        public void JobLogViewModelToJobLogDtoMapping_CorrectIncludes()
        {
            // Arrange
            var mapping = new JobLogViewModelToJobLogDtoMapping();

            // Act
            // Assert
            CollectionAssert.AreEquivalent(Enumerable.Empty<string>(), mapping.NavigationProperties);
        }

        [Test]
        public void UserViewModelToUserDtoMapping_CorrectIncludes()
        {
            // Arrange
            var mapping = new UserViewModelToUserDtoMapping();

            // Act
            // Assert
            CollectionAssert.AreEquivalent(Enumerable.Empty<string>(), mapping.NavigationProperties);
        }

        [Test]
        public void PlainTextMessageTemplateViewModelToPlainTextMessageTemplateDtoMapping_CorrectIncludes()
        {
            // Arrange
            var mapping = new PlainTextMessageTemplateViewModelToPlainTextMessageTemplateDtoMapping();

            // Act
            // Assert
            CollectionAssert.AreEquivalent(Enumerable.Empty<string>(), mapping.NavigationProperties);
        }

        [Test]
        public void DataFilterViewModelToDataFilterDtoMapping_CorrectIncludes()
        {
            // Arrange
            var mapping = new DataFilterViewModelToDataFilterDtoMapping(null);

            // Act
            // Assert
            CollectionAssert.AreEquivalent(Enumerable.Empty<string>(), mapping.NavigationProperties);
        }

        [Test]
        public void StubEntityToStubEntityDtoMapping_CorrectIncludes()
        {
            // Arrange
            var mapping = new StubEntityToStubEntityDtoMapping();

            // Act
            // Assert
            CollectionAssert.AreEquivalent(new[]
                {
                    "RecursiveEntity.RecursiveCollection1",
                    "RecursiveEntity.RecursiveCollection1.RecursiveCollection1",
                    "RecursiveEntity.RecursiveCollection1.RecursiveCollection1.RecursiveCollection1",
                    "RecursiveCollection1",

                    "RecursiveEntity.RecursiveCollection2",
                    "RecursiveEntity.RecursiveCollection2.RecursiveCollection2",

                    "RecursiveCollection2"
                }, mapping.NavigationProperties);
        }

        [Test]
        public void MappingWithStringEmptyOnly_NoIncludes()
        {
            // Arrange
            var mapping = new StringEmptyMapping();

            // Act
            // Assert
            CollectionAssert.AreEquivalent(new StringEmptyMapping[0], mapping.NavigationProperties);
        }

        [Test]
        public void GetFieldMappings_MappingEntityToDtoContainingFieldsInComplexType()
        {
            //Arrange
            var mapping = new ComplexTypeToComplexTypeDtoMapping();

            //Act
            var mappedFields = mapping.GetFieldMappings(typeof(ComplexType), typeof(ComplexTypeDto));

            //Assert
            Assert.IsTrue(mappedFields.ContainsKey("ComplexValue"));
            Assert.IsTrue(mappedFields.ContainsKey("ComplexValue.English"));
            Assert.IsTrue(mappedFields.ContainsKey("ComplexValue.Polish"));
            Assert.IsTrue(mappedFields.ContainsKey("SimpleValue"));

            Assert.IsTrue(mappedFields["ComplexValue.English"].Contains("ValueEn"));
            Assert.IsTrue(mappedFields["ComplexValue.Polish"].Contains("ValuePl"));

            Assert.IsTrue(mappedFields["ComplexValue.English"].Contains("ValueEn"));
            Assert.IsTrue(mappedFields["ComplexValue.Polish"].Contains("ValuePl"));
        }

        [Test]
        public void GetFieldMappings_MappingEntityToDtoContainingFieldsInManySameComplexTypes()
        {
            //Arrange
            var mapping = new ComplexTypesToComplexTypesDtoMapping();

            //Act
            var mappedFields = mapping.GetFieldMappings(typeof(ComplexTypes), typeof(ComplexTypesDto));

            //Assert
            Assert.IsTrue(mappedFields.ContainsKey("ComplexValue"));
            Assert.IsTrue(mappedFields.ContainsKey("ComplexValue.English"));
            Assert.IsTrue(mappedFields.ContainsKey("ComplexValue.Polish"));

            Assert.IsTrue(mappedFields.ContainsKey("ComplexDescription"));
            Assert.IsTrue(mappedFields.ContainsKey("ComplexDescription.English"));
            Assert.IsTrue(mappedFields.ContainsKey("ComplexDescription.Polish"));

            Assert.IsTrue(mappedFields.ContainsKey("ComplexName"));
            Assert.IsTrue(mappedFields.ContainsKey("ComplexName.English"));
            Assert.IsTrue(mappedFields.ContainsKey("ComplexName.Polish"));
        }

        [Test]
        public void GetFieldMappings_MappingEntityToDtoContainingFieldsInBaseType()
        {
            //Arrange
            var mapping = new BaseTypesToBaseTypesDtoMapping();

            //Act
            var mappedFields = mapping.GetFieldMappings(typeof(BaseTypes), typeof(BaseTypesDto));

            //Assert
            Assert.AreEqual(11, mappedFields.Count);
            Assert.IsTrue(mappedFields.ContainsKey("Text"));
            Assert.IsTrue(mappedFields.ContainsKey("Long"));
            Assert.IsTrue(mappedFields.ContainsKey("DateTime"));
            Assert.IsTrue(mappedFields.ContainsKey("Timespan"));
            Assert.IsTrue(mappedFields.ContainsKey("ByteArray"));
            Assert.IsTrue(mappedFields.ContainsKey("IsActive"));
            Assert.IsTrue(mappedFields.ContainsKey("NullableNumber"));
            Assert.IsTrue(mappedFields.ContainsKey("Picture"));
            Assert.IsTrue(mappedFields.ContainsKey("Picture.ContentType"));
            Assert.IsTrue(mappedFields.ContainsKey("Picture.DocumentName"));
            Assert.IsTrue(mappedFields.ContainsKey("Picture.DocumentId"));
        }
    }
}