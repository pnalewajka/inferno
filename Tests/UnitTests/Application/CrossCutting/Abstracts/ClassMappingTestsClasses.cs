﻿using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.CrossCutting.Common.Abstacts;

namespace UnitTests.Application.CrossCutting.Abstracts
{
    public class StubEntityToStubEntityDtoMapping : ClassMapping<StubEntity, StubEntityDto>
    {
        public StubEntityToStubEntityDtoMapping()
        {
            Mapping = x => new StubEntityDto
            {
                RecursiveCollection1 =
                    x.RecursiveEntity.RecursiveCollection1.SelectMany(a => a.RecursiveCollection1)
                        .Select(a => new StubEntityDto
                        {
                            RecursiveCollection1 = a.RecursiveCollection1.Select(b => new StubEntityDto()).ToList()
                        }).Select(a => new StubEntityDto
                        {
                            RecursiveCollection1 = x.RecursiveCollection1.Select(b => new StubEntityDto()).ToList()
                        })
                        .ToList(),

                RecursiveCollection2 =
                    x.RecursiveEntity.RecursiveCollection2.SelectMany(a => a.RecursiveCollection2)
                        .Select(a => new StubEntityDto())
                        .ToList(),

                IntegerList = x.RecursiveCollection2.Select(y => y.IntegerValue).ToList()
            };
        }
    }

    public class StringEmptyMapping : ClassMapping<BaseTypes, BaseTypesDto>
    {
        public StringEmptyMapping()
        {
            Mapping = e => new BaseTypesDto
            {
                Text = string.Empty
            };
        }
    }

    public class StubEntityDto
    {
        public IList<StubEntityDto> RecursiveCollection1 { get; set; }
        public IList<StubEntityDto> RecursiveCollection2 { get; set; }
        public IList<int> IntegerList { get; set; }
    }

    public class StubEntity
    {
        public ICollection<StubEntity> RecursiveCollection1 { get; set; }
        public ICollection<StubEntity> RecursiveCollection2 { get; set; }
        public StubEntity RecursiveEntity { get; set; }
        public int IntegerValue { get; set; }
    }
}