﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Models;

namespace UnitTests.Application.CrossCutting.Abstracts
{
    public class ComplexTypesToComplexTypesDtoMapping : ClassMapping<ComplexTypes, ComplexTypesDto>
    {
        public ComplexTypesToComplexTypesDtoMapping()
        {
            Mapping = e => new ComplexTypesDto
            {
                ComplexValue = new LocalizedString
                {
                    English = e.ValueEn,
                    Polish = e.ValuePl
                },
                ComplexDescription = new LocalizedString
                {
                    English = e.DescriptionEn,
                    Polish = e.DescriptionPl
                },
                ComplexName = new LocalizedString
                {
                    English = e.NameEn,
                    Polish = e.NamePl
                }
            };
        }
    }
}
