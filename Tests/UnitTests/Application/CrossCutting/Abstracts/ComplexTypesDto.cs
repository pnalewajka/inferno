﻿using Smt.Atomic.CrossCutting.Common.Models;

namespace UnitTests.Application.CrossCutting.Abstracts
{
    public class ComplexTypesDto
    {
        public LocalizedString ComplexValue { get; set; }

        public LocalizedString ComplexDescription { get; set; }

        public LocalizedString ComplexName { get; set; }

    }
}
