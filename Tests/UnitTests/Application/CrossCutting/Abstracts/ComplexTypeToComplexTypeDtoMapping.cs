﻿using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Models;

namespace UnitTests.Application.CrossCutting.Abstracts
{
    public class ComplexTypeToComplexTypeDtoMapping : ClassMapping<ComplexType, ComplexTypeDto>
    {
        public ComplexTypeToComplexTypeDtoMapping()
        {
            Mapping = e => new ComplexTypeDto
            {
                ComplexValue = new LocalizedString
                {
                    English = e.ValueEn,
                    Polish = e.ValuePl
                },
                SimpleValue = e.SimpleValue
            };
        }
    }
}
