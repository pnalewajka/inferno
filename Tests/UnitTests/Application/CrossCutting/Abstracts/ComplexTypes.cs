﻿namespace UnitTests.Application.CrossCutting.Abstracts
{
    public class ComplexTypes
    {
        public string ValuePl { get; set; }

        public string ValueEn { get; set; }

        public string DescriptionPl { get; set; }

        public string DescriptionEn { get; set; }

        public string NamePl { get; set; }

        public string NameEn { get; set; }
    }
}
