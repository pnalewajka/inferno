﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Models;

namespace UnitTests.Application.CrossCutting.Abstracts
{
    public class ExampleLocalizedString : LocalizedStringBase
    {
        [TargetCulture("en")]
        public string English { get; set; }

        [TargetCulture("en-US")]
        public string AmericanEnglish { get; set; }

        [TargetCulture("de-CH")]
        public string SwissGerman { get; set; }

        [TargetCulture("")]
        public string Polish { get; set; }
    }
}
