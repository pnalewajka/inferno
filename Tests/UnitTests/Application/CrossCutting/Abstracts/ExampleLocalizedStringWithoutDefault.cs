﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Models;

namespace UnitTests.Application.CrossCutting.Abstracts
{
    public class ExampleLocalizedStringWithoutDefault : LocalizedStringBase
    {
        [TargetCulture("en")]
        public string English { get; set; }

        [TargetCulture("en-US")]
        public string AmericanEnglish { get; set; }

        [TargetCulture("de-CH")]
        public string SwissGerman { get; set; }
    }
}
