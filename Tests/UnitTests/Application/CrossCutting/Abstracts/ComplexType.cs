﻿namespace UnitTests.Application.CrossCutting.Abstracts
{
    public class ComplexType
    {
        public string ValuePl { get; set; }

        public string ValueEn { get; set; }

        public string SimpleValue { get; set; }
    }
}
