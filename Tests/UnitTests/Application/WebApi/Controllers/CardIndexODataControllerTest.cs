﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.OData;
using Castle.MicroKernel.Registration;
using Moq;
using NUnit.Framework;
using Smt.Atomic.Business.Accounts.Dto;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.Business.Common.Services;
using Smt.Atomic.CrossCutting.Common.Abstacts;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Models;
using Smt.Atomic.CrossCutting.Common.Services;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Data.Repositories.FilteringQueries;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Data.Repositories.Services;
using Smt.Atomic.Tests.Common;
using Smt.Atomic.Tests.Common.AutoMock;
using Smt.Atomic.WebApi.Controllers;
using Smt.Atomic.WebApi.Models.LittlePony;
using UnitTests.Application.Business.Common;

namespace UnitTests.Application.WebApi.Controllers
{
    [TestFixture]
    public class CardIndexODataControllerTest
    {
        private AutoMockingContainer _container;
        private UsersDatabaseMock _databaseMock;
        private Mock<IAtomicPrincipal> _principalMock;

        [SetUp]
        public void Setup()
        {
            _principalMock = new Mock<IAtomicPrincipal>();
            _databaseMock = new UsersDatabaseMock();

            var testContainerConfiguration = new TestContainerConfiguration();
            testContainerConfiguration.Configure(ContainerType.UnitTests);

            _container = testContainerConfiguration.AutoMockingContainer;
            _container.RegisterDatabase(_databaseMock);

            _container.Register(Component.For<CardIndexODataController<UserViewModel, UserDto>>());
            _container.Register(Component.For<ICardIndexDataService<UserDto>>().ImplementedBy<TestCardIndexService>());
            _container.Register(Component.For(typeof(IUnitOfWorkService<>)).ImplementedBy(typeof(UnitOfWorkService<>)).LifestyleTransient());
            _container.Register(Component.For<IClassMappingFactory>().ImplementedBy<ClassMappingFactory>().LifestyleTransient());
            _container.Register(Component.For<IClassMapping<User, UserDto>>().ImplementedBy<UserToUserDtoMapping>().LifestyleTransient());
            _container.Register(Component.For<IClassMapping<UserDto, User>>().ImplementedBy<UserDtoToUserMapping>().LifestyleTransient());
            _container.Register(Component.For<IClassMapping<UserViewModel, UserDto>>().ImplementedBy<UserViewModelToUserDtoMapping>().LifestyleTransient());
            _container.Register(Component.For<IClassMapping<UserDto, UserViewModel>>().ImplementedBy<UserDtoToUserViewModelMapping>().LifestyleTransient());

            _container
                .Register(Component
                    .For<ICardIndexServiceDependencies<IAccountsDbScope>>()
                    .ImplementedBy<CardIndexDataServiceDependencies<IAccountsDbScope>>()
                    .LifestyleTransient());

            _container
                .GetMock<IDbContextWrapperFactory>()
                .Setup(m => m.Create())
                .Returns(new Mock<IDbContextWrapper>().Object);

            _container
                .Register(Component.For(typeof(IRepository<>))
                .UsingFactoryMethod((k, c) => _databaseMock.GetRepositoryMockFactoryMethod(c.RequestedType.GenericTypeArguments[0])));

            _container
                .GetMock<IPrincipalProvider>()
                .Setup(m => m.Current)
                .Returns(_principalMock.Object);

            _container
                .GetMock<IDataFilterService>()
                .Setup(m => m.GetUserDataFilters(It.IsAny<long>()))
                .Returns(new UserDataFilters());
        }

        [Test]
        public void Get_ShouldNotAuthorize()
        {
            var controller = GetController();
            controller.Settings.GetRequiredRole = SecurityRoleType.CanViewUsers;

            VerifyResponseException(HttpStatusCode.Unauthorized, () => controller.Get());
        }

        [Test]
        public void GetById_ShouldNotAuthorize()
        {
            var controller = GetController();
            controller.Settings.GetByIdRequiredRole = SecurityRoleType.CanViewUsers;

            VerifyResponseException(HttpStatusCode.Unauthorized, () => controller.Get(42));
        }

        [Test]
        public void Post_ShouldNotAuthorize()
        {
            var controller = GetController();
            controller.Settings.AddRequiredRole = SecurityRoleType.CanAddUsers;

            VerifyResponseException(HttpStatusCode.Unauthorized, () => controller.Post(null));
        }

        [Test]
        public void Patch_ShouldNotAuthorize()
        {
            var controller = GetController();
            controller.Settings.EditRequiredRole = SecurityRoleType.CanEditUsers;

            VerifyResponseException(HttpStatusCode.Unauthorized, () => controller.Patch(42, null));
        }

        [Test]
        public void Put_ShouldNotAuthorize()
        {
            var controller = GetController();
            controller.Settings.EditRequiredRole = SecurityRoleType.CanEditUsers;

            VerifyResponseException(HttpStatusCode.Unauthorized, () => controller.Put(42, null));
        }

        [Test]
        public void Delete_ShouldNotAuthorize()
        {
            var controller = GetController();
            controller.Settings.DeleteRequiredRole = SecurityRoleType.CanDeleteUsers;

            VerifyResponseException(HttpStatusCode.Unauthorized, () => controller.Delete(42));
        }

        [Test]
        public void Get_ShouldReturnAllExistingRecords()
        {
            var controller = GetController();
            controller.Settings.GetRequiredRole = SecurityRoleType.CanViewUsers;
            SetupPrincipalRoles(new[] {SecurityRoleType.CanViewUsers});
            _databaseMock.Users.First().IsDeleted = true;

            var records = controller.Get();

            Assert.AreEqual(_databaseMock.Users.Count(u => !u.IsDeleted), records.Count());
        }

        [Test]
        public void Get_ShouldReturnUsersWithLastNameStartingWithP()
        {
            var controller = GetController();

            var records = controller.Get("last-name-starts-with-p");

            Assert.AreEqual(
                _databaseMock.Users.Count(u => u.LastName.StartsWith("P")),
                records.Count());
        }

        [Test]
        public void Get_ShouldReturnUsersWithLastAndFirstNameStartingWithP()
        {
            var controller = GetController();

            var records = controller.Get("last-name-starts-with-p*first-name-starts-with-p");

            Assert.AreEqual(
                _databaseMock.Users.Count(u => u.LastName.StartsWith("P") && u.FirstName.StartsWith("P")),
                records.Count());
        }

        [Test]
        public void Get_ShouldReturnUsersWithLastOrFirstNameStartingWithP()
        {
            var controller = GetController();

            var records = controller.Get("last-name-starts-with-p first-name-starts-with-p");

            Assert.AreEqual(
                _databaseMock.Users.Count(u => u.LastName.StartsWith("P") || u.FirstName.StartsWith("P")),
                records.Count());
        }

        [Test]
        public void GetById_ShouldNotReturnSoftDeletedRecord()
        {
            var controller = GetController();
            _databaseMock.Users.Single(u => u.Id == 2).IsDeleted = true;

            var record = controller.Get(2);

            CollectionAssert.IsEmpty(record.Queryable.ToList());
        }

        [Test]
        public void GetById_ShouldReturnSoftDeletedRecord()
        {
            var controller = GetController();
            SetupPrincipalRoles(new[] {SecurityRoleType.CanViewSoftDeletedEntities});
            _databaseMock.Users.Single(u => u.Id == 2).IsDeleted = true;

            var record = controller.Get(2);

            CollectionAssert.IsNotEmpty(record.Queryable.ToList());
        }

        [Test]
        public void GetById_ShouldNotReturnNotExistingRecord()
        {
            var controller = GetController();

            var record = controller.Get(42);

            CollectionAssert.IsEmpty(record.Queryable.ToList());
        }

        [Test]
        public void Post_ShouldAddRecord()
        {
            var controller = GetController();
            controller.Settings.AddRequiredRole = SecurityRoleType.CanAddUsers;
            SetupPrincipalRoles(new[] { SecurityRoleType.CanAddUsers });
            var newUser = new UserViewModel
            {
                FirstName = "New",
                LastName = "User",
            };

            var response = controller.Post(newUser);
            var dbUser = controller.Get().Single(u => u.FirstName == newUser.FirstName);

            Assert.AreEqual(newUser.FirstName, dbUser.FirstName);
            Assert.AreEqual(newUser.LastName, dbUser.LastName);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public void Post_ShouldReturnBadRequestOnUserUniqueKeysViolation()
        {
            _container
                .GetMock<IRepository<User>>()
                .Setup(m => m.GetUniqueKeyViolations(It.IsAny<User>()))
                .Returns(new[] {new PropertyGroup {PropertyNames = new[] { "FirstName" } }});

            var controller = GetController();
            var newUser = new UserViewModel
            {
                FirstName = "New",
                LastName = "User",
            };

            var response = controller.Post(newUser);

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public void Put_ShouldUpdateRecord()
        {
            var controller = GetController();
            controller.Settings.AddRequiredRole = SecurityRoleType.CanEditUsers;
            SetupPrincipalRoles(new[] { SecurityRoleType.CanEditUsers });
            var editUser = CloningHelper.CloneObject(_databaseMock.Users.First());
            editUser.FirstName = "Edit";

            var response = controller.Put(editUser.Id, ToUserViewModel(editUser));
            var dbUser = controller.Get(editUser.Id).Queryable.Single();

            Assert.AreEqual(editUser.FirstName, dbUser.FirstName);
            Assert.AreEqual(editUser.LastName, dbUser.LastName);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public void Put_ShouldBadRequestOnIdentifiersMistmatch()
        {
            var controller = GetController();
            var editUser = CloningHelper.CloneObject(_databaseMock.Users.First());
            editUser.FirstName = "Edit";

            var response = controller.Put(42, ToUserViewModel(editUser));

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public void Put_ShouldBadRequestOnUserUniqueKeysViolation()
        {
            _container
                .GetMock<IRepository<User>>()
                .Setup(m => m.GetUniqueKeyViolations(It.IsAny<User>()))
                .Returns(new[] { new PropertyGroup { PropertyNames = new[] { "FirstName" } } });

            var controller = GetController();
            var editUser = CloningHelper.CloneObject(_databaseMock.Users.First());
            editUser.FirstName = "Edit";

            var response = controller.Put(editUser.Id, ToUserViewModel(editUser));

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public void Patch_ShouldUpdateRecord()
        {
            var controller = GetController();
            controller.Settings.AddRequiredRole = SecurityRoleType.CanEditUsers;
            SetupPrincipalRoles(new[] { SecurityRoleType.CanEditUsers });
            var delta = new Delta<UserViewModel>();
            delta.TrySetPropertyValue("FirstName", "Edit");

            var response = controller.Patch(1, delta);
            var dbUser = controller.Get(1).Queryable.Single();

            Assert.AreEqual("Edit", dbUser.FirstName);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public void Patch_ShouldNotFoundOnNotExistingRecord()
        {
            var controller = GetController();

            var response = controller.Patch(42, new Delta<UserViewModel>());

            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        public void Patch_ShouldBadRequestOnUserUniqueKeysViolation()
        {
            _container
                .GetMock<IRepository<User>>()
                .Setup(m => m.GetUniqueKeyViolations(It.IsAny<User>()))
                .Returns(new[] { new PropertyGroup { PropertyNames = new[] { "FirstName" } } });

            var controller = GetController();

            var response = controller.Patch(1, new Delta<UserViewModel>());

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public void Delete_ShouldDeleteRecord()
        {
            var controller = GetController();
            controller.Settings.AddRequiredRole = SecurityRoleType.CanDeleteUsers;
            SetupPrincipalRoles(new[] { SecurityRoleType.CanDeleteUsers });

            var response = controller.Delete(1);
            var countDeleted = controller.Get().Count(u => u.Id == 1);

            Assert.AreEqual(0, countDeleted);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public void Delete_ShouldNotFoundOnNotExistingRecord()
        {
            var controller = GetController();

            var response = controller.Delete(42);

            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        private CardIndexODataController<UserViewModel, UserDto> GetController()
        {
            var controller = _container.Resolve<CardIndexODataController<UserViewModel, UserDto>>();
            controller.Request = new HttpRequestMessage();
            controller.Request.SetConfiguration(new HttpConfiguration());

            return controller;
        }

        private void SetupPrincipalRoles(SecurityRoleType[] roles)
        {
            _principalMock
                .Setup(p => p.Roles)
                .Returns(roles.Select(r => r.ToString()).ToArray());

            _principalMock
                .Setup(p => p.IsInRole(It.IsAny<SecurityRoleType>()))
                .Returns<SecurityRoleType>(i => roles.Any(r => r == i));
        }

        private void VerifyResponseException(HttpStatusCode code, Action action)
        {
            try
            {
                action();
                Assert.Fail("Expected HttpResponseException " + code);
            }
            catch (HttpResponseException ex)
            {
                Assert.AreEqual(code, ex.Response.StatusCode);
            }
        }

        private UserViewModel ToUserViewModel(User user)
        {
            var userDto = new UserToUserDtoMapping().CreateFromSource(user);
            return new UserDtoToUserViewModelMapping().CreateFromSource(userDto);
        }
    }
}



