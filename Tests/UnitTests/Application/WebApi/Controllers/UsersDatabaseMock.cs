﻿using System.Collections.Generic;
using Smt.Atomic.Data.Entities.Modules.Accounts;
using Smt.Atomic.Tests.Common.Wrappers;

namespace UnitTests.Application.WebApi.Controllers
{
    public class UsersDatabaseMock : DatabaseMock
    {
        private readonly List<User> _users = new List<User>
        {
            new User {Id = 1, FirstName = "John", LastName = "Doe", Login = "john69"},
            new User {Id = 2, FirstName = "Mary", LastName = "Doe", Login = "mary69"},
            new User {Id = 3, FirstName = "Hannah", LastName = "Poopenberg", Login = "hpoo"},
            new User {Id = 3, FirstName = "Pablo", LastName = "Poopenberg", Login = "ppoo"},
            new User {Id = 3, FirstName = "Patrick", LastName = "Stewart", Login = "p.stewart"},
        };

        public List<User> Users => _users;

        public UsersDatabaseMock()
        {
            AddEntities(_users);
        }
    }
}
