﻿using System;
using System.Linq;
using System.Text;
using Castle.MicroKernel.Registration;
using Moq;
using NLog;
using NLog.Config;
using NUnit.Framework;
using Smt.Atomic.Business.Allocation.Interfaces;
using Smt.Atomic.Business.Common.Interfaces;
using Smt.Atomic.CrossCutting.Common.Attributes;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.CrossCutting.Security.Principal;
using Smt.Atomic.JobScheduler;
using Smt.Atomic.Tests.Common;

namespace UnitTests.Application.JobScheduler
{
    internal class PrincipalProviderMock : IPrincipalProvider
    {
        public IAtomicPrincipal Current => new AtomicPrincipal("admin");

        public bool IsSet => true;
    }

    [TestFixture]
    public class ContainerConfigurationTest
    {
        [SetUp]
        public void Setup()
        {
#pragma warning disable 618
            ContainerConfiguration.CleanUpConfiguration();
#pragma warning restore 618

            var config = new LoggingConfiguration();
            LogManager.Configuration = config;

            var testContainerConfiguration = new TestContainerConfiguration();
            testContainerConfiguration.Configure(ContainerType.JobScheduler);

            ContainerConfiguration.Container.Register(
                Component.For<IPrincipalProvider>()
                .ImplementedBy<PrincipalProviderMock>()
                .IsDefault());
        }

        [Test]
        public void AllRegisteredJobsShouldBeResolvable()
        {
            var jobClasses = TypeHelper.GetLoadedTypes()
                .Where(t => AttributeHelper.GetClassAttribute<IdentifierAttribute>(t) != null)
                .Where(t => typeof(IJob).IsAssignableFrom(t));

            var errors = new StringBuilder();

            foreach (var jobClass in jobClasses)
            {
                try
                {
                    ReflectionHelper.CreateInstanceWithIocDependencies(jobClass);
                }
                catch (Exception ex)
                {
                    var message = $"{jobClass.Name} => {ex.Message}\r\n\r\n";
                    errors.Append(message);
                    errors.AppendLine(message);
                    errors.AppendLine();
                }
            }

            if (!string.IsNullOrEmpty(errors.ToString()))
            {
                throw new Exception("There are non-resolvable jobs:\r\n\r\n" + errors);
            }
        }

        [TearDown]
        public void CleanUp()
        {
#pragma warning disable 618
            ContainerConfiguration.CleanUpConfiguration();
#pragma warning restore 618
        }
    }
}