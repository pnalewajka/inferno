﻿using NUnit.Framework;
using PowerShellAutomation.CodeAnalysisClasses;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Tools.PowerShellAutomation.CodeAnalysisClasses
{
    [TestFixture]
    public class CreateControllerTests
    {
        [Test]
        public void Generate_BasicTest()
        {
            var expected = ResourceHelper.GetResourceAsStringByShortName("Resources.CreateController_ClassCode.txt", GetType());
            var generator = new CreateController();

            var actual = generator.GenerateControllerClassCode("Ent", "Act");

            Assert.AreEqual(expected, actual);
        }
    }
}
