﻿using Microsoft.CodeAnalysis.CSharp;
using NUnit.Framework;
using PowerShellAutomation.CodeAnalysisClasses;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Tools.PowerShellAutomation.CodeAnalysisClasses
{
    [TestFixture]
    public class MappingFormatterTests
    {
        [Test]
        [TestCase("ClassWithSimpleMapping")]
        [TestCase("ClassWithNestedLambdas")]
        public void SimpleMapping_LambdaPropertyFormatted(string fileName)
        {
            // Arrange
            var baseCode = ResourceHelper.GetResourceAsStringByShortName($"Resources.FormatMappings_Base_{fileName}.txt", GetType());
            var targetCode = ResourceHelper.GetResourceAsStringByShortName($"Resources.FormatMappings_Target_{fileName}.txt", GetType());
            var formatter = new MappingFormatter();

            // Act
            var generatedCode = formatter.Visit(SyntaxFactory.ParseSyntaxTree(baseCode).GetRoot()).ToFullString();
            
            // Assert
            Assert.That(generatedCode.NormalizeEndLinesToUnix(), Is.EqualTo(targetCode.NormalizeEndLinesToUnix()));
        }
    }
}