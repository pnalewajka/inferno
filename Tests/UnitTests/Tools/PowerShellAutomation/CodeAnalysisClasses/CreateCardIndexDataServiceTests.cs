﻿using NUnit.Framework;
using PowerShellAutomation.CodeAnalysisClasses;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Tools.PowerShellAutomation.CodeAnalysisClasses
{
    [TestFixture]
    public class CreateCardIndexDataServiceTests
    {
        [Test]
        public void Generate_BasicTest()
        {
            var targetInterfaceCode = ResourceHelper.GetResourceAsStringByShortName("Resources.CreateCardIndexDataService_InterfaceCode.txt", GetType());
            var targetClassCode = ResourceHelper.GetResourceAsStringByShortName("Resources.CreateCardIndexDataService_ClassCode.txt", GetType());
            var generator = new CreateCardIndexDataService();

            var generatedInterfaceCode = generator.GenerateServiceInterfaceCode("Entertainment", "Actor");
            var generatedClassCode = generator.GenerateServiceClassCode("Entertainment", "Actor");

            Assert.AreEqual(targetInterfaceCode, generatedInterfaceCode);
            Assert.AreEqual(targetClassCode, generatedClassCode);
        }
    }
}
