﻿using NUnit.Framework;
using PowerShellAutomation.Commands;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Tools.PowerShellAutomation.CodeAnalysisClasses
{
    [TestFixture]
    public partial class CreateSqlFromEnumTests
    {
        [Test]
        public void Generate_GeneralTest()
        {
            var command = new CreateSqlForEnumCommand();
            var enumType = typeof(TestEnum);
            command.FullClassName = enumType.FullName;
            command.Schema = "Accounts";
            command.AssemblyPath = enumType.Assembly.Location;
            command.DescriptionLocale = "pl-pl";

            var output = (CreateSqlForEnumCommand.CreateSqlForEnumOutput) command.Execute();

            var expectedCreateStatement = ResourceHelper.GetResourceAsStringByShortName("Resources.CreateSqlFromEnum_GeneralTest_CreateTable.txt", GetType());
            var expectedDropStatement = ResourceHelper.GetResourceAsStringByShortName("Resources.CreateSqlFromEnum_GeneralTest_DropTable.txt", GetType());
            var expectedMergeStatement = ResourceHelper.GetResourceAsStringByShortName("Resources.CreateSqlFromEnum_GeneralTest_Merge.txt", GetType());

            Assert.AreEqual(expectedCreateStatement, output.TableCreation);
            Assert.AreEqual(expectedDropStatement, output.TableDestruction);
            Assert.AreEqual(expectedMergeStatement, output.ContentCreation);
        }

        [Test]
        public void Generate_DescriptionOmission()
        {
            var command = new CreateSqlForEnumCommand();
            var enumType = typeof(TestEnum);
            command.FullClassName = enumType.FullName;
            command.Schema = "Accounts";
            command.OmitDescriptions = true;
            command.AssemblyPath = enumType.Assembly.Location;

            var output = (CreateSqlForEnumCommand.CreateSqlForEnumOutput)command.Execute();

            var expectedMergeStatement = ResourceHelper.GetResourceAsStringByShortName("Resources.CreateSqlFromEnum_DescriptionOmission_Merge.txt", GetType());

            Assert.AreEqual(expectedMergeStatement, output.ContentCreation);
        }

        [Test]
        public void Generate_Localization()
        {
            var enumType = typeof(TestEnum);
            var command = new CreateSqlForEnumCommand
            {
                FullClassName = enumType.FullName,
                Schema = "Accounts",
                DescriptionLocale = "en",
                AssemblyPath = enumType.Assembly.Location
            };

            var output = (CreateSqlForEnumCommand.CreateSqlForEnumOutput)command.Execute();

            var expectedMergeStatement = ResourceHelper.GetResourceAsStringByShortName("Resources.CreateSqlFromEnum_Localization_Merge.txt", GetType());

            Assert.AreEqual(expectedMergeStatement, output.ContentCreation);
        }

        [Test]
        public void Generate_OverrideName()
        {
            var enumType = typeof(TestEnum);
            var command = new CreateSqlForEnumCommand
            {
                FullClassName = enumType.FullName,
                Schema = "Accounts",
                OverrideName = true,
                OverrideNameValue = "BlahType",
                AssemblyPath = enumType.Assembly.Location,
                DescriptionLocale = "pl-pl"
            };

            var output = (CreateSqlForEnumCommand.CreateSqlForEnumOutput)command.Execute();

            var expectedMergeStatement = ResourceHelper.GetResourceAsStringByShortName("Resources.CreateSqlFromEnum_OverrideName_Merge.txt", GetType());

            Assert.AreEqual(expectedMergeStatement, output.ContentCreation);
        }

        [Test]
        public void Generate_OverrideNameTypeName()
        {
            var enumType = typeof(TestEnum);
            var command = new CreateSqlForEnumCommand
            {
                FullClassName = enumType.FullName,
                Schema = "Accounts",
                OverrideName = true,
                AssemblyPath = enumType.Assembly.Location,
                DescriptionLocale = "pl-pl"
            };

            var output = (CreateSqlForEnumCommand.CreateSqlForEnumOutput)command.Execute();

            var expectedMergeStatement = ResourceHelper.GetResourceAsStringByShortName("Resources.CreateSqlFromEnum_OverrideNameTypeName_Merge.txt", GetType());

            Assert.AreEqual(expectedMergeStatement, output.ContentCreation);
        }

    }
}
