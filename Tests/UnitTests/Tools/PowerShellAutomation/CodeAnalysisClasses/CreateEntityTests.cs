﻿using NUnit.Framework;
using PowerShellAutomation.CodeAnalysisClasses;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Tools.PowerShellAutomation.CodeAnalysisClasses
{
    [TestFixture]
    public class CreateEntityTests
    {
        private CreateEntity Generator { get; set; }

        [TestFixtureSetUp]
        public void Init()
        {
            Generator = new CreateEntity();
        }

        private void AssertStringsIgnoringLineEndings(string left, string right)
        {
            left = left.Replace("\r", "");
            right = right.Replace("\r", "");

            Assert.AreEqual(left, right);
        }

        [Test]
        public void Generate_DefaultEntityTest()
        {
            var targetCode = ResourceHelper.GetResourceAsStringByShortName("Resources.CreateEntity_ClassCode_ModificationTrackedEntity.txt", GetType());

            var generatedCode = Generator.GenerateCode("Scheduling", "JobDefinition", null);

            AssertStringsIgnoringLineEndings(targetCode, generatedCode);
        }

        [Test]
        public void Generate_SimpleTest()
        {
            var targetCode = ResourceHelper.GetResourceAsStringByShortName("Resources.CreateEntity_ClassCode_SimpleEntity.txt", GetType());           

            var generatedCode = Generator.GenerateCode("Scheduling", "JobDefinition", "Simple");

            AssertStringsIgnoringLineEndings(targetCode, generatedCode);
        }

        [Test]
        public void Generate_ModificationTrackedTest()
        {
            var targetCode = ResourceHelper.GetResourceAsStringByShortName("Resources.CreateEntity_ClassCode_ModificationTrackedEntity.txt", GetType());

            var generatedCode = Generator.GenerateCode("Scheduling", "JobDefinition", "ModificationTracked");

            AssertStringsIgnoringLineEndings(targetCode, generatedCode);
        }

        [Test]
        public void Generate_CreationTrackedTest()
        {
            var targetCode = ResourceHelper.GetResourceAsStringByShortName("Resources.CreateEntity_ClassCode_CreationTracked.txt", GetType());

            var generatedCode = Generator.GenerateCode("Scheduling", "JobDefinition", "CreationTracked");

            AssertStringsIgnoringLineEndings(targetCode, generatedCode);
        }

        [Test]
        public void Generate_TrackedTest()
        {
            var targetCode = ResourceHelper.GetResourceAsStringByShortName("Resources.CreateEntity_ClassCode_TrackedEntity.txt", GetType());

            var generatedCode = Generator.GenerateCode("Scheduling", "JobDefinition", "Tracked");

            AssertStringsIgnoringLineEndings(targetCode, generatedCode);
        }

        [Test]
        public void Generate_SimpleEntityTest()
        {
            var targetCode = ResourceHelper.GetResourceAsStringByShortName("Resources.CreateEntity_ClassCode_SimpleEntity.txt", GetType());

            var generatedCode = Generator.GenerateCode("Scheduling", "JobDefinition", "SimpleEntity");

            AssertStringsIgnoringLineEndings(targetCode, generatedCode);
        }

        [Test]
        public void Generate_ModificationTrackedEntityTest()
        {
            var targetCode = ResourceHelper.GetResourceAsStringByShortName("Resources.CreateEntity_ClassCode_ModificationTrackedEntity.txt", GetType());

            var generatedCode = Generator.GenerateCode("Scheduling", "JobDefinition", "ModificationTrackedEntity");

            AssertStringsIgnoringLineEndings(targetCode, generatedCode);
        }

        [Test]
        public void Generate_CreationTrackedEntityTest()
        {
            var targetCode = ResourceHelper.GetResourceAsStringByShortName("Resources.CreateEntity_ClassCode_CreationTracked.txt", GetType());

            var generatedCode = Generator.GenerateCode("Scheduling", "JobDefinition", "CreationTrackedEntity");

            AssertStringsIgnoringLineEndings(targetCode, generatedCode);
        }

        [Test]
        public void Generate_TrackedEntityTest()
        {
            var targetCode = ResourceHelper.GetResourceAsStringByShortName("Resources.CreateEntity_ClassCode_TrackedEntity.txt", GetType());

            var generatedCode = Generator.GenerateCode("Scheduling", "JobDefinition", "TrackedEntity");

            AssertStringsIgnoringLineEndings(targetCode, generatedCode);
        }
    }
}
