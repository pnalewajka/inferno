﻿using Smt.Atomic.CrossCutting.Common.Attributes;
using UnitTests.Tools.PowerShellAutomation.CodeAnalysisClasses.Resources;

namespace UnitTests.Tools.PowerShellAutomation.CodeAnalysisClasses
{
    public partial class CreateSqlFromEnumTests
    {
        public enum TestEnum
        {
            [DescriptionLocalized("Can1", typeof(CreateSqlFromEnumResources))]
            Can1 = 1,
            [DescriptionLocalized("Can2", typeof(CreateSqlFromEnumResources))]
            Can2,
            [DescriptionLocalized("Can3", typeof(CreateSqlFromEnumResources))]
            Can3,
        }
    }
}
