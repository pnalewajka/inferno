﻿using NUnit.Framework;
using PowerShellAutomation.CodeAnalysisClasses;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Tools.PowerShellAutomation.CodeAnalysisClasses
{
    [TestFixture]
    public class InjectPropertyTests
    {
        [Test]
        public void Generate_InjectDbSetPropertyIntoCoreEntitiesContextTest()
        {
            var baseCode = ResourceHelper.GetResourceAsStringByShortName("Resources.InjectProperty_BaseCoreEntitiesContextCode.txt", GetType());
            var targetCode = ResourceHelper.GetResourceAsStringByShortName("Resources.InjectProperty_TargetCoreEntitiesContextCode.txt", GetType());
            var generator = new DbSetPropertyCodeInjector();

            var generatedCode = generator.GetCode(baseCode, "Entertainment", "Actor");

            Assert.AreEqual(targetCode, generatedCode);
        }

        [Test]
        public void Generate_InjectRepositoryIntoScopeTest()
        { 
            var baseCode = ResourceHelper.GetResourceAsStringByShortName("Resources.InjectProperty_BaseScopeCode.txt", GetType());
            var targetCode = ResourceHelper.GetResourceAsStringByShortName("Resources.InjectProperty_TargetScopeCode.txt", GetType());
            var generator = new RepositoryScopeCodeInjector();

            var generatedCode = generator.GetCode(baseCode, "Entertainment", "Actor");

            Assert.AreEqual(targetCode, generatedCode);
        }
    }
}
