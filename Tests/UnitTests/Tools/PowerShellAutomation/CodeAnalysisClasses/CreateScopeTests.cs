﻿using NUnit.Framework;
using PowerShellAutomation.CodeAnalysisClasses;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Tools.PowerShellAutomation.CodeAnalysisClasses
{
    [TestFixture]
    public class CreateScopeTests
    {
        [Test]
        public void Generate_BasicTest()
        {
            var targetCode = ResourceHelper.GetResourceAsStringByShortName("Resources.InjectProperty_BaseScopeCode.txt", GetType());
            var generator = new DbScopeCodeBuilder();

            var generatedCode = generator.GenerateCode("Entertainment");
            
            Assert.AreEqual(targetCode, generatedCode);
        }
    }
}
