﻿using NUnit.Framework;
using PowerShellAutomation.CodeAnalysisClasses;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Tools.PowerShellAutomation.CodeAnalysisClasses
{
    [TestFixture]
    public class CreateServiceInstallerTests
    {
        [Test]
        public void Generate_BasicTest()
        {
            var targetCode = ResourceHelper.GetResourceAsStringByShortName("Resources.CreateServiceInstaller_ClassCode.txt", GetType());
            var generator = new ServiceInstallerCodeBuilder();

            var generatedCode = generator.GenerateCode("Smt.Atomic.Business.Entertainment");
            
            Assert.AreEqual(targetCode, generatedCode);
        }
    }
}
