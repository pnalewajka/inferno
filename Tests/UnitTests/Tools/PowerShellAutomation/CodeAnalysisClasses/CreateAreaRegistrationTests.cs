﻿using NUnit.Framework;
using PowerShellAutomation.CodeAnalysisClasses;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Tools.PowerShellAutomation.CodeAnalysisClasses
{
    [TestFixture]
    public class CreateAreaRegistrationTests
    {
        [Test]
        public void Generate_BasicTest()
        {
            var targetCode = ResourceHelper.GetResourceAsStringByShortName("Resources.CreateAreaRegistration_ClassCode.txt", GetType());
            var generator = new AreaRegistrationCodeBuilder();

            var generatedCode = generator.GenerateCode("Entertainment");

            if (!string.IsNullOrEmpty(targetCode))
            {
                targetCode = targetCode.NormalizeEndLinesToUnix();
            }

            if (!string.IsNullOrEmpty(generatedCode))
            {
                generatedCode = generatedCode.NormalizeEndLinesToUnix();
            }

            Assert.AreEqual(targetCode, generatedCode);
        }
    }
}