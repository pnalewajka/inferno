﻿using NUnit.Framework;
using PowerShellAutomation.CodeAnalysisClasses;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace UnitTests.Tools.PowerShellAutomation.CodeAnalysisClasses
{
    [TestFixture]
    public class InjectServiceRegistrationTests
    {
        [Test]
        public void Generate_InjectServiceRegistrationIntoServiceInstallerTest()
        {
            var baseCode = ResourceHelper.GetResourceAsStringByShortName("Resources.CreateServiceInstaller_ClassCode.txt", GetType());
            var targetCode = ResourceHelper.GetResourceAsStringByShortName("Resources.InjectIntoServiceInstaller_TargetCode.txt", GetType());
            var generator = new ServiceRegistrationCodeInjector();

            var generatedCode = generator.GetCode(baseCode, "IActorCardIndexDataService", "ActorCardIndexDataService");

            Assert.AreEqual(targetCode, generatedCode);
        }

        [Test]
        public void Generate_InjectServiceRegistrationIntoContainerRegistrationTest()
        {
            var baseCode = ResourceHelper.GetResourceAsStringByShortName("Resources.InjectIntoContainerConfiguration_ClassCode.txt", GetType());
            var targetCode = ResourceHelper.GetResourceAsStringByShortName("Resources.InjectIntoContainerConfiguration_TargetCode.txt", GetType());
            var generator = new ServiceInstallerCodeInjector();

            var generatedCode = generator.GetCode(baseCode, "Business.Entertainment");

            Assert.AreEqual(targetCode, generatedCode);
        }
    }
}
