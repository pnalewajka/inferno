﻿SET IDENTITY_INSERT [Accounts].[TestEnumEnum] ON
GO
MERGE INTO [Accounts].[TestEnumEnum]  AS target
USING ( 
VALUES (1,'Can1'),
(2,'Can2'),
(3,'Can3') ) AS source ([Id], [Name])
ON target.[Id] = source.[Id]
WHEN NOT MATCHED BY SOURCE THEN
    DELETE
WHEN MATCHED THEN 
    UPDATE SET [Name] = source.[Name]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id], [Name]) VALUES (source.Id, source.Name);
GO
SET IDENTITY_INSERT [Accounts].[TestEnumEnum] OFF
