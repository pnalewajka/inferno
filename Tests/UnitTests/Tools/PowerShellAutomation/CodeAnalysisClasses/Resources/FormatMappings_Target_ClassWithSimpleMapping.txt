﻿namespace Smt.Atomic.Business.Notifications.Dto
{
    public class EmailToEmailDtoMapping : ClassMapping<Email, EmailDto>
    {
        public EmailToEmailDtoMapping()
        {
            Mapping = e => new EmailDto{
                Id = e.Id,
                To = e.To,
                From = e.From,
                Subject = e.Subject,
                Status = e.Status,
                SentOn = e.SentOn,
                Content = (EmailMessageDto) XmlSerializationHelper.DeserializeFromXml(e.Content,
                typeof(EmailMessageDto)),
                Exception = e.Exception,
                CreatedBy = e.CreatedBy.Login,
                CreatedOn = e.CreatedOn
            };
        }
    }
}
