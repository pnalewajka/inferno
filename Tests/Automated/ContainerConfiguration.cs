﻿using System.Collections.Generic;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;

namespace Smt.Atomic.Tests.Automated
{
    public class AutomatedTestsContainerConfiguration : ContainerConfiguration
    {
        protected override IEnumerable<AtomicInstaller> GetInstallers(ContainerType containerType)
        {
            yield return new CrossCutting.Common.ServiceInstaller();
            yield return new Atomic.Data.Entities.ServiceInstaller();
            yield return new Atomic.Data.Repositories.ServiceInstaller();
            yield return new CrossCutting.Security.ServiceInstaller();
            yield return new CrossCutting.Settings.ServiceInstaller();
            yield return new Business.Configuration.ServiceInstaller();
            yield return new ServiceInstaller();
        }
    }
}