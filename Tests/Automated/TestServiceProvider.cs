﻿using System;
using System.Linq;
using System.Threading;
using Castle.Core;
using Castle.Core.Logging;
using Castle.Windsor;
using Smt.Atomic.CrossCutting.Common.Diagnostics;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.CrossCutting.Security.Interfaces;
using Smt.Atomic.CrossCutting.Security.Principal;

namespace Smt.Atomic.Tests.Automated
{
    public static class TestServiceProvider
    {
        private static readonly AutomatedTestsContainerConfiguration Configuration = new AutomatedTestsContainerConfiguration();

        [ThreadStatic]
        private static IWindsorContainer _container;

        [ThreadStatic]
        private static ILogger _logger;

        public static IWindsorContainer Container
        {
            get
            {
                if (_container == null)
                {
                    SetupContainer();
                    RunStartupTimeDiagnostics();
                    SetupThreadPrincipal();
                }

                return _container;
            }
        }

        private static void SetupThreadPrincipal()
        {
            var principal = AtomicPrincipal.Anonymous;

            Thread.CurrentPrincipal = principal;
        }

        private static void SetupContainer()
        {
            _container = Configuration.Configure(ContainerType.JobScheduler);
            _logger = _container.Resolve<ILogger>();
        }

        private static void RunStartupTimeDiagnostics()
        {
            WindsorContainerDiagnostics.Inspect(_container, _logger);
            CheckServicesRegistration();
        }

        private static void CheckServicesRegistration()
        {
            var handlers = _container.Kernel.GetAssignableHandlers(typeof(object));
            var invalidHandlers = handlers
                .Where(h => h.ComponentModel.LifestyleType == LifestyleType.PerWebRequest);

            var invalidRequests = invalidHandlers
                .Select(h => h.ComponentModel.ComponentName)
                .ToList();

            if (invalidRequests.Any())
            {
                throw new BasicConfigurationException(
                    $"JobScheduler misconfiguration: {string.Join(", ", invalidRequests)}");
            }
        }
    }
}
