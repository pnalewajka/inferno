﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Smt.Atomic.Tests.Automated.Common.Consts;
using System.IO;
using System.Collections.Generic;

namespace Smt.Atomic.Tests.Automated.Common.Extensions
{
    public static class WebDriverExtensions
    {
        private static int ElementTimeout => TestServiceProvider.Container.Resolve<Configuration>().ElementTimeout;

        private static int WaitTimeoutInMiliseconds => TestServiceProvider.Container.Resolve<Configuration>().WaitTimeoutInMiliseconds;

        public static IWebElement FindElementById(this IWebDriver driver, string id)
        {
            IWebElement result;

            try
            {
                result = driver.FindElement(By.Id(id));
                Debug.WriteLine("Find element with id '{0}' on {1}", id, driver.Title);
            }
            catch
            {
                Debug.WriteLine("Can't find element with id '{0}' on {1}", id, driver.Title);
                throw;
            }

            return result;
        }

        public static IWebElement FindElementByName(this IWebDriver driver, string name)
        {
            return driver.FindElement(By.Name(name));
        }

        public static IWebElement FindElementByClassName(this IWebDriver driver, string className)
        {
            return driver.FindElement(By.ClassName(className));
        }

        public static IReadOnlyCollection<IWebElement> FindElementsByClassName(this IWebDriver driver, string className)
        {
            return driver.FindElements(By.ClassName(className));
        }

        public static IWebElement FindElementByCssSelector(this IWebDriver driver, string sccSelector)
        {
            return driver.FindElement(By.CssSelector(sccSelector));
        }

        public static IReadOnlyCollection<IWebElement> FindElementsByCssSelector(this IWebDriver driver, string cssSelector)
        {
            return driver.FindElements(By.CssSelector(cssSelector));
        }

        public static IWebElement FindElementByLinkText(this IWebDriver driver, string link)
        {
            return driver.FindElement(By.LinkText(link));
        }

        public static IWebElement FindElementByXPath(this IWebDriver driver, string xpathToFind)
        {
            return driver.FindElement(By.XPath(xpathToFind));
        }

        public static IJavaScriptExecutor Scripts(this IWebDriver driver)
        {
            return (IJavaScriptExecutor)driver;
        }

        public static IWebElement WaitForElementVisible(this IWebDriver driver, By by)
        {
            return driver.WaitForElementVisible(by, ElementTimeout);
        }

        public static IWebElement WaitForElementVisible(this IWebDriver driver, By by, Int32 timeoutInSeconds)
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(0);

            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));
                IWebElement element = wait.Until(ExpectedConditions.ElementIsVisible(by));
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(ElementTimeout);

                return element;
            }
            catch (WebDriverTimeoutException e)
            {
                Console.WriteLine(e.ToString());
            }

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(ElementTimeout);

            return null;
        }

        public static void WaitForElementNotVisible(this IWebDriver driver, By by, Int32 timeoutInSeconds)
        {
            Thread.Sleep(10);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(0);
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));

            wait.Until(d =>
            {
                try
                {
                    var element = d.FindElement(by);
                    return !element.Displayed;
                }
                catch (NoSuchElementException)
                {
                    return true;
                }
                catch (StaleElementReferenceException)
                {
                    Console.WriteLine("StaleElementReferenceException");
                    return !d.FindElement(by).Displayed;
                }
            });

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(ElementTimeout);
        }

        public static IWebElement FindElementOnPage(this IWebDriver webDriver, By by)
        {
            var element = webDriver.FindElement(by);

            return webDriver.FindElementOnPage(element);
        }

        public static IWebElement FindElementOnPage(this IWebDriver webDriver, IWebElement element)
        {
            webDriver.Scripts().ExecuteScript("arguments[0].scrollIntoView(false);", element);
            webDriver.Scripts().ExecuteScript("window.scrollBy(0,100);");
            Thread.Sleep(500);

            return element;
        }

        public static IWebDriver WaitTimeout(this IWebDriver driver)
        {
            return driver.WaitTimeout(WaitTimeoutInMiliseconds);
        }

        public static IWebDriver WaitTimeout(this IWebDriver driver, Int32 timeoutInMilliseconds)
        {
            Thread.Sleep(timeoutInMilliseconds);

            return driver;
        }

        public static IWebDriver CaptureScreenshot(this IWebDriver driver)
        {
            string fileName = Regex.Replace(TestContext.CurrentContext.Test.Name, "[^a-z0-9\\-_]+", "_", RegexOptions.IgnoreCase);
            fileName = fileName + "_" + TestContext.CurrentContext.Result.Status + "_" + DateTime.Now.Ticks + ".png";
            var screenshotSavePath = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\")) + "Reports\\";

            Console.WriteLine("Saving screenshot: {0}{1}", screenshotSavePath, fileName);
            Screenshot capturedImage = ((ITakesScreenshot)driver).GetScreenshot();
            capturedImage.SaveAsFile((screenshotSavePath + fileName), ScreenshotImageFormat.Png);

            return driver;
        }

        public static bool HasAlert(this IWebDriver driver)
        {
            try
            {
                var handle = driver.CurrentWindowHandle;
                driver.SwitchTo().Alert();
                driver.SwitchTo().Window(handle);

                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }
    }
}
