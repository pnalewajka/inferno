﻿using System;
using System.Diagnostics;
using System.Threading;
using OpenQA.Selenium;
using Smt.Atomic.Tests.Automated.Common.Consts;
using Smt.Atomic.Tests.Automated.WebApp.Consts;

namespace Smt.Atomic.Tests.Automated.Common.Extensions
{
    public static class WebElementExtensions
    {
        private static int WaitTimeoutInMiliseconds => TestServiceProvider.Container.Resolve<Configuration>().WaitTimeoutInMiliseconds;

        public static void ClickElement(this IWebElement element)
        {
            string elementName = GetElementName(element);

            try
            {
                element.Click();
                Debug.WriteLine($"Click element '{elementName}'");
            }
            catch (Exception)
            {
                Debug.WriteLine($"Can't click element '{elementName}'");
                throw;
            }
        }

        public static IWebElement FindElementById(this IWebElement element, string id)
        {
            return element.FindElement(By.Id(id));
        }

        public static IWebElement FindElementByClassName(this IWebElement element, string className)
        {
            string elementName = GetElementName(element);

            do
            {
                try
                {
                    IWebElement foundedElemet = element.FindElement(By.ClassName(className));
                    Debug.WriteLine("Find class'{0}' in element '{1}", className, elementName);
                    return foundedElemet;
                }
                catch (StaleElementReferenceException)
                {
                    Debug.WriteLine("StaleElementException catched");
                }
                catch (Exception)
                {
                    Debug.WriteLine($"Can't find class'{className}' in element '{elementName}");
                    throw;
                }

            } while (true);
        }

        public static IWebElement FindElementByCssSelector(this IWebElement element, string cssSelector)
        {
            return element.FindElement(By.CssSelector(cssSelector));
        }

        public static IWebElement WaitTimeout(this IWebElement element)
        {
            return element.WaitTimeout(WaitTimeoutInMiliseconds);
        }

        public static IWebElement WaitTimeout(this IWebElement element, Int32 timeoutInMilliseconds)
        {
            Thread.Sleep(timeoutInMilliseconds);

            return element;
        }

        private static string GetElementName(IWebElement element)
        {
            string elementName;

            if (element.TagName == "a")
            {
                elementName = element.Text;
            }
            else if (element.TagName == "input")
            {
                elementName = element.GetAttribute("value");
            }
            else if (element.TagName == "button")
            {
                elementName = element.Text;
            }
            else
            {
                elementName = null;
            }

            return elementName;
        }

        public static IWebElement GetModelContent(this IWebDriver driver, string searchElement)
        {
            var elements = WebDriverExtensions.FindElementsByClassName(driver, AutomationIdentifiers.PopUpSelectorCssClass);

            foreach (IWebElement element in elements)
            {
                IWebElement modelTitleElement = element.FindElement(By.ClassName(AutomationIdentifiers.ModelTitle));

                if (modelTitleElement.Text == searchElement)
                {
                    return element;
                }
            }

            return null;
        }
    }
}