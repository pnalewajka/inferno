﻿using System;
using NUnit.Framework;
using Smt.Atomic.Tests.Automated.Common.Exceptions;
using Smt.Atomic.Tests.Automated.Common.PageObjects;

namespace Smt.Atomic.Tests.Automated.Common.Extensions
{
    public static class BasePageExtension
    {
        public static TPage NavigateSelf<TPage>(this TPage page) where TPage : BasePage
        {
            page.NavigateTo(page);

            return page;
        }

        public static TPage WaitForLoaded<TPage>(this TPage page) where TPage : BasePage
        {
            if (page.IsLoaded())
            {

                return page;
            }

            throw new PageNotLoadedException(typeof(TPage));
        }

        public static TPage WaitForLoaded<TPage>(this TPage page, int timeoutInSeconds) where TPage : BasePage
        {
            if (page.IsLoaded(timeoutInSeconds))
            {
                return page;
            }

            throw new PageNotLoadedException(typeof(TPage), timeoutInSeconds);
        }

        public static TPage AssertIsTrue<TPage>(this TPage page, Func<bool> predicate) where TPage : BasePage
        {
            Assert.IsTrue(predicate());

            return page;
        }

        public static TPage AssertIsTrue<TPage>(this TPage page, Func<TPage, bool> predicate) where TPage : BasePage
        {
            Assert.IsTrue(predicate(page));

            return page;
        }

        public static TPage DoAndContinue<TPage>(this TPage page, Action<TPage> predicate) where TPage : BasePage
        {
            predicate(page);

            return page;
        }

        public static TResult Do<TPage, TResult>(this TPage page, Func<TPage, TResult> action)
        {
            return action(page);
        }
    }
}
