﻿using Castle.Core.Logging;
using Smt.Atomic.CrossCutting.Settings.Configuration;

namespace Smt.Atomic.Tests.Automated.Common.Consts
{
    public class Configuration : SettingsProviderBase
    {
        public Configuration(ILogger logger) : base(logger)
        {
        }

        public int PageTimeout => GetInt("PageTimeout", 0);

        public int ElementTimeout => GetInt("ElementTimeout", 0);

        public int WaitTimeoutInMiliseconds => GetInt("WaitTimeoutInMiliseconds", 0);

        public string WebAppUrl => GetString("WebAppUrl");

        public string AdminUsername => GetString("AdminUsername");

        public string AdminPassword => GetString("AdminPassword");
    }
}
