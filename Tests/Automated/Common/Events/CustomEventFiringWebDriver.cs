﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Events;

namespace Smt.Atomic.Tests.Automated.Common.Events
{
    public class CustomEventFiringWebDriver : EventFiringWebDriver
    {
        public CustomEventFiringWebDriver(IWebDriver parentDriver)
            : base(parentDriver)
        {
        }

        protected override void OnElementClicking(WebElementEventArgs e)
        {
            Console.WriteLine("{0} clicking", e.Element.TagName);

            base.OnElementClicking(e);
        }

        protected override void OnElementClicked(WebElementEventArgs e)
        {
            Console.WriteLine("Element clicked");

            base.OnElementClicked(e);
        }

        protected override void OnNavigated(WebDriverNavigationEventArgs e)
        {
            Console.WriteLine("{0} navigated", e.Url);

            base.OnNavigated(e);
        }

        protected override void OnNavigating(WebDriverNavigationEventArgs e)
        {
            Console.WriteLine("{0} navigating", e.Url);

            base.OnNavigating(e);
        }
    }
}
