﻿using OpenQA.Selenium;

namespace Smt.Atomic.Tests.Automated.Common.PageObjects
{
    public abstract class BaseComponent<TPage> : PageObject where TPage : BasePage
    {
        public TPage Parent { get; private set; }

        protected BaseComponent(IWebDriver driver, TPage parent) : base(driver)
        {
            Parent = parent;
        }
    }
}
