﻿using System;
using OpenQA.Selenium;

namespace Smt.Atomic.Tests.Automated.Common.PageObjects
{
    public abstract class BasePage : PageObject
    {
        public abstract string Url { get; }

        protected BasePage(IWebDriver driver) : base(driver)
        {
        }

        public TPage NavigateTo<TPage>(string urlParams = "") where TPage : BasePage
        {
            var nextPage = (TPage)Activator.CreateInstance(typeof(TPage), new object[] { Driver });
            Driver.Navigate().GoToUrl(nextPage.Url + urlParams);

            return nextPage;
        }

        public TPage NavigateTo<TPage>(TPage page, string urlParams = "") where TPage : BasePage
        {
            Driver.Navigate().GoToUrl(page.Url + urlParams);

            return page;
        }

        public TPage ChangeTo<TPage>() where TPage : BasePage
        {
            var nextPage = (TPage)Activator.CreateInstance(typeof(TPage), new object[] { Driver });

            return nextPage;
        }

        public BasePage VerifyCurrentPageUrl(Action<String> verify)
        {
            verify(Driver.Url);

            return this;
        }

        public BasePage VerifyCurrentPageTitle(Action<String> verify)
        {
            verify(Driver.Title);

            return this;
        }
    }
}
