﻿using OpenQA.Selenium;
using Smt.Atomic.Tests.Automated.Common.Consts;
using Smt.Atomic.Tests.Automated.Common.Extensions;

namespace Smt.Atomic.Tests.Automated.Common.PageObjects
{
    public abstract class PageObject
    {
        protected IWebDriver Driver;
        protected Configuration Configuration;

        protected PageObject(IWebDriver driver)
        {
            Driver = driver;
            Configuration = Resolve<Configuration>();
        }

        public abstract By GetIsLoadedLocator();

        public bool IsLoaded()
        {
            return IsLoaded(Configuration.PageTimeout);
        }

        public bool IsLoaded(int pageTimeoutInSeconds)
        {
            bool isLoaded = false;
            var element = Driver.WaitForElementVisible(GetIsLoadedLocator(), pageTimeoutInSeconds);
            if (element != null)
            {
                isLoaded = true;
            }

            return isLoaded;
        }

        protected T Resolve<T>()
        {
            return TestServiceProvider.Container.Resolve<T>();
        }

        protected void Release(object instance)
        {
            TestServiceProvider.Container.Release(instance);
        }
    }
}
