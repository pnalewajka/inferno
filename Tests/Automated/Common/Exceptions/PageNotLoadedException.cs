﻿using System;

namespace Smt.Atomic.Tests.Automated.Common.Exceptions
{
    public class PageNotLoadedException : Exception
    {
        public PageNotLoadedException(Type pageType)
            : base($"{pageType.Name} is not loaded")
        {
        }
        public PageNotLoadedException(Type pageType, int timeoutInSeconds)
            : base($"{pageType.Name} is not loaded after {timeoutInSeconds} seconds")
        {
        }
    }
}
