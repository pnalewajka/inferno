﻿using System;
using Smt.Atomic.Tests.Automated.Common.PageObjects;

namespace Smt.Atomic.Tests.Automated.Common.Models
{
    public class PageWithValue<TPage, TValue> where TPage : BasePage
    {
        public TPage Page { get; set; }
        public TValue Value { get; set; }

        public PageWithValue<TPage, TValue> Do(Action<PageWithValue<TPage, TValue>> action)
        {
            action(this);
            return this;
        }

        public TPage DoAndContinue(Action<PageWithValue<TPage, TValue>> action)
        {
            action(this);
            return Page;
        }

        public TResult Do<TResult>(Func<PageWithValue<TPage, TValue>, TResult> action)
        {
            return action(this);
        }
    }
}
