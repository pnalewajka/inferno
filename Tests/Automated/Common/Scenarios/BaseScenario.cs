﻿using OpenQA.Selenium;

namespace Smt.Atomic.Tests.Automated.Common.Scenarios
{
    public abstract class BaseScenario<TModel, TResult> where TModel : IScenarioModel
    {
        protected IWebDriver Driver { get; set; }

        protected BaseScenario(IWebDriver driver)
        {
            Driver = driver;
        }

        public abstract TResult Perform(TModel model);
    }
}
