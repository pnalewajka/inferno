﻿using OpenQA.Selenium;

namespace Smt.Atomic.Tests.Automated.Common.Scenarios
{
    public abstract class DefaultScenario<TModel> : BaseScenario<TModel, bool> where TModel : IScenarioModel
    {
        protected DefaultScenario(IWebDriver driver)
            : base(driver)
        {
        }
    }
}
