﻿using System;
using System.Globalization;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.Events;
using Smt.Atomic.Tests.Automated.Common.Consts;
using Smt.Atomic.Tests.Automated.Common.Events;
using Smt.Atomic.Tests.Automated.Common.Extensions;

namespace Smt.Atomic.Tests.Automated.Common.Tests
{
    [TestFixture]
    public abstract class BaseTest
    {
        protected IWebDriver Driver { get; set; }
        protected Configuration Configuration;

        protected BaseTest()
        {
            Configuration = Resolve<Configuration>();
        }

        [TestFixtureSetUp]
        public virtual void TestFixtureSetUp()
        {
            Console.WriteLine("TestFixtureSetUp Started");
            var driver = CreateDriver();
            SetupDriver(driver);
            var setupDriver = new CustomEventFiringWebDriver(driver);
            SetupEventDriver(setupDriver);
            Driver = setupDriver;
            SetUpCulture();

            try
            {
                OnTestFixtureInitialize();
            }
            catch
            {
                Driver.CaptureScreenshot();
                throw;
            }

            Console.WriteLine("TestFixtureSetUp Finished");
        }

        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            Console.WriteLine("TestFixtureTearDown Started");
            OnTestFixtureCleanup();
            Driver.Quit();
            Console.WriteLine("TestFixtureTearDown Finished");
        }

        [SetUp]
        public void SetUp()
        {
            Console.WriteLine("SetUp Started");
            OnTestSetUp();
            Console.WriteLine("SetUp Finished");
        }

        [TearDown]
        public void TearDown()
        {
            Console.WriteLine("TearDown Started");
            CaptureScreenshotIfTestNotPassed();
            OnTestCleanup();
            Console.WriteLine("TearDown Finished");
        }

        protected virtual void OnTestFixtureCleanup() { }

        protected virtual void OnTestFixtureInitialize() { }

        protected virtual void OnTestSetUp() { }

        protected virtual void OnTestCleanup() { }

        private void CaptureScreenshotIfTestNotPassed()
        {
            Console.WriteLine(TestContext.CurrentContext.Result.Status);

            if (TestContext.CurrentContext.Result.Status != TestStatus.Passed)
            {
                Driver.CaptureScreenshot();
            }
        }

        protected virtual void SetUpCulture()
        {
            var ci = new CultureInfo("en-GB");
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;
        }

        protected virtual IWebDriver CreateDriver()
        {
            var profile = new FirefoxProfile();
            profile.SetPreference("network.automatic-ntlm-auth.trusted-uris", ".corp.smtsoftware.com, atomic.local");
            profile.SetPreference("reader.parse-on-load.enabled", false);
            var driver = new FirefoxDriver(profile);

            return driver;
        }

        protected virtual void SetupDriver(IWebDriver driver)
        {
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().PageLoad =     TimeSpan.FromSeconds(Configuration.PageTimeout);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(Configuration.ElementTimeout);
        }

        protected virtual void SetupEventDriver(EventFiringWebDriver eventDriver)
        {

        }

        protected T Resolve<T>()
        {
            return TestServiceProvider.Container.Resolve<T>();
        }

        protected void Release(object instance)
        {
            TestServiceProvider.Container.Release(instance);
        }
    }
}
