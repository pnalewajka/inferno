﻿using OpenQA.Selenium;
using Smt.Atomic.Tests.Automated.Common.Extensions;

namespace Smt.Atomic.Tests.Automated.WebApp.Pages.Accounts
{
    public class SetPasswordPage : WebAppPage
    {
        private const string PageSelectorCssClass = "auto-smt-atomic-webapp-areas-accounts-controllers-authenticationcontroller-setpassword";
        private const string PasswordSelectorId = "new-password";
        private const string PasswordConfirmationSelectorId = "re-entered-password";
        private const string SubmitSelectorCss = "input[type=submit]";

        public override string Url => Configuration.WebAppUrl + "/Accounts/Authentication/SetPassword";

        public SetPasswordPage(IWebDriver driver) : base(driver)
        {
        }

        public override By GetIsLoadedLocator()
        {
            return By.ClassName(PageSelectorCssClass);
        }

        public GenericAuthenticatedWebAppPage SetPassword(string password)
        {
            var elementWithPassword = Driver.FindElementById(PasswordSelectorId);
            var elementWithPasswordConfirmation = Driver.FindElementById(PasswordConfirmationSelectorId);
            var button = Driver.FindElementByCssSelector(SubmitSelectorCss);

            elementWithPasswordConfirmation.SendKeys(password);
            elementWithPassword.SendKeys(password);
            button.ClickElement();

            return new GenericAuthenticatedWebAppPage(Driver);
        }
    }
}
