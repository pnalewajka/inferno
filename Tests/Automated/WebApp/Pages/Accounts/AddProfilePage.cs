﻿using OpenQA.Selenium;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Tests.Automated.Common.Extensions;
using Smt.Atomic.Tests.Automated.WebApp.Models.Accounts;

namespace Smt.Atomic.Tests.Automated.WebApp.Pages.Accounts
{
    public class AddProfilePage : AuthenticatedWebAppPage
    {
        private const string PageSelectorClassName = "auto-smt-atomic-webapp-areas-accounts-controllers-profilecontroller-add";

        private const string ValuePickerAddButtonClassName = "value-picker-add-button";
        private const string ProfileNameInputId = "name";
        private const string ProfileActiveDirectoryGroupInputId = "active-directory-group-name";
        private const string SelectRolePopUpClassName = "modal-content";
        private const string SelectRoleSearchPanelClassName = "input-group";
        private const string SelectRoleSearchPanelInputClassName = "form-control";
        private const string SelectRoleSearchPanelSearchButtonClassName = "input-group-addon";
        private const string SelectRoleSearchPanelOkButtonClassName = "btn-primary";
        private const string SelectRoleTableClassName = "grid-table";
        private const string AddProfileButtonCssSelector = "body > div > div.footer-section > div.form-footer > div > div > input.btn.btn-primary";

        public override string Url => Configuration.WebAppUrl + "/Accounts/Profile/Add";

        private IWebElement SelectRolePopUp => Driver.FindElementByClassName(SelectRolePopUpClassName);
        private IWebElement ProfileNameInput => ContentSection.FindElementById(ProfileNameInputId);
        private IWebElement ProfileActiveDirectoryGroupInput => ContentSection.FindElementById(ProfileActiveDirectoryGroupInputId);
        private IWebElement ValuePickerAddButton => ContentSection.FindElementByClassName(ValuePickerAddButtonClassName);
        private IWebElement SelectRoleSearchPanel => SelectRolePopUp.FindElementByClassName(SelectRoleSearchPanelClassName);
        private IWebElement SelectRoleTable => SelectRolePopUp.FindElementByClassName(SelectRoleTableClassName);
        private IWebElement AddProfileButton => FooterSection.FindElementByCssSelector(AddProfileButtonCssSelector);

        public AddProfilePage(IWebDriver driver)
            : base(driver)
        {
        }

        public override By GetIsLoadedLocator() { return By.ClassName(PageSelectorClassName); }

        public ProfileIndexPage FillProfileForm(ProfileModel model)
        {
            ProfileNameInput.SendKeys(model.ProfileName);
            AddRoleToProfile(model.ProfileRole);
            AddProfileButton.ClickElement();
            return new ProfileIndexPage(Driver);
        }

        private void AddRoleToProfile(SecurityRoleType role)
        {
            ValuePickerAddButton.ClickElement();
            SelectRoleSearchPanel.FindElementByClassName(SelectRoleSearchPanelInputClassName).SendKeys(role.GetDescription());
            SelectRoleSearchPanel.FindElementByClassName(SelectRoleSearchPanelSearchButtonClassName).ClickElement();
            SelectRoleTable.FindElement(By.TagName("td")).ClickElement();
            SelectRolePopUp.FindElementByClassName(SelectRoleSearchPanelOkButtonClassName).ClickElement();
        }
    }
}
