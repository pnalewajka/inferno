﻿using OpenQA.Selenium;
using Smt.Atomic.Tests.Automated.Common.Extensions;
using Smt.Atomic.Tests.Automated.WebApp.Consts;

namespace Smt.Atomic.Tests.Automated.WebApp.Pages.Accounts
{
    public class ProfileIndexPage : CardIndexPage
    {
        private const string AddProfileButtonSelectorCssClass = AutomationIdentifiers.CardIndexAddButtonIdentifier;
        private const string PageSelectorCssClass = "auto-smt-atomic-webapp-areas-accounts-controllers-profilecontroller-list";

        public override string Url => Configuration.WebAppUrl + "/Accounts/Profile/List";

        public ProfileIndexPage(IWebDriver driver)
            : base(driver)
        {
        }

        public override By GetIsLoadedLocator()
        {
            return By.ClassName(PageSelectorCssClass);
        }

        public AddProfilePage BeginAddProfile()
        {
            var button = Driver.FindElementByClassName(AddProfileButtonSelectorCssClass);
            button.ClickElement();

            return new AddProfilePage(Driver);
        }
    }
}
