﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using Smt.Atomic.CrossCutting.Common.Extensions;
using Smt.Atomic.Tests.Automated.Common.Extensions;
using Smt.Atomic.Tests.Automated.WebApp.Models.Accounts;

namespace Smt.Atomic.Tests.Automated.WebApp.Pages.Accounts
{
    public class AddUserPage : AuthenticatedWebAppPage
    {
        private const string UserFirstNameInputId = "first-name";
        private const string UserLastNameInputId = "last-name";
        private const string UserLoginInputId = "login";
        private const string UserEmailInputId = "email";
        private const string IsActiveCheckboxId = "is-active";
        private const string AddUserButtonCss = "body > div > div.footer-section > div.form-footer > div > div > input.btn.btn-primary";
        private const string PageSelectorCssClass = "page-layout";

        private IWebElement UserFirstNameInput => Driver.FindElementById(UserFirstNameInputId);
        private IWebElement UserLastNameInput => Driver.FindElementById(UserLastNameInputId);
        private IWebElement UserLoginInput => Driver.FindElementById(UserLoginInputId);
        private IWebElement UserEmailInput => Driver.FindElementById(UserEmailInputId);
        private IWebElement IsActiveCheckbox => Driver.FindElementById(IsActiveCheckboxId);

        public override string Url
        {
            get { throw new NotImplementedException(); }
        }

        public AddUserPage(IWebDriver driver)
            : base(driver)
        {
        }

        public override By GetIsLoadedLocator()
        {
            return By.ClassName(PageSelectorCssClass);
        }

        public AddUserPage FillUserForm(AddUserModel model)
        {
            UserFirstNameInput.SendKeys(model.FirstName);
            UserLastNameInput.SendKeys(model.LastName);
            UserLoginInput.SendKeys(model.Login);
            UserEmailInput.SendKeys(model.Email);

            if (!IsActiveCheckbox.Selected)
            {
                IsActiveCheckbox.ClickElement();
            }

            return this;
        }

        public UserIndexPage ClickAddUser()
        {
            var button = Driver.FindElementByCssSelector(AddUserButtonCss);
            button.ClickElement();

            return new UserIndexPage(Driver);
        }
    }
}
