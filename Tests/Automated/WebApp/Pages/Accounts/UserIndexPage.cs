﻿using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using Smt.Atomic.Tests.Automated.Common.Extensions;
using Smt.Atomic.Tests.Automated.WebApp.Consts;
using Smt.Atomic.Tests.Automated.WebApp.Models.Accounts;
using Smt.Atomic.WebApp.Areas.Accounts.Resources;

namespace Smt.Atomic.Tests.Automated.WebApp.Pages.Accounts
{
    public class UserIndexPage : CardIndexPage
    {
        private const string PageSelectorCssClass = "auto-smt-atomic-webapp-areas-accounts-controllers-usercontroller-list";
        private const string AddUserButtonSelectorCssClass = AutomationIdentifiers.CardIndexAddButtonIdentifier;
        private const string UserIndexTableCss = "body > div > div.content-section > div > div > div > div.grid-table > table > tbody > tr > td";
        

        public override string Url => Configuration.WebAppUrl + "/Accounts/User/List";

        public UserIndexPage(IWebDriver driver)
            : base(driver)
        {
        }

        public override By GetIsLoadedLocator()
        {
            return By.ClassName(PageSelectorCssClass);
        }

        public AddUserPage BeginAddingUser()
        {
            var button = Driver.FindElementByClassName(AddUserButtonSelectorCssClass);
            button.ClickElement();

            return new AddUserPage(Driver);
        }

        public bool IsUserOnList(AddUserModel model)
        {
            var userTableContainer = Driver.FindElementByCssSelector(UserIndexTableCss);
            IList<IWebElement> userList = userTableContainer.FindElements(By.TagName("p"));
            return userList.Any(el => el.Text.Contains(model.FullName));
        }

        public UserIndexPage SendLoginToken()
        {
            GetManageUserButton().Click();
            GetSetLoginTokenButton().Click();

            ConfirmChoice();

            return this;
        }

        public UserIndexPage SelectUser(string userFullName)
        {
            var userItem = GetUserItem(userFullName);
            userItem.Click();

            return this;
        }

        private IWebElement GetUserItem(string userFullName)
        {
            var locator = "//div[contains(@class,'user-tile') and ./p[contains(@class,'full-name') and contains(text(),'"
                + userFullName
                + "')]]";
            var item = Driver.FindElementByXPath(locator);
            return item;
        }

        private IWebElement GetManageUserButton()
        {
            var locator = "//div[contains(@class,'grid-toolbar')]//div[contains(@class,'burger-box')]//button[contains(text(),'"
                + UserResources.ManageUserDropdownText
                + "')]";
            var button = Driver.FindElementByXPath(locator);
            return button;
        }

        private IWebElement GetSetLoginTokenButton()
        {
            var locator = "//a[contains(text(),'"
                + UserResources.IssueLoginTokenButtonText
                + "')]";
            var button = Driver.FindElementByXPath(locator);
            return button;
        }
    }
}
