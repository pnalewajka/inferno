﻿using OpenQA.Selenium;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Tests.Automated.Common.Consts;
using Smt.Atomic.Tests.Automated.Common.Extensions;
using Smt.Atomic.Tests.Automated.WebApp.Consts;
using Smt.Atomic.Tests.Automated.WebApp.Models;
using Smt.Atomic.Tests.Automated.WebApp.Selectors;
using System.Linq;

namespace Smt.Atomic.Tests.Automated.WebApp.Pages.Cities
{
    public class CitiesPage : AuthenticatedWebAppPage
    {
        public CitiesPage(IWebDriver driver)
             : base(driver)
        {
        }

        public override string Url => Configuration.WebAppUrl + "/Dictionaries/City/List";
        private const string PageSelectorCssClass = "page-layout";
        private const string SelectCountryTableClassName = "grid-table";

        private IWebElement SelectCountryPopUp
            => Driver.FindElementByClassName(AutomationIdentifiers.PopUpSelectorCssClass);

        private IWebElement CityNameInput
            => Driver.FindElementByCssSelector(AddCitySelectors.CityNameTextField);

        private IWebElement OpenCountryPicker
            => SelectCountryPopUp.FindElementByClassName(AddCitySelectors.CityCountryPicker);

        private IWebElement CompanyApartmentAvailableCheckbox
              => Driver.FindElementById(AddCitySelectors.CompanyApartmentAvailable);

        private IWebElement VoucherServiceAvailableeCheckbox
               => Driver.FindElementById(AddCitySelectors.VoucherServiceAvailable);

        public bool ElementExists(string newElement)
                => WebDriverExtensions.FindElementsByCssSelector(Driver, AutomationIdentifiers.TableElements).Any(e => e.Text == newElement);

        public override By GetIsLoadedLocator()
        {
            return By.ClassName(PageSelectorCssClass);
        }

        public CitiesPage FillInCityDetails(AddCityModel model)
        {
            CityNameInput.SendKeys(model.CityName);
            AddCountryToCity(model.CountryName);

            if (!CompanyApartmentAvailableCheckbox.Selected)
            {
                CompanyApartmentAvailableCheckbox.ClickElement();
            }

            if (!VoucherServiceAvailableeCheckbox.Selected)
            {
                VoucherServiceAvailableeCheckbox.ClickElement();
            }

            return this;
        }

        public void AddCountryToCity(SecurityRoleType role)
        {
            OpenCountryPicker.ClickElement();
            var element = WebElementExtensions.GetModelContent(Driver, "Select Value From The List");
            element.FindElement(By.TagName("td")).ClickElement();
            element.FindElementByCssSelector(AutomationIdentifiers.OkButton).ClickElement();
        }

        public CitiesPage OpenAddCityPopUp()
        {
            var button = Driver.FindElementByCssSelector(AutomationIdentifiers.CardIndexAddButtonIdentifier);
            button.ClickElement();

            return new CitiesPage(Driver);
        }

        public CitiesPage AddCity()
        {
            var element = WebElementExtensions.GetModelContent(Driver, "Cities: Add");
            element.FindElementByCssSelector(AutomationIdentifiers.OkButton).ClickElement();

            return new CitiesPage(Driver);
        }
    }
}