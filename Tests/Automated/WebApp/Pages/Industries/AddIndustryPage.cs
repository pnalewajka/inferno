﻿using OpenQA.Selenium;
using Smt.Atomic.Tests.Automated.Common.Extensions;
using Smt.Atomic.Tests.Automated.WebApp.Selectors;
using Smt.Atomic.Tests.Automated.WebApp.Models;
using Smt.Atomic.Tests.Automated.WebApp.Consts;

namespace Smt.Atomic.Tests.Automated.WebApp.Pages.Industries
{
    public class AddIndustryPage : AuthenticatedWebAppPage
    {
        private const string PageSelectorCssClass = "page-layout";
        public override string Url => Configuration.WebAppUrl + "/Dictionaries/Industry/Add";

        private IWebElement IndustryNameInputEn
            => Driver.FindElementByCssSelector(AddIndustrySelectors.IndustryNameTextFieldEn);

        private IWebElement IndustryNameInputPl
            => Driver.FindElementByCssSelector(AddIndustrySelectors.IndustryNameTextFieldPl);

        private IWebElement AddIndustryAddButton
            => Driver.FindElementByCssSelector(AutomationIdentifiers.AddForm);

        public AddIndustryPage(IWebDriver driver)
            : base(driver)
        {
        }

        public override By GetIsLoadedLocator()
        {
            return By.ClassName(PageSelectorCssClass);
        }

        public AddIndustryPage FillInIndustryDetails(AddIndustryModel model)
        {
            IndustryNameInputEn.SendKeys(model.IndustryName);
            IndustryNameInputPl.SendKeys(model.IndustryName);

            return this;
        }

        public IndustriesPage AddIndustry()
        {
            AddIndustryAddButton.ClickElement();

            return new IndustriesPage(Driver);
        }
    }
}