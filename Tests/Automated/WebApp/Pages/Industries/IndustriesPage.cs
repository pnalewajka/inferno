﻿using OpenQA.Selenium;
using Smt.Atomic.Tests.Automated.Common.Extensions;
using Smt.Atomic.Tests.Automated.WebApp.Consts;

namespace Smt.Atomic.Tests.Automated.WebApp.Pages.Industries
{
    public class IndustriesPage : AuthenticatedWebAppPage
    {
        private const string PageSelectorCssClass = "page-layout";
        public override string Url => Configuration.WebAppUrl + "/Dictionaries/Industry/List";
        public string FirstRowIndustryTab => Driver.FindElementByCssSelector(AutomationIdentifiers.FirstRowFirstColumnElement).Text;

        public IndustriesPage(IWebDriver driver)
            : base(driver)
        {
        }

        public override By GetIsLoadedLocator()
        {
            return By.ClassName(PageSelectorCssClass);
        }

        public AddIndustryPage GoToTheAddIndustrySubpage()
        {
            var button = Driver.FindElementByCssSelector(AutomationIdentifiers.CardIndexAddButtonIdentifier);
            button.ClickElement();

            return new AddIndustryPage(Driver);
        }
    }
}