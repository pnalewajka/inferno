﻿using System;
using OpenQA.Selenium;

namespace Smt.Atomic.Tests.Automated.WebApp.Pages
{
    public class GenericAuthenticatedWebAppPage: AuthenticatedWebAppPage
    {
        protected const string PageSelectorCssClass = "authenticated-user-info";

        public GenericAuthenticatedWebAppPage(IWebDriver driver)
            : base(driver)
        {
        }

        public override string Url
        {
            get { throw new NotImplementedException(); }
        }

        public override By GetIsLoadedLocator()
        {
            return By.ClassName(PageSelectorCssClass);
        }
    }
}
