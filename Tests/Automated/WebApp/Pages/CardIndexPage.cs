﻿using NUnit.Framework;
using OpenQA.Selenium;
using Smt.Atomic.Tests.Automated.Common.Extensions;
using Smt.Atomic.Tests.Automated.WebApp.Components;

namespace Smt.Atomic.Tests.Automated.WebApp.Pages
{
    public abstract class CardIndexPage : AuthenticatedWebAppPage
    {
        private const string SearchFilterCssSelector = @"input[name=""search""]";
        private const string SearchFilterButtonCssSelector = @".grid-toolbar span.input-group-addon";

        protected CardIndexPage(IWebDriver driver) : base(driver)
        {
        }

        public CardIndexPage FindText(string searchFilter)
        {
            var searchFilterInput = Driver.FindElementByCssSelector(SearchFilterCssSelector);
            var searchFilterButton = Driver.FindElementByCssSelector(SearchFilterButtonCssSelector);
            searchFilterInput.Clear();
            searchFilterInput.SendKeys(searchFilter);
            searchFilterButton.Click();

            return this;
        }

        public CardIndexPage ConfirmChoice()
        {
            var modal = new CommonDialogConfirm<CardIndexPage>(Driver, this);
            Assert.IsTrue(modal.IsLoaded());

            return modal.ClickYes();
        }
    }
}
