﻿using System;
using OpenQA.Selenium;
using Smt.Atomic.Tests.Automated.Common.Extensions;
using Smt.Atomic.Tests.Automated.WebApp.Selectors;
using Smt.Atomic.Tests.Automated.WebApp.Models;
using Smt.Atomic.Tests.Automated.WebApp.Pages.Companies;
using Smt.Atomic.Tests.Automated.WebApp.Consts;

namespace Smt.Atomic.Tests.Automated.WebApp.Pages
{
    public class AddCompanyPage : AuthenticatedWebAppPage
    {
        public override string Url => Configuration.WebAppUrl + "/Dictionaries/Company/Add";
        private const string PageSelectorCssClass = "page-layout";

        private IWebElement CompanyNameInput
            => Driver.FindElementByCssSelector(AddCompanySelectors.CompanyNameTextField);
        private IWebElement CompanyDescriptionInput
            => Driver.FindElementByCssSelector(AddCompanySelectors.CompanyDescriptionTextField);
        private IWebElement CompanyEmailDomainInput
            => Driver.FindElementByCssSelector(AddCompanySelectors.CompanyEmailDomainTextField);
        private IWebElement CompanyActiveDirectoryCodeInput
            => Driver.FindElementByCssSelector(AddCompanySelectors.CompanyActiveDirectoryCodeTextField);
        private IWebElement CompanyNavisionCodeInput
            => Driver.FindElementByCssSelector(AddCompanySelectors.CompanyNavisionCodeTextField);

        private IWebElement AddCompanyAddButton
            => Driver.FindElementByCssSelector(AutomationIdentifiers.AddForm);

        public AddCompanyPage(IWebDriver driver)
            : base(driver)
        {
        }

        public override By GetIsLoadedLocator()
        {
            return By.ClassName(PageSelectorCssClass);
        }

        public AddCompanyPage FillInCompanyDetails(AddCompanyModel model)
        {
            Console.WriteLine("Filling Company Details with details: " + model.CompanyName);
            CompanyNameInput.SendKeys(model.CompanyName);
            CompanyDescriptionInput.SendKeys(model.CompanyDescription);
            CompanyActiveDirectoryCodeInput.SendKeys(model.CompanyActiveDirectoryCode);
            CompanyNavisionCodeInput.SendKeys(model.CompanyNavisionCode);

            return this;
        }

        public CompaniesPage AddCompany()
        {
            AddCompanyAddButton.ClickElement();

            return new CompaniesPage(Driver);
        }
    }
}