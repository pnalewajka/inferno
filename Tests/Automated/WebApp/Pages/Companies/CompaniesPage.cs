﻿using OpenQA.Selenium;
using System;
using Smt.Atomic.Tests.Automated.Common.Extensions;
using Smt.Atomic.Tests.Automated.WebApp.Selectors;
using Smt.Atomic.Tests.Automated.WebApp.Models;
using NUnit.Framework;
using Smt.Atomic.Tests.Automated.WebApp.Consts;

namespace Smt.Atomic.Tests.Automated.WebApp.Pages.Companies
{
    public class CompaniesPage : AuthenticatedWebAppPage
    {
        public CompaniesPage(IWebDriver driver)
            : base(driver)
        {
        }

        public override string Url => Configuration.WebAppUrl + "/Dictionaries/Company/List";
        private const string PageSelectorCssClass = "page-layout";
        public string FirstRowCompaniesTab => Driver.FindElementByCssSelector(AutomationIdentifiers.FirstRowFirstColumnElement).Text;

        public override By GetIsLoadedLocator()
        {
            return By.ClassName(PageSelectorCssClass);
        }

        public AddCompanyPage GoToTheAddCompanySubpage()
        {
            var button = Driver.FindElementByCssSelector(AutomationIdentifiers.CardIndexAddButtonIdentifier);
            button.ClickElement();

            return new AddCompanyPage(Driver);
        }
    }
}
