﻿using OpenQA.Selenium;
using Smt.Atomic.Tests.Automated.Common.Extensions;

namespace Smt.Atomic.Tests.Automated.WebApp.Pages
{
    public abstract class AuthenticatedWebAppPage : WebAppPage
    {
        protected const string HeaderSectionClassName = "header-section";
        protected const string ContentSectionClassName = "content-section";
        protected const string FooterSectionClassName = "footer-section";

        protected IWebElement HeaderSection => Driver.FindElementByClassName(HeaderSectionClassName);
        protected IWebElement ContentSection => Driver.FindElementByClassName(ContentSectionClassName);
        protected IWebElement FooterSection => Driver.FindElementByClassName(FooterSectionClassName);

        protected AuthenticatedWebAppPage(IWebDriver driver) : base(driver)
        {
        }

        public PublicPage LogOut()
        {
            new LogoutPage(Driver)
                .NavigateSelf();

            return new PublicPage(Driver);
        }
    }
}
