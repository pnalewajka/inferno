﻿using OpenQA.Selenium;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Tests.Automated.Common.Extensions;
using Smt.Atomic.Tests.Automated.WebApp.Consts;
using Smt.Atomic.Tests.Automated.WebApp.Models;
using Smt.Atomic.Tests.Automated.WebApp.Selectors;
using System;

namespace Smt.Atomic.Tests.Automated.WebApp.Pages.Locations
{
    public class AddLocationPage : AuthenticatedWebAppPage
    {
        public override string Url => Configuration.WebAppUrl + "/Dictionaries/Location/Add";
        private const string PageSelectorCssClass = "page-layout";
        private const string PopUpSelectorCssClass = "modal-content";
        private const string SelectLocationTableClassName = "grid-table";

        private IWebElement SelectLocationPopUp
            => Driver.FindElementByClassName(PopUpSelectorCssClass);

        private IWebElement LocationNameInput
            => Driver.FindElementByCssSelector(AddLocationSelectors.LocationNameTextField);

        private IWebElement LocationAddressInput
            => Driver.FindElementByCssSelector(AddLocationSelectors.LocationAdddressTextField);

        private IWebElement LocationCityPicker
            => ContentSection.FindElementByClassName(AddLocationSelectors.LocationCityPicker);

        private IWebElement AddLocationAddButton
            => Driver.FindElementByCssSelector(AutomationIdentifiers.AddForm);

        private IWebElement SelectLocationTable => SelectLocationPopUp.FindElementByClassName(SelectLocationTableClassName);

        public AddLocationPage(IWebDriver driver)
            : base(driver)
        {
        }

        public override By GetIsLoadedLocator()
        {
            return By.ClassName(PageSelectorCssClass);
        }

        public AddLocationPage FillInLocationDetails(AddLocationModel model)
        {
            LocationNameInput.SendKeys(model.LocationName);
            LocationAddressInput.SendKeys(model.AddressName);
            AddCityToLocation(model.CityName);

            return this;
        }

        public void AddCityToLocation(SecurityRoleType role)
        {
            LocationCityPicker.ClickElement();
            SelectLocationTable.FindElement(By.TagName("td")).ClickElement();
            SelectLocationPopUp.FindElementByCssSelector(AutomationIdentifiers.OkButton).ClickElement();
        }

        public LocationsPage AddLocation()
        {
            AddLocationAddButton.ClickElement();

            return new LocationsPage(Driver);
        }
    }
}