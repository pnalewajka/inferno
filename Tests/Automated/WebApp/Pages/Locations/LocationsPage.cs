﻿using OpenQA.Selenium;
using Smt.Atomic.Tests.Automated.Common.Extensions;
using Smt.Atomic.Tests.Automated.WebApp.Consts;

namespace Smt.Atomic.Tests.Automated.WebApp.Pages.Locations
{
    public class LocationsPage : AuthenticatedWebAppPage
    {
        public LocationsPage(IWebDriver driver)
            : base(driver)
        {
        }

        public override string Url => Configuration.WebAppUrl + "/Dictionaries/Location/List";
        private const string PageSelectorCssClass = "page-layout";
        public string FirstRowCompaniesTab => Driver.FindElementByCssSelector(AutomationIdentifiers.FirstRowFirstColumnElement).Text;

        public override By GetIsLoadedLocator()
        {
            return By.ClassName(PageSelectorCssClass);
        }

        public AddLocationPage GoToTheAddLocationSubpage()
        {
            var button = Driver.FindElementByCssSelector(AutomationIdentifiers.CardIndexAddButtonIdentifier);
            button.ClickElement();

            return new AddLocationPage(Driver); 
        }
    }
}