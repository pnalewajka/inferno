﻿using OpenQA.Selenium;
using Smt.Atomic.Tests.Automated.Common.Extensions;
using Smt.Atomic.Tests.Automated.Common.PageObjects;

namespace Smt.Atomic.Tests.Automated.WebApp.Pages
{
    public class LogInPage : BasePage
    {
        private const string UserNameSelectorId = "login";
        private const string PasswordSelectorId = "password";
        private const string SubmitSelectorCss = "input[type=submit]";
        private const string FakePasswordExtension = "fake-part";
        private const string PageSelectorCss = "#remember-me";

        public override string Url => Configuration.WebAppUrl + "/Accounts/Authentication/LogIn";

        public LogInPage(IWebDriver driver)
            : base(driver)
        {
        }

        public override By GetIsLoadedLocator()
        {
            return By.CssSelector(PageSelectorCss);
        }

        public LogInPage FailLogin(string username, string password)
        {
            var elementWithUsername = Driver.FindElementById(UserNameSelectorId);
            var elementWithPassword = Driver.FindElementById(PasswordSelectorId);
            var button = Driver.FindElementByCssSelector(SubmitSelectorCss);

            elementWithUsername.SendKeys(username);
            elementWithPassword.SendKeys(password + FakePasswordExtension);
            button.ClickElement();

            return this;
        }

        public AuthenticatedWebAppPage LogIn(string username, string password)
        {
            var elementWithUsername = Driver.FindElementById(UserNameSelectorId);
            var elementWithPassword = Driver.FindElementById(PasswordSelectorId);
            var button = Driver.FindElementByCssSelector(SubmitSelectorCss);

            elementWithUsername.SendKeys(username);
            elementWithPassword.SendKeys(password);
            button.ClickElement();

            return new GenericAuthenticatedWebAppPage(Driver);
        }
    }
}
