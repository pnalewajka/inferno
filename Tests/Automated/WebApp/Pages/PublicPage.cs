﻿using System;
using OpenQA.Selenium;
using Smt.Atomic.Tests.Automated.Common.Extensions;


namespace Smt.Atomic.Tests.Automated.WebApp.Pages
{
    public class PublicPage : WebAppPage
    {
        private const string PageSelectorCss = "div.content-section > div.content-wrapper > div.content.container-fluid";
        private const string LoginButtonSelectorCss = "ul.anonymous-user-info li a";

        public PublicPage(IWebDriver driver)
            : base(driver)
        {
        }

        public override string Url => Configuration.WebAppUrl;

        public override By GetIsLoadedLocator()
        {
            Console.WriteLine("looking for a page selector PageSelectorCss : " + PageSelectorCss);
            
            return By.CssSelector(PageSelectorCss);
        }

        public LogInPage GoToLogin()
        {
            var actionLink = Driver.FindElementByCssSelector(LoginButtonSelectorCss);
            actionLink.ClickElement();

            return new LogInPage(Driver);
        }
    }
}
