﻿using System;
using OpenQA.Selenium;
using Smt.Atomic.Tests.Automated.Common.PageObjects;

namespace Smt.Atomic.Tests.Automated.WebApp.Pages
{
    public class LogoutPage : BasePage
    {
        public override string Url => Configuration.WebAppUrl + "/Accounts/Authentication/LogOut";

        public LogoutPage(IWebDriver driver) : base(driver) { }

        public override By GetIsLoadedLocator()
        {
            throw new NotSupportedException();
        }
    }
}
