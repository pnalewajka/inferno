﻿using OpenQA.Selenium;
using Smt.Atomic.Tests.Automated.Common.Extensions;
using Smt.Atomic.Tests.Automated.Common.PageObjects;

namespace Smt.Atomic.Tests.Automated.WebApp.Pages
{
    public abstract class WebAppPage : BasePage
    {
        protected WebAppPage(IWebDriver driver) : base(driver)
        {
        }

        public string GetUICulture()
        {
            var body = Driver.FindElementByCssSelector("body");
            return body.GetAttribute("data-culture");
        }
    }
}
