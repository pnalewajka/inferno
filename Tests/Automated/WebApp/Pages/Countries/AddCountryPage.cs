﻿using OpenQA.Selenium;
using Smt.Atomic.Tests.Automated.Common.Extensions;
using Smt.Atomic.Tests.Automated.WebApp.Selectors;
using Smt.Atomic.Tests.Automated.WebApp.Models;
using Smt.Atomic.Tests.Automated.WebApp.Consts;
using Smt.Atomic.Tests.Automated.Common.Consts;

namespace Smt.Atomic.Tests.Automated.WebApp.Pages.Countries
{
    public class AddCountryPage : AuthenticatedWebAppPage
    {
        public override string Url => Configuration.WebAppUrl + "/Dictionaries/Country/Add";

        private IWebElement CountryNameInput
            => Driver.FindElementByCssSelector(AddCountrySelectors.CountryNameTextField);

        private IWebElement CountryIsoCodeInput
            => Driver.FindElementByCssSelector(AddCountrySelectors.CountryIsoCode);

        private IWebElement AddCountryAddButton
            => Driver.FindElementByCssSelector(AutomationIdentifiers.AddForm);

        public AddCountryPage(IWebDriver driver)
            : base(driver)
        {
        }

        public override By GetIsLoadedLocator()
        {
            return By.ClassName(AutomationIdentifiers.PageSelectorCssClass);
        }

        public AddCountryPage FillInCountryDetails(AddCountryModel model)
        {
            CountryNameInput.SendKeys(model.CountryName);
            CountryIsoCodeInput.SendKeys(model.IsoCode);

            return this;
        }

        public CountriesPage AddCountry()
        {
            AddCountryAddButton.ClickElement();

            return new CountriesPage(Driver);
        }
    }
}