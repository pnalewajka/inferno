﻿using OpenQA.Selenium;
using Smt.Atomic.Tests.Automated.Common.Consts;
using Smt.Atomic.Tests.Automated.Common.Extensions;
using Smt.Atomic.Tests.Automated.WebApp.Consts;
using System.Linq;

namespace Smt.Atomic.Tests.Automated.WebApp.Pages.Countries
{
    public class CountriesPage : AuthenticatedWebAppPage
    {
        public override string Url => Configuration.WebAppUrl + "/Dictionaries/Country/List";

        public bool ElementExists(string newElement)
            => WebDriverExtensions.FindElementsByCssSelector(Driver, AutomationIdentifiers.TableElements).Any(e => e.Text == newElement);

        public CountriesPage(IWebDriver driver)
            : base(driver)
        {
        }

        public override By GetIsLoadedLocator()
        {
            return By.ClassName(AutomationIdentifiers.PageSelectorCssClass);
        }

        public AddCountryPage GoToTheAddCountrySubpage()
        {
            var button = Driver.FindElementByCssSelector(AutomationIdentifiers.CardIndexAddButtonIdentifier);
            button.ClickElement();

            return new AddCountryPage(Driver);
        }
    }
}