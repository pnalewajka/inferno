﻿using System;
using System.Collections.Generic;

namespace Smt.Atomic.Tests.Automated.WebApp.Helpers
{
    internal class RandomHelper
    {
        public static TValue GetRandomValues<TValue>(IList<TValue> list)
        {
            var rand = new Random();
            return list[rand.Next(list.Count)];
        }

        public static int GetRandomValues(int start, int stop)
        {
            var rand = new Random();
            return rand.Next(start, stop);
        }
    }
}