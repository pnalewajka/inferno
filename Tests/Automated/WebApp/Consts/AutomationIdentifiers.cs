﻿namespace Smt.Atomic.Tests.Automated.WebApp.Consts
{
    public static class AutomationIdentifiers
    {
        // Add, view, edit, delete buttons
        public const string CardIndexViewButtonIdentifier = "button.btn.auto-card-index-view";
        public const string CardIndexAddButtonIdentifier = "button.btn.auto-card-index-add";
        public const string CardIndexEditButtonIdentifier = "button.btn.auto-card-index-edit";
        public const string CardIndexDeleteButtonIdentifier = "button.btn.auto-card-index-delete";

        // Export button 
        public const string CardIndexExportToExcelButtonIdentifier = "auto-card-index-export-excel";
        public const string CardIndexExportToCsvButtonIdentifier = "auto-card-index-export-csv";

        // Check first row and first column element
        public const string FirstRowFirstColumnElement = ".table > tbody > tr:nth-child(1) > td:nth-child(1)";
        public const string TableElements = ".table-striped.table-bordered.table-hover > tbody > tr > td";

        public const string AddForm = "input.btn.btn-primary";
        public const string OkButton = "button.btn.btn-primary";

        public const string PageSelectorCssClass = "page-layout";
        public const string ModelTitle = "modal-title";
        public const string PopUpSelectorCssClass = "modal-content";
    }
}