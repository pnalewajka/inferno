﻿using OpenQA.Selenium;
using Smt.Atomic.Tests.Automated.Common.Extensions;
using Smt.Atomic.Tests.Automated.Common.PageObjects;
using Smt.Atomic.Tests.Automated.WebApp.Pages;

namespace Smt.Atomic.Tests.Automated.WebApp.Components
{
    public class CommonDialogConfirm<TPage> : BaseComponent<TPage> where TPage : WebAppPage
    {
        private const string DialogId = "common-dialog-confirm";
        private const string ButtonOkCssSelector = @"#common-dialog-confirm button[data-button=""ok""]";
        private const string ButtonCancelCssSelector = @"#common-dialog-confirm button[data-button=""cancel""]";

        public CommonDialogConfirm(IWebDriver driver, TPage parent) : base(driver, parent)
        {
        }

        public override By GetIsLoadedLocator()
        {
            return By.Id(DialogId);
        }

        public TPage ClickYes()
        {
            var okButton = Driver.FindElementByCssSelector(ButtonOkCssSelector);
            okButton.Click();

            return Parent;
        }

        public TPage ClickCancel()
        {
            var cancelButton = Driver.FindElementByCssSelector(ButtonCancelCssSelector);
            cancelButton.Click();

            return Parent;
        }
    }
}
