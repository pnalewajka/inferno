﻿using System.Globalization;
using System.Threading;
using Smt.Atomic.Tests.Automated.Common.Extensions;
using Smt.Atomic.Tests.Automated.Common.Tests;
using Smt.Atomic.Tests.Automated.WebApp.Models;
using Smt.Atomic.Tests.Automated.WebApp.Pages;
using Smt.Atomic.Tests.Automated.WebApp.Scenarios.Accounts;
using Smt.Atomic.Tests.Automated.WebApp.Scenarios;

namespace Smt.Atomic.Tests.Automated.WebApp.Tests
{
    public abstract class WebAppTest : BaseTest
    {
        private string _originalCultureName;

        protected override void OnTestSetUp()
        {
            var publicPage = new PublicPage(Driver).NavigateSelf();
            _originalCultureName = Thread.CurrentThread.CurrentUICulture.Name;
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(publicPage.GetUICulture());
            Thread.CurrentThread.CurrentCulture = new CultureInfo(publicPage.GetUICulture());

            new LoginScenario(Driver).Perform(new LoginModel
            {
                Username = Configuration.AdminUsername,
                Password = Configuration.AdminPassword,
            });
        }

        protected override void OnTestCleanup()
        {
            new LogoutScenario(Driver).Perform(new EmptyModel());
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(_originalCultureName);
            Thread.CurrentThread.CurrentCulture = new CultureInfo(_originalCultureName);
        }
    }
}
