﻿using NUnit.Framework;
using Smt.Atomic.Tests.Automated.WebApp.Models;
using Smt.Atomic.Tests.Automated.WebApp.Scenarios;

namespace Smt.Atomic.Tests.Automated.WebApp.Tests
{
    class AddCountryTest : WebAppTest
    {
        [Test]
        public void PositiveAddCountryTest()
        {
            new AddCountryScenario(Driver).Perform(new AddCountryModel
            {
                IsoCode = "AT"
            });
        }
    }
}