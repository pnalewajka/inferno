﻿using NUnit.Framework;
using Smt.Atomic.Tests.Automated.WebApp.Models;
using Smt.Atomic.Tests.Automated.WebApp.Scenarios;

namespace Smt.Atomic.Tests.Automated.WebApp.Tests
{
    public class AddIndustryTest : WebAppTest
    {
        [Test]
        public void PositiveAddIndustryTest()
        {
            new AddIndustryScenario(Driver).Perform(new AddIndustryModel
            {
            });
        }
    }
}