﻿using NUnit.Framework;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.CrossCutting.Common.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.Atomic.Tests.Automated.Common.Extensions;
using Smt.Atomic.Tests.Automated.Data.Queries;
using Smt.Atomic.Tests.Automated.WebApp.Extensions;
using Smt.Atomic.Tests.Automated.WebApp.Models.Accounts;
using Smt.Atomic.Tests.Automated.WebApp.Pages;
using Smt.Atomic.Tests.Automated.WebApp.Pages.Accounts;
using Smt.Atomic.Tests.Automated.WebApp.Scenarios.Accounts;

namespace Smt.Atomic.Tests.Automated.WebApp.Tests.Accounts
{
    public class AddUserTest : WebAppTest
    {
        [Test]
        public void AddUserAndRegenerateToken()
        {
            var timeService = Resolve<ITimeService>();
            var scope = Resolve<IUnitOfWorkService<IAccountsDbScope>>().Create();

            var model = new AddUserModel
            {
                FirstName = "test",
                LastName = "user.01",
            };

            var publicPage =
                new AddUserScenario(Driver)
                    .Perform(model)
                    .LogOut();

            var time = timeService.GetCurrentTime();
            var firstToken = scope.Repositories.Users.GetUserLoginToken(model.Login, time);

            Assert.That(firstToken, Is.Not.Null.Or.Empty, "First token is empty");

            publicPage
                .NavigateTo<LogInPage>("?token=" + firstToken)
                .ChangeTo<SetPasswordPage>()
                .SetPassword(Configuration.AdminPassword)
                .AssertIsTrue(p => p.IsLoaded())
                .LogOut()
                .GoToLogin()
                .LogIn(Configuration.AdminUsername, Configuration.AdminPassword)
                .NavigateTo<UserIndexPage>()
                .Find(model.FullName)
                .SelectUser(model.FullName)
                .SendLoginToken()
                .LogOut();

            time = timeService.GetCurrentTime();
            var secondToken = scope.Repositories.Users.GetUserLoginToken(model.Login, time);

            Assert.That(secondToken, Is.Not.Null.Or.Empty, "Second token is empty");

            Assert.AreNotEqual(firstToken, secondToken, "Tokens are the same");

            publicPage
                .NavigateTo<LogInPage>("?token=" + secondToken)
                .ChangeTo<SetPasswordPage>()
                .SetPassword(Configuration.AdminPassword)
                .AssertIsTrue(p => p.IsLoaded())
                .LogOut()
                .GoToLogin()
                .LogIn(Configuration.AdminUsername, Configuration.AdminPassword);
        }

        [Test]
        public void AddProfile()
        {
            new AddUserScenario(Driver).Perform(new AddUserModel
            {
                FirstName = "test",
                LastName = "user.02",
            });

            new CreateProfileAndAddToUserScenario(Driver).Perform(new ProfileModel
            {
                ProfileName = "test.profile02",
                ProfileRole = SecurityRoleType.CanViewUsers,
            });
        }
    }
}
