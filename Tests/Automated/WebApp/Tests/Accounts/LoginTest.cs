﻿using NUnit.Framework;
using Smt.Atomic.Tests.Automated.Common.Extensions;
using Smt.Atomic.Tests.Automated.Common.Tests;
using Smt.Atomic.Tests.Automated.WebApp.Models;
using Smt.Atomic.Tests.Automated.WebApp.Pages;
using Smt.Atomic.Tests.Automated.WebApp.Scenarios.Accounts;
using Smt.Atomic.Tests.Automated.WebApp.Scenarios;

namespace Smt.Atomic.Tests.Automated.WebApp.Tests.Accounts
{    
    public class LoginTest : BaseTest
    {
        [Test]
        public void FailedFormsLogin()
        {
            var model = new LoginModel
            {
                Username = Configuration.AdminUsername,
                Password = Configuration.AdminPassword,
            };

            new PublicPage(Driver)
                .NavigateSelf()
                .AssertIsTrue(p => p.IsLoaded())
                .GoToLogin()
                .AssertIsTrue(p => p.IsLoaded())
                .FailLogin(model.Username, model.Password)
                .AssertIsTrue(p => p.IsLoaded());
        }

        [Test]
        public void SuccessfulFormsLogin()
        {
            new LoginScenario(Driver).Perform(new LoginModel
            {
                Username = Configuration.AdminUsername,
                Password = Configuration.AdminPassword,
            });

            new LogoutScenario(Driver).Perform(new EmptyModel());
        }
    }
}
