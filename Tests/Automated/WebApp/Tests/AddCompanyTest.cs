﻿using NUnit.Framework;
using Smt.Atomic.Tests.Automated.WebApp.Models;
using Smt.Atomic.Tests.Automated.WebApp.Scenarios.AddCompanyScenario;

namespace Smt.Atomic.Tests.Automated.WebApp.Tests
{
    public class AddCompanyTest : WebAppTest
    {
        [Test]
        public void PositiveAddCompanyTest()
        {
            new AddCompanyScenario(Driver).Perform(new AddCompanyModel 
            {
                CompanyDescription = "AUTO_TEST - Description",
                CompanyNavisionCode = "AUTO_TEST - Navision Code",
            });
        }
    }
}
