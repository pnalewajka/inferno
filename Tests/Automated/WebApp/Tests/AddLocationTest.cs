﻿using NUnit.Framework;
using Smt.Atomic.Tests.Automated.WebApp.Models;
using Smt.Atomic.Tests.Automated.WebApp.Scenarios;

namespace Smt.Atomic.Tests.Automated.WebApp.Tests
{
    public class AddLocationTest : WebAppTest
    {
        [Test]
        public void PositiveAddLocationTest()
        {
            new AddLocationScenario(Driver).Perform(new AddLocationModel
            {
                AddressName = "AUTO_TEST - ADDRESS",
            });
        }
    }
}