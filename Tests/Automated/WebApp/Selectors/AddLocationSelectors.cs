﻿namespace Smt.Atomic.Tests.Automated.WebApp.Selectors
{
    internal class AddLocationSelectors
    {
        // elements under the 'add locations' subpage
        public const string LocationNameTextField = "input#name.form-control";
        public const string LocationAdddressTextField = "input#address.form-control";
        public const string LocationCityPicker = "value-picker-add-button";
    }
}