﻿namespace Smt.Atomic.Tests.Automated.WebApp.Selectors
{
    public class AddCountrySelectors
    {
        public const string CountryNameTextField = "input#name.form-control";
        public const string CountryIsoCode = "input#iso-code.form-control";
    }
}