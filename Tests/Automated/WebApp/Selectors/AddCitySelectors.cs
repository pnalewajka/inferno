﻿namespace Smt.Atomic.Tests.Automated.WebApp.Selectors
{
    public class AddCitySelectors
    {
        public const string CityNameTextField = "input#name.form-control";
        public const string CityCountryPicker = "input-group";
        public const string CompanyApartmentAvailable = "is-company-apartment-available";
        public const string VoucherServiceAvailable = "is-voucher-service-available";

    }
}