﻿namespace Smt.Atomic.Tests.Automated.WebApp.Selectors
{
    internal class AddCompanySelectors
    {
        // elements under the 'add Companies' subpage
        public const string CompanyNameTextField = "input#name.form-control";
        public const string CompanyDescriptionTextField = "input#description.form-control";
        public const string CompanyEmailDomainTextField = "input#email-domain.form-control";
        public const string CompanyActiveDirectoryCodeTextField = "input#active-directory-code.form-control";
        public const string CompanyNavisionCodeTextField = "input#navision-code.form-control";
    }
}