﻿namespace Smt.Atomic.Tests.Automated.WebApp.Selectors
{
    internal class AddIndustrySelectors
    {
        public const string IndustryNameTextFieldEn = "input#name-English.form-control";
        public const string IndustryNameTextFieldPl = "input#name-Polish.form-control";
    }
}