﻿using OpenQA.Selenium;
using Smt.Atomic.Tests.Automated.Common.Extensions;
using Smt.Atomic.Tests.Automated.Common.Scenarios;
using Smt.Atomic.Tests.Automated.WebApp.Models;
using Smt.Atomic.Tests.Automated.WebApp.Pages;

namespace Smt.Atomic.Tests.Automated.WebApp.Scenarios
{
    public class LogoutScenario : DefaultScenario<EmptyModel>
    {
        public LogoutScenario(IWebDriver driver) : base(driver)
        {
        }

        public override bool Perform(EmptyModel model)
        {
            new GenericAuthenticatedWebAppPage(Driver)
                .AssertIsTrue(p => p.IsLoaded())
                .LogOut()
                .AssertIsTrue(p => p.IsLoaded());

            return true;
        }
    }
}
