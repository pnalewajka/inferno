﻿using Smt.Atomic.Tests.Automated.Common.Scenarios;
using Smt.Atomic.Tests.Automated.WebApp.Pages.Countries;
using OpenQA.Selenium;
using Smt.Atomic.Tests.Automated.WebApp.Models;
using Smt.Atomic.Tests.Automated.Common.Extensions;

namespace Smt.Atomic.Tests.Automated.WebApp.Scenarios
{
    public class AddCountryScenario : BaseScenario<AddCountryModel, CountriesPage>
    {
        public AddCountryScenario(IWebDriver driver)
            : base(driver)
        {
        }

        public override CountriesPage Perform(AddCountryModel model)
        {
            return new CountriesPage(Driver)
               .NavigateSelf()
               .AssertIsTrue(p => p.IsLoaded())
               .GoToTheAddCountrySubpage()
               .NavigateSelf()
               .AssertIsTrue(p => p.IsLoaded())
               .FillInCountryDetails(model)
               .AddCountry()
               .AssertIsTrue(p => p.ElementExists(model.CountryName));
        }
    }
}