﻿using OpenQA.Selenium;
using Smt.Atomic.Tests.Automated.WebApp.Pages.Companies;
using Smt.Atomic.Tests.Automated.Common.Extensions;
using Smt.Atomic.Tests.Automated.Common.Scenarios;
using Smt.Atomic.Tests.Automated.WebApp.Models;

namespace Smt.Atomic.Tests.Automated.WebApp.Scenarios.AddCompanyScenario
{
    public class AddCompanyScenario : BaseScenario<AddCompanyModel, CompaniesPage>
    {
        public AddCompanyScenario(IWebDriver driver)
            : base(driver)
        {
        }

        public override CompaniesPage Perform(AddCompanyModel model)
        {
            return new CompaniesPage(Driver)
                .NavigateSelf()
                .AssertIsTrue(p => p.IsLoaded())
                .GoToTheAddCompanySubpage()
                .NavigateSelf()
                .AssertIsTrue(p => p.IsLoaded())
                .FillInCompanyDetails(model)
                .AddCompany()
                .AssertIsTrue(p => p.FirstRowCompaniesTab.Equals(model.CompanyName));
        }
    }
}
