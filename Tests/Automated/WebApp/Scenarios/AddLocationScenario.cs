﻿using OpenQA.Selenium;
using Smt.Atomic.Tests.Automated.Common.Extensions;
using Smt.Atomic.Tests.Automated.Common.Scenarios;
using Smt.Atomic.Tests.Automated.WebApp.Models;
using Smt.Atomic.Tests.Automated.WebApp.Pages.Locations;

namespace Smt.Atomic.Tests.Automated.WebApp.Scenarios
{
    public class AddLocationScenario : BaseScenario<AddLocationModel, LocationsPage>
    {
        public AddLocationScenario(IWebDriver driver)
            : base(driver)
        {
        }

        public override LocationsPage Perform(AddLocationModel model)
        {
            return new LocationsPage(Driver)
                .NavigateSelf()
                .AssertIsTrue(p => p.IsLoaded())
                .GoToTheAddLocationSubpage()
                .NavigateSelf()
                .AssertIsTrue(p => p.IsLoaded())
                .FillInLocationDetails(model)
                .AddLocation()
                .AssertIsTrue(p => p.FirstRowCompaniesTab.Equals(model.LocationName));
        }
    }
}