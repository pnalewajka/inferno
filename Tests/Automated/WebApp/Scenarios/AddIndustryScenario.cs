﻿using OpenQA.Selenium;
using Smt.Atomic.Tests.Automated.Common.Extensions;
using Smt.Atomic.Tests.Automated.Common.Scenarios;
using Smt.Atomic.Tests.Automated.WebApp.Models;
using Smt.Atomic.Tests.Automated.WebApp.Pages.Industries;

namespace Smt.Atomic.Tests.Automated.WebApp.Scenarios
{
    public class AddIndustryScenario : BaseScenario<AddIndustryModel, IndustriesPage>
    {
        public AddIndustryScenario(IWebDriver driver)
            : base(driver)
        {
        }

        public override IndustriesPage Perform(AddIndustryModel model)
        {
            return new IndustriesPage(Driver)
               .NavigateSelf()
               .AssertIsTrue(p => p.IsLoaded())
               .GoToTheAddIndustrySubpage()
               .NavigateSelf()
               .AssertIsTrue(p => p.IsLoaded())
               .FillInIndustryDetails(model)
               .AddIndustry()
               .AssertIsTrue(p => p.FirstRowIndustryTab.Equals(model.IndustryName));
        }
    }
}