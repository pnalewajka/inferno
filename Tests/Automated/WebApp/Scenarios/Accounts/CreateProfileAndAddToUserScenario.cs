﻿using OpenQA.Selenium;
using Smt.Atomic.Tests.Automated.Common.Extensions;
using Smt.Atomic.Tests.Automated.Common.Scenarios;
using Smt.Atomic.Tests.Automated.WebApp.Models.Accounts;
using Smt.Atomic.Tests.Automated.WebApp.Pages.Accounts;

namespace Smt.Atomic.Tests.Automated.WebApp.Scenarios.Accounts
{
    public class CreateProfileAndAddToUserScenario : DefaultScenario<ProfileModel>
    {
        public CreateProfileAndAddToUserScenario(IWebDriver driver)
            : base(driver)
        {
        }

        public override bool Perform(ProfileModel model)
        {
            new ProfileIndexPage(Driver)
                .NavigateSelf()
                .AssertIsTrue(p => p.IsLoaded())
                .BeginAddProfile()
                .FillProfileForm(model)
                .AssertIsTrue(p => p.IsLoaded())
                .NavigateTo<UserIndexPage>()
                .AssertIsTrue(p => p.IsLoaded());

            return true;
        }
    }
}
