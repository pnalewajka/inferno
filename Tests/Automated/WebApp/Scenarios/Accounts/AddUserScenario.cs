﻿using OpenQA.Selenium;
using Smt.Atomic.Tests.Automated.Common.Extensions;
using Smt.Atomic.Tests.Automated.Common.Scenarios;
using Smt.Atomic.Tests.Automated.WebApp.Extensions;
using Smt.Atomic.Tests.Automated.WebApp.Models.Accounts;
using Smt.Atomic.Tests.Automated.WebApp.Pages.Accounts;

namespace Smt.Atomic.Tests.Automated.WebApp.Scenarios.Accounts
{
    public class AddUserScenario : BaseScenario<AddUserModel, UserIndexPage>
    {
        public AddUserScenario(IWebDriver driver)
            : base(driver)
        {
        }

        public override UserIndexPage Perform(AddUserModel model)
        {
            return new UserIndexPage(Driver)
                .NavigateSelf()
                .AssertIsTrue(p => p.IsLoaded())
                .BeginAddingUser()
                .AssertIsTrue(p => p.IsLoaded())
                .FillUserForm(model)
                .ClickAddUser()
                .AssertIsTrue(p => p.IsLoaded())
                .Find(model.FullName)
                .AssertIsTrue(p => p.IsUserOnList(model));
        }
    }
}
