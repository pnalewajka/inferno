﻿using Smt.Atomic.Tests.Automated.Common.Scenarios;
using Smt.Atomic.Tests.Automated.WebApp.Models;
using Smt.Atomic.Tests.Automated.WebApp.Pages.Cities;
using OpenQA.Selenium;
using Smt.Atomic.Tests.Automated.Common.Extensions;

namespace Smt.Atomic.Tests.Automated.WebApp.Scenarios
{
    public class AddCityScenarios : BaseScenario<AddCityModel, CitiesPage>
    {
        public AddCityScenarios(IWebDriver driver)
            : base(driver)
        {
        }

        public override CitiesPage Perform(AddCityModel model)
        {
            return new CitiesPage(Driver)
                .NavigateSelf()
                .AssertIsTrue(p => p.IsLoaded())
                .OpenAddCityPopUp()
                .FillInCityDetails(model)
                .AddCity()
                .AssertIsTrue(p => p.ElementExists(model.CityName));
        }
    }
}