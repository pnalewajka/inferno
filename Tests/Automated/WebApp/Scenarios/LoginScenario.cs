﻿using OpenQA.Selenium;
using Smt.Atomic.Tests.Automated.Common.Extensions;
using Smt.Atomic.Tests.Automated.Common.Scenarios;
using Smt.Atomic.Tests.Automated.WebApp.Models;
using Smt.Atomic.Tests.Automated.WebApp.Pages;

namespace Smt.Atomic.Tests.Automated.WebApp.Scenarios
{
    public class LoginScenario : DefaultScenario<LoginModel>
    {
        public LoginScenario(IWebDriver driver) : base(driver)
        {
        }

        public override bool Perform(LoginModel model)
        {
            new PublicPage(Driver)
                .NavigateSelf()
                .AssertIsTrue(p => p.IsLoaded())
                .GoToLogin()
                .AssertIsTrue(p => p.IsLoaded())
                .LogIn(model.Username, model.Password)
                .AssertIsTrue(p => p.IsLoaded());

            return true;
        }
    }
}
