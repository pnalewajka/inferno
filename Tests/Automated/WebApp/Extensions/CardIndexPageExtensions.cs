﻿using Smt.Atomic.Tests.Automated.WebApp.Pages;

namespace Smt.Atomic.Tests.Automated.WebApp.Extensions
{
    public static class CardIndexPageExtensions
    {
        public static TCardIndexPage Find<TCardIndexPage>(this TCardIndexPage cardIndexPage, string filter)
            where TCardIndexPage : CardIndexPage
        {
            cardIndexPage.FindText(filter);
            return cardIndexPage;
        }

        public static TCardIndexPage Confirm<TCardIndexPage>(this TCardIndexPage cardIndexPage)
            where TCardIndexPage : CardIndexPage
        {
            cardIndexPage.ConfirmChoice();
            return cardIndexPage;
        }
    }
}
