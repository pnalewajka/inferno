﻿using Smt.Atomic.Tests.Automated.Common.Scenarios;
using Smt.Atomic.Tests.Automated.WebApp.Helpers;

namespace Smt.Atomic.Tests.Automated.WebApp.Models
{
    public class AddCountryModel : IScenarioModel
    {
        private string _countryName;

        private static string[] _countryNameList = new string[]
        {
            "Poland",
            "France",
            "Germany",
            "Sweden",
            "United Kingdom",
            "United States"
        };

        public string IsoCode { get; set; }

        public string CountryName
        {
            get
            {
                if (_countryName == null)
                {
                    _countryName = $"{RandomHelper.GetRandomValues(_countryNameList)}{RandomHelper.GetRandomValues(1, 200)}_TEST";
                }

                return _countryName;
            }
            set
            {
                _countryName = value;
            }
        }
    }
}