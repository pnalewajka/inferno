﻿using Smt.Atomic.Tests.Automated.Common.Scenarios;
using Smt.Atomic.Tests.Automated.WebApp.Helpers;
using System.Collections.Generic;

namespace Smt.Atomic.Tests.Automated.WebApp.Models
{
    public class AddIndustryModel : IScenarioModel
    {
        private string _industryName;

        private static string[] _industryNameList = new string[]
        {
            "Education",
            "Transportation",
            "Public sector",
            "Automotive",
            "Insurance",
            "Media+Entertainment",
            "Healthcare"
        };

        public string IndustryName
        {
            get
            {
                if (_industryName == null)
                {
                    _industryName = $"{RandomHelper.GetRandomValues(_industryNameList)}{RandomHelper.GetRandomValues(1, 200)}_TEST";
                }

                return _industryName;
            }

            set
            {
                _industryName = value;
            }
        }
    }
}