﻿using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Tests.Automated.Common.Scenarios;
using Smt.Atomic.Tests.Automated.WebApp.Helpers;
using System.Collections.Generic;

namespace Smt.Atomic.Tests.Automated.WebApp.Models
{
    public class AddLocationModel : IScenarioModel
    {
        private string _locationName;

        private static List<string> locationNameList = new List<string>(new string[]
        {
            "Białystok",
            "Warszawa",
            "Wrocław",
            "Kraków",
            "Poznań",
            "Katowice"
        });

        public string AddressName { get; set; }
        public SecurityRoleType CityName { get; set; }

        public string LocationName
        {
            get
            {
                if (_locationName == null)
                {
                    _locationName = $"{RandomHelper.GetRandomValues(locationNameList)}{RandomHelper.GetRandomValues(1, 100)}_TEST";
                }

                return _locationName;
            }

            set
            {
                _locationName = value;
            }
        }
    }
}