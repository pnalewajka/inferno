﻿using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Tests.Automated.Common.Scenarios;

namespace Smt.Atomic.Tests.Automated.WebApp.Models.Accounts
{
    public class ProfileModel : IScenarioModel
    {
        public string ProfileName { get; set; }
        public string ActiveDirectoryGroup { get; set; }
        public SecurityRoleType ProfileRole { get; set; }
    }
}
