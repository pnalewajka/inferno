﻿using Smt.Atomic.Tests.Automated.Common.Scenarios;

namespace Smt.Atomic.Tests.Automated.WebApp.Models.Accounts
{
    public class LoginModel : IScenarioModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
