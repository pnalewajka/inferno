﻿using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Tests.Automated.Common.Scenarios;

namespace Smt.Atomic.Tests.Automated.WebApp.Models.Accounts
{
    public class AddUserModel : IScenarioModel
    {
        private string _userEmail;

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Login => $"{FirstName.ToLower()}.{LastName.ToLower()}";

        public string Email
        {
            get
            {
                if (_userEmail == null)
                {
                    return $"{FirstName}.atomic.{LastName}@smtsoftware.com";
                }
                else
                {
                    return _userEmail;
                }
            }
            set
            {
                _userEmail = value;
            }
        }

        public bool IsActive { get; set; }
    }
}
