﻿using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.Atomic.Tests.Automated.Common.Scenarios;
using Smt.Atomic.Tests.Automated.WebApp.Helpers;
using System.Collections.Generic;

namespace Smt.Atomic.Tests.Automated.WebApp.Models
{
    public class AddCityModel : IScenarioModel
    {
        private string _cityName;

        private static List<string> cityNameList = new List<string>(new string[]
        {
            "Białystok",
            "Berlin",
            "Katowice",
            "Kraków",
            "Lublin",
        });

        public SecurityRoleType CountryName { get; set; }

        public bool CompanyApartmentAvailable { get; set; }

        public bool VoucherServiceAvailable { get; set; }

        public string CityName
        {
            get
            {
                if (_cityName == null)
                {
                    _cityName = $"{RandomHelper.GetRandomValues(cityNameList)}{RandomHelper.GetRandomValues(1, 100)}_TEST";
                }

                return _cityName;
            }
            set
            {
                _cityName = value;
            }
        }
    }
}