﻿using System.Collections.Generic;
using Smt.Atomic.Tests.Automated.Common.Scenarios;
using Smt.Atomic.Tests.Automated.WebApp.Helpers;

namespace Smt.Atomic.Tests.Automated.WebApp.Models
{
    public class AddCompanyModel : IScenarioModel
    {
        private string _companyName;
        private string _activeDirectoryCode;

        private static List<string> companyNameList = new List<string>(new string[]
        {
            "SMT Software",
            "SMT Software Services",
            "BLS team",
            "Software Development Center",
            "intive"
        });

        private static List<string> companyTypeList = new List<string>(new string[]
         {
             "UK Ltd",
             "S.A.",
             "Sp. z O.O.",
             "GmbH",
             "UK"
        });

        private string companyNamePrefix = $"{RandomHelper.GetRandomValues(companyNameList)}{RandomHelper.GetRandomValues(1, 100)}";
        public string CompanyDescription { get; set; }
        public string CompanyEmailDomain { get; set; }
        public string CompanyNavisionCode { get; set; }

        public string CompanyName
        {
            get
            {
                if (_companyName == null)
                {
                    _companyName = $"{companyNamePrefix}_{RandomHelper.GetRandomValues(companyTypeList)}_TEST";
                }

                return _companyName;
            }
            set
            {
                _companyName = value;
            }
        }

        public string CompanyActiveDirectoryCode
        {
            get
            {
                return _activeDirectoryCode != null ? _activeDirectoryCode : companyNamePrefix;
            }
            set
            {
                _activeDirectoryCode = value;
            }
        }
    }
}
