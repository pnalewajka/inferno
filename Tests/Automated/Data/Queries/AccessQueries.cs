﻿using System;
using System.Linq;
using Smt.Atomic.Data.Entities.Modules.Accounts;

namespace Smt.Atomic.Tests.Automated.Data.Queries
{
    public static class AccessQueries
    {
        public static string GetUserLoginToken(this IQueryable<User> users, long userId, DateTime time)
        {
            return users.Where(u => u.Id == userId)
                .Select(u => u.LoginToken)
                .Where(lt => lt.ExpiresOn > time)
                .Select(lt => lt.Content)
                .SingleOrDefault();
        }

        public static string GetUserLoginToken(this IQueryable<User> users, string userLogin, DateTime time)
        {
            return users.Where(u => u.Login == userLogin)
                .Select(u => u.LoginToken)
                .Where(lt => lt.ModifiedOn <= time)
                .Where(lt => lt.ExpiresOn > time)
                .Select(lt => lt.Content)
                .SingleOrDefault();
        }
    }
}
