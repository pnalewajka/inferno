﻿using System.ServiceModel;
using Smt.Atomic.WebServices.Accounts.Contracts.Data;
using Smt.Atomic.WebServices.Accounts.Contracts.Service;

namespace Smt.Atomic.Tests.Automated.WebService.Helpers
{
    public static class AuthenticationServiceHelper
    {
        public static AuthenticationResponse Authenticate(string username, string password)
        {
            using (var factory = new ChannelFactory<IAuthenticationService>("IAuthenticationService"))
            {
                var channel = factory.CreateChannel();
                var request = new AuthenticationRequest
                {
                    Login = username,
                    Password = password,
                };

                var result = channel.Authenticate(request);

                return result;
            }
        }
    }
}
