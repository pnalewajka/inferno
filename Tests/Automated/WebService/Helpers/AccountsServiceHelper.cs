﻿using System.ServiceModel;
using Smt.Atomic.WebServices.Accounts.Contracts.Data;
using Smt.Atomic.WebServices.Accounts.Contracts.Service;
using Smt.Atomic.WebServices.Common.Factories;

namespace Smt.Atomic.Tests.Automated.WebService.Helpers
{
    public static class AccountsServiceHelper
    {
        public static GetMyUserDataResponse GetMyUserData(string username, string token)
        {
            using (var factory = new ChannelFactory<IAccountsService>("IAccountsService"))
            {
                var channel = factory.CreateChannel();
                using (OperationContextScopeFactory.CreateWithIdentityHeader((IContextChannel)channel, token))
                {
                    var request = new GetMyUserDataRequest
                    {
                        Username = username,
                    };

                    return channel.GetMyUserData(request);
                }
            }
        }

        public static GetUserDataResponse GetUserData(string username, string token)
        {
            using (var factory = new ChannelFactory<IAccountsService>("IAccountsService"))
            {
                var channel = factory.CreateChannel();
                using (OperationContextScopeFactory.CreateWithIdentityHeader((IContextChannel)channel, token))
                {
                    var request = new GetUserDataRequest
                    {
                        Username = username,
                    };

                    return channel.GetUserData(request);
                }
            }
        }
    }
}
