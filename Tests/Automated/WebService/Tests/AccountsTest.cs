﻿using NUnit.Framework;
using Smt.Atomic.Tests.Automated.WebService.Helpers;

namespace Smt.Atomic.Tests.Automated.WebService.Tests
{
    [TestFixture]
    public class AccountsTest
    {
        [Test]
        [TestCase("x", "y")]
        public void TestFailedAuthentication(string username, string password)
        {
            var authenticationResponse = AuthenticationServiceHelper.Authenticate(username, password);

            Assert.IsNotNull(authenticationResponse);
            Assert.IsNullOrEmpty(authenticationResponse.Token);
            Assert.IsNullOrEmpty(authenticationResponse.Username);
            Assert.IsNotNullOrEmpty(authenticationResponse.Message);
        }

        [Test]
        [TestCase("administrator","Haslo123!")]
        public void TestGetMyUserData(string username, string password)
        {
            var authorizationResponse = AuthenticationServiceHelper.Authenticate(username, password);

            Assert.IsNotNull(authorizationResponse);
            Assert.IsNullOrEmpty(authorizationResponse.Message);
            Assert.IsNotNullOrEmpty(authorizationResponse.Token);
            Assert.IsNotNullOrEmpty(authorizationResponse.Username);

            var userData = AccountsServiceHelper.GetMyUserData(username, authorizationResponse.Token);

            Assert.IsNotNull(userData);
            Assert.IsTrue(username.Equals(userData.UserName));
        }

        [Test]
        [TestCase("administrator", "Haslo123!")]
        public void TestGetUserData(string username, string password)
        {
            var authorizationResponse = AuthenticationServiceHelper.Authenticate(username, password);

            Assert.IsNotNull(authorizationResponse);
            Assert.IsNullOrEmpty(authorizationResponse.Message);
            Assert.IsNotNullOrEmpty(authorizationResponse.Token);
            Assert.IsNotNullOrEmpty(authorizationResponse.Username);

            var userData = AccountsServiceHelper.GetUserData(username, authorizationResponse.Token);

            Assert.IsNotNull(userData);
            Assert.IsTrue(username.Equals(userData.UserName));
        }
    }
}
