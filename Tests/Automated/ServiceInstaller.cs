﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Tests.Automated.Common.Consts;

namespace Smt.Atomic.Tests.Automated
{
    public class ServiceInstaller : AtomicInstaller
    {
        public override void Install(ContainerType containerType, IWindsorContainer container)
        {
            container.Register
            (
                Component.For<Configuration>().ImplementedBy<Configuration>().LifestyleTransient()
            );
        }
    }
}