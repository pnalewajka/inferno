﻿using System.Collections;
using System.Collections.Generic;
using Moq;

namespace Smt.Atomic.Tests.Common.Wrappers
{
    public class RepositoryDescription
    {
        public Mock RepositoryMock { get; set; }
        public Mock ReadonlyRepositoryMock { get; set; }

        public IList EntityList { get; set; }
    }
}