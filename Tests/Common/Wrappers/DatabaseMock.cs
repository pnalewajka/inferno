using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using Castle.Core.Internal;
using Moq;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Interfaces;
using Smt.Atomic.Data.Repositories.Interfaces;

namespace Smt.Atomic.Tests.Common.Wrappers
{
    /// <summary>
    /// Database mocking class
    /// </summary>
    public abstract class DatabaseMock
    {
        private uint _timestamp;

        protected static readonly object LockObject = new object();

        protected readonly IDictionary<Type, RepositoryDescription> Mocks = new Dictionary<Type, RepositoryDescription>();

        protected readonly IDictionary<Type, List<Action<object, DatabaseMock>>> NavigationProperyHandlers =
            new Dictionary<Type, List<Action<object, DatabaseMock>>>();

        public long LastId { get; private set; }

        public byte[] LastTimestamp => BitConverter.GetBytes(_timestamp);

        /// <summary>
        /// Add entity to database mock, will create repository mock if one doesn't exists yet
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        public long AddEntity<TEntity>(TEntity entity)
            where TEntity : class, IEntity
        {
            GetRepositoryMock<TEntity>();

            var mockDescription = Mocks[typeof(TEntity)];

            var list = (List<TEntity>)mockDescription.EntityList;
            list.Add(entity);
            CreateCollections(entity);
            FillInNavigationProperties(entity);
            SetTimestamp(entity);
            LastId = Math.Max(LastId, entity.Id);

            return entity.Id;
        }

        /// <summary>
        /// Add entities
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entities"></param>
        public void AddEntities<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IEntity
        {
            foreach (var entity in entities)
            {
                AddEntity(entity);
            }
        }

        /// <summary>
        /// Add code for handling creation & maintainance of navigation properties & collections
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TContext"></typeparam>
        /// <param name="action"></param>
        public void RegisterNavigationPropertyHandler<TEntity, TContext>(
            Action<TEntity, TContext> action
            )
            where TEntity : IEntity
            where TContext : DatabaseMock
        {
            var entityType = typeof (TEntity);

            if (!NavigationProperyHandlers.ContainsKey(entityType))
            {
                NavigationProperyHandlers[entityType] = new List<Action<object, DatabaseMock>>();
            }

            NavigationProperyHandlers[entityType].Add((o, c) => action((TEntity) o, (TContext) c));
        }

        protected void FillInNavigationProperties<TEntity>(TEntity entity) where TEntity : IEntity
        {
            var entityType = typeof (TEntity);

            if (NavigationProperyHandlers.ContainsKey(entityType))
            {
                foreach (var handler in NavigationProperyHandlers[entityType])
                {
                    handler(entity, this);
                }
            }
        }

        protected void CreateCollections<TEntity>(TEntity entity)
        {
            var entityType = typeof (TEntity);

            foreach (var collectionProperty in GetVirtualPropertiesForCollections(entityType))
            {
                var collectionType =
                    typeof (List<>).MakeGenericType(collectionProperty.PropertyType.GetGenericArguments().Single());
                collectionProperty.SetValue(entity, ReflectionHelper.CreateInstance(collectionType));
            }
        }

        private IEnumerable<PropertyInfo> GetVirtualPropertiesForCollections(Type entityType)
        {
            var virtualProperties =
                entityType.GetProperties(BindingFlags.Public
                                         | BindingFlags.GetProperty
                                         | BindingFlags.SetProperty
                                         | BindingFlags.Instance)
                    .Where(
                        p => PropertyHelper.IsVirtual(p) == true
                             && p.PropertyType.IsGenericType
                             && p.PropertyType.GetGenericTypeDefinition() == typeof (ICollection<>)
                    ).ToList();

            return virtualProperties;
        }

        protected IEnumerable<T> GetAll<T>() where T : IEntity
        {
            if (!Mocks.ContainsKey(typeof (T)))
            {
                return Enumerable.Empty<T>();
            }

            var mockDescription = Mocks[typeof (T)];

            return (IEnumerable<T>) mockDescription.EntityList;
        }

        protected long GetNextId<T>() where T : IEntity
        {
            var all = GetAll<T>().ToList();

            if (all.Any())
            {
                return all.Max(t => t.Id) + 1;
            }

            return 1;
        }

        protected T GetDefault<T>() where T : IEntity, new()
        {
            var result = new T {Id = GetNextId<T>()};

            return result;
        }

        protected T GetById<T>(long id) where T : IEntity
        {
            var mockDescription = Mocks[typeof (T)];
            var entityList = (List<T>) mockDescription.EntityList;

            return entityList.Single(t => t.Id == id);
        }

        /// <summary>
        /// Return list of all registered repository mocks
        /// </summary>
        /// <returns></returns>
        public IEnumerable<KeyValuePair<Type, Mock>> GetMocks()
        {
            foreach (var item in Mocks)
            {
                var entityType = item.Key;

                var repositoryMock = item.Value.RepositoryMock;
                var repositoryType = typeof(IRepository<>).MakeGenericType(entityType);

                var readonlyRepositoryMock = item.Value.ReadonlyRepositoryMock;
                var readonlyRepositoryType = typeof(IReadOnlyRepository<>).MakeGenericType(entityType);

                yield return new KeyValuePair<Type, Mock>(repositoryType, repositoryMock);
                yield return new KeyValuePair<Type, Mock>(readonlyRepositoryType, readonlyRepositoryMock);
            }
        }

        private RepositoryDescription InternalGetRepositoryMock<TEntity>() where TEntity : class, IEntity
        {
            var entityType = typeof (TEntity);

            if (Mocks.ContainsKey(entityType))
            {
                return Mocks[entityType];
            }

            var mockDescription = new RepositoryDescription();

            {
                var repositoryMock = new Mock<IRepository<TEntity>>();

                var entityList = new List<TEntity>();

                repositoryMock.Setup(x => x.Add(It.IsAny<TEntity>())).Callback<TEntity>(x =>
                {
                    lock (LockObject)
                    {
                        SetTimestamp(x);
                        entityList.Add(x);
                        x.Id = ++LastId;
                    }

                    FillInNavigationProperties(x);
                });

                repositoryMock.Setup(x => x.Edit(It.IsAny<TEntity>())).Callback<TEntity>(x =>
                {
                    lock (LockObject)
                    {
                        var oldEntity = entityList.Single(e => e.Id == x.Id);
                        SetTimestamp(oldEntity);
                        var propertyDictionary = DictionaryHelper.ToDictionary(x, n => n, v => v);
                        DictionaryHelper.PopulateObjectProperties(oldEntity, propertyDictionary);
                    }
                });

                repositoryMock.Setup(x => x.Delete(It.IsAny<TEntity>(), It.IsAny<bool>())).Callback<TEntity, bool>((x,force) =>
                {
                    lock (LockObject)
                    {
                        var oldEntity = entityList.Single(e => e.Id == x.Id);
                        entityList.Remove(oldEntity);
                    }
                });

                //mocking IQuerable
                repositoryMock.Setup(m => m.GetEnumerator()).Returns(() => CloningHelper.CloneList(entityList).AsQueryable().GetEnumerator());
                repositoryMock.Setup(m => m.Expression).Returns(() => CloningHelper.CloneList(entityList).AsQueryable().Expression);
                repositoryMock.Setup(m => m.ElementType).Returns(typeof(TEntity));
                repositoryMock.Setup(m => m.Provider).Returns(() => CloningHelper.CloneList(entityList).AsQueryable().Provider);

                repositoryMock.Setup(m => m.Include(It.IsAny<string>()))
                    .Returns(() => CloningHelper.CloneList(entityList).AsQueryable());

                mockDescription.EntityList = entityList;
                mockDescription.ReadonlyRepositoryMock = repositoryMock;
                mockDescription.RepositoryMock = repositoryMock;
            }

            Mocks[entityType] = mockDescription;

            return mockDescription;
            
        }

        /// <summary>
        /// Get or create repository mock for specific entity type
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public Mock<IRepository<TEntity>> GetRepositoryMock<TEntity>() where TEntity : class, IEntity
        {
            var mock = InternalGetRepositoryMock<TEntity>();

            return (Mock<IRepository<TEntity>>)mock.RepositoryMock;
        }

        /// <summary>
        /// Get or create repository mock for specific entity type
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public Mock<IReadOnlyRepository<TEntity>> GetReadOnlyRepositoryMock<TEntity>() where TEntity : class, IEntity
        {
            var mock = InternalGetRepositoryMock<TEntity>();

            return (Mock<IReadOnlyRepository<TEntity>>)mock.ReadonlyRepositoryMock;
        }

        /// <summary>
        /// Get repository mock basing on entity type provided as method argument
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        public object GetRepositoryMockFactoryMethod(Type entityType)
        {
            var method = typeof(DatabaseMock).GetMethod("GetRepositoryMock");
            var generic = method.MakeGenericMethod(entityType);

            return ((Mock)generic.Invoke(this, null)).Object;
        }

        /// <summary>
        /// Get repository mock basing on entity type provided as method argument
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        public object GetReadOnlyRepositoryMockFactoryMethod(Type entityType)
        {
            var method = typeof(DatabaseMock).GetMethod("GetReadOnlyRepositoryMock");
            var generic = method.MakeGenericMethod(entityType);

            return ((Mock)generic.Invoke(this, null)).Object;
        }

        private void SetTimestamp(object entity)
        {
            _timestamp++;

            entity
                .GetType()
                .GetProperties()
                .Where(p => Attribute.IsDefined(p, typeof(TimestampAttribute)) && p.CanWrite)
                .ForEach(p => p.SetValue(entity, LastTimestamp));
        }
    }
}