﻿using System;
using Moq;

namespace Smt.Atomic.Tests.Common.AutoMock
{
    /// <summary>
    /// Access to mock creation 
    /// </summary>
    internal interface IMockingService
    {
        /// <summary>
        /// Create mock if not yet registered, return registered otherwise
        /// </summary>
        /// <param name="service"></param>
        /// <returns></returns>
        Mock CreateUnregisteredMock(Type service);
    }
}
