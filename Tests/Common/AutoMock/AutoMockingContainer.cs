﻿using System;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers;
using Castle.Windsor;
using Moq;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.Tests.Common.Wrappers;

namespace Smt.Atomic.Tests.Common.AutoMock
{
    /// <summary>
    /// Automocking container, all not registered interfaces will be resolved as mocks
    /// To setup mocks 
    /// </summary>
    public class AutoMockingContainer : WindsorContainer, IMockingService
    {
        private readonly MockHeap _heap = new MockHeap();
        private readonly MockGenerator _mockGenerator = new MockGenerator();

        public AutoMockingContainer()
        {
            Initialize();
        }

        private void Initialize()
        {
            AddFacility<TypedFactoryFacility>();
            Register(Component.For<ILazyComponentLoader>().Instance(new MockLazyComponentLoader(this)));
        }

        /// <summary>
        /// Get or create (if neccessary) - strongly typed
        /// </summary>
        /// <typeparam name="TService"></typeparam>
        /// <returns></returns>
        public Mock<TService> GetMock<TService>() where TService : class
        {
            var type = typeof(TService);
            return (Mock<TService>)GetMock(type);
        }

        /// <summary>
        /// Get or create (if neccessary) - weakly typed
        /// </summary>
        /// <param name="service"></param>
        /// <returns></returns>
        public Mock GetMock(Type service)
        {
            if (_heap.ContainsType(service))
            {
                return _heap[service];
            }
            var mock = CreateUnregisteredMock(service);
            return mock;
        }

        /// <summary>
        /// Create mock if not registered, return previously registered otherwise
        /// </summary>
        /// <param name="service"></param>
        /// <returns></returns>
        public Mock CreateUnregisteredMock(Type service)
        {
            if (_heap.ContainsType(service))
            {
                return _heap[service];
            }
            var mock = _mockGenerator.Generate(service);
            if (mock == null)
            {
                throw new ArgumentOutOfRangeException(nameof(service));
            }
            _heap[service] = mock;
            return mock;
        }

        /// <summary>
        /// Register repositories exposed by database mock
        /// </summary>
        /// <param name="databaseMock"></param>
        public void RegisterDatabase([NotNull] DatabaseMock databaseMock)
        {
            if (databaseMock == null)
            {
                throw new ArgumentNullException(nameof(databaseMock));
            }

            foreach (var keyValuePair in databaseMock.GetMocks())
            {
                if (!_heap.ContainsType(keyValuePair.Key))
                {
                    _heap[keyValuePair.Key] = keyValuePair.Value;
                }
            }
        }

        /// <summary>
        /// Register mock by passing it's instance
        /// </summary>
        /// <typeparam name="TService"></typeparam>
        /// <param name="mockInstance"></param>
        public void RegisterMock<TService>([NotNull] Mock<TService> mockInstance) where TService : class
        {
            if (mockInstance == null)
            {
                throw new ArgumentNullException(nameof(mockInstance));
            }

            if (!_heap.ContainsType(typeof (TService)))
            {
                _heap[typeof (TService)] = mockInstance;
            }
        }
    }
}
