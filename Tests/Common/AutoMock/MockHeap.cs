﻿using System;
using System.Collections.Generic;
using Moq;

namespace Smt.Atomic.Tests.Common.AutoMock
{
    /// <summary>
    /// Storage class for mock and types that were mocked
    /// </summary>
    internal class MockHeap
    {
        private class Data
        {
            public Mock Mock;
            public Type Type;
        }
        
        private readonly Dictionary<string, Data> _mocks = new Dictionary<string, Data>();

        /// <summary>
        /// By type indexer
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public virtual Mock this[Type type]
        {
            get
            {
                return this[type, type.FullName];
            }
            set
            {
                this[type, type.FullName] = value;
            }
        }

        /// <summary>
        /// By type & key indexer
        /// </summary>
        /// <param name="type"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public virtual Mock this[Type type, string key]
        {
            get
            {
                try
                {
                    var result = _mocks[key];
                    if (result.Type != type)
                    {
                        throw new ArgumentException($"'{key}' was not of type {type}");
                    }
                    return result.Mock;
                }
                catch (KeyNotFoundException)
                {
                    throw new IndexOutOfRangeException();
                }
            }
            set
            {
                _mocks[key] = new Data { Mock = value, Type = type };
            }
        }

        /// <summary>
        /// Check if type was added previously
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool ContainsType(Type type)
        {
            return ContainsType(type, type.FullName);
        }

        /// <summary>
        /// Check if type and key was added previously
        /// </summary>
        /// <param name="type"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool ContainsType(Type type, string key)
        {
            return _mocks.ContainsKey(key);
        }
    }
}
