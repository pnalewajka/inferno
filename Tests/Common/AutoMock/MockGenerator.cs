﻿using System;
using System.Diagnostics;
using Moq;
using Smt.Atomic.CrossCutting.Common.Annotations;

namespace Smt.Atomic.Tests.Common.AutoMock
{
    /// <summary>
    /// Mock generation
    /// </summary>
    internal class MockGenerator
    {
        /// <summary>
        /// Generate mock for type, only if type is interface
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public Mock Generate([NotNull] Type type)
        {
            if (type == null)
            {
                throw new ArgumentException("type");
            }

            if (type.IsInterface)
            {
                return InstantiateMock(type);
            }

            throw new NotSupportedException("Non-interface type aren't supported");
        }

        private Mock InstantiateMock(Type type)
        {
            var genericConstructor = GetMockType(type).GetConstructor(new Type[0]);
            Debug.Assert(genericConstructor != null, "genericCtor != null");
            return (Mock)genericConstructor.Invoke(new object[0]);
        }

        private static Type GetMockType(Type type)
        {
            return typeof(Mock<>).MakeGenericType(type);
        }

    }
}
