using System;
using System.Collections;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers;

namespace Smt.Atomic.Tests.Common.AutoMock
{
    /// <summary>
    /// Lazy component loader implementation for resolving mocks after usual windsor lookups
    /// </summary>
    internal class MockLazyComponentLoader : ILazyComponentLoader
    {
        private readonly IMockingService _mockingService;

        public MockLazyComponentLoader(IMockingService mockingService)
        {
            _mockingService = mockingService;
        }

        public IRegistration Load(string name, Type service, IDictionary arguments)
        {
            if (service.IsInterface || service.IsAbstract)
            {
                return Component.For(service).Instance(_mockingService.CreateUnregisteredMock(service).Object);
            }

            return null;
        }
    }
}