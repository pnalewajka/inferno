﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.Tests.Common.AutoMock;

namespace Smt.Atomic.Tests.Common
{
    /// <summary>
    /// Container configuration for tests with automocking container
    /// </summary>
    public class TestContainerConfiguration : ContainerConfiguration
    {
        public AutoMockingContainer AutoMockingContainer { get; private set; }

        public override WindsorContainer Configure(ContainerType containerType)
        {
            InternalContainer = AutoMockingContainer = new AutoMockingContainer();
            return InternalContainer;
        }

        public void Register(Type service, object instance)
        {
            AutoMockingContainer.Register(Component.For(service).Instance(instance));
        }

        public void Register<TService>(object instance)
        {
            Register(typeof(TService), instance);
        }

        protected override IEnumerable<AtomicInstaller> GetInstallers(ContainerType containerType)
        {
            return Enumerable.Empty<AtomicInstaller>();
        }
    }
}
