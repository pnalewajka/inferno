#Script for running any registered scriptlet. 
#Use it passing scriptlet and it's parameters as _build arguments
$path = (Get-Variable MyInvocation).Value.MyCommand.Path | Split-Path
Import-Module (Join-Path $path "Tools\PowerShellAutomation\Modules\FileProcessing.psm1")
$expression = $args[0]

for($i = 1; $i -lt $args.Length; $i++)
{
	$expression += " '" + $args[$i] + "'"
}

Invoke-Expression $expression
