﻿using CommandLine;

namespace Smt.CalculatorUtils
{
    class StartupOptions
    {
        [Option("DetectLegacyDiagnosticItems", HelpText = "Detect all legacy diagnostic items from Invoices and Settlements")]
        public bool DetectLegacyDiagnosticItems { get; set; }

        [Option("CheckCostItemsRegression", HelpText = "Check all the CostItems from Invoices and Settlements")]
        public bool CheckCostItemsRegression { get; set; }

        [Option("RefreshDiagnosticItems", HelpText = "Refresh all the DiagnosticItems from Invoices and Settlements")]
        public bool RefreshDiagnosticItems { get; set; }

        [Option("CheckRegression", HelpText = "Check all the Calculations (CostItems + DiagnosticItems) from Invoices and Settlements")]
        public bool CheckRegression { get; set; }

        [Option("Recalculate", HelpText = "Recalculate everyting")]
        public bool Recalculate { get; set; }
    }
}
