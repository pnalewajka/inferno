﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.DiagnosticItems;

namespace Smt.CalculatorUtils.Helpers
{
    public static class ComparisonHelper
    {
        public static BoolResult CompareCostItems(ICollection<CostItem> original, ICollection<CostItem> current, bool compareDiagnosticItems = false)
        {
            var originalCostItems = original.GroupBy(c => c.Group).ToArray();
            var currentCostItems = current.GroupBy(c => c.Group).ToArray();

            var alerts = new List<AlertDto>();
            var success = true;

            foreach (var originalCostItemGroup in originalCostItems)
            {
                var currentCostItemGroup = currentCostItems.Where(p => p.Key == originalCostItemGroup.Key).SingleOrDefault();
                var oldTotal = Decimal.Round(originalCostItemGroup.Sum(c => c.Value), 4);

                if (currentCostItemGroup == null)
                {
                    if (oldTotal != 0M)
                    {
                        alerts.Add(AlertDto.CreateError($"Missing cost item `{originalCostItemGroup.Key}`"));
                        success = false;
                    }

                    continue;
                }

                if (originalCostItemGroup.Count() != currentCostItemGroup.Count())
                {
                    alerts.Add(AlertDto.CreateError($"Incorrect cost item count `{originalCostItemGroup.Key}`"));
                    success = false;
                    continue;
                }

                for (var i = 0; i < originalCostItemGroup.Count(); ++i)
                {
                    var originalCostItem = originalCostItemGroup.ElementAt(i);
                    var currentCostItem = currentCostItemGroup.ElementAt(i);

                    var originalValue = Decimal.Round(originalCostItem.Value, 4);
                    var currentValue = Decimal.Round(currentCostItem.Value, 4);

                    if (originalValue != currentValue)
                    {
                        alerts.Add(AlertDto.CreateError($"Incorrect cost item value `{originalCostItemGroup.Key}`: new:{currentValue} vs old:{originalValue}"));
                        success = false;
                        continue;
                    }
                }

                if (compareDiagnosticItems)
                {
                    var diagnosticResult = CompareDiagnosticItems(
                        originalCostItemGroup.SelectMany(g => g.DiagnosticItems).ToArray(),
                        currentCostItemGroup.SelectMany(g => g.DiagnosticItems).ToArray());

                    success &= diagnosticResult.IsSuccessful;
                    alerts.AddRange(diagnosticResult.Alerts);
                }
            }

            return new BoolResult(success && !alerts.Any(), alerts);
        }

        public static BoolResult CompareDiagnosticItems(ICollection<DiagnosticItem> original, ICollection<DiagnosticItem> current)
        {
            var originalByType = original.GroupBy(c => c.Type).ToArray();
            var currentByType = current.GroupBy(c => c.Type).ToArray();

            var alerts = new List<AlertDto>();
            var success = true;

            foreach (var originalByTypeGroup in originalByType)
            {
                var currentByTypeGroup = currentByType.Where(p => p.Key == originalByTypeGroup.Key).SingleOrDefault();

                if (currentByTypeGroup == null)
                {
                    alerts.Add(AlertDto.CreateError($"Missing diagnostic item `{originalByTypeGroup.Key}`"));
                    success = false;
                    continue;
                }

                if (originalByTypeGroup.Count() != currentByTypeGroup.Count())
                {
                    alerts.Add(AlertDto.CreateError($"Incorrect diagnostic item count `{originalByTypeGroup.Key}`"));
                    success = false;
                    continue;
                }

                for (var i = 0; i < originalByTypeGroup.Count(); ++i)
                {
                    var originalCostItem = originalByTypeGroup.ElementAt(i);
                    var currentCostItem = currentByTypeGroup.ElementAt(i);

                    if (!originalCostItem.Values.SequenceEqual(currentCostItem.Values))
                    {
                        alerts.Add(AlertDto.CreateError($"Incorrect diagnostic item values `{originalByTypeGroup.Key}`:"
                            + $"new:[{string.Join(", ", currentCostItem.Values)}] vs old:[{string.Join(", ", originalCostItem.Values)}]"));
                        success = false;
                        continue;
                    }
                }
            }

            return new BoolResult(success && !alerts.Any(), alerts);
        }
    }
}
