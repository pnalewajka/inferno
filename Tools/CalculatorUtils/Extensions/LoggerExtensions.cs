﻿using System;
using Castle.Core.Logging;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.CrossCutting.Common.Enums;
using Smt.CalculatorUtils.Interfaces;

namespace Smt.CalculatorUtils.Extensions
{
    public static class LoggerExtensions
    {
        public static void LogDisplay(
            this ILogger logger,
            ILoggerContext context,
            AlertDto alert)
        {
            var output = $"[{context}] {alert.Type}: {alert.Message}";

            Console.WriteLine(output);

            switch (alert.Type)
            {
                case AlertType.Information:
                    logger.Info(output);
                    break;

                default:
                    logger.Error(output);
                    break;
            }
        }
    }
}
