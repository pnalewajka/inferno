﻿namespace Smt.CalculatorUtils.Interfaces
{
    public interface ITask
    {
        void RunTask();
    }
}
