﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.Windsor;
using Smt.Atomic.CrossCutting.Common.Consts;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.CrossCutting.Common.ServicesConfiguration;
using Smt.Atomic.CrossCutting.Security.Interfaces;
using Smt.Atomic.CrossCutting.Settings.Interfaces;
using Smt.Atomic.JobScheduler;
using Smt.CalculatorUtils.Interfaces;

namespace Smt.CalculatorUtils
{
    public class Program
    {
        private readonly JobSchedulerContainerConfiguration _configuration = new JobSchedulerContainerConfiguration();
        private IWindsorContainer _container;

        private static void Main(string[] args)
        {
            new Program().Run(args);
        }

        private void Run(string[] args)
        {
            var options = ParseStartupOptions(args);

            if (options != null)
            {
                SetupContainer();
                SetupThreadPrincipal();

                ProcessStartingOptions(options, args);
            }

            Console.WriteLine("Done :)");
            Console.ReadLine();
        }

        private void SetupContainer()
        {
            _container = _configuration.Configure(ContainerType.JobScheduler);
            _container.Kernel.Resolver.AddSubResolver(new CollectionResolver(_container.Kernel));
        }

        private void SetupThreadPrincipal()
        {
            var principalBuilder = _container.Resolve<IPrincipalBuilder>();
            var settingsProvider = _container.Resolve<ISettingsProvider>();
            var userName = settingsProvider.JobSchedulerSettings.UserName;
            var principal = principalBuilder.BuildPrincipal(userName);

            Thread.CurrentPrincipal = principal;
        }

        private StartupOptions ParseStartupOptions(string[] args)
        {
            var result = CommandLine.Parser.Default.ParseArguments<StartupOptions>(args);

            if (result.Errors.Any())
            {
                Console.WriteLine(CommandLine.Text.HelpText.AutoBuild(result, null));

                return null;
            }

            return result.Value;
        }

        private void ProcessStartingOptions(StartupOptions options, string[] args)
        {
            ITask task = null;

            if (options.DetectLegacyDiagnosticItems)
            {
                task = ReflectionHelper.CreateInstanceWithIocDependencies<DetectLegacyDiagnosticItems>();
            }
            else if (options.CheckCostItemsRegression)
            {
                task = ReflectionHelper.CreateInstanceWithIocDependencies<CheckCostItemsRegression>();
            }
            else if (options.RefreshDiagnosticItems)
            {
                task = ReflectionHelper.CreateInstanceWithIocDependencies<RefreshDiagnosticItems>();
            }
            else if (options.CheckRegression)
            {
                task = ReflectionHelper.CreateInstanceWithIocDependencies<CheckRegression>();
            }
            else if (options.Recalculate)
            {
                task = ReflectionHelper.CreateInstanceWithIocDependencies<Recalculate>();
            }

            if (task == null)
            {
                throw new Exception("Command not found");
            }

            RunTask(task);
        }

        private void RunTask(ITask task)
        {
            var enCulture = new CultureInfo(CultureCodes.EnglishAmerican);

            using (var culture = CultureInfoHelper.SetCurrentCulture(enCulture))
            using (var uiCulture = CultureInfoHelper.SetCurrentUICulture(enCulture))
            {
                task.RunTask();
            }
        }
    }
}
