﻿using System;
using System.Linq;
using Castle.Core.Logging;
using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.CalculatorUtils.Interfaces;

namespace Smt.CalculatorUtils
{
    public class Recalculate : ITask
    {
        private readonly IUnitOfWorkService<ICompensationDbScope> _unitOfWorkService;
        private readonly ICalculatorResolver _calculatorResolver;
        private readonly ILogger _logger;

        public Recalculate(
            ILogger logger,
            IUnitOfWorkService<ICompensationDbScope> unitOfWorkService,
            ICalculatorResolver calculatorResolver)
        {
            _logger = logger;
            _unitOfWorkService = unitOfWorkService;
            _calculatorResolver = calculatorResolver;
        }

        public void RunTask()
        {
            using (var transaction = TransactionHelper.CreateDefaultTransactionScope())
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                RecalculateInvoices(unitOfWork);
                RecalculateSettlements(unitOfWork);

                transaction.Complete();
            }
        }

        private void RecalculateInvoices(IUnitOfWork<ICompensationDbScope> unitOfWork)
        {
            var invoices = unitOfWork.Repositories.InvoiceCalculations.ToArray();

            foreach (var invoice in invoices)
            {
                CalculationResult calculation;

                try
                {
                    calculation = _calculatorResolver
                        .GetCompensationCalculator(invoice.EmployeeId, invoice.Year, invoice.Month)
                        .Calculate();
                }
                catch (NotImplementedException)
                {
                    continue;
                }

                invoice.CalculationResult = JsonHelper.Serialize(calculation);
                unitOfWork.Commit();
            }
        }

        private void RecalculateSettlements(IUnitOfWork<ICompensationDbScope> unitOfWork)
        {
            var settlements = unitOfWork.Repositories.BusinessTripSettlementRequests.ToArray();

            foreach (var settlement in settlements)
            {
                CalculationResult calculation;

                try
                {
                    calculation = _calculatorResolver
                        .GetCalculatorBySettlementRequestId(settlement.Id)
                        .Calculate();
                }
                catch (NotImplementedException)
                {
                    continue;
                }

                settlement.CalculationResult = JsonHelper.Serialize(calculation);
                unitOfWork.Commit();
            }
        }
    }
}
