﻿using System;
using System.Linq;
using Castle.Core.Logging;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.CalculatorUtils.Extensions;
using Smt.CalculatorUtils.Helpers;
using Smt.CalculatorUtils.Interfaces;
using Smt.CalculatorUtils.LoggerContexts;

namespace Smt.CalculatorUtils
{
    public class CheckRegression : ITask
    {
        private readonly IUnitOfWorkService<ICompensationDbScope> _unitOfWorkService;
        private readonly ICalculatorResolver _calculatorResolver;
        private readonly ILogger _logger;

        public CheckRegression(
            ILogger logger,
            IUnitOfWorkService<ICompensationDbScope> unitOfWorkService,
            ICalculatorResolver calculatorResolver)
        {
            _logger = logger;
            _unitOfWorkService = unitOfWorkService;
            _calculatorResolver = calculatorResolver;
        }

        public void RunTask()
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                CheckInvoices(unitOfWork);
                CheckSettlements(unitOfWork);
            }
        }

        private void CheckInvoices(IUnitOfWork<ICompensationDbScope> unitOfWork)
        {
            var invoices = unitOfWork.Repositories.InvoiceCalculations.ToArray();

            foreach (var invoice in invoices)
            {
                var loggerContext = new InvoiceLoggerContext
                {
                    EmployeeId = invoice.EmployeeId,
                    Year = invoice.Year,
                    Month = invoice.Month,
                };

                var originalCalculation = JsonHelper.Deserialize<CalculationResult>(invoice.CalculationResult);
                CalculationResult currentCalculation;

                try
                {
                    currentCalculation = _calculatorResolver
                        .GetCompensationCalculator(invoice.EmployeeId, invoice.Year, invoice.Month)
                        .Calculate();
                }
                catch (NotImplementedException)
                {
                    continue;
                }
                catch (BusinessException be)
                {
                    _logger.LogDisplay(loggerContext, AlertDto.CreateError(be.Message));
                    continue;
                }

                PerformCheck(loggerContext, originalCalculation, currentCalculation);
            }
        }

        private void CheckSettlements(IUnitOfWork<ICompensationDbScope> unitOfWork)
        {
            var settlements = unitOfWork.Repositories.BusinessTripSettlementRequests.ToArray();

            foreach (var settlement in settlements)
            {
                var loggerContext = new SettlementLoggerContext
                {
                    EmployeeId = settlement.BusinessTripParticipant.EmployeeId,
                    BusinessTripId = settlement.BusinessTripParticipant.BusinessTripId,
                    SettlementRequestId = settlement.Id,
                };

                var originalCalculation = JsonHelper.Deserialize<CalculationResult>(settlement.CalculationResult);
                CalculationResult currentCalculation;

                try
                {
                    currentCalculation = _calculatorResolver
                        .GetCalculatorBySettlementRequestId(settlement.Id)
                        .Calculate();
                }
                catch (NotImplementedException)
                {
                    continue;
                }
                catch (BusinessException be)
                {
                    _logger.LogDisplay(loggerContext, AlertDto.CreateError(be.Message));
                    continue;
                }

                PerformCheck(loggerContext, originalCalculation, currentCalculation);
            }
        }

        private void PerformCheck(ILoggerContext loggerContext, CalculationResult original, CalculationResult current)
        {
            var diagnosticItemsResult = ComparisonHelper.CompareDiagnosticItems(original.DiagnosticItems, current.DiagnosticItems);

            foreach (var alert in diagnosticItemsResult.Alerts)
            {
                _logger.LogDisplay(loggerContext, alert);
            }

            var costItemResult = ComparisonHelper.CompareCostItems(original.CostItems, current.CostItems, true);

            foreach (var alert in costItemResult.Alerts)
            {
                _logger.LogDisplay(loggerContext, alert);
            }
        }
    }
}
