﻿using System;
using System.Linq;
using Castle.Core.Logging;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.DiagnosticItems;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.CalculatorUtils.Extensions;
using Smt.CalculatorUtils.Interfaces;
using Smt.CalculatorUtils.LoggerContexts;

namespace Smt.CalculatorUtils
{
    public class DetectLegacyDiagnosticItems : ITask
    {
        private readonly IUnitOfWorkService<ICompensationDbScope> _unitOfWorkService;
        private readonly ILogger _logger;

        public DetectLegacyDiagnosticItems(
            ILogger logger,
            IUnitOfWorkService<ICompensationDbScope> unitOfWorkService)
        {
            _logger = logger;
            _unitOfWorkService = unitOfWorkService;
        }

        public void RunTask()
        {
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                CheckInvoices(unitOfWork);
                CheckSettlements(unitOfWork);
            }
        }

        private void CheckInvoices(IUnitOfWork<ICompensationDbScope> unitOfWork)
        {
            var invoices = unitOfWork.Repositories.InvoiceCalculations.ToArray();

            foreach (var invoice in invoices)
            {
                var loggerContext = new InvoiceLoggerContext
                {
                    EmployeeId = invoice.EmployeeId,
                    Year = invoice.Year,
                    Month = invoice.Month,
                };

                var calculation = JsonHelper.Deserialize<CalculationResult>(invoice.CalculationResult);
                var legacyDiagnosticItems = calculation.DiagnosticItems.Concat(calculation.CostItems.SelectMany(i => i.DiagnosticItems))
                    .OfType<LegacyDiagnosticItem>()
                    .ToList();

                if (legacyDiagnosticItems.Any())
                {
                    var legacyValues = string.Join(Environment.NewLine, legacyDiagnosticItems.Select(i => string.Join(", ", i.LegacyValues)));

                    _logger.LogDisplay(loggerContext, AlertDto.CreateError(invoice.CalculationResult));
                    _logger.LogDisplay(loggerContext, AlertDto.CreateError(legacyValues));
                }
            }
        }

        private void CheckSettlements(IUnitOfWork<ICompensationDbScope> unitOfWork)
        {
            var settlements = unitOfWork.Repositories.BusinessTripSettlementRequests.ToArray();

            foreach (var settlement in settlements)
            {
                var loggerContext = new SettlementLoggerContext
                {
                    EmployeeId = settlement.BusinessTripParticipant.EmployeeId,
                    BusinessTripId = settlement.BusinessTripParticipant.BusinessTripId,
                    SettlementRequestId = settlement.Id,
                };

                var calculation = JsonHelper.Deserialize<CalculationResult>(settlement.CalculationResult);
                var legacyDiagnosticItems = calculation.DiagnosticItems.Concat(calculation.CostItems.SelectMany(i => i.DiagnosticItems))
                    .OfType<LegacyDiagnosticItem>()
                    .ToList();

                if (legacyDiagnosticItems.Any())
                {
                    var legacyValues = string.Join(Environment.NewLine, legacyDiagnosticItems.Select(i => string.Join(", ", i.LegacyValues)));

                    _logger.LogDisplay(loggerContext, AlertDto.CreateError(settlement.CalculationResult));
                    _logger.LogDisplay(loggerContext, AlertDto.CreateError(legacyValues));
                }
            }
        }
    }
}
