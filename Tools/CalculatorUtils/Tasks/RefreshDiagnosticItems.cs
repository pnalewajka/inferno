﻿using System;
using System.Linq;
using Castle.Core.Logging;
using Smt.Atomic.Business.Common.Dto;
using Smt.Atomic.Business.Compensation.CalculationResults;
using Smt.Atomic.Business.Compensation.Interfaces;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Smt.Atomic.Data.Entities.Helpers;
using Smt.Atomic.Data.Repositories.Interfaces;
using Smt.Atomic.Data.Repositories.Scopes;
using Smt.CalculatorUtils.Extensions;
using Smt.CalculatorUtils.Helpers;
using Smt.CalculatorUtils.Interfaces;
using Smt.CalculatorUtils.LoggerContexts;

namespace Smt.CalculatorUtils
{
    public class RefreshDiagnosticItems : ITask
    {
        private readonly IUnitOfWorkService<ICompensationDbScope> _unitOfWorkService;
        private readonly ICalculatorResolver _calculatorResolver;
        private readonly ILogger _logger;

        public RefreshDiagnosticItems(
            ILogger logger,
            IUnitOfWorkService<ICompensationDbScope> unitOfWorkService,
            ICalculatorResolver calculatorResolver)
        {
            _logger = logger;
            _unitOfWorkService = unitOfWorkService;
            _calculatorResolver = calculatorResolver;
        }

        public void RunTask()
        {
            using (var transaction = TransactionHelper.CreateDefaultTransactionScope())
            using (var unitOfWork = _unitOfWorkService.Create())
            {
                RefreshInvoices(unitOfWork);
                RefreshSettlements(unitOfWork);

                transaction.Complete();
            }
        }

        private void RefreshInvoices(IUnitOfWork<ICompensationDbScope> unitOfWork)
        {
            var invoices = unitOfWork.Repositories.InvoiceCalculations.ToArray();

            foreach (var invoice in invoices)
            {
                var loggerContext = new InvoiceLoggerContext
                {
                    EmployeeId = invoice.EmployeeId,
                    Year = invoice.Year,
                    Month = invoice.Month,
                };

                try
                {
                    var originalCalculation = JsonHelper.Deserialize<CalculationResult>(invoice.CalculationResult);
                    var currentCalculation = _calculatorResolver.GetCompensationCalculator(invoice.EmployeeId, invoice.Year, invoice.Month).Calculate();
                    var comparisonResult = ComparisonHelper.CompareCostItems(originalCalculation.CostItems, currentCalculation.CostItems);

                    // everything is fine, refresh
                    if (comparisonResult.IsSuccessful)
                    {
                        invoice.CalculationResult = JsonHelper.Serialize(currentCalculation);
                        unitOfWork.Commit();
                    }
                    else
                    {
                        _logger.LogDisplay(loggerContext, AlertDto.CreateError("skipping"));
                    }
                }
                catch (NotImplementedException)
                {
                    continue;
                }
                catch (BusinessException be)
                {
                    _logger.LogDisplay(loggerContext, AlertDto.CreateError(be.Message));
                    continue;
                }
            }
        }

        private void RefreshSettlements(IUnitOfWork<ICompensationDbScope> unitOfWork)
        {
            var settlements = unitOfWork.Repositories.BusinessTripSettlementRequests.ToArray();

            foreach (var settlement in settlements)
            {
                var loggerContext = new SettlementLoggerContext
                {
                    EmployeeId = settlement.BusinessTripParticipant.EmployeeId,
                    BusinessTripId = settlement.BusinessTripParticipant.BusinessTripId,
                    SettlementRequestId = settlement.Id,
                };

                try
                {
                    var originalCalculation = JsonHelper.Deserialize<CalculationResult>(settlement.CalculationResult);
                    var currentCalculation = _calculatorResolver.GetCalculatorBySettlementRequestId(settlement.Id).Calculate();
                    var comparisonResult = ComparisonHelper.CompareCostItems(originalCalculation.CostItems, currentCalculation.CostItems);

                    // everything is fine, refresh
                    if (comparisonResult.IsSuccessful)
                    {
                        settlement.CalculationResult = JsonHelper.Serialize(currentCalculation);
                        unitOfWork.Commit();
                    }
                    else
                    {
                        _logger.LogDisplay(loggerContext, AlertDto.CreateError("skipping"));
                    }
                }
                catch (NotImplementedException)
                {
                    continue;
                }
                catch (BusinessException be)
                {
                    _logger.LogDisplay(loggerContext, AlertDto.CreateError(be.Message));
                    continue;
                }
            }
        }
    }
}
