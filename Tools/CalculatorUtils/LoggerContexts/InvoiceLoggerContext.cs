﻿using Smt.CalculatorUtils.Interfaces;

namespace Smt.CalculatorUtils.LoggerContexts
{
    public struct InvoiceLoggerContext : ILoggerContext
    {
        public long EmployeeId { get; set; }

        public int Year { get; set; }

        public byte Month { get; set; }

        public override string ToString()
        {
            return $"Employee: {EmployeeId}, Year: {Year}, Month: {Month}";
        }
    }
}
