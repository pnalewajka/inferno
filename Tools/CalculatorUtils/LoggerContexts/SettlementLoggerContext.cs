﻿using Smt.CalculatorUtils.Interfaces;

namespace Smt.CalculatorUtils.LoggerContexts
{
    public struct SettlementLoggerContext : ILoggerContext
    {
        public long EmployeeId { get; set; }

        public long BusinessTripId { get; set; }

        public long SettlementRequestId { get; set; }

        public override string ToString()
        {
            return $"Employee: {EmployeeId}, BT: {BusinessTripId}, SettlementRequest: {SettlementRequestId}";
        }
    }
}
