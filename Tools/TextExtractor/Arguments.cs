﻿using CommandLine;

namespace TextExtractor
{
    public class Arguments
    {
        [Option('f', HelpText = "Tries to extract text from file with provided FILENAME.", MetaValue = "FILENAME")]
        public string Filename { get; set; }

        [Option('c', HelpText = "Tries to extract text from Base64-encoded data of EXTENSION type read from standard input.", MetaValue = "EXTENSION")]
        public string Extension { get; set; }
    }
}
