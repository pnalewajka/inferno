﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CommandLine;
using IFilterTextReader;

namespace TextExtractor
{
    internal class Program
    {
        internal static int Main(string[] args)
        {
            var arguments = Parser.Default.ParseArguments<Arguments>(args);

            if (arguments.Errors.Any())
            {
                return -1;
            }

            ProcessInput(arguments.Value);

            return 0;
        }

        private static void ProcessInput(Arguments arguments)
        {
            var job = new Job();

            job.AddProcess(Process.GetCurrentProcess().Handle);

            if (!string.IsNullOrEmpty(arguments.Filename))
            {
                ExtractFromFile(arguments.Filename);
            }
            else if (!string.IsNullOrEmpty(arguments.Extension))
            {
                ExtractFromBase64(arguments.Extension);
            }
        }

        private static void ExtractFromFile(string filename)
        {
            using (var filterReader = new FilterReader(filename))
            {
                Console.Write(filterReader.ReadToEnd());
            }
        }

        private static void ExtractFromBase64(string extension)
        {
            using (var inputStream = Console.OpenStandardInput())
            using (var inputReader = new StreamReader(inputStream))
            {
                var data = Convert.FromBase64String(inputReader.ReadToEnd());

                using (var memoryStream = new MemoryStream(data))
                using (var filterReader = new FilterReader(memoryStream, extension))
                {
                    Console.Write(filterReader.ReadToEnd());
                }
            }
        }
    }
}
