﻿using Microsoft.CodeAnalysis;
using PowerShellAutomation.Helpers;
using System.Linq;

namespace PowerShellAutomation.Extensions
{
    public static class SyntaxNodeExtension
    {
        public static TSyntax WithLeadingTabTrivia<TSyntax>(this TSyntax node, int count = 1) where TSyntax : SyntaxNode
        {
            var trivias = Enumerable.Repeat(TriviaHelper.TabTrivia, count);
             
            return node.WithLeadingTrivia(trivias);
        }

        public static TSyntax WithLeadingWhiteSpaceTrivia<TSyntax>(this TSyntax node, int count = 1) where TSyntax : SyntaxNode
        {
            var trivias = Enumerable.Repeat(TriviaHelper.WhitespaceTrivia, count);

            return node.WithLeadingTrivia(trivias);
        }

        public static TSyntax WithLeadingNewLineTrivia<TSyntax>(this TSyntax node, int count = 1) where TSyntax : SyntaxNode
        {
            var trivias = Enumerable.Repeat(TriviaHelper.NewLineTrivia, count);

            return node.WithLeadingTrivia(trivias);
        }

        public static TSyntax WithTrailingTabTrivia<TSyntax>(this TSyntax node, int count = 1) where TSyntax : SyntaxNode
        {
            var trivias = Enumerable.Repeat(TriviaHelper.TabTrivia, count);

            return node.WithTrailingTrivia(trivias);
        }

        public static TSyntax WithTrailingWhiteSpaceTrivia<TSyntax>(this TSyntax node, int count = 1) where TSyntax : SyntaxNode
        {
            var trivias = Enumerable.Repeat(TriviaHelper.WhitespaceTrivia, count);

            return node.WithTrailingTrivia(trivias);
        }

        public static TSyntax WithTrailingNewLineTrivia<TSyntax>(this TSyntax node, int count = 1) where TSyntax : SyntaxNode
        {
            var trivias = Enumerable.Repeat(TriviaHelper.NewLineTrivia, count);

            return node.WithTrailingTrivia(trivias);
        }
    }
}
