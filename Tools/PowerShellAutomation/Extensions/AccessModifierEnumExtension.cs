﻿using Microsoft.CodeAnalysis.CSharp;
using PowerShellAutomation.Enums;
using System;

namespace PowerShellAutomation.Extensions
{
    public static class AccessModifierEnumExtension
    {
        public static SyntaxKind ToSyntaxKindModifier(this AccessModifierEnum value)
        {
            switch(value)
            {
                case AccessModifierEnum.Private:
                    return SyntaxKind.PrivateKeyword;
                case AccessModifierEnum.Public:
                    return SyntaxKind.PublicKeyword;
                default:
                    throw new Exception("Only private and public access modifier can be converted into SyntaxKind type");
            }
        }
    }
}
