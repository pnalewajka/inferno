﻿using Microsoft.CodeAnalysis;
using PowerShellAutomation.Helpers;
using System.Linq;

namespace PowerShellAutomation.Extensions
{
    public static class SyntaxTokenExtension
    {
        public static SyntaxToken WithLeadingTabTrivia(this SyntaxToken token, int count = 1)
        {
            var trivias = Enumerable.Repeat(TriviaHelper.TabTrivia, count);

            return token.WithLeadingTrivia(trivias);
        }

        public static SyntaxToken WithLeadingWhiteSpaceTrivia(this SyntaxToken token, int count = 1)
        {
            var trivias = Enumerable.Repeat(TriviaHelper.WhitespaceTrivia, count);

            return token.WithLeadingTrivia(trivias);
        }

        public static SyntaxToken WithLeadingNewLineTrivia(this SyntaxToken token, int count = 1)
        {
            var trivias = Enumerable.Repeat(TriviaHelper.NewLineTrivia, count);

            return token.WithLeadingTrivia(trivias);
        }

        public static SyntaxToken WithTrailingTabTrivia(this SyntaxToken token, int count = 1)
        {
            var trivias = Enumerable.Repeat(TriviaHelper.TabTrivia, count);

            return token.WithTrailingTrivia(trivias);
        }

        public static SyntaxToken WithTrailingWhiteSpaceTrivia(this SyntaxToken token, int count = 1)
        {
            var trivias = Enumerable.Repeat(TriviaHelper.WhitespaceTrivia, count);

            return token.WithTrailingTrivia(trivias);
        }

        public static SyntaxToken WithTrailingNewLineTrivia(this SyntaxToken token, int count = 1)
        {
            var trivias = Enumerable.Repeat(TriviaHelper.NewLineTrivia, count);

            return token.WithTrailingTrivia(trivias);
        }
    }
}
