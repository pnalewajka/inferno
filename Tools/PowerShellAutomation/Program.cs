﻿using System;
using System.IO;
using PowerShellAutomation.Commands;
using PowerShellAutomation.Helpers;

namespace PowerShellAutomation
{
    class Program
    {
        public static void Main(string[] args)
        {
            string commandText = GetCommandText(args);
            var command = CommandSerializationHelper.DeserializeFromXml<Command>(commandText);
            var result = command.Execute();
            Console.WriteLine(CommandSerializationHelper.SerializeToXml<Output>(result));
        }

        private static string GetCommandText(string[] args)
        {
            if (args == null || args.Length < 2)
            {
                return ConsoleHelper.GetWholeInput();
            }
            
            //Support for debugging, pass file name as first argument
            return File.ReadAllText(args[1]);
        }
    }
}
