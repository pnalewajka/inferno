﻿function Invoke-SqlUpdateIndexes
{
<#
.SYNOPSIS
Rebuild indexes and statistics on targer db, server

.DESCRIPTION
Rebuild indexes and statistics on targer db, server

.PARAMETER query
Sql query 

.PARAMETER database
Database to run script against, default to 'Atomic'

.PARAMETER server
Server to run script against, default to 'localhost'


.Example
Invoke-SqlUpdateIndexes

.LINK
#>
	[CmdletBinding(SupportsShouldProcess=$true)]
	param(
		[Parameter(Position=1,Mandatory=1)]
		[string]$query,
		[Parameter(Position=2,Mandatory=0)]
		[string]$database = "Atomic",
		[Parameter(Position=3,Mandatory=0)]
		[string]$server = "localhost"
	)	
	$query = "
SET NOCOUNT ON
DECLARE @table NVARCHAR(200), @index NVARCHAR(200), @stat NVARCHAR(100)
DECLARE CUR_IDX CURSOR FOR
	SELECT t.name, si.name
	FROM 
		sys.indexes si 
		JOIN sys.tables t ON si.object_id = t.object_id AND t.type = 'U' AND si.type = 2
		CROSS APPLY sys.dm_db_index_physical_stats(DB_ID('Camelot'), t.object_id, si.index_id, NULL, 'sampled') st
	WHERE
		st.avg_fragmentation_in_percent >= 30.0

DECLARE @sql NVARCHAR(MAX)
OPEN CUR_IDX
FETCH NEXT FROM CUR_IDX INTO @table, @index
WHILE @@FETCH_STATUS=0
	BEGIN
		SET @sql = 'ALTER INDEX '+@index +' ON '+@table + ' REBUILD WITH (FILLFACTOR = 80, SORT_IN_TEMPDB = ON, STATISTICS_NORECOMPUTE = ON);'
		EXEC sys.sp_executesql @sql
		PRINT @table + ' '+@index
		FETCH NEXT FROM CUR_IDX INTO @table, @index
	END
CLOSE CUR_IDX
DEALLOCATE CUR_IDX

DECLARE CUR_STA CURSOR FOR
	SELECT 
		OBJECT_NAME(st.object_id) AS TableName,
		st.name AS StatName
	FROM
		sys.stats st
	JOIN
		sys.tables t ON st.object_id = t.object_id AND t.type = 'U' AND st.auto_created = 1 AND t.schema_id=SCHEMA_ID('dbo')
		OUTER APPLY sys.dm_db_stats_properties(t.object_id, st.stats_id) props
	WHERE 
		props.rows > props.rows_sampled 

OPEN CUR_STA
FETCH NEXT FROM CUR_STA INTO @table, @stat
WHILE @@FETCH_STATUS=0
	BEGIN
		SET @sql = 'UPDATE STATISTICS '+@table +' ' + @stat
		EXEC sys.sp_executesql @sql
		PRINT @table + ' '+@stat
		FETCH NEXT FROM CUR_STA INTO @table, @stat
	END
CLOSE CUR_STA
DEALLOCATE CUR_STA
	"
	Invoke-SqlExecuteNonQuery $query $database $server
}

function Disable-AtomicAudit
{
<#
.SYNOPSIS
Disable audit on atomic database

.DESCRIPTION
Disable audit on atomic database, by disabling global triggers, and removing table triggers.

.Example
Disable-AtomicAudit
Disable-AtomicAudit -Clean

.PARAMETER Clean
If set then remove log tables as well

.LINK
#>
	[CmdletBinding(SupportsShouldProcess=$true)]
	param(
		[Parameter(Position=1,Mandatory=0)]
		[switch]$Clean =  $false
	)

	$sql = "EXEC [Audit].[atomic_DisableAudit] "

	if($Clean -eq $true)
	{
		$sql = $sql + ' 1'
	}

	Write-Verbose $sql
	Invoke-SqlExecuteNonQuery $sql  -useEF
	Write-Host "Audit disabled"
}

function Enable-AtomicAudit
{
<#
.SYNOPSIS
Enable audit on atomic database

.DESCRIPTION
Enable audit on atomic database, enable global triggers, rebuild log tables, attaches new table triggers

.Example
Enable-AtomicAudit

.LINK
#>
	[CmdletBinding(SupportsShouldProcess=$true)]
	param(
	)

	$sql = "EXEC [Audit].[atomic_EnableAudit] "

	Write-Verbose $sql
	Invoke-SqlExecuteNonQuery $sql  -useEF
	Write-Host "Audit enabled"
}
