﻿function Search-SqlJob
{	
<#
.SYNOPSIS
Search for specific job in database


.DESCRIPTION
Search for specific job in database

.PARAMETER jobname
Part of the job name

.Example
Search-SqlJob data

.LINK
#>
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[Parameter(Position=0,Mandatory=0)]
		[string]$jobname
	)
	$array = Get-SqlExecuteReader "SELECT Name FROM [Scheduling].[JobDefinition] WHERE Name LIKE '%$jobname%' ORDER BY NAME" -useEF
	return $array
}

function Enable-SqlRunNowJob
{
<#
.SYNOPSIS
Set run now flag of specific job to true


.DESCRIPTION
Set run now flag of specific job to true

.PARAMETER fullJobName
Full job name. Can be entered using tab extensions

.Example
Enable-SqlRunNowJob <job name>

.LINK
#>
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[Parameter(Position=0,Mandatory=1)]
		[string]$fullJobName
	)
	Invoke-SqlExecuteNonQuery "UPDATE [Scheduling].[JobDefinition] SET [ShouldRunNow] = 1 WHERE Name = '$fullJobName'" -useEF
	Get-SqlExecuteReader "SELECT Name, [IsEnabled], [ShouldRunNow] FROM [Scheduling].[JobDefinition] WHERE Name = '$fullJobName'" -useEF
	Write-Host "$fullJobName set to [ShouldRunNow]"
}

function Show-SqlRunningJobs
{
<#
.SYNOPSIS
Show all jobs with runnow flag set to true


.DESCRIPTION
Show all jobs with runnow flag set to true

.Example
Show-SqlRunningJobs

.LINK
#>
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
	)
	Get-SqlExecuteReader "SELECT Name, [IsEnabled], [ShouldRunNow] FROM [Scheduling].[JobDefinition]  WHERE [ShouldRunNow] = 1" -useEF
}

function Disable-SqlAllJobs
{
<#
.SYNOPSIS
Set enabled flag on all jobs to false

.DESCRIPTION
Set enabled flag on all jobs to false

.Example
Sql-DisableAllJobs

.LINK
#>
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
	)
	Invoke-SqlExecuteNonQuery "UPDATE [Scheduling].[JobDefinition] SET [IsEnabled] = 0, [ShouldRunNow] = 0 " -useEF
	Write-Host "All jobs disabled"
}

#Export-ModuleMember -function Sql-JobSearch, Sql-RunNowJob, Sql-DisableAllJobs, Sql-ShowRunningJobs