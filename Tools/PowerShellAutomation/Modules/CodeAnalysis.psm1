﻿<#
.SYNOPSIS
	Add new entity into Entities (Data) project

.DESCRIPTION
	Add new entity into Entities (Data) project

.PARAMETER Module
	Entity module name

.PARAMETER Name
	New entity name
#>
function Add-Entity
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[Parameter(Position=1,Mandatory=1)]
		[string]$Module,
		[Parameter(Position=0,Mandatory=1)]
		[string]$Name,
		[Parameter(Position=2,Mandatory=0)]
		[string]$BaseClassName

	)
	$entityClassPath = ( Get-Project -Name 'Entities (Data)' ).FullName | Split-Path | Join-Path -ChildPath ("\Modules\$Module\$Name.cs")
	if( (Test-Path -Path $entityClassPath) -eq $True)
	{
		Write-Warning "Entity $Name located in area $Module already exists"
		return $false
	}
	if( (Test-Path -Path (Split-Path $entityClassPath)) -ne $True)
	{
		New-Item -Path (Split-Path $entityClassPath) -ItemType Directory
	}

	$coreEntitiesContextClassPath = ( Get-Project -Name 'Entities (Data)' ).FullName | Split-Path | Join-Path -ChildPath ("\CoreEntitiesContext.cs")
	if( (Test-Path -Path $coreEntitiesContextClassPath) -eq $False)
	{
		Write-Warning "CoreEntitiesContext class does not exist in 'Entities (Data)' project."
		return $false
	}

	Write-Host "Scaffolding Entity $Name in area $Module"

	#gen. code
	$xmlCreateEntityCommand = [xml] (Get-CreateEntityCommand $Module $Name $BaseClassName)
	Write-Verbose 'CreateEntityCommand xml'
	Write-Verbose $xmlCreateEntityCommand.OuterXml
	$createEntityOutput = Invoke-PowerShellAutomationCommand $xmlCreateEntityCommand
	Write-Verbose 'CreateEntityOutput xml'
	Write-Verbose $createEntityOutput.OuterXml

	Write-Host "Injecting DbSet<$Name> property into CoreEntitiesContext"

	$coreEntitiesContexContent = (Get-Content -Path $coreEntitiesContextClassPath | Out-String ) 
	$xmlInjectDbSetPropertyIntoCoreEntitiesContextCommand = [xml] (Get-InjectDbSetPropertyIntoCoreEntitiesContextCommand -ClassCode $coreEntitiesContexContent -Area $Module -EntityName $Name )
	Write-Verbose 'InjectDbSetPropertyIntoCoreEntitiesContextCommand xml'
	Write-Verbose $xmlInjectDbSetPropertyIntoCoreEntitiesContextCommand.OuterXml
	$injectDbSetPropertyIntoCoreEntitiesContextOutput = Invoke-PowerShellAutomationCommand $xmlInjectDbSetPropertyIntoCoreEntitiesContextCommand
	Write-Verbose 'InjectDbSetPropertyIntoCoreEntitiesContextOutput xml'
	Write-Verbose $injectDbSetPropertyIntoCoreEntitiesContextOutput.OuterXml

	$scopeClassPath = ( Get-Project -Name 'Repositories (Data)' ).FullName | Split-Path | Join-Path -ChildPath ("\Scopes\I$($Module)DbScope.cs")
	if( (Test-Path -Path $scopeClassPath) -eq $False)
	{
		Write-Host "Scaffolding I$($Module)DbScope"

		$xmlCreateScopeCommand = [xml] (Get-CreateScopeCommand -Area $Module )
		Write-Verbose 'CreateScopeCommand xml'
		Write-Verbose $xmlCreateScopeCommand.OuterXml
		$createScopeOutput = Invoke-PowerShellAutomationCommand $xmlCreateScopeCommand
		Write-Verbose 'CreateScopeOutput xml'
		Write-Verbose $createScopeOutput.OuterXml

		$scopeClassCode = $createScopeOutput.Output.ScopeClassCode
	}
	else
	{
		$scopeClassCode = (Get-Content -Path $scopeClassPath | Out-String )
	}

	Write-Host "Injecting I$($Name)Repository into I$($Module)DbScope" 

	$xmlInjectRepositoryIntoScopeCommand = [xml] (Get-InjectRepositoryIntoScopeCommand -ClassCode $scopeClassCode -Area $Module -EntityName $Name )
	Write-Verbose 'InjectRepositoryIntoScopeCommand xml'
	Write-Verbose $xmlInjectRepositoryIntoScopeCommand.OuterXml
	$injectRepositoryIntoScopeOutput = Invoke-PowerShellAutomationCommand $xmlInjectRepositoryIntoScopeCommand
	Write-Verbose 'InjectRepositoryIntoScopeOutput xml'
	Write-Verbose $injectRepositoryIntoScopeOutput.OuterXml

	if($PSCmdlet.MyInvocation.BoundParameters['Verbose'].IsPresent)
	{
		return $true
	}
	try
	{
		Write-Host "Creating file $entityClassPath"

		$files = @()
		$items = @()

		$null = $createEntityOutput.Output.EntityClassCode | Convert-UnicodeLineEndings | Out-File -FilePath $entityClassPath -Encoding UTF8
		$files += $entityClassPath
		$items += (Get-Project -Name 'Entities (Data)').ProjectItems.AddFromFile($entityClassPath)
		$null = Start-OpenAndCleanup (Get-Project -Name 'Entities (Data)') $entityClassPath

		Write-Host "Saving changes in file $coreEntitiesContextClassPath"

		$projectItem = Start-OpenFile (Get-Project -Name 'Entities (Data)') $coreEntitiesContextClassPath
		$null = $projectItem.Document.Close()

		$null = $injectDbSetPropertyIntoCoreEntitiesContextOutput.Output.ClassCode | Convert-UnicodeLineEndings | Out-File -FilePath $coreEntitiesContextClassPath -Encoding UTF8
		$null = Start-OpenAndCleanup (Get-Project -Name 'Entities (Data)') $coreEntitiesContextClassPath

		if($xmlCreateScopeCommand)
		{
			Write-Host "Creating file $scopeClassPath"

			$null = $createScopeOutput.Output.ScopeClassCode | Convert-UnicodeLineEndings | Out-File -FilePath $scopeClassPath -Encoding UTF8
			$files += $scopeClassPath
			$items += (Get-Project -Name 'Repositories (Data)').ProjectItems.AddFromFile($scopeClassPath)
		}

		Write-Host "Saving changes in file $scopeClassPath"

		$projectItem = Start-OpenFile (Get-Project -Name 'Entities (Data)') $scopeClassPath
		$null = $projectItem.Document.Close()
		
		$null = $injectRepositoryIntoScopeOutput.Output.ClassCode | Convert-UnicodeLineEndings | Out-File -FilePath $scopeClassPath -Encoding UTF8
		$null = Start-OpenAndCleanup (Get-Project -Name 'Repositories (Data)') $scopeClassPath
	}
	catch
	{
		Write-Error $_
		Write-Host 'Revoking changes'

		$null = $files | ? { Test-Path $_ } | % { Remove-Item $_ }
		$null = $items | ? { $_ -ne $null } | % { $_.Remove() }

		Write-Host 'Done'
		return $false
	}

	return $true
}

<#
.SYNOPSIS
	Add new dto based on specified entity 

.DESCRIPTION
	Add new dto based on specified entity 

.PARAMETER Name
	Entity name

.PARAMETER ModuleName
	Optional. Business project name. If not exists, project will be created
#>
function Add-DtoFromEntity
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[Parameter(Position=0,Mandatory=1)]
		[string]$Name,
		[Parameter(Position=1,Mandatory=0)]
		[string]$ModuleName
	)
	$project = Get-Project -Name 'Entities (Data)'
	$module = (Get-ChildItem -Path  ($project.FullName | Split-Path | Join-Path -ChildPath Modules ) -Recurse -Include '*.cs' | Where-Object {$_.Name -eq "$($Name).cs" }).FullName | Split-Path | Split-Path -Leaf
	if($PSBoundParameters.ContainsKey('ModuleName'))
	{
		$module = $ModuleName
	}
	$assemblyPath = Get-MainAssemblyOfProjectPath 'Entities (Data)'
	$entityClassPath = $project.FullName | Split-Path | Join-Path -ChildPath ("\Modules\$module\$Name.cs")

	if($module.Count -gt 1)
	{
		$moduleStr = $module -join ","
		Write-Warning "Entity found in multiple modules ($moduleStr), please provide explicit module name. Command aborted"
		return $false
	}
	if( (Test-Path -Path $entityClassPath) -ne $True)
	{
		Write-Warning "Entity $Name doesn't exist"
		return $false
	}
	$targetProject = (Get-Project -Name "*Business\$module")
	if($targetProject -eq $null)
	{
		$targetProject = (Get-Project -Name "*Business\$module (Business)")
	}
	if(($targetProject -eq $null) -and $ModuleName)
	{
		Add-Project -Location Business -ProjectName "$module (Business)"
		$targetProject = (Get-Project -Name "*Business\$module (Business)")
	}
	if ($targetProject -eq $null)
	{
		Write-Warning "Project for DTO $module doesn't exist"
		return $false
	}
	if(($targetProject.ProjectItems | ? { $_.Name -eq "Dto" }) -eq $null)
	{
		$targetProject.ProjectItems.AddFolder("Dto")
	}
	$dtoClassName = "$($Name)Dto"
	$toDtoClassName = "$($Name)To$($dtoClassName)Mapping"
	$fromDtoClassName = "$($dtoClassName)To$($Name)Mapping"
	$dtoFolderPath  = $targetProject.FullName | Split-Path | Join-Path -ChildPath Dto
	Write-Host "Scaffolding Dto $dtoClassName in module $module"
	Write-Host "Scaffolding Dto mapping $toDtoClassName in module $module"
	Write-Host "Scaffolding Dto mapping $fromDtoClassName in module $module"
	$targetNameSpace = $targetProject.Properties.Item('DefaultNamespace').Value + ".Dto"
	$fullEntityClassName = $project.Properties.Item('DefaultNamespace').Value + ".Modules.$module.$Name"
	$xmlCommand = [xml] (Get-CreateDtoFromEntityCommand $module $fullEntityClassName $assemblyPath $targetNameSpace $dtoClassName $toDtoClassName $fromDtoClassName)
	Write-Verbose 'Command xml'
	Write-Verbose $xmlCommand.OuterXml
	$output = Invoke-PowerShellAutomationCommand $xmlCommand
	Write-Verbose 'Output xml'
	Write-Verbose $output.OuterXml
	if($PSCmdlet.MyInvocation.BoundParameters['Verbose'].IsPresent)
	{
		return $true
	}
	$dtoClassPath = $dtoFolderPath | Join-Path -ChildPath ("$($dtoClassName).cs")
	$toDtoClassPath = $dtoFolderPath | Join-Path -ChildPath ("$($toDtoClassName).cs")
	$fromDtoClassPath = $dtoFolderPath | Join-Path -ChildPath ("$($fromDtoClassName).cs")
	if((Test-Path -Path $dtoClassPath) -ne $false)
	{
		Write-Warning "File $dtoClassPath already exists"
		return $false
	}
	if((Test-Path -Path $toDtoClassPath) -ne $false)
	{
		Write-Warning "File $toDtoClassPath already exists"
		return $false
	}
	if((Test-Path -Path $fromDtoClassPath) -ne $false)
	{
		Write-Warning "File $fromDtoClassPath already exists"
		return $false
	}

	try
	{
		$files = @()
		$items = @()

		$null = $output.Output.DtoCode | Convert-UnicodeLineEndings | Out-File -FilePath $dtoClassPath -Encoding UTF8
		$files += $dtoClassPath
		$dtoClassItem = $targetProject.ProjectItems.AddFromFile($dtoClassPath)
		$items += $dtoClassItem

		$null = $output.Output.ToDtoMappingCode | Convert-UnicodeLineEndings | Out-File -FilePath $toDtoClassPath -Encoding UTF8
		$files += $toDtoClassPath
		$null = $output.Output.FromDtoMappingCode | Convert-UnicodeLineEndings | Out-File -FilePath $fromDtoClassPath -Encoding UTF8
		$files += $fromDtoClassPath

		$items += $dtoClassItem.ProjectItems.AddFromFile($toDtoClassPath)

		$items += $dtoClassItem.ProjectItems.AddFromFile($fromDtoClassPath)

		$null = Start-OpenAndCleanup $targetProject $dtoClassPath
		$null = Start-OpenAndCleanup $targetProject $toDtoClassPath
		$null = Start-OpenAndCleanup $targetProject $fromDtoClassPath
	}
	catch
	{
		Write-Error $_
		Write-Host 'Revoking changes'

		$null = $files | ? { Test-Path $_ } | % { Remove-Item $_ }
		$null = $items | ? { $_ -ne $null } | % { $_.Remove() }

		Write-Host 'Done'
		return $false
	}

	return $true
}

<#
.SYNOPSIS
	Add new ViewModel based on specified dto 

.DESCRIPTION
	Add new ViewModel based on specified dto 

.PARAMETER Name
	Dto name

.PARAMETER IsForCardIndex
    Use to generate Visibility attributes for displaying in card index view
#>
function Add-ViewModelFromDto
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[Parameter(Position=0,Mandatory=1)]
		[string]$Name,
		[Parameter(Position=1,Mandatory=0)]
		[string]$ResourceName,
		[switch]$IsForCardIndex
	)
	$commonProject = Get-Project -Name "Common (Business)"
	$classFile = Get-ChildItem -Path ($commonProject.FullName | Split-Path | Split-Path) | Where-Object {(Split-Path -Leaf $_) -ne 'Common (Business)' } | % { Get-ChildItem -Path ( Join-Path $_.FullName -ChildPath Dto) -Filter *Dto.cs} | % { if($_.Name.Replace('.cs','') -eq $Name) { $_.FullName  } }
	if( ($classFile -eq $null) -or (-not (Test-Path $classFile)) )
	{
		Write-Host "DTO $Name doesn't exist"
		return $false
	}
	$Module = $classFile | Split-Path | Split-Path | Split-Path -Leaf
	$dtoProjectName = "*Business\$Module"
	$project = Get-Project -Name $dtoProjectName
	if($project -eq $null)
	{
		$dtoProjectName = "*Business\$Module (Business)"
		$project = Get-Project -Name $dtoProjectName
	}
	$assemblyPath = Get-MainAssemblyOfProjectPath $dtoProjectName
	Write-Verbose ("Assembly Path: $assemblyPath" )
	$targetProject = (Get-Project -Name WebApp)
	if( $targetProject -eq $null)
	{
		Write-Host "Project for ViewModel $Module doesn't exist"
		return $false
	}

	$areaItem = $targetProject.ProjectItems.Item("Areas")
	if(($areaItem.ProjectItems | ? { $_.Name -eq $Module }) -eq $null)
	{
		$areaItem.ProjectItems.AddFolder($Module)
		$moduleItem = $areaItem.ProjectItems.Item($Module)

		$moduleItem.ProjectItems.AddFolder("Views")

		$areaRegistrationClassPath = $targetProject.FullName | Split-Path | Join-Path -ChildPath "Areas\$Module\$($Module)AreaRegistration.cs"

		Write-Host "Scaffolding AreaRegistration in area $Module of WebApp project"

		$createAreaCommand = [xml] (Get-CreateAreaRegistrationCommand $Module)
		Write-Verbose 'CreateAreaCommand xml'
		Write-Verbose $createAreaCommand.OuterXml
		$createAreaOutput = Invoke-PowerShellAutomationCommand $createAreaCommand
		Write-Verbose 'CreateAreaOutput xml'
		Write-Verbose $createAreaOutput.OuterXml
	}

	if($PSBoundParameters.ContainsKey('ResourceName'))
	{
		$resourcePath = $targetProject.FullName | Split-Path | Join-Path -ChildPath "Areas\$Module\Resources\$($ResourceName).resx"
		if((Test-Path $resourcePath) -eq $false)
		{
			$resourcesFolderPath = "WebApp\Areas\$Module\Resources"
			Add-Resource $ResourceName $resourcesFolderPath
		}
	}

	$moduleItem = $areaItem.ProjectItems.Item($Module)
	if(($moduleItem.ProjectItems | ? { $_.Name -eq "Models" }) -eq $null)
	{
		$moduleItem.ProjectItems.AddFolder("Models")
	}

	$strippedName = $Name -replace 'Dto$'
	$viewModelClassName = "$($strippedName)ViewModel"
	$toViewModeClassName = "$($Name)To$($viewModelClassName)Mapping"
	$fromViewModelClassName = "$($viewModelClassName)To$($Name)Mapping"
	$viewModelFolderPath  = $targetProject.FullName | Split-Path | Join-Path -ChildPath "Areas\$Module\Models"
	Write-Host "Scaffolding ViewModel $viewModelClassName in area $Module of WebApp project"
	Write-Host "Scaffolding ViewModel mapping $toViewModeClassName in area $Module of WebApp project"
	Write-Host "Scaffolding ViewModel mapping $fromViewModelClassName in area $Module of WebApp project path"
	$targetNamespace = $targetProject.Properties.Item('DefaultNamespace').Value + ".Areas.$Module.Models"
	$sourceFullClassName = $project.Properties.Item('DefaultNamespace').Value + ".Dto.$Name"
	$xmlCommand = [xml] (Get-CreateViewModelFromDtoCommand $Module $sourceFullClassName $assemblyPath $targetNamespace $viewModelClassName $toViewModeClassName $fromViewModelClassName $ResourceName -isForCardIndex:$IsForCardIndex)
	Write-Verbose 'Command xml'
	Write-Verbose $xmlCommand.OuterXml
	$output = Invoke-PowerShellAutomationCommand $xmlCommand
	Write-Verbose 'Output xml'
	Write-Verbose $output.OuterXml
	$viewModelClassPath = $viewModelFolderPath | Join-Path -ChildPath "$($viewModelClassName).cs"
	$toViewModelClassPath = $viewModelFolderPath | Join-Path -ChildPath "$($toViewModeClassName).cs"
	$fromViewModelClassPath = $viewModelFolderPath | Join-Path -ChildPath "$($fromViewModelClassName).cs"
	Write-Verbose "Viewmodel class path: $viewModelClassPath"
	Write-Verbose "Viewmodel class path: $toDtoClassPath"
	Write-Verbose "Viewmodel class path: $fromDtoClassPath"
	if((Test-Path -Path $viewModelClassPath) -ne $false)
	{
		Write-Warning "$viewModelClassPath already exists"
		return $false
	}
	if((Test-Path -Path $toViewModelClassPath) -ne $false)
	{
		Write-Warning "$toViewModelClassPath already exists"
		return  $false
	}
	if((Test-Path -Path $fromViewModelClassPath) -ne $false)
	{
		Write-Warning "$fromViewModelClassPath already exists"
		return $false
	}
	if($PSCmdlet.MyInvocation.BoundParameters['Verbose'].IsPresent)
	{
		return $true
	}

	try
	{
		$files = @()
		$items = @()
		$null = $output.Output.ViewModelCode | Convert-UnicodeLineEndings | Out-File -FilePath $viewModelClassPath -Encoding UTF8
		$files += $viewModelClassPath

		$viewModelClassItem = $targetProject.ProjectItems.AddFromFile($viewModelClassPath)
		$items += $viewModelClassItem

		$null = $output.Output.ToViewModelMappingCode | Convert-UnicodeLineEndings | Out-File -FilePath $toViewModelClassPath  -Encoding UTF8
		$files += $toViewModelClassPath
		$null = $output.Output.FromViewModelMappingCode | Convert-UnicodeLineEndings | Out-File -FilePath $fromViewModelClassPath -Encoding UTF8
		$files += $fromViewModelClassPath

		$items += $viewModelClassItem.ProjectItems.AddFromFile($toViewModelClassPath)
		$items += $viewModelClassItem.ProjectItems.AddFromFile($fromViewModelClassPath)

		$resourceValues =  $output.Output.ResourceValues | Convert-UnicodeLineEndings

		if($resourceValues)
		{
			Write-Host "Injecting resource values"

			$resourcesPath = "WebApp\Areas\$Module\Resources\$($ResourceName).resx"
			$pairs = $resourceValues.Split("`r`n")
			$keys = @()

			foreach($pair in $pairs)
			{
				if($pair -ne "")
				{
					$values = $pair.Split("-")
					$keys += $values[0]
				}
			}

			Add-StringsToResource $resourcesPath $keys
			
			$resourcesPath = $targetProject.FullName | Split-Path | Join-Path -ChildPath "Areas\$Module\Resources\$($ResourceName).resx"

			foreach($pair in $pairs)
			{
				if($pair -ne "")
				{
					$tab = $pair.Split("-")
					$resourceKey = $tab[0]
					$resourceValue = $tab[1]
					Update-ValueInResource $resourcesPath $resourceKey $resourceValue
				}
			}
		}

		if($createAreaOutput)
		{
			$null = $createAreaOutput.Output.ClassCode | Convert-UnicodeLineEndings | Out-File -FilePath $areaRegistrationClassPath  -Encoding UTF8
			$files += $areaRegistrationClassPath
			$items += $moduleItem
			$items += $moduleItem.ProjectItems.AddFromFile($areaRegistrationClassPath)
			$null = Start-OpenAndCleanup $targetProject $viewModelClassPath
		}

		$null = Start-OpenAndCleanup $targetProject $viewModelClassPath
		$null = Start-OpenAndCleanup $targetProject $toViewModelClassPath
		$null = Start-OpenAndCleanup $targetProject $fromViewModelClassPath
	}
	catch
	{
		Write-Error $_
		Write-Host 'Revoking changes'

		$null = $files | ? { Test-Path $_ } | % { Remove-Item $_ }
		$null = $items | ? { $_ -ne $null } | % { $_.Remove() }

		Write-Host 'Done'
	}
}

<#
.SYNOPSIS
	Add new CardIndexDataService based on specified entity

.DESCRIPTION
	Add new CardIndexDataService based on specified entity

.PARAMETER ModuleWithEntity
	Module with Entity name
	Eg. "Entertainment\Actor"
#>
function Add-CardIndexDataService
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[Parameter(Position=0,Mandatory=1)]
		[string]$ModuleWithEntity
	)

	$module, $entity = $ModuleWithEntity.split('\',2);

	if($module -eq $null -or $entity -eq $null)
	{
		Write-Warning "Parameter must specify module and entity name separated by '\', e.g Entertainment\Actor"
		return $false
	}

	$targetProject = (Get-Project -Name "*Business\$module")
	if( $targetProject -eq $null)
	{
		$targetProject = (Get-Project -Name "*Business\$module (Business)")
		if($targetProject -eq $null)
		{
			Write-Warning "Project $Module doesn't exist"
			return $false
		}
	}

	$dtoClassPath = $targetProject.FullName | Split-Path | Join-Path -ChildPath ("\Dto\$($entity)Dto.cs")

	if( (Test-Path -Path $dtoClassPath) -ne $True)
	{
		Write-Warning "Dto for $entity doesn't exist"
		return $false
	}

	$dtoClassName = "$($entity)Dto"
	$cardIndexDataServiceInterface = "I$($entity)CardIndexDataService"
	$cardIndexDataService = "$($entity)CardIndexDataService"

	$serviceFolderPath  = $targetProject.FullName | Split-Path | Join-Path -ChildPath Services

	$interfaceFolderPath = $targetProject.FullName | Split-Path | Join-Path -ChildPath Interfaces

	$serviceClassPath = $serviceFolderPath | Join-Path -ChildPath ("$($cardIndexDataService).cs")
	$serviceInterfacePath = $interfaceFolderPath | Join-Path -ChildPath ("$($cardIndexDataServiceInterface).cs")
	$serviceInstallerPath = $targetProject.FullName | Split-Path | Join-Path -ChildPath ("\ServiceInstaller.cs")

	if((Test-Path -Path $serviceClassPath) -ne $false)
	{
		Write-Warning "File $serviceClassPath already exists"
		return $false
	}
	if((Test-Path -Path $serviceInterfacePath) -ne $false)
	{
		Write-Warning "File $serviceInterfacePath already exists"
		return $false
	}
	if((Test-Path -Path $serviceInstallerPath) -eq $false)
	{
		Write-Warning "File $serviceInstallerPath does not exists"
		return $false
	}

	Write-Host "Scaffolding $cardIndexDataServiceInterface in module $module"
	Write-Host "Scaffolding $cardIndexDataService in module $module"

	$cardIndexCommand = [xml] (Get-CreateCardIndexDataServiceCommand $module $entity)
	Write-Verbose 'CardIndexCommand xml'
	Write-Verbose $cardIndexCommand.OuterXml
	$cardIndexOutput = Invoke-PowerShellAutomationCommand $cardIndexCommand
	Write-Verbose 'CardIndexOutput xml'
	Write-Verbose $cardIndexOutput.OuterXml

	Write-Host "Injecting service registration into ServiceInstaller"

	$serviceInstallerCode = (Get-Content -Path $serviceInstallerPath | Out-String )
	$injectServiceRegistrationCommand = [xml] (Get-InjectServiceRegistrationIntoServiceInstallerCommand $serviceInstallerCode $cardIndexDataServiceInterface $cardIndexDataService)
	Write-Verbose 'InjectServiceRegistrationCommand xml'
	Write-Verbose $injectServiceRegistrationCommand.OuterXml
	$injectServiceRegistrationOutput = Invoke-PowerShellAutomationCommand $injectServiceRegistrationCommand
	Write-Verbose 'InjectServiceRegistrationOutput xml'
	Write-Verbose $injectServiceRegistrationOutput.OuterXml

	if($PSCmdlet.MyInvocation.BoundParameters['Verbose'].IsPresent)
	{
		return $true
	}
	try
	{
		$files = @()
		$items = @()

		$null = $cardIndexOutput.Output.ServiceClassCode | Convert-UnicodeLineEndings | Out-File -FilePath $serviceClassPath -Encoding UTF8
		$files += $serviceClassPath
		$items += $targetProject.ProjectItems.AddFromFile($serviceClassPath)

		$null = $cardIndexOutput.Output.ServiceInterfaceCode | Convert-UnicodeLineEndings | Out-File -FilePath $serviceInterfacePath -Encoding UTF8
		$files += $serviceInterfacePath
		$items += $targetProject.ProjectItems.AddFromFile($serviceInterfacePath)

		$projectItem = Start-OpenFile $targetProject $serviceInstallerPath
		$null = $projectItem.Document.Close()

		$null = $injectServiceRegistrationOutput.Output.ClassCode | Convert-UnicodeLineEndings | Out-File -FilePath $serviceInstallerPath -Encoding UTF8

		$null = Start-OpenAndCleanup $targetProject $serviceClassPath
		$null = Start-OpenAndCleanup $targetProject $serviceInterfacePath
		$null = Start-OpenAndCleanup $targetProject $serviceInstallerPath
	}
	catch
	{
		Write-Error $_
		Write-Host 'Revoking changes'

		$null = $files | ? { Test-Path $_ } | % { Remove-Item $_ }
		$null = $items | ? { $_ -ne $null } | % { $_.Remove() }

		Write-Host 'Done'
	}
}

<#
.SYNOPSIS
	Inject ServiceInstaller registration into ContainerConfiguration

.DESCRIPTION
	Inject ServiceInstaller registration into ContainerConfiguration

.PARAMETER Project
	Project name with module
	Eg. "Business\Entertainment"
#>
function Add-ServiceInstallerRegistration
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[Parameter(Position=0,Mandatory=1)]
		[string]$Project
	)

	if($Project -like "*\*")
	{
		$module, $projectName = $Project.split('\',2);
		$projectBasename = $projectName.Split(' ')[0]
		$projectNameWithModule = "$module.$projectBasename"
	}
	else
	{
		$projectBasename = $Project.Split(' ')[0]
		$projectNameWithModule = $projectBasename
	}

	$targetProject = (Get-Project -Name $projectName)
	if( $targetProject -eq $null)
	{
		Write-Warning "Project $Project doesn't exist"
		return $false
	}

	$serviceInstallerPath = $targetProject.FullName | Split-Path | Join-Path -ChildPath ("\ServiceInstaller.cs")

	if( (Test-Path -Path $serviceInstallerPath) -ne $True)
	{
		Write-Warning "ServiceInstaller for $Project doesn't exist"
		return $false
	}

	$webAppProject = (Get-Project -Name "WebApp")
	if( $webAppProject -eq $null)
	{
		Write-Warning "Project WebApp doesn't exist"
		return $false
	}

	$containerConfigurationPath = $webAppProject.FullName | Split-Path | Join-Path -ChildPath ("\ContainerConfiguration.cs")
	if( (Test-Path -Path $containerConfigurationPath) -ne $True)
	{
		Write-Warning "ContainerConfiguration for WebApp doesn't exist"
		return $false
	}

	Write-Host "Injecting ServiceInstaller registration into ContainerConfiguration"

	$containerConfigurationCode = (Get-Content -Path $containerConfigurationPath | Out-String )

	$xmlCommand = [xml] (Get-InjectServiceInstallerIntoContainerConfigurationCommand $containerConfigurationCode $projectNameWithModule)
	Write-Verbose 'Command xml'
	Write-Verbose $xmlCommand.OuterXml
	$xmlOutput = Invoke-PowerShellAutomationCommand $xmlCommand
	Write-Verbose 'Output xml'
	Write-Verbose $xmlOutput.OuterXml

	if($PSCmdlet.MyInvocation.BoundParameters['Verbose'].IsPresent)
	{
		return $true
	}

	$projectItem = Start-OpenFile $webAppProject $containerConfigurationPath
	$null = $projectItem.Document.Close()

	$null = $xmlOutput.Output.ClassCode | Convert-UnicodeLineEndings | Out-File -FilePath $containerConfigurationPath -Encoding UTF8

	$null = Start-OpenAndCleanup $targetProject $containerConfigurationPath
}

<#
.SYNOPSIS
	Add new card index controller

.DESCRIPTION
	Add new card index controller

.PARAMETER ModuleWithEntity
	Project name with module
	Eg. "Business\Entertainment"
#>
function Add-CardIndexController
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[Parameter(Position=0,Mandatory=1)]
		[string]$ModuleWithEntity
	)

	$module, $entity = $ModuleWithEntity.Split("/\\", 2)

	if($module -eq $null -or $entity -eq $null)
	{
		Write-Warning "Parameter must specify module and entity name separated by '\', e.g Entertainment\Actor"
		return $false
	}

	# Check whether entity exists
	$entityClassPath = ( Get-Project -Name 'Entities (Data)' ).FullName | Split-Path | Join-Path -ChildPath ("\Modules\$module\$entity.cs")
	if( (Test-Path -Path $entityClassPath) -eq $false)
	{
		Write-Warning "Entity $entity located in 'Entities (Data)\Modules\$module' doesn't exist"
		return $false
	}

	# Check whether module business project exists
	$entityDto = "$($entity)Dto"
	$moduleProject = (Get-Project -Name "*Business\$module")
	if( $moduleProject -eq $null)
	{
		$moduleProject = (Get-Project -Name "*Business\$module (Business)")
		if( $moduleProject -eq $null )
		{
			Write-Warning "$entityDto not found: 'Business\$module (Business)' project doesn't exist"
			return $false
		}
	}

	# Check whether entity dto exists
	$dtoClassPath = $moduleProject.FullName | Split-Path | Join-Path -ChildPath ("\Dto\$($entityDto).cs")
	if( (Test-Path -Path $dtoClassPath) -ne $true )
	{
		Write-Warning "$entityDto doesn't exist in Business\$($moduleProject.Name)\Dto"
		return $false
	}

	# Check whether entity viewmodel exists
	$entityViewModel = "$($entity)ViewModel"
	$webappProject = (Get-Project -Name WebApp)
	if( $webappProject -eq $null)
	{
		Write-Warning "WebApp project doesn't exist"
		return $false
	}
	$entityViewModelPath = $webappProject.FullName | Split-Path | Join-Path -ChildPath "Areas\$module\Models\$entityViewModel.cs"
	if( (Test-Path -Path $entityViewModelPath) -eq $false )
	{
		Write-Warning "$entityViewModel doesn't exist in WebApp\Areas\$module\Models"
		return $false
	}

	# Check whether card index data service exists
	$cardIndexServiceInterface = "I$($entity)CardIndexDataService"
	$cardIndexServiceInterfacePath = $moduleProject.FullName | Split-Path | Join-Path -ChildPath "Interfaces\$cardIndexServiceInterface.cs"
	if( (Test-Path -Path $cardIndexServiceInterfacePath) -ne $true )
	{
		Write-Warning "$cardIndexServiceInterface doesn't exist in 'Business\$($moduleProject.Name)\Interfaces'"
		return $false
	}

	# Generate controller class code if file not exists
	$controllerName = "$($entity)Controller"
	if( ($webappProject.ProjectItems | ? { $_.Name -eq "Areas" }) -eq $null )
	{
		Write-Host "Adding 'Areas' folder to WebApp"
		$webappProject.ProjectItems.AddFolder("Areas")
	}
	$areasItem = $webappProject.ProjectItems.Item("Areas")
	if( ($areasItem.ProjectItems | ? { $_.Name -eq $module }) -eq $null )
	{
		Write-Host "Adding '$module' folder to WebApp\Areas"
		$areasItem.ProjectItems.AddFolder($module)
	}
	$moduleItem = $areasItem.ProjectItems.Item($module)
	if( ($moduleItem.ProjectItems | ? { $_.Name -eq "Controllers" }) -eq $null )
	{
		Write-Host "Adding 'Controllers' folder to WebApp\Areas\$module"
		$moduleItem.ProjectItems.AddFolder("Controllers")
	}
	$controllersItem = $moduleItem.ProjectItems.Item("Controllers")
	if( ($controllersItem.ProjectItems | ? { $_.Name -eq "$controllerName.cs" }) -ne $null )
	{
		Write-Warning "$controllerName already exists in WebApp\Areas\$module\Controllers"
		return $false
	}

	Write-Host "Scaffolding $controllerName in WebApp\Areas\$module\Controllers"

	$controllerCommand = [xml] (Get-CreateControllerCommand $module $entity)
	Write-Verbose 'ControllerCommand xml'
	Write-Verbose $controllerCommand.OuterXml
	$controllerOutput = Invoke-PowerShellAutomationCommand $controllerCommand
	Write-Verbose 'ControllerOutput xml'
	Write-Verbose $controllerOutput.OuterXml

	if($PSCmdlet.MyInvocation.BoundParameters['Verbose'].IsPresent)
	{
		return $true
	}
	try
	{
		$files = @()
		$items = @()

		$controllerClassPath = $webappProject.FullName | Split-Path | Join-Path -ChildPath "Areas\$module\Controllers\$controllerName.cs"
		$null = $controllerOutput.Output.ClassCode | Convert-UnicodeLineEndings | Out-File -FilePath $controllerClassPath -Encoding UTF8
		$files += $controllerClassPath
		$items += $controllersItem.ProjectItems.AddFromFile($controllerClassPath)

		$null = Start-OpenAndCleanup $webappProject $controllerClassPath
	}
	catch
	{
		Write-Error $_
		Write-Host 'Revoking changes'

		$null = $files | ? { Test-Path $_ } | % { Remove-Item $_ }
		$null = $items | ? { $_ -ne $null } | % { $_.Remove() }

		Write-Host 'Done'
		return $false
	}

	return $true
}

<#
.SYNOPSIS
	Add new menu item to the sitemap.xml

.DESCRIPTION
	Add new menu item to the sitemap.xml. New item's resource keys are added to the MenuResources.resx

.PARAMETER Path
	Path to the parent menu item. Menu items in the path are identified by resource keys.
	Eg.: Administration/Accounts
	Not existing menu items are created along with their resource keys.

.PARAMETER ResourceKey
	Name of the resource key.
	Eg.: Users

.PARAMETER Module
	Name of the module (area).
	Eg.: Accounts

.PARAMETER Controller
	Name of the controller without 'Controller' suffix.
	Eg.: User

.PARAMETER Action
	Name of the action. Default is empty 'List'
	Eg.: Index

.PARAMETER Params
	Value of the params attribute. Default is empty
	Eg.: filter=active

.PARAMETER Group
	Value of the group attribute. Default is empty
	Eg.: layout
#>
function Add-SiteMapMenuItem
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[Parameter(Position=0,Mandatory=1)]
		[string]$Path,
		[Parameter(Position=1,Mandatory=1)]
		[string]$ResourceKey,
		[Parameter(Position=2,Mandatory=1)]
		[string]$Module,
		[Parameter(Position=3,Mandatory=1)]
		[string]$Controller,
		[Parameter(Position=4,Mandatory=0)]
		[string]$Action,
		[Parameter(Position=5,Mandatory=0)]
		[string]$Params,
		[Parameter(Position=6,Mandatory=0)]
		[string]$Group
	)

	# Process parameters
	$menuResourceKeys = @()
	$keys = $Path.Split("/\\", [System.StringSplitOptions]::RemoveEmptyEntries)

	$actionAttr = 'List'
	if( $PSBoundParameters.ContainsKey('Action') )
	{
		$actionAttr = $Action
	}

	# Check whether WebApp project exists
	$webappProject = (Get-Project -Name WebApp)
	if( $webappProject -eq $null)
	{
		Write-Warning "WebApp project doesn't exist"
		return $false
	}

	# Check whether sitemap.xml exists
	$siteMapPath = $webappProject.FullName | Split-Path | Join-Path -ChildPath "sitemap.xml"
	if( (Test-Path -Path $siteMapPath) -eq $false )
	{
		Write-Warning "sitemap.xml doesn't exist in WebApp/sitemap.xml"
		return $false
	}

	[xml]$siteMapXml = Get-Content $siteMapPath -Raw

	# Build menu item subpath if not exist
	$xpath = '//Menu'
	$subNode = $siteMapXml.SelectSingleNode($xpath)
	foreach( $key in $keys )
	{
		$newSubNode = $subNode.SelectSingleNode("MenuItem[@resourceKey='$key']")

		if( $newSubNode -eq $null )
		{
			Write-Verbose "SubMenuItem $key doesn't exist as a child node of $xpath, creating..."
			$nodeXml = [xml] "<MenuItem resourceKey=""$key"" />"
			$nodeXml = $siteMapXml.ImportNode($nodeXml.DocumentElement, $true)
			$newSubNode = $subNode.AppendChild($nodeXml)
			$menuResourceKeys += $key
			Write-Verbose "SubMenuItem $key added to the $xpath node successfully"
		}

		$xpath += "/MenuItem[@resourceKey='$key']"
		$subNode = $newSubNode
	}

	# Construct leaf item XML node
	if( $subNode.SelectSingleNode("MenuItem[@resourceKey='$ResourceKey']") -ne $null )
	{
		Write-Warning "MenuItem $ResourceKey already exists as a child node of $xpath"
		return $false
	}

	Write-Host "Adding $nodeXml to $xpath..."

	$nodeXml = "<MenuItem area=""$Module"" controller=""$Controller"" action=""$actionAttr"" resourceKey=""$ResourceKey"""
	if( $PSBoundParameters.ContainsKey('Params') )
	{
		$nodeXml += " params=""$Params"""
	}
	if( $PSBoundParameters.ContainsKey('Group') )
	{
		$nodeXml += " group=""$Group"""
	}
	$nodeXml += " />"
	$nodeXml = [xml]$nodeXml
	$nodeXml = $siteMapXml.ImportNode($nodeXml.DocumentElement, $true)
	$null = $subNode.AppendChild($nodeXml)
	$menuResourceKeys += $ResourceKey
	Write-Verbose "MenuItem $ResourceKey added to the $xpath node successfully"

	if( $PSCmdlet.MyInvocation.BoundParameters['Verbose'].IsPresent )
	{
		return $true
	}

	$null = $siteMapXml.OuterXml | Convert-UnicodeLineEndings | Format-Xml | Out-File -FilePath $siteMapPath -Encoding UTF8
	Add-StringsToResource "WebApp\Resources\MenuResources" $menuResourceKeys

	return $true
}

<#
.SYNOPSIS
	Add all CardIndex components based on specified entity

.DESCRIPTION
	Add all CardIndex components based on specified entity in following order:
	- create DTO (if it doesn't exist)
	- create CardIndexDataService (if it doesn't exist)
	- add CardIndexDataService installer registration (if it doesn't exist)
	- add ViewModel (if it doesn't exist)
	- add Controller (if it doesn't exist)
	- add menu items to the sitemap.xml (if they don't exist)
	- create controller resource files in WebApp/Areas/Module/Resources (if they don't exist)

.PARAMETER ModuleWithEntity
	Module with Entity name
	Eg. "Entertainment\Actor"
#>
function Add-CardIndex
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[Parameter(Position=0,Mandatory=1)]
		[string]$ModuleWithEntity
	)

	$module, $entity = $ModuleWithEntity.Split("/\\", 2)

	if($module -eq $null -or $entity -eq $null)
	{
		Write-Warning "Parameter must specify module and entity name separated by '\', e.g Entertainment\Actor"
		return $false
	}

	$entityClassPath = ( Get-Project -Name 'Entities (Data)' ).FullName | Split-Path | Join-Path -ChildPath ("\Modules\$module\$entity.cs")
	if( (Test-Path -Path $entityClassPath) -eq $false)
	{
		Write-Warning "Entity $entity located in area $module doesn't exist"
		return $false
	}

	# Check whether project with entityDto exists (if not, create along with entityDto)
	$entityDto = "$($entity)Dto"
	$moduleProject = (Get-Project -Name "*Business\$module")
	if( $moduleProject -eq $null)
	{
		$moduleProject = (Get-Project -Name "*Business\$module (Business)")
		if( $moduleProject -eq $null )
		{
			Write-Host "Project $module containing $entityDto doesn't exist. Running: Add-DtoFromEntity -Name $entity -ModuleName $module"
			$success = Add-DtoFromEntity -Name $entity -ModuleName $module -Verbose:($PSCmdlet.MyInvocation.BoundParameters["Verbose"].IsPresent -eq $true)
			if( $success -eq $false )
			{
				return $false
			}

			$moduleProject = (Get-Project -Name "*Business\$module")
			if( $moduleProject -eq $null)
			{
				$moduleProject = (Get-Project -Name "*Business\$module (Business)")
			}
		}
	}

	# Check whether entityDto exsits (if not, create)
	$dtoClassPath = $moduleProject.FullName | Split-Path | Join-Path -ChildPath ("\Dto\$($entityDto).cs")
	if( (Test-Path -Path $dtoClassPath) -ne $true)
	{
		Write-Host "Project $entityDto doesn't exist in $module. Running: Add-DtoFromEntity -Name $entity -ModuleName $module"
		$success = Add-DtoFromEntity -Name $entity -ModuleName $module -Verbose:($PSCmdlet.MyInvocation.BoundParameters["Verbose"].IsPresent -eq $true)
		if( $success -eq $false )
		{
			return $false
		}
	}

	# Check whether CardIndexDataService exists (if not, create)
	$cardIndexDataService = "$($entity)CardIndexDataService"
	$cardIndexDataServicePath = $moduleProject.FullName | Split-Path | Join-Path -ChildPath ("Services\$($cardIndexDataService).cs")
	if( (Test-Path -Path $cardIndexDataServicePath) -ne $true )
	{
		Write-Host "$cardIndexDataService doesn't exist in $module\Services. Running: Add-CardIndexDataService -ModuleWithEntity $module\$entity"
		$success = Add-CardIndexDataService -ModuleWithEntity "$module\$entity" -Verbose:($PSCmdlet.MyInvocation.BoundParameters["Verbose"].IsPresent -eq $true)
		if( $success -eq $false )
		{
			return $false
		}
	}

	# Install service into web app's container config if not exist
	Write-Host "Running: Add-CardIndexDataService -ModuleWithEntity Business\$($moduleProject.Name)"
	$success = Add-ServiceInstallerRegistration -Project "Business\$($moduleProject.Name)" -Verbose:($PSCmdlet.MyInvocation.BoundParameters["Verbose"].IsPresent -eq $true)
	if( $success -eq $false )
	{
		return $false
	}

	# Check whether entityViewModel exists (if not, create)
	$entityViewModel = "$($entity)ViewModel"
	$webappProject = (Get-Project -Name WebApp)
	if( $webappProject -eq $null)
	{
		Write-Warning "WebApp project doesn't exist"
		return $false
	}
	$entityViewModelPath = $webappProject.FullName | Split-Path | Join-Path -ChildPath "Areas\$module\Models\$entityViewModel.cs"
	if( (Test-Path -Path $entityViewModelPath) -eq $false )
	{
		Write-Host "$entityViewModel doesn't exist in WebApp\Areas\$module\Models\$entityViewModel.cs. Running: Add-ViewModelFromDto -Name $entityDto"
		$success = Add-ViewModelFromDto -Name $entityDto -Verbose:($PSCmdlet.MyInvocation.BoundParameters["Verbose"].IsPresent -eq $true)
		if( $success -eq $false)
		{
			return $false
		}
	}

	# Check whether controller exists (if not, create)
	$entityController = "$($entity)Controller"
	$entityControllerPath = $webappProject.FullName | Split-Path | Join-Path -ChildPath "Areas\$module\Controllers\$entityController.cs"
	if( (Test-Path -Path $entityControllerPath) -eq $false )
	{
		Write-Host "$entityController doesn't exist in WebApp\Areas\$module\Controllers. Running: Add-CardIndexController -ModuleWithEntity $module\$entity"
		$success = Add-CardIndexController -ModuleWithEntity "$module\$entity" -Verbose:($PSCmdlet.MyInvocation.BoundParameters["Verbose"].IsPresent -eq $true)
		if( $success -eq $false)
		{
			return $false
		}
	}

	# Add site map menu items
	$null = Add-SiteMapMenuItem -Path "/$module" -Module $module -Controller $entity -ResourceKey $entity -Verbose:($PSCmdlet.MyInvocation.BoundParameters["Verbose"].IsPresent -eq $true)

	# Add controller resources
	$resourceName = "$($entity)Resource"
	$controllerResourcesPath = $webappProject.FullName | Split-Path | Join-Path -ChildPath "Areas\$module\Resources\$resourceName.resx"
	if( (Test-Path -Path $controllerResourcesPath) -eq $false )
	{
		Write-Host "$resourceName doesn't exist in WebApp\Areas\$module\Resources. Running: Add-Resource -Name $resourceName -ProjectPath WebApp\Areas\$module\Resources"
		Add-Resource -Name $resourceName -ProjectPath "WebApp\Areas\$module\Resources" -Verbose:($PSCmdlet.MyInvocation.BoundParameters["Verbose"].IsPresent -eq $true)
	}

	return $true
}