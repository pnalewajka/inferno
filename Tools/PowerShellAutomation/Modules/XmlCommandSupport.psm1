﻿<#
.SYNOPSIS
	Get name CreateEntity command for powershell automation execution
	
.DESCRIPTION
	Get name CreateEntity command for powershell automation execution
	
#>
function Get-CreateEntityCommand
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[string]$area,
		[string]$className,
		[string]$baseClassName
	)
	$xml = [xml]'
<Command xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xsi:type="CreateEntityCommand">
  <Area></Area>
  <ClassName></ClassName>
  <BaseClassName></BaseClassName>
</Command>'
	$xml.Command.Area = $area
	$xml.Command.ClassName = $className
	$xml.Command.BaseClassName = $baseClassName
	return $xml
}
<#
.SYNOPSIS
	Get name CreateDtoFromEntity command for powershell automation execution
	
.DESCRIPTION
	Get name CreateDtoFromEntity command for powershell automation execution
	
#>
function Get-CreateDtoFromEntityCommand
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[string]$area,
		[string]$fullClassName,
		[string]$assemblyPath,
		[string]$targetNameSpace,
		[string]$dtoClassName,
		[string]$toDtoClassName,
		[string]$fromDtoClassName
	)
	$xml = [xml]'
<Command xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xsi:type="CreateDtoFromEntityCommand">
  <Area></Area>
  <FullClassName></FullClassName>
  <EntitiesAssemblyPath></EntitiesAssemblyPath>
  <TargetNameSpace></TargetNameSpace>
  <DtoClassName></DtoClassName>
  <ToDtoMapping></ToDtoMapping>
  <FromDtoMapping></FromDtoMapping>
</Command>'
	$xml.Command.Area = $area
	$xml.Command.FullClassName = $fullClassName
	$xml.Command.EntitiesAssemblyPath = $assemblyPath
	$xml.Command.TargetNameSpace = $targetNameSpace
	$xml.Command.DtoClassName = $dtoClassName
	$xml.Command.ToDtoMapping = $toDtoClassName
	$xml.Command.FromDtoMapping = $fromDtoClassName
	return $xml
}

<#
.SYNOPSIS
	Get name CreateViewModeFromDto command for powershell automation execution
	
.DESCRIPTION
	Get name CreateViewModeFromDto command for powershell automation execution
	
#>
function Get-CreateViewModelFromDtoCommand
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[string]$area,
		[string]$fullClassName,
		[string]$assemblyPath,
		[string]$targetNameSpace,
		[string]$viewModelClassName,
		[string]$toViewModeMapping,
		[string]$fromViewModelMapping,
		[string]$resourceName,
		[switch]$isForCardIndex
	)
	$xml = [xml]'
<Command xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xsi:type="CreateViewModelFromDtoCommand">
	<Area>Accounts</Area>
	<FullClassName></FullClassName>
	<DtoAssemblyPath></DtoAssemblyPath>
	<TargetNameSpace></TargetNameSpace>
	<ViewModelClassName></ViewModelClassName>
	<ToViewModeMapping></ToViewModeMapping>
	<FromViewModelMapping></FromViewModelMapping>
	<ResourceName></ResourceName>
	<IsForCardIndex></IsForCardIndex>
</Command>
'
	$xml.Command.Area = $area
	$xml.Command.FullClassName = $fullClassName
	$xml.Command.DtoAssemblyPath = $assemblyPath
	$xml.Command.TargetNameSpace = $targetNameSpace
	$xml.Command.ViewModelClassName = $viewModelClassName
	$xml.Command.ToViewModeMapping = $toViewModeMapping
	$xml.Command.FromViewModelMapping = $fromViewModelMapping
	$xml.Command.ResourceName = $resourceName
	$xml.Command.IsForCardIndex = ( & { if( $isForCardIndex -eq $true) { '1' } else { '0' } } ) 
	return $xml
}

<#
.SYNOPSIS
	Get name CreateSqlForEnum command for powershell automation execution
	
.DESCRIPTION
	Get name CreateSqlForEnum command for powershell automation execution
	
#>
function Get-CreateSqlForEnumCommand
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[string]$schema,
		[string]$fullClassName,
		[string]$assemblyPath,
		[string]$overrideName,
		[string]$overrideNameValue,
		[switch]$omitDescriptions,
		[string]$descriptionLocale,
		[switch]$doNotMaintainRemovedItems
	)
	$xml = [xml]'
<Command xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xsi:type="CreateSqlForEnumCommand">
	<Schema></Schema>
	<FullClassName></FullClassName>
	<AssemblyPath></AssemblyPath>
	<OmitDescriptions></OmitDescriptions>
	<DescriptionLocale></DescriptionLocale>
	<OverrideName></OverrideName>
	<OverrideNameValue></OverrideNameValue>
	<DoNotMaintainRemovedItems></DoNotMaintainRemovedItems>
</Command>
'
	$xml.Command.Schema = $schema
	$xml.Command.FullClassName = $fullClassName
	$xml.Command.AssemblyPath = $assemblyPath
	$xml.Command.OverrideName =  ( & { if( $overrideName -eq $true) { '1' } else { '0' } } )
	$xml.Command.OverrideNameValue = $overrideNameValue
	$xml.Command.OmitDescriptions =  ( & { if( $omitDescriptions -eq $true) { '1' } else { '0' } } )
	$xml.Command.DescriptionLocale = $descriptionLocale
	$xml.Command.DoNotMaintainRemovedItems = ( & { if( $doNotMaintainRemovedItems -eq $true) { '1' } else { '0' } } ) 
	return $xml
}

<#
.SYNOPSIS
	Get name InjectStatementsIntoMigrationCommand command for powershell automation execution
	
.DESCRIPTION
	Get name InjectStatementsIntoMigrationCommand command for powershell automation execution
	
#>
function Get-InjectStatementsIntoMigrationCommand
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[string]$ClassCode,
		[string]$UpScriptResourceName,
		[string]$DownScriptResourceName
	)
	$xml = [xml]'
<Command xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xsi:type="InjectStatementsIntoMigrationCommand">
	<ClassCode></ClassCode>
	<UpScriptResourceName></UpScriptResourceName>
	<DownScriptResourceName></DownScriptResourceName>
</Command>
'
	$xml.Command.ClassCode = $ClassCode
	$xml.Command.UpScriptResourceName = $UpScriptResourceName
	$xml.Command.DownScriptResourceName = $DownScriptResourceName
	return $xml
}

<#
.SYNOPSIS
	Get name InjectDbSetPropertyIntoCoreEntitiesContext command for powershell automation execution
	
.DESCRIPTION
	Get name InjectDbSetPropertyIntoCoreEntitiesContext command for powershell automation execution
	
#>
function Get-InjectDbSetPropertyIntoCoreEntitiesContextCommand
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[string]$ClassCode,
		[string]$Area,
		[string]$EntityName
	)
	$xml = [xml]'
<Command xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xsi:type="InjectDbSetPropertyIntoCoreEntitiesContextCommand">
  <ClassCode></ClassCode>
  <Area></Area>
  <EntityName></EntityName>
</Command>'
	$xml.Command.ClassCode = $ClassCode
	$xml.Command.Area = $Area
	$xml.Command.EntityName = $EntityName
	return $xml
}

<#
.SYNOPSIS
	Get name InjectRepositoryIntoScopeCommand command for powershell automation execution
	
.DESCRIPTION
	Get name InjectRepositoryIntoScopeCommand command for powershell automation execution
	
#>
function Get-InjectRepositoryIntoScopeCommand
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[string]$ClassCode,
		[string]$Area,
		[string]$EntityName
	)
	$xml = [xml]'
<Command xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xsi:type="InjectRepositoryIntoScopeCommand">
  <ClassCode></ClassCode>
  <Area></Area>
  <EntityName></EntityName>
</Command>'
	$xml.Command.ClassCode = $ClassCode
	$xml.Command.Area = $Area
	$xml.Command.EntityName = $EntityName
	return $xml
}

<#
.SYNOPSIS
	Get name CreateCardIndexDataServiceCommand command for powershell automation execution
	
.DESCRIPTION
	Get name CreateCardIndexDataServiceCommand command for powershell automation execution
	
#>
function Get-CreateCardIndexDataServiceCommand
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[string]$module,
		[string]$entity
	)
	$xml = [xml]'
<Command xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xsi:type="CreateCardIndexDataServiceCommand">
  <Module></Module>
  <Entity></Entity>
</Command>'
	$xml.Command.Module = $module
	$xml.Command.Entity = $entity
	return $xml
}

<#
.SYNOPSIS
	Get name CreateControllerCommand command for powershell automation execution
	
.DESCRIPTION
	Get name CreateControllerCommand command for powershell automation execution
	
#>
function Get-CreateControllerCommand
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[string]$module,
		[string]$entity
	)
	$xml = [xml]'
<Command xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xsi:type="CreateControllerCommand">
  <Module></Module>
  <Entity></Entity>
</Command>'
	$xml.Command.Module = $module
	$xml.Command.Entity = $entity
	return $xml
}

<#
.SYNOPSIS
	Get name CreateScopeCommand command for powershell automation execution
	
.DESCRIPTION
	Get name CreateScopeCommand command for powershell automation execution
	
#>
function Get-CreateScopeCommand
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[string]$Area
	)
	$xml = [xml]'
<Command xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xsi:type="CreateScopeCommand">
  <Area></Area>
</Command>'
	$xml.Command.Area = $Area
	return $xml
}

<#
.SYNOPSIS
	Get name CreateServiceInstallerCommand command for powershell automation execution
	
.DESCRIPTION
	Get name CreateServiceInstallerCommand command for powershell automation execution
	
#>
function Get-CreateServiceInstallerCommand
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[string]$ProjectNamespace
	)
	$xml = [xml]'
<Command xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xsi:type="CreateServiceInstallerCommand">
  <ProjectNamespace></ProjectNamespace>
</Command>'
	$xml.Command.ProjectNamespace = $ProjectNamespace
	return $xml
}

<#
.SYNOPSIS
	Get name InjectServiceRegistrationIntoServiceInstallerCommand command for powershell automation execution
	
.DESCRIPTION
	Get name InjectServiceRegistrationIntoServiceInstallerCommand command for powershell automation execution
	
#>
function Get-InjectServiceRegistrationIntoServiceInstallerCommand
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[string]$ClassCode,
		[string]$InterfaceName,
		[string]$ClassName
	)
	$xml = [xml]'
<Command xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xsi:type="InjectServiceRegistrationIntoServiceInstallerCommand">
  <ClassCode></ClassCode>
  <InterfaceName></InterfaceName>
  <ClassName></ClassName>
</Command>'
	$xml.Command.ClassCode = $ClassCode
	$xml.Command.InterfaceName = $InterfaceName
	$xml.Command.ClassName = $ClassName
	return $xml
}

<#
.SYNOPSIS
	Get name InjectServiceInstallerIntoContainerConfiguration command for powershell automation execution
	
.DESCRIPTION
	Get name InjectServiceInstallerIntoContainerConfiguration command for powershell automation execution
	
#>
function Get-InjectServiceInstallerIntoContainerConfigurationCommand
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[string]$ClassCode,
		[string]$ProjectName
	)
	$xml = [xml]'
<Command xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xsi:type="InjectServiceInstallerIntoContainerConfigurationCommand">
  <ClassCode></ClassCode>
  <ProjectName></ProjectName>
</Command>'
	$xml.Command.ClassCode = $ClassCode
	$xml.Command.ProjectName = $ProjectName
	return $xml
}

<#
.SYNOPSIS
	Get name CreateAreaRegistration command for powershell automation execution
	
.DESCRIPTION
	Get name CreateAreaRegistration command for powershell automation execution
	
#>
function Get-CreateAreaRegistrationCommand
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[string]$Area
	)
	$xml = [xml]'
<Command xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xsi:type="CreateAreaRegistrationCommand">
  <Area></Area>
</Command>'
	$xml.Command.Area = $Area
	return $xml
}