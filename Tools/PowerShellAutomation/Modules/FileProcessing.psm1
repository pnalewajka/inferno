﻿<#
.SYNOPSIS
	Fix source maps by removing BOM marker (if exists) from predefined locations (.css.map and .js.map) files
	
.DESCRIPTION
	Fix source maps by removing BOM marker (if exists)
	
.PARAMETER ProjectPath
	Path to project, in VS defaults to WebApp project path

#>
function Update-FixSourceMaps
{
	[CmdletBinding(SupportsShouldProcess = $true)]
	param(
		[Parameter(Mandatory = $false, Position = 0)]
		[string]$ProjectPath
	)

	$private:extensions = @( "*.css.map", "*.js.map")
	$private:webAppRootPath = $ProjectPath

	if($ProjectPath -eq $null){
		$private:webAppRootPath = (Get-Project WebApp).FullName | Split-Path
	}

	$private:foldersToScan  = @( (Join-Path $private:webAppRootPath Content) )

	$private:filesToScan = $private:extensions | % { Get-ChildItem -Path $private:foldersToScan -Filter $_ | % { $_.FullName } }
	$private:filesToScan | % { Update-ClearFileBom $_ }
}

<#
.SYNOPSIS
	Remove BOM marker from file (if exists)
	
.DESCRIPTION
	Remove BOM marker from file (if exists)
	
.PARAMETER File
	File name

#>
function Update-ClearFileBom
{
	[CmdletBinding(SupportsShouldProcess = $true)]
	param(
		[Parameter(Mandatory = $true, ValueFromPipeline = $true, Position = 0)]
		[string]$File
	)
	
	$private:fileData = [System.IO.File]::ReadAllBytes($File)
	$private:utf8Encoding = New-Object System.Text.UTF8Encoding $true
	$private:preamble = $private:utf8Encoding.GetPreamble()

	if($private:fileData.Length -lt $private:preamble.Length){
		return
	}
	
	if($private:fileData[0] -eq $private:preamble[0] -and $private:fileData[1] -eq $private:preamble[1] -and $private:fileData[2] -eq $private:preamble[2])
	{
		("$File : BOM found, removing")
		$private:fileData = $private:fileData[3..($private:fileData.Length-1)]
		[System.IO.File]::WriteAllBytes($File, $private:fileData)
	}
}