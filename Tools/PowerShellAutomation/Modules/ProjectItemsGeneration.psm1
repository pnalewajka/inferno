﻿<#
.SYNOPSIS
Add new empty library class project with modified csproj, added Castle.Windsor references, Nuget package and etc.

.DESCRIPTION
Add new empty library class project with modified csproj, added Castle.Windsor references, Nuget package and etc.

.PARAMETER Location
Folder path inside Smt.Application

.PARAMETER ProjectName
New project name

.Example
Add-Project Business Test

.LINK
#>
function Add-Project ()
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[Parameter(Position=0,Mandatory=1)]
		[string]$Location,
		[Parameter(Position=1,Mandatory=1)]
		[string]$ProjectName
	)

	$rootPath = "Smt.Application"
	$applicationNamespace = "Application"
	
	Write-Host "Adding new project '$applicationNamespace.$Location.$ProjectName'"

	if (-not (Get-Project -Name *$applicationNamespace\$Location*))
	{
		Write-Warning "Location '$rootPath.$Location' doesn't exist"

		return
	}

	if (-not (Get-Project -Name *$applicationNamespace\$Location\*))
	{		
		Write-Warning "Location '$rootPath.$Location' isn't a correct directory"

		return
	}

	if (Get-Project -Name *$applicationNamespace\$Location\$ProjectName*)
	{
		Write-Warning "Project '$rootPath.$Location.$ProjectName' already exist"

		return
	}

	Try
	{
		$projectBasename = $ProjectName.Split(' ')[0]
		$solutionEnvironment = Get-Interface $dte.Solution ([EnvDTE80.Solution2])
		$solution = (Get-Project).dte.Solution;
		$solutionPath = ($solution.FullName | Split-Path) + "\"
		$projectPath = $solutionPath + "$applicationNamespace\$Location\$projectBasename";
		$templatePath = $solutionEnvironment.GetProjectTemplate("Class Library (.NET Framework)", "CSharp")
		$applicationFolder = $solution.PROJECTS | ? Name -eq $applicationNamespace;
		$locationFolder = $applicationFolder.ProjectItems | ? Kind -eq $FolderTypeGuid | ? Name -eq $Location
		$emptyClassFileName = "Class1.cs"

		$LocationFolder.Object.Object.AddFromTemplate($templatePath, $projectPath, $ProjectName)
		
		$project = (Get-Project -Name $ProjectName)
		
		Write-Host "Removing '$emptyClassFileName' file"
		
		[void]($project.ProjectItems | ? Name -eq $emptyClassFileName).Delete()
		
		Write-Host "Modifying project configuration"

		$projectNamespace = "Smt.Atomic.$Location.$projectBasename"
		
		$project.Properties.Item("RootNamespace").Value = $projectNamespace
		$project.Properties.Item("AssemblyName").Value = $projectNamespace
		$project.Properties.Item("OutputType").Value = 2
		$project.Properties.Item("TargetFramework").Value = "$FrameworkVersion"

		Add-MSProjectMetaData $ProjectName RestorePackages $true
		Add-MSProjectMetaData $ProjectName PublishUrl publish\
		Add-MSProjectMetaData $ProjectName Install $true
		Add-MSProjectMetaData $ProjectName InstallFrom Disk
		Add-MSProjectMetaData $ProjectName UpdateEnabled $false
		Add-MSProjectMetaData $ProjectName UpdateMode Foreground
		Add-MSProjectMetaData $ProjectName UpdateInterval 7
		Add-MSProjectMetaData $ProjectName UpdateIntervalUnits Days
		Add-MSProjectMetaData $ProjectName UpdatePeriodically $false
		Add-MSProjectMetaData $ProjectName UpdateRequired $false
		Add-MSProjectMetaData $ProjectName MapFileExtensions $true
		Add-MSProjectMetaData $ProjectName ApplicationRevision 0
		Add-MSProjectMetaData $ProjectName ApplicationVersion 1.0.0.%2a
		Add-MSProjectMetaData $ProjectName IsWebBootstrapper $false
		Add-MSProjectMetaData $ProjectName UseApplicationTrust $false
		Add-MSProjectMetaData $ProjectName BootstrapperEnabled $true
		
		$project = (Get-Project -Name $ProjectName)
	
		[void]$project.ConfigurationManager.AddConfigurationRow("DEV", "Debug", $true)
		[void]$project.ConfigurationManager.AddConfigurationRow("STAGE", "Debug", $true)
		[void]$project.ConfigurationManager.AddConfigurationRow("PROD", "Release", $true)
				
		[void]$project.ConfigurationManager.DeleteConfigurationRow("Debug")
		[void]$project.ConfigurationManager.DeleteConfigurationRow("Release")

		$solutionConfigurations = $solution.SolutionBuild.SolutionConfigurations
		$solutionConfigurations.Item("Debug").Delete()
		$solutionConfigurations.Item("Release").Delete()

		Write-Host "Adding nuget to '$rootPath.$Location.$ProjectName'"

		Write-Host "Adding Castle Windsor references to '$rootPath.$Location.$ProjectName'"
		Install-Package Castle.Core -ProjectName $ProjectName -Version (Get-Project -Name WebApp).Object.References.Item("Castle.Core").Version
		Install-Package Castle.Windsor -ProjectName $ProjectName -Version (Get-Project -Name WebApp).Object.References.Item("Castle.Windsor").Version
		
		$assemblyInfoFileName = "AssemblyInfo.cs"
		$assemblyInfoDirectory = "Properties"
		$filepath = "$projectPath\$assemblyInfoDirectory\$assemblyInfoFileName"

		Write-Host "Modyfing '$filepath'"
		
		$content = Get-Content $filepath
		$content = $content | ? { $_ -notlike '*//*' -and ($_ -like "*using System.*" -or $_ -like "*AssemblyTitle*" -or $_ -like "*AssemblyDescription*" -or $_ -like "*Guid*") }

		$content | Set-Content $filepath

		$globalAsseblyFile = ".global\GlobalAssemblyInfo.cs"

		Write-Host "Linking file '$globalAsseblyFile' to '$rootPath.$Location.$ProjectName' $solutionPath+$globalAsseblyFile"
		
		[void]($project.ProjectItems | ? Name -eq Properties).ProjectItems.AddFromFile($solutionPath+$globalAsseblyFile)

		Write-Host "Scaffolding ServiceInstaller"

		$createServiceInstallerCommand = [xml] (Get-CreateServiceInstallerCommand $projectNamespace)
		Write-Verbose 'CreateServiceInstallerCommand xml'
		Write-Verbose $createServiceInstallerCommand.OuterXml
		$createServiceInstallerOutput = Invoke-PowerShellAutomationCommand $createServiceInstallerCommand
		Write-Verbose 'CreateServiceInstallerOutput xml'
		Write-Verbose $createServiceInstallerOutput.OuterXml

		if($PSCmdlet.MyInvocation.BoundParameters['Verbose'].IsPresent)
		{
			return
		}

		if($Location -eq "Business")
		{
			Write-Host "Creating Dto, Services, Interfaces folders"
			[void]$project.ProjectItems.AddFolder("Dto")
			[void]$project.ProjectItems.AddFolder("Services")
			[void]$project.ProjectItems.AddFolder("Interfaces")

			Write-Host "Adding references"
			[void]$project.Object.References.AddProject((Get-Project -Name 'Configuration (Business)'))
			[void]$project.Object.References.AddProject((Get-Project -Name 'Common (Business)'))
			[void]$project.Object.References.AddProject((Get-Project -Name 'Common (CrossCutting)'))
			[void]$project.Object.References.AddProject((Get-Project -Name 'Entities (Data)'))
			[void]$project.Object.References.AddProject((Get-Project -Name 'Repositories (Data)'))

			[void](Get-Project -Name WebApp).Object.References.AddProject($project)
		}

		Write-Host "Adding ServiceInstaller into project"

		$serviceInstallerClassPath = "$projectPath\ServiceInstaller.cs"
		$null = $createServiceInstallerOutput.Output.ServiceCode | Convert-UnicodeLineEndings | Out-File -FilePath $serviceInstallerClassPath -Encoding UTF8
		$null = $project.ProjectItems.AddFromFile($serviceInstallerClassPath)
		$null = Start-OpenAndCleanup $project $serviceInstallerClassPath

		Write-Host "Saving project '$rootPath.$Location.$ProjectName'"

		[void]$project.Save()
	}
	Catch
	{
		Write-Error "Error while creating project '$rootPath.$Location.$ProjectName': $_.Exception.Message"
		Break
	}
}

 <#
.SYNOPSIS
	Create new Resource (.resx) file in specified path within project

.DESCRIPTION
	Create new Resource (.resx) file in specified path within project

.PARAMETER Name
	Resource file name (without extension)

.PARAMETER ProjectPath
	Relative path to folder where resource should be created
	Eg. "WebApp/Areas/Accounts/Resources"

.PARAMETER Languages
	Optional. List of languages (so script can create additional, language-specific files)
	English is a default language, so script will create .resx file instead of .en.resx
	Eg. Passing @("pl", "en") will result in creating default .resx file and .pl.resx file
	Passing @("en") will result in creating only default file
	If no value is passed at all, global:SupportedLanguages value from Globals.psm1 is used

#>
function Add-Resource
 {
 	[CmdletBinding(SupportsShouldProcess=$true)]
 	param
 	(
 		[Parameter(Position=0,Mandatory=1)]
		[string]$Name,
		[Parameter(Position=1,Mandatory=1)]
		[string]$ProjectPath,
		[Parameter(Position=2,Mandatory=0)]
		[string[]]$Languages
	)
	if(!$PSBoundParameters.ContainsKey('Languages'))
	{
		$Languages = $global:SupportedLanguages
	}

	if($Languages -eq $null)
	{
		$Languages = @("en")
	}

	$pathItems = $ProjectPath.Split("\")
	$projectName = $pathItems[0]
	$pathItems = $pathItems[1..($pathItems.Length-1)]
	$project = Get-Project $projectName
	$projectItem = $project

	foreach($item in $pathItems)
	{
		if(($projectItem.ProjectItems | ? { $_.Name -eq $item }) -eq $null)
		{
			$projectItem.ProjectItems.AddFolder($item)
		}

		$projectItem = $projectItem.ProjectItems.Item($item)
	}

	$templatePath = (Split-Path $project.Dte.FullName -parent) + '\ItemTemplates\CSharp\General\1033\Resource\Resource.vstemplate'

	foreach($lang in $Languages)
	{
		$newFileName = if($lang -eq "en") { "$Name.resx" } else { "$Name.$lang.resx" }
		$newFilePath = (Split-Path $project.FullName -parent) + "\" + $ProjectPath.Split("\",2)[1] + "\$newFileName"

		if(Test-Path $newFilePath)
		{
			Write-Warning "File $newFilePath already exists. Skipping..."
			continue
		} 

		$null = $projectItem.ProjectItems.AddFromTemplate($templatePath, "$newFileName")
		$null = $projectItem.ProjectItems.Item("$newFileName").Properties.Item('CustomTool').Value = 'PublicResXFileCodeGenerator'
		Write-Host "Created file $newFilePath"
	}

	$project.Save()
}

 <#
.SYNOPSIS
	Add keys to specified resource file (files)

.DESCRIPTION
	Add keys to specified resource file (files)

.PARAMETER ResourcePath
	Relative resource path - project name\path \to \resource without extension 
	Example: 'Accounts (Business)\DocumentMappings\UserResources' will cause files
		'Accounts (Business)\DocumentMappings\UserResources.resx' 
		'Accounts (Business)\DocumentMappings\UserResources.pl.resx'
	to be modified
	Or with extension to modify specified file

.PARAMETER KeysToAdd
	Comma separated (or array/list object) list of keys to add

#>
function Add-StringsToResource
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[Parameter(Position=0,Mandatory=1)]
		[string]$ResourcePath,
		[Parameter(Position=1,Mandatory=1)]
		[string[]]$KeysToAdd
	)

	$private:pathParts = $ResourcePath.Split("\")
	$private:projectName = $private:pathParts[0]
	$private:project = Get-Project $private:projectName
	$private:resourceLocation = ($private:project.FullName | Split-Path ) + '\' + [string]::Join('\', $private:pathParts[1..($private:pathParts.Length-1)])
	if($private:pathParts[$private:pathParts.Length-1] -like "*.resx")
	{
		$private:resourceForModification = $private:resourceLocation
	}
	else
	{
		$private:resourceForModification = Get-ChildItem -Path ($private:resourceLocation | Split-Path) -Filter (($private:resourceLocation | Split-Path -Leaf) + '*.resx') | % FullName
	}

	foreach($private:resource in $private:resourceForModification)
	{
		$private:xml = [xml](Get-Content $private:resource)

		foreach($private:key in $KeysToAdd)
		{
			$private:nodes = @($private:xml.Root.SelectNodes('data[@name="'+$private:key+'"]'))
			if($private:nodes.Length -ne 0) 
			{
				continue
			}

			$private:data = $private:xml.CreateElement("data")
			$private:value = $private:xml.CreateElement("value")
			$private:valueData = $private:xml.CreateTextNode($private:key)
			$null = $private:value.AppendChild($private:valueData)
			$null = $private:data.AppendChild($private:value)
			$null = $private:data.SetAttribute("name", $private:key)
			$null = $private:data.SetAttribute("xml:space", "preserve")

			$null = $private:xml.Root.AppendChild($private:data)
		}

		Write-Verbose "Saving $private:resource"
		$null = $private:xml.Save($private:resource)

		Write-Verbose "Starting custom tool for $private:resource"
		$null = Start-CustomTool $private:resource

		$null = Start-OpenFile -project $private:project -filepath $private:resource
	}
} 


 <#
.SYNOPSIS
	Update value for specific key

.DESCRIPTION
	Update value for specific key

.PARAMETER ResourcePath
	Relative resource path - project name\path \to \resource without extension,
	Example: 'Accounts (Business)\DocumentMappings\UserResources' will cause files
		'Accounts (Business)\DocumentMappings\UserResources.resx' 
		'Accounts (Business)\DocumentMappings\UserResources.pl.resx'
	Or Absolute path

.PARAMETER Key
	Key

.PARAMETER Value
	Value

#>
function Update-ValueInResource
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[Parameter(Position=0,Mandatory=1)]
		[string]$ResourcePath,
		[Parameter(Position=1,Mandatory=1)]
		[string]$Key,
		[Parameter(Position=2,Mandatory=1)]
		[string]$Value
	)
	
	$private:resourceFilePaths = @($ResourcePath)

	if( (Split-Path -IsAbsolute $ResourcePath) -eq $false)
	{
		$private:pathParts = $ResourcePath.Split("\")
		$private:projectName = $private:pathParts[0]
		$private:project = Get-Project $private:projectName
		$private:resourceLocation = ($private:project.FullName | Split-Path ) + '\' + [string]::Join('\', $private:pathParts[1..($private:pathParts.Length-1)])
		$private:resourceFilePaths = Get-ChildItem -Path ($private:resourceLocation | Split-Path) -Filter (($private:resourceLocation | Split-Path -Leaf) + '*.resx') | % FullName
	}

	foreach($private:resource in $private:resourceFilePaths) 
	{
		$private:xml = [xml](Get-Content $private:resource)

		$private:node = $private:xml.Root.SelectSingleNode('data[@name="'+$Key+'"]/value')

		if($private:node -eq $null)
		{
			Write-Error "$Key node not found in $private:resource"
			return
		}

		$private:node.InnerText = $Value
		Write-Verbose "Saving modified content in file $private:resource"
		$null = $private:xml.Save($private:resource)
	}
}