﻿<#
.SYNOPSIS
	Get global service using VS Shell.Package, if interface is provided, cmdlet will return wrapped interface
	
.DESCRIPTION
	Get global service using VS Shell.Package, if interface is provided, cmdlet will return wrapped interface
	
.PARAMETER ServiceType
	Requested service type

.PARAMETER InterfaceType
	Requested interface type

#>
function Get-VSService
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param(
		[Parameter(Position = 0, Mandatory = $true)]
		[type]$ServiceType,
		[Parameter(Position = 1, Mandatory = $false)]
		[type]$InterfaceType
	)

	$private:service = [Microsoft.VisualStudio.Shell.Package]::GetGlobalService($ServiceType)

	if ($private:service -and $InterfaceType) {
		$private:service = Get-Interface $private:service $InterfaceType
	}
 
	return $private:service
}

<#
.SYNOPSIS
	Get wrapped interface 
	
.DESCRIPTION
	Get wrapped interface from COM (usually hidden behind transparent proxy)
	
.PARAMETER ComObject
	COM object

.PARAMETER InterfaceType
	Requested interface type

#>
function Get-Interface
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param(
		[Parameter(Position = 0, Mandatory = $true)]
		$ComObject,
		[Parameter(Position = 1, Mandatory = $false)]
		[System.Type]$InterfaceType
	)
   
	[NuGetConsole.Host.PowerShell.Implementation.PSTypeWrapper]::GetInterface($ComObject, $InterfaceType)
}

<#
.SYNOPSIS
	Add MSBuild metadata to item in csproj
	
.DESCRIPTION
	Add MSBuild metadata to item in csproj
	
.PARAMETER ProjectName
	Project name

.PARAMETER InterfaceType
	Requested interface type

#>
function Add-MSBuildMetaData
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param(
		[Parameter(Position = 0, Mandatory = $true)]
		$ProjectName,
		[Parameter(Position = 1, Mandatory = $true)]
		$FilePath,
		[Parameter(Position = 2, Mandatory = $true)]
		[string]$Key,
		[Parameter(Position = 3, Mandatory = $true)]
		[string]$Value
	)

	$private:project = Get-Project -Name $ProjectName
	$private:solutionInterface = Get-VSService ([Microsoft.VisualStudio.Shell.Interop.SVsSolution]) ([Microsoft.VisualStudio.Shell.Interop.IVsSolution])
	$private:hierarchyCom = ''
	$null = $private:solutionInterface.GetProjectOfUniqueName($private:project.UniqueName, [ref]$private:hierarchyCom)
	$private:hierarchyInterface = Get-Interface $private:hierarchyCom ([Microsoft.VisualStudio.Shell.Interop.IVsHierarchy])
	$private:propertyStorageInterface = Get-Interface $hierarchyCom ([Microsoft.VisualStudio.Shell.Interop.IVsBuildPropertyStorage])
	Write-Verbose "Looking for file: $FilePath"
	$private:projectItem = Get-ProjectItemByFileName -ProjectName $ProjectName -FilePath $FilePath
	$private:itemId = 0
	$null = $private:hierarchyInterface.ParseCanonicalName($private:projectItem.Properties.Item('FullPath').Value, [ref]$private:itemId)
	Write-Verbose "Found item: $($private:itemId)"
	$null = $propertyStorageInterface.SetItemAttribute($private:itemId, $Key, $Value)
	$private:project.Save()
	Write-Verbose "Metadata [$Key] = '$Value' updated for file $FilePath"
}

<#
.SYNOPSIS
	Add Project metadata (property)
	
.DESCRIPTION
	Add Project metadata (property) 
	
.PARAMETER ProjectName
	Project name

.PARAMETER Key
	Property name

.PARAMETER Value
	Property value

#>
function Add-MSProjectMetaData
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param(
		[Parameter(Position = 0, Mandatory = $true)]
		$ProjectName,
		[Parameter(Position = 1, Mandatory = $true)]
		[string]$Key,
		[Parameter(Position = 2, Mandatory = $true)]
		[string]$Value
	)

	$private:project = Get-Project -Name $ProjectName
	$private:solutionInterface = Get-VSService ([Microsoft.VisualStudio.Shell.Interop.SVsSolution]) ([Microsoft.VisualStudio.Shell.Interop.IVsSolution])
	$private:hierarchyCom = ''
	$null = $private:solutionInterface.GetProjectOfUniqueName($private:project.UniqueName, [ref]$private:hierarchyCom)
	$private:hierarchyInterface = Get-Interface $private:hierarchyCom ([Microsoft.VisualStudio.Shell.Interop.IVsHierarchy])
	$private:propertyStorageInterface = Get-Interface $private:hierarchyCom ([Microsoft.VisualStudio.Shell.Interop.IVsBuildPropertyStorage])

	$null = $propertyStorageInterface.SetPropertyValue($Key, $null, [int][Microsoft.VisualStudio.Shell.Interop._PersistStorageType]::PST_PROJECT_FILE, $Value)
	$private:project.Save()
	Write-Verbose "Metadata [$Key] = '$Value' updated for project $ProjectName" 
}

