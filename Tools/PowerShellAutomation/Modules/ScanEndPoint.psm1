﻿function Get-DownloadFromUrl($url)
{
	$return = @{}
	[Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}
	$wc=new-object system.net.webclient
	try
	{
		$t=$wc.downloadstring($url)
		$return.content = $t
		$return.code=200
	}
	catch [System.Net.WebException]
	{
		$return.code = [int]$_.Exception.Response.StatusCode
	}
	return $return
}

function Test-EndPoints
{
<#
.SYNOPSIS
Looks for all wcf endpoint in all configuration files, then tries to download associated wsdl  from extracted location


.DESCRIPTION
Looks for all wcf endpoint in all configuration files, then tries to download associated wsdl  from extracted location

.PARAMETER path
Folder path, or solution dir if not passed

.Example
Test-EndPoints

.LINK
#>
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[Parameter(Position=0,Mandatory=0)]
		[string]$path = ((Get-Project).DTE.Solution.Filename | Split-Path )
	)
	$configurationFiles = @()
	$null = Get-ChildItem $path -Filter *.config -Recurse | % { $configurationFiles+=$_.FullName.ToLower() }

	$processedEndPoint = @{}
	$configurationFiles | % { 	
		$fileForProcessing =$_
		$config = [xml](Get-Content -Path $fileForProcessing)
		$config.SelectNodes("//*[@address!='']") | % { 
			$endpointAddress = $_.address + '?wsdl'
			$result = Get-DownloadFromUrl $endpointAddress
			if($processedEndPoint[$endpointAddress] -eq $null)
			{
				$processedEndPoint[$endpointAddress] = '1'
				if($result.code -ne 200)
				{
					Write-Host ($_.address+":FAILURE:"+($result.code.ToString()))
				}
				else
				{
					try
					{
						[xml]$wsdl = $result.content
						if($wsdl.definitions -eq $null)
						{
							Write-Host ($_.address+":FAILURE:nieprawidłowy wsdl")
						}
						else {
							Write-Host ($_.address+":OK:"+($result.code.ToString()))
						}
					}
					catch [System.Exception]
					{
						Write-Host ($_.address+":FAILURE:"+($result.code.ToString()))
					}
				}
			}
		}
	}
}

#Export-ModuleMember -function Scan-EndPoints


