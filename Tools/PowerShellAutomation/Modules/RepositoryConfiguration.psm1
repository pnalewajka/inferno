﻿function Enable-GitHooks
{
<#
.SYNOPSIS
Enables git hooks

.DESCRIPTION
Enables git hooks by copying all files from $(SolutionDir)/Tools/GitHooks to .git/hooks

.Example
Enable-GitHooks

.LINK
#>
	[CmdletBinding(SupportsShouldProcess=$true)] 
	param
	(
	)
	Write-Host ("[RepositoryConfiguration] Enabling git-hooks")
	
	Copy-Item "Tools\GitHooks\*" ".git\hooks"
	
	Write-Host ("[RepositoryConfiguration] All git hooks enabled")
}

function Disable-GitHooks
{
<#
.SYNOPSIS
Disables git hooks

.DESCRIPTION
Disables git hooks by removing all files from .git/hooks matching files in $(SolutionDir)/Tools/GitHooks

.Example
Enable-GitHooks

.LINK
#>
	[CmdletBinding(SupportsShouldProcess=$true)] 
	param
	(
	)
	Write-Host ("[RepositoryConfiguration] Disabling git-hooks")
	
	$defaultHookFiles = Get-ChildItem "Tools\GitHooks" | % { $_.Name }

	Get-ChildItem ".git\hooks" | ? { $defaultHookFiles -contains $_.Name } | Remove-Item -Force
	
	Write-Host ("[RepositoryConfiguration] All git hooks disabled")
}


#Export-ModuleMember Enable-GitHooks
#Export-ModuleMember Disable-GitHooks