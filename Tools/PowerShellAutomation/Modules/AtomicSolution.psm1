﻿function Clear-Binaries
{
<#
.SYNOPSIS
Remove all binaries located under bin/ obj/ in any subfolder of given path.
If path is not given solution base dir is used

.DESCRIPTION
Remove all binaries located under bin/ obj/ in any subfolder of given path.
If path is not given solution base dir is used

.PARAMETER path
Folder path, or solution dir if not passed

.Example
Clear-Binaries

.LINK
#>
	[CmdletBinding(SupportsShouldProcess=$true)] 
	param
	(
		[Parameter(Position=0,Mandatory=0)]
		[string]$path = ((Get-Project).DTE.Solution.Filename | Split-Path )
	)
	Write-Host ("[CLEAN] - Using directory "+$path)
	Get-ChildItem -Path $path -Recurse -Include @('*.dll','*.pdb','*.config','*.exe') | % { 
		if($_.FullName -notlike "*\.hg\*") 
		{
			if ( $_.FullName -like "*\bin\*" -or   $_.FullName -like "*\obj\*"  )
			{
				Remove-Item -Path $_.FullName
				Write-Verbose $_.FullName 
			}
		}
	}
	Write-Host "[CLEAN] - Binaries removed"
}


#Export-ModuleMember Clear-Binaries