﻿function Search-Users
{	
<#
.SYNOPSIS
Search for specific user in database


.DESCRIPTION
Search for specific user in database

.PARAMETER name
Part of the  name (first name, last name, user name)

.Example
Search-Users data

.LINK
#>
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[Parameter(Position=0,Mandatory=0)]
		[string]$Name
	)
	$array = Get-SqlExecuteReader "SELECT Login FROM [Accounts].[User] WHERE FirstName LIKE '%$Name%' OR  LastName LIKE '%$Name%' OR Login LIKE '%$Name%' ORDER BY Login" -useEF
	return $array
}

function Set-UserPassword
{
<#
.SYNOPSIS
Set run now flag of specific job to true


.DESCRIPTION
Set run now flag of specific job to true

.PARAMETER Login
User login

.PARAMETER Password
User password

.Example
Set-UserPassword <user login>

.LINK
#>
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[Parameter(Position=0,Mandatory=1)]
		[string]$Login,
		[Parameter(Position=1,Mandatory=1)]
		[string]$Password
	)
	($pass, $salt) = Get-CredentialForPassword $Password
	Invoke-SqlExecuteNonQuery "UPDATE [Accounts].[Credential] SET [PasswordHash] = '$pass', [Salt]='$salt' WHERE UserId = (  SELECT ID FROM [Accounts].[User] WHERE Login = '$Login' )" -useEF
	Get-SqlExecuteReader "SELECT Login, [IsActive] FROM [Accounts].[User] WHERE Login = '$Login'" -useEF
	Write-Host "Password set for $Login "
}

function Clear-UserExpiration
{
<#
.SYNOPSIS
Set [IsExpired] to zero, and last usage to [DateTime.Now]


.DESCRIPTION
Set [IsExpired] to zero, and last usage to [DateTime.Now]

.PARAMETER Login
User login

.Example
Clear-UserExpiration <user login>

.LINK
#>
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[Parameter(Position=0,Mandatory=1)]
		[string]$Login
	)
	Invoke-SqlExecuteNonQuery "UPDATE [Accounts].[Credential] SET [IsExpired] = 0, [LastUsedOn]=GETDATE() WHERE UserId = ( SELECT ID FROM [Accounts].[User] WHERE Login = '$Login' )" -useEF
	Get-SqlExecuteReader "SELECT Login, [IsActive] FROM [Accounts].[User] WHERE Login = '$Login'" -useEF
	Write-Host "$Login expiration cleared"
}

function Get-CredentialForPassword
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[Parameter(Position=0,Mandatory=1)]
		[string]$Password
	)
	Add-Type -AssemblyName System.Security
	$crypt = New-Object System.Security.Cryptography.Rfc2898DeriveBytes($Password,16)
	$generatedPasswordHash = $crypt.GetBytes(32)
	$generatedSalt = $crypt.Salt
	$base64PasswordHash = [System.Convert]::ToBase64String($generatedPasswordHash)
	$base64Salt = [System.Convert]::ToBase64String($generatedSalt)
	return ($base64PasswordHash, $base64Salt)
}