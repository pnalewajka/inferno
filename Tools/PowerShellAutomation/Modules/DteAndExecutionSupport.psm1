﻿<#
.SYNOPSIS
	Build project, for similar naming use part with wildcard - *CrossCutting\Common*

.DESCRIPTION
	Build project, for similar naming use part with wildcard - *CrossCutting\Common*

.PARAMETER projectName
	Required project name

#>
function Start-BuildProject
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param(
		[Parameter(Position = 0, Mandatory = $true)]
		$ProjectName
	)
	$configuration = 'DEV'
	$project = (Get-Project -Name $projectName)
	$uniqueName = $project.UniqueName
	$null = $project.DTE.Solution.SolutionBuild.BuildProject($configuration,$uniqueName,$true)
	if( $project.DTE.Solution.SolutionBuild.LastBuildInfo -ne 0)
	{
		throw 'Powershell automation project build failure'
	}
}

<#
.SYNOPSIS
	Invoke powershellautomation.exe passing xml command as input stream, returns output from .exe

.DESCRIPTION
	Invoke powershellautomation.exe passing xml command as input stream, returns output from .exe

.PARAMETER xml
	Xml object with xml command

#>
function Invoke-PowerShellAutomationCommand
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param(
		[Parameter(Position = 0, Mandatory = $true)]
		[xml]$xml
	)
	$binPath = (Get-MainAssemblyOfProjectPath 'PowerShellAutomation')
	$result = [xml]($xml.OuterXml | & $binPath)
	if($lastexitcode -ne 0)
	{
		throw 'PowerShell automation command failed'
	}
	return $result
}


<#
.SYNOPSIS
	Convert unix style line endings to windows

.DESCRIPTION
	Convert unix style line endings to windows

#>
function Convert-LineEndings
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param(
		[Parameter(Position = 0, Mandatory = $true, ValueFromPipeline=$true)]
		[string]$data
	)
	return ($data -ireplace "([^`r])`n",('$1'+"`r`n"))
}

<#
.SYNOPSIS
	Convert unix style line endings to windows

.DESCRIPTION
	Convert unix style line endings to windows

#>
function Convert-UnicodeLineEndings
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param(
		[Parameter(Position = 0, Mandatory = $false, ValueFromPipeline=$true)]
		[string]$data
	)
	$data = $data -ireplace "\u000d", ''
	$data = $data -ireplace "\u000a", ("`r`n")

	return $data
}

<#
.SYNOPSIS
	Format Xml document

.DESCRIPTION
	Format Xml document

.PARAMETER xml
	Xml string with xml document to format
#>
function Format-Xml 
{ 
	[CmdletBinding(SupportsShouldProcess=$true)]
	param(
		[Parameter(Position=0, Mandatory=$true, ValueFromPipeline=$true)]
		$xml
	)

	$xml = [xml]$xml
	$StringWriter = New-Object System.IO.StringWriter
	$XmlWriter = New-Object System.XMl.XmlTextWriter $StringWriter
	$xmlWriter.Formatting = "indented"
	$xmlWriter.Indentation = 2
	$xml.WriteContentTo($XmlWriter)
	$XmlWriter.Flush()
	$StringWriter.Flush()

	return $StringWriter.ToString()
}

<#
.SYNOPSIS
	Set startup project

.DESCRIPTION
	Set startup project to chosen one

#>
function Set-StartupProject
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param(
		[Parameter(Position = 0, Mandatory = $true)]
		[string]$ProjectName
	)
	$project = Get-Project $ProjectName
	$project.DTE.Solution.Properties.Item("StartupProject").Value = $project.Name
}

<#
.SYNOPSIS
	Get  full path to main assembly of specific project, build if required

.DESCRIPTION
	Get  full path to main assembly of specific project, build if required

#>
function Get-MainAssemblyOfProjectPath
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param(
		[Parameter(Position = 0, Mandatory = $true)]
		[string]$ProjectName,
		[Parameter(Position = 1, Mandatory = $false)]
		[switch]$ForceBuild = $true
	)
	$configuration = 'DEV'
	$project = (Get-Project -Name $ProjectName)
	$binariesPath = $project.FullName | Split-Path | Join-Path -ChildPath ($project.ConfigurationManager.ActiveConfiguration.Properties.Item('OutputPath').Value) | Join-Path -ChildPath $project.Properties.Item("OutputFilename").Value
	if( ((Test-Path -Path $binariesPath) -ne $true) -or $ForceBuild)
	{
		Start-BuildProject $ProjectName
	}
	return $binariesPath
}

function Start-OpenAndCleanup
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param(
		[Parameter(Position = 0, Mandatory = $true)]
		$project,
		[Parameter(Position = 1, Mandatory = $true)]
		$filepath
	)
	$projectItem = Start-OpenFile @PsBoundParameters
	$null = $projectItem.Document.Activate()
	$null = $projectItem.Document.DTE.ExecuteCommand("Edit.FormatDocument")
	$null = $projectItem.Document.Save()
}

function Start-OpenFile
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param(
		[Parameter(Position = 0, Mandatory = $true)]
		$project,
		[Parameter(Position = 1, Mandatory = $true)]
		$filepath
	)
	$projectItem = $project.DTE.ItemOperations.OpenFile($filepath, [EnvDTE.Constants]::vsViewKindPrimary)
	return $projectItem
}

function Get-ProjectItemByFileName
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param(
		[Parameter(Position = 0, Mandatory = $true)]
		[string]$ProjectName,
		[Parameter(Position = 1, Mandatory = $true)]
		[string]$FilePath
	)
	$projectItemsList = @()
	(Get-Project -Name $ProjectName).ProjectItems | % { $projectItemsList+= $_}
	while($projectItemsList)
	{
		$item, $projectItemsList = $projectItemsList
		if($item.Properties.Item('FullPath').Value -eq $FilePath)
		{
			return $item
		}
		$item.ProjectItems | % { $projectItemsList+= $_ }
	}
	throw "File not found in project items $FilePath"
}

function Get-LogicalNameElement
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param(
		[Parameter(Position = 0, Mandatory = $true)]
		[string]$ProjectName,
		[Parameter(Position = 1, Mandatory = $true)]
		[string]$FilePath,
		[Parameter(Position = 2, Mandatory = $false, ValueFromPipeline = $true)]
		[xml]$CsProj = $null
	)

	$project = Get-Project -Name $ProjectName
	$projectPath = $project.FullName
	$projectFolder = $projectPath | Split-Path
	$relativePath = $FilePath.Replace($projectFolder +'\','')
	$projectNamespace = $project.Properties.Item('DefaultNamespace').Value
	$logicalName = $projectNamespace + '.'+($relativePath -replace '\\','.')
	$result = @{}
	$result.LogicalName=$logicalName
	$result.RelativePath=$relativePath
	return $result
}

<#
.SYNOPSIS
	Add file to specific project item (as subitem) and set it's itemtype property to chosen one (default: embedded resource)

.DESCRIPTION
	Add file to specific project item (as subitem) and set it's itemtype property to chosen one (default: embedded resource)

.PARAMETER ProjectName
	Entities project name

.PARAMETER ProjectItemFile
	Path to required projectitem

.PARAMETER ProjectItemFileType
	Project item type, defaults to embeddedresource

.PARAMETER NewFilePath
	Path to new file
#>
function Add-FileToProjectItem
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[Parameter(Position=0, Mandatory=0)]
		[string]$ProjectName = 'Entities (Data)',
		[Parameter(Position=1, Mandatory=0)]
		[string]$ProjectItemFile,
		[Parameter(Position=2, Mandatory=0)]
		[string]$ProjectItemFileType = 'EmbeddedResource',
		[Parameter(Position=3, Mandatory=1)]
		[string]$NewFilePath
	)
	$migrationProjectItem = Get-ProjectItemByFileName -ProjectName $ProjectName -FilePath $ProjectItemFile
	$projectItem = $migrationProjectItem.ProjectItems.AddFromFile($NewFilePath)
	$projectItem.Properties.Item("ItemType").Value  = $ProjectItemFileType
	$data = $null
	if($ProjectItemFileType -eq 'EmbeddedResource')
	{
		$data = Get-LogicalNameElement -ProjectName $ProjectName -FilePath $NewFilePath
	}
	return $data
}

<#
.SYNOPSIS
	Starts custom tool for specified file
	
.DESCRIPTION
	Starts custom tool for specified file
	
.PARAMETER FilePath
	absolute location of file to run custom tool against

#>
function Start-CustomTool
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[Parameter(Position=0,Mandatory=1)]
		[string]$FilePath
	)

	(Get-Project).DTE.Solution.FindProjectItem($FilePath).Object.RunCustomTool()
}