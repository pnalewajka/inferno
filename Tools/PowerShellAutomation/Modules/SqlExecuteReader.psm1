﻿function Get-SqlExecuteReader
{
<#
.SYNOPSIS
Executes a sqlreader from query against database, then returns list of rows represented as dictionaries

.DESCRIPTION
Executes a sqlreader against database, then returns list of rows represented as dictionaries

.PARAMETER query
Sql query 

.PARAMETER database
Database to query against, defaults to 'Atomic'

.PARAMETER server
Server to query against, defaults to 'localhost'

.PARAMETER useEF
Extract connection string from configuration file for entityframework entry

.Example
Get-SqlExecuteReader "SELECT GETDATE() AS t" 

.Example
$arr = Get-SqlExecuteReader "SELECT GETDATE() AS t" 
$arr[0]["t"]

.LINK
#>
	[CmdletBinding(SupportsShouldProcess=$true)]
	param(
		[Parameter(Position=1,Mandatory=1)]
		[string]$query,
		[Parameter(Position=2,Mandatory=0)]
		[string]$database = "Atomic",
		[Parameter(Position=3,Mandatory=0)]
		[string]$server = "localhost",
		[Parameter(Position=4,Mandatory=0)]
		[switch]$useEF = $false
	)
	$connection = "server=$server;database=$database;Integrated Security=SSPI"
	if($useEF)
	{
		Write-Verbose 'Using EF from config file'
		$connection = Get-EFConnectionString
		Write-Verbose $connection
	}
	try {
		$sqlConnection = new-object System.Data.SqlClient.SqlConnection $connection
		$sqlConnection.Open()
		$sqlCommand = $sqlConnection.CreateCommand()
		$sqlCommand.CommandText = $query
		$sqlReader = $sqlCommand.ExecuteReader()
		$columns = @()
		$dataset = @()
		for( $i=0; $i -lt $sqlReader.FieldCount; $i++)
		{
			$columns+=$sqlReader.GetName($i)
		}
		while ($sqlReader.Read())
		{
			$row = @{}
			$columns | foreach {
				$row[$_]=$sqlReader[$_]
			}
			$dataset+=,$row 
		}
		$sqlConnection.Close()
		return $dataset
	}
	catch
	{
		Write-Error -Exception $_.Exception
	}
	return @()
}

function Invoke-SqlExecuteNonQuery
{
<#
.SYNOPSIS
Executes a nonquery from query against database, no results are returned

.DESCRIPTION
Executes a nonquery from query against database, no results are returned

.PARAMETER query
Sql query 

.PARAMETER database
Database to query against, defaults to 'Atomic'

.PARAMETER server
Server to query against, defaults to 'localhost'

.PARAMETER useEF
Extract connection string from configuration file for entityframework entry

.Example
Invoke-SqlExecuteNonQuery "SELECT GETDATE() AS t" 

.LINK
#>
	[CmdletBinding(SupportsShouldProcess=$true)]
	param(
		[Parameter(Position=1,Mandatory=1)]
		[string]$query,
		[Parameter(Position=2,Mandatory=0)]
		[string]$database = "Atomic",
		[Parameter(Position=3,Mandatory=0)]
		[string]$server = "localhost",
		[Parameter(Position=4,Mandatory=0)]
		[switch]$useEF = $false
	)
	$connection = "server=$server;database=$database;Integrated Security=SSPI"
	if($useEF)
	{
		Write-Verbose 'Using EF from config file'
		$connection = Get-EFConnectionString
		Write-Verbose $connection
	}
	$sqlConnection = new-object System.Data.SqlClient.SqlConnection $connection
	$sqlConnection.Open()
	$sqlCommand = $sqlConnection.CreateCommand()
	$sqlCommand.CommandText = $query
	$sqlReader = $sqlCommand.ExecuteNonQuery()
	$sqlConnection.Close()
}

function Get-EFConnectionString
{
<#
.SYNOPSIS
Get connection string from app or web.config for core entities

.DESCRIPTION
Get connection string from app or web.config for core entities

#>
	[CmdletBinding(SupportsShouldProcess=$true)]
	param(
		[Parameter(Position=1,Mandatory=0)]
		[string]$startupProjectName = 'WebApp',
		[Parameter(Position=2,Mandatory=0)]
		[string]$entititesContext = 'Smt.Atomic.Data.Entities.CoreEntitiesContext'
	)
	$configFile = 'web.config'
	$path = (Get-Project $startupProjectName).FullName | Split-Path | Join-Path -ChildPath $configFile
	if((Test-Path -Path $path) -eq $false)
	{
		$configFile = 'app.config'
		$path = (Get-Project $startupProjectName).FullName | Split-Path | Join-Path -ChildPath $configFile
	}
	Write-Verbose "Looking for config at: $path"
	[xml]$configXml = Get-Content ( $path )
	$conn = $configXml.configuration.connectionStrings.add | Where-Object {$_.name -eq $entititesContext } 
	return $conn.connectionString
}
#Export-ModuleMember Sql-ExecuteReader, Sql-ExecuteNonQuery