﻿using System;
using PowerShellAutomation.CodeAnalysisClasses;

namespace PowerShellAutomation.Commands
{
    [Serializable]
    public class CreateDtoFromEntityCommand : Command
    {
        public string Area { get; set; }
        public string FullClassName { get; set; }
        public string EntitiesAssemblyPath { get; set; }
        public string TargetNameSpace { get; set; }
        public string DtoClassName { get; set; }
        public string ToDtoMapping { get; set; }
        public string FromDtoMapping { get; set; }

        protected override Output ExecuteCommand()
        {
            var obj = new CreateDtoFromEntity();
            var result = obj.GenerateCode(FullClassName, DtoClassName, ToDtoMapping, FromDtoMapping, TargetNameSpace, EntitiesAssemblyPath);
            var output = new CreateDtoFromEntityOutput
            {
                Success = true,
                DtoCode = result.Code,
                ToDtoMappingCode = result.ToTargetMappingCode,
                FromDtoMappingCode = result.FromTargetMappingCode
            };

            return output;
        }

        [Serializable]
        public class CreateDtoFromEntityOutput : Output
        {
            public string DtoCode { get; set; }
            public string ToDtoMappingCode { get; set; }
            public string FromDtoMappingCode { get; set; }
        }
    }
}