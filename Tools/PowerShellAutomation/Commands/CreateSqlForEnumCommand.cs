﻿using PowerShellAutomation.CodeAnalysisClasses;

namespace PowerShellAutomation.Commands
{
    public class CreateSqlForEnumCommand : Command
    {
        public string FullClassName { get; set; }
        public string AssemblyPath { get; set; }
        public string Schema { get; set; }
        public bool OverrideName { get; set; }
        public string OverrideNameValue { get; set; }
        public bool OmitDescriptions { get; set; }
        public string DescriptionLocale { get; set; }
        public bool DoNotMaintainRemovedItems { get; set; }

        protected override Output ExecuteCommand()
        {
            var conv = new CreateSqlForEnum();
            var result = conv.GetCode(this);
            var output = new CreateSqlForEnumOutput
            {
                Success = true,
                TableCreation = result.CreateTableCode,
                ContentCreation = result.DataCode,
                TableDestruction = result.DropTableCode
            };

            return output;
        }

        public class CreateSqlForEnumOutput : Output
        {
            public string TableCreation { get; set; }
            public string ContentCreation { get; set; }
            public string TableDestruction { get; set; }
        }
    }
}
