﻿using System;

namespace PowerShellAutomation.Commands
{
    [Serializable]
    public class Output
    {
        public bool Success { get; set; }
        public Exception Exception { get; set; }
    }
}
