﻿using System;
using PowerShellAutomation.CodeAnalysisClasses;

namespace PowerShellAutomation.Commands
{
    public class InjectDbSetPropertyIntoCoreEntitiesContextCommand : Command
    {
        public string ClassCode { get; set; }
        public string Area { get; set; }
        public string EntityName { get; set; }

        protected override Output ExecuteCommand()
        {
            var obj = new DbSetPropertyCodeInjector();
            var resultCode = obj.GetCode(ClassCode, Area, EntityName);
            var output =  new InjectDbSetPropertyIntoCoreEntitiesOutput
            {
                Success = true,
                ClassCode = resultCode
            };

            return output;
        }

        [Serializable]
        public class InjectDbSetPropertyIntoCoreEntitiesOutput : Output
        {
            public string ClassCode { get; set; }
        }
    }
}
