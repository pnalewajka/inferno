﻿using System;
using PowerShellAutomation.CodeAnalysisClasses;

namespace PowerShellAutomation.Commands
{
    [Serializable]
    public class CreateEntityCommand : Command
    {
        public string Area { get; set; }
        public string ClassName { get; set; }
        public string BaseClassName { get; set; }

        protected override Output ExecuteCommand()
        {
            var obj = new CreateEntity();
            var resultCode = obj.GenerateCode(Area, ClassName, BaseClassName);
            var output =  new CreateEntityOutput
            {
                Success = true,
                EntityClassCode = resultCode
            };

            return output;
        }

        [Serializable]
        public class CreateEntityOutput : Output
        {
            public string EntityClassCode { get; set; }
        }
    }
}