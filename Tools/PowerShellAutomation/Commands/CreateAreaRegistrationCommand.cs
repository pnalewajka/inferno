﻿using PowerShellAutomation.CodeAnalysisClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerShellAutomation.Commands
{
    public class CreateAreaRegistrationCommand : Command
    {
        public string Area { get; set; }

        protected override Output ExecuteCommand()
        {
            var obj = new AreaRegistrationCodeBuilder();
            var resultCode = obj.GenerateCode(Area);
            var output = new CreateAreaRegistrationOutput
            {
                Success = true,
                ClassCode = resultCode
            };

            return output;
        }

        [Serializable]
        public class CreateAreaRegistrationOutput : Output
        {
            public string ClassCode { get; set; }
        }
    }
}
