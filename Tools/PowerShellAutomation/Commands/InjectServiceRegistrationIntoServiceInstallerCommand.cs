﻿using PowerShellAutomation.CodeAnalysisClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerShellAutomation.Commands
{
    public class InjectServiceRegistrationIntoServiceInstallerCommand : Command
    {
        public string ClassCode { get; set; }
        public string InterfaceName { get; set; }
        public string ClassName { get; set; }

        protected override Output ExecuteCommand()
        {
            var obj = new ServiceRegistrationCodeInjector();
            var resultCode = obj.GetCode(ClassCode, InterfaceName, ClassName);
            var output = new InjectServiceRegistrationIntoServiceInstallerOutput
            {
                Success = true,
                ClassCode = resultCode
            };

            return output;
        }

        [Serializable]
        public class InjectServiceRegistrationIntoServiceInstallerOutput : Output
        {
            public string ClassCode { get; set; }
        }
    }
}
