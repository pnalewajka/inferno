﻿using PowerShellAutomation.CodeAnalysisClasses;
using System;

namespace PowerShellAutomation.Commands
{
    public class InjectServiceInstallerIntoContainerConfigurationCommand : Command
    {
        public string ClassCode { get; set; }
        public string ProjectName { get; set; }

        protected override Output ExecuteCommand()
        {
            var obj = new ServiceInstallerCodeInjector();
            var resultCode = obj.GetCode(ClassCode, ProjectName);
            var output = new InjectServiceInstallerIntoContainerConfigurationOutput
            {
                Success = true,
                ClassCode = resultCode
            };

            return output;
        }

        [Serializable]
        public class InjectServiceInstallerIntoContainerConfigurationOutput : Output
        {
            public string ClassCode { get; set; }
        }
    }
}
