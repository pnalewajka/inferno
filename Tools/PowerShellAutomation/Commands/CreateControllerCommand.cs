﻿using System;
using PowerShellAutomation.CodeAnalysisClasses;

namespace PowerShellAutomation.Commands
{
    [Serializable]
    public class CreateControllerCommand : Command
    {
        public string Module { get; set; }
        public string Entity { get; set; }

        protected override Output ExecuteCommand()
        {
            var generator = new CreateController();
            var classCode = generator.GenerateControllerClassCode(Module, Entity);

            return new CreateControllerOutput
            {
                Success = true,
                ClassCode = classCode,
            };
        }

        [Serializable]
        public class CreateControllerOutput : Output
        {
            public string ClassCode { get; set; }
        }
    }
}
