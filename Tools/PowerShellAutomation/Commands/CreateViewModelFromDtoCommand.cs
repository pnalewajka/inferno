﻿using System;
using PowerShellAutomation.CodeAnalysisClasses;

namespace PowerShellAutomation.Commands
{
    [Serializable]
    public class CreateViewModelFromDtoCommand : Command
    {
        public string Area { get; set; }
        public string FullClassName { get; set; }
        public string DtoAssemblyPath { get; set; }
        public string TargetNameSpace { get; set; }
        public string ViewModelClassName { get; set; }
        public string ToViewModeMapping { get; set; }
        public string FromViewModelMapping { get; set; }
        public string ResourceName { get; set; }
        public bool IsForCardIndex { get; set; }

        protected override Output ExecuteCommand()
        {
            var createViewModelFromDto = new CreateViewModelFromDto(Area, FullClassName, ViewModelClassName, ToViewModeMapping, FromViewModelMapping, TargetNameSpace, DtoAssemblyPath, ResourceName, IsForCardIndex);
            var result = createViewModelFromDto.GenerateCode();
            var output = new CreateViewModelFromDtoOutput
            {
                Success = true,
                ViewModelCode = result.Code,
                ToViewModelMappingCode = result.ToTargetMappingCode,
                FromViewModelMappingCode = result.FromTargetMappingCode,
                ResourceValues = result.ResourceValues
            };

            return output;
        }

        [Serializable]
        public class CreateViewModelFromDtoOutput : Output
        {
            public string ViewModelCode { get; set; }
            public string ToViewModelMappingCode { get; set; }
            public string FromViewModelMappingCode { get; set; }
            public string ResourceValues { get; set; }
        }
    }
}