﻿using PowerShellAutomation.CodeAnalysisClasses;
using System;

namespace PowerShellAutomation.Commands
{
    public class CreateServiceInstallerCommand : Command
    {
        public string ProjectNamespace { get; set; }

        protected override Output ExecuteCommand()
        {
            var obj = new ServiceInstallerCodeBuilder();
            var resultCode = obj.GenerateCode(ProjectNamespace);
            var output = new CreateServiceInstallerOutput
            {
                Success = true,
                ServiceCode = resultCode
            };

            return output;
        }

        [Serializable]
        public class CreateServiceInstallerOutput : Output
        {
            public string ServiceCode { get; set; }
        }
    }
}
