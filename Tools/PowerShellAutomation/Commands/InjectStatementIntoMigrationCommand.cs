﻿using System;
using PowerShellAutomation.CodeAnalysisClasses;

namespace PowerShellAutomation.Commands
{
    [Serializable]
    public class InjectStatementsIntoMigrationCommand : Command 
    {
        public string ClassCode { get; set; }
        public string UpScriptResourceName { get; set; }
        public string DownScriptResourceName { get; set; }

        protected override Output ExecuteCommand()
        {
            var rewriter = new InjectStatementsIntoMigrationClassCode();
            var result = rewriter.GetCode(ClassCode, UpScriptResourceName, DownScriptResourceName);
            var output = new InjectStatementIntoMigrationOutput
            {
                ClassCode = result
            };

            return output;
        }

        [Serializable]
        public class InjectStatementIntoMigrationOutput : Output
        {
            public string ClassCode { get; set; }
        }
    }
}
