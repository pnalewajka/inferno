﻿using System;
using PowerShellAutomation.CodeAnalysisClasses;

namespace PowerShellAutomation.Commands
{
    public class InjectRepositoryIntoScopeCommand : Command
    {
        public string ClassCode { get; set; }
        public string Area { get; set; }
        public string EntityName { get; set; }

        protected override Output ExecuteCommand()
        {
            var obj = new RepositoryScopeCodeInjector();
            var resultCode = obj.GetCode(ClassCode, Area, EntityName);
            var output = new InjectRepositoryIntoScopeOutput
            {
                Success = true,
                ClassCode = resultCode
            };

            return output;
        }

        [Serializable]
        public class InjectRepositoryIntoScopeOutput : Output
        {
            public string ClassCode { get; set; }
        }
    }
}
