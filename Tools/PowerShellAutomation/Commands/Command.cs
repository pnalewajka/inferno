﻿using System;

namespace PowerShellAutomation.Commands
{
    [Serializable]
    public abstract class Command
    {
        public Output Execute()
        {
            try
            {
                return ExecuteCommand();
            }
            catch (Exception e)
            {
                return new Output
                {
                    Success = false,
                    Exception = e
                };
            }
        }

        protected abstract Output ExecuteCommand();
    }
}
