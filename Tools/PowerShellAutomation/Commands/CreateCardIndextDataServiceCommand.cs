﻿using System;
using PowerShellAutomation.CodeAnalysisClasses;

namespace PowerShellAutomation.Commands
{
    [Serializable]
    public class CreateCardIndexDataServiceCommand : Command
    {
        public string Module { get; set; }
        public string Entity { get; set; }

        protected override Output ExecuteCommand()
        {
            var obj = new CreateCardIndexDataService();
            var serviceClassCode = obj.GenerateServiceClassCode(Module, Entity);
            var serviceInterfaceCode = obj.GenerateServiceInterfaceCode(Module, Entity);
            var output = new CreateCardIndexDataServiceOutput
            {
                Success = true,
                ServiceClassCode = serviceClassCode,
                ServiceInterfaceCode = serviceInterfaceCode
            };

            return output;
        }

        [Serializable]
        public class CreateCardIndexDataServiceOutput : Output
        {
            public string ServiceClassCode { get; set; }
            public string ServiceInterfaceCode { get; set; }
        }
    }
}
