﻿using System;
using PowerShellAutomation.CodeAnalysisClasses;

namespace PowerShellAutomation.Commands
{
    public class CreateScopeCommand : Command
    {
        public string Area { get; set; }

        protected override Output ExecuteCommand()
        {
            var obj = new DbScopeCodeBuilder();
            var resultCode = obj.GenerateCode(Area);
            var output = new CreateScopeOutput
            {
                Success = true,
                ScopeClassCode = resultCode
            };

            return output;
        }

        [Serializable]
        public class CreateScopeOutput : Output
        {
            public string ScopeClassCode { get; set; }
        }
    }
}
