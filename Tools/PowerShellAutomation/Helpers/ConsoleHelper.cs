﻿using System;
using System.Text;

namespace PowerShellAutomation.Helpers
{
    public static class ConsoleHelper
    {
        public static string GetWholeInput()
        {
            var stringBuilder = new StringBuilder();

            string line;

            do
            {
                line = Console.ReadLine();
                stringBuilder.AppendLine(line);
            } while (line != null);

            return stringBuilder.ToString();
        }        
    }
}