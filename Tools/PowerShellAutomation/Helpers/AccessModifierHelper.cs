﻿using PowerShellAutomation.Enums;
using System.Linq;

namespace PowerShellAutomation.Helpers
{
    public static class AccessModifierHelper
    {
        public static AccessModifierEnum GetMaxAccessibility(params AccessModifierEnum[] accessModifiers)
        {
            var maxAccessibility = (AccessModifierEnum)accessModifiers.Max(m => (int)m);

            return maxAccessibility;
        }
    }
}
