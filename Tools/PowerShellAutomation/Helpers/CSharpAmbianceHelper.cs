﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PowerShellAutomation.Helpers
{
    internal static class CSharpAmbianceHelper
    {
        private static readonly Dictionary<Type, string> Aliases = new Dictionary<Type, string>();

        static CSharpAmbianceHelper()
        {
            Aliases[typeof(byte)] = "byte";
            Aliases[typeof(sbyte)] = "sbyte";
            Aliases[typeof(short)] = "short";
            Aliases[typeof(ushort)] = "ushort";
            Aliases[typeof(int)] = "int";
            Aliases[typeof(uint)] = "uint";
            Aliases[typeof(long)] = "long";
            Aliases[typeof(ulong)] = "ulong";
            Aliases[typeof(char)] = "char";

            Aliases[typeof(float)] = "float";
            Aliases[typeof(double)] = "double";

            Aliases[typeof(decimal)] = "decimal";

            Aliases[typeof(bool)] = "bool";

            Aliases[typeof(object)] = "object";
            Aliases[typeof(string)] = "string";
        }

        private static string RemoveGenericNamePart(string name)
        {
            int backtick = name.IndexOf('`');
            
            if (backtick != -1)
            {
                name = name.Substring(0, backtick);
            }

            return name;
        }

        public static string GetTypeName(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            string keyword;

            if (Aliases.TryGetValue(type, out keyword))
            {
                return keyword;
            }

            if (type.IsArray)
            {
                var stringBuilder = new StringBuilder();

                var ranks = new Queue<int>();
                do
                {
                    ranks.Enqueue(type.GetArrayRank() - 1);
                    type = type.GetElementType();
                } while (type.IsArray);

                stringBuilder.Append(GetTypeName(type));

                while (ranks.Count != 0)
                {
                    stringBuilder.Append('[');

                    int rank = ranks.Dequeue();
                    for (int i = 0; i < rank; i++)
                        stringBuilder.Append(',');

                    stringBuilder.Append(']');
                }

                return stringBuilder.ToString();
            }

            if (type.IsGenericTypeDefinition)
            {
                var stringBuilder = new StringBuilder();

                stringBuilder.Append(RemoveGenericNamePart(type.FullName));
                stringBuilder.Append('<');

                var args = type.GetGenericArguments().Length - 1;
                for (var i = 0; i < args; i++)
                {
                    stringBuilder.Append(',');
                }

                stringBuilder.Append('>');

                return stringBuilder.ToString();
            }

            if (type.IsGenericType)
            {
                if (type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    return GetTypeName(type.GetGenericArguments()[0]) + "?";

                var stringBuilder = new StringBuilder();

                stringBuilder.Append(RemoveGenericNamePart(type.FullName));
                stringBuilder.Append('<');

                var args = type.GetGenericArguments();
                for (int i = 0; i < args.Length; i++)
                {
                    if (i != 0)
                    {
                        stringBuilder.Append(", ");
                    }

                    stringBuilder.Append(GetTypeName(args[i]));
                }

                stringBuilder.Append('>');

                return stringBuilder.ToString();
            }

            return type.FullName;
        }
    }
}
