﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

namespace PowerShellAutomation.Helpers
{
    public static class TriviaHelper
    {
        public static readonly SyntaxTrivia WhitespaceTrivia = SyntaxFactory.Whitespace(" ");
        public static readonly SyntaxTrivia TabTrivia = SyntaxFactory.Whitespace("    ");
        public static readonly SyntaxTrivia NewLineTrivia = SyntaxFactory.CarriageReturnLineFeed;
    }
}
