﻿using System;
using System.Linq;

namespace PowerShellAutomation.Helpers
{
    public class TypeHelper
    {
        public static Type[] GetAssignableTypes(Type type)
        {
            return type.Assembly.GetTypes().Where(t => type.IsAssignableFrom(t) && !t.IsAbstract).ToArray();
        }        
    }
}