﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace PowerShellAutomation.Helpers
{
    public static class CommandSerializationHelper
    {
        public static TClass DeserializeFromXml<TClass>(string xml) where TClass : class
        {
            var buff = Encoding.UTF8.GetBytes(xml);
            var type = typeof(TClass);
            using (var ms = new MemoryStream(buff))
            {
                using (var reader = XmlDictionaryReader.CreateTextReader(ms, new XmlDictionaryReaderQuotas { MaxStringContentLength = int.MaxValue }))
                {
                    var ser = new XmlSerializer(typeof(TClass), TypeHelper.GetAssignableTypes(type));
                    var obj = ser.Deserialize(reader);
                    return obj as TClass;
                }
            }
        }

        public static string SerializeToXml<TClass>(object dataObj) where TClass : class
        {
            using (var writer = new MemoryStream())
            {
                var type = typeof(TClass);
                var attrOverrides = new XmlAttributeOverrides();
                var attrs = new XmlAttributes { XmlIgnore = true };
                attrOverrides.Add(typeof(Exception), "Data", attrs);
                attrOverrides.Add(typeof(Exception), "HResult", attrs);

                var ser = new XmlSerializer(type, attrOverrides, TypeHelper.GetAssignableTypes(type), null, null);
                var encoding = new UTF8Encoding(false);
                var settings = new XmlWriterSettings
                {
                    Indent = true,
                    OmitXmlDeclaration = true,
                    Encoding = encoding
                };

                using (var w = XmlWriter.Create(writer, settings))
                {
                    ser.Serialize(w, dataObj);
                }
                return encoding.GetString(writer.ToArray());
            }
        }        
    }
}