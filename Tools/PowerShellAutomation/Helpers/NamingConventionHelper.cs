﻿using System;
using System.Data.Entity.Design.PluralizationServices;
using System.Globalization;

namespace PowerShellAutomation.Helpers
{
    public static class NamingConventionHelper
    {
        public static string CreateNamespace(params string[] values)
        {
            return string.Join(".", values);
        }

        public static string GetModuleDtoNamespace(string module)
        {
            return $"Smt.Atomic.Business.{module}.Dto";
        }

        public static string GetModuleInterfacesNamespace(string module)
        {
            return $"Smt.Atomic.Business.{module}.Interfaces";
        }

        public static string GetModuleServicesNamespace(string module)
        {
            return $"Smt.Atomic.Business.{module}.Services";
        }

        public static string GetEntitiesModuleNamespace(string module)
        {
            return $"Smt.Atomic.Data.Entities.Modules.{module}";
        }

        public static string GetCardIndexDataServiceClassName(string entity)
        {
            return $"{entity}CardIndexDataService";
        }

        public static string GetStringInQuotes(string value)
        {
            return $@"""{value}""";
        }

        public static string GetCardIndexDataServiceInterfaceName(string entity)
        {
            return $"I{GetCardIndexDataServiceClassName(entity)}";
        }

        public static string GetDbScopeInterfaceName(string module)
        {
            return $"I{module}DbScope";
        }

        public static string GetDtoName(string entity)
        {
            return $"{entity}Dto";
        }

        public static string GetViewModelName(string entity)
        {
            return $"{entity}ViewModel";
        }

        public static string GetControllerClassName(string entity)
        {
            return $"{entity}Controller";
        }

        public static string GetPluralForm(string noun)
        {
            var service = PluralizationService.CreateService(CultureInfo.GetCultureInfo("en-GB"));

            return service.Pluralize(noun);
        }

        public static string GetServiceRegistration(string interfaceName, string className)
        {
            return
                $"container.Register(Component.For<{interfaceName}>().ImplementedBy<{className}>().LifestyleTransient());";
        }

        public static string GetServiceInstallerRegistration(string projectName)
        {
            return $"yield return new {projectName}.ServiceInstaller();";
        }

        public static string GetAreaNamespace(string areaName)
        {
            return $"Smt.Atomic.WebApp.Areas.{areaName}";
        }

        public static string GetAreaModelsNamespace(string areaName)
        {
            return $"{GetAreaNamespace(areaName)}.Models";
        }

        public static string GetAreaControllerNamespace(string areaName)
        {
            return $"{GetAreaNamespace(areaName)}.Controllers";
        }

        public static string GetAreaRegistrationClassName(string area)
        {
            return $"{area}AreaRegistration";
        }

        public static string GetAreaRegistration(string area)
        {
            string baseString = @"context.MapRoute(
                ""{0}_default"",
                ""{0}/{{controller}}/{{action}}/{{id}}"",
                new {{ action = ""Index"", id = UrlParameter.Optional }}
            );";

            return string.Format(baseString, area);
        }

        public static string GetLabelNameForPropertyInQuotes(string entity, string name)
        {
            return $@"""{entity}{name}Label""";
        }
        public static string GetEntityNameFromViewModel(string viewModel)
        {
            return viewModel.Replace("ViewModel", "");
        }

        public static string GetResourceNamespaceForArea(string area)
        {
            return $"Smt.Atomic.WebApp.Areas.{area}.Resources";
        }
    }
}
