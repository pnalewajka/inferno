using System.Diagnostics;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Formatting;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using PowerShellAutomation.CodeAnalysisClasses;

using Microsoft.CodeAnalysis.Formatting;
using Microsoft.CodeAnalysis.MSBuild;

namespace PowerShellAutomation.Helpers
{
    internal static class SyntaxTreeFormatter
    {
        public static string ToString(CompilationUnitSyntax compilationUnit)
        {
            var normalizedCompilationUnit = compilationUnit.NormalizeWhitespace();
            var formattedMappingCompilationUnit = new MappingFormatter().Visit(normalizedCompilationUnit);

            var workspace = MSBuildWorkspace.Create();
            var options = workspace.Options
                .WithChangedOption(FormattingOptions.NewLine, LanguageNames.CSharp, "\n")
                .WithChangedOption(FormattingOptions.UseTabs, LanguageNames.CSharp, false)
                .WithChangedOption(FormattingOptions.TabSize, LanguageNames.CSharp, 4)
                .WithChangedOption(CSharpFormattingOptions.NewLineForElse, true);

            var withOptionsFormattedCompilationUnit = (CompilationUnitSyntax)Formatter.Format(formattedMappingCompilationUnit, workspace, options);
            var result = withOptionsFormattedCompilationUnit.ToFullString();
            result = result.Replace("\r\n        {\r\n            get;\r\n            set;\r\n        }", " {get;set;}\r\n");
            result = result.Replace("\r\n\r\n", "\r\n");
            result = result.Replace("}\r\n            ;\r\n", "};\r\n");
            return result;
        }
    }
}
