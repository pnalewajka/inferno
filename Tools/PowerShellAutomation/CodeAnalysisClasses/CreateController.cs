﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using PowerShellAutomation.Extensions;
using PowerShellAutomation.Helpers;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace PowerShellAutomation.CodeAnalysisClasses
{
    public class CreateController
    {
        public string GenerateControllerClassCode(string module, string entity)
        {
            var commonControllersNamespace = "Smt.Atomic.WebApp.Controllers";
            var renderersInterfacesNamespace = "Smt.Atomic.Presentation.Renderers.Interfaces";
            var moduleDtoNamespace = NamingConventionHelper.GetModuleDtoNamespace(module);
            var moduleInterfacesNamespace = NamingConventionHelper.GetModuleInterfacesNamespace(module);
            var areaModelsNamespace = NamingConventionHelper.GetAreaModelsNamespace(module);

            var entityDto = NamingConventionHelper.GetDtoName(entity);
            var entityViewModel = NamingConventionHelper.GetViewModelName(entity);
            var entityDataServiceInterface = NamingConventionHelper.GetCardIndexDataServiceInterfaceName(entity);
            var entityDataServiceArgName = "cardIndexDataService";
            var dependenciesInterface = "IBaseControllerDependencies";
            var dependenciesArgName = "dependencies";

            var mainNamespace = NamingConventionHelper.GetAreaControllerNamespace(module);
            var className = NamingConventionHelper.GetControllerClassName(entity);

            var usingList = new List<UsingDirectiveSyntax>
            {
                CreateUsingDirective(moduleDtoNamespace),
                CreateUsingDirective(moduleInterfacesNamespace),
                CreateUsingDirective(renderersInterfacesNamespace),
                CreateUsingDirective(areaModelsNamespace),
                CreateUsingDirective(commonControllersNamespace)
                    .WithTrailingNewLineTrivia(2),
            };

            var classNameToken = SyntaxFactory.Identifier(className);

            var baseList = SyntaxFactory.BaseList
                (
                    SyntaxFactory.SeparatedList<BaseTypeSyntax>(new[]
                    {
                        SyntaxFactory
                            .SimpleBaseType
                            (
                                CreateGenericName(
                                    "CardIndexController",
                                    SyntaxFactory.IdentifierName(entityViewModel),
                                    SyntaxFactory.IdentifierName(entityDto))
                            )
                            .WithLeadingWhiteSpaceTrivia()
                            .WithTrailingNewLineTrivia()
                    })
                );

            var ctorParams = CreateParameterList(
                CreateParameter(entityDataServiceInterface, entityDataServiceArgName),
                CreateParameter(dependenciesInterface, dependenciesArgName));

            var ctorInitializer = SyntaxFactory
                .ConstructorInitializer
                (
                    SyntaxKind.BaseConstructorInitializer,
                    CreateArgumentList(CreateArgument(entityDataServiceArgName), CreateArgument(dependenciesArgName))
                        .WithOpenParenToken(SyntaxFactory.Token(SyntaxKind.OpenParenToken))
                        .WithCloseParenToken(SyntaxFactory.Token(SyntaxKind.CloseParenToken))
                )
                .WithColonToken(SyntaxFactory.Token(SyntaxKind.ColonToken).WithTrailingWhiteSpaceTrivia())
                .WithThisOrBaseKeyword(SyntaxFactory.Token(SyntaxKind.BaseKeyword))
                .WithLeadingTabTrivia(3)
                .WithTrailingNewLineTrivia();

            var ctorDeclaration = SyntaxFactory
                .ConstructorDeclaration(classNameToken.WithLeadingWhiteSpaceTrivia())
                .WithModifiers(SyntaxFactory.TokenList(SyntaxFactory.Token(SyntaxKind.PublicKeyword)))
                .WithParameterList(ctorParams)
                .WithLeadingTabTrivia(2)
                .WithTrailingNewLineTrivia()
                .WithInitializer(ctorInitializer)
                .WithBody
                (
                    SyntaxFactory
                        .Block()
                        .WithOpenBraceToken
                        (
                            SyntaxFactory
                                .Token(SyntaxKind.OpenBraceToken)
                                .WithTrailingNewLineTrivia()
                                .WithLeadingTabTrivia(2)
                        )
                        .WithCloseBraceToken
                        (
                            SyntaxFactory
                                .Token(SyntaxKind.CloseBraceToken)
                                .WithLeadingTabTrivia(2)
                                .WithTrailingNewLineTrivia()
                        )
                );

            var classDeclarationSyntax = SyntaxFactory
                .ClassDeclaration
                (
                    classNameToken
                        .WithLeadingWhiteSpaceTrivia()
                        .WithTrailingWhiteSpaceTrivia()
                )
                .WithModifiers
                (
                    SyntaxFactory
                        .TokenList
                        (
                            SyntaxFactory
                                .Token(SyntaxKind.PublicKeyword)
                                .WithTrailingWhiteSpaceTrivia()
                                .WithLeadingTabTrivia()
                        )
                )
                .WithBaseList(baseList)
                .AddMembers(ctorDeclaration)
                .WithOpenBraceToken
                (
                    SyntaxFactory
                        .Token(SyntaxKind.OpenBraceToken)
                        .WithTrailingNewLineTrivia()
                        .WithLeadingTabTrivia()
                )
                .WithCloseBraceToken
                (
                    SyntaxFactory
                        .Token(SyntaxKind.CloseBraceToken)
                        .WithLeadingTabTrivia()
                        .WithTrailingNewLineTrivia()
                );

            var namespaceDeclaration = SyntaxFactory
                .NamespaceDeclaration
                (
                    SyntaxFactory
                        .IdentifierName(mainNamespace)
                        .WithLeadingWhiteSpaceTrivia()
                        .WithTrailingNewLineTrivia()
                )
                .WithOpenBraceToken
                (
                    SyntaxFactory
                        .Token(SyntaxKind.OpenBraceToken)
                        .WithTrailingNewLineTrivia()
                )
                .WithCloseBraceToken
                (
                    SyntaxFactory.Token(SyntaxKind.CloseBraceToken)
                )
                .AddMembers(classDeclarationSyntax);

            return SyntaxFactory
                .CompilationUnit()
                .AddMembers(namespaceDeclaration)
                .WithUsings(SyntaxFactory.List(usingList))
                .ToFullString();
        }


        private static SeparatedSyntaxList<T> CreateSeparatedList<T>(params T[] arguments) where T : SyntaxNode
        {
            var nodes = arguments.SelectMany(a => new SyntaxNodeOrToken[]
            {
                a,
                SyntaxFactory.Token(SyntaxKind.CommaToken).WithTrailingWhiteSpaceTrivia(),
            });

            return SyntaxFactory.SeparatedList<T>(nodes.TakeAllButLast());
        }

        private static TypeArgumentListSyntax CreateTypeArgumentList(params TypeSyntax[] types)
        {
            return SyntaxFactory.TypeArgumentList(CreateSeparatedList(types));
        }

        private static ParameterListSyntax CreateParameterList(params ParameterSyntax[] parameters)
        {
            return SyntaxFactory.ParameterList(CreateSeparatedList(parameters));
        }

        private static ArgumentListSyntax CreateArgumentList(params ArgumentSyntax[] arguments)
        {
            return SyntaxFactory.ArgumentList(CreateSeparatedList(arguments));
        }

        private static UsingDirectiveSyntax CreateUsingDirective(string @namespace)
        {
            return SyntaxFactory
                .UsingDirective(SyntaxFactory.ParseName(@namespace).WithLeadingWhiteSpaceTrivia())
                .WithTrailingNewLineTrivia();
        }

        private static GenericNameSyntax CreateGenericName(string name, params TypeSyntax[] genericArgs)
        {
            return SyntaxFactory.GenericName(
                SyntaxFactory.Identifier(name),
                CreateTypeArgumentList(genericArgs));
        }

        private static ParameterSyntax CreateParameter(string type, string name)
        {
            return SyntaxFactory
                .Parameter(SyntaxFactory.Identifier(name).WithLeadingWhiteSpaceTrivia())
                .WithType(SyntaxFactory.IdentifierName(type));
        }

        private static ArgumentSyntax CreateArgument(string name)
        {
            return SyntaxFactory.Argument(SyntaxFactory.IdentifierName(name));
        }
    }
}
