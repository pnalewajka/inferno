﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using PowerShellAutomation.Extensions;
using PowerShellAutomation.Helpers;
using System.Collections.Generic;

namespace PowerShellAutomation.CodeAnalysisClasses
{
    public class AreaRegistrationCodeBuilder
    {
        public string GenerateCode(string areaName)
        {
            const string webMvcNamespace = "System.Web.Mvc";
            const string baseClassName = "AreaRegistration";
            const string propertyName = "AreaName";
            const string methodName = "RegisterArea";
            const string parameterType = "AreaRegistrationContext";
            const string parameterName = "context";

            string mainNamespace = NamingConventionHelper.GetAreaNamespace(areaName);
            string className = NamingConventionHelper.GetAreaRegistrationClassName(areaName);
            string statement = NamingConventionHelper.GetAreaRegistration(areaName);
            string returnAreaName = NamingConventionHelper.GetStringInQuotes(areaName);

            var usingList = new List<UsingDirectiveSyntax>();

            usingList.Add(
                SyntaxFactory.UsingDirective
                    (
                        SyntaxFactory.ParseName(webMvcNamespace)
                            .WithLeadingWhiteSpaceTrivia()
                    )
                    .WithTrailingNewLineTrivia(2));

            var identifierNameSyntax = SyntaxFactory.IdentifierName(mainNamespace);

            var namespaceDeclarationSyntax = SyntaxFactory.NamespaceDeclaration
                    (
                        identifierNameSyntax
                            .WithLeadingWhiteSpaceTrivia()
                            .WithTrailingNewLineTrivia()
                    )
                    .WithOpenBraceToken
                    (
                        SyntaxFactory.Token(SyntaxKind.OpenBraceToken).WithTrailingNewLineTrivia()
                    )
                    .WithCloseBraceToken
                    (
                        SyntaxFactory.Token(SyntaxKind.CloseBraceToken)
                    );

            var baseListSyntax = SyntaxFactory.BaseList(SyntaxFactory.SeparatedList<BaseTypeSyntax>(new[]
            {
                SyntaxFactory.SimpleBaseType
                (
                    SyntaxFactory.IdentifierName(baseClassName)
                )
                .WithLeadingWhiteSpaceTrivia()
                .WithTrailingNewLineTrivia()
            }));

            var returnSyntaxToken = SyntaxFactory.ParseStatement(areaName);

            var getDeclaration = SyntaxFactory
                .AccessorDeclaration(SyntaxKind.GetAccessorDeclaration,
                    SyntaxFactory.Block
                    (
                        SyntaxFactory.ReturnStatement
                        (
                            SyntaxFactory.Token(SyntaxKind.ReturnKeyword).WithTrailingWhiteSpaceTrivia(),
                            SyntaxFactory.ParseExpression(returnAreaName),
                            SyntaxFactory.Token(SyntaxKind.SemicolonToken).WithTrailingNewLineTrivia()
                        )
                        .WithLeadingTabTrivia(4)
                    )
                    .WithOpenBraceToken
                    (
                        SyntaxFactory.Token(SyntaxKind.OpenBraceToken)
                        .WithLeadingTrivia(TriviaHelper.NewLineTrivia, TriviaHelper.TabTrivia, TriviaHelper.TabTrivia, TriviaHelper.TabTrivia)
                        .WithTrailingNewLineTrivia()
                    )
                    .WithCloseBraceToken
                    (
                        SyntaxFactory.Token(SyntaxKind.CloseBraceToken)
                        .WithLeadingTabTrivia(3)
                        .WithTrailingNewLineTrivia()
                    )
                )
                .WithLeadingTabTrivia(3);

            var accessorListSyntax = SyntaxFactory
                .AccessorList(SyntaxFactory.List(new[] { getDeclaration }))
                    .WithOpenBraceToken
                    (
                        SyntaxFactory.Token(SyntaxKind.OpenBraceToken)
                        .WithLeadingTrivia(TriviaHelper.NewLineTrivia, TriviaHelper.TabTrivia, TriviaHelper.TabTrivia)
                        .WithTrailingNewLineTrivia()
                    )
                    .WithCloseBraceToken
                    (
                        SyntaxFactory.Token(SyntaxKind.CloseBraceToken)
                        .WithLeadingTabTrivia(2)
                        .WithTrailingNewLineTrivia()
                    );

            var propertyNameSyntaxToken = SyntaxFactory.Identifier(propertyName);

            var propertyDeclarationSyntax = SyntaxFactory
                .PropertyDeclaration(SyntaxFactory.PredefinedType(SyntaxFactory.Token(SyntaxKind.StringKeyword)).WithTrailingWhiteSpaceTrivia(),
                        propertyNameSyntaxToken)
                    .WithModifiers(SyntaxFactory.TokenList(
                        new[] {
                            SyntaxFactory.Token(SyntaxKind.PublicKeyword).WithTrailingWhiteSpaceTrivia(),
                            SyntaxFactory.Token(SyntaxKind.OverrideKeyword).WithTrailingWhiteSpaceTrivia()
                        }))
                    .WithAccessorList(accessorListSyntax)
                    .WithLeadingTabTrivia(2)
                    .WithTrailingNewLineTrivia(2);

            var methodNameSyntaxToken = SyntaxFactory.Identifier(methodName);

            var methodDeclarationSyntax = SyntaxFactory
                .MethodDeclaration(SyntaxFactory.PredefinedType(SyntaxFactory.Token(SyntaxKind.VoidKeyword)).WithTrailingWhiteSpaceTrivia(),
                    methodNameSyntaxToken)
                .WithModifiers
                (
                    SyntaxFactory.TokenList(
                        new[] {
                            SyntaxFactory.Token(SyntaxKind.PublicKeyword).WithTrailingWhiteSpaceTrivia(),
                            SyntaxFactory.Token(SyntaxKind.OverrideKeyword).WithTrailingWhiteSpaceTrivia()
                        })
                )
                .WithParameterList
                (
                    SyntaxFactory.ParameterList(SyntaxFactory.SeparatedList<ParameterSyntax>(
                        new SyntaxNodeOrToken[] {
                            SyntaxFactory.Parameter(SyntaxFactory.Identifier(parameterName))
                            .WithType(SyntaxFactory.IdentifierName(parameterType).WithTrailingWhiteSpaceTrivia())
                        })
                    )
                )
                .WithLeadingTabTrivia(2)
                .WithTrailingNewLineTrivia()
                .WithBody
                (
                    SyntaxFactory.Block(SyntaxFactory.ParseStatement(statement).WithLeadingTabTrivia(3).WithTrailingNewLineTrivia())
                    .WithOpenBraceToken
                    (
                        SyntaxFactory.Token(SyntaxKind.OpenBraceToken)
                        .WithLeadingTabTrivia(2)
                        .WithTrailingNewLineTrivia()
                    )
                    .WithCloseBraceToken
                    (
                        SyntaxFactory.Token(SyntaxKind.CloseBraceToken)
                        .WithLeadingTabTrivia(2)
                        .WithTrailingNewLineTrivia()
                    )
                );

            var classNameSyntaxToken = SyntaxFactory.Identifier(className);

            var classDeclarationSyntax = SyntaxFactory
                .ClassDeclaration(classNameSyntaxToken.WithLeadingWhiteSpaceTrivia().WithTrailingWhiteSpaceTrivia())
                .WithModifiers
                (
                    SyntaxFactory.TokenList(SyntaxFactory.Token(SyntaxKind.PublicKeyword)
                    .WithTrailingWhiteSpaceTrivia()
                    .WithLeadingTabTrivia()
                    )
                )
                .WithBaseList(baseListSyntax)
                .AddMembers(propertyDeclarationSyntax, methodDeclarationSyntax)
                .WithOpenBraceToken
                (
                    SyntaxFactory.Token(SyntaxKind.OpenBraceToken)
                        .WithTrailingNewLineTrivia()
                        .WithLeadingTabTrivia()
                )
                .WithCloseBraceToken
                (
                    SyntaxFactory.Token(SyntaxKind.CloseBraceToken)
                        .WithLeadingTabTrivia()
                        .WithTrailingNewLineTrivia()
                );

            namespaceDeclarationSyntax =
                namespaceDeclarationSyntax.AddMembers(classDeclarationSyntax);

            var compilationUnit = SyntaxFactory.CompilationUnit()
                .AddMembers(namespaceDeclarationSyntax)
                .WithUsings(SyntaxFactory.List(usingList));

            return compilationUnit.ToFullString();
        }
    }
}
