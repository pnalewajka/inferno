﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using PowerShellAutomation.Extensions;
using PowerShellAutomation.Helpers;
using System.Collections.Generic;
using System.Linq;

namespace PowerShellAutomation.CodeAnalysisClasses
{
    public abstract class CodeInjector : CSharpSyntaxRewriter
    {
        private readonly string[] _namespaces;

        public CodeInjector(string[] namespaces = null)
        {
            if (namespaces == null)
            {
                _namespaces = new string[0];
            }
            else
            {
                _namespaces = namespaces;
            }
        }

        public string GenerateCode(string classCode)
        {
            var tree = SyntaxFactory.ParseSyntaxTree(classCode);
            var resultNode = Visit(tree.GetRoot());
            var result = resultNode.ToFullString();

            return result;
        }

        public override SyntaxNode VisitCompilationUnit(CompilationUnitSyntax node)
        {
            var usingList = CreateUsings(node);

            if (usingList.Any())
            {
                return base.VisitCompilationUnit(node.AddUsings(usingList.ToArray()));
            }

            return base.VisitCompilationUnit(node);
        }

        private List<UsingDirectiveSyntax> CreateUsings(CompilationUnitSyntax node)
        {
            var usingList = new List<UsingDirectiveSyntax>();

            foreach (var newNamespace in _namespaces)
            {
                if (string.IsNullOrEmpty(newNamespace) || node.Usings.Any(u => u.Name.ToFullString() == newNamespace))
                {
                    continue;
                }

                usingList.Add(SyntaxFactory.UsingDirective(
                        SyntaxFactory.ParseName(newNamespace).WithLeadingWhiteSpaceTrivia())
                        .WithTrailingNewLineTrivia());
            }

            return usingList;
        }
    }
}
