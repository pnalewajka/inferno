﻿using PowerShellAutomation.Enums;
using PowerShellAutomation.Helpers;

namespace PowerShellAutomation.CodeAnalysisClasses
{
    public class RepositoryScopeCodeInjector
    {
        public string GetCode(string classCode, string area, string entityName)
        {
            const string genericTypeNamespace = "Smt.Atomic.Data.Repositories.Interfaces";
            const string genericType = "IRepository";

            var entityNamespace = NamingConventionHelper.GetEntitiesModuleNamespace(area);
            var namespaces = new[] { genericTypeNamespace, entityNamespace };
            var propertyType = $"{genericType}<{entityName}>";
            var propertyName = NamingConventionHelper.GetPluralForm(entityName);

            var injector = new InterfacePropertyInjector(propertyType, propertyName, AccessModifierEnum.Public,
                                                        AccessModifierEnum.None, namespaces);
            var result = injector.GenerateCode(classCode);

            return result;
        }
    }
}
