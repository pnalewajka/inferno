﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using PowerShellAutomation.Extensions;
using PowerShellAutomation.Helpers;
using System.Collections.Generic;


namespace PowerShellAutomation.CodeAnalysisClasses
{
    public class ServiceInstallerCodeBuilder
    {
        public string GenerateCode(string projectNamespace)
        {
            const string windsorNamespace = "Castle.Windsor";
            const string microKernelNamespace = "Castle.MicroKernel.Registration";
            const string commonServicesConfigurationNamespace = "Smt.Atomic.CrossCutting.Common.ServicesConfiguration";
            const string className = "ServiceInstaller";
            const string baseClassName = "AtomicInstaller";
            const string methodName = "Install";
            const string containerType = "ContainerType";
            const string containerTypeParameterName = "containerType";
            const string windsorContainer = "IWindsorContainer";
            const string windsorContainerParameterName = "container";

            var interfacesNamespace = NamingConventionHelper.CreateNamespace(projectNamespace, "Interfaces");
            var servicesNamespace = NamingConventionHelper.CreateNamespace(projectNamespace, "Services");

            var usingList = new List<UsingDirectiveSyntax>();

            usingList.Add(
                SyntaxFactory.UsingDirective
                    (
                        SyntaxFactory.ParseName(microKernelNamespace)
                            .WithLeadingWhiteSpaceTrivia()
                    )
                    .WithTrailingNewLineTrivia());

            usingList.Add(
                SyntaxFactory.UsingDirective
                    (
                        SyntaxFactory.ParseName(windsorNamespace)
                            .WithLeadingWhiteSpaceTrivia()
                    )
                    .WithTrailingNewLineTrivia());

            usingList.Add(
                SyntaxFactory.UsingDirective
                    (
                        SyntaxFactory.ParseName(interfacesNamespace)
                            .WithLeadingWhiteSpaceTrivia()
                    )
                    .WithTrailingNewLineTrivia());

            usingList.Add(
                SyntaxFactory.UsingDirective
                    (
                        SyntaxFactory.ParseName(servicesNamespace)
                            .WithLeadingWhiteSpaceTrivia()
                    )
                    .WithTrailingNewLineTrivia());

            usingList.Add(
                SyntaxFactory.UsingDirective
                    (
                        SyntaxFactory.ParseName(commonServicesConfigurationNamespace)
                            .WithLeadingWhiteSpaceTrivia()
                    )
                    .WithTrailingNewLineTrivia(2));

            var identifierNameSyntax = SyntaxFactory.IdentifierName(projectNamespace);

            var namespaceDeclarationSyntax = SyntaxFactory.NamespaceDeclaration
                    (
                        identifierNameSyntax
                            .WithLeadingWhiteSpaceTrivia()
                            .WithTrailingNewLineTrivia()
                    )
                    .WithOpenBraceToken
                    (
                        SyntaxFactory.Token(SyntaxKind.OpenBraceToken).WithTrailingNewLineTrivia()
                    )
                    .WithCloseBraceToken
                    (
                        SyntaxFactory.Token(SyntaxKind.CloseBraceToken).WithTrailingNewLineTrivia()
                    );

            var baseListSyntax = SyntaxFactory.BaseList(SyntaxFactory.SeparatedList<BaseTypeSyntax>(new[]
            {
                SyntaxFactory.SimpleBaseType(
                    SyntaxFactory.IdentifierName(baseClassName)).WithLeadingWhiteSpaceTrivia()
                    .WithTrailingNewLineTrivia()
            }));

            var methodNameSyntaxToken = SyntaxFactory.Identifier(methodName);

            var methodDeclarationSyntax = SyntaxFactory
                .MethodDeclaration(SyntaxFactory.PredefinedType(SyntaxFactory.Token(SyntaxKind.VoidKeyword)).WithTrailingWhiteSpaceTrivia(),
                    methodNameSyntaxToken)
                .WithModifiers(SyntaxFactory.TokenList(
                    new[] {
                        SyntaxFactory.Token(SyntaxKind.PublicKeyword).WithTrailingWhiteSpaceTrivia(),
                        SyntaxFactory.Token(SyntaxKind.OverrideKeyword).WithTrailingWhiteSpaceTrivia()
                    }))
                .WithParameterList(SyntaxFactory.ParameterList(SyntaxFactory.SeparatedList<ParameterSyntax>(
                    new SyntaxNodeOrToken[] {
                        SyntaxFactory.Parameter(SyntaxFactory.Identifier(containerTypeParameterName))
                            .WithType(SyntaxFactory.IdentifierName(containerType).WithTrailingWhiteSpaceTrivia()),
                            SyntaxFactory.Token(SyntaxKind.CommaToken).WithTrailingWhiteSpaceTrivia(),
                        SyntaxFactory.Parameter(SyntaxFactory.Identifier(windsorContainerParameterName))
                            .WithType(SyntaxFactory.IdentifierName(windsorContainer).WithTrailingWhiteSpaceTrivia())
                    })))
                .WithLeadingTabTrivia(2)
                .WithTrailingNewLineTrivia()
                .WithBody(SyntaxFactory.Block()
                            .WithOpenBraceToken(SyntaxFactory.Token(SyntaxKind.OpenBraceToken).WithLeadingTabTrivia(2)
                            .WithTrailingNewLineTrivia())
                            .WithCloseBraceToken(SyntaxFactory.Token(SyntaxKind.CloseBraceToken).WithLeadingTabTrivia(2)
                            .WithTrailingNewLineTrivia())
                );

            var classNameSyntaxToken = SyntaxFactory.Identifier(className);

            var classDeclarationSyntax = SyntaxFactory
                .ClassDeclaration(classNameSyntaxToken.WithLeadingWhiteSpaceTrivia().WithTrailingWhiteSpaceTrivia())
                .WithModifiers(SyntaxFactory.TokenList(SyntaxFactory.Token(SyntaxKind.PublicKeyword)
                    .WithTrailingWhiteSpaceTrivia()
                    .WithLeadingTabTrivia()
                    )
                )
                .WithBaseList(baseListSyntax)
                .AddMembers(methodDeclarationSyntax)
                .WithOpenBraceToken
                (
                    SyntaxFactory.Token(SyntaxKind.OpenBraceToken)
                        .WithTrailingNewLineTrivia()
                        .WithLeadingTabTrivia()
                )
                .WithCloseBraceToken
                (
                    SyntaxFactory.Token(SyntaxKind.CloseBraceToken)
                        .WithLeadingTabTrivia()
                        .WithTrailingNewLineTrivia()
                );

            namespaceDeclarationSyntax =
                namespaceDeclarationSyntax.AddMembers(classDeclarationSyntax);

            var compilationUnit = SyntaxFactory.CompilationUnit()
                .AddMembers(namespaceDeclarationSyntax)
                .WithUsings(SyntaxFactory.List(usingList));

            return compilationUnit.ToFullString();
        }
    }
}
