﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using PowerShellAutomation.Extensions;
using PowerShellAutomation.Helpers;
using System.Collections.Generic;

namespace PowerShellAutomation.CodeAnalysisClasses
{
    public class CreateEntity
    {
        private const string EntityBaseNamespace = "Smt.Atomic.Data.Entities.Abstracts";
        private const string DataAnnotationsNamespace = "System.ComponentModel.DataAnnotations.Schema";

        private const string ModificationTrackedClassName = "ModificationTrackedEntity";
        private const string CreationTrackeddClassName = "CreationTrackedEntity";
        private const string SimpleClassName = "SimpleEntity";
        private const string TrackedClassName = "TrackedEntity";

        private const string EntityClass = "Entity";

        private readonly IList<string> _classNames =
            new List<string> { CreationTrackeddClassName, ModificationTrackedClassName, SimpleClassName, TrackedClassName };

        public string GenerateCode(string area, string className, string baseClass)
        {
            var mainNamespace = NamingConventionHelper.GetEntitiesModuleNamespace(area);
            var baseTypesList = new List<BaseTypeSyntax>();
            var baseTypeTokens = new List<SyntaxToken>();
            var usingList = new List<UsingDirectiveSyntax>();
            var propertyList = new List<MemberDeclarationSyntax>();

            usingList.Add(
                SyntaxFactory.UsingDirective
                    (
                        SyntaxFactory.ParseName(DataAnnotationsNamespace)
                            .WithLeadingWhiteSpaceTrivia()
                    )
                    .WithTrailingNewLineTrivia());

            usingList.Add(
                SyntaxFactory.UsingDirective(
                    SyntaxFactory.ParseName(EntityBaseNamespace).WithLeadingWhiteSpaceTrivia()
                    )
                    .WithTrailingNewLineTrivia(2));

            var identifierNameSyntax = SyntaxFactory.IdentifierName(mainNamespace);
            var namespaceDeclarationSyntax = SyntaxFactory.NamespaceDeclaration
                    (
                        identifierNameSyntax
                            .WithLeadingWhiteSpaceTrivia()
                            .WithTrailingNewLineTrivia()
                    )
                    .WithOpenBraceToken
                    (
                        SyntaxFactory.Token(SyntaxKind.OpenBraceToken).WithTrailingNewLineTrivia()
                    )
                    .WithCloseBraceToken
                    (
                        SyntaxFactory.Token(SyntaxKind.CloseBraceToken).WithTrailingNewLineTrivia()
                    );

            var classNameSyntaxToken = SyntaxFactory.Identifier(className);
            var tableAttribute = SyntaxFactory.Attribute(SyntaxFactory.IdentifierName("Table"))
                .WithArgumentList(
                    SyntaxFactory.AttributeArgumentList(
                        SyntaxFactory.SeparatedList(
                            new[]
                            {
                                SyntaxFactory.AttributeArgument(
                                    SyntaxFactory.ParseExpression("\"" + className + "\"")),
                                SyntaxFactory.AttributeArgument(
                                    SyntaxFactory.ParseExpression("Schema = \"" + area + "\"")).WithLeadingWhiteSpaceTrivia()
                            }
                            )
                        )
                );

            var listSyntaxs = new[]
            {
                SyntaxFactory.AttributeList(SyntaxFactory.SeparatedList(new[]{ tableAttribute }))
                .WithOpenBracketToken(SyntaxFactory.Token(SyntaxKind.OpenBracketToken).WithLeadingTabTrivia())
                .WithCloseBracketToken(SyntaxFactory.Token(SyntaxKind.CloseBracketToken).WithTrailingNewLineTrivia())
            };

            var attributeLists = SyntaxFactory.List(listSyntaxs);

            var entityBaseClassName = GetBaseClass(baseClass);

            baseTypesList.Add(SyntaxFactory.SimpleBaseType(SyntaxFactory.ParseTypeName(entityBaseClassName)));

            var baseListSyntax = SyntaxFactory.BaseList(SyntaxFactory.SeparatedList(baseTypesList, baseTypeTokens));

            var classDeclarationSyntax = SyntaxFactory
                .ClassDeclaration(classNameSyntaxToken.WithLeadingWhiteSpaceTrivia())
                .WithModifiers(SyntaxFactory.TokenList(SyntaxFactory.Token(SyntaxKind.PublicKeyword)
                    .WithTrailingWhiteSpaceTrivia()
                    .WithLeadingTabTrivia()
                  )
                )
                .WithMembers(SyntaxFactory.List(propertyList))
                .WithAttributeLists(attributeLists)
                .WithBaseList(
                    baseListSyntax
                    .WithColonToken(SyntaxFactory.Token(SyntaxKind.ColonToken)
                        .WithLeadingWhiteSpaceTrivia()
                        .WithTrailingWhiteSpaceTrivia()
                    )
                    .WithTrailingNewLineTrivia()
                )
                .WithOpenBraceToken
                (
                    SyntaxFactory.Token(SyntaxKind.OpenBraceToken)
                    .WithTrailingNewLineTrivia()
                    .WithLeadingTabTrivia()
                )
                .WithCloseBraceToken
                (
                    SyntaxFactory.Token(SyntaxKind.CloseBraceToken)
                    .WithLeadingTabTrivia()
                    .WithTrailingNewLineTrivia()
                );

            if (entityBaseClassName == TrackedClassName)
            {
                classDeclarationSyntax = classDeclarationSyntax.AddMembers(
                    GenerateMethod("override void OnSoftDeleting"));
            }

            namespaceDeclarationSyntax =
                namespaceDeclarationSyntax.AddMembers(classDeclarationSyntax);

            var compilationUnit = SyntaxFactory.CompilationUnit()
                .AddMembers(namespaceDeclarationSyntax)
                .WithUsings(SyntaxFactory.List(usingList));

            return compilationUnit.ToFullString();
        }

        private string GetBaseClass(string baseClass)
        {
            if (_classNames.Contains(baseClass))
            {
                return baseClass;
            }

            if (_classNames.Contains($"{baseClass}{EntityClass}"))
            {
                return $"{baseClass}{EntityClass}";
            }

            return string.IsNullOrEmpty(baseClass) ? ModificationTrackedClassName : baseClass;
        }

        private MethodDeclarationSyntax GenerateMethod(string methodSignature)
        {
            return SyntaxFactory.MethodDeclaration(
                        SyntaxFactory.List<AttributeListSyntax>(),
                        SyntaxFactory.TokenList(
                            SyntaxFactory.Token(SyntaxKind.PublicKeyword)
                              .WithLeadingWhiteSpaceTrivia()
                              .WithTrailingWhiteSpaceTrivia()
                              .WithLeadingTabTrivia(2)),
                        SyntaxFactory.ParseName(methodSignature),
                        null,
                        SyntaxFactory.Identifier(""),
                        null,
                        SyntaxFactory.ParameterList(),
                        SyntaxFactory.List<TypeParameterConstraintClauseSyntax>(),
                        SyntaxFactory.Block(
                            SyntaxFactory
                                .ParseStatement("")  
                                .WithTrailingTrivia(
                                    new SyntaxTriviaList()
                                    .Add(TriviaHelper.NewLineTrivia)
                                    .Add(TriviaHelper.TabTrivia)
                                    .Add(TriviaHelper.TabTrivia)))
                            .WithLeadingTrivia(
                                new SyntaxTriviaList()
                                    .Add(TriviaHelper.NewLineTrivia)
                                    .Add(TriviaHelper.TabTrivia)
                                    .Add(TriviaHelper.TabTrivia))                             
                            .WithTrailingNewLineTrivia(),
                        null);
        }
    }
}