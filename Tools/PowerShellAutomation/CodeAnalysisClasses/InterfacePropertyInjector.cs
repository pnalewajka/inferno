﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using PowerShellAutomation.Enums;

namespace PowerShellAutomation.CodeAnalysisClasses
{
    public class InterfacePropertyInjector : PropertyInjector
    {
        public InterfacePropertyInjector(string propertyType, string propertyName, AccessModifierEnum getAccessorAccessModifier,
                                        AccessModifierEnum setAccessorAccessModifier, string[] namespaces = null)
            : base(propertyType, propertyName, getAccessorAccessModifier, setAccessorAccessModifier, namespaces)
        {
        }

        public override SyntaxNode VisitInterfaceDeclaration(InterfaceDeclarationSyntax node)
        {
            var newMembers = InjectPropertyIntoDeclarationSyntax(node);

            return base.VisitInterfaceDeclaration(node.WithMembers(newMembers));
        }

        protected override SyntaxTokenList GenerateAccessModifier()
        {
            return new SyntaxTokenList();
        }
    }
}
