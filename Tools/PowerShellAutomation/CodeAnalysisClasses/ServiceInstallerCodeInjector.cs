﻿namespace PowerShellAutomation.CodeAnalysisClasses
{
    public class ServiceInstallerCodeInjector
    {
        public string GetCode(string classCode, string projectName)
        {
            var injector = new ServiceInstallerInjector(projectName);
            var resultCode = injector.GenerateCode(classCode);

            return resultCode;
        }
    }
}
