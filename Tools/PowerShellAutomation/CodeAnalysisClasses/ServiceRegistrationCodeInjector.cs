﻿namespace PowerShellAutomation.CodeAnalysisClasses
{
    public class ServiceRegistrationCodeInjector
    {
        public string GetCode(string classCode, string interfaceName, string className)
        {
            var injector = new ServiceRegistrationInjector(interfaceName, className);
            var resultCode = injector.GenerateCode(classCode);

            return resultCode;
        }
    }
}
