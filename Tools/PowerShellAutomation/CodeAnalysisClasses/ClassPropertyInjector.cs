﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using PowerShellAutomation.Enums;

namespace PowerShellAutomation.CodeAnalysisClasses
{
    public class ClassPropertyInjector : PropertyInjector
    {
        public ClassPropertyInjector(string propertyType, string propertyName, AccessModifierEnum getAccessorAccessModifier,
                                    AccessModifierEnum setAccessorAccessModifier, string[] namespaces)
            : base(propertyType, propertyName, getAccessorAccessModifier, setAccessorAccessModifier, namespaces)
        {
        }

        public override SyntaxNode VisitClassDeclaration(ClassDeclarationSyntax node)
        {
            var newMembers = InjectPropertyIntoDeclarationSyntax(node);

            return base.VisitClassDeclaration(node.WithMembers(newMembers));
        }
    }
}
