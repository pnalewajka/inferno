﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using PowerShellAutomation.Extensions;
using PowerShellAutomation.Helpers;

namespace PowerShellAutomation.CodeAnalysisClasses
{
    public class ServiceRegistrationInjector : StatementInjector
    {
        private const string _methodName = "Install";
        private const string _containerTypeName = "containerType";
        private const string _containerType = "ContainerType.WebApp";

        private readonly string _interfaceName;
        private readonly string _className;

        public ServiceRegistrationInjector(string interfaceName, string className)
            : base(_methodName)
        {
            _interfaceName = interfaceName;
            _className = className;
        }

        protected override BlockSyntax InjectStatement(BlockSyntax body)
        {
            var parentTrivia = body.Parent.GetLeadingTrivia();
            var leadingTrivia = parentTrivia.InsertRange(0, new[] { TriviaHelper.NewLineTrivia, TriviaHelper.TabTrivia });

            var registrationString = NamingConventionHelper.GetServiceRegistration(_interfaceName, _className);
            var syntax = SyntaxFactory.IfStatement(
                SyntaxFactory.BinaryExpression
                    (
                        SyntaxKind.EqualsExpression,
                        SyntaxFactory.IdentifierName(_containerTypeName).WithTrailingWhiteSpaceTrivia(),
                        SyntaxFactory.IdentifierName(_containerType).WithLeadingWhiteSpaceTrivia()
                    )
                    .WithOperatorToken(SyntaxFactory.Token(SyntaxKind.EqualsEqualsToken)),
                    SyntaxFactory.Block
                    (
                        SyntaxFactory.Token(SyntaxKind.OpenBraceToken).WithLeadingTrivia(leadingTrivia),
                        SyntaxFactory.List(new[]
                        {
                            SyntaxFactory.ParseStatement(registrationString).WithLeadingTrivia(leadingTrivia.Add(TriviaHelper.TabTrivia))
                        }),
                        SyntaxFactory.Token(SyntaxKind.CloseBraceToken).WithLeadingTrivia(leadingTrivia).WithTrailingNewLineTrivia()
                    )
                )
                .WithLeadingTrivia(parentTrivia.Add(TriviaHelper.TabTrivia));

            var result = body.AddStatements(syntax);

            return result;
        }
    }
}
