﻿using Microsoft.CodeAnalysis.CSharp;
using PowerShellAutomation.Enums;
using PowerShellAutomation.Helpers;

namespace PowerShellAutomation.CodeAnalysisClasses
{
    public class DbSetPropertyCodeInjector
    {
        public string GetCode(string classCode, string area, string entityName)
        {
            const string genericTypeNamespace = "System.Data.Entity";
            const string genericType = "DbSet";

            var entityNamespace = NamingConventionHelper.GetEntitiesModuleNamespace(area);
            var namespaces = new[] { genericTypeNamespace, entityNamespace };
            var propertyType = $"{genericType}<{entityName}>";
            var propertyName = NamingConventionHelper.GetPluralForm(entityName);

            var injector = new ClassPropertyInjector(propertyType, propertyName, AccessModifierEnum.Public,
                                                    AccessModifierEnum.Public, namespaces);
            var result = injector.GenerateCode(classCode);

            return result;
        }
    }
}
