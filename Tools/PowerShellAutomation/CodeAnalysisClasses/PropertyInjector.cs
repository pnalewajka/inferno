﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using PowerShellAutomation.Enums;
using PowerShellAutomation.Extensions;
using PowerShellAutomation.Helpers;

namespace PowerShellAutomation.CodeAnalysisClasses
{
    public abstract class PropertyInjector : CodeInjector
    {
        private readonly string _propertyType;
        private readonly string _propertyName;

        private readonly AccessModifierEnum _getAccessorAccessModifier;
        private readonly AccessModifierEnum _setAccessorAccessModifier;

        public PropertyInjector(string propertyType, string propertyName, AccessModifierEnum getAccessorAccessModifier,
            AccessModifierEnum setAccessorAccessModifier, string[] namespaces = null)
            : base(namespaces)
        {
            _propertyType = propertyType;
            _propertyName = propertyName;
            _getAccessorAccessModifier = getAccessorAccessModifier;
            _setAccessorAccessModifier = setAccessorAccessModifier;
        }

        protected SyntaxList<MemberDeclarationSyntax> InjectPropertyIntoDeclarationSyntax(TypeDeclarationSyntax node)
        {
            var modifiers = GenerateAccessModifier();
            var typeSyntax = GenerateTypeSyntax();
            var accessorListSyntax = GenerateAccessorListSyntax();
            var properties = node.Members.Where(m => m.Kind() == SyntaxKind.PropertyDeclaration);

            if (properties.All(member => ((PropertyDeclarationSyntax)member).Identifier.ToString() != _propertyName))
            {
                var newProperty = SyntaxFactory.PropertyDeclaration(typeSyntax.WithTrailingWhiteSpaceTrivia(), _propertyName)
                .WithTrailingWhiteSpaceTrivia()
                .WithModifiers(modifiers)
                .WithAccessorList(accessorListSyntax)
                .WithLeadingTabTrivia(2)
                .WithTrailingNewLineTrivia();

                int index = node.Members.LastIndexOf(member => member.Kind() == SyntaxKind.PropertyDeclaration);

                return node.Members.Insert(index + 1, newProperty);
            }

            return node.Members;
        }

        protected virtual SyntaxTokenList GenerateAccessModifier()
        {
            var propertyModifier = AccessModifierHelper.GetMaxAccessibility(_setAccessorAccessModifier, _getAccessorAccessModifier);

            return SyntaxFactory.TokenList(new[] { SyntaxFactory.Token(propertyModifier.ToSyntaxKindModifier())
                                                                    .WithTrailingWhiteSpaceTrivia()});
        }

        private TypeSyntax GenerateTypeSyntax()
        {
            var typeSyntax = SyntaxFactory.ParseTypeName(_propertyType);

            return typeSyntax;
        }

        private AccessorListSyntax GenerateAccessorListSyntax()
        {
            var accessors = new List<AccessorDeclarationSyntax>();

            var getAccessor = CreateAccessor(SyntaxKind.GetAccessorDeclaration, _getAccessorAccessModifier);
            var setAccessor = CreateAccessor(SyntaxKind.SetAccessorDeclaration, _setAccessorAccessModifier);

            if (setAccessor != null)
            {
                setAccessor = setAccessor.WithTrailingWhiteSpaceTrivia();
                accessors.Add(getAccessor);
                accessors.Add(setAccessor);
            }
            else
            {
                getAccessor = getAccessor.WithTrailingWhiteSpaceTrivia();
                accessors.Add(getAccessor);
            }

            return SyntaxFactory.AccessorList(SyntaxFactory.List(accessors));
        }

        private AccessorDeclarationSyntax CreateAccessor(SyntaxKind accessorType, AccessModifierEnum accessModifier)
        {
            if (accessModifier != AccessModifierEnum.None)
            {
                AccessorDeclarationSyntax accesorDeclaration = SyntaxFactory.AccessorDeclaration(accessorType)
                .WithSemicolonToken(SyntaxFactory.Token(SyntaxKind.SemicolonToken))
                .WithLeadingWhiteSpaceTrivia();

                if (accessModifier != AccessModifierEnum.Public)
                {
                    accesorDeclaration = accesorDeclaration.WithModifiers(SyntaxFactory.TokenList(SyntaxFactory.Token(
                                            accessModifier.ToSyntaxKindModifier())
                                            .WithLeadingWhiteSpaceTrivia()));
                }

                return accesorDeclaration;
            }

            return null;
        }
    }
}
