﻿using System.Collections.Generic;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using PowerShellAutomation.Helpers;
using PowerShellAutomation.Extensions;

namespace PowerShellAutomation.CodeAnalysisClasses
{
    public class DbScopeCodeBuilder
    {
        public string GenerateCode(string module)
        {
            const string mainNamespace = "Smt.Atomic.Data.Repositories.Scopes";
            const string repositoryInterfacesNamespace = "Smt.Atomic.Data.Repositories.Interfaces";
            const string scopeBaseInterfaceName = "IDbScope";

            var scopeName = NamingConventionHelper.GetDbScopeInterfaceName(module);
            var areaNamespace = NamingConventionHelper.GetEntitiesModuleNamespace(module);

            var usingList = new List<UsingDirectiveSyntax>();

            usingList.Add(
                SyntaxFactory.UsingDirective
                    (
                        SyntaxFactory.ParseName(areaNamespace)
                            .WithLeadingWhiteSpaceTrivia()
                    )
                    .WithTrailingNewLineTrivia());

            usingList.Add(
                SyntaxFactory.UsingDirective
                    (
                        SyntaxFactory.ParseName(repositoryInterfacesNamespace)
                            .WithLeadingWhiteSpaceTrivia()
                    )
                    .WithTrailingNewLineTrivia(2));

            var identifierNameSyntax = SyntaxFactory.IdentifierName(mainNamespace);
            var namespaceDeclarationSyntax = SyntaxFactory.NamespaceDeclaration
                (
                    identifierNameSyntax
                        .WithLeadingWhiteSpaceTrivia()
                        .WithTrailingNewLineTrivia()
                )
                .WithOpenBraceToken
                (
                    SyntaxFactory.Token(SyntaxKind.OpenBraceToken).WithTrailingNewLineTrivia()
                )
                .WithCloseBraceToken
                (
                    SyntaxFactory.Token(SyntaxKind.CloseBraceToken)
                );

            var baseListSyntax = SyntaxFactory.BaseList(SyntaxFactory.SeparatedList<BaseTypeSyntax>(new[]
            {
                SyntaxFactory.SimpleBaseType(
                    SyntaxFactory.IdentifierName(scopeBaseInterfaceName)).WithLeadingWhiteSpaceTrivia()
                    .WithTrailingNewLineTrivia()
            }));

            var scopeNameSyntaxToken = SyntaxFactory.Identifier(scopeName);

            var interfaceDeclarationSyntax = SyntaxFactory
                .InterfaceDeclaration(scopeNameSyntaxToken.WithLeadingWhiteSpaceTrivia().WithTrailingWhiteSpaceTrivia())
                .WithModifiers(SyntaxFactory.TokenList(SyntaxFactory.Token(SyntaxKind.PublicKeyword)
                    .WithTrailingWhiteSpaceTrivia()
                    .WithLeadingTabTrivia()
                    )
                )
                .WithTrailingNewLineTrivia()
                .WithBaseList(baseListSyntax)
                .WithOpenBraceToken
                (
                    SyntaxFactory.Token(SyntaxKind.OpenBraceToken)
                        .WithTrailingNewLineTrivia()
                        .WithLeadingTabTrivia()
                )
                .WithCloseBraceToken
                (
                    SyntaxFactory.Token(SyntaxKind.CloseBraceToken)
                        .WithLeadingTabTrivia()
                        .WithTrailingNewLineTrivia()
                );

            namespaceDeclarationSyntax =
                namespaceDeclarationSyntax.AddMembers(interfaceDeclarationSyntax);

            var compilationUnit = SyntaxFactory.CompilationUnit()
                .AddMembers(namespaceDeclarationSyntax)
                .WithUsings(SyntaxFactory.List(usingList));

            return compilationUnit.ToFullString();
        }
    }
}