﻿using System.Linq;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.CSharp;
using PowerShellAutomation.Helpers;
using PowerShellAutomation.Extensions;


namespace PowerShellAutomation.CodeAnalysisClasses
{
    public class ServiceInstallerInjector : StatementInjector
    {
        private const string _methodName = "GetInstallers";

        private readonly string _projectName;

        public ServiceInstallerInjector(string projectName)
            : base(_methodName)
        {
            _projectName = projectName;
        }

        protected override BlockSyntax InjectStatement(BlockSyntax body)
        {
            var registrationString = NamingConventionHelper.GetServiceInstallerRegistration(_projectName);

            if (body.Statements.All(s => s.ToString() != registrationString))
            {
                var syntax = SyntaxFactory.ParseStatement(registrationString)
                    .WithLeadingTabTrivia(3)
                    .WithTrailingNewLineTrivia();
                var index = body.Statements.Count > 0 ? body.Statements.Count - 1 : 0;
                var statements = body.Statements.Insert(index, syntax);
                body = body.WithStatements(statements);
            }
            
            return body;
        }
    }
}
