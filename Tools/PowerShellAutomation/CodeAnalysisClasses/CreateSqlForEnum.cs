﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using PowerShellAutomation.Commands;
using PowerShellAutomation.SupportClasses;
using Smt.Atomic.CrossCutting.Common.Annotations;
using Smt.Atomic.CrossCutting.Common.Exceptions;
using Smt.Atomic.CrossCutting.Common.Extensions;

namespace PowerShellAutomation.CodeAnalysisClasses
{
    public class CreateSqlForEnum
    {
        private CreateSqlForEnumCommand _commandParameters;
        private Type _enumType;

        #region Templates
        private const string MergeStatementTemplate = @"MERGE INTO {0}  AS target
USING ( 
VALUES {1} ) AS source ([Id], [Name], [Description])
ON target.[Id] = source.[Id]{2}WHEN MATCHED THEN 
    UPDATE SET [Description] = source.[Description], [Name] = source.[Name]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id], [Name], [Description]) VALUES (source.Id, source.Name, source.Description);
";

        private const string MergeStatementWithoutDescriptionTemplate = @"MERGE INTO {0}  AS target
USING ( 
VALUES {1} ) AS source ([Id], [Name])
ON target.[Id] = source.[Id]{2}WHEN MATCHED THEN 
    UPDATE SET [Name] = source.[Name]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id], [Name]) VALUES (source.Id, source.Name);
";

        private const string NoMatchSectionDeleteStatement = @"
WHEN NOT MATCHED BY SOURCE THEN
    DELETE
";

        private const string CreateTableTemplate = @"CREATE TABLE {0} (
    Id [{1}] PRIMARY KEY NOT NULL,
    Name [VARCHAR](50) NOT NULL,
    Description [NVARCHAR](250) NULL
)
";
        private const string SetIdentityOnTemplate = @"SET IDENTITY_INSERT {0} ON
GO
";

        private const string SetIdentityOffTemplate = @"GO
SET IDENTITY_INSERT {0} OFF
";

        private const string DropTableTemplate = @"DROP TABLE {0}
";

        private const string SourceRowTemplate = @"({0},'{1}','{2}')";
        private const string SourceRowWithoutDescriptionTemplate = @"({0},'{1}')";

        #endregion

        private IEnumerable<object> GetEnumValues(Type enumType)
        {
            return Enum.GetValues(enumType).Cast<object>().ToArray();
        }


        private string GetEnumTableNameByConvention()
        {
            var tableNameFormat = "[{0}].[{1}Enum]";
            var targetEnumTableName = _enumType.Name;
            if (!_enumType.IsEnum)
            {
                throw new GenericParameterException(_enumType, typeof (Enum));
            }

            if (_commandParameters.OverrideName)
            {
                tableNameFormat = "[{0}].[{1}]";

                if (!string.IsNullOrEmpty(_commandParameters.OverrideNameValue))
                {
                    targetEnumTableName = _commandParameters.OverrideNameValue;
                }
            }

            return string.Format(tableNameFormat, _commandParameters.Schema, targetEnumTableName);
        }

        /// <summary>
        /// Create table for specific enum, with constant structure
        /// Id of underlying type
        /// Name 
        /// Description 
        /// </summary>
        private  string CreateTableForEnum() 
        {
            if (!_enumType.IsEnum)
            {
                throw new GenericParameterException(_enumType, typeof(Enum));
            }

            return string.Format(CreateTableTemplate, GetEnumTableNameByConvention(), GetIdColumnForUnderlyingEnumType());
        }

        private string DropTableForEnum()
        {

            return string.Format(DropTableTemplate, GetEnumTableNameByConvention());
        }

        private string GetIdColumnForUnderlyingEnumType()
        {
            if (_enumType.GetEnumUnderlyingType() == typeof(int))
            {
                return "INT";
            }

            if (_enumType.GetEnumUnderlyingType() == typeof(long))
            {
                return "BIGINT";
            }

            throw new GenericParameterException("Underlying type of enum is not supported. Should be int or long");
        }

        private void AppendEnumLine(StringBuilder enumValuesBuilder, object enumValue)
        {
            if (enumValuesBuilder.Length > 0)
            {
                enumValuesBuilder.AppendLine(",");
            }

            //support for int underlying type
            var description = ((Enum)enumValue).GetDescription();

            if (description == null)
            {
                var enumValueName = enumValue.ToString();
                throw new Exception($"Description attribute for '{enumValueName}' enum member is missing");
            }

            var enumDescription = description.Replace("'", "''");

            var rowTemplate = _commandParameters.OmitDescriptions
                ? SourceRowWithoutDescriptionTemplate
                : SourceRowTemplate;

            if (_enumType.GetEnumUnderlyingType() == typeof(int))
            {
                //boxing and unboxing here, 2-times, but it is required as there doesn't exist enum type generic constraint
                // ReSharper disable once PossibleInvalidCastException
                enumValuesBuilder.AppendFormat(rowTemplate, (int)enumValue, enumValue, enumDescription);
            }
            
            //support for long underlying type
            if (_enumType.GetEnumUnderlyingType() == typeof(long))
            {
                //boxing and unboxing here, 2-times, but it is required as there doesn't exist enum type generic constraint
                // ReSharper disable once PossibleInvalidCastException
                enumValuesBuilder.AppendFormat(rowTemplate, (long)enumValue, enumValue, enumDescription);
            }
        }

        /// <summary>
        /// Merge (insert or update or delete) enum table contents with following values from enum 
        /// Id
        /// Name - enum option name
        /// Description - value from DescriptionAttribute of specific enum option
        /// </summary>
        private string FeedTableWithEnumValues() 
        {
            var enumTable = GetEnumTableNameByConvention();
            var enumValuesBuilder = new StringBuilder();

            var currentThreadCulture = Thread.CurrentThread.CurrentUICulture;

            //Change local for localization of descriptions
            if (!string.IsNullOrEmpty(_commandParameters.DescriptionLocale))
            {
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(_commandParameters.DescriptionLocale);
            }

            foreach (var enumValue in GetEnumValues(_enumType))
            {
                AppendEnumLine(enumValuesBuilder, enumValue);
            }

            var templateForUse = _commandParameters.OmitDescriptions
                ? MergeStatementWithoutDescriptionTemplate
                : MergeStatementTemplate;

            //revert to previously used culture
            Thread.CurrentThread.CurrentUICulture = currentThreadCulture;

            var noMatchSection = _commandParameters.DoNotMaintainRemovedItems ? Environment.NewLine : NoMatchSectionDeleteStatement;

            var code = new StringBuilder();
            code.AppendFormat(SetIdentityOnTemplate, enumTable);
            code.AppendFormat(templateForUse, enumTable, enumValuesBuilder, noMatchSection);
            code.AppendFormat(SetIdentityOffTemplate, enumTable);
            return code.ToString();
        }

        public SqlEnumCode GetCode([NotNull] CreateSqlForEnumCommand commandParameters)
        {
            if (commandParameters == null)
            {
                throw new ArgumentNullException(nameof(commandParameters));
            }

            _commandParameters = commandParameters;

            AssemblyResolver.AddAssemblyLocation(commandParameters.AssemblyPath);
            var assembly = Assembly.LoadFrom(commandParameters.AssemblyPath);
            _enumType = assembly.GetType(commandParameters.FullClassName);

            if (!_enumType.IsEnum)
            {
                throw new GenericParameterException(_enumType, typeof(Enum));
            }

            return new SqlEnumCode
            {
                CreateTableCode = CreateTableForEnum(),
                DataCode = FeedTableWithEnumValues(),
                DropTableCode = DropTableForEnum()
            };
        }

        public class SqlEnumCode
        {
            public string CreateTableCode { get; set; }
            public string DataCode { get; set; }
            public string DropTableCode { get; set; }
        }

    }
}
