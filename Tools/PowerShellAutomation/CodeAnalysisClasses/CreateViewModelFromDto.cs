﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using PowerShellAutomation.Helpers;
using PowerShellAutomation.SupportClasses;
using Smt.Atomic.CrossCutting.Common.Helpers;
using Microsoft.CodeAnalysis.CSharp;

namespace PowerShellAutomation.CodeAnalysisClasses
{
    public class CreateViewModelFromDto : CreateClassFromClass
    {
        private const string CardIndexAttributeNamespace = "Smt.Atomic.Presentation.Renderers.CardIndex.Attributes";
        private const string BootstrapAttributeNamespace = "Smt.Atomic.Presentation.Renderers.Bootstrap.Attributes";

        private readonly string[] _propertiesWithoutDisplayAttribute = { "Id", "Timestamp" };
        private readonly string[] _propertiesToHide = { "Id", "ImpersonatedById", "ModifiedById", "ModifiedOn", "Timestamp" };
        private readonly string _fullDtoClassName;
        private readonly string _viewModelClassName;
        private readonly string _toViewModelClassMappingName;
        private readonly string _fromViewModelClassMappingName;
        private readonly string _targetNamespace;
        private readonly string _dtoAssemblyPath;
        private readonly string _resourceName;
        private readonly string _area;
        private readonly bool _isForCardIndex;
        private string _resourceValues;

        public class GeneratedCodeForViewModel : GeneratedCode
        {
            public string ResourceValues { get; set; }
        }

        public CreateViewModelFromDto(string area,
            string fullDtoClassName,
            string viewModelClassName,
            string toViewModelClassMappingName,
            string fromViewModelClassMappingName,
            string targetNamespace,
            string dtoAssemblyPath,
            string resourceName,
            bool isForCardIndex)
        {
            _area = area;
            _fullDtoClassName = fullDtoClassName;
            _viewModelClassName = viewModelClassName;
            _toViewModelClassMappingName = toViewModelClassMappingName;
            _fromViewModelClassMappingName = fromViewModelClassMappingName;
            _targetNamespace = targetNamespace;
            _dtoAssemblyPath = dtoAssemblyPath;
            _resourceName = resourceName;
            _isForCardIndex = isForCardIndex;
        }

        public GeneratedCodeForViewModel GenerateCode()
        {
            AssemblyResolver.AddAssemblyLocation(_dtoAssemblyPath);
            var assembly = Assembly.LoadFrom(_dtoAssemblyPath);
            var result = new GeneratedCodeForViewModel();
            var dtoType = assembly.GetType(_fullDtoClassName);
            var info = dtoType.GetProperties().Select(p => new PropertyTypeAndName
            {
                IsVirtual = PropertyHelper.IsVirtual(p) == true,
                Name = p.Name,
                Type = CSharpAmbianceHelper.GetTypeName(p.PropertyType),
                Namespace = p.PropertyType.Namespace
            }).ToList();

            var properties = info.Where(p => !string.IsNullOrEmpty(p.Namespace) && p.Type.StartsWith(p.Namespace));

            foreach (var property in properties)
            {
                property.Type = property.Type.Replace(property.Namespace + ".", string.Empty);
            }

            result.Code = GetTargetCode(info, _viewModelClassName, Enumerable.Empty<string>(), _targetNamespace);
            result.ToTargetMappingCode = GetMappingCode(
                info,
                dtoType.Name,
                _viewModelClassName,
                _targetNamespace,
                _toViewModelClassMappingName,
                Enumerable.Empty<string>(),
                dtoType.Namespace,
                "d");

            var ignoreProperties = new[]
            {
                "ImpersonatedById",
                "ModifiedById",
                "ModifiedOn"
            };

            result.FromTargetMappingCode = GetMappingCode(
                info,
                _viewModelClassName,
                dtoType.Name,
                _targetNamespace,
                _fromViewModelClassMappingName,
                ignoreProperties,
                dtoType.Namespace,
                "v"
                );

            result.ResourceValues = _resourceValues;

            return result;
        }

        protected override void AddNamespaces(HashSet<string> namespaces)
        {
            if (_isForCardIndex)
            {
                namespaces.Add(CardIndexAttributeNamespace);
            }

            if (!string.IsNullOrEmpty(_resourceName))
            {
                string resourcesNamespace = Helpers.NamingConventionHelper.GetResourceNamespaceForArea(_area);

                namespaces.Add(BootstrapAttributeNamespace);
                namespaces.Add(resourcesNamespace);
            }

        }

        protected override PropertyDeclarationSyntax AddPropertyAttribute(PropertyDeclarationSyntax propertyDeclarationSyntax, PropertyTypeAndName property)
        {
            var attributeSyntaxList = new List<AttributeSyntax>();

            if (_isForCardIndex && _propertiesToHide.Contains(property.Name))
            {
                var visibilityScopeArgument = SyntaxFactory.AttributeArgument(SyntaxFactory.ParseExpression("VisibilityScope.None"));

                attributeSyntaxList.Add(SyntaxFactory.Attribute
                (
                    SyntaxFactory.IdentifierName("Visibility"),
                    SyntaxFactory.AttributeArgumentList(SyntaxFactory.SeparatedList(new[] { visibilityScopeArgument }))
                ));
            }

            if (!string.IsNullOrEmpty(_resourceName) && !_propertiesWithoutDisplayAttribute.Contains(property.Name))
            {
                var entityName = Helpers.NamingConventionHelper.GetEntityNameFromViewModel(_viewModelClassName);
                var labelName = Helpers.NamingConventionHelper.GetLabelNameForPropertyInQuotes(entityName, property.Name);

                var labelNameArgument = SyntaxFactory.AttributeArgument(SyntaxFactory.ParseExpression(labelName));
                var resourceArgument = SyntaxFactory.AttributeArgument
                    (
                        SyntaxFactory.TypeOfExpression(SyntaxFactory.IdentifierName(_resourceName))
                        .WithKeyword(SyntaxFactory.Token(SyntaxKind.TypeOfKeyword))
                    );

                attributeSyntaxList.Add(SyntaxFactory.Attribute
                (
                    SyntaxFactory.IdentifierName("DisplayNameLocalized"),
                    SyntaxFactory.AttributeArgumentList(SyntaxFactory.SeparatedList(
                        new[] { labelNameArgument, resourceArgument }))
                ));

                AddResource(labelName, property.Name);
            }

            if (attributeSyntaxList.Any())
            {
                return propertyDeclarationSyntax.WithAttributeLists(SyntaxFactory.List(new[] { SyntaxFactory.AttributeList(SyntaxFactory.SeparatedList(attributeSyntaxList)) }));
            }

            return base.AddPropertyAttribute(propertyDeclarationSyntax, property);
        }

        private void AddResource(string label, string propertyName)
        {
            string value = Smt.Atomic.CrossCutting.Common.Helpers.NamingConventionHelper.ConvertPascalCaseToLabel(propertyName);
            string labelWithoutQuotes = label.Replace("\"", "");
            _resourceValues += $"{labelWithoutQuotes}-{value}\r\n";
        }
    }
}