﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace PowerShellAutomation.CodeAnalysisClasses
{
    public abstract class StatementInjector : CodeInjector
    {
        private readonly string _methodName;

        public StatementInjector(string methodName, string[] namespaces = null)
            : base(namespaces)
        {
            _methodName = methodName;
        }

        public override SyntaxNode VisitMethodDeclaration(MethodDeclarationSyntax node)
        {
            if (node.Identifier.Text == _methodName)
            {
                var body = InjectStatement(node.Body);
                node = node.WithBody(body);
            }

            return base.VisitMethodDeclaration(node);
        }

        protected abstract BlockSyntax InjectStatement(BlockSyntax body);
    }

}
