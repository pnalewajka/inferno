﻿using System.Collections.Generic;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using PowerShellAutomation.Helpers;
using PowerShellAutomation.Extensions;

namespace PowerShellAutomation.CodeAnalysisClasses
{
    public class CreateCardIndexDataService
    {
        private const string _commonInterfacesNamespace = "Smt.Atomic.Business.Common.Interfaces";

        public string GenerateServiceClassCode(string module, string entity)
        {
            const string commonServicesNamespace = "Smt.Atomic.Business.Common.Services";
            const string scopesNamespace = "Smt.Atomic.Data.Repositories.Scopes";
            const string ctorParameterName = "dependencies";
            const string ctorGenericTypeParameter = "ICardIndexServiceDependencies";
            const string baseClassName = "CardIndexDataService";

            var moduleNamespace = NamingConventionHelper.GetEntitiesModuleNamespace(module);
            var moduleDtoNamespace = NamingConventionHelper.GetModuleDtoNamespace(module);
            var moduleIntrefacesNamespace = NamingConventionHelper.GetModuleInterfacesNamespace(module);
            var mainNamespace = NamingConventionHelper.GetModuleServicesNamespace(module);
            var className = NamingConventionHelper.GetCardIndexDataServiceClassName(entity);
            var baseInterfaceName = NamingConventionHelper.GetCardIndexDataServiceInterfaceName(entity);
            var entityDto = NamingConventionHelper.GetDtoName(entity);
            var moduleDbScopeName = NamingConventionHelper.GetDbScopeInterfaceName(module);

            var usingList = new List<UsingDirectiveSyntax>
            {
                SyntaxFactory.UsingDirective
                    (
                        SyntaxFactory.ParseName(_commonInterfacesNamespace)
                            .WithLeadingWhiteSpaceTrivia()
                    )
                    .WithTrailingNewLineTrivia(),
                SyntaxFactory.UsingDirective
                    (
                        SyntaxFactory.ParseName(commonServicesNamespace)
                            .WithLeadingWhiteSpaceTrivia()
                    )
                    .WithTrailingNewLineTrivia(),
                SyntaxFactory.UsingDirective
                    (
                        SyntaxFactory.ParseName(moduleDtoNamespace)
                            .WithLeadingWhiteSpaceTrivia()
                    )
                    .WithTrailingNewLineTrivia(),
                SyntaxFactory.UsingDirective
                    (
                        SyntaxFactory.ParseName(moduleIntrefacesNamespace)
                            .WithLeadingWhiteSpaceTrivia()
                    )
                    .WithTrailingNewLineTrivia(),
                SyntaxFactory.UsingDirective
                    (
                        SyntaxFactory.ParseName(moduleNamespace)
                            .WithLeadingWhiteSpaceTrivia()
                    )
                    .WithTrailingNewLineTrivia(),
                    SyntaxFactory.UsingDirective
                    (
                        SyntaxFactory.ParseName(scopesNamespace)
                            .WithLeadingWhiteSpaceTrivia()
                    )
                    .WithTrailingNewLineTrivia(2)
            };

            var namespaceDeclarationSyntax = SyntaxFactory.NamespaceDeclaration
                    (
                        SyntaxFactory.IdentifierName(mainNamespace)
                            .WithLeadingWhiteSpaceTrivia()
                            .WithTrailingNewLineTrivia()
                    )
                    .WithOpenBraceToken
                    (
                        SyntaxFactory.Token(SyntaxKind.OpenBraceToken).WithTrailingNewLineTrivia()
                    )
                    .WithCloseBraceToken
                    (
                        SyntaxFactory.Token(SyntaxKind.CloseBraceToken)
                    );

            var classNameSyntaxToken = SyntaxFactory.Identifier(className);
            var baseTypeList =
                SyntaxFactory.BaseList(SyntaxFactory.SeparatedList<BaseTypeSyntax>(new SyntaxNodeOrToken[]
                {
                    SyntaxFactory.SimpleBaseType
                        (
                            SyntaxFactory.GenericName(SyntaxFactory.Identifier(baseClassName))
                                .WithTypeArgumentList(
                                    SyntaxFactory.TypeArgumentList(
                                        SyntaxFactory.SeparatedList<TypeSyntax>(new SyntaxNodeOrToken[]
                                        {
                                            SyntaxFactory.IdentifierName(entityDto),
                                            SyntaxFactory.Token(SyntaxKind.CommaToken).WithTrailingWhiteSpaceTrivia(),
                                            SyntaxFactory.IdentifierName(entity),
                                            SyntaxFactory.Token(SyntaxKind.CommaToken).WithTrailingWhiteSpaceTrivia(),
                                            SyntaxFactory.IdentifierName(moduleDbScopeName)
                                        })
                                    )
                                )
                                .WithLeadingWhiteSpaceTrivia()
                        ),
                    SyntaxFactory.Token(SyntaxKind.CommaToken).WithTrailingWhiteSpaceTrivia(),
                    SyntaxFactory.SimpleBaseType(SyntaxFactory.ParseTypeName(baseInterfaceName))
                }));

            var ctorParameterList =
                            SyntaxFactory.ParameterList(
                                SyntaxFactory.SeparatedList(new[] { SyntaxFactory.Parameter(
                                    SyntaxFactory.Identifier(ctorParameterName).WithLeadingWhiteSpaceTrivia())
                                    .WithType(SyntaxFactory.GenericName(SyntaxFactory.Identifier(ctorGenericTypeParameter),
                                        SyntaxFactory.TypeArgumentList(
                                            SyntaxFactory.SeparatedList<TypeSyntax>(new [] {
                                                SyntaxFactory.IdentifierName(moduleDbScopeName)
                                            })
                                        )
                                    ))
                                })
                            );

            var ctorInitializer = SyntaxFactory.ConstructorInitializer(SyntaxKind.BaseConstructorInitializer,
                SyntaxFactory.ArgumentList(SyntaxFactory.SeparatedList(new[] {
                    SyntaxFactory.Argument(SyntaxFactory.IdentifierName(ctorParameterName))}))
                    .WithOpenParenToken(SyntaxFactory.Token(SyntaxKind.OpenParenToken))
                    .WithCloseParenToken(SyntaxFactory.Token(SyntaxKind.CloseParenToken)))
                .WithColonToken(SyntaxFactory.Token(SyntaxKind.ColonToken).WithTrailingWhiteSpaceTrivia())
                .WithThisOrBaseKeyword(SyntaxFactory.Token(SyntaxKind.BaseKeyword))
                .WithLeadingTabTrivia(3)
                .WithTrailingNewLineTrivia();

            var ctorDeclarationSyntax = SyntaxFactory
                .ConstructorDeclaration(classNameSyntaxToken.WithLeadingWhiteSpaceTrivia())
                .WithModifiers(SyntaxFactory.TokenList(SyntaxFactory.Token(SyntaxKind.PublicKeyword)))
                .WithParameterList(ctorParameterList)
                .WithLeadingTabTrivia(2)
                .WithTrailingNewLineTrivia()
                .WithInitializer(ctorInitializer)
                .WithBody(SyntaxFactory.Block()
                    .WithOpenBraceToken
                    (
                        SyntaxFactory.Token(SyntaxKind.OpenBraceToken)
                            .WithTrailingNewLineTrivia()
                            .WithLeadingTabTrivia(2)
                    )
                    .WithCloseBraceToken
                    (
                        SyntaxFactory.Token(SyntaxKind.CloseBraceToken)
                            .WithLeadingTabTrivia(2)
                            .WithTrailingNewLineTrivia()
                    ));

            var classDeclarationSyntax = SyntaxFactory
                .ClassDeclaration(classNameSyntaxToken.WithLeadingWhiteSpaceTrivia())
                .WithModifiers(SyntaxFactory.TokenList(SyntaxFactory.Token(SyntaxKind.PublicKeyword)
                    .WithTrailingWhiteSpaceTrivia()
                    .WithLeadingTabTrivia())
                )
                .WithBaseList(baseTypeList
                    .WithLeadingWhiteSpaceTrivia()
                    .WithTrailingNewLineTrivia()
                )
                .WithTrailingNewLineTrivia()
                .AddMembers(ctorDeclarationSyntax)
                .WithOpenBraceToken
                (
                    SyntaxFactory.Token(SyntaxKind.OpenBraceToken)
                        .WithTrailingNewLineTrivia()
                        .WithLeadingTabTrivia()
                )
                .WithCloseBraceToken
                (
                    SyntaxFactory.Token(SyntaxKind.CloseBraceToken)
                        .WithLeadingTabTrivia()
                        .WithTrailingNewLineTrivia()
                );

            namespaceDeclarationSyntax =
                namespaceDeclarationSyntax.AddMembers(classDeclarationSyntax);

            var compilationUnit = SyntaxFactory.CompilationUnit()
                .AddMembers(namespaceDeclarationSyntax)
                .WithUsings(SyntaxFactory.List(usingList));

            return compilationUnit.ToFullString();
        }

        public string GenerateServiceInterfaceCode(string module, string entity)
        {
            const string baseInterfaceName = "ICardIndexDataService";

            var entityDtoNamespace = NamingConventionHelper.GetModuleDtoNamespace(module);
            var mainNamespace = NamingConventionHelper.GetModuleInterfacesNamespace(module);
            var entityDto = NamingConventionHelper.GetDtoName(entity);
            var interfaceName = NamingConventionHelper.GetCardIndexDataServiceInterfaceName(entity);

            var usingList = new List<UsingDirectiveSyntax>
            {
                SyntaxFactory.UsingDirective
                    (
                        SyntaxFactory.ParseName(_commonInterfacesNamespace)
                            .WithLeadingWhiteSpaceTrivia()
                    )
                    .WithTrailingNewLineTrivia(),
                SyntaxFactory.UsingDirective
                    (
                        SyntaxFactory.ParseName(entityDtoNamespace)
                            .WithLeadingWhiteSpaceTrivia()
                    )
                    .WithTrailingNewLineTrivia(2)
            };

            var namespaceDeclarationSyntax = SyntaxFactory.NamespaceDeclaration
                    (
                        SyntaxFactory.IdentifierName(mainNamespace)
                            .WithLeadingWhiteSpaceTrivia()
                            .WithTrailingNewLineTrivia()
                    )
                    .WithOpenBraceToken
                    (
                        SyntaxFactory.Token(SyntaxKind.OpenBraceToken).WithTrailingNewLineTrivia()
                    )
                    .WithCloseBraceToken
                    (
                        SyntaxFactory.Token(SyntaxKind.CloseBraceToken).WithTrailingNewLineTrivia()
                    );

            var interfaceNameSyntaxToken = SyntaxFactory.Identifier(interfaceName);
            var baseInterface = SyntaxFactory.BaseList(SyntaxFactory.SeparatedList<BaseTypeSyntax>(
                new[] { SyntaxFactory.SimpleBaseType
                    (
                        SyntaxFactory.GenericName(SyntaxFactory.Identifier(baseInterfaceName),
                            SyntaxFactory.TypeArgumentList(SyntaxFactory.SeparatedList<TypeSyntax>(
                                new [] { SyntaxFactory.IdentifierName(entityDto)}
                            ))
                        )
                        .WithLeadingWhiteSpaceTrivia()
                    )}
                ));

            var interfaceDeclarationSyntax = SyntaxFactory
                .InterfaceDeclaration(interfaceNameSyntaxToken.WithLeadingWhiteSpaceTrivia())
                .WithModifiers(SyntaxFactory.TokenList(SyntaxFactory.Token(SyntaxKind.PublicKeyword)
                    .WithTrailingWhiteSpaceTrivia()
                    .WithLeadingTabTrivia()
                    )
                )
                .WithBaseList((baseInterface
                            .WithLeadingWhiteSpaceTrivia()
                            .WithTrailingWhiteSpaceTrivia()
                        )
                        .WithTrailingNewLineTrivia()
                )
                .WithTrailingNewLineTrivia()
                .WithOpenBraceToken
                (
                    SyntaxFactory.Token(SyntaxKind.OpenBraceToken)
                        .WithTrailingNewLineTrivia()
                        .WithLeadingTabTrivia()
                )
                .WithCloseBraceToken
                (
                    SyntaxFactory.Token(SyntaxKind.CloseBraceToken)
                        .WithLeadingTabTrivia()
                        .WithTrailingNewLineTrivia()
                );

            namespaceDeclarationSyntax =
                namespaceDeclarationSyntax.AddMembers(interfaceDeclarationSyntax);

            var compilationUnit = SyntaxFactory.CompilationUnit()
                .AddMembers(namespaceDeclarationSyntax)
                .WithUsings(SyntaxFactory.List(usingList));

            return compilationUnit.ToFullString();
        }
    }
}
