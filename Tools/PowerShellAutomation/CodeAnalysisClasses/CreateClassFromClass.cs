﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using PowerShellAutomation.Helpers;
using Smt.Atomic.Data.Entities.Interfaces;

namespace PowerShellAutomation.CodeAnalysisClasses
{
    public abstract class CreateClassFromClass
    {
        private static readonly IList<string> PropertiesOnTop = new[] { nameof(IEntity.Id) };

        private static int PropertySortOrder(PropertyTypeAndName property)
        {
            return PropertiesOnTop.Contains(property.Name) ?
                PropertiesOnTop.IndexOf(property.Name) :
                PropertiesOnTop.Count;
        }

        protected class PropertyTypeAndName
        {
            public string Type { get; set; }
            public string Name { get; set; }
            public string Namespace { get; set; }
            public bool IsVirtual { get; set; }
        }

        public class GeneratedCode
        {
            public string Code { get; set; }
            public string ToTargetMappingCode { get; set; }
            public string FromTargetMappingCode { get; set; }
        }

        protected IEnumerable<UsingDirectiveSyntax> GetUsings(IEnumerable<string> usingNamespacesList)
        {
            return usingNamespacesList
                .Distinct()
                .Select(usingNamespace => SyntaxFactory.UsingDirective(SyntaxFactory.ParseName(usingNamespace)))
                .OrderByDescending(x => x.Name.ToString().StartsWith($"{nameof(System)}."))
                .ThenBy(x => x.Name.ToString());
        }

        protected string GetTargetCode(
            List<PropertyTypeAndName> properties,
            string className,
            IEnumerable<string> ignoreProperties,
            string targetNamespace)
        {
            var usingNamespacesList = new HashSet<string>();
            var propertyList = new List<MemberDeclarationSyntax>();

            AddNamespaces(usingNamespacesList);

            var propertiesToConvert = properties
                .Where(p => !p.IsVirtual && !ignoreProperties.Contains(p.Name))
                .OrderBy(PropertySortOrder);

            foreach (var property in propertiesToConvert)
            {
                usingNamespacesList.Add(property.Namespace);
                var modifiers = new List<SyntaxToken> { SyntaxFactory.Token(SyntaxKind.PublicKeyword) };
                var typeSyntax = SyntaxFactory.ParseTypeName(property.Type);

                var getDeclaration = SyntaxFactory.AccessorDeclaration(SyntaxKind.GetAccessorDeclaration)
                    .WithSemicolonToken(SyntaxFactory.Token(SyntaxKind.SemicolonToken));

                var setDeclaration = SyntaxFactory.AccessorDeclaration(SyntaxKind.SetAccessorDeclaration)
                    .WithSemicolonToken(SyntaxFactory.Token(SyntaxKind.SemicolonToken));

                var accessorListSyntax = SyntaxFactory.AccessorList(SyntaxFactory.List(new[] { getDeclaration, setDeclaration }));

                var propertyDeclarationSyntax = SyntaxFactory.PropertyDeclaration(typeSyntax, property.Name)
                    .WithModifiers(SyntaxFactory.TokenList(modifiers))
                    .WithAccessorList(accessorListSyntax);

                propertyDeclarationSyntax = AddPropertyAttribute(propertyDeclarationSyntax, property);

                propertyList.Add(propertyDeclarationSyntax);
            }

            var usingsList = GetUsings(usingNamespacesList);
            var namespaceDeclaration = SyntaxFactory.NamespaceDeclaration(SyntaxFactory.IdentifierName(targetNamespace));
            var classDeclarationSyntax = SyntaxFactory.ClassDeclaration(SyntaxFactory.Identifier(className))
                .WithModifiers(SyntaxFactory.TokenList(SyntaxFactory.Token(SyntaxKind.PublicKeyword)))
                .WithMembers(SyntaxFactory.List(propertyList));

            namespaceDeclaration = namespaceDeclaration.AddMembers(classDeclarationSyntax);
            var compilationUnit = SyntaxFactory.CompilationUnit()
                .AddMembers(namespaceDeclaration.WithLeadingTrivia(SyntaxFactory.Comment(Environment.NewLine)))
                .WithUsings(SyntaxFactory.List(usingsList));

            return SyntaxTreeFormatter.ToString(compilationUnit);
        }

        protected string GetMappingCode(
            List<PropertyTypeAndName> properties,
            string sourceClassName,
            string targetClassName,
            string mappingClassNamespace,
            string mappingClass,
            IEnumerable<string> ignoreProperties,
            string includeNamespace,
            string lambdaExpressionName)
        {
            var usingNamespacesList = new HashSet<string>();
            var typeName = $"ClassMapping<{sourceClassName},{targetClassName}>";
            var typeSyntax = SyntaxFactory.ParseTypeName(typeName);
            var baseTypesList = new List<BaseTypeSyntax> { SyntaxFactory.SimpleBaseType(typeSyntax) };

            var modifiers = new List<SyntaxToken> { SyntaxFactory.Token(SyntaxKind.PublicKeyword) };
            var propertyList = new List<MemberDeclarationSyntax>();
            var initilizationList = new List<ExpressionSyntax>();
            var propertiesToConvert = properties
                .Where(p => !p.IsVirtual && !ignoreProperties.Contains(p.Name))
                .OrderBy(PropertySortOrder);

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var property in propertiesToConvert)
            {
                var memberAccessExpression =
                    SyntaxFactory.MemberAccessExpression(SyntaxKind.SimpleMemberAccessExpression,
                        SyntaxFactory.ParseExpression(lambdaExpressionName),
                        SyntaxFactory.IdentifierName(property.Name));

                var assignmentExpressionSyntax = SyntaxFactory.AssignmentExpression(
                    SyntaxKind.SimpleAssignmentExpression,
                    SyntaxFactory.IdentifierName(property.Name),
                    memberAccessExpression);

                initilizationList.Add(assignmentExpressionSyntax);
            }

            var initializerExpressionSyntax = SyntaxFactory.InitializerExpression(
                SyntaxKind.ObjectInitializerExpression,
                SyntaxFactory.SeparatedList(initilizationList));

            var objectCreationExpressionSyntax = SyntaxFactory.ObjectCreationExpression(
                SyntaxFactory.ParseTypeName(targetClassName))
                .WithInitializer(initializerExpressionSyntax);

            var simpleLambdaExpressionSyntax = SyntaxFactory.SimpleLambdaExpression(
                SyntaxFactory.Parameter(
                    SyntaxFactory.ParseToken(lambdaExpressionName)
                ),
                objectCreationExpressionSyntax
                );

            const string mappingProperty = "Mapping";
            var assignmentExpression = SyntaxFactory.AssignmentExpression(
                SyntaxKind.SimpleAssignmentExpression,
                SyntaxFactory.ParseExpression(mappingProperty),
                simpleLambdaExpressionSyntax);

            var ctorExpressionStatement = SyntaxFactory.ExpressionStatement(assignmentExpression);
            var constructorBody = SyntaxFactory.Block(ctorExpressionStatement);
            var constructorDeclarationSyntax = SyntaxFactory.ConstructorDeclaration(mappingClass)
                .WithBody(constructorBody)
                .WithModifiers(SyntaxFactory.TokenList(modifiers));

            propertyList.Add(constructorDeclarationSyntax);

            var namespaceDeclaration =
                SyntaxFactory.NamespaceDeclaration(SyntaxFactory.IdentifierName(mappingClassNamespace));
            var classDeclaration = SyntaxFactory.ClassDeclaration(SyntaxFactory.Identifier(mappingClass))
                .WithModifiers(SyntaxFactory.TokenList(SyntaxFactory.Token(SyntaxKind.PublicKeyword)))
                .WithMembers(SyntaxFactory.List(propertyList))
                .WithBaseList(SyntaxFactory.BaseList(SyntaxFactory.SeparatedList(baseTypesList)));

            namespaceDeclaration = namespaceDeclaration.AddMembers(classDeclaration);
            usingNamespacesList.Add(includeNamespace);
            const string abstractsNamespace = "Smt.Atomic.CrossCutting.Common.Abstacts";
            usingNamespacesList.Add(abstractsNamespace);
            var usingsList = GetUsings(usingNamespacesList);

            var compilationUnit = SyntaxFactory.CompilationUnit()
                .AddMembers(namespaceDeclaration.WithLeadingTrivia(SyntaxFactory.Comment(Environment.NewLine)))
                .WithUsings(SyntaxFactory.List(usingsList));

            return SyntaxTreeFormatter.ToString(compilationUnit);
        }

        protected virtual PropertyDeclarationSyntax AddPropertyAttribute(PropertyDeclarationSyntax propertyDeclarationSyntax, PropertyTypeAndName property)
        {
            return propertyDeclarationSyntax;
        }

        protected virtual void AddNamespaces(HashSet<string> namespaces)
        {
        }
    }
}