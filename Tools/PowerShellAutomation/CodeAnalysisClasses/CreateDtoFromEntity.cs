﻿using System.Linq;
using System.Reflection;
using PowerShellAutomation.Helpers;
using PowerShellAutomation.SupportClasses;
using Smt.Atomic.CrossCutting.Common.Helpers;

namespace PowerShellAutomation.CodeAnalysisClasses
{
    public class CreateDtoFromEntity : CreateClassFromClass
    {
        public GeneratedCode GenerateCode(
            string fullEntityClassName, 
            string dtoClassName, 
            string toDtoClassMappingName,
            string fromDtoClassMappingName, 
            string targetNamespace, 
            string entitiesAssemblyPath)
        {
            AssemblyResolver.AddAssemblyLocation(entitiesAssemblyPath);
            var assembly = Assembly.LoadFrom(entitiesAssemblyPath);
            var result = new GeneratedCode();
            var entityType = assembly.GetType(fullEntityClassName);
            var info = entityType.GetProperties().Select(p => new PropertyTypeAndName
            {
                IsVirtual = PropertyHelper.IsVirtual(p) == true,
                Name = p.Name,
                Type = CSharpAmbianceHelper.GetTypeName(p.PropertyType),
                Namespace = p.PropertyType.Namespace
            }).ToList();

            var properties = info.Where(p=>!string.IsNullOrEmpty(p.Namespace) && p.Type.StartsWith(p.Namespace));
            
            foreach (var property in properties)
            {
                property.Type = property.Type.Replace(property.Namespace + ".", string.Empty);
            }
            
            result.Code = GetTargetCode(info, dtoClassName, Enumerable.Empty<string>(), targetNamespace );
            result.ToTargetMappingCode = GetMappingCode(
                info, 
                entityType.Name, 
                dtoClassName, 
                targetNamespace,
                toDtoClassMappingName,
                Enumerable.Empty<string>(),
                entityType.Namespace,
                "e");

            var ignoreProperties = new[]
            {
                "ImpersonatedById", 
                "ModifiedById", 
                "ModifiedOn"
            };

            result.FromTargetMappingCode = GetMappingCode(
                info, 
                dtoClassName, 
                entityType.Name, 
                targetNamespace,
                fromDtoClassMappingName,
                ignoreProperties,
                entityType.Namespace,
                "d");

            return result;
        }
    }
}