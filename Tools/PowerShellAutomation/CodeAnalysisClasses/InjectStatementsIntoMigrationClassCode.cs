﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;


namespace PowerShellAutomation.CodeAnalysisClasses
{
    internal class InjectStatementsIntoMigrationClassCode 
    {
        public string GetCode(string migrationClassCode, string upScriptResourceName, string downScriptResourceName)
        {
            var tree = SyntaxFactory.ParseSyntaxTree(migrationClassCode);
            var rewriter = new InjectionRewriter(upScriptResourceName, downScriptResourceName);
            var result = rewriter.Visit(tree.GetRoot());

            return result.ToFullString();
        }

        private class InjectionRewriter : CSharpSyntaxRewriter
        {
            private readonly string _upScriptResourceName;
            private readonly string _downScriptResourceName;
            private bool _isUpMigrationMethod;
            private bool _isDownMigrationMethod;

            public InjectionRewriter(string upScriptResourceName, string downScriptResourceName)
            {
                _upScriptResourceName = upScriptResourceName;
                _downScriptResourceName = downScriptResourceName;
            }

            public override SyntaxNode VisitNamespaceDeclaration(NamespaceDeclarationSyntax node)
            {
                const string atomicDbMigrationUsingNamespace = "Smt.Atomic.Data.Entities.Helpers";
                const string altAtomicDbMigrationUsingNamespace = "Helpers";

                if (
                    node.Usings.Select(u => u.Name).OfType<QualifiedNameSyntax>().All(q => q.ToFullString() != atomicDbMigrationUsingNamespace) &&
                    node.Usings.Select(u => u.Name).OfType<IdentifierNameSyntax>().All(q => q.ToFullString() != altAtomicDbMigrationUsingNamespace) 
                    )
                {
                    var leadingTriviaList = new List<SyntaxTrivia>(node.GetLeadingTrivia());
                    leadingTriviaList.Add(SyntaxFactory.Whitespace("    "));

                    var trailingTriviaList = new List<SyntaxTrivia>
                    {
                        SyntaxFactory.CarriageReturn,
                        SyntaxFactory.LineFeed
                    };

                    return
                        base.VisitNamespaceDeclaration(
                            node.AddUsings(
                                SyntaxFactory.UsingDirective(SyntaxFactory.ParseName(altAtomicDbMigrationUsingNamespace))
                                    .NormalizeWhitespace()
                                    .WithLeadingTrivia(leadingTriviaList)
                                    .WithTrailingTrivia(trailingTriviaList)
                                    ));
                }

                return base.VisitNamespaceDeclaration(node);
            }

            public override SyntaxNode VisitBaseList(BaseListSyntax node)
            {
                const string dbMigration = "DbMigration";
                var baseClassSyntaxList = new List<BaseTypeSyntax>();
                baseClassSyntaxList.AddRange(node.Types);

                var nodesToOverwrite =
                    baseClassSyntaxList.Where(
                        b =>
                            b.Type is IdentifierNameSyntax &&
                            (b.Type as IdentifierNameSyntax).Identifier.ValueText == dbMigration).ToList();

                if (nodesToOverwrite.Count == 1)
                {
                    const string atomicDbMigration = "AtomicDbMigration"; 
                    var oldNode = nodesToOverwrite.First();
                    var idx = baseClassSyntaxList.IndexOf(oldNode);

                    baseClassSyntaxList[idx] = SyntaxFactory.SimpleBaseType(
                        SyntaxFactory.IdentifierName(atomicDbMigration)
                        ).WithLeadingTrivia(oldNode.GetLeadingTrivia()).WithTrailingTrivia(oldNode.GetTrailingTrivia());

                    return
                        base.VisitBaseList(
                            SyntaxFactory.BaseList(SyntaxFactory.SeparatedList(baseClassSyntaxList))
                                .NormalizeWhitespace()
                                .WithLeadingTrivia(node.GetLeadingTrivia())
                                .WithTrailingTrivia(node.GetTrailingTrivia()));
                }

                return base.VisitBaseList(node);
            }

            public override SyntaxNode VisitMethodDeclaration(MethodDeclarationSyntax node)
            {
                _isUpMigrationMethod = false;
                _isDownMigrationMethod = false;

                if (node.Identifier.ValueText == "Up")
                {
                    _isUpMigrationMethod = true;
                }

                if (node.Identifier.ValueText == "Down")
                {
                    _isDownMigrationMethod = true;
                }

                return base.VisitMethodDeclaration(node);
            }

            public override SyntaxNode VisitBlock(BlockSyntax node)
            {
                if (_isUpMigrationMethod && !string.IsNullOrEmpty(_upScriptResourceName))
                {
                    if (!node.Statements.Any(n => n.ToFullString().Contains(_upScriptResourceName)))
                    {
                        return base.VisitBlock(InsertStatement(node, _upScriptResourceName, false));
                    }
                }

                if (_isDownMigrationMethod && !string.IsNullOrEmpty(_downScriptResourceName))
                {
                    if (!node.Statements.Any(n => n.ToFullString().Contains(_downScriptResourceName)))
                    {
                        return base.VisitBlock(InsertStatement(node, _downScriptResourceName, true));
                    }
                }

                return base.VisitBlock(node);
            }

            private BlockSyntax InsertStatement(BlockSyntax node, string resourceName, bool reverseOrder)
            {
                const string batchExecutionMethod = "SqlBatchFromResources";
                var statementsList = new List<StatementSyntax>(node.Statements);
                if (reverseOrder)
                {
                    statementsList.Reverse();
                }

                var leadingTriviaList = new List<SyntaxTrivia>(node.GetLeadingTrivia());

                leadingTriviaList.Add(SyntaxFactory.Whitespace("    "));

                var trailingTriviaList = new List<SyntaxTrivia>
                {
                    SyntaxFactory.CarriageReturn,
                    SyntaxFactory.LineFeed
                };

                var newPos = 0;
                foreach (var statement in statementsList)
                {
                    if (!statement.WithoutLeadingTrivia().ToFullString().StartsWith(batchExecutionMethod))
                    {
                        break;
                    }
                    newPos++;
                }

                if (newPos != 0)
                {
                    leadingTriviaList.Clear();
                    leadingTriviaList.AddRange(statementsList[0].GetLeadingTrivia());
                    trailingTriviaList.Clear();
                    trailingTriviaList.AddRange(statementsList[0].GetTrailingTrivia());
                }

                statementsList.Insert(newPos,
                    SyntaxFactory.ParseStatement($"{batchExecutionMethod}(\"{resourceName}\");")
                        .WithLeadingTrivia(SyntaxFactory.TriviaList(leadingTriviaList))
                        .WithTrailingTrivia(SyntaxFactory.TriviaList(trailingTriviaList))
                    );

                if (reverseOrder)
                {
                    statementsList.Reverse();
                }

                return
                    SyntaxFactory.Block(statementsList)
                        .WithLeadingTrivia(node.GetLeadingTrivia())
                        .WithTrailingTrivia(node.GetTrailingTrivia())
                        .WithOpenBraceToken(
                            SyntaxFactory.Token(SyntaxKind.OpenBraceToken)
                                .WithLeadingTrivia(node.GetLeadingTrivia())
                                .WithTrailingTrivia(node.GetTrailingTrivia())
                        )
                        .WithCloseBraceToken(
                            SyntaxFactory.Token(SyntaxKind.CloseBraceToken)
                                .WithLeadingTrivia(node.GetLeadingTrivia())
                                .WithTrailingTrivia(node.GetTrailingTrivia()));
            }
        }

    }
}
