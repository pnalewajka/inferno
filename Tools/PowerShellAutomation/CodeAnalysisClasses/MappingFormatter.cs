using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

namespace PowerShellAutomation.CodeAnalysisClasses
{
    public class MappingFormatter : CSharpSyntaxRewriter
    {
        private readonly Stack<SyntaxTriviaList> _mappingIndentations = new Stack<SyntaxTriviaList>();
        private readonly Stack<int> _braceLevels = new Stack<int>(new [] { 0 });
        private const int IndentationSize = 4;
        private readonly IDictionary<SyntaxToken, SyntaxToken> _modifiedTokens = new Dictionary<SyntaxToken, SyntaxToken>();

        public override SyntaxToken VisitToken(SyntaxToken token)
        {
            if (IsMappingStart(token))
            {
                StartMapping(token);
            }

            if (IsMappingSeparator(token))
            {
                var newToken = token.WithTrailingTrivia(GetExtendedIndentation());

                _modifiedTokens[token] = newToken;

                return newToken;
            }

            if (IsOpenBrace(token))
            {
                _braceLevels.Push(_braceLevels.Pop() + 1);

                if (IsFirstMappingOpenBrace())
                {
                    var newToken = token.WithTrailingTrivia(GetExtendedIndentation());
                    _modifiedTokens[token] = newToken;

                    return newToken;
                }
            }

            if (IsCloseBrace(token))
            {
                _braceLevels.Push(_braceLevels.Pop() - 1);

                if (IsLastMappingCloseBrace())
                {
                    var newToken = token.WithLeadingTrivia(GetNormalIndentation());
                    EndMapping();
                    _modifiedTokens[token] = newToken;

                    return newToken;
                }
            }

            return token;
        }

        private void EndMapping()
        {
            _braceLevels.Pop();
            _mappingIndentations.Pop();
        }

        private void StartMapping(SyntaxToken token)
        {
            _braceLevels.Push(0);
            _mappingIndentations.Push(CalculateIndentation(token));
        }

        private bool IsLastMappingCloseBrace()
        {
            return _braceLevels.Peek() == 0 && HasAtLeastOneMapping();
        }

        private bool HasAtLeastOneMapping()
        {
            return _braceLevels.Count > 1;
        }

        private bool IsFirstMappingOpenBrace()
        {
            return _braceLevels.Peek() == 1 && HasAtLeastOneMapping();
        }

        private SyntaxTrivia[] GetNormalIndentation()
        {
            return new [] { SyntaxFactory.CarriageReturnLineFeed }
                .Concat(_mappingIndentations.Peek())
                .ToArray();
        }

        private SyntaxTrivia[] GetExtendedIndentation()
        {
            return
                new[] {SyntaxFactory.CarriageReturnLineFeed}
                .Concat(new[]
                    {
                        SyntaxFactory.Whitespace(new string(' ', _mappingIndentations.Peek().FullSpan.Length + IndentationSize))
                    }
                ).ToArray();
        }

        private SyntaxTriviaList CalculateIndentation(SyntaxToken token)
        {
            SyntaxToken originalOrModifiedToken;

            do
            {
                token = token.GetPreviousToken();

                if (!_modifiedTokens.TryGetValue(token, out originalOrModifiedToken))
                {
                    originalOrModifiedToken = token;
                }
            } while (!originalOrModifiedToken.HasLeadingTrivia
                && !(originalOrModifiedToken.HasTrailingTrivia && originalOrModifiedToken.TrailingTrivia.First().Kind() == SyntaxKind.EndOfLineTrivia)
            );

            return originalOrModifiedToken.HasLeadingTrivia
                ? originalOrModifiedToken.LeadingTrivia
                : originalOrModifiedToken.TrailingTrivia;
        }

        private static bool IsCloseBrace(SyntaxToken token)
        {
            return token.Kind() == SyntaxKind.CloseBraceToken;
        }

        private static bool IsOpenBrace(SyntaxToken token)
        {
            return token.Kind() == SyntaxKind.OpenBraceToken;
        }

        private static bool IsMappingStart(SyntaxToken token)
        {
            return token.Kind() == SyntaxKind.EqualsGreaterThanToken;
        }

        private bool IsMappingSeparator(SyntaxToken token)
        {
            return token.Kind() == SyntaxKind.CommaToken && HasAtLeastOneMapping();
        }
    }
}