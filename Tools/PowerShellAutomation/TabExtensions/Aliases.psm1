﻿function Update-AtomicDatabase
{
	$PSBoundParameters['ProjectName'] = 'Entities (Data)'
	$PSBoundParameters['StartupProjectName'] = 'WebApp'

	Update-Database @PSBoundParameters
}


function Add-AtomicMigration
{
	param
	(
		[Parameter(Position=0,Mandatory=1)]
		[string]$Name
	)

	if ($Name.EndsWith('Migration') -eq $False)
	{
		$Name = $Name + 'Migration'
	}

	$PSBoundParameters['Name'] = $Name
	$PSBoundParameters['ProjectName'] = 'Entities (Data)'
	$PSBoundParameters['StartupProjectName'] = 'WebApp'

	Add-Migration @PSBoundParameters
}

function Add-MergeMigration
{
	$PSBoundParameters['Name'] = 'MergeMigration' + [DateTime]::Now.ToString('ddMMyyyyHHmm')
	$PSBoundParameters['ProjectName'] = 'Entities (Data)'
	$PSBoundParameters['StartupProjectName'] = 'WebApp'

	Add-Migration @PSBoundParameters
}

#Export-ModuleMember Update-AtomicDatabase -alias up 
#Export-ModuleMember Add-AtomicMigration -alias am
#Export-ModuleMember Add-MergeMigration -alias mm
