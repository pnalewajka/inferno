﻿Register-TabExpansion 'Enable-SqlRunNowJob' @{
    'fullJobName' = { Search-SqlJob | % Name }
}
Register-TabExpansion 'Add-Entity' @{
	'Module' =  { Get-ChildItem -Path ((Get-Project -Name 'Entities (Data)').FullName | Split-Path | Join-Path -ChildPath Modules) | % { if($_.PSIsContainer) { $_.Name } } };
}
Register-TabExpansion 'Add-DtoFromEntity' @{
	'name' =  { Get-ChildItem -Path ((Get-Project -Name 'Entities (Data)').FullName | Split-Path | Join-Path -ChildPath Modules) -Filter *.cs -Recurse | % { $_.Name.Replace('.cs','')  }  };
}
Register-TabExpansion 'Add-ViewModelFromDto' @{
	'name' = { Get-ChildItem -Path ((Get-project *Business\Common*).FullName | Split-Path | Split-Path) | Where-Object {(Split-Path -Leaf $_) -ne 'Common' } | % { Get-ChildItem -Path ( Join-Path $_.FullName -ChildPath Dto) -Filter *Dto.cs} | % { $_.Name.Replace('.cs','') } };
}
Register-TabExpansion 'Add-SqlFromEnum' @{
	'schema' =  { Get-ChildItem -Path ((Get-Project -Name 'Entities (Data)').FullName | Split-Path | Join-Path -ChildPath Modules) | % { if($_.PSIsContainer) { $_.Name } } };
	'name' =  { (Get-Project -Name *CrossCutting\*) | ? { Test-Path ($_.FullName | Split-Path | Join-Path -ChildPath Enums) } | % {
		$proj = $_
		Get-ChildItem -Path ($proj.FullName | Split-Path | Join-Path -ChildPath Enums) -Filter *.cs | % { $proj.Name + "\" + $_.Name.Replace('.cs', '') }
	}  };
}
Register-TabExpansion 'Update-Database' @{
	'TargetMigration' =  { Get-ChildItem -File -Path ((Get-Project -Name 'Entities (Data)').FullName | Split-Path | Join-Path -ChildPath Migrations) | % { $_.Name -ireplace '\..+' } | Group-Object | Where-Object Count -gt 2 | % { $_.Name } };
	'StartupProjectName' = { 'WebApp' };
	'ProjectName' = { 'Entities (Data)' };
}
Register-TabExpansion 'Set-StartupProject' @{
	'ProjectName' = { 'WebApp', 'WebServices', 'PowerShellAutomation', 'JobScheduler' }
}
Register-TabExpansion 'Clear-UserExpiration' @{
    'Login' = { Search-Users | % Login }
}
Register-TabExpansion 'Set-UserPassword' @{
    'Login' = { Search-Users | % Login }
}
Register-TabExpansion 'Add-Migration' @{
	'StartupProjectName' = { 'WebApp' };
	'ProjectName' = { 'Entities (Data)' };
}
Register-TabExpansion 'Add-Project' @{
	'Location' = { 'Business', 'CrossCutting', 'Data', 'Presentation' }
} 
Register-TabExpansion 'Add-Resource' @{
	'ProjectPath' = { Get-PossibleResourceLocations };
}
Register-TabExpansion 'Add-StringsToResource' @{
	'ResourcePath' = { Get-AvailableResourceLocations };
}
Register-TabExpansion 'Get-EdmxFromMigration' @{
	'TargetMigration' = { Get-ChildItem -File -Path ((Get-Project -Name 'Entities (Data)').FullName | Split-Path | Join-Path -ChildPath Migrations) | % { $_.Name -ireplace '\..+' } | Group-Object | Where-Object Count -gt 2 | % { $_.Name } };
}
Register-TabExpansion 'Set-EdmxInMigration' @{
	'TargetMigration' = { Get-ChildItem -File -Path ((Get-Project -Name 'Entities (Data)').FullName | Split-Path | Join-Path -ChildPath Migrations) | % { $_.Name -ireplace '\..+' } | Group-Object | Where-Object Count -gt 2 | % { $_.Name } };
	'EdmxFilePath'    = { Get-ChildItem -Path $env:TEMP -Filter 'edmx_*.xml' | % FullName };
}
Register-TabExpansion 'Update-ValueInResource' @{
	'ResourcePath' = { Get-AvailableResourceLocations };
}

Register-TabExpansion 'Add-ServiceInstallerRegistration' @{
	'Project' = { Get-Project -All |  foreach { if($_.ProjectName -Match "Application") { $_.ProjectName.Replace("Application\","") } } } ; 
} 
Register-TabExpansion 'Add-CardIndexDataService' @{
	'EntityName' =  { 
		$modulesPath = (Get-Project -Name 'Entities (Data)').FullName | Split-Path | Join-Path -ChildPath Modules
		Get-ChildItem -Path $modulesPath | % { Get-ChildItem -Path $_.FullName -Filter *.cs } | % { $_.FullName.Replace("$modulesPath\",'').Replace('.cs','') } 
	};
} 
Register-TabExpansion 'Add-SiteMapMenuItem' @{
	'Module' =  { Get-ChildItem -Path ((Get-Project -Name 'Entities (Data)').FullName | Split-Path | Join-Path -ChildPath Modules) | % { if($_.PSIsContainer) { $_.Name } } };
	'Controller' = { 
		$modulesPath = (Get-Project -Name 'Entities (Data)').FullName | Split-Path | Join-Path -ChildPath Modules
		Get-ChildItem -Path $modulesPath | % { Get-ChildItem -Path $_.FullName -Filter *.cs } | % { $_.Name.Replace('.cs','') } 
	};
	'Path' = {
		$webappProject = (Get-Project -Name WebApp)
		$siteMapPath = $webappProject.FullName | Split-Path | Join-Path -ChildPath "sitemap.xml"
		[xml]$siteMapXml = Get-Content $siteMapPath -Raw
		$keys = $siteMapXml.SelectNodes('//MenuItem/@resourceKey')
		$paths = @()
		foreach($key in $keys)
		{
			$pathKeys = $siteMapXml.SelectNodes("//MenuItem[@resourceKey='$($key.Value)']/ancestor-or-self::MenuItem/@resourceKey")
			$paths += (($pathKeys | % { $_.Value }) -join '/')
		}
		return $paths
	}
}
Register-TabExpansion 'Add-CardIndex' @{
	'ModuleWithEntity' =  { 
		$modulesPath = (Get-Project -Name 'Entities (Data)').FullName | Split-Path | Join-Path -ChildPath Modules
		Get-ChildItem -Path $modulesPath | % { Get-ChildItem -Path $_.FullName -Filter *.cs } | % { $_.FullName.Replace("$modulesPath\",'').Replace('.cs','') } 
	};
}
Register-TabExpansion 'Add-CardIndexController' @{
	'ModuleWithEntity' =  { 
		$modulesPath = (Get-Project -Name 'Entities (Data)').FullName | Split-Path | Join-Path -ChildPath Modules
		Get-ChildItem -Path $modulesPath | % { Get-ChildItem -Path $_.FullName -Filter *.cs } | % { $_.FullName.Replace("$modulesPath\",'').Replace('.cs','') } 
	};
}