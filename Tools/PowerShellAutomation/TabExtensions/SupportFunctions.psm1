﻿<#
.SYNOPSIS
	This is tab expansion specific function, gets all possible resource file locations (dirs)
#>
function Get-PossibleResourceLocations()
{
	$private:projects = Get-Project -All
	$private:ignoreDirs = @('obj', 'bin', 'Scripts', 'Fonts', 'logs')
	$private:dirs = @()

	foreach($project in $projects)
	{
		$private:subDirectories = @()
		$private:name = ($private:project.ProjectName -split '\\')[-1]
		$private:path = $private:project.FullName | Split-Path
		$private:subDirectories += @( Get-ChildItem -Path $private:path -Directory | ? { !$private:ignoreDirs.Contains($_.Name) } )

		while($private:subDirectories.Length -ne 0)
		{
			($private:subDir,$private:subDirectories) = $private:subDirectories
			$private:directoryPath = $private:subDir.FullName
			$private:dirs += $private:name + $private:directoryPath.Replace($private:path,"")

			$private:newDirs = @(Get-ChildItem -Directory -Path $private:directoryPath | ? { !$private:ignoreDirs.Contains($_.Name) })

			if($private:subDirectories -eq $null)
			{
				$private:subDirectories = @()
			} 
			elseif($private:subDirectories.Length -eq 1)
			{
				$private:subDirectories = @($private:subDirectories)
			}

			$private:subDirectories += $private:newDirs
		}
	}

	$private:dirs = $private:dirs | Sort-Object

	return $private:dirs
}

<#
.SYNOPSIS
	This is tab expansion specific function, gets all resource files locations (only base name for multiple languages)
#>
function Get-AvailableResourceLocations()
{
	$private:projects = Get-Project -All
	$private:resources = @()

	foreach($project in $projects)
	{
		$private:name = ($private:project.ProjectName -split '\\')[-1]
		$private:path = $private:project.FullName | Split-Path

		$resourceFiles = Get-ChildItem -Path $private:path -Filter '*.resx' -Recurse | % FullName | % { ($_ | Split-Path ) + '\' + (($_ | Split-Path -Leaf) -split '\.')[0] } | Group-Object | % Name

		$resourceFiles | % {
				$private:resources += $private:name + $_.Replace($private:path,"")
			}
	}

	$private:resources = $private:resources | Sort-Object

	return $private:resources
}