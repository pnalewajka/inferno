﻿namespace PowerShellAutomation.Enums
{
    public enum AccessModifierEnum
    {
        None = 0,
        Private = 1,
        Public = 2
    }
}
