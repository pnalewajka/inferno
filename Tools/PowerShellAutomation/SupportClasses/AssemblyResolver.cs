﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace PowerShellAutomation.SupportClasses
{
    public static class AssemblyResolver
    {
        private static readonly Dictionary<string, string> Assemblies;

        static AssemblyResolver()
        {
            var comparer = StringComparer.CurrentCultureIgnoreCase;
            Assemblies = new Dictionary<string, string>(comparer);
            AppDomain.CurrentDomain.AssemblyResolve += ResolveHandler;
        }

        public static void AddAssemblyLocation(string path)
        {
            var name = Path.GetFileNameWithoutExtension(path);
            Debug.Assert(name != null, "name != null");

            if (!Assemblies.ContainsKey(name))
            {
                Assemblies.Add(name, path);
            }
        }

        private static Assembly ResolveHandler(object sender, ResolveEventArgs args)
        {
            var assemblyName = new AssemblyName(args.Name);
            if (Assemblies.ContainsKey(assemblyName.Name))
            {
                return Assembly.LoadFrom(Assemblies[assemblyName.Name]);
            }
            
            return null;
        }
    }
}
